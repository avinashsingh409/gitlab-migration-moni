# Atonix Apps

## Getting Started

Install the application with Node and Npm using the command `npm i`

Install nx globally with `npm install --global nx@latest` to run nx commands
without having to prefix with `npx`

## Installing Prettier

Install the `Prettier - Code formatter` extension, and set Prettier as your default formatter. Go to settings in VSCode, search for 'format', and use the settings shown below:

![prettier settings](./tools/docs/prettier.png)

## Development server

- To run any app use the nx commands: https://nx.dev/l/a/getting-started/. You can install Nx globally with `npm install -g nx`

To run the apps locally use `npx nx serve atx-md`

This will launch a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

- To run the test rigs, the folders flatten to dashes, like this: `npx nx serve test-rigs-atx-alerts`

- To debug any app locally against backend services, follow the below steps:

Update the value of 'baseMicroServicesURL' in file apps/build-specifics/local-visual-studio/config.json. This enables debugging microservices.
Ex: For alerts-internal-api-alerts service the value is http://localhost:61817

Now run the corresponding app with local config as below.

```
npx nx serve atx-ae -- --configuration local
npx nx serve atx-md -- --configuration local
npx nx serve atx-ev -- --configuration local
```

### Dependency Graphs:

```
npx nx dep-graph
```

This will give you an interactive browser for checking out the many dependencies between libraries and apps
![dependency graph](./tools/docs/dep-graph.png)

- to see all the libraries and applications a potential change has created, run: `npx nx affected:dep-graph`

## Test Setup:

![trophy](./tools/docs/testing-trophy.jpeg)

When testing an API, you want to use a `Testing Pyramid` consisting primarily of Unit Tests. However, Javascript UI applications are different. What a function call returns isn't usually what breaks; it's the complex interactions between dependencies, services, state, HTML, and CSS that will break down over time.

That is why the `Testing Trophy` (or Test Honeycomb) is the model for testing UI Applications: "Write tests. Not too many. Mostly integration". The tests are 'balanced' - not too many. Consider the return-on-investment of your tests. Not just about quantity or coverage; it's about catching errors in the future.

- E2E tests: Cypress
- Integration and Unit Tests: Jest
- Static Tests: Typescript and ESLint

Tests have value, but they also have costs.

![trophy](./tools/docs/test.png)

- Not Too Many Tests. - Kent C. Dodds. https://kentcdodds.com/blog/write-tests

> I've heard managers and teams mandating 100% code coverage for applications. That's a really bad idea. The problem is that you get diminishing returns on your tests as the coverage increases much beyond 70% (I made that number up... no science there). Why is that? Well, when you strive for 100% all the time, you find yourself spending time testing things that really don't need to be tested. Things that really have no logic in them at all (so any bugs could be caught by ESLint and Typescript). Maintaining tests like this actually really slow you and your team down.

> You may also find yourself testing implementation details just so you can make sure you get that one line of code that's hard to reproduce in a test environment. You really want to avoid testing implementation details because it doesn't give you very much confidence that your application is working and it slows you down when refactoring. You should very rarely have to change tests when you refactor code.

- Write more integration tests. https://kentcdodds.com/blog/write-tests

> The line between integration and unit tests is a little bit fuzzy. Regardless, I think the biggest thing you can do to write more integration tests is to stop mocking so much stuff. When you mock something you're removing all confidence in the integration between what you're testing and what's being mocked.

### Jest Tests

To run the tests, install the `Jest Runner` VS Code Extension. Above each test there will appear a 'Run' and 'Debug' option. This will run the individual test. The `Jest` Vscode extension also now has sidebar with a list of all tests.

From the command line use:

- `npm run test-all` will run all tests
- `npx nx test atx-chart` will just run the chart library tests

Interactively running them will be the fastest for feedback: `npx nx test atx-chart -- --watchAll`. This will give a variety of options.

Tips.

- Use the Angular Testing Library: https://timdeschryver.dev/blog/good-testing-practices-with-angular-testing-library

- Search the current tests. We have examples of `createMockWithValues`, `mockReturnValueOnce`, `jest-marbles`, `screen.getByTestId`, `jest-marbles`, `rxjs-for-await` for various testing strategies.

- If you see a `jest angular UnhandledPromiseRejectionWarning`. There is an open test that hasn't torn down. Use detectOpenHandles to find it `npx nx test atx-navigation -- --detectOpenHandles`

- If you see `TypeError: window.matchMedia is not a function`. The FlexLayout module sometimes can throw a `matchmedia` error. If this happens, add MockMatchMedia and import into the test bed:

```
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
```

then add this to the provider: `{ provide: MatchMedia, useClass: MockMatchMedia }`

### Visual Regression Testing

Percy is used for visual regression.

Don't worry about these settings unless you are debugging visual regression tests.
Mac/Unix: export PERCY_TOKEN=percyTokenNumber
Windows: set PERCY_TOKEN=percyTokenNumber
Powershell: $env:PERCY_TOKEN = "token"
Run E2E integration with Percy: `npm run cypress`

If you want to run the tests locally, and don't want them to show up in Percy, leave the PERCY_TOKEN blank, the tests will still run, but not upload.

To run the Percy Tests Locally run `npm run cypress`

To run Cypress locally to create and debug tests use: `npx nx e2e atx-md-e2e-ci --watch`

If an API changes, you will need to update the Fixture api json call. These are located in: `apps/atx-md-e2e-ci/src/fixtures/`

## CI - CD

### Test Rigs

- Task Centers
  - [Work Management](https://d2dm3odpcgy2ww.cloudfront.net/work-management)

### Deployed Apps

- atx-ae
  - [Asset Explorer App - Dev](https://dev.atonix.com/ae)
  - [Deployed Dev Version Number](https://dev.atonix.com/ae/asset360-version.html)
- atx-md
  - [Prometheus APM App - Dev](https://dev.atonix.com/md)
  - [Deployed Dev Version Number](https://dev.atonix.com/md/asset360-version.html)
- atx-ev
  - [Events App - Dev](https://dev.atonix.com/ev)
  - [Deployed Dev Version Number](https://dev.atonix.com/ev/asset360-version.html)
