Message: "Model config loaded, begin training data retrieval from 2021-06-30T10:15:06.716570 to 2022-06-30T00:15:06.716570"
ModelExtID: "29d862fc-1359-4819-90ff-6ee92860a314"
RequesterID: 2394
ValidationToken: "eyJhbGci

Message: "Model config loaded, begin training data retrieval from 2021-06-30T10:15:07.374333 to 2022-06-30T00:15:07.374333"
ModelExtID: "198090b4-54f7-43a0-b04e-92ece63bf435"
RequesterID: 2394

Model config loaded, begin training data retrieval from 2021-06-30T10:15:07.393136 to 2022-06-30T00:15:07.393136"
ModelExtID: "97d88b9b-a090-4212-b5df-a59ea54f86d7"
RequesterID: 2394

"Number of data rows retrieved: 17525 rows. Rows eliminated due to missing or invalid NaN values: 4733 rows. Rows eliminated by statistical filter of 10.0 standard deviation: 0 rows. Rows eliminated by: op mode filter 0 rows, bad tag status 0 rows. Number of data rows remained: 12792 rows."
ModelExtID: "29d862fc-1359-4819-90ff-6ee92860a314"

"Training data prepped, begin model training"
"Training data retrieved in 1secs, data row count: 12792. Begin data split and prep for training."

Active: true
ActiveSinceTime: "2022-06-29T19:14:55.087-05:00"
AlertStatusTypeID: null
AlertTypes: null
AssetVariableTypeTagMapID: 1200002947
AutoRetrain: false
BuildInterval: 7
BuildIntervalTemporalType: {TemporalTypeID: 4, TemporalTypeAbbrev: 'Days', TemporalTypeDesc: 'Days', DisplayOrder: 4}
BuildIntervalTemporalTypeID: 4
Built: true
BuiltSinceTime: "2022-06-29T19:15:48.0892158-05:00"
CalcPointID: null
Categories: []
ChangeDate: "2022-06-29T19:15:03.393-05:00"
ChangedBy: "RinckD@atonix.com"
ComputedCriticality: {ModelID: 180887, UseDefault: true, UpperCriticality: 2, LowerCriticality: 2, CreatedByUsername: 'RinckD@atonix.com', …}
CreateDate: "2022-06-29T19:15:03.393-05:00"
CreatedBy: "RinckD@atonix.com"
DefaultCriticality: {ModelID: 180887, UseDefault: true, UpperCriticality: 2, LowerCriticality: 2, CreatedByUsername: 'RinckD@atonix.com', …}
DefaultOpModeParams: null
DependentVariable: {AssetVariableTypeTagMapID: 1200002947, AssetID: 790001847, VariableTypeID: 10439, TagID: 920010128, CalcString: null, …}
DiagnosticDrilldownURL: ""
DisplayModelPath: "Live MD Test Unit:QD Steam Turbine Generator System\\QD HP Turbine"
DisplayOrder: null
Ignore: null
IgnoreLowerBound: null
IgnoreUntil: null
IgnoreUpperBound: null
IsPublic: true
LastErrorMessage: null
LastInterrogatedTime: null
LockReasonTypeID: null
Locked: false
LockedSinceTime: null
ModelConfigURL: ""
ModelDesc: ""
ModelExtID: "63E18EB4-0215-4071-B315-78BAF99D4779"
ModelID: 180887
ModelInputSeries: [{…}]
ModelInputs: [{…}]
ModelName: "QD_tag0108 - Steady State"
ModelRequest: null
ModelTemplate: {ModelTemplateID: 4681, AssetClassTypeVariableTypeID: 3915, ParentAssetClassTypeID: 5040, ModelTypeID: 1, Temporal: false, …}
ModelTemplateID: 4681
ModelType: {ModelTypeID: 1, ModelTypeAbbrev: 'APR', ModelTypeDesc: 'Advanced Pattern Recognition', ModelTypeEdictID: 1, DisplayOrder: 1, …}
ModelTypeID: 1
ModelTypeVersion: null
NDProjectName: ""
NDProjectPath: "Live MD Test Group/Live MD Test Station/Live MD Test Unit/QD Steam Turbine Generator System/QD HP Turbine"
NDResultsPath: "results"
OpModeParams: {OpModeParamsID: 2, AreaFastResponseValue: null, AreaFastResponseTime: null, AreaFastResponseTimeTemporalTypeID: null, AreaSlowResponseValue: null, …}
OpModeParamsID: 2
OpModeTypeMaps: [{…}]
PredictiveMethodMaps: (3) [{…}, {…}, {…}]
PreserveAlertStatus: null
ReferenceModelId: ""
Release: false
Temporal: false
