export interface ITaskCenterLoadData {
  taskCenterID: string;
  asset: string;
  assetTree?: boolean;
  assetTreeOpened?: boolean;
  timeSlider?: boolean;
  timeSliderOpened?: boolean;
}
