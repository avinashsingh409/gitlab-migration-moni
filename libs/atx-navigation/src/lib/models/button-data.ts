// Most of these fields are nullable because the UI should be capable of
// determining which fields to use and the user shouldn't have to fill in
// unnecessary fields.
export type ButtonType =
  | ''
  | 'asset_button'
  | 'spacer'
  | 'link'
  | 'route'
  | 'back';
export type ButtonBehaviorType = '' | 'toggle' | 'exclusive' | 'nothing';

export interface IButtonData {
  id: string;
  order?: number;
  icon?: string;
  matIcon?: string;
  image?: string;
  text?: string;
  url?: string;
  urlParams?: any;
  newTab?: boolean;
  hover?: string;
  visible?: boolean;
  gone?: boolean;
  enabled?: boolean;
  selected?: boolean;
  data?: any;
  type?: ButtonType;
  behavior?: ButtonBehaviorType;
  behaviorGroup?: string;
}
