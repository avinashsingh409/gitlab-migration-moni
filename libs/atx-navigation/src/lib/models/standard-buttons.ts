import { IButtonData } from './button-data';

export const assetNavigatorButton: IButtonData = {
  id: 'asset_tree',
  order: 1,
  icon: 'a360_assetNav',
  text: 'Asset Navigator',
  hover: 'Asset Navigator',
  visible: true,
  enabled: true,
  selected: true,
  type: 'asset_button',
};
