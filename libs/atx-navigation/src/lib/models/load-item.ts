export interface ILoadItem {
  taskCenterID: string;
  asset: string;
  data: any;
}
