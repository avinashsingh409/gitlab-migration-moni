import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { IButtonData } from '../../models/button-data';
import { Location } from '@angular/common';

@Component({
  selector: 'atx-breadcrumb-menu',
  templateUrl: './breadcrumb-menu.component.html',
  styleUrls: ['./breadcrumb-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BreadcrumbMenuComponent {
  @Input() breadcrumb: IButtonData;

  goBack() {
    this.location.back();
  }
  constructor(private location: Location) {}
}
