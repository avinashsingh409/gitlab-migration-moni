import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { LaunchPadComponent } from './launch-pad.component';
import { CarouselComponent } from '../carousel/carousel.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('LaunchPadComponent', () => {
  let component: LaunchPadComponent;
  let fixture: ComponentFixture<LaunchPadComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, AtxMaterialModule, RouterTestingModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [LaunchPadComponent, CarouselComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('show emit the Launchpad state change event', () => {
    jest.spyOn(component.launchPadStateChange, 'emit');
    component.open(null);
    expect(component.launchPadStateChange.emit).toHaveBeenCalledWith(null);
  });
});
