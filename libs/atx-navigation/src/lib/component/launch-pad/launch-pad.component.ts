import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { IButtonData } from '../../models/button-data';

@Component({
  selector: 'atx-launch-pad',
  templateUrl: './launch-pad.component.html',
  styleUrls: ['./launch-pad.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LaunchPadComponent implements OnChanges {
  public batches: Array<IButtonData[]>;
  public isShowControl: boolean;
  public width: string;
  public widthXS: string;

  @Input() navPaneState: string;
  @Input() assetTreePaneState: IButtonData;
  @Input() layoutMode: string;
  @Input() layoutLeftTraySize: number;
  @Input() launchPadItems: IButtonData[];
  @Input() splitValue = 5;
  @Output() launchPadStateChange = new EventEmitter<any>();

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.launchPadItems && changes.launchPadItems.currentValue) {
      this.computeContentWidth(changes.launchPadItems.currentValue.length);
      this.splitIntoBatches(changes.launchPadItems.currentValue);
    }
  }

  splitIntoBatches(detailsArr: IButtonData[]) {
    this.batches = [];
    if (detailsArr) {
      for (let i = 0; i < detailsArr.length; i += this.splitValue) {
        this.batches.push(
          detailsArr.slice(
            i,
            Math.min(i + this.splitValue, detailsArr.length + 1)
          )
        );
      }

      // four or fewer tiles controls/arrows will be hidden from the view
      const count = Object.keys(this.batches).length;
      this.isShowControl = count > 1;
    }
  }

  open(buttonData: IButtonData) {
    this.launchPadStateChange.emit(buttonData);
  }

  computeContentWidth(itemLength: number) {
    const divisor = itemLength <= 4 ? 4 : this.splitValue;
    this.width = 100 / divisor - 4 + '%';
    this.widthXS = 100 / divisor - 2 + '%';
  }
}
