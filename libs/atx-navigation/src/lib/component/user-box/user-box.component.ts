/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Input,
  Output,
  ChangeDetectionStrategy,
} from '@angular/core';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NavFacade } from '../../store/facade/nav.facade';
import { IUser } from '@atonix/shared/api';
import { Router } from '@angular/router';

@Component({
  selector: 'atx-user-box',
  templateUrl: './user-box.component.html',
  styleUrls: ['./user-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserBoxComponent implements OnInit {
  @Output() logout = new EventEmitter<any>();
  @Output() changeTheme = new EventEmitter<string>();
  @Input() user: IUser;
  @Input() initials: string;
  @Input() theme: string;
  @Input() showBigButtons$: Observable<boolean>;
  @Input() showSmallButtons$: Observable<boolean>;
  userSettingsUrl = `${this.appConfig.baseSiteURL}/user-settings`;
  unsubscribe$ = new Subject<any>();
  // config is used to create a link to the user settings page.
  constructor(
    private navFacade: NavFacade,
    private router: Router,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.showBigButtons$ = this.navFacade.showBigButtons$;
    this.showSmallButtons$ = this.navFacade.showSmallButtons$;
  }

  public logoutUser() {
    this.logout.emit();
  }

  public changeUserTheme(event: MatButtonToggleChange) {
    this.changeTheme.emit(event?.value);
  }

  ngOnInit() {
    this.navFacade.showBigButtons$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {});

    this.navFacade.showSmallButtons$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {});
  }

  public goToAccount() {
    this.navFacade.toggleUser(false);
    this.router.navigate(['/account-settings']);
  }
}
