import { waitForAsync } from '@angular/core/testing';
import { UserBoxComponent } from './user-box.component';
import { NavFacade } from '../../store/facade/nav.facade';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { BehaviorSubject } from 'rxjs';
import { IButtonData } from '../../models/button-data';
import { AtxMaterialModule } from '@atonix/atx-material';
import { render, screen } from '@testing-library/angular';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { Router } from '@angular/router';
describe('UserBoxComponent', () => {
  let navFacade: NavFacade;
  let router: Router;

  beforeEach(waitForAsync(async () => {
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
      showBigButtons$: new BehaviorSubject<boolean>(true),
      showSmallButtons$: new BehaviorSubject<boolean>(false),
      showAssetTreeNavItemOnly$: new BehaviorSubject<boolean>(false),
      showTimeSlider$: new BehaviorSubject<boolean>(true),
      showNotifications$: new BehaviorSubject<boolean>(true),
      navItems$: new BehaviorSubject<IButtonData[]>([
        {
          id: 'aScorecards',
          order: 1,
          icon: 'a360_scorecards',
          image: '',
          text: 'Scorecards',
          hover: 'Scorecards',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Sekoia/index.html#!?ac=Scorecards',
        },
        {
          id: 'aCurSit',
          order: 2,
          icon: 'a360_current_situation',
          image: '',
          text: 'Current Situation',
          hover: 'Current Situation',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Asset360#!?ac=Current%20Situation',
        },
      ]),
      showTimeSliderButton$: new BehaviorSubject<boolean>(true),
    });
    await render(UserBoxComponent, {
      imports: [AtxMaterialModule, NoopAnimationsModule, FlexLayoutModule],
      providers: [
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatchMedia, useClass: MockMatchMedia },
        {
          provide: NavFacade,
          useValue: navFacade,
        },
        { provide: Router, useValue: router },
      ],
      componentProperties: {
        user: {
          firstName: 'Test',
          lastName: 'This',
          email: 'test@test.com',
        },
        initials: 'TT',
      },
      declarations: [UserBoxComponent],
    });
  }));

  it(`should get current user's initials`, () => {
    const control = screen.getByTestId('initials');
    expect(control.textContent).toBe('TT');
  });
});
