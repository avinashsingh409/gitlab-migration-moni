import { NavigationComponent } from './navigation.component';
import { render, screen } from '@testing-library/angular';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { LoginComponent } from '../login/login.component';
import { SimpleLoginComponent } from '../login/simple-login/simple-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NavFacade } from '../../store/facade/nav.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthFacade, AuthService } from '@atonix/shared/state/auth';
import { AtxMaterialModule } from '@atonix/atx-material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IButtonData } from '../../models/button-data';
import { NotificationBoxComponent } from '../notification-box/notification-box.component';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('NavigationComponent', () => {
  let navFacade: NavFacade;
  let authService: AuthService;
  let authFacade: AuthFacade;
  beforeEach(async () => {
    authFacade = createMockWithValues(AuthFacade, {
      isLoggedIn$: new BehaviorSubject<boolean>(true),
      showLogin$: new BehaviorSubject<boolean>(true),
    });
    authService = createMockWithValues(AuthService, {
      onLoggedOut: new BehaviorSubject<boolean>(false),
    });
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
      showBigButtons$: new BehaviorSubject<boolean>(true),
      showSmallButtons$: new BehaviorSubject<boolean>(false),
      showAssetTreeNavItemOnly$: new BehaviorSubject<boolean>(false),
      showTimeSlider$: new BehaviorSubject<boolean>(true),
      showNotifications$: new BehaviorSubject<boolean>(true),
      navItems$: new BehaviorSubject<IButtonData[]>([
        {
          id: 'aScorecards',
          order: 1,
          icon: 'a360_scorecards',
          image: '',
          text: 'Scorecards',
          hover: 'Scorecards',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Sekoia/index.html#!?ac=Scorecards',
        },
        {
          id: 'aCurSit',
          order: 2,
          icon: 'a360_current_situation',
          image: '',
          text: 'Current Situation',
          hover: 'Current Situation',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Asset360#!?ac=Current%20Situation',
        },
      ]),
      showTimeSliderButton$: new BehaviorSubject<boolean>(true),
    });
    await render(NavigationComponent, {
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        AtxMaterialModule,
      ],
      declarations: [
        LoginComponent,
        SimpleLoginComponent,
        NotificationBoxComponent,
      ],
      providers: [
        { provide: AuthFacade, useValue: authFacade },
        {
          provide: AuthService,
          useValue: authService,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: NavFacade, useValue: navFacade },
      ],
    });
  });

  it('should create', () => {
    expect(screen.getByTestId('nav_content')).toBeInTheDocument();
  });

  it('should click help', () => {
    const control = screen.getByTestId('help_button');
    control.click();
    expect(navFacade.helpClicked).toHaveBeenCalled();
  });

  it('should toggle navigation', () => {
    const control = screen.getByTestId('toggle_nav');
    control.click();
    expect(navFacade.toggleNavigation).toHaveBeenCalled();
  });

  it('should toggle user', () => {
    const control = screen.getByTestId('user_settings');
    control.click();
    expect(navFacade.toggleUser).toHaveBeenCalledWith(true);
  });

  it('should toggle time slider', () => {
    const control = screen.getByTestId('time_slider');
    control.click();
    expect(navFacade.toggleTimeSlider).toHaveBeenCalled();
  });

  it('should get and show notifications', () => {
    const control = screen.getByTestId('notifications');
    control.click();
    expect(navFacade.toggleNotifications).toHaveBeenCalledWith(true);
  });
});
