import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import {
  OriginConnectionPosition,
  ConnectionPositionPair,
  OverlayConnectionPosition,
  ScrollStrategyOptions,
  OverlayContainer,
  Overlay,
  PositionStrategy,
} from '@angular/cdk/overlay';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NavFacade } from '../../store/facade/nav.facade';
import { NavPaneState } from '../../store/state/nav-state';
import { IButtonData } from '../../models/button-data';
import { Router } from '@angular/router';
import { INotificationItem, ISentiment, IUser } from '@atonix/shared/api';
import { AuthFacade, AuthService } from '@atonix/shared/state/auth';

// The navigation component is the main component for this library.  It provides the frame around the top and left
// of the web page.  It allows the user to login/logout and navigate to different parts of the application.
@Component({
  selector: 'atx-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent implements OnInit, OnDestroy {
  // Grab the selectors from the facade.  These observables should only fire when the
  // values change, so they should be an efficient way to display the data.
  public isLoggedIn$: Observable<boolean>;
  public user$: Observable<IUser>;
  public initials$: Observable<string>;
  public theme$: Observable<string>;
  public themeClass$: Observable<string>;
  public logo$: Observable<string>;
  public logoRoute$: Observable<string>;
  public showAppName$: Observable<boolean>;
  public appName$: Observable<string>;
  public showLogin$: Observable<boolean>;
  public showSentiment$: Observable<boolean>;
  public showLoading$: Observable<boolean>;
  public showNotifications$: Observable<boolean>;
  public notifications$: Observable<INotificationItem[][]>;
  public notificationsCount$: Observable<string>;
  public navPaneState$: Observable<NavPaneState>;
  public showBigButtons$: Observable<boolean>;
  public showSmallButtons$: Observable<boolean>;
  public layoutItems$: Observable<IButtonData[]>;
  public navItems$: Observable<IButtonData[]>;
  public showTimeSlider$: Observable<boolean>;
  public showTimeSliderButton$: Observable<boolean>;
  public showRightTrayIcon$: Observable<boolean>;

  // This will fire when the component is being destroyed as a signal
  // to the other subscriptions to unsubscribe.  It allows us to avoid
  // memory leaks from forgotten subscriptions.
  public unsubscribe$ = new Subject<void>();

  // These parameters are necessary for the overlays.
  private originPos: OriginConnectionPosition;
  private overlayPos: OverlayConnectionPosition;
  private position: ConnectionPositionPair;
  public positions: ConnectionPositionPair[];
  public scrollStrategy;
  public loginPositionStrategy: PositionStrategy;
  public loadingPositionStrategy: PositionStrategy;

  constructor(
    public auth: AuthService, // To let us know whether the user is logged in
    private matIconRegistry: MatIconRegistry, // To register icons that the navigation library will use
    private domSanitizer: DomSanitizer, // To register icons the navigation library will use
    public overlayContainer: OverlayContainer, // To apply theme on overlay based components
    private overlay: Overlay,
    sso: ScrollStrategyOptions,
    public facade: NavFacade,
    private authFacade: AuthFacade,
    private router: Router
  ) {
    this.registerIcons();
    this.scrollStrategy = sso.noop();
    this.originPos = {
      originX: 'start',
      originY: 'bottom',
    };
    this.overlayPos = {
      overlayX: 'start',
      overlayY: 'top',
    };
    this.position = new ConnectionPositionPair(
      this.originPos,
      this.overlayPos,
      0,
      5
    );
    this.positions = [this.position];

    this.loginPositionStrategy = this.overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();
    this.loadingPositionStrategy = this.overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();

    // This is a hack to fix a bug in the cdk related to displaying overlays.
    (this.loadingPositionStrategy as any).setOrigin = () => {
      return this.overlay.position().flexibleConnectedTo(null);
    };

    // This is a hack to fix a bug in the cdk related to displaying overlays.
    (this.loginPositionStrategy as any).setOrigin = () => {
      return this.overlay.position().flexibleConnectedTo(null);
    };

    // This will log the user out in the event that the token is invalid.
    // If the auth service detects a problem this will trigger.
    this.auth.onLoggedOut
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((previouslyLoggedIn) => {
        if (previouslyLoggedIn) {
          this.tokenLogout();
        }
      });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authFacade.isLoggedIn$;
    this.user$ = this.facade.user$;
    this.initials$ = this.facade.initials$;
    this.theme$ = this.facade.theme$;
    this.themeClass$ = this.facade.themeClass$;
    this.logo$ = this.facade.logo$;
    this.logoRoute$ = this.facade.logoRoute$;
    this.showAppName$ = this.facade.showAppName$;
    this.appName$ = this.facade.appName$;
    this.showLogin$ = this.authFacade.showLogin$;
    this.showSentiment$ = this.facade.showSentiment$;
    this.showLoading$ = this.facade.showLoading$;
    this.showNotifications$ = this.facade.showNotifications$;
    this.notifications$ = this.facade.notifications$;
    this.notificationsCount$ = this.facade.notificationsCount$;
    this.navPaneState$ = this.facade.navPaneState$;
    this.showBigButtons$ = this.facade.showBigButtons$;
    this.showSmallButtons$ = this.facade.showSmallButtons$;
    this.layoutItems$ = this.facade.layoutItems$;
    this.navItems$ = this.facade.navItems$;
    this.showTimeSlider$ = this.facade.showTimeSlider$;
    this.showTimeSliderButton$ = this.facade.showTimeSliderButton$;
    this.showRightTrayIcon$ = this.facade.showRightTrayIcon$;

    // this.facade.navInit('');

    // There is no good way to get the theme applied to the overlay.  This should
    // get the job done.  It would be better to be in an effect, but this is easier and
    // cleaner with fewer edge cases.
    this.theme$.pipe(takeUntil(this.unsubscribe$)).subscribe((theme) => {
      if (theme === 'light') {
        this.setOverlayContainerClass('alternative-light-theme');
      } else if (theme === 'dark') {
        this.setOverlayContainerClass('default-dark-theme');
      } else {
        this.setOverlayContainerClass(theme);
      }
    });
  }

  // This allows us to track the unique buttons.  It makes the data binding much more efficient.
  trackButton(index: number, item: IButtonData) {
    return item?.id;
  }

  toggleRightTray() {
    this.facade.toggleRightTray();
  }

  helpClicked() {
    this.facade.helpClicked();
  }

  sentimentClicked() {
    this.facade.openSentiment();
  }

  hideSentiment(sentiment?: ISentiment) {
    this.facade.closeSentiment(sentiment);
  }

  // Let the app know the user wants to toggle the menu display (Left side)
  toggleNavigation() {
    this.facade.toggleNavigation();
  }

  // Let the app know the user want to display the user popup.
  showUserDialog() {
    this.facade.toggleUser(true);
  }

  hideUserDialog() {
    this.facade.toggleUser(false);
  }

  // Let the app know the user want to change whether the time slider is shown
  toggleTimeSlider() {
    this.facade.toggleTimeSlider();
  }

  // This is different from the others.  This will actually log the user out then
  // let the app know what the new authentication state is.
  logout() {
    this.authFacade.logout();
  }

  tokenLogout() {
    this.authFacade.tokenLogout();
  }

  // This function is the event handler for the notification button's click event.
  // It calls the getAndSetNotifications function, which gets the notifications and sets them in the navigation state.
  // It then emits a state change that the user wants to show the notification tray.
  getAndShowNotifications() {
    this.facade.toggleNotifications(true);
  }

  public downloadNotification(notifications: INotificationItem[]) {
    this.facade.downloadNotification(notifications);
  }
  public viewedNotification(notification: INotificationItem) {
    this.facade.viewNotification(notification);
  }

  // This function simply hides the notification overlay when the user clicks off of it.
  // It is part of the redux pattern in that it is a stipped function in a stripped component, and
  // It passes the decision making to its parent component.
  hideNotifications() {
    this.facade.toggleNotifications(false);
  }

  layoutButtonClicked(buttonData: IButtonData) {
    this.facade.clickLayoutButton(buttonData);
  }

  navButtonClicked(buttonData: IButtonData) {
    if (!buttonData.enabled) {
      return;
    }
    this.facade.clickNavButton(buttonData);
  }

  changeTheme(theme: string) {
    this.facade.setTheme(theme);
  }

  setOverlayContainerClass(className: string) {
    const overlayContainerClasses =
      this.overlayContainer.getContainerElement().classList;
    const themeClassesToRemove = Array.from(overlayContainerClasses).filter(
      (item: string) => item.includes('-theme')
    );
    if (themeClassesToRemove.length) {
      overlayContainerClasses.remove(...themeClassesToRemove);
    }
    overlayContainerClasses.add(className || 'default-dark-theme');
  }

  /* eslint-disable max-len */
  // This shoves icons into the material design libraries.  The icons are defined
  // in code so that we don't have to make the user install a bunch of icons
  // in their applications to use the library.
  private registerIcons() {
    this.matIconRegistry.addSvgIcon(
      'a360_scorecards',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/scorecards.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_lock',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/atx-lock.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'atx_lock_open',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-lock-open.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_check',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/atx-check.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'atx_alert',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/atx-alert.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'atx_delete',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-delete.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_inform',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/issues-management.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_current_situation',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/current-situation.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_monitor',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/alerts.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'a360_events',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/events.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'a360_views',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/view-explorer.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_trends',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/data-explorer.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_trends_sm',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/data-explorer-sm.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_AssetEx',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/asset-explorer.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_sideToggle',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/side-toggle.svg`
      )
    );

    this.matIconRegistry.addSvgIcon(
      'a360_launchPad',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/launch-pad.svg`
      )
    );

    this.matIconRegistry.addSvgIcon(
      'a360_assetNav',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/asset-navigator.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_timeSlider',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/time-slider.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_notifications',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/notifications.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_modelConfig',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/model-config.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_opModeConfig',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/op-mode-config.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_beta',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/beta-feedback.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'watch',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/emblem-watch.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'diagnose',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/emblem-diagnose.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'maintainence',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/emblem-maintainence.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'alert',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/emblem-alerts.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_logo',
      this.domSanitizer.bypassSecurityTrustResourceUrl(`./assets/pg-w.svg`)
    );
    this.matIconRegistry.addSvgIcon(
      'a360_userAdmin',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/user-admin.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_modified-standard-chart',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/modified-standard-chart.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'a360_standard-chart',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/standard-chart.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_active',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-active.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_inactive',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-inactive.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_std-model',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-std-model.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_non-std',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-non-std-model.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_asset-config',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/asset-config.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'atx_recommended',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/atx-recommended.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'recommended_not_selected',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/recommended-not-selected.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'recommended_selected',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/recommended-selected.svg`
      )
    );
    this.matIconRegistry.addSvgIcon(
      'not_recommended_selected',
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `./assets/not-recommended-selected.svg`
      )
    );
  }
}
