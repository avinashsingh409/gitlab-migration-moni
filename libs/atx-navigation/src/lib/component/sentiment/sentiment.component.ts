import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ISentiment, Satisfieds } from '@atonix/shared/api';

@Component({
  selector: 'atx-sentiment',
  templateUrl: './sentiment.component.html',
  styleUrls: ['./sentiment.component.scss'],
})
export class SentimentComponent {
  sentiment: Satisfieds = '';
  text: string;

  @Output() save = new EventEmitter<ISentiment>();

  saveSentiment() {
    const result: ISentiment = {
      Disposition: this.sentiment,
      Content: this.text,
    };
    this.save.emit(result);
  }

  cancelSentiment() {
    this.save.emit(null);
  }

  setSentiment(value: Satisfieds) {
    this.sentiment = value;
  }

  textChanged(event: any) {
    this.text = event.target.value;
  }
}
