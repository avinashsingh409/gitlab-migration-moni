/* eslint-disable @typescript-eslint/no-empty-function */
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';
import moment from 'moment';
import { NavFacade } from '../../store/facade/nav.facade';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { INotificationItem } from '@atonix/shared/api';

@Component({
  selector: 'atx-notification-box',
  templateUrl: './notification-box.component.html',
  styleUrls: ['./notification-box.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationBoxComponent implements OnInit {
  @Input() notifications: INotificationItem[];
  @Output() viewed = new EventEmitter<INotificationItem>();
  @Output() download = new EventEmitter<INotificationItem[]>();
  @Input() showBigButtons$: Observable<boolean>;
  @Input() showSmallButtons$: Observable<boolean>;
  unsubscribe$ = new Subject<any>();

  constructor(private navFacade: NavFacade) {
    this.showBigButtons$ = this.navFacade.showBigButtons$;
    this.showSmallButtons$ = this.navFacade.showSmallButtons$;
  }

  public calculateAgo(myDate: Date) {
    const now = moment();
    const d = moment(myDate);
    const duration = moment.duration(now.diff(d));

    const days = duration.asDays();
    if (days > 1) {
      return String(Math.floor(days)) + 'd ago';
    }

    const hours = duration.asHours();
    if (hours > 1) {
      return String(Math.floor(hours)) + 'h ago';
    }

    const minutes = duration.asMinutes();
    if (minutes > 1) {
      return String(Math.floor(minutes)) + 'm ago';
    }

    return 'now';
  }

  public downloadNotificationFile(notifications: INotificationItem[]) {
    this.download.emit(notifications);
  }

  public newViewed(notification: INotificationItem) {
    this.viewed.emit(notification);
  }

  ngOnInit() {
    this.navFacade.showBigButtons$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {});

    this.navFacade.showSmallButtons$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {});
  }
}
