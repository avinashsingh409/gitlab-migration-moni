import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { NotificationBoxComponent } from './notification-box.component';
import { NavFacade } from '../../store/facade/nav.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { IButtonData } from '../../models/button-data';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('NotificationBoxComponent', () => {
  let component: NotificationBoxComponent;
  let fixture: ComponentFixture<NotificationBoxComponent>;
  let navFacade: NavFacade;

  beforeEach(() => {
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
      showBigButtons$: new BehaviorSubject<boolean>(true),
      showSmallButtons$: new BehaviorSubject<boolean>(false),
      showAssetTreeNavItemOnly$: new BehaviorSubject<boolean>(false),
      showTimeSlider$: new BehaviorSubject<boolean>(true),
      showNotifications$: new BehaviorSubject<boolean>(true),
      navItems$: new BehaviorSubject<IButtonData[]>([
        {
          id: 'aScorecards',
          order: 1,
          icon: 'a360_scorecards',
          image: '',
          text: 'Scorecards',
          hover: 'Scorecards',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Sekoia/index.html#!?ac=Scorecards',
        },
        {
          id: 'aCurSit',
          order: 2,
          icon: 'a360_current_situation',
          image: '',
          text: 'Current Situation',
          hover: 'Current Situation',
          visible: true,
          enabled: true,
          selected: false,
          data: '/Asset360#!?ac=Current%20Situation',
        },
      ]),
      showTimeSliderButton$: new BehaviorSubject<boolean>(true),
    });
    TestBed.configureTestingModule({
      imports: [MatIconModule, MatButtonModule, MatCardModule],
      declarations: [NotificationBoxComponent],
      providers: [
        {
          provide: NavFacade,
          useValue: navFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate ago', () => {
    let myDate = new Date();
    myDate.setMinutes(myDate.getMinutes() + 1);
    let result = component.calculateAgo(myDate);
    expect(result).toEqual('now');

    myDate = new Date();
    myDate.setMinutes(myDate.getMinutes() - 2);
    result = component.calculateAgo(myDate);
    expect(result).toEqual('2m ago');

    myDate = new Date();
    myDate.setHours(myDate.getHours() - 2);
    result = component.calculateAgo(myDate);
    expect(result).toEqual('2h ago');

    myDate = new Date();
    myDate.setDate(myDate.getDate() - 2);
    result = component.calculateAgo(myDate);
    expect(result).toEqual('2d ago');
  });
});
