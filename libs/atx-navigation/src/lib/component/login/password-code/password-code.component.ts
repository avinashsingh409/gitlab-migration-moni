import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'atx-password-code',
  templateUrl: './password-code.component.html',
  styleUrls: ['./password-code.component.scss'],
})
export class PasswordCodeComponent implements OnInit, OnDestroy {
  @Input() state: ILoginState;
  @Output() password = new EventEmitter<{
    email: string;
    password: string;
    code: string;
  }>();
  public unsubscribe$ = new Subject<void>();

  form: UntypedFormGroup = this.formBuilder.group({
    email: ['', Validators.required],
    code: ['', Validators.required],
    password1: [
      '',
      [
        Validators.minLength(10),
        Validators.required,
        Validators.pattern(
          // eslint-disable-next-line no-useless-escape
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\^$*.\[\]{}\(\)?\-“!@#%&/,><\’:;|_~`])\S{10,99}$/
        ),
      ],
    ],
    password2: ['', Validators.required],
  });
  constructor(private formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((frm) => {
        const password = frm.password1;
        const confirm = frm.password2;
        if (password) {
          if (password.length < 10) {
            this.form.get('password1').setErrors({
              length: true,
            });
          } else {
            const re =
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\^$*.[\]{}()?\-“!@#%&/,><’:;|_~`])\S{10,99}$/;
            if (!re.test(password)) {
              this.form.get('password1').setErrors({
                invalid: true,
              });
            } else {
              this.form.get('password1').setErrors(null);
            }
          }
        }
        if (password !== confirm) {
          this.form.get('password2').setErrors({ notMatched: true });
        } else {
          this.form.get('password2').setErrors(null);
        }
      });
  }
  submit() {
    if (this.form.valid) {
      this.password.emit({
        email: this.form.get('email').value.toLowerCase(),
        password: this.form.get('password1').value,
        code: this.form.get('code').value,
      });
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
