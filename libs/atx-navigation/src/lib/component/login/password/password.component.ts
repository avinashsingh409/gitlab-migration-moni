import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  AfterViewInit,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
})
export class PasswordComponent {
  @Input() userName: string;
  @Input() state: ILoginState;
  @Output() login = new EventEmitter<{
    username: string;
    password: string;
    rememberMe: boolean;
  }>();
  @Output() forgot = new EventEmitter<string>();
  @Output() code = new EventEmitter();
  @Output() goBack = new EventEmitter();

  rememberMe = true;
  loginForm: UntypedFormGroup;

  constructor(private formBuilder: UntypedFormBuilder) {
    this.loginForm = this.formBuilder.group({
      password: ['', Validators.required],
    });
  }

  goBackEmit() {
    this.goBack.emit();
  }
  haveACode() {
    this.code.emit();
  }

  forgotPassword() {
    const email = this.userName;
    // eslint-disable-next-line no-useless-escape
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      this.forgot.emit(email);
    }
  }
  rememberMeChanged(value: MatCheckboxChange) {
    this.rememberMe = value.checked;
  }

  submit() {
    if (this.loginForm.valid) {
      this.login.emit({
        username: this.userName,
        password: this.loginForm.get('password').value,
        rememberMe: this.rememberMe,
      });
    }
  }
}
