import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginComponent } from './login.component';
import { NavFacade } from '../../store/facade/nav.facade';
import {
  AuthFacade,
  ILoginState,
  initialAuthState,
  LoginStateType,
} from '@atonix/shared/state/auth';
import { BehaviorSubject } from 'rxjs';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let navFacade: NavFacade;
  let authFacade: AuthFacade;
  const initialState: any = { auth: initialAuthState };
  beforeEach(() => {
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
    });
    authFacade = createMockWithValues(AuthFacade, {
      logout: jest.fn(),
      login: jest.fn(),
      isLoggedIn$: new BehaviorSubject<boolean>(false),
      loginShowError$: new BehaviorSubject<boolean>(false),
      showLogin$: new BehaviorSubject<boolean>(true),
      loginErrorMessage$: new BehaviorSubject<string>(''),
      loginState$: new BehaviorSubject<ILoginState>(null),
      loginDisplay$: new BehaviorSubject<LoginStateType>(null),
    });

    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: AuthFacade, useValue: authFacade },
        {
          provide: NavFacade,
          useValue: navFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: Store,
          useValue: MockStore,
        },
        provideMockStore({
          initialState: {
            auth: initialState,
          },
        }),
      ],
      declarations: [LoginComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
