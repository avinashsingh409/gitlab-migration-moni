import { Component, OnInit, Input } from '@angular/core';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-login-waiting',
  templateUrl: './login-waiting.component.html',
  styleUrls: ['./login-waiting.component.scss'],
})
export class LoginWaitingComponent {
  @Input() state: ILoginState;
}
