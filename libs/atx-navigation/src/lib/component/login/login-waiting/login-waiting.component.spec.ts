import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';

import { LoginWaitingComponent } from './login-waiting.component';

describe('LoginWaitingComponent', () => {
  let component: LoginWaitingComponent;
  let fixture: ComponentFixture<LoginWaitingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [LoginWaitingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginWaitingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
