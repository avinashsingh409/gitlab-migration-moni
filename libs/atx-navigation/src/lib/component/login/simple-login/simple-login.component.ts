import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-simple-login',
  templateUrl: './simple-login.component.html',
  styleUrls: ['./simple-login.component.scss'],
})
export class SimpleLoginComponent {
  @Input() state: ILoginState;
  @Output() login = new EventEmitter<{
    username: string;
    password: string;
    rememberMe: boolean;
  }>();
  @Output() forgot = new EventEmitter<string>();
  @Output() code = new EventEmitter();

  rememberMe = true;
  loginForm: UntypedFormGroup;

  constructor(private formBuilder: UntypedFormBuilder) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.email],
      password: ['', Validators.required],
    });
  }

  submit() {
    if (this.loginForm.valid) {
      this.login.emit({
        username: this.loginForm.get('username').value.toLowerCase(),
        password: this.loginForm.get('password').value,
        rememberMe: this.rememberMe,
      });
    }
  }

  forgotPassword() {
    const email = this.loginForm.get('username').value.toLowerCase();
    // eslint-disable-next-line no-useless-escape
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      this.forgot.emit(email);
    }
  }

  rememberMeChanged(value: MatCheckboxChange) {
    this.rememberMe = value.checked;
  }

  haveACode() {
    this.code.emit();
  }
}
