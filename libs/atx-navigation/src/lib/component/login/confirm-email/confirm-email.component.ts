import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss'],
})
export class ConfirmEmailComponent {
  @Input() state: ILoginState;
  @Output() send = new EventEmitter();
  @Output() dismiss = new EventEmitter();
  @Output() code = new EventEmitter<string>();

  form = this.formBuilder.group({
    code: ['', Validators.required],
  });

  constructor(private formBuilder: UntypedFormBuilder) {}

  sendCode() {
    this.send.emit();
  }

  dismissDialog() {
    this.dismiss.emit();
  }

  submit() {
    if (this.form.valid) {
      this.code.emit(this.form.get('code').value);
    }
  }
}
