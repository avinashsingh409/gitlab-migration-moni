import { ReactiveFormsModule, FormsModule, Validators } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InitialPasswordComponent } from './initial-password.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('InitialPasswordComponent', () => {
  let component: InitialPasswordComponent;
  let fixture: ComponentFixture<InitialPasswordComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [InitialPasswordComponent],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
        AtxMaterialModule,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check password for match', () => {
    const group = component.form;

    group.patchValue({ password1: 'password1', password2: 'password1' });
    const result1 = component.checkPasswords(group);
    expect(result1).toBeNull();

    group.patchValue({ password1: 'password1', password2: 'password2' });
    const result2 = component.checkPasswords(group);
    expect(result2).toEqual({ notSame: true });
  });

  it('should reject an invalid form', () => {
    component.form.patchValue({
      name: 'something',
      password1: 'notavalidpassword',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'notavalidpassword',
      password2: 'notavalidpassword',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'notAvalidpassword',
      password2: 'notAvalidpassword',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'n0tAvalid',
      password2: 'n0tAvalid',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'n0tAvalidpassword',
      password2: 'n0tAvalidpasswor',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'n0tAvalidpassword',
      password2: 'n0tAvalidpassword',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'n0tAvalidpassword#',
      password2: 'n0tAvalidpassword',
    });
    expect(component.form.valid).toEqual(false);
    component.form.patchValue({
      password1: 'Validpassword1#',
      password2: 'Validpassword1#',
    });
    expect(component.form.valid).toEqual(true);
  });

  it('should submit', () => {
    let result: { password: string; requiredAttributes: any };
    component.password.subscribe((n) => {
      result = n;
    });
    component.form.patchValue({
      password1: 'n0tAvalidpassword#',
      password2: 'n0tAvalidpassword#',
    });
    component.submit();
    expect(result).toBeUndefined();
    component.form.patchValue({ name: 'something' });
    component.submit();
    expect(result.password).toEqual('n0tAvalidpassword#');
    expect(result.requiredAttributes.name).toEqual('something');
  });
});
