import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
  UntypedFormControl,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ILoginState } from '@atonix/shared/state/auth';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: UntypedFormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(
      control &&
      control.parent &&
      control.parent.invalid &&
      control.parent.dirty
    );

    return invalidCtrl || invalidParent;
  }
}

@Component({
  selector: 'atx-initial-password',
  templateUrl: './initial-password.component.html',
  styleUrls: ['./initial-password.component.scss'],
})
export class InitialPasswordComponent implements OnInit {
  @Input() state: ILoginState;
  @Output() password = new EventEmitter<{
    password: string;
    requiredAttributes: any;
  }>();
  form: UntypedFormGroup = this.formBuilder.group(
    {
      name: ['', Validators.required],
      password1: [
        '',
        [
          Validators.minLength(10),
          Validators.required,
          Validators.pattern(
            // eslint-disable-next-line no-useless-escape
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\^$*.\[\]{}\(\)?\-!@#%&/,><:;|_~`])[A-Za-z\d\^$*.\[\]{}\(\)?\-!@#%&/,><:;|_~`]{10,99}$/
          ),
        ],
      ],
      password2: ['', Validators.required],
    },
    { validators: this.checkPasswords }
  );
  matcher = new MyErrorStateMatcher();

  constructor(public formBuilder: UntypedFormBuilder) {}

  ngOnInit(): void {
    const name = this.state?.userAttributes?.name ?? '';
    this.form.patchValue({ name });
  }

  submit() {
    if (this.form.valid) {
      this.password.emit({
        password: this.form.get('password1').value,
        requiredAttributes: { name: this.form.get('name').value },
      });
    }
  }

  checkPasswords(group: UntypedFormGroup) {
    const p1 = group.get('password1').value;
    const p2 = group.get('password2').value;
    return p1 === p2 ? null : { notSame: true };
  }
}
