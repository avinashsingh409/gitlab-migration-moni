import { Component, OnInit, Input } from '@angular/core';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-mfa-setup',
  templateUrl: './mfa-setup.component.html',
  styleUrls: ['./mfa-setup.component.scss'],
})
export class MfaSetupComponent {
  @Input() state: ILoginState;
}
