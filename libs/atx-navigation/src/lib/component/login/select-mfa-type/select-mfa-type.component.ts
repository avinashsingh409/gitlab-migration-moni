import { Component, OnInit, Input } from '@angular/core';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-select-mfa-type',
  templateUrl: './select-mfa-type.component.html',
  styleUrls: ['./select-mfa-type.component.scss'],
})
export class SelectMfaTypeComponent {
  @Input() state: ILoginState;
}
