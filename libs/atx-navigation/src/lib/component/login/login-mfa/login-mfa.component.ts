import { Component, Input, Output, EventEmitter } from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-login-mfa',
  templateUrl: './login-mfa.component.html',
  styleUrls: ['./login-mfa.component.scss'],
})
export class LoginMFAComponent {
  @Input() state: ILoginState;
  @Output() code = new EventEmitter<string>();
  form: UntypedFormGroup = this.formBuilder.group({
    code: [
      '',
      [
        Validators.minLength(6),
        Validators.maxLength(6),
        Validators.required,
        Validators.pattern(/\d{6}/),
      ],
    ],
  });

  constructor(private formBuilder: UntypedFormBuilder) {}

  submit() {
    if (this.form.valid) {
      this.code.emit(this.form.get('code').value);
    }
  }
}
