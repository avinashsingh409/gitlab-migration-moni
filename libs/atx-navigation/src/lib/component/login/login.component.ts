import { Component } from '@angular/core';
import {
  AuthFacade,
  ILoginState,
  LoginStateType,
} from '@atonix/shared/state/auth';
import { Observable } from 'rxjs';
import { NavFacade } from '../../store/facade/nav.facade';

// This is the login component.  It will ultimately be a composite component that holds the entire
// user authentication workflow.  That includes setting up new accounts, changing passwords, MFA and
// simply logging in.  THose will probably be a bunch of different components that are displayed based
// on the user's state.
@Component({
  selector: 'atx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  logo$: Observable<string>;
  loginDisplay$: Observable<LoginStateType>;
  loginShowError$: Observable<boolean>;
  loginErrorMessage$: Observable<string>;
  loginState$: Observable<ILoginState>;

  constructor(private authFacade: AuthFacade, private facade: NavFacade) {
    this.logo$ = facade.logo$;
    this.loginDisplay$ = authFacade.loginDisplay$;
    this.loginShowError$ = authFacade.loginShowError$;
    this.loginErrorMessage$ = authFacade.loginErrorMessage$;
    this.loginState$ = authFacade.loginState$;
  }

  simpleLogin(event: {
    username: string;
    password: string;
    rememberMe: boolean;
  }) {
    this.authFacade.login(event.username, event.password, event.rememberMe);
  }

  totpCode(code: string) {
    this.authFacade.totpCode(code);
  }

  mfaCode(code: string) {
    this.authFacade.mfaCode(code);
  }

  initialPassword(value: { password: string; requiredAttributes: any }) {
    this.authFacade.initialPassword(value.password, value.requiredAttributes);
  }

  passwordCode(value: { email: string; password: string; code: string }) {
    this.authFacade.passwordCode(value.email, value.password, value.code);
  }

  forgotPasswordInit() {
    this.authFacade.forgotPasswordInit();
  }

  forgotPassword(email: string) {
    this.authFacade.forgotPassword(email);
  }

  code() {
    this.authFacade.iHaveACode();
  }
  username(value: { username: string }) {
    this.authFacade.setUserName(value.username);
  }

  goBack() {
    this.authFacade.resetLogin();
  }

  sendConfirmEmailCode() {
    this.authFacade.sendVerifyEmailMessage();
  }
  dismissConfirmEmail() {
    this.authFacade.dismissVerifyEmailDialog();
  }
  confirmEmail(code: string) {
    this.authFacade.submitVerifyEmailCode(code);
  }
}
