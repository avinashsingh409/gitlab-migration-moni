import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { CustomChallengeComponent } from './custom-challenge.component';

describe('CustomChallengeComponent', () => {
  let component: CustomChallengeComponent;
  let fixture: ComponentFixture<CustomChallengeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [CustomChallengeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
