import { Component, OnInit, Input } from '@angular/core';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-custom-challenge',
  templateUrl: './custom-challenge.component.html',
  styleUrls: ['./custom-challenge.component.scss'],
})
export class CustomChallengeComponent {
  @Input() state: ILoginState;
}
