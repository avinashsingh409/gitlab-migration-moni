import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginComponent } from './login.component';
import { UserEmailComponent } from './user-email/user-email.component';
import { NavFacade } from '../../store/facade/nav.facade';
import {
  AuthFacade,
  ILoginState,
  initialAuthState,
} from '@atonix/shared/state/auth';
import { BehaviorSubject } from 'rxjs';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { render, screen, fireEvent } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { PasswordComponent } from './password/password.component';
import { InitialPasswordComponent } from './initial-password/initial-password.component';
import { PasswordCodeComponent } from './password-code/password-code.component';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';

describe('When a user logs in', () => {
  let navFacade: NavFacade;
  let authFacade: AuthFacade;

  let fixture: ComponentFixture<LoginComponent>;
  async function renderLogin(loginState: ILoginState) {
    const authState = initialAuthState();
    authState.loginState = loginState;
    const initialState: any = { auth: authState };
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
    });

    const renderComponent = await render(LoginComponent, {
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        AuthFacade,
        {
          provide: NavFacade,
          useValue: navFacade,
        },
        { provide: MatchMedia, useClass: MockMatchMedia },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: Store,
          useValue: MockStore,
        },
        provideMockStore<any>({ initialState }),
      ],
      declarations: [
        LoginComponent,
        PasswordComponent,
        UserEmailComponent,
        InitialPasswordComponent,
        PasswordCodeComponent,
      ],
    });
    fixture = renderComponent.fixture;
    authFacade = TestBed.inject<AuthFacade>(AuthFacade);
  }

  it('should allow the user to enter an email', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    expect(screen.queryByTestId('username')).toBeInTheDocument();
    const userNameField = screen.getByTestId('username');
    const submit = screen.getByTestId('submit');

    // submit button is disabled until valid email is typed
    expect(submit).toHaveAttribute('disabled', 'true');

    const email = 'test@test.com';
    userNameField.focus();
    user.paste(email);
    // submit button is now enabled
    fireEvent.blur(userNameField);
    expect(submit).not.toHaveAttribute('disabled', 'true');

    // setUserName is called when submit button is clicked
    jest.spyOn(authFacade, 'setUserName');
    await user.click(submit);
    fixture.detectChanges();
    expect(authFacade.setUserName).toBeCalledWith(email);
  });

  it('should accept a password', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'Password',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const passwordInput = screen.getByTestId('password');
    const password = 'testPassword89@';
    passwordInput.focus();
    userEvent.paste(password);

    const submit = screen.getByTestId('submit');
    jest.spyOn(authFacade, 'login');
    await user.click(submit);
    fixture.detectChanges();
    expect(authFacade.login).toBeCalled();
  });

  it('should allow the user to change a password', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'NewPassword',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const nameInput = screen.queryByTestId('name');
    const name = 'test123@test.com';
    nameInput.focus();
    userEvent.paste(name);

    const passwordInput1 = screen.queryByTestId('password1');
    const password1 = 'Password123$';
    passwordInput1.focus();
    userEvent.paste(password1);

    const passwordInput2 = screen.queryByTestId('password2');
    const password2 = 'Password123$';
    passwordInput2.focus();
    userEvent.paste(password2);

    const submit = screen.getByTestId('submit');
    jest.spyOn(authFacade, 'initialPassword');
    await user.click(submit);
    fixture.detectChanges();
    expect(authFacade.initialPassword).toBeCalled();
  });

  it('Password reset successfully with code.', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'PasswordCode',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const emailInput = screen.queryByTestId('email');
    const email = 'test123@test.com';
    emailInput.focus();
    user.paste(email);

    const codeInput = screen.queryByTestId('code');
    const code = '123456';
    codeInput.focus();
    userEvent.paste(code);

    const passwordInput1 = screen.queryByTestId('password1');
    const password1 = 'Password123$';
    passwordInput1.focus();
    user.paste(password1);

    const passwordInput2 = screen.queryByTestId('password2');
    const password2 = 'Password123$';
    passwordInput2.focus();
    user.paste(password2);
    fireEvent.blur(passwordInput2);
    const submit = screen.getByTestId('submit');
    jest.spyOn(authFacade, 'passwordCode');

    await user.click(submit);

    fixture.detectChanges();
    expect(authFacade.passwordCode).toBeCalled();
  });

  it('Passwords mis-match. Cannot reset password', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'PasswordCode',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const submit = screen.getByTestId('submit');
    // paste instead of type speeds up test. See https://github.com/testing-library/user-event/issues/577
    const emailInput = screen.queryByTestId('email');
    const email = 'test123@test.com';
    emailInput.focus();
    userEvent.paste(email);

    const codeInput = screen.queryByTestId('code');
    const code = '123456';
    codeInput.focus();
    userEvent.paste(code);

    const passwordInput1 = screen.queryByTestId('password1');
    const password1 = 'Password123$';
    passwordInput1.focus();
    userEvent.paste(password1);

    const passwordInput2 = screen.queryByTestId('password2');
    passwordInput2.focus();
    const password2 = 'Password123!';
    userEvent.paste(password2);

    // submit button is disabled until valid email is typed
    expect(submit).toHaveAttribute('disabled', 'true');

    //correct the password
    await user.clear(passwordInput2);
    userEvent.paste(password1);

    fireEvent.blur(submit);

    // submit button is now enabled
    expect(submit).not.toHaveAttribute('disabled', 'true');
  });

  it('Verify all password criteria matches.', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'PasswordCode',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const submit = screen.getByTestId('submit');

    const emailInput = screen.queryByTestId('email');
    const email = 'test123@test.com';
    emailInput.focus();
    userEvent.paste(email);

    const codeInput = screen.queryByTestId('code');
    const code = '123456';
    codeInput.focus();
    userEvent.paste(code);

    const passwordInput1 = screen.queryByTestId('password1');
    passwordInput1.focus();
    let password1 = 'password';
    userEvent.paste(password1);

    const passwordInput2 = screen.queryByTestId('password2');
    passwordInput2.focus();
    let password2 = 'password';
    userEvent.paste(password2);

    // submit button is disabled until valid email is typed
    expect(submit).toHaveAttribute('disabled', 'true');

    //Add lower & upper-case characterss, but still invalid password
    await user.clear(passwordInput1);
    password1 = 'Password';
    passwordInput1.focus();
    userEvent.paste(password1);
    await user.clear(passwordInput2);
    password2 = 'Password';
    passwordInput2.focus;
    userEvent.paste(password2);

    fireEvent.blur(submit);
    expect(submit).toHaveAttribute('disabled', 'true');
  });

  it('Verify all password criteria matches. Special Characters', async () => {
    const user = userEvent.setup();
    const loginState: ILoginState = {
      display: 'PasswordCode',
      showError: false,
      errorMessage: '',
    };
    await renderLogin(loginState);

    const submit = screen.getByTestId('submit');

    const emailInput = screen.queryByTestId('email');
    const email = 'test123@test.com';
    emailInput.focus();
    userEvent.paste(email);

    const codeInput = screen.queryByTestId('code');
    const code = '123456';
    codeInput.focus();
    userEvent.paste(code);

    const passwordInput1 = screen.queryByTestId('password1');
    const passwordInput2 = screen.queryByTestId('password2');

    //Add lower, uppercase and number characters, but still invalid password
    let password1 = 'Password12';
    passwordInput1.focus();
    userEvent.paste(password1);
    let password2 = 'Password12';
    passwordInput2.focus();
    userEvent.paste(password2);
    expect(submit).toHaveAttribute('disabled', 'true');

    //Add lower, uppercase, number and special characters. Should be a valid password.
    await user.clear(passwordInput1);
    password1 = 'Password12!';
    passwordInput1.focus();
    userEvent.paste(password1);
    await user.clear(passwordInput2);
    password2 = 'Password12!';
    passwordInput2.focus();
    userEvent.paste(password2);
    fireEvent.blur(submit);
    // submit button is now enabled
    expect(submit).not.toHaveAttribute('disabled', 'true');
  });
});
