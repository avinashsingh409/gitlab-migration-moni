import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent {
  @Input() state: ILoginState;
  @Output() forgot = new EventEmitter<string>();
  form: UntypedFormGroup = this.formBuilder.group({
    username: [
      '',
      [
        Validators.required,
        // eslint-disable-next-line no-useless-escape
        Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/),
      ],
    ],
  });

  constructor(private formBuilder: UntypedFormBuilder) {}

  submit() {
    if (this.form.valid) {
      this.forgot.emit(this.form.get('username').value.toLowerCase());
    }
  }
}
