import { Component, Output, EventEmitter, Input } from '@angular/core';
import {
  UntypedFormBuilder,
  Validators,
  UntypedFormGroup,
} from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-login-totp',
  templateUrl: './login-totp.component.html',
  styleUrls: ['./login-totp.component.scss'],
})
export class LoginTOTPComponent {
  @Input() state: ILoginState;
  @Output() code = new EventEmitter<string>();
  form: UntypedFormGroup = this.formBuilder.group({
    code: [
      '',
      [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6),
        Validators.pattern(/\d{6}/),
      ],
    ],
  });

  constructor(private formBuilder: UntypedFormBuilder) {}

  submit() {
    if (this.form.valid) {
      this.code.emit(this.form.get('code').value);
    }
  }
}
