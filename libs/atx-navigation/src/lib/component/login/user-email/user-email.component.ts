import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  AfterViewInit,
  ElementRef,
  ViewChild,
} from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormBuilder,
  Validators,
} from '@angular/forms';
import { ILoginState } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-user-email',
  templateUrl: './user-email.component.html',
  styleUrls: ['./user-email.component.scss'],
})
export class UserEmailComponent {
  @Input() state: ILoginState;
  @Output() login = new EventEmitter<{
    username: string;
  }>();

  loginForm: UntypedFormGroup;

  constructor(private formBuilder: UntypedFormBuilder) {
    this.loginForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.email,
          Validators.required,
          Validators.pattern(
            '^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$'
          ),
        ],
      ],
    });
  }

  submit() {
    if (this.loginForm.valid) {
      this.login.emit({
        username: this.loginForm.get('username').value.toLowerCase(),
      });
    }
  }
}
