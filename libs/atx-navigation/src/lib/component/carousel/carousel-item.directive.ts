import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[atxCarouselItem]',
})
export class CarouselItemDirective {
  constructor(public tpl: TemplateRef<any>) {}
}
