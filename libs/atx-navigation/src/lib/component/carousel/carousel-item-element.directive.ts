import { Directive } from '@angular/core';

@Directive({
  // eslint-disable-next-line
  selector: '.atx-carousel-item',
})
export class CarouselItemElementDirective {}
