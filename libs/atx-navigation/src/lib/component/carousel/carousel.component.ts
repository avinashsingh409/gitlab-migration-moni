import {
  animate,
  AnimationBuilder,
  AnimationFactory,
  AnimationPlayer,
  style,
} from '@angular/animations';
import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  HostListener,
  Input,
  QueryList,
  ViewChild,
  ViewChildren,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';

import { CarouselItemDirective } from './carousel-item.directive';
import { CarouselItemElementDirective } from './carousel-item-element.directive';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'atxCarousel',
  exportAs: 'atxCarousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CarouselComponent implements AfterViewInit, OnChanges {
  @ContentChildren(CarouselItemDirective)
  items: QueryList<CarouselItemDirective>;

  @ViewChildren(CarouselItemElementDirective, { read: ElementRef })
  private itemsElements: QueryList<ElementRef>;

  @ViewChild('atxCarousel') private carousel: ElementRef;
  @Input() timing = '250ms ease-in';
  @Input() showControls = true;

  /* Please don't delete below properties.
    Changes of these are observe on ngOnchanges() */
  @Input() navPaneState: string;
  @Input() assetTreeVisibility: boolean;
  @Input() assetTreeSelected: boolean;
  @Input() layoutMode: string;
  @Input() layoutLeftTraySize: number;

  player: AnimationPlayer;
  itemWidth: number;
  currentSlide = 0;

  constructor(private builder: AnimationBuilder) {}

  /* This will observe all changes of @inputs() */
  ngOnChanges(changes: SimpleChanges): void {
    this.reSizeCarousel();
  }

  buildAnimation(offset, time: any) {
    if (isNaN(offset)) {
      offset = 0;
    }
    return this.builder.build([
      animate(
        time == null ? this.timing ?? 0 : 0,
        style({ transform: `translateX(-${offset ?? 0}px)` })
      ),
    ]);
  }

  /**
   * Progresses the carousel forward by 1 slide.
   */
  showNext() {
    let arr = this.items.toArray();
    const first = arr.shift();
    arr = arr.concat([first]);
    this.items.reset(arr);
  }

  /**
   * Regresses the carousel backwards by 1 slide.
   */
  showPrev() {
    if (this.currentSlide === 0) {
      let arr = this.items.toArray();
      const last = arr.pop();
      arr = [last].concat(arr);
      this.items.reset(arr);
      this.currentSlide++;

      this.transitionCarousel(0);
    }

    this.currentSlide =
      (this.currentSlide - 1 + this.items.length) % this.items.length;
    this.transitionCarousel(null);
  }

  ngAfterViewInit() {
    this.reSizeCarousel();
  }

  /**
   * Listens for changes to the viewport size and triggers a re-sizing of the carousel.
   */

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.reSizeCarousel();
  }

  /**
   * Re-sizes the carousel container and triggers `this.transitionCarousel()` to reset the childrens' positions.
   *
   * For use on initial load, and when changing viewport size.
   */
  reSizeCarousel(): void {
    setTimeout(() => {
      // re-size the container
      this.itemWidth =
        this?.itemsElements?.first?.nativeElement?.getBoundingClientRect()
          ?.width ?? 0;

      // trigger a fresh transition to the current slide to reset the position of the children
      this.transitionCarousel(null);
    }, 100);
  }

  /**
   * Animates the carousel to the currently selected slide.
   *
   * **You must set `this.currentSlide` before calling this method, or it will have no effect.**
   */
  transitionCarousel(time: any) {
    const offset = this.currentSlide * this.itemWidth;
    const myAnimation: AnimationFactory = this.buildAnimation(offset, time);
    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }
}
