import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CarouselItemDirective } from './carousel-item.directive';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

@Component({ template: `<div *atxCarouselItem>Something</div>` })
class TestComponent {}

describe('CarouselItem', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let ele: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [CarouselItemDirective, TestComponent],
    });
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    ele = fixture.debugElement.query(By.css('div'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
