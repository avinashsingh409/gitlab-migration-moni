import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { CarouselItemDirective } from './carousel-item.directive';
import { CarouselComponent } from './carousel.component';
import { LaunchPadComponent } from '../launch-pad/launch-pad.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('CarouselComponent', () => {
  let component: LaunchPadComponent;
  let fixture: ComponentFixture<LaunchPadComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [
        CarouselComponent,
        CarouselItemDirective,
        LaunchPadComponent,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchPadComponent);
    component = fixture.componentInstance;
    component.navPaneState = 'SOMETHING';
    component.assetTreePaneState = {
      id: 'BUTTON',
    };
    component.layoutMode = 'SOMELAYOUTTHING';
    component.layoutLeftTraySize = 100;
    component.launchPadItems = [
      {
        id: 'A',
      },
      {
        id: 'B',
      },
      {
        id: 'C',
      },
      {
        id: 'D',
      },
      {
        id: 'E',
      },
      {
        id: 'F',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const carouselComponent: CarouselComponent =
      fixture.debugElement.children[0].componentInstance;
    expect(carouselComponent).toBeTruthy();
    expect(carouselComponent.showNext).toBeTruthy();
  });

  it('should build animations', () => {
    const carouselComponent: CarouselComponent =
      fixture.debugElement.children[0].componentInstance;
    let animationFactory = carouselComponent.buildAnimation(NaN, 0);
    expect(animationFactory).toBeTruthy();
    carouselComponent.timing = null;
    animationFactory = carouselComponent.buildAnimation(null, null);
    expect(animationFactory).toBeTruthy();
  });

  it('should show next', () => {
    const carouselComponent: CarouselComponent =
      fixture.debugElement.children[0].componentInstance;
    carouselComponent.items.reset([
      { tpl: null, id: 1 } as CarouselItemDirective,
      { tpl: null, id: 2 } as CarouselItemDirective,
      { tpl: null, id: 3 } as CarouselItemDirective,
    ]);
    let items = carouselComponent.items.toArray();
    const item1 = items[0];
    expect(item1).toBeTruthy();
    expect((item1 as any).id).toEqual(1);
    carouselComponent.showNext();
    items = carouselComponent.items.toArray();
    const item2 = items[0];
    expect((item2 as any).id).toEqual(2);
    expect(item1).not.toBe(item2);
  });

  it('should show prev', () => {
    const carouselComponent: CarouselComponent =
      fixture.debugElement.children[0].componentInstance;
    carouselComponent.items.reset([
      { tpl: null, id: 1 } as CarouselItemDirective,
      { tpl: null, id: 2 } as CarouselItemDirective,
      { tpl: null, id: 3 } as CarouselItemDirective,
    ]);
    let items = carouselComponent.items.toArray();
    const item1 = items[0];
    expect(item1).toBeTruthy();
    expect((item1 as any).id).toEqual(1);
    carouselComponent.showPrev();
    items = carouselComponent.items.toArray();
    const item2 = items[0];
    expect((item2 as any).id).toEqual(3);
    expect(item1).not.toBe(item2);
  });

  it('should resize', () => {
    const carouselComponent: CarouselComponent =
      fixture.debugElement.children[0].componentInstance;
    jest.spyOn(carouselComponent, 'reSizeCarousel');
    window.dispatchEvent(new Event('resize'));
    expect(carouselComponent.reSizeCarousel).toHaveBeenCalled();
  });
});
