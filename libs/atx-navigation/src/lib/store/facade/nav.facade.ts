/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as actions from '../actions/nav.actions';
import {
  NavPaneState,
  TimeSliderPaneState,
  INavigationState,
} from '../state/nav-state';

import { IButtonData } from '../../models/button-data';
import * as selectors from '../selectors/nav.selector';
import {
  INotificationItem,
  ISentiment,
  IUserPreferences,
} from '@atonix/shared/api';

@Injectable()
export class NavFacade {
  initials$ = this.store.pipe(select(selectors.selectInitials));
  theme$ = this.store.pipe(select(selectors.selectTheme));
  logo$ = this.store.pipe(select(selectors.selectLogo));
  logoRoute$ = this.store.pipe(select(selectors.logoRoute));
  user$ = this.store.pipe(select(selectors.selectUser));
  userPreferences$ = this.store.pipe(select(selectors.selectUserPreferences));
  isWhiteListed$ = this.store.pipe(select(selectors.selectIsUserWhiteListed));
  showUserDialog$ = this.store.pipe(select(selectors.showUserDialog));
  showSentiment$ = this.store.pipe(select(selectors.showSentiment));
  showLoading$ = this.store.pipe(select(selectors.showLoading));
  themeClass$ = this.store.pipe(select(selectors.themeClass));
  showAppName$ = this.store.pipe(select(selectors.showAppName));
  appName$ = this.store.pipe(select(selectors.appName));
  showRightTrayIcon$ = this.store.pipe(select(selectors.showRightTrayIcon));
  showRightTray$ = this.store.pipe(select(selectors.rightTrayOpen));
  showNotifications$ = this.store.pipe(select(selectors.showNotifications));
  notifications$ = this.store.pipe(select(selectors.notifications));
  notificationsCount$ = this.store.pipe(select(selectors.notificationsCount));
  navPaneState$ = this.store.pipe(select(selectors.navPaneState));
  showBigButtons$ = this.store.pipe(select(selectors.showBigButtons));
  showSmallButtons$ = this.store.pipe(select(selectors.showSmallButtons));
  layoutItems$ = this.store.pipe(select(selectors.selectLayoutItems));
  navItems$ = this.store.pipe(select(selectors.selectNavItems));
  showTimeSlider$ = this.store.pipe(select(selectors.showTimeSlider));
  showTimeSliderButton$ = this.store.pipe(
    select(selectors.showTimeSliderButton)
  );
  assetTreeButton$ = this.store.pipe(select(selectors.selectAssetTreeButton));
  assetTreeSelected$ = this.store.pipe(
    select(selectors.selectAssetTreeSelected)
  );
  breadcrumb$ = this.store.pipe(select(selectors.selectBreadcrumb));
  showAssetTreeNavItemOnly$ = this.store.pipe(
    select(selectors.showAssetTreeNavItemOnly)
  );

  // We are only looking for the alerts query params, because they will be the same
  // for all the buttons. For each of the links on the Home page and on the navigation bar,
  // we need to consistently update the asset value selected so that when users click a link
  // they will be directed to the right page.
  queryParams$ = this.store.pipe(
    select(selectors.selectNavItemsById, { id: 'alerts' })
  );

  constructor(private store: Store<any>) {}

  configure(configuration: Partial<INavigationState>) {
    this.store.dispatch(actions.configure({ configuration }));
  }
  toggleNavigation(newValue?: NavPaneState) {
    this.store.dispatch(actions.toggleNavigation({ newValue }));
  }
  toggleUser(showUserDialog?: boolean) {
    this.store.dispatch(actions.toggleUser({ showUserDialog }));
  }
  setTheme(theme: string) {
    const userPreferences: Partial<IUserPreferences> = {
      Theme: theme,
    };
    this.userPreferencesChanged(userPreferences as IUserPreferences);
  }
  userPreferencesChanged(preferences: IUserPreferences) {
    this.store.dispatch(actions.userPreferencesChanged({ preferences }));
  }
  appNameChanged(appName: string) {
    this.store.dispatch(actions.appNameChanged({ appName }));
  }
  setInitialAppState() {
    this.store.dispatch(actions.setInitialAppState());
  }
  clickNavButton(button: IButtonData) {
    this.store.dispatch(actions.clickNavButton({ button }));
  }
  clickLayoutButton(button: IButtonData) {
    this.store.dispatch(actions.clickLayoutButton({ button }));
  }

  toggleNotifications(showNotifications?: boolean) {
    this.store.dispatch(actions.toggleNotifications({ showNotifications }));
  }
  toggleTimeSlider(newNavValue?: TimeSliderPaneState) {
    this.store.dispatch(actions.toggleTimeslider({ newNavValue }));
  }
  visibilityChanged(id: string, visibility: boolean) {
    this.store.dispatch(actions.visibilityChanged({ id, visibility }));
  }
  helpClicked() {
    this.store.dispatch(actions.helpClicked());
  }
  toggleLoading(showLoading?: boolean) {
    this.store.dispatch(actions.toggleLoading({ showLoading }));
  }
  openSentiment() {
    this.store.dispatch(actions.openSentiment());
  }
  closeSentiment(sentiment?: ISentiment) {
    this.store.dispatch(actions.closeSentiment({ sentiment }));
  }

  getNotifications() {
    this.store.dispatch(actions.getNotifications());
  }
  viewNotification(notification: INotificationItem) {
    this.store.dispatch(actions.viewNotification({ notification }));
  }
  downloadNotification(notifications: INotificationItem[]) {
    this.store.dispatch(actions.downloadNotification({ notifications }));
  }
  getUser() {
    this.store.dispatch(actions.getUser());
  }

  configureNavigationButton(id: string, button: Partial<IButtonData>) {
    this.store.dispatch(actions.configureNavigationButton({ id, button }));
  }
  configureLayoutButton(id: string, button: Partial<IButtonData>) {
    this.store.dispatch(actions.configureLayoutButton({ id, button }));
  }

  toggleRightTray() {
    this.store.dispatch(actions.toggleRightTray());
  }

  showRightTray() {
    this.store.dispatch(actions.showRightTray());
  }

  hideRightTray() {
    this.store.dispatch(actions.hideRightTray());
  }
}
