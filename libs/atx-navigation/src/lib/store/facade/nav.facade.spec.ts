import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import { NavFacade } from './nav.facade';
import * as actions from '../actions/nav.actions';
import {
  INavigationState,
  getInitialNavState,
  createDefaultNavigationState,
  NavPaneState,
} from '../state/nav-state';
import { IButtonData } from '../../models/button-data';
import { INotificationItem, IUser, IUserPreferences } from '@atonix/shared/api';

describe('NavFacade', () => {
  let facade: NavFacade;
  let store: MockStore<any>;
  const initialState: any = { nav: getInitialNavState() };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore<any>({ initialState }), NavFacade],
    });
    store = TestBed.inject<any>(Store);
    facade = TestBed.inject<NavFacade>(NavFacade);
  });

  it('should get initials', () => {
    let result: string;
    facade.initials$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        user: {
          firstName: 'ABBA',
          lastName: 'ZXXZ',
          email: 'email',
        },
      }),
    };
    store.setState(state);
    expect(result).toEqual('AZ');
  });

  it('should get the user', () => {
    let result: IUser;
    facade.user$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        user: {
          firstName: 'ABBA',
          lastName: 'ZXXZ',
          email: 'email',
        },
      }),
    };
    store.setState(state);
    expect(result.firstName).toEqual('ABBA');
  });

  it('should get the user preferences', () => {
    let result: IUserPreferences;
    facade.userPreferences$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        userPreferences: {
          Theme: 'dark',
          Logo: './assets/pg-w.png',
          Title: 'Prometheus APM',
        },
      }),
    };
    store.setState(state);
    expect(result).not.toBeNull();
  });

  it('should get show user dialog', () => {
    let result: boolean;
    facade.showUserDialog$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        showUserDialog: true,
      }),
    };
    store.setState(state);
    expect(result).toEqual(true);
  });

  it('should get the theme', () => {
    let result: IUserPreferences;
    facade.userPreferences$.subscribe((n) => {
      result = n;
    });

    const partialUserPreferences: Partial<IUserPreferences> = {
      Theme: 'sometheme',
    };
    const userPreferences = partialUserPreferences as IUserPreferences;

    facade.userPreferencesChanged(userPreferences as IUserPreferences);

    const state = {
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    };
    store.setState(state);
    expect(result.Theme).toEqual('sometheme');
  });

  it('should get the logo', () => {
    let logo: string;
    facade.logo$.subscribe((n) => {
      logo = n;
    });
    expect(logo).toEqual('./assets/pg-w.png');

    let result: IUserPreferences;
    facade.userPreferences$.subscribe((n) => {
      result = n;
    });

    const partialUserPreferences: Partial<IUserPreferences> = {
      Logo: './assets/ethos-theme.svg',
    };
    const userPreferences = partialUserPreferences as IUserPreferences;

    facade.userPreferencesChanged(userPreferences as IUserPreferences);

    const state = {
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    };

    store.setState(state);
    expect(result.Logo).toEqual('./assets/ethos-theme.svg');
  });

  it('should get the logo route', () => {
    let result: string;
    facade.logoRoute$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        logoRoute: 'something',
      }),
    };
    store.setState(state);
    expect(result).toEqual('something');
  });

  it('should show sentiment', () => {
    let result: boolean;
    facade.showSentiment$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        showSentiment: true,
      }),
    };
    store.setState(state);
    expect(result).toEqual(true);
  });

  it('should show loading', () => {
    let result: boolean;
    facade.showLoading$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        showLoading: true,
      }),
    };
    store.setState(state);
    expect(result).toEqual(true);
  });

  it('should get theme class', () => {
    let result: string;
    facade.themeClass$.subscribe((n) => {
      result = n;
    });

    let partialUserPreferences: Partial<IUserPreferences> = {
      Theme: 'light',
    };
    let userPreferences = partialUserPreferences as IUserPreferences;
    facade.userPreferencesChanged(userPreferences as IUserPreferences);

    store.setState({
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    });
    expect(result).toEqual('alternative-light-theme');

    partialUserPreferences = {
      Theme: 'dark',
    };
    userPreferences = partialUserPreferences as IUserPreferences;
    facade.userPreferencesChanged(userPreferences as IUserPreferences);

    store.setState({
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    });
    expect(result).toEqual('default-dark-theme');

    partialUserPreferences = {
      Theme: 'other-theme',
    };
    userPreferences = partialUserPreferences as IUserPreferences;
    facade.userPreferencesChanged(userPreferences as IUserPreferences);
    store.setState({
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    });
    expect(result).toEqual('other-theme');

    partialUserPreferences = {
      Theme: null,
    };
    userPreferences = partialUserPreferences as IUserPreferences;
    facade.userPreferencesChanged(userPreferences as IUserPreferences);
    store.setState({
      nav: createDefaultNavigationState({
        userPreferences: userPreferences,
      }),
    });
    expect(result).toEqual('default-dark-theme');
  });

  it('should get show app name', () => {
    let result: boolean;
    facade.showAppName$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        appName: null,
      }),
    });
    expect(result).toEqual(false);

    store.setState({
      nav: createDefaultNavigationState({
        appName: 'something',
      }),
    });
    expect(result).toEqual(true);
  });

  it('should get the app name', () => {
    let result: string;
    facade.appName$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        appName: 'something',
      }),
    };
    store.setState(state);
    expect(result).toEqual('something');
  });

  it('should show notifications', () => {
    let result: boolean;
    facade.showNotifications$.subscribe((n) => {
      result = n;
    });

    const state = {
      nav: createDefaultNavigationState({
        showNotifications: true,
      }),
    };
    store.setState(state);
    expect(result).toEqual(true);
  });

  it('should get notifications', () => {
    let result: INotificationItem[][];
    facade.notifications$.subscribe((n) => {
      result = n;
    });

    const notification: INotificationItem = {
      NotificationID: 1,
      NotificationGroupID: '1',
      NotificationTypeID: 2,
      Message: 'm',
      Options: 'o',
      Result: 'p',
      Viewed: true,
      CreatedDate: null,
      CreatedBy: null,
      ChangedDate: null,
      ChangedBy: null,
    };
    const state = {
      nav: createDefaultNavigationState({
        notificationList: [
          [{ ...notification }, { ...notification }],
          [{ ...notification }, { ...notification }],
        ],
      }),
    };
    store.setState(state);
    expect(result.length).toEqual(2);
  });

  it('should get the notification count', () => {
    let result: string;
    facade.notificationsCount$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        newNotificationsCount: 0,
      }),
    });
    expect(result).toEqual(null);

    store.setState({
      nav: createDefaultNavigationState({
        newNotificationsCount: 100,
      }),
    });
    expect(result).toEqual('99+');

    store.setState({
      nav: createDefaultNavigationState({
        newNotificationsCount: 3,
      }),
    });
    expect(result).toEqual('3');
  });

  it('should get the nav pane state', () => {
    let result: NavPaneState;
    facade.navPaneState$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'expanded',
      }),
    });
    expect(result).toEqual('expanded');
  });

  it('should get show big buttons', () => {
    let result: boolean;
    facade.showBigButtons$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'expanded',
      }),
    });
    expect(result).toEqual(true);

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'hidden',
      }),
    });
    expect(result).toEqual(false);

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'open',
      }),
    });
    expect(result).toEqual(false);
  });

  it('should get show small buttons', () => {
    let result: boolean;
    facade.showSmallButtons$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'expanded',
      }),
    });
    expect(result).toEqual(true);

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'hidden',
      }),
    });
    expect(result).toEqual(false);

    store.setState({
      nav: createDefaultNavigationState({
        navPaneState: 'open',
      }),
    });
    expect(result).toEqual(true);
  });

  it('should get layout items', () => {
    let result: IButtonData[];
    facade.layoutItems$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        layoutItems: [{ id: 'id1' }, { id: 'id2' }, { id: 'id3' }],
      }),
    });
    expect(result.length).toEqual(3);
    expect(result[1].id).toEqual('id2');
  });

  it('should get nav items', () => {
    let result: IButtonData[];
    facade.navItems$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navigationItems: [{ id: 'id1' }, { id: 'id2' }, { id: 'id3' }],
      }),
    });
    expect(result.length).toEqual(3);
    expect(result[1].id).toEqual('id2');
  });

  it('should show time slider', () => {
    let showTimeSlider: boolean;
    let showTimeSliderButton: boolean;
    facade.showTimeSlider$.subscribe((n) => {
      showTimeSlider = n;
    });
    facade.showTimeSliderButton$.subscribe((n) => {
      showTimeSliderButton = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        timeSliderPaneState: 'none',
      }),
    });
    expect(showTimeSlider).toEqual(false);
    expect(showTimeSliderButton).toEqual(false);

    store.setState({
      nav: createDefaultNavigationState({
        timeSliderPaneState: 'hidden',
      }),
    });
    expect(showTimeSlider).toEqual(false);
    expect(showTimeSliderButton).toEqual(true);

    store.setState({
      nav: createDefaultNavigationState({
        timeSliderPaneState: 'show',
      }),
    });
    expect(showTimeSlider).toEqual(true);
    expect(showTimeSliderButton).toEqual(true);
  });

  it('should get asset tree button', () => {
    let result: IButtonData;
    facade.assetTreeButton$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navigationItems: [
          { id: 'id1' },
          { id: 'asset_tree', selected: false },
          { id: 'id3' },
        ],
      }),
    });
    expect(result.id).toEqual('asset_tree');
    expect(result.selected).toEqual(false);
  });

  it('should get asset tree selected', () => {
    let result: boolean;
    facade.assetTreeSelected$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        navigationItems: [
          { id: 'id1' },
          { id: 'asset_tree', selected: false },
          { id: 'id3' },
        ],
      }),
    });
    expect(result).toEqual(false);

    store.setState({
      nav: createDefaultNavigationState({
        navigationItems: [
          { id: 'id1' },
          { id: 'asset_tree', selected: true },
          { id: 'id3' },
        ],
      }),
    });
    expect(result).toEqual(true);
  });
  it('should get get breadcrumb', () => {
    let result: IButtonData;
    facade.breadcrumb$.subscribe((n) => {
      result = n;
    });

    store.setState({
      nav: createDefaultNavigationState({
        breadcrumbMenu: { id: 'id1' },
      }),
    });
    expect(result.id).toEqual('id1');
  });

  it('should create', () => {
    expect(facade).toBeDefined();
  });

  it('should configure navigation', () => {
    jest.spyOn(store, 'dispatch');
    const configuration: Partial<INavigationState> = { appName: 'something' };
    facade.configure(configuration);
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.configure({ configuration })
    );
  });

  it('should toggle navigation', () => {
    jest.spyOn(store, 'dispatch');
    facade.toggleNavigation('hidden');
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.toggleNavigation({ newValue: 'hidden' })
    );
  });

  it('should toggle user', () => {
    jest.spyOn(store, 'dispatch');
    facade.toggleUser(true);
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.toggleUser({ showUserDialog: true })
    );
  });

  it('should set the theme', () => {
    jest.spyOn(store, 'dispatch');
    facade.setTheme('theme');

    const partialUserPreferences: Partial<IUserPreferences> = {
      Theme: 'theme',
    };
    const userPreferences = partialUserPreferences as IUserPreferences;
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.userPreferencesChanged({ preferences: userPreferences })
    );
  });

  it('should set app name', () => {
    jest.spyOn(store, 'dispatch');
    facade.appNameChanged('theme');
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.appNameChanged({ appName: 'theme' })
    );
  });

  it('should click nav button', () => {
    jest.spyOn(store, 'dispatch');
    facade.clickNavButton({ id: 'id1', selected: false });
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.clickNavButton({ button: { id: 'id1', selected: false } })
    );
  });

  it('click layout button', () => {
    jest.spyOn(store, 'dispatch');
    facade.clickLayoutButton({ id: 'id1', selected: false });
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.clickLayoutButton({ button: { id: 'id1', selected: false } })
    );
  });

  it('toggle notifications', () => {
    jest.spyOn(store, 'dispatch');
    facade.toggleNotifications();
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.toggleNotifications({ showNotifications: undefined })
    );
  });
  it('toggle time slider', () => {
    jest.spyOn(store, 'dispatch');
    facade.toggleTimeSlider();
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.toggleTimeslider({ newNavValue: undefined })
    );
  });
  it('visibility changed', () => {
    jest.spyOn(store, 'dispatch');
    facade.visibilityChanged('id', true);
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.visibilityChanged({ id: 'id', visibility: true })
    );
  });
  it('help clicked', () => {
    jest.spyOn(store, 'dispatch');
    facade.helpClicked();
    expect(store.dispatch).toHaveBeenCalledWith(actions.helpClicked());
  });
  it('toggle loading', () => {
    jest.spyOn(store, 'dispatch');
    facade.toggleLoading();
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.toggleLoading({ showLoading: undefined })
    );
  });
  it('open sentiment', () => {
    jest.spyOn(store, 'dispatch');
    facade.openSentiment();
    expect(store.dispatch).toHaveBeenCalledWith(actions.openSentiment());
  });
  it('close sentiment', () => {
    jest.spyOn(store, 'dispatch');
    facade.closeSentiment();
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.closeSentiment({ sentiment: undefined })
    );
  });
  it('get notifications', () => {
    jest.spyOn(store, 'dispatch');
    facade.getNotifications();
    expect(store.dispatch).toHaveBeenCalledWith(actions.getNotifications());
  });
  it('view notifications', () => {
    const n: INotificationItem = {
      NotificationID: 1,
      NotificationGroupID: '2',
      NotificationTypeID: 3,
      Message: 'm',
      Options: 'n',
      Result: 'o',
      Viewed: false,
      CreatedDate: undefined,
      CreatedBy: 'cb',
      ChangedDate: undefined,
      ChangedBy: 'cb',
    };
    jest.spyOn(store, 'dispatch');
    facade.viewNotification(n);
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.viewNotification({ notification: n })
    );
  });
  it('download notification', () => {
    const n: INotificationItem = {
      NotificationID: 1,
      NotificationGroupID: '2',
      NotificationTypeID: 3,
      Message: 'm',
      Options: 'n',
      Result: 'o',
      Viewed: false,
      CreatedDate: undefined,
      CreatedBy: 'cb',
      ChangedDate: undefined,
      ChangedBy: 'cb',
    };
    jest.spyOn(store, 'dispatch');
    facade.downloadNotification([n]);
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.downloadNotification({ notifications: [n] })
    );
  });
  it('get user', () => {
    jest.spyOn(store, 'dispatch');
    facade.getUser();
    expect(store.dispatch).toHaveBeenCalledWith(actions.getUser());
  });

  it('configure navigation button', () => {
    jest.spyOn(store, 'dispatch');
    facade.configureNavigationButton('id', { selected: false });
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.configureNavigationButton({
        id: 'id',
        button: { selected: false },
      })
    );
  });
  it('configure layout button', () => {
    jest.spyOn(store, 'dispatch');
    facade.configureLayoutButton('id', { selected: false });
    expect(store.dispatch).toHaveBeenCalledWith(
      actions.configureLayoutButton({ id: 'id', button: { selected: false } })
    );
  });
});
