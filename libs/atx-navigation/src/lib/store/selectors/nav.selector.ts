/* eslint-disable ngrx/prefix-selectors-with-select */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { INavigationState } from '../state/nav-state';
import { getInitials } from '../../service/utilities/getInitials';
import { IButtonData } from '../../models/button-data';

export const navState = createFeatureSelector<INavigationState>('nav');

export const selectUser = createSelector(navState, (state) => state?.user);

export const selectUserPreferences = createSelector(
  navState,
  (state) => state?.userPreferences
);
export const selectInitials = createSelector(selectUser, (user) => {
  return getInitials(user);
});
export const selectIsUserWhiteListed = createSelector(
  selectUserPreferences,
  (user) => {
    return user?.Title !== null && user?.Logo !== null;
  }
);
export const selectTheme = createSelector(
  navState,
  (state) => state?.userPreferences?.Theme
);
export const selectLogo = createSelector(
  selectUserPreferences,
  (userPreferences) => {
    return userPreferences.Logo === null
      ? './assets/pg-w.png'
      : `./assets/${userPreferences.Logo}`;
  }
);
export const selectNavAsset = createSelector(navState, (state) => state?.asset);
export const logoRoute = createSelector(navState, (state) => state?.logoRoute);
export const appName = createSelector(navState, (state) => state.appName);
export const showAppName = createSelector(appName, (state) => {
  if (state !== undefined && state !== null && state !== '') {
    return true;
  }
  return false;
});

export const showRightTrayIcon = createSelector(
  navState,
  (state) => state.showRightTrayIcon
);
export const rightTrayOpen = createSelector(
  navState,
  (state) => state.rightTrayOpen
);

export const showUserDialog = createSelector(
  navState,
  (state) => state?.showUserDialog ?? false
);

export const showSentiment = createSelector(
  navState,
  (state) => state?.showSentiment ?? false
);
export const showLoading = createSelector(
  navState,
  (state) => state?.showLoading ?? false
);

export const showNotifications = createSelector(
  navState,
  (state) => state?.showNotifications ?? false
);
export const notifications = createSelector(
  navState,
  (state) => state?.notificationList
);
export const notificationsCount = createSelector(navState, (state) => {
  const count = state?.newNotificationsCount;
  if (count === undefined || count === null || count <= 0) {
    return null;
  } else if (count > 99) {
    return '99+';
  } else {
    return String(count);
  }
});

export const navPaneState = createSelector(
  navState,
  (state) => state.navPaneState
);
export const showBigButtons = createSelector(
  navPaneState,
  (state) => state === 'expanded'
);
export const showSmallButtons = createSelector(
  navPaneState,
  (state) => state !== 'hidden'
);

export const themeClass = createSelector(selectTheme, (theme) => {
  if (theme === 'light') {
    return 'alternative-light-theme';
  } else if (theme === 'dark') {
    return 'default-dark-theme';
  } else {
    return theme || 'default-dark-theme';
  }
});

export const selectLayoutItems = createSelector(
  navState,
  (state) => state.layoutItems
);
export const selectNavItems = createSelector(
  navState,
  (state) => state.navigationItems
);

export const showTimeSlider = createSelector(
  navState,
  (state) => state.timeSliderPaneState === 'show'
);
export const showTimeSliderButton = createSelector(
  navState,
  (state) => state.timeSliderPaneState !== 'none'
);

export const selectAssetTreeButton = createSelector(
  selectNavItems,
  (navItems) => {
    return navItems?.find((n) => n.id === 'asset_tree');
  }
);

export const selectAssetTreeSelected = createSelector(
  selectAssetTreeButton,
  (state) => state?.selected ?? false
);

export const selectBreadcrumb = createSelector(
  navState,
  (state) => state?.breadcrumbMenu
);

export const selectNavItemsById = createSelector(
  navState,
  (nav: { navigationItems: any[] }, props: { id: any }) => {
    if (props) {
      const navItem: IButtonData = nav?.navigationItems.find(
        (x) => x.id === props.id
      );
      if (navItem) {
        return navItem.urlParams;
      }
    }
  }
);

export const showAssetTreeNavItemOnly = createSelector(
  navState,
  (state) => state?.showAssetTreeNavItemOnly
);
