import { IButtonData } from '../../models/button-data';
import {
  IUserAccount,
  INotificationItem,
  IUser,
  IUserPreferences,
} from '@atonix/shared/api';

export type NavPaneState = 'hidden' | 'open' | 'expanded';
export type TimeSliderPaneState = 'none' | 'show' | 'hidden';

export interface INavigationState {
  user: IUser;
  userPreferences: IUserPreferences;
  asset: string;
  logoRoute?: string;
  appName: string;
  navPaneState: NavPaneState;
  navigationItems: IButtonData[];
  showUserDialog: boolean;
  showSentiment?: boolean;
  layoutItems: IButtonData[];
  showNotifications: boolean;
  notificationList: INotificationItem[][];
  newNotifications: boolean;
  newNotificationsCount: number;
  timeSliderPaneState: TimeSliderPaneState;
  breadcrumbMenu?: IButtonData;
  showLoading?: boolean;
  showRightTrayIcon: boolean;
  rightTrayOpen: boolean;
  showAssetTreeNavItemOnly: boolean;
}

export const defaultNavigationState: INavigationState = {
  appName: '',
  asset: null,
  navPaneState: 'open',
  showUserDialog: false,
  newNotifications: false,
  newNotificationsCount: 0,
  user: {
    firstName: null,
    lastName: null,
    email: null,
    customerId: null,
  },
  userPreferences: {
    Title: null,
    Logo: null,
    Theme: 'dark',
  },
  navigationItems: [],
  layoutItems: [],
  showNotifications: false,
  notificationList: [],
  timeSliderPaneState: 'none',
  breadcrumbMenu: null,
  showRightTrayIcon: false,
  rightTrayOpen: true,
  showLoading: false,
  showAssetTreeNavItemOnly: false,
};

export function getInitialNavState() {
  return defaultNavigationState;
}
export function createDefaultNavigationState(
  navState?: Partial<INavigationState>
) {
  return { ...defaultNavigationState, ...navState };
}
