/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Injectable, Inject } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import * as navActions from '../actions/nav.actions';
import { NavModelService } from '../../service/model/nav-model.service';
import { of } from 'rxjs';
import { catchError, switchMap, map, filter, tap } from 'rxjs/operators';
import { AuthActions, AuthService } from '@atonix/shared/state/auth';
import { LoggerService } from '@atonix/shared/utils';
import { NotificationBannerComponent } from '@atonix/shared/ui';
import {
  JobsFrameworkService,
  AccountFrameworkService,
  UIConfigFrameworkService,
  ImagesFrameworkService,
} from '@atonix/shared/api';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { RouteSelectors } from '@atonix/shared/state/router';
import { Store } from '@ngrx/store';

@Injectable()
export class NavEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private navModel: NavModelService,
    private accountFrameworkService: AccountFrameworkService,
    private imagesFrameworkService: ImagesFrameworkService,
    private authService: AuthService,
    private jobsFrameworkService: JobsFrameworkService,
    private uiConfigFrameworkService: UIConfigFrameworkService,
    private logger: LoggerService,
    private snackbar: MatSnackBar,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  initializeNav$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.authenticatedSuccess),
      switchMap(() => {
        const windowHostName = window.location.hostname;
        if (
          this.appConfig.enableAnnouncement === 1 &&
          windowHostName.search(/.atonix.com/gi) === -1
        ) {
          this.snackbar.openFromComponent(NotificationBannerComponent, {
            verticalPosition: 'top',
            horizontalPosition: 'center',
            duration: 7000,
            panelClass: 'notification-banner-container',
          });
        }

        return [
          navActions.getUser(),
          navActions.getUserPreferences(),
          navActions.getNotifications(),
        ];
      })
    )
  );

  getUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(navActions.getUser),
      switchMap(() =>
        this.accountFrameworkService.user().pipe(
          map((user) => navActions.getUserSuccess({ user })),
          catchError((error) => of(navActions.getUserFailure({ error })))
        )
      )
    )
  );

  getUserPreferences$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        navActions.getUserPreferences,
        navActions.userPreferencesChangedSuccess
      ),
      switchMap(() =>
        this.accountFrameworkService.userPreferences().pipe(
          map((userPreferences) =>
            navActions.getUserPreferencesSuccess({ user: userPreferences })
          ),
          catchError((error) =>
            of(navActions.getUserPreferencesFailure({ error }))
          )
        )
      )
    )
  );

  userPreferencesChanged$ = createEffect(() =>
    this.actions$.pipe(
      ofType(navActions.userPreferencesChanged),
      map((n) => n.preferences),
      switchMap((preferences) =>
        this.accountFrameworkService.setUserPreferences(preferences).pipe(
          map((n) =>
            navActions.userPreferencesChangedSuccess({ preferences: n })
          ),
          catchError((error) =>
            of(navActions.userPreferencesChangedFailure({ error }))
          )
        )
      )
    )
  );

  userPreferencesChangedSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(navActions.userPreferencesChangedSuccess),
        concatLatestFrom(() =>
          this.store.select(RouteSelectors.selectRouterTitle)
        ),
        tap(([preferences, routerTitle]) => {
          if (routerTitle && routerTitle.toLowerCase() === 'data explorer') {
            location.reload();
          }
        })
      ),
    {
      dispatch: false,
    }
  );

  getNotifications$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        navActions.getNotifications,
        navActions.toggleNotifications,
        navActions.viewNotificationSuccess
      ),
      switchMap(() =>
        this.jobsFrameworkService.getNotifications().pipe(
          map((notifications) =>
            navActions.getNotificationsSuccess({
              notifications,
              notificationsCount: 0,
            })
          ),
          catchError((error) =>
            of(navActions.getNotificationsFailure({ error }))
          )
        )
      )
    )
  );

  viewNotification$ = createEffect(() =>
    this.actions$.pipe(
      ofType(navActions.viewNotification),
      map((n) => n.notification),
      switchMap((notification) =>
        this.navModel.viewed(notification).pipe(
          map(() => navActions.viewNotificationSuccess()),
          catchError((error) => of(navActions.viewNotificationError({ error })))
        )
      )
    )
  );

  downloadNotifications$ = createEffect(() =>
    this.actions$.pipe(
      ofType(navActions.downloadNotification),
      map((n) => n.notifications),
      switchMap((notifications) =>
        this.imagesFrameworkService
          .downloadNotificationFile(notifications)
          .pipe(
            map((url) => navActions.downloadNotificationSuccess()),
            catchError((error) =>
              of(navActions.downloadNotificationFailure({ error }))
            )
          )
      )
    )
  );

  helpClicked$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(navActions.helpClicked),
        switchMap(() => this.authService.getHelpUrl()),
        tap((helpUrl) => {
          window.open(helpUrl, '_blank');
        })
      ),
    { dispatch: false }
  );

  navButton$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(navActions.clickNavButton),
        tap((n) => {
          this.logger.feature('navbutton_' + n.button.id, 'nav');
        })
      ),
    { dispatch: false }
  );

  layoutButton$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(navActions.clickLayoutButton),
        tap((n) => {
          this.logger.feature('layoutbutton_' + n.button.id, 'nav');
        })
      ),
    { dispatch: false }
  );

  saveSentiment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(navActions.closeSentiment),
      filter((n) => !!n.sentiment),
      switchMap((n) =>
        this.uiConfigFrameworkService.saveSentiment(n.sentiment).pipe(
          map(() => navActions.saveSentimentSuccess()),
          catchError((error) => of(navActions.saveSentimentFailure()))
        )
      )
    )
  );

  helpClick$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(navActions.helpClick),
        map((n) => n.url),
        tap((url) => {
          window.open(url, '_blank');
        })
      ),
    { dispatch: false }
  );
}
