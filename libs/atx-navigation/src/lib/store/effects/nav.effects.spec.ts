import { TestBed, inject } from '@angular/core/testing';
import { Store, Action } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Subject, Observable, of, BehaviorSubject } from 'rxjs';
import { hot, cold } from 'jest-marbles';
import { NavModelService } from '../../service/model/nav-model.service';
import { getInitialNavState } from '../state/nav-state';
import { NavEffects } from './nav.effects';
import {
  viewNotification,
  viewNotificationError,
  viewNotificationSuccess,
} from '../actions/nav.actions';
import { RouterTestingModule } from '@angular/router/testing';
import { AssetTreeDBService } from '@atonix/atx-indexed-db';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { INotificationItem } from '@atonix/shared/api';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { render, screen } from '@testing-library/angular';

class FakeAssetTreeDBService {
  addAssetTreesOnIndxDB() {
    return of(null);
  }
  addAssetTreeNodesOnIndxDB() {
    return of(null);
  }

  addDefaultTreeNode() {
    return of(null);
  }
}

describe('NavEffects', () => {
  let navModelService: NavModelService;
  let effects: NavEffects;
  let actions$: Observable<Action>;
  let snackBar: MatSnackBar;

  beforeEach(() => {
    const initialState: any = { nav: getInitialNavState() };
    actions$ = new Subject<Action>();
    snackBar = createMock(MatSnackBar);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockActions(() => actions$),
        provideMockStore<any>({ initialState }),
        {
          provide: NavModelService,
          useValue: {
            notificationViewed2: jest.fn(),
            viewed: jest.fn(),
          },
        },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: AssetTreeDBService, useClass: FakeAssetTreeDBService },
        {
          provide: MatSnackBar,
          useValue: snackBar,
        },
        NavEffects,
      ],
    });
    navModelService = TestBed.inject<NavModelService>(NavModelService);
    effects = TestBed.inject<NavEffects>(NavEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
  it('view notification', () => {
    const notification: INotificationItem = {
      NotificationID: 1,
      NotificationGroupID: 'a',
      NotificationTypeID: 1,
      Message: 'a',
      Options: 'a',
      Result: 'a',
      Viewed: false,
      CreatedDate: null,
      CreatedBy: 'a',
      ChangedDate: null,
      ChangedBy: 'a',
    };
    const action = viewNotification({ notification });
    actions$ = hot('--a-', {
      a: action,
    });
    const expected = cold('--(b)', { b: viewNotificationSuccess() });
    navModelService.viewed = jest.fn(() => new BehaviorSubject<boolean>(true));
    expect(effects.viewNotification$).toBeObservable(expected);
  });

  it('view notification error', () => {
    const notification: INotificationItem = {
      NotificationID: 1,
      NotificationGroupID: 'a',
      NotificationTypeID: 1,
      Message: 'a',
      Options: 'a',
      Result: 'a',
      Viewed: false,
      CreatedDate: null,
      CreatedBy: 'a',
      ChangedDate: null,
      ChangedBy: 'a',
    };

    actions$ = hot('a-b', {
      a: viewNotification({ notification }),
      b: viewNotification({ notification }),
    });
    jest
      .spyOn(navModelService, 'viewed')
      .mockReturnValueOnce(cold('#', undefined, { message: 'message' }))
      .mockReturnValueOnce(cold('a|', {}));
    const expected = hot('a-b', {
      a: viewNotificationError({ error: { message: 'message' } }),
      b: viewNotificationSuccess(),
    });
    expect(effects.viewNotification$).toBeObservable(expected);
  });
});
