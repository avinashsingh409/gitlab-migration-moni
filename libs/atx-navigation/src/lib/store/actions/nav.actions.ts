/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { IButtonData } from '../../models/button-data';
import {
  NavPaneState,
  TimeSliderPaneState,
  INavigationState,
} from '../state/nav-state';

import { ITaskCenterLoadData } from '../../models/task-center-load-data';
import { ILoadItem } from '../../models/load-item';
import {
  INotificationItem,
  ISentiment,
  IUser,
  IUserPreferences,
} from '@atonix/shared/api';

export const configure = createAction(
  '[Navigation] Configure',
  props<{ configuration: Partial<INavigationState> }>()
);

export const setInitialAppState = createAction(
  '[Navigation] Set Initial App State'
);

export const getUser = createAction('[Navigation] Get User');
export const getUserSuccess = createAction(
  '[Navigation] Get User Success',
  props<{ user: IUser }>()
);
export const getUserFailure = createAction(
  '[Navigation] Get User Failure',
  props<{ error }>()
);

export const getUserPreferences = createAction(
  '[Navigtion] Get User Preferences'
);
export const getUserPreferencesSuccess = createAction(
  '[Navigation] Get User Preferences Success',
  props<{ user: IUserPreferences }>()
);
export const getUserPreferencesFailure = createAction(
  '[Navigation] Get User Preferences Failure',
  props<{ error }>()
);
export const userPreferencesChanged = createAction(
  '[Navigation] User Preferences Changed',
  props<{ preferences: IUserPreferences }>()
);
export const userPreferencesChangedSuccess = createAction(
  '[Navigation] User Preferences Changed Success',
  props<{ preferences: IUserPreferences }>()
);
export const userPreferencesChangedFailure = createAction(
  '[Navigation] User Preferences Changed Failure',
  props<{ error }>()
);

export const getNotifications = createAction('[Navigation] Get Notifications');
export const getNotificationsSuccess = createAction(
  '[Navigation] Get Notifications Success',
  props<{
    notifications: INotificationItem[][];
    notificationsCount: number;
  }>()
);
export const getNotificationsFailure = createAction(
  '[Navigation] Get Notifications Failure',
  props<{ error }>()
);
export const toggleNotifications = createAction(
  '[Navigation] Toggle Notifications',
  props<{ showNotifications?: boolean }>()
);

export const downloadNotification = createAction(
  '[Navigation] Download Notification',
  props<{ notifications: INotificationItem[] }>()
);
export const downloadNotificationSuccess = createAction(
  '[Navigation] Download Notification Success'
);
export const downloadNotificationFailure = createAction(
  '[Navigation] Download Notification Failure',
  props<{ error }>()
);

export const viewNotification = createAction(
  '[Navigation] View Notification',
  props<{ notification: INotificationItem }>()
);
export const viewNotificationSuccess = createAction(
  '[Navigation] View Notification Success'
);
export const viewNotificationError = createAction(
  '[Navigation] View Notification Error',
  props<{ error }>()
);

export const toggleNavigation = createAction(
  '[Navigation] Toggle Navigation',
  props<{ newValue?: NavPaneState }>()
);
export const toggleUser = createAction(
  '[Navigation] Toggle User',
  props<{ showUserDialog?: boolean }>()
);

export const appNameChanged = createAction(
  '[Navigation] App Name Changed',
  props<{ appName: string }>()
);

export const clickNavButton = createAction(
  '[Navigation] Click Nav Button',
  props<{ button: IButtonData }>()
);
export const clickLayoutButton = createAction(
  '[Navigation] Click Layout Button',
  props<{ button: IButtonData }>()
);

export const toggleTimeslider = createAction(
  '[Navigation] Toggle Time Slider',
  props<{ newNavValue?: TimeSliderPaneState }>()
);

export const showTimeSlider = createAction('[TimeSlider] Show Time Slider');

export const visibilityChanged = createAction(
  '[Navigation] Visibility Changed',
  props<{ id: string; visibility: boolean }>()
);
export const helpClicked = createAction('[Navigation] Help Clicked');
export const toggleLoading = createAction(
  '[Navigation] Toggle Loading',
  props<{ showLoading?: boolean }>()
);
export const openSentiment = createAction('[Navigation] Open Sentiment');
export const closeSentiment = createAction(
  '[Navigation] Close Sentiment',
  props<{ sentiment?: ISentiment }>()
);
export const saveSentimentSuccess = createAction(
  '[Navigation] Save Sentiment Success'
);
export const saveSentimentFailure = createAction(
  '[Navigation] Save Sentiment Failure'
);

export const configureNavigationButton = createAction(
  '[Navigation] Configure Navigation Button',
  props<{ id: string; button: Partial<IButtonData> }>()
);
export const configureLayoutButton = createAction(
  '[Navigation] Configure Layout Button',
  props<{ id: string; button: Partial<IButtonData> }>()
);

export const showRightTrayIcon = createAction(
  '[Navigation] Show Right Tray Icon'
);
export const hideRightTrayIcon = createAction(
  '[Navigation] Hide Right Tray Icon'
);

export const showAssetTreeNavItemOnly = createAction(
  '[Navigation] Show Asset Tree Navigation Item Only',
  props<{ show: boolean }>()
);

export const showRightTray = createAction('[Navigation] Show Right Tray');
export const hideRightTray = createAction('[Navigation] Hide Right Tray');
export const toggleRightTray = createAction('[Navigation] Toggle Right Tray');
export const helpClick = createAction(
  '[Navigation] Help Click',
  props<{ url: string }>()
);
export const routeChange = createAction(
  '[Navigation] Route Change',
  props<{ route: string }>()
);
////
export const setApplicationAsset = createAction(
  '[Application] Set Asset',
  props<{ asset: string | string[] }>()
);
export const taskCenterLoad = createAction(
  '[Application] Task Center Load',
  props<ITaskCenterLoadData>()
);
export const updateNavigationItems = createAction(
  '[Application] Update Navigation Items',
  props<{ urlParams?: any }>()
);
export const loadItem = createAction(
  '[Application] Load Item',
  props<ILoadItem>()
);
export const openIssue = createAction(
  '[Application] Open Issue',
  props<{ issue: string; issueObject?: any }>()
);
export const openTrend = createAction(
  '[Application] Open Trend',
  props<{ trend: string; start: string; end: string; trendObject?: any }>()
);
export const openModel = createAction(
  '[Application] Open Model',
  props<{ model: string }>()
);

export const navItemSetSelected = createAction(
  '[Application] Selected Navigation Item',
  props<{ navItemId: string }>()
);
