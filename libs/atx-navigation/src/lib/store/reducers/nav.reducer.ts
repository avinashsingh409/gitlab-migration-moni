/* eslint-disable ngrx/on-function-explicit-return-type */
import * as actions from '../actions/nav.actions';
import { INavigationState, getInitialNavState } from '../state/nav-state';
import { Action, on, createReducer } from '@ngrx/store';
import { IButtonData } from '../../models/button-data';
import { ButtonClick } from '../../service/utilities/button-click';
import { produce } from 'immer';

const assetNavButtons = [
  'aHome',
  'alerts',
  'work_management',
  'data-explorer',
  'view-explorer',
  'dashboards',
  'opModeConfig',
  'modelConfig',
  'assetConfig',
];
const navReducers = createReducer(
  getInitialNavState(),
  on(actions.configure, (state, action) => {
    return { ...state, ...action.configuration };
  }),
  on(actions.setApplicationAsset, (state, action) => {
    let asset: string = null;
    if (Array.isArray(action.asset)) {
      asset = action.asset[0];
    } else {
      asset = action.asset;
    }
    return { ...state, asset };
  }),
  on(actions.taskCenterLoad, (state, action) => {
    if (action.asset === '') {
      return state;
    }
    const updatedNavigationItems = produce(state.navigationItems, (navItem) => {
      assetNavButtons.forEach((element) => {
        const index = navItem.findIndex((button) => button.id === element);
        if (index !== -1) {
          if (element === 'opModeConfig' && navItem[index].type !== 'route') {
            return;
          }
          if (element === 'modelConfig' && navItem[index].type !== 'route') {
            return;
          }
          if (navItem[index].urlParams) {
            navItem[index].urlParams.id = action.asset;
          } else {
            navItem[index].urlParams = { id: action.asset };
          }
        }
      });
    });
    return { ...state, navigationItems: updatedNavigationItems };
  }),
  on(actions.updateNavigationItems, (state, action) => {
    if (!action.urlParams) {
      return;
    }
    const hasEmptyTimestamp = !action.urlParams?.end ? true : false;
    const updatedNavigationItems = produce(
      state.navigationItems,
      (draftState) => {
        assetNavButtons.forEach((element) => {
          const index = draftState.findIndex((button) => button.id === element);
          if (index !== -1) {
            if (
              element === 'opModeConfig' &&
              draftState[index].type !== 'route'
            ) {
              return;
            }
            if (
              element === 'modelConfig' &&
              draftState[index].type !== 'route'
            ) {
              return;
            }
            if (hasEmptyTimestamp) {
              if (action.urlParams?.id) {
                if (!draftState[index].urlParams) {
                  draftState[index].urlParams = {
                    id: action.urlParams.id,
                  };
                } else {
                  draftState[index].urlParams.id = action.urlParams.id;
                }
              }
            } else {
              draftState[index].urlParams = action.urlParams;
            }
          }
        });
      }
    );
    return { ...state, navigationItems: updatedNavigationItems };
  }),
  on(actions.navItemSetSelected, (state, action) => {
    const navigationItems = [...state.navigationItems];
    const updatedNavigationItems = produce(navigationItems, (navItems) => {
      navItems.forEach((item) => {
        if (item.id !== 'asset_tree') {
          item.selected = false;
        }
      });

      if (action.navItemId) {
        const idx = navItems.findIndex((item) => item.id === action.navItemId);
        navItems[idx].selected = true;
      }
    });
    return { ...state, navigationItems: updatedNavigationItems };
  }),
  on(actions.toggleNavigation, (state, action) => {
    let navPaneState = action.newValue;
    if (!navPaneState) {
      if (
        state.navPaneState === 'hidden' ||
        state.navPaneState === 'expanded'
      ) {
        navPaneState = 'open';
      } else {
        navPaneState = 'expanded';
      }
    }
    return { ...state, navPaneState };
  }),
  on(actions.toggleUser, (state, action) => {
    return {
      ...state,
      showUserDialog: action.showUserDialog ?? !state.showUserDialog,
    };
  }),
  on(actions.getUser, (state, action) => {
    return { ...state, user: null };
  }),
  on(actions.getUserSuccess, (state, action) => {
    return { ...state, user: action.user };
  }),
  on(actions.getUserFailure, (state, action) => {
    return {
      ...state,
      user: {
        firstName: 'Unauthorized',
        lastName: 'User',
        email: '',
        customerId: '',
      },
    };
  }),
  on(actions.getUserPreferencesSuccess, (state, action) => {
    return {
      ...state,
      userPreferences: action?.user,
      appName:
        action?.user?.Title === null ? 'Prometheus APM' : action?.user?.Title,
    };
  }),
  on(actions.userPreferencesChangedSuccess, (state, action) => {
    return { ...state, userPreferences: action.preferences };
  }),
  on(actions.appNameChanged, (state, action) => {
    return { ...state, appName: action.appName };
  }),
  on(actions.clickNavButton, (state, action) => {
    const newArray: IButtonData[] = ButtonClick(
      action.button,
      state.navigationItems
    );
    return { ...state, navigationItems: newArray };
  }),
  on(actions.clickLayoutButton, (state, action) => {
    let navPaneState = state.navPaneState;
    let selected = action.button.selected;
    if (action.button.id === 'nNavbar') {
      selected = false;

      if (navPaneState === 'hidden') {
        navPaneState = 'open';
      } else {
        navPaneState = 'hidden';
      }
    } else {
      selected = !selected;
    }
    const layoutItems: IButtonData[] = [...state.layoutItems];
    const idx = layoutItems.findIndex((n) => n.id === action.button.id);
    if (idx >= 0) {
      layoutItems.splice(idx, 1, { ...layoutItems[idx], selected });
    }
    return { ...state, layoutItems, navPaneState };
  }),
  on(actions.toggleNotifications, (state, action) => {
    return {
      ...state,
      showNotifications: action.showNotifications ?? !state.showNotifications,
    };
  }),
  on(actions.getNotificationsSuccess, (state, action) => {
    return {
      ...state,
      notificationList: action.notifications,
      newNotificationsCount: action.notificationsCount,
    };
  }),
  on(actions.getNotificationsFailure, (state, action) => {
    return { ...state, notificationList: null, newNotificationsCount: null };
  }),
  on(actions.showTimeSlider, (state, action) => {
    return { ...state, timeSliderPaneState: 'show' };
  }),
  on(actions.toggleTimeslider, (state, action) => {
    let newNavValue = action.newNavValue;
    if (!newNavValue) {
      if (state.timeSliderPaneState === 'hidden') {
        newNavValue = 'show';
      } else if (state.timeSliderPaneState === 'show') {
        newNavValue = 'hidden';
      } else {
        newNavValue = 'none';
      }
    }
    return { ...state, timeSliderPaneState: newNavValue };
  }),
  on(actions.visibilityChanged, (state, action) => {
    const navigationItems: IButtonData[] = [...state.navigationItems];
    const idx = navigationItems.findIndex((n) => n.id === action.id);
    if (idx >= 0) {
      navigationItems.splice(idx, 1, {
        ...navigationItems[idx],
        visible: action.visibility,
      });
    }
    return { ...state, navigationItems };
  }),
  on(actions.toggleLoading, (state, action) => {
    return { ...state, showLoading: action.showLoading ?? !state.showLoading };
  }),
  on(actions.openSentiment, (state, action) => {
    return { ...state, showSentiment: true };
  }),
  on(actions.closeSentiment, (state, action) => {
    return { ...state, showSentiment: false };
  }),
  on(actions.configureNavigationButton, (state, action) => {
    const navigationItems = [...state.navigationItems];
    const idx = navigationItems.findIndex((n) => n.id === action.id);
    if (idx >= 0) {
      navigationItems.splice(idx, 1, {
        ...navigationItems[idx],
        ...action.button,
        id: action.id,
      });
    }
    return { ...state, navigationItems };
  }),
  on(actions.configureLayoutButton, (state, action) => {
    const layoutItems = [...state.layoutItems];
    const idx = layoutItems.findIndex((n) => n.id === action.id);
    if (idx >= 0) {
      layoutItems.splice(idx, 1, {
        ...layoutItems[idx],
        ...action.button,
        id: action.id,
      });
    }
    return { ...state, layoutItems };
  }),
  on(actions.showRightTrayIcon, (state, action) => {
    return {
      ...state,
      showRightTrayIcon: true,
    };
  }),
  on(actions.hideRightTrayIcon, (state, action) => {
    return {
      ...state,
      showRightTrayIcon: false,
    };
  }),
  on(actions.showRightTray, (state, action) => {
    return {
      ...state,
      rightTrayOpen: true,
    };
  }),
  on(actions.hideRightTray, (state, action) => {
    return {
      ...state,
      rightTrayOpen: false,
    };
  }),
  on(actions.toggleRightTray, (state, action) => {
    return {
      ...state,
      rightTrayOpen: !state.rightTrayOpen,
    };
  }),
  on(actions.showAssetTreeNavItemOnly, (state, action) => {
    return { ...state, showAssetTreeNavItemOnly: action.show };
  })
);

export function navReducer(state: INavigationState, action: Action) {
  return navReducers(state, action);
}
