import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  ParamMap,
  Router,
} from '@angular/router';
import { Location } from '@angular/common';
import { isNil } from '@atonix/atx-core';
import { getAMonthAgoLive } from '@atonix/shared/utils';

export function getUniqueIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('id');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getIssueIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('iid');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getAssetIssueIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('aiid');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getQuickReplyIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('qrid');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getAssetIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('aid');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getTrendIdFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('trend');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getStartFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('start');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getEndFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('end');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getAssetFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('asset');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getModelFromRoute(queryParamMap: ParamMap) {
  let result = queryParamMap.get('model');
  if (result === 'null' || result === 'undefined' || isNil(result)) {
    result = null;
  }
  return result;
}

export function getModelConfigRoute(queryParamMap: ParamMap): {
  model: string;
  start: string;
  end: string;
  asset: string;
  isStandard: string;
  startDate: Date;
  endDate: Date;
  modelIds: string;
} {
  let model = queryParamMap.get('modelId');
  if (model === 'null' || model === 'undefined' || isNil(model)) {
    model = null;
  }
  let start = queryParamMap.get('start');
  if (start === 'null' || start === 'undefined' || isNil(start)) {
    start = null;
  }
  let end = queryParamMap.get('end');
  if (end === 'null' || end === 'undefined' || isNil(end)) {
    end = null;
  }
  let asset = queryParamMap.get('aid');
  if (asset === 'null' || asset === 'undefined' || isNil(asset)) {
    asset = null;
  }
  let isStandard = queryParamMap.get('isStandard');
  if (
    isStandard === 'null' ||
    isStandard === 'undefined' ||
    isNil(isStandard)
  ) {
    isStandard = null;
  }
  let modelIds = queryParamMap.get('modelIds');
  if (modelIds === 'null' || modelIds === 'undefined' || isNil(modelIds)) {
    modelIds = null;
  }

  const startDate = start ? new Date(parseFloat(start)) : getAMonthAgoLive();
  const endDate = end ? new Date(parseFloat(end)) : new Date();
  return { model, start, end, asset, isStandard, startDate, endDate, modelIds };
}

export function updateRouteWithoutReloading(
  id: string,
  trend: string,
  start: string,
  end: string,
  router: Router,
  activatedRoute: ActivatedRoute
) {
  router.navigate([], {
    queryParams: { id, trend, start, end },
    relativeTo: activatedRoute,
  });
}

export function updateRouteIdTimeSliderWithoutReloading(
  id: string,
  start: string,
  end: string,
  router: Router,
  activatedRoute: ActivatedRoute
) {
  router.navigate([], {
    queryParams: { id, start, end },
    relativeTo: activatedRoute,
  });
}

export function updateRouteTimeSliderWithoutReloading(
  start: string,
  end: string,
  router: Router,
  activatedRoute: ActivatedRoute
) {
  router.navigate([], {
    queryParams: { start, end },
    relativeTo: activatedRoute,
  });
}

export function updateRouteModelAssetTimeSliderWithoutReloading(
  modelId: string | null,
  assetId: string | null,
  start: string,
  end: string,
  router: Router,
  activatedRoute: ActivatedRoute
) {
  router.navigate([], {
    queryParams: { model: modelId, asset: assetId, start, end },
    relativeTo: activatedRoute,
  });
}

// kept all the different functions for these
// but consolidated in one place
export function updateRouterIdWithoutReloading(
  id: string,
  activatedRoute: ActivatedRoute,
  router: Router
) {
  router.navigate([], {
    queryParams: { id },
    relativeTo: activatedRoute,
  });
}

export function updateRouteIdWithoutReloading(
  id: string,
  router: Router,
  activatedRoute: ActivatedRoute,
  location: Location
) {
  const url = router
    .createUrlTree([{ id }], { relativeTo: activatedRoute })
    .toString();
  location.go(url);
}

// the id url is different in the asset explorer app.
const invalidIds: Array<string> = [
  'asset-explorer',
  'location',
  'events',
  'tags',
  'alarms',
  'alerts',
  'store',
  'info',
  'attachments',
  'blog',
  'null',
];
export function getAssetExplorerRoute(route: ActivatedRouteSnapshot) {
  const id = route.paramMap.get('id');
  if (id) {
    if (invalidIds.includes(id)) {
      // a null value will include the route name as the query parameter
      // instead of a valid asset
      return;
    }
    return id;
  }
  return null;
}
// let result = this.activatedRoute.snapshot.queryParamMap.get('id');
// getUniqueIdFromRoute(this.activatedRoute.snapshot.queryParamMap)
