import { ButtonClick } from './button-click';
import { IButtonData } from '../../models/button-data';

describe('ButtonClick', () => {
  it('ignore non-existant buttons', () => {
    const button: IButtonData = {
      id: 'id1',
    };
    const buttons: IButtonData[] = [{ id: 'id2' }, { id: 'id3' }];
    const result = ButtonClick(button, buttons);
    expect(result).toBe(buttons);
  });

  it('toggle toggle buttons', () => {
    const button: IButtonData = {
      id: 'id1',
      behavior: 'toggle',
      selected: false,
    };
    const buttons: IButtonData[] = [
      { id: 'id2', selected: false },
      { id: 'id3', selected: false },
      button,
    ];
    const result = ButtonClick(button, buttons);
    expect(result[2].id).toBe('id1');
    expect(result[2].selected).toEqual(true);
    expect(result[0].selected || result[1].selected).toEqual(false);
  });

  it('toggle group buttons', () => {
    const button: IButtonData = {
      id: 'id0',
      behavior: 'exclusive',
      selected: false,
      behaviorGroup: '1',
    };
    const buttons: IButtonData[] = [
      button,
      { id: 'id1', behavior: 'exclusive', behaviorGroup: '1', selected: true },
      { id: 'id2', selected: true },
    ];
    const result = ButtonClick(button, buttons);
    expect(result[0].id).toBe('id0');
    expect(result[1].id).toBe('id1');
    expect(result[2].id).toBe('id2');
    expect(result[0].selected).toEqual(true);
    expect(result[1].selected).toEqual(false);
    expect(result[2].selected).toEqual(true);

    const result2 = ButtonClick(result[1], result);
    expect(result2[0].selected).toEqual(false);
    expect(result2[1].selected).toEqual(true);
    expect(result2[2].selected).toEqual(true);
  });
});
