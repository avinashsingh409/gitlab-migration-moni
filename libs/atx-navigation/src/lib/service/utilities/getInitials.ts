import { IUser } from '@atonix/shared/api';

export function getInitials(user: IUser) {
  let result = '-';
  if (user) {
    result = '';
    if (user.firstName && user.firstName.length > 0) {
      result += user.firstName.substr(0, 1);
    }
    if (user.lastName && user.lastName.length > 0) {
      result += user.lastName.substr(0, 1);
    }
  }
  return result;
}
