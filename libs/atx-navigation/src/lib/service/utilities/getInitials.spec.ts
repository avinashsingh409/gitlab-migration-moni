import { TestBed } from '@angular/core/testing';
import { IUser } from '@atonix/shared/api';

import { getInitials } from './getInitials';

describe('getInitials', () => {
  it('should get Initials full name', () => {
    const user: IUser = {
      firstName: 'ABBA',
      lastName: 'ZXXZ',
      email: '',
    };
    const result = getInitials(user);
    expect(result).toEqual('AZ');
  });

  it('should get Initials null user', () => {
    const result = getInitials(null);
    expect(result).toEqual('-');
  });

  it('should get Initials first name', () => {
    const user: IUser = {
      firstName: 'ABBA',
      lastName: null,
      email: '',
    };
    const result = getInitials(user);
    expect(result).toEqual('A');
  });

  it('should get Initials last name', () => {
    const user: IUser = {
      lastName: 'ABBA',
      firstName: null,
      email: '',
    };
    const result = getInitials(user);
    expect(result).toEqual('A');
  });

  it('should get Initials no name', () => {
    const user: IUser = {
      lastName: null,
      firstName: null,
      email: '',
    };
    const result = getInitials(user);
    expect(result).toEqual('');
  });
});
