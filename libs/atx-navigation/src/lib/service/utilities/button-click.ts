import { IButtonData } from '../../models/button-data';

export function ButtonClick(
  button: IButtonData,
  buttons: IButtonData[]
): IButtonData[] {
  const idx = buttons.findIndex((n) => n.id === button.id);
  if (
    idx >= 0 &&
    (button.behavior === 'toggle' || button.behavior === 'exclusive')
  ) {
    let result = [...buttons];
    if (button.behavior === 'toggle') {
      result.splice(idx, 1, {
        ...result[idx],
        selected: !result[idx].selected,
      });
    } else if (button.behavior === 'exclusive') {
      result = result.map((n) => {
        if (
          n.behaviorGroup === button.behaviorGroup &&
          n.behavior === 'exclusive'
        ) {
          return {
            ...n,
            selected: n.id === button.id,
          };
        }
        return n;
      });
    }
    return result;
  } else {
    return buttons;
  }
}
