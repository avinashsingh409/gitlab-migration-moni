import { Injectable } from '@angular/core';
import { take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { INotificationItem, JobsFrameworkService } from '@atonix/shared/api';
@Injectable({
  providedIn: 'root',
})
export class NavModelService {
  constructor(private jobsFrameworkService: JobsFrameworkService) {}

  public viewed(notification: INotificationItem): Observable<boolean> {
    return this.notificationViewed2(notification.NotificationGroupID).pipe(
      take(1)
    );
  }

  // this one is from the component
  public notificationViewed2(groupID: string | string[]): Observable<boolean> {
    groupID = this.makeGroupIDArray(groupID);
    return this.jobsFrameworkService.notificationViewed(groupID).pipe(take(1));
  }

  public makeGroupIDArray(groupID: string | string[]): string[] {
    if (!Array.isArray(groupID)) {
      groupID = [groupID];
    }
    return groupID;
  }
}
