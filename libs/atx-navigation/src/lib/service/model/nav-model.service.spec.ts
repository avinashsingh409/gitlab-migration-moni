import { TestBed, inject } from '@angular/core/testing';
import { NavModelService } from './nav-model.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('ModelService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    })
  );

  it('should be created', () => {
    const service: NavModelService = TestBed.inject(NavModelService);
    expect(service).toBeTruthy();
  });
});
