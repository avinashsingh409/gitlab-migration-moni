/*
 * Public API Surface of atx-navigation
 */

export * from './navigation.module';

export {
  getEndFromRoute,
  getAssetExplorerRoute,
  updateRouteWithoutReloading,
  getUniqueIdFromRoute,
  getTrendIdFromRoute,
  updateRouteIdTimeSliderWithoutReloading,
  updateRouteTimeSliderWithoutReloading,
  updateRouteModelAssetTimeSliderWithoutReloading,
  updateRouteIdWithoutReloading,
  getStartFromRoute,
  getIssueIdFromRoute,
  getAssetIdFromRoute,
  getAssetIssueIdFromRoute,
  getAssetFromRoute,
  getModelFromRoute,
  getModelConfigRoute,
  getQuickReplyIdFromRoute,
  updateRouterIdWithoutReloading,
} from './lib/utils/getIds';
export * from './lib/models/button-data';
export { assetNavigatorButton } from './lib/models/standard-buttons';
export * from './lib/models/task-center-load-data';
export * from './lib/models/load-item';
export * from './lib/store/state/nav-state';

export { NavModelService } from './lib/service/model/nav-model.service';
export {
  selectNavAsset,
  selectAssetTreeSelected,
  selectTheme,
  selectNavItems,
} from './lib/store/selectors/nav.selector';
export { NavFacade } from './lib/store/facade/nav.facade';
import * as NavActions from './lib/store/actions/nav.actions';
export { NavActions };
