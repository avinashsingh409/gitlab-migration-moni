import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NavigationComponent } from './lib/component/navigation/navigation.component';
import { LoginComponent } from './lib/component/login/login.component';
import { LaunchPadComponent } from './lib/component/launch-pad/launch-pad.component';
import { CarouselComponent } from './lib/component/carousel/carousel.component';
import { CarouselItemDirective } from './lib/component/carousel/carousel-item.directive';
import { CarouselItemElementDirective } from './lib/component/carousel/carousel-item-element.directive';
import { UserBoxComponent } from './lib/component/user-box/user-box.component';
import { NavModelService } from './lib/service/model/nav-model.service';
import { NotificationBoxComponent } from './lib/component/notification-box/notification-box.component';
import { BreadcrumbMenuComponent } from './lib/component/breadcrumb-menu/breadcrumb-menu.component';
import { SentimentComponent } from './lib/component/sentiment/sentiment.component';
import { navReducer } from './lib/store/reducers/nav.reducer';
import { NavEffects } from './lib/store/effects/nav.effects';
import { NavFacade } from './lib/store/facade/nav.facade';
import { SimpleLoginComponent } from './lib/component/login/simple-login/simple-login.component';
import { InitialPasswordComponent } from './lib/component/login/initial-password/initial-password.component';
import { LoginWaitingComponent } from './lib/component/login/login-waiting/login-waiting.component';
import { LoginMFAComponent } from './lib/component/login/login-mfa/login-mfa.component';
import { LoginTOTPComponent } from './lib/component/login/login-totp/login-totp.component';
import { CustomChallengeComponent } from './lib/component/login/custom-challenge/custom-challenge.component';
import { MfaSetupComponent } from './lib/component/login/mfa-setup/mfa-setup.component';
import { SelectMfaTypeComponent } from './lib/component/login/select-mfa-type/select-mfa-type.component';
import { PasswordCodeComponent } from './lib/component/login/password-code/password-code.component';
import { ForgotPasswordComponent } from './lib/component/login/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './lib/component/login/confirm-email/confirm-email.component';
import { CommonModule } from '@angular/common';
import { AtxMaterialModule } from '@atonix/atx-material';
import { UserEmailComponent } from './lib/component/login/user-email/user-email.component';
import { AutofocusDirective } from './lib/utils/autofocus.directive';
import { PasswordComponent } from './lib/component/login/password/password.component';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { SharedStateAuthModule } from '@atonix/shared/state/auth';
import { SharedApiModule } from '@atonix/shared/api';

@NgModule({
  declarations: [
    NavigationComponent,
    LoginComponent,
    UserEmailComponent,
    UserBoxComponent,
    PasswordComponent,
    AutofocusDirective,
    LaunchPadComponent,
    CarouselComponent,
    BreadcrumbMenuComponent,
    CarouselItemDirective,
    CarouselItemElementDirective,
    NotificationBoxComponent,
    SentimentComponent,
    SimpleLoginComponent,
    InitialPasswordComponent,
    LoginWaitingComponent,
    LoginMFAComponent,
    LoginTOTPComponent,
    CustomChallengeComponent,
    MfaSetupComponent,
    SelectMfaTypeComponent,
    PasswordCodeComponent,
    ForgotPasswordComponent,
    ConfirmEmailComponent,
  ],
  imports: [
    CommonModule,
    SharedStateAuthModule,
    SharedApiModule,
    AtxMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    SharedUtilsModule,
    RouterModule,
    EffectsModule.forFeature([NavEffects]),
    StoreModule.forFeature('nav', navReducer),
  ],
  exports: [
    NavigationComponent,
    LaunchPadComponent,
    CarouselComponent,
    BreadcrumbMenuComponent,
  ],
})
export class NavigationModule {
  static forRoot(): ModuleWithProviders<NavigationModule> {
    return {
      ngModule: NavigationModule,
      providers: [NavModelService, NavFacade],
    };
  }
}
