# atx-navigation

## Azure Pipeline Build and Publish

The test rig can be accessed directly from the dev-atx-navigation S3 bucket using this link:
https://dev-atx-navigation.s3.amazonaws.com/index.html

It can also be accessed using the AWS CloudFront link:
https://d22ecgqrnjk9v7.cloudfront.net

The CloudFront distribution for the test rig has an ID of E8IE687X6GFXV and is configured with an origin of dev-atx-navigation.s3.amazonaws.com. The following notable settings were made to the configuration:

- Default TTL: Set to 1 second so changes to S3 content are recognized almost immediately
- Default Root Object: index.html

## Running unit tests

Run `nx test atx-navigation` to execute the unit tests.
