# atx-core

## Running unit tests

Run `nx test atx-core` to execute the unit tests.

## Utilities

- `addEngUnits(val: string, attribute: IAssetAttribute, excludeUnits?: boolean):string`
  Will append the engineering units to the value as necessary.
- `attributeValue(attribute: IAssetAttribute, options?: IAttributeTypeOption[], excludeUnits?: boolean):string`
  Produces a friendly string representation of an attribute.
- `dateComparator(filterValue:Date, cellValue: Date | string):number`
  Used to implement a sort that will compare dates.
- `getAttributeDisplayValue(attribute: IAssetAttribute, attributeType: IAttributeTypeWithOptions): ITempAttributeType`
  Turns an attribute into a data type that can be put into a table.
- `getDate(val: any): Date`
  Turns a value into a date if possible
- `getValueFromOptions(att: IAssetAttribute, options?: IAttributeTypeOption[]):string`
  Gets a string representation of an attribute of the type options.
- `isFloatDecimals(format: string):number`
  Extracts the number of decimal places to display from an attribute
- `function isInvalidDate(val: any): boolean`
  Indicates whether a date is valid
- `function isNil(value):boolean`
  Copy of the lodash isNil function. Included here so that a reference to lodash can be avoided.
- `function processAttributes(newAsset: IAssetAndAttributes): ITempAttributeType[]`
  Takes a newly selected asset and adds all of its attributes to this class's attributes property.
- `function sigFig(input: number, digits: number): string`
  Formats a number with the indicated number of significant figures.

### Cacher

- `export interface IKeyCreator<T> { createKey(value: T): string; }`
  Generic function to produce a string key from some type. Implement this to use the cacher.
- `interface ICacher<T, S>`
  Generic interface so the concrete cachers can be used interchangeably
- `class Cacher<T, S>`
  Keeps an in-memory cache.
- `class PersistentCacher<T, S>`
  Keeps a cache that is stored in local storage.
  `constructor(private keyCreator: IKeyCreator<T>, private size: number, private storagePrefix: string)`
  The storage prefix allows you to indicate where the cache is stored.
  size indicates the maximum size of the cache. When a new item is added the oldest item is discarded when full.

## Types

Many data types are included in this library. They match the data types on the server and should be used whenever available.
