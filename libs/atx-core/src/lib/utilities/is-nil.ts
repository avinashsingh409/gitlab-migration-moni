export function isNil(value) {
  return value == null || value == undefined;
}
export function isNilOrEmptyString(value) {
  return value == null || value == undefined || value === '';
}
