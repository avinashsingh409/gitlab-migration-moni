import { IAssetMeasurements } from '../data-types/asset-type';
import { IPDTrend } from '../shared-models/pd-trend';

import { isNil } from './is-nil';

export function setTimestampDictionary(
  timestampDictionary: { [key: number]: any },
  xyData: IAssetMeasurements
) {
  if (xyData) {
    timestampDictionary = timestampDictionary || {};
    for (const data of xyData.Data) {
      timestampDictionary[data.Time.getTime()] = data.Value;
    }
  }
  return timestampDictionary;
}

export function getTrendTotalSeries(trend: IPDTrend) {
  let result: number = null;
  if (trend.Series.length > 0) {
    result = trend.Series.length;
  } else if (trend.BandAxisBarsResolved) {
    let cnt = trend.BandAxisBarsResolved.length;
    if (cnt === 0 && trend.BandAxisMeasurements) {
      for (const m of trend.BandAxisMeasurements) {
        for (let i = 1; i <= 5; i++) {
          if (m['TagID' + i]) {
            cnt++;
          }
        }
      }
    }

    result = cnt;
  } else if (trend.BandAxis && trend.BandAxisMeasurements) {
    const dict: { [id: number]: boolean } = {};
    result = trend.BandAxisMeasurements.map((m) => {
      let r = 0;
      for (let idx = 1; idx < 5; idx++) {
        if (
          !isNil(m['TagID' + String(idx)]) &&
          !dict[m['TagID' + String(idx)]]
        ) {
          r++;
          dict[m['TagID' + String(idx)]] = true;
        }
      }

      return r;
    }).reduce((previous, current) => {
      return previous + current;
    }, 0);
  } else {
    result = 0;
  }
  return result;
}
