import {
  IAssetMeasurementsSet,
  IPDTrend,
  IProcessedTrend,
} from '../shared-models/pd-trend';
import { processModelContextTrend } from './process-trends';

describe('ProcessTrends', () => {
  it('processModelContextTrend should function properly', () => {
    const startDate: Date = new Date('2022-01-01');
    const endDate: Date = new Date('2022-05-01');
    const givenMeasurementSet: IAssetMeasurementsSet = {
      Annotations: [],
      Bands: null,
      Errors: null,
      Measurements: [],
      Pins: [],
    };
    const givenTrend: IProcessedTrend = {
      assetGuid: '2852c6c3-27ed-42dc-8bcc-4f96492e5b17',
      assetID: '4518280',
      groupedSeriesSubType: '',
      groupedSeriesType: 0,
      id: '-1',
      label: 'Model Context',
      labelIndex: 0,
      modelTrendMap: {
        AssetGuid: '2852c6c3-27ed-42dc-8bcc-4f96492e5b17',
        AssetID: 4518280,
        DisplayOrder: 0,
        ModelID: 249053,
        MappedPDTrend: null,
        NDModelPDTrendMapID: -1,
        PDTrendID: -1,
        ReplaceTrendContents: false,
      },
      showDataCursor: true,
      totalSeries: 5,
      trendDefinition: {} as IPDTrend,
      startDate,
      endDate,
      measurements: givenMeasurementSet,
    };

    const expected: IProcessedTrend = { ...givenTrend };

    expect(
      processModelContextTrend(
        givenTrend,
        givenMeasurementSet,
        startDate,
        endDate
      )
    ).toEqual(expected);
  });
});
