import find from 'lodash/find';
import { IAssetAttribute } from '../data-types/asset-type';
import { IAttributeTypeOption } from '../data-types/attribute-type';

export function getValueFromOptions(
  att: IAssetAttribute,
  options?: IAttributeTypeOption[]
) {
  if (options) {
    const op = find(
      options,
      (n) => n.attributeOptionTypeID === att.AttributeOptionTypeID
    );
    const result = op ? op.attributeOptionTypeDesc : '';
    return result;
  }
  return String(att.AttributeOptionTypeID);
}
