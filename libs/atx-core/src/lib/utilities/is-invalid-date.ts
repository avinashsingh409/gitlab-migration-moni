export function isInvalidDate(val: any): boolean {
  return val instanceof Date && isNaN(val as any);
}

export function isValidDate(date: Date): boolean {
  return date instanceof Date && !isNaN(date.getTime());
}
