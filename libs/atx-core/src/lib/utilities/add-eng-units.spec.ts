import { IAssetAttribute } from '../data-types/asset-type';
import { IAttributeType } from '../data-types/attribute-type';
import { addEngUnits } from './add-eng-units';

describe('addEngUnits', () => {
  it('should add the units on a normal input', () => {
    const testAttributeType: IAttributeType = {
      EngrUnits: 'Newtons',
    } as IAttributeType;
    const testAttribute: IAssetAttribute = {
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(addEngUnits('22', testAttribute)).toEqual('22 Newtons');
  });
});
