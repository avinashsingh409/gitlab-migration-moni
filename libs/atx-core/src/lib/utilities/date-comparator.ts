import moment from 'moment';

export function dateComparator(filterValue, cellValue) {
  const value = cellValue;

  const cellDate = moment(value);
  cellDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });

  if (cellDate.toDate() < filterValue) {
    return -1;
  } else if (cellDate.toDate() > filterValue) {
    return 1;
  } else if (value !== null && value !== 'In Progress') {
    return 0;
  }
}
