/**
 * @deprecated Use this built-in function instead: input?.toPrecision(digits).
 * This method was dates back to the building of the AngularJS Legacy site.
 * It does not do leading 0s correctly (for example: 0.0001).
 * Rather than fix it, we should just move to the built-in javascript
 * function that does this.
 * See: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toPrecision
 */
export function sigFig(input: number, digits: number): string {
  if (input === null || isNaN(input)) {
    return '';
  } else if (input === 0) {
    return '0';
  } else {
    if (!digits) {
      digits = 4;
    }
    const roundValue = Math.pow(
      10,
      Math.ceil(Math.log(Math.abs(input)) / Math.LN10) - digits
    );
    // roundValue now holds the power of 10 to which we should round numToFormat to preserve the specified number of significant figures.
    return String(
      Math.round(1000000 * (Math.round(input / roundValue) * roundValue)) /
        1000000
    );
  }
}
