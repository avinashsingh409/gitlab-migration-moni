import { isFloatDecimals } from './is-float-decimals';

describe('isFloatDecimals', () => {
  it('should return null if a criterion is not met', () => {
    expect(isFloatDecimals(null)).toBe(null);
    expect(isFloatDecimals('')).toBe(null);
    expect(isFloatDecimals('radio')).toBe(null);
    expect(isFloatDecimals('int')).toBe(null);
    expect(isFloatDecimals('float')).toBe(null);
    expect(isFloatDecimals('radio')).toBe(null);
    expect(isFloatDecimals('x')).toBe(null);
    expect(isFloatDecimals('f')).not.toBe(null);
    expect(isFloatDecimals('p')).not.toBe(null);
  });

  it('should run', () => {
    expect(isFloatDecimals('p15')).toBe(15);
    expect(isFloatDecimals('f8')).toBe(8);
    // The default number of decimals is 2.
    // It falls into the default case if the first letter is p or f, and the following chars aren't numbers.
    expect(isFloatDecimals('pxx')).toBe(2);
  });
});
