import { isNil } from './is-nil';

export function isValidFilenameForAttachment(fileName: string) {
  // Limit file names to characters included in both ISO-8859-1 and Windows-1252.
  // https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s08.html
  // S3 presigned urls require that the file name can be represented in a response header
  // which only supports characters from ISO-8859-1 (i.e., en dash '–' is not allowed).
  // 14-Jun-2022 RMB
  // Support standard whitespace/space character plus the printable characters from ISO-8859-1.
  //   Range 1: space to tilde [x20-x7E]
  //   Range 2: ¡ (inverted exclamation mark) to ÿ (y with diaeresis) [xA1-xFF]

  return !isNil(fileName) && /^[ -~¡-ÿ]+$/.test(fileName);
}
