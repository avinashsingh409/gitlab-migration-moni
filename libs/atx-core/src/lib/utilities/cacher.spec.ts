import { ICacher, Cacher, PersistentCacher, IKeyCreator } from './cacher';

interface ITestKeyObject {
  FirstName: string;
  LastName: string;
}

interface ITestValueObject {
  Value: string;
}

const KeyCreator: IKeyCreator<ITestKeyObject> = {
  createKey(value: ITestKeyObject) {
    return value.FirstName;
  },
};

describe('cacher', () => {
  describe('regular cacher', () => {
    it('should check existence', () => {
      const c = new Cacher<ITestKeyObject, ITestValueObject>(KeyCreator, 5);
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      expect(c.exists(keyObject)).toEqual(false);
      c.add(keyObject, { Value: 'VALUE!!!!' });
      expect(c.exists(keyObject)).toEqual(true);
    });

    it('should save and return a value', () => {
      const c = new Cacher<ITestKeyObject, ITestValueObject>(KeyCreator, 5);
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      const valObject: ITestValueObject = {
        Value: 'SOMETHING!!!',
      };
      expect(c.get(keyObject)).toBeNull();
      c.add(keyObject, valObject);
      const myVal = c.get(keyObject);
      expect(myVal.Value).toEqual('SOMETHING!!!');
    });

    it('should roll off', () => {
      const c = new Cacher<ITestKeyObject, ITestValueObject>(KeyCreator, 3);

      const keyObject1: ITestKeyObject = {
        FirstName: 'Josh1',
        LastName: 'Bowen',
      };
      const keyObject2: ITestKeyObject = {
        FirstName: 'Josh2',
        LastName: 'Bowen',
      };
      const keyObject3: ITestKeyObject = {
        FirstName: 'Josh3',
        LastName: 'Bowen',
      };
      const keyObject4: ITestKeyObject = {
        FirstName: 'Josh4',
        LastName: 'Bowen',
      };
      const keyObject5: ITestKeyObject = {
        FirstName: 'Josh5',
        LastName: 'Bowen',
      };

      const valObject: ITestValueObject = {
        Value: 'SOMETHING!!!',
      };
      c.add(keyObject1, valObject);
      c.add(keyObject2, valObject);
      c.add(keyObject3, valObject);
      c.add(keyObject4, valObject);
      c.add(keyObject5, valObject);
      expect(c.exists(keyObject1)).toEqual(false);
      expect(c.exists(keyObject2)).toEqual(false);
      expect(c.exists(keyObject3)).toEqual(true);
      expect(c.exists(keyObject4)).toEqual(true);
      expect(c.exists(keyObject5)).toEqual(true);
    });

    it('should clear', () => {
      const c = new Cacher<ITestKeyObject, ITestValueObject>(KeyCreator, 5);
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      expect(c.exists(keyObject)).toEqual(false);
      c.add(keyObject, { Value: 'VALUE!!!!' });
      expect(c.exists(keyObject)).toEqual(true);
      c.clear();
      expect(c.exists(keyObject)).toEqual(false);
    });
  });

  describe('persistent cacher', () => {
    const key = 'testingcacherkey';
    beforeEach(() => {
      window.localStorage.removeItem(key);
    });

    it('should check existence', () => {
      const c = new PersistentCacher<ITestKeyObject, ITestValueObject>(
        KeyCreator,
        5,
        key
      );
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      expect(c.exists(keyObject)).toEqual(false);
      c.add(keyObject, { Value: 'VALUE!!!!' });
      expect(c.exists(keyObject)).toEqual(true);
    });

    it('should save and return a value', () => {
      const c = new PersistentCacher<ITestKeyObject, ITestValueObject>(
        KeyCreator,
        5,
        key
      );
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      const valObject: ITestValueObject = {
        Value: 'SOMETHING!!!',
      };
      expect(c.get(keyObject)).toBeNull();
      c.add(keyObject, valObject);
      const myVal = c.get(keyObject);
      expect(myVal.Value).toEqual('SOMETHING!!!');
    });

    it('should roll off', () => {
      const c = new PersistentCacher<ITestKeyObject, ITestValueObject>(
        KeyCreator,
        3,
        key
      );

      const keyObject1: ITestKeyObject = {
        FirstName: 'Josh1',
        LastName: 'Bowen',
      };
      const keyObject2: ITestKeyObject = {
        FirstName: 'Josh2',
        LastName: 'Bowen',
      };
      const keyObject3: ITestKeyObject = {
        FirstName: 'Josh3',
        LastName: 'Bowen',
      };
      const keyObject4: ITestKeyObject = {
        FirstName: 'Josh4',
        LastName: 'Bowen',
      };
      const keyObject5: ITestKeyObject = {
        FirstName: 'Josh5',
        LastName: 'Bowen',
      };

      const valObject: ITestValueObject = {
        Value: 'SOMETHING!!!',
      };
      c.add(keyObject1, valObject);
      c.add(keyObject2, valObject);
      c.add(keyObject3, valObject);
      c.add(keyObject4, valObject);
      c.add(keyObject5, valObject);
      expect(c.exists(keyObject1)).toEqual(false);
      expect(c.exists(keyObject2)).toEqual(false);
      expect(c.exists(keyObject3)).toEqual(true);
      expect(c.exists(keyObject4)).toEqual(true);
      expect(c.exists(keyObject5)).toEqual(true);
    });

    it('should clear', () => {
      const c = new PersistentCacher<ITestKeyObject, ITestValueObject>(
        KeyCreator,
        5,
        key
      );
      const keyObject: ITestKeyObject = {
        FirstName: 'Josh',
        LastName: 'Bowen',
      };
      expect(c.exists(keyObject)).toEqual(false);
      c.add(keyObject, { Value: 'VALUE!!!!' });
      expect(c.exists(keyObject)).toEqual(true);
      c.clear();
      expect(c.exists(keyObject)).toEqual(false);
    });
  });
});
