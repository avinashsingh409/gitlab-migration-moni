export interface IKeyCreator<T> {
  createKey(value: T): string;
}

export interface ICacher<T, S> {
  clear();
  exists(key: T): boolean;
  add(key: T, value: S);
  get(key: T): S;
}

export class Cacher<T, S> implements ICacher<T, S> {
  private keys: string[] = [];
  private values: { [key: string]: S } = {};
  constructor(private keyCreator: IKeyCreator<T>, private size: number) {}

  clear() {
    this.keys = [];
    this.values = {};
  }
  exists(key: T): boolean {
    const keyString = this.keyCreator.createKey(key);
    if (this.values[keyString]) {
      return true;
    }
    return false;
  }
  add(key: T, value: S) {
    const keyString = this.keyCreator.createKey(key);
    const newLength = this.keys.unshift(keyString);
    this.values[keyString] = value;
    if (newLength > this.size) {
      const valToDelete = this.keys.pop();
      delete this.values[valToDelete];
    }
  }
  get(key: T): S {
    const keyString = this.keyCreator.createKey(key);
    return this.values[keyString] ?? null;
  }
}

export class PersistentCacher<T, S> implements ICacher<T, S> {
  private keys: string[] = [];
  private values: { [key: string]: S } = {};
  constructor(
    private keyCreator: IKeyCreator<T>,
    private size: number,
    private storagePrefix: string
  ) {
    const values = window.localStorage[this.storagePrefix];
    if (values) {
      const hydratedValues = JSON.parse(values);
      this.values = {};
      this.keys = [];
      for (const n of hydratedValues) {
        this.keys.push(n.key);
        this.values[n.key] = n.value;
      }
    }
  }

  clear() {
    this.keys = [];
    this.values = {};
    delete window.localStorage[this.storagePrefix];
  }
  exists(key: T): boolean {
    const keyString = this.keyCreator.createKey(key);
    if (this.values[keyString]) {
      return true;
    }
    return false;
  }
  add(key: T, value: S) {
    const keyString = this.keyCreator.createKey(key);
    const newLength = this.keys.unshift(keyString);
    this.values[keyString] = value;
    if (newLength > this.size) {
      const valToDelete = this.keys.pop();
      delete this.values[valToDelete];
    }
    this.save();
  }
  get(key: T): S {
    const keyString = this.keyCreator.createKey(key);
    return this.values[keyString] ?? null;
  }
  save() {
    const values = this.keys.map((key) => {
      return { key, value: this.values[key] };
    });
    window.localStorage.setItem(this.storagePrefix, JSON.stringify(values));
  }
}
