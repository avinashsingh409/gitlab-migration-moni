// Checks the object backing an AgGrid field (the rowNode.data object)
// The field changed in AgGrid maps to that object as a string
// This function checks the change against that object.
// if the values are different it returns true
export function agGridFieldChangeDirtiesObject(
  modelObject: any,
  key: string,
  newValue: any
): boolean {
  const columnFields = key.split('.');
  let currentValue = null;
  columnFields.reduce((p, c, i) => {
    if (columnFields.length === ++i) {
      currentValue = p[c];
    }
    return p[c] || {};
  }, modelObject);
  if (typeof currentValue === 'string') {
    if (newValue?.toLocaleUpperCase() === 'N/A') {
      // fields that are null are mapped to AgGrid Value 'N/A' - do not
      // allow these changes to propagate through the model, since it would
      // set a number value to 'N/A'
      return false;
    }
    return currentValue === newValue ? false : true;
  } else if (typeof currentValue === 'boolean') {
    // AgGrid True values are typically assigned 'Yes' / 'No' values
    // So they display correctly for the user. If your use case is different,
    // this logic might not work correctly:
    const newBooleanValue = newValue === 'Yes' ? true : false;
    return currentValue === newBooleanValue ? false : true;
  } else {
    return currentValue === newValue ? false : true;
  }
}
