import moment from 'moment';
import { isInvalidDate } from './is-invalid-date';
const isDate = (d) => d instanceof Date;

export function getDate(val: any): Date {
  if (isDate(val)) {
    return new Date(val.getTime());
  } else if (isString(val)) {
    let result: Date = null;
    result = new Date(val);

    if (isInvalidDate(result)) {
      const m = moment(val, 'M/D/YYYY H:m:s a', false);
      if (m.isValid()) {
        result = m.toDate();
      }
    }
    return result;
  } else {
    return new Date(val);
  }
}

export function toDateTimeOffset(myDate: Date) {
  if (moment(myDate).isValid()) {
    let month = String(myDate.getMonth() + 1);
    if (month.length === 1) {
      month = '0' + month;
    }
    let day = String(myDate.getDate());
    if (day.length === 1) {
      day = '0' + day;
    }
    let hour = String(myDate.getHours());
    if (hour.length === 1) {
      hour = '0' + hour;
    }
    let minute = String(myDate.getMinutes());
    if (minute.length === 1) {
      minute = '0' + minute;
    }
    let second = String(myDate.getSeconds());
    if (second.length === 1) {
      second = '0' + second;
    }

    const timeZoneOffset = myDate.getTimezoneOffset();
    let timeZoneHours = String(Math.floor(timeZoneOffset / 60));
    let timeZoneMinutes = String(timeZoneOffset % 60);
    if (timeZoneHours.length === 1) {
      timeZoneHours = '0' + timeZoneHours;
    }
    if (timeZoneMinutes.length === 1) {
      timeZoneMinutes = '0' + timeZoneMinutes;
    }
    return (
      myDate.getFullYear() +
      '-' +
      month +
      '-' +
      day +
      'T' +
      hour +
      ':' +
      minute +
      ':' +
      second +
      '.0000000-' +
      timeZoneHours +
      ':' +
      timeZoneMinutes
    );
  } else {
    return null;
  }
}

export function isString(str) {
  if (str && typeof str.valueOf() === 'string') {
    return true;
  }
  return false;
}
