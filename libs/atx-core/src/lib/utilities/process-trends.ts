import produce from 'immer';
import {
  GroupedSeriesType,
  IAssetMeasurementsSet,
  IGSMeasurement,
  IProcessedTrend,
} from '../shared-models/pd-trend';

export function processPDNDTrends(givenTrends: IProcessedTrend[]) {
  const AtxChartTrend = 4;
  const AtxScatterPlot = 24;
  const AtxChartLine = 1;

  let trends = (givenTrends ?? []).filter(
    (n) =>
      n.totalSeries > 0 ||
      n.trendDefinition.TrendSource ||
      (n.trendDefinition.BandAxisBarsResolved &&
        n.trendDefinition.BandAxisBarsResolved.length > 0) // filter trends
  );

  trends = [...trends].sort((a, b) => {
    const aLabel = a?.label?.toLowerCase();
    const bLabel = b?.label?.toLowerCase();
    if (aLabel !== bLabel) {
      if (a.label.toLowerCase() === 'model context') {
        return -1;
      } else if (b.label.toLowerCase() === 'model context') {
        return 1;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  });

  trends = trends.map((trend) => {
    let isGroupedSeries = false;
    if (trend.trendDefinition?.BandAxis) {
      isGroupedSeries = true;
    }
    let isXY = false;
    if (trend.trendDefinition?.Series) {
      trend.trendDefinition.Series.map((series) => {
        if (series.IsXAxis) {
          isXY = true;
        }
      });
    }

    const newTrend = produce(trend, (draftState) => {
      if (!trend?.trendDefinition?.ChartTypeID) {
        if (isGroupedSeries) {
          draftState.trendDefinition.ChartTypeID = AtxChartTrend;
        } else {
          if (isXY) {
            draftState.trendDefinition.ChartTypeID = AtxScatterPlot;
          } else {
            draftState.trendDefinition.ChartTypeID = AtxChartLine;
          }
        }
      }

      if (isGroupedSeries) {
        draftState.groupSeriesMeasurements = draftState.groupSeriesMeasurements
          ? draftState.groupSeriesMeasurements
          : [];
        // There are 4 types of series group: TIME, PINS, CUSTOM, and ASSET
        // TIME and PINS values are from BandAxisMeasurements
        // CUSTOM and ASSET values are from BandAxisBars
        // groupSeriesMeasurements array holds one of those group type data
        // so that it is more easy to manipulate when changing between different types
        draftState.groupSeriesMeasurements =
          draftState.trendDefinition.BandAxisMeasurements?.filter(
            (bandAxisMeasurements) =>
              bandAxisMeasurements.DisplayOrder === null ||
              bandAxisMeasurements.DisplayOrder === 0
          ) || []; //if not defined or 0, put at top of list and draw first

        draftState.trendDefinition.BandAxisMeasurements?.map(
          (bandAxisMeasurements) => {
            if (
              bandAxisMeasurements.DisplayOrder &&
              bandAxisMeasurements.DisplayOrder !== 0
            ) {
              draftState.groupSeriesMeasurements?.push({
                ...bandAxisMeasurements,
              });
            }
          }
        );

        draftState.groupedSeriesType = 3;
        if (
          draftState.trendDefinition.BandAxisBars &&
          draftState.trendDefinition.BandAxisBars.length > 0
        ) {
          // eslint-disable-next-line no-unsafe-optional-chaining
          for (const bandAxisBars of draftState.trendDefinition?.BandAxisBars) {
            if (bandAxisBars.Measurements) {
              for (const m of bandAxisBars.Measurements) {
                (<IGSMeasurement>m).AssetID = bandAxisBars.AssetID;
                (<IGSMeasurement>m).BarName = bandAxisBars.Label;
                m.Params1 = m.Params1 ? +m.Params1 : 0;
                m.Params2 = m.Params2 ? +m.Params2 : 0;
                m.Params3 = m.Params3 ? +m.Params3 : 0;
                m.Params4 = m.Params4 ? +m.Params4 : 0;
                m.Params5 = m.Params5 ? +m.Params5 : 0;
                draftState.groupSeriesMeasurements.push({
                  ...m,
                });
              }
            }
          }

          if (
            draftState.trendDefinition.BandAxisBars.every(
              (c) => !!c.TimeDivision
            )
          ) {
            draftState.groupedSeriesType = GroupedSeriesType.TIME;
          }
          if (
            draftState.trendDefinition.BandAxisBars.every(
              (c) => c.TimeDivision === 'pins'
            )
          ) {
            draftState.groupedSeriesType = GroupedSeriesType.PINS;
          }
          if (draftState.groupedSeriesType === GroupedSeriesType.TIME) {
            draftState.groupedSeriesSubType = 'week';
            if (
              draftState.trendDefinition.BandAxisBars &&
              draftState.trendDefinition.BandAxisBars.length > 0
            ) {
              if (draftState.trendDefinition.BandAxisBars[0].TimeDivision) {
                draftState.groupedSeriesSubType =
                  draftState.trendDefinition.BandAxisBars[0].TimeDivision;
              }
            }
          }
        }

        if (draftState.groupSeriesMeasurements?.length > 0) {
          draftState.groupSeriesMeasurements.sort((a, b) => {
            if (a.DisplayOrder !== undefined && b.DisplayOrder !== undefined) {
              return a.DisplayOrder - b.DisplayOrder;
            } else {
              return 0;
            }
          });

          draftState.groupSeriesMeasurements =
            draftState.groupSeriesMeasurements.map((series, index) => {
              const newSeries = { ...series };
              newSeries.DisplayOrder = index + 1;
              return newSeries;
            });
        }
      } else {
        draftState.groupedSeriesType = GroupedSeriesType.NONE;
        draftState.groupedSeriesSubType = '';
      }

      draftState.trendDefinition.ShowSelected = true;
      // Turn off the active selection in the event that pins are present.
      // This might be use in the future when a trend is opened on DE.
      if (
        draftState.trendDefinition?.Pins &&
        draftState.trendDefinition?.Pins.length > 0
      ) {
        draftState.trendDefinition.ShowSelected = false;
      }

      draftState.trendDefinition.Pins = [
        ...draftState.trendDefinition.Pins,
      ].map((pins, index) => {
        const newPins = { ...pins };
        newPins.DisplayOrder = index + 1;
        return newPins;
      });

      draftState.trendDefinition.Series = [
        ...draftState.trendDefinition.Series,
      ].sort((a, b) => a.DisplayOrder - b.DisplayOrder);

      draftState.trendDefinition.Series = [
        ...draftState.trendDefinition.Series,
      ].map((series, index) => {
        const newSeries = { ...series };
        newSeries.visible = true;
        newSeries.DisplayOrder = index + 1;
        return newSeries;
      });

      draftState.trendDefinition.Axes = [
        ...draftState.trendDefinition.Axes,
      ].map((axis) => {
        const newAxis = { ...axis };
        newAxis.OriginalMin = newAxis.Min;
        newAxis.OriginalMax = newAxis.Max;
        return newAxis;
      });
    });

    return newTrend;
  });

  return trends;
}

export function processModelContextTrend(
  givenTrend: IProcessedTrend,
  measurementsSet: IAssetMeasurementsSet,
  startDate?: Date | undefined,
  endDate?: Date | undefined
): IProcessedTrend {
  const newTrend = produce(givenTrend, (draftState) => {
    if (givenTrend.trendDefinition?.Pins) {
      // this is to fix the startTime / end time pin bug
      // the PDTrends endpoint gives the wrong times for pins
      // TagsDataFiltered gives the correct ones
      draftState.trendDefinition.Pins = measurementsSet.Pins;
    }
    draftState.measurements = measurementsSet;
    draftState.startDate = startDate;
    draftState.endDate = endDate;
  });

  return newTrend;
}
