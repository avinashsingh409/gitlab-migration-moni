import { isInvalidDate } from './is-invalid-date';

describe('isInvalidDate', () => {
  it('should return a date value if supplied a date value', () => {
    expect(isInvalidDate('dummyDate')).toEqual(false);
  });

  it('should return a date value if supplied a date value2', () => {
    expect(isInvalidDate(new Date('february 32 2012'))).toEqual(true);
  });
});
