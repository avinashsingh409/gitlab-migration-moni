// Groupby is an experimental feature in Chrome/Firefox as of 2022
export function groupByKey(array: any, key: any) {
  return array.reduce((hash: any, obj: any) => {
    if (obj[key] === undefined) return hash;
    const tempObject = Object.assign(hash, {
      [obj[key]]: (hash[obj[key]] || []).concat(obj),
    });
    const finalArray = [];
    Object.keys(tempObject).forEach((each) => {
      finalArray.push(tempObject[`${each}`]);
    });
    return finalArray;
  }, {});
}
