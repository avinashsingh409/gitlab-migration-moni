export const MAX_INT_VALUE = 2147483647;
export const KEY_BACKSPACE = 'Backspace';
export const KEY_DELETE = 'Delete';
export const KEY_F2 = 'F2';
export const KEY_ENTER = 'Enter';
export const KEY_TAB = 'Tab';
