import { IAssetAttribute } from '../data-types/asset-type';
import {
  IAttributeTypeWithOptions,
  ITempAttributeType,
} from '../data-types/attribute-type';
import { attributeValue } from './attribute-value';

// This is a pure function.
export function getAttributeDisplayValue(
  attribute: IAssetAttribute,
  attributeType: IAttributeTypeWithOptions
): ITempAttributeType {
  const result: ITempAttributeType = {
    name: '',
    value: '',
    displayOrder: attribute.DisplayOrder,
    isURL: false,
    typeString: 'string',
  };

  result.value = attributeValue(attribute, attributeType.Options);

  if (attributeType) {
    result.name = attributeType.AttributeType.AttributeTypeDesc;
    const myDispFormat = (
      attributeType.AttributeType.DisplayFormat || ''
    ).toLowerCase();
    result.typeString = myDispFormat;
  }
  if (
    !attributeType.AttributeType.IsStandard &&
    result.value &&
    result.value.length > 0
  ) {
    result.isURL =
      result.value.toUpperCase().indexOf('HTTP') > -1 ||
      result.value.substring(0, 1) === '/';
    if (result.isURL) {
      result.typeString = 'url';
    }
  }
  return result;
}
