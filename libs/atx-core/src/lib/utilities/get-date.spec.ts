import { getDate } from './get-date';

describe('getDate', () => {
  it('should return a date value if supplied a date value', () => {
    const testDate: Date = new Date('2019-01-16');
    expect(getDate(testDate)).toEqual(testDate);
  });

  it('should return a date value if supplied an integer', () => {
    const testDate: Date = new Date(500000000000);
    expect(getDate(500000000000)).toEqual(testDate);
  });

  it('should return a date value if supplied a date string value', () => {
    expect(getDate('2019-01-16')).toEqual(new Date('2019-01-16'));
  });
});
