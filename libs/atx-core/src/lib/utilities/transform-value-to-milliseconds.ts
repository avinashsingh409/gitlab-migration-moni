export function transformValueToMilliseconds(
  value: number,
  type: string
): number {
  let result = 0;
  const hourInMill = 1000 * 60 * 60;
  const dayInMill = hourInMill * 24;
  const weekInMill = dayInMill * 7;
  const monthInMill = dayInMill * 30;
  const yearInMill = dayInMill * 365;

  switch (type) {
    case 'Seconds':
      result = value * 1000;
      break;
    case 'Minutes':
      result = value * 1000 * 60;
      break;
    case 'Hours':
      result = value * hourInMill;
      break;
    case 'Days':
      result = value * dayInMill;
      break;
    case 'Weeks':
      result = value * weekInMill;
      break;
    case 'Months':
      result = value * monthInMill;
      break;
    case 'Years':
      result = value * yearInMill;
      break;
  }

  return result;
}
