import { IAssetAttribute } from '../data-types/asset-type';
import {
  IAttributeType,
  IAttributeTypeWithOptions,
} from '../data-types/attribute-type';
import { attributeValue } from './attribute-value';
import { getAttributeDisplayValue } from './get-attribute-display-value';

describe('getAttributeDisplayValue', () => {
  const testAttributeType: IAttributeType = {
    DisplayFormat: 'int',
  } as IAttributeType;
  const testAssetAttribute: IAssetAttribute = {
    Attribute_int: 1,
    AttributeType: testAttributeType,
    DisplayOrder: 2,
  } as IAssetAttribute;
  const testAttributeTypeWithOpts: IAttributeTypeWithOptions = {
    AttributeType: testAttributeType,
  } as IAttributeTypeWithOptions;

  it('should work on a test attribute', () => {
    expect(
      getAttributeDisplayValue(testAssetAttribute, testAttributeTypeWithOpts)
    ).toEqual({
      value: attributeValue(testAssetAttribute),
      name: testAttributeTypeWithOpts.AttributeType.AttributeTypeDesc,
      typeString: testAttributeTypeWithOpts.AttributeType.DisplayFormat,
      displayOrder: testAssetAttribute.DisplayOrder,
      isURL: false,
    });
  });
});
