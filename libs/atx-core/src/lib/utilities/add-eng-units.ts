import { IAssetAttribute } from '../data-types/asset-type';

export function addEngUnits(
  val: string,
  attribute: IAssetAttribute,
  excludeUnits?: boolean
): string {
  if (!excludeUnits && attribute.AttributeType) {
    return val + ' ' + (attribute.AttributeType.EngrUnits || '');
  }
  return val;
}
