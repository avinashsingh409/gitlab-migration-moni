import {
  IAssetMeasurements,
  IAssetVariableTypeTagMap,
  IPDTag,
} from '../data-types/asset-type';
import { ICategory } from '../data-types/category-type';

export enum GroupedSeriesType {
  NONE = 0,
  PINS = 1,
  TIME = 2,
  CUSTOM = 3,
  ASSET = 4,
}

export interface IArchiveExt {
  Archive: string;
  ArchiveDesc: string;
  CreateDate: Date;
  ChangeDate: Date;
  Selected: boolean;
  ValueStartDate: Date;
  ValueEndDate: Date;
  Period?: number;
}

export interface IPDRecordedPoint {
  Time: Date;
  Value: number;
  Status: number;
}

export interface IMapData {
  Map: IAssetVariableTypeTagMap;
  PinID?: number;
  Data: IPDRecordedPoint[];
}

export interface IPDTrendSeries {
  PDTrendSeriesID: number;
  PDTrendID: number;
  PDTagID?: number;
  PDVariableID?: number;
  IsMain: boolean;
  DisplayOrder: number;
  Axis: number;
  DisplayText: string;
  AlternateDisplayTexts: string[];
  UseDisplayText: boolean;
  ValueTypeID?: number;
  IsXAxis: boolean;
  IsFiltered: boolean;
  FilterMin?: number | string;
  FilterMax?: number | string;
  ShowBestFitLine: boolean;
  AssetClassTypeID?: number;
  ScaleMin?: number;
  ScaleMax?: number;
  ScaleDecimals?: number;
  ChartTypeID?: number;
  ChartType: any;
  SeriesColor: string;
  IsZAxis: boolean;
  FlatlineThreshold: number;
  Exclude: string;
  Stack: string;
  StackType: string;
  UseVariableID1?: number;
  UserVariableID2?: number;
  CalcDeviation?: boolean;
  IncludeAllAssets: boolean;
  IncludeDescendants: boolean;
  IncludeChildren: boolean;
  IncludeAncestors: boolean;
  IncludeParents: boolean;
  IncludeSiblings: boolean;
  IncludeUnitDescendants: boolean;
  IncludeCousins: boolean;
  IncludeSecondCousins: boolean;
  IncludeUnit: boolean;
  IncludeSelf: boolean;
  MapData: IMapData[];
  ApplyToAll: boolean;
  Archive: string;
  ArchiveExt: IArchiveExt;
  UseGlobalDataRetrieval?: boolean;
  SummaryTypeID?: number;
  SummaryType: ISummaryType;
  // client-side only
  isAP?: boolean;
  availableArchives?: string[];
  excludeMonday?: boolean;
  excludeTuesday?: boolean;
  excludeWednesday?: boolean;
  excludeThursday?: boolean;
  excludeFriday?: boolean;
  excludeSaturday?: boolean;
  excludeSunday?: boolean;
  excludeHours?: string;
  availArchives?: IArchiveExt[];
  showDataRetrieval?: boolean;
  showFilter?: boolean;
  visible?: boolean;
  isBold?: boolean;
  // end client-side only
}

export interface IPDTrendAxis {
  PDTrendAxisID: number;
  PDTrendID: number;
  Axis: number;
  Title: string;
  Position: number;
  OriginalMin?: number | string;
  OriginalMax?: number | string;
  Min?: number | string;
  Max?: number | string;
  Step?: number | string;
  MinorStep?: number | string;
  IsDefault: boolean;
  GridLine: boolean;
  PositionBool?: boolean;
}

export interface IPDTrendFilter {
  FilterMin?: number | string;
  FilterMax?: number | string;
  FlatlineThreshold: number;
  Exclude: string;
  Color?: string;
  SeriesID?: number;
  ApplyToAll: boolean;
  Archive: string;
  excludeMonday?: boolean;
  excludeTuesday?: boolean;
  excludeWednesday?: boolean;
  excludeThursday?: boolean;
  excludeFriday?: boolean;
  excludeSaturday?: boolean;
  excludeSunday?: boolean;
  excludeHours?: string;
  excludeJanuary?: boolean;
  excludeFebruary?: boolean;
  excludeMarch?: boolean;
  excludeApril?: boolean;
  excludeMay?: boolean;
  excludeJune?: boolean;
  excludeJuly?: boolean;
  excludeAugust?: boolean;
  excludeSeptember?: boolean;
  excludeOctober?: boolean;
  excludeNovember?: boolean;
  excludeDecember?: boolean;
  PDTrendPinSeriesID?: number; // used for referencing pin filter
}

export interface IPDTrendBandAxis {
  PDTrendBandAxisID: number;
  PDTrendID: number;
  Label: string;
  Opposite: boolean;
  IsVertical: boolean;
  RenderAs?: string;
  BarWidth?: number;
  Summarize?: string;
  Clockwise?: boolean;
  StartAngle?: number;
  EndAngle?: number;
}

export interface ITimeData {
  Start: Date;
  End: Date;
}

export interface ICriteria {
  CoID: number;
  GlobalID: string;
  CoTitle: string;
  CoDescription: string;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  CoDFID: number;
  RangeFlag: boolean;
  IndicatorFlag: boolean;
}

export interface IProcessedTrend {
  id: string;
  label: string;
  trendDefinition: IPDTrend;
  totalSeries: number;
  labelIndex?: number; // these are the label selections on the trend.
  startDate?: Date;
  endDate?: Date;
  assetID?: string;
  assetGuid?: string;
  isNew?: boolean;
  groupedSeriesType?: GroupedSeriesType;
  groupedSeriesSubType?: string;
  measurements?: IAssetMeasurementsSet;
  groupSeriesMeasurements?: IGSMeasurement[];
  showDataCursor?: boolean;
  loadingData?: boolean;
  isDirty?: boolean;
  modelTrendMap?: INDModelPDTrendMap;
}

export interface IPDTrendPin {
  PDTrendPinID: number;
  PDTrendID: number;
  StartTime: string | Date;
  EndTime: string | Date;
  Name: string;
  NameFormat: string;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: string | Date;
  ChangeDate: string | Date;
  TempPinName?: string;
  EditName?: boolean;
  PinColor?: number;
  Filters: IPDTrendFilter[];
  Hidden?: boolean;
  SpanUnits?: string;
  CriteriaObjectId?: number;
  SelectedCriteriaObject?: any;
  span?: number;
  DisplayOrder?: number;
}

export interface IPDTrendBandAxisBarMeasuredValue {
  MeasurementID: number;
  Value1: number;
  Value2: number;
  Value3: number;
  Value4: number;
  Value5: number;
  Points: number[];
  Markers: IPDTrendBandAxisMeasurementMarkerConfig[];
}

export interface IPDTrendBandAxisBar {
  PDTrendBandAxisBarID: number;
  PDTrendID: number;
  Label?: string;
  AssetID?: number;
  AssetClassTypeID?: number;
  IncludeSelf?: boolean;
  IncludeChildren?: boolean;
  IncludeDescendants?: boolean;
  IncludeParents?: boolean;
  IncludeSiblings?: boolean;
  IncludeAncestors?: boolean;
  IncludeCousins?: boolean;
  IncludeSecondCousins?: boolean;
  IncludeUnit?: boolean;
  IncludeUnitDescendants?: boolean;
  IncludeAllAssets?: boolean;
  CriteriaObjectID?: number;
  VariableTypeID?: number;
  ValueType?: number;
  TagID?: number;
  TimeDivision?: string;
  DisplayOrder?: number;
  Context?: string;

  Derived: boolean;
  StartTime?: Date;
  EndTime?: Date;

  Measurements?: IPDTrendBandAxisMeasurement[];
}

export interface IPDTrendArea {
  PDTrendAreaID: number;
  PDTrendID: number;
  Axes: number[];
  Size: number;
  Invert?: boolean;
  DisplayOrder: number;
}

export interface IPDTrendBandAxisMeasurementMarkerConfig {
  PDTrendBandAxisMeasurementMarkerConfigID: number;
  PDTrendBandAxisMeasurementID: number;
  Name?: string;
  Symbol?: string;
  Color?: string;
  FillColor?: string;
  BorderThickness?: number;
  VariableTypeID?: number;
  ValueTypeID?: number;
  AttributeTypeID?: number;

  TagID?: number;
  AggregationTypeID?: number;
  Min?: number;
  Max?: number;
  MinInclusive?: boolean;
  MaxInclusive?: boolean;

  Value?: number;
}

// Grouped Series Charts Use this interface
export interface IPDTrendBandAxisMeasurement {
  TrendBandAxisMeasurementID: number;
  BarID?: number;
  PDTrendID: number;
  Axis: number;
  Name: string;
  Type: string;
  Offset?: number;
  TooltipStyle?: string;
  Symbol?: string;
  Color?: string;
  FillColor?: string;
  BorderThickness?: number;
  Invert?: boolean;
  Value1?: number; // this number is not-null for a 'constant' data source
  Value2?: number;
  Value3?: number;
  Value4?: number;
  Value5?: number;
  VariableType1?: number;
  VariableType2?: number;
  VariableType3?: number;
  VariableType4?: number;
  VariableType5?: number;
  ValueType1?: number;
  ValueType2?: number;
  ValueType3?: number;
  ValueType4?: number;
  ValueType5?: number;
  AggregationType1?: number;
  AggregationType2?: number;
  AggregationType3?: number;
  AggregationType4?: number;
  AggregationType5?: number;
  Params1?: string | number;
  Params2?: string | number;
  Params3?: string | number;
  Params4?: string | number;
  Params5?: string | number;
  AttributeType1?: number;
  AttributeType2?: number;
  AttributeType3?: number;
  AttributeType4?: number;
  AttributeType5?: number;
  TagID1?: number; // this number is not-null for a 'tag' data source
  TagID2?: number;
  TagID3?: number;
  TagID4?: number;
  TagID5?: number;
  Result1?: number;
  Result2?: number;
  Result3?: number;
  Result4?: number;
  Result5?: number;
  TooltipHideMin?: boolean;
  MarkerConfigs?: IPDTrendBandAxisMeasurementMarkerConfig[];
  IsFiltered: boolean;
  FilterMin?: number;
  FilterMax?: number;
  FlatlineThreshold?: number;
  Exclude?: string;
  ApplyToAll: boolean;
  UseGlobal: boolean;
  Flags?: string;
  DisplayOrder?: number;
  Points?: number[];
}

export interface IPDTrendDataRetrieval {
  // UseDefault: boolean;
  DataRetrievalID?: number;
  PDTrendID?: number;
  Method: number; // 0 = default, 1 = archive name, 2 = interval & units
  MinInterval: number;
  MinIntervalUnits: string;
  Archive: string;
  Period?: number;
}

export interface ISummaryType {
  SummaryTypeID: number;
  SummaryDesc: string;
  SummaryAbbrev: string;
  DisplayOrder: number;
}

export interface IGSMeasurement extends IPDTrendBandAxisMeasurement {
  AssetID?: number;
  BarName?: string;
  config?: boolean;
  showFilter?: boolean;
  showDataRetrieval?: boolean;
  Tag1?: IPDTag;
  Tag2?: IPDTag;
  Tag3?: IPDTag;
  Tag4?: IPDTag;
  Tag5?: IPDTag;
  Custom?: boolean;
  SourceType1?: string;
  SourceType2?: string;
  SourceType3?: string;
  SourceType4?: string;
  SourceType5?: string;
  excludeMonday?: boolean;
  excludeTuesday?: boolean;
  excludeWednesday?: boolean;
  excludeThursday?: boolean;
  excludeFriday?: boolean;
  excludeSaturday?: boolean;
  excludeSunday?: boolean;
}
export interface IPDTrend {
  PDTrendID: number;
  TrendDesc: string;
  Title: string;
  LegendVisible: boolean;
  LegendDock: number;
  LegendContentLayout: number;
  IsCustom: boolean;
  CreatedBy: string;
  IsPublic: boolean;
  DisplayOrder: number;
  ParentTrendID?: number;
  ChartTypeID?: number | string;
  ChartType: any;
  Series: IPDTrendSeries[];
  Axes: IPDTrendAxis[];
  IsStandardTrend: boolean;
  Filter: IPDTrendFilter;
  Pins: IPDTrendPin[];
  PinTypeID: number;
  ShowPins: boolean;
  ShowSelected: boolean;
  TrendSource?: string;
  ProjectId?: string;
  Path?: string;
  Math?: string;
  DesignCurves?: DesignCurve[];
  BandAxis?: IPDTrendBandAxis;
  BandAxisBars?: IPDTrendBandAxisBar[];
  BandAxisBarsResolved?: IPDTrendBandAxisBar[];
  Areas?: IPDTrendArea[];
  BandAxisMeasurements?: IPDTrendBandAxisMeasurement[];
  DataRetrieval?: IPDTrendDataRetrieval;
  NDModelTrendMapID?: number;
  SummaryTypeID?: number;
  SummaryType: ISummaryType;
  Categories: ICategory[];
  XAxisGridlines: boolean;
}

export interface DesignCurve {
  Axis: number;
  Color: string;
  DisplayText: string;
  PDTrendDesignCurveID: number;
  PDTrendID: number;
  Repeat: string;
  Type: string;
  Values: string;
}

interface IDataPoint {
  Status: number;
  Value: number;
  Time: Date;
  Icon?: string;
}

export interface IAssetMeasurementsSet {
  Measurements: IAssetMeasurements[];
  Annotations: IAnnotation[];
  Bands: IPDTrendBandAxisBar[];
  Pins: IPDTrendPin[];
  Errors: ITagRetrievalError[];
}

export interface IAnnotationCategory {
  AnnotationCategoryId: number;
  AnnotationCategoryName: string;
  AnnotationCategoryDesc: string;
  Icon: string;
}

export interface IAnnotation {
  AnnotationId: number;
  AssetId: number;
  TagId: number;
  AnnotationCategoryId: number;
  CreateDate?: Date | string;
  UserId?: number;
  StartDate: Date | string;
  EndDate: Date | string;
  AnnotationProperties: IAnnotationProperty[];
  AnnotationCategory?: IAnnotationCategory;
}

export interface IAnnotationType {
  AnnotationTypePropertyId: number;
  AnnotationTypeId: number;
  AnnotationTypePropertyTypeId: number;
  PropertyName: string;
}

export interface IAnnotationProperty {
  AnnotationPropertyMapId: number;
  AnnotationId: number;
  AnnotationTypePropertyId: number;
  ValueAsText: string;
  AnnotationProperty?: IAnnotationType;
}

export interface ITagRetrievalError {
  ErrorType: number; // 0:No feasible archive found;  1:Exceeding maximum allowable data points
  TagNames: string[];
}

export interface IWidget {
  ReportSectionMapID: number;
  ReportSectionID: number;
  WidgetID: string;
  Title: string;
  Subtitle: string;
  Width: number;
  Kind: 'Chart' | 'Table' | 'FixedChart' | 'Gauge' | 'KPI' | 'Text';
  AssetID: number;
  DisplayOrder: number;
  widthPct?: number;
  loading?: boolean;
}

export interface IWidgetTable extends IWidget {
  PDTrendID: number;
  trend: IProcessedTrend;
}

export interface IWidgetChart extends IWidget {
  PDTrendID: number;
  trend: IProcessedTrend;
}

export interface IWidgetFixedChart extends IWidget {
  Content: string;
}

export interface IWidgetText extends IWidget {
  Content: string;
  Editable: boolean;
}

export interface IWidgetGauge extends IWidget {
  AssetVariableTypeTagMapID: number;
  Min: number;
  Max: number;
  SummaryID: number;
  Summary: ISummaryType;
  Scenario: string; // only used for AP tags
  value?: number;
  units?: string;
  TagName?: string;
  TagDesc?: string;
  TagAssetId?: number;
}

export interface IWidgetKPI extends IWidget {
  AssetVariableTypeTagMapID: number;
  Background: string;
  SummaryID: number;
  Summary: ISummaryType;
  Scenario: string; // only used for AP tags
  value?: number;
  units?: string;
  TagName?: string;
  TagDesc?: string;
  TagAssetId?: number;
}

export interface ViewExplorerTrends {
  widget: IWidgetChart | IWidgetTable;
  trend: IProcessedTrend;
}

export interface INDModelPDTrendMap {
  NDModelPDTrendMapID: number;
  ModelID: number;
  PDTrendID: number;
  AssetID: number;
  ReplaceTrendContents: boolean;
  DisplayOrder: number;
  MappedPDTrend: IPDTrend;
  AssetGuid: string;
}

export interface IPDServer {
  pDServerID: number;
  pDServerTypeID: number;
  serverDesc: string;
  filterDesc: string;
  comment: string;
  active: boolean;
  assetID: number;
  externalID: string;
  globalID: string;
  nDCreated: boolean;
  nDPumpID: string;
  nDPumpAction: number;
  apiDetails: string;
  nDNewArchitecture: boolean;
  isAlerts: boolean;
  alertPointPumpID: number;
}
