export interface IAtxAssetTree {
  TreeId?: string;
  TreeName: string;
  IsDefaultTree: boolean;
  IsUserOwned: boolean;
  CustomerId: string;
}

export interface IAtxAssetTreeNode {
  NodeId: string;
  TreeId: string;
  NodeAbbrev: string;
  NodeDesc: string;
  ParentNodeId?: string;
  NodeTypeId: number;
  DisplayOrder: number; // not using. Do we need to? => yes, tree-builder.service uses it to sort nodes before rendering them.
  AssetId?: number; // this is used in model.service.ts. Not sure it is needed anywhere else.
  AssetNodeBehaviorId?: number;
  Asset?: any;
  ParentUniqueKey: string; // this is used in asset tree logic. See tree-builder service
  UniqueKey: string;
  AssetGuid: string;
  ReferencedBy: any;
  HasChildren: boolean; // tree builder service
  AssetPath?: string;
}

export interface IAtxTreeRetrieval {
  assetTrees: IAtxAssetTree[];
  nodes: IAtxAssetTreeNode[];
  tree: string;
  selectedNode: string;
}

export interface IAutoCompleteItem {
  UniqueID: string;
  Display: string;
}

export interface IAutoCompleteResult {
  items: IAutoCompleteItem[];
  search: string;
  treeID: string;
}

export interface ITreePermissions {
  canView: boolean;
  canAdd: boolean;
  canEdit: boolean;
  canDelete: boolean;
}

// Helper method to get the tree that is selected from a list of nodes.
//  Typically the list of nodes will all have the same
// tree ID.  This will select that tree ID from the list.
export function getSelectedTree(nodes: IAtxAssetTreeNode[]) {
  return nodes
    .map((n) => n.TreeId)
    .reduce((total: string, currentValue: string, currentIndex, arr) => {
      return total || currentValue;
    }, null);
}
