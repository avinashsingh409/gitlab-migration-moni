export interface IEvent {
  EventID: number;
  AssetID: number;
  AssetPath: string;
  EventTypeID: number;
  StartTime: Date;
  EndTime: Date;
  Impact: number;
  Description: string;
  ModifiedByEventID?: number;
  modified?: boolean;
  eventType?: string;
}

export interface IEventType {
  EventTypeID: number;
  Description: string;
}

export interface IEventChange {
  EventID: number;
  New: IEvent;
  Previous: IEvent;
}

export interface IUserEventWithDescription {
  EventID: number;
  EventTypeID: number;
  Description: string;
  StartTime: Date;
  Impact?: number;
  EndTime?: Date;
}
