export interface ISecurityRights {
  CanView: boolean;
  CanAdd: boolean;
  CanEdit: boolean;
  CanDelete: boolean;
  CanAdmin: boolean;
}
export interface AdvancedPreview {
  Name: string;
  Rights: ISecurityRights;
}

export interface SecurityRights {
  alpha: boolean;
  beta: boolean;
}
