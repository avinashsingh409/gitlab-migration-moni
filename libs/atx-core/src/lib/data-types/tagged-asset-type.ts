import { ITaggingKeyword } from './tagging-keyword-type';

export interface ITaggedAsset {
  AssetInfo: ITaggedAssetInfo;
  Tags: ITaggingKeyword[];
}

export interface ITaggedAssetInfo {
  AssetId: number;
  AssetAbbrev: string;
  AssetPath: string;
  Guid: string;
  ParentAssetId: number;
  AssetClassTypeDesc: string;
}

export interface ITaggedAssetAttachment {
  AssetAttachmentId: string;
  ContentGuid: string;
  Filename: string;
  AssetInfo: ITaggedAssetInfo;
  Tags: ITaggingKeyword[];
  AttachmentType: number;
  Path: string;
}

export interface ITaggedAssetIssue {
  AssetIssueId: number;
  AssetInfo: ITaggedAssetInfo;
  Tags: ITaggingKeyword[];
  Summary: string;
  Guid: string;
}
