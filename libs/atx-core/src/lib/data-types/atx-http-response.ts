export interface IAtxHttpResponse<T> {
  Success: boolean;
  StatusCode: number;
  Type: string;
  Count: number;
  Results: T[];
}

export interface IAtxHttpError {
  Code: string;
  Message: string;
  ObjectName: string;
  PropertyName: string;
  CorrelationID: string;
}
