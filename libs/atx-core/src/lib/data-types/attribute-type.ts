import { ICategory } from './category-type';

export interface IAttributeTypeWithOptions {
  AttributeType: IAttributeType;
  Options: IAttributeTypeOption[];
  Categories: ICategory[];
}

export interface IAttributeType {
  AttributeTypeID: number;
  AttributeTypeDesc: string;
  AttributeTypeKey: string;
  EngrUnits: string;
  DisplayFormat: string;
  Categories: ICategory[];
  IsStandard: boolean;
  IsExempt: boolean;
  ClientID: number;
}

export interface IAttributeTypeOption {
  attributeOptionTypeID: number;
  attributeTypeID: number;
  attributeOptionTypeDesc: string;
  displayOrder: number;
}

export interface ITempAttributeType {
  name: string;
  value: string;
  displayOrder: number;
  isURL: boolean;
  typeString: string;
}
