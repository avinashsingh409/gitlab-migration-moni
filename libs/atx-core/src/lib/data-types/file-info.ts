export interface IFileInfo {
  UniqueID: string;
  Url: string;
  Name: string;
  IsImage: boolean;
}

export interface IAWSFileUrl {
  ContentID: string;
  Url: string;
  ContentType: string;
}
