export interface TemporalType {
  CoTemporalTypeID: number;
  Title: string;
  Abbrev: string;
  DisplayOrder: number;
}

export interface ReferenceType {
  CoDFRelativeReferenceTypeID: number;
  Title: string;
  Abbrev: string;
  DisplayOrder: number;
}

export interface ShiftType {
  CoDFRelativeShiftTypeID: number;
  Title: string;
  Abbrev: string;
  DisplayOrder: number;
}

export interface TimeFilterType {
  CoDFTimeFilterTypeID: number;
  Title: string;
  Abbrev: string;
  IsPreDataRetrieval: boolean;
  DisplayOrder: number;
  ParamDefaults: any[];
}

export interface CoDSSourceType {
  CoDSSourceTypeID: number;
  Title: string;
  Abbrev: string;
  DisplayOrder: number;
}

export interface CriteriaObjectTypes {
  TemporalTypes: TemporalType[];
  ReferenceTypes: ReferenceType[];
  ShiftTypes: ShiftType[];
  TimeFilterTypes: TimeFilterType[];
  CoDSSourceTypes: CoDSSourceType[];
}

export interface TimeFilter {
  CoID: number;
  CoDFID: number;
  CoDFTimeFilterID: number;
  IsPrivate: boolean;
  ExecuteOrder: number;
  CoDFTimeFilterTypeID: number;
  AssetGlobalID: string;
  UniqueKey: string;
  TFParams: CoDFTimeParam[];
  Title: string;
  Description: string;
  ChangedBy: string;
  ChangeDate: Date;
}

export enum TimeFilterRangeType {
  Fixed = 1,
  RelativeSpanOffset = 2,
}
export const DefaultStartTypeID = 2;
export const DefaultRelativeToID = 1;
export const DefaultOffsetID = 1;
export const DefaultSpanID = 1;
export const DefaultTimeFilter: TimeFilter = {
  CoDFID: -1,
  CoID: -1,
  IsPrivate: false,
  ExecuteOrder: 1,
  Description: 'New Time Range Description',
  Title: 'New Time Range',
  CoDFTimeFilterID: -1,
  CoDFTimeFilterTypeID: TimeFilterRangeType.Fixed,
  AssetGlobalID: null,
  UniqueKey: null,
  TFParams: [
    {
      CoDFTimeParamsID: -1,
      PName: 'AbsoluteStart',
      PValue: '',
    },
    {
      CoDFTimeParamsID: -1,
      PName: 'AbsoluteEnd',
      PValue: '',
    },
  ],
  ChangedBy: null,
  ChangeDate: null,
};

export interface CoDFTimeParam {
  PName: string;
  PValue: string;
  CoDFTimeParamsID: number;
}

export interface LegacyTimeFilter {
  CoDFID: number;
  CoDFTimeFilterID: number;
  CoDFTimeFilterType?: any;
  CoDFTimeFilterTypeID: number;
  ExecuteOrder: number;
  TFParams: CoDFTimeParam[];
}

export interface LegacyDataFilter {
  CoDFID: number;
  ExcludeDaysFilter?: any;
  ExcludeHoursFilter?: any;
  AssetID?: any;
  ChangeDate?: any;
  ChangedBy?: any;
  CreateDate?: any;
  CreatedBy?: any;
  Description: string;
  GlobalID?: any;
  IsPrivate: boolean;
  Title: string;
  TimeFilter: LegacyTimeFilter;
}
