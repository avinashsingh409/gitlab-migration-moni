import { IAssetVariableTypeTagMap } from './asset-type';

export interface IDataPoint {
  Status: number;
  Value: number;
  Time: Date;
  Icon?: string;
}

export interface IDataPointResult {
  data: IDataPoint[];
  queryID: any;
}

export interface INDDataPoint {
  ModelInputTagMapID?: number;
  AssetVariableTypeTagMapID: number;
  ExternalID: string;
  GlobalID?: string;
  Path: string;
  Name: string;
  AssetVariableTypeTagMap: IAssetVariableTypeTagMap;
  selected: boolean;
  NDId?: string;
}

export interface ILastDataPoint {
  Sensor: IAssetVariableTypeTagMap;
  Data: ISample;
}

export interface ISample {
  Status: number;
  Value: number;
  Timestamp: Date;
}
