export interface CalculationFactors {
  AssetIssueImpactCalculationFactorID: number;
  CalcuationFactorAbbrev: string;
  CalculationFactorDesc: string;
  CalculationFactor: number;
  CalculationFactorRatio: number;
  CalculationFactorUnits: string;
  AssetIssueImpactCategoryTypeID: number;
}

export interface ImpactCalculatorSave {
  AssetIssueID: number;
  ImpactScenariosToSave?: ImpactScenarioSave[];
  ImpactScenarioIDsToDelete?: number[];
}

export interface ImpactTypeMapSave {
  AssetIssueAssetIssueImpactTypeMapID: number;
  AssetIssueImpactTypeID: number;
  CategoryID: number;
  Impact?: number;
  Impact_Cost?: number;
  InputString: string;
  Impact_Cost_Monthly?: number;
}

export interface ImpactTypeMapV2Save {
  AssetIssueAssetIssueImpactTypeMapID: number;
  AssetIssueImpactTypeID: number;
  CategoryID: number;
  Impact?: number;
  ImpactContent: string;
  Impact_Cost?: number;
  InputString: string;
  Impact_Cost_Monthly?: number;
}

export interface ImpactScenarioSave {
  AssetIssueImpactScenarioID: number;
  AssetIssueImpactScenarioName: string;
  AssetIssueImpactScenarioNotes: string;
  PercentLikelihood: number;
  Generation_AssetIssueAssetIssueImpactTypeMapID?: number;
  Efficiency_AssetIssueAssetIssueImpactTypeMapID?: number;
  Maintenance_AssetIssueAssetIssueImpactTypeMapID?: number;
  OtherCosts_AssetIssueAssetIssueImpactTypeMapID?: number;
  GenerationImpactRange?: ImpactDateRange;
  EfficiencyImpactRange?: ImpactDateRange;
  MaintenanceImpactRange?: ImpactDateRange;
  OtherCostsImpactRange?: ImpactDateRange;
  GenerationImpact?: ImpactTypeMapV2Save;
  EfficiencyImpact?: ImpactTypeMapV2Save;
  MaintenanceImpact?: ImpactTypeMapSave;
  OtherCostsImpact?: ImpactTypeMapSave;
}
export enum CalculationType {
  CalculatedImpact = 'CalculatedImpact',
  UserDefinedImpact = 'UserDefinedImpact',
}

export interface TimeValues {
  toDate: number;
  potentialFuture: number;
  totalAvg: number;
}

export interface AssetIssueImpactScenario {
  AssetIssueImpactScenarioID: number;
  OwningAssetIssueID: number;
  AssetIssueImpactScenarioName: string;
  AssetIssueImpactScenarioNotes: string;
  PercentLikelihood: number;
  Generation_AssetIssueAssetIssueImpactTypeMapID?: number;
  Efficiency_AssetIssueAssetIssueImpactTypeMapID?: number;
  Maintenance_AssetIssueAssetIssueImpactTypeMapID?: number;
  OtherCosts_AssetIssueAssetIssueImpactTypeMapID?: number;
  Scope_AssetIssueAssetissueImpactTypeMapID?: number;
  Schedule_AssetIssueAssetIssueImpactTypeMapID?: number;
  Budget_AssetIssueAssetIssueImpactTypeMapID?: number;
  Safety_AssetIssueAssetIssueImpactTypeMapID?: number;
  ImpactContent: string;
  GenerationImpactRange?: ImpactDateRange;
  EfficiencyImpactRange?: ImpactDateRange;
  MaintenanceImpactRange?: ImpactDateRange;
  OtherCostsImpactRange?: ImpactDateRange;
  ScopeImpactRange?: ImpactDateRange;
  ScheduleImpactRange?: ImpactDateRange;
  BudgetImpactRange?: ImpactDateRange;
  SafetyImpactRange?: ImpactDateRange;
  CapacityCost: number;
  FuelCost: number;
  CapacityFactor: number;
  UnitCapability: number;
  NetUnitHeatRate: number;
  GenerationImpact?: any;
  generationCalculations?: any;
  EfficiencyImpact?: any;
  MaintenanceImpact?: any;
  OtherCostsImpact?: any;
  ScopeImpact?: any;
  ScheduleImpact?: any;
  BudgetImpact?: any;
  SafetyImpact?: any;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedBy: string;
  ChangedBy: string;
  CreatedByUserID: number;
}

export interface ImpactDateRange {
  StartDate: Date;
  CurrentDate: Date;
  EndDate: Date;
  DaysElapsed: number;
  DaysPending: number;
  DaysTotal: number;
}

export interface ImpactAttributes {
  AttrName: string;
  AttrType: string;
  AttrValue: string;
}

export interface BlankScenario {
  CreatedByUserID: number;
  AssetIssueImpactScenarioName: string;
  AssetIssueImpactScenarioID: number;
  NetUnitHeatRate: number;
  UnitCapability: number;
  CapacityFactor: number;
  FuelCost: number;
  CapacityCost: number;
  PercentLikelihood: number;
  ImpactAttributesList: ImpactAttributes[];
}
