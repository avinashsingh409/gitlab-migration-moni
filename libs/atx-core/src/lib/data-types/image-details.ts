import { SafeStyle } from '@angular/platform-browser';

export interface IImageDetails {
  BlobUrl: SafeStyle;
  ContentID: string;
  Url: string;
}
