export interface IPieSlice {
  SliceDescription: string;
  SliceID: number;
  SliceSize: number;
  SlicePercent: number;
}

export interface IAssetIssueDonutData {
  IssuesByAsset: IPieSlice[];
  IssuesByResolution: IPieSlice[];
  IssuesByCategory: IPieSlice[];
  Impact: IPieSlice[];
}

export interface IAssetIssueDonutDataRetrieval {
  Success: boolean;
  StatusCode: number;
  Type: any;
  Count: number;
  Results: IAssetIssueDonutData;
}
