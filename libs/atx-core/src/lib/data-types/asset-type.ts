import {
  IAttributeType,
  IAttributeTypeOption,
  IAttributeTypeWithOptions,
} from './attribute-type';
import { ICategory } from './category-type';
import { IDataPoint, ISample } from './data-point-type';
import { IGPSCoordinate } from './gps-coordinate-type';
import { ITaggingKeyword } from './tagging-keyword-type';

export interface IAssetAndAttributes {
  Asset: IAsset;
  Parent: IAsset;
  Attributes: IAssetAttribute[];
  AvailableAttributes: IAttributeTypeWithOptions[];
  AllAttributes: IAttributeTypeWithOptions[];
  Attachments: IAssetAttachment[];
  AvailableAttributeCategories: ICategory[];
  AvailableAttachmentCategories: ICategory[];
  UsedAttributeCategories: ICategory[];
  UsedAttachmentCategories: ICategory[];
}

export interface IAssetAttachment {
  AttachmentID: string;
  AssetID: number;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedBy: string;
  ChangedBy: string;
  ContentID: string;
  DisplayInstructions: string;
  AttachmentType: number;
  Title: string;
  Caption: string;
  DisplayOrder: number;
  Favorite: boolean;
  Head: string;
  complete?: boolean;
  progressPercentage?: number;
  path?: string;
  uploadError?: string;
  $$treeLevel?: number;
  hasKids?: boolean;
  Keywords?: ITaggingKeyword[];
  IsTheOwner: boolean;
  CreatedByUserID?: number;
  ChangedByUserID?: number;
  PreSignedURL: string;
  Categories: ICategory[];
  CategoriesLabel: string;
}

export interface IAssetAttribute {
  AssetAttributeID: number;
  AssetID: number;
  AttributeTypeID: number;
  AttributeOptionTypeID: number;
  Attribute_int: number;
  Attribute_string: string;
  Attribute_float: number;
  Attribute_date: Date;
  AttributeType: IAttributeType;
  Favorite: boolean;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate?: Date;
  ChangeDate?: Date;
  IsTheOwner: boolean;
  Categories: ICategory[];
  Options: IAttributeTypeOption[];
  Value?: string;
}

export interface IAsset {
  AssetID: number;
  AssetAbbrev: string; // do we still need both Abbrev and Desc? Not sure what differences are
  AssetDesc: string;
  AssetClassTypeID: number; // also in AssetClassType.AssetClassTypeID
  AssetTypeID: number; // used in Asset Config
  DisplayOrder: number;
  GlobalId: string; // strangely, just used one place in aset explorer :)
  AssetClassType: IAssetClassType; // used by Info tab in Asset Explorer. Looks like we just need AssetClassTypeID and AssetClassTypeDesc
  Keywords?: ITaggingKeyword[]; // used in Asset Explorer. Probably easy to refactor out
}

export interface IAssetType {
  AssetTypeID: number;
  AssetTypeAbbrev: string;
  AssetTypeDesc: string;
}

export interface IAssetClassType {
  AssetClassTypeID: number;
  AssetTypeID: number;
  AssetClassTypeKey: string;
  AssetClassTypeAbbrev: string;
  AssetClassTypeDesc: string;
  DisplayOrder: number;
}

export interface IAssetInfo {
  Asset: IAsset;
  System: IAsset;
  Unit: IAsset;
  Station: IAsset;
  StationGroup: IAsset;
  Client: IAsset;
  Longitude: number;
  Latitude: number;
}

export interface IAggregateAssetMeasurements {
  Tag: IPDTag;
  TagVariableValueType: number;
  TagVariableDesc: string;
  DataAggregateValue: number;
  Data: Array<ISample>;
}

export interface IAssetMeasurements {
  Tag: any;
  Data: IDataPoint[];
  SensorID: number;
  proposedSeriesMin?: number;
  proposedSeriesMax?: number;
  actualSeriesMin?: number;
  actualSeriesMax?: number;
  Statistics: any;
  Archive: string;
  PinID?: any;
  displayText?: string;
  color?: string;
  units?: string;
}

export interface IAssetVariableTypeTagMap {
  AssetVariableTypeTagMapID: number;
  AssetID: number;
  VariableTypeID?: number;
  TagID?: number;
  CalcString: string;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: string | Date;
  ChangeDate: string | Date;
  ValueTypeID?: number;
  WeightingVariableTypeID?: number;
  WeightingValueTypeID?: number;
  StatusVariableTypeID?: number;
  Asset: any;
  Tag: IPDTag;
  VariableType: IVariableType;
  ValueType: any;
  IsModelled: boolean;
  selected?: boolean;
  StatusValueTypeID?: number;
  Recommended?: TagMapRecommendedType;
}

export enum TagMapRecommendedType {
  RecommendedSelected = 0,
  NotRecommendedSelected = 1,
  RecommendedNotSelected = 2,
  NotRecommendedNotSelected = 3,
}

export interface IAssetClassTypeVariableTypeMap {
  AssetClassTypeVariableTypeMapID: number;
  AssetClassTypeID: number;
  VariableTypeID: number;
  DisplayOrder: number;
}

export interface IAssetVariableTypeTagMapResult {
  data: IAssetVariableTypeTagMap[];
  queryID: any;
}

export interface IPDTag {
  PDTagID: number;
  PDServerID: number;
  TagName: string;
  TagDesc: string;
  EngUnits: string;
  ExistsOnServer: boolean;
  ExternalID: string;
  Qualifier: string;
  Mapping: string;
  GlobalID: string;
  NDCreated: boolean;
  NDPath: string;
  NDName: string;
  IsAP?: boolean;
  NDId?: string;
  ChangeDate?: string | Date;
}

export interface IVariableType {
  PDVariableID: number;
  VariableDesc: string;
  VariableAbbrev: string;
  VariableName: string;
  EngUnits: string;
  CalcString: string;
  FunctionTypeID?: number;
  WeightingVariableTypeID?: number;
  DefaultValueTypeID?: number;
}

export interface IValueType {
  ValueTypeID: number;
  ValueTypeDesc: string;
  ValueTypeKey: string;
  Comment: string;
  IsDefault?: boolean;
  IssuePrecedence?: number;
}

export interface IAssetAccess {
  Asset: IAsset;
  Allowed: boolean;
  NERCCIP: boolean;
}

export interface ITagMapBare {
  AssetVariableTypeTagMapId: number | null;
  AssetId: string;
  TagId: string;
  ValueTypeKey: string;
  VariableDesc: string;
}

export interface IPDTagMap extends ITagMapBare {
  FullPath: string;
  AssetName: string;
  AssetPath: string;
  AssetClassTypeKey: string;
  VariableTypeUnits: string;
  PDServerName: string;
  TagName: string;
  TagDescription: string;
  TagEngUnit: string;
  ExistsOnServer: boolean;
  Hierarchy: string[];
}

export interface ITagMapCreateUpdateResult extends ITagMapBare {
  Error: string;
}

export interface ITagMapAuditResult {
  AssetVariableTypeTagMapId: number;
  Message: string;
}

export interface IServerResult {
  Id: string;
  Name: string;
  AssetId: string;
  PDServerId: number;
}

export interface ITagResult {
  Id: string;
  Name: string;
  Description: string;
  EngUnit: string;
  Qualifier: string;
  Source: string;
  CreateDate?: Date;
  ChangeDate?: Date;
  ExistsOnServer: boolean;
  InUse: number;
  IsDirty: boolean;
}

export interface ITagCreateUpdateResult extends ITagResult {
  Error: string;
}

export interface IAssetResult {
  AssetId: string;
  AssetName: string;
  AssetDesc: string;
  AssetClassTypeKey: string;
  AssetClassTypeDesc: string;
  ParentName: string;
  ParentAssetId: string;
  AssetPath: string;
  DisplayOrder: number;
  IsStandard: boolean;
  CreateDate: Date;
  ChangeDate: Date;
  Hierarchy: string[];
}

export interface IAssetClassTypeResult {
  AssetClassTypeKey: string;
  AssetClassTypeDesc: string;
  IsStandard: boolean;
}

export interface IAssetCreate {
  ParentAssetPath: string;
  AssetName: string;
  AssetDesc: string;
  AssetClassTypeKey: string;
  DisplayOrder: number;
}

export interface IAssetUpdate {
  AssetId: string;
  ParentAssetId: string;
  AssetName: string;
  AssetDesc: string;
  AssetClassTypeKey: string;
  DisplayOrder: number;
}

export interface IAssetCreateUpdateResult extends IAssetResult {
  Error: string;
}

export interface IAssetCreateValidateResult extends IAssetCreate {
  Message: string;
  IsError: boolean;
}

export type UpdateAssetType = 'single' | 'multiple';
