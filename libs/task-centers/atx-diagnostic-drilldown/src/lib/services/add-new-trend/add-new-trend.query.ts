import {
  createTreeBuilder,
  getDefaultTree,
  getIDFromSelectedAssets,
  getSelectedNodes,
  ITreeState,
} from '@atonix/atx-asset-tree';
import { INDModelPDTrendMap } from '@atonix/atx-core';
import { ComponentStore } from '@ngrx/component-store';
import moment from 'moment';

export interface AddNewTrendState {
  assetDesc: string;
  modelId: string;
  modelName: string;
  timeSelection: { startDate: Date; endDate: Date };
  assetTreeState: ITreeState;
  existingTrends: INDModelPDTrendMap[];
  availableTrends: INDModelPDTrendMap[];
  loadingTrends: boolean;
  isSaved: boolean;
}

export const initialAddNewTrendState: AddNewTrendState = {
  assetDesc: '',
  modelId: '',
  modelName: '',
  timeSelection: { startDate: moment().toDate(), endDate: moment().toDate() },
  assetTreeState: {
    treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  existingTrends: [],
  availableTrends: [],
  loadingTrends: false,
  isSaved: false,
};

export class AddNewTrendQuery {
  constructor(private store: ComponentStore<AddNewTrendState>) {}

  readonly assetDesc$ = this.store.select((state) => state.assetDesc, {
    debounce: true,
  });

  readonly modelId$ = this.store.select((state) => state.modelId, {
    debounce: true,
  });

  readonly modelName$ = this.store.select((state) => state.modelName, {
    debounce: true,
  });

  readonly timeSelection$ = this.store.select((state) => state.timeSelection, {
    debounce: true,
  });

  readonly assetTreeState$ = this.store.select(
    (state) => state.assetTreeState,
    {
      debounce: true,
    }
  );

  readonly treeConfiguration$ = this.store.select(
    (state) => state.assetTreeState?.treeConfiguration,
    {
      debounce: true,
    }
  );

  readonly assetId$ = this.store.select(
    (state) =>
      state.assetTreeState?.treeConfiguration?.nodes?.find(
        (a) =>
          a.uniqueKey ===
          getIDFromSelectedAssets(
            getSelectedNodes(state.assetTreeState.treeNodes)
          )
      )?.data?.AssetId,
    {
      debounce: true,
    }
  );

  readonly existingTrends$ = this.store.select(
    (state) => state.existingTrends,
    {
      debounce: true,
    }
  );

  readonly availableTrends$ = this.store.select(
    (state) => state.availableTrends,
    {
      debounce: true,
    }
  );

  readonly loadingTrends$ = this.store.select((state) => state.loadingTrends, {
    debounce: true,
  });

  readonly isSaved$ = this.store.select((state) => state.isSaved, {
    debounce: true,
  });

  readonly vm$ = this.store.select(
    this.assetDesc$,
    this.modelName$,
    this.treeConfiguration$,
    this.existingTrends$,
    this.availableTrends$,
    this.loadingTrends$,
    (
      assetDesc,
      modelName,
      treeConfiguration,
      existingTrends,
      availableTrends,
      loadingTrends
    ): any => {
      return {
        assetDesc,
        modelName,
        treeConfiguration,
        existingTrends,
        availableTrends,
        loadingTrends,
      };
    }
  );
}
