import { Injectable } from '@angular/core';
import { ITreeState, ITreeStateChange } from '@atonix/atx-asset-tree';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  Observable,
  exhaustMap,
  switchMap,
  withLatestFrom,
  tap,
  map,
  of,
  mergeMap,
} from 'rxjs';
import { ModelService as AssetTreeModelService } from '@atonix/atx-asset-tree';
import { AddNewTrendCommands } from './add-new-trend.commands';
import {
  AddNewTrendQuery,
  AddNewTrendState,
  initialAddNewTrendState,
} from './add-new-trend.query';
import {
  AlertsFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';
import { INDModelPDTrendMap } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class AddNewTrendFacade extends ComponentStore<AddNewTrendState> {
  public query = new AddNewTrendQuery(this);
  public command = new AddNewTrendCommands(this);

  constructor(
    private assetTreeModelService: AssetTreeModelService,
    private alertsFrameworkService: AlertsFrameworkService,
    private processDataFrameworkService: ProcessDataFrameworkService
  ) {
    super(initialAddNewTrendState);
  }

  readonly treeStateChange = this.effect(
    (change$: Observable<ITreeStateChange>) => {
      return change$.pipe(
        withLatestFrom(this.query.assetTreeState$),
        tap(([change, treeState]) => this.command.treeStateChange(change)),
        switchMap(([change, treeState]) =>
          this.assetTreeModelService
            .getAssetTreeData(
              change,
              treeState.treeConfiguration,
              treeState.treeNodes
            )
            .pipe(
              tapResponse(
                (c) => this.treeStateChange(c),
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            )
        )
      );
    }
  );

  readonly getAvailableTrends = this.effect((assetId$: Observable<string>) => {
    return assetId$.pipe(
      withLatestFrom(this.query.modelId$, this.query.timeSelection$),
      tap(() => this.command.setLoadingTrends()),
      switchMap(([assetId, modelId, timeSelection]) =>
        this.processDataFrameworkService
          .getPDNDModelTrends(
            assetId,
            modelId,
            false,
            timeSelection.startDate,
            timeSelection.endDate
          )
          .pipe(
            tapResponse(
              (mappedTrends) =>
                this.command.processAvailableTrends(mappedTrends),
              (error: string) => {
                console.error(JSON.stringify(error));
              }
            )
          )
      )
    );
  });

  readonly saveSelectedTrends = this.effect(
    (selectedTrends$: Observable<INDModelPDTrendMap[]>) => {
      return selectedTrends$.pipe(
        withLatestFrom(this.query.modelId$),
        tap(() => this.command.setLoadingTrends()),
        switchMap(([trends, modelId]) =>
          this.alertsFrameworkService
            .savePDTrendsMappedToNDModel(modelId, trends)
            .pipe(
              tapResponse(
                () => this.command.saveCompleted(),
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            )
        )
      );
    }
  );
}
