import {
  alterAssetTreeState,
  ITreeStateChange,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import {
  getTrendTotalSeries,
  INDModelPDTrendMap,
  IProcessedTrend,
} from '@atonix/atx-core';
import { ComponentStore } from '@ngrx/component-store';
import {
  AddNewTrendState,
  initialAddNewTrendState,
} from './add-new-trend.query';
export class AddNewTrendCommands {
  constructor(private store: ComponentStore<AddNewTrendState>) {}

  readonly resetAddNewTrendState = this.store.updater(
    (state: AddNewTrendState): AddNewTrendState => {
      return initialAddNewTrendState;
    }
  );

  readonly initializeAddNewTrendState = this.store.updater(
    (
      state: AddNewTrendState,
      payload: {
        assetDesc: string;
        modelId: string;
        modelName: string;
        timeSelection: {
          startDate: Date;
          endDate: Date;
        };
        existingTrends: INDModelPDTrendMap[];
      }
    ): AddNewTrendState => {
      return {
        ...state,
        assetDesc: payload.assetDesc,
        modelId: payload.modelId,
        modelName: payload.modelName,
        timeSelection: payload.timeSelection,
        existingTrends: payload.existingTrends,
      };
    }
  );

  readonly treeStateChange = this.store.updater(
    (state: AddNewTrendState, change: ITreeStateChange): AddNewTrendState => {
      const newChange = treeRetrievedStateChange(change);
      return {
        ...state,
        assetTreeState: alterAssetTreeState(state.assetTreeState, newChange),
      };
    }
  );

  readonly processAvailableTrends = this.store.updater(
    (
      state: AddNewTrendState,
      mappedTrends: INDModelPDTrendMap[]
    ): AddNewTrendState => {
      const processedTrends = mappedTrends?.map((m) => {
        const result = {
          id: String(m.MappedPDTrend?.PDTrendID),
          label: m.MappedPDTrend?.Title,
          trendDefinition: m.MappedPDTrend,
          totalSeries: getTrendTotalSeries(m?.MappedPDTrend),
          labelIndex: 0,
        } as IProcessedTrend;
        return result;
      });

      const trendsWithSeries: IProcessedTrend[] = [];
      const mapsWithTrendsWithSeries: INDModelPDTrendMap[] = [];
      const availableTrends: INDModelPDTrendMap[] = [];

      for (const trend of processedTrends) {
        if (trend.totalSeries > 0 || trend.trendDefinition.TrendSource) {
          trendsWithSeries.push(trend);
        }
      }

      for (const pt of trendsWithSeries) {
        for (const m of mappedTrends) {
          if (pt.trendDefinition.PDTrendID === m.PDTrendID) {
            mapsWithTrendsWithSeries.push(m);
          }
        }
      }
      for (const m of mapsWithTrendsWithSeries) {
        if (
          (m.PDTrendID > -1 || m.NDModelPDTrendMapID > -1) &&
          [...state.existingTrends].findIndex(
            (e) => e.AssetID === m.AssetID && e.PDTrendID === m.PDTrendID
          ) === -1
        ) {
          availableTrends.push(m);
        }
      }

      return {
        ...state,
        availableTrends,
        loadingTrends: false,
      };
    }
  );

  readonly setLoadingTrends = this.store.updater(
    (state: AddNewTrendState): AddNewTrendState => {
      return {
        ...state,
        availableTrends: [],
        loadingTrends: true,
      };
    }
  );

  readonly saveCompleted = this.store.updater(
    (state: AddNewTrendState): AddNewTrendState => {
      return {
        ...state,
        isSaved: true,
        loadingTrends: false,
      };
    }
  );
}
