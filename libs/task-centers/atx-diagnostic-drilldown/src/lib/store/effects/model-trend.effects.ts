/* eslint-disable ngrx/no-multiple-actions-in-effects */
import { Injectable } from '@angular/core';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import * as actions from '../actions/model-trend.actions';
import { getModelContextCorrelationTrends } from '../actions/model-context.actions';
import * as fromDiagnosticDrilldown from '../reducers/diagnostic-drilldown.reducer';

@Injectable()
export class ModelTrendEffects {
  constructor(
    private actions$: Actions,
    private store: Store,
    private alertsFrameworkService: AlertsFrameworkService,
    private toastService: ToastService
  ) {}

  loadModelTrend$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getModelTrend),
      withLatestFrom(
        this.store.select(fromDiagnosticDrilldown.selectModelId),
        this.store.select(fromDiagnosticDrilldown.selectTimeSliderSelection)
      ),
      switchMap(([action, modelId, timeSelection]) => {
        return this.alertsFrameworkService
          .getPDModelTrendForAlertChartingByModelExtId(
            modelId,
            timeSelection.start,
            timeSelection.end
          )
          .pipe(
            map((trend) =>
              actions.getModelTrendSuccess({
                trend,
                start: timeSelection.start,
                end: timeSelection.end,
              })
            ),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) => {
              return of(actions.getModelTrendFailure(error));
            })
          );
      }),
      switchMap((action) => {
        return [action, getModelContextCorrelationTrends()];
      })
    );
  });
}
