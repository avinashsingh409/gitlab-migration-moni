import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { updateRouteModelAssetTimeSliderWithoutReloading } from '@atonix/atx-navigation';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import {
  catchError,
  concatMap,
  map,
  mergeMap,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { NavActions } from '@atonix/atx-navigation';
import * as diagnosticDrilldownActions from '../actions/diagnostic-drilldown.actions';
import * as timeSliderActions from '../actions/time-slider.actions';
import * as modelContextActions from '../actions/model-context.actions';
import * as modelTrendActions from '../actions/model-trend.actions';
import { isNil } from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import {
  selectAssetId,
  selectModelId,
} from '../reducers/diagnostic-drilldown.reducer';
import { copyToClipboard } from '@atonix/atx-chart-v2';
import { of } from 'rxjs';
import { AlertsCoreService, IModelConfigData } from '@atonix/shared/api';

@Injectable()
export class DiagnosticDrilldownEffects {
  constructor(
    private actions$: Actions,
    private store: Store,
    private router: Router,
    private alertsCoreService: AlertsCoreService,
    private activatedRoute: ActivatedRoute
  ) {}

  getModelAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(diagnosticDrilldownActions.getModelAsset),
      // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
      switchMap((payload) =>
        this.alertsCoreService.getModelConfig([payload.modelId]).pipe(
          map((models: IModelConfigData[]) =>
            diagnosticDrilldownActions.updateAssetId({
              assetId: models[0]?.dependent.assetGuid || '',
            })
          )
        )
      )
    );
  });

  initializeDiagnosticDrilldown$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(diagnosticDrilldownActions.initializeDiagnosticDrilldown),
      // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
      switchMap((payload) => [
        timeSliderActions.initializeTimeSlider({
          start: payload.start,
          end: payload.end,
        }),
        NavActions.showTimeSlider(),
        NavActions.taskCenterLoad({
          taskCenterID: 'diagnostic-drilldown',
          timeSlider: true,
          timeSliderOpened: true,
          assetTree: false,
          assetTreeOpened: false,
          asset: payload.assetId ?? '',
        }),
        modelContextActions.getModelContextTrends(),
        modelTrendActions.getModelTrend(),
      ])
    );
  });

  updateRoute$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(timeSliderActions.updateTimeSlider),
      withLatestFrom(
        this.store.select(selectModelId),
        this.store.select(selectAssetId)
      ),
      concatMap(([action, modelId, assetId]) => {
        const start = action?.start?.getTime().toString();
        const end = action?.end?.getTime().toString();
        if (!isNil(start) && !isNil(end)) {
          updateRouteModelAssetTimeSliderWithoutReloading(
            modelId,
            assetId,
            start,
            end,
            this.router,
            this.activatedRoute
          );
        }
        // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
        return [
          NavActions.updateNavigationItems({
            urlParams: { start, end },
          }),
          modelContextActions.loadModelContextTrendsData(),
          modelTrendActions.getModelTrend(),
        ];
      })
    );
  });

  copyLink$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(diagnosticDrilldownActions.copyLink),
      mergeMap((payload) =>
        copyToClipboard(payload.link).pipe(
          map(
            () => diagnosticDrilldownActions.copyLinkSuccess(),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) =>
              of(diagnosticDrilldownActions.copyLinkFailure(error))
            )
          )
        )
      )
    );
  });

  reflowChart$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(
          NavActions.toggleNavigation,
          NavActions.toggleTimeslider,
          diagnosticDrilldownActions.splitDrag,
          diagnosticDrilldownActions.expandTopSection,
          modelContextActions.expandTrends
        ),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 200);
        })
      );
    },
    { dispatch: false }
  );
}
