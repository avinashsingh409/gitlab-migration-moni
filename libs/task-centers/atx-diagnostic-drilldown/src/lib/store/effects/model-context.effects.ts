/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  getTrendTotalSeries,
  INDModelPDTrendMap,
  IProcessedTrend,
} from '@atonix/atx-core';
import {
  AlertsFrameworkService,
  ProcessDataFrameworkService,
  INDModelSummary,
  AlertsCoreService,
} from '@atonix/shared/api';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { merge, of, iif } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import * as actions from '../actions/model-context.actions';
import * as diagnosticDrilldownActions from '../actions/diagnostic-drilldown.actions';
import {
  selectAllModelContextTrends,
  selectAssetId,
  selectModelContextTrends,
  selectModelId,
  selectModelTypeInfo,
  selectTimeSlider,
  selectTimeSliderSelection,
} from '../reducers/diagnostic-drilldown.reducer';

@Injectable()
export class ModelContextEffects {
  constructor(
    private actions$: Actions,
    private store: Store,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private alertsFrameworkService: AlertsFrameworkService,
    private alertsCoreService: AlertsCoreService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getModelContextCorrelationTrends$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getModelContextCorrelationTrends),
      concatLatestFrom(() => [
        this.store.select(selectModelTypeInfo),
        this.store.select(selectAssetId),
        this.store.select(selectModelId),
        this.store.select(selectTimeSlider),
      ]),
      switchMap(([action, modelTypeInfo, assetId, modelId, timeSlider]) => {
        return iif(
          () =>
            modelTypeInfo?.ModelTypeAbbrev === 'Forecast' ||
            modelTypeInfo?.ModelTypeAbbrev === 'Rate of Change',
          this.alertsCoreService.getCorrelationTrend(
            modelId || '',
            timeSlider?.startDate || new Date(),
            timeSlider?.endDate || new Date()
          ),
          of([])
        );
      }),
      switchMap((correlationTrends) => {
        return [
          actions.getModelContextCorrelationTrendsSuccess({
            correlationTrends: correlationTrends,
          }),
        ];
      })
    );
  });

  getModelContextTrends$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getModelContextTrends),
      withLatestFrom(
        this.store.select(selectAssetId),
        this.store.select(selectModelId)
      ),
      switchMap(([_, assetId, modelId]) => {
        if (modelId && assetId) {
          return [actions.getModelContextTrendsWithAssetGuid()];
        } else {
          return [
            actions.getAssetGuidForModelContextTrends({
              modelId: modelId ?? '',
            }),
          ];
        }
      })
    );
  });

  getAssetGuidForModelContextTrends$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAssetGuidForModelContextTrends),
      switchMap((action) => {
        if (action.modelId) {
          return this.alertsFrameworkService
            .getModelSummary(action.modelId)
            .pipe(
              switchMap((modelSummary: INDModelSummary[]) => {
                return [
                  diagnosticDrilldownActions.updateAssetId({
                    assetId: modelSummary[0].ModelExtID ?? '',
                  }),
                ];
              })
            );
        } else {
          return [
            actions.getModelContextTrendsFailure({
              error: new Error('no model Id'),
            }),
          ];
        }
      })
    );
  });

  getModelContextTrendsWithAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getModelContextTrendsWithAssetGuid),
      withLatestFrom(
        this.store.select(selectAssetId),
        this.store.select(selectModelId)
      ),
      switchMap(([action, assetId, modelId]) =>
        this.processDataFrameworkService
          .getPDNDModelTrendsWithAssetGuid(assetId, modelId)
          .pipe(
            switchMap((trendsMap: INDModelPDTrendMap[]) => {
              const trends = trendsMap.map((m) => {
                const result = {
                  id: String(m.MappedPDTrend?.PDTrendID),
                  assetID: String(m.AssetID),
                  assetGuid: m.AssetGuid,
                  label: m.MappedPDTrend?.Title,
                  trendDefinition: m.MappedPDTrend,
                  totalSeries: getTrendTotalSeries(m?.MappedPDTrend),
                  labelIndex: 0,
                  showDataCursor: true,
                  modelTrendMap: m,
                } as IProcessedTrend;
                return result;
              });

              return [
                actions.getModelContextTrendsSuccess({ trends }),
                actions.loadModelContextTrendsData(),
              ];
            }),
            catchError((error) =>
              of(actions.getModelContextTrendsFailure(error))
            )
          )
      )
    );
  });

  loadModelContextTrendsData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.loadModelContextTrendsData),
      withLatestFrom(
        this.store.select(selectModelContextTrends),
        this.store.select(selectTimeSliderSelection),
        this.store.select(selectAssetId)
      ),
      map(([action, trends, timeSelection, assetId]) => {
        return {
          trends,
          start: timeSelection.start,
          end: timeSelection.end,
          assetId,
        };
      }),
      switchMap((vals) =>
        // This will simultaneously call multiple APIs and return an action when the call is completed
        merge(
          ...vals.trends.map((trend) =>
            this.processDataFrameworkService
              .getTagsDataFiltered(
                trend.trendDefinition,
                vals.start,
                vals.end,
                0,
                '',
                vals.assetId
              )
              .pipe(
                map((measurementsSet) =>
                  actions.loadModelContextTrendsDataSuccess({
                    trend,
                    measurementsSet,
                    startDate: vals.start,
                    endDate: vals.end,
                  })
                ),
                catchError((error) =>
                  of(actions.loadModelContextTrendsDataFailure({ error }))
                )
              )
          )
        )
      )
    );
  });

  editChart$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.editChart),
        withLatestFrom(
          this.store.select(selectAssetId),
          this.store.select(selectTimeSliderSelection),
          this.store.select(selectAllModelContextTrends)
        ),
        tap(([action, asset, time, allTrends]) => {
          const trend = allTrends.find((t) => t.id === action.trendId);
          // Used sessionStorage rather than localStorage here to
          // respect opening multiple Model Context Trend consecutively in a short period of time.
          if (trend && +trend?.id < 0) {
            window.sessionStorage.setItem(
              'ModelContextTrend',
              JSON.stringify(trend.trendDefinition)
            );
          } else {
            window.sessionStorage.removeItem('ModelContextTrend');

            if (trend?.assetGuid !== '00000000-0000-0000-0000-000000000000') {
              asset = trend?.assetGuid || null;
            }
          }

          const dataExplorerUrl = `data-explorer?id=${asset}&trend=${trend?.trendDefinition.PDTrendID.toString()}&start=${time?.start
            ?.getTime()
            .toString()}&end=${time?.end?.getTime().toString()}`;
          let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
          }

          window?.open(url, '_blank')?.focus();
        })
      );
    },
    { dispatch: false }
  );
}
