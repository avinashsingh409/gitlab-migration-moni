/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable rxjs/no-unsafe-switchmap */
import { Injectable } from '@angular/core';
import { AlertsFrameworkService, INDModelActionItem } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import * as actions from '../actions/model-history.actions';

@Injectable()
export class ModelHistoryEffects {
  constructor(
    private actions$: Actions,
    private alertsFrameworkService: AlertsFrameworkService,
    private toastService: ToastService
  ) {}

  setFavoriteActionItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setModelHistoryFavorite),
      map((payload) => {
        return {
          ...payload.modelAction,
          Favorite: payload.isFavorite,
        } as INDModelActionItem;
      }),
      switchMap((actionItem) =>
        this.alertsFrameworkService.updateActionItem(actionItem).pipe(
          map(() => actions.setModelHistoryFavoriteSuccess()),
          catchError(() => of(actions.setModelHistoryFavoriteFailure()))
        )
      )
    )
  );

  setActionItemFavoriteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setModelHistoryFavoriteSuccess),
        tap(() => {
          this.toastService.openSnackBar('Favorite Changed', 'success');
        })
      ),
    { dispatch: false }
  );

  setActionItemFavoriteFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setModelHistoryFavoriteFailure),
        tap(() => {
          this.toastService.openSnackBar('Favorite Change Failed', 'error');
        })
      ),
    { dispatch: false }
  );

  setActionItemNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setModelHistoryNote),
      map((payload) => {
        return {
          ...payload.modelAction,
          NoteText: payload.note,
        } as INDModelActionItem;
      }),
      switchMap((actionItem) =>
        this.alertsFrameworkService.updateActionItem(actionItem).pipe(
          map(() => actions.setModelHistoryNoteSuccess()),
          catchError(() => of(actions.setModelHistoryNoteFailure()))
        )
      )
    )
  );

  setActionItemNoteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setModelHistoryNoteSuccess),
        tap(() => {
          this.toastService.openSnackBar('Note Changed', 'success');
        })
      ),
    { dispatch: false }
  );

  setActionItemNoteFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setModelHistoryNoteFailure),
        tap(() => {
          this.toastService.openSnackBar('Note Change Failed', 'error');
        })
      ),
    { dispatch: false }
  );
}
