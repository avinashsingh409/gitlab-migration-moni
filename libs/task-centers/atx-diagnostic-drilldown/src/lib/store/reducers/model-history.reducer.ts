import { IModelHistoryFilters } from '@atonix/shared/api';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/model-history.actions';

export const modelHistoryFeatureKey = 'modelHistory';
export interface IModelHistoryState {
  modelHistoryFilters?: IModelHistoryFilters;
  loading: boolean;
}

export const initialModelHistoryState: IModelHistoryState = {
  loading: false,
  modelHistoryFilters: {
    historyTypes: [
      'Watch Set',
      'Diagnose Set',
      'Diagnose Cleared',
      'Model Maintenance Set',
      'Model Maintenance Cleared',
      'Note Added',
    ],
    start: null,
    end: null,
    user: null,
    note: null,
    favorite: null,
  },
};

export const reducer = createReducer(
  initialModelHistoryState,
  on(actions.setModelHistoryFilters, (state, payload): IModelHistoryState => {
    return {
      ...state,
      modelHistoryFilters: payload.filters,
    };
  }),
  on(
    actions.setModelHistoryFavorite,
    actions.setModelHistoryNote,
    (state): IModelHistoryState => {
      return {
        ...state,
        loading: true,
      };
    }
  ),
  on(
    actions.setModelHistoryFavoriteSuccess,
    actions.setModelHistoryFavoriteFailure,
    actions.setModelHistoryNoteSuccess,
    actions.setModelHistoryNoteFailure,
    (state): IModelHistoryState => {
      return {
        ...state,
        loading: false,
      };
    }
  )
);

export const selectIsLoading = (state: IModelHistoryState) => state.loading;
