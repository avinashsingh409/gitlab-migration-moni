import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/diagnostic-drilldown.actions';
export const sharedFeatureKey = 'shared';

export interface ISharedState {
  assetId: string | null;
  modelId: string | null;
  expandTopSection: boolean;
}

export const initialSharedState: ISharedState = {
  assetId: null,
  modelId: null,
  expandTopSection: false,
};

export const reducer = createReducer(
  initialSharedState,
  on(actions.initializeDiagnosticDrilldown, (state, payload): ISharedState => {
    return {
      ...state,
      assetId: payload.assetId,
      modelId: payload.modelId,
    };
  }),
  on(actions.updateAssetId, (state, payload): ISharedState => {
    return {
      ...state,
      assetId: payload.assetId,
    };
  }),
  on(actions.expandTopSection, (state, payload): ISharedState => {
    return {
      ...state,
      expandTopSection: !state.expandTopSection,
    };
  })
);

export const assetId = (state: ISharedState) => state.assetId;
export const modelId = (state: ISharedState) => state.modelId;
export const expandTopSection = (state: ISharedState) => state.expandTopSection;
