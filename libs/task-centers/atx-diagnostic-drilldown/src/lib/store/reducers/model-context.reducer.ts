import { resetAxes, updateAxes } from '@atonix/atx-chart-v2';
import {
  IProcessedTrend,
  processModelContextTrend,
  processPDNDTrends,
} from '@atonix/atx-core';
import {
  createEntityAdapter,
  EntityAdapter,
  EntityState,
  Update,
} from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import produce from 'immer';
import * as actions from '../actions/model-context.actions';
import { ICorrelationData } from '@atonix/shared/api';
export const modelContextFeatureKey = 'modelContext';

export interface IModelContextState extends EntityState<IProcessedTrend> {
  loadingTrends: boolean;
  displayedTrendIndex: number | null;
  correlationTrends: ICorrelationData[];
}

export const modelContextAdapter: EntityAdapter<IProcessedTrend> =
  createEntityAdapter<IProcessedTrend>();

export const initialModelContextState: IModelContextState = {
  ids: [],
  entities: {},
  loadingTrends: false,
  displayedTrendIndex: null,
  correlationTrends: [],
};

export const reducer = createReducer(
  initialModelContextState,
  on(
    actions.getModelContextCorrelationTrendsSuccess,
    (state, payload): IModelContextState => {
      if (payload.correlationTrends.length === 0) {
        return { ...state };
      }
      return {
        ...state,
        correlationTrends: payload.correlationTrends,
      };
    }
  ),
  on(actions.getModelContextTrends, (state, payload): IModelContextState => {
    return {
      ...modelContextAdapter.removeAll(state),
      loadingTrends: true,
    };
  }),
  on(
    actions.getModelContextTrendsSuccess,
    (state, payload): IModelContextState => {
      return {
        ...modelContextAdapter.addMany(
          processPDNDTrends(payload.trends),
          state
        ),
        loadingTrends: false,
      };
    }
  ),
  on(
    actions.getModelContextTrendsFailure,
    (state, payload): IModelContextState => {
      return {
        ...modelContextAdapter.removeAll(state),
        loadingTrends: false,
      };
    }
  ),
  on(
    actions.loadModelContextTrendsData,
    (state, payload): IModelContextState => {
      const updatedTrends: Update<IProcessedTrend>[] = [];

      Object.entries(state.entities).forEach(([key, value]) => {
        updatedTrends.push({
          changes: {
            loadingData: true,
          },
          id: key,
        } as Update<IProcessedTrend>);
      });

      return {
        ...modelContextAdapter.updateMany(updatedTrends, state),
      };
    }
  ),
  on(
    actions.loadModelContextTrendsDataSuccess,
    (state, payload): IModelContextState => {
      const newTrend = processModelContextTrend(
        payload.trend,
        payload.measurementsSet,
        payload.startDate,
        payload.endDate
      );

      const updatedTrendData = {
        changes: {
          ...newTrend,
          loadingData: false,
        },
        id: payload.trend.id,
      } as Update<IProcessedTrend>;

      return {
        ...modelContextAdapter.updateOne(updatedTrendData, state),
      };
    }
  ),
  on(
    actions.loadModelContextTrendsDataFailure,
    (state, payload): IModelContextState => {
      return {
        ...state,
      };
    }
  ), // To do
  on(actions.expandTrends, (state, payload): IModelContextState => {
    return {
      ...state,
      displayedTrendIndex:
        state.displayedTrendIndex !== null ? null : payload.index,
    };
  }),
  on(actions.changeLabels, (state, payload): IModelContextState => {
    const updatedTrend: Update<IProcessedTrend> = {
      changes: {
        labelIndex: payload.index,
      },
      id: payload.trendId,
    } as Update<IProcessedTrend>;

    return {
      ...modelContextAdapter.updateOne(updatedTrend, state),
    };
  }),
  on(actions.showHideDataCursor, (state, payload): IModelContextState => {
    const trend = state.entities[payload.trendId];
    const updatedTrend: Update<IProcessedTrend> = {
      changes: {
        showDataCursor: !trend?.showDataCursor,
      },
      id: payload.trendId,
    } as Update<IProcessedTrend>;

    return {
      ...modelContextAdapter.updateOne(updatedTrend, state),
    };
  }),
  on(actions.legendItemToggle, (state, payload): IModelContextState => {
    //May be broken
    const newTrendDefinition = produce(
      state.entities[payload.trendId]?.trendDefinition,
      (trendDefinition) => {
        if (trendDefinition && trendDefinition?.Series[payload.seriesIndex]) {
          if (trendDefinition?.Series[payload.seriesIndex].visible) {
            trendDefinition.Series[payload.seriesIndex].visible = false;
          } else {
            trendDefinition.Series[payload.seriesIndex].visible = true;
          }
        }
      }
    );

    const updatedTrend: Update<IProcessedTrend> = {
      changes: {
        trendDefinition: newTrendDefinition,
      },
      id: payload.trendId,
    } as Update<IProcessedTrend>;

    return {
      ...modelContextAdapter.updateOne(updatedTrend, state),
    };
  }),
  on(actions.updateLimits, (state, payload): IModelContextState => {
    const trendDefinition = state.entities[payload.trendId]?.trendDefinition;
    const updatedTrend: Update<IProcessedTrend> = {
      changes: {
        trendDefinition: {
          ...trendDefinition,
          Axes: trendDefinition
            ? updateAxes(trendDefinition, payload.limitsData)
            : [],
        },
      },
      id: payload.trendId,
    } as Update<IProcessedTrend>;

    return {
      ...modelContextAdapter.updateOne(updatedTrend, state),
    };
  }),
  on(actions.resetLimits, (state, payload): IModelContextState => {
    const trendDefinition = state.entities[payload.trendId]?.trendDefinition;
    const updatedTrend: Update<IProcessedTrend> = {
      changes: {
        trendDefinition: {
          ...trendDefinition,
          Axes: trendDefinition
            ? resetAxes(trendDefinition, payload.axisIndex)
            : [],
        },
      },
      id: payload.trendId,
    } as Update<IProcessedTrend>;

    return {
      ...modelContextAdapter.updateOne(updatedTrend, state),
    };
  })
);

export const { selectAll } = modelContextAdapter.getSelectors();
export const loadingTrends = (state: IModelContextState) => state.loadingTrends;
export const displayedTrendIndex = (state: IModelContextState) =>
  state.displayedTrendIndex;
