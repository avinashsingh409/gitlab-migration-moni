import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as fromShared from './shared.reducer';
import * as fromModelContext from './model-context.reducer';
import * as fromTimeSlider from './time-slider.reducer';
import * as fromModelTrend from './model-trend.reducer';
import * as fromModelHistory from './model-history.reducer';
import { INDModelPDTrendMap } from '@atonix/atx-core';

export const diagnosticDrillDownFeatureKey = 'diagnostic-drilldown';
export interface DiagnosticDrilldownState {
  [fromShared.sharedFeatureKey]: fromShared.ISharedState;
  [fromModelContext.modelContextFeatureKey]: fromModelContext.IModelContextState;
  [fromTimeSlider.timeSliderFeatureKey]: fromTimeSlider.TimeSliderState;
  [fromModelTrend.modelTrendFeatureKey]: fromModelTrend.IModelTrendState;
  [fromModelHistory.modelHistoryFeatureKey]: fromModelHistory.IModelHistoryState;
}

export function reducers(
  state: DiagnosticDrilldownState | undefined,
  action: Action
) {
  return combineReducers({
    [fromShared.sharedFeatureKey]: fromShared.reducer,
    [fromModelContext.modelContextFeatureKey]: fromModelContext.reducer,
    [fromTimeSlider.timeSliderFeatureKey]: fromTimeSlider.reducer,
    [fromModelTrend.modelTrendFeatureKey]: fromModelTrend.reducer,
    [fromModelHistory.modelHistoryFeatureKey]: fromModelHistory.reducer,
  })(state, action);
}

const selectDiagnosticDrilldownState =
  createFeatureSelector<DiagnosticDrilldownState>(
    diagnosticDrillDownFeatureKey
  );

// Shared
export const selectSharedState = createSelector(
  selectDiagnosticDrilldownState,
  (state) => state.shared
);
export const selectAssetId = createSelector(
  selectSharedState,
  fromShared.assetId
);
export const selectModelId = createSelector(
  selectSharedState,
  fromShared.modelId
);
export const selectExpandTopSection = createSelector(
  selectSharedState,
  fromShared.expandTopSection
);

// Time Slider
export const selectTimeSliderState = createSelector(
  selectDiagnosticDrilldownState,
  (state) => state.timeSlider
);
export const selectTimeSlider = createSelector(
  selectTimeSliderState,
  (state) => state.timeSliderState
);
export const selectTimeSliderSelection = createSelector(
  selectTimeSliderState,
  fromTimeSlider.selectTimeSliderSelection
);
export const selectTimeSliderAsOfDate = createSelector(
  selectTimeSliderState,
  (state) => state.timeSliderState?.asOfDate
);

// Model Context
export const selectModelContextState = createSelector(
  selectDiagnosticDrilldownState,
  (state) => state.modelContext
);

export const selectModelContextCorrelationTrends = createSelector(
  selectModelContextState,
  (state) => state.correlationTrends
);

export const selectAllModelContextTrends = createSelector(
  selectModelContextState,
  fromModelContext.selectAll
);

export const selectModelContextLoadingTrends = createSelector(
  selectModelContextState,
  fromModelContext.loadingTrends
);

export const selectModelContextDisplayedTrendIndex = createSelector(
  selectModelContextState,
  fromModelContext.displayedTrendIndex
);

export const selectModelContextTrends = createSelector(
  selectAllModelContextTrends,
  selectModelContextDisplayedTrendIndex,
  (allTrends, displayedTrendIndex) => {
    if (displayedTrendIndex === null) {
      return allTrends;
    } else {
      return [allTrends[displayedTrendIndex]];
    }
  }
);

// Model Trend
export const selectModelTrendState = createSelector(
  selectDiagnosticDrilldownState,
  (state) => state.modelTrend
);
export const selectModelTypeInfo = createSelector(
  selectModelTrendState,
  (state) => state.trendData?.Model.ModelType
);
export const selectModelTrendData = createSelector(
  selectModelTrendState,
  fromModelTrend.trendData
);
export const selectModelTrendModelName = createSelector(
  selectModelTrendState,
  fromModelTrend.modelName
);
export const selectModelTrendModelID = createSelector(
  selectModelTrendState,
  fromModelTrend.modelID
);

export const selectModelTrendDisplayModelPath = createSelector(
  selectModelTrendState,
  fromModelTrend.displayModelPath
);

export const selectModelTrendLoading = createSelector(
  selectModelTrendState,
  fromModelTrend.loading
);

//Model History
export const selectModelHistoryState = createSelector(
  selectDiagnosticDrilldownState,
  (state) => state.modelHistory
);
export const selectModelHistoryIsLoading = createSelector(
  selectModelHistoryState,
  fromModelHistory.selectIsLoading
);

// Add New Trend Dialog
export const selectAllDrilldownStates = createSelector(
  selectModelTrendModelName,
  selectModelTrendDisplayModelPath,
  selectTimeSliderSelection,
  selectAllModelContextTrends,
  (modelName, modelPath, time, allTrends) => {
    const existingTrends: INDModelPDTrendMap[] = [];

    for (const m of allTrends) {
      if (
        m.modelTrendMap &&
        (m.modelTrendMap?.NDModelPDTrendMapID > -1 ||
          m.modelTrendMap?.PDTrendID > -1)
      ) {
        existingTrends.push(m.modelTrendMap);
      }
    }

    return {
      ModelName: modelName,
      AssetDesc: modelPath?.trim().split('\\').pop(),
      TimeSelection: { startDate: time.start, endDate: time.end },
      ExistingTrends: existingTrends,
    };
  }
);
