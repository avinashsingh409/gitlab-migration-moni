import { IAssetModelChartingData, INDModel } from '@atonix/shared/api';
import { createReducer, on } from '@ngrx/store';
import produce from 'immer';
import * as actions from '../actions/model-trend.actions';

export const modelTrendFeatureKey = 'modelTrend';
export interface IModelTrendState {
  trendData: IAssetModelChartingData | null;
  loading: boolean;
}

export const initialModelTrendState: IModelTrendState = {
  loading: false,
  trendData: null,
};

export const reducer = createReducer(
  initialModelTrendState,
  on(actions.getModelTrend, (state): IModelTrendState => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(actions.getModelTrendSuccess, (state, payload): IModelTrendState => {
    const trendData = {
      ...payload.trend,
      start: payload.start,
      end: payload.end,
      OriginalMin: payload.trend.Min,
      OriginalMax: payload.trend.Max,
    };

    return {
      ...state,
      trendData,
      loading: false,
    };
  }),
  on(actions.getModelTrendFailure, (state): IModelTrendState => {
    return {
      ...state,
      loading: false,
    };
  }),
  on(actions.updateLimits, (state, payload): IModelTrendState => {
    let trendData = state.trendData;

    if (state.trendData) {
      trendData = produce(state.trendData, (data) => {
        data.Min = payload.limitsData.Min;
        data.Max = payload.limitsData.Max;
      });
    }

    return {
      ...state,
      trendData,
    };
  }),
  on(actions.resetLimits, (state): IModelTrendState => {
    let trendData = state.trendData;

    if (state.trendData) {
      trendData = produce(state.trendData, (data) => {
        data.Min = data.OriginalMin;
        data.Max = data.OriginalMax;
      });
    }

    return {
      ...state,
      trendData,
    };
  })
);

export const trendData = (state: IModelTrendState) => state.trendData;
export const modelID = (state: IModelTrendState) =>
  state.trendData?.Model?.ModelID;
export const modelName = (state: IModelTrendState) =>
  state.trendData?.Model?.ModelName;
export const displayModelPath = (state: IModelTrendState) =>
  state.trendData?.Model?.DisplayModelPath;
export const loading = (state: IModelTrendState) => state.loading;
