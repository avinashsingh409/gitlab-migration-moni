import { IUpdateLimitsData } from '@atonix/atx-chart-v2';
import { IAssetModelChartingData, INDModel } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const getModelTrend = createAction(
  '[Diagnostic Drilldown - Model Trend] Get Model Trend'
);

export const getModelTrendSuccess = createAction(
  '[Diagnostic Drilldown - Model Trend] Get Model Trend Success',
  props<{
    trend: IAssetModelChartingData;
    start?: Date;
    end?: Date;
  }>()
);

export const getModelTrendFailure = createAction(
  '[Diagnostic Drilldown - Model Trend] Get Model Trend Failure',
  props<{ error: Error }>()
);

export const updateLimits = createAction(
  '[Diagnostic Drilldown - Model Trend] Update Limits',
  props<{ limitsData: IUpdateLimitsData }>()
);

export const resetLimits = createAction(
  '[Diagnostic Drilldown - Model Trend] Reset Limits'
);
