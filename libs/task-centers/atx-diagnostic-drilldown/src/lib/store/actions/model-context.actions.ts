import { IUpdateLimitsData } from '@atonix/atx-chart-v2';
import { IAssetMeasurementsSet, IProcessedTrend } from '@atonix/atx-core';
import { ICorrelationData } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const getModelContextCorrelationTrends = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Correlation Trends'
);

export const getModelContextCorrelationTrendsSuccess = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Correlation Trends Success',
  props<{ correlationTrends: ICorrelationData[] }>()
);

export const getAssetGuidForModelContextTrends = createAction(
  '[Diagnostic Drilldown - Model Context] Get Asset Guid for Model Context Trends',
  props<{ modelId: string }>()
);

export const getModelContextTrendsWithAssetGuid = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Trends With Asset Guid'
);

export const getModelContextTrends = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Trends'
);

export const getModelContextTrendsSuccess = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Trends Success',
  props<{ trends: IProcessedTrend[] }>()
);

export const getModelContextTrendsFailure = createAction(
  '[Diagnostic Drilldown - Model Context] Get Model Context Trends Failure',
  props<{ error: Error }>()
);

export const loadModelContextTrendsData = createAction(
  '[Diagnostic Drilldown - Model Context] Load Model Context Trends Data'
);

export const loadModelContextTrendsDataSuccess = createAction(
  '[Diagnostic Drilldown - Model Context] Load Model Context Trends Data Success',
  props<{
    trend: IProcessedTrend;
    measurementsSet: IAssetMeasurementsSet;
    startDate?: Date;
    endDate?: Date;
  }>()
);

export const loadModelContextTrendsDataFailure = createAction(
  '[Diagnostic Drilldown - Model Context] Load Model Context Trends Data Failure',
  props<{ error: Error }>()
);

export const expandTrends = createAction(
  '[Diagnostic Drilldown - Model Context] Expand Trends',
  props<{ index: number }>()
);

export const changeLabels = createAction(
  '[Diagnostic Drilldown - Model Context] Change Labels',
  props<{ index: number; trendId: string }>()
);

export const editChart = createAction(
  '[Diagnostic Drilldown - Model Context] Edit Chart',
  props<{ trendId: string }>()
);

export const showHideDataCursor = createAction(
  '[Diagnostic Drilldown - Model Context] Show/Hide Data Cursor',
  props<{ trendId: string }>()
);

export const legendItemToggle = createAction(
  '[Diagnostic Drilldown - Model Context] Legend Item Toggle',
  props<{ seriesIndex: number; trendId: string }>()
);

export const updateLimits = createAction(
  '[Diagnostic Drilldown - Model Context] Update Limits',
  props<{ limitsData: IUpdateLimitsData; trendId: string }>()
);

export const resetLimits = createAction(
  '[Diagnostic Drilldown - Model Context] Reset Limits',
  props<{ axisIndex: number; trendId: string }>()
);
