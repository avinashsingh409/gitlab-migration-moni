import { IModelHistoryFilters, INDModelActionItem } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const setModelHistoryFilters = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Filters',
  props<{ filters: IModelHistoryFilters }>()
);

export const setModelHistoryFavorite = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Favorite',
  props<{ modelAction?: INDModelActionItem; isFavorite: boolean }>()
);
export const setModelHistoryFavoriteSuccess = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Favorite Success'
);
export const setModelHistoryFavoriteFailure = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Favorite Failure'
);
export const setModelHistoryNote = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Note',
  props<{ modelAction?: INDModelActionItem; note: string }>()
);
export const setModelHistoryNoteSuccess = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Note Success'
);
export const setModelHistoryNoteFailure = createAction(
  '[Diagnostic Drilldown - Model History] Set Model History Note Failure'
);
