import { createAction, props } from '@ngrx/store';

export const getModelAsset = createAction(
  '[Diagnostic Drilldown] Get Model Asset',
  props<{
    modelId: string;
  }>()
);

export const initializeDiagnosticDrilldown = createAction(
  '[Diagnostic Drilldown] Initialize Diagnostic Drilldown',
  props<{
    assetId: string | null;
    modelId: string | null;
    start: Date;
    end: Date;
  }>()
);

export const updateAssetId = createAction(
  '[Diagnostic Drilldown] Get Asset Guid for Model Context Trends',
  props<{ assetId: string }>()
);

export const updateRoute = createAction('[Diagnostic Drilldown] Update Route');
export const splitDrag = createAction('[Diagnostic Drilldown] Split Drag');

export const expandTopSection = createAction(
  '[Diagnostic Drilldown] Expand Top Section'
);

export const copyLink = createAction(
  '[Diagnostic Drilldown] Copy Chart Link',
  props<{ link: string }>()
);

export const copyLinkSuccess = createAction(
  '[Diagnostic Drilldown] Copy Chart Link Success'
);

export const copyLinkFailure = createAction(
  '[Diagnostic Drilldown] Copy Chart Link Failure',
  props<{ error: any }>()
);
