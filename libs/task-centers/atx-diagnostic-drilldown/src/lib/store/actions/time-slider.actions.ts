import { createAction, props } from '@ngrx/store';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';

export const initializeTimeSlider = createAction(
  '[TimeSlider] Initialize Time Slider',
  props<{ start: Date; end: Date }>()
);
export const updateTimeSlider = createAction(
  '[TimeSlider] Update Time Slider',
  props<{ start: Date; end: Date }>()
);
export const timeSliderStateChange = createAction(
  '[TimeSlider] Time Slider State Change',
  props<{ stateChange: ITimeSliderStateChange }>()
);

export const hideTimeSlider = createAction('[TimeSlider] Hide Time Slider');
