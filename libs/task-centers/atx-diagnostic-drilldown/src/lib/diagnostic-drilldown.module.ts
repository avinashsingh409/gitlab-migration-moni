import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationModule } from '@atonix/atx-navigation';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedUiModule } from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DiagnosticDrilldownComponent } from './components/diagnostic-drilldown/diagnostic-drilldown.component';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { StoreModule } from '@ngrx/store';
import {
  diagnosticDrillDownFeatureKey,
  reducers,
} from './store/reducers/diagnostic-drilldown.reducer';
import { EffectsModule } from '@ngrx/effects';
import { DiagnosticDrilldownEffects } from './store/effects/diagnostic-drilldown.effects';
import { AngularSplitModule } from 'angular-split';
import { ModelTrendComponent } from './components/model-trend/model-trend.component';
import { ModelContextComponent } from './components/model-context/model-context.component';
import { ModelContextEffects } from './store/effects/model-context.effects';
import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { AgGridModule } from '@ag-grid-community/angular';
import { ModelTrendEffects } from './store/effects/model-trend.effects';
import { ModelHistoryEffects } from './store/effects/model-history.effects';
import { ModelHistoryComponent } from './components/model-history/model-history.component';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { AddNewTrendDialogComponent } from './components/add-new-trend-dialog/add-new-trend-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    DiagnosticDrilldownComponent,
    ModelTrendComponent,
    ModelContextComponent,
    ModelHistoryComponent,
    AddNewTrendDialogComponent,
  ],
  imports: [
    CommonModule,
    AngularSplitModule,
    NavigationModule,
    AtxMaterialModule,
    AtxChartModuleV2,
    SharedUiModule,
    SharedApiModule,
    SharedUtilsModule,
    FormsModule,
    TimeSliderModule,
    ReactiveFormsModule,
    AgGridModule,
    AssetTreeModule,
    DragDropModule,
    StoreModule.forFeature(diagnosticDrillDownFeatureKey, reducers),
    EffectsModule.forFeature([
      DiagnosticDrilldownEffects,
      ModelContextEffects,
      ModelTrendEffects,
      ModelHistoryEffects,
    ]),
    RouterModule.forChild([
      {
        path: '',
        component: DiagnosticDrilldownComponent,
        pathMatch: 'full',
      },
    ]),
  ],
  exports: [DiagnosticDrilldownComponent],
})
export class DiagnosticDrilldownModule {}
