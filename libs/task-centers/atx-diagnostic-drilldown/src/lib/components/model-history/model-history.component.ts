import { Component } from '@angular/core';
import { IModelHistoryEventStateChange } from '@atonix/atx-chart-v2';
import { Store } from '@ngrx/store';
import {
  selectModelHistoryIsLoading,
  selectModelTrendModelID,
  selectModelTrendModelName,
  selectTimeSliderAsOfDate,
} from '../../store/reducers/diagnostic-drilldown.reducer';
import * as modelHistoryActions from '../../store/actions/model-history.actions';
import { IModelHistoryFilters } from '@atonix/shared/api';
import { NavFacade } from '@atonix/atx-navigation';

@Component({
  selector: 'atx-model-history',
  templateUrl: './model-history.component.html',
  styleUrls: ['./model-history.component.scss'],
})
export class ModelHistoryComponent {
  readonly modelID$ = this.store.select(selectModelTrendModelID);
  readonly modelName$ = this.store.select(selectModelTrendModelName);
  readonly timeSliderAsOfDate$ = this.store.select(selectTimeSliderAsOfDate);
  readonly modelHistoryIsLoading$ = this.store.select(
    selectModelHistoryIsLoading
  );
  constructor(public navFacade: NavFacade, private readonly store: Store) {}

  onModelHistoryEventChange(eventChange: IModelHistoryEventStateChange) {
    if (eventChange.event === 'setmodelfilters') {
      this.store.dispatch(
        modelHistoryActions.setModelHistoryFilters({
          filters: eventChange.value as IModelHistoryFilters,
        })
      );
    } else if (eventChange.event === 'setfavorite') {
      this.store.dispatch(
        modelHistoryActions.setModelHistoryFavorite({
          modelAction: eventChange.modelAction,
          isFavorite: eventChange.value as boolean,
        })
      );
    } else if (eventChange.event === 'setnote') {
      this.store.dispatch(
        modelHistoryActions.setModelHistoryNote({
          modelAction: eventChange.modelAction,
          note: eventChange.value as string,
        })
      );
    }
  }
}
