import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  ISelectAsset,
  ITreeStateChange,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { INDModelPDTrendMap } from '@atonix/atx-core';
import { AddNewTrendFacade } from '../../services/add-new-trend/add-new-trend.facade';

@Component({
  selector: 'atx-add-new-trend-dialog',
  templateUrl: './add-new-trend-dialog.component.html',
  styleUrls: ['./add-new-trend-dialog.component.scss'],
})
export class AddNewTrendDialogComponent implements OnInit, OnDestroy {
  public availableTrends!: INDModelPDTrendMap[];
  public selectedTrends!: INDModelPDTrendMap[];
  public unsubscribe$ = new Subject<void>();

  readonly vm$ = this.addNewTrendFacade.query.vm$;

  constructor(
    public dialogRef: MatDialogRef<AddNewTrendDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public addNewTrendFacade: AddNewTrendFacade
  ) {
    addNewTrendFacade.query.availableTrends$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((trends) => {
        this.availableTrends = trends;
      });

    addNewTrendFacade.query.existingTrends$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((trends) => {
        this.selectedTrends = this.sortDisplayOrder(trends);
      });

    addNewTrendFacade.query.assetId$
      .pipe(debounceTime(100), takeUntil(this.unsubscribe$))
      .subscribe((assetId) => {
        if (assetId) {
          this.addNewTrendFacade.getAvailableTrends(assetId);
        }
      });

    addNewTrendFacade.query.isSaved$
      .pipe(debounceTime(100), takeUntil(this.unsubscribe$))
      .subscribe((saved) => {
        if (saved) {
          this.dialogRef.close(true);
        }
      });
  }

  ngOnInit(): void {
    this.addNewTrendFacade.command.initializeAddNewTrendState({
      assetDesc: this.data.assetDesc,
      modelId: this.data.modelId,
      modelName: this.data.modelName,
      timeSelection: this.data.timeSelection,
      existingTrends: this.data.existingTrends,
    });

    this.addNewTrendFacade.treeStateChange(
      selectAsset(String(this.data.assetId ?? ''), false)
    );
  }

  // TODO: Make the moving and order of the items on the Drag be saved
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  sortDisplayOrder(array: INDModelPDTrendMap[]) {
    // Checks if all has zero display order and sort it by title by default
    if (array.every((val) => val.DisplayOrder === 0)) {
      return array.sort((a, b) => {
        if (a?.MappedPDTrend?.Title < b?.MappedPDTrend?.Title) {
          return -1;
        } else if (a?.MappedPDTrend?.Title > b?.MappedPDTrend?.Title) {
          return 1;
        } else {
          return 0;
        }
      });
    } else {
      return array.sort((a: INDModelPDTrendMap, b: INDModelPDTrendMap) => {
        if (a.DisplayOrder < b.DisplayOrder) {
          return -1;
        } else if (a.DisplayOrder > b.DisplayOrder) {
          return 1;
        } else {
          return 0;
        }
      });
    }
  }

  onModelContextAssetTreeStateChange(change: ITreeStateChange) {
    this.addNewTrendFacade.treeStateChange(change);
  }

  save() {
    this.selectedTrends = this.selectedTrends.map((trend, index) => {
      const obj = { ...trend };
      obj.DisplayOrder = index;
      return obj;
    });
    this.addNewTrendFacade.saveSelectedTrends(this.selectedTrends);
  }

  ngOnDestroy(): void {
    this.addNewTrendFacade.command.resetAddNewTrendState();
    this.unsubscribe$.next();
  }
}
