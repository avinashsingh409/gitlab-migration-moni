/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
import { Component } from '@angular/core';
import { IBtnGrpStateChange, IUpdateLimitsData } from '@atonix/atx-chart-v2';
import { Store } from '@ngrx/store';
import {
  selectModelTrendData,
  selectModelTrendLoading,
} from '../../store/reducers/diagnostic-drilldown.reducer';
import * as actions from '../../store/actions/model-trend.actions';

@Component({
  selector: 'atx-model-trend',
  templateUrl: './model-trend.component.html',
  styleUrls: ['./model-trend.component.scss'],
})
export class ModelTrendComponent {
  readonly trendData$ = this.store.select(selectModelTrendData);
  readonly loading$ = this.store.select(selectModelTrendLoading);

  constructor(private readonly store: Store) {}

  onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'UpdateLimits') {
      this.store.dispatch(
        actions.updateLimits({
          limitsData: change.newValue as IUpdateLimitsData,
        })
      );
    } else if (change.event === 'ResetLimits') {
      this.store.dispatch(actions.resetLimits());
    }
  }
}
