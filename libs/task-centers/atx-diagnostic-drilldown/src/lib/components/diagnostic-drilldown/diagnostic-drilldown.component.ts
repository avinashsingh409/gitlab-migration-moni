/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  getAssetFromRoute,
  getEndFromRoute,
  getModelFromRoute,
  getStartFromRoute,
  NavFacade,
} from '@atonix/atx-navigation';
import { getAMonthAgoLive } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import * as timeSliderActions from '../../store/actions/time-slider.actions';
import * as diagnosticDrilldownActions from '../../store/actions/diagnostic-drilldown.actions';
import * as modelContextActions from '../../store/actions/model-context.actions';

import {
  selectAllDrilldownStates,
  selectAssetId,
  selectExpandTopSection,
  selectModelTrendDisplayModelPath,
  selectTimeSlider,
  selectTimeSliderSelection,
} from '../../store/reducers/diagnostic-drilldown.reducer';
import { Subject } from 'rxjs';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { take, takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AddNewTrendDialogComponent } from '../add-new-trend-dialog/add-new-trend-dialog.component';
export type ModelTabTypes = 'modelHistory' | 'modelTrend';

@Component({
  selector: 'atx-diagnostic-drilldown',
  templateUrl: './diagnostic-drilldown.component.html',
  styleUrls: ['./diagnostic-drilldown.component.scss'],
})
export class DiagnosticDrilldownComponent implements OnDestroy, AfterViewInit {
  private onDestroy = new Subject<void>();
  readonly expandTopSection$ = this.store.select(selectExpandTopSection);
  readonly timeSlider$ = this.store.select(selectTimeSlider);
  readonly timeSliderSelection$ = this.store.select(selectTimeSliderSelection);
  readonly modelTrendDisplayModelPath$ = this.store.select(
    selectModelTrendDisplayModelPath
  );

  topSizePercent = 50;
  bottomSizePercent = 50;

  public currentTab: ModelTabTypes = 'modelTrend';
  constructor(
    public navFacade: NavFacade,
    private activatedRoute: ActivatedRoute,
    private readonly store: Store,
    public dialog: MatDialog
  ) {
    const assetId = getAssetFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    if (assetId) {
      this.store.dispatch(
        diagnosticDrilldownActions.updateAssetId({
          assetId,
        })
      );
    } else {
      const modelId = getModelFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      this.store.dispatch(
        diagnosticDrilldownActions.getModelAsset({
          modelId: modelId || '',
        })
      );
    }
  }

  ngAfterViewInit(): void {
    this.store
      .select(selectAssetId)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((selectedAssetId) => {
        if (selectedAssetId) {
          const modelId = getModelFromRoute(
            this.activatedRoute.snapshot.queryParamMap
          );
          const startFromRoute = getStartFromRoute(
            this.activatedRoute.snapshot.queryParamMap
          );
          const start = startFromRoute
            ? new Date(parseFloat(startFromRoute))
            : getAMonthAgoLive();
          const endFromRoute = getEndFromRoute(
            this.activatedRoute.snapshot.queryParamMap
          );
          const end = endFromRoute
            ? new Date(parseFloat(endFromRoute))
            : new Date();

          this.store.dispatch(
            diagnosticDrilldownActions.initializeDiagnosticDrilldown({
              assetId: selectedAssetId,
              modelId,
              start,
              end,
            })
          );
          this.store.dispatch(modelContextActions.getModelContextTrends());
        }
      });
  }

  splitDragEnd(e: { gutterNum: number; sizes: Array<number> }) {
    if (e?.sizes) {
      this.topSizePercent = e.sizes[0];
      this.bottomSizePercent = e.sizes[1];
      this.store.dispatch(diagnosticDrilldownActions.splitDrag());
    }
  }

  onTimeSliderStateChange(change: ITimeSliderStateChange) {
    if (change) {
      this.store.dispatch(
        timeSliderActions.timeSliderStateChange({ stateChange: change })
      );
    }
    if (change.event === 'SelectDateRange') {
      if (change?.newStartDateValue && change?.newEndDateValue) {
        this.store.dispatch(
          timeSliderActions.updateTimeSlider({
            start: change.newStartDateValue,
            end: change.newEndDateValue,
          })
        );
      }
    }
  }

  expandTopSection() {
    this.store.dispatch(diagnosticDrilldownActions.expandTopSection());
  }

  setTab(newTab: ModelTabTypes) {
    this.currentTab = newTab;
  }

  copyChartLink(type: string) {
    let link = window.location.href;
    if (type === 'live') {
      const assetId = getAssetFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      const modelId = getModelFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      const diagnosticDrilldownUrl = `?model=${modelId}&asset=${assetId}`;
      link = `${window.location.origin}${window.location.pathname}${diagnosticDrilldownUrl}`;
    }
    this.store.dispatch(diagnosticDrilldownActions.copyLink({ link }));
  }

  addNewTrend(): void {
    let dialogRef: any;

    this.store
      .select(selectAllDrilldownStates)
      .pipe(take(1))
      .subscribe((vm) => {
        if (vm) {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.disableClose = true;
          dialogConfig.panelClass = 'custom-dialog';
          dialogConfig.data = {
            assetId: getAssetFromRoute(
              this.activatedRoute.snapshot.queryParamMap
            ),
            assetDesc: vm.AssetDesc,
            modelId: getModelFromRoute(
              this.activatedRoute.snapshot.queryParamMap
            ),
            modelName: vm.ModelName,
            timeSelection: vm.TimeSelection,
            existingTrends: vm.ExistingTrends,
          };
          dialogConfig.autoFocus = false;

          dialogRef = this.dialog.open(
            AddNewTrendDialogComponent,
            dialogConfig
          );
        }
      });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.store.dispatch(modelContextActions.getModelContextTrends());
      }
    });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
