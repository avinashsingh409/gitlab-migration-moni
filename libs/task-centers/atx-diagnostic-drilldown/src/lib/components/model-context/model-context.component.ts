/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectModelContextCorrelationTrends,
  selectModelContextDisplayedTrendIndex,
  selectModelContextLoadingTrends,
  selectModelContextTrends,
} from '../../store/reducers/diagnostic-drilldown.reducer';
import * as modelContextActions from '../../store/actions/model-context.actions';
import { NavFacade } from '@atonix/atx-navigation';
import { IBtnGrpStateChange, IUpdateLimitsData } from '@atonix/atx-chart-v2';
import { IProcessedTrend } from '@atonix/atx-core';
// import { SummaryFacade } from '@atonix/atx-alerts';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
// import {selectModelContextCorrelationTrend} from '../../../../../atx-alerts/src/lib/store/selectors/summary.selector';
import { Observable } from 'rxjs';
import { ICorrelationData } from '@atonix/shared/api';

@Component({
  selector: 'atx-model-context',
  templateUrl: './model-context.component.html',
  styleUrls: ['./model-context.component.scss'],
})
export class ModelContextComponent {
  readonly trends$ = this.store.select(selectModelContextTrends);
  readonly loadingTrends$ = this.store.select(selectModelContextLoadingTrends);

  readonly displayedTrendIndex$ = this.store.select(
    selectModelContextDisplayedTrendIndex
  );

  public modelContextCorrelationTrends$: Observable<ICorrelationData[]> =
    this.store.select(selectModelContextCorrelationTrends);

  constructor(private readonly store: Store, public navFacade: NavFacade) {}

  trackByTrendId(index: number, trend: IProcessedTrend): string {
    return trend.id;
  }

  expandTrends(index: number) {
    this.store.dispatch(modelContextActions.expandTrends({ index }));
  }

  onBtnGrpStateChange(change: IBtnGrpStateChange, trendId: string) {
    if (change.event === 'ChangeLabels') {
      this.store.dispatch(
        modelContextActions.changeLabels({
          index: change.newValue as number,
          trendId,
        })
      );
    } else if (change.event === 'DataCursorToggle') {
      this.store.dispatch(modelContextActions.showHideDataCursor({ trendId }));
    } else if (change.event === 'EditChart') {
      this.store.dispatch(modelContextActions.editChart({ trendId }));
    } else if (change.event === 'LegendItemToggle') {
      this.store.dispatch(
        modelContextActions.legendItemToggle({
          seriesIndex: change.newValue as number,
          trendId,
        })
      );
    } else if (change.event === 'UpdateLimits') {
      this.store.dispatch(
        modelContextActions.updateLimits({
          limitsData: change.newValue as IUpdateLimitsData,
          trendId,
        })
      );
    } else if (change.event === 'ResetLimits') {
      this.store.dispatch(
        modelContextActions.resetLimits({
          axisIndex: change.newValue as number,
          trendId,
        })
      );
    }
  }
}
