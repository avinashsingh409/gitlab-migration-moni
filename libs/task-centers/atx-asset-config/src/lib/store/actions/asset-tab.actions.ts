/* eslint-disable ngrx/prefer-inline-action-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  IAssetClassTypeResult,
  IAssetCreateUpdateResult,
  IAssetResult,
  IAssetUpdate,
  UpdateAssetType,
} from '@atonix/atx-core';
import { createAction, props } from '@ngrx/store';

export const getAssets = createAction('[Asset Tab] Get Assets');
export const getAssetsSuccess = createAction(
  '[Asset Tab] Get Assets Success',
  props<{ assets: IAssetResult[] }>()
);
export const getAssetsFailure = createAction(
  '[Asset Tab] Get Assets Failure',
  props<Error>()
);
export const reParentAsset = createAction(
  '[Asset Tab] Re-parent Asset',
  props<{ newParent: IAssetResult; draggedAsset: IAssetResult }>()
);
export const reParentAssetSuccess = createAction(
  '[Asset Tab] Re-parent Asset Success',
  props<{
    newParent: IAssetResult;
    draggedAsset: IAssetResult;
  }>()
);
export const reParentAssetFailure = createAction(
  '[Asset Tab] Re-parent Asset Failure',
  props<Error>()
);
export const reOrderAsset = createAction(
  '[Asset Tab] Re-order Asset',
  props<{ targetAsset: IAssetResult; draggedAsset: IAssetResult }>()
);
export const reOrderAssetSuccess = createAction(
  '[Asset Tab] Re-order Asset Success',
  props<{
    targetAsset: IAssetResult;
    draggedAsset: IAssetResult;
  }>()
);
export const reOrderAssetFailure = createAction(
  '[Asset Tab] Re-order Asset Failure',
  props<Error>()
);
export const setFilters = createAction(
  '[Asset Tab] Set Grid Filters',
  props<{ filters: any[] }>()
);
export const clearFilters = createAction('[Asset Tab] Clear Grid Filters');
export const removeFilter = createAction(
  '[Asset Tab] Remove Grid Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);
export const updateAssetToCreateOrEdit = createAction(
  '[Asset Tab] Update Asset to Create or Edit',
  props<{ updatedAssetToCreateOrEdit: IAssetResult | null }>()
);
export const getAssetClassTypes = createAction(
  '[Asset Tab] Get Asset Class Types',
  props<{ parentAssetClassTypeKey: string; searchString?: string }>()
);
export const getAssetClassTypesSuccess = createAction(
  '[Asset Tab] Get Asset Class Types Success',
  props<{ assetClassTypes: IAssetClassTypeResult[] }>()
);
export const getAssetClassTypesFailure = createAction(
  '[Asset Tab] Get Asset Class Types Failure',
  props<Error>()
);
export const updateAssetClassType = createAction(
  "[Asset Tab] Update Asset's Class Type Key",
  props<{ key: string; desc: string }>()
);
export const deleteAsset = createAction(
  '[Asset Tab] Delete Asset',
  props<{ assetIdToDelete: string; selectedAssetPath: string }>()
);
export const deleteAssetSuccess = createAction(
  '[Asset Tab] Delete Asset Success',
  props<{ assetIdToDelete: string; selectedAssetPath: string }>()
);
export const deleteAssetFailure = createAction(
  '[Asset Tab] Delete Asset Failure',
  props<Error>()
);
export const updateAssets = createAction(
  '[Asset Tab] Update Assets',
  props<{ assets: IAssetUpdate[]; updateAssetType: UpdateAssetType }>()
);
export const updateAssetsSuccess = createAction(
  '[Asset Tab] Update Assets Success',
  props<{ assets: IAssetCreateUpdateResult[] }>()
);
export const updateAssetsFailure = createAction(
  '[Asset Tab] Update Assets Failure',
  props<Error>()
);
export const insertSingleAsset = createAction(
  '[Asset Tab] Insert Single Asset',
  props<{ assetToInsert: IAssetResult }>()
);
export const updateSingleAsset = createAction(
  '[Asset Tab] Update Single Asset',
  props<{ updatedAsset: IAssetResult }>()
);
export const insertAssets = createAction(
  '[Asset Tab] Insert Assets',
  props<{ assets: IAssetResult[] }>()
);
