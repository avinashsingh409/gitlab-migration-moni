/* eslint-disable ngrx/prefer-inline-action-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  IAttribute,
  IAttributeUpsertResult,
} from '../../models/attribute-type';
import { createAction, props } from '@ngrx/store';

export const getAttributes = createAction('[Attributes Tab] Get Attributes');
export const getAttributesSuccess = createAction(
  '[Attributes Tab] Get Attributes Succcess',
  props<{ attributes: IAttribute[] }>()
);
export const getAttributesFailure = createAction(
  '[Attributes Tab] Get Attributes Failure',
  props<Error>()
);
export const setFilters = createAction(
  '[Attributes Tab] Set Grid Filters',
  props<{ filters: any[] }>()
);
export const clearFilters = createAction('[Attributes Tab] Clear Grid Filters');
export const removeFilter = createAction(
  '[Attributes Tab] Remove Grid Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);
export const updateAttributes = createAction(
  '[Attributes Tab] Update Attributes',
  props<{ attributesToUpdate: IAttribute[] }>()
);
export const updateAttributesSuccess = createAction(
  '[Attributes Tab] Update Attributes Success',
  props<{ updateResults: IAttributeUpsertResult[] }>()
);
export const updateAttributesFailure = createAction(
  '[Attributes Tab] Update Attributes Failure',
  props<Error>()
);
export const deleteAttributes = createAction(
  '[Attributes Tab] Delete Attributes',
  props<{ attributesToDelete: IAttribute[] }>()
);
export const deleteAttributesSuccess = createAction(
  '[Attributes Tab] Delete Attributes Success',
  props<{ deletedAttributes: number[] }>()
);
export const deleteAttributesFailure = createAction(
  '[Attributes Tab] Delete Attributes Failure',
  props<Error>()
);
