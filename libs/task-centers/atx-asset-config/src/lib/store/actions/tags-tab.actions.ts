/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import {
  IServerResult,
  IPDTagMap,
  ITagMapAuditResult,
  ITagResult,
} from '@atonix/atx-core';

// Tags Tab
export const routeLoadTags = createAction(
  '[Asset Config - Tags Tab] Route Load Tags',
  props<{ asset: string }>()
);
export const routeLoadTagsSuccess = createAction(
  '[Asset Config - Tags Tab] Route Load Tags Success',
  props<{ asset: string; servers: IServerResult[]; init: boolean }>()
);
export const routeLoadTagsFailure = createAction(
  '[Asset Config - Tags Tab] Route Load Tags Failure',
  props<{ asset: string }>()
);

// Tag Configuration Section
export const getTags = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Get Tags',
  props<{ servers?: IServerResult[]; server: IServerResult }>()
);
export const getTagsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Get Tags Success',
  props<{ tags: ITagResult[]; server: IServerResult }>()
);
export const getTagsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Get Tags Failure',
  props<Error>()
);
export const addTag = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Add Tag'
);
export const saveTags = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Save Tags'
);
export const saveTagsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Save Tags Success',
  props<{ result: ITagResult[]; server: IServerResult | null }>()
);
export const saveTagsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Save Tags Failure',
  props<{ error: Error; successful?: ITagResult[] | null }>()
);
export const discardChanges = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Discard Changes'
);
export const discardChangesSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Discard Changes Success',
  props<{ tags: ITagResult[]; server: IServerResult | null }>()
);
export const discardChangesFailure = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Discard Changes Failure',
  props<Error>()
);
export const changeTag = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Change Tag',
  props<{ tag: ITagResult }>()
);
export const showDeletedTagsChange = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Show Deleted Tags Change',
  props<{ newValue: boolean }>()
);
export const deleteTag = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Delete Tag',
  props<{ tagId: string }>()
);
export const pasteTags = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Paste Tags',
  props<{ tags: ITagResult[] }>()
);
export const refreshTags = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Refresh Tags'
);
export const associateTagsToTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Associate Tags to Tag Maps',
  props<{ selectedTagMap: IPDTagMap; tags: ITagResult[] }>()
);
export const associateTagsToTagMapsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Associate Tags to Tag Maps Success',
  props<{
    savedTagMaps: IPDTagMap[];
    selectedTagMap: IPDTagMap;
  }>()
);
export const associateTagsToTagMapsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Associate Tags to Tag Maps Failure',
  props<Error>()
);
export const updateValueTypeOfTagMap = createAction(
  "[Asset Config - Tags Tab - Tag Maps] Update a Tag Map's Value Type",
  props<{ selectedTagMap: IPDTagMap; newValueType: string }>()
);
export const updateValueTypeOfTagMapSuccess = createAction(
  "[Asset Config - Tags Tab - Tag Maps] Update a Tag Map's Value Type Success",
  props<{ selectedTagMap: IPDTagMap; newValueType: string }>()
);
export const updateValueTypeOfTagMapFailure = createAction(
  "[Asset Config - Tags Tab - Tag Maps] Update a Tag Map's Value Type Failure",
  props<Error>()
);
export const updateTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Update Tag Maps',
  props<{ selectedTagMap: IPDTagMap; draggedTagMaps: IPDTagMap[] }>()
);
export const updateTagMapsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Update Tag Maps Success',
  props<{
    filteredTagMaps: IPDTagMap[];
    updatedTagMaps: IPDTagMap[];
    selectedTagMap: IPDTagMap;
  }>()
);
export const updateTagMapsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Update Tag Maps Failure',
  props<Error>()
);
export const getTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Get Tag Maps',
  props<{
    server: IServerResult | null;
    assetId: string;
    includeDescendants: boolean;
  }>()
);
export const refreshTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Refresh Tag Maps'
);
export const getTagMapsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Get Tag Maps Success',
  props<{ tagMaps: IPDTagMap[] }>()
);
export const getTagMapsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Get Tag Maps Failure',
  props<Error>()
);
export const setTagMapFilters = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Set Tag Map Filters',
  props<{ filters: any[] }>()
);
export const clearTagMapFilters = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Clear Tag Map Filters'
);
export const removeTagMapFilter = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Remove Tag Map Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);
export const setTagFilters = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Set Tag Filters',
  props<{ filters: any[] }>()
);
export const clearTagFilters = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Clear Tag Filters'
);
export const removeTagFilter = createAction(
  '[Asset Config - Tags Tab - Tag Configuration] Remove Tag Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);
export const auditTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Audit Tag Maps',
  props<{
    tagMapIds: number[];
    callback: (tagMapIds: number[], text: string) => void;
  }>()
);
export const auditTagMapsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Audit Tag Maps Success',
  props<{
    auditResult: ITagMapAuditResult[];
    callback: (tagMapIds: number[], text: string) => void;
  }>()
);
export const auditTagMapsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Audit Tag Maps Failure',
  props<Error>()
);
export const deleteTagMaps = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Delete Tag Maps',
  props<{ tagMapIds: number[] }>()
);
export const deleteTagMapsSuccess = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Delete Tag Maps Success',
  props<{ tagMapIds: number[] }>()
);
export const deleteTagMapsFailure = createAction(
  '[Asset Config - Tags Tab - Tag Maps] Delete Tag Maps Failure',
  props<Error>()
);
export const init = createAction('[Asset Config] Initialize');
