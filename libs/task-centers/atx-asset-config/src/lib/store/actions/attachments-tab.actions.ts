/* eslint-disable ngrx/prefer-inline-action-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { IAttachment } from '../../models/attachment-type';
import { createAction, props } from '@ngrx/store';
import { AttachmentAction } from '../reducers/attachments-tab.reducer';

export const getAttachments = createAction('[Attachments Tab] Get Attachments');
export const getAttachmentsSuccess = createAction(
  '[Attachments Tab] Get Attachments Success',
  props<{ attachments: IAttachment[] }>()
);
export const getAttachmentsFailure = createAction(
  '[Attachments Tab] Get Attachments Failure',
  props<Error>()
);
export const setFilters = createAction(
  '[Attachments Tab] Set Grid Filters',
  props<{ filters: any[] }>()
);
export const clearFilters = createAction(
  '[Attachments Tab] Clear Grid Filters'
);
export const removeFilter = createAction(
  '[Attachments Tab] Remove Grid Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);
export const upsertAttachment = createAction(
  '[Attachments Tab] Upsert Attachment',
  props<{
    attachmentToUpsert: IAttachment;
    file: File | undefined;
    action: AttachmentAction;
  }>()
);
export const upsertAttachmentSuccess = createAction(
  '[Attachments Tab] Upsert Attachment Success',
  props<{ upsertResult: IAttachment; action: AttachmentAction }>()
);
export const upsertAttachmentFailure = createAction(
  '[Attachments Tab] Upsert Attachment Failure',
  props<Error>()
);
export const uploadAttachment = createAction(
  '[Attachments Tab] Upload Attachment',
  props<{
    createdAttachment: IAttachment;
    file: File | undefined;
    action: AttachmentAction;
  }>()
);
export const deleteAttachments = createAction(
  '[Attachments Tab] Delete Attachments',
  props<{ attachmentsToDelete: IAttachment[] }>()
);
export const deleteAttachmentsSuccess = createAction(
  '[Attachments Tab] Delete Attachments Success',
  props<{ deletedAttachments: string[] }>()
);
export const deleteAttachmentsFailure = createAction(
  '[Attachments Tab] Delete Attachments Failure',
  props<Error>()
);
