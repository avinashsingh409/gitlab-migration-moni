/* eslint-disable ngrx/prefer-inline-action-props */
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITreePermissions } from '@atonix/atx-core';
import { AuthState } from '@atonix/shared/state/auth';
import { createAction, props } from '@ngrx/store';

export const systemInitialize = createAction(
  '[Asset Config] Initialize',
  props<{ asset?: string }>()
);
export const updateRoute = createAction(
  '[Asset Config] Update Route',
  props<{ asset: string; tab: string }>()
);
export const selectAsset = createAction(
  '[Asset Config] Select Asset',
  props<{ asset: string }>()
);
export const deselectAsset = createAction('[Asset Config] Deselect Asset');

export const treeStateChange = createAction(
  '[Asset Config] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[Asset Config] Tree Data Request Failure',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[Asset Config] Asset Tree Size Change',
  props<{ value: number }>()
);
export const permissionsRequest = createAction(
  '[Asset Config] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[Asset Config] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[Asset Config] Permissions Request Failure',
  props<Error>()
);
export const showInfoMessage = createAction(
  '[Asset Config] Show Info Message',
  props<{ message: string }>()
);
export const showSuccessMessage = createAction(
  '[Asset Config] Show Success Message',
  props<{ message: string }>()
);
export const showWarningMessage = createAction(
  '[Asset Config] Show Warning Message',
  props<{ message: string }>()
);
export const showErrorMessage = createAction(
  '[Asset Config] Show Error Message',
  props<{ message: string }>()
);

// Asset Tree Dropdown
export const assetTreeDropdownStateChange = createAction(
  '[Asset Config - Asset Tree Dropdown] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDropdownDataRequestFailure = createAction(
  '[Asset Config - Asset Tree Dropdown] Tree Data Request Failure',
  props<Error>()
);

// Tabs
export const fillTabs = createAction(
  '[Asset Config] Fill Tabs',
  props<AuthState>()
);
export const toggleSelectedAssetOnly = createAction(
  '[Asset Config] Toggle Selected Asset Only',
  props<{ value: boolean | null }>()
);
