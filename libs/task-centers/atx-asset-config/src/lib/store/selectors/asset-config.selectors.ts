/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  ITreeConfiguration,
  ITreeNode,
  TrayState,
} from '@atonix/atx-asset-tree';
import { createSelector } from '@ngrx/store';
import * as assetTreeSelectors from './asset-nav.selectors';
import { selectAssetTreeSelected } from '@atonix/atx-navigation';
import { selectTabs, selectCurrentTab } from './asset-nav.selectors';
import { ITab } from '../reducers/asset-nav.reducer';
import { selectTagServers, selectSelectedServer } from './tags-tab.selectors';
import { IServerResult } from '@atonix/atx-core';
import { attachmentsTabFeatureKey } from '../reducers/attachments-tab.reducer';

export interface AssetConfigViewModel {
  assetTreeConfiguration: ITreeConfiguration;
  assetName: string;
  leftTrayMode: TrayState;
  leftTraySize: number;
  assetTreeSelected: boolean;
  tabs: ITab[];
  selectedAssetOnly: boolean | null;
  isToggleDisabled: boolean;
  currentTab: string;
  selectedServer: IServerResult | null;
  tagServers: IServerResult[];
}

export const selectSelectedAsset = createSelector(
  assetTreeSelectors.selectSelectedAsset,
  (selectedAsset) => selectedAsset
);

export const selectSelectedTreeNode = createSelector(
  selectSelectedAsset,
  assetTreeSelectors.selectAssetTreeConfiguration,
  (selectedAsset, assetTreeConfiguration): ITreeNode | null =>
    assetTreeConfiguration?.nodes.find(
      (n: ITreeNode) => n.uniqueKey === selectedAsset
    ) ?? null
);

export const selectAssetGuid = createSelector(
  selectSelectedTreeNode,
  (selectedTreeNode): string => selectedTreeNode?.data?.AssetGuid ?? ''
);

export const selectSelectedAssetObject = createSelector(
  selectSelectedTreeNode,
  (selectedTreeNode): any => selectedTreeNode?.data ?? null
);

export const selectAssetConfigViewModel = createSelector(
  [
    assetTreeSelectors.selectAssetTreeConfiguration,
    selectSelectedAssetObject,
    assetTreeSelectors.selectLeftTrayMode,
    assetTreeSelectors.selectLeftTraySize,
    selectAssetTreeSelected,
    selectTabs,
    assetTreeSelectors.selectSelectedAssetOnly,
    selectCurrentTab,
    selectSelectedServer,
    selectTagServers,
  ],
  (
    assetTreeConfiguration,
    selectedAssetObject,
    leftTrayMode,
    leftTraySize,
    assetTreeSelected,
    tabs,
    selectedAssetOnly,
    currentTab,
    selectedServer,
    tagServers
  ): AssetConfigViewModel => ({
    assetTreeConfiguration,
    assetName: selectedAssetObject?.NodeAbbrev ?? '',
    leftTrayMode,
    leftTraySize,
    assetTreeSelected,
    tabs,
    selectedAssetOnly,
    isToggleDisabled:
      [1, 2].includes(selectedAssetObject?.Asset.AssetTypeID) ||
      currentTab === attachmentsTabFeatureKey,
    currentTab,
    selectedServer,
    tagServers,
  })
);
