/* eslint-disable @typescript-eslint/no-explicit-any */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectTheme } from '@atonix/atx-navigation';
import {
  assetConfigFeatureKey,
  AssetConfigState,
} from '../reducers/asset-config.reducers';

const selectAssetConfigState = createFeatureSelector<AssetConfigState>(
  assetConfigFeatureKey
);

const selectAttachmentsTabState = createSelector(
  selectAssetConfigState,
  (state) => state?.attachmentsTab
);

export const selectAttachmentsState = createSelector(
  selectAttachmentsTabState,
  (state) => state.AttachmentDataAvailable
);

export const selectAttachments = createSelector(
  selectAttachmentsTabState,
  (state) => state.Attachments
);

export const selectAttachmentsFilters = createSelector(
  selectAttachmentsTabState,
  (state) => state.AttachmentFilters
);

export const selectTotalAttachments = createSelector(
  selectAttachmentsTabState,
  (state) => state.Attachments.length
);

export interface AttachmentsViewModel {
  theme: string;
  attachmentFilters: any[];
  totalAttachments: number;
}

export const selectAttachmentsViewModel = createSelector(
  selectTheme,
  selectAttachmentsFilters,
  selectTotalAttachments,
  (theme, attachmentFilters, totalAttachments): AttachmentsViewModel => ({
    theme,
    attachmentFilters,
    totalAttachments,
  })
);
