/* eslint-disable @typescript-eslint/no-explicit-any */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectTheme } from '@atonix/atx-navigation';
import {
  assetConfigFeatureKey,
  AssetConfigState,
} from '../reducers/asset-config.reducers';

const selectAssetConfigState = createFeatureSelector<AssetConfigState>(
  assetConfigFeatureKey
);

const selectAttributesTabState = createSelector(
  selectAssetConfigState,
  (state) => state?.attributesTab
);

export const selectAttributesState = createSelector(
  selectAttributesTabState,
  (state) => state.AttributeDataAvailable
);

export const selectAttributes = createSelector(
  selectAttributesTabState,
  (state) => state.Attributes
);

export const selectAttributesFilters = createSelector(
  selectAttributesTabState,
  (state) => state.AttributeFilters
);

export const selectTotalAttributes = createSelector(
  selectAttributesTabState,
  (state) => state.Attributes.length
);

export interface AttributesViewModel {
  theme: string;
  attributeFilters: any[];
  totalAttributes: number;
}

export const selectAttributesViewModel = createSelector(
  selectTheme,
  selectTotalAttributes,
  selectAttributesFilters,
  (theme, totalAttributes, attributeFilters): AttributesViewModel => ({
    theme,
    totalAttributes,
    attributeFilters,
  })
);
