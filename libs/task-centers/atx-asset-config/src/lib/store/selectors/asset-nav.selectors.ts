import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  assetConfigFeatureKey,
  AssetConfigState,
} from '../reducers/asset-config.reducers';
import * as assetNavReducer from '../reducers/asset-nav.reducer';

const selectAssetConfigState = createFeatureSelector<AssetConfigState>(
  assetConfigFeatureKey
);

export const selectAssetNavState = createSelector(
  selectAssetConfigState,
  (state) => state.assetNav
);

//Asset Nav Selectors
export const selectLeftTraySize = createSelector(
  selectAssetNavState,
  assetNavReducer.leftTraySize
);

export const selectAssetTreeState = createSelector(
  selectAssetNavState,
  assetNavReducer.treeState
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetNavState,
  assetNavReducer.treeConfiguration
);

export const selectTabs = createSelector(
  selectAssetNavState,
  assetNavReducer.tabs
);

export const selectLeftTrayMode = createSelector(
  selectAssetTreeConfiguration,
  (state) => {
    const result: TrayState = state?.pin ? 'side' : 'over';
    return result;
  }
);

export const selectSelectedAsset = createSelector(
  selectAssetTreeState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
);

export const selectAssetTreeNodes = createSelector(
  selectAssetTreeState,
  (state) => state.treeNodes
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  selectSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const selectSelectedAssetTreeNodeParent = createSelector(
  selectAssetTreeState,
  selectSelectedAssetTreeNode,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find(
      (a) => a?.uniqueKey === asset?.parentUniqueKey
    )?.data
);

export const selectAssetTreeDropdownState = createSelector(
  selectAssetNavState,
  assetNavReducer.assetTreeDropdownState
);

export const selectAssetTreeDropdownConfiguration = createSelector(
  selectAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const selectAssetTreeDropdownNode = createSelector(
  selectAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectAssetTreeDropdownName = createSelector(
  selectAssetTreeDropdownNode,
  (state) => state?.nodeAbbrev
);

export const selectAssetTreeDropdownGlobalId = createSelector(
  selectAssetTreeDropdownNode,
  (state) => state?.data?.AssetGuid
);

export const selectSelectedAssetOnly = createSelector(
  selectAssetNavState,
  (state) => state.SelectedAssetOnly
);

export const selectCurrentTab = createSelector(
  selectAssetNavState,
  (state) => state.CurrentTab
);
