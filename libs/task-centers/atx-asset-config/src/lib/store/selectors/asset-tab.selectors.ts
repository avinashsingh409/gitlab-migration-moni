/* eslint-disable @typescript-eslint/no-explicit-any */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectTheme } from '@atonix/atx-navigation';
import {
  assetConfigFeatureKey,
  AssetConfigState,
} from '../reducers/asset-config.reducers';

const selectAssetConfigState = createFeatureSelector<AssetConfigState>(
  assetConfigFeatureKey
);

const selectAssetTabState = createSelector(
  selectAssetConfigState,
  (state) => state?.assetTab
);

export const selectAssetFilters = createSelector(
  selectAssetTabState,
  (state) => state.AssetFilters
);

export const selectTotalAssets = createSelector(
  selectAssetTabState,
  (state) => state.Assets.length
);

export const selectAssets = createSelector(
  selectAssetTabState,
  (state) => state.Assets
);

export const selectAssetState = createSelector(
  selectAssetTabState,
  (state) => state.AssetDataAvailable
);

export const selectAssetClassTypeState = createSelector(
  selectAssetTabState,
  (state) => state.AssetClassTypeDataAvailable
);

export const selectAssetToCreateOrEdit = createSelector(
  selectAssetTabState,
  (state) => state.AssetToCreateOrEdit
);

export const selectAssetClassTypes = createSelector(
  selectAssetTabState,
  (state) => state.AssetClassTypes
);

export interface AssetViewModel {
  theme: string;
  assetFilters: any[];
  totalAssets: number;
}

export const selectAssetViewModel = createSelector(
  selectTheme,
  selectTotalAssets,
  selectAssetFilters,
  (theme, totalAssets, assetFilters): AssetViewModel => ({
    theme,
    totalAssets,
    assetFilters,
  })
);
