/* eslint-disable @typescript-eslint/no-explicit-any */
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IPDTagMap, IServerResult } from '@atonix/atx-core';
import { selectTheme } from '@atonix/atx-navigation';
import {
  AssetConfigState,
  assetConfigFeatureKey,
} from '../reducers/asset-config.reducers';
import { groupBy } from 'lodash';
import {
  TagsDataAvailable,
  TagMapsDataAvailable,
} from '../reducers/tags-tab.reducer';

const selectAssetConfigState = createFeatureSelector<AssetConfigState>(
  assetConfigFeatureKey
);

const selectTagsTabState = createSelector(
  selectAssetConfigState,
  (state) => state?.tagsTab
);

export const selectTagServers = createSelector(
  selectTagsTabState,
  (state) => state.Servers
);

export const selectTags = createSelector(
  selectTagsTabState,
  (state) => state.DisplayedTags
);

export const selectAllTags = createSelector(
  selectTagsTabState,
  (state) => state.Tags
);

export const selectAllTagMaps = createSelector(
  selectTagsTabState,
  (state) => state.TagMaps
);

export const selectTagMapsDict = createSelector(selectTagsTabState, (state) =>
  groupBy(
    state.TagMaps,
    (tagMap: IPDTagMap) => `${tagMap.AssetPath}_${tagMap.VariableDesc}`
  )
);

export const selectSelectedServer = createSelector(
  selectTagsTabState,
  (state) => state.SelectedServer
);

export const selectIsServerSelected = createSelector(
  selectSelectedServer,
  (state) => state != null
);

export const selectTagsState = createSelector(
  selectTagsTabState,
  (state) => state.TagsDataAvailable
);

export const selectTagMapsState = createSelector(
  selectTagsTabState,
  (state) => state.TagMapsDataAvailable
);

export const selectShowDeletedTags = createSelector(
  selectTagsTabState,
  (state) => state.ShowDeletedTags
);

export const selectValidationStatus = createSelector(
  selectTagsTabState,
  (state) => state.ValidationMessage
);

export const selectIsDirty = createSelector(
  selectTagsTabState,
  (state) => state.IsDirty
);

export const selectTagFilters = createSelector(
  selectTagsTabState,
  (state) => state.TagFilters
);

export const selectTotalTags = createSelector(
  selectTagsTabState,
  (state) => state.Tags.length
);

export interface TagConfigViewModel {
  selectedServer: IServerResult | null;
  tagServers: IServerResult[];
  isServerSelected: boolean;
  showDeletedTags: boolean;
  theme: string;
  tagsDataAvailable: TagsDataAvailable;
  isDirty: boolean;
  tagFilters: any[];
  totalTags: number;
}

export const selectTagConfigViewModel = createSelector(
  selectSelectedServer,
  selectTagServers,
  selectIsServerSelected,
  selectShowDeletedTags,
  selectTheme,
  selectTagsState,
  selectIsDirty,
  selectTagFilters,
  selectTotalTags,
  (
    selectedServer,
    tagServers,
    isServerSelected,
    showDeletedTags,
    theme,
    tagsDataAvailable,
    isDirty,
    tagFilters,
    totalTags
  ): TagConfigViewModel => ({
    selectedServer,
    tagServers,
    isServerSelected,
    showDeletedTags,
    theme,
    tagsDataAvailable,
    isDirty,
    tagFilters,
    totalTags,
  })
);

export const selectTagMapFilters = createSelector(
  selectTagsTabState,
  (state) => state.TagMapFilters
);

export const selectTotalTagMaps = createSelector(
  selectTagsTabState,
  (state) => state.TagMaps.length
);

export interface TagMapViewModel {
  isServerSelected: boolean;
  tagMapFilters: any[];
  theme: string;
  totalTagMaps: number;
  tagMapsDataAvailable: TagMapsDataAvailable;
}

export const selectTagMapViewModel = createSelector(
  selectIsServerSelected,
  selectTheme,
  selectTagMapFilters,
  selectTotalTagMaps,
  selectTagMapsState,
  (
    isServerSelected,
    theme,
    tagMapFilters,
    totalTagMaps,
    tagMapsDataAvailable
  ): TagMapViewModel => ({
    isServerSelected,
    theme,
    tagMapFilters,
    totalTagMaps,
    tagMapsDataAvailable,
  })
);
