/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { IAssetResult } from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  skip,
  takeUntil,
} from 'rxjs/operators';
import * as actions from '../actions/asset-tab.actions';
import { selectAssetToCreateOrEdit } from '../selectors/asset-tab.selectors';

@Injectable({
  providedIn: 'root',
})
export class AssetFormService implements OnDestroy {
  private selectedAssetToCreate$: Observable<IAssetResult | null> =
    this.store.select(selectAssetToCreateOrEdit);
  private assetToCreate: IAssetResult | null = null;
  private onDestroy$ = new Subject<void>();

  constructor(private store: Store) {
    this.selectedAssetToCreate$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetToCreate: IAssetResult | null) => {
        this.assetToCreate = assetToCreate;
      });
  }

  assetName(name?: string): UntypedFormControl {
    const assetName = new UntypedFormControl(name);
    assetName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        if (this.assetToCreate) {
          const updatedAssetToCreateOrEdit: IAssetResult = {
            ...this.assetToCreate,
            AssetName: value,
          };
          this.store.dispatch(
            actions.updateAssetToCreateOrEdit({
              updatedAssetToCreateOrEdit,
            })
          );
        }
      });

    return assetName;
  }

  description(desc?: string): UntypedFormControl {
    const description = new UntypedFormControl(desc);
    description.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        if (this.assetToCreate) {
          const updatedAssetToCreateOrEdit: IAssetResult = {
            ...this.assetToCreate,
            AssetDesc: value,
          };
          this.store.dispatch(
            actions.updateAssetToCreateOrEdit({
              updatedAssetToCreateOrEdit,
            })
          );
        }
      });

    return description;
  }

  searchString(parentAssetClassTypeKey: string): UntypedFormControl {
    const active = new UntypedFormControl(null);
    active.valueChanges
      .pipe(
        skip(1),
        debounceTime(1000),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((searchString: string) => {
        if ([null, ''].includes(searchString) || searchString.length > 2) {
          this.store.dispatch(
            actions.getAssetClassTypes({
              parentAssetClassTypeKey,
              searchString,
            })
          );
        }
      });

    return active;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
