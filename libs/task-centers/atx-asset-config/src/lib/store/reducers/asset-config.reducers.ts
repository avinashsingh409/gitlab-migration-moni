import { Action, combineReducers } from '@ngrx/store';
import * as assetNavReducer from './asset-nav.reducer';
import * as tagsTabReducer from './tags-tab.reducer';
import * as assetTabReducer from './asset-tab.reducer';
import * as attributesTabReducer from './attributes-tab.reducer';
import * as attachmentsTabReducer from './attachments-tab.reducer';

export const assetConfigFeatureKey = 'asset-config';

export interface AssetConfigState {
  [assetNavReducer.assetNavFeatureKey]: assetNavReducer.AssetNavState;
  [tagsTabReducer.tagsTabFeatureKey]: tagsTabReducer.TagsTabState;
  [assetTabReducer.assetTabFeatureKey]: assetTabReducer.AssetTabState;
  [attributesTabReducer.attributesTabFeatureKey]: attributesTabReducer.AttributesTabState;
  [attachmentsTabReducer.attachmentsTabFeatureKey]: attachmentsTabReducer.AttachmentsTabState;
}

export function reducers(state: AssetConfigState | undefined, action: Action) {
  return combineReducers({
    [assetNavReducer.assetNavFeatureKey]: assetNavReducer.reducer,
    [tagsTabReducer.tagsTabFeatureKey]: tagsTabReducer.reducer,
    [assetTabReducer.assetTabFeatureKey]: assetTabReducer.reducer,
    [attributesTabReducer.attributesTabFeatureKey]:
      attributesTabReducer.reducer,
    [attachmentsTabReducer.attachmentsTabFeatureKey]:
      attachmentsTabReducer.reducer,
  })(state, action);
}
