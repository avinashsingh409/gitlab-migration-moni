/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  IAttribute,
  IAttributeUpsertResult,
} from '../../models/attribute-type';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/attributes-tab.actions';
import produce from 'immer';
import { WritableDraft } from 'immer/dist/internal';

export const attributesTabFeatureKey = 'attributesTab';

export type AttributeDataAvailable = 'loading' | 'nodata' | '';

export interface AttributesTabState {
  IsDirty: boolean;
  Attributes: IAttribute[];
  AttributeDataAvailable: AttributeDataAvailable;
  AttributeFilters: any[];
}

export const initialAttributesTabState: AttributesTabState = {
  Attributes: [],
  IsDirty: false,
  AttributeDataAvailable: '',
  AttributeFilters: [],
};

export const reducer = createReducer(
  initialAttributesTabState,
  on(actions.getAttributes, (state: AttributesTabState): AttributesTabState => {
    return {
      ...state,
      Attributes: [],
      AttributeDataAvailable: 'loading',
    };
  }),
  on(
    actions.getAttributesSuccess,
    (
      state: AttributesTabState,
      payload: { attributes: IAttribute[] }
    ): AttributesTabState => {
      const dataAvailable: AttributeDataAvailable =
        payload.attributes.length > 0 ? '' : 'nodata';
      return {
        ...state,
        Attributes: payload.attributes,
        AttributeDataAvailable: dataAvailable,
      };
    }
  ),
  on(
    actions.getAttributesFailure,
    (state: AttributesTabState): AttributesTabState => {
      return {
        ...state,
        AttributeDataAvailable: '',
      };
    }
  ),
  on(
    actions.setFilters,
    (
      state: AttributesTabState,
      payload: { filters: any[] }
    ): AttributesTabState => {
      return {
        ...state,
        AttributeFilters: payload.filters,
      };
    }
  ),
  on(actions.clearFilters, (state: AttributesTabState): AttributesTabState => {
    return {
      ...state,
      AttributeFilters: [],
    };
  }),
  on(
    actions.removeFilter,
    (
      state: AttributesTabState,
      payload: { filter: any; callback: (filterName: string) => void }
    ): AttributesTabState => {
      const filters: any[] = [...state.AttributeFilters];
      const index: number = filters.indexOf(payload.filter);
      if (index >= 0) {
        filters.splice(index, 1);
        payload.callback(payload.filter.Name);
      }
      return {
        ...state,
        AttributeFilters: filters,
      };
    }
  ),
  on(
    actions.updateAttributes,
    (state: AttributesTabState): AttributesTabState => {
      return {
        ...state,
        AttributeDataAvailable: 'loading',
      };
    }
  ),
  on(
    actions.updateAttributesSuccess,
    (
      state: AttributesTabState,
      payload: { updateResults: IAttributeUpsertResult[] }
    ): AttributesTabState => {
      const attributes: IAttribute[] = [
        ...produce(
          state.Attributes,
          (draftAttributes: WritableDraft<IAttribute[]>) => {
            payload.updateResults.forEach(
              (updateResult: IAttributeUpsertResult) => {
                const idx: number = draftAttributes.findIndex(
                  (a: IAttribute) => a.Id === updateResult.Id
                );
                if (idx > -1) {
                  draftAttributes[idx] = {
                    ...draftAttributes[idx],
                    Value: updateResult.Value,
                    IsFavorite: updateResult.IsFavorite,
                    DisplayOrder: updateResult.DisplayOrder,
                  };
                }
              }
            );
          }
        ),
      ];

      return {
        ...state,
        Attributes: attributes,
        AttributeDataAvailable: '',
      };
    }
  ),
  on(
    actions.updateAttributesFailure,
    (state: AttributesTabState, payload: Error): AttributesTabState => {
      return {
        ...state,
        AttributeDataAvailable: '',
      };
    }
  ),
  on(
    actions.deleteAttributesSuccess,
    (
      state: AttributesTabState,
      payload: { deletedAttributes: number[] }
    ): AttributesTabState => {
      const attributes: IAttribute[] = [
        ...produce(
          state.Attributes,
          (draftAttributes: WritableDraft<IAttribute[]>) => {
            return draftAttributes.filter((draftAttribute) => {
              if (draftAttribute.Id) {
                return !payload.deletedAttributes.includes(draftAttribute.Id);
              } else {
                return true;
              }
            });
          }
        ),
      ];

      return {
        ...state,
        Attributes: attributes,
        AttributeDataAvailable: '',
      };
    }
  ),
  on(
    actions.deleteAttributes,
    (state: AttributesTabState): AttributesTabState => {
      return {
        ...state,
        AttributeDataAvailable: 'loading',
      };
    }
  ),
  on(
    actions.deleteAttributesFailure,
    (state: AttributesTabState, payload: Error): AttributesTabState => {
      return {
        ...state,
        AttributeDataAvailable: '',
      };
    }
  )
);
