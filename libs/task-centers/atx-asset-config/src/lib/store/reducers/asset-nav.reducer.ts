import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/asset-config.actions';
import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
  ITreeNodes,
  ITreeNode,
} from '@atonix/atx-asset-tree';
import produce from 'immer';

export const assetNavFeatureKey = 'assetNav';

export interface AssetNavState {
  TreeSize: number;
  AssetTreeState: ITreeState;
  AssetTreeDropdownState: ITreeState;
  Tabs: ITab[];
  SelectedAssetOnly: boolean | null;
  CurrentTab: string;
}

export interface ITab {
  path: string;
  label: string;
  isDirty: boolean;
}

export const initialAssetConfigState: AssetNavState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({
      pin: true,
      collapseOthers: false,
      hideConfigureButton: true,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  TreeSize: 250,
  AssetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: true }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  Tabs: [],
  SelectedAssetOnly: null,
  CurrentTab: '',
};

export const reducer = createReducer(
  initialAssetConfigState,
  on(actions.permissionsRequest, (state): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: false,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestSuccess, (state, payload): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: payload.canView,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestFailure, (state): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: false,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.treeStateChange, (state, payload): AssetNavState => {
    const newPayload = treeRetrievedStateChange(payload);
    return {
      ...state,
      AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
    };
  }),
  on(actions.treeSizeChange, (state, payload): AssetNavState => {
    return {
      ...state,
      TreeSize: payload.value,
    };
  }),
  on(actions.assetTreeDropdownStateChange, (state, payload): AssetNavState => {
    const newPayload = treeRetrievedStateChange(payload);
    return {
      ...state,
      AssetTreeDropdownState: alterAssetTreeState(
        state.AssetTreeDropdownState,
        newPayload
      ),
    };
  }),
  on(actions.fillTabs, (state, payload): AssetNavState => {
    let tabs: ITab[] = [];
    if (payload.assetConfigUtilityAccess?.CanView) {
      tabs = tabs.concat([
        {
          path: 'assets',
          label: 'Assets',
          isDirty: false,
        },
      ]);
    }
    if (payload.tagConfigUtilityAccess?.CanView) {
      tabs = tabs.concat([
        {
          path: 'tags',
          label: 'Tags',
          isDirty: false,
        },
      ]);
    }
    if (payload.assetConfigUtilityAccess?.CanView) {
      tabs = tabs.concat([
        {
          path: 'attributes',
          label: 'Attributes',
          isDirty: false,
        },
        {
          path: 'attachments',
          label: 'Attachments',
          isDirty: false,
        },
      ]);
    }
    return { ...state, Tabs: tabs };
  }),
  on(actions.deselectAsset, (state): AssetNavState => {
    const treeNodes: ITreeNodes = produce(
      state.AssetTreeState.treeNodes,
      (draftState: ITreeNodes) => {
        if (draftState && draftState.ids) {
          for (const uniqueKey of draftState.ids as string[]) {
            const v: ITreeNode | undefined = draftState.entities[uniqueKey];
            if (v) {
              v.selected = false;
            }
          }
        }
      }
    );
    return {
      ...state,
      AssetTreeState: {
        ...state.AssetTreeState,
        treeNodes,
      },
    };
  }),
  on(actions.toggleSelectedAssetOnly, (state, payload): AssetNavState => {
    return {
      ...state,
      SelectedAssetOnly: payload.value,
    };
  }),
  on(actions.updateRoute, (state, payload): AssetNavState => {
    return {
      ...state,
      CurrentTab: payload.tab,
    };
  })
);

export const leftTraySize = (state: AssetNavState) => state?.TreeSize;
export const treeState = (state: AssetNavState) => state?.AssetTreeState;
export const treeConfiguration = (state: AssetNavState) =>
  state?.AssetTreeState?.treeConfiguration;
export const assetTreeDropdownState = (state: AssetNavState) =>
  state?.AssetTreeDropdownState || null;
export const tabs = (state: AssetNavState) => state?.Tabs || [];
