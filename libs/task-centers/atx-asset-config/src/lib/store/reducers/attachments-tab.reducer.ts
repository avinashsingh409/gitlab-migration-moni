/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { IAttachment } from '../../models/attachment-type';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/attachments-tab.actions';
import produce from 'immer';
import { WritableDraft } from 'immer/dist/internal';

export const attachmentsTabFeatureKey = 'attachmentsTab';

export type AttachmentDataAvailable = 'loading' | 'nodata' | '';
export type AttachmentAction =
  | 'create attachment'
  | 'update attachment'
  | 'update attachment file';

export interface AttachmentsTabState {
  IsDirty: boolean;
  Attachments: IAttachment[];
  AttachmentDataAvailable: AttachmentDataAvailable;
  AttachmentFilters: any[];
}

export const initialAttachmentsTabState: AttachmentsTabState = {
  IsDirty: false,
  Attachments: [],
  AttachmentDataAvailable: '',
  AttachmentFilters: [],
};

export const reducer = createReducer(
  initialAttachmentsTabState,
  on(
    actions.getAttachments,
    (state: AttachmentsTabState): AttachmentsTabState => {
      return {
        ...state,
        Attachments: [],
        AttachmentDataAvailable: 'loading',
      };
    }
  ),
  on(
    actions.getAttachmentsSuccess,
    (
      state: AttachmentsTabState,
      payload: { attachments: IAttachment[] }
    ): AttachmentsTabState => {
      const dataAvailable: AttachmentDataAvailable =
        payload.attachments.length > 0 ? '' : 'nodata';
      return {
        ...state,
        Attachments: payload.attachments,
        AttachmentDataAvailable: dataAvailable,
      };
    }
  ),
  on(
    actions.getAttachmentsFailure,
    (state: AttachmentsTabState): AttachmentsTabState => {
      return {
        ...state,
        AttachmentDataAvailable: '',
      };
    }
  ),
  on(
    actions.setFilters,
    (
      state: AttachmentsTabState,
      payload: { filters: any[] }
    ): AttachmentsTabState => {
      return {
        ...state,
        AttachmentFilters: payload.filters,
      };
    }
  ),
  on(
    actions.clearFilters,
    (state: AttachmentsTabState): AttachmentsTabState => {
      return {
        ...state,
        AttachmentFilters: [],
      };
    }
  ),
  on(
    actions.removeFilter,
    (
      state: AttachmentsTabState,
      payload: { filter: any; callback: (filterName: string) => void }
    ): AttachmentsTabState => {
      const filters: any[] = [...state.AttachmentFilters];
      const index: number = filters.indexOf(payload.filter);
      if (index >= 0) {
        filters.splice(index, 1);
        payload.callback(payload.filter.Name);
      }
      return {
        ...state,
        AttachmentFilters: filters,
      };
    }
  ),
  on(
    actions.upsertAttachment,
    (state: AttachmentsTabState): AttachmentsTabState => {
      return {
        ...state,
        AttachmentDataAvailable: 'loading',
      };
    }
  ),
  on(
    actions.upsertAttachmentSuccess,
    (
      state: AttachmentsTabState,
      payload: { upsertResult: IAttachment; action: string }
    ): AttachmentsTabState => {
      const attachments: IAttachment[] = [
        ...produce(
          state.Attachments,
          (draftAttachments: WritableDraft<IAttachment[]>) => {
            if (payload.action === 'create attachment') {
              draftAttachments.splice(0, 0, payload.upsertResult);
            } else if (
              payload.action === 'update attachment' ||
              payload.action === 'update attachment file'
            ) {
              const idx: number = draftAttachments.findIndex(
                (a: IAttachment) => a.Id === payload.upsertResult.Id
              );
              if (idx > -1) {
                draftAttachments[idx] = {
                  ...payload.upsertResult,
                  Upload: undefined,
                  TemporaryLink: undefined,
                };
              }
            }
          }
        ),
      ];
      return {
        ...state,
        Attachments: attachments,
        AttachmentDataAvailable: '',
      };
    }
  ),
  on(
    actions.upsertAttachmentFailure,
    (state: AttachmentsTabState, payload: Error): AttachmentsTabState => {
      return {
        ...state,
        AttachmentDataAvailable: '',
      };
    }
  ),
  on(
    actions.deleteAttachments,
    (state: AttachmentsTabState): AttachmentsTabState => {
      return {
        ...state,
        AttachmentDataAvailable: 'loading',
      };
    }
  ),
  on(
    actions.deleteAttachmentsSuccess,
    (
      state: AttachmentsTabState,
      payload: { deletedAttachments: string[] }
    ): AttachmentsTabState => {
      const attachments: IAttachment[] = [
        ...produce(
          state.Attachments,
          (draftAttachments: WritableDraft<IAttachment[]>) => {
            return draftAttachments.filter((draftAttachment: IAttachment) => {
              return !payload.deletedAttachments.includes(draftAttachment.Id);
            });
          }
        ),
      ];

      return {
        ...state,
        Attachments: attachments,
        AttachmentDataAvailable: '',
      };
    }
  ),
  on(
    actions.deleteAttachmentsFailure,
    (state: AttachmentsTabState, payload: Error): AttachmentsTabState => {
      return {
        ...state,
        AttachmentDataAvailable: '',
      };
    }
  )
);
