/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  IServerResult,
  IPDTagMap,
  ITagMapAuditResult,
  ITagResult,
  isNil,
} from '@atonix/atx-core';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/tags-tab.actions';
import { groupBy, isEqual } from 'lodash';
import { Dictionary } from '@ngrx/entity';
import produce from 'immer';
import { WritableDraft } from 'immer/dist/internal';

export const tagsTabFeatureKey = 'tagsTab';

export type TagsDataAvailable = 'loading' | 'nodata' | '';
export type TagMapsDataAvailable = 'loading' | 'nodata' | '';

export interface TagsTabState {
  Servers: IServerResult[];
  SelectedServer: IServerResult | null;
  Tags: ITagResult[];
  TagMaps: IPDTagMap[];
  DisplayedTags: ITagResult[];
  TagsDataAvailable: TagsDataAvailable;
  ShowDeletedTags: boolean;
  ValidationMessage: string;
  TagMapFilters: any[];
  TagMapsDataAvailable: TagMapsDataAvailable;
  IsDirty: boolean;
  TagFilters: any[];
}

export const initialTagsTabState: TagsTabState = {
  Servers: [],
  SelectedServer: null,
  Tags: [],
  TagMaps: [],
  DisplayedTags: [],
  TagsDataAvailable: '',
  ShowDeletedTags: false,
  ValidationMessage: '',
  TagMapFilters: [],
  TagMapsDataAvailable: '',
  IsDirty: false,
  TagFilters: [],
};

export const reducer = createReducer(
  initialTagsTabState,
  on(actions.init, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      Tags: [],
      TagMaps: [],
      DisplayedTags: [],
      Servers: [],
      SelectedServer: null,
      ValidationMessage: '',
    };
  }),
  on(
    actions.routeLoadTagsSuccess,
    (
      state: TagsTabState,
      payload: { asset: string; servers: IServerResult[]; init: boolean }
    ): TagsTabState => {
      if (payload.init) {
        return {
          ...state,
          Servers: payload.servers,
          SelectedServer: null,
          Tags: [],
          TagMaps: [],
          DisplayedTags: [],
          ValidationMessage: '',
        };
      }
      const server: IServerResult | undefined = payload.servers.find(
        (server: IServerResult) => server.Id === state.SelectedServer?.Id
      );

      return {
        ...state,
        Servers: payload.servers,
        SelectedServer: server ?? null,
        ValidationMessage: '',
      };
    }
  ),
  on(actions.routeLoadTagsFailure, (state: TagsTabState): TagsTabState => {
    return { ...state };
  }),
  on(
    actions.getTags,
    (
      state: TagsTabState,
      payload: { servers?: IServerResult[]; server: IServerResult }
    ): TagsTabState => {
      let tagsDataAvailable: TagsDataAvailable = '';
      if (payload.server ?? -1 >= 0) {
        tagsDataAvailable = 'loading';
      }
      return {
        ...state,
        Servers: payload.servers ?? state.Servers,
        SelectedServer: payload.server,
        Tags: [],
        TagMaps: [],
        DisplayedTags: [],
        TagMapsDataAvailable: tagsDataAvailable,
        TagsDataAvailable: tagsDataAvailable,
      };
    }
  ),
  on(
    actions.getTagsSuccess,
    (
      state: TagsTabState,
      payload: { tags: ITagResult[]; server: IServerResult }
    ): TagsTabState => {
      let tagsDataAvailable: TagsDataAvailable = 'nodata';
      const servers: IServerResult[] = produce(
        state.Servers,
        (draftState: WritableDraft<IServerResult[]>) => {
          const server: IServerResult | undefined = draftState.find(
            (s: IServerResult) => s.Id === payload.server.Id
          );
          if (server && payload.tags && payload.tags.length > 0) {
            tagsDataAvailable = '';
          }
        }
      );
      const server: IServerResult | undefined = servers.find(
        (server: IServerResult) => server.Id === payload.server.Id
      );
      const displayedTags: ITagResult[] = getSortedFilteredTags(
        payload.tags,
        state.ShowDeletedTags
      );
      return {
        ...state,
        Servers: servers,
        SelectedServer: server ?? null,
        Tags: payload.tags,
        DisplayedTags: displayedTags,
        TagsDataAvailable: tagsDataAvailable,
      };
    }
  ),
  on(actions.getTagsFailure, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      SelectedServer: null,
      Tags: [],
      TagMaps: [],
      DisplayedTags: [],
      TagsDataAvailable: '',
    };
  }),
  on(actions.discardChanges, (state: TagsTabState): TagsTabState => {
    const tagsDataAvailable: TagsDataAvailable = 'loading';
    return {
      ...state,
      Tags: [],
      TagMaps: [],
      DisplayedTags: [],
      TagsDataAvailable: tagsDataAvailable,
      TagMapsDataAvailable: tagsDataAvailable,
      IsDirty: false,
    };
  }),
  on(
    actions.discardChangesSuccess,
    (
      state: TagsTabState,
      payload: { tags: ITagResult[]; server: IServerResult | null }
    ): TagsTabState => {
      let tagsDataAvailable: TagsDataAvailable = 'nodata';
      const servers: IServerResult[] = produce(
        state.Servers,
        (draftState: WritableDraft<IServerResult[]>) => {
          const server: IServerResult | undefined = draftState.find(
            (s: IServerResult) => s.Id === payload.server?.Id
          );
          if (server && payload.tags && payload.tags.length > 0) {
            tagsDataAvailable = '';
          }
        }
      );
      const server: IServerResult | undefined = servers.find(
        (server: IServerResult) => server.Id === payload.server?.Id
      );
      const displayedTags = getSortedFilteredTags(
        payload.tags,
        state.ShowDeletedTags
      );
      return {
        ...state,
        Servers: servers,
        SelectedServer: server ?? null,
        Tags: payload.tags,
        DisplayedTags: displayedTags,
        TagsDataAvailable: tagsDataAvailable,
      };
    }
  ),
  on(actions.discardChangesFailure, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      SelectedServer: null,
      Tags: [],
      TagMaps: [],
      DisplayedTags: [],
      TagsDataAvailable: '',
      TagMapsDataAvailable: '',
      IsDirty: true,
    };
  }),
  on(actions.addTag, (state: TagsTabState): TagsTabState => {
    const newTag: ITagResult = {
      Id: `${getNextTagID(state.Tags)}`,
      Name: 'New Tag',
      Description: 'New Tag',
      EngUnit: '',
      Qualifier: '',
      Source: '',
      CreateDate: new Date(),
      ChangeDate: undefined,
      ExistsOnServer: true,
      InUse: 0,
      IsDirty: true,
    };
    const tags = changeSingleTag(state.Tags, newTag);
    const displayedTags = getSortedFilteredTags(tags, state.ShowDeletedTags);
    return {
      ...state,
      Tags: tags,
      DisplayedTags: displayedTags,
      IsDirty: true,
    };
  }),
  on(
    actions.changeTag,
    (
      state: TagsTabState,
      payload: {
        tag: ITagResult;
      }
    ): TagsTabState => {
      const tags = changeSingleTag(state.Tags, payload.tag);
      const displayedTags = getSortedFilteredTags(tags, state.ShowDeletedTags);
      return {
        ...state,
        Tags: tags,
        DisplayedTags: displayedTags,
        IsDirty: true,
      };
    }
  ),
  on(
    actions.showDeletedTagsChange,
    (
      state: TagsTabState,
      payload: {
        newValue: boolean;
      }
    ): TagsTabState => {
      const displayedTags = getSortedFilteredTags(state.Tags, payload.newValue);
      return {
        ...state,
        ShowDeletedTags: payload.newValue,
        DisplayedTags: displayedTags,
      };
    }
  ),
  on(
    actions.deleteTag,
    (
      state: TagsTabState,
      payload: {
        tagId: string;
      }
    ): TagsTabState => {
      let tags = state.Tags;
      const index = tags.findIndex((n) => {
        return n.Id === payload.tagId;
      });
      if (index >= 0) {
        if (payload.tagId !== '') {
          const newTag = {
            ...state.Tags[index],
            ExistsOnServer: !state.Tags[index].ExistsOnServer,
            IsDirty: true,
          };
          tags = [...state.Tags];
          tags.splice(index, 1, newTag);
        } else {
          tags = [...state.Tags];
          tags.splice(index, 1);
        }
      }

      const displayedTags = getSortedFilteredTags(tags, state.ShowDeletedTags);

      return {
        ...state,
        Tags: tags,
        DisplayedTags: displayedTags,
        IsDirty: true,
      };
    }
  ),
  on(actions.refreshTags, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      TagsDataAvailable: 'loading',
    };
  }),
  on(actions.saveTags, (state: TagsTabState): TagsTabState => {
    return { ...state, ValidationMessage: 'Saving Tags' };
  }),
  on(
    actions.saveTagsSuccess,
    (
      state: TagsTabState,
      payload: {
        result: ITagResult[];
        server: IServerResult | null;
      }
    ): TagsTabState => {
      if (payload.server?.PDServerId === state.SelectedServer?.PDServerId) {
        const tags: ITagResult[] = [
          ...produce(state.Tags, (draftTags: WritableDraft<ITagResult[]>) => {
            const tagByTagIdDict: { [key: string]: ITagResult } = {};
            draftTags.forEach((tag: ITagResult) => {
              tagByTagIdDict[tag.Id] = tag;
            });

            payload.result.forEach((tag: ITagResult) => {
              const tagName: string = tag.Name;
              const idx: number = draftTags.findIndex(
                (tag: ITagResult) => tag.Name === tagName
              );
              if (idx > -1) {
                draftTags[idx] = {
                  Id: tag.Id,
                  Name: tag.Name,
                  Description: tag.Description,
                  EngUnit: tag.EngUnit,
                  Qualifier: tag.Qualifier,
                  Source: tag.Source,
                  CreateDate: tag.CreateDate,
                  ChangeDate: tag.ChangeDate,
                  ExistsOnServer: tag.ExistsOnServer,
                  InUse: tagByTagIdDict[tag.Id]?.InUse ?? 0,
                  IsDirty: false,
                };
              }
            });
          }),
        ];
        const displayedTags: ITagResult[] = getSortedFilteredTags(
          tags,
          state.ShowDeletedTags
        );

        return {
          ...state,
          Tags: tags,
          DisplayedTags: displayedTags,
          ValidationMessage: 'Tags Saved Successfully',
          IsDirty: false,
        };
      }

      return { ...state };
    }
  ),
  on(
    actions.saveTagsFailure,
    (
      state: TagsTabState,
      payload: { error: Error; successful?: ITagResult[] | null }
    ): TagsTabState => {
      const tags: ITagResult[] = [
        ...produce(state.Tags, (draftTags: WritableDraft<ITagResult[]>) => {
          if (payload.successful != null) {
            const tagByTagIdDict: { [key: string]: ITagResult } = {};
            draftTags.forEach((tag: ITagResult) => {
              tagByTagIdDict[tag.Id] = tag;
            });

            payload.successful.forEach((tag: ITagResult) => {
              const tagName: string = tag.Name;
              const idx: number = draftTags.findIndex(
                (tag: ITagResult) => tag.Name === tagName
              );
              if (idx > -1) {
                draftTags[idx] = {
                  Id: tag.Id,
                  Name: tag.Name,
                  Description: tag.Description,
                  EngUnit: tag.EngUnit,
                  Qualifier: tag.Qualifier,
                  Source: tag.Source,
                  CreateDate: tag.CreateDate,
                  ChangeDate: tag.ChangeDate,
                  ExistsOnServer: tag.ExistsOnServer,
                  InUse: tagByTagIdDict[tag.Id]?.InUse ?? 0,
                  IsDirty: false,
                };
              }
            });
          }
        }),
      ];
      const displayedTags: ITagResult[] = getSortedFilteredTags(
        tags,
        state.ShowDeletedTags
      );

      return {
        ...state,
        Tags: tags,
        DisplayedTags: displayedTags,
        ValidationMessage:
          'Tag Save Failed' +
          (!isNil(payload.error?.message) ? `: ${payload.error.message}` : ''),
        IsDirty: false,
      };
    }
  ),
  on(
    actions.pasteTags,
    (state: TagsTabState, payload: { tags: ITagResult[] }): TagsTabState => {
      const existingTagsDict: { [id: string]: ITagResult } = {};
      for (const t of state.Tags.filter((tag: ITagResult) => {
        return tag.Id !== '';
      })) {
        existingTagsDict[t.Id] = t;
      }

      let tags: ITagResult[] = state.Tags;
      for (const t of payload.tags) {
        if (existingTagsDict[t.Id]) {
          tags = changeSingleTag(tags, t);
        } else {
          tags = changeSingleTag(tags, {
            ...t,
            Id: '',
            ExistsOnServer: true,
          });
        }
      }

      const displayedTags = getSortedFilteredTags(tags, state.ShowDeletedTags);
      return {
        ...state,
        Tags: tags,
        DisplayedTags: displayedTags,
        IsDirty: true,
      };
    }
  ),
  on(actions.associateTagsToTagMaps, (state: TagsTabState): TagsTabState => {
    const tagsDataAvailable: TagsDataAvailable = 'loading';
    return {
      ...state,
      TagMapsDataAvailable: tagsDataAvailable,
    };
  }),
  on(
    actions.associateTagsToTagMapsSuccess,
    (
      state: TagsTabState,
      {
        savedTagMaps,
        selectedTagMap,
      }: { savedTagMaps: IPDTagMap[]; selectedTagMap: IPDTagMap }
    ): TagsTabState => {
      if (savedTagMaps.length > 0) {
        const tagInUseCountByTagIdDict: { [key: string]: number } = {};
        const stm: IPDTagMap[] = savedTagMaps.map((tagMap: IPDTagMap) => ({
          ...tagMap,
        }));
        let placeHolderDeleted = false;
        // get the last index of selectedTagMap's AssetPath_VariableDesc
        let tagMapToUpdateIdx = state.TagMaps.length - 1;
        for (
          ;
          tagMapToUpdateIdx >= 0 &&
          `${state.TagMaps[tagMapToUpdateIdx].AssetPath}_${state.TagMaps[tagMapToUpdateIdx].VariableDesc}` !==
            `${selectedTagMap.AssetPath}_${selectedTagMap.VariableDesc}`;
          tagMapToUpdateIdx--
        );
        const updatedStateTagMaps: IPDTagMap[] = [
          ...produce(
            state.TagMaps,
            (draftState: WritableDraft<IPDTagMap[]>) => {
              if (selectedTagMap.TagId == null || selectedTagMap.TagId === '') {
                // remove placeholder
                draftState.splice(tagMapToUpdateIdx, 1);
                placeHolderDeleted = true;
              }

              // build the hierarchy
              for (let i = 0; i < stm.length; i++) {
                // increment tag's InUse count
                if (tagInUseCountByTagIdDict[stm[i].TagId] == null) {
                  tagInUseCountByTagIdDict[stm[i].TagId] = 0;
                }
                tagInUseCountByTagIdDict[stm[i].TagId] += 1;
                stm[i].Hierarchy = [];
                stm[i].Hierarchy.push(
                  `${stm[i].AssetPath}_${stm[i].VariableDesc}`
                );
                if (i == 0 && placeHolderDeleted) {
                  // placeholder was removed
                  continue;
                }
                stm[i].Hierarchy.push(stm[i].TagName);
              }
            }
          ),
        ];

        updatedStateTagMaps.splice(
          placeHolderDeleted ? tagMapToUpdateIdx : tagMapToUpdateIdx + 1,
          0,
          ...stm
        );

        // update InUse count
        const updatedStateTags: ITagResult[] = updateInUse(
          state.Tags,
          tagInUseCountByTagIdDict
        );
        const updatedStateDisplayedTags: ITagResult[] = getSortedFilteredTags(
          updatedStateTags,
          state.ShowDeletedTags
        );

        return {
          ...state,
          TagMaps: updatedStateTagMaps,
          TagMapsDataAvailable: '',
          Tags: updatedStateTags,
          DisplayedTags: updatedStateDisplayedTags,
        };
      }

      return { ...state, TagMapsDataAvailable: '' };
    }
  ),
  on(
    actions.associateTagsToTagMapsFailure,
    (state: TagsTabState): TagsTabState => {
      const tagsDataAvailable: TagsDataAvailable = '';
      return {
        ...state,
        TagMapsDataAvailable: tagsDataAvailable,
      };
    }
  ),
  on(actions.updateTagMaps, (state: TagsTabState): TagsTabState => {
    const tagsDataAvailable: TagsDataAvailable = 'loading';
    return {
      ...state,
      TagMapsDataAvailable: tagsDataAvailable,
    };
  }),
  on(
    actions.updateTagMapsSuccess,
    (
      state: TagsTabState,
      {
        filteredTagMaps,
        updatedTagMaps,
        selectedTagMap,
      }: {
        filteredTagMaps: IPDTagMap[];
        updatedTagMaps: IPDTagMap[];
        selectedTagMap: IPDTagMap;
      }
    ): TagsTabState => {
      if (updatedTagMaps.length > 0) {
        let placeHolderDeleted = false;
        let selectedTagMapIdx = -1;
        const tagInUseCountByTagIdDict: { [key: string]: number } = {};
        const utm: IPDTagMap[] = updatedTagMaps.map((tagMap: IPDTagMap) => ({
          ...tagMap,
        }));
        const updated: IPDTagMap[] = [
          ...produce(
            state.TagMaps,
            (draftTagMaps: WritableDraft<IPDTagMap[]>) => {
              selectedTagMapIdx = draftTagMaps.findIndex((tagMap: IPDTagMap) =>
                isEqual(tagMap, selectedTagMap)
              );
              if (
                draftTagMaps[selectedTagMapIdx].TagId == null ||
                draftTagMaps[selectedTagMapIdx].TagId === ''
              ) {
                // remove placeholder
                draftTagMaps.splice(selectedTagMapIdx, 1);
                placeHolderDeleted = true;
              }
            }
          ),
        ];

        // build the hierarchy
        for (let i = 0; i < utm.length; i++) {
          // increment tag's InUse count
          if (tagInUseCountByTagIdDict[utm[i].TagId] == null) {
            tagInUseCountByTagIdDict[utm[i].TagId] = 0;
          }
          tagInUseCountByTagIdDict[utm[i].TagId] += 1;
          utm[i].Hierarchy = [];
          utm[i].Hierarchy.push(`${utm[i].AssetPath}_${utm[i].VariableDesc}`);
          if (i == 0 && placeHolderDeleted) {
            // placeholder was removed
            continue;
          }
          utm[i].Hierarchy.push(utm[i].TagName);
        }

        if (placeHolderDeleted) {
          updated.splice(selectedTagMapIdx, 0, ...utm);
        } else {
          // insert newly-added tagMaps to the bottom of the target's AssetPath_VariableDesc
          let lastIndex = state.TagMaps.length - 1;
          for (
            ;
            lastIndex >= 0 &&
            `${state.TagMaps[lastIndex].AssetPath}_${state.TagMaps[lastIndex].VariableDesc}` !==
              `${selectedTagMap.AssetPath}_${selectedTagMap.VariableDesc}`;
            lastIndex--
          );
          updated.splice(lastIndex + 1, 0, ...utm);
        }

        const toDelete: IPDTagMap[] = [];
        const updatedIDs: (number | null)[] = updatedTagMaps.map(
          (tagMap: IPDTagMap) => tagMap.AssetVariableTypeTagMapId
        );
        updatedIDs.forEach((id: number | null) => {
          const tm: IPDTagMap | undefined = filteredTagMaps.find(
            (tagMap: IPDTagMap) => tagMap.AssetVariableTypeTagMapId === id
          );
          if (tm) {
            toDelete.push(tm);
          }
        });

        // delete dragged tagMaps that were successfuly updated
        deleteTagMaps(updated, toDelete, tagInUseCountByTagIdDict);

        // update InUse count
        const updatedTags: ITagResult[] = updateInUse(
          state.Tags,
          tagInUseCountByTagIdDict
        );
        const updatedDisplayedTags: ITagResult[] = getSortedFilteredTags(
          updatedTags,
          state.ShowDeletedTags
        );

        return {
          ...state,
          TagMaps: updated,
          TagMapsDataAvailable: '',
          Tags: updatedTags,
          DisplayedTags: updatedDisplayedTags,
        };
      }

      return { ...state, TagMapsDataAvailable: '' };
    }
  ),
  on(actions.updateTagMapsFailure, (state: TagsTabState): TagsTabState => {
    const tagsDataAvailable: TagsDataAvailable = '';
    return {
      ...state,
      TagMapsDataAvailable: tagsDataAvailable,
    };
  }),
  on(
    actions.getTagMaps,
    (
      state: TagsTabState,
      payload: { server: IServerResult | null; assetId: string }
    ): TagsTabState => {
      let tagMapsDataAvailable: TagMapsDataAvailable = '';
      if (payload.server) {
        tagMapsDataAvailable = 'loading';
      }
      return {
        ...state,
        TagMapsDataAvailable: tagMapsDataAvailable,
        TagMaps: [],
      };
    }
  ),
  on(
    actions.getTagMapsSuccess,
    (state: TagsTabState, payload: { tagMaps: IPDTagMap[] }): TagsTabState => {
      let tagMapsDataAvailable: TagMapsDataAvailable = 'nodata';
      if (payload.tagMaps && payload.tagMaps.length > 0) {
        tagMapsDataAvailable = '';
      }
      const tagMaps: IPDTagMap[] = computeHierarchy(payload.tagMaps);
      return {
        ...state,
        TagMapsDataAvailable: tagMapsDataAvailable,
        TagMaps: tagMaps,
      };
    }
  ),
  on(actions.getTagMapsFailure, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      TagMaps: [],
      TagMapsDataAvailable: '',
    };
  }),
  on(
    actions.setTagMapFilters,
    (state: TagsTabState, payload: { filters: any[] }): TagsTabState => {
      return {
        ...state,
        TagMapFilters: payload.filters,
      };
    }
  ),
  on(actions.clearTagMapFilters, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      TagMapFilters: [],
    };
  }),
  on(
    actions.removeTagMapFilter,
    (
      state: TagsTabState,
      payload: { filter: any; callback: (filterName: string) => void }
    ): TagsTabState => {
      const filters: any[] = [...state.TagMapFilters];
      const index: number = filters.indexOf(payload.filter);
      if (index >= 0) {
        filters.splice(index, 1);
        payload.callback(payload.filter.Name);
      }
      return {
        ...state,
        TagMapFilters: filters,
      };
    }
  ),
  on(
    actions.setTagFilters,
    (state: TagsTabState, payload: { filters: any[] }): TagsTabState => {
      return {
        ...state,
        TagFilters: payload.filters,
      };
    }
  ),
  on(actions.clearTagFilters, (state: TagsTabState): TagsTabState => {
    return {
      ...state,
      TagFilters: [],
    };
  }),
  on(
    actions.removeTagFilter,
    (
      state: TagsTabState,
      payload: { filter: any; callback: (filterName: string) => void }
    ): TagsTabState => {
      const filters: any[] = [...state.TagFilters];
      const index: number = filters.indexOf(payload.filter);
      if (index >= 0) {
        filters.splice(index, 1);
        payload.callback(payload.filter.Name);
      }
      return {
        ...state,
        TagFilters: filters,
      };
    }
  ),
  on(actions.auditTagMaps, (state): TagsTabState => {
    const tagMapsDataAvailable: TagMapsDataAvailable = 'loading';
    return {
      ...state,
      TagMapsDataAvailable: tagMapsDataAvailable,
    };
  }),
  on(
    actions.auditTagMapsSuccess,
    (
      state,
      {
        auditResult,
        callback,
      }: {
        auditResult: ITagMapAuditResult[];
        callback: (tagMapIds: number[], text: string) => void;
      }
    ): TagsTabState => {
      const tagMapsDataAvailable: TagMapsDataAvailable = '';
      const message: ITagMapAuditResult[] = auditResult.filter(
        (audit: ITagMapAuditResult) => audit.Message != null
      );

      // build the string to be shown on the popup
      let text = '';
      const tagMapIds: number[] = auditResult.map(
        (audit: ITagMapAuditResult) => audit.AssetVariableTypeTagMapId
      );
      if (message.length > 0) {
        const tagMapDict = groupBy(
          state.TagMaps.filter(
            (tagMap: IPDTagMap) =>
              tagMap.AssetVariableTypeTagMapId != null &&
              tagMapIds.includes(tagMap.AssetVariableTypeTagMapId)
          ),
          (tagMap: IPDTagMap) => tagMap.AssetVariableTypeTagMapId
        );

        message.forEach((audit: ITagMapAuditResult) => {
          const assetName =
            tagMapDict[audit.AssetVariableTypeTagMapId][0].AssetName;
          const variableDesc =
            tagMapDict[audit.AssetVariableTypeTagMapId][0].VariableDesc;
          const tagName =
            tagMapDict[audit.AssetVariableTypeTagMapId][0].TagName;
          text += `• ${assetName}\\${variableDesc} ${tagName}\n${audit.Message}\n\n`;
        });
      }

      callback(tagMapIds, text);

      return {
        ...state,
        TagMapsDataAvailable: tagMapsDataAvailable,
      };
    }
  ),
  on(actions.deleteTagMaps, (state): TagsTabState => {
    const tagMapsDataAvailable: TagMapsDataAvailable = 'loading';
    return {
      ...state,
      TagMapsDataAvailable: tagMapsDataAvailable,
    };
  }),
  on(
    actions.deleteTagMapsSuccess,
    (state, { tagMapIds }: { tagMapIds: number[] }): TagsTabState => {
      const tagMapsDataAvailable: TagMapsDataAvailable = '';

      const deleted: IPDTagMap[] = state.TagMaps.filter(
        (tagMap: IPDTagMap) =>
          tagMap.AssetVariableTypeTagMapId != null &&
          tagMapIds.includes(tagMap.AssetVariableTypeTagMapId)
      );
      const updated: IPDTagMap[] = [
        ...produce(
          state.TagMaps,
          (draftState: WritableDraft<IPDTagMap[]>) => draftState
        ),
      ];

      // delete the deleted tagMaps from the grid
      const tagInUseCountByTagIdDict: { [key: string]: number } = {};
      deleteTagMaps(updated, deleted, tagInUseCountByTagIdDict);

      // update InUse count
      const updatedTags: ITagResult[] = updateInUse(
        state.Tags,
        tagInUseCountByTagIdDict
      );
      const updatedDisplayedTags: ITagResult[] = getSortedFilteredTags(
        updatedTags,
        state.ShowDeletedTags
      );

      return {
        ...state,
        TagMaps: updated,
        TagMapsDataAvailable: tagMapsDataAvailable,
        Tags: updatedTags,
        DisplayedTags: updatedDisplayedTags,
      };
    }
  ),
  on(actions.deleteTagMapsFailure, (state): TagsTabState => {
    const tagMapsDataAvailable: TagMapsDataAvailable = '';
    return {
      ...state,
      TagMapsDataAvailable: tagMapsDataAvailable,
    };
  }),
  on(actions.updateValueTypeOfTagMap, (state: TagsTabState): TagsTabState => {
    const tagMapsDataAvailable: TagMapsDataAvailable = 'loading';
    return {
      ...state,
      TagMapsDataAvailable: tagMapsDataAvailable,
    };
  }),
  on(
    actions.updateValueTypeOfTagMapSuccess,
    (
      state: TagsTabState,
      payload: { selectedTagMap: IPDTagMap; newValueType: string }
    ): TagsTabState => {
      const tagMapsDataAvailable: TagMapsDataAvailable = '';
      const updated: IPDTagMap[] = [
        ...produce(
          state.TagMaps,
          (draftState: WritableDraft<IPDTagMap[]>) => draftState
        ),
      ];
      const index: number = updated.findIndex((tagMap: IPDTagMap) =>
        isEqual(tagMap, payload.selectedTagMap)
      );
      updated[index] = {
        ...updated[index],
        ValueTypeKey: payload.newValueType,
      };

      return {
        ...state,
        TagMaps: updated,
        TagMapsDataAvailable: tagMapsDataAvailable,
      };
    }
  ),
  on(
    actions.updateValueTypeOfTagMapFailure,
    (state: TagsTabState): TagsTabState => {
      const tagMapsDataAvailable: TagMapsDataAvailable = '';
      return {
        ...state,
        TagMapsDataAvailable: tagMapsDataAvailable,
      };
    }
  )
);

function getSortedFilteredTags(
  tags: ITagResult[],
  showDeletedTags: boolean
): ITagResult[] {
  let result = [...tags];
  if (!showDeletedTags) {
    result = result.filter((n) => n.ExistsOnServer);
  }
  return result;
}

function getNextTagID(tags: ITagResult[]): number {
  const newTagsIds = tags
    .filter((tag: ITagResult) => tag.Id.startsWith('-'))
    .map((tag: ITagResult) => tag.Id);
  let nextTagID = Math.min(...newTagsIds.map((n) => parseInt(n)));
  if (nextTagID >= 0) {
    nextTagID = -1;
  } else {
    nextTagID = nextTagID - 1;
  }
  return nextTagID;
}

function changeSingleTag(tags: ITagResult[], tag: ITagResult): ITagResult[] {
  let result: ITagResult[];
  const idx = tags.findIndex((n) => n.Id === tag.Id);
  if (idx >= 0) {
    result = [...tags];
    result.splice(idx, 1, { ...tag });
  } else {
    result = [tag, ...tags];
  }
  return result;
}

function computeHierarchy(tagMaps: IPDTagMap[]): IPDTagMap[] {
  const updatedTagMaps: IPDTagMap[] = tagMaps.map((tagMap: IPDTagMap) => ({
    ...tagMap,
    Hierarchy: [],
  }));

  const dict: Dictionary<IPDTagMap[]> = groupBy(
    updatedTagMaps,
    (tagMap: IPDTagMap) => {
      return `${tagMap.AssetPath}_${tagMap.VariableDesc}`;
    }
  );

  Object.entries(dict).forEach(
    ([, value]: [string, IPDTagMap[] | undefined]) => {
      for (let i = 0; value && i < value?.length; i++) {
        value[i].Hierarchy.push(
          `${value[i].AssetPath}_${value[i].VariableDesc}`
        );
        if (i != 0 && value[i].TagName !== '') {
          value[i].Hierarchy.push(value[i].TagName);
        }
      }
    }
  );

  return updatedTagMaps;
}

function deleteTagMaps(
  updated: IPDTagMap[],
  tagMapsToDelete: IPDTagMap[],
  tagInUseCountByTagIdDict: { [key: string]: number } = {}
): void {
  const tagMapsDict: Dictionary<IPDTagMap[]> = groupBy(
    tagMapsToDelete,
    (tagMap: IPDTagMap) => `${tagMap.AssetPath}_${tagMap.VariableDesc}`
  );
  Object.entries(tagMapsDict).forEach(
    ([, value]: [string, IPDTagMap[] | undefined]) => {
      if (value) {
        const numOfTagMaps: number = updated.filter(
          (tagMap: IPDTagMap) =>
            tagMap.AssetPath === value[0].AssetPath &&
            tagMap.VariableDesc === value[0].VariableDesc
        ).length;
        const lastNodeBecomesPlaceHolder = numOfTagMaps === value.length;
        for (let i = 0; i < value.length; i++) {
          const idxToBeDeleted: number = updated.findIndex(
            (tagMap: IPDTagMap) => isEqual(tagMap, value[i])
          );
          // decrement tag's InUse count
          if (tagInUseCountByTagIdDict[updated[idxToBeDeleted].TagId] == null) {
            tagInUseCountByTagIdDict[updated[idxToBeDeleted].TagId] = 0;
          }
          tagInUseCountByTagIdDict[updated[idxToBeDeleted].TagId] -= 1;
          if (i == value.length - 1 && lastNodeBecomesPlaceHolder) {
            // last node for that AssetPath_VariableDesc becomes a placeholder
            updated[idxToBeDeleted] = {
              ...updated[idxToBeDeleted],
              TagName: '',
              TagDescription: '',
              TagEngUnit: '',
              TagId: '',
              PDServerName: '',
              AssetVariableTypeTagMapId: null,
              ValueTypeKey: '',
              // leave ExistsOnServer as true, to prevent placeholder from being filtered out
              ExistsOnServer: true,
              Hierarchy: [
                `${updated[idxToBeDeleted].AssetPath}_${updated[idxToBeDeleted].VariableDesc}`,
              ],
            };
          } else {
            updated.splice(idxToBeDeleted, 1);
          }
        }
        if (!lastNodeBecomesPlaceHolder) {
          // re-compute Hierarchy of the updated top-most AssetPath_VariableDesc
          const index: number = updated.findIndex(
            (tagMap: IPDTagMap) =>
              tagMap.AssetPath === value[0].AssetPath &&
              tagMap.VariableDesc === value[0].VariableDesc
          );
          updated[index] = {
            ...updated[index],
            Hierarchy: [
              `${updated[index].AssetPath}_${updated[index].VariableDesc}`,
            ],
          };
        }
      }
    }
  );
}

function updateInUse(
  stateTags: ITagResult[],
  tagInUseCountByTagIdDict: { [key: string]: number }
): ITagResult[] {
  return [
    ...produce(stateTags, (draftState: WritableDraft<ITagResult[]>) => {
      draftState.forEach((tag: ITagResult) => {
        if (tagInUseCountByTagIdDict[tag.Id] != null) {
          tag.InUse += tagInUseCountByTagIdDict[tag.Id];
        }
      });
    }),
  ];
}
