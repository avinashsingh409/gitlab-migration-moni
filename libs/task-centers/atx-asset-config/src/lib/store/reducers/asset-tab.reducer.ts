/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  IAssetClassTypeResult,
  IAssetCreateUpdateResult,
  IAssetResult,
} from '@atonix/atx-core';
import { createReducer, on } from '@ngrx/store';
import produce from 'immer';
import { WritableDraft } from 'immer/dist/internal';
import { findLastIndex, orderBy } from 'lodash';
import * as actions from '../actions/asset-tab.actions';

export const assetTabFeatureKey = 'assetTab';

export type DataAvailable = 'loading' | 'nodata' | '';

export interface AssetTabState {
  Assets: IAssetResult[];
  IsDirty: boolean;
  AssetDataAvailable: DataAvailable;
  AssetFilters: any[];
  AssetToCreateOrEdit: IAssetResult | null;
  AssetClassTypes: IAssetClassTypeResult[];
  AssetClassTypeDataAvailable: DataAvailable;
}

export const initialAssetTabState: AssetTabState = {
  Assets: [],
  IsDirty: false,
  AssetDataAvailable: '',
  AssetFilters: [],
  AssetToCreateOrEdit: null,
  AssetClassTypes: [],
  AssetClassTypeDataAvailable: '',
};

export const reducer = createReducer(
  initialAssetTabState,
  on(actions.getAssets, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      Assets: [],
      AssetDataAvailable: 'loading',
    };
  }),
  on(
    actions.getAssetsSuccess,
    (
      state: AssetTabState,
      payload: { assets: IAssetResult[] }
    ): AssetTabState => {
      const dataAvailable: DataAvailable =
        payload.assets.length > 0 ? '' : 'nodata';
      const newAssets: IAssetResult[] = [
        ...produce(
          payload.assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            draftState.map((a: IAssetResult) => {
              populateParentNameAndAssetPath(a);
              return a;
            });
          }
        ),
      ];
      return {
        ...state,
        Assets: orderAssets(newAssets),
        AssetDataAvailable: dataAvailable,
      };
    }
  ),
  on(actions.getAssetsFailure, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: '',
    };
  }),
  on(actions.updateAssets, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: 'loading',
    };
  }),
  on(
    actions.updateAssetsSuccess,
    (
      state: AssetTabState,
      payload: { assets: IAssetCreateUpdateResult[] }
    ): AssetTabState => {
      const updatedDictById: {
        [key: string]: IAssetResult;
      } = Object.assign(
        {},
        ...payload.assets
          .filter((asset: IAssetCreateUpdateResult) => !asset.Error)
          .map((asset: IAssetCreateUpdateResult) => ({
            [asset.AssetId]: {
              AssetId: asset.AssetId,
              AssetName: asset.AssetName,
              AssetDesc: asset.AssetDesc,
              AssetClassTypeKey: asset.AssetClassTypeKey,
              AssetClassTypeDesc: asset.AssetClassTypeDesc,
              ParentAssetId: asset.ParentAssetId,
              DisplayOrder: asset.DisplayOrder,
              IsStandard: asset.IsStandard,
              CreateDate: asset.CreateDate,
              ChangeDate: asset.ChangeDate,
              Hierarchy: [...asset.Hierarchy],
            } as IAssetResult,
          }))
      );

      const updatedAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            for (let i = 0; i < draftState.length; i++) {
              const updatedAsset: IAssetResult | undefined =
                updatedDictById[draftState[i].AssetId];
              if (updatedAsset) {
                populateParentNameAndAssetPath(updatedAsset);
                // recompute Hierarchy and AssetPath of its descendants (if any)
                const parentPath: string =
                  draftState[i].Hierarchy.length === 1
                    ? `/${draftState[i].AssetName}`
                    : `${draftState[i].AssetPath}/${draftState[i].AssetName}`;
                const descendants: IAssetResult[] = draftState.filter(
                  (asset: IAssetResult) => asset.AssetPath.includes(parentPath)
                );
                draftState[i] = Object.assign({}, updatedAsset);
                if (descendants.length > 0) {
                  const pathDict: { [id: string]: string[] } = {};
                  pathDict[draftState[i].AssetId] = [
                    ...draftState[i].Hierarchy,
                  ];
                  for (let y = 0; y < descendants.length; y++) {
                    let hierarchy: string[] = [];
                    const assetName: string = descendants[y].AssetName;
                    const assetId: string = descendants[y].AssetId;
                    const parentAssetId: string = descendants[y].ParentAssetId;
                    if (!pathDict[assetId]) {
                      if (parentAssetId && pathDict[parentAssetId] != null) {
                        hierarchy = [...pathDict[parentAssetId]];
                      }
                      hierarchy.push(assetName);
                      pathDict[assetId] = [...hierarchy];
                    }

                    descendants[y].Hierarchy = pathDict[assetId];
                    populateParentNameAndAssetPath(descendants[y]);
                  }
                }
              }
            }
          }
        ),
      ];

      return {
        ...state,
        Assets: orderAssets(updatedAssets),
        AssetDataAvailable: '',
      };
    }
  ),
  on(actions.updateAssetsFailure, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: '',
    };
  }),
  on(actions.reParentAsset, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: 'loading',
    };
  }),
  on(
    actions.reParentAssetSuccess,
    (
      state: AssetTabState,
      {
        newParent,
        draggedAsset,
      }: {
        newParent: IAssetResult;
        draggedAsset: IAssetResult;
      }
    ): AssetTabState => {
      const newAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            const draggedAssetIndex: number = draftState.findIndex(
              (asset: IAssetResult) => asset.AssetId === draggedAsset.AssetId
            );

            let draggedAssetAndDescendants: IAssetResult[] = [];
            // get the last index
            const draggedAssetPath: string =
              draggedAsset.Hierarchy.length === 1
                ? `/${draggedAsset.AssetName}`
                : `${draggedAsset.AssetPath}/${draggedAsset.AssetName}`;
            let lastIndex: number = findLastIndex(
              draftState,
              (asset: IAssetResult) =>
                asset.AssetPath.includes(draggedAssetPath)
            );
            // if no descendants, assign draggedAssetIndex to lastIndex
            if (lastIndex < 0) {
              lastIndex = draggedAssetIndex;
            }
            // get the dragged asset + its descendants (if any)
            draggedAssetAndDescendants = draftState.slice(
              draggedAssetIndex,
              lastIndex + 1
            );

            // remove the dragged assets + its descendants (if any)
            draftState.splice(
              draggedAssetIndex,
              lastIndex - draggedAssetIndex + 1
            );

            // update draggedAsset's properties accordingly
            const assetToInsert: IAssetResult = {
              ...draggedAsset,
              ParentAssetId: newParent.AssetId,
              Hierarchy: [...newParent.Hierarchy],
            };

            assetToInsert.Hierarchy.push(assetToInsert.AssetName);
            draggedAssetAndDescendants[0] = assetToInsert;
            populateParentNameAndAssetPath(draggedAssetAndDescendants[0]);

            // recompute Hierarchy and AssetPath of its descendants (if any)
            const pathDict: { [id: string]: string[] } = {};
            pathDict[assetToInsert.AssetId] = [...assetToInsert.Hierarchy];
            for (let i = 1; i < draggedAssetAndDescendants.length; i++) {
              let hierarchy: string[] = [];
              const assetName: string = draggedAssetAndDescendants[i].AssetName;
              const assetId: string = draggedAssetAndDescendants[i].AssetId;
              const parentAssetId: string =
                draggedAssetAndDescendants[i].ParentAssetId;
              if (!pathDict[assetId]) {
                if (parentAssetId && pathDict[parentAssetId] != null) {
                  hierarchy = [...pathDict[parentAssetId]];
                }
                hierarchy.push(assetName);
                pathDict[assetId] = [...hierarchy];
              }

              draggedAssetAndDescendants[i].Hierarchy = pathDict[assetId];
              populateParentNameAndAssetPath(draggedAssetAndDescendants[i]);
            }

            // insert draggedAsset + its descendants (if any) below the newParent
            const newParentIndex: number = draftState.findIndex(
              (asset: IAssetResult) => asset.AssetId === newParent.AssetId
            );

            draftState.splice(
              newParentIndex + 1,
              0,
              ...draggedAssetAndDescendants
            );

            // update the DisplayOrder of the newParent's children
            draftState
              .filter(
                (asset: IAssetResult) =>
                  asset.ParentAssetId === newParent.AssetId
              )
              .forEach((sibling: IAssetResult, index: number) => {
                sibling.DisplayOrder = index + 1;
              });
          }
        ),
      ];

      return { ...state, Assets: newAssets, AssetDataAvailable: '' };
    }
  ),
  on(actions.reParentAssetFailure, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: '',
    };
  }),
  on(actions.reOrderAsset, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: 'loading',
    };
  }),
  on(
    actions.reOrderAssetSuccess,
    (
      state: AssetTabState,
      payload: {
        targetAsset: IAssetResult;
        draggedAsset: IAssetResult;
      }
    ): AssetTabState => {
      const newAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            const draggedAssetIndex: number = draftState.findIndex(
              (asset: IAssetResult) =>
                asset.AssetId === payload.draggedAsset.AssetId
            );

            let draggedAssetAndDescendants: IAssetResult[] = [];
            // get the last index
            const draggedAssetPath: string =
              payload.draggedAsset.Hierarchy.length === 1
                ? `/${payload.draggedAsset.AssetName}`
                : `${payload.draggedAsset.AssetPath}/${payload.draggedAsset.AssetName}`;
            let lastIndex: number = findLastIndex(
              draftState,
              (asset: IAssetResult) =>
                asset.AssetPath.includes(draggedAssetPath)
            );
            // if no descendants, assign draggedAssetIndex to lastIndex
            if (lastIndex < 0) {
              lastIndex = draggedAssetIndex;
            }
            // get the dragged asset + its descendants (if any)
            draggedAssetAndDescendants = draftState.slice(
              draggedAssetIndex,
              lastIndex + 1
            );

            // remove the dragged assets + its descendants (if any)
            draftState.splice(
              draggedAssetIndex,
              lastIndex - draggedAssetIndex + 1
            );

            // get the targetAsset's last descendant index
            const targetAssetPath: string =
              payload.targetAsset.Hierarchy.length === 1
                ? `/${payload.targetAsset.AssetName}`
                : `${payload.targetAsset.AssetPath}/${payload.targetAsset.AssetName}`;
            let lastDescendantIndex: number = findLastIndex(
              draftState,
              (asset: IAssetResult) => asset.AssetPath.includes(targetAssetPath)
            );
            if (lastDescendantIndex < 0) {
              lastDescendantIndex = draftState.findIndex(
                (asset: IAssetResult) =>
                  asset.AssetId === payload.targetAsset.AssetId
              );
            }
            // insert draggedAsset below the targetAsset's last descendant
            draftState.splice(
              lastDescendantIndex + 1,
              0,
              ...draggedAssetAndDescendants
            );

            // update the DisplayOrder of the siblings
            draftState
              .filter(
                (asset: IAssetResult) =>
                  asset.ParentAssetId === payload.draggedAsset.ParentAssetId
              )
              .forEach((sibling: IAssetResult, index: number) => {
                sibling.DisplayOrder = index + 1;
              });
          }
        ),
      ];

      return { ...state, Assets: newAssets, AssetDataAvailable: '' };
    }
  ),
  on(actions.reOrderAssetFailure, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: '',
    };
  }),
  on(
    actions.setFilters,
    (state: AssetTabState, payload: { filters: any[] }): AssetTabState => {
      return {
        ...state,
        AssetFilters: payload.filters,
      };
    }
  ),
  on(actions.clearFilters, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetFilters: [],
    };
  }),
  on(
    actions.removeFilter,
    (
      state: AssetTabState,
      payload: { filter: any; callback: (filterName: string) => void }
    ): AssetTabState => {
      const filters: any[] = [...state.AssetFilters];
      const index: number = filters.indexOf(payload.filter);
      if (index >= 0) {
        filters.splice(index, 1);
        payload.callback(payload.filter.Name);
      }
      return {
        ...state,
        AssetFilters: filters,
      };
    }
  ),
  on(
    actions.updateAssetToCreateOrEdit,
    (
      state: AssetTabState,
      payload: { updatedAssetToCreateOrEdit: IAssetResult | null }
    ): AssetTabState => {
      return {
        ...state,
        AssetToCreateOrEdit: payload.updatedAssetToCreateOrEdit,
      };
    }
  ),
  on(actions.getAssetClassTypes, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetClassTypes: [],
      AssetClassTypeDataAvailable: 'loading',
    };
  }),
  on(
    actions.getAssetClassTypesSuccess,
    (
      state: AssetTabState,
      payload: { assetClassTypes: IAssetClassTypeResult[] }
    ): AssetTabState => {
      // sort by IsStandard (true, then false) then AssetClassTypeKey (asc)
      const assetClassTypes: IAssetClassTypeResult[] = orderBy(
        payload.assetClassTypes,
        ['IsStandard', 'AssetClassTypeKey'],
        ['desc', 'asc']
      );
      return {
        ...state,
        AssetClassTypes: assetClassTypes,
        AssetClassTypeDataAvailable: '',
      };
    }
  ),
  on(
    actions.getAssetClassTypesFailure,
    (state: AssetTabState): AssetTabState => {
      return {
        ...state,
        AssetClassTypeDataAvailable: '',
      };
    }
  ),
  on(
    actions.updateAssetClassType,
    (
      state: AssetTabState,
      payload: { key: string; desc: string }
    ): AssetTabState => {
      let updatedAssetToCreateOrEdit: IAssetResult | null = null;
      if (state.AssetToCreateOrEdit) {
        updatedAssetToCreateOrEdit = {
          ...state.AssetToCreateOrEdit,
          AssetClassTypeKey: payload.key,
          AssetClassTypeDesc: payload.desc,
        };
      }
      return {
        ...state,
        AssetToCreateOrEdit: updatedAssetToCreateOrEdit,
      };
    }
  ),
  on(actions.deleteAsset, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: 'loading',
    };
  }),
  on(
    actions.deleteAssetSuccess,
    (
      state: AssetTabState,
      payload: { assetIdToDelete: string; selectedAssetPath: string }
    ): AssetTabState => {
      const newAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            const assetIdsToRemove: string[] = [payload.assetIdToDelete].concat(
              draftState
                .filter((asset: IAssetResult) =>
                  asset.AssetPath.includes(payload.selectedAssetPath)
                )
                .map((asset: IAssetResult) => asset.AssetId)
            );
            return draftState.filter(
              (asset: IAssetResult) => !assetIdsToRemove.includes(asset.AssetId)
            );
          }
        ),
      ];
      const dataAvailable: DataAvailable = newAssets.length > 0 ? '' : 'nodata';
      return {
        ...state,
        Assets: newAssets,
        AssetDataAvailable: dataAvailable,
      };
    }
  ),
  on(actions.deleteAssetFailure, (state: AssetTabState): AssetTabState => {
    return {
      ...state,
      AssetDataAvailable: '',
    };
  }),
  on(
    actions.insertSingleAsset,
    (
      state: AssetTabState,
      payload: { assetToInsert: IAssetResult }
    ): AssetTabState => {
      const updatedAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            const assetToInsert: IAssetResult = {
              ...payload.assetToInsert,
            };
            populateParentNameAndAssetPath(assetToInsert);
            draftState.push(assetToInsert);
          }
        ),
      ];

      return {
        ...state,
        Assets: orderAssets(updatedAssets),
      };
    }
  ),
  on(
    actions.updateSingleAsset,
    (
      state: AssetTabState,
      payload: { updatedAsset: IAssetResult }
    ): AssetTabState => {
      const updatedAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            const assetToUpdateIdx: number = draftState.findIndex(
              (asset: IAssetResult) =>
                asset.AssetId === payload.updatedAsset.AssetId
            );
            if (assetToUpdateIdx > -1) {
              const updatedAsset: IAssetResult = {
                ...payload.updatedAsset,
              };
              populateParentNameAndAssetPath(updatedAsset);
              draftState[assetToUpdateIdx] = {
                AssetId: updatedAsset.AssetId,
                AssetName: updatedAsset.AssetName,
                AssetDesc: updatedAsset.AssetDesc,
                AssetClassTypeKey: updatedAsset.AssetClassTypeKey,
                AssetClassTypeDesc: updatedAsset.AssetClassTypeDesc,
                ParentName: updatedAsset.ParentName,
                ParentAssetId: updatedAsset.ParentAssetId,
                AssetPath: updatedAsset.AssetPath,
                DisplayOrder: updatedAsset.DisplayOrder,
                IsStandard: updatedAsset.IsStandard,
                CreateDate: updatedAsset.CreateDate,
                ChangeDate: updatedAsset.ChangeDate,
                Hierarchy: [...updatedAsset.Hierarchy],
              };
            }
          }
        ),
      ];

      return {
        ...state,
        Assets: orderAssets(updatedAssets),
      };
    }
  ),
  on(
    actions.insertAssets,
    (
      state: AssetTabState,
      payload: { assets: IAssetResult[] }
    ): AssetTabState => {
      const updatedAssets: IAssetResult[] = [
        ...produce(
          state.Assets,
          (draftState: WritableDraft<IAssetResult[]>) => {
            for (const assetToInsert of payload.assets) {
              const asset: IAssetResult = {
                ...assetToInsert,
              };
              populateParentNameAndAssetPath(asset);
              draftState.push(asset);
            }
          }
        ),
      ];

      return {
        ...state,
        Assets: orderAssets(updatedAssets),
      };
    }
  )
);

function populateParentNameAndAssetPath(asset: IAssetResult) {
  asset.ParentName = '';
  // path syntax: '/{starting asset}/{to}/{parent}'
  // for starting asset: just '/'
  asset.AssetPath = '/';
  if (asset.Hierarchy?.length > 1) {
    asset.ParentName = asset.Hierarchy[asset.Hierarchy.length - 2];
    asset.AssetPath += asset.Hierarchy.slice(0, -1).join('/');
  }
}

function orderAssets(assetsToOrder: IAssetResult[]): IAssetResult[] {
  let orderedAssets: IAssetResult[] = [];
  if (assetsToOrder.length > 0) {
    const finalArr: Set<IAssetResult> = new Set();
    fillAssetsArray(assetsToOrder, finalArr, assetsToOrder[0]);
    orderedAssets = Array.from(finalArr);
  }

  return orderedAssets;
}

function fillAssetsArray(
  givenAssets: IAssetResult[],
  setResults: Set<IAssetResult>,
  asset: IAssetResult
): Set<IAssetResult> {
  setResults.add(asset);
  let children: IAssetResult[] = givenAssets.filter(
    (a: IAssetResult) => a.ParentAssetId === asset.AssetId
  );
  if (children.length > 0) {
    children = orderBy(children, ['DisplayOrder', 'AssetName']);
    for (const child of children) {
      fillAssetsArray(givenAssets, setResults, child);
    }
  }

  return setResults;
}
