import { Injectable } from '@angular/core';
import {
  IAssetClassTypeResult,
  IAssetCreateUpdateResult,
  IAssetResult,
  IAssetUpdate,
} from '@atonix/atx-core';
import { AssetsCoreService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  map,
  catchError,
  switchMap,
  mergeMap,
  withLatestFrom,
  share,
  take,
} from 'rxjs/operators';
import * as actions from '../actions/asset-tab.actions';
import { selectAssetGuid } from '../selectors/asset-config.selectors';
import {
  selectSelectedAssetOnly,
  selectSelectedAssetTreeNodeParent,
} from '../selectors/asset-nav.selectors';
import { selectAssets } from '../selectors/asset-tab.selectors';

@Injectable()
export class AssetTabEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetsCoreService: AssetsCoreService,
    private toastService: ToastService
  ) {}

  getAssets$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAssets),
      concatLatestFrom(() => [
        this.store.select(selectAssetGuid),
        this.store.select(selectSelectedAssetOnly),
      ]),
      switchMap(([, assetId, selectAssetOnly]) => {
        return this.assetsCoreService.getAssets(assetId, !selectAssetOnly).pipe(
          map((assets: IAssetResult[]) => {
            return actions.getAssetsSuccess({ assets });
          }),
          catchError(() => of(actions.getAssetsFailure))
        );
      })
    );
  });

  getAssetClassTypes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAssetClassTypes),
      mergeMap(({ parentAssetClassTypeKey, searchString }) => {
        if (!parentAssetClassTypeKey) {
          return of();
        }
        return this.assetsCoreService
          .getAssetClassTypes(parentAssetClassTypeKey, searchString)
          .pipe(
            map(
              (assetClassTypes: IAssetClassTypeResult[]) => {
                return actions.getAssetClassTypesSuccess({ assetClassTypes });
              },
              catchError(() => of(actions.getAssetClassTypesFailure))
            )
          );
      })
    );
  });

  getAssetClassTypes2$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAssetClassTypes),
      concatLatestFrom(() => [
        this.store.select(selectSelectedAssetTreeNodeParent),
      ]),
      mergeMap(([{ parentAssetClassTypeKey, searchString }, parent]) => {
        if (parentAssetClassTypeKey) {
          return of();
        }
        return this.assetsCoreService
          .getAssets(parent.Asset.GlobalId, false)
          .pipe(
            map((assets) => {
              return { assets, searchString };
            })
          );
      }),
      switchMap(({ assets, searchString }) => {
        return this.assetsCoreService
          .getAssetClassTypes(assets[0].AssetClassTypeKey, searchString)
          .pipe(
            map(
              (assetClassTypes: IAssetClassTypeResult[]) => {
                return actions.getAssetClassTypesSuccess({ assetClassTypes });
              },
              catchError(() => of(actions.getAssetClassTypesFailure))
            )
          );
      })
    );
  });

  updateAssets$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.updateAssets),
      concatLatestFrom(() => this.store.select(selectAssetGuid)),
      mergeMap(([{ assets, updateAssetType }, assetId]) =>
        this.assetsCoreService.updateAssets(assetId, assets).pipe(
          map((results: IAssetCreateUpdateResult[]) => {
            if (updateAssetType === 'single' && results.length === 1) {
              const error: string = results[0].Error;
              if (error) {
                this.toastService.openSnackBar(error, 'error');
                return actions.updateAssetsFailure(new Error(error));
              }
            }
            return actions.updateAssetsSuccess({ assets: results });
          }),
          catchError(() => of(actions.updateAssetsFailure))
        )
      )
    );
  });

  deleteAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.deleteAsset),
      mergeMap(({ assetIdToDelete, selectedAssetPath }) =>
        this.assetsCoreService.deleteAsset([assetIdToDelete]).pipe(
          map((result: string) =>
            result.includes('Failed to delete')
              ? actions.deleteAssetFailure(new Error(result))
              : actions.deleteAssetSuccess({
                  assetIdToDelete,
                  selectedAssetPath,
                })
          ),
          catchError(() => of(actions.deleteAssetFailure))
        )
      )
    );
  });

  reOrderAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.reOrderAsset),
      concatLatestFrom(() => [
        this.store.select(selectAssetGuid),
        this.store.select(selectAssets),
      ]),
      mergeMap(([{ targetAsset, draggedAsset }, assetId, assets]) => {
        const siblings: IAssetResult[] = assets.filter(
          (asset: IAssetResult) =>
            asset.AssetId !== draggedAsset.AssetId &&
            asset.ParentAssetId === draggedAsset.ParentAssetId
        );
        const targetIndex = siblings.findIndex(
          (sibling: IAssetResult) => sibling.AssetId === targetAsset.AssetId
        );
        siblings.splice(targetIndex + 1, 0, draggedAsset);
        const assetsToUpdate: IAssetUpdate[] = siblings.map(
          (sibling: IAssetResult, index: number) =>
            ({
              AssetId: sibling.AssetId,
              ParentAssetId: sibling.ParentAssetId,
              AssetName: sibling.AssetName,
              AssetDesc: sibling.AssetDesc,
              AssetClassTypeKey: sibling.AssetClassTypeKey,
              DisplayOrder: index + 1,
            } as IAssetUpdate)
        );

        return this.assetsCoreService
          .updateAssets(assetId, assetsToUpdate)
          .pipe(
            map((reOrderedAssets: IAssetCreateUpdateResult[]) => {
              let error: IAssetCreateUpdateResult | undefined = undefined;
              if (reOrderedAssets.length > 0) {
                error = reOrderedAssets.find(
                  (asset: IAssetCreateUpdateResult) => asset.Error
                );
                if (!error) {
                  return actions.reOrderAssetSuccess({
                    targetAsset,
                    draggedAsset,
                  });
                }
              }
              return actions.reParentAssetFailure(
                new Error(error?.Error ?? '')
              );
            }),
            catchError(() => of(actions.reOrderAssetFailure))
          );
      })
    );
  });

  reParentAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.reParentAsset),
      concatLatestFrom(() => [
        this.store.select(selectAssetGuid),
        this.store.select(selectAssets),
      ]),
      mergeMap(([{ newParent, draggedAsset }, assetId, assets]) => {
        const siblings: IAssetUpdate[] = assets
          .filter(
            (asset: IAssetResult) => asset.ParentAssetId === newParent.AssetId
          )
          .map((asset: IAssetResult, index: number) => ({
            ...asset,
            DisplayOrder: index + 2,
          }));
        const assetsToUpdate: IAssetUpdate[] = [
          {
            AssetId: draggedAsset.AssetId,
            ParentAssetId: newParent.AssetId,
            AssetName: draggedAsset.AssetName,
            AssetDesc: draggedAsset.AssetDesc,
            AssetClassTypeKey: draggedAsset.AssetClassTypeKey,
            DisplayOrder: 1,
          } as IAssetUpdate,
          ...siblings,
        ];

        return this.assetsCoreService
          .updateAssets(assetId, assetsToUpdate)
          .pipe(
            map((assets: IAssetCreateUpdateResult[]) => {
              let error: IAssetCreateUpdateResult | undefined = undefined;
              if (assets.length > 0) {
                error = assets.find(
                  (asset: IAssetCreateUpdateResult) => asset.Error
                );
                if (!error) {
                  return actions.reParentAssetSuccess({
                    newParent,
                    draggedAsset,
                  });
                }
              }
              return actions.reParentAssetFailure(
                new Error(error?.Error ?? '')
              );
            }),
            catchError(() => of(actions.reParentAssetFailure))
          );
      })
    );
  });
}
