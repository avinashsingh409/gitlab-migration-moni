/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModelService, selectAsset } from '@atonix/atx-asset-tree';
import {
  NavActions,
  selectNavAsset,
  updateRouterIdWithoutReloading,
} from '@atonix/atx-navigation';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import {
  catchError,
  filter,
  map,
  mergeMap,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  selectAssetTreeDropdownState,
  selectAssetTreeState,
  selectCurrentTab,
  selectSelectedAsset,
} from '../selectors/asset-nav.selectors';
import * as actions from '../actions/asset-config.actions';
import { of } from 'rxjs';
import { ToastService } from '@atonix/shared/utils';
import { getAssets } from '../actions/asset-tab.actions';
import { assetTabFeatureKey } from '../reducers/asset-tab.reducer';
import { getAttributes } from '../actions/attributes-tab.actions';
import { getAttachments } from '../actions/attachments-tab.actions';
import { attributesTabFeatureKey } from '../reducers/attributes-tab.reducer';
import { attachmentsTabFeatureKey } from '../reducers/attachments-tab.reducer';

@Injectable()
export class AssetConfigEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetTreeModelService: ModelService,
    private activatedRoute: ActivatedRoute,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private router: Router,
    private toastService: ToastService
  ) {}

  initializeAssetConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.systemInitialize),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(selectNavAsset)
      ),
      map(([action, newAsset, appAsset]) => {
        return action.asset || appAsset || newAsset;
      }),
      // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
      switchMap((asset) => [
        actions.treeStateChange(selectAsset(asset, false)),
        NavActions.taskCenterLoad({
          taskCenterID: 'assetConfig',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: false,
          asset,
        }),
      ])
    );
  });

  getPermissions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) => actions.permissionsRequestSuccess(permissions)),
          catchError((error) => of(actions.permissionsRequestFailure(error)))
        )
      )
    );
  });

  treeStateChange$ = createEffect(() => {
    // eslint-disable-next-line ngrx/avoid-cyclic-effects
    return this.actions$.pipe(
      ofType(actions.treeStateChange),
      concatLatestFrom(() => this.store.select(selectAssetTreeState)),
      mergeMap(([change, state]) =>
        this.assetTreeModelService
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => actions.treeStateChange(n),
              catchError((error) =>
                of(actions.assetTreeDataRequestFailure(error))
              )
            )
          )
      )
    );
  });

  assetTreeDropdownStateChange$ = createEffect(() => {
    // eslint-disable-next-line ngrx/avoid-cyclic-effects
    return this.actions$.pipe(
      ofType(actions.assetTreeDropdownStateChange),
      concatLatestFrom(() => this.store.select(selectAssetTreeDropdownState)),
      switchMap(([change, state]) =>
        this.assetTreeModelService
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.assetTreeDropdownStateChange(n)),
            catchError((error) =>
              of(actions.assetTreeDropdownDataRequestFailure(error))
            )
          )
      )
    );
  });

  selectAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.selectAsset),
      switchMap((action) => {
        // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          NavActions.updateNavigationItems({
            urlParams: { id: action.asset },
          }),
        ];
      })
    );
  });

  reflow$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 400);
        })
      );
    },
    { dispatch: false }
  );

  updateRoute$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.updateRoute),
        tap((action) => {
          setTimeout(() => {
            updateRouterIdWithoutReloading(
              action.asset,
              this.activatedRoute,
              this.router
            );
          }, 500);
        })
      );
    },
    { dispatch: false }
  );

  showInfoMessage$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.showInfoMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'information');
        })
      );
    },
    { dispatch: false }
  );

  showSuccessMessage$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.showSuccessMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'success');
        })
      );
    },
    { dispatch: false }
  );

  showWarningMessage$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.showWarningMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'warning');
        })
      );
    },
    { dispatch: false }
  );

  showErrorMessage$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.showErrorMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'error');
        })
      );
    },
    { dispatch: false }
  );

  toggleSelectedAssetOnly$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.toggleSelectedAssetOnly),
      concatLatestFrom(() => this.store.select(selectCurrentTab)),
      filter(([, currentTab]) =>
        [
          assetTabFeatureKey,
          attributesTabFeatureKey,
          attachmentsTabFeatureKey,
        ].includes(currentTab)
      ),
      map(([, currentTab]) => {
        if (currentTab === assetTabFeatureKey) {
          return getAssets();
        } else if (currentTab === attributesTabFeatureKey) {
          return getAttributes();
        } else {
          return getAttachments();
        }
      })
    );
  });
}
