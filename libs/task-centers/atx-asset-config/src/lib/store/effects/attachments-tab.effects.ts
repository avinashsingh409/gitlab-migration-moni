/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import { IAttachment } from '../../models/attachment-type';
import { AssetsCoreService, ImagesFrameworkService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import * as actions from '../actions/attachments-tab.actions';
import { selectAssetGuid } from '../selectors/asset-config.selectors';
import { AttachmentsTabService } from '../../service/attachments-tab.service';
import { of } from 'rxjs';

@Injectable()
export class AttachmentsTabEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetsCoreService: AssetsCoreService,
    private imagesFrameworkService: ImagesFrameworkService,
    private toastService: ToastService,
    private attachmentsTabService: AttachmentsTabService
  ) {}

  getAttachments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAttachments),
      concatLatestFrom(() => [this.store.select(selectAssetGuid)]),
      switchMap(([, assetId]) => {
        return this.assetsCoreService.getAttachments(assetId).pipe(
          map((untypedAttachments: any[]) => {
            const attachments: IAttachment[] =
              this.attachmentsTabService.attachmentsFromAnys(
                untypedAttachments
              );
            return actions.getAttachmentsSuccess({ attachments });
          }),
          catchError(() => of(actions.getAttachmentsFailure))
        );
      })
    );
  });

  upsertAttachment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.upsertAttachment),
      concatLatestFrom(() => this.store.select(selectAssetGuid)),
      mergeMap(([{ attachmentToUpsert, file, action }, assetId]) => {
        return this.assetsCoreService
          .upsertAttachment(assetId, attachmentToUpsert)
          .pipe(
            map((untypedUpsertResult: any) => {
              const typedUpsertResult: IAttachment =
                this.attachmentsTabService.attachmentsFromAnys([
                  untypedUpsertResult,
                ])[0];
              const error: string = untypedUpsertResult.Error;
              if (error) {
                this.toastService.openSnackBar(
                  `Not able to ${action}.`,
                  'error'
                );
                return actions.upsertAttachmentFailure(new Error(error));
              }
              if (
                (action === 'create attachment' ||
                  action === 'update attachment file') &&
                typedUpsertResult.TemporaryLink
              ) {
                typedUpsertResult.TemporaryLink = decodeURIComponent(
                  typedUpsertResult.TemporaryLink
                );
                this.toastService.openSnackBar(
                  'Uploading file... this may take a moment.',
                  'warning'
                );
                return actions.uploadAttachment({
                  createdAttachment: typedUpsertResult,
                  file,
                  action,
                });
              } else {
                return actions.upsertAttachmentSuccess({
                  upsertResult: typedUpsertResult,
                  action,
                });
              }
            }),
            catchError((error) => {
              this.toastService.openSnackBar(`Not able to ${action}.`, 'error');
              return of(actions.upsertAttachmentFailure(error));
            })
          );
      }),
      catchError((error) => {
        this.toastService.openSnackBar(
          'Not able to create or update attachment',
          'error'
        );
        return of(actions.upsertAttachmentFailure(error));
      })
    );
  });

  uploadAttachment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.uploadAttachment),
      mergeMap(({ createdAttachment, file, action }) => {
        if (createdAttachment.TemporaryLink && file) {
          return this.imagesFrameworkService
            .uploadFile(createdAttachment.TemporaryLink, file, false)
            .pipe(
              map((result: boolean) => {
                const uploadedAttachment: IAttachment = {
                  ...createdAttachment,
                  Upload: undefined,
                  TemporaryLink: undefined,
                  LinkExpirationDate: undefined,
                };
                this.toastService.openSnackBar('Upload complete.', 'success');
                return actions.upsertAttachmentSuccess({
                  upsertResult: uploadedAttachment,
                  action,
                });
              }),
              catchError((error) => {
                this.toastService.openSnackBar(
                  `Not able to ${action}.`,
                  'error'
                );
                return of(actions.upsertAttachmentFailure(error));
              })
            );
        } else {
          this.toastService.openSnackBar(`Not able to ${action}.`, 'error');
          return of(
            actions.upsertAttachmentFailure(new Error(`Not able to ${action}.`))
          );
        }
      }),
      catchError((error) => {
        this.toastService.openSnackBar(
          'Not able to upload attachment.',
          'error'
        );
        return of(actions.upsertAttachmentFailure(error));
      })
    );
  });

  deleteAttachments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.deleteAttachments),
      concatLatestFrom(() => this.store.select(selectAssetGuid)),
      mergeMap(([{ attachmentsToDelete }, assetId]) => {
        const attachmentIds: string[] = attachmentsToDelete.map(
          (attachmentToDelete: IAttachment) => attachmentToDelete.Id
        );
        return this.assetsCoreService
          .deleteAttachments(assetId, attachmentIds)
          .pipe(
            map((deleteResponse: string) => {
              if (deleteResponse === 'Attachment(s) was deleted successfully') {
                this.toastService.openSnackBar(deleteResponse, 'success');
                return actions.deleteAttachmentsSuccess({
                  deletedAttachments: attachmentIds,
                });
              } else {
                const error = 'Not able to delete asset attachment(s)';
                this.toastService.openSnackBar(error, 'error');
                return actions.deleteAttachmentsFailure(new Error(error));
              }
            }),
            catchError((error) => of(actions.deleteAttachmentsFailure(error)))
          );
      }),
      catchError((error) => of(actions.deleteAttachmentsFailure(error)))
    );
  });
}
