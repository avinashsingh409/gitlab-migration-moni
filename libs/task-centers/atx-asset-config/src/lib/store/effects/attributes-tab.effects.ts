/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import {
  IAttribute,
  IAttributeUpsertResult,
} from '../../models/attribute-type';
import { AssetsCoreService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, catchError, switchMap, mergeMap } from 'rxjs/operators';
import * as actions from '../actions/attributes-tab.actions';
import { selectAssetGuid } from '../selectors/asset-config.selectors';
import { selectSelectedAssetOnly } from '../selectors/asset-nav.selectors';
import { AttributesTabService } from '../../service/attributes-tab.service';

@Injectable()
export class AttributesTabEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetsCoreService: AssetsCoreService,
    private toastService: ToastService,
    private attributesTabService: AttributesTabService
  ) {}

  getAttributes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getAttributes),
      concatLatestFrom(() => [
        this.store.select(selectAssetGuid),
        this.store.select(selectSelectedAssetOnly),
      ]),
      switchMap(([, assetId, selectAssetOnly]) => {
        return this.assetsCoreService
          .getAttributes(assetId, !selectAssetOnly)
          .pipe(
            map((untypedAttributes: any[]) => {
              const attributes: IAttribute[] =
                this.attributesTabService.attributesFromAnys(untypedAttributes);
              return actions.getAttributesSuccess({ attributes });
            }),
            catchError(() => of(actions.getAttributesFailure))
          );
      })
    );
  });

  updateAttributes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.updateAttributes),
      concatLatestFrom(() => this.store.select(selectAssetGuid)),
      mergeMap(([{ attributesToUpdate }, assetId]) =>
        this.assetsCoreService
          .upsertAttributes(assetId, attributesToUpdate)
          .pipe(
            map((untypedUpdateResults: any[]) => {
              const typedUpdateResults: IAttributeUpsertResult[] =
                this.attributesTabService.attributeUpsertResultsFromAnys(
                  untypedUpdateResults
                );
              if (typedUpdateResults.length === 1) {
                const error: string = untypedUpdateResults[0].Error;
                if (error) {
                  this.toastService.openSnackBar(error, 'error');
                  return actions.updateAttributesFailure(new Error(error));
                }
              }
              return actions.updateAttributesSuccess({
                updateResults: typedUpdateResults,
              });
            }),
            catchError((error) => {
              this.toastService.openSnackBar(
                'Not able to update attribute(s)',
                'error'
              );
              return of(actions.updateAttributesFailure(error));
            })
          )
      ),
      catchError((error) => {
        this.toastService.openSnackBar(
          'Not able to update attribute(s)',
          'error'
        );
        return of(actions.updateAttributesFailure(error));
      })
    );
  });

  deleteAttributes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.deleteAttributes),
      concatLatestFrom(() => this.store.select(selectAssetGuid)),
      mergeMap(([{ attributesToDelete }, assetId]) => {
        const attributePks: number[] = [];
        attributesToDelete.forEach((a: IAttribute) => {
          if (a.Id) {
            attributePks.push(a.Id);
          }
        });
        return this.assetsCoreService
          .deleteAttributes(assetId, attributePks)
          .pipe(
            map((deleteResponse: string) => {
              if (deleteResponse === 'Attribute(s) was deleted successfully') {
                this.toastService.openSnackBar(deleteResponse, 'success');
                return actions.deleteAttributesSuccess({
                  deletedAttributes: attributePks,
                });
              } else {
                const error = 'Not able to delete asset attribute(s)';
                this.toastService.openSnackBar(error, 'error');
                return actions.deleteAttributesFailure(new Error(error));
              }
            }),
            catchError((error) => of(actions.deleteAttributesFailure(error)))
          );
      }),
      catchError((error) => of(actions.deleteAttributesFailure(error)))
    );
  });
}
