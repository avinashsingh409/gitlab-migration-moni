/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
import { Injectable } from '@angular/core';
import {
  ITagMapBare,
  IServerResult,
  IPDTagMap,
  ITagMapCreateUpdateResult,
  ITagMapAuditResult,
  ITagResult,
  ITagCreateUpdateResult,
  isNil,
} from '@atonix/atx-core';
import { ProcessDataCoreService } from '@atonix/shared/api';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import { groupBy } from 'lodash';
import { forkJoin, of } from 'rxjs';
import { map, switchMap, catchError, mergeMap } from 'rxjs/operators';
import * as actions from '../actions/tags-tab.actions';
import { selectAssetGuid } from '../selectors/asset-config.selectors';
import * as selectors from '../selectors/tags-tab.selectors';
import {
  selectCurrentTab,
  selectSelectedAssetOnly,
} from '../selectors/asset-nav.selectors';
import { toggleSelectedAssetOnly } from '../actions/asset-config.actions';
import { tagsTabFeatureKey } from '../reducers/tags-tab.reducer';

@Injectable()
export class TagsTabEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private processDataCoreService: ProcessDataCoreService
  ) {}

  routeLoadTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.routeLoadTags),
      concatLatestFrom(() => [
        this.store.select(selectAssetGuid),
        this.store.select(selectors.selectSelectedServer),
      ]),
      switchMap(([, asset, selectedServer]) =>
        this.processDataCoreService.getServers(asset).pipe(
          map(
            (servers: IServerResult[]) => {
              if (selectedServer != null && servers.length > 0) {
                const index: number = servers.findIndex(
                  (s: IServerResult) => s.Id === selectedServer?.Id
                );
                if (index > -1) {
                  return actions.routeLoadTagsSuccess({
                    asset,
                    servers,
                    init: false,
                  });
                }
              }
              return servers.length > 0
                ? actions.getTags({ servers, server: servers[0] })
                : actions.routeLoadTagsSuccess({ asset, servers, init: true });
            },
            catchError(() => of(actions.routeLoadTagsFailure({ asset: '' })))
          )
        )
      )
    );
  });

  loadTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getTags),
      map((payload: { server: IServerResult }) => payload.server),
      switchMap((server: IServerResult) =>
        this.processDataCoreService.getTagsForServer(server.Id).pipe(
          map(
            (tags: ITagResult[]) => {
              const modifiedTags = tags.map((tag: ITagResult) => ({
                Id: tag.Id,
                Name: tag.Name.replace(/(\r\n|\n|\r)/gm, ''),
                Description: tag.Description.replace(/(\r\n|\n|\r)/gm, ''),
                EngUnit: tag.EngUnit.replace(/(\r\n|\n|\r)/gm, ''),
                Qualifier: tag.Qualifier.replace(/(\r\n|\n|\r)/gm, ''),
                Source: tag.Source.replace(/(\r\n|\n|\r)/gm, ''),
                CreateDate: tag.CreateDate,
                ChangeDate: tag.ChangeDate,
                ExistsOnServer: tag.ExistsOnServer,
                InUse: tag.InUse,
                IsDirty: false,
              }));
              return actions.getTagsSuccess({ tags: modifiedTags, server });
            },
            catchError((error) => of(actions.getTagsFailure(error)))
          )
        )
      )
    );
  });

  refreshTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.refreshTags),
      concatLatestFrom(() => this.store.select(selectors.selectSelectedServer)),
      switchMap(([, server]) => {
        if (server != null) {
          return this.processDataCoreService.getTagsForServer(server.Id).pipe(
            map(
              (tags: ITagResult[]) => actions.getTagsSuccess({ tags, server }),
              catchError((error) => of(actions.getTagsFailure(error)))
            )
          );
        }
        return [];
      })
    );
  });

  loadTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        toggleSelectedAssetOnly,
        actions.routeLoadTagsSuccess,
        actions.getTagsSuccess,
        actions.discardChangesSuccess
      ),
      concatLatestFrom(() => [
        this.store.select(selectSelectedAssetOnly),
        this.store.select(selectors.selectSelectedServer),
        this.store.select(selectAssetGuid),
        this.store.select(selectCurrentTab),
      ]),
      switchMap(([payload, selectedAssetOnly, server, assetId, currentTab]) =>
        (payload.type === '[Asset Config - Tags Tab] Route Load Tags Success' &&
          payload.init) ||
        currentTab !== tagsTabFeatureKey
          ? []
          : [
              actions.getTagMaps({
                server,
                assetId,
                includeDescendants: !selectedAssetOnly,
              }),
            ]
      )
    );
  });

  refreshTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.refreshTagMaps),
      concatLatestFrom(() => [
        this.store.select(selectSelectedAssetOnly),
        this.store.select(selectors.selectSelectedServer),
        this.store.select(selectAssetGuid),
      ]),
      switchMap(([, selectedAssetOnly, server, assetId]) => [
        actions.getTagMaps({
          server,
          assetId,
          includeDescendants: !selectedAssetOnly,
        }),
      ])
    );
  });

  getTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getTagMaps),
      switchMap(({ server, assetId, includeDescendants }) =>
        this.processDataCoreService
          .getTagMaps(assetId, server?.Id ?? '', includeDescendants)
          .pipe(
            map(
              (tagMaps: IPDTagMap[]) => actions.getTagMapsSuccess({ tagMaps }),
              catchError((error) => of(actions.getTagMapsFailure(error)))
            )
          )
      )
    );
  });

  saveTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.associateTagsToTagMaps),
      concatLatestFrom(() => [
        this.store.select(selectors.selectTagMapsDict),
        this.store.select(selectors.selectSelectedServer),
      ]),
      switchMap(([{ tags, selectedTagMap }, tagMapsDict, server]) => {
        const toSave: ITagMapBare[] = [];

        tags.forEach((tag: ITagResult) => {
          if (
            // tag doesn't exist in the TagMap yet
            !tagMapsDict[
              `${selectedTagMap.AssetPath}_${selectedTagMap.VariableDesc}`
            ]?.find((tagMap: IPDTagMap) => tagMap.TagId === tag.Id)
          ) {
            toSave.push({
              AssetId: selectedTagMap.AssetId,
              TagId: tag.Id,
              VariableDesc: selectedTagMap.VariableDesc,
              // On create, default ValueTypeKey='Measured' for mapped tagmaps and ValueTypeKey=null for adhoc tagmaps.
              ValueTypeKey:
                selectedTagMap.VariableDesc === '' ? '' : 'Measured',
            } as ITagMapBare);
          }
        });

        if (toSave.length > 0) {
          return this.processDataCoreService.saveTagMaps(toSave).pipe(
            map(
              (saved: ITagMapCreateUpdateResult[]) => {
                // reconcile saved and toSave by outputting the new/saved tagMaps
                const tagDict: Dictionary<ITagResult[]> = groupBy(
                  tags,
                  (tag: ITagResult) => tag.Id
                );

                const savedTagMaps: IPDTagMap[] = [];
                saved.forEach((mapping: ITagMapCreateUpdateResult) => {
                  if (mapping.Error === '') {
                    savedTagMaps.push({
                      FullPath: selectedTagMap.FullPath,
                      AssetId: mapping.AssetId,
                      AssetName: selectedTagMap.AssetName,
                      AssetPath: selectedTagMap.AssetPath,
                      AssetClassTypeKey: selectedTagMap.AssetClassTypeKey,
                      VariableDesc: mapping.VariableDesc,
                      VariableTypeUnits: selectedTagMap.VariableTypeUnits,
                      AssetVariableTypeTagMapId:
                        mapping.AssetVariableTypeTagMapId,
                      PDServerName: server?.Name,
                      TagId: mapping.TagId,
                      TagName: tagDict[mapping.TagId]?.[0].Name ?? '',
                      TagDescription:
                        tagDict[mapping.TagId]?.[0].Description ?? '',
                      TagEngUnit: tagDict[mapping.TagId]?.[0].EngUnit ?? '',
                      ValueTypeKey: mapping.ValueTypeKey,
                      ExistsOnServer:
                        tagDict[mapping.TagId]?.[0].ExistsOnServer ?? false,
                      Hierarchy: [],
                    } as IPDTagMap);
                  }
                });

                return actions.associateTagsToTagMapsSuccess({
                  savedTagMaps,
                  selectedTagMap,
                });
              },
              catchError((error) =>
                of(actions.associateTagsToTagMapsFailure(error))
              )
            )
          );
        } else {
          // all dragged tags have been mapped already
          return of(actions.associateTagsToTagMapsFailure(new Error()));
        }
      })
    );
  });

  updateTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.updateTagMaps),
      concatLatestFrom(() => this.store.select(selectors.selectTagMapsDict)),
      mergeMap(([{ selectedTagMap, draggedTagMaps }, tagMapsDict]) => {
        // remove dragged tagMaps that are:
        // 1) placeholders
        // 2) dragged to its same AssetPath_VariableDesc
        // 3) existing such that the draggedTagMap's TagID exists in the target's AssetPath_VariableDesc
        const tagMapsToUpdate: IPDTagMap[] = draggedTagMaps.filter(
          (draggedTagMap: IPDTagMap) =>
            draggedTagMap.TagName !== '' &&
            `${draggedTagMap.AssetPath}_${draggedTagMap.VariableDesc}` !==
              `${selectedTagMap.AssetPath}_${selectedTagMap.VariableDesc}` &&
            !tagMapsDict[
              `${selectedTagMap.AssetPath}_${selectedTagMap.VariableDesc}`
            ]?.find(
              (tagMap: IPDTagMap) =>
                tagMap.TagId !== '' && tagMap.TagId === draggedTagMap.TagId
            )
        );
        const toUpdate: ITagMapBare[] = [];
        tagMapsToUpdate.forEach((tagMap: IPDTagMap) => {
          toUpdate.push({
            AssetVariableTypeTagMapId: tagMap.AssetVariableTypeTagMapId,
            AssetId: selectedTagMap.AssetId,
            TagId: tagMap.TagId,
            VariableDesc: selectedTagMap.VariableDesc,
            // On update, default ValueTypeKey='Measured' for mapped tagmaps and ValueTypeKey=null for adhoc tagmaps.
            ValueTypeKey: selectedTagMap.VariableDesc === '' ? '' : 'Measured',
          } as ITagMapBare);
        });

        if (toUpdate.length > 0) {
          return this.processDataCoreService.updateTagMaps(toUpdate).pipe(
            map(
              (updated: ITagMapCreateUpdateResult[]) => {
                const tagMapDict: Dictionary<IPDTagMap[]> = groupBy(
                  tagMapsToUpdate,
                  (tag: IPDTagMap) => tag.TagId
                );

                const updatedTagMaps: IPDTagMap[] = [];
                updated.forEach((mapping: ITagMapCreateUpdateResult) => {
                  if (mapping.Error === '') {
                    updatedTagMaps.push({
                      FullPath: selectedTagMap.FullPath,
                      AssetId: mapping.AssetId,
                      AssetName: selectedTagMap.AssetName,
                      AssetPath: selectedTagMap.AssetPath,
                      AssetClassTypeKey: selectedTagMap.AssetClassTypeKey,
                      VariableDesc: mapping.VariableDesc,
                      VariableTypeUnits: selectedTagMap.VariableTypeUnits,
                      AssetVariableTypeTagMapId:
                        mapping.AssetVariableTypeTagMapId,
                      PDServerName: selectedTagMap.PDServerName,
                      TagId: mapping.TagId,
                      TagName: tagMapDict[mapping.TagId]?.[0].TagName ?? '',
                      TagDescription:
                        tagMapDict[mapping.TagId]?.[0].TagDescription ?? '',
                      TagEngUnit:
                        tagMapDict[mapping.TagId]?.[0].TagEngUnit ?? '',
                      ValueTypeKey: mapping.ValueTypeKey,
                      ExistsOnServer:
                        tagMapDict[mapping.TagId]?.[0].ExistsOnServer ?? false,
                      Hierarchy: [],
                    } as IPDTagMap);
                  }
                });

                return actions.updateTagMapsSuccess({
                  filteredTagMaps: tagMapsToUpdate,
                  updatedTagMaps,
                  selectedTagMap,
                });
              },
              catchError((error) => of(actions.updateTagMapsFailure(error)))
            )
          );
        } else {
          return of(actions.updateTagMapsFailure(new Error()));
        }
      })
    );
  });

  updateValueType$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.updateValueTypeOfTagMap),
      mergeMap(({ selectedTagMap, newValueType }) => {
        const tagMapToUpdate: ITagMapBare = {
          AssetVariableTypeTagMapId: selectedTagMap.AssetVariableTypeTagMapId,
          AssetId: selectedTagMap.AssetId,
          TagId: selectedTagMap.TagId,
          ValueTypeKey: newValueType,
          VariableDesc: selectedTagMap.VariableDesc,
        };
        return this.processDataCoreService.updateTagMaps([tagMapToUpdate]).pipe(
          map(
            (updated: ITagMapCreateUpdateResult[]) => {
              if (!updated[0].Error) {
                return actions.updateValueTypeOfTagMapSuccess({
                  selectedTagMap,
                  newValueType,
                });
              } else {
                return actions.updateValueTypeOfTagMapFailure(new Error());
              }
            },
            catchError((error) =>
              of(actions.updateValueTypeOfTagMapFailure(error))
            )
          )
        );
      })
    );
  });

  discardTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.discardChanges),
      concatLatestFrom(() => this.store.select(selectors.selectSelectedServer)),
      switchMap(([, server]) =>
        this.processDataCoreService.getTagsForServer(server?.Id ?? '').pipe(
          map(
            (tags: ITagResult[]) =>
              actions.discardChangesSuccess({ tags, server }),
            catchError((error) => of(actions.discardChangesFailure(error)))
          )
        )
      )
    );
  });

  auditTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.auditTagMaps),
      mergeMap(({ tagMapIds, callback }) =>
        this.processDataCoreService.auditTagMaps(tagMapIds).pipe(
          map(
            (auditResult: ITagMapAuditResult[]) =>
              actions.auditTagMapsSuccess({ auditResult, callback }),
            catchError((error) => of(actions.auditTagMapsFailure(error)))
          )
        )
      )
    );
  });

  deleteTagMaps$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.deleteTagMaps),
      mergeMap(({ tagMapIds }) =>
        this.processDataCoreService.deleteTagMaps(tagMapIds).pipe(
          map(
            () => actions.deleteTagMapsSuccess({ tagMapIds }),
            catchError((error) => of(actions.deleteTagMapsFailure(error)))
          )
        )
      )
    );
  });

  saveTags$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.saveTags),
      concatLatestFrom(() => [
        this.store.select(selectors.selectSelectedServer),
        this.store.select(selectors.selectAllTags),
      ]),
      switchMap(([, server, tags]) =>
        forkJoin({
          creates: this.processDataCoreService.createTags(
            server?.Id ?? '',
            tags.filter((tag: ITagResult) => {
              return (
                !isNil(tag.Id) && tag.Id.startsWith('-') && tag.IsDirty === true
              );
            })
          ),
          updates: this.processDataCoreService.updateTags(
            server?.Id ?? '',
            tags.filter((tag: ITagResult) => {
              return (
                !isNil(tag.Id) &&
                !tag.Id.startsWith('-') &&
                tag.IsDirty === true
              );
            })
          ),
        }).pipe(
          map((result) => {
            const upserts: ITagCreateUpdateResult[] = result.creates.concat(
              result.updates
            );
            const failures: ITagCreateUpdateResult[] = upserts.filter(
              (upsert: ITagCreateUpdateResult) => upsert.Error !== ''
            );
            if (failures.length > 0) {
              const successful: ITagResult[] = upserts.filter(
                (upsert: ITagCreateUpdateResult) =>
                  upsert.Error === '' || upsert.Error == null
              );
              return actions.saveTagsFailure({
                error: new Error(
                  failures.map((f: ITagCreateUpdateResult) => f.Error).join()
                ),
                successful: successful.length > 0 ? successful : null,
              });
            } else {
              return actions.saveTagsSuccess({ result: upserts, server });
            }
          }),
          catchError((error) => of(actions.saveTagsFailure(error)))
        )
      )
    );
  });
}
