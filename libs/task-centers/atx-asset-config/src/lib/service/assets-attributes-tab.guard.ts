/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { AuthActions, AuthFacade } from '@atonix/shared/state/auth';
import { processResourceAccessType } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import { isNil } from 'lodash';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AssetsAttributesGuard implements CanActivate {
  constructor(
    private router: Router,
    public service: AuthorizationFrameworkService,
    public authFacade: AuthFacade,
    public store: Store<any>,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAccessinApi(): Observable<boolean> {
    return this.service.getAllUIAccess().pipe(
      take(1),
      map(
        (resourceAccessTypes) =>
          AuthActions.getAllUIAccessSuccess({ resourceAccessTypes }),
        catchError(() => {
          return of(false);
        })
      ),
      tap((action) => this.store.dispatch(action)),
      map((val) => {
        const assetconfigAccess = processResourceAccessType(
          'UI/Utility/AssetConfig',
          val.resourceAccessTypes
        );

        return assetconfigAccess?.CanView;
      }),
      catchError(() => {
        return of(false);
      })
    );
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authFacade.navItemsAccess$.pipe(
      withLatestFrom(this.authFacade.isLoggedIn$),
      switchMap(([access, isLoggedIn]) => {
        if (!isLoggedIn) {
          this.authFacade.authInit('', '');
        }
        if (!isNil(access)) {
          const assetConfigAttributesAccess: boolean =
            access.assetConfigAccess?.CanView;
          if (!assetConfigAttributesAccess) {
            if (access.assetConfigAccess?.CanView) {
              this.router.navigate(['asset-config/assets']);
            } else if (access.tagConfigAccess?.CanView) {
              this.router.navigate(['asset-config/tags']);
            } else {
              this.router.navigate(['']);
            }
          }
          return of(assetConfigAttributesAccess);
        } else {
          return this.getAccessinApi();
        }
      })
    );
  }
}
