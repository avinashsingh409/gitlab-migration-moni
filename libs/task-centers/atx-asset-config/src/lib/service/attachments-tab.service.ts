/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { IAttachment } from '../models/attachment-type';

@Injectable({
  providedIn: 'root',
})
export class AttachmentsTabService {
  constructor() {}

  private attachmentFromAny(untypedAttachment: any): IAttachment {
    const typedAttachment: IAttachment = {
      Id: untypedAttachment.Id,
      Filename: untypedAttachment.Filename,
      Caption: untypedAttachment.Caption,
      ChangeDate: untypedAttachment.ChangeDate,
      ChangedBy: untypedAttachment.ChangedBy,
      IsFavorite: untypedAttachment.IsFavorite,
      DisplayOrder: untypedAttachment.DisplayOrder,
      ContentId: untypedAttachment.ContentId,
      AssetName: untypedAttachment.AssetName,
      Type: untypedAttachment.Type,
      TemporaryLink: untypedAttachment.TemporaryLink,
      LinkExpirationDate: untypedAttachment.LinkExpirationDate,
    };
    return typedAttachment;
  }

  attachmentsFromAnys(untypedAttachments: any[]): IAttachment[] {
    const typedAttachments: IAttachment[] = untypedAttachments.map(
      (untypedAttachment: any) => this.attachmentFromAny(untypedAttachment)
    );
    return typedAttachments;
  }
}
