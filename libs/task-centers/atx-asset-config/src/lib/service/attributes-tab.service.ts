/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Injectable } from '@angular/core';
import { IAttribute, IAttributeUpsertResult } from '../models/attribute-type';
import moment from 'moment';

export interface IValidationResult {
  IsValid: boolean;
  ValidIntValue?: number;
  ValidValue?: string;
}

@Injectable({
  providedIn: 'root',
})
export class AttributesTabService {
  constructor() {}

  validate(
    attribute: IAttribute,
    candidateValue: string,
    propertyName: string
  ): IValidationResult {
    const validationResult: IValidationResult = {
      IsValid: false,
    };
    switch (propertyName) {
      case 'Value': {
        switch (attribute.ValueType) {
          case 'Boolean': {
            validationResult.IsValid =
              candidateValue.toLowerCase() == 'true' ||
              candidateValue.toLowerCase() == 'false' ||
              candidateValue == '1' ||
              candidateValue == '0';
            if (validationResult.IsValid) {
              validationResult.ValidValue = candidateValue;
            }
            break;
          }
          case 'Date': {
            const candidateDate = moment(candidateValue);
            validationResult.IsValid = candidateDate.isValid();
            if (validationResult.IsValid) {
              validationResult.ValidValue = candidateValue;
            }
            break;
          }
          case 'DiscreteList': {
            if (attribute.ValueOptions.includes(candidateValue)) {
              validationResult.IsValid = true;
              validationResult.ValidValue = candidateValue;
            }
            break;
          }
          case 'Float': {
            const validFloatValue = parseFloat(candidateValue);
            validationResult.IsValid = !isNaN(validFloatValue);
            if (validationResult.IsValid) {
              validationResult.ValidValue = validFloatValue.toString();
            }
            break;
          }
          case 'Int': {
            const intValidationResult: IValidationResult = this.validateInt(
              candidateValue,
              false
            );
            validationResult.IsValid = intValidationResult.IsValid;
            validationResult.ValidValue = intValidationResult.ValidValue;
            break;
          }
          case 'String': {
            validationResult.IsValid = true;
            validationResult.ValidValue = candidateValue;
            break;
          }
        }
        break;
      }
      case 'DisplayOrder': {
        const intValidationResult: IValidationResult = this.validateInt(
          candidateValue,
          true
        );
        validationResult.IsValid = intValidationResult.IsValid;
        validationResult.ValidIntValue = intValidationResult.ValidIntValue;
        break;
      }
    }

    return validationResult;
  }

  validateInt(
    candidateInt: string,
    requireGreaterThanEqualToZero: boolean
  ): IValidationResult {
    const validationResult: IValidationResult = {
      IsValid: false,
    };
    const validIntValue = parseInt(candidateInt);
    validationResult.IsValid =
      !isNaN(validIntValue) && candidateInt.indexOf('.') === -1;
    if (requireGreaterThanEqualToZero) {
      validationResult.IsValid = validationResult.IsValid && validIntValue > -1;
    }
    if (validationResult.IsValid) {
      validationResult.ValidIntValue = validIntValue;
      validationResult.ValidValue = validIntValue.toString();
    }
    return validationResult;
  }

  private attributeFromAny(untypedAttribute: any): IAttribute {
    const typedAttribute: IAttribute = {
      AssetId: untypedAttribute.AssetId,
      AssetPath: untypedAttribute.AssetPath,
      AssetName: untypedAttribute.AssetName,
      AssetClassTypeKey: untypedAttribute.AssetClassTypeKey,
      Id: untypedAttribute.Id,
      Name: untypedAttribute.Name,
      Value: untypedAttribute.Value,
      ValueType: untypedAttribute.ValueType,
      ValueOptions: untypedAttribute.ValueOptions,
      EngUnit: untypedAttribute.EngUnit,
      DisplayOrder: untypedAttribute.DisplayOrder,
      IsStandard: untypedAttribute.IsStandard,
      IsFavorite: untypedAttribute.IsFavorite,
      CreateDate: untypedAttribute.CreateDate,
      ChangeDate: untypedAttribute.ChangeDate,
    };
    return typedAttribute;
  }

  private attributeUpsertResultFromAny(
    untypedAttribute: any
  ): IAttributeUpsertResult {
    const typedAttributeResult: IAttribute =
      this.attributeFromAny(untypedAttribute);
    const typedAttributeUpsertResult: IAttributeUpsertResult = {
      ...typedAttributeResult,
      Error: untypedAttribute.Error,
    };
    return typedAttributeUpsertResult;
  }

  attributesFromAnys(untypedAttributes: any[]): IAttribute[] {
    const typedAttributes: IAttribute[] = untypedAttributes.map(
      (untypedAttribute: any) => this.attributeFromAny(untypedAttribute)
    );
    return typedAttributes;
  }

  attributeUpsertResultsFromAnys(
    untypedAttributes: any[]
  ): IAttributeUpsertResult[] {
    const typedAttributeUpsertResults: IAttributeUpsertResult[] =
      untypedAttributes.map((untypedAttribute: any) =>
        this.attributeUpsertResultFromAny(untypedAttribute)
      );
    return typedAttributeUpsertResults;
  }
}
