/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AssetConfigCSVParserService {
  constructor() {}

  parseCSVStringToArrayString(str: string): string[][] {
    const arr: string[][] = [];
    let quote = false;
    let row, col, c, cc, nc;
    for (row = col = c = 0; c < str.length; c++) {
      cc = str[c];
      nc = str[c + 1];
      arr[row] = arr[row] || [];
      arr[row][col] = arr[row][col] || '';

      if (cc == '"' && quote && nc == '"') {
        arr[row][col] += cc;
        ++c;
        continue;
      }
      if (cc == '"') {
        quote = !quote;
        continue;
      }
      if (cc == '\t' && !quote) {
        ++col;
        continue;
      }
      if (cc == '\n' && !quote) {
        ++row;
        col = 0;
        continue;
      }

      arr[row][col] += cc;
    }
    return arr;
  }
}
