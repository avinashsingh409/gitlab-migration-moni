import { TestBed } from '@angular/core/testing';
import { AttachmentsTabService } from './attachments-tab.service';

describe('AttachmentsTabService', () => {
  let attachmentsTabService: AttachmentsTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [AttachmentsTabService],
      declarations: [],
    });
    attachmentsTabService = TestBed.inject(AttachmentsTabService);
  });

  it('should create', () => {
    expect(attachmentsTabService).toBeTruthy();
  });
});
