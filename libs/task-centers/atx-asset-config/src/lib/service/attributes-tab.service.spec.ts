import { TestBed } from '@angular/core/testing';
import {
  AttributesTabService,
  IValidationResult,
} from './attributes-tab.service';
import { IAttribute } from '../models/attribute-type';

const attribute: IAttribute = {
  AssetId: '',
  AssetPath: '',
  AssetName: '',
  AssetClassTypeKey: '',
  Id: 1,
  Name: '',
  Value: '',
  ValueType: '',
  ValueOptions: [],
  EngUnit: '',
  DisplayOrder: 0,
  IsStandard: true,
  IsFavorite: false,
  CreateDate: new Date(),
  ChangeDate: new Date(),
};

describe('AttributesTabService', () => {
  let attributesTabService: AttributesTabService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [AttributesTabService],
      declarations: [],
    });
    attributesTabService = TestBed.inject(AttributesTabService);
  });

  it('should create', () => {
    expect(attributesTabService).toBeTruthy();
  });

  it('should validate Boolean Values', () => {
    const boolAttribute: IAttribute = {
      ...attribute,
      ValueType: 'Boolean',
    };

    let newBoolValue = '1';
    let validationResult: IValidationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1');

    newBoolValue = '+1';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = '-1';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = '+1';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = '1.3';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = '0';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0');

    newBoolValue = '6';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = 'true';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('true');

    newBoolValue = 'TRUE';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('TRUE');

    newBoolValue = 'TruE';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('TruE');

    newBoolValue = 'false';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('false');

    newBoolValue = 'FALSE';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('FALSE');

    newBoolValue = 'FalsE';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('FalsE');

    newBoolValue = 'a';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newBoolValue = '';
    validationResult = attributesTabService.validate(
      boolAttribute,
      newBoolValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);
  });

  it('should validate DiscreteList Values', () => {
    const dlAttribute: IAttribute = {
      ...attribute,
      ValueType: 'DiscreteList',
      ValueOptions: ['pw', '', '4'],
    };

    const newDLValue = 'pw';
    const validationResult: IValidationResult = attributesTabService.validate(
      dlAttribute,
      newDLValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('pw');
  });

  it('should validate Float Values', () => {
    const floatAttribute: IAttribute = {
      ...attribute,
      ValueType: 'Float',
    };

    let newFloatValue = '1';
    let validationResult: IValidationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1');

    newFloatValue = '1.0';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1');

    newFloatValue = '1.1';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1.1');

    newFloatValue = '1.50';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1.5');

    newFloatValue = '.5';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0.5');

    newFloatValue = '0.5';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0.5');

    newFloatValue = '0.5abc';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0.5');

    newFloatValue = '0.0';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0');

    newFloatValue = '.0';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0');

    newFloatValue = '0';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0');

    newFloatValue = '-5';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('-5');

    newFloatValue = '-5.2';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('-5.2');

    newFloatValue = 'a5';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newFloatValue = '';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newFloatValue = ' ';
    validationResult = attributesTabService.validate(
      floatAttribute,
      newFloatValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);
  });

  it('should validate Int Values', () => {
    const intAttribute: IAttribute = {
      ...attribute,
      ValueType: 'Int',
    };

    let newIntValue = '1';
    let validationResult: IValidationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('1');

    newIntValue = '18.5';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '18abc';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('18');

    newIntValue = 'abc18';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '18.0';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '00018.9';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '0';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('0');

    newIntValue = '0.0';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '-1';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(validationResult.IsValid);
    expect(validationResult.ValidValue);
    expect(validationResult.ValidValue).toEqual('-1');

    newIntValue = '-1.6';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '-01.6';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '-1.6def4';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = 'def-1.6';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);

    newIntValue = '';
    validationResult = attributesTabService.validate(
      intAttribute,
      newIntValue,
      'Value'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidValue);
  });

  it('should validate DisplayOrder', () => {
    let newDisplayOrder = '1';
    let validationResult: IValidationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(1);
    }

    newDisplayOrder = '18.5';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(18);
    }

    newDisplayOrder = '18abc';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(18);
    }

    newDisplayOrder = 'abc18';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidIntValue);

    newDisplayOrder = '18.0';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(18);
    }

    newDisplayOrder = '00018.9';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(18);
    }

    newDisplayOrder = '0';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(0);
    }

    newDisplayOrder = '0.0';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(validationResult.IsValid);
    expect(typeof validationResult.ValidIntValue === 'number');
    if (typeof validationResult.ValidIntValue === 'number') {
      expect(validationResult.ValidIntValue).toEqual(0);
    }

    newDisplayOrder = '-1';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidIntValue);

    newDisplayOrder = '';
    validationResult = attributesTabService.validate(
      attribute,
      newDisplayOrder,
      'DisplayOrder'
    );
    expect(!validationResult.IsValid);
    expect(!validationResult.ValidIntValue);
  });
});
