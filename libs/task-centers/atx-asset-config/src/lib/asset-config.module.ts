import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssetConfigComponent } from './components/asset-config/asset-config.component';
import { StoreModule } from '@ngrx/store';
import {
  assetConfigFeatureKey,
  reducers,
} from './store/reducers/asset-config.reducers';
import { EffectsModule } from '@ngrx/effects';
import { AssetConfigEffects } from './store/effects/asset-config.effects';
import { RouterModule } from '@angular/router';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NavigationModule } from '@atonix/atx-navigation';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedUiModule } from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssetsTabComponent } from './components/assets-tab/assets-tab.component';
import { AttachmentsTabComponent } from './components/attachments-tab/attachments-tab.component';
import { AttributesTabComponent } from './components/attributes-tab/attributes-tab.component';
import { TagsTabComponent } from './components/tags-tab/tags-tab.component';
import { TagsTabGuard } from './service/tags-tab.guard';
import { AssetsTagGuard } from './service/assets-tab.guard';
import { AssetsAttributesGuard } from './service/assets-attributes-tab.guard';
import { AssetsAttachmentsGuard } from './service/assets-attachments-tab.guard';
import { TagConfigurationComponent } from './components/tag-configuration/tag-configuration.component';
import { ToastService } from '@atonix/shared/utils';
import { DeleteColumnComponent } from './components/delete-column/delete-column.component';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/core';
LicenseManager.setLicenseKey(
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
import { TagsTabEffects } from './store/effects/tags-tab.effects';
import { TagMapComponent } from './components/tag-map/tag-map.component';
import { DeleteTagMapColumnComponent } from './components/delete-tagmap-column/delete-tagmap-column.component';
import { DeleteConfirmationDialogComponent } from './components/delete-confirmation-dialog/delete-confirmation-dialog.component';
import { AssetTabEffects } from './store/effects/asset-tab.effects';
import { AttributesTabEffects } from './store/effects/attributes-tab.effects';
import { AttachmentsTabEffects } from './store/effects/attachments-tab.effects';
import { AssetConfirmationDialogComponent } from './components/asset-confirmation-dialog/asset-confirmation-dialog.component';
import { CreateAssetDialogComponent } from './components/create-asset-dialog/create-asset-dialog.component';
import { AssetDeleteConfirmationDialogComponent } from './components/asset-delete-confirmation-dialog/asset-delete-confirmation-dialog.component';
import { AssetPasteDialogComponent } from './components/asset-paste-dialog/asset-paste-dialog.component';
import { TagMapPasteDialogComponent } from './components/tagmaps-paste-dialog/tagmaps-paste-dialog.component';
import { TagsPasteDialogComponent } from './components/tags-paste-dialog copy/tags-paste-dialog.component';
import { StandardAttributeFormatterComponent } from './components/standard-attribute-formatter/standard-attribute-formatter.component';
import { FavoriteAttributeFormatterComponent } from './components/favorite-attribute-formatter/favorite-attribute-formatter.component';
import { AttributeDeleteConfirmationDialogComponent } from './components/attribute-delete-confirmation-dialog/attribute-delete-confirmation-dialog.component';
import { AttributesPasteDialogComponent } from './components/attributes-paste-dialog/attributes-paste-dialog.component';
import { FavoriteAttachmentFormatterComponent } from './components/favorite-attachment-formatter/favorite-attachment-formatter.component';
import { FilenameAttachmentFormatterComponent } from './components/filename-attachment-formatter/filename-attachment-formatter.component';
import { AttachmentViewerDialogComponent } from './components/attachment-viewer-dialog/attachment-viewer-dialog.component';
import { AttachmentDeleteEditFileConfirmationDialogComponent } from './components/attachment-delete-edit-file-confirmation-dialog/attachment-delete-edit-file-confirmation-dialog.component';

@NgModule({
  imports: [
    AssetTreeModule,
    NavigationModule,
    AtxMaterialModule,
    SharedUiModule,
    SharedApiModule,
    SharedUtilsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    StoreModule.forFeature(assetConfigFeatureKey, reducers),
    EffectsModule.forFeature([
      AssetConfigEffects,
      AssetTabEffects,
      TagsTabEffects,
      AttributesTabEffects,
      AttachmentsTabEffects,
    ]),
    RouterModule.forChild([
      {
        path: '',
        component: AssetConfigComponent,
        children: [
          {
            path: '',
            redirectTo: 'assets',
            pathMatch: 'full',
          },
          {
            path: 'assets',
            component: AssetsTabComponent,
            canActivate: [AssetsTagGuard],
          },
          {
            path: 'tags',
            component: TagsTabComponent,
            canActivate: [TagsTabGuard],
          },
          {
            path: 'attributes',
            component: AttributesTabComponent,
            canActivate: [AssetsAttributesGuard],
          },
          {
            path: 'attachments',
            component: AttachmentsTabComponent,
            canActivate: [AssetsAttachmentsGuard],
          },
        ],
      },
    ]),
  ],
  declarations: [
    AssetConfigComponent,
    AssetsTabComponent,
    TagsTabComponent,
    AttributesTabComponent,
    AttachmentsTabComponent,
    TagConfigurationComponent,
    DeleteColumnComponent,
    TagMapComponent,
    DeleteTagMapColumnComponent,
    DeleteConfirmationDialogComponent,
    AssetConfirmationDialogComponent,
    CreateAssetDialogComponent,
    AssetDeleteConfirmationDialogComponent,
    AssetPasteDialogComponent,
    TagMapPasteDialogComponent,
    TagsPasteDialogComponent,
    StandardAttributeFormatterComponent,
    FavoriteAttributeFormatterComponent,
    AttributeDeleteConfirmationDialogComponent,
    AttributesPasteDialogComponent,
    FavoriteAttachmentFormatterComponent,
    FilenameAttachmentFormatterComponent,
    AttachmentViewerDialogComponent,
    AttachmentDeleteEditFileConfirmationDialogComponent,
  ],
  providers: [ToastService],
})
export class AssetConfigModule {}
