export interface IAttribute {
  AssetId: string;
  AssetPath: string;
  AssetName: string;
  AssetClassTypeKey: string;
  Id: number | undefined;
  Name: string;
  Value: string;
  ValueType: string;
  ValueOptions: string[];
  EngUnit: string;
  DisplayOrder: number;
  IsStandard: boolean;
  IsFavorite: boolean;
  CreateDate: Date;
  ChangeDate: Date;
}

export interface IAttributeUpsertResult extends IAttribute {
  Error: string;
}
