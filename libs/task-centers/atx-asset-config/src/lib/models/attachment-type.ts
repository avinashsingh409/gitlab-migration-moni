export interface IAttachment {
  Id: string;
  Filename: string;
  Caption: string;
  ChangeDate: Date;
  ChangedBy: string;
  IsFavorite: boolean;
  DisplayOrder: number;
  ContentId: string;
  AssetName: string;
  Type: string;
  Upload?: boolean;
  TemporaryLink?: string;
  LinkExpirationDate?: Date;
}
