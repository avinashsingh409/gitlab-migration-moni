/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  Inject,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IAttachment } from '../../models/attachment-type';
import { ImagesFrameworkService } from '@atonix/shared/api';
import { take, catchError, of } from 'rxjs';
import { IAWSFileUrl } from '@atonix/atx-core';

@Component({
  selector: 'atx-attachment-viewer-dialog',
  templateUrl: './attachment-viewer-dialog.component.html',
  styleUrls: ['./attachment-viewer-dialog.component.scss'],
})
export class AttachmentViewerDialogComponent implements AfterViewInit {
  @ViewChild('imageContainer') imageContainer: ElementRef | any = null;
  private attachment: IAttachment;
  constructor(
    public dialogRef: MatDialogRef<AttachmentViewerDialogComponent>,
    private imagesFrameworkService: ImagesFrameworkService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.attachment = data.attachment as IAttachment;
  }

  ngAfterViewInit(): void {
    this.imagesFrameworkService
      .getFileUrl(this.attachment.ContentId, this.attachment.Filename, false)
      .pipe(
        take(1),
        catchError(() => {
          if (this.imageContainer) {
            this.imageContainer.nativeElement.replaceChildren();
            this.imageContainer.nativeElement.insertAdjacentText(
              'afterbegin',
              'Not able to load image'
            );
          }
          return of();
        })
      )
      .subscribe((url: IAWSFileUrl) => {
        if (this.imageContainer) {
          this.imageContainer.nativeElement.replaceChildren();
          if (url && url.Url && url.Url !== 'null') {
            this.imageContainer.nativeElement.insertAdjacentHTML(
              'afterbegin',
              `<img src="${url.Url}" alt="${this.attachment.Filename}">`
            );
          } else {
            this.imageContainer.nativeElement.insertAdjacentText(
              'afterbegin',
              'Not able to load image'
            );
          }
        }
      });
  }
}
