/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AssetsCoreService } from '@atonix/shared/api';
import { catchError, Subject, take, throwError } from 'rxjs';
import { AssetConfigCSVParserService } from '../../service/csv-parser.service';
import {
  IAttribute,
  IAttributeUpsertResult,
} from '../../models/attribute-type';
import {
  AttributesTabService,
  IValidationResult,
} from '../../service/attributes-tab.service';

@Component({
  selector: 'atx-asset-config-attributes-paste-dialog',
  templateUrl: './attributes-paste-dialog.component.html',
  styleUrls: ['./attributes-paste-dialog.component.scss'],
})
export class AttributesPasteDialogComponent implements OnDestroy {
  @ViewChild('pasteBox') pasteBox: ElementRef | undefined;
  public isValidating = false;
  public isSaveBtnEnabled = false;
  public isProceedVisible = false;
  public clientValidationResult = '';
  public serverValidationResult = '';
  private pastedText = '';
  public attributes: IAttribute[] = [];
  private existingAttributesDict: {
    [assetId: string]: {
      [attributeName: string]: IAttribute;
    };
  } = {};
  private referenceId = '';
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AttributesPasteDialogComponent>,
    private assetsCoreService: AssetsCoreService,
    private csvService: AssetConfigCSVParserService,
    private attributesTabService: AttributesTabService
  ) {
    this.existingAttributesDict = {
      ...data.existingAttributesDict,
    };
    this.referenceId = data.referenceId;
  }

  public onKeydown(event: KeyboardEvent): void {
    if (event.ctrlKey && ['V', 'v'].includes(event.key)) {
      setTimeout(() => {
        this.initialize();
        this.pastedText = this.pasteBox?.nativeElement.value ?? '';
        this.validate();
      });
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onSave(): void {
    if (this.pasteBox) {
      this.pasteBox.nativeElement.disable = true;
    }
    this.isSaveBtnEnabled = false;

    this.assetsCoreService
      .upsertAttributes(this.referenceId, [...this.attributes])
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.disable = false;
          }
          this.isSaveBtnEnabled = true;
          return throwError(() => new Error(message));
        })
      )
      .subscribe((untypedUpsertResults: any[]) => {
        const typedUpsertResults: IAttributeUpsertResult[] =
          untypedUpsertResults.map((r: any) => {
            const result: IAttributeUpsertResult = {
              AssetId: r.AssetId,
              AssetPath: '',
              AssetName: '',
              AssetClassTypeKey: '',
              Id: r.Id,
              Name: r.Name,
              Value: r.Value,
              ValueType: r.ValueType,
              ValueOptions: [],
              EngUnit: r.EngUnit,
              DisplayOrder: r.DisplayOrder,
              IsStandard: r.IsStandard,
              IsFavorite: r.IsFavorite,
              CreateDate: r.CreateDate,
              ChangeDate: r.ChangeDate,
              Error: r.Error,
            };
            return result;
          });

        this.serverValidationResult = '';
        for (const attribute of typedUpsertResults) {
          if (attribute.Error) {
            this.serverValidationResult += `Asset Id: ${attribute.AssetId} Name: ${attribute.Name} Error: ${attribute.Error}\n`;
          }
        }

        if (!this.serverValidationResult) {
          this.dialogRef.close(typedUpsertResults);
        } else if (this.pasteBox) {
          this.pasteBox.nativeElement.disable = false;
        }
        this.isSaveBtnEnabled = true;
      });
  }

  public onValidateCancel(): void {
    this.initialize();
    if (this.pasteBox) {
      this.pasteBox.nativeElement.value = '';
    }
  }

  public onProceed(): void {
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = true;
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private validate(): void {
    if (this.pastedText !== '') {
      this.isValidating = true;
      const pasted: string[][] = this.csvService.parseCSVStringToArrayString(
        this.pastedText
      );
      const data: { [key: string]: string[] } = {};
      const mandatoryHeaders: string[] = [
        'Asset ID',
        'Attribute Name',
        'Attribute Value',
      ];
      const optionalHeaders: string[] = [
        'Favorite',
        'Display Order',
        'Value Type',
      ];

      // validate headers
      for (let i = 0; i < pasted[0].length; i++) {
        const header: string = pasted[0][i];
        if (
          mandatoryHeaders.includes(header) ||
          optionalHeaders.includes(header)
        ) {
          data[header] = [];
        }
      }

      const missingMandatoryHeaders = mandatoryHeaders.filter(
        (header: string) => !Object.keys(data).includes(header)
      );

      if (missingMandatoryHeaders.length > 0) {
        this.clientValidationResult =
          missingMandatoryHeaders.length === 1
            ? `Header Row missing: ${missingMandatoryHeaders[0]}`
            : `Header Rows missing: ${missingMandatoryHeaders.join(', ')}`;
        this.isValidating = false;
        return;
      }

      // build data
      for (let row = 1; row < pasted.length; row++) {
        for (let col = 0; col < pasted[row].length; col++) {
          const header: string = pasted[0][col];
          if (data[header] != null) {
            data[header].push(pasted[row][col]);
          }
        }
      }

      const pastedDataLength: number = data[Object.keys(data)[0]].length;
      const missingRows: number[] = [];
      const invalidRows: number[] = [];
      this.attributes = [];
      for (let i = 0; i < pastedDataLength; i++) {
        const assetId: string = data['Asset ID'][i];
        const attributeName: string = data['Attribute Name'][i];
        const attributeValue: string = data['Attribute Value'][i];
        const favoriteString: string = data['Favorite']
          ? data['Favorite'][i]
          : '';
        const displayOrderString: string = data['Display Order']
          ? data['Display Order'][i]
          : '';
        let valueType: string = data['Value Type'] ? data['Value Type'][i] : '';

        if (!assetId || !attributeName) {
          missingRows.push(i + 1);
          continue;
        }

        let existingAttribute: IAttribute | undefined;
        if (
          this.existingAttributesDict[assetId] &&
          this.existingAttributesDict[assetId][attributeName]
        ) {
          existingAttribute =
            this.existingAttributesDict[assetId][attributeName];
        }

        let favorite = false;
        if (favoriteString) {
          if (['1', 'true'].includes(favoriteString.toLowerCase())) {
            favorite = true;
          } else if (!['0', 'false'].includes(favoriteString.toLowerCase())) {
            invalidRows.push(i + 1);
            continue;
          }
        } else if (existingAttribute) {
          favorite = existingAttribute.IsFavorite;
        }

        let displayOrder = 0;
        if (displayOrderString) {
          const displayOrderValidationResult: IValidationResult =
            this.attributesTabService.validateInt(displayOrderString, true);
          if (
            displayOrderValidationResult.IsValid &&
            typeof displayOrderValidationResult.ValidIntValue === 'number'
          ) {
            displayOrder = displayOrderValidationResult.ValidIntValue;
          } else {
            invalidRows.push(i + 1);
            continue;
          }
        } else if (existingAttribute) {
          displayOrder = existingAttribute.DisplayOrder;
        }

        if (existingAttribute) {
          valueType = existingAttribute.ValueType;
        } else {
          if (valueType) {
            if (
              !['boolean', 'date', 'float', 'int', 'string'].includes(
                valueType.toLowerCase()
              )
            ) {
              invalidRows.push(i + 1);
              continue;
            }
          } else {
            valueType = 'String';
          }
        }

        this.attributes.push({
          AssetId: assetId,
          AssetPath: '',
          AssetName: '',
          AssetClassTypeKey: '',
          Id: existingAttribute ? existingAttribute.Id : undefined,
          Name: attributeName,
          Value: attributeValue,
          ValueType: valueType,
          ValueOptions: [],
          EngUnit: '',
          DisplayOrder: displayOrder,
          IsStandard: false,
          IsFavorite: favorite,
          CreateDate: new Date(),
          ChangeDate: new Date(),
        });
      }

      if (missingRows.length > 0) {
        this.clientValidationResult =
          missingRows.length === 1
            ? `Data missing in Row ${missingRows[0]}`
            : `Data missing in Rows ${missingRows.join(', ')}`;
        this.isValidating = false;
        return;
      }

      this.clientValidationResult = 'Data Format OK';
      if (this.attributes.length > 0) {
        if (invalidRows.length > 0) {
          this.isProceedVisible = true;
          this.serverValidationResult = `${
            this.attributes.filter(
              (attribute: IAttribute) => attribute.Id === -1
            ).length
          } valid new attributes found. ${
            this.attributes.filter(
              (attribute: IAttribute) => attribute.Id !== -1
            ).length
          } valid existing attributes found. ${
            invalidRows.length
          } invalid attributes found.`;
        } else {
          this.isSaveBtnEnabled = true;
        }
      } else {
        this.serverValidationResult = `0 valid new attributes found. 0 valid existing attributes found. ${invalidRows.length} invalid attributes found.`;
      }
      this.onValidate(this.attributes);
    }
  }

  private onValidate(attributes: IAttribute[]): void {
    this.assetsCoreService
      .validateAttributes(this.referenceId, attributes)
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = `Error: ${err.error.Results[0].Message}`;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.value = '';
          }
          this.isValidating = false;
          return throwError(() => new Error(message));
        })
      )
      .subscribe((untypedValidateResults: any[]) => {
        const typedValidateResults: IAttributeUpsertResult[] =
          this.attributesTabService.attributeUpsertResultsFromAnys(
            untypedValidateResults
          );

        if (typedValidateResults.length > 0) {
          let errorText = '';

          for (const attribute of typedValidateResults) {
            if (attribute.Error) {
              errorText += `Asset Id: ${attribute.AssetId} Name: ${attribute.Name} Error: ${attribute.Error}\n`;
            }
          }

          if (errorText.length > 0 && this.pasteBox) {
            this.serverValidationResult = errorText;
            this.pasteBox.nativeElement.value = '';
            this.isSaveBtnEnabled = false;
          } else {
            this.serverValidationResult =
              'Data is Valid. Select "Save" below to save Attributes';
            this.isSaveBtnEnabled = true;
          }
          this.isValidating = false;
        }
      });
  }

  private initialize(): void {
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = false;
    this.isValidating = false;
    this.clientValidationResult = '';
    this.serverValidationResult = '';
  }
}
