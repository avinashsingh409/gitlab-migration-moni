/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { IAttribute } from '../../models/attribute-type';
import { updateAttributes } from '../../store/actions/attributes-tab.actions';

@Component({
  selector: 'atx-favorite-attribute-formatter',
  template:
    '<mat-icon style="cursor: pointer" (click)="onIconClick()">{{favoriteAttributeIcon}}</mat-icon>',
})
export class FavoriteAttributeFormatterComponent
  implements ICellRendererAngularComp
{
  favoriteAttributeIcon: 'star' | 'star_border' | undefined;
  private attribute: IAttribute | undefined;

  constructor(private store: Store) {}

  agInit(params: any) {
    this.favoriteAttributeIcon = params?.data?.IsFavorite
      ? 'star'
      : 'star_border';
    this.attribute = params?.data;
  }

  refresh(params: any) {
    this.favoriteAttributeIcon = params?.data?.IsFavorite
      ? 'star'
      : 'star_border';
    return true;
  }

  onIconClick() {
    if (this.attribute) {
      const attributeToUpdate: IAttribute = {
        ...this.attribute,
        IsFavorite: !this.attribute.IsFavorite,
      };
      this.store.dispatch(
        updateAttributes({
          attributesToUpdate: [attributeToUpdate],
        })
      );
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached(_?: IAfterGuiAttachedParams): void {}
}
