/* eslint-disable ngrx/use-consistent-global-store-name */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { Store } from '@ngrx/store';

import { FavoriteAttributeFormatterComponent } from './favorite-attribute-formatter.component';

describe('FavoriteAttributeFormatterComponent', () => {
  let component: FavoriteAttributeFormatterComponent;
  let fixture: ComponentFixture<FavoriteAttributeFormatterComponent>;
  let mockStore: Store;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: Store, useValue: mockStore },
      ],
      declarations: [FavoriteAttributeFormatterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteAttributeFormatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
