/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-standard-attribute-formatter',
  template:
    '<mat-icon [ngStyle]="{color: standardAttributeColor}">{{standardAttributeIcon}}</mat-icon>',
})
export class StandardAttributeFormatterComponent
  implements ICellRendererAngularComp
{
  standardAttributeIcon: 'check_circle' | 'cancel' | undefined;
  standardAttributeColor: '#5e9a40' | 'grey' | undefined;

  agInit(params: any) {
    this.standardAttributeIcon = params?.data?.IsStandard
      ? 'check_circle'
      : 'cancel';
    this.standardAttributeColor = params?.data?.IsStandard ? '#5e9a40' : 'grey';
  }

  refresh(params: any): boolean {
    this.standardAttributeIcon = params?.data?.IsStandard
      ? 'check_circle'
      : 'cancel';
    this.standardAttributeColor = params?.data?.IsStandard ? '#5e9a40' : 'grey';
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached(_?: IAfterGuiAttachedParams): void {}
}
