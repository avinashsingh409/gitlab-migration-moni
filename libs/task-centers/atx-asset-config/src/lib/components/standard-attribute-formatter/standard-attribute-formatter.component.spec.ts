import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';

import { StandardAttributeFormatterComponent } from './standard-attribute-formatter.component';

describe('StandardAttributeFormatterComponent', () => {
  let component: StandardAttributeFormatterComponent;
  let fixture: ComponentFixture<StandardAttributeFormatterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [StandardAttributeFormatterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandardAttributeFormatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
