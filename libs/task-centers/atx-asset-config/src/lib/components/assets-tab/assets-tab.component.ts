/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { selectSelectedAssetObject } from '../../store/selectors/asset-config.selectors';
import { selectSelectedAssetTreeNodeParent } from '../../store/selectors/asset-nav.selectors';
import * as actions from '../../store/actions/asset-tab.actions';
import {
  toggleSelectedAssetOnly,
  showSuccessMessage,
  updateRoute,
} from '../../store/actions/asset-config.actions';
import {
  AssetViewModel,
  selectAssets,
  selectAssetState,
  selectAssetViewModel,
} from '../../store/selectors/asset-tab.selectors';
import {
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  ClipboardModule,
  GridOptions,
  RowDragEndEvent,
  RowNode,
  GridReadyEvent,
  FilterChangedEvent,
  GridApi,
  ColDef,
  ColGroupDef,
  RowDragEvent,
} from '@ag-grid-enterprise/all-modules';
import {
  dateComparator,
  IAssetCreateUpdateResult,
  IAssetResult,
  IAssetUpdate,
  isNil,
} from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';
import {
  DataAvailable,
  assetTabFeatureKey,
} from '../../store/reducers/asset-tab.reducer';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AssetConfirmationDialogComponent } from '../asset-confirmation-dialog/asset-confirmation-dialog.component';
import { CreateAssetDialogComponent } from '../create-asset-dialog/create-asset-dialog.component';
import { AssetDeleteConfirmationDialogComponent } from '../asset-delete-confirmation-dialog/asset-delete-confirmation-dialog.component';
import { AssetPasteDialogComponent } from '../asset-paste-dialog/asset-paste-dialog.component';
import { groupBy } from 'lodash';

@Component({
  selector: 'atx-asset-config-assets-tab',
  templateUrl: './assets-tab.component.html',
  styleUrls: ['./assets-tab.component.scss'],
})
export class AssetsTabComponent implements AfterViewInit, OnDestroy {
  @ViewChild('gridBody') gridBody: ElementRef | any = null;
  @ViewChild('menuBar')
  menuBar: ElementRef | any = null;
  public selectedRowAsset: IAssetResult | null = null;
  public assetViewModel$: Observable<AssetViewModel> =
    this.store.select(selectAssetViewModel);
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];
  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'single',
    suppressMoveWhenRowDragging: true,
    rowDragEntireRow: true,
    rowHeight: 30,
    onRowDragEnd: (event: RowDragEndEvent) => {
      this.removeHover();
      if (this.gridApi != null && event.overNode != null) {
        const draggedAsset: IAssetResult = event.node.data;
        const targetAsset: IAssetResult = event.overNode.data;

        // (1) asset to its existing parent asset: NOT ALLOWED
        if (draggedAsset.ParentAssetId === targetAsset.AssetId) {
          return;
        }
        // (2) parent asset to its direct descendants: NOT ALLOWED
        if (targetAsset.Hierarchy.includes(draggedAsset.AssetName)) {
          return;
        }

        const dialogConfig = new MatDialogConfig();
        dialogConfig.height = '250px';
        dialogConfig.width = '400px';
        dialogConfig.data = {
          draggedAsset: draggedAsset.AssetName,
          targetAsset: targetAsset.AssetName,
        };

        // (3) sibling-to-sibling: POP-UP (either re-order or re-parent)
        if (draggedAsset.ParentAssetId === targetAsset.ParentAssetId) {
          dialogConfig.data.siblingToSibling = true;

          const dialogRef = this.dialog.open(
            AssetConfirmationDialogComponent,
            dialogConfig
          );
          dialogRef.afterClosed().subscribe(({ action }) => {
            if (action === 'reparent') {
              this.store.dispatch(
                actions.reParentAsset({ newParent: targetAsset, draggedAsset })
              );
            } else {
              this.store.dispatch(
                actions.reOrderAsset({ targetAsset, draggedAsset })
              );
            }
          });
        }
        // (4) asset to a root asset (asset that DOES NOT have a parent) : POP-UP (re-parent)
        // (5) asset to a different asset (outside of his parent) that HAS a parent: POP-UP (re-parent)
        else {
          dialogConfig.data.showTickbox =
            !window.sessionStorage.getItem('ShowAssetDialogTickBox') ||
            window.sessionStorage.getItem('ShowAssetDialogTickBox') === 'true';

          if (dialogConfig.data.showTickbox) {
            const dialogRef = this.dialog.open(
              AssetConfirmationDialogComponent,
              dialogConfig
            );
            dialogRef.afterClosed().subscribe(({ action, saveSession }) => {
              if (saveSession) {
                window.sessionStorage.setItem(
                  'ShowAssetDialogTickBox',
                  'false'
                );
              }
              if (action === 'reparent') {
                this.store.dispatch(
                  actions.reParentAsset({
                    newParent: targetAsset,
                    draggedAsset,
                  })
                );
              }
            });
          } else {
            this.store.dispatch(
              actions.reParentAsset({
                newParent: targetAsset,
                draggedAsset,
              })
            );
          }
        }
      }
    },
    onRowDragMove: (event: RowDragEvent) => {
      this.removeHover();
      if (this.gridApi != null && event.overNode != null) {
        const element: NodeList = this.el.nativeElement.querySelectorAll(
          `[row-id="${event.overNode.id}"]`
        )[1];
        this.renderer.addClass(element, 'ag-row-hover');
        this.targetNode = event.overNode;

        const ghostIcon: Element | null =
          document.querySelector('.ag-dnd-ghost-icon');
        if (ghostIcon) {
          const draggedAsset: IAssetResult = event.node.data;
          const draggedAssetPath: string =
            draggedAsset.Hierarchy.length === 1
              ? `/${draggedAsset.AssetName}`
              : `${draggedAsset.AssetPath}/${draggedAsset.AssetName}`;
          // disable reparenting of: (1) an asset to any of its own descendants AND (2) a child to its parent
          ghostIcon.innerHTML =
            this.targetNode.data.AssetPath.includes(draggedAssetPath) ||
            draggedAsset.ParentAssetId === this.targetNode.data.AssetId
              ? `<span class="ag-icon ag-icon-not-allowed" unselectable="on" role="presentation"></span>`
              : `<span class="ag-icon ag-icon-arrows" unselectable="on" role="presentation"></span>`;
        }
      }
    },
    onRowDragLeave: () => {
      this.removeHover();
    },
    onSelectionChanged: () => {
      this.selectedRowAsset =
        this.gridApi && this.gridApi.getSelectedNodes().length > 0
          ? this.gridApi.getSelectedNodes()[0].data
          : null;

      this.menuBar?.nativeElement.click();
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      sortable: true,
      enableRowGroup: false,
      enablePivot: false,
      enableValue: false,
      editable: false,
    },
    treeData: true,
    groupDefaultExpanded: 0,
    getDataPath: (data: IAssetResult) => data.Hierarchy,
    autoGroupColumnDef: {
      headerName: 'Hierarchy',
      cellRendererParams: {
        suppressCount: true,
      },
    },
    columnDefs: [
      {
        headerName: 'Asset Name',
        colId: 'AssetName',
        field: 'AssetName',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAssetResult;
          newValue: string;
        }) => {
          const assetUpdate: IAssetUpdate = this.assetResultToAssetUpdate(data);
          assetUpdate.AssetName = newValue;
          this.store.dispatch(
            actions.updateAssets({
              assets: [assetUpdate],
              updateAssetType: 'single',
            })
          );
          return false;
        },
      },
      {
        headerName: 'Description',
        colId: 'AssetDesc',
        field: 'AssetDesc',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAssetResult;
          newValue: string;
        }) => {
          const assetUpdate: IAssetUpdate = this.assetResultToAssetUpdate(data);
          assetUpdate.AssetDesc = newValue;
          this.store.dispatch(
            actions.updateAssets({
              assets: [assetUpdate],
              updateAssetType: 'single',
            })
          );
          return false;
        },
      },
      {
        headerName: 'Type',
        colId: 'AssetClassTypeKey',
        field: 'AssetClassTypeKey',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAssetResult;
          newValue: string;
        }) => {
          const assetUpdate: IAssetUpdate = this.assetResultToAssetUpdate(data);
          assetUpdate.AssetClassTypeKey = newValue;
          this.store.dispatch(
            actions.updateAssets({
              assets: [assetUpdate],
              updateAssetType: 'single',
            })
          );
          return false;
        },
      },
      {
        headerName: 'Parent',
        colId: 'ParentName',
        field: 'ParentName',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAssetResult;
          newValue: string;
        }) => {
          const parentAsset: IAssetResult | undefined =
            this.getAllGridAssets().find(
              (asset: IAssetResult) => asset.AssetName === newValue
            );
          if (parentAsset) {
            const assetUpdate: IAssetUpdate =
              this.assetResultToAssetUpdate(data);
            assetUpdate.ParentAssetId = parentAsset.AssetId;
            this.store.dispatch(
              actions.updateAssets({
                assets: [assetUpdate],
                updateAssetType: 'single',
              })
            );
          }
          return false;
        },
      },
      {
        headerName: 'Parent ID',
        colId: 'ParentAssetId',
        field: 'ParentAssetId',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Parent Asset Path',
        colId: 'AssetPath',
        field: 'AssetPath',
        filter: 'agTextColumnFilter',
        hide: true,
      },
      {
        headerName: 'Asset ID',
        colId: 'AssetId',
        field: 'AssetId',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Order',
        colId: 'DisplayOrder',
        field: 'DisplayOrder',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAssetResult;
          newValue: string;
        }) => {
          const num: number = parseInt(newValue);
          if (!isNaN(num) && num > -1) {
            const assetUpdate: IAssetUpdate =
              this.assetResultToAssetUpdate(data);
            assetUpdate.DisplayOrder = num;
            this.store.dispatch(
              actions.updateAssets({
                assets: [assetUpdate],
                updateAssetType: 'single',
              })
            );
          }
          return false;
        },
        hide: true,
      },
      {
        headerName: 'IsStandard',
        colId: 'IsStandard',
        field: 'IsStandard',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Change Date',
        colId: 'ChangeDate',
        field: 'ChangeDate',
        editable: false,
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        headerName: 'Create Date',
        colId: 'CreateDate',
        field: 'CreateDate',
        editable: false,
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
    ],
    suppressAggFuncInHeader: true,
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.gridApi.hideOverlay();

      this.gridBody.nativeElement.addEventListener(
        'selectstart',
        (e: Event) => {
          e.preventDefault();
          return false;
        }
      );

      this.gridBody.nativeElement.addEventListener('mousedown', (e: any) => {
        if (
          e.target?.className !== 'ag-input-field-input ag-text-field-input'
        ) {
          e.preventDefault();
        }
        return false;
      });
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
    getRowId: (params) => {
      return `${params.data.AssetId}_${params.data.AssetDesc}`;
    },
  };
  private gridApi: GridApi | null = null;
  private enableFloatingFilter = false;
  private assets$: Observable<IAssetResult[]> = this.store.select(selectAssets);
  private assetState$: Observable<DataAvailable> =
    this.store.select(selectAssetState);
  private selectedAssetObject$: Observable<any> = this.store.select(
    selectSelectedAssetObject
  );
  private targetNode: RowNode | null = null;
  private referenceId = '';
  private onDestroy$ = new Subject<void>();

  constructor(
    private store: Store,
    private el: ElementRef,
    private renderer: Renderer2,
    private dialog: MatDialog
  ) {
    this.assets$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assets: IAssetResult[]) => {
        if (this.gridApi) {
          this.gridApi.setRowData(assets);
          this.gridApi.refreshCells({ force: true });
        }
      });

    this.assetState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: DataAvailable) => {
        if (this.gridApi) {
          if (data === 'loading') {
            this.gridApi.showLoadingOverlay();
          } else if (data === 'nodata') {
            this.gridApi.showNoRowsOverlay();
          } else {
            this.gridApi.hideOverlay();
          }
        }
      });
  }

  public clearFilters(_: any): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

      Object.keys(gridFilters).forEach((key: string) => {
        this.gridApi?.getFilterInstance(key)?.setModel(null);
      });

      this.store.dispatch(actions.clearFilters());
      this.refreshGrid();
    }
  }

  public removeFilter(filter: any): void {
    this.store.dispatch(
      actions.removeFilter({
        filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  public toggleFloatingFilter(): void {
    if (this.gridApi) {
      const columnDefs: (ColDef | ColGroupDef)[] | undefined =
        this.gridApi.getColumnDefs();
      if (columnDefs) {
        this.enableFloatingFilter = !this.enableFloatingFilter;
        columnDefs.forEach((colDef: ColDef) => {
          colDef.floatingFilter = this.enableFloatingFilter;
        });
        this.gridApi.setColumnDefs(columnDefs);
      }
    }
  }

  public refresh(): void {
    this.store.dispatch(actions.getAssets());
  }

  public onCreateAssetClick(): void {
    this.selectedRowAsset =
      this.gridApi && this.gridApi.getSelectedNodes().length > 0
        ? this.gridApi.getSelectedNodes()[0].data
        : null;
    if (this.selectedRowAsset) {
      const updatedAssetToCreateOrEdit: IAssetResult = {
        ...this.selectedRowAsset,
        AssetName: '',
        AssetDesc: '',
        AssetClassTypeDesc: '',
        AssetClassTypeKey: '',
      };
      this.store.dispatch(
        actions.updateAssetToCreateOrEdit({ updatedAssetToCreateOrEdit })
      );

      const dialogConfig: MatDialogConfig = new MatDialogConfig();
      dialogConfig.height = '710px';
      dialogConfig.width = '750px';
      dialogConfig.data = {
        type: 'create',
        parentAsset: this.selectedRowAsset,
        referenceId: this.referenceId,
      };

      const dialogRef = this.dialog.open(
        CreateAssetDialogComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe((asset: IAssetCreateUpdateResult) => {
        if (asset == null) {
          return;
        }
        const assetToInsert: IAssetResult =
          this.assetCreateUpdateResultToAssetResult(asset);
        this.store.dispatch(actions.insertSingleAsset({ assetToInsert }));
      });
    }
  }

  public onEditAssetClick(): void {
    this.selectedRowAsset =
      this.gridApi && this.gridApi.getSelectedNodes().length > 0
        ? this.gridApi.getSelectedNodes()[0].data
        : null;
    if (this.selectedRowAsset) {
      const updatedAssetToCreateOrEdit: IAssetResult = {
        ...this.selectedRowAsset,
      };
      this.store.dispatch(
        actions.updateAssetToCreateOrEdit({ updatedAssetToCreateOrEdit })
      );

      let updatedAssetToCreateOrEditParentACTypeKey = '';
      let foundParent = false;
      this.assets$.pipe(take(1)).subscribe((assets: IAssetResult[]) => {
        const parent: IAssetResult | undefined = assets.find(
          (a) => a.AssetId === updatedAssetToCreateOrEdit.ParentAssetId
        );
        if (parent) {
          updatedAssetToCreateOrEditParentACTypeKey = parent.AssetClassTypeKey;
          foundParent = true;
        }
      });
      if (!foundParent) {
        // we're editing the top-level asset in the grid, so
        // get the parent from the Navigator
        this.store
          .select(selectSelectedAssetTreeNodeParent)
          .pipe(take(1))
          .subscribe((asset: any) => {
            updatedAssetToCreateOrEditParentACTypeKey =
              asset.Asset.AssetClassType.AssetClassTypeKey;
          });
      }

      const dialogConfig: MatDialogConfig = new MatDialogConfig();
      dialogConfig.height = '710px';
      dialogConfig.width = '750px';
      dialogConfig.data = {
        type: 'edit',
        parentAsset: this.selectedRowAsset,
        referenceId: this.referenceId,
        parentACTypeKey: updatedAssetToCreateOrEditParentACTypeKey,
      };

      const dialogRef = this.dialog.open(
        CreateAssetDialogComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe((asset: IAssetCreateUpdateResult) => {
        if (asset == null) {
          return;
        }
        const updatedAsset: IAssetResult =
          this.assetCreateUpdateResultToAssetResult(asset);
        this.store.dispatch(actions.updateSingleAsset({ updatedAsset }));
      });
    }
  }

  public onCopyClick(): void {
    this.gridApi?.selectAll();
    this.gridApi?.copySelectedRowsToClipboard({
      includeHeaders: true,
      columnKeys: [
        'AssetPath',
        'AssetName',
        'AssetDesc',
        'AssetClassTypeKey',
        'AssetId',
      ],
    });
    this.gridApi?.deselectAll();
    this.store.dispatch(
      showSuccessMessage({ message: 'Assets are copied to Clipboard' })
    );
  }

  public onPasteClick(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '758px';
    dialogConfig.height = '650px';
    dialogConfig.data = {
      existingAssetIds: this.getAllGridAssets().map(
        (asset: IAssetResult) => asset.AssetId
      ),
      referenceId: this.referenceId,
      parentToChildAssets: groupBy(this.getAllGridAssets(), 'AssetPath'),
    };

    const dialogRef = this.dialog.open(AssetPasteDialogComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .subscribe((createdAssets: IAssetCreateUpdateResult[]) => {
        if (!createdAssets || createdAssets.length === 0) {
          return;
        }
        const assets: IAssetResult[] = [];
        for (const createdAsset of createdAssets) {
          assets.push(this.assetCreateUpdateResultToAssetResult(createdAsset));
        }
        this.store.dispatch(actions.insertAssets({ assets }));
      });
  }

  public onDeleteAssetClick(): void {
    if (this.selectedRowAsset) {
      const selectedAssetPath: string =
        this.selectedRowAsset.Hierarchy.length === 1
          ? `/${this.selectedRowAsset.AssetName}`
          : `${this.selectedRowAsset.AssetPath}/${this.selectedRowAsset.AssetName}`;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.width = '350px';
      dialogConfig.height = '300px';
      dialogConfig.data = {
        assetName: this.selectedRowAsset.AssetName,
        numOfDescendants: this.getAssetDescendantsCount(selectedAssetPath),
      };

      const dialogRef = this.dialog.open(
        AssetDeleteConfirmationDialogComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe((exit: boolean) => {
        if (exit === true && this.selectedRowAsset) {
          this.store.dispatch(
            actions.deleteAsset({
              assetIdToDelete: this.selectedRowAsset.AssetId,
              selectedAssetPath,
            })
          );
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.selectedAssetObject$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetObject: any) => {
        if (assetObject?.AssetGuid) {
          this.referenceId = assetObject.AssetGuid;
          this.store.dispatch(
            updateRoute({
              asset: this.referenceId,
              tab: assetTabFeatureKey,
            })
          );
        }
        const asset: any = assetObject?.Asset ?? null;
        if (asset) {
          // if the selected asset is AssetType 1 or 2
          // we force the selected asset only toggle to true and disable it
          if ([1, 2].includes(asset.AssetTypeID)) {
            this.store.dispatch(toggleSelectedAssetOnly({ value: true }));
          } else {
            this.store.dispatch(actions.getAssets());
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private getFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      const filters: any[] = [];

      Object.keys(gridFilters).forEach((key: string) => {
        const colDef: ColDef | null | undefined =
          this.gridApi?.getColumnDef(key);
        if (colDef) {
          filters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef?.headerName,
          });
        }
      });

      this.store.dispatch(actions.setFilters({ filters }));
    }
  }

  private refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  private postRemoveFilterCallbackFn(filterName: string): void {
    if (this.gridApi) {
      this.gridApi.getFilterInstance(filterName)?.setModel(null);
      this.refreshGrid();
    }
  }

  private removeHover(): void {
    if (this.targetNode != null) {
      const element: NodeList = this.el.nativeElement.querySelectorAll(
        `[row-id="${this.targetNode.id}"]`
      )[1];
      this.renderer.removeClass(element, 'ag-row-hover');
      this.targetNode = null;
    }
  }

  private getAllGridAssets(): IAssetResult[] {
    const assets: IAssetResult[] = [];
    this.gridApi?.forEachNode((node: RowNode) => assets.push(node.data));
    return assets;
  }

  private getAssetDescendants(selectedAssetPath: string): IAssetResult[] {
    return this.getAllGridAssets().filter((asset: IAssetResult) =>
      asset.AssetPath.includes(selectedAssetPath)
    );
  }

  private getAssetDescendantsCount(selectedAssetPath: string): number {
    return this.getAssetDescendants(selectedAssetPath).length;
  }

  private assetResultToAssetUpdate(asset: IAssetResult): IAssetUpdate {
    return {
      AssetId: asset.AssetId,
      ParentAssetId: asset.ParentAssetId,
      AssetName: asset.AssetName,
      AssetDesc: asset.AssetDesc,
      AssetClassTypeKey: asset.AssetClassTypeKey,
      DisplayOrder: asset.DisplayOrder,
    } as IAssetUpdate;
  }

  private assetCreateUpdateResultToAssetResult(
    asset: IAssetCreateUpdateResult
  ): IAssetResult {
    return {
      AssetId: asset.AssetId,
      AssetName: asset.AssetName,
      AssetDesc: asset.AssetDesc,
      AssetClassTypeKey: asset.AssetClassTypeKey,
      AssetClassTypeDesc: asset.AssetClassTypeDesc,
      ParentName: asset.ParentName,
      ParentAssetId: asset.ParentAssetId,
      AssetPath: asset.AssetPath,
      DisplayOrder: asset.DisplayOrder,
      IsStandard: asset.IsStandard,
      CreateDate: asset.CreateDate,
      ChangeDate: asset.ChangeDate,
      Hierarchy: [...asset.Hierarchy],
    } as IAssetResult;
  }
}
