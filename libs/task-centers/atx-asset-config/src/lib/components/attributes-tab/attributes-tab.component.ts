/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { selectSelectedAssetObject } from '../../store/selectors/asset-config.selectors';
import * as actions from '../../store/actions/attributes-tab.actions';
import {
  toggleSelectedAssetOnly,
  showSuccessMessage,
  showErrorMessage,
  updateRoute,
} from '../../store/actions/asset-config.actions';
import {
  AttributeDataAvailable,
  attributesTabFeatureKey,
} from '../../store/reducers/attributes-tab.reducer';
import {
  AttributesViewModel,
  selectAttributes,
  selectAttributesState,
  selectAttributesViewModel,
} from '../../store/selectors/attributes-tab.selectors';
import { StandardAttributeFormatterComponent } from '../standard-attribute-formatter/standard-attribute-formatter.component';
import { FavoriteAttributeFormatterComponent } from '../favorite-attribute-formatter/favorite-attribute-formatter.component';
import { AttributeDeleteConfirmationDialogComponent } from '../attribute-delete-confirmation-dialog/attribute-delete-confirmation-dialog.component';
import { AttributesPasteDialogComponent } from '../attributes-paste-dialog/attributes-paste-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  ClipboardModule,
  GridOptions,
  GridReadyEvent,
  FilterChangedEvent,
  GridApi,
  ColDef,
  ValueGetterParams,
  CellEditorSelectorResult,
  ICellEditorParams,
} from '@ag-grid-enterprise/all-modules';
import { IAttribute } from '../../models/attribute-type';
import { dateComparator, isNil } from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';

import {
  AttributesTabService,
  IValidationResult,
} from '../../service/attributes-tab.service';

@Component({
  selector: 'atx-asset-config-attributes-tab',
  templateUrl: './attributes-tab.component.html',
  styleUrls: ['./attributes-tab.component.scss'],
})
export class AttributesTabComponent implements AfterViewInit, OnDestroy {
  @ViewChild('menuBar') menuBar: ElementRef | any = null;
  public selectedAttributes: IAttribute[] = [];
  public attributesViewModel$: Observable<AttributesViewModel> =
    this.store.select(selectAttributesViewModel);
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];
  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'multiple',
    rowMultiSelectWithClick: false,
    singleClickEdit: true,
    rowHeight: 30,
    suppressClipboardPaste: true,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
    },
    treeData: false,
    columnDefs: [
      {
        headerName: 'Favorite',
        colId: 'IsFavorite',
        field: 'IsFavorite',
        cellRenderer: 'favoriteRenderer',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.IsFavorite ?? false);
        },
        width: 65,
        headerTooltip: 'Favorite',
      },
      {
        headerName: 'Asset ID',
        colId: 'AssetID',
        field: 'AssetId',
        hide: true,
        suppressColumnsToolPanel: true,
      },
      {
        headerName: 'Asset Name',
        colId: 'AssetName',
        field: 'AssetName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Attribute Name',
        colId: 'AttributeName',
        field: 'Name',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Attribute Value',
        colId: 'AttributeValue',
        field: 'Value',
        filter: 'agTextColumnFilter',
        editable: true,
        cellEditorSelector: (
          params: ICellEditorParams
        ): CellEditorSelectorResult => {
          if (
            (params.data as IAttribute).ValueType === 'DiscreteList' &&
            (params.data as IAttribute).ValueOptions.length > 0
          ) {
            return {
              component: 'agSelectCellEditor',
              params: {
                values: [...(params.data as IAttribute).ValueOptions],
              },
            };
          } else {
            return {
              component: 'agTextCellEditor',
            };
          }
        },
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAttribute;
          newValue: string;
        }) => {
          const validationResult: IValidationResult =
            this.attributesTabService.validate(data, newValue, 'Value');
          if (
            validationResult.IsValid &&
            typeof validationResult.ValidValue === 'string'
          ) {
            const attributeToUpdate: IAttribute = {
              ...data,
              Value: validationResult.ValidValue,
            };
            this.store.dispatch(
              actions.updateAttributes({
                attributesToUpdate: [attributeToUpdate],
              })
            );
          } else {
            let message = 'Please enter a valid Attribute value.';
            if (data.ValueType == 'DiscreteList') {
              message += 'Valid values: ' + data.ValueOptions.join();
            }
            this.store.dispatch(
              showErrorMessage({
                message: message,
              })
            );
          }
          return false;
        },
      },
      {
        headerName: 'Change Date',
        colId: 'ChangeDate',
        field: 'ChangeDate',
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        headerName: 'Display Order',
        colId: 'DisplayOrder',
        field: 'DisplayOrder',
        filter: 'agNumberColumnFilter',
        editable: true,
        width: 100,
        headerTooltip: 'Display Order',
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAttribute;
          newValue: string;
        }) => {
          const validationResult: IValidationResult =
            this.attributesTabService.validate(data, newValue, 'DisplayOrder');
          if (
            validationResult.IsValid &&
            typeof validationResult.ValidIntValue === 'number'
          ) {
            const attributeToUpdate: IAttribute = {
              ...data,
              DisplayOrder: validationResult.ValidIntValue,
            };
            this.store.dispatch(
              actions.updateAttributes({
                attributesToUpdate: [attributeToUpdate],
              })
            );
          } else {
            this.store.dispatch(
              showErrorMessage({
                message: 'Please enter a valid Display Order value.',
              })
            );
          }
          return false;
        },
      },
      {
        headerName: 'IsStandard',
        colId: 'IsStandard',
        field: 'IsStandard',
        cellRenderer: 'standardRenderer',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.IsStandard ?? false);
        },
        width: 100,
      },
      {
        headerName: 'Attribute Units',
        colId: 'AttributeUnits',
        field: 'EngUnit',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Id',
        colId: 'Id',
        field: 'Id',
        hide: true,
        suppressColumnsToolPanel: true,
      },
      {
        headerName: 'Value Type',
        colId: 'ValueType',
        field: 'ValueType',
      },
      {
        headerName: 'Value Options',
        colId: 'ValueOptions',
        valueGetter: (params: ValueGetterParams) => {
          return params.data.ValueOptions.join();
        },
        hide: true,
        suppressColumnsToolPanel: true,
      },
      {
        headerName: 'Asset Path',
        colId: 'AssetPath',
        field: 'AssetPath',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Asset Type',
        colId: 'AssetType',
        field: 'AssetClassTypeKey',
        filter: 'agTextColumnFilter',
      },
    ],
    suppressAggFuncInHeader: true,
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    suppressScrollOnNewData: true,
    components: {
      standardRenderer: StandardAttributeFormatterComponent,
      favoriteRenderer: FavoriteAttributeFormatterComponent,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.gridApi.hideOverlay();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
    onSelectionChanged: () => {
      if (this.gridApi) {
        this.selectedAttributes = this.gridApi
          .getSelectedRows()
          .map<IAttribute>((r: any) => {
            return r as IAttribute;
          });
      }
      this.menuBar?.nativeElement.click();
    },
  };
  private gridApi: GridApi | null = null;
  private enableFloatingFilter = true;
  private attributes$: Observable<IAttribute[]> =
    this.store.select(selectAttributes);
  public attributesState$: Observable<AttributeDataAvailable> =
    this.store.select(selectAttributesState);
  public selectedAssetObject$: Observable<any> = this.store.select(
    selectSelectedAssetObject
  );
  private referenceId = '';
  private onDestroy$ = new Subject<void>();

  constructor(
    private store: Store,
    private attributesTabService: AttributesTabService,
    private dialog: MatDialog
  ) {
    this.attributes$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((attributes: IAttribute[]) => {
        if (this.gridApi) {
          this.gridApi.setRowData(attributes);
          this.gridApi.refreshCells({ force: true });
        }
      });

    this.attributesState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: AttributeDataAvailable) => {
        if (this.gridApi) {
          if (data === 'loading') {
            this.gridApi.showLoadingOverlay();
          } else if (data === 'nodata') {
            this.gridApi.showNoRowsOverlay();
          } else {
            this.gridApi.hideOverlay();
          }
        }
      });
  }

  public clearFilters(_: any): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

      Object.keys(gridFilters).forEach((key: string) => {
        this.gridApi?.getFilterInstance(key)?.setModel(null);
      });

      this.store.dispatch(actions.clearFilters());
      this.refreshGrid();
    }
  }

  public removeFilter(filter: any): void {
    this.store.dispatch(
      actions.removeFilter({
        filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  private getFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      const filters: any[] = [];

      Object.keys(gridFilters).forEach((key: string) => {
        const colDef: ColDef | null | undefined =
          this.gridApi?.getColumnDef(key);
        if (colDef) {
          filters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef?.headerName,
          });
        }
      });

      this.store.dispatch(actions.setFilters({ filters }));
    }
  }

  private refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  private postRemoveFilterCallbackFn(filterName: string): void {
    if (this.gridApi) {
      this.gridApi.getFilterInstance(filterName)?.setModel(null);
      this.refreshGrid();
    }
  }

  public toggleFloatingFilter(): void {
    if (this.gridApi) {
      const columnDefs: ColDef[] | undefined = this.gridApi.getColumnDefs();
      if (columnDefs) {
        this.enableFloatingFilter = !this.enableFloatingFilter;
        columnDefs.forEach((colDef: ColDef) => {
          colDef.floatingFilter = this.enableFloatingFilter;
        });
        this.gridApi.setColumnDefs(columnDefs);
      }
    }
  }

  public refresh(): void {
    this.store.dispatch(actions.getAttributes());
  }

  public onCopyClick(): void {
    if (this.gridApi) {
      this.gridApi.selectAll();
      this.gridApi.copySelectedRowsToClipboard({
        includeHeaders: true,
        columnKeys: [
          'AssetID',
          'AssetName',
          'AttributeName',
          'AttributeValue',
          'ChangeDate',
          'IsFavorite',
          'DisplayOrder',
          'IsStandard',
          'AttributeUnits',
          'ValueType',
          'ValueOptions',
          'AssetPath',
          'AssetType',
        ],
      });
      this.gridApi.deselectAll();
      this.store.dispatch(
        showSuccessMessage({ message: 'Attributes are copied to Clipboard' })
      );
    }
  }

  public onPasteClick(): void {
    const attributeData: {
      [key: string]: {
        [key: string]: IAttribute;
      };
    } = {};
    this.attributes$.pipe(take(1)).subscribe((attributes: IAttribute[]) => {
      for (const attribute of attributes) {
        if (!attributeData[attribute.AssetId]) {
          attributeData[attribute.AssetId] = {};
        }
        attributeData[attribute.AssetId][attribute.Name] = attribute;
      }
    });
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '800px';
    dialogConfig.height = '750px';
    dialogConfig.data = {
      existingAttributesDict: attributeData,
      referenceId: this.referenceId,
    };

    const dialogRef = this.dialog.open(
      AttributesPasteDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((upsertedAttributes: IAttribute[]) => {
      if (!upsertedAttributes || upsertedAttributes.length === 0) {
        return;
      }
      this.store.dispatch(actions.getAttributes());
    });
  }

  public onDeleteClick(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.height = '300px';
    const dialogRef = this.dialog.open(
      AttributeDeleteConfirmationDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true && this.selectedAttributes.length > 0) {
        this.store.dispatch(
          actions.deleteAttributes({
            attributesToDelete: this.selectedAttributes,
          })
        );
        this.selectedAttributes = [];
      }
    });
  }

  ngAfterViewInit(): void {
    this.selectedAssetObject$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetObject: any) => {
        if (assetObject?.AssetGuid) {
          this.referenceId = assetObject.AssetGuid;
          this.store.dispatch(
            updateRoute({
              asset: this.referenceId,
              tab: attributesTabFeatureKey,
            })
          );
        }
        const asset: any = assetObject?.Asset ?? null;
        if (asset) {
          // if the selected asset is AssetType 1 or 2
          // we force the selected asset only toggle to true and disable it
          if ([1, 2].includes(asset.AssetTypeID)) {
            this.store.dispatch(toggleSelectedAssetOnly({ value: true }));
          } else {
            this.store.dispatch(actions.getAttributes());
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
