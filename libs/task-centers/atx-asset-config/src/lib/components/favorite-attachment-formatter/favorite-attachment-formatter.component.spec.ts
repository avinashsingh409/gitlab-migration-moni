/* eslint-disable ngrx/use-consistent-global-store-name */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { Store } from '@ngrx/store';

import { FavoriteAttachmentFormatterComponent } from './favorite-attachment-formatter.component';

describe('FavoriteAttachmentFormatterComponent', () => {
  let component: FavoriteAttachmentFormatterComponent;
  let fixture: ComponentFixture<FavoriteAttachmentFormatterComponent>;
  let mockStore: Store;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: Store, useValue: mockStore },
      ],
      declarations: [FavoriteAttachmentFormatterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteAttachmentFormatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
