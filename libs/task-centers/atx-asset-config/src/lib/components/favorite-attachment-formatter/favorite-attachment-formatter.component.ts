/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { IAttachment } from '../../models/attachment-type';
import { Store } from '@ngrx/store';
import { upsertAttachment } from '../../store/actions/attachments-tab.actions';

@Component({
  selector: 'atx-favorite-attachment-formatter',
  template:
    '<mat-icon style="cursor: pointer" (click)="onIconClick()">{{favoriteAttachmentIcon}}</mat-icon>',
})
export class FavoriteAttachmentFormatterComponent
  implements ICellRendererAngularComp
{
  favoriteAttachmentIcon: 'star' | 'star_border' | undefined;
  private attachment: IAttachment | undefined;

  constructor(private store: Store) {}

  agInit(params: any) {
    this.favoriteAttachmentIcon = params?.data?.IsFavorite
      ? 'star'
      : 'star_border';
    this.attachment = params?.data;
  }

  refresh(params: any) {
    this.favoriteAttachmentIcon = params?.data?.IsFavorite
      ? 'star'
      : 'star_border';
    return true;
  }

  onIconClick() {
    if (this.attachment) {
      const attachmentToUpsert: IAttachment = {
        ...this.attachment,
        IsFavorite: !this.attachment.IsFavorite,
      };
      this.store.dispatch(
        upsertAttachment({
          attachmentToUpsert,
          file: undefined,
          action: 'update attachment',
        })
      );
    }
  }

  afterGuiAttached(_?: IAfterGuiAttachedParams): void {}
}
