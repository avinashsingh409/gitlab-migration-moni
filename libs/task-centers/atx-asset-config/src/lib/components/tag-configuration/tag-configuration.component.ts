/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import {
  selectTagConfigViewModel,
  TagConfigViewModel,
  selectAllTags,
} from '../../store/selectors/tags-tab.selectors';
import * as actions from '../../store/actions/tags-tab.actions';
import * as assetConfigActions from '../../store/actions/asset-config.actions';
import * as selectors from '../../store/selectors/tags-tab.selectors';
import { Clipboard } from '@angular/cdk/clipboard';
import {
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  ClipboardModule,
  GridOptions,
  GridReadyEvent,
  GridApi,
  ColDef,
  FilterChangedEvent,
  ColGroupDef,
  CellClassParams,
} from '@ag-grid-enterprise/all-modules';
import {
  ITagResult,
  dateComparator,
  isNil,
  ITagCreateUpdateResult,
  IServerResult,
} from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';
import { DeleteColumnComponent } from '../delete-column/delete-column.component';
import { TagsDataAvailable } from '../../store/reducers/tags-tab.reducer';
import { takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TagsPasteDialogComponent } from '../tags-paste-dialog copy/tags-paste-dialog.component';

@Component({
  selector: 'atx-tag-configuration',
  templateUrl: './tag-configuration.component.html',
  styleUrls: ['./tag-configuration.component.scss'],
})
export class TagConfigurationComponent implements OnDestroy {
  @ViewChild('gridBody') gridBody: ElementRef | any = null;
  @Input() asset = '';
  @Input() isTagConfigExpanded = false;
  @Output() expandFn = new EventEmitter<string>();
  @Output() collapseFn = new EventEmitter<string>();
  @Output() gridPostInitFn = new EventEmitter<GridApi>();
  public tagConfigViewModel$: Observable<TagConfigViewModel> =
    this.store.select(selectTagConfigViewModel);
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];
  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'multiple',
    rowMultiSelectWithClick: false,
    suppressRowClickSelection: false,
    suppressRowDeselection: false,
    suppressRowDrag: false,
    rowDragMultiRow: true,
    suppressMoveWhenRowDragging: true,
    rowDragManaged: false,
    rowDragEntireRow: true,
    rowHeight: 30,
    suppressClipboardPaste: true,
    components: {
      deleteButton: DeleteColumnComponent,
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      cellClassRules: {
        'rag-amber': (params: CellClassParams) => params.data.IsDirty,
      },
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    columnDefs: [
      {
        headerName: 'In Use',
        colId: 'InUse',
        editable: false,
        width: 90,
        sortable: true,
        filter: 'agNumberColumnFilter',
        field: 'InUse',
      },
      {
        headerName: 'Tag Name',
        colId: 'Name',
        field: 'Name',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: ITagResult; newValue: string }) => {
          if (this.validate('Name', value.newValue)) {
            this.store.dispatch(
              actions.changeTag({
                tag: {
                  ...value.data,
                  Name: value.newValue,
                  IsDirty: true,
                },
              })
            );
          }
          return false;
        },
        width: 310,
      },
      {
        headerName: 'Tag Description',
        colId: 'Description',
        field: 'Description',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: ITagResult; newValue: string }) => {
          if (this.validate('Description', value.newValue)) {
            this.store.dispatch(
              actions.changeTag({
                tag: {
                  ...value.data,
                  Description: value.newValue,
                  IsDirty: true,
                },
              })
            );
          }
          return false;
        },
        width: 390,
      },
      {
        headerName: 'Tag Units',
        colId: 'EngUnit',
        field: 'EngUnit',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: ITagResult; newValue: string }) => {
          if (this.validate('EngUnit', value.newValue)) {
            this.store.dispatch(
              actions.changeTag({
                tag: {
                  ...value.data,
                  EngUnit: value.newValue,
                  IsDirty: true,
                },
              })
            );
          }
          return false;
        },
        width: 104,
      },
      {
        headerName: 'Tag Active',
        colId: 'ExistsOnServer',
        field: 'ExistsOnServer',
        editable: false,
        filter: 'agTextColumnFilter',
        width: 90,
      },
      {
        headerName: 'Qualifier',
        colId: 'Qualifier',
        field: 'Qualifier',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: ITagResult; newValue: string }) => {
          if (this.validate('Qualifier', value.newValue)) {
            this.store.dispatch(
              actions.changeTag({
                tag: {
                  ...value.data,
                  Qualifier: value.newValue,
                  IsDirty: true,
                },
              })
            );
          }
          return false;
        },
        width: 90,
      },
      {
        headerName: 'Source ID',
        colId: 'Source',
        field: 'Source',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: ITagResult; newValue: string }) => {
          if (this.validate('Source', value.newValue)) {
            this.store.dispatch(
              actions.changeTag({
                tag: {
                  ...value.data,
                  Source: value.newValue,
                  IsDirty: true,
                },
              })
            );
          }
          return false;
        },
        width: 276,
      },
      {
        headerName: 'Tag ID',
        colId: 'Id',
        field: 'Id',
        editable: false,
        hide: true,
        suppressColumnsToolPanel: true,
      },
      {
        headerName: 'Create Date',
        colId: 'CreateDate',
        field: 'CreateDate',
        editable: false,
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
        sort: 'desc',
      },
      {
        headerName: 'Change Date',
        colId: 'ChangeDate',
        field: 'ChangeDate',
        editable: false,
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        headerName: '',
        colId: 'deleteButton',
        editable: false,
        cellRenderer: 'deleteButton',
        width: 44,
        sortable: false,
      },
    ],
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.gridApi.hideOverlay();
      this.gridPostInitFn.emit(this.gridApi);

      this.gridBody.nativeElement.addEventListener(
        'selectstart',
        (e: Event) => {
          e.preventDefault();
          return false;
        }
      );
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
    getRowId: (params) => {
      return params.data.Id;
    },
    sendToClipboard: (params: any) => {
      let data = params.data?.replace(/\r\r\n/gm, '\r\n'); // This will negate additional new line

      const matches = data.match(/"[a-zA-Z0-9"]+/gm); // This will search for strings that begins with double quotes
      if (matches?.length > 0) {
        matches.map((match: any) => {
          data = data.replace(match, '""""' + match.substring(1));
        });
      }

      const copied = this.clipboard.copy(data);
      if (copied) {
        this.store.dispatch(
          assetConfigActions.showSuccessMessage({
            message: 'Tags are copied to Clipboard',
          })
        );
      } else {
        this.store.dispatch(
          assetConfigActions.showErrorMessage({
            message:
              'Copied selection exceeds maximum. Filter a smaller data set and try again.',
          })
        );
      }
    },
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
  };
  private gridApi: GridApi | any = null;
  private selectedServer$: Observable<IServerResult | null> = this.store.select(
    selectors.selectSelectedServer
  );
  private selectedServer: IServerResult | null = null;
  private validationStatus$: Observable<string> = this.store.select(
    selectors.selectValidationStatus
  );
  private tagsState$: Observable<TagsDataAvailable> = this.store.select(
    selectors.selectTagsState
  );
  private tags$: Observable<ITagResult[]> = this.store.select(
    selectors.selectTags
  );
  private allTags$: Observable<ITagResult[]> = this.store.select(
    selectors.selectAllTags
  );
  private allTags: ITagResult[] = [];
  private onDestroy$ = new Subject<void>();
  private enableFloatingFilter = true;

  constructor(
    private store: Store,
    private clipboard: Clipboard,
    private dialog: MatDialog
  ) {
    this.validationStatus$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((message: string) => {
        if (message) {
          this.store.dispatch(assetConfigActions.showInfoMessage({ message }));
        }
      });

    this.tags$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: ITagResult[]) => {
        if (this.gridApi) {
          this.gridApi.setRowData(data);
          this.gridApi.refreshCells({ force: true });
        }
      });

    this.allTags$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((allTags: ITagResult[]) => {
        this.allTags = [...allTags];
      });

    this.tagsState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: TagsDataAvailable) => {
        if (this.gridApi) {
          if (data === 'loading') {
            this.gridApi.showLoadingOverlay();
          } else if (data === 'nodata') {
            this.gridApi.showNoRowsOverlay();
          } else {
            this.gridApi.hideOverlay();
          }
        }
      });

    this.selectedServer$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((selectedServer: IServerResult | null) => {
        this.selectedServer = selectedServer;
      });
  }

  addTag(): void {
    this.store.dispatch(actions.addTag());
  }

  copyTags(): void {
    this.gridApi.selectAll();
    this.gridApi.copySelectedRowsToClipboard({
      includeHeaders: true,
      columnKeys: [
        'Id',
        'Name',
        'Description',
        'EngUnit',
        'Qualifier',
        'Source',
        'ExistsOnServer',
        'InUse',
        'ChangeDate',
        'CreateDate',
      ],
    });
    this.gridApi.deselectAll();
  }

  showDeletedTagsChange(change: { checked: boolean }): void {
    this.store.dispatch(
      actions.showDeletedTagsChange({ newValue: change.checked })
    );
  }

  clearFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      Object.keys(gridFilters).forEach((key: string) => {
        this.gridApi?.getFilterInstance(key)?.setModel(null);
      });

      this.store.dispatch(actions.clearTagFilters());
      this.refreshGrid();
    }
  }

  removeFilter(filter: any): void {
    this.store.dispatch(
      actions.removeTagFilter({
        filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  toggleFloatingFilter(): void {
    if (this.gridApi) {
      const columnDefs: (ColDef | ColGroupDef)[] | undefined =
        this.gridApi.getColumnDefs();
      if (columnDefs) {
        this.enableFloatingFilter = !this.enableFloatingFilter;
        columnDefs.forEach((colDef: ColDef) => {
          colDef.floatingFilter = this.enableFloatingFilter;
        });
        this.gridApi.setColumnDefs(columnDefs);
      }
    }
  }

  refresh(): void {
    this.store.dispatch(actions.discardChanges());
  }

  public undo(): void {
    this.store.dispatch(actions.discardChanges());
  }

  public save(): void {
    this.store.dispatch(actions.saveTags());
  }

  public expand(): void {
    this.expandFn.emit('config');
  }

  public collapse(): void {
    this.collapseFn.emit('config');
  }

  public onPasteClick(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '800px';
    dialogConfig.height = '750px';
    dialogConfig.data = {
      tagIdInUseAndActiveByNameDict:
        this.getTagGlobalIdInUseAndActiveByNameDict(),
      serverId: this.selectedServer?.Id ?? '',
    };

    const dialogRef = this.dialog.open(TagsPasteDialogComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .subscribe((createdOrUpdatedTags: ITagCreateUpdateResult[]) => {
        if (!createdOrUpdatedTags || createdOrUpdatedTags.length === 0) {
          return;
        }
        this.store.dispatch(actions.refreshTags());
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private getFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      const filters: any[] = [];

      Object.keys(gridFilters).forEach((key: string) => {
        const colDef: ColDef | null | undefined =
          this.gridApi?.getColumnDef(key);
        if (colDef) {
          filters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef?.headerName,
          });
        }
      });

      this.store.dispatch(actions.setTagFilters({ filters }));
    }
  }

  private refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  private postRemoveFilterCallbackFn(filterName: string): void {
    if (this.gridApi) {
      this.gridApi.getFilterInstance(filterName)?.setModel(null);
      this.refreshGrid();
    }
  }

  private getTagGlobalIdInUseAndActiveByNameDict(): {
    [key: string]: [string, number];
  } {
    return Object.assign(
      {},
      ...this.allTags.map((tag: ITagResult) => ({
        [tag.Name]: [tag.Id, tag.InUse, tag.ExistsOnServer],
      }))
    );
  }

  private validate(propertyName: string, candidateValue: string): boolean {
    let valid = false;
    candidateValue = candidateValue.replace(/(\r\n|\n|\r)/gm, '');
    this.store
      .select(selectAllTags)
      .pipe(take(1))
      .subscribe((tags: ITagResult[]) => {
        // names must be unique
        // property values cannot exceed length
        //  Name (400)
        //  Description (400)
        //  EngUnit (255)
        //  Qualifier (256)
        //  Source (400)
        switch (propertyName) {
          case 'Description':
          case 'Source':
          case 'Name': {
            let equalNames: ITagResult[] = [];
            let notEmptyString = false;
            if (propertyName === 'Name') {
              equalNames = tags.filter(
                (tag: ITagResult) => tag.Name === candidateValue
              );
              notEmptyString =
                candidateValue !== '' && candidateValue.trim() !== '';
            } else {
              notEmptyString = true;
            }
            if (
              equalNames.length === 0 &&
              candidateValue.length <= 400 &&
              notEmptyString
            ) {
              valid = true;
            }
            break;
          }
          case 'EngUnit': {
            if (candidateValue.length <= 255) {
              valid = true;
            }
            break;
          }
          case 'Qualifier': {
            if (candidateValue.length <= 256) {
              valid = true;
            }
            break;
          }
          default: {
            valid = true;
          }
        }
        if (!valid) {
          this.store.dispatch(
            assetConfigActions.showWarningMessage({
              message: `Invalid ${propertyName}: ${candidateValue}`,
            })
          );
        }
      });
    return valid;
  }
}
