/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  Renderer2,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import * as actions from '../../store/actions/tags-tab.actions';
import {
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  ClipboardModule,
  GridOptions,
  GridReadyEvent,
  GridApi,
  ICellRendererParams,
  RowNode,
  RowDragEndEvent,
  FilterChangedEvent,
  ColDef,
  ColGroupDef,
  ValueGetterParams,
  CellFocusedEvent,
  ITooltipParams,
  RowDragMoveEvent,
  RowDragEvent,
} from '@ag-grid-enterprise/all-modules';
import { ITagResult, IPDTagMap, isNil } from '@atonix/atx-core';
import { DeleteTagMapColumnComponent } from '../delete-tagmap-column/delete-tagmap-column.component';
import { DeleteConfirmationDialogComponent } from '../delete-confirmation-dialog/delete-confirmation-dialog.component';
import {
  selectAllTagMaps,
  selectAllTags,
  selectTagMapsState,
  selectTagMapViewModel,
  TagMapViewModel,
} from '../../store/selectors/tags-tab.selectors';
import { takeUntil } from 'rxjs/operators';
import { sortBy } from 'lodash';
import { TagMapsDataAvailable } from '../../store/reducers/tags-tab.reducer';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { showSuccessMessage } from '../../store/actions/asset-config.actions';
import { TagMapPasteDialogComponent } from '../tagmaps-paste-dialog/tagmaps-paste-dialog.component';

@Component({
  selector: 'atx-tag-map',
  templateUrl: './tag-map.component.html',
  styleUrls: ['./tag-map.component.scss'],
})
export class TagMapComponent implements OnChanges, OnDestroy {
  @ViewChild('gridBody') gridBody: ElementRef | any = null;
  @ViewChild('menuBar') menuBar: ElementRef | any = null;
  @Input() asset = '';
  @Input() isTagMapExpanded = false;
  @Input() tagConfigGridApi: GridApi | null = null;
  @Output() expandFn = new EventEmitter<string>();
  @Output() collapseFn = new EventEmitter<string>();
  public tagMapViewModel$: Observable<TagMapViewModel> = this.store.select(
    selectTagMapViewModel
  );
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];
  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'multiple',
    rowMultiSelectWithClick: false,
    suppressRowClickSelection: false,
    suppressRowDeselection: false,
    suppressRowDrag: false,
    rowDragMultiRow: true,
    suppressMoveWhenRowDragging: true,
    rowDragManaged: false,
    rowDragEntireRow: true,
    rowHeight: 30,
    onRowDragEnd: (event: RowDragEndEvent) => {
      this.removeHover();

      // check if the node dragged is a TagMap
      if (event.overNode && event.node.data?.AssetPath !== undefined) {
        let tagMaps: IPDTagMap[] = sortBy(event.nodes, 'rowIndex').map(
          (node: RowNode) => node.data
        );
        if (
          tagMaps.length === 0 ||
          (tagMaps.length > 0 && !tagMaps.includes(event.node.data))
        ) {
          // if dragged tagMap is not included as one of the selected, only include the dragged
          tagMaps = [event.node.data];
        }
        this.store.dispatch(
          actions.updateTagMaps({
            selectedTagMap: event.overNode.data,
            draggedTagMaps: tagMaps,
          })
        );
      }
    },
    onRowDragMove: (params: RowDragEvent) => {
      if (this.gridApi != null && params.overNode != null) {
        this.removeHover();
        const element: NodeList = this.el.nativeElement.querySelectorAll(
          `[row-id="${params.overNode.id}"]`
        )[1];
        this.renderer.addClass(element, 'ag-row-hover');
        this.targetNode = params.overNode;
      } else {
        this.removeHover();
      }
    },
    onRowDragLeave: () => {
      this.removeHover();
    },
    components: {
      deleteButton: DeleteTagMapColumnComponent,
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      sortable: true,
      enableRowGroup: false,
      enablePivot: false,
      enableValue: false,
      editable: false,
    },
    treeData: true,
    groupDefaultExpanded: 0,
    getDataPath: (data: IPDTagMap) => data.Hierarchy,
    excludeChildrenWhenTreeDataFiltering: true,
    autoGroupColumnDef: {
      headerName: 'Maps',
      maxWidth: 100,
      cellRendererParams: {
        suppressCount: true,
        innerRenderer: this.getGroupRenderer(),
      },
      tooltipValueGetter: (params: ITooltipParams) =>
        params.node?.allChildrenCount &&
        params.node?.allChildrenCount + 1 >= 1000
          ? `(${params.node?.allChildrenCount + 1})`
          : '',
    },
    onCellFocused: (event: CellFocusedEvent) => {
      this.gridOptions.suppressRowClickSelection =
        event.column != null && event.column.getColId() === 'deleteButton';
    },
    columnDefs: [
      {
        headerName: 'Full Path',
        colId: 'FullPath',
        field: 'FullPath',
        filter: 'agTextColumnFilter',
        hide: true,
        floatingFilter: true,
      },
      {
        headerName: 'Asset ID',
        colId: 'AssetId',
        field: 'AssetId',
        filter: 'agTextColumnFilter',
        hide: true,
        floatingFilter: true,
      },
      {
        headerName: 'Asset Name',
        colId: 'AssetName',
        field: 'AssetName',
        filter: 'agTextColumnFilter',
        tooltipValueGetter: (params: ITooltipParams) => params.data.AssetPath,
        floatingFilter: true,
      },
      {
        headerName: 'Asset Path',
        colId: 'AssetPath',
        field: 'AssetPath',
        filter: 'agTextColumnFilter',
        hide: true,
        cellClass: 'ag-rtl',
        floatingFilter: true,
      },
      {
        headerName: 'Asset Type',
        colId: 'AssetClassTypeKey',
        field: 'AssetClassTypeKey',
        filter: 'agTextColumnFilter',
        hide: true,
        floatingFilter: true,
      },
      {
        headerName: 'Variable',
        colId: 'VariableDesc',
        field: 'VariableDesc',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: [
            'contains',
            'notContains',
            'equals',
            'notEqual',
            'startsWith',
            'endsWith',
            {
              displayKey: 'none',
              displayName: '{none}',
              predicate: (_: any, cellValue: any) =>
                cellValue == null || cellValue === '{none}',
              hideFilterInput: true,
            },
          ],
          suppressAndOrCondition: true,
        },
        valueGetter: (params: ValueGetterParams) =>
          params.data.VariableDesc === '' ? '{none}' : params.data.VariableDesc,
        tooltipValueGetter: (params: ITooltipParams) => params.data.AssetPath,
        floatingFilter: true,
      },
      {
        headerName: 'Variable Units',
        colId: 'VariableTypeUnits',
        field: 'VariableTypeUnits',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: [
            'contains',
            'notContains',
            'equals',
            'notEqual',
            'startsWith',
            'endsWith',
            {
              displayKey: 'blank',
              displayName: 'Blank',
              predicate: (_: any, cellValue: any) =>
                cellValue == null || cellValue === '',
              hideFilterInput: true,
            },
          ],
          suppressAndOrCondition: true,
        },
        width: 150,
        floatingFilter: true,
      },
      {
        headerName: '',
        colId: 'deleteButton',
        cellRenderer: 'deleteButton',
        maxWidth: 60,
        sortable: false,
        hide: true,
      },
      {
        headerName: 'Server',
        colId: 'PDServerName',
        field: 'PDServerName',
        filter: 'agTextColumnFilter',
        hide: true,
        floatingFilter: true,
      },
      {
        headerName: 'Tag ID',
        colId: 'TagId',
        field: 'TagId',
        filter: 'agTextColumnFilter',
        hide: true,
        floatingFilter: true,
      },
      {
        headerName: 'Tag Name',
        colId: 'TagName',
        field: 'TagName',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: [
            'contains',
            'notContains',
            'equals',
            'notEqual',
            'startsWith',
            'endsWith',
            {
              displayKey: 'blank',
              displayName: 'Blank',
              predicate: (_: any, cellValue: any) =>
                cellValue == null || cellValue === '',
              hideFilterInput: true,
            },
          ],
          suppressAndOrCondition: true,
        },
        floatingFilter: true,
      },
      {
        headerName: 'Tag Description',
        colId: 'TagDescription',
        field: 'TagDescription',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: [
            'contains',
            'notContains',
            'equals',
            'notEqual',
            'startsWith',
            'endsWith',
            {
              displayKey: 'blank',
              displayName: 'Blank',
              predicate: (_: any, cellValue: any) =>
                cellValue == null || cellValue === '',
              hideFilterInput: true,
            },
          ],
          suppressAndOrCondition: true,
        },
        floatingFilter: true,
      },
      {
        headerName: 'Tag Units',
        colId: 'TagEngUnit',
        field: 'TagEngUnit',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: [
            'contains',
            'notContains',
            'equals',
            'notEqual',
            'startsWith',
            'endsWith',
            {
              displayKey: 'blank',
              displayName: 'Blank',
              predicate: (_: any, cellValue: any) =>
                cellValue == null || cellValue === '',
              hideFilterInput: true,
            },
          ],
          suppressAndOrCondition: true,
        },
        width: 150,
        floatingFilter: true,
      },
      {
        headerName: 'Value Type',
        colId: 'ValueTypeKey',
        field: 'ValueTypeKey',
        filter: 'agSetColumnFilter',
        editable: true,
        hide: false,
        filterParams: {
          values: ['Measured', 'Output', ''],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Measured', 'Output'],
        },
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IPDTagMap;
          newValue: string;
        }) => {
          if (data.ValueTypeKey !== newValue) {
            this.store.dispatch(
              actions.updateValueTypeOfTagMap({
                selectedTagMap: data,
                newValueType: newValue,
              })
            );
          }
          return false;
        },
        floatingFilter: true,
      },
      {
        headerName: 'Tag Active',
        colId: 'ExistsOnServer',
        field: 'ExistsOnServer',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        hide: true,
        valueGetter: (params: ValueGetterParams) =>
          params.data.ExistsOnServer === true ? 'true' : 'false',
        floatingFilter: true,
      },
    ],
    suppressAggFuncInHeader: true,
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.gridApi.hideOverlay();

      this.setInitialFilters();
      this.addGridDropZone();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
    getRowId: (params) => {
      return `${params.data.AssetPath}_${params.data.VariableDesc}_${params.data.TagName}`;
    },
    onSelectionChanged: () => {
      this.rowsSelected = this.mappedSelectedRows().length > 0 ? true : false;
      this.menuBar?.nativeElement.click();
    },
  };
  private gridApi: GridApi | null = null;
  private tagMaps$: Observable<IPDTagMap[]> =
    this.store.select(selectAllTagMaps);
  private tagMapsState$: Observable<TagMapsDataAvailable> =
    this.store.select(selectTagMapsState);
  private tags$: Observable<ITagResult[]> = this.store.select(selectAllTags);
  private tags: ITagResult[] = [];
  private enableFloatingFilter = true;
  private targetNode: RowNode | null = null;
  private onDestroy$ = new Subject<void>();
  public rowsSelected = false;
  private tagMapsState: TagMapsDataAvailable = '';

  constructor(
    private store: Store,
    private el: ElementRef,
    private renderer: Renderer2,
    private dialog: MatDialog
  ) {
    this.tagMapsState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: TagMapsDataAvailable) => {
        if (this.gridApi) {
          this.tagMapsState = data;
          if (data === 'loading') {
            this.gridApi.showLoadingOverlay();
          } else if (data === 'nodata') {
            this.gridApi.showNoRowsOverlay();
          } else {
            this.gridApi.hideOverlay();
          }
        }
      });

    this.tagMaps$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((tagMaps: IPDTagMap[]) => {
        if (this.gridApi && this.tagMapsState !== 'loading') {
          this.gridApi.setRowData(tagMaps);
          this.gridApi.refreshCells({ force: true });
        }
      });

    this.tags$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((tags: ITagResult[]) => {
        this.tags = [...tags];
      });
  }

  public expand(): void {
    this.expandFn.emit('mapping');
  }

  public collapse(): void {
    this.collapseFn.emit('mapping');
  }

  public clearFilters(event: any): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

      Object.keys(gridFilters).forEach((key: string) => {
        this.gridApi?.getFilterInstance(key)?.setModel(null);
      });

      this.store.dispatch(actions.clearTagMapFilters());
      this.refreshGrid();
    }
  }

  public removeFilter(filter: any): void {
    this.store.dispatch(
      actions.removeTagMapFilter({
        filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  public toggleFloatingFilter(): void {
    if (this.gridApi) {
      const columnDefs: (ColDef | ColGroupDef)[] | undefined =
        this.gridApi.getColumnDefs();
      if (columnDefs) {
        this.enableFloatingFilter = !this.enableFloatingFilter;
        columnDefs.forEach((colDef: ColDef) => {
          colDef.floatingFilter = this.enableFloatingFilter;
        });
        this.gridApi.setColumnDefs(columnDefs);
      }
    }
  }

  public refresh(): void {
    this.store.dispatch(actions.refreshTagMaps());
  }

  public deleteTagMaps(): void {
    let tagMapIds: number[] = [];
    const mappedRows: RowNode[] = this.mappedSelectedRows();
    tagMapIds = mappedRows.map(
      (row: RowNode) => row.data.AssetVariableTypeTagMapId
    );

    if (tagMapIds.length > 0) {
      this.store.dispatch(
        actions.auditTagMaps({ tagMapIds, callback: this.popup.bind(this) })
      );
    }
  }

  public copyTagMaps(): void {
    this.gridApi?.selectAll();
    this.gridApi?.copySelectedRowsToClipboard({
      includeHeaders: true,
      columnKeys: [
        'AssetPath',
        'AssetName',
        'AssetClassTypeKey',
        'VariableDesc',
        'VariableTypeUnits',
        'PDServerName',
        'TagName',
        'TagDescription',
        'TagEngUnit',
        'ValueTypeKey',
        'ExistsOnServer',
      ],
    });
    this.gridApi?.deselectAll();
    this.store.dispatch(
      showSuccessMessage({ message: 'Tagmaps are copied to Clipboard' })
    );
  }

  public onPasteClick(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '750px';
    dialogConfig.height = '700px';
    dialogConfig.data = {
      tagNamesByFullPathDict: this.getTagNamesByFullPathDict(),
      assetIdByPathDict: this.getAssetIdByPathDict(),
      tagIdByNameDict: this.getTagGlobalIdByNameDict(),
    };

    const dialogRef = this.dialog.open(
      TagMapPasteDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((createdTagMaps: IPDTagMap[]) => {
      if (!createdTagMaps || createdTagMaps.length === 0) {
        return;
      }
      this.store.dispatch(actions.refreshTagMaps());
    });
  }

  private mappedSelectedRows(): RowNode[] {
    let nodes: RowNode[] = [];
    if (this.gridApi) {
      nodes = sortBy(this.gridApi.getSelectedNodes(), 'rowIndex').filter(
        (node: RowNode) => node.data.AssetVariableTypeTagMapId != null
      );
    }
    return nodes;
  }

  private popup(tagMapIds: number[], text: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = text === '' ? '350px' : '500px';
    dialogConfig.data = {
      text,
    };

    const dialogRef = this.dialog.open(
      DeleteConfirmationDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        this.store.dispatch(actions.deleteTagMaps({ tagMapIds }));
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
      if (changes.asset && this.asset !== '') {
        this.store.dispatch(actions.routeLoadTags({ asset: this.asset }));
      }
      if (changes.tagConfigGridApi && this.gridApi) {
        this.addGridDropZone();
      }
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private getGroupRenderer(): any {
    class GroupRenderer {
      private eGui: ChildNode | null = null;

      init(params: ICellRendererParams | any) {
        const nodes: RowNode[] = [];
        params.api.getModel().forEachNode((rowNode: RowNode) => {
          nodes.push(rowNode);
        });

        const tempDiv = document.createElement('div');
        if (params.data.Hierarchy.length === 1) {
          const count = !params.data.TagName
            ? 0
            : (params.node.allChildrenCount ?? 0) + 1;
          tempDiv.innerHTML = `<span>(${count})</span>`;
        } else {
          tempDiv.innerHTML = `<span></span>`;
        }

        this.eGui = tempDiv.firstChild;
      }

      getGui() {
        return this.eGui;
      }

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      refresh(params: ICellRendererParams) {
        return false;
      }
    }

    return GroupRenderer;
  }

  private addGridDropZone(): void {
    if (this.tagConfigGridApi && this.gridApi) {
      const dropZone = this.gridApi.getRowDropZoneParams({
        onDragStop: (params: RowDragEndEvent) => {
          this.removeHover();

          // check if the node dragged is a Tag
          if (
            params.overNode &&
            !isNil(params.node.data?.Id) &&
            !params.node.data?.Id.startsWith('-')
          ) {
            let tags: ITagResult[] = sortBy(
              this.tagConfigGridApi?.getSelectedNodes(),
              'rowIndex'
            ).map((node: RowNode) => node.data);
            if (
              tags.length === 0 ||
              (tags.length > 0 && !tags.includes(params.node.data))
            ) {
              // if dragged tag is not included as one of the selected, only include the dragged
              tags = [params.node.data];
            }
            this.store.dispatch(
              actions.associateTagsToTagMaps({
                selectedTagMap: params.overNode.data,
                tags,
              })
            );
          }
        },
        onDragging: (params: RowDragMoveEvent) => {
          if (this.gridApi != null && params.overNode != null) {
            this.removeHover();
            const element = this.el.nativeElement.querySelectorAll(
              `[row-id="${params.overNode.id}"]`
            );
            this.renderer.addClass(element[1], 'ag-row-hover');
            this.targetNode = params.overNode;
          } else {
            this.removeHover();
          }
        },
        onDragLeave: () => {
          this.removeHover();
        },
      });
      this.tagConfigGridApi.addRowDropZone(dropZone);

      this.gridBody.nativeElement.addEventListener(
        'selectstart',
        (e: Event) => {
          e.preventDefault();
          return false;
        }
      );

      this.gridBody.nativeElement.addEventListener('mousedown', (e: any) => {
        if (
          e.target?.className !== 'ag-input-field-input ag-text-field-input'
        ) {
          e.preventDefault();
        }
        return false;
      });
    }
  }

  private setInitialFilters(): void {
    this.gridApi?.setFilterModel({
      ExistsOnServer: {
        values: ['true'],
        filterType: 'set',
      },
    });
  }

  private getFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      const filters: any[] = [];

      Object.keys(gridFilters).forEach((key: string) => {
        const colDef: ColDef | null | undefined =
          this.gridApi?.getColumnDef(key);
        if (colDef) {
          filters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef?.headerName,
          });
        }
      });

      this.store.dispatch(actions.setTagMapFilters({ filters }));
    }
  }

  private refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  private postRemoveFilterCallbackFn(filterName: string): void {
    if (this.gridApi) {
      this.gridApi.getFilterInstance(filterName)?.setModel(null);
      this.refreshGrid();
    }
  }

  private removeHover(): void {
    if (this.targetNode != null) {
      const element: NodeList = this.el.nativeElement.querySelectorAll(
        `[row-id="${this.targetNode.id}"]`
      )[1];
      this.renderer.removeClass(element, 'ag-row-hover');
      this.targetNode = null;
    }
  }

  private getAllGridTagMaps(): IPDTagMap[] {
    const tagMaps: IPDTagMap[] = [];
    this.gridApi?.forEachNode((node: RowNode) => tagMaps.push(node.data));
    return tagMaps;
  }

  private getTagNamesByFullPathDict(): { [key: string]: string[] } {
    return Object.assign(
      {},
      ...this.getAllGridTagMaps().map(
        (tagMap: IPDTagMap, _: number, arr: IPDTagMap[]) => ({
          [tagMap.FullPath]: arr
            .filter(
              (tm: IPDTagMap) =>
                tm.FullPath === tagMap.FullPath && tm.TagName !== ''
            )
            .map((tagMap: IPDTagMap) => tagMap.TagName),
        })
      )
    );
  }

  private getAssetIdByPathDict(): { [key: string]: string } {
    return Object.assign(
      {},
      ...this.getAllGridTagMaps().map((tagMap: IPDTagMap) => ({
        [tagMap.AssetPath]: tagMap.AssetId,
      }))
    );
  }

  private getTagGlobalIdByNameDict(): { [key: string]: string } {
    return Object.assign(
      {},
      ...this.tags.map((tag: ITagResult) => ({
        [tag.Name]: tag.Id,
      }))
    );
  }
}
