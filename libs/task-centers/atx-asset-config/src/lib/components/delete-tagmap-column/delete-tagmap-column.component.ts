import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IPDTagMap } from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import {
  GridApi,
  ICellRendererParams,
  RowNode,
} from '@ag-grid-enterprise/all-modules';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeleteConfirmationDialogComponent } from '../delete-confirmation-dialog/delete-confirmation-dialog.component';
import {
  auditTagMaps,
  deleteTagMaps,
} from '../../store/actions/tags-tab.actions';
import { sortBy } from 'lodash';

@Component({
  selector: 'atx-delete-tagmap-column',
  templateUrl: './delete-tagmap-column.component.html',
  styleUrls: ['./delete-tagmap-column.component.scss'],
})
export class DeleteTagMapColumnComponent implements ICellRendererAngularComp {
  public tagMap: IPDTagMap | null = null;
  private gridApi: GridApi | null = null;

  constructor(private store: Store, private dialog: MatDialog) {}

  agInit(params: ICellRendererParams) {
    this.setTag(params);
  }

  popup(tagMapIds: number[], text: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = text === '' ? '350px' : '500px';
    dialogConfig.data = {
      text,
    };

    const dialogRef = this.dialog.open(
      DeleteConfirmationDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        this.store.dispatch(deleteTagMaps({ tagMapIds }));
      }
    });
  }

  delete() {
    // include only those whose AssetVariableTypeTagMapId != null among the selected nodes
    let tagMapIds: number[] = sortBy(
      this.gridApi?.getSelectedNodes(),
      'rowIndex'
    )
      .filter((node: RowNode) => node.data.AssetVariableTypeTagMapId != null)
      .map((node: RowNode) => node.data.AssetVariableTypeTagMapId);

    if (
      tagMapIds.length === 0 ||
      (tagMapIds.length > 0 &&
        this.tagMap?.AssetVariableTypeTagMapId != null &&
        !tagMapIds.includes(this.tagMap.AssetVariableTypeTagMapId))
    ) {
      // if deleted tagMap is not included as one of the selected, only include the deleted
      tagMapIds = [this.tagMap?.AssetVariableTypeTagMapId ?? 0];
    }

    this.store.dispatch(
      auditTagMaps({ tagMapIds, callback: this.popup.bind(this) })
    );
  }

  refresh(params: ICellRendererParams) {
    this.setTag(params);
    return true;
  }

  setTag(params: ICellRendererParams) {
    this.tagMap = params?.node?.data ?? this.tagMap ?? null;
    this.gridApi = params.api;
  }
}
