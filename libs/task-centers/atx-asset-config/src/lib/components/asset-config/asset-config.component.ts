import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import {
  AssetConfigViewModel,
  selectAssetConfigViewModel,
  selectSelectedAsset,
} from '../../store/selectors/asset-config.selectors';
import { selectAssetTreeState } from '../../store/selectors/asset-nav.selectors';
import * as actions from '../../store/actions/asset-config.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { getUniqueIdFromRoute, NavActions } from '@atonix/atx-navigation';
import { ITree, ITreeState, ITreeStateChange } from '@atonix/atx-asset-tree';
import { getTags } from '../../store/actions/tags-tab.actions';
import { concatLatestFrom } from '@ngrx/effects';
import { isNil } from '@atonix/atx-core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'atx-asset-config',
  templateUrl: './asset-config.component.html',
  styleUrls: ['./asset-config.component.scss'],
})
export class AssetConfigComponent implements AfterViewInit, OnDestroy {
  public assetConfigViewModel$: Observable<AssetConfigViewModel> =
    this.store.select(selectAssetConfigViewModel);
  private selectedAsset$: Observable<string> =
    this.store.select(selectSelectedAsset);
  private assetTreeState$: Observable<ITreeState> =
    this.store.select(selectAssetTreeState);
  private onDestroy$ = new Subject<void>();

  constructor(
    private store: Store,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.selectedAsset$
      .pipe(
        filter((selectedAsset: string) => !isNil(selectedAsset)),
        concatLatestFrom(() => this.assetTreeState$),
        takeUntil(this.onDestroy$)
      )
      .subscribe(([asset, assetTreeState]: [string, ITreeState]) => {
        const defaultTreeID: string =
          assetTreeState.treeConfiguration.trees.find(
            (tree: ITree) => tree.name === 'Default Asset Tree'
          )?.id ?? '';
        const selectedTreeID: string =
          assetTreeState.treeConfiguration.selectedTree;
        if (selectedTreeID !== defaultTreeID) {
          const prop: ITreeStateChange = {
            event: 'SelectAsset',
            newValue: defaultTreeID,
          };
          this.store.dispatch(actions.treeStateChange(prop));
        } else {
          this.store.dispatch(actions.selectAsset({ asset }));
        }
      });
  }

  ngAfterViewInit(): void {
    const result = getUniqueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.store.dispatch(
      actions.systemInitialize({ asset: result ? result : undefined })
    );
  }

  public mainNavclicked(): void {
    this.store.dispatch(
      NavActions.configureNavigationButton({
        id: 'asset_tree',
        button: {
          selected: false,
        },
      })
    );
  }

  public onAssetTreeStateChange(change: ITreeStateChange): void {
    this.store.dispatch(actions.treeStateChange(change));
  }

  public onAssetTreeSizeChange(newSize: number): void {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.store.dispatch(actions.treeSizeChange({ value: newSize }));
    }
  }

  public selectedAssetChange(event: MatSlideToggleChange): void {
    this.store.dispatch(
      actions.toggleSelectedAssetOnly({ value: event.checked })
    );
  }

  selectServer(server: MatSelectChange): void {
    this.store.dispatch(getTags({ server: server.value }));
  }

  ngOnDestroy(): void {
    this.store.dispatch(actions.deselectAsset());
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
