/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  IAssetCreate,
  IAssetCreateUpdateResult,
  IAssetCreateValidateResult,
  IAssetResult,
} from '@atonix/atx-core';
import { AssetsCoreService } from '@atonix/shared/api';
import { Dictionary } from 'lodash';
import { catchError, Subject, take, throwError } from 'rxjs';
import { AssetConfigCSVParserService } from '../../service/csv-parser.service';

@Component({
  selector: 'atx-user-admin-asset-paste-dialog',
  templateUrl: './asset-paste-dialog.component.html',
  styleUrls: ['./asset-paste-dialog.component.scss'],
})
export class AssetPasteDialogComponent implements OnDestroy {
  @ViewChild('pasteBox') pasteBox: ElementRef | undefined;
  public isValidating = false;
  public clientValidationResult = '';
  public serverValidationResult = '';
  public isSaveBtnEnabled = false;
  private pastedText = '';
  private existingAssetIds: string[] = [];
  private referenceId = '';
  private parentToChildAssets: Dictionary<IAssetResult[]> = {};
  private assetArr: IAssetCreate[] = [];
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AssetPasteDialogComponent>,
    private assetsCoreService: AssetsCoreService,
    private csvService: AssetConfigCSVParserService
  ) {
    this.existingAssetIds = [...data.existingAssetIds];
    this.referenceId = data.referenceId;
    this.parentToChildAssets = { ...data.parentToChildAssets };
  }

  public onKeydown(event: KeyboardEvent): void {
    if (event.ctrlKey && ['V', 'v'].includes(event.key)) {
      setTimeout(() => {
        this.isSaveBtnEnabled = false;
        this.isValidating = false;
        this.clientValidationResult = '';
        this.serverValidationResult = '';
        this.pastedText = this.pasteBox?.nativeElement.value ?? '';
        this.validate();
      }, 100);
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    if (this.pasteBox) {
      this.pasteBox.nativeElement.disable = true;
    }
    this.isSaveBtnEnabled = false;
    this.assetsCoreService
      .createAssets(this.referenceId, [...this.assetArr])
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.disable = false;
          }
          this.isSaveBtnEnabled = true;
          return throwError(() => new Error(message));
        })
      )
      .subscribe((assets: IAssetCreateUpdateResult[]) => {
        this.serverValidationResult = '';
        for (const asset of assets) {
          if (asset.Error) {
            this.serverValidationResult += `Asset Name: ${asset.AssetName} Error: ${asset.Error}\n`;
          }
        }

        if (!this.serverValidationResult) {
          this.dialogRef.close(assets);
        } else if (this.pasteBox) {
          this.pasteBox.nativeElement.disable = false;
        }
        this.isSaveBtnEnabled = true;
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private validate(): void {
    if (this.pastedText !== '') {
      this.isValidating = true;
      const pasted: string[][] = this.csvService.parseCSVStringToArrayString(
        this.pastedText
      );
      const data: { [key: string]: string[] } = {};
      const mandatoryHeaders: string[] = [
        'Parent Asset Path',
        'Asset Name',
        'Description',
        'Type',
      ];
      const customHeaders: string[] = [];

      // validate headers
      for (let i = 0; i < pasted[0].length; i++) {
        const header: string = pasted[0][i];
        if (!mandatoryHeaders.includes(header)) {
          customHeaders.push(header);
        } else {
          data[header] = [];
        }
      }

      if (Object.keys(data).length !== mandatoryHeaders.length) {
        const missingHeaders: string[] = mandatoryHeaders.filter(
          (header: string) => !Object.keys(data).includes(header)
        );
        this.clientValidationResult =
          missingHeaders.length === 1
            ? `Header Row missing: ${missingHeaders[0]}`
            : `Header Rows missing: ${missingHeaders.join(', ')}`;
        this.isValidating = false;
        return;
      }

      // build data
      const hasAssetIDHeader = customHeaders.includes('Asset ID');
      if (hasAssetIDHeader) {
        data['Asset ID'] = [];
      }

      for (let i = 1; i < pasted.length; i++) {
        for (let y = 0; y < pasted[i].length; y++) {
          const header: string = pasted[0][y];
          if (data[header] != null) {
            data[header].push(pasted[i][y]);
          }
        }
      }

      const pastedDataLength: number = data[Object.keys(data)[0]].length;
      const removedAssetIds: string[] = [];
      const missingRows: number[] = [];
      // build array of IAssetCreate[]
      this.assetArr = [];
      for (let i = 0; i < pastedDataLength; i++) {
        if (data['Asset ID'] != null) {
          const assetID: string = data['Asset ID'][i];
          if (this.existingAssetIds.includes(assetID)) {
            removedAssetIds.push(assetID);
            continue;
          }
        }
        const parentAssetPath: string = data['Parent Asset Path'][i];
        const assetName: string = data['Asset Name'][i];
        const assetDesc: string = data['Description'][i];
        const assetClassTypeKey: string = data['Type'][i];

        if (this.parentToChildAssets[parentAssetPath] != null) {
          const assetExists: IAssetResult | undefined =
            this.parentToChildAssets[parentAssetPath].find(
              (asset: IAssetResult) =>
                asset.AssetName === assetName || asset.AssetDesc === assetDesc
            );
          if (assetExists) {
            removedAssetIds.push(assetExists.AssetId);
            continue;
          }
        }

        this.assetArr.push({
          ParentAssetPath: parentAssetPath,
          AssetName: assetName,
          AssetDesc: assetDesc,
          AssetClassTypeKey: assetClassTypeKey,
          DisplayOrder: 1,
        });

        if (
          !parentAssetPath ||
          !assetName ||
          !assetDesc ||
          !assetClassTypeKey
        ) {
          missingRows.push(i + 1);
        }
      }

      if (missingRows.length > 0) {
        this.clientValidationResult =
          missingRows.length === 1
            ? `Data missing in Row ${missingRows[0]}`
            : `Data missing in Rows ${missingRows.join(', ')}`;
        this.isValidating = false;
        return;
      }

      const totalAssets = this.assetArr.length;
      if (removedAssetIds.length > 0) {
        if (totalAssets < 1) {
          this.clientValidationResult = `No assets found: ${removedAssetIds.length} existing out of ${pastedDataLength} pasted assets.`;
          this.isValidating = false;
          return;
        } else {
          this.clientValidationResult = `Ignoring ${removedAssetIds.length} Existing Assets out of ${pastedDataLength} Pasted Assets.`;
        }
      }

      if (totalAssets < 1) {
        this.clientValidationResult = 'No assets found.';
        this.isValidating = false;
        return;
      } else if (totalAssets > 1000) {
        this.clientValidationResult =
          'More than 1000 Assets are found, Reduce the count and try again.';
        this.isValidating = false;
        return;
      }
      this.onValidate(this.assetArr);
    }
  }

  private onValidate(assets: IAssetCreate[]): void {
    this.assetsCoreService
      .validateCreateAssets(this.referenceId, assets)
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = `Error: ${err.error.Results[0].Message}`;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.value = '';
          }
          this.isValidating = false;
          if (!this.clientValidationResult.includes('Ignoring')) {
            this.clientValidationResult = 'Data Format OK';
          }
          return throwError(() => new Error(message));
        })
      )
      .subscribe((result: IAssetCreateValidateResult[]) => {
        if (result.length > 0) {
          let errorText = '';

          for (const validatedAsset of result) {
            if (validatedAsset.IsError) {
              errorText += `Asset Name: ${validatedAsset.AssetName} Error: ${validatedAsset.Message}\n`;
            }
          }

          if (errorText.length > 0 && this.pasteBox) {
            this.serverValidationResult = errorText;
            this.pasteBox.nativeElement.value = '';
          } else {
            this.serverValidationResult =
              'Data is Valid. Select "Save" below to create new assets';
            this.isSaveBtnEnabled = true;
          }
        }
        this.isValidating = false;
        if (!this.clientValidationResult.includes('Ignoring')) {
          this.clientValidationResult = 'Data Format OK';
        }
      });
  }
}
