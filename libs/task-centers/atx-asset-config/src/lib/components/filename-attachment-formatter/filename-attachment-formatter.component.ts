/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import {
  ICellRendererParams,
  IAfterGuiAttachedParams,
} from '@ag-grid-enterprise/all-modules';
import { IAttachment } from '../../models/attachment-type';

@Component({
  selector: 'atx-filename-attachment-formatter',
  template:
    '<u style="cursor: pointer" (click)="onFilenameClick()">{{ filename }}</u>',
})
export class FilenameAttachmentFormatterComponent
  implements ICellRendererAngularComp
{
  public filename = '';
  private attachment: IAttachment | undefined;
  private params: any;

  constructor() {}

  agInit(params: ICellRendererParams): void {
    this.params = params;
    if (params?.data) {
      this.filename = params.value;
      this.attachment = params.data;
    }
  }

  refresh(params: ICellRendererParams): boolean {
    if (params?.data) {
      this.filename = params.value;
    }
    return true;
  }

  public onFilenameClick() {
    this.params.clickCallback(this.attachment);
  }

  afterGuiAttached(_?: IAfterGuiAttachedParams): void {}
}
