/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ITagCreateUpdateResult, ITagResult } from '@atonix/atx-core';
import { ProcessDataCoreService } from '@atonix/shared/api';
import { catchError, forkJoin, map, Subject, throwError } from 'rxjs';
import { AssetConfigCSVParserService } from '../../service/csv-parser.service';

@Component({
  selector: 'atx-user-admin-tags-paste-dialog',
  templateUrl: './tags-paste-dialog.component.html',
  styleUrls: ['./tags-paste-dialog.component.scss'],
})
export class TagsPasteDialogComponent implements OnDestroy {
  @ViewChild('pasteBox') pasteBox: ElementRef | undefined;
  public isValidating = false;
  public isProceedVisible = false;
  public clientValidationResult = '';
  public serverValidationResult = '';
  public isSaveBtnEnabled = false;
  public totalValidTags = 0;
  private pastedText = '';
  private tagIdInUseAndActiveByNameDict: {
    [key: string]: [string, number, boolean];
  } = {};
  private tagsToCreate: ITagResult[] = [];
  private tagsToUpdate: ITagResult[] = [];
  private serverId = '';
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<TagsPasteDialogComponent>,
    private processDataCoreService: ProcessDataCoreService,
    private csvService: AssetConfigCSVParserService
  ) {
    this.tagIdInUseAndActiveByNameDict = {
      ...data.tagIdInUseAndActiveByNameDict,
    };
    this.serverId = data.serverId;
  }

  public onKeydown(event: KeyboardEvent): void {
    if (event.ctrlKey && ['V', 'v'].includes(event.key)) {
      setTimeout(() => {
        this.initialize();
        this.pastedText = this.pasteBox?.nativeElement.value ?? '';
        this.validate();
      }, 100);
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onSave(): void {
    if (this.pasteBox) {
      this.pasteBox.nativeElement.disable = true;
    }
    this.isSaveBtnEnabled = false;

    forkJoin({
      creates: this.processDataCoreService.createTags(
        this.serverId,
        this.tagsToCreate
      ),
      updates: this.processDataCoreService.updateTags(
        this.serverId,
        this.tagsToUpdate
      ),
    })
      .pipe(
        map(
          (result: {
            creates: ITagCreateUpdateResult[];
            updates: ITagCreateUpdateResult[];
          }) => {
            const createdOrUpdatedTags: ITagCreateUpdateResult[] =
              result.creates.concat(result.updates);
            return createdOrUpdatedTags;
          }
        ),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.disable = false;
            this.pasteBox.nativeElement.value = '';
          }
          return throwError(() => new Error(message));
        })
      )
      .subscribe((createdOrUpdatedTags: ITagCreateUpdateResult[]) => {
        this.serverValidationResult = '';
        for (const tag of createdOrUpdatedTags) {
          if (tag.Error) {
            this.serverValidationResult += `Tag Name: ${tag.Name} Error: ${tag.Error}\n`;
          }
        }

        if (!this.serverValidationResult) {
          this.dialogRef.close(createdOrUpdatedTags);
        } else if (this.pasteBox) {
          this.pasteBox.nativeElement.disable = false;
          this.pasteBox.nativeElement.value = '';
        }
        this.isSaveBtnEnabled = false;
      });
  }

  public onValidateCancel(): void {
    this.initialize();
    if (this.pasteBox) {
      this.pasteBox.nativeElement.value = '';
    }
  }

  public onProceed(): void {
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = true;
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private validate(): void {
    if (this.pastedText !== '') {
      this.isValidating = true;
      const pasted: string[][] = this.csvService.parseCSVStringToArrayString(
        this.pastedText
      );
      const data: { [key: string]: string[] } = {};
      const mandatoryHeaders: string[] = [
        'Tag ID',
        'Tag Name',
        'Tag Description',
        'Tag Units',
        'Source ID',
        'Qualifier',
        'Tag Active',
      ];

      // validate headers
      for (let i = 0; i < pasted[0].length; i++) {
        const header: string = pasted[0][i];
        if (mandatoryHeaders.includes(header)) {
          data[header] = [];
        }
      }

      if (Object.keys(data).length !== mandatoryHeaders.length) {
        const missingHeaders: string[] = mandatoryHeaders.filter(
          (header: string) => !Object.keys(data).includes(header)
        );
        this.clientValidationResult =
          missingHeaders.length === 1
            ? `Header Row missing: ${missingHeaders[0]}`
            : `Header Rows missing: ${missingHeaders.join(', ')}`;
        this.isValidating = false;
        return;
      }

      // build data
      for (let i = 1; i < pasted.length; i++) {
        for (let y = 0; y < pasted[i].length; y++) {
          const header: string = pasted[0][y];
          if (data[header] != null) {
            data[header].push(pasted[i][y]);
          }
        }
      }

      const pastedDataLength: number = data[Object.keys(data)[0]].length;
      const missingRows: number[] = [];
      const duplicatedTagNames: number[] = [];
      // build array of ITagResult[] to create and update
      this.tagsToCreate = [];
      this.tagsToUpdate = [];
      // invalid tags are tags whose IDs are provided but do not exist in [Name]: Id
      let invalidTagsCount = 0;
      for (let i = 0; i < pastedDataLength; i++) {
        const tagID: string = data['Tag ID'][i];
        const tagName: string = data['Tag Name'][i];
        const description: string = data['Tag Description'][i];
        const units: string = data['Tag Units'][i];
        const sourceID: string = data['Source ID'][i];
        const qualifier: string = data['Qualifier'][i];
        const activeStr: string = data['Tag Active'][i].toLowerCase();

        if (!tagName) {
          missingRows.push(i + 1);
          continue;
        }

        if (tagID) {
          // look for the tagID
          const foundTagName: string | undefined = Object.keys(
            this.tagIdInUseAndActiveByNameDict
          ).find((key) => this.tagIdInUseAndActiveByNameDict[key][0] === tagID);
          if (foundTagName != null) {
            let active = this.tagIdInUseAndActiveByNameDict[foundTagName][2];
            if (activeStr) {
              try {
                active = JSON.parse(activeStr);
              } catch {
                ++invalidTagsCount;
                continue;
              }
            }
            // add to tagsToUpdate arr
            this.tagsToUpdate.push({
              Id: tagID,
              Name: tagName,
              Description: description,
              EngUnit: units,
              Source: sourceID,
              Qualifier: qualifier,
              ExistsOnServer: active,
              InUse: this.tagIdInUseAndActiveByNameDict[foundTagName][1],
              IsDirty: true,
            });
          } else {
            // provided Id is non-existent, therefore update is not possible
            ++invalidTagsCount;
          }
        } else {
          if (this.tagIdInUseAndActiveByNameDict[tagName] != null) {
            // duplicated Tag Name
            duplicatedTagNames.push(i + 1);
            continue;
          } else {
            let active = true;
            if (activeStr) {
              try {
                active = JSON.parse(activeStr);
              } catch {
                ++invalidTagsCount;
                continue;
              }
            }
            // add to tagsToCreate arr
            this.tagsToCreate.push({
              Id: tagID,
              Name: tagName,
              Description: description,
              EngUnit: units,
              Source: sourceID,
              Qualifier: qualifier,
              ExistsOnServer: active,
              InUse: 0,
              IsDirty: true,
            });
          }
        }
      }

      if (missingRows.length > 0) {
        this.clientValidationResult =
          missingRows.length === 1
            ? `Data missing in Row ${missingRows[0]}`
            : `Data missing in Rows ${missingRows.join(', ')}`;
        this.isValidating = false;
        return;
      }

      if (duplicatedTagNames.length > 0) {
        this.clientValidationResult =
          duplicatedTagNames.length === 1
            ? `Duplicate Tag Name in Row ${duplicatedTagNames[0]}`
            : `Duplicate Tag Name in Rows ${duplicatedTagNames.join(', ')}`;
        this.isValidating = false;
        return;
      }

      const newTagsCount = this.tagsToCreate.length;
      const toUpdateTagsCount = this.tagsToUpdate.length;
      this.totalValidTags = newTagsCount + toUpdateTagsCount;
      this.clientValidationResult = 'Data Format OK';
      if (this.totalValidTags > 0) {
        if (invalidTagsCount > 0) {
          this.isProceedVisible = true;
          this.serverValidationResult = `${newTagsCount} valid New tags found. ${toUpdateTagsCount} valid Existing tags found.  ${invalidTagsCount} Invalid tags found.`;
        } else {
          this.serverValidationResult =
            'Data is Valid. Select "Save" below to create new tags.';
          this.isSaveBtnEnabled = true;
        }
      } else {
        this.serverValidationResult = `${newTagsCount} valid New tags found. ${toUpdateTagsCount} valid Existing tags found.  ${invalidTagsCount} Invalid tags found.`;
      }
      this.isValidating = false;
    }
  }

  private initialize(): void {
    this.totalValidTags = 0;
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = false;
    this.isValidating = false;
    this.clientValidationResult = '';
    this.serverValidationResult = '';
  }
}
