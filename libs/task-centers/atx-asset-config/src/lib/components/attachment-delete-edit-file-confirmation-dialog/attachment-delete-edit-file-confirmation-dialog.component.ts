/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-asset-config-attachment-delete-edit-file-confirmation-dialog',
  templateUrl:
    './attachment-delete-edit-file-confirmation-dialog.component.html',
  styleUrls: [
    './attachment-delete-edit-file-confirmation-dialog.component.scss',
  ],
})
export class AttachmentDeleteEditFileConfirmationDialogComponent {
  public titleText = '';
  public dialogText = '';
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AttachmentDeleteEditFileConfirmationDialogComponent>
  ) {
    this.titleText = data.titleText;
    this.dialogText = data.dialogText;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onContinue(): void {
    this.dialogRef.close(true);
  }
}
