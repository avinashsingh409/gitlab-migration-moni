import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AttachmentDeleteEditFileConfirmationDialogComponent } from './attachment-delete-edit-file-confirmation-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AttachmentDeleteEditFileConfirmationDialogComponent', () => {
  let component: AttachmentDeleteEditFileConfirmationDialogComponent;
  let fixture: ComponentFixture<AttachmentDeleteEditFileConfirmationDialogComponent>;
  const mockDialogRef = {};
  const dialogData = {
    titleText: '',
    dialogText: '',
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: dialogData,
          },
        },
      ],
      declarations: [AttachmentDeleteEditFileConfirmationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(
      AttachmentDeleteEditFileConfirmationDialogComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
