/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject, take, catchError, of } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { selectSelectedAssetObject } from '../../store/selectors/asset-config.selectors';
import * as actions from '../../store/actions/attachments-tab.actions';
import {
  toggleSelectedAssetOnly,
  updateRoute,
} from '../../store/actions/asset-config.actions';
import {
  AttachmentDataAvailable,
  attachmentsTabFeatureKey,
} from '../../store/reducers/attachments-tab.reducer';
import {
  AttachmentsViewModel,
  selectAttachments,
  selectAttachmentsState,
  selectAttachmentsViewModel,
} from '../../store/selectors/attachments-tab.selectors';
import { IAttachment } from '../../models/attachment-type';
import {
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  ClipboardModule,
  GridOptions,
  GridReadyEvent,
  FilterChangedEvent,
  GridApi,
  ColDef,
  GetContextMenuItemsParams,
  MenuItemDef,
  MenuModule,
} from '@ag-grid-enterprise/all-modules';
import { FavoriteAttachmentFormatterComponent } from '../favorite-attachment-formatter/favorite-attachment-formatter.component';
import { FilenameAttachmentFormatterComponent } from '../filename-attachment-formatter/filename-attachment-formatter.component';
import {
  dateComparator,
  isNil,
  isValidFilenameForAttachment,
} from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AttachmentViewerDialogComponent } from '../attachment-viewer-dialog/attachment-viewer-dialog.component';
import { ImagesFrameworkService } from '@atonix/shared/api';
import { IAWSFileUrl } from '@atonix/atx-core';
import { ToastService } from '@atonix/shared/utils';
import {
  IValidationResult,
  AttributesTabService,
} from '../../service/attributes-tab.service';
import { AttachmentDeleteEditFileConfirmationDialogComponent } from '../attachment-delete-edit-file-confirmation-dialog/attachment-delete-edit-file-confirmation-dialog.component';

@Component({
  selector: 'atx-asset-config-attachments-tab',
  templateUrl: './attachments-tab.component.html',
  styleUrls: ['./attachments-tab.component.scss'],
})
export class AttachmentsTabComponent implements AfterViewInit, OnDestroy {
  @ViewChild('menuBar') menuBar: ElementRef | any = null;
  @ViewChild('createAttachmentUploader') createAttachmentUploader:
    | ElementRef
    | any = null;
  @ViewChild('updateAttachmentUploader') editAttachmentUploader:
    | ElementRef
    | any = null;
  public selectedAttachments: IAttachment[] = [];
  public attachmentsViewModel$: Observable<AttachmentsViewModel> =
    this.store.select(selectAttachmentsViewModel);
  public selectedAssetObject$: Observable<any> = this.store.select(
    selectSelectedAssetObject
  );
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
    MenuModule,
  ];
  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'multiple',
    rowMultiSelectWithClick: false,
    singleClickEdit: true,
    rowHeight: 30,
    suppressClipboardPaste: true,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
    },
    treeData: false,
    columnDefs: [
      {
        headerName: 'Favorite',
        colId: 'IsFavorite',
        field: 'IsFavorite',
        cellRenderer: 'favoriteRenderer',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.IsFavorite ?? false);
        },
        width: 65,
        headerTooltip: 'Favorite',
      },
      {
        headerName: 'Filename',
        colId: 'Filename',
        field: 'Filename',
        filter: 'agTextColumnFilter',
        cellRenderer: 'filenameRenderer',
        cellRendererParams: {
          clickCallback: this.filenameClickCallback.bind(this),
        },
      },
      {
        headerName: 'Caption',
        colId: 'Caption',
        field: 'Caption',
        filter: 'agTextColumnFilter',
        editable: true,
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAttachment;
          newValue: string;
        }) => {
          const attachmentToUpsert: IAttachment = {
            ...data,
            Caption: newValue,
          };
          this.store.dispatch(
            actions.upsertAttachment({
              attachmentToUpsert,
              file: undefined,
              action: 'update attachment',
            })
          );
          return false;
        },
      },
      {
        headerName: 'Change Date',
        colId: 'ChangeDate',
        field: 'ChangeDate',
        valueFormatter: dateFormatter,
        filter: 'agDateColumnFilter',
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        headerName: 'Owner',
        colId: 'Owner',
        field: 'ChangedBy',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Display Order',
        colId: 'DisplayOrder',
        field: 'DisplayOrder',
        filter: 'agNumberColumnFilter',
        editable: true,
        width: 100,
        headerTooltip: 'Display Order',
        valueSetter: ({
          data,
          newValue,
        }: {
          data: IAttachment;
          newValue: string;
        }) => {
          const validationResult: IValidationResult =
            this.attributesTabService.validateInt(newValue, true);
          if (
            validationResult.IsValid &&
            typeof validationResult.ValidIntValue === 'number'
          ) {
            const attachmentToUpsert: IAttachment = {
              ...data,
              DisplayOrder: validationResult.ValidIntValue,
            };
            this.store.dispatch(
              actions.upsertAttachment({
                attachmentToUpsert,
                file: undefined,
                action: 'update attachment',
              })
            );
          } else {
            this.toastService.openSnackBar(
              'Please enter a valid Display Order value.',
              'error'
            );
          }
          return false;
        },
      },
      {
        headerName: 'Global ID',
        colId: 'GlobalID',
        field: 'ContentId',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Asset',
        colId: 'AssetName',
        field: 'AssetName',
        filter: 'agTextColumnFilter',
      },
      {
        headerName: 'Type',
        colId: 'Type',
        field: 'Type',
        filter: 'agTextColumnFilter',
      },
    ],
    suppressAggFuncInHeader: true,
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="attachmentsSideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="attachmentsSideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    suppressScrollOnNewData: true,
    components: {
      favoriteRenderer: FavoriteAttachmentFormatterComponent,
      filenameRenderer: FilenameAttachmentFormatterComponent,
    },
    onGridReady: (event: GridReadyEvent) => {
      (this.gridApi = event.api), this.gridApi.hideOverlay();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
    onSelectionChanged: () => {
      if (this.gridApi) {
        this.selectedAttachments = this.gridApi
          .getSelectedRows()
          .map<IAttachment>((r: any) => {
            return r as IAttachment;
          });
      }
      this.menuBar?.nativeElement.click();
    },
    getContextMenuItems: (
      params: GetContextMenuItemsParams
    ): (string | MenuItemDef)[] => {
      const items: (string | MenuItemDef)[] = [];
      if (params.node?.data) {
        const myAttachment: IAttachment = params.node.data as IAttachment;
        if (params.column?.getColId() === 'Filename') {
          if (myAttachment.Type === 'image') {
            const openPreview: MenuItemDef = {
              name: 'Open Preview',
              action: () => {
                this.filenameClickCallback(params.node?.data as IAttachment);
              },
            };
            items.push(openPreview);
          }
          const downloadFile: MenuItemDef = {
            name: 'Download File',
            action: () => {
              this.downloadFile(myAttachment.ContentId, myAttachment.Filename);
            },
          };
          items.push(downloadFile);
        }
      }
      return items;
    },
  };
  private gridApi: GridApi | null = null;
  private enableFloatingFilter = true;
  private attachments$: Observable<IAttachment[]> =
    this.store.select(selectAttachments);
  private attachmentsState$: Observable<AttachmentDataAvailable> =
    this.store.select(selectAttachmentsState);
  private referenceId = '';
  private onDestroy$ = new Subject<void>();

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private imagesFrameworkService: ImagesFrameworkService,
    private toastService: ToastService,
    private attributesTabService: AttributesTabService
  ) {
    this.attachments$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((attachments: IAttachment[]) => {
        if (this.gridApi) {
          this.gridApi.setRowData(attachments);
          this.gridApi.refreshCells({ force: true });
        }
      });

    this.attachmentsState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((data: AttachmentDataAvailable) => {
        if (this.gridApi) {
          if (data === 'loading') {
            this.gridApi.showLoadingOverlay();
          } else if (data === 'nodata') {
            this.gridApi.showNoRowsOverlay();
          } else {
            this.gridApi.hideOverlay();
          }
        }
      });
  }

  public filenameClickCallback(attachment: IAttachment): void {
    if (attachment) {
      if (attachment.Type === 'image') {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.width = '100vw !important';
        dialogConfig.height = '100vh !important';
        dialogConfig.maxWidth = '100vw !important';
        dialogConfig.maxHeight = '100vh !important';
        dialogConfig.data = {
          attachment,
        };

        this.dialog.open(AttachmentViewerDialogComponent, dialogConfig);
      } else {
        this.imagesFrameworkService
          .getFileUrl(attachment.ContentId, attachment.Filename, false)
          .pipe(
            take(1),
            catchError(() => {
              this.toastService.openSnackBar(
                'Not able to download attachment',
                'error'
              );
              return of();
            })
          )
          .subscribe((url: IAWSFileUrl) => {
            window.open(url.Url, '_blank');
          });
      }
    }
  }

  public downloadFile(contentId: string, filename: string): void {
    this.imagesFrameworkService
      .getFileUrl(contentId, filename, false)
      .pipe(take(1))
      .subscribe((url: IAWSFileUrl) => {
        this.imagesFrameworkService.downloadFile(url.Url, filename);
      });
  }

  public clearFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

      Object.keys(gridFilters).forEach((key: string) => {
        this.gridApi?.getFilterInstance(key)?.setModel(null);
      });

      this.store.dispatch(actions.clearFilters());
      this.refreshGrid();
    }
  }

  public removeFilter(filter: any): void {
    this.store.dispatch(
      actions.removeFilter({
        filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  private getFilters(): void {
    if (this.gridApi) {
      const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
      const filters: any[] = [];

      Object.keys(gridFilters).forEach((key: string) => {
        const colDef: ColDef | null | undefined =
          this.gridApi?.getColumnDef(key);
        if (colDef) {
          filters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef.headerName,
          });
        }
      });

      this.store.dispatch(actions.setFilters({ filters }));
    }
  }

  private refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  private postRemoveFilterCallbackFn(filterName: string): void {
    if (this.gridApi) {
      this.gridApi.getFilterInstance(filterName)?.setModel(null);
      this.refreshGrid();
    }
  }

  public refresh(): void {
    this.store.dispatch(actions.getAttachments());
  }

  public toggleFloatingFilter(): void {
    if (this.gridApi) {
      const columnDefs: ColDef[] | undefined = this.gridApi.getColumnDefs();
      if (columnDefs) {
        this.enableFloatingFilter = !this.enableFloatingFilter;
        columnDefs.forEach((colDef: ColDef) => {
          colDef.floatingFilter = this.enableFloatingFilter;
        });
        this.gridApi.setColumnDefs(columnDefs);
      }
    }
  }

  public onCreateAttachmentSelected($event: any): void {
    const files: FileList = $event?.target?.files;
    if (this.validateAttachmentFile(files)) {
      const file: File = files[0];
      const attachmentToUpsert: IAttachment = {
        Id: '',
        Filename: file.name,
        Caption: file.name,
        ChangeDate: new Date(),
        ChangedBy: '',
        IsFavorite: false,
        DisplayOrder: 0,
        ContentId: '',
        AssetName: '',
        Type: '',
        Upload: true,
      };
      this.store.dispatch(
        actions.upsertAttachment({
          attachmentToUpsert,
          file,
          action: 'create attachment',
        })
      );
    }
  }

  public onUpdateAttachmentSelected($event: any): void {
    const files: FileList = $event?.target?.files;
    if (
      this.validateAttachmentFile(files) &&
      this.selectedAttachments &&
      this.selectedAttachments.length === 1
    ) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.width = '350px';
      dialogConfig.height = '300px';
      dialogConfig.data = {
        titleText: 'Update Attachment',
        dialogText:
          'Are you sure you want to update the attachment? Any configuration(s) referencing specific attachments may need to be updated.',
      };
      const dialogRef = this.dialog.open(
        AttachmentDeleteEditFileConfirmationDialogComponent,
        dialogConfig
      );
      dialogRef.afterClosed().subscribe((exit: boolean) => {
        if (
          exit == true &&
          this.selectedAttachments &&
          this.selectedAttachments.length === 1
        ) {
          const file: File = files[0];
          const selectedAttachment: IAttachment = this.selectedAttachments[0];
          const attachmentToUpsert: IAttachment = {
            ...selectedAttachment,
            Filename: file.name,
            Caption:
              selectedAttachment.Caption === selectedAttachment.Filename
                ? file.name
                : selectedAttachment.Caption,
            Upload: true,
          };
          this.store.dispatch(
            actions.upsertAttachment({
              attachmentToUpsert,
              file,
              action: 'update attachment file',
            })
          );
        }
      });
    }
  }

  private validateAttachmentFile(files: FileList): boolean {
    let valid = false;
    if (files) {
      if (files.length > 1) {
        this.toastService.openSnackBar('Please select a single file.', 'error');
        return valid;
      }
      const file: File = files[0];
      // limit is 100 MB
      // that is 100,000,000 KB (1 MB = 1,000 KB x 1,000 B/KB x 100 MB limit = 100,000,000)
      if (file.size >= 100000000) {
        this.toastService.openSnackBar(
          'File size too large. Maximum Limit is 100MB',
          'error'
        );
        return valid;
      }
      if (!isValidFilenameForAttachment(file.name)) {
        this.toastService.openSnackBar(
          'Please select a file without an unsupported character(s) in the file name.',
          'error'
        );
        return valid;
      }
      valid = true;
    }
    return valid;
  }

  public onDeleteClick(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.height = '300px';
    dialogConfig.data = {
      titleText: 'Delete Attachment(s)',
      dialogText:
        'Are you sure you want to permanently delete the selected attachment(s)? This action cannot be undone. Any configuration(s) referencing specific attachments will be lost.',
    };
    const dialogRef = this.dialog.open(
      AttachmentDeleteEditFileConfirmationDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit == true && this.selectedAttachments.length > 0) {
        this.store.dispatch(
          actions.deleteAttachments({
            attachmentsToDelete: this.selectedAttachments,
          })
        );
        this.selectedAttachments = [];
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.createAttachmentUploader) {
      this.createAttachmentUploader.nativeElement.addEventListener(
        'change',
        this.onCreateAttachmentSelected.bind(this),
        false
      );
    }
    if (this.editAttachmentUploader) {
      this.editAttachmentUploader.nativeElement.addEventListener(
        'change',
        this.onUpdateAttachmentSelected.bind(this),
        false
      );
    }
    this.selectedAssetObject$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetObject: any) => {
        if (assetObject?.AssetGuid) {
          this.referenceId = assetObject.AssetGuid;
          this.store.dispatch(
            updateRoute({
              asset: this.referenceId,
              tab: attachmentsTabFeatureKey,
            })
          );
        }
        const asset: any = assetObject.Asset ?? null;
        if (asset) {
          // force the selected asset only toggle to true and disable it
          this.store.dispatch(toggleSelectedAssetOnly({ value: true }));
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
