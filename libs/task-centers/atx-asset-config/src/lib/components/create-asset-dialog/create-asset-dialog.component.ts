/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject, OnDestroy } from '@angular/core';
import { AbstractControl, UntypedFormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  IAssetClassTypeResult,
  IAssetCreate,
  IAssetCreateUpdateResult,
  IAssetResult,
  IAssetUpdate,
} from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import { AssetFormService } from '../../store/service/asset-form.service';
import {
  selectAssetClassTypes,
  selectAssetClassTypeState,
  selectAssetToCreateOrEdit,
} from '../../store/selectors/asset-tab.selectors';
import { Observable, Subject, throwError } from 'rxjs';
import { takeUntil, take, catchError } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import * as actions from '../../store/actions/asset-tab.actions';
import { AssetsCoreService } from '@atonix/shared/api';

@Component({
  selector: 'atx-user-admin-create-asset-dialog',
  templateUrl: './create-asset-dialog.component.html',
  styleUrls: ['./create-asset-dialog.component.scss'],
})
export class CreateAssetDialogComponent implements OnDestroy {
  public headerText = '';
  public buttonText = '';
  public parentAsset: IAssetResult | null = null;
  public assetForm: UntypedFormGroup = new UntypedFormGroup({});
  public displayedColumns: string[] = [
    'standard',
    'assetTypeName',
    'description',
  ];
  public dataSource: MatTableDataSource<IAssetClassTypeResult> =
    new MatTableDataSource<IAssetClassTypeResult>();
  public selection: SelectionModel<IAssetClassTypeResult> =
    new SelectionModel<IAssetClassTypeResult>(false, undefined);
  public errorText = '';
  public assetPathHeading = '';
  public assetPath = '';
  public selectAssetClassTypeState = '';
  private assetClassTypes$: Observable<IAssetClassTypeResult[]> =
    this.store.select(selectAssetClassTypes);
  private selectAssetClassTypeState$: Observable<string> = this.store.select(
    selectAssetClassTypeState
  );
  private assetClassTypes: IAssetClassTypeResult[] = [];
  private assetToCreateOrEdit$: Observable<IAssetResult | null> =
    this.store.select(selectAssetToCreateOrEdit);
  private assetToCreateOrEdit: IAssetResult | null = null;
  private referenceId = '';
  private parentAssetPath = '';
  private onDestroy$ = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<CreateAssetDialogComponent>,
    private assetFormService: AssetFormService,
    private store: Store,
    private assetsCoreService: AssetsCoreService
  ) {
    this.referenceId = this.data.referenceId;
    this.parentAsset = this.data.parentAsset;
    if (this.parentAsset) {
      let parentAssetClassTypeKey: string = this.parentAsset.AssetClassTypeKey;
      if (this.data.type === 'create') {
        this.assetPathHeading = 'Parent Asset:';
        this.headerText = 'Create New Asset';
        this.buttonText = 'Create';
        this.assetForm = new UntypedFormGroup({
          assetName: this.assetFormService.assetName(),
          description: this.assetFormService.description(),
          searchString: this.assetFormService.searchString(
            parentAssetClassTypeKey
          ),
        });
      } else {
        this.assetPathHeading = 'Selected Asset:';
        this.headerText = 'Edit Asset';
        this.buttonText = 'Update';
        parentAssetClassTypeKey = this.data.parentACTypeKey;
        this.assetForm = new UntypedFormGroup({
          assetName: this.assetFormService.assetName(
            this.parentAsset.AssetName
          ),
          description: this.assetFormService.description(
            this.parentAsset.AssetDesc
          ),
          searchString: this.assetFormService.searchString(
            parentAssetClassTypeKey
          ),
        });
      }
      this.parentAssetPath = this.parentAsset.AssetPath;
      this.assetPath =
        this.parentAsset.Hierarchy.length === 1
          ? `${this.parentAsset.AssetName}`
          : `${this.parentAssetPath.slice(1)}/${this.parentAsset.AssetName}`;
      this.store.dispatch(
        actions.getAssetClassTypes({
          parentAssetClassTypeKey,
        })
      );
    }

    this.selectAssetClassTypeState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((state: string) => {
        this.selectAssetClassTypeState = state;
      });

    this.assetClassTypes$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetClassTypes: IAssetClassTypeResult[]) => {
        this.assetClassTypes = assetClassTypes;
        this.dataSource = new MatTableDataSource<IAssetClassTypeResult>(
          this.assetClassTypes
        );

        if (this.assetToCreateOrEdit && this.assetClassTypes.length > 0) {
          const key: string = this.assetToCreateOrEdit.AssetClassTypeKey;
          if (key) {
            const row: IAssetClassTypeResult | undefined =
              this.assetClassTypes.find(
                (act: IAssetClassTypeResult) => act.AssetClassTypeKey === key
              );
            const assetFormControl: AbstractControl =
              this.assetForm.controls['searchString'];
            if (row) {
              this.selection?.select(row);
            } else if (assetFormControl.value === null) {
              assetFormControl.patchValue(key);
            } else {
              this.selection?.clear();
            }
          }
        }
      });

    this.assetToCreateOrEdit$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((assetToCreateOrEdit: IAssetResult | null) => {
        this.assetToCreateOrEdit = assetToCreateOrEdit;
      });
  }

  public handleSelection(row: IAssetClassTypeResult): void {
    this.selection?.select(row);
    this.store.dispatch(
      actions.updateAssetClassType({
        key: row.AssetClassTypeKey,
        desc: row.AssetClassTypeDesc,
      })
    );
  }

  public onResetClick(): void {
    this.selection?.clear();
    this.store.dispatch(actions.updateAssetClassType({ key: '', desc: '' }));
    this.assetForm.controls['searchString'].patchValue(null);
  }

  public isSaveDisabled(): boolean {
    return !(
      this.assetToCreateOrEdit?.AssetName !== '' && this.selection?.hasValue()
    );
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onContinue(): void {
    this.errorText = '';
    if (this.data.type === 'create') {
      this.create();
    } else {
      this.update();
    }
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private update(): void {
    if (this.assetToCreateOrEdit) {
      const assetToUpdate: IAssetUpdate = {
        AssetId: this.assetToCreateOrEdit.AssetId,
        ParentAssetId: this.assetToCreateOrEdit.ParentAssetId,
        AssetName: this.assetToCreateOrEdit.AssetName,
        AssetDesc: this.assetToCreateOrEdit.AssetDesc,
        AssetClassTypeKey: this.assetToCreateOrEdit.AssetClassTypeKey,
        DisplayOrder: this.assetToCreateOrEdit.DisplayOrder,
      };
      this.assetsCoreService
        .updateAssets(this.referenceId, [assetToUpdate])
        .pipe(
          take(1),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((err: any) => {
            let message = '';
            if (err?.error?.Results?.length > 0) {
              if (err?.error?.Results[0]?.Message) {
                message = err.error.Results[0].Message;
              }
            }
            this.errorText = message;
            return throwError(() => message);
          })
        )
        .subscribe((asset: IAssetCreateUpdateResult[]) => {
          if (asset.length > 0) {
            this.errorText = asset[0].Error;
            if (!this.errorText) {
              this.dialogRef.close(asset[0]);
            }
          }
        });
    }
  }

  private create(): void {
    if (this.assetToCreateOrEdit && this.parentAsset) {
      const path: string =
        this.parentAsset.Hierarchy.length === 1
          ? `${this.parentAsset.AssetName}`
          : `${this.parentAssetPath}/${this.parentAsset.AssetName}`;
      const assetToCreate: IAssetCreate = {
        ParentAssetPath: path,
        AssetName: this.assetToCreateOrEdit.AssetName,
        AssetDesc: this.assetToCreateOrEdit.AssetDesc,
        AssetClassTypeKey: this.assetToCreateOrEdit.AssetClassTypeKey,
        DisplayOrder: 1,
      };
      this.assetsCoreService
        .createAssets(this.referenceId, [assetToCreate])
        .pipe(
          take(1),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((err: any) => {
            let message = '';
            if (err?.error?.Results?.length > 0) {
              if (err?.error?.Results[0]?.Message) {
                message = err.error.Results[0].Message;
              }
            }
            this.errorText = message;
            return throwError(() => message);
          })
        )
        .subscribe((asset: IAssetCreateUpdateResult[]) => {
          if (asset.length > 0) {
            this.errorText = asset[0].Error;
            if (!this.errorText) {
              this.dialogRef.close(asset[0]);
            }
          }
        });
    }
  }
}
