/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ITagMapBare, ITagMapCreateUpdateResult } from '@atonix/atx-core';
import { ProcessDataCoreService } from '@atonix/shared/api';
import { catchError, Subject, take, throwError } from 'rxjs';
import { AssetConfigCSVParserService } from '../../service/csv-parser.service';

@Component({
  selector: 'atx-user-admin-tagmaps-paste-dialog',
  templateUrl: './tagmaps-paste-dialog.component.html',
  styleUrls: ['./tagmaps-paste-dialog.component.scss'],
})
export class TagMapPasteDialogComponent implements OnDestroy {
  @ViewChild('pasteBox') pasteBox: ElementRef | undefined;
  public isValidating = false;
  public isProceedVisible = false;
  public clientValidationResult = '';
  public serverValidationResult = '';
  public isSaveBtnEnabled = false;
  public totalValidTagMaps = 0;
  private pastedText = '';
  private assetIdByPathDict: { [key: string]: string } = {};
  private tagIdByNameDict: { [key: string]: string } = {};
  private tagMapsArr: ITagMapBare[] = [];
  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<TagMapPasteDialogComponent>,
    private processDataCoreService: ProcessDataCoreService,
    private csvService: AssetConfigCSVParserService
  ) {
    this.assetIdByPathDict = { ...data.assetIdByPathDict };
    this.tagIdByNameDict = { ...data.tagIdByNameDict };
  }

  public onKeydown(event: KeyboardEvent): void {
    if (event.ctrlKey && ['V', 'v'].includes(event.key)) {
      setTimeout(() => {
        this.initialize();
        this.pastedText = this.pasteBox?.nativeElement.value ?? '';
        this.validate();
      }, 100);
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onSave(): void {
    if (this.pasteBox) {
      this.pasteBox.nativeElement.disable = true;
    }
    this.isSaveBtnEnabled = false;
    this.processDataCoreService
      .saveTagMaps([...this.tagMapsArr])
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.disable = false;
          }
          this.isSaveBtnEnabled = true;
          return throwError(() => new Error(message));
        })
      )
      .subscribe((tagmaps: ITagMapCreateUpdateResult[]) => {
        this.dialogRef.close(tagmaps);
      });
  }

  public onValidateCancel(): void {
    this.initialize();
    if (this.pasteBox) {
      this.pasteBox.nativeElement.value = '';
    }
  }

  public onProceed(): void {
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = true;
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private validate(): void {
    if (this.pastedText !== '') {
      this.isValidating = true;
      const pasted: string[][] = this.csvService.parseCSVStringToArrayString(
        this.pastedText
      );
      const data: { [key: string]: string[] } = {};
      const mandatoryHeaders: string[] = [
        'Asset Path',
        'Variable',
        'Tag Name',
        'Value Type',
      ];

      // validate headers
      for (let i = 0; i < pasted[0].length; i++) {
        const header: string = pasted[0][i];
        if (mandatoryHeaders.includes(header)) {
          data[header] = [];
        }
      }

      if (Object.keys(data).length !== mandatoryHeaders.length) {
        const missingHeaders: string[] = mandatoryHeaders.filter(
          (header: string) => !Object.keys(data).includes(header)
        );
        this.clientValidationResult =
          missingHeaders.length === 1
            ? `Header Row missing: ${missingHeaders[0]}`
            : `Header Rows missing: ${missingHeaders.join(', ')}`;
        this.isValidating = false;
        return;
      }

      // build data
      for (let i = 1; i < pasted.length; i++) {
        for (let y = 0; y < pasted[i].length; y++) {
          const header: string = pasted[0][y];
          if (data[header] != null) {
            data[header].push(pasted[i][y]);
          }
        }
      }

      const pastedDataLength: number = data[Object.keys(data)[0]].length;
      const missingRows: number[] = [];
      let missingTagNames = 0;
      // build array of ITagMapBare[]
      this.tagMapsArr = [];
      for (let i = 0; i < pastedDataLength; i++) {
        const assetPath: string = data['Asset Path'][i];
        const variable: string = data['Variable'][i];
        const tagName: string = data['Tag Name'][i];
        const valueType: string = data['Value Type'][i];
        const assetID: string = this.assetIdByPathDict[assetPath] ?? '';
        const tagID: string = this.tagIdByNameDict[tagName] ?? '';

        if (!assetPath || !variable) {
          missingRows.push(i + 1);
          continue;
        } else if (!tagName) {
          ++missingTagNames;
        }

        this.tagMapsArr.push({
          AssetVariableTypeTagMapId: null,
          AssetId: assetID,
          TagId: tagID,
          ValueTypeKey: variable !== '{none}' ? valueType : '', // ad-hoc tagmaps cannot have value type
          VariableDesc: variable !== '{none}' ? variable : '', // translate user-specified ad-hoc tagmap into api-recognized ad-hoc tagmap
        } as ITagMapBare);
      }

      if (missingRows.length > 0) {
        this.clientValidationResult =
          missingRows.length === 1
            ? `Data missing in Row ${missingRows[0]}`
            : `Data missing in Rows ${missingRows.join(', ')}`;
        this.isValidating = false;
        return;
      }

      this.totalValidTagMaps = this.tagMapsArr.length;
      if (missingTagNames > 0) {
        this.totalValidTagMaps = this.tagMapsArr.length - missingTagNames;
        if (this.totalValidTagMaps < 1) {
          this.clientValidationResult = `No tagmaps found: ${missingTagNames} Rows without Tagname present out of ${pastedDataLength} pasted tagmaps.`;
          this.isValidating = false;
          return;
        }
        this.clientValidationResult = `Ignoring ${missingTagNames} Rows without Tagname present`;
      }
      if (this.totalValidTagMaps < 1) {
        this.clientValidationResult = 'No tagmaps found.';
        this.isValidating = false;
        return;
      }

      this.onValidate(this.tagMapsArr);
    }
  }

  private onValidate(tagMaps: ITagMapBare[]): void {
    this.processDataCoreService
      .validateCreateTagMaps(tagMaps)
      .pipe(
        take(1),
        catchError((err: any) => {
          let message = '';
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = `Error: ${err.error.Results[0].Message}`;
            }
          }
          this.serverValidationResult = message;
          if (this.pasteBox) {
            this.pasteBox.nativeElement.value = '';
          }
          this.isValidating = false;
          if (!this.clientValidationResult.includes('Ignoring')) {
            this.clientValidationResult = 'Data Format OK';
          }
          return throwError(() => new Error(message));
        })
      )
      .subscribe((result: ITagMapCreateUpdateResult[]) => {
        if (result.length > 0) {
          const invalidTagMaps = result.filter(
            (res: ITagMapCreateUpdateResult) =>
              res.Error !== '' && res.Error != null
          );
          const invalidTagMapsLength = invalidTagMaps.length;

          if (invalidTagMapsLength > 0 && this.pasteBox) {
            this.totalValidTagMaps =
              this.totalValidTagMaps - invalidTagMapsLength;
            let errorText = `${this.totalValidTagMaps} valid tagmaps found. ${invalidTagMapsLength} invalid tagmaps found.\n`;
            for (const invalidTagMap of invalidTagMaps) {
              const tagName: string =
                Object.keys(this.tagIdByNameDict).find(
                  (key) => this.tagIdByNameDict[key] === invalidTagMap.TagId
                ) ?? '';
              const assetPath: string =
                Object.keys(this.assetIdByPathDict).find(
                  (key) => this.assetIdByPathDict[key] === invalidTagMap.AssetId
                ) ?? '';
              errorText += `Asset Path: ${assetPath} Tag Name: ${tagName} Error: ${invalidTagMap.Error}\n`;
            }
            this.serverValidationResult = errorText;
            if (this.totalValidTagMaps > 0) {
              this.isProceedVisible = true;
            }
          } else {
            this.serverValidationResult =
              'Data is Valid. Select "Save" below to create new tagmaps.';
            this.isSaveBtnEnabled = true;
          }
        }
        this.isValidating = false;
        if (!this.clientValidationResult.includes('Ignoring')) {
          this.clientValidationResult = 'Data Format OK';
        }
      });
  }

  private initialize(): void {
    this.totalValidTagMaps = 0;
    this.isProceedVisible = false;
    this.isSaveBtnEnabled = false;
    this.isValidating = false;
    this.clientValidationResult = '';
    this.serverValidationResult = '';
  }
}
