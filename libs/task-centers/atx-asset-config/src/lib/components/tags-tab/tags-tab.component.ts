import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { selectSelectedAsset } from '../../store/selectors/asset-nav.selectors';
import * as actions from '../../store/actions/asset-config.actions';
import { GridApi } from '@ag-grid-enterprise/all-modules';
import { tagsTabFeatureKey } from '../../store/reducers/tags-tab.reducer';
import { init } from '../../store/actions/tags-tab.actions';

@Component({
  selector: 'atx-asset-config-tags-tab',
  templateUrl: './tags-tab.component.html',
  styleUrls: ['./tags-tab.component.scss'],
})
export class TagsTabComponent implements AfterViewInit, OnDestroy {
  public selectedAsset$: Observable<string> =
    this.store.select(selectSelectedAsset);
  public isTagMapExpanded = false;
  public isTagConfigExpanded = false;
  public tagConfigGridApi: GridApi | null = null;
  private onDestroy$ = new Subject<void>();

  constructor(private store: Store) {}

  ngAfterViewInit(): void {
    this.selectedAsset$.pipe(takeUntil(this.onDestroy$)).subscribe((asset) => {
      if (asset) {
        this.store.dispatch(
          actions.updateRoute({ asset: asset, tab: tagsTabFeatureKey })
        );
      }
    });
  }

  public expand(tab: string): void {
    if (tab === 'mapping') {
      this.isTagMapExpanded = true;
    } else {
      this.isTagConfigExpanded = true;
    }
  }

  public collapse(tab: string): void {
    if (tab === 'mapping') {
      this.isTagMapExpanded = false;
    } else {
      this.isTagConfigExpanded = false;
    }
  }

  public setTagConfigGridApi(tagConfigGridApi: GridApi) {
    this.tagConfigGridApi = tagConfigGridApi;
  }

  ngOnDestroy(): void {
    this.store.dispatch(init());
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
