/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-asset-config-attribute-delete-confirmation-dialog',
  templateUrl: './attribute-delete-confirmation-dialog.component.html',
  styleUrls: ['./attribute-delete-confirmation-dialog.component.scss'],
})
export class AttributeDeleteConfirmationDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<AttributeDeleteConfirmationDialogComponent>
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  onContinue(): void {
    this.dialogRef.close(true);
  }
}
