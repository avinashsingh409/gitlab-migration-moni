import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AttributeDeleteConfirmationDialogComponent } from './attribute-delete-confirmation-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AttributeDeleteConfirmationDialogComponent', () => {
  let component: AttributeDeleteConfirmationDialogComponent;
  let fixture: ComponentFixture<AttributeDeleteConfirmationDialogComponent>;
  const mockDialogRef = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
      declarations: [AttributeDeleteConfirmationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(
      AttributeDeleteConfirmationDialogComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
