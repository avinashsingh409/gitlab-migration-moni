import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AssetDeleteConfirmationDialogComponent } from './asset-delete-confirmation-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AssetDeleteConfirmationDialogComponent', () => {
  let component: AssetDeleteConfirmationDialogComponent;
  let fixture: ComponentFixture<AssetDeleteConfirmationDialogComponent>;
  const mockDialogRef = {};
  const dialogData = {
    assetName: '',
    numOfDescendants: 0,
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: dialogData,
          },
        },
      ],
      declarations: [AssetDeleteConfirmationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetDeleteConfirmationDialogComponent);
    component = fixture.componentInstance;
    component.assetName = '';
    component.numOfDescendants = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
