/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-user-admin-asset-delete-confirmation-dialog',
  templateUrl: './asset-delete-confirmation-dialog.component.html',
  styleUrls: ['./asset-delete-confirmation-dialog.component.scss'],
})
export class AssetDeleteConfirmationDialogComponent {
  public assetName = '';
  public numOfDescendants = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AssetDeleteConfirmationDialogComponent>
  ) {
    this.assetName = data.assetName;
    this.numOfDescendants = data.numOfDescendants;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onContinue(): void {
    this.dialogRef.close(true);
  }
}
