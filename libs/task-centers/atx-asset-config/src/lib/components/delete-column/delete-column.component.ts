/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ITagResult } from '@atonix/atx-core';
import * as actions from '../../store/actions/tags-tab.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'atx-delete-column',
  templateUrl: './delete-column.component.html',
  styleUrls: ['./delete-column.component.scss'],
})
export class DeleteColumnComponent implements ICellRendererAngularComp {
  public tag: ITagResult = {} as ITagResult;

  constructor(private store: Store) {}

  agInit(params: any) {
    this.setTag(params);
  }

  delete() {
    this.store.dispatch(actions.deleteTag({ tagId: this.tag.Id }));
  }

  refresh(params: any) {
    this.setTag(params);
    return true;
  }

  setTag(params: any) {
    this.tag = params?.node?.data ?? this.tag ?? null;
  }
}
