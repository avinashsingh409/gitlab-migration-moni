import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AssetConfirmationDialogComponent } from './asset-confirmation-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AssetConfirmationDialogComponent', () => {
  let component: AssetConfirmationDialogComponent;
  let fixture: ComponentFixture<AssetConfirmationDialogComponent>;
  const mockDialogRef = {};
  const dialogData = {
    siblingToSibling: true,
    draggedAsset: '',
    targetAsset: '',
    showTickbox: true,
  };

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: dialogData,
          },
        },
      ],
      declarations: [AssetConfirmationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
