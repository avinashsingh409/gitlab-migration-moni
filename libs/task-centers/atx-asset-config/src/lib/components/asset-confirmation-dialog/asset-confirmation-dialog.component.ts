/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component, Inject } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';

@Component({
  selector: 'atx-user-admin-asset-confirmation-dialog',
  templateUrl: './asset-confirmation-dialog.component.html',
  styleUrls: ['./asset-confirmation-dialog.component.scss'],
})
export class AssetConfirmationDialogComponent {
  public siblingToSibling = false;
  public headerText = '';
  public options: any[] = [];
  public reParentText = '';
  public showTickbox = false;
  public saveSession = false;
  public showError = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AssetConfirmationDialogComponent>
  ) {
    if (data.siblingToSibling) {
      this.siblingToSibling = true;
      this.headerText = 'Choose your action:';
      this.options = [
        {
          action: 'reorder',
          text: `ReOrder '${this.data.draggedAsset}' to below '${this.data.targetAsset}'`,
          checked: false,
        },
        {
          action: 'reparent',
          text: `Move '${this.data.draggedAsset}' to new parent: '${this.data.targetAsset}'`,
          checked: false,
        },
      ];
    } else {
      this.showTickbox = this.data.showTickbox;
      this.headerText = 'Confirm your action:';
      this.reParentText = `Move '${this.data.draggedAsset}' to new parent: '${this.data.targetAsset}'`;
    }
  }

  onChange(event: MatRadioChange): void {
    this.showError = false;
    const option: any = this.options.find(
      (option: any) => option.action === event.value
    );
    if (option) {
      option.checked = true;
    }
  }

  onTickSaveSession(event: MatCheckboxChange): void {
    this.saveSession = event.checked;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onContinue(): void {
    let action = 'reparent';
    if (this.siblingToSibling) {
      const data = this.options.find((option: any) => option.checked === true);
      if (!data) {
        this.showError = true;
        return;
      }
      action = data.action;
    }
    this.dialogRef.close({ action, saveSession: this.saveSession });
  }
}
