import { TestBed } from '@angular/core/testing';

import { ModelHistoryRetrieverService } from './model-history-retriever.service';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { createMock } from '@testing-library/angular/jest-utils';

describe('ModelHistoryRetrieverService', () => {
  let service: ModelHistoryRetrieverService;
  let mockModelService: AlertsFrameworkService;

  beforeEach(() => {
    mockModelService = createMock(AlertsFrameworkService);
    TestBed.configureTestingModule({
      providers: [
        { provide: AlertsFrameworkService, useValue: mockModelService },
      ],
    });
    service = TestBed.inject(ModelHistoryRetrieverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
