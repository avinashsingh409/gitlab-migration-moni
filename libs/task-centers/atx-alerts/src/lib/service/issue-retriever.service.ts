import { Injectable } from '@angular/core';
import {
  IServerSideGetRowsParams,
  IServerSideDatasource,
} from '@ag-grid-enterprise/all-modules';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IssuesCoreService } from '@atonix/shared/api';

@Injectable({
  providedIn: 'root',
})
export class IssueRetrieverService implements IServerSideDatasource {
  constructor(private issuesCoreService: IssuesCoreService) {}

  private unsubscribe$ = new Subject<void>();

  public cancel() {
    this.unsubscribe$.next();
  }

  public getRows(params: IServerSideGetRowsParams) {
    const filters = params.request.filterModel;

    const req = {
      ...params.request,
      filterModel: filters,
    };

    this.issuesCoreService
      .getIssuesForAlerts(req)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (n) => {
          params.success({
            rowData: n.Results[0].Issues,
            rowCount: n.Results[0].NumIssues,
          });
        },
        (error: unknown) => {
          params.fail();
          console.error('Could not retrieve issues');
        }
      );
  }
}
