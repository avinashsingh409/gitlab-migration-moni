import { IActionFlyoutStateChange } from './../model/action-flyout-state-change';
import { INDModelSummary, IActionItem } from '@atonix/shared/api';

export function setActionFlyoutToggle(newVal?: boolean) {
  const result: IActionFlyoutStateChange = {
    event: 'ToggleActionFlyout',
    newValue: newVal,
  };
  return result;
}

export function setWatch(models: INDModelSummary[], newVal?: string) {
  const result: IActionFlyoutStateChange = {
    event: 'SetWatch',
    newValue: newVal,
    models,
  };
  return result;
}

export function clearWatch(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'ClearWatch',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function clearAlert(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'ClearAlert',
    models,
  };
  return result;
}

export function refresh(models: INDModelSummary[], newVal?: string) {
  const result: IActionFlyoutStateChange = {
    event: 'Refresh',
    newValue: newVal,
    models,
  };
  return result;
}

export function setCustomWatch(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'SetCustomWatch',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function saveDiagnose(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'SaveDiagnose',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function saveMaintenance(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'SaveMaintenance',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function saveNote(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'SaveNote',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function clearDiagnose(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'ClearDiagnose',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function clearMaintenance(
  models: INDModelSummary[],
  newVal?: IActionItem,
  actionType?: string
) {
  const result: IActionFlyoutStateChange = {
    event: 'ClearMaintenance',
    newValue: newVal,
    newAction: actionType,
    models,
  };
  return result;
}

export function newIssue(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'NewIssue',
    models,
  };
  return result;
}

export function showRelatedModels(assetGuid: string) {
  const result: IActionFlyoutStateChange = {
    event: 'ShowRelatedModels',
    newValue: assetGuid,
  };
  return result;
}

export function showRelatedIssues(assetGuid: string) {
  const result: IActionFlyoutStateChange = {
    event: 'ShowRelatedIssues',
    newValue: assetGuid,
  };
  return result;
}

export function modelConfig(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'ModelConfig',
    models,
  };
  return result;
}

export function opModeConfig(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'OpModeConfig',
    models,
  };
  return result;
}

export function diagnosticDrilldown(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'DiagnosticDrilldown',
    models,
  };
  return result;
}

export function dataExplorer(models: INDModelSummary[]) {
  const result: IActionFlyoutStateChange = {
    event: 'DataExplorer',
    models,
  };
  return result;
}
