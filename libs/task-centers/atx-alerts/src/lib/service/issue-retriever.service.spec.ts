/* eslint-disable @typescript-eslint/no-empty-function */
import { TestBed, inject } from '@angular/core/testing';

import { IssueRetrieverService } from './issue-retriever.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IssuesCoreService } from '@atonix/shared/api';
import { of } from 'rxjs';
import {
  IServerSideGetRowsRequest,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { IIssueDataRetrieval, IIssueSummary } from '@atonix/shared/api';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('IssueRetrieverService', () => {
  const dataRetrieval: IIssueDataRetrieval = {
    Count: 200,
    StatusCode: 200,
    Success: true,
    Type: 'type',
    Results: [
      {
        Issues: [
          {
            AssetIssueGUID: '123456789',
            AssetIssueID: 123456789,
            CategoryDesc: 'Test',
            CategoryID: 1,
            IssueTitle: null,
            IssueSummary: null,
            ShortSummary: null,
            Description: null,
            Client: null,
            StationGroup: null,
            Station: null,
            StationAssetAbbrev: null,
            Unit: null,
            UnitAssetAbbrev: null,
            System: null,
            SystemAssetClassTypeID: 1,
            Asset: null,
            AssetClassTypeID: 2,
            AssetID: 3,
            ImpactsString: null,
            ImpactTotal: 4,
            Confidence_Pct: 5,
            IssueTypeDesc: null,
            IssueTypeID: 6,
            IssueClassTypeID: 7,
            IssueClassTypeDescription: null,
            Priority: null,
            CreatedBy: null,
            CreateDate: new Date('1/1/2020'),
            ChangeDate: new Date('1/1/2020'),
            ChangedBy: null,
            ActivityStatus: null,
            ResolutionStatus: null,
            CloseDate: new Date('1/1/2020'),
            IsSubscribed: true,
            GlobalID: null,
            OpenDuration: 8,
            OpenDate: new Date('1/1/2020'),
            AssignedTo: null,
            Scorecard: true,
            ResolveBy: new Date('1/1/2020'),
            AssetClassTypeAbbrev: null,
            AssetClassTypeDesc: null,
            AssetTypeAbbrev: null,
            AssetTypeDesc: null,
            CreatedByUserID: 9,
            CanEdit: true,
            CanDelete: true,
            CanEditIssueStatus: true,
          } as IIssueSummary,
        ],
        NumIssues: 1,
      },
    ],
  };

  const mockIssuesCoreService = {
    getIssuesForAlerts(params: IServerSideGetRowsRequest) {
      return of(dataRetrieval);
    },
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {},
        },
        {
          provide: IssuesCoreService,
          useValue: mockIssuesCoreService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    })
  );

  it('should be created', () => {
    const service: IssueRetrieverService = TestBed.inject(
      IssueRetrieverService
    );
    expect(service).toBeTruthy();
  });

  it('should get rows', inject(
    [IssueRetrieverService],
    (service: IssueRetrieverService) => {
      const params: IServerSideGetRowsParams = {
        context: null,
        request: {
          startRow: 1,
          endRow: 100,
          rowGroupCols: [
            {
              id: '123',
              displayName: 'Test Only',
              field: 'test',
              aggFunc: null,
            },
          ],
          valueCols: null,
          pivotCols: null,
          pivotMode: false,
          groupKeys: ['test'],
          filterModel: null,
          sortModel: null,
        },
        parentNode: null,
        successCallback: (rowsThisPage: any[], lastRow: number) => {},
        failCallback: () => {},
        api: null,
        columnApi: null,
        success: () => {},
        fail: () => {},
      };
      jest.spyOn(params, 'success');

      service.getRows(params);
      expect(params.success).toHaveBeenCalledWith({
        rowData: dataRetrieval.Results[0].Issues,
        rowCount: dataRetrieval.Results[0].NumIssues,
      });
    }
  ));
});
