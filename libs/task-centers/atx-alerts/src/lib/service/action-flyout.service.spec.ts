import { TestBed } from '@angular/core/testing';
import { setActionFlyoutToggle } from './action-flyout.service';
import { IActionFlyoutStateChange } from '../model/action-flyout-state-change';

describe('ActionFlyoutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));
  it('should be created', () => {
    expect(setActionFlyoutToggle).toBeTruthy();
  });

  it('should return set the toggle value of Action FlyOut state change', () => {
    const expectedOutput: IActionFlyoutStateChange = {
      event: 'ToggleActionFlyout',
      newValue: true,
    };
    expect(setActionFlyoutToggle(true)).toEqual(expectedOutput);
  });
});
