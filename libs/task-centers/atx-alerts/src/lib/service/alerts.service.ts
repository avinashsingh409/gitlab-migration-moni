import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { INavigationState } from '@atonix/atx-navigation';
import {
  IAlertStateChange,
  ISelectedViewAsset,
} from '../model/alerts-state-change';
import { IScreeningViewStateChange } from '../model/screening-view-state-change';
import {
  INDModelSummary,
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
} from '@atonix/shared/api';
import { sigFig } from '@atonix/atx-core';
import { getDefaultHighchartsOptions } from '@atonix/atx-chart';

@Injectable({
  providedIn: 'root',
})
export class AlertsService {
  private navStateObservable$: BehaviorSubject<INavigationState>;

  public getNavState() {
    return this.navStateObservable$;
  }
}

export function selectingModel(newVal?: INDModelSummary[]) {
  const result: IScreeningViewStateChange = {
    event: 'SelectingModel',
    newValue: newVal,
  };
  return result;
}

export function selectedViewAsset(
  AssetID: string,
  ModelExtID: string,
  ModelID: string
) {
  const result: IAlertStateChange = {
    event: 'GetRowData',
    newValue: { AssetID, ModelExtID, ModelID } as ISelectedViewAsset,
  };
  return result;
}

export function setIconChange(newVal?: boolean) {
  const result: IAlertStateChange = {
    event: 'IconChange',
    newValue: newVal,
  };

  return result;
}

export function setSelectedTrend(newVal?: string) {
  const result: IAlertStateChange = {
    event: 'SelectedTrend',
    newValue: newVal,
  };
  return result;
}

export function renderModelContextChart(newVal?: any) {
  const result: IAlertStateChange = {
    event: 'RenderModelContextChart',
    newValue: newVal,
  };
  return result;
}
