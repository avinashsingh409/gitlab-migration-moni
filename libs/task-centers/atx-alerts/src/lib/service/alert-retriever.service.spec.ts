import { TestBed } from '@angular/core/testing';

import { AlertRetrieverService } from './alert-retriever.service';
import { AlertsCoreService } from '@atonix/shared/api';
import { createMock } from '@testing-library/angular/jest-utils';

describe('AlertRetrieverService', () => {
  let service: AlertRetrieverService;
  let alertsCoreService: AlertsCoreService;

  beforeEach(() => {
    alertsCoreService = createMock(AlertsCoreService);

    TestBed.configureTestingModule({
      providers: [{ provide: AlertsCoreService, useValue: alertsCoreService }],
    });
    service = TestBed.inject(AlertRetrieverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
