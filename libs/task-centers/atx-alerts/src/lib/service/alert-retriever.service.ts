import { Injectable } from '@angular/core';
import {
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AlertsCoreService } from '@atonix/shared/api';

@Injectable({
  providedIn: 'root',
})
export class AlertRetrieverService implements IServerSideDatasource {
  constructor(private alertsCoreService: AlertsCoreService) {}

  public totalModels = null;
  public downloadDisabled: boolean;
  private unsubscribe$ = new Subject<void>();

  public cancel() {
    this.unsubscribe$.next();
  }

  public getRows(params: IServerSideGetRowsParams) {
    const request = params.request;
    this.downloadDisabled = true;

    this.alertsCoreService
      .getAlerts(request)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (n) => {
          if (n.Summary.length === 0) {
            this.totalModels = 0;
          } else if (n.NumModelSummary !== 0) {
            this.totalModels = n.NumModelSummary;
          }

          if (this.totalModels > 0) {
            this.downloadDisabled = false;
          }

          params.success({ rowData: n.Summary, rowCount: n.NumRowSummary });
        },
        (error: unknown) => {
          this.downloadDisabled = true;
          params.fail();
          console.error('Could not retrieve alerts');
        }
      );
  }
}
