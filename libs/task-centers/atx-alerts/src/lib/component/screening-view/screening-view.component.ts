/* eslint-disable max-len */
import {
  Component,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  OnChanges,
  ViewChild,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  ElementRef,
} from '@angular/core';
import {
  GridOptions,
  DisplayedColumnsChangedEvent,
  Module,
  GridReadyEvent,
  ModelUpdatedEvent,
  GridApi,
  DetailGridInfo,
  AgGridEvent,
  DragStoppedEvent,
  ColumnVisibleEvent,
  ColumnPinnedEvent,
  SortChangedEvent,
  FilterChangedEvent,
  ColumnRowGroupChangedEvent,
  ValueFormatterParams,
  ColumnApi,
  ICellRendererParams,
  Column,
  CellDoubleClickedEvent,
  RowGroupOpenedEvent,
  ClipboardModule,
  ServerSideStoreType,
  ColDef,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  MenuModule,
  RangeSelectionModule,
  RowGroupingModule,
  ServerSideRowModelModule,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
  ViewportRowModelModule,
  MasterDetailModule,
  ColumnMovedEvent,
} from '@ag-grid-enterprise/all-modules';

import isNumber from 'lodash/isNumber';
import { MatButton } from '@angular/material/button';
import { sigFig, isNil } from '@atonix/atx-core';
import { SavedToolPanelComponent } from '../saved-tool-panel/saved-tool-panel.component';
import {
  INDModelSummary,
  IGridParameters,
  IListState,
} from '@atonix/shared/api';
import { ScreeningActionsRendererComponent } from './screening-view-renderer/screening-actions-renderer.component';
import { AlertRetrieverService } from '../../service/alert-retriever.service';
import { ActionFilterComponent } from './filter/action-filter/action-filter.component';
import { IssueRetrieverService } from '../../service/issue-retriever.service';
import { setListState, getListState } from '../../store/state/persistence';
import { dateFormatter } from '../model-history/formatter';
import { TypeRendererComponent } from './screening-view-renderer/screening-type-renderer.component';
import { IUpdatedModels } from '../../store/state/summary-state';
import { Clipboard } from '@angular/cdk/clipboard';
import { LoggerService } from '@atonix/shared/utils';
import { NavFacade } from '@atonix/atx-navigation';

@Component({
  selector: 'atx-screening-view',
  templateUrl: './screening-view.component.html',
  styleUrls: ['./screening-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScreeningViewComponent implements OnChanges, OnDestroy {
  @Input() expanded: boolean;
  @Input() showRefresh: boolean;
  @Input() asset: string;
  @Input() selectedModels: INDModelSummary[];
  @Input() listTheme: string;
  @Input() listState: IListState;
  @Input() floatingFilter: boolean;
  @Input() updatedModels: IUpdatedModels;
  @Input() reloadScreeningView: EventEmitter<boolean>;
  @Input() refreshDate: Date;
  @Input() clearDataFilters: boolean;
  @Output() expandClick = new EventEmitter();
  @Output() selectedModelsChanged = new EventEmitter<INDModelSummary[]>();
  @Output() refresh = new EventEmitter();
  @Output() stateChanged = new EventEmitter<IListState>();
  @Output() diagnosticDrilldown = new EventEmitter();
  @Output() floatingFilterToggle = new EventEmitter();
  @Output() issueSelected = new EventEmitter<string>();
  @Output() download = new EventEmitter<IGridParameters>();
  @ViewChild('refreshButton') button: MatButton;
  @ViewChild('screeningBody') screeningBody: ElementRef;

  gridApi: GridApi;
  columnApi: ColumnApi;

  totalModels = '-';
  disableDownload: boolean;
  lastRefresh: Date;
  screeningViewFilters: any[] = [];
  hiddenFilters = ['AssetID'];
  public isArrowDown = true;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    RowGroupingModule,
    ServerSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
    ViewportRowModelModule,
    MasterDetailModule,
  ];
  private storeType: ServerSideStoreType = 'partial';

  constructor(
    private alertRetriever: AlertRetrieverService,
    private issueRetriever: IssueRetrieverService,
    private changeDetector: ChangeDetectorRef,
    private navFacade: NavFacade,
    private logger: LoggerService,
    private clipboard: Clipboard
  ) {}

  // Configurations of the detail grid options
  detailGridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    columnDefs: [
      {
        colId: 'AssetID',
        headerName: 'Asset ID',
        field: 'AssetID',
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'OpenIssuesAssetID',
        headerName: 'Open Issues AssetID',
        field: 'OpenIssuesAssetID',
        hide: true,
        filter: 'agNumberColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'AssetIssueID',
        headerName: 'IssueID',
        field: 'AssetIssueID',
      },
      {
        colId: 'Title',
        headerName: 'Title',
        field: 'IssueTitle',
      },
      {
        colId: 'ChangeDate',
        headerName: 'Change Date',
        field: 'ChangeDate',
        sort: 'desc',
        valueFormatter: dateFormatter,
      },
      {
        colId: 'CreateDate',
        headerName: 'Created Date',
        field: 'CreateDate',
        valueFormatter: dateFormatter,
      },
      {
        colId: 'Priority',
        headerName: 'Priority',
        field: 'Priority',
      },
      {
        colId: 'ResolutionStatus',
        headerName: 'Resolution',
        field: 'ResolutionStatus',
      },
      {
        colId: 'ShortSummary',
        headerName: 'Short Summary',
        field: 'ShortSummary',
      },
    ],
    defaultColDef: {
      resizable: true,
      cellClass: 'cell-selectable',
    },
    onCellDoubleClicked: (event: CellDoubleClickedEvent) => {
      this.issueSelected.emit(event.data.GlobalID);
    },
  };

  // Configurations of the master grid options
  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    rowGroupPanelShow: 'never',
    enableRangeSelection: true,
    animateRows: true,
    debug: false,
    cacheBlockSize: 500,
    rowSelection: 'multiple',
    suppressCopyRowsToClipboard: true,
    rowHeight: 30,
    blockLoadDebounceMillis: 500,
    maxConcurrentDatasourceRequests: 1,
    tooltipShowDelay: 0,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      cellClass: 'cell-selectable',
    },
    columnDefs: [
      {
        colId: 'AssetID',
        headerName: 'Asset ID',
        field: 'AssetID',
        sortable: false,
        enableRowGroup: true,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'ActionDiagnose',
        maxWidth: 65,
        headerComponentParams: {
          template: `
              <div class="ag-cell-label-container icon-only" role="presentation">
                <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
                <div ref="eLabel" class="ag-header-cell-label" role="presentation">
                  <img src="./assets/emblem-diagnose.svg" />
                  <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
                  <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
                  <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
                  <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
                  <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
                  <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
                </div>
              </div>
              `,
        },
        headerName: 'Diagnose',
        headerTooltip: 'Diagnose',
        field: 'ActionDiagnose',
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.ActionDiagnose ?? false;
          const result: any = {
            component: 'actionsCellRenderer',
            params: {
              icon: 'error_outline',
              field: 'ActionDiagnose',
              tooltip: 'Diagnose',
              value,
            },
          };
          return result;
        },
        sortable: true,
        enableRowGroup: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
      },
      {
        colId: 'ActionWatch',
        maxWidth: 65,
        headerComponentParams: {
          template: `
              <div class="ag-cell-label-container icon-only" role="presentation">
                <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
                <div ref="eLabel" class="ag-header-cell-label" role="presentation">
                  <img src="./assets/emblem-watch.svg" />
                  <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
                  <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
                  <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
                  <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
                  <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
                  <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
                </div>
              </div>
              `,
        },
        headerName: 'Watch',
        headerTooltip: 'Watch',
        field: 'ActionWatch',
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.ActionWatch ?? false;
          return {
            component: 'actionsCellRenderer',
            params: {
              icon: 'radio_button_checked',
              field: 'ActionWatch',
              tooltip: 'Watch',
              value,
            },
          } as any;
        },
        sortable: true,
        enableRowGroup: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
      },
      {
        colId: 'ActionMaintenance',
        maxWidth: 65,
        headerComponentParams: {
          template: `
              <div class="ag-cell-label-container icon-only" role="presentation">
                <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
                <div ref="eLabel" class="ag-header-cell-label" role="presentation">
                  <img src="./assets/emblem-maintainence.svg" />
                  <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
                  <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
                  <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
                  <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
                  <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
                  <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
                </div>
              </div>
              `,
        },
        headerName: 'Maintenance',
        headerTooltip: 'Maintenance',
        field: 'ActionModelMaintenance',
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.ActionModelMaintenance ?? false;
          return {
            component: 'actionsCellRenderer',
            params: {
              icon: 'trip_origin',
              field: 'ActionModelMaintenance',
              tooltip: 'Maintenance',
              value,
            },
          } as any;
        },
        sortable: true,
        enableRowGroup: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
      },
      {
        colId: 'Alert',
        maxWidth: 65,
        headerComponentParams: {
          template: `
              <div class="ag-cell-label-container icon-only" role="presentation">
                <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
                <div ref="eLabel" class="ag-header-cell-label" role="presentation">
                  <img src="./assets/emblem-alerts.svg" />
                  <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
                  <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
                  <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
                  <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
                  <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
                  <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
                </div>
              </div>
              `,
        },
        headerName: 'Alert',
        headerTooltip: 'Alert',
        field: 'Alert',
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Alert ?? false;
          return {
            component: 'actionsCellRenderer',
            params: {
              icon: 'warning',
              field: 'Alert',
              tooltip: 'Alert',
              value,
            },
          } as any;
        },
        sortable: true,
        enableRowGroup: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
      },
      {
        colId: 'ExternalID',
        headerName: 'External ID',
        field: 'ModelExtID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'Issues',
        headerName: 'Issues',
        field: 'OpenIssues',
        filter: 'agNumberColumnFilter',
        sortable: true,
        cellClass: 'number-column',
        width: 90,
        type: 'numericColumn',
        cellRenderer: 'agGroupCellRenderer', // This column will trigger the collapsing of row
      },
      {
        colId: 'NotePriority',
        headerName: 'Note Priority',
        field: 'NotePriority',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['', 'Low', 'Medium', 'High'],
          valueFormatter: this.customEmptyTextFormatter('Unspecified'),
        },
        enableRowGroup: true,
        sortable: true,
      },
      {
        colId: 'UnitAbbrev',
        headerName: 'Unit',
        field: 'UnitAbbrev',
        sortable: true,
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'ModelName',
        headerName: 'Model Name',
        field: 'ModelName',
        tooltipField: 'ModelName',
        sortable: true,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'PercentOutOfBounds',
        headerName: '% OOB',
        field: 'PercentOutOfBounds',
        sortable: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        valueFormatter: this.wholeNumberFormatter,
        type: 'numericColumn',
        cellStyle: { textAlign: 'right' },
        // cellClass: 'number-column',
        width: 110,
      },
      {
        colId: 'PercentExpected',
        headerName: '% EXP',
        field: 'PercentExpected',
        sortable: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        valueFormatter: this.wholeNumberFormatter,
        type: 'numericColumn',
        cellStyle: { textAlign: 'right' },
        // cellClass: 'number-column',
        width: 90,
      },
      {
        colId: 'Actual',
        headerName: 'Actual',
        field: 'Actual',
        sortable: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        valueFormatter: this.sigFigFormatter,
        type: 'numericColumn',
        // cellClass: 'number-column',
        width: 90,
      },
      {
        colId: 'LowerLimit',
        headerName: 'Lower',
        field: 'LowerLimit',
        sortable: true,
        width: 90,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        type: 'numericColumn',
        valueFormatter: this.sigFigFormatter,
        cellClassRules: {
          'lower-limit-cell': (params) => {
            return params?.data?.Actual < params?.data?.LowerLimit;
          },
        },
      },
      {
        colId: 'Expected',
        headerName: 'Expected',
        field: 'Expected',
        sortable: true,
        width: 110,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        type: 'numericColumn',
        valueFormatter: this.sigFigFormatter,
        // cellClass: 'number-column'
      },
      {
        colId: 'UpperLimit',
        headerName: 'Upper',
        field: 'UpperLimit',
        sortable: true,
        width: 90,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        type: 'numericColumn',
        valueFormatter: this.sigFigFormatter,
        cellClassRules: {
          'upper-limit-cell': (params) => {
            return params?.data?.Actual > params?.data?.UpperLimit;
          },
        },
      },
      {
        colId: 'EngUnits',
        headerName: 'Units',
        field: 'EngUnits',
        sortable: true,
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'LastNote',
        headerName: 'Last Note',
        field: 'LastNote',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'LastNote',
      },
      {
        colId: 'LastNoteDate',
        headerName: 'Last Note Date',
        field: 'LastNoteDate',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'PrioritizedOpMode',
        headerName: 'Mode',
        field: 'PrioritizedOpMode',
        sortable: true,
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'Score',
        headerName: 'Score',
        field: 'Score',
        sortable: true,
        enableValue: true,
        width: 90,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'avg'],
        // cellClass: 'number-column'
        type: 'numericColumn',
      },
      {
        colId: 'AlertTypesActive',
        headerName: 'Alerts',
        field: 'AlertTypesActive',
        sortable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: [
            '(FRQ) Anomaly Frequency',
            '(HHA) Hi-Hi Alert',
            '(LLA) Low-Low Alert',
            '(AAF) Area Alert - Fast',
            '(AAS) Area Alert - Slow',
            '(FRZ) Frozen Data Alert',
            '(OSC) Oscillations',
            '(IOR) Ignore Override',
            '(WOR) Watch Override',
          ],
        },
      },
      {
        colId: 'ModelTypeAbbrev',
        headerName: 'Model Type',
        field: 'ModelTypeAbbrev',
        sortable: true,
        filter: 'agSetColumnFilter',
        tooltipField: 'ModelTypeAbbrev',
        filterParams: {
          values: [
            'APR',
            'Rolling Average',
            'Fixed Limits',
            'Frozen Data Check',
            'External Model',
            'Moving Average',
            'Forecast',
            'Rate of Change',
          ],
        },
        enableRowGroup: true,
      },
      {
        colId: 'AddedToAlerts',
        headerName: 'Added To Alerts',
        field: 'AddedToAlerts',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'TagGroup',
        headerName: 'Tag Group',
        field: 'TagGroup',
        sortable: true,
        tooltipField: 'TagGroup',
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'TagName',
        headerName: 'Tag Name',
        field: 'TagName',
        sortable: true,
        tooltipField: 'TagName',
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'AssetCriticality',
        headerName: 'Asset Criticality',
        field: 'AssetCriticality',
        sortable: true,
        width: 150,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: [1, 2, 3],
        },
        enableRowGroup: true,
        // cellClass: 'number-column'
        type: 'numericColumn',
      },
      {
        colId: 'ModelCriticality',
        headerName: 'Model Criticality',
        field: 'ModelCriticality',
        sortable: true,
        width: 150,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['1', '2', '3'],
        },
        enableRowGroup: true,
        // cellClass: 'number-column'
        type: 'numericColumn',
      },
      {
        colId: 'Criticality',
        headerName: 'Criticality',
        field: 'Criticality',
        sortable: true,
        width: 150,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['1', '2', '3', '4', '6', '9'],
        },
        enableRowGroup: true,
        type: 'numericColumn',
      },
      {
        colId: 'AlertPriority',
        headerName: 'Alert Priority',
        field: 'AlertPriority',
        sortable: true,
        width: 130,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Low', 'High'],
        },
        enableRowGroup: true,
        type: 'numericColumn',
        // cellClass: 'number-column'
      },
      {
        colId: 'OutOfBounds',
        headerName: 'OOB',
        field: 'OutOfBounds',
        sortable: true,
        cellRenderer: (params: ICellRendererParams) => {
          if (
            params?.data?.OutOfBounds == 'true' ||
            params?.data?.OutOfBounds === true
          ) {
            return `<i class="material-icons" title="OOB">check</i>`;
          }
          return '';
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        enableRowGroup: true,
      },
      {
        colId: 'Ignore',
        headerName: 'Ignore',
        field: 'Ignore',
        sortable: true,
        enableValue: true,
        filter: 'agSetColumnFilter',
        allowedAggFuncs: ['count'],
        filterParams: {
          values: ['true', 'false'],
        },
        enableRowGroup: true,
      },
      {
        colId: 'StatusInfo',
        headerName: 'Status Info',
        field: 'StatusInfo',
        sortable: true,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'AssetGUID',
        headerName: 'Asset',
        field: 'AssetGUID',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'AssetDesc',
        enableRowGroup: true,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.AssetDesc;
          return {
            component: 'typeCellRenderer',
            params: {
              field: 'AssetGUID',
              value,
            },
          } as any;
        },
      },
      {
        colId: 'AssetClassTypeID',
        headerName: 'Asset Type',
        field: 'AssetClassTypeID',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'AssetClassType',
        enableRowGroup: true,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.AssetClassType;
          return {
            component: 'typeCellRenderer',
            params: {
              field: 'AssetClassTypeID',
              value,
            },
          } as any;
        },
      },
      {
        colId: 'VariableTypeID',
        headerName: 'Data Type',
        field: 'VariableTypeID',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'VariableType',
        enableRowGroup: true,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.VariableType;
          return {
            component: 'typeCellRenderer',
            params: {
              field: 'VariableTypeID',
              value,
            },
          } as any;
        },
      },
    ],
    masterDetail: true,
    isRowMaster: (dataItem) => {
      // If there are open issues, the row can be collapsible
      return dataItem ? dataItem.OpenIssues > 0 : false;
    },
    detailCellRendererParams: {
      detailGridOptions: this.detailGridOptions,
      getDetailRowData: (params) => {
        // This will get the detail grid of the selected row using the row id.
        const detailGridInfo = this.gridOptions.api.getDetailGridInfo(
          params.node.id
        );

        this.updateIssuesAssetIDFilter(
          detailGridInfo,
          this.asset,
          params.data.AssetID
        );

        detailGridInfo.api.setServerSideDatasource(this.issueRetriever);
        detailGridInfo.api.stopEditing();
      },
    } as any,
    getRowHeight: (params) => {
      // Dynamically get the row's height if it has detail grid.
      // Will show max of 3 rows on the detail grid then vertical scroll will be enabled.
      if (params.node && params.node.detail) {
        const offset = 40;
        const maxRowLength =
          params.data.OpenIssues > 3 ? 3 : params.data.OpenIssues;
        const allDetailRowHeight =
          maxRowLength * params.api.getSizesForCurrentTheme().rowHeight;
        const gridSizes = params.api.getSizesForCurrentTheme();
        return allDetailRowHeight + gridSizes.headerHeight + offset;
      }
    },
    icons: {
      column:
        '<span class="ag-icon ag-icon-column " style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
      'my-views':
        '<span class="ag-icon ag-icon-my-views" style="background: url(assets/my-views.svg) no-repeat center;" data-cy="sideButtonMyViews"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
        {
          id: 'views',
          labelDefault: 'My Views',
          labelKey: 'my-views',
          iconKey: 'my-views',
          toolPanel: 'saveToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    components: {
      saveToolPanel: SavedToolPanelComponent,
      actionsCellRenderer: ScreeningActionsRendererComponent,
      actionFilter: ActionFilterComponent,
      typeCellRenderer: TypeRendererComponent,
    },
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.screeningBody.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-column'
      ).title = 'Columns';
      this.screeningBody.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-filters'
      ).title = 'Filters';
      this.screeningBody.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-my-views'
      ).title = 'My Views';
      this.initializeGrid();
    },
    getChildCount: (data) => {
      if (data && data.Count) {
        return data.Count;
      }
      return 0;
    },
    getRowId: (params) => {
      if (params.data) {
        return String(params.data.ModelID);
      }
      return null;
    },
    onDisplayedColumnsChanged: (event: DisplayedColumnsChangedEvent) => {
      const hiddenCols: string[] = [
        'startAssetID',
        'AssetID',
        'OpenIssuesAssetID',
      ];
      const colsToHide: string[] = [];

      for (const c of hiddenCols) {
        const myCol = event.columnApi.getColumn(c);
        if (myCol && myCol.isVisible()) {
          colsToHide.push(c);
        }
      }
      if (colsToHide.length > 0) {
        event.columnApi.setColumnsVisible(colsToHide, false);
      }
    },
    onModelUpdated: (event: ModelUpdatedEvent) => {
      this.disableDownload = true;
      this.getTotalModels();
      this.getDownloadableState();
      this.changeDetector.detectChanges();

      this.alertRetriever.downloadDisabled = true;

      if (this.selectedModels?.length > 0) {
        this.setSelectedModels(this.selectedModels);
      }
    },
    onDragStopped: (event: DragStoppedEvent) => {
      this.saveState(event);
    },
    onColumnVisible: (event: ColumnVisibleEvent) => {
      this.saveState(event);
    },
    onColumnMoved: (event: ColumnMovedEvent) => {
      this.saveState(event);
    },
    onColumnPinned: (event: ColumnPinnedEvent) => {
      if (event) {
        this.logger.feature('gridpin', 'alerts');
      }
      this.saveState(event);
    },
    onSortChanged: (event: SortChangedEvent) => {
      if (event) {
        this.logger.feature('gridsort', 'alerts');
      }
      this.saveState(event);
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (event) {
        this.getFilters();
      }
      this.saveState(event);
    },
    onColumnRowGroupChanged: (event: ColumnRowGroupChangedEvent) => {
      if (event.column) {
        this.logger.feature('gridgroup', 'alerts');
        this.logger.gridAction('draggroup', event.column.getColId(), 'alerts');
      }
      this.saveState(event);
    },
    onRowGroupOpened: (event: RowGroupOpenedEvent) => {
      // NOTE: This is for future purposes in supporting model downloads to include group formatting in the csv file
      // TODO: Obtain group keys here to be passed on to dowload models endpoint
      // TODO: Store it somewhere so that downloadModels function can access it whenever download button is clicked
    },
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.asset?.currentValue) {
      this.updateAssetFilter(changes.asset.currentValue);
      this.lastRefresh = new Date();
    }

    if (changes?.listState?.currentValue) {
      this.updateListState(changes.listState.currentValue);
    }

    // Updates the RowNode data when there are updates
    if (changes.selectedModels?.currentValue) {
      const newModels = changes.selectedModels.currentValue;
      if (this.gridApi) {
        const keys = Object.keys(newModels);
        for (const key of keys) {
          const nodeToUpdate = this.gridApi.getRowNode(
            newModels[key].ModelID.toString()
          );
          const newNode = newModels[key];
          if (nodeToUpdate) {
            nodeToUpdate.setData(newNode);
          }
        }
      }
    }

    if (changes.floatingFilter && this.gridApi) {
      this.setFloatingFilterValue(changes.floatingFilter.currentValue);
    }

    if (changes.updatedModels && !changes.updatedModels.firstChange) {
      this.updateModels(changes.updatedModels.currentValue);
    }

    if (changes?.refreshDate?.currentValue) {
      this.refreshGrid();
      this.lastRefresh = changes.refreshDate.currentValue;
    }
  }

  setFloatingFilterValue(newValue: boolean) {
    if (this.columnApi && this.gridApi) {
      const columnDefs = this.gridApi.getColumnDefs();
      columnDefs.forEach((colDef: ColDef) => {
        colDef.floatingFilter = newValue;
      });
      this.gridApi.setColumnDefs(columnDefs);
    }
  }

  ngOnDestroy(): void {
    if (this.gridApi) {
      this.alertRetriever.cancel();
      this.issueRetriever.cancel();
      this.gridApi.destroy();
      this.gridApi = null;
      this.columnApi = null;
    }
  }

  initializeGrid() {
    if (this.columnApi && this.gridApi) {
      const savedList = this.listState || getListState();
      if (savedList) {
        if (savedList.columns) {
          this.columnApi.applyColumnState({
            state: savedList.columns,
            applyOrder: true,
          });
        }
        if (savedList.sort) {
          this.columnApi.applyColumnState({ state: savedList.sort });
        }
        if (savedList.filter) {
          let myFilter = savedList.filter;
          myFilter = {
            ...myFilter,
            AssetID: this.getAssetFilter(),
          };
          this.gridApi.setFilterModel(myFilter);
        }
        if (savedList.groups) {
          this.columnApi.setColumnGroupState(savedList.groups);
        }
      } else {
        if (this.clearDataFilters) {
          this.gridApi.setFilterModel({
            AssetID: this.getAssetFilter(),
          });
        } else {
          this.gridApi.setFilterModel({
            AssetID: this.getAssetFilter(),
            Alert: {
              values: ['true'],
              filterType: 'set',
            },
            ActionWatch: {
              values: ['false'],
              filterType: 'set',
            },
            Ignore: {
              values: ['false'],
              filterType: 'set',
            },
            // TODO: AlertTypesActive:{}
          });
        }
      }

      this.gridApi.setServerSideDatasource(this.alertRetriever);
    }
  }
  getContextMenuItems(params) {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }
  setSelectedModels(selectedModels: INDModelSummary[]) {
    if (this.gridApi) {
      // First, turn the ids into sets.  These are basically unique lists
      const oldValues = new Set(
        this.gridApi.getSelectedNodes().map((n) => n.data.ModelID)
      );
      const newValues = new Set((selectedModels || []).map((n) => n.ModelID));

      // If the two sets have the same number of items and they have the same number of items when combined
      // the sets are equal and we can just stop.
      if (oldValues.size === newValues.size) {
        const allSelected = new Set([...oldValues, ...newValues]);
        if (allSelected.size === oldValues.size) {
          return;
        }
      }

      this.gridApi.forEachNode((node) => {
        if (node?.data) {
          node.setSelected(newValues.has(node.data.ModelID), false, true);
        }
      });
    }
  }

  selectionChanged() {
    const selectedRows = this.gridApi.getSelectedRows();
    this.selectedModelsChanged.emit(selectedRows);
  }

  // They just want the decimal rounded to the hundredths place
  decimalFormatter(params) {
    if (params.value) {
      if (params.value % 1 !== 0) {
        // checks if not whole number
        return params.value.toFixed(2).toString();
      } else {
        return params.value;
      }
    } else {
      return null;
    }
  }

  percentFormatter(params) {
    if (typeof params.value === 'number') {
      // checks if not whole number
      return Number((params.value ?? 0) / 100)
        .toLocaleString(undefined, {
          style: 'percent',
          minimumFractionDigits: 2,
        })
        .toString();
    } else {
      return params?.value ?? null;
    }
  }

  sigFigFormatter(params) {
    const input: number = params?.value ?? null;
    const digits = 4;

    const output = sigFig(input, digits);
    return String(output);
  }

  wholeNumberFormatter(params) {
    const input: number = params?.value ?? null;
    const output = Math.round(input);
    return String(output);
  }

  // This will format a number to its specified currency
  currencyNumberFormatter(params: ValueFormatterParams) {
    let result = '';
    if (!isNil(params.value)) {
      if (isNumber(params.value)) {
        result = params.value.toLocaleString(undefined, {
          style: 'currency',
          currency: 'USD',
        });
      } else {
        result = String(params.value);
      }
    }
    return result;
  }

  customEmptyTextFormatter(displayAs: string) {
    return (params: ValueFormatterParams) =>
      !params.value ? 'Unspecified' : params.value;
  }

  getAssetFilter() {
    return {
      filter: this.asset,
      filterType: 'text',
      type: 'contains',
    };
  }

  // This will save the state of the list
  // The saved state will be use in saving and displaying filters.
  saveState(event: AgGridEvent) {
    const state: IListState = {
      columns: event.columnApi.getColumnState(),
      sort: this.getSortState(event.columnApi),
      filter: event.api.getFilterModel(),
      groups: event.columnApi.getColumnGroupState(),
    };
    setListState(state);
    this.stateChanged.emit(state);
  }

  // This will update the state of the list
  updateListState(listState: IListState) {
    if (
      this.gridOptions &&
      this.gridOptions.columnApi &&
      this.gridOptions.api &&
      listState
    ) {
      if (listState.columns) {
        this.gridOptions.columnApi.applyColumnState({
          state: listState.columns,
          applyOrder: true,
        });
      } else {
        this.gridOptions.columnApi.resetColumnState();
      }

      if (listState.sort) {
        this.gridOptions.columnApi.applyColumnState({ state: listState.sort });
      } else {
        this.gridOptions.columnApi.applyColumnState({
          defaultState: { sort: null },
        });
      }

      if (listState.filter) {
        const myFilter = {
          ...listState.filter,
          AssetID: this.getAssetFilter(),
        };
        this.gridOptions.api.setFilterModel(myFilter);
      } else {
        this.gridOptions.api.setFilterModel(null);
      }

      if (listState.groups) {
        this.gridOptions.columnApi.setColumnGroupState(listState.groups);
      } else {
        this.gridOptions.columnApi.resetColumnGroupState();
      }
    }
  }

  updateAssetFilter(asset: string) {
    if (this.gridOptions && this.gridOptions.api) {
      const filter = this.gridOptions.api.getFilterInstance('AssetID');
      if (filter) {
        const oldAssetModel = filter.getModel();
        if (!oldAssetModel || oldAssetModel.filter !== this.asset) {
          filter.setModel(this.getAssetFilter());
          this.refreshGrid();
        }
      }
    }
  }

  updateIssuesAssetIDFilter(
    detailGridInfo: DetailGridInfo,
    asset: string,
    issuesAssetID: number
  ) {
    if (detailGridInfo && detailGridInfo.api) {
      const assetIdFilter = detailGridInfo.api.getFilterInstance('AssetID');
      const issuesAssetIDFilter =
        detailGridInfo.api.getFilterInstance('OpenIssuesAssetID');

      if (assetIdFilter && issuesAssetIDFilter) {
        assetIdFilter.setModel({ type: 'asset', filter: asset });
        issuesAssetIDFilter.setModel({ type: 'issues', filter: issuesAssetID });
        detailGridInfo.api.onFilterChanged();
      }
    }
  }

  updateIcon() {
    this.expandClick.emit();
  }

  refreshGrid() {
    if (this.gridOptions && this.gridOptions.api) {
      this.gridOptions.api.onFilterChanged();
    }
  }

  refreshScreen($event: MouseEvent) {
    this.lastRefresh = new Date();
    this.refresh.emit();

    // make the refresh animation restart every time it is clicked
    setTimeout(() => {
      this.button._elementRef.nativeElement.blur();
    }, 1000);

    $event.stopPropagation();
  }

  downloadModels($event: MouseEvent) {
    const gridParams: IGridParameters = {
      startRow: 0,
      endRow: 20000,
      rowGroupCols: [],
      valueCols: [],
      pivotCols: [],
      groupKeys: [],
      pivotMode: false,
      filterModel: null,
      sortModel: [],
      displayCols: [],
    };

    const columns = this.columnApi.getAllGridColumns();
    const rowGroupCols = this.columnApi.getRowGroupColumns();
    const valueCols = this.columnApi.getValueColumns();
    const pivotCols = this.columnApi.getPivotColumns();

    gridParams.filterModel = this.gridApi.getFilterModel();
    gridParams.sortModel = this.getSortState(this.columnApi);

    if (columns) {
      columns.map((column) => {
        const colDef = column.getColDef();
        if (column.isVisible()) {
          gridParams.displayCols.push({
            id: colDef.colId,
            displayName: colDef.headerName,
            field: colDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (rowGroupCols) {
      rowGroupCols.map((rowGroupCol) => {
        const groupColDef = rowGroupCol.getColDef();
        if (groupColDef) {
          gridParams.rowGroupCols.push({
            id: groupColDef.colId,
            displayName: groupColDef.headerName,
            field: groupColDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (valueCols) {
      valueCols.map((valueCol) => {
        const valueColDef = valueCol.getColDef();
        if (valueColDef) {
          gridParams.valueCols.push({
            id: valueColDef.colId,
            displayName: valueColDef.headerName,
            field: valueColDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (pivotCols) {
      pivotCols.map((pivotCol) => {
        const pivotColDef = pivotCol.getColDef();
        if (pivotColDef) {
          gridParams.valueCols.push({
            id: pivotColDef.colId,
            displayName: pivotColDef.headerName,
            field: pivotColDef.field,
            aggFunc: null,
          });
        }
      });
    }
    this.download.emit(gridParams);
  }

  getTotalModels() {
    const result = String(this.alertRetriever?.totalModels ?? '-');
    this.totalModels = result;
  }

  getDownloadableState() {
    const result = this.alertRetriever?.downloadDisabled ?? true;
    this.disableDownload = result;
  }

  doubleClicked(params) {
    params.node.setSelected(true);
    this.diagnosticDrilldown.emit();
  }

  toggleFloatingFilter() {
    this.floatingFilterToggle.emit();
  }

  updateModels(data: IUpdatedModels) {
    if (this.gridApi && data && data.ids && data.ids.length > 0) {
      for (const i of data.ids) {
        const node = this.gridApi.getRowNode(String(i));
        if (node) {
          let newData = node.data as INDModelSummary;
          if (data.action === 'alert') {
            newData = { ...newData, Alert: data.newValue };
          } else if (data.action === 'diagnose') {
            newData = { ...newData, ActionDiagnose: data.newValue };
          } else if (data.action === 'maintenance') {
            newData = { ...newData, ActionModelMaintenance: data.newValue };
          } else if (data.action === 'watch') {
            newData = { ...newData, ActionWatch: data.newValue };
          }

          node.setData(newData);
        }
      }
    }
  }

  getFilters() {
    const filters = this.gridApi.getFilterModel();
    this.screeningViewFilters = [];
    Object.keys(filters).forEach((key) => {
      if (!this.screeningViewFilters.includes(key)) {
        const isHidden = this.hiddenFilters.includes(key);
        if (!isHidden) {
          const colDef = this.gridApi.getColumnDef(key);
          this.screeningViewFilters.push({
            Name: key,
            Removable: true,
            DisplayName: colDef.headerName,
          });
        }
      }
    });
  }

  removeFilter(filter: any) {
    const idx = this.screeningViewFilters.indexOf(filter);

    if (idx >= 0) {
      this.screeningViewFilters.splice(idx, 1);
      this.gridApi.getFilterInstance(filter.Name).setModel(null);
      this.refreshGrid();
    }
  }

  clearFilters($event: MouseEvent) {
    const filters = this.gridApi.getFilterModel();

    Object.keys(filters).forEach((key) => {
      const isHidden = this.hiddenFilters.includes(key);
      if (!isHidden) {
        this.gridApi.getFilterInstance(key).setModel(null);
      }
    });

    this.screeningViewFilters = [];
    this.refreshGrid();
  }
  getSortState(columnApi: ColumnApi) {
    const sortState = columnApi.getColumnState().filter((x) => !isNil(x.sort));
    const sortModels = sortState.map(function (state) {
      return {
        colId: state.colId,
        sort: state.sort,
      };
    });

    return sortModels;
  }

  toggleRightTray() {
    this.navFacade.toggleRightTray();
  }
}
