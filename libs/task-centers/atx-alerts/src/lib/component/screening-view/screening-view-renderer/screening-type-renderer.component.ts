import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-screening-type-renderer',
  template: ` <p>{{ value }}</p> `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypeRendererComponent implements ICellRendererAngularComp {
  field = '';
  value = '';
  group = false;

  agInit(params: any) {
    this.field = params?.field ?? '';
    this.value = params?.value ?? '';
  }

  refresh(params: any): boolean {
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
