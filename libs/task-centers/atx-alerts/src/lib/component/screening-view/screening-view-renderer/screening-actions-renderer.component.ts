import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-screening-actions-renderer',
  template: `
    <img
      class="screening-icon"
      [title]="tooltip"
      [ngClass]="{ 'active-icon': active, 'group-icon': group }"
      [src]="imageSrc"
    />
  `,
  styleUrls: ['./screening-actions-renderer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScreeningActionsRendererComponent
  implements ICellRendererAngularComp
{
  tooltip = '';
  icon = '';
  field = '';
  active = false;
  imageSrc = '';
  group = false;

  agInit(params: any) {
    this.icon = params?.icon ?? 'trip_origin';
    this.field = params?.field ?? '';
    this.tooltip = params?.tooltip ?? '';
    this.active = params?.value;
    this.imageSrc = this.getImageSource(this.icon);
    this.group = params?.colDef?.showRowGroup ?? false;
  }

  getImageSource(icon: string) {
    if (icon === 'error_outline') {
      return './assets/emblem-diagnose.svg';
    } else if (icon === 'warning') {
      return './assets/emblem-alerts.svg';
    } else if (icon === 'radio_button_checked') {
      return './assets/emblem-watch.svg';
    } else {
      return './assets/emblem-maintainence.svg';
    }
  }

  refresh(params: any): boolean {
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
