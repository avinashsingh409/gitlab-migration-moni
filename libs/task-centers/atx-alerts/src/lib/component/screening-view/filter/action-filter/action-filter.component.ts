/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnInit } from '@angular/core';
import { AgFilterComponent } from '@ag-grid-community/angular';
import {
  IAfterGuiAttachedParams,
  IFilterParams,
  IDoesFilterPassParams,
} from '@ag-grid-enterprise/all-modules';
import { INDModelSummary } from '@atonix/shared/api';

@Component({
  selector: 'atx-action-filter',
  templateUrl: './action-filter.component.html',
  styleUrls: ['./action-filter.component.scss'],
})
export class ActionFilterComponent implements AgFilterComponent {
  params: IFilterParams;

  diagnose: boolean = null;
  watch: boolean = null;
  maintenance: boolean = null;

  agInit(params: IFilterParams): void {
    this.params = params;
  }

  isFilterActive(): boolean {
    return (
      this.diagnose !== null || this.watch !== null || this.maintenance !== null
    );
  }
  doesFilterPass(params: IDoesFilterPassParams): boolean {
    let result = true;
    const alert = params.data as INDModelSummary;
    if (this.diagnose !== null && alert.ActionDiagnose !== this.diagnose) {
      result = false;
    }
    if (result && this.watch !== null && alert.ActionWatch !== this.watch) {
      result = false;
    }
    if (
      result &&
      this.maintenance !== null &&
      alert.ActionModelMaintenance !== this.maintenance
    ) {
      result = false;
    }

    return result;
  }
  getModel() {
    return JSON.stringify({
      diagnose: this.diagnose,
      watch: this.watch,
      maintenance: this.maintenance,
    });
  }
  setModel(model: { value: string }): void {
    const val = model?.value ?? null;
    if (val) {
      const data = JSON.parse(val);
      this.diagnose = data.diagnose;
      this.watch = data.watch;
      this.maintenance = data.maintenance;
    }
  }
  onNewRowsLoaded?(): void {}
  getFrameworkComponentInstance?() {}
  getModelAsString?(model: any): string {
    return this.getModel();
  }

  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}

  changeDiagnose(newValue: boolean) {
    this.diagnose = newValue;
    this.params.filterChangedCallback();
  }

  changeWatch(newValue: boolean) {
    this.watch = newValue;
    this.params.filterChangedCallback();
  }
  changeMaintenance(newValue: boolean) {
    this.maintenance = newValue;
    this.params.filterChangedCallback();
  }
}
