import {
  ComponentFixture,
  TestBed,
  inject,
  waitForAsync,
} from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { SavedToolPanelComponent } from './saved-tool-panel.component';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { ISavedModelList } from '@atonix/shared/api';
import { NavFacade } from '@atonix/atx-navigation';
import { BehaviorSubject } from 'rxjs';
import {
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('SavedToolPanelComponent', () => {
  let component: SavedToolPanelComponent;
  let fixture: ComponentFixture<SavedToolPanelComponent>;
  const navFacadeMock = createMockWithValues(NavFacade, {
    theme$: new BehaviorSubject<string>('light'),
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SavedToolPanelComponent],
      imports: [ReactiveFormsModule, AtxMaterialModule, NoopAnimationsModule],
      providers: [
        provideMock(SummaryFacade),
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: NavFacade, useValue: navFacadeMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedToolPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.agInit(null);
    expect(component).toBeTruthy();
  });

  it('should track by fn', () => {
    const savedIssueList: ISavedModelList = {
      id: 1234,
      name: 'Test Only',
      state: null,
      checked: false,
    };

    component.trackByFn(savedIssueList);
    expect(component.trackByFn(savedIssueList)).toEqual(savedIssueList.id);
  });

  it('should save list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(summaryFacade, 'saveList');

      component.name.setValue('Test Only');
      component.saveList();
      expect(summaryFacade.saveList).toHaveBeenCalledWith({
        id: -1,
        name: 'Test Only',
      });
    }
  ));

  it('should load list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(summaryFacade, 'loadList');
      const savedIssueList: ISavedModelList = {
        id: 1234,
        name: 'Test Only',
        state: null,
        checked: false,
      };

      component.loadList(savedIssueList);
      expect(summaryFacade.loadList).toHaveBeenCalledWith(savedIssueList.id);
    }
  ));

  it('should delete list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(summaryFacade, 'deleteList');
      const savedIssueList: ISavedModelList = {
        id: 1234,
        name: 'Test Only',
        state: null,
        checked: false,
      };

      jest.spyOn(window, 'confirm').mockReturnValue(true);
      component.delete(savedIssueList);
      expect(summaryFacade.deleteList).toHaveBeenCalledWith(savedIssueList.id);
    }
  ));
});
