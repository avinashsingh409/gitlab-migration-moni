import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModelActionComponent } from './model-action.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService, JwtInterceptorService } from '@atonix/shared/state/auth';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { createMock } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('ModelActionComponent', () => {
  let component: ModelActionComponent;
  let fixture: ComponentFixture<ModelActionComponent>;
  let mockAuthService: AuthService;

  beforeEach(() => {
    mockAuthService = createMock(AuthService);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
      ],
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModelActionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
