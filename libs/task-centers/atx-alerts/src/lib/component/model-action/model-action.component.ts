import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  getChartConfigForEmptyResultModelAction,
  HighchartsService,
  turnModelActionToHighCharts,
} from '@atonix/atx-chart';
import {
  INDModelSummary,
  INDModelActionItemAnalysis,
  ImagesFrameworkService,
} from '@atonix/shared/api';
import { DarkUnicaTheme } from '@atonix/shared/utils';

@Component({
  selector: 'atx-model-action',
  templateUrl: './model-action.component.html',
  styleUrls: ['./model-action.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelActionComponent implements OnChanges, AfterViewInit {
  @Input() theme: string;
  @Input() model: INDModelSummary;
  @Input() loading: boolean;
  @Input() multiselected: boolean;
  @Input() data: INDModelActionItemAnalysis[];
  @ViewChild('actionchart') chartRef: ElementRef;
  highcharts;

  constructor(
    highchartsService: HighchartsService,
    private imagesFrameworkService: ImagesFrameworkService
  ) {
    this.highcharts = highchartsService.highcharts();
  }
  ngAfterViewInit(): void {
    this.redraw();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.redraw();
  }
  redraw() {
    const loading = this.loading;
    const model = this.model;
    const data = this.data;

    let newTrend: Highcharts.Options = {
      title: { text: '' },
      series: [],
      credits: {
        enabled: false,
      },
    };
    if (this.multiselected) {
      newTrend.title.text = 'Please select one Alert';
    } else if (loading) {
      newTrend.title.text = 'loading ...';
    } else if (!data || data.length === 0) {
      newTrend = getChartConfigForEmptyResultModelAction();
    } else {
      newTrend = turnModelActionToHighCharts(data, model);
      newTrend.title.text = '';
    }
    newTrend.exporting = this.imagesFrameworkService.getExportSettings();
    if (this.theme === 'dark') {
      newTrend = this.highcharts.merge(DarkUnicaTheme, newTrend);
    }
    if (this.chartRef) {
      this.highcharts.chart(this.chartRef.nativeElement, newTrend);
    }
  }
}
