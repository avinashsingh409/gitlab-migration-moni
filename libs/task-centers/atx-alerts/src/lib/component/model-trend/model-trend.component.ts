import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from '@angular/core';
import { IUpdateLimitsData, IBtnGrpStateChange } from '@atonix/atx-chart';
import {
  INDModelSummary,
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
} from '@atonix/shared/api';

@Component({
  selector: 'atx-model-trend',
  templateUrl: './model-trend.component.html',
  styleUrls: ['./model-trend.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelTrendComponent {
  @Input() model: INDModelSummary;
  @Input() data: IAssetModelChartingData;
  @Input() actionData: INDModelActionItemAnalysis[];
  @Input() loading: boolean;
  @Input() multiselected: boolean;
  @Output() updateLimits = new EventEmitter<IUpdateLimitsData>();
  @Output() resetLimits = new EventEmitter<number>();

  onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'UpdateLimits') {
      this.updateLimits.emit(change.newValue as IUpdateLimitsData);
    } else if (change.event === 'ResetLimits') {
      this.resetLimits.emit(change.newValue as number);
    }
  }
}
