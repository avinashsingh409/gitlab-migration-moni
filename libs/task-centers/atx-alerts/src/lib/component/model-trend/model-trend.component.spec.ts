import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ModelTrendComponent } from './model-trend.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService, JwtInterceptorService } from '@atonix/shared/state/auth';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { createMock } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('ModelTrendComponent', () => {
  let component: ModelTrendComponent;
  let fixture: ComponentFixture<ModelTrendComponent>;

  let mockAuthService: AuthService;

  beforeEach(() => {
    mockAuthService = createMock(AuthService);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
      ],
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [ModelTrendComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
