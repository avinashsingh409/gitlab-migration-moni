import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisePanelComponent } from './advise-panel.component';
import { SummaryFacade } from '../../store/facade/summary.facade';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ITreeConfiguration } from '@atonix/atx-asset-tree';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { RouterTestingModule } from '@angular/router/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AdvisePanelComponent', () => {
  let component: AdvisePanelComponent;
  let fixture: ComponentFixture<AdvisePanelComponent>;

  beforeEach(async () => {
    const mockSummaryFacade = createMockWithValues(SummaryFacade, {
      selectedAsset$: new BehaviorSubject<string>('123'),
      selectedModel$: new BehaviorSubject<any>(null),
      aiResponse$: new BehaviorSubject<string>(''),
      aiResponseLoading$: new BehaviorSubject<boolean>(false),
      selectTimeSelection$: new BehaviorSubject<{
        startDate: Date;
        endDate: Date;
      }>(null),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: null,
        pin: false,
        loadingAssets: false,
        loadingTree: false,
        canView: true,
        canAdd: true,
        canEdit: true,
        canDelete: true,
        collapseOthers: false,
        hideConfigureButton: false,
      }),
    });
    await TestBed.configureTestingModule({
      declarations: [AdvisePanelComponent],
      imports: [
        RouterTestingModule,
        AtxMaterialModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: SummaryFacade, useValue: mockSummaryFacade },
        FormBuilder,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AdvisePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
