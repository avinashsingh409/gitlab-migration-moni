import { Component, OnInit } from '@angular/core';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { Observable } from 'rxjs';
import { INDModelSummary } from '@atonix/shared/api';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'atx-advise-panel',
  templateUrl: './advise-panel.component.html',
  styleUrls: ['./advise-panel.component.scss'],
})
export class AdvisePanelComponent {
  adviceForm = this.fb.group({
    application: ['', Validators.maxLength(50)],
    assetType: ['', [Validators.required, Validators.maxLength(50)]],
    parameterType: ['', [Validators.required, Validators.maxLength(50)]],
    parameterModifier: ['', [Validators.required, Validators.maxLength(50)]],
  });

  adviceOutput = this.fb.control('');

  selectedModel$ = new Observable<INDModelSummary>();
  aiResponse$ = new Observable<string>();
  aiResponseLoading$ = new Observable<boolean>();

  constructor(private summaryFacade: SummaryFacade, private fb: FormBuilder) {
    this.selectedModel$ = this.summaryFacade.selectedModel$;
    this.aiResponse$ = this.summaryFacade.aiResponse$;
    this.aiResponseLoading$ = this.summaryFacade.aiResponseLoading$;

    this.selectedModel$.subscribe((model) => {
      if (!model) {
        return;
      }
      const m = model as any; //the object contains more properties that aren't in its type INDModelSummary

      // this.adviceForm.get('application').setValue('')

      if (m.AssetClassType) {
        this.adviceForm.get('assetType').setValue(m.AssetClassType);
      }

      if (m.VariableType) {
        this.adviceForm.get('parameterType').setValue(m.VariableType);
      }

      if (model.Actual && model.Expected) {
        if (model.Actual >= model.Expected) {
          this.adviceForm.get('parameterModifier').setValue('Increasing');
        } else {
          this.adviceForm.get('parameterModifier').setValue('Decreasing');
        }
      }
    });

    this.aiResponse$.subscribe((response) => {
      this.adviceOutput.setValue(response);
    });
  }

  onSubmitAdviceForm() {
    this.summaryFacade.loadAIAdvice(
      this.adviceForm.get('application').value,
      this.adviceForm.get('assetType').value,
      this.adviceForm.get('parameterType').value,
      this.adviceForm.get('parameterModifier').value
    );
  }
}
