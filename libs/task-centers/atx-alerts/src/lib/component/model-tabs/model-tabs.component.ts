import {
  Component,
  EventEmitter,
  Output,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';

import { IUpdateLimitsData } from '@atonix/atx-chart';
import {
  INDModelSummary,
  INDModelActionItem,
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
  ICorrelationData,
} from '@atonix/shared/api';
import { INewActionItem } from '../../model/new-action-item';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import { LoggerService } from '@atonix/shared/utils';
import { IProcessedTrend } from '@atonix/atx-core';

export type ModelTabTypes =
  | 'modelhistory'
  | 'modelcontext'
  | 'modeltrend'
  | 'modelaction';

@Component({
  selector: 'atx-model-tabs',
  templateUrl: './model-tabs.component.html',
  styleUrls: ['./model-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelTabsComponent {
  public datastatus: string;
  constructor(private logger: LoggerService) {}

  @Input() expanded: boolean;

  @Input() modelContextTrends: IProcessedTrend[];
  @Input() selectedModelContextTrend: IProcessedTrend;
  @Input() selectedModelContextTrendID: string;
  @Input() modelContextCorrelationTrend: ICorrelationData[];
  @Input() modelContextLoading: boolean;
  @Input() modelTrend: IAssetModelChartingData;
  @Input() modelTrendLoading: boolean;

  @Input() selectedRow: INDModelSummary;
  @Input() dataLoaded: boolean;

  @Input() modelHistorySaving: boolean;
  @Input() modelHistoryFilters: IModelHistoryFilters;
  @Input() listTheme: string;
  @Input() multiselected: boolean;

  @Input() modelAction: INDModelActionItemAnalysis[];
  @Input() modelActionLoading: boolean;
  @Input() refreshDate: Date;

  @Output() expandClick = new EventEmitter();
  @Output() modelContextTrendChanged = new EventEmitter<string>();
  @Output() redrawView = new EventEmitter();
  @Output() modelContextLabelClick = new EventEmitter();
  @Output() modelContextEditChartClick = new EventEmitter();
  @Output() modelContextUpdateLimits = new EventEmitter<IUpdateLimitsData>();
  @Output() modelContextResetLimits = new EventEmitter<number>();
  @Output() modelContextLegendItemToggle = new EventEmitter<number>();
  @Output() modelTrendUpdateLimits = new EventEmitter<IUpdateLimitsData>();
  @Output() modelTrendResetLimits = new EventEmitter<number>();
  @Output() setFavorite = new EventEmitter<{
    actionItem: INDModelActionItem;
    favorite: boolean;
  }>();
  @Output() setNote = new EventEmitter<{
    actionItem: INDModelActionItem;
    note: string;
  }>();
  @Output() newActionItem = new EventEmitter<INewActionItem>();
  @Output() setModelHistoryFilters = new EventEmitter<IModelHistoryFilters>();

  tab: ModelTabTypes = 'modeltrend';

  expand() {
    this.expandClick.emit();
  }

  redraw() {
    this.redrawView.emit();
  }

  modelContextTrendSelected(trendID: string) {
    this.modelContextTrendChanged.emit(trendID);
  }

  onChangeLabelsClick() {
    this.modelContextLabelClick.emit();
  }

  onEditChartClicked() {
    this.modelContextEditChartClick.emit();
  }

  onUpdateModelContextLimits(event: IUpdateLimitsData) {
    this.modelContextUpdateLimits.emit(event);
  }

  onResetModelContextLimits(event: number) {
    this.modelContextResetLimits.emit(event);
  }

  onModelContextLegendItemToggle(event: number) {
    this.modelContextLegendItemToggle.emit(event);
  }

  onUpdateModelTrendLimits(event: IUpdateLimitsData) {
    this.modelTrendUpdateLimits.emit(event);
  }

  onResetModelTrendLimits(event: number) {
    this.modelTrendResetLimits.emit(event);
  }

  setTab(newTab: ModelTabTypes) {
    this.tab = newTab;
    this.logger.feature(newTab, 'alerts');
  }

  setModelHistoryFavorite(event: {
    actionItem: INDModelActionItem;
    favorite: boolean;
  }) {
    this.setFavorite.emit(event);
  }

  setModelHistoryNote(event: { actionItem: INDModelActionItem; note: string }) {
    this.setNote.emit(event);
  }

  setNewActionItem(event: INewActionItem) {
    this.newActionItem.emit(event);
  }

  modelHistoryFiltersChanged(event: IModelHistoryFilters) {
    this.setModelHistoryFilters.emit(event);
  }
}
