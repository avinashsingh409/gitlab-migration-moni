import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ChartModule } from '@atonix/atx-chart';
import { ModelTabsComponent } from './model-tabs.component';
import { ModelContextComponent } from '../model-context/model-context.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ModelTrendComponent } from '../model-trend/model-trend.component';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('ModelTabsComponent', () => {
  let component: ModelTabsComponent;
  let fixture: ComponentFixture<ModelTabsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModelTabsComponent,
        ModelContextComponent,
        ModelTrendComponent,
      ],
      imports: [AtxMaterialModule, NoopAnimationsModule, ChartModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
