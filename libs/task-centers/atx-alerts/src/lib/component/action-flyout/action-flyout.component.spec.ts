import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionFlyoutComponent } from './action-flyout.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRadioModule } from '@angular/material/radio';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AuthFacade } from '@atonix/shared/state/auth';
import { render, screen } from '@testing-library/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { BehaviorSubject } from 'rxjs';
import { ITreeConfiguration } from '@atonix/atx-asset-tree';
describe('ActionFlyoutComponent', () => {
  let authFacadeMock: AuthFacade;
  let summaryFacadeMock: SummaryFacade;

  beforeEach(waitForAsync(async () => {
    authFacadeMock = createMock(AuthFacade);
    summaryFacadeMock = createMockWithValues(SummaryFacade, {
      selectedAsset$: new BehaviorSubject<string>('123'),
      selectTimeSelection$: new BehaviorSubject<{
        startDate: Date;
        endDate: Date;
      }>(null),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: null,
        pin: false,
        loadingAssets: false,
        loadingTree: false,
        canView: true,
        canAdd: true,
        canEdit: true,
        canDelete: true,
        collapseOthers: false,
        hideConfigureButton: false,
      }),
    });
    await render(ActionFlyoutComponent, {
      imports: [RouterTestingModule, AtxMaterialModule, FlexLayoutModule],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: AuthFacade, useValue: authFacadeMock },
        { provide: SummaryFacade, useValue: summaryFacadeMock },
        { provide: MatchMedia, useClass: MockMatchMedia },
      ],
      componentProperties: {
        refreshing: true,
      },
    });
  }));

  it('should show Loading when refreshing is true', () => {
    expect(screen.getByTestId('refresh'));
  });
});
