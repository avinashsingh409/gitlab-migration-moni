import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import {
  UntypedFormGroup,
  UntypedFormControl,
  Validators,
} from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import moment from 'moment';
import { sigFig, isNil } from '@atonix/atx-core';
import {
  setActionFlyoutToggle,
  setWatch,
  clearWatch,
  clearAlert,
  refresh,
  setCustomWatch,
  newIssue,
  saveDiagnose,
  saveNote,
  saveMaintenance,
  clearDiagnose,
  clearMaintenance,
  modelConfig,
  opModeConfig,
  diagnosticDrilldown,
  showRelatedModels,
  showRelatedIssues,
  dataExplorer,
} from '../../service/action-flyout.service';
import { IActionFlyoutStateChange } from '../../model/action-flyout-state-change';
import { INDModelSummary } from '@atonix/shared/api';

import { MatCheckboxChange } from '@angular/material/checkbox';
import { LoggerService } from '@atonix/shared/utils';
import { AuthFacade } from '@atonix/shared/state/auth';
import { SummaryFacade } from '../../store/facade/summary.facade';

interface ICustomWatchForm {
  timeSpan: number;
  timeUnit: string;
  upperUnits: number;
  lowerUnits: number;
  emailAlert: boolean;
  customNote: string;
  upperControl: string;
  lowerControl: string;
}

export enum ModelTypes {
  APR = 'APR',
  FL = 'Fixed Limit',
  RA = 'Rolling Average',
}

export enum UpperBoundTypes {
  Model = 'Model',
  Relative = 'Relative',
  AbsoluteValue = 'Absolute',
}
export const UpperBoundTypeMapping = {
  [UpperBoundTypes.Model]: 'Model Upper Bound',
  [UpperBoundTypes.Relative]: 'Relative to Expected',
  [UpperBoundTypes.AbsoluteValue]: 'Absolute Value',
};

export enum LowerBoundTypes {
  Model = 'Model',
  Relative = 'Relative',
  AbsoluteValue = 'Absolute',
}
export const LowerBoundTypeMapping = {
  [LowerBoundTypes.Model]: 'Model Lower Bound',
  [LowerBoundTypes.Relative]: 'Relative to Expected',
  [LowerBoundTypes.AbsoluteValue]: 'Absolute Value',
};

enum CriteriaType {
  Greater = 1,
  Lesser = 2,
  Equal = 3,
}

@Component({
  selector: 'atx-action-flyout',
  templateUrl: './action-flyout.component.html',
  styleUrls: ['./action-flyout.component.scss'],
})
export class ActionFlyoutComponent implements OnDestroy, OnChanges {
  @Input() model: INDModelSummary[];
  @Input() refreshing: boolean;
  @Input() refreshDate: Date;
  @Output() actionFlyOutStateChange =
    new EventEmitter<IActionFlyoutStateChange>();
  @ViewChild('refreshModelButton') button: MatButton;

  public unsubscribe$ = new Subject<void>();
  public selectedModel$;
  public selectedTab = 'Actions';

  // for model properties
  public isMultipleSelect = false;
  public modelObject: INDModelSummary;
  public timestamp: string;
  public disableOpModeConfig = false;
  public disableModelConfig = false;
  public disableDiagnosticDrilldown = false;
  // for model status
  public isActionWatch: boolean;
  public isAlert: boolean;
  public isDiagnose: boolean;
  public isMaintenance: boolean;

  // for watch
  public showCustomWatchForm = false;
  public customWatchForm: UntypedFormGroup;
  public isMultiClearWatch = false;
  public isMultiClearMaintenance = false;
  public isMultiClearDiagnose = false;
  public defaultNote: string;

  // for action forms
  public showOnlyActionForms = false;
  public showDiagnoseForm = false;
  public showAddNoteForm = false;
  public showMaintenanceForm = false;
  public newDiagnoseForm: UntypedFormGroup;
  public newNoteForm: UntypedFormGroup;
  public newMaintenanceForm: UntypedFormGroup;

  upperCheck = true;
  lowerCheck = true;

  public upperBoundTypes = Object.keys(UpperBoundTypes).map(
    (itm) => UpperBoundTypes[itm]
  );
  public upperBoundTypeMapping: any = UpperBoundTypeMapping;

  public lowerBoundTypes = Object.keys(LowerBoundTypes).map(
    (itm) => LowerBoundTypes[itm]
  );
  public lowerBoundTypeMapping: any = LowerBoundTypeMapping;

  selectFormControl = new UntypedFormControl('', Validators.required);

  constructor(
    private loggerService: LoggerService,
    public authFacade: AuthFacade,
    public summaryFacade: SummaryFacade
  ) {
    this.selectedModel$ = summaryFacade.selectedModel$;
    this.customWatchForm = new UntypedFormGroup({
      timeSpan: new UntypedFormControl('', [
        Validators.required,
        Validators.min(0),
      ]),
      timeUnit: new UntypedFormControl('', [Validators.required]),
      upperUnits: new UntypedFormControl(''),
      lowerUnits: new UntypedFormControl(''),
      emailAlert: new UntypedFormControl(false),
      customNote: new UntypedFormControl(''),
      upperControl: new UntypedFormControl(),
      lowerControl: new UntypedFormControl(),
      disableSelect: new UntypedFormControl(false),
    });

    this.newDiagnoseForm = new UntypedFormGroup({
      diagnoseNote: new UntypedFormControl(''),
    });

    this.newNoteForm = new UntypedFormGroup({
      noteNote: new UntypedFormControl(''),
    });

    this.newMaintenanceForm = new UntypedFormGroup({
      maintenanceNote: new UntypedFormControl(''),
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  onSetTab(tab: string) {
    this.selectedTab = tab;
  }

  changeUpperBound(value: any) {
    let newUpperBound: number = null;

    if (value === UpperBoundTypes.Model) {
      const expected =
        this.model[0].Expected === null
          ? Number(0)
          : Number(this.model[0].Expected);
      const curUpperBound =
        this.model[0].UpperLimit === null
          ? Number(0)
          : Number(this.model[0].UpperLimit);
      newUpperBound = curUpperBound - expected;
    }

    this.setUpperValidators();

    this.customWatchForm.patchValue({
      upperControl: value,
      upperUnits: newUpperBound,
    });
  }

  setUpperValidators() {
    const value = this.customWatchForm.value.upperControl;
    if (!this.upperCheck || value === UpperBoundTypes.Model) {
      this.customWatchForm.controls['upperUnits'].clearValidators();
    } else if (
      value === UpperBoundTypes.Relative ||
      value === UpperBoundTypes.AbsoluteValue
    ) {
      this.customWatchForm.controls['upperUnits'].setValidators([
        Validators.required,
      ]);
    }

    this.customWatchForm.controls['upperUnits'].updateValueAndValidity({
      onlySelf: true,
    });
  }

  isDisabledFixedUpper(value: UpperBoundTypes) {
    if (
      (value === UpperBoundTypes.Relative || value === UpperBoundTypes.Model) &&
      this.hasFixedLimitType(this.model)
    ) {
      return true;
    }
    return false;
  }

  isDisabledFixedLower(value: LowerBoundTypes) {
    if (
      (value === LowerBoundTypes.Relative || value === LowerBoundTypes.Model) &&
      this.hasFixedLimitType(this.model)
    ) {
      return true;
    }
    return false;
  }

  changeLowerBound(value: any) {
    let newLowerBound: number = null;

    if (value === LowerBoundTypes.Model) {
      const expected =
        this.model[0].Expected === null
          ? Number(0)
          : Number(this.model[0].Expected);
      const curLowerBound =
        this.model[0].LowerLimit === null
          ? Number(0)
          : Number(this.model[0].LowerLimit);
      newLowerBound = expected - curLowerBound;
    }

    this.setLowerValidators();

    this.customWatchForm.patchValue({
      lowerControl: value,
      lowerUnits: newLowerBound,
    });
  }

  setLowerValidators() {
    const value = this.customWatchForm.value.lowerControl;
    if (!this.lowerCheck || value === LowerBoundTypes.Model) {
      this.customWatchForm.controls['lowerUnits'].clearValidators();
    } else if (
      value === LowerBoundTypes.Relative ||
      value === LowerBoundTypes.AbsoluteValue
    ) {
      this.customWatchForm.controls['lowerUnits'].setValidators([
        Validators.required,
      ]);
    }

    this.customWatchForm.controls['lowerUnits'].updateValueAndValidity({
      onlySelf: true,
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.model?.currentValue?.[0]) {
      // not multiple select
      if (changes.model.currentValue.length === 1) {
        this.isMultipleSelect = false;
        this.modelObject = changes.model.currentValue[0]; // save as main model object
        this.isActionWatch = this.modelObject.ActionWatch;
        this.isAlert = this.modelObject.Alert;
        this.isDiagnose = this.modelObject.ActionDiagnose;
        this.isMaintenance = this.modelObject.ActionModelMaintenance;
        if (this.modelObject.LastNoteDate) {
          this.timestamp =
            moment(this.modelObject.LastNoteDate).format(
              'MM/DD/YYYY, h:mm a'
            ) ?? '';
        } else {
          this.timestamp = 'No Valid Date';
        }

        this.isMultiClearWatch = false;
        this.isMultiClearMaintenance = false;
        this.isMultiClearDiagnose = false;
        this.upperCheck = true;
        this.lowerCheck = true;
        this.disableOpModeConfig =
          this.modelObject.ModelTypeAbbrev === 'External';
        this.disableDiagnosticDrilldown =
          this.modelObject.ModelTypeAbbrev === 'External'
            ? this.modelObject.DiagnosticDrilldownURL === ''
            : false;
        this.disableModelConfig =
          this.modelObject.ModelTypeAbbrev === 'External'
            ? this.modelObject.ModelConfigURL === ''
            : false;
      }
      // for multiple selection
      else {
        this.isMultipleSelect = true;

        // set to false the single model variables
        this.isActionWatch = false;
        this.isAlert = false;
        this.isDiagnose = false;
        this.isMaintenance = false;

        // checks if multiple selects has same state and valid for multiple clear action
        this.isMultiClearWatch = this.getIsMultiClearWatch(
          changes.model.currentValue
        );
        this.isMultiClearMaintenance = this.getIsMultiClearMaintenance(
          changes.model.currentValue
        );
        this.isMultiClearDiagnose = this.getIsMultiClearDiagnose(
          changes.model.currentValue
        );
        this.disableOpModeConfig = true;
        this.disableDiagnosticDrilldown = true;
        this.disableModelConfig = true;
      }
      this.resetForms();
    }
    if (changes?.refreshDate?.currentValue) {
      this.actionFlyOutStateChange.emit(refresh(this.getModels()));
    }

    if (changes.model?.currentValue?.[0].ModelID) {
      const defaultBoundType = this.hasFixedLimitType(
        changes.model?.currentValue
      )
        ? UpperBoundTypes.AbsoluteValue
        : UpperBoundTypes.Model;

      this.customWatchForm = new UntypedFormGroup({
        timeSpan: new UntypedFormControl('', [
          Validators.required,
          Validators.min(0),
        ]),
        timeUnit: new UntypedFormControl('', [Validators.required]),
        upperUnits: new UntypedFormControl(''),
        lowerUnits: new UntypedFormControl(''),
        emailAlert: new UntypedFormControl(false),
        customNote: new UntypedFormControl(''),
        upperControl: new UntypedFormControl(defaultBoundType),
        lowerControl: new UntypedFormControl(defaultBoundType),
        disableSelect: new UntypedFormControl(false),
      });

      this.changeUpperBound(defaultBoundType);
      this.changeLowerBound(defaultBoundType);

      this.customWatchForm.valueChanges
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((watch) => {
          if (watch) {
            this.defaultNote =
              this.createDefaultNote(watch) || this.defaultNote;
          }
        });
    }
  }

  hasFixedLimitType(models: INDModelSummary[]) {
    const fixedLimitModel = models.find(
      (mod) => mod.ModelTypeAbbrev === ModelTypes.FL
    );

    if (fixedLimitModel) {
      return true;
    }

    return false;
  }

  getModels() {
    return this.model || [];
  }
  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearWatch(models: INDModelSummary[]) {
    const valueArr = models.map((item) => {
      return item.ActionWatch;
    });
    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0].ActionWatch : false;
  }
  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearMaintenance(models: INDModelSummary[]) {
    const valueArr = models.map((item) => {
      return item.ActionModelMaintenance;
    });
    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0].ActionModelMaintenance : false;
  }
  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearDiagnose(models: INDModelSummary[]) {
    const valueArr = models.map((item) => {
      return item.ActionDiagnose;
    });

    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0].ActionDiagnose : false;
  }

  refreshModel($event: MouseEvent) {
    this.actionFlyOutStateChange.emit(refresh(this.getModels()));

    setTimeout(() => {
      this.button._elementRef.nativeElement.blur();
    }, 1000);

    $event.stopPropagation();
  }

  // Set Watch
  onWatchChange(data: string) {
    const watchValue = data;
    if (data !== 'custom') {
      if (data === '7') {
        this.actionFlyOutStateChange.emit(setWatch(this.getModels(), '168'));
      } else {
        this.actionFlyOutStateChange.emit(
          setWatch(this.getModels(), watchValue)
        );
      }
      this.resetForms();
    } else {
      this.showCustomWatchForm = true;
    }
  }
  submitClearWatch() {
    const params = {
      NoteText: '',
    };
    const actionItemType = 'clearwatch';
    this.actionFlyOutStateChange.emit(
      clearWatch(this.getModels(), params, actionItemType)
    );
    this.resetForms();
  }

  submitCustomWatch() {
    if (this.customWatchForm.valid) {
      const params = {
        NoteText: this.customWatchForm.value.customNote,
        WatchDuration: this.customWatchForm.value.timeSpan,
        WatchDurationTemporalTypeId: this.customWatchForm.value.timeUnit,
        WatchLimit: this.customWatchForm.value.upperUnits,
        WatchLimit2:
          this.customWatchForm.value.lowerControl !==
            LowerBoundTypes.AbsoluteValue && this.lowerCheck === true
            ? +this.customWatchForm.value.lowerUnits * -1
            : this.customWatchForm.value.lowerUnits,
        WatchEmailIfAlert: this.customWatchForm.value.emailAlert,
        WatchRelativeToExpected:
          this.customWatchForm.value.upperControl ===
            UpperBoundTypes.AbsoluteValue || this.upperCheck === false
            ? false
            : true,
        WatchRelativeToExpected2:
          this.customWatchForm.value.lowerControl ===
            LowerBoundTypes.AbsoluteValue || this.lowerCheck === false
            ? false
            : true,
        WatchCriteriaTypeId:
          this.upperCheck === true ? CriteriaType.Greater : null,
        WatchCriteriaTypeId2:
          this.lowerCheck === true ? CriteriaType.Lesser : null,
      };
      const actionItemType = 'savewatch';
      this.actionFlyOutStateChange.emit(
        setCustomWatch(this.getModels(), params, actionItemType)
      );
    }
    this.resetForms();
  }

  lowerCheckChanged(value: MatCheckboxChange) {
    this.lowerCheck = value.checked;

    if (!this.lowerCheck) {
      this.customWatchForm.controls['lowerControl'].disable();
      this.customWatchForm.controls['lowerUnits'].disable();
    } else {
      this.customWatchForm.controls['lowerControl'].enable();
      this.customWatchForm.controls['lowerUnits'].enable();
    }

    this.setLowerValidators();
  }

  upperCheckChanged(value: MatCheckboxChange) {
    this.upperCheck = value.checked;
    if (!this.upperCheck) {
      this.customWatchForm.controls['upperControl'].disable();
      this.customWatchForm.controls['upperUnits'].disable();
    } else {
      this.customWatchForm.controls['upperControl'].enable();
      this.customWatchForm.controls['upperUnits'].enable();
    }

    this.setUpperValidators();
  }

  // Action Items
  submitDiagnose() {
    let actionItemType: string;
    if (this.newDiagnoseForm.valid) {
      const params = {
        NoteText: this.newDiagnoseForm.value.diagnoseNote,
      };
      if (this.isDiagnose || this.isMultiClearDiagnose) {
        actionItemType = 'cleardiagnose';
        this.actionFlyOutStateChange.emit(
          clearDiagnose(this.getModels(), params, actionItemType)
        );
      } else {
        actionItemType = 'savediagnose';
        this.actionFlyOutStateChange.emit(
          saveDiagnose(this.getModels(), params, actionItemType)
        );
      }
      this.resetForms();
    }
  }

  submitNote() {
    if (this.newNoteForm.valid) {
      const params = {
        NoteText: this.newNoteForm.value.noteNote,
      };
      const actionItemType = 'savenote';
      this.actionFlyOutStateChange.emit(
        saveNote(this.getModels(), params, actionItemType)
      );
      this.resetForms();
    }
  }

  submitMaintenance() {
    let actionItemType: string;
    if (this.newMaintenanceForm.valid) {
      const params = {
        NoteText: this.newMaintenanceForm.value.maintenanceNote,
      };
      if (this.isMaintenance || this.isMultiClearMaintenance) {
        actionItemType = 'clearmaintenance';
        this.actionFlyOutStateChange.emit(
          clearMaintenance(this.getModels(), params, actionItemType)
        );
      } else {
        actionItemType = 'savemaintenance';
        this.actionFlyOutStateChange.emit(
          saveMaintenance(this.getModels(), params, actionItemType)
        );
      }
      this.resetForms();
    }
  }

  clearAlert() {
    this.actionFlyOutStateChange.emit(clearAlert(this.getModels()));
  }

  hideInfoTray() {
    this.actionFlyOutStateChange.emit(setActionFlyoutToggle(false));
    this.resetForms();
  }
  showInfoTray() {
    this.actionFlyOutStateChange.emit(setActionFlyoutToggle(true));
    this.resetForms();
  }

  createActionNote() {
    if (this.model?.length === 1) {
      let note: string;
      note = 'Actual = ' + sigFig(this.model[0].Actual, 4) + ', \n';
      note += 'Expected = ' + sigFig(this.model[0].Expected, 4) + ', \n';
      note += 'Lower = ' + sigFig(this.model[0].LowerLimit, 4) + ', \n';
      note += 'Upper = ' + sigFig(this.model[0].UpperLimit, 4);
      return note;
    } else {
      return '';
    }
  }

  setDefaultNote() {
    this.customWatchForm.patchValue({ customNote: this.defaultNote });
  }

  createDefaultNote(watch: ICustomWatchForm) {
    let newDefaultNote = 'Default Note: Watch expires after ';

    if (watch.timeSpan) {
      newDefaultNote += watch.timeSpan;
    } else {
      newDefaultNote += '{}';
    }

    if (watch.timeUnit) {
      switch (watch.timeUnit) {
        case '1':
          newDefaultNote += ' second';
          break;
        case '2':
          newDefaultNote += ' minute';
          break;
        case '3':
          newDefaultNote += ' hour';
          break;
        case '4':
          newDefaultNote += ' day';
          break;
        case '7':
          newDefaultNote += ' week';
          break;
        default:
          // should this set an invalid flag
          break;
      }
      if (watch.timeSpan > 1) {
        newDefaultNote += 's';
      }
    } else {
      newDefaultNote += ' {}';
    }

    //upper bound default note
    if (this.upperCheck) {
      newDefaultNote += ' or is overridden if the actual value is greater than';

      if (
        watch.upperControl === UpperBoundTypes.Model ||
        watch.upperControl === UpperBoundTypes.Relative
      ) {
        newDefaultNote += ' the model’s expected value plus';
      }
      if (!isNil(watch.upperUnits)) {
        newDefaultNote += ' ' + watch.upperUnits.toPrecision(6);
      } else {
        newDefaultNote += ' {}';
      }
      if (this.model[0].EngUnits) {
        newDefaultNote += ' ' + this.model[0].EngUnits;
      }
    }

    //lower bound default note
    if (this.lowerCheck) {
      if (this.upperCheck) {
        newDefaultNote += ' or less than';
      } else {
        newDefaultNote += ' or is overridden if the actual value is less than';
      }

      if (
        watch.lowerControl === LowerBoundTypes.Model ||
        watch.lowerControl === LowerBoundTypes.Relative
      ) {
        newDefaultNote += ' the model’s expected value minus';
      }
      if (!isNil(watch.lowerUnits)) {
        newDefaultNote += ' ' + watch.lowerUnits.toPrecision(6);
      } else {
        newDefaultNote += ' {}';
      }
      if (this.model[0].EngUnits) {
        newDefaultNote += ' ' + this.model[0].EngUnits;
      }
    }

    return newDefaultNote + '.';
  }

  actionForm(action: string) {
    this.resetForms();
    switch (action) {
      case 'diagnose':
        this.showDiagnoseForm = true;
        break;
      case 'note':
        this.showAddNoteForm = true;
        break;
      case 'maintenance':
        this.showMaintenanceForm = true;
        break;
      case 'cleardiagnose':
        this.showDiagnoseForm = true;
        break;
      case 'clearmaintenance':
        this.showMaintenanceForm = true;
        break;
      default:
        break;
    }
    this.showOnlyActionForms = true;
  }

  resetForms() {
    this.defaultNote = '';
    this.showCustomWatchForm = false;
    this.showOnlyActionForms = false;
    this.showDiagnoseForm = false;
    this.showAddNoteForm = false;
    this.showMaintenanceForm = false;
    this.customWatchForm.reset();
    this.newDiagnoseForm.reset();
    this.newNoteForm.reset();
    this.newMaintenanceForm.reset();

    const actionNote = this.createActionNote();
    this.newDiagnoseForm.patchValue({ diagnoseNote: actionNote });
    this.newMaintenanceForm.patchValue({ maintenanceNote: actionNote });
  }
  ModelConfig() {
    this.actionFlyOutStateChange.emit(modelConfig(this.getModels()));
  }

  OpModeConfig() {
    this.actionFlyOutStateChange.emit(opModeConfig(this.getModels()));
  }

  DiagnosticDrilldown() {
    this.loggerService.trackAppInsightsEvent(
      'Alerts:Button:Diagnostic_Drilldown',
      {
        button: 'diagnostic_drilldown',
      }
    );
    this.actionFlyOutStateChange.emit(diagnosticDrilldown(this.getModels()));
  }

  DataExplorer() {
    this.loggerService.trackAppInsightsEvent('Alerts:Button:Data_Explorer', {
      button: 'data_explorer',
    });
    this.actionFlyOutStateChange.emit(dataExplorer(this.getModels()));
  }

  createIssue() {
    this.actionFlyOutStateChange.emit(newIssue(this.getModels()));
  }

  showRelatedModels() {
    this.actionFlyOutStateChange.emit(
      showRelatedModels(this.model[0].AssetGUID)
    );
  }

  showRelatedIssues() {
    this.actionFlyOutStateChange.emit(
      showRelatedIssues(this.model[0].AssetGUID)
    );
  }

  /*upperUnitRequiredValidator(formGroup: FormGroup) {
    if (
      formGroup.value.upperControl !== null &&
      formGroup.value.upperControl !== UpperBoundTypes.Model
    ) {
      return formGroup.get('upperUnits').value === null
        ? {
            upperUnitRequiredValidator: true,
          }
        : null;
    }
    return null;
  }

  lowerUnitRequiredValidator(formGroup: FormGroup) {
    if (
      formGroup.value.lowerControl !== null &&
      formGroup.value.lowerControl !== LowerBoundTypes.Model
    ) {
      return formGroup.get('lowerUnits').value === null
        ? {
            lowerUnitRequiredValidator: true,
          }
        : null;
    }
    return null;
  }*/
}
