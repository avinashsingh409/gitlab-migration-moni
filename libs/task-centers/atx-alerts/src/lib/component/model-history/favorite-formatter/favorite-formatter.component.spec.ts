import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';

import { FavoriteFormatterComponent } from './favorite-formatter.component';

describe('FavoriteFormatterComponent', () => {
  let component: FavoriteFormatterComponent;
  let fixture: ComponentFixture<FavoriteFormatterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [FavoriteFormatterComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteFormatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
