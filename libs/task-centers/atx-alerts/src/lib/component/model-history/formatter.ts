import moment from 'moment';
import { isNil } from '@atonix/atx-core';

export function dateFormatter(params) {
  return !isNil(params.value)
    ? moment(params.value).format('MM/DD/YYYY hh:mm A')
    : null;
}
