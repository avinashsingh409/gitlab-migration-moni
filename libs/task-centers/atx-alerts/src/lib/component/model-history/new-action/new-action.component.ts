import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { INewActionItem } from '../../../model/new-action-item';

@Component({
  selector: 'atx-new-action',
  templateUrl: './new-action.component.html',
  styleUrls: ['./new-action.component.scss'],
})
export class NewActionComponent {
  @Output() save = new EventEmitter<INewActionItem>();

  state: 'none' | 'adding' = 'none';
  noteType = new UntypedFormControl();
  notePriority = new UntypedFormControl();
  noteValue = new UntypedFormControl();
  favorite = false;

  add() {
    this.noteType.setValue('savenote');
    this.notePriority.setValue(0);
    this.noteValue.setValue('');
    this.favorite = false;
    this.state = 'adding';
  }

  saveActionItem() {
    const value: INewActionItem = {
      model: null,
      state: this.getModelActionItemType(),
      priority: this.getPriority(),
      expiresAfter: null,
      expiresAfterUnits: null,
      alertLimitRule: null,
      limitValue: null,
      alertLimitRateTimeUnit: null,
      combinedNote: this.noteValue.value,
      relativeToExpected: false,
      favorite: this.favorite,
    };
    this.save.emit(value);
    this.state = 'none';
  }

  getPriority() {
    return parseInt(this.notePriority.value, 10);
  }

  getModelActionItemType() {
    return this.noteType.value;
  }

  cancelActionItem() {
    this.state = 'none';
  }

  setFavorite(newValue: boolean) {
    this.favorite = newValue;
  }
}
