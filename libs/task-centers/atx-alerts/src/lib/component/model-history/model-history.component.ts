import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  SimpleChanges,
  OnChanges,
  OnDestroy,
  ChangeDetectorRef,
  EventEmitter,
  Output,
} from '@angular/core';
import {
  GridApi,
  ColumnApi,
  GridReadyEvent,
  Module,
  GridOptions,
  EnterpriseCoreModule,
  ServerSideRowModelModule,
  ValueFormatterParams,
  CellDoubleClickedEvent,
  CellValueChangedEvent,
  FilterChangedEvent,
  ServerSideStoreType,
  _,
} from '@ag-grid-enterprise/all-modules';

import { dateFormatter } from './formatter';
import { dateComparator, isNil } from '@atonix/atx-core';
import { FavoriteFormatterComponent } from './favorite-formatter/favorite-formatter.component';
import { ModelHistoryRetrieverService } from '../../service/model-history-retriever.service';
import { INDModelSummary, INDModelActionItem } from '@atonix/shared/api';
import { INewActionItem } from '../../model/new-action-item';
import { IModelHistoryFilters } from '../../model/model-history-filters';

@Component({
  selector: 'atx-model-history',
  templateUrl: './model-history.component.html',
  styleUrls: ['./model-history.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelHistoryComponent implements OnChanges, OnDestroy {
  @Input() model: INDModelSummary;
  @Input() listTheme: string;
  @Input() count = '-';
  @Input() busy = false;
  @Input() filters: IModelHistoryFilters;
  @Input() refreshDate: Date;
  @Output() setFavorite = new EventEmitter<{
    actionItem: INDModelActionItem;
    favorite: boolean;
  }>();
  @Output() setNote = new EventEmitter<{
    actionItem: INDModelActionItem;
    note: string;
  }>();
  @Output() newActionItem = new EventEmitter<INewActionItem>();
  @Output() setFilters = new EventEmitter<IModelHistoryFilters>();

  columnApi: ColumnApi;
  gridApi: GridApi;
  sideBar;
  modules: Module[] = [EnterpriseCoreModule, ServerSideRowModelModule];
  private storeType: ServerSideStoreType = 'partial';

  constructor(
    private modelHistoryDataSource: ModelHistoryRetrieverService,
    private changeDetector: ChangeDetectorRef
  ) {
    modelHistoryDataSource.onRowCountChanged = (value: number) => {
      this.count = isNil(value) ? '-' : String(value);
      changeDetector.markForCheck();
    };
  }

  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    animateRows: true,
    debug: false,
    rowSelection: 'single',
    suppressRowClickSelection: true,
    suppressCellFocus: true,
    suppressMultiSort: true,
    tooltipShowDelay: 0,
    defaultColDef: {
      resizable: true,
      filter: true,
      floatingFilter: true,
      sortable: true,
    },
    onColumnResized: this.onColumnResized,
    columnDefs: [
      {
        colId: 'ModelID',
        headerName: 'Model',
        field: 'ModelID',
        filter: 'agNumberColumnFilter',
        valueFormatter: (params: ValueFormatterParams) => {
          return this.model?.ModelName;
        },
        sortable: false,
        hide: true,
        floatingFilter: false,
        suppressMenu: true,
        filterParams: {
          suppressAndOrCondition: true,
          filterOptions: ['equals'],
        },
      },
      {
        colId: 'Favorite',
        width: 65,
        headerName: 'Favorite',
        headerTooltip: 'Favorite',
        field: 'Favorite',
        cellRenderer: 'favoriteRenderer',
        sortable: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        onCellDoubleClicked: (event: CellDoubleClickedEvent) => {
          this.favoriteChanged(event);
        },
      },
      {
        colId: 'History',
        headerName: 'History',
        field: 'ModelActionItemTypeAbbrev',
        sortable: true,
        width: 250,
        filter: 'agSetColumnFilter',
        cellClass: 'cell-selectable',
        filterParams: {
          values: [
            'Diagnose Set',
            'Diagnose Cleared',
            'Watch Set',
            'Watch Cleared',
            'Watch Expiration',
            'Model Maintenance Set',
            'Model Maintenance Cleared',
            'Issue Created',
            'Issue Closed',
            'Note Added',
            'Ignore Set',
            'Clear Alert Status',
            'Ignore Expiration',
            'Watch Override',
            'Stop Ignoring',
            'Quick Watch Set',
          ],
        },
      },
      {
        colId: 'ChangeDate',
        headerName: 'TimeStamp',
        field: 'ChangeDate',
        sortable: true,
        width: 250,
        filter: 'agDateColumnFilter',
        cellClass: 'cell-selectable',
        valueFormatter: dateFormatter,
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
          suppressAndOrCondition: true,
          filterOptions: ['inRange'],
          debounceMs: 1000,
        },
      },
      {
        colId: 'Executor',
        headerName: 'User',
        field: 'Executor',
        sortable: true,
        width: 250,
        cellClass: 'cell-selectable',
        tooltipField: 'Executor',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
      },
      {
        colId: 'Note',
        headerName: 'Note',
        field: 'NoteText',
        sortable: true,
        filter: 'agTextColumnFilter',
        cellClass: ['cell-selectable', 'cell-wrap-text'],
        flex: 1,
        autoHeight: true,
        tooltipField: 'NoteText',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
        editable: true,
        onCellValueChanged: (event: CellValueChangedEvent) => {
          this.noteChanged(event);
        },
      },
    ],
    getRowId: (params) => {
      return (
        params.data?.ModelActionItemArchiveID ??
        params.data?.ModelConditionNoteID
      );
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.initializeGrid();
    },
    components: {
      favoriteRenderer: FavoriteFormatterComponent,
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      this.filterChanged(event);
    },
  };

  ngOnDestroy(): void {
    this.modelHistoryDataSource.cancel();
  }

  onColumnResized(params) {
    params.api.resetRowHeights();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.gridApi && changes.model) {
      this.setModelFilter(changes.model.currentValue?.ModelID);
      this.reload();
    } else if (
      changes.busy &&
      changes.busy.currentValue === false &&
      changes.busy.previousValue === true
    ) {
      this.reload();
    } else if (changes?.refreshDate?.currentValue) {
      this.reload();
    }
  }

  setModelFilter(newValue?: number) {
    const val = newValue ?? this.model?.ModelID;

    if (this.gridApi) {
      const filterInstance = this.gridApi.getFilterInstance('ModelID');
      if (filterInstance) {
        const oldModel = filterInstance.getModel();
        if (oldModel?.filter !== val) {
          filterInstance.setModel({ type: 'equals', filter: val });
        }
      }
    }
  }

  getFilterModel() {
    const filterModel: any = {
      ModelID: { type: 'equals', filter: this.model?.ModelID },
    };
    if (this.filters) {
      if (this.filters.historyTypes) {
        filterModel.History = {
          values: this.filters.historyTypes,
          filterType: 'set',
        };
      }
      if (this.filters.start && this.filters.end) {
        filterModel.ChangeDate = {
          dateFrom: this.filters.start,
          dateTo: this.filters.end,
          filterType: 'date',
          type: 'inRange',
        };
      }
      if (!isNil(this.filters.favorite)) {
        filterModel.Favorite = {
          values: [this.filters.favorite ? 'true' : 'false'],
          filterType: 'set',
        };
      }
      if (this.filters.note) {
        filterModel.Note = {
          filter: this.filters.note,
          filterType: 'text',
          type: 'contains',
        };
      }
      if (this.filters.user) {
        filterModel.Executor = {
          filter: this.filters.user,
          filterType: 'text',
          type: 'contains',
        };
      }
    } else {
      filterModel.History = {
        values: [
          'Watch Set',
          'Diagnose Set',
          'Diagnose Cleared',
          'Model Maintenance Set',
          'Model Maintenance Cleared',
          'Note Added',
        ],
        filterType: 'set',
      };
    }
    if (this.model?.ModelID) {
      filterModel.ModelID = {
        type: 'equals',
        filter: this.model?.ModelID,
        filterType: 'number',
      };
    }
    return filterModel;
  }

  initializeGrid() {
    if (this.gridApi && this.columnApi) {
      this.gridApi.setFilterModel(this.getFilterModel());
      this.gridApi.setServerSideDatasource(this.modelHistoryDataSource);
    }
  }

  favoriteChanged(event: CellDoubleClickedEvent) {
    const value: INDModelActionItem = event.data;
    this.setFavorite.emit({ actionItem: value, favorite: !value.Favorite });
    event.node.updateData({ ...value, Favorite: !value.Favorite });
  }

  noteChanged(event: CellValueChangedEvent) {
    const value: INDModelActionItem = event.data;
    this.setNote.emit({ actionItem: value, note: event.newValue });
  }

  saveNewActionItem(event: INewActionItem) {
    event.model = this.model;
    this.newActionItem.emit(event);
  }

  reload() {
    if (this.gridApi) {
      this.gridApi.onFilterChanged();
    }
  }

  filterChanged(event: FilterChangedEvent) {
    const filters = event.api.getFilterModel();
    const newFilters: IModelHistoryFilters = {
      historyTypes: [],
      start: null,
      end: null,
      user: null,
      note: null,
      favorite: null,
    };
    if (filters.Favorite) {
      newFilters.favorite = filters.Favorite.values[0] === 'true';
    }
    if (filters.History) {
      newFilters.historyTypes = filters.History.values;
    }
    if (filters.ChangeDate) {
      newFilters.start = filters.ChangeDate.dateFrom;
      newFilters.end = filters.ChangeDate.dateTo;
    }
    if (filters.Executor) {
      newFilters.user = filters.Executor.filter;
    }
    if (filters.Note) {
      newFilters.note = filters.Note.filter;
    }
    if (!_.jsonEquals(newFilters, this.filters)) {
      this.setFilters.emit(newFilters);
    }
  }
}
