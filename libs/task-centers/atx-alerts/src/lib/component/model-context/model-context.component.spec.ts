import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ModelContextComponent } from './model-context.component';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { createMock } from '@testing-library/angular/jest-utils';

describe('ModelContextComponent', () => {
  let authFacadeMock: AuthFacade;
  let component: ModelContextComponent;
  let fixture: ComponentFixture<ModelContextComponent>;

  beforeEach(waitForAsync(() => {
    const summaryFacadeMock = createMock(SummaryFacade);

    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [
        { provide: AuthFacade, useValue: authFacadeMock },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: SummaryFacade, useValue: summaryFacadeMock },
      ],
      declarations: [ModelContextComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelContextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
