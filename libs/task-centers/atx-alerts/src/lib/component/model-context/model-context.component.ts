import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ICorrelationData, INDModelSummary } from '@atonix/shared/api';
import { IBtnGrpStateChange, IUpdateLimitsData } from '@atonix/atx-chart-v2';
import {
  MatDialogConfig,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { AddTrendDialogComponent } from './add-trend-dialog/add-trend-dialog.component';
import { IProcessedTrend } from '@atonix/atx-core';
import { AuthFacade } from '@atonix/shared/state/auth';
import { SummaryFacade } from '../../store/facade/summary.facade';

@Component({
  selector: 'atx-model-context',
  templateUrl: './model-context.component.html',
  styleUrls: ['./model-context.component.scss'],
})
export class ModelContextComponent {
  @Input() model: INDModelSummary;
  @Input() theme: string;
  @Input() trends: IProcessedTrend[];
  @Input() selectedTrend: IProcessedTrend;
  @Input() selectedTrendID: string;
  @Input() correlationTrend: ICorrelationData[];
  @Input() loading: boolean;
  @Input() multiselected: boolean;

  @Output() selection = new EventEmitter<string>();
  @Output() changeLabelsClicked = new EventEmitter();
  @Output() editChartClicked = new EventEmitter();
  @Output() updateLimits = new EventEmitter<IUpdateLimitsData>();
  @Output() resetLimits = new EventEmitter<number>();
  @Output() legendItemToggle = new EventEmitter<number>();
  dialogRef: MatDialogRef<AddTrendDialogComponent>;

  public dialogEmit: any;
  constructor(
    public dialog: MatDialog,
    private authFacade: AuthFacade,
    private summaryFacade: SummaryFacade
  ) {}

  selectTrend(trendID: string) {
    this.selection.emit(trendID);
  }

  onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'ChangeLabels') {
      this.summaryFacade.toggleClick(+change.newValue);
    } else if (change.event === 'EditChart') {
      this.editChartClicked.emit();
    } else if (change.event === 'UpdateLimits') {
      this.updateLimits.emit(change.newValue as IUpdateLimitsData);
    } else if (change.event === 'ResetLimits') {
      this.resetLimits.emit(change.newValue as number);
    } else if (change.event === 'LegendItemToggle') {
      this.legendItemToggle.emit(change.newValue as number);
    }
  }
  addTrend(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog';
    dialogConfig.data = {
      assetDesc: this.model.AssetDesc,
      modelName: this.model.ModelName,
    };
    dialogConfig.autoFocus = false;

    this.dialogEmit = this.dialog.open(AddTrendDialogComponent, dialogConfig);
  }
}
