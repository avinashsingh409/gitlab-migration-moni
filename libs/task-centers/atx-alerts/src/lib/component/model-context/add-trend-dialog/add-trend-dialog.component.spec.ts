import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddTrendDialogComponent } from './add-trend-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SummaryFacade } from '../../../store/facade/summary.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { INDModelPDTrendMap } from '@atonix/atx-core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import {
  AssetTreeModule,
  ITree,
  ITreeConfiguration,
} from '@atonix/atx-asset-tree';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AddTrendDialogComponent', () => {
  let component: AddTrendDialogComponent;
  let fixture: ComponentFixture<AddTrendDialogComponent>;

  const mockDialogRef = {
    close: jest.fn(),
  };
  const dialogData = {
    assetDesc: 'Asset Description',
    modelName: 'Model Name',
  };
  let summaryFacadeMock: SummaryFacade;

  beforeEach(waitForAsync(() => {
    summaryFacadeMock = createMockWithValues(SummaryFacade, {
      selectLoadingList$: new BehaviorSubject<boolean>(false),
      modelContextCharts$: new BehaviorSubject<{
        [id: string]: INDModelPDTrendMap;
      }>(null),
      modelContextDrilldown$: new BehaviorSubject<{
        [id: string]: INDModelPDTrendMap;
      }>(null),
      selectSavedTrend$: new BehaviorSubject<boolean>(false),
      selectedAsset$: new BehaviorSubject<string>('123'),
      selectTimeSelection$: new BehaviorSubject<{
        startDate: Date;
        endDate: Date;
      }>(null),
      modelContextAssetTreeConfiguration$:
        new BehaviorSubject<ITreeConfiguration>({
          showTreeSelector: true,
          trees: [],
          selectedTree: null,
          autoCompleteValue: null,
          autoCompletePending: false,
          autoCompleteAssets: null,
          nodes: null,
          pin: false,
          loadingAssets: false,
          loadingTree: false,
          canView: true,
          canAdd: true,
          canEdit: true,
          canDelete: true,
          collapseOthers: false,
          hideConfigureButton: false,
        }),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: null,
        pin: false,
        loadingAssets: false,
        loadingTree: false,
        canView: true,
        canAdd: true,
        canEdit: true,
        canDelete: true,
        collapseOthers: false,
        hideConfigureButton: false,
      }),
    });

    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: dialogData,
          },
        },
        {
          provide: SummaryFacade,
          useValue: summaryFacadeMock,
        },
      ],
      imports: [
        AssetTreeModule,
        FormsModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      declarations: [AddTrendDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrendDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
