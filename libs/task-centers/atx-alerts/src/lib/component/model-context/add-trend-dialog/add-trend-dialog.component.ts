import {
  Component,
  OnInit,
  Inject,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ITreeConfiguration, ITreeStateChange } from '@atonix/atx-asset-tree';
import { SummaryFacade } from '../../../store/facade/summary.facade';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { INDModelPDTrendMap } from '@atonix/atx-core';

@Component({
  selector: 'atx-add-trend-dialog',
  templateUrl: './add-trend-dialog.component.html',
  styleUrls: ['./add-trend-dialog.component.scss'],
})
export class AddTrendDialogComponent implements OnInit, OnDestroy {
  assetDesc: string;
  modelName: string;
  drilldown: INDModelPDTrendMap[];
  charts: INDModelPDTrendMap[];
  treeState: ITreeConfiguration;
  loadingList: boolean;
  public selectloadingList$: Observable<boolean>;
  public selectSavedTrend$: Observable<boolean>;
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public drilldown$: Observable<{ [id: string]: INDModelPDTrendMap }>;
  public charts$: Observable<{ [id: string]: INDModelPDTrendMap }>;
  public unsubscribe$ = new Subject<void>();

  constructor(
    public dialogRef: MatDialogRef<AddTrendDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public summaryFacade: SummaryFacade
  ) {
    summaryFacade.addTrendDialogOpened();
    this.assetTreeConfiguration$ =
      summaryFacade.modelContextAssetTreeConfiguration$;
    this.drilldown$ = summaryFacade.modelContextDrilldown$;
    this.charts$ = summaryFacade.modelContextCharts$;
    this.selectloadingList$ = summaryFacade.selectLoadingList$;
    this.selectSavedTrend$ = summaryFacade.selectSavedTrend$;

    this.selectloadingList$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        this.loadingList = state;
      });

    this.selectSavedTrend$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        if (state) {
          this.closeDialog();
        }
      });

    this.charts$.pipe(takeUntil(this.unsubscribe$)).subscribe((state) => {
      this.charts = [];
      for (const key in state) {
        if (state) {
          const value = state[key];
          this.charts.push(value);
        }
      }
    });
    this.drilldown$.pipe(takeUntil(this.unsubscribe$)).subscribe((state) => {
      this.drilldown = [];

      for (const key in state) {
        if (state) {
          const value = state[key];
          this.drilldown.push(value);
        }
      }
      this.drilldown = this.sortDisplayOrder(this.drilldown);
    });
  }

  // TODO: Make the moving and order of the items on the Drag be saved
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  save() {
    let index = 0;
    this.drilldown = this.drilldown.map((drill) => {
      const obj = { ...drill };
      obj.DisplayOrder = index++;
      return obj;
    });
    this.summaryFacade.savePDTrendsMappedToNDModel(this.drilldown);
  }

  sortDisplayOrder(array: INDModelPDTrendMap[]) {
    // Checks if all has zero display order and sort it by title by default
    if (array.every((val) => val.DisplayOrder === 0)) {
      return array.sort((a, b) => {
        if (a?.MappedPDTrend?.Title < b?.MappedPDTrend?.Title) {
          return -1;
        } else if (a?.MappedPDTrend?.Title > b?.MappedPDTrend?.Title) {
          return 1;
        } else {
          return 0;
        }
      });
    } else {
      return array.sort((a: INDModelPDTrendMap, b: INDModelPDTrendMap) => {
        if (a.DisplayOrder < b.DisplayOrder) {
          return -1;
        } else if (a.DisplayOrder > b.DisplayOrder) {
          return 1;
        } else {
          return 0;
        }
      });
    }
  }

  ngOnInit(): void {
    this.assetDesc = this.data.assetDesc;
    this.modelName = this.data.modelName;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  closeDialog() {
    this.dialogRef.close();
  }

  public onModelContextAssetTreeStateChange(change: ITreeStateChange) {
    this.summaryFacade.modelContextTreeStateChange(change);
  }
}
