import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SummaryComponent } from './summary.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AssetTreeModule, ITreeConfiguration } from '@atonix/atx-asset-tree';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { ChartModule } from '@atonix/atx-chart';
import { ReactiveFormsModule } from '@angular/forms';
import { NavFacade } from '@atonix/atx-navigation';
import { ModelTabsComponent } from '../model-tabs/model-tabs.component';
import { AgGridModule } from '@ag-grid-community/angular';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { AlertsService } from '../../service/alerts.service';
import { SavedToolPanelComponent } from '../saved-tool-panel/saved-tool-panel.component';
import { IssueRetrieverService } from '../../service/issue-retriever.service';
import { AlertRetrieverService } from '../../service/alert-retriever.service';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ScreeningViewComponent } from '../screening-view/screening-view.component';
import {
  createMock,
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { BehaviorSubject } from 'rxjs';
import { ActionFlyoutComponent } from '../action-flyout/action-flyout.component';
import { UIConfigFrameworkService } from '@atonix/shared/api';
import { ModelTrendComponent } from '../model-trend/model-trend.component';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
describe('SummaryComponent', () => {
  let navFacadeMock: NavFacade;
  let alertRetrieverServiceMock: AlertRetrieverService;
  let issueRetrieverServiceMock: IssueRetrieverService;
  let summaryFacadeMock: SummaryFacade;
  let authFacadeMock: AuthFacade;
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;

  beforeEach(() => {
    alertRetrieverServiceMock = createMock(AlertRetrieverService);
    issueRetrieverServiceMock = createMock(IssueRetrieverService);

    summaryFacadeMock = createMockWithValues(SummaryFacade, {
      selectedAsset$: new BehaviorSubject<string>('123'),
      selectTimeSelection$: new BehaviorSubject<{
        startDate: Date;
        endDate: Date;
      }>(null),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: null,
        pin: false,
        loadingAssets: false,
        loadingTree: false,
        canView: true,
        canAdd: true,
        canEdit: true,
        canDelete: true,
        collapseOthers: false,
        hideConfigureButton: false,
      }),
    });

    const alertsServiceMock = createMock(AlertsService);

    navFacadeMock = createMockWithValues(NavFacade, {
      assetTreeSelected$: new BehaviorSubject<boolean>(true),
      navPaneState$: new BehaviorSubject<boolean>(true),
      showTimeSlider$: new BehaviorSubject<boolean>(true),
      showRightTray$: new BehaviorSubject<boolean>(true),
      theme$: new BehaviorSubject<string>('light'),
    });
    TestBed.configureTestingModule({
      imports: [
        AssetTreeModule,
        ChartModule,
        RouterTestingModule,
        TimeSliderModule,
        ReactiveFormsModule,
        AgGridModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        provideMock(UIConfigFrameworkService),
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: SummaryFacade, useValue: summaryFacadeMock },
        { provide: AlertsService, useValue: alertsServiceMock },
        { provide: NavFacade, useValue: navFacadeMock },
        { provide: AuthFacade, useValue: authFacadeMock },
        { provide: AlertRetrieverService, useValue: alertRetrieverServiceMock },
        { provide: IssueRetrieverService, useValue: issueRetrieverServiceMock },
      ],
      declarations: [
        ActionFlyoutComponent,
        SummaryComponent,
        ScreeningViewComponent,
        ModelTrendComponent,
        ModelTabsComponent,
        SavedToolPanelComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
