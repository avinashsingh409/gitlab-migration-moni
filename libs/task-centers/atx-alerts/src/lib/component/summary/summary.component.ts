import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  AfterViewInit,
} from '@angular/core';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import {
  ITreeStateChange,
  ITreeConfiguration,
  TrayState,
} from '@atonix/atx-asset-tree';
import {
  ITimeSliderState,
  ITimeSliderStateChange,
} from '@atonix/atx-time-slider';
import {
  NavFacade,
  INavigationState,
  IButtonData,
  getStartFromRoute,
  getEndFromRoute,
  getUniqueIdFromRoute,
} from '@atonix/atx-navigation';
import {
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
  IActionItem,
  IListState,
  INDModelActionItem,
  IGridParameters,
  IUserPreferences,
  ICorrelationData,
} from '@atonix/shared/api';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { INDModelsListResult, INDModelSummary } from '@atonix/shared/api';
import { IActionFlyoutStateChange } from '../../model/action-flyout-state-change';
import { IUpdatedModels } from '../../store/state/summary-state';
import { IToast } from '../../store/state/alerts-state';
import { INewActionItem } from '../../model/new-action-item';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { LoggerService } from '@atonix/shared/utils';
import { IAssetMeasurementsSet, IProcessedTrend } from '@atonix/atx-core';
import { AuthFacade } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-alerts-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryComponent implements OnInit, AfterViewInit, OnDestroy {
  public isSplit$: Observable<boolean>;
  public isList$: Observable<boolean>;
  public isTrend$: Observable<boolean>;

  public alertModelListResult$: Observable<INDModelsListResult>;
  public layoutMode$: Observable<TrayState>;
  public leftTraySize$: Observable<number>;
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public userPreferencesState$: Observable<IUserPreferences>;
  public unsubscribe$ = new Subject<void>();
  public asset$: Observable<string>;
  public timeSliderState$: Observable<ITimeSliderState>;
  public timeSliderStateChanges$: Subject<ITimeSliderStateChange>;
  public showTimeSlider$: Observable<boolean>;
  public listState$: Observable<IListState>;
  public showLaunchPad$: Observable<boolean>;
  public navPaneState$: Observable<'hidden' | 'open' | 'expanded'>;
  public assetTreeSelected$: Observable<boolean>;
  public modelTrend$: Observable<IAssetModelChartingData>;
  public modelTrendLoading$: Observable<boolean>;
  public pdNdTrends$: Observable<IProcessedTrend[]>;
  public pdNdSelectedTrend$: Observable<IProcessedTrend>;
  public modelContextTrends$: Observable<IProcessedTrend[]>;
  public modelContextSelectedTrend$: Observable<IProcessedTrend>;
  public modelContextSelectedTrendID$: Observable<string>;
  public modelContextCorrelationTrend$: Observable<ICorrelationData[]>;
  public modelContextLoading$: Observable<boolean>;
  public getRenderModelContextChart$: Observable<IProcessedTrend>;
  public modelHistorySaving$: Observable<boolean>;
  public modelHistoryFilters$: Observable<IModelHistoryFilters>;
  public selectedModel$: Observable<INDModelSummary>;
  public selectedModels$: Observable<INDModelSummary[]>;
  public navState$: Observable<INavigationState>;
  public getProcessedTrendData$: Observable<IAssetMeasurementsSet>;
  public selectRefreshing$: Observable<boolean>;
  public theme$: Observable<string>;
  public selectToast$: Observable<IToast>;
  public floatingFilter$: Observable<boolean>;
  public multiselected$: Observable<boolean>;
  public modelAction$: Observable<INDModelActionItemAnalysis[]>;
  public modelActionLoading$: Observable<boolean>;
  public showRightTray$: Observable<boolean>;
  public updatedModels$: Observable<IUpdatedModels>;

  public refreshable$: Observable<boolean>;
  public trendsLoaded$: Observable<boolean>;
  public dataLoaded$: Observable<boolean>;
  public refreshDate$: Observable<Date>;
  public clearFilters: boolean;

  constructor(
    private navFacade: NavFacade,
    private summaryFacade: SummaryFacade,
    private logger: LoggerService,
    private activatedRoute: ActivatedRoute,
    private authFacade: AuthFacade
  ) {
    const initialStartFromRoute = getStartFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const initialEndFromRoute = getEndFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.clearFilters =
      this.activatedRoute.snapshot.queryParamMap.get('clearFilters') === 'true';

    this.summaryFacade.initializeSummary(
      getUniqueIdFromRoute(this.activatedRoute.snapshot.queryParamMap),
      initialStartFromRoute
        ? moment(parseFloat(initialStartFromRoute)).toDate()
        : moment().subtract(1, 'months').toDate(),
      initialEndFromRoute
        ? moment(parseFloat(initialEndFromRoute)).toDate()
        : moment().toDate()
    );

    this.isSplit$ = this.summaryFacade.summaryViewSplit$;
    this.isList$ = this.summaryFacade.summaryViewList$;
    this.isTrend$ = this.summaryFacade.summaryViewTrend$;

    this.updatedModels$ = this.summaryFacade.updatedModels$;
    this.theme$ = this.navFacade.theme$;
    this.assetTreeConfiguration$ = this.summaryFacade.assetTreeConfiguration$;
    this.asset$ = this.summaryFacade.selectedAsset$;
    this.timeSliderState$ = this.summaryFacade.timeSliderState$;
    this.refreshable$ = this.summaryFacade.refreshable$;
    this.layoutMode$ = this.summaryFacade.layoutMode$;
    this.leftTraySize$ = this.summaryFacade.leftTraySize$;
    this.navPaneState$ = this.navFacade.navPaneState$;
    this.listState$ = this.summaryFacade.savedListState$;
    this.assetTreeSelected$ = this.navFacade.assetTreeSelected$;
    this.showTimeSlider$ = this.navFacade.showTimeSlider$;
    this.modelTrend$ = this.summaryFacade.modelTrend$;
    this.modelTrendLoading$ = this.summaryFacade.modelTrendLoading$;
    this.modelContextTrends$ = this.summaryFacade.modelContextTrends$;
    this.modelContextSelectedTrend$ =
      this.summaryFacade.selectedModelContextTrend$;
    this.modelContextSelectedTrendID$ =
      this.summaryFacade.selectedModelContextTrendID$;
    this.modelContextCorrelationTrend$ =
      this.summaryFacade.selectModelContextCorrelationTrend$;
    this.modelContextLoading$ = this.summaryFacade.modelContextLoading$;
    this.selectedModel$ = this.summaryFacade.selectedModel$;
    this.selectedModels$ = this.summaryFacade.selectedModels$;
    this.selectRefreshing$ = this.summaryFacade.selectRefreshing$;
    this.selectToast$ = this.summaryFacade.selectToast$;
    this.modelHistorySaving$ = this.summaryFacade.modelHistorySaving$;
    this.modelHistoryFilters$ = this.summaryFacade.modelHistoryFilters$;
    this.floatingFilter$ = this.summaryFacade.floatingFilter$;
    this.multiselected$ = this.summaryFacade.multiselected$;
    this.modelAction$ = this.summaryFacade.modelAction$;
    this.modelActionLoading$ = this.summaryFacade.modelActionLoading$;
    this.refreshDate$ = this.summaryFacade.refreshDate$;
    this.showRightTray$ = this.navFacade.showRightTray$;

    this.assetTreeSelected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        this.summaryFacade.reflow();
      });

    this.navPaneState$.pipe(takeUntil(this.unsubscribe$)).subscribe((state) => {
      this.summaryFacade.reflow();
    });

    this.showTimeSlider$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        this.summaryFacade.reflow();
      });
  }

  ngAfterViewInit(): void {
    this.asset$.pipe(takeUntil(this.unsubscribe$)).subscribe((asset) => {
      if (asset) {
        this.summaryFacade.selectAsset(asset);
      }
    });

    this.summaryFacade.selectTimeSelection$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((time) => {
        if (time) {
          this.summaryFacade.updateRoute();
        }
      });
  }

  ngOnInit(): void {
    this.showRightTray$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((rightTray) => {
        this.summaryFacade.reflow();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  public onActionFlyOutStateChange(change: IActionFlyoutStateChange) {
    const models = change.models;
    switch (change.event) {
      case 'ToggleActionFlyout':
        this.navFacade.hideRightTray();
        this.summaryFacade.reflow();
        break;
      case 'SetWatch':
        this.summaryFacade.setWatch(models, change.newValue as string);
        break;
      case 'ClearWatch':
        this.summaryFacade.clearWatch(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'ClearAlert':
        this.summaryFacade.clearAlert(models);
        break;
      case 'Refresh':
        // eslint-disable-next-line no-case-declarations
        const modelID = change.models || [];
        this.summaryFacade.reloadFlyOutModel(
          modelID.map((model) => String(model.ModelID))
        );
        break;
      case 'SetCustomWatch':
        this.summaryFacade.setCustomWatch(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'SaveMaintenance':
        this.summaryFacade.saveMaintenance(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'SaveDiagnose':
        this.summaryFacade.saveDiagnose(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'ClearMaintenance':
        this.summaryFacade.clearMaintenance(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'ClearDiagnose':
        this.summaryFacade.clearDiagnose(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'SaveNote':
        this.summaryFacade.saveNote(
          models,
          change.newValue as IActionItem,
          change.newAction as string
        );
        break;
      case 'NewIssue':
        this.summaryFacade.actionFlyOutNewIssue();
        break;
      case 'ModelConfig':
        this.summaryFacade.actionFlyOutModelConfig();
        break;
      case 'OpModeConfig':
        this.summaryFacade.actionFlyOutOpModeConfig();
        break;
      case 'DataExplorer':
        this.summaryFacade.dataExplorer();
        break;
      case 'DiagnosticDrilldown':
        this.summaryFacade.diagnosticDrilldown();
        break;
      case 'ShowRelatedModels':
        this.summaryFacade.showRelatedModels(change.newValue as string);
        break;
      case 'ShowRelatedIssues':
        this.summaryFacade.showRelatedIssues(change.newValue as string);
        break;
    }
  }

  public onModelContextTrendChanged(modelContextTrendID: string) {
    this.summaryFacade.selectModelContextTrend(modelContextTrendID);
  }

  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.summaryFacade.treeStateChange(change);
  }

  public onAssetTreeSizeChange(newSize: number) {
    this.summaryFacade.treeSizeChange(newSize);
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  public onTimeSliderStateChange(change: ITimeSliderStateChange) {
    this.summaryFacade.timeSliderStateChange(change);
    if (change.event === 'ToggleCalendarIndicator') {
      this.logger.feature('togglecalendarindicator', 'alerts');
    }
    if (change.event === 'HideTimeSlider') {
      this.summaryFacade.hideTimeSlider();
    }
    if (change.event === 'SelectDateRange') {
      this.summaryFacade.reloadAllTrends(
        change.newStartDateValue,
        change.newEndDateValue
      );

      if (change.source && change.source === 'live') {
        this.summaryFacade.reloadSummaryData();
      }

      this.logger.feature(
        'selectdaterange_' + (change.source ?? 'any'),
        'alerts'
      );
    }
    if (change.event === 'SelectDate') {
      this.logger.feature('selectdate_' + (change.source ?? 'any'), 'alerts');
    }
    if (change.event === 'ToggleLive') {
      this.logger.feature('togglelive', 'alerts');
    }
    if (change.event === 'RangeShift') {
      this.logger.feature('rangeshift', 'alerts');
    }
    if (change.event === 'Zoom') {
      this.logger.feature('zoom', 'alerts');
    }
  }

  public onListExpand() {
    this.summaryFacade.listExpandClicked();
  }

  public onTrendExpand() {
    this.summaryFacade.trendExpandClicked();
  }

  public modelSelected(model: INDModelSummary[]) {
    this.summaryFacade.selectModel(model);
  }

  listStateChanged(listState: IListState) {
    this.summaryFacade.listStateChanged(listState);
  }

  refresh() {
    this.summaryFacade.reloadAllTrends();
    this.summaryFacade.reloadSummaryData();
  }

  downloadModels(gridParams: IGridParameters) {
    this.summaryFacade.downloadModels(gridParams);
  }

  redraw() {
    this.summaryFacade.reflow();
  }

  toggleClick() {
    this.logger.feature('charttoggle', 'alerts');
    // calling facade in Model context instead of passing up event
    //   this.summaryFacade.toggleClick();
  }

  editModelContextChart() {
    this.logger.feature('editmodelcontextchart', 'alerts');
    this.summaryFacade.editModelContextChart();
  }

  onModelContextTreeStateChange(change: ITreeStateChange) {
    this.summaryFacade.modelContextTreeStateChange(change);
  }

  diagnosticDrilldown() {
    this.summaryFacade.diagnosticDrilldown();
  }

  floatingFilterToggle() {
    this.summaryFacade.toggleFloatingFilter();
  }

  issueSelected(id: string) {
    this.authFacade.issuesManagementTaskCenterAccess$
      .pipe(take(1))
      .subscribe((rights) => {
        if (rights.CanView) {
          this.summaryFacade.issueSelected(id);
        }
      });
  }

  setModelHistoryFavorite(event: {
    actionItem: INDModelActionItem;
    favorite: boolean;
  }) {
    this.summaryFacade.setModelHistoryFavorite(
      event.actionItem,
      event.favorite
    );
  }

  setModelHistoryNote(event: { actionItem: INDModelActionItem; note: string }) {
    this.summaryFacade.setModelHistoryNote(event.actionItem, event.note);
  }

  newActionItem(event: INewActionItem) {
    this.summaryFacade.newActionItem(event);
  }

  modelHistoryFiltersChanged(event: IModelHistoryFilters) {
    this.summaryFacade.setModelHistoryFilters(event);
  }

  updateLimitsModelContextChart(event: IUpdateLimitsData) {
    this.summaryFacade.updateLimitsModelContextChart(event);
  }

  resetLimitsModelContextChart(event: number) {
    this.summaryFacade.resetLimitsModelContextChart(event);
  }

  legendItemToggleModelContextChart(event: number) {
    this.summaryFacade.legendItemToggleModelContextChart(event);
  }

  updateLimitsModelTrend(event: IUpdateLimitsData) {
    this.summaryFacade.updateLimitsModelTrend(event);
  }

  resetLimitsModelTrend(event: number) {
    this.summaryFacade.resetLimitsModelTrend(event);
  }
}
