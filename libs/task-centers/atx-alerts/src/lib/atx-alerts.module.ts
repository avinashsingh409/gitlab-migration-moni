import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HighchartsChartModule } from 'highcharts-angular';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

import { NavigationModule } from '@atonix/atx-navigation';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { TimeSliderModule } from '@atonix/atx-time-slider';
// import { ChartModule } from '@atonix/atx-chart';
import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { SummaryComponent } from './component/summary/summary.component';
import { ScreeningViewComponent } from './component/screening-view/screening-view.component';
import { ModelTabsComponent } from './component/model-tabs/model-tabs.component';
import { AtxCoreModule } from '@atonix/atx-core';
import { SummaryFacade } from './store/facade/summary.facade';
import { SummaryEffects } from './store/effects/summary.effects';
import { ReactiveFormsModule } from '@angular/forms';
import { alertsReducers } from './store/reducers/alerts.reducer';
import { ScreeningActionsRendererComponent } from './component/screening-view/screening-view-renderer/screening-actions-renderer.component';
import { SavedToolPanelComponent } from './component/saved-tool-panel/saved-tool-panel.component';
import { ActionFilterComponent } from './component/screening-view/filter/action-filter/action-filter.component';
import { ModelTrendComponent } from './component/model-trend/model-trend.component';
import { ModelContextComponent } from './component/model-context/model-context.component';
import { ActionFlyoutComponent } from './component/action-flyout/action-flyout.component';
import { ModelHistoryComponent } from './component/model-history/model-history.component';
import { ModelActionComponent } from './component/model-action/model-action.component';
import { TypeRendererComponent } from './component/screening-view/screening-view-renderer/screening-type-renderer.component';
import { AddTrendDialogComponent } from './component/model-context/add-trend-dialog/add-trend-dialog.component';
import { FavoriteFormatterComponent } from './component/model-history/favorite-formatter/favorite-formatter.component';
import { NewActionComponent } from './component/model-history/new-action/new-action.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { AtxMaterialModule } from '@atonix/atx-material';
import { RouterModule } from '@angular/router';
import { SharedApiModule } from '@atonix/shared/api';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AdvisePanelComponent } from './component/advise-panel/advise-panel.component';

@NgModule({
  declarations: [
    SummaryComponent,
    ModelTabsComponent,
    ScreeningViewComponent,
    SavedToolPanelComponent,
    ScreeningActionsRendererComponent,
    ActionFilterComponent,
    ModelTrendComponent,
    ModelContextComponent,
    ModelHistoryComponent,
    ActionFlyoutComponent,
    ModelActionComponent,
    TypeRendererComponent,
    AddTrendDialogComponent,
    FavoriteFormatterComponent,
    NewActionComponent,
    AdvisePanelComponent,
  ],
  imports: [
    AgGridModule.withComponents([
      ScreeningActionsRendererComponent,
      TypeRendererComponent,
    ]),
    RouterModule.forChild([
      { path: '', component: SummaryComponent, pathMatch: 'full' },
    ]),
    AssetTreeModule,
    AtxCoreModule,
    AtxMaterialModule,
    // ChartModule,
    AtxChartModuleV2,
    CommonModule,
    EffectsModule.forFeature([SummaryEffects]),
    NavigationModule,
    ReactiveFormsModule,
    DragDropModule,
    ClipboardModule,
    StoreModule.forFeature('alerts', alertsReducers),
    TimeSliderModule,
    HighchartsChartModule,
    SharedApiModule,
  ],
  providers: [SummaryFacade, AuthFacade],
  exports: [SummaryComponent],
})
export class AlertsModule {}
