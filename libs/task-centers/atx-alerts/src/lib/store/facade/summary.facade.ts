/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { IButtonData } from '@atonix/atx-navigation';
import {
  ITreeStateChange,
  ISelectAsset,
  ModelService,
} from '@atonix/atx-asset-tree';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { Store, select } from '@ngrx/store';
import * as summarySelectors from '../selectors/summary.selector';
import * as actions from '../actions/summary.actions';
import {
  INDModelSummary,
  INDModelActionItem,
  IGridParameters,
  IActionItem,
  IListState,
  ISavedModelList,
} from '@atonix/shared/api';
import { INewActionItem } from '../../model/new-action-item';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import { INDModelPDTrendMap } from '@atonix/atx-core';

@Injectable()
export class SummaryFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(
    private store: Store<any>,
    private assetModelService: ModelService
  ) {}

  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  selectedAsset$ = this.store.pipe(select(summarySelectors.getSelectedAsset));
  timeSliderState$ = this.store.pipe(
    select(summarySelectors.getTimeSliderState)
  );
  refreshable$ = this.store.pipe(select(summarySelectors.selectRefreshable));

  // Configuration of the asset tree
  assetTreeConfiguration$ = this.store.pipe(
    select(summarySelectors.getAssetTreeConfiguration)
  );
  selectedAssetTreeNode$ = this.store.pipe(
    select(summarySelectors.getSelectedAssetTreeNode)
  );

  // Configuration of the ag-grid
  savedLists$ = this.store.pipe(select(summarySelectors.getSavedLists));
  savedListState$ = this.store.pipe(select(summarySelectors.getListState));

  // Grab the layout state from the store.  This will indicate whether
  // the asset tree is open or closed.
  // layoutState$ = this.store.pipe(select(alertsSelectors.getLayoutState));
  layoutMode$ = this.store.pipe(select(summarySelectors.selectLayoutMode));
  leftTraySize$ = this.store.pipe(select(summarySelectors.selectLeftTraySize));

  // Model Trend
  modelTrend$ = this.store.pipe(select(summarySelectors.selectModelTrendData));
  modelTrendLoading$ = this.store.pipe(
    select(summarySelectors.selectModelTrendLoading)
  );

  // Model Context
  modelContextTrends$ = this.store.pipe(
    select(summarySelectors.selectModelContextTrends)
  );
  selectedModelContextTrend$ = this.store.pipe(
    select(summarySelectors.selectSelectedModelContext)
  );
  selectedModelContextTrendID$ = this.store.pipe(
    select(summarySelectors.selectSelectedModelContextID)
  );
  selectModelContextCorrelationTrend$ = this.store.pipe(
    select(summarySelectors.selectModelContextCorrelationTrend)
  );
  modelContextLoading$ = this.store.pipe(
    select(summarySelectors.selectModelContextLoading)
  );
  selectLoadingList$ = this.store.pipe(
    select(summarySelectors.selectLoadingList)
  );
  selectSavedTrend$ = this.store.pipe(
    select(summarySelectors.selectSavedTrend)
  );

  // Model Context - Asset Tree
  selectedModelContextAsset$ = this.store.pipe(
    select(summarySelectors.getModelContextSelectedAsset)
  );
  modelContextAssetTreeConfiguration$ = this.store.pipe(
    select(summarySelectors.getModelContextAssetTreeConfiguration)
  );
  selectedModelContextAssetTreeNode$ = this.store.pipe(
    select(summarySelectors.getModelContextSelectedAssetTreeNode)
  );
  modelContextDrilldown$ = this.store.pipe(
    select(summarySelectors.getModelContextDrilldown)
  );
  modelContextCharts$ = this.store.pipe(
    select(summarySelectors.getModelContextAvailableCharts)
  );

  // Model History
  modelHistory$ = this.store.pipe(
    select(summarySelectors.selectModelHistoryState)
  );
  modelHistorySaving$ = this.store.pipe(
    select(summarySelectors.selectModelHistorySaving)
  );
  modelHistoryFilters$ = this.store.pipe(
    select(summarySelectors.selectModelHistoryFilters)
  );

  // View Status
  summaryViewSplit$ = this.store.pipe(
    select(summarySelectors.selectSummaryViewSplit)
  );
  summaryViewList$ = this.store.pipe(
    select(summarySelectors.selectSummaryViewList)
  );
  summaryViewTrend$ = this.store.pipe(
    select(summarySelectors.selectSummaryViewTrend)
  );

  // Models
  selectedModels$ = this.store.pipe(select(summarySelectors.selectModels));
  selectedModel$ = this.store.pipe(select(summarySelectors.selectModel));
  selectRefreshing$ = this.store.pipe(
    select(summarySelectors.selectRefreshing)
  );
  multiselected$ = this.store.pipe(select(summarySelectors.multiselect));

  selectToast$ = this.store.pipe(select(summarySelectors.selectToast));
  floatingFilter$ = this.store.pipe(
    select(summarySelectors.selectFloatingFilter)
  );

  // Model Action Action
  modelAction$ = this.store.pipe(
    select(summarySelectors.selectModelActionData)
  );
  modelActionLoading$ = this.store.pipe(
    select(summarySelectors.selectModelActionLoading)
  );

  updatedModels$ = this.store.pipe(
    select(summarySelectors.selectUpdatedModels)
  );
  refreshDate$ = this.store.pipe(select(summarySelectors.selectRefreshDate));

  selectTimeSelection$ = this.store.pipe(
    select(summarySelectors.selectTimeSelection)
  );

  aiResponse$ = this.store.pipe(select(summarySelectors.selectAIResponse));
  aiResponseLoading$ = this.store.pipe(
    select(summarySelectors.selectAIResponseLoading)
  );

  // This sets up the summary tab
  initializeSummary(asset?: string, startDate?: Date, endDate?: Date) {
    this.store.dispatch(
      actions.summaryInitialize({ asset, startDate, endDate })
    );
  }

  // Sets the selected model in the state
  selectModel(model: INDModelSummary[]) {
    this.store.dispatch(actions.selectModel({ model }));
  }

  actionFlyOutNewIssue() {
    this.store.dispatch(actions.actionFlyOutNewIssue());
  }

  actionFlyOutModelConfig() {
    this.store.dispatch(actions.actionFlyOutModelConfig());
  }

  actionFlyOutOpModeConfig() {
    this.store.dispatch(actions.actionFlyOutOpModeConfig());
  }

  selectModelContextTrend(trendID: string) {
    this.store.dispatch(actions.selectModelContextTrend({ trendID }));
  }

  reloadAllTrends(startDate?: Date, endDate?: Date) {
    this.store.dispatch(actions.reloadModelContext({ startDate, endDate }));
    this.store.dispatch(actions.reloadModelTrend({ startDate, endDate }));
    this.store.dispatch(actions.reloadModelAction({ startDate, endDate }));
  }

  downloadModels(gridParams: IGridParameters) {
    this.store.dispatch(actions.downloadModelsRequest({ gridParams }));
  }

  // This will be triggered when a tile is not in type 'link' or 'route'
  launchPadTileSelected(btnData: IButtonData) {
    this.store.dispatch(actions.launchPadTileSelect(btnData));
  }

  // For asset tree
  treeSizeChange(value: number) {
    if (value !== null && value !== undefined && value >= 0) {
      this.store.dispatch(actions.treeSizeChange({ value }));
      this.reflow();
    }
  }

  selectAsset(asset: string) {
    this.store.dispatch(actions.selectAsset({ asset }));
  }

  updateRoute() {
    this.store.dispatch(actions.updateRoute());
  }

  treeStateChange(change: ITreeStateChange) {
    if (change.event === 'SelectAsset') {
      this.store.dispatch(actions.updateModelNavConfig(null));
    }
    this.store.dispatch(actions.treeStateChange(change));
    if (change.event === 'PinToggle') {
      this.reflow();
    }
  }

  modelContextTreeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.modelContextAssetTreeStateChange(change));
    if (change.event === 'SelectAsset') {
      const selAss: ISelectAsset = change.newValue as ISelectAsset;
      if (selAss.multiSelect === false) {
        this.store.dispatch(actions.selectModelContextAsset());
      }
    }
  }

  // For Save Tool panel
  listStateChanged(listState: IListState) {
    this.store.dispatch(actions.listStateChange({ listState }));
  }

  loadLists() {
    this.store.dispatch(actions.listsLoad());
  }

  saveList(list: Partial<ISavedModelList>) {
    this.store.dispatch(actions.listSave({ list }));
  }

  loadDefaultList() {
    this.store.dispatch(actions.loadDefaultList());
  }

  loadList(id: number) {
    this.store.dispatch(actions.listLoad({ id }));
  }

  deleteList(id: number) {
    this.store.dispatch(actions.listDelete({ id }));
  }

  timeSliderStateChange(change: ITimeSliderStateChange) {
    this.store.dispatch(actions.timeSliderStateChange(change));
  }

  hideTimeSlider() {
    this.store.dispatch(actions.hideTimeSlider());
  }

  runLive() {
    this.store.dispatch(actions.runLive());
  }

  listExpandClicked() {
    this.store.dispatch(actions.expandButtonClick({ source: 'List' }));
  }

  trendExpandClicked() {
    this.store.dispatch(actions.expandButtonClick({ source: 'Trend' }));
  }

  reflow() {
    this.store.dispatch(actions.reflow());
  }

  toggleClick(index: number) {
    this.store.dispatch(actions.changeLabelsClicked({ index }));
  }

  editModelContextChart() {
    this.store.dispatch(actions.editModelContextChart());
  }

  updateLimitsModelContextChart(limitsData: IUpdateLimitsData) {
    this.store.dispatch(actions.updateLimitsModelContextChart({ limitsData }));
  }

  resetLimitsModelContextChart(axisIndex: number) {
    this.store.dispatch(actions.resetLimitsModelContextChart({ axisIndex }));
  }

  legendItemToggleModelContextChart(seriesIndex: number) {
    this.store.dispatch(
      actions.legendItemToggleModelContextChart({ seriesIndex })
    );
  }

  updateLimitsModelTrend(limitsData: IUpdateLimitsData) {
    this.store.dispatch(actions.updateLimitsModelTrend({ limitsData }));
  }

  resetLimitsModelTrend(axisIndex: number) {
    this.store.dispatch(actions.resetLimitsModelTrend({ axisIndex }));
  }

  reloadFlyOutModel(modelID: string | string[]) {
    if (!Array.isArray(modelID)) {
      modelID = [modelID];
    }
    this.store.dispatch(actions.reloadFlyOutModel({ modelID }));
  }

  setWatch(models: INDModelSummary[], hours: string) {
    this.store.dispatch(actions.setWatch({ models, hours }));
  }

  clearWatch(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(actions.clearWatch({ models, params, actionItemType }));
  }

  clearAlert(models: INDModelSummary[]) {
    this.store.dispatch(actions.clearAlert({ models }));
  }

  setCustomWatch(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(
      actions.setCustomWatch({ models, params, actionItemType })
    );
  }

  saveDiagnose(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(
      actions.saveDiagnose({ models, params, actionItemType })
    );
  }
  saveMaintenance(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(
      actions.saveMaintenance({ models, params, actionItemType })
    );
  }
  clearDiagnose(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(
      actions.clearDiagnose({ models, params, actionItemType })
    );
  }
  clearMaintenance(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(
      actions.clearMaintenance({ models, params, actionItemType })
    );
  }
  saveNote(
    models: INDModelSummary[],
    params: IActionItem,
    actionItemType: string
  ) {
    this.store.dispatch(actions.saveNote({ models, params, actionItemType }));
  }
  diagnosticDrilldown() {
    this.store.dispatch(actions.diagnosticDrilldown());
  }
  dataExplorer() {
    this.store.dispatch(actions.dataExplorer());
  }
  toggleFloatingFilter(filterOn?: boolean) {
    this.store.dispatch(actions.toggleFloatingFilter({ filterOn }));
  }
  issueSelected(id: string) {
    this.store.dispatch(actions.issueSelected({ id }));
  }
  addTrendDialogOpened() {
    this.store.dispatch(actions.addTrendDialogOpened());
  }
  setModelHistoryFavorite(actionItem: INDModelActionItem, favorite: boolean) {
    this.store.dispatch(
      actions.setActionItemFavorite({ actionItem, favorite })
    );
  }
  setModelHistoryNote(actionItem: INDModelActionItem, note: string) {
    this.store.dispatch(actions.setActionItemNote({ actionItem, note }));
  }
  setModelHistoryFilters(filter: IModelHistoryFilters) {
    this.store.dispatch(actions.setModelHistoryFilters({ filter }));
  }
  newActionItem(value: INewActionItem) {
    const models = [value.model];
    if (value.state === 'savenote') {
      const params: IActionItem = {
        NoteText: value.combinedNote,
        ModelID: value.model.ModelID,
        ModelActionItemPriorityTypeId: value.priority,
        Favorite: value.favorite,
      };
      this.store.dispatch(
        actions.saveNote({ models, params, actionItemType: value.state })
      );
    } else if (value.state === 'savemaintenance') {
      const params: IActionItem = {
        NoteText: value.combinedNote,
        ModelID: value.model.ModelID,
        ModelActionItemPriorityTypeId: value.priority,
        Favorite: value.favorite,
      };
      this.store.dispatch(
        actions.saveMaintenance({ models, params, actionItemType: value.state })
      );
    } else if (value.state === 'savediagnose') {
      const params: IActionItem = {
        NoteText: value.combinedNote,
        ModelID: value.model.ModelID,
        ModelActionItemPriorityTypeId: value.priority,
        Favorite: value.favorite,
      };
      this.store.dispatch(
        actions.saveDiagnose({ models, params, actionItemType: value.state })
      );
    }
  }

  savePDTrendsMappedToNDModel(maps: INDModelPDTrendMap[]) {
    this.store.dispatch(actions.savePDTrendsMappedToNDModel({ maps }));
  }

  showRelatedModels(assetGuid: string) {
    this.store.dispatch(actions.showRelatedModels({ assetGuid }));
  }

  reloadSummaryData() {
    this.store.dispatch(actions.reloadSummaryData());
  }

  showRelatedIssues(assetGuid: string) {
    this.store.dispatch(actions.showRelatedIssues({ assetGuid }));
  }

  loadAIAdvice(
    application: string,
    assetType: string,
    parameterType: string,
    parameterModifier: string
  ) {
    this.store.dispatch(
      actions.loadAIAdvice({
        application,
        assetType,
        parameterType,
        parameterModifier,
      })
    );
  }
}
