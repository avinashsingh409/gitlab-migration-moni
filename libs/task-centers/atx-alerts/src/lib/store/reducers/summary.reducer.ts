import isEqual from 'lodash/isEqual';
import { Update } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import produce from 'immer';

import {
  ITreeState,
  ITreeStateChange,
  alterAssetTreeState,
  setPermissions,
  getDefaultTree,
  createTreeBuilder,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import { IUpdateLimitsData, resetAxes, updateAxes } from '@atonix/atx-chart';
import {
  ITimeSliderStateChange,
  alterTimeSliderState,
  getDefaultTimeSlider,
} from '@atonix/atx-time-slider';

import * as actions from '../actions/summary.actions';
import {
  INDModelSummary,
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
  IListState,
  ISavedModelList,
  ICorrelationData,
} from '@atonix/shared/api';
import {
  ISummaryState,
  initialSummaryState,
  savedModelListAdapter,
} from '../state/summary-state';
import {
  initialModelContextState,
  initialModelTrendState,
  initialModelHistoryState,
  YAxisMinMax,
  yAxisAdapter,
} from '../state/chart-state';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import * as moment from 'moment';
import {
  getTrendTotalSeries,
  IAssetMeasurementsSet,
  IAtxTreeRetrieval,
  ICriteria,
  IGSMeasurement,
  INDModelPDTrendMap,
  IProcessedTrend,
  isNil,
  ITreePermissions,
} from '@atonix/atx-core';

const _summaryReducer = createReducer(
  initialSummaryState,
  on(actions.permissionsRequest, getPermissions),
  on(actions.permissionsRequestSuccess, getPermissionsSuccess),
  on(actions.permissionsRequestFailure, getPermissionsFailure),
  on(actions.treeStateChange, treeStateChange),
  on(actions.assetTreeDataRequestFailure, treeDataFailed),
  on(actions.treeSizeChange, treeSizeChange),
  on(actions.listStateChange, listStateChanged),
  on(actions.listSaveSuccess, saveListSuccess),
  on(actions.listsLoadSuccess, loadListsSuccess),
  on(actions.listDeleteSuccess, deleteListSuccess),
  on(actions.listLoad, loadList),
  on(actions.loadDefaultList, loadDefaultList),
  on(actions.initializeTimeSlider, initializeTimeSliderState),
  on(actions.timeSliderStateChange, timeSliderStateChange),
  on(actions.loadModelContextCharts, loadModelContextCharts),
  on(actions.loadModelContextChartsSuccess, loadModelContextChartsSuccess),
  on(actions.loadModelContextChartsFailure, loadModelContextChartsFailure),
  on(actions.selectModelContextTrend, selectModelContextTrend),
  on(actions.loadModelContext, loadModelContext),
  on(actions.loadModelContextSuccess, loadModelContextSuccess),
  on(actions.loadModelContextFailure, loadModelContextFailure),
  on(actions.loadModelTrend, loadModelTrend),
  on(actions.loadModelTrendSuccess, loadModelTrendSuccess),
  on(actions.loadModelTrendFailure, loadModelTrendFailure),
  on(actions.reloadModelAction, loadModelAction),
  on(actions.loadModelAction, loadModelAction),
  on(actions.loadModelActionSuccess, loadModelActionSuccess),
  on(actions.loadModelActionFailure, loadModelActionFailure),
  on(actions.selectModel, selectModel),
  on(actions.expandButtonClick, expandButtonClick),
  on(actions.changeLabelsClicked, changeLabelsClicked),
  on(actions.reloadFlyOutModel, reloadFlyOutModel),
  on(actions.reloadFlyOutModelSuccess, reloadFlyOutModelSuccess),
  on(actions.reloadFlyOutModelFailure, reloadFlyOutModelFailure),
  on(actions.setWatch, setWatch),
  on(actions.setWatchSuccess, setWatchSuccess),
  on(actions.setWatchFailure, setWatchFailure),
  on(actions.clearWatch, clearWatch),
  on(actions.clearWatchSuccess, clearWatchSuccess),
  on(actions.clearWatchFailure, clearWatchFailure),
  on(actions.clearAlert, clearAlert),
  on(actions.clearAlertSuccess, clearAlertSuccess),
  on(actions.clearAlertFailure, clearAlertFailure),
  on(actions.setCustomWatch, setCustomWatch),
  on(actions.setCustomWatchSuccess, setCustomWatchSuccess),
  on(actions.setCustomWatchFailure, setCustomWatchFailure),
  on(actions.saveDiagnose, saveDiagnose),
  on(actions.saveDiagnoseSuccess, saveDiagnoseSuccess),
  on(actions.saveDiagnoseFailure, saveDiagnoseFailure),
  on(actions.saveMaintenance, saveMaintenance),
  on(actions.saveMaintenanceSuccess, saveMaintenanceSuccess),
  on(actions.saveMaintenanceFailure, saveMaintenanceFailure),
  on(actions.saveNote, saveNote),
  on(actions.saveNoteSuccess, saveNoteSuccess),
  on(actions.saveNoteFailure, saveNoteFailure),
  on(actions.clearDiagnose, clearDiagnose),
  on(actions.clearDiagnoseSuccess, clearDiagnoseSuccess),
  on(actions.clearDiagnoseFailure, clearDiagnoseFailure),
  on(actions.clearMaintenance, clearMaintenance),
  on(actions.clearMaintenanceSuccess, clearMaintenanceSuccess),
  on(actions.clearMaintenanceFailure, clearMaintenanceFailure),
  on(actions.setActionItemFavorite, saveModelHistory),
  on(actions.setActionItemFavoriteSuccess, saveModelHistorySuccess),
  on(actions.setActionItemFavoriteFailure, saveModelHistoryFailure),
  on(actions.setActionItemNote, saveModelHistory),
  on(actions.setActionItemNoteSuccess, saveModelHistorySuccess),
  on(actions.setActionItemNoteFailure, saveModelHistoryFailure),
  on(actions.setModelHistoryFilters, setModelHistoryFilter),
  on(actions.toggleFloatingFilter, toggleFloatingFilter),
  on(actions.multiSelect, multiSelect),
  on(
    actions.modelContextAssetTreeStateChange,
    modelContextAssetTreeStateChange
  ),
  on(actions.modelContextPermissionsRequest, getModelContextPermissions),
  on(
    actions.modelContextPermissionsRequestSuccess,
    getModelContextPermissionsSuccess
  ),
  on(
    actions.modelContextPermissionsRequestFailure,
    getModelContextPermissionsFailure
  ),
  on(actions.filterModelContextDrilldown, filterModelContextDrilldown),
  on(actions.addTrendDialogOpened, addTrendDialogOpened),
  on(actions.selectModelContextAsset, selectModelContextAsset),
  on(actions.filterModelContextAvailable, filterModelContextAvailable),
  on(
    actions.filterModelContextAvailableFailure,
    filterModelContextAvailableFailure
  ),
  on(actions.savePDTrendsMappedToNDModel, savePDTrendsMappedToNDModel),
  on(
    actions.savePDTrendsMappedToNDModelSuccess,
    savePDTrendsMappedToNDModelSuccess
  ),
  on(
    actions.savePDTrendsMappedToNDModelFailure,
    savePDTrendsMappedToNDModelFailure
  ),
  on(actions.reloadSummaryData, reloadData),
  on(actions.updateLimitsModelContextChart, updateLimitsModelContextChart),
  on(actions.resetLimitsModelContextChart, resetLimitsModelContextChart),
  on(
    actions.legendItemToggleModelContextChart,
    legendItemToggleModelContextChart
  ),
  on(actions.updateLimitsModelTrend, updateLimitsModelTrend),
  on(actions.resetLimitsModelTrend, resetLimitsModelTrend),
  on(actions.loadAIAdvice, loadAIAdvice),
  on(actions.loadAIAdviceSuccess, loadAIAdviceSuccess),
  on(actions.loadAIAdviceFailure, loadAIAdviceFailure)
);

export function loadAIAdvice(state: ISummaryState) {
  return { ...state, AIResponseLoading: true };
}

export function loadAIAdviceSuccess(
  state: ISummaryState,
  payload: { response: string }
) {
  return { ...state, AIResponse: payload.response, AIResponseLoading: false };
}

export function loadAIAdviceFailure(
  state: ISummaryState,
  payload: { error: Error }
) {
  return { ...state, AIResponseLoading: false };
}

// Asset Tree
export function getPermissions(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsSuccess(
  state: ISummaryState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(state.AssetTreeState.treeConfiguration, {
      canView: payload.canView,
      canEdit: false,
      canDelete: false,
      canAdd: false,
    }),
    treeNodes: { ...state.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsFailure(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function treeStateChange(
  state: ISummaryState,
  payload: ITreeStateChange
) {
  const newPayload = treeRetrievedStateChange(payload);
  return {
    ...state,
    AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
  };
}

export function treeDataFailed(state: ISummaryState, payload: Error) {
  return { ...state };
}

export function treeSizeChange(
  state: ISummaryState,
  payload: { value: number }
) {
  return {
    ...state,
    LeftTraySize: payload.value,
  };
}

// Saved list configurations
export function listStateChanged(
  state: ISummaryState,
  payload: { listState: IListState }
) {
  return filterChangeSoUncheck(
    { ...state, DisplayedListState: payload.listState, SavedListState: null },
    payload
  );
}

export function saveListSuccess(
  state: ISummaryState,
  payload: { list: ISavedModelList }
) {
  return {
    ...state,
    ModelLists: savedModelListAdapter.upsertOne(payload.list, state.ModelLists),
  };
}

export function loadListsSuccess(
  state: ISummaryState,
  payload: { lists: ISavedModelList[] }
) {
  return {
    ...state,
    ModelLists: savedModelListAdapter.addMany(payload.lists, state.ModelLists),
  };
}

export function deleteListSuccess(
  state: ISummaryState,
  payload: { id: number }
) {
  return {
    ...state,
    ModelLists: savedModelListAdapter.removeOne(payload.id, state.ModelLists),
  };
}

export function getDefaultListState(): IListState {
  return {
    columns: null,
    sort: [],
    groups: [],
    filter: {
      Alert: {
        values: ['true'],
        filterType: 'set',
      },
      ActionWatch: {
        values: ['false'],
        filterType: 'set',
      },
      Ignore: {
        values: ['false'],
        filterType: 'set',
      },
    },
  };
}

export function loadDefaultList(state: ISummaryState): ISummaryState {
  const updates: Update<ISavedModelList>[] = (
    state.ModelLists.ids as number[]
  ).map((id) => {
    return { id, changes: { checked: false } };
  });

  const ModelLists = savedModelListAdapter.updateMany(
    updates,
    state.ModelLists
  );

  return {
    ...state,
    SavedListState: getDefaultListState(),
    ModelLists,
  };
}

export function loadList(
  summaryState: ISummaryState,
  payload: { id: number }
): ISummaryState {
  const updates: Update<ISavedModelList>[] = (
    summaryState.ModelLists.ids as number[]
  ).map((id) => {
    return { id, changes: { checked: id === payload.id } };
  });

  const ModelLists = savedModelListAdapter.updateMany(
    updates,
    summaryState.ModelLists
  );

  const displayedModelList = {
    ...summaryState.DisplayedListState,
    filter: {
      ...summaryState.DisplayedListState.filter,
      AssetID: null,
    },
  };

  const iteratedModelList = {
    ...summaryState.ModelLists.entities[payload.id].state,
    filter: {
      ...summaryState.ModelLists.entities[payload.id].state.filter,
      AssetID: null,
    },
  };

  let newColumns: any[];
  if (
    iteratedModelList?.columns?.length < displayedModelList?.columns?.length
  ) {
    const displayedCol: any[] = displayedModelList.columns;
    const iteratedCol: any[] = iteratedModelList.columns;
    newColumns = displayedCol.filter(
      (x) => !iteratedCol.some((y) => y.colId === x.colId)
    );

    if (
      !isNil(newColumns) &&
      Array.isArray(newColumns) &&
      newColumns.length > 0
    ) {
      for (const index in newColumns) {
        if (newColumns[index].colId === 'Criticality') {
          const index = summaryState.ModelLists.entities[
            payload.id
          ].state.columns.findIndex((x) => x.colId === 'ModelCriticality');

          const newState = produce(
            summaryState.ModelLists.entities[payload.id].state,
            (newState) => {
              newState.columns.splice(index + 1, 0, {
                aggFunc: null,
                colId: 'Criticality',
                flex: null,
                hide: true,
                pinned: null,
                pivot: true,
                pivotIndex: null,
                rowGroup: false,
                rowGroupIndex: null,
                sort: null,
                sortIndex: null,
                width: 150,
              });
            }
          );

          const newModelLists = produce(
            summaryState.ModelLists,
            (newModelListsState) => {
              newModelListsState.entities[payload.id].state = newState;
            }
          );

          return {
            ...summaryState,
            SavedListState: newState,
            ModelLists: newModelLists,
          };
        }
      }
    }
  }

  return {
    ...summaryState,
    SavedListState: {
      ...summaryState.ModelLists.entities[payload.id].state,
    },
    ModelLists,
  };
}

function filterChangeSoUncheck(
  state: ISummaryState,
  _: { listState: IListState }
) {
  const listOfModels = [] as Update<ISavedModelList>[];
  // displayedModelList is the state of the model list that is currently displayed
  // Ignore the AssetID filter
  const displayedModelList = {
    ...state.DisplayedListState,
    filter: { ...state.DisplayedListState.filter, AssetID: null },
  };
  for (const key of state.ModelLists.ids) {
    // iteratedModelList is the state of the model list in the loop
    // Ignore the AssetID filter
    const iteratedModelList = {
      ...state.ModelLists.entities[key].state,
      filter: { ...state.ModelLists.entities[key].state.filter, AssetID: null },
    };
    if (
      iteratedModelList?.filter?.NotePriority?.values &&
      displayedModelList?.filter?.NotePriority?.values
    ) {
      if (
        isEqual(
          {
            ...iteratedModelList,
            filter: {
              ...iteratedModelList.filter,
              Priority: {
                ...iteratedModelList.filter.NotePriority,
                values: [
                  ...iteratedModelList.filter.NotePriority.values,
                ].sort(),
              },
            },
          },
          {
            ...displayedModelList,
            filter: {
              ...displayedModelList.filter,
              Priority: {
                ...displayedModelList.filter.NotePriority,
                values: [
                  ...displayedModelList.filter.NotePriority.values,
                ].sort(),
              },
            },
          }
        )
      ) {
        listOfModels.push({
          changes: { checked: true },
          id: key,
        } as Update<ISavedModelList>);
      } else {
        listOfModels.push({
          changes: { checked: false },
          id: key,
        } as Update<ISavedModelList>);
      }
    } else {
      if (isEqual(iteratedModelList, displayedModelList)) {
        listOfModels.push({
          changes: { checked: true },
          id: key,
        } as Update<ISavedModelList>);
      } else {
        listOfModels.push({
          changes: { checked: false },
          id: key,
        } as Update<ISavedModelList>);
      }
    }
  }
  return {
    ...state,
    ModelLists: savedModelListAdapter.updateMany(
      listOfModels,
      state.ModelLists
    ),
  };
}

// Time Slider
export function initializeTimeSliderState(
  state: ISummaryState,
  payload: { startDate: Date; endDate: Date }
) {
  return {
    ...state,
    TimeSliderState: getDefaultTimeSlider({
      dateIndicator: 'Range',
      startDate: payload.startDate,
      endDate: payload.endDate,
    }),
  };
}

export function timeSliderStateChange(
  state: ISummaryState,
  payload: ITimeSliderStateChange
) {
  return {
    ...state,
    TimeSliderState: alterTimeSliderState(state.TimeSliderState, payload),
  };
}

// Model Context
export function loadModelContextCharts(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends: null,
      selectedTrendID: null,
      selectedTrend: null,
      loading: true,
      availableCharts: null,
    },
  };
}

export function addTrendDialogOpened(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      savedTrend: false,
      modelContextAssetTree: {
        treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
        treeNodes: createTreeBuilder(),
        hasDefaultSelectedAsset: true,
      },
    },
  };
}
export function selectModelContextAsset(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      availableCharts: null,
      loadingList: true,
    },
  };
}

export function filterModelContextAvailable(
  state: ISummaryState,
  payload: {
    existingMaps: { [id: string]: INDModelPDTrendMap };
    maps: INDModelPDTrendMap[];
  }
) {
  const processedTrends = payload.maps?.map((m) => {
    const result = {
      id: String(m.MappedPDTrend?.PDTrendID),
      label: m.MappedPDTrend?.Title,
      trendDefinition: m.MappedPDTrend,
      totalSeries: getTrendTotalSeries(m?.MappedPDTrend),
      labelIndex: null,
    } as IProcessedTrend;
    return result;
  });
  const trendsWithSeries: IProcessedTrend[] = [];
  const mapsWithTrendsWithSeries: INDModelPDTrendMap[] = [];
  const availableCharts: { [id: string]: INDModelPDTrendMap } = {};

  for (const trend of processedTrends) {
    if (trend.totalSeries > 0 || trend.trendDefinition.TrendSource) {
      trendsWithSeries.push(trend);
    }
  }

  for (const pt of trendsWithSeries) {
    for (const m of payload.maps) {
      if (pt.trendDefinition.PDTrendID === m.PDTrendID) {
        mapsWithTrendsWithSeries.push(m);
      }
    }
  }
  for (const m of mapsWithTrendsWithSeries) {
    if (m.PDTrendID > -1 || m.NDModelPDTrendMapID > -1) {
      if (allowPick(payload.existingMaps, m)) {
        availableCharts[m.PDTrendID] = m;
      }
    }
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      availableCharts,
      loadingList: false,
    },
  };
}

export function filterModelContextAvailableFailure(
  state: ISummaryState,
  payload: Error
) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      availableCharts: null,
      loadingList: false,
    },
  };
}

// This will filter if the map is already in the drilldown list
export function allowPick(existingMaps, maps) {
  let allow = true;
  for (const key in existingMaps) {
    if (existingMaps) {
      const value = existingMaps[key];
      if (
        maps.AssetID === value.AssetID &&
        maps.PDTrendID === value.PDTrendID
      ) {
        allow = false;
        break;
      }
    }
  }

  return allow;
}

export function filterModelContextDrilldown(
  state: ISummaryState,
  payload: { maps: INDModelPDTrendMap[] }
) {
  const modelTrendMapDictionary: { [id: string]: INDModelPDTrendMap } = {};

  if (payload.maps && payload.maps.length > 0) {
    for (const m of payload.maps) {
      if (m.NDModelPDTrendMapID > -1) {
        modelTrendMapDictionary[m.NDModelPDTrendMapID] = m;
      } else if (m.PDTrendID > -1) {
        modelTrendMapDictionary[m.PDTrendID] = m;
      }
    }
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelTrendMapDictionary,
      availableCharts: null,
      loadingList: true,
    },
  };
}

export function loadModelContextChartsSuccess(
  state: ISummaryState,
  payload: { trends: IProcessedTrend[] }
) {
  let trends = (payload.trends ?? []).filter(
    (n) =>
      n.totalSeries > 0 ||
      n.trendDefinition.TrendSource ||
      (n.trendDefinition.BandAxisBarsResolved &&
        n.trendDefinition.BandAxisBarsResolved.length > 0) // filter trends
  );

  trends = [...trends].sort((a, b) => {
    const aLabel = a?.label?.toLowerCase();
    const bLabel = b?.label?.toLowerCase();
    if (aLabel !== bLabel) {
      if (a.label.toLowerCase() === 'model context') {
        return -1;
      } else if (b.label.toLowerCase() === 'model context') {
        return 1;
      }
    }
  });

  trends = trends.map((trend) => {
    const trendDefinition = { ...trend.trendDefinition, ShowSelected: true };
    // Turn off the active selection in the event that pins are present.
    // This might be use in the future when a trend is opened on DE.
    if (trendDefinition?.Pins && trendDefinition?.Pins.length > 0) {
      trendDefinition.ShowSelected = false;
    }

    const sortedSeries = [...trendDefinition.Series].sort((a, b) => {
      return a.DisplayOrder - b.DisplayOrder;
    });

    const Series = [...sortedSeries].map((series, index) => {
      return { ...series, DisplayOrder: index, visible: true };
    });

    const Axes = [...trendDefinition.Axes].map((axis) => {
      const newAxis = { ...axis };
      newAxis.OriginalMin = newAxis.Min;
      newAxis.OriginalMax = newAxis.Max;
      return newAxis;
    });

    return {
      ...trend,
      trendDefinition: {
        ...trendDefinition,
        Series,
        Axes,
      },
    };
  });

  let selectedTrendID: string = null;
  let selectedTrend = null;
  if (trends && trends.length > 0) {
    selectedTrendID = trends[0].id;
    selectedTrend = trends[0];
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends,
      selectedTrendID,
      selectedTrend,
      loading: false,
    },
  };
}

export function savePDTrendsMappedToNDModel(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      loadingList: true,
    },
  };
}

export function savePDTrendsMappedToNDModelSuccess(
  state: ISummaryState,
  payload: { maps: INDModelPDTrendMap[] }
) {
  const modelTrendMapDictionary: { [id: string]: INDModelPDTrendMap } = {};

  if (payload.maps && payload.maps.length > 0) {
    for (const m of payload.maps) {
      if (m.NDModelPDTrendMapID > -1) {
        modelTrendMapDictionary[m.NDModelPDTrendMapID] = m;
      } else if (m.PDTrendID > -1) {
        modelTrendMapDictionary[m.PDTrendID] = m;
      }
    }
  }
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelTrendMapDictionary,
      savedTrend: true,
      loadingList: false,
    },
  };
}

export function savePDTrendsMappedToNDModelFailure(
  state: ISummaryState,
  paylod: Error
) {
  return { ...state };
}

export function loadModelContextChartsFailure(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends: [],
      selectedTrendID: null,
      selectedTrend: null,
      loading: false,
    },
  };
}

export function selectModelContextTrend(
  state: ISummaryState,
  payload: { trendID: string }
) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      selectedTrendID: payload.trendID,
      selectedTrend: null,
    },
  };
}

export function loadModelContext(
  state: ISummaryState,
  payload: { startDate: Date; endDate: Date; trend: IProcessedTrend }
) {
  const trendID = payload.trend?.id ?? state.ModelContextState.selectedTrendID;

  let selectedTrend = state?.ModelContextState?.trends?.find(
    (n) => n.id === trendID
  );
  let trends = state?.ModelContextState?.trends;
  if (selectedTrend) {
    selectedTrend = {
      ...selectedTrend,
      startDate: payload.startDate || state.TimeSliderState?.startDate || null,
      endDate: payload.endDate || state.TimeSliderState?.endDate || null,
    };

    trends = trends.map((n) => {
      if (n.id === selectedTrend.id) {
        return { ...selectedTrend };
      }
      return n;
    });

    return {
      ...state,
      ModelContextState: {
        ...state.ModelContextState,
        trends,
        correlationTrend: null,
        loading: true,
      },
    };
  }

  return { ...state };
}

export function loadModelContextSuccess(
  state: ISummaryState,
  payload: {
    trendID: string;
    measurementsSet: IAssetMeasurementsSet;
    correlationTrend: ICorrelationData[];
  }
) {
  const selectedTrend = state?.ModelContextState?.trends?.find(
    (n) => n.id === payload.trendID
  );

  let trends = state?.ModelContextState?.trends;
  if (selectedTrend) {
    let isGroupedSeries = false;
    if (selectedTrend?.trendDefinition?.BandAxis) {
      isGroupedSeries = true;
    }
    const newTrend = produce(selectedTrend, (draftState) => {
      if (selectedTrend?.trendDefinition?.Pins) {
        // this is to fix the startTime / end time pin bug
        // the PDTrends endpoint gives the wrong times for pins
        // TagsDataFiltered gives the correct ones
        draftState.trendDefinition.Pins = payload.measurementsSet.Pins;
      }
      draftState.measurements = payload.measurementsSet;
      draftState.measurements.Bands;
      draftState.startDate = state.TimeSliderState?.startDate || null;
      draftState.endDate = state.TimeSliderState?.endDate || null;

      if (isGroupedSeries) {
        draftState.groupSeriesMeasurements = draftState.groupSeriesMeasurements
          ? draftState.groupSeriesMeasurements
          : [];
        // There are 4 types of series group: TIME, PINS, CUSTOM, and ASSET
        // TIME and PINS values are from BandAxisMeasurements
        // CUSTOM and ASSET values are from BandAxisBars
        // groupSeriesMeasurements array holds one of those group type data
        // so that it is more easy to manipulate when changing between different types
        draftState.groupSeriesMeasurements =
          draftState.trendDefinition.BandAxisMeasurements?.filter(
            (bandAxisMeasurements) =>
              bandAxisMeasurements.DisplayOrder === null ||
              bandAxisMeasurements.DisplayOrder === 0
          ) || []; //if not defined or  0, put at top of list and draw first

        if (draftState.trendDefinition.BandAxisMeasurements?.length > 0) {
          for (const bandAxisMeasurements of draftState.trendDefinition
            .BandAxisMeasurements) {
            if (
              bandAxisMeasurements.DisplayOrder &&
              bandAxisMeasurements.DisplayOrder !== 0
            ) {
              draftState.groupSeriesMeasurements.push({
                ...bandAxisMeasurements,
              });
            }
          }
        }

        draftState.groupedSeriesType = 3;
        if (draftState.trendDefinition.BandAxisBars?.length > 0) {
          for (const bandAxisBars of draftState.trendDefinition.BandAxisBars) {
            if (bandAxisBars.Measurements) {
              for (const m of bandAxisBars.Measurements) {
                (<IGSMeasurement>m).AssetID = bandAxisBars.AssetID;
                (<IGSMeasurement>m).BarName = bandAxisBars.Label;
                m.Params1 = +m.Params1;
                m.Params2 = +m.Params2;
                m.Params3 = +m.Params3;
                m.Params4 = +m.Params4;
                m.Params5 = +m.Params5;
                draftState.groupSeriesMeasurements.push({
                  ...m,
                });
              }
            }
          }

          if (
            draftState.trendDefinition.BandAxisBars.every(
              (c) => !!c.TimeDivision
            )
          ) {
            draftState.groupedSeriesType = 2;
          }
          if (
            draftState.trendDefinition.BandAxisBars.every(
              (c) => c.TimeDivision === 'pins'
            )
          ) {
            draftState.groupedSeriesType = 1;
          }
          if (draftState.groupedSeriesType === 2) {
            draftState.groupedSeriesSubType = 'week';
            if (
              draftState.trendDefinition.BandAxisBars &&
              draftState.trendDefinition.BandAxisBars.length > 0
            ) {
              if (draftState.trendDefinition.BandAxisBars[0].TimeDivision) {
                draftState.groupedSeriesSubType =
                  draftState.trendDefinition.BandAxisBars[0].TimeDivision;
              }
            }
          }
        }

        if (draftState.groupSeriesMeasurements?.length > 0) {
          draftState.groupSeriesMeasurements.sort((a, b) => {
            return a.DisplayOrder - b.DisplayOrder;
          });

          draftState.groupSeriesMeasurements =
            draftState.groupSeriesMeasurements.map((series, index) => {
              const newSeries = { ...series };
              newSeries.DisplayOrder = index + 1;
              return newSeries;
            });
        }
      }
    });

    trends = trends.map((n) => {
      if (n.id === newTrend.id) {
        return { ...newTrend };
      }
      return n;
    });

    return {
      ...state,
      ModelContextState: {
        ...state.ModelContextState,
        trends,
        correlationTrend: payload.correlationTrend,
        loading: false,
      },
    };
  }
  return { ...state };
}

export function loadModelContextFailure(state: ISummaryState, payload: Error) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      loading: false,
    },
  };
}

export function getModelContextPermissions(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.ModelContextState.modelContextAssetTree.treeConfiguration,
      null
    ),
    treeNodes: { ...state.ModelContextState.modelContextAssetTree.treeNodes },
    hasDefaultSelectedAsset:
      state.ModelContextState.modelContextAssetTree.hasDefaultSelectedAsset,
  };
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelContextAssetTree: treeState,
    },
  };
}

export function getModelContextPermissionsSuccess(
  state: ISummaryState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.ModelContextState.modelContextAssetTree.treeConfiguration,
      payload
    ),
    treeNodes: { ...state.ModelContextState.modelContextAssetTree.treeNodes },
    hasDefaultSelectedAsset:
      state.ModelContextState.modelContextAssetTree.hasDefaultSelectedAsset,
  };
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelContextAssetTree: treeState,
    },
  };
}

export function getModelContextPermissionsFailure(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.ModelContextState.modelContextAssetTree.treeConfiguration,
      null
    ),
    treeNodes: { ...state.ModelContextState.modelContextAssetTree.treeNodes },
    hasDefaultSelectedAsset:
      state.ModelContextState.modelContextAssetTree.hasDefaultSelectedAsset,
  };
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelContextAssetTree: treeState,
    },
  };
}

export function modelContextAssetTreeStateChange(
  state: ISummaryState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      modelContextAssetTree: alterAssetTreeState(
        state.ModelContextState.modelContextAssetTree,
        payload
      ),
    },
  };
}

export function updateLimitsModelContextChart(
  state: ISummaryState,
  payload: { limitsData: IUpdateLimitsData }
) {
  // eslint-disable-next-line no-unsafe-optional-chaining
  const newTrends = [...state?.ModelContextState?.trends];

  const idx = newTrends.findIndex(
    (trend) => trend.id === state?.ModelContextState?.selectedTrendID
  );
  if (idx >= 0) {
    newTrends.splice(idx, 1, {
      ...newTrends[idx],
      trendDefinition: {
        ...newTrends[idx].trendDefinition,
        Axes: updateAxes(newTrends[idx].trendDefinition, payload.limitsData),
      },
    });
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends: newTrends,
    },
  };
}

export function resetLimitsModelContextChart(
  state: ISummaryState,
  payload: { axisIndex: number }
) {
  // eslint-disable-next-line no-unsafe-optional-chaining
  const newTrends = [...state?.ModelContextState?.trends];

  const idx = newTrends.findIndex(
    (trend) => trend.id === state?.ModelContextState?.selectedTrendID
  );
  if (idx >= 0) {
    newTrends.splice(idx, 1, {
      ...newTrends[idx],
      trendDefinition: {
        ...newTrends[idx].trendDefinition,
        Axes: resetAxes(newTrends[idx].trendDefinition, payload.axisIndex),
      },
    });
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends: newTrends,
    },
  };
}

//this doesn't work when seriesIndex is a correlation trend index, but we still
//dispatch this action when toggling correlation trends
export function legendItemToggleModelContextChart(
  state: ISummaryState,
  payload: { seriesIndex: number }
) {
  // eslint-disable-next-line no-unsafe-optional-chaining
  const newTrends = [...state?.ModelContextState?.trends];

  const idx = newTrends.findIndex(
    (trend) => trend.id === state?.ModelContextState?.selectedTrendID
  );

  if (idx >= 0) {
    const newTrendDefinition = produce(
      { ...newTrends[idx]?.trendDefinition },
      (trendDefinition) => {
        if (trendDefinition && trendDefinition?.Series[payload.seriesIndex]) {
          if (trendDefinition?.Series[payload.seriesIndex].visible) {
            trendDefinition.Series[payload.seriesIndex].visible = false;
          } else {
            trendDefinition.Series[payload.seriesIndex].visible = true;
          }
        }
      }
    );

    newTrends.splice(idx, 1, {
      ...newTrends[idx],
      trendDefinition: newTrendDefinition,
    });
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends: newTrends,
    },
  };
}

// Model Trend
export function loadModelTrend(state: ISummaryState) {
  return {
    ...state,
    ModelTrendState: {
      ...state.ModelTrendState,
      // trendData: null, //Don't reset trendData here so we can keep Trend Chart Y-Axis Min/Max
      loading: true,
    },
  };
}

export function loadModelTrendSuccess(
  state: ISummaryState,
  payload: { trend: IAssetModelChartingData }
): ISummaryState {
  return {
    ...state,
    ModelTrendState: {
      ...state.ModelTrendState,
      trendData: {
        ...payload.trend,
        start: state.TimeSliderState?.startDate,
        end: state.TimeSliderState?.endDate,
        Min:
          payload.trend.Min !== null
            ? payload.trend.Min
            : state.ModelTrendState.trendData?.Min, //Keep Model Y-Axis Min/Max on reload
        Max:
          payload.trend.Max !== null
            ? payload.trend.Max
            : state.ModelTrendState.trendData?.Max,
      },
      loading: false,
    },
  };
}

export function loadModelTrendFailure(state: ISummaryState) {
  return {
    ...state,
    ModelTrendState: {
      ...state.ModelTrendState,
      trendData: null,
      loading: false,
    },
  };
}

export function updateLimitsModelTrend(
  state: ISummaryState,
  payload: { limitsData: IUpdateLimitsData }
) {
  const modelId = state.SelectedModel.at(0).ModelID; //selected ModelId
  const yAxis: YAxisMinMax = {
    id: modelId,
    Min: payload.limitsData.Min,
    Max: payload.limitsData.Max,
  };

  const trendYAxisState = state.ModelTrendState.trendYAxis;

  return {
    ...state,
    ModelTrendState: {
      ...state.ModelTrendState,
      trendData: {
        ...state.ModelTrendState.trendData,
        Min: payload.limitsData.Min,
        Max: payload.limitsData.Max,
      },
      trendYAxis: yAxisAdapter.setOne(yAxis, trendYAxisState),
    },
  };
}

export function resetLimitsModelTrend(
  state: ISummaryState,
  payload: { axisIndex: number }
) {
  const modelId = state.SelectedModel.at(0).ModelID; //selected ModelId
  const trendYAxisState = state.ModelTrendState.trendYAxis;

  return {
    ...state,
    ModelTrendState: {
      ...state.ModelTrendState,
      trendData: {
        ...state.ModelTrendState.trendData,
        Min: null,
        Max: null,
      },
      trendYAxis: yAxisAdapter.removeOne(modelId, trendYAxisState),
    },
  };
}

// Model Action
export function loadModelAction(state: ISummaryState) {
  return {
    ...state,
    ModelActionState: {
      ...state.ModelActionState,
      trendData: null,
      loading: true,
    },
  };
}

export function loadModelActionSuccess(
  state: ISummaryState,
  payload: { data: INDModelActionItemAnalysis[] }
): ISummaryState {
  return {
    ...state,
    ModelActionState: {
      ...state.ModelActionState,
      trendData: [...payload.data],
      loading: false,
    },
  };
}

export function loadModelActionFailure(state: ISummaryState) {
  return {
    ...state,
    ModelActionState: {
      ...state.ModelActionState,
      trendData: null,
      loading: false,
    },
  };
}

// Model History
export function saveModelHistory(state: ISummaryState) {
  return {
    ...state,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function saveModelHistorySuccess(state: ISummaryState): ISummaryState {
  return {
    ...state,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function saveModelHistoryFailure(state: ISummaryState) {
  return {
    ...state,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function setModelHistoryFilter(
  state: ISummaryState,
  payload: { filter: IModelHistoryFilters }
) {
  return {
    ...state,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      filters: { ...payload.filter },
    },
  };
}

// Model selection
export function selectModel(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
) {
  return {
    ...state,
    SelectedModel: payload.model,
    ModelTrendState: {
      ...state.ModelTrendState,
      trendData: null, //Reset trend data here instead of loadModelTrend to only reset Y-axis when changing Models
    },
  };
}

export function multiSelect(state: ISummaryState) {
  return {
    ...state,
    ModelContextState: initialModelContextState,
    ModelTrendState: initialModelTrendState,
    ModelHistoryState: initialModelHistoryState,
  };
}

// Change Layout
export function expandButtonClick(
  state: ISummaryState,
  payload: { source: 'List' | 'Trend' }
) {
  let SummarySplit = state.SummarySplit;
  if (SummarySplit === payload.source) {
    SummarySplit = 'Split';
  } else {
    SummarySplit = payload.source;
  }
  return { ...state, SummarySplit };
}

// Model selection
export function changeLabelsClicked(
  state: ISummaryState,
  payload: { index: number }
) {
  let selectedTrend = state?.ModelContextState?.trends?.find(
    (n) => n.id === state.ModelContextState.selectedTrendID
  );
  let trends = state?.ModelContextState?.trends;

  if (selectedTrend) {
    selectedTrend = {
      ...selectedTrend,
      labelIndex: payload.index,
    };

    trends = trends.map((n) => {
      if (n.id === selectedTrend.id) {
        return selectedTrend;
      }
      return n;
    });
  }

  return {
    ...state,
    ModelContextState: {
      ...state.ModelContextState,
      trends,
    },
  };
}

// Get Flyout Model

export function reloadFlyOutModel(state: ISummaryState) {
  return {
    ...state,
    Refreshing: true,
  };
}

export function reloadFlyOutModelSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  if (payload.model) {
    state = mergeModelsIntoSelectedModel(state, payload.model);
  }
  return {
    ...state,
    Refreshing: false,
  };
}

export function reloadFlyOutModelFailure(state: ISummaryState) {
  return {
    ...state,
    Refreshing: false,
  };
}

export function setWatch(state: ISummaryState) {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function setWatchSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: true,
      action: 'watch',
    },
  };
}

export function setWatchFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
  };
}

export function clearWatch(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function clearWatchSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: false,
      action: 'watch',
    },
  };
}

export function clearWatchFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
  };
}

export function clearAlert(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function clearAlertSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: false,
      action: 'alert',
    },
  };
}

export function clearAlertFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
  };
}

export function setCustomWatch(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function setCustomWatchSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: true,
      action: 'watch',
    },
  };
}

export function setCustomWatchFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
  };
}

export function saveDiagnose(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function saveDiagnoseSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: true,
      action: 'diagnose',
    },
  };
}

export function saveDiagnoseFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function saveMaintenance(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function saveMaintenanceSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: true,
      action: 'maintenance',
    },
  };
}

export function saveMaintenanceFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function saveNote(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function saveNoteSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function saveNoteFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function clearDiagnose(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function clearDiagnoseSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: false,
      action: 'diagnose',
    },
  };
}

export function clearDiagnoseFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function clearMaintenance(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: true,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: true,
    },
  };
}

export function clearMaintenanceSuccess(
  state: ISummaryState,
  payload: { model: INDModelSummary[] }
): ISummaryState {
  state = mergeModelsIntoSelectedModel(state, payload.model);
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
    UpdatedModels: {
      ids: payload.model.map((n) => n.ModelID),
      newValue: false,
      action: 'maintenance',
    },
  };
}

export function clearMaintenanceFailure(state: ISummaryState): ISummaryState {
  return {
    ...state,
    Refreshing: false,
    ModelHistoryState: {
      ...state.ModelHistoryState,
      busy: false,
    },
  };
}

export function toggleFloatingFilter(
  state: ISummaryState,
  payload: { filterOn?: boolean }
): ISummaryState {
  return {
    ...state,
    FloatingFilter: payload.filterOn ?? !state.FloatingFilter,
  };
}

// The idea behind this method is that someone has requested these models be reloaded.  It is possible that the
// selected models have been changed while the models were being loaded.  So we want to update what we can and
// leave the rest intact.
export function mergeModelsIntoSelectedModel(
  state: ISummaryState,
  updatedModels: INDModelSummary[]
): ISummaryState {
  const modelDict: { [key: number]: INDModelSummary } = {};
  for (const model of updatedModels) {
    if (model) {
      modelDict[model.ModelID] = model;
    }
  }
  const selectedModels = state.SelectedModel ?? [];
  const models: INDModelSummary[] = selectedModels.map((n) => {
    return modelDict[n.ModelID] || n;
  });

  return {
    ...state,
    SelectedModel: models,
  };
}

export function reloadData(state: ISummaryState): ISummaryState {
  return {
    ...state,
    ReloadDate: moment().toDate(),
  };
}

export function summaryReducer(state, action) {
  return _summaryReducer(state, action);
}
