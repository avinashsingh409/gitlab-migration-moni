import { ActionReducerMap } from '@ngrx/store';
import { IAlertsState } from '../state/alerts-state';
import { summaryReducer } from './summary.reducer';

export const alertsReducers: ActionReducerMap<IAlertsState, any> = {
  summaryState: summaryReducer,
};
