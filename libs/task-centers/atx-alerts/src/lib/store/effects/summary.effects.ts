/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect, concatLatestFrom } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { forkJoin, iif, of } from 'rxjs';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  tap,
  filter,
  mergeMap,
  concatMap,
} from 'rxjs/operators';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import {
  NavActions,
  updateRouteIdTimeSliderWithoutReloading,
  updateRouterIdWithoutReloading,
} from '@atonix/atx-navigation';
import { getTrendTotalSeries, IProcessedTrend, isNil } from '@atonix/atx-core';
import * as actions from '../actions/summary.actions';
import {
  AlertsCoreService,
  AlertsFrameworkService,
  UIConfigFrameworkService,
  ProcessDataFrameworkService,
  IGridExportRetrieval,
  ISavedModelList,
  AuthorizationFrameworkService,
  AIAdviceService,
} from '@atonix/shared/api';
import {
  getAssetTreeState,
  getDisplayedListState,
  getSelectedAsset,
  selectTimeSelection,
  selectModelContextTrends,
  selectSelectedModelContext,
  selectModelID,
  selectModelExtID,
  selectModelAssetGuid,
  selectSelectedModelContextTrendDef,
  getModelContextAssetTreeState,
  getModelContextDrilldown,
  selectModel,
  getModelContextSelectedAssetTreeNode,
  selectAssetTreeConfigNodes,
  selectNavItems,
  selectModelTypeAbbrev,
  selectDiagnosticDrilldownURL,
  selectModelConfigURL,
  selectModelTrendYAxisEntities,
} from '../selectors/summary.selector';
import { selectAssetID, assetState } from '../selectors/alerts.selector';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LoggerService,
  ToastService,
  catchSwitchMapError,
} from '@atonix/shared/utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AuthSelectors } from '@atonix/shared/state/auth';

@Injectable()
export class SummaryEffects {
  constructor(
    private actions$: Actions,
    private alertsCoreService: AlertsCoreService,
    private alertsFrameworkService: AlertsFrameworkService,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private uiconfigFrameworkService: UIConfigFrameworkService,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private aiAdviceService: AIAdviceService,
    private assetTreeModel: AssetTreeModel,
    private store: Store<any>,
    private snackBarService: ToastService,
    private logger: LoggerService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  initializeSummary$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.summaryInitialize),
      withLatestFrom(
        this.store.select(getSelectedAsset),
        this.store.select(assetState)
      ),
      map(([action, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return {
          asset: action.asset || appAsset?.toString() || newAsset,
          startDate: action.startDate,
          endDate: action.endDate,
        };
      }),
      switchMap((vals) => [
        actions.initializeTimeSlider({
          startDate: vals.startDate,
          endDate: vals.endDate,
        }),
        actions.permissionsRequest(),
        actions.modelContextPermissionsRequest(),
        actions.treeStateChange(selectAsset(vals.asset, false)),
        NavActions.taskCenterLoad({
          taskCenterID: 'alerts',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: true,
          timeSliderOpened: true,
          asset: vals.asset,
        }),
      ])
    )
  );

  selectWorkItem$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.issueSelected),
        tap((payload) => {
          const id = payload.id;
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=${id}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=${id}`;
          }

          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  opModeConfig$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.actionFlyOutOpModeConfigToAsset),
        tap((payload) => {
          let url = `${window.location.protocol}//${window.location.host}/model-config?tab=opmode&id=${payload.asset}&start=${payload.start}&end=${payload.end}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/model-config?tab=opmode&id=${payload.asset}&start=${payload.start}&end=${payload.end}`;
          }
          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  diagnosticDrilldown$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.diagnosticDrilldownToAsset),
        tap((payload) => {
          if (this.appConfig.enableNewDiagnosticDrilldown) {
            const url =
              payload.mtype !== 'External'
                ? this.appConfig.baseSiteURL +
                  '/MD/diagnostic-drilldown?model=' +
                  payload.model +
                  '&start=' +
                  payload.start +
                  '&end=' +
                  payload.end +
                  '&asset=' +
                  payload.asset
                : payload.ddurl;
            window.open(url, '_blank').focus();
          } else {
            //legacy maybe remove
            window
              .open(
                this.appConfig.baseSiteURL +
                  '/Alerts/DiagnosticDrilldown.html#!?model=' +
                  payload.model +
                  '&start=' +
                  payload.start +
                  '&end=' +
                  payload.end +
                  '&asset=' +
                  payload.asset,
                '_blank'
              )
              .focus();
          }
        })
      ),
    { dispatch: false }
  );

  modelConfig$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.actionFlyOutModelConfigToAsset),
        tap((payload) => {
          let url = window.location.href;
          url = url.substring(0, url.indexOf(this.router.url));
          url = `${url}/model-config/m?modelId=${payload.model}&aid=${payload.asset}&start=${payload.start}&end=${payload.end}`;
          window.open(url, '_blank').focus();
        })
      ),
    { dispatch: false }
  );

  newIssue$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.actionFlyOutNewIssueToAsset),
        tap((action) => {
          const assetId = action.assetId;
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=-1&ast=${assetId}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=-1&ast=${assetId}`;
          }

          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  getPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) => actions.permissionsRequestSuccess(permissions)),
          catchError((error) => of(actions.permissionsRequestFailure(error)))
        )
      )
    )
  );

  getModelContextPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.modelContextPermissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) =>
            actions.modelContextPermissionsRequestSuccess(permissions)
          ),
          catchError((error) =>
            of(actions.modelContextPermissionsRequestFailure(error))
          )
        )
      )
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.treeStateChange),
      withLatestFrom(this.store.pipe(select(getAssetTreeState))),
      mergeMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.treeStateChange(n)),
            catchError((error) =>
              of(actions.assetTreeDataRequestFailure(error))
            )
          )
      )
    )
  );

  modelContextTreeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.modelContextAssetTreeStateChange),
      withLatestFrom(this.store.pipe(select(getModelContextAssetTreeState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.modelContextAssetTreeStateChange(n)),
            catchError((error) =>
              of(actions.modelContextAssetTreeDataRequestFailure(error))
            )
          )
      )
    )
  );

  selectAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectAsset),
      switchMap((action) => {
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          actions.updateRoute(),
        ];
      })
    )
  );

  updateRoute$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateRoute),
      withLatestFrom(
        this.store.select(getSelectedAsset),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, id, time]) => {
        const start = time?.startDate?.getTime().toString();
        const end = time?.endDate?.getTime().toString();

        updateRouteIdTimeSliderWithoutReloading(
          id,
          start,
          end,
          this.router,
          this.activatedRoute
        );
        return [
          NavActions.updateNavigationItems({
            urlParams: { id, start, end },
          }),
        ];
      })
    )
  );

  loadLists = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listsLoad),
      switchMap(() =>
        this.uiconfigFrameworkService
          .getModelLists()
          .pipe(map((lists) => actions.listsLoadSuccess({ lists })))
      )
    )
  );

  saveList = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listSave),
      tap((action) => {
        this.logger.savedFilter(null, 'alerts', 'save');
      }),
      withLatestFrom(this.store.pipe(select(getDisplayedListState))),
      map(([newList, oldState]) => {
        const myList: ISavedModelList = {
          id: newList.list.id,
          name: newList.list.name,
          state: oldState,
          checked: true,
        };
        return myList;
      }),
      switchMap((list) =>
        this.uiconfigFrameworkService
          .saveModelList(list)
          .pipe(map((newList) => actions.listSaveSuccess({ list: newList })))
      )
    )
  );

  deleteList = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listDelete),
      tap((action) => {
        this.logger.savedFilter(null, 'alerts', 'delete');
      }),
      map((n) => n.id),
      switchMap((id) =>
        this.uiconfigFrameworkService
          .deleteList(id)
          .pipe(map((result) => actions.listDeleteSuccess({ id })))
      )
    )
  );

  selectedModel$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectModel),
      map((n) => n.model),
      filter((n) => !!n),
      withLatestFrom(this.store.pipe(select(selectTimeSelection))),
      switchMap(([model, timeSelection]) => {
        if (model?.length === 1) {
          const mdl = model[0];
          return [
            actions.loadModelContextCharts({
              assetID: mdl.AssetID.toString(),
              modelExtID: mdl.ModelExtID,
            }),
            actions.loadModelTrend({
              modelId: String(mdl.ModelID ?? ''),
              startDate: timeSelection.startDate,
              endDate: timeSelection.endDate,
            }),
            // actions.loadModelHistory({ modelId: mdl.ModelID.toString() }),
            actions.loadModelAction({
              modelId: String(mdl.ModelID ?? ''),
            }),
            actions.updateModelNavConfig({
              model: mdl.ModelExtID ?? '',
              asset: mdl.AssetGUID ?? '',
              start: String(timeSelection?.startDate?.getTime()),
              end: String(timeSelection?.endDate?.getTime()),
            }),
          ];
        } else {
          return [actions.multiSelect(), actions.updateModelNavConfig(null)];
        }
      })
    )
  );

  selectModelContextAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectModelContextAsset),
      withLatestFrom(
        this.store.pipe(select(getModelContextSelectedAssetTreeNode)),
        this.store.pipe(select(selectModel)),
        this.store.pipe(select(selectTimeSelection)),
        this.store.pipe(select(getModelContextDrilldown))
      ),
      switchMap(
        ([unusedAction, node, selectedModel, timeSelection, existingMaps]) => {
          return this.processDataFrameworkService
            .getPDNDModelTrends(
              node.data.AssetId,
              selectedModel.ModelExtID,
              false,
              timeSelection.startDate,
              timeSelection.endDate
            )
            .pipe(
              map(
                (trends) =>
                  actions.filterModelContextAvailable({
                    existingMaps,
                    maps: trends,
                  }),
                catchError((error) =>
                  of(actions.filterModelContextAvailableFailure(error))
                )
              )
            );
        }
      )
    )
  );

  loadModelContextCharts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadModelContextCharts),
      switchMap((payload) =>
        this.processDataFrameworkService
          .getPDNDModelTrends(payload.assetID, payload.modelExtID)
          .pipe(
            switchMap((trends) => {
              const pt = trends.map((m) => {
                const result = {
                  id: String(m.MappedPDTrend?.PDTrendID),
                  label: m.MappedPDTrend?.Title,
                  trendDefinition: m.MappedPDTrend,
                  totalSeries: getTrendTotalSeries(m?.MappedPDTrend),
                  labelIndex: null,
                } as IProcessedTrend;
                return result;
              });
              return [
                actions.loadModelContextChartsSuccess({ trends: pt }),
                actions.filterModelContextDrilldown({ maps: trends }),
              ];
            }),
            catchError((error) =>
              of(actions.loadModelContextChartsFailure(error))
            )
          )
      )
    )
  );

  addTrendDialogOpened$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addTrendDialogOpened),
      withLatestFrom(
        this.store.pipe(select(selectModel)),
        this.store.pipe(select(selectTimeSelection)),
        this.store.pipe(select(getModelContextDrilldown))
      ),
      filter(([unusedAction, selectedModel, timeSelection, existingMaps]) => {
        return !!selectedModel;
      }),
      switchMap(
        ([unusedAction, selectedModel, timeSelection, existingMaps]) => {
          const mdl = selectedModel;
          return this.processDataFrameworkService
            .getPDNDModelTrends(
              String(mdl.AssetID),
              mdl.ModelExtID,
              false,
              timeSelection.startDate,
              timeSelection.endDate
            )
            .pipe(
              switchMap((trends) => {
                return [
                  actions.modelContextAssetTreeStateChange(
                    selectAsset(String(mdl.AssetID ?? ''), false)
                  ),
                  actions.filterModelContextAvailable({
                    existingMaps,
                    maps: trends,
                  }),
                ];
              })
            );
        }
      )
    )
  );

  loadModelTrend$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadModelTrend),
      withLatestFrom(this.store.pipe(select(selectModelTrendYAxisEntities))),
      switchMap(([payload, YAxisList]) => {
        return this.alertsFrameworkService
          .getPDModelTrendForAlertCharting(
            payload.modelId,
            payload.startDate,
            payload.endDate
          )
          .pipe(
            map((trend) => {
              const yAxis = YAxisList[trend.Model.ModelID];
              if (yAxis) {
                trend.Min = yAxis.Min;
                trend.Max = yAxis.Max;
              }
              return actions.loadModelTrendSuccess({ trend });
            }),
            catchError((error) => of(actions.loadModelTrendFailure(error)))
          );
      })
    )
  );

  loadModelAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadModelAction),
      switchMap((payload) => {
        return this.alertsCoreService
          .getModelActionForAlertCharting(payload.modelId)
          .pipe(
            map((data) => actions.loadModelActionSuccess({ data })),
            catchError((error) => of(actions.loadModelActionFailure(error)))
          );
      })
    )
  );

  getTrendsSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadModelContextChartsSuccess),
      filter((action) => {
        return (action?.trends?.length ?? 0) > 0;
      }),
      switchMap((payload) => [
        actions.selectModelContextTrend({
          trendID: payload.trends[0].id,
          trend: payload.trends[0],
        }),
      ])
    )
  );

  savePDTrendsMappedToNDModel$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.savePDTrendsMappedToNDModel),
      withLatestFrom(this.store.pipe(select(selectModel))),
      filter(([payload, selectedModel]) => {
        return !!selectedModel;
      }),
      switchMap(([payload, selectedModel]) => {
        const mdl = selectedModel;
        return this.alertsFrameworkService
          .savePDTrendsMappedToNDModel(mdl.ModelExtID, payload.maps)
          .pipe(
            switchMap((maps) => {
              this.snackBarService.openSnackBar(
                'Saving Trends Success!',
                'success'
              );
              return [
                actions.savePDTrendsMappedToNDModelSuccess({ maps }),
                actions.loadModelContextCharts({
                  assetID: String(mdl.AssetID),
                  modelExtID: mdl.ModelExtID,
                }),
              ];
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar('Error Saving!', 'error');
              console.log(JSON.stringify(error));
              return of(actions.savePDTrendsMappedToNDModelFailure(error));
            })
          );
      })
    )
  );

  reloadModelTrend$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reloadModelTrend),
      withLatestFrom(
        this.store.pipe(select(selectModelID)),
        this.store.pipe(select(selectTimeSelection))
      ),
      filter(([payload, modelID, timeSelection]) => {
        return !!modelID;
      }),
      switchMap(([payload, modelId, timeSelection]) => [
        actions.loadModelTrend({
          modelId,
          startDate: payload.startDate ?? timeSelection.startDate,
          endDate: payload.endDate ?? timeSelection.endDate,
        }),
      ])
    )
  );

  reloadModelAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reloadModelAction),
      withLatestFrom(
        this.store.pipe(select(selectModelID)),
        this.store.pipe(select(selectTimeSelection))
      ),
      filter(([payload, modelID, timeSelection]) => {
        return !!modelID;
      }),
      switchMap(([payload, modelId, timeSelection]) => [
        actions.loadModelAction({
          modelId,
        }),
      ])
    )
  );

  selectModelContext$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectModelContextTrend),
      withLatestFrom(this.store.pipe(select(selectModelContextTrends))),
      map(([action, trends]) => {
        if (action.trend) {
          return action.trend;
        }
        return trends?.find((n) => n.id === action.trendID);
      }),
      withLatestFrom(this.store.pipe(select(selectTimeSelection))),
      switchMap(([trend, time]) => {
        return [
          actions.loadModelContext({
            startDate: time.startDate,
            endDate: time.endDate,
            trend,
          }),
        ];
      })
    )
  );

  editModelContextChart$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.editModelContextChart),
        withLatestFrom(
          this.store.select(selectModelAssetGuid),
          this.store.select(selectTimeSelection),
          this.store.select(selectSelectedModelContextTrendDef),
          this.store.select(getModelContextDrilldown)
        ),
        filter(
          ([action, asset, time, trendDefinition, modelTrendMapDictionary]) =>
            !isNil(trendDefinition)
        ),
        tap(
          ([action, asset, time, trendDefinition, modelTrendMapDictionary]) => {
            // Used sessionStorage rather than localStorage here to
            // respect opening multiple Model Context Trend consecutively in a short period of time.
            if (trendDefinition.PDTrendID < 0) {
              window.sessionStorage.setItem(
                'ModelContextTrend',
                JSON.stringify(trendDefinition)
              );
            } else {
              window.sessionStorage.removeItem('ModelContextTrend');

              const newAsset = Object.values(modelTrendMapDictionary).find(
                (value) => value.PDTrendID === trendDefinition.PDTrendID
              )?.AssetGuid;

              if (newAsset !== '00000000-0000-0000-0000-000000000000') {
                asset = newAsset;
              }
            }

            const dataExplorerUrl = `data-explorer?id=${asset}&trend=${trendDefinition.PDTrendID.toString()}&start=${time.startDate
              .getTime()
              .toString()}&end=${time.endDate.getTime().toString()}`;
            let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
            const hostname = window.location.hostname;
            if (hostname !== 'localhost') {
              url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
            }

            window.open(url, '_blank').focus();
          }
        )
      ),
    { dispatch: false }
  );

  actionFlyOutModelConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.actionFlyOutModelConfig),
      withLatestFrom(
        this.store.select(selectModelAssetGuid),
        this.store.select(selectModelExtID),
        this.store.select(selectTimeSelection),
        this.store.select(selectModelTypeAbbrev),
        this.store.select(selectModelConfigURL)
      ),
      switchMap(([unusedAction, asset, model, time, mtype, configurl]) => {
        return [
          actions.actionFlyOutModelConfigToAsset({
            model: model.toString(),
            asset: asset.toString(),
            start: time.startDate.getTime().toString(),
            end: time.endDate.getTime().toString(),
            mtype: mtype.toString(),
            configurl: configurl.toString(),
          }),
        ];
      })
    )
  );

  actionFlyOutOpModeConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.actionFlyOutOpModeConfig),
      withLatestFrom(
        this.store.select(selectModelAssetGuid),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, asset, time]) => {
        return [
          actions.actionFlyOutOpModeConfigToAsset({
            asset,
            start: time.startDate.getTime().toString(),
            end: time.endDate.getTime().toString(),
          }),
        ];
      })
    )
  );

  actionFlyOutDiagnosticDrilldown$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.diagnosticDrilldown),
      withLatestFrom(
        this.store.select(selectModelAssetGuid),
        this.store.select(selectModelExtID),
        this.store.select(selectTimeSelection),
        this.store.select(selectModelTypeAbbrev),
        this.store.select(selectDiagnosticDrilldownURL)
      ),
      switchMap(([unusedAction, asset, model, time, mtype, ddurl]) => {
        return [
          actions.diagnosticDrilldownToAsset({
            model: model.toString(),
            asset: asset.toString(),
            start: time.startDate.getTime().toString(),
            end: time.endDate.getTime().toString(),
            mtype: mtype.toString(),
            ddurl: ddurl.toString(),
          }),
        ];
      })
    )
  );

  actionFlyOutDataExplorer$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.dataExplorer),
        withLatestFrom(
          this.store.select(selectModelAssetGuid),
          this.store.select(selectTimeSelection)
        ),
        tap(([unusedAction, asset, time]) => {
          const dataExplorerUrl = `data-explorer?id=${asset.toString()}&start=${time.startDate
            .getTime()
            .toString()}&end=${time.endDate.getTime().toString()}`;
          let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
          }

          window.open(url, '_blank').focus();
        })
      ),
    { dispatch: false }
  );

  reloadModelContext$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reloadModelContext),
      withLatestFrom(
        this.store.pipe(select(selectSelectedModelContext)),
        this.store.pipe(select(selectTimeSelection))
      ),
      switchMap(([action, trend, timeSelection]) => [
        actions.loadModelContext({
          startDate: action.startDate ?? timeSelection.startDate,
          endDate: action.endDate ?? timeSelection.endDate,
          trend,
        }),
      ])
    )
  );

  loadModelContext$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadModelContext),
      filter((payload) => !!payload.trend),
      withLatestFrom(
        this.store.select(selectModelAssetGuid),
        this.store.select(selectModel)
      ),
      switchMap(([payload, assetId, model]) =>
        iif(
          () =>
            payload.trend.id === '-1' &&
            (model?.ModelTypeAbbrev === 'Forecast' ||
              model?.ModelTypeAbbrev === 'Rate of Change'),
          forkJoin([
            of(payload.trend.id),
            this.processDataFrameworkService.getTagsDataFiltered(
              payload.trend?.trendDefinition,
              payload.startDate,
              payload.endDate,
              0,
              '',
              assetId
            ),
            this.alertsCoreService.getCorrelationTrend(
              model.ModelExtID,
              payload.startDate,
              payload.endDate
            ),
          ]),
          forkJoin([
            of(payload.trend.id),
            this.processDataFrameworkService.getTagsDataFiltered(
              payload.trend?.trendDefinition,
              payload.startDate,
              payload.endDate,
              0,
              '',
              assetId
            ),
            of(null),
          ])
        )
      ),
      switchMap(([trendID, measurementsSet, correlationTrend]) => {
        return [
          actions.loadModelContextSuccess({
            trendID,
            measurementsSet,
            correlationTrend,
          }),
        ];
      }),
      catchError((error) => of(actions.loadModelContextFailure(error)))
    )
  );

  reflow$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          actions.reflow,
          actions.expandButtonClick,
          actions.hideTimeSlider
        ),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 400);
        })
      ),
    { dispatch: false }
  );

  reflowSlower$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 700);
        })
      ),
    { dispatch: false }
  );

  reloadFlyoutModel$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reloadFlyOutModel),
      switchMap((payload) =>
        this.alertsFrameworkService.getModelSummary(payload.modelID).pipe(
          map((result) => actions.reloadFlyOutModelSuccess({ model: result })),
          catchError((error) => {
            console.log(JSON.stringify(error));
            return of(actions.reloadFlyOutModelFailure(error));
          })
        )
      )
    )
  );

  actionFlyOutNewIssue$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.actionFlyOutNewIssue),
      withLatestFrom(this.store.select(selectAssetID)),
      switchMap(([action, assetId]) => {
        return [actions.actionFlyOutNewIssueToAsset({ assetId })];
      })
    )
  );

  setWatch$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setWatch),
      switchMap((action) =>
        this.alertsFrameworkService.setWatch(action.hours, action.models).pipe(
          switchMap((n) => {
            this.snackBarService.openSnackBar('Set Watch Success!', 'success');
            if (n.length === 1) {
              return [
                actions.setWatchSuccess({ model: n }),
                actions.loadModelHistory({
                  modelId: String(action.models[0].ModelID),
                }),
                actions.loadModelAction({
                  modelId: action.models[0].ModelID.toString(),
                }),
              ];
            } else {
              return [actions.setWatchSuccess({ model: n })];
            }
          }),
          catchError((error) => {
            this.snackBarService.openSnackBar('Error Watch Set!', 'error');
            console.log(JSON.stringify(error));
            return of(actions.setWatchFailure(error));
          })
        )
      )
    )
  );

  clearWatch$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.clearWatch),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Clear Watch Success!',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.clearWatchSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.clearWatchSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar('Clear Watch Failed!', 'error');
              console.log(JSON.stringify(error));
              return of(actions.clearWatchFailure(error));
            })
          )
      )
    )
  );

  setCustomWatch$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setCustomWatch),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Set Custom Watch Success!',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.setCustomWatchSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.setCustomWatchSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar(
                'Set Custom Watch Failed!',
                'error'
              );
              console.log(JSON.stringify(error));
              return of(actions.clearWatchFailure(error));
            })
          )
      )
    )
  );

  saveDiagnose$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveDiagnose),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Diagnose Note Saved',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.saveDiagnoseSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.saveDiagnoseSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar(
                'Save Diagnose Note Failed!',
                'error'
              );
              console.log(JSON.stringify(error));
              return of(actions.saveDiagnoseFailure(error));
            })
          )
      )
    )
  );

  saveMaintenance$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveMaintenance),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Maintenance Note Saved!',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.saveMaintenanceSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.saveMaintenanceSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar('Failed Saving Note', 'error');
              console.log(JSON.stringify(error));
              return of(actions.saveMaintenanceFailure(error));
            })
          )
      )
    )
  );

  saveNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveNote),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar('Note Saved!', 'success');
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.saveNoteSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.saveNoteSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar('Failed Saving Note', 'error');
              console.log(JSON.stringify(error));
              return of(actions.saveNoteFailure(error));
            })
          )
      )
    )
  );

  clearMaintenance$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.clearMaintenance),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Clear Maintenance Success!',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.clearMaintenanceSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.clearMaintenanceSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar(
                'Clear Maintenance Failed',
                'error'
              );
              console.log(JSON.stringify(error));
              return of(actions.clearMaintenanceFailure(error));
            })
          )
      )
    )
  );

  clearDiagnose$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.clearDiagnose),
      switchMap((action) =>
        this.alertsFrameworkService
          .actionItem(action.models, action.params, action.actionItemType)
          .pipe(
            switchMap((n) => {
              this.snackBarService.openSnackBar(
                'Clear Diagnose Success!',
                'success'
              );
              if (!Array.isArray(n)) {
                const newModel = Array<any>();
                newModel.push(n);
                return [
                  actions.clearDiagnoseSuccess({ model: newModel }),
                  actions.loadModelHistory({
                    modelId: String(newModel[0].ModelID),
                  }),
                  actions.loadModelAction({
                    modelId: action.models[0].ModelID.toString(),
                  }),
                ];
              } else {
                return [actions.clearDiagnoseSuccess({ model: n })];
              }
            }),
            catchError((error) => {
              this.snackBarService.openSnackBar(
                'Clear Diagnose Failed',
                'error'
              );
              console.log(JSON.stringify(error));
              return of(actions.clearDiagnoseFailure(error));
            })
          )
      )
    )
  );

  clearAlert$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.clearAlert),
      switchMap((action) =>
        this.alertsFrameworkService.clearAlert(action.models).pipe(
          switchMap((n) => {
            this.snackBarService.openSnackBar(
              'Clear Alert Success!',
              'success'
            );
            if (action.models.length === 1) {
              return [
                actions.loadModelHistory({
                  modelId: String(action.models[0].ModelID),
                }),
                actions.reloadFlyOutModel({
                  modelID: [action.models[0].ModelID.toString()],
                }),
                actions.clearAlertSuccess({ model: action.models }),
                actions.loadModelAction({
                  modelId: action.models[0].ModelID.toString(),
                }),
              ];
            } else {
              const array: string[] = [];
              for (const item of action.models) {
                array.push(item.ModelID.toString());
              }
              return [
                actions.reloadFlyOutModel({ modelID: array }),
                actions.clearAlertSuccess({ model: action.models }),
              ];
            }
          }),
          catchError((error) => {
            this.snackBarService.openSnackBar('Clear Action Failed!', 'error');
            console.log(JSON.stringify(error));
            return of(actions.clearAlertFailure(error));
          })
        )
      )
    )
  );

  toggleFloatingFilter = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.toggleFloatingFilter),
        tap(() => {
          this.logger.feature('floating_filter', 'alerts');
        })
      ),
    { dispatch: false }
  );

  loadList = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.listLoad),
        tap((action) => {
          this.logger.savedFilter(null, 'alerts', 'load');
        })
      ),
    { dispatch: false }
  );

  loadDefaultList = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.loadDefaultList),
        tap((action) => {
          this.logger.savedFilter('default', 'alerts', 'load');
        })
      ),
    { dispatch: false }
  );

  timeSliderToggle = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NavActions.toggleTimeslider),
        tap((action) => {
          this.logger.feature('timeslidertoggle', 'alerts');
        })
      ),
    { dispatch: false }
  );

  setActionItemFavorite$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setActionItemFavorite),
      map((value) => {
        return { ...value.actionItem, Favorite: value.favorite };
      }),
      switchMap((value) =>
        this.alertsFrameworkService.updateActionItem(value).pipe(
          map((result) =>
            actions.setActionItemFavoriteSuccess({
              actionItem: result.ActionItem,
            })
          ),
          catchError((error) => of(actions.setActionItemFavoriteFailure(error)))
        )
      )
    )
  );

  setActionItemFavoriteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setActionItemFavoriteSuccess),
        tap((value) => {
          this.snackBarService.openSnackBar('Favorite Changed', 'success');
        })
      ),
    { dispatch: false }
  );

  setActionItemFavoriteFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setActionItemFavoriteFailure),
        tap((value) => {
          this.snackBarService.openSnackBar('Favorite Change Failed', 'error');
        })
      ),
    { dispatch: false }
  );

  setActionItemNote$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setActionItemNote),
      map((value) => {
        return { ...value.actionItem, NoteText: value.note };
      }),
      switchMap((value) =>
        this.alertsFrameworkService.updateActionItem(value).pipe(
          map((result) =>
            actions.setActionItemNoteSuccess({ actionItem: result.ActionItem })
          ),
          catchError((error) => of(actions.setActionItemNoteFailure(error)))
        )
      )
    )
  );

  setActionItemNoteSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setActionItemNoteSuccess),
        tap((value) => {
          this.snackBarService.openSnackBar('Note Changed', 'success');
        })
      ),
    { dispatch: false }
  );

  setActionItemNoteFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.setActionItemNoteFailure),
        tap((value) => {
          this.snackBarService.openSnackBar('Note Change Failed', 'error');
        })
      ),
    { dispatch: false }
  );

  showRelatedModels$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showRelatedModels),
        map((n) => n.assetGuid),
        concatMap((action) =>
          of(action).pipe(
            withLatestFrom(this.store.pipe(select(selectAssetTreeConfigNodes)))
          )
        ),
        switchMap(([assetGuid, nodes]) => {
          let nodeID: string;
          // eslint-disable-next-line prefer-const
          nodeID =
            nodes.find((node) => node.data?.AssetGuid === assetGuid)
              ?.uniqueKey || null;

          if (nodeID) {
            return of(nodeID);
          } else {
            return of(assetGuid);
          }
        }),
        tap((id) => {
          if (id) {
            let url = window.location.href;
            if (url.includes('?')) {
              url = url.substr(0, url.indexOf('?'));
            }
            url = url + '?id=' + id;
            window.open(url, '_blank');
          } else {
            this.snackBarService.openSnackBar(
              'Could not open related',
              'warning'
            );
          }
        })
      ),
    { dispatch: false }
  );

  showRelatedIssues$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showRelatedIssues),
        map((n) => n.assetGuid),
        concatMap((action) =>
          of(action).pipe(
            withLatestFrom(this.store.pipe(select(selectAssetTreeConfigNodes)))
          )
        ),
        switchMap(([assetGuid, nodes]) => {
          let nodeID: string;
          // eslint-disable-next-line prefer-const
          nodeID =
            nodes.find((node) => node.data?.AssetGuid === assetGuid)
              ?.uniqueKey || null;

          if (nodeID) {
            return of(nodeID);
          } else {
            return of(assetGuid);
          }
        }),
        tap((id) => {
          if (id) {
            let url = window.location.href;
            url = url.substr(0, url.indexOf(this.router.url));
            url = url + '/issues?id=' + id;
            window.open(url, '_blank');
          } else {
            this.snackBarService.openSnackBar(
              'Could not open related',
              'warning'
            );
          }
        })
      ),
    { dispatch: false }
  );

  downloadModels = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.downloadModelsRequest),
      switchMap((action) => {
        this.snackBarService.openSnackBar('Downloading models...', 'info');
        return this.alertsCoreService.downloadModelsService(action.gridParams);
      }),
      map((n: IGridExportRetrieval): void => {
        // this function downloads the file.
        // Reference for this implementation: https://ourcodeworld.com/articles/read/
        // 189/how-to-create-a-file-and-generate-a-download-with-javascript-in-the-browser-without-a-server
        const element = document.createElement('a');
        element.setAttribute(
          'href',
          'data:text/plain;charset=utf-8,' + encodeURIComponent(n.Results[0])
        );
        element.setAttribute('download', 'AlertsModels.csv');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
      }),
      map(
        () => {
          this.snackBarService.openSnackBar(
            'Models downloaded sucessfully.',
            'success'
          );
          return actions.downloadModelsRequestSuccess();
        },
        catchError((error) => {
          this.snackBarService.openSnackBar(error, 'error');
          return of(actions.downloadModelsRequestFailure(error));
        })
      )
    )
  );

  loadAIAdvise = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadAIAdvice),
      switchMap((action) => {
        return this.aiAdviceService.loadAIAdvice(
          generatePrompt(
            action.application,
            action.assetType,
            action.parameterType,
            action.parameterModifier
          )
        );
      }),
      switchMap((res) => {
        return [
          actions.loadAIAdviceSuccess({
            response: res.choices[0].message.content,
          }),
        ];
      }),
      catchSwitchMapError((err) => {
        return of(actions.loadAIAdviceFailure({ error: err }));
      })
    )
  );
}

function generatePrompt(
  application: string,
  assetType: string,
  parameterType: string,
  parameterModifier: string
): string {
  //do some logic to create/refine the prompt
  let prompt = `What are possible causes of ${parameterModifier} ${parameterType} on an ${assetType}`;
  if (application) {
    prompt += `used for ${application}.`;
  } else {
    prompt += '. ';
  }
  prompt +=
    'Give me a list of parts to inspect and measurements to take to diagnose the cause. Be concise. Start your response with "Possible causes:".';
  return prompt;
}
