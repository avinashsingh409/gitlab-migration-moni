import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';
import { Store, Action } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Subject, Observable } from 'rxjs';
import { hot, cold } from 'jest-marbles';

import {
  ModelService as AssetTreeModel,
  selectAsset,
  nodesRetrieved,
  ITreeConfiguration,
  permissionsChanged,
} from '@atonix/atx-asset-tree';
import { createMock } from '@testing-library/angular/jest-utils';

import { SummaryEffects } from './summary.effects';
import { initialAlertsState } from '../state/alerts-state';
import * as actions from '../actions/summary.actions';
import { RouterTestingModule } from '@angular/router/testing';
import { LoggerService, ToastService } from '@atonix/shared/utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ITreePermissions } from '@atonix/atx-core';
import { AuthorizationFrameworkService } from '@atonix/shared/api';

describe('AlertsSummaryEffects', () => {
  let actions$: Observable<Action>;
  let store: MockStore<any>;
  let effects: SummaryEffects;
  let mockAuthFrameworkService: AuthorizationFrameworkService;
  let mockAssetTreeModel: AssetTreeModel;
  let mockToastService: ToastService;
  let mockLoggerService: LoggerService;

  const myTreeConfiguration: ITreeConfiguration = {
    showTreeSelector: true,
    trees: [],
    selectedTree: '1',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    nodes: [],
    pin: false,
    loadingAssets: false,
    loadingTree: false,
    canDrag: false,
    dropTargets: null,
    canView: true,
    canAdd: true,
    canEdit: true,
    canDelete: true,
    collapseOthers: false,
    hideConfigureButton: false,
  };

  beforeEach(() => {
    const initialState: any = { alerts: initialAlertsState };
    actions$ = new Subject<Action>();

    mockAssetTreeModel = createMock(AssetTreeModel);
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey1', []) }))
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    mockAuthFrameworkService = createMock(AuthorizationFrameworkService);
    mockToastService = createMock(ToastService);
    mockLoggerService = createMock(LoggerService);

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockActions(() => actions$),
        provideMockStore<any>({ initialState }),
        { provide: AssetTreeModel, useValue: mockAssetTreeModel },
        {
          provide: AuthorizationFrameworkService,
          useValue: mockAuthFrameworkService,
        },
        { provide: ToastService, useValue: mockToastService },
        { provide: LoggerService, useValue: mockLoggerService },
        SummaryEffects,
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    });
    store = TestBed.inject<any>(Store);
    mockAssetTreeModel = TestBed.inject(AssetTreeModel);
    effects = TestBed.inject<SummaryEffects>(SummaryEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should get asset info twice', () => {
    store.setState({
      alerts: {
        summaryState: {
          AssetTreeState: {
            treeConfiguration: myTreeConfiguration,
            treeNodes: null,
          },
        },
      },
    });

    actions$ = hot('a-b', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });

    const expected = hot('a-b', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get asset info overlap', () => {
    store.setState({
      alerts: {
        summaryState: {
          AssetTreeState: {
            treeConfiguration: myTreeConfiguration,
            treeNodes: null,
          },
        },
      },
    });

    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(
        cold('--a|', { a: nodesRetrieved('uniquekey1', []) })
      )
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    actions$ = hot('ab', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });

    const expected = hot('-ba', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get permissions', () => {
    const result1: ITreePermissions = {
      canAdd: true,
      canDelete: true,
      canEdit: true,
      canView: true,
    };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: result1 }))
      .mockReturnValueOnce(cold('a|', { a: result2 }));
    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestSuccess(result1),
      b: actions.permissionsRequestSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });

  it('should get permissions with error', () => {
    const error1: Error = { name: 'name', message: 'something' };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a|', { a: result2 }));
    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestFailure(error1),
      b: actions.permissionsRequestSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });
});
