import {
  IAssetModelChartingData,
  ICorrelationData,
  INDModelActionItemAnalysis,
} from '@atonix/shared/api';
import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import { INDModelPDTrendMap, IProcessedTrend } from '@atonix/atx-core';
import { EntityAdapter, EntityState, createEntityAdapter } from '@ngrx/entity';

export interface IModelContextState {
  trends: IProcessedTrend[];
  correlationTrend: ICorrelationData[];
  selectedTrendID: string;
  loading?: boolean;
  modelContextAssetTree: ITreeState;
  modelTrendMapDictionary: { [id: string]: INDModelPDTrendMap };
  availableCharts: { [id: string]: INDModelPDTrendMap };
  loadingList: boolean;
  savedTrend: boolean;
}

export const initialModelContextState: IModelContextState = {
  trends: null,
  correlationTrend: null,
  selectedTrendID: null,
  loading: false,
  modelContextAssetTree: {
    treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  modelTrendMapDictionary: null,
  availableCharts: null,
  loadingList: null,
  savedTrend: null,
};

export interface YAxisMinMax {
  id: number;
  Min: number;
  Max: number;
}

export interface YAxisList extends EntityState<YAxisMinMax> {
  selectedId: number | null;
}

export interface IModelTrendState {
  trendData: IAssetModelChartingData;
  loading: boolean;
  trendYAxis: YAxisList;
}
export const yAxisAdapter: EntityAdapter<YAxisMinMax> =
  createEntityAdapter<YAxisMinMax>({});

export const initialModelTrendState: IModelTrendState = {
  trendData: null,
  loading: false,
  trendYAxis: yAxisAdapter.getInitialState({ selectedId: null }),
};

export interface IModelHistoryState {
  busy?: boolean;
  filters: IModelHistoryFilters;
}

export const initialModelHistoryState: IModelHistoryState = {
  busy: false,
  filters: {
    historyTypes: [
      'Watch Set',
      'Diagnose Set',
      'Diagnose Cleared',
      'Model Maintenance Set',
      'Model Maintenance Cleared',
      'Note Added',
    ],
    start: null,
    end: null,
    user: null,
    note: null,
    favorite: null,
  },
};

export interface IModelActionState {
  trendData: INDModelActionItemAnalysis[];
  loading: boolean;
}

export const initialModelActionState: IModelActionState = {
  trendData: null,
  loading: false,
};
