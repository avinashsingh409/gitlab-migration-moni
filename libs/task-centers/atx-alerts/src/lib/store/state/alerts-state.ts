import { ISummaryState, initialSummaryState } from './summary-state';

export interface IAlertsState {
  summaryState: ISummaryState;
  // add details state here
}

export const initialAlertsState: IAlertsState = {
  summaryState: initialSummaryState,
  // add details state here
};

export interface IToast {
  message: string;
  type: 'success' | 'error' | 'info';
}
