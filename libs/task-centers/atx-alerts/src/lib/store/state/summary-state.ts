import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
  TrayState,
  IInfoTrayState,
} from '@atonix/atx-asset-tree';
import { ITimeSliderState } from '@atonix/atx-time-slider';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import {
  initialModelContextState,
  initialModelTrendState,
  IModelContextState,
  IModelTrendState,
  IModelHistoryState,
  initialModelHistoryState,
  IModelActionState,
  initialModelActionState,
} from './chart-state';
import { IListState, ISavedModelList } from '@atonix/shared/api';
import { INDModelSummary } from '@atonix/shared/api';
import { IToast } from './alerts-state';

export function sortModelList(a: ISavedModelList, b: ISavedModelList) {
  return a.name.localeCompare(b.name);
}

export const savedModelListAdapter = createEntityAdapter<ISavedModelList>({
  sortComparer: sortModelList,
});
export type ISavedModelListState = EntityState<ISavedModelList>;

export type SummarySplitEnum = 'Split' | 'List' | 'Trend';

export interface IUpdatedModels {
  ids: number[];
  newValue: boolean;
  action: 'watch' | 'alert' | 'maintenance' | 'diagnose';
}

export interface ISummaryState {
  AssetTreeState: ITreeState;
  TimeSliderState: ITimeSliderState;
  SummarySplit: SummarySplitEnum;
  ModelContextState: IModelContextState;
  ModelTrendState: IModelTrendState;
  ModelActionState: IModelActionState;
  ModelHistoryState: IModelHistoryState;
  SelectedModel: INDModelSummary[];
  ModelLists: ISavedModelListState;
  SavedListState: IListState; // Saved columns are the columns from a configuration we want to send to the list view.
  DisplayedListState: IListState; // Displayed columns are the columns the list view is currently displaying.
  Refreshing: boolean;
  Toast: IToast;
  FloatingFilter: boolean;
  LeftTraySize: number;
  UpdatedModels: IUpdatedModels;
  ReloadDate: Date;
  AIResponse: string;
  AIResponseLoading: boolean;
}

export const initialSummaryState: ISummaryState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  TimeSliderState: null,
  SummarySplit: 'Split',
  ModelContextState: initialModelContextState,
  ModelTrendState: initialModelTrendState,
  ModelHistoryState: initialModelHistoryState,
  ModelActionState: initialModelActionState,
  SelectedModel: null,
  ModelLists: savedModelListAdapter.getInitialState(),
  SavedListState: null,
  DisplayedListState: null,
  Refreshing: false,
  Toast: null,
  FloatingFilter: false,
  LeftTraySize: 250,
  UpdatedModels: null,
  ReloadDate: null,
  AIResponse: '',
  AIResponseLoading: false,
};
