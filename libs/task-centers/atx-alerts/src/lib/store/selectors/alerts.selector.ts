import { IAlertsState } from '../state/alerts-state';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { ISummaryState } from '../state/summary-state';

// This is actually grabbing the application state.  It doesn't really
// know what the full application state is
// export const selectApp = (state: { nav: INavigationState; alerts: IAlertsState; asset: any }) => state;
export const selectApp = (state: { alerts: IAlertsState; asset: any }) => state;

// This looks at the application state and grabs the member
// named alerts.  It isn't doing anything more complex than that.
const alertsState = createFeatureSelector<IAlertsState>('alerts');

// This grabs the state of the selected asset
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);

// This is grabbing the applicationstate.alerts.summaryState object
export const selectSummaryState = createSelector(
  alertsState,
  (state: IAlertsState) => state.summaryState
);

export const selectSelectedModel = createSelector(
  selectSummaryState,
  (state: ISummaryState) => state.SelectedModel
);

export const selectAssetID = createSelector(selectSelectedModel, (state) => {
  return state?.[0]?.AssetID ?? null;
});
