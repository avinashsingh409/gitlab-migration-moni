import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { selectSummaryState } from './alerts.selector';
import { savedModelListAdapter, ISummaryState } from '../state/summary-state';
import { IButtonData, INavigationState } from '@atonix/atx-navigation';

export const selectApp = (state: { nav: INavigationState; asset: any }) =>
  state;

const selectNavState = createFeatureSelector<INavigationState>('nav');
export const selectNavItems = createSelector(
  selectNavState,
  (state) => state.navigationItems
);

export const selectSummaryView = createSelector(
  selectSummaryState,
  (state) => state.SummarySplit
);
export const selectSummaryViewSplit = createSelector(
  selectSummaryView,
  (state) => state === 'Split'
);
export const selectSummaryViewList = createSelector(
  selectSummaryView,
  (state) => state === 'List'
);
export const selectSummaryViewTrend = createSelector(
  selectSummaryView,
  (state) => state === 'Trend'
);

// Model
export const selectModels = createSelector(
  selectSummaryState,
  (state) => state.SelectedModel
);
export const selectModel = createSelector(selectModels, (state) => {
  return state?.length === 1 ? state[0] : null;
});
export const multiselect = createSelector(selectSummaryState, (state) => {
  return (state?.SelectedModel?.length ?? 0) > 1;
});
export const selectModelID = createSelector(selectModel, (state) => {
  return String(state?.ModelID?.toString() ?? '');
});
export const selectModelExtID = createSelector(selectModel, (state) => {
  return String(state?.ModelExtID ?? '');
});
export const selectModelAssetGuid = createSelector(selectModel, (state) => {
  return String(state?.AssetGUID ?? '');
});
export const selectModelTypeAbbrev = createSelector(selectModel, (state) => {
  return String(state?.ModelTypeAbbrev ?? '');
});
export const selectDiagnosticDrilldownURL = createSelector(
  selectModel,
  (state) => {
    return String(state?.DiagnosticDrilldownURL ?? '');
  }
);
export const selectModelConfigURL = createSelector(selectModel, (state) => {
  return String(state?.ModelConfigURL ?? '');
});

// For Asset Tree
export const selectAssetTreeState = createSelector(
  selectSummaryState,
  (state) => state.AssetTreeState
);

export const selectAssetTreeNodes = createSelector(
  selectAssetTreeState,
  (state) => state.treeNodes
);

export const getSelectedAsset = createSelector(selectAssetTreeNodes, (state) =>
  getIDFromSelectedAssets(getSelectedNodes(state))
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  getSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const selectSelectedAssetKey = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return (
        data.TreeId +
        '~' +
        data.ParentNodeId +
        '~' +
        data.NodeId +
        '~' +
        data.AssetGuid
      );
    }
    return null;
  }
);

export const getSelectedAssetTreeNode = createSelector(
  selectSummaryState,
  (state: ISummaryState) =>
    state.AssetTreeState.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(
          getSelectedNodes(state.AssetTreeState.treeNodes)
        )
    )
);

export const getAssetTreeState = createSelector(
  selectSummaryState,
  (state) => state.AssetTreeState
);

export const getAssetTreeConfiguration = createSelector(
  selectSummaryState,
  (state) => state.AssetTreeState.treeConfiguration
);

export const selectAssetTreeConfigNodes = createSelector(
  getAssetTreeConfiguration,
  (state) => state.nodes
);

// For Saved Tool Panel
const { selectIds, selectEntities, selectAll, selectTotal } =
  savedModelListAdapter.getSelectors();

export const getListState = createSelector(
  selectSummaryState,
  (state) => state.SavedListState
);

export const getDisplayedListState = createSelector(
  selectSummaryState,
  (state) => state.DisplayedListState
);

export const getListsState = createSelector(
  selectSummaryState,
  (state) => state.ModelLists
);

export const getSavedLists = createSelector(getListsState, selectAll);

// For Model History
export const selectModelHistoryState = createSelector(
  selectSummaryState,
  (state) => state?.ModelHistoryState
);
export const selectModelHistorySaving = createSelector(
  selectModelHistoryState,
  (state) => state?.busy ?? false
);
export const selectModelHistoryFilters = createSelector(
  selectModelHistoryState,
  (state) => state?.filters
);

// For Model Context
export const selectModelContextState = createSelector(
  selectSummaryState,
  (state) => state?.ModelContextState
);

export const selectModelContextTrends = createSelector(
  selectModelContextState,
  (state) => state?.trends
);

export const selectSelectedModelContextID = createSelector(
  selectModelContextState,
  (state) => state?.selectedTrendID
);

export const selectModelContextCorrelationTrend = createSelector(
  selectModelContextState,
  (state) => state?.correlationTrend
);

export const selectModelContextLoading = createSelector(
  selectModelContextState,
  (state) => state.loading
);

export const selectSelectedModelContext = createSelector(
  selectModelContextState,
  (state) => {
    const trends = state.trends ?? [];
    const ID = state.selectedTrendID ?? '';
    return trends.find((n) => n.id === ID);
  }
);

export const selectSelectedModelContextTrendDef = createSelector(
  selectSelectedModelContext,
  (state) => state?.trendDefinition || null
);

// For Model Context - Asset Tree
export const getModelContextSelectedAsset = createSelector(
  selectSummaryState,
  (state) =>
    getIDFromSelectedAssets(
      getSelectedNodes(state.ModelContextState.modelContextAssetTree.treeNodes)
    )
);

export const getModelContextSelectedAssetTreeNode = createSelector(
  selectSummaryState,
  (state: ISummaryState) =>
    state.ModelContextState.modelContextAssetTree.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(
          getSelectedNodes(
            state.ModelContextState.modelContextAssetTree.treeNodes
          )
        )
    )
);

export const getModelContextAssetTreeState = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.modelContextAssetTree
);

export const getModelContextAssetTreeConfiguration = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.modelContextAssetTree.treeConfiguration
);

export const getModelContextDrilldown = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.modelTrendMapDictionary
);

export const getModelContextAvailableCharts = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.availableCharts
);

export const selectLoadingList = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.loadingList
);

export const selectSavedTrend = createSelector(
  selectSummaryState,
  (state) => state.ModelContextState.savedTrend
);

// For Model Trends
export const selectModelTrendState = createSelector(
  selectSummaryState,
  (state) => state.ModelTrendState
);

export const selectModelTrendData = createSelector(
  selectModelTrendState,
  (state) => state.trendData
);

export const selectModelTrendLoading = createSelector(
  selectModelTrendState,
  (state) => state.loading
);

export const selectModelTrendYAxisEntities = createSelector(
  selectModelTrendState,
  (state) => state.trendYAxis.entities
);

// For Time Slider
export const getTimeSliderState = createSelector(
  selectSummaryState,
  (state: ISummaryState) => state.TimeSliderState
);
export const selectRefreshDate = createSelector(
  selectSummaryState,
  (state: ISummaryState) => state.ReloadDate
);

export const selectTimeSelection = createSelector(
  getTimeSliderState,
  (state) => {
    return { startDate: state?.startDate, endDate: state?.endDate };
  }
);

export const selectRefreshable = createSelector(
  getTimeSliderState,
  (state) => !state?.isLive
);
export const selectRefreshing = createSelector(
  selectSummaryState,
  (state) => state?.Refreshing
);
export const selectToast = createSelector(
  selectSummaryState,
  (state) => state?.Toast
);
export const selectFloatingFilter = createSelector(
  selectSummaryState,
  (state) => state.FloatingFilter
);
export const selectLeftTraySize = createSelector(
  selectSummaryState,
  (s) => s.LeftTraySize
);
export const selectLayoutMode = createSelector(
  getAssetTreeConfiguration,
  (s) => {
    const result: TrayState = s.pin ? 'side' : 'over';
    return result;
  }
);

// For Model Action
export const selectModelActionState = createSelector(
  selectSummaryState,
  (state) => state.ModelActionState
);

export const selectModelActionData = createSelector(
  selectModelActionState,
  (state) => state.trendData
);

export const selectModelActionLoading = createSelector(
  selectModelActionState,
  (state) => state.loading
);

export const selectUpdatedModels = createSelector(
  selectSummaryState,
  (state) => state.UpdatedModels
);

export const selectAIResponse = createSelector(
  selectSummaryState,
  (state) => state.AIResponse
);

export const selectAIResponseLoading = createSelector(
  selectSummaryState,
  (state) => state.AIResponseLoading
);
