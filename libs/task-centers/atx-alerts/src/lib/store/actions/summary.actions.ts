/* eslint-disable ngrx/prefer-inline-action-props */
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import { IButtonData } from '@atonix/atx-navigation';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { createAction, props } from '@ngrx/store';
import {
  IAssetModelChartingData,
  INDModelActionItemAnalysis,
  IListState,
  ISavedModelList,
  IActionItem,
  INDModelSummary,
  IGridParameters,
  ICorrelationData,
} from '@atonix/shared/api';
import { IAlertTimelineResult, INDModelActionItem } from '@atonix/shared/api';
import { IModelHistoryFilters } from '../../model/model-history-filters';
import {
  IAssetMeasurementsSet,
  ICriteria,
  INDModelPDTrendMap,
  IProcessedTrend,
  ITreePermissions,
} from '@atonix/atx-core';

export const summaryInitialize = createAction(
  '[Alerts Summary] Initialize',
  props<{ asset?: string; startDate?: Date; endDate?: Date }>()
);

export const selectModel = createAction(
  '[Alerts Summary] Select Model',
  props<{ model: INDModelSummary[] }>()
);

export const updateModelNavConfig = createAction(
  '[Alerts Summary] Update Model Navigation Config',
  props<{ model: string; asset: string; start: string; end: string }>()
);

export const multiSelect = createAction('[Alerts Summary] Multi Select Model');
export const permissionsRequest = createAction(
  '[Alerts Summary] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[Alerts Summary] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[Alerts Summary] Permissions Request Failure',
  props<Error>()
);

export const selectAsset = createAction(
  '[Alerts Summary] Select Asset',
  props<{ asset: string }>()
);
export const updateRoute = createAction('[Alerts Summary] Update Route');

export const treeStateChange = createAction(
  '[Alerts Summary] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[Alerts Summary] Tree Data Request Failure',
  props<Error>()
);

export const treeSizeChange = createAction(
  '[Alerts Summary] Asset Tree Size Change',
  props<{ value: number }>()
);

// This action will be manipulated on the App's state management
export const launchPadTileSelect = createAction(
  '[Alerts Summary] Launch pad tile select',
  props<IButtonData>()
);

export const listStateChange = createAction(
  '[Alerts Summary] List State Change',
  props<{ listState: IListState }>()
);

export const listsLoad = createAction('[Alerts Summary] Lists Load');

export const actionFlyOutModelConfig = createAction(
  '[Alerts Summary - Actions Flyout] Model Configuration'
);
export const actionFlyOutModelConfigToAsset = createAction(
  '[Alerts Summary - Actions Flyout] Model Configuration To Asset',
  props<{
    model: string;
    asset: string;
    start: string;
    end: string;
    mtype: string;
    configurl: string;
  }>()
);

export const actionFlyOutOpModeConfig = createAction(
  '[Alerts Summary - Actions FlyOut] OP Mode Configuration'
);
export const actionFlyOutOpModeConfigToAsset = createAction(
  '[Alerts Summary - Actions FlyOut] OP Mode Configuration To Asset',
  props<{ asset: string; start: string; end: string }>()
);

export const diagnosticDrilldown = createAction(
  '[Alerts Summary] Diagnostic Drilldown'
);
export const dataExplorer = createAction('[Alerts Summary] Data Explorer');
export const diagnosticDrilldownToAsset = createAction(
  '[Alerts Summary] Diagnostic Drilldown To Asset',
  props<{
    model: string;
    asset: string;
    start: string;
    end: string;
    mtype: string;
    ddurl: string;
  }>()
);

export const actionFlyOutNewIssue = createAction(
  '[Alerts Summary - Actions FlyOut] New Issue'
);

export const actionFlyOutNewIssueToAsset = createAction(
  '[Alerts Summary - Actions FlyOut] New Issue To Asset',
  props<{ assetId: number }>()
);

export const listsLoadSuccess = createAction(
  '[Alerts Summary] Load Lists Success',
  props<{ lists: ISavedModelList[] }>()
);

export const listsLoadFailure = createAction(
  '[Alerts Summary] Load Lists Failure'
);

export const listSave = createAction(
  '[Alerts Summary] List Save',
  props<{ list: Partial<ISavedModelList> }>()
);

export const listSaveSuccess = createAction(
  '[Alerts Summary] List Save Success',
  props<{ list: ISavedModelList }>()
);

export const listSaveFailure = createAction(
  '[Alerts Summary] List Save Failure',
  props<{ list: ISavedModelList }>()
);

export const listLoad = createAction(
  '[Alerts Summary] List Load',
  props<{ id: number }>()
);

export const listDelete = createAction(
  '[Alerts Summary] List Delete',
  props<{ id: number }>()
);

export const listDeleteSuccess = createAction(
  '[Alerts Summary] List Delete Success',
  props<{ id: number }>()
);

export const listDeleteFailure = createAction(
  '[Alerts Summary] List Delete Failure',
  props<{ id: number }>()
);

// Time Slider
export const initializeTimeSlider = createAction(
  '[Alerts Summary] Initialize Time Slider',
  props<{ startDate: Date; endDate: Date }>()
);

export const timeSliderStateChange = createAction(
  '[Alerts Summary] Time Slider State Change',
  props<ITimeSliderStateChange>()
);

export const hideTimeSlider = createAction('[Alerts Summary] Hide Time Slider');

export const runLive = createAction('[Alerts Summary] Run Live');

export const issueSelected = createAction(
  '[Alerts Screening View] Issue Selected',
  props<{ id: string }>()
);

// Model History
export const loadModelHistory = createAction(
  '[Alerts Model History] Load Model History',
  props<{ modelId: string }>()
);

export const loadModelHistorySuccess = createAction(
  '[Alerts Model History] Get Model History Success',
  props<{ history: IAlertTimelineResult }>()
);

export const loadModelHistoryFailure = createAction(
  '[Alerts Model History] Get Model History Failed',
  props<Error>()
);

export const setModelHistoryFilters = createAction(
  '[Alerts Model History] Set Model History Filters',
  props<{ filter: IModelHistoryFilters }>()
);

// Model Context
export const loadModelContextCharts = createAction(
  '[Alerts Model Context Chart] Load Model Context Trends Charts',
  props<{ assetID: string; modelExtID: string }>()
);

export const loadModelContextChartsSuccess = createAction(
  '[Alerts Model Context Chart] Get Model Context Trends Success',
  props<{ trends: IProcessedTrend[] }>()
);

export const addTrendDialogOpened = createAction(
  '[Alerts Model Context Chart] Model Dialog Open'
);

export const filterModelContextDrilldown = createAction(
  '[Alerts Model Context Chart] Filter Model Context Drilldown',
  props<{ maps: INDModelPDTrendMap[] }>()
);

export const filterModelContextAvailable = createAction(
  '[Alerts Model Context Chart] Filter Model Context Available',
  props<{
    existingMaps: { [id: string]: INDModelPDTrendMap };
    maps: INDModelPDTrendMap[];
  }>()
);

export const filterModelContextAvailableFailure = createAction(
  '[Alerts Model Context Chart] Filter Model Context Available Failed',
  props<Error>()
);

export const loadModelContextChartsFailure = createAction(
  '[Alerts Model Context Chart] Get Model Context Trends Failed',
  props<Error>()
);

export const selectModelContextTrend = createAction(
  '[Alerts Model Context Chart] Model Context Trend Selected',
  props<{ trendID: string; trend?: IProcessedTrend }>()
);

export const reloadModelContext = createAction(
  '[Alerts Model Context] Model Context Reloaded',
  props<{ startDate: Date; endDate: Date }>()
);

export const loadModelContext = createAction(
  '[Alerts Model Context] Load Model Context',
  props<{ startDate: Date; endDate: Date; trend: IProcessedTrend }>()
);

export const loadModelContextSuccess = createAction(
  '[Alerts Model Context] Model Context Loaded',
  props<{
    trendID: string;
    measurementsSet: IAssetMeasurementsSet;
    correlationTrend: ICorrelationData[];
  }>()
);

export const loadModelContextFailure = createAction(
  '[Alerts Model Context] Model Context Failed',
  props<Error>()
);

export const renderModelContextChart = createAction(
  '[Alerts Model Context Chart] Render Data Model Context Trends Charts',
  props<{ trend: IProcessedTrend }>()
);

export const editModelContextChart = createAction(
  '[Alerts Model Context Chart] Edit Chart'
);

export const updateLimitsModelContextChart = createAction(
  '[Alerts Model Context Chart] Update Limits',
  props<{ limitsData: IUpdateLimitsData }>()
);

export const resetLimitsModelContextChart = createAction(
  '[Alerts Model Context Chart] Reset Limits',
  props<{ axisIndex: number }>()
);

export const legendItemToggleModelContextChart = createAction(
  '[Alerts Model Context Chart] Legend Item Toggle',
  props<{ seriesIndex: number }>()
);

export const selectModelContextAsset = createAction(
  '[Alerts Model Context] Select Asset'
);
export const modelContextPermissionsRequest = createAction(
  '[Alerts Model Context] Permissions Request'
);
export const modelContextPermissionsRequestSuccess = createAction(
  '[Alerts Model Context] Permissions Request Success',
  props<ITreePermissions>()
);
export const modelContextPermissionsRequestFailure = createAction(
  '[Alerts Model Context] Permissions Request Failure',
  props<Error>()
);

export const modelContextAssetTreeStateChange = createAction(
  '[Alerts Model Context] Tree State Change',
  props<ITreeStateChange>()
);

export const modelContextAssetTreeDataRequestFailure = createAction(
  '[Alerts Model Context] Tree Data Request Failure',
  props<Error>()
);

export const savePDTrendsMappedToNDModel = createAction(
  '[Alerts Model Context] Save PDTrends Map',
  props<{ maps: INDModelPDTrendMap[] }>()
);

export const savePDTrendsMappedToNDModelSuccess = createAction(
  '[Alerts Model Context] Save PDTrends Map Success',
  props<{ maps: INDModelPDTrendMap[] }>()
);

export const savePDTrendsMappedToNDModelFailure = createAction(
  '[Alerts Model Context] Save PDTrends Map Failure',
  props<Error>()
);

// Model Trend
export const reloadModelTrend = createAction(
  '[Alerts Model Trend Chart] Reload Model Trend',
  props<{ startDate: Date; endDate: Date }>()
);

export const loadModelTrend = createAction(
  '[Alerts Model Trend] Load Model Trend',
  props<{ modelId: string; startDate: Date; endDate: Date }>()
);

export const loadModelTrendSuccess = createAction(
  '[Alerts Model Trend] Load Model Trend Success',
  props<{ trend: IAssetModelChartingData }>()
);

export const loadModelTrendFailure = createAction(
  '[Alerts Model Trend] Load Model Trend Failed',
  props<Error>()
);

export const updateLimitsModelTrend = createAction(
  '[Alerts Model Trend] Update Limits',
  props<{ limitsData: IUpdateLimitsData }>()
);

export const resetLimitsModelTrend = createAction(
  '[Alerts Model Trend] Reset Limits',
  props<{ axisIndex: number }>()
);

export const expandButtonClick = createAction(
  '[Alerts Summary] Expand Button Click',
  props<{ source: 'List' | 'Trend' }>()
);

// Model Action
export const reloadModelAction = createAction(
  '[Alerts Model Action Chart] Reload Model Action',
  props<{ startDate: Date; endDate: Date }>()
);

export const loadModelAction = createAction(
  '[Alerts Model Action] Load Model Action',
  props<{ modelId: string }>()
);

export const loadModelActionSuccess = createAction(
  '[Alerts Model Action] Load Model Action Success',
  props<{ data: INDModelActionItemAnalysis[] }>()
);

export const loadModelActionFailure = createAction(
  '[Alerts Model Action] Load Model Action Failed',
  props<Error>()
);

export const reflow = createAction('[Alerts Summary] Reflow');

export const changeLabelsClicked = createAction(
  '[Alerts Model Context] Change labels clicked',
  props<{ index: number }>()
);

// Action Flyout

export const reloadFlyOutModel = createAction(
  '[Alerts Action Flyout] Refresh',
  props<{ modelID: string[] }>()
);

export const reloadFlyOutModelSuccess = createAction(
  '[Alerts Action Flyout] Get Model Success',
  props<{ model: INDModelSummary[] }>()
);

export const reloadFlyOutModelFailure = createAction(
  '[Alerts Action Flyout] Get Model Failure',
  props<Error>()
);

export const setWatch = createAction(
  '[Alerts Action Flyout] Set Watch',
  props<{ models: INDModelSummary[]; hours: string }>()
);

export const setWatchSuccess = createAction(
  '[Alerts Action Flyout] Set Watch Success',
  props<{ model: INDModelSummary[] }>()
);

export const setWatchFailure = createAction(
  '[Alerts Action Flyout] Set Watch Failure',
  props<Error>()
);

export const clearWatch = createAction(
  '[Alerts Action Flyout] Clear Watch',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const clearWatchSuccess = createAction(
  '[Alerts Action Flyout] Clear Watch Success',
  props<{ model: INDModelSummary[] }>()
);

export const clearWatchFailure = createAction(
  '[Alerts Action Flyout] Clear Watch Failure',
  props<Error>()
);

export const clearAlert = createAction(
  '[Alerts Action Flyout] Clear  Alert',
  props<{ models: INDModelSummary[] }>()
);

export const clearAlertSuccess = createAction(
  '[Alerts Action Flyout] Clear Alert Success',
  props<{ model: INDModelSummary[] }>()
);

export const clearAlertFailure = createAction(
  '[Alerts Action Flyout] Clear Alert Failure',
  props<Error>()
);

export const setCustomWatch = createAction(
  '[Alerts Action Flyout] Set Custom Watch',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const setCustomWatchSuccess = createAction(
  '[Alerts Action Flyout] Set Custom Watch Success',
  props<{ model: INDModelSummary[] }>()
);

export const setCustomWatchFailure = createAction(
  '[Alerts Action Flyout] Set Custom Watch Failure',
  props<Error>()
);

export const saveDiagnose = createAction(
  '[Alerts Action Flyout] Save Diagnose',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const saveDiagnoseSuccess = createAction(
  '[Alerts Action Flyout] Save Diagnose Success',
  props<{ model: INDModelSummary[] }>()
);

export const saveDiagnoseFailure = createAction(
  '[Alerts Action Flyout] Save Diagnose Failure',
  props<Error>()
);

export const saveMaintenance = createAction(
  '[Alerts Action Flyout] Save Maintenance',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const saveMaintenanceSuccess = createAction(
  '[Alerts Action Flyout] Save Maintenance Success',
  props<{ model: INDModelSummary[] }>()
);

export const saveMaintenanceFailure = createAction(
  '[Alerts Action Flyout] Save Maintenance Failure',
  props<Error>()
);

export const saveNote = createAction(
  '[Alerts Action Flyout] Save Note',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const saveNoteSuccess = createAction(
  '[Alerts Action Flyout] Save Note Success',
  props<{ model: INDModelSummary[] }>()
);

export const saveNoteFailure = createAction(
  '[Alerts Action Flyout] Save Note Failure',
  props<Error>()
);

export const clearMaintenance = createAction(
  '[Alerts Action Flyout] Clear Maintenance',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const clearMaintenanceSuccess = createAction(
  '[Alerts Action Flyout] Clear Maintenance Success',
  props<{ model: INDModelSummary[] }>()
);

export const clearMaintenanceFailure = createAction(
  '[Alerts Action Flyout] Clear Maintenance Failure',
  props<Error>()
);

export const clearDiagnose = createAction(
  '[Alerts Action Flyout] Clear Diagnose',
  props<{
    models: INDModelSummary[];
    params: IActionItem;
    actionItemType: string;
  }>()
);

export const clearDiagnoseSuccess = createAction(
  '[Alerts Action Flyout] Clear Diagnose Success',
  props<{ model: INDModelSummary[] }>()
);

export const clearDiagnoseFailure = createAction(
  '[Alerts Action Flyout] Clear Diagnose Failure',
  props<Error>()
);
export const toggleFloatingFilter = createAction(
  '[Alerts Summary] Toggle Floating Filter',
  props<{ filterOn?: boolean }>()
);
export const loadDefaultList = createAction('[Alerts] Load Default List');

export const setActionItemFavorite = createAction(
  '[Alerts Summary] Set Action Item Favorite',
  props<{ actionItem: INDModelActionItem; favorite: boolean }>()
);

export const setActionItemFavoriteSuccess = createAction(
  '[Alerts Summary] Set Action Item Favorite Success',
  props<{ actionItem: INDModelActionItem }>()
);

export const setActionItemFavoriteFailure = createAction(
  '[Alerts Summary] Set Action Item Favorite Failure',
  props<{ actionItem: INDModelActionItem }>()
);

export const setActionItemNote = createAction(
  '[Alerts Summary] Set Action Item Note',
  props<{ actionItem: INDModelActionItem; note: string }>()
);

export const setActionItemNoteSuccess = createAction(
  '[Alerts Summary] Set Action Item Note Success',
  props<{ actionItem: INDModelActionItem }>()
);

export const setActionItemNoteFailure = createAction(
  '[Alerts Summary] Set Action Item Note Failure',
  props<{ actionItem: INDModelActionItem }>()
);

export const showRelatedModels = createAction(
  '[Alerts Summary] Show Related Models',
  props<{ assetGuid: string }>()
);
export const showRelatedIssues = createAction(
  '[Alerts Summary] Show Related Issues',
  props<{ assetGuid: string }>()
);

export const downloadModelsRequest = createAction(
  '[Alerts Summary] Download Models Request',
  props<{ gridParams: IGridParameters }>()
);
export const downloadModelsRequestSuccess = createAction(
  '[Alerts Summary] Download Models Request Success'
);
export const downloadModelsRequestFailure = createAction(
  '[Alerts Summary] Download Models Request Failure',
  props<{ error: any }>()
);

export const reloadSummaryData = createAction(
  '[Alerts Summary] Reload Summary Data'
);

export const loadAIAdvice = createAction(
  '[Alerts Summary] Load AI Advice',
  props<{
    application: string;
    assetType: string;
    parameterType: string;
    parameterModifier: string;
  }>()
);

export const loadAIAdviceSuccess = createAction(
  '[Alerts Summary] Load AI Advice Success',
  props<{ response: string }>()
);

export const loadAIAdviceFailure = createAction(
  '[Alerts Summary] Load AI Advice Failure',
  props<{ error: Error }>()
);
