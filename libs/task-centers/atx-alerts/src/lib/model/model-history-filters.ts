export interface IModelHistoryFilters {
  historyTypes: string[];
  start: Date;
  end: Date;
  user: string;
  note: string;
  favorite: boolean;
}
