import { INDModelSummary, IActionItem } from '@atonix/shared/api';

export interface IActionFlyoutStateChange {
  event:
    | 'ToggleActionFlyout'
    | 'SetWatch'
    | 'ClearWatch'
    | 'ClearAlert'
    | 'Refresh'
    | 'SetCustomWatch'
    | 'SaveDiagnose'
    | 'SaveMaintenance'
    | 'SaveNote'
    | 'ClearDiagnose'
    | 'ClearMaintenance'
    | 'ModelConfig'
    | 'OpModeConfig'
    | 'DiagnosticDrilldown'
    | 'DataExplorer'
    | 'NewIssue'
    | 'ShowRelatedModels'
    | 'ShowRelatedIssues';

  newValue?: boolean | string | number | IActionItem;
  newAction?: string;
  models?: INDModelSummary[];
}
