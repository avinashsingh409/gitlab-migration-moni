export interface IStatusCount {
  Status: string;
  StatusID: number;
  Count: number;
  Percent: number;
}
