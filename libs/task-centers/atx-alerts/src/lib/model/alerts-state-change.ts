import { IAssetMeasurementsSet, IProcessedTrend } from '@atonix/atx-core';

export interface ISelectedViewAsset {
  AssetID: string;
  ModelExtID: string;
  ModelID: string;
}

export interface IAlertStateChange {
  event:
    | 'GetRowData'
    | 'IconChange'
    | 'SelectedTrend'
    | 'RenderModelContextChart'
    | 'ExpandStatus';
  newValue?:
    | ISelectedViewAsset
    | string
    | boolean
    | IProcessedTrend
    | IAssetMeasurementsSet;
}
