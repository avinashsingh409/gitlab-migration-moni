import { INDModelSummary } from '@atonix/shared/api';

export interface INewActionItem {
  model: INDModelSummary;
  state: 'savediagnose' | 'savemaintenance' | 'savenote';
  priority: number;
  expiresAfter: string;
  expiresAfterUnits: string;
  alertLimitRule: string;
  limitValue: string;
  alertLimitRateTimeUnit: string;
  combinedNote: string;
  relativeToExpected: boolean;
  favorite: boolean;
}
