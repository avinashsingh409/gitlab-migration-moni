import { INDModelSummary } from '@atonix/shared/api';

export interface IScreeningViewStateChange {
  event: 'SelectingModel';
  newValue?: INDModelSummary[];
}
