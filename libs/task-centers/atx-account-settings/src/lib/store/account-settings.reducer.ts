import { isNil } from '@atonix/atx-core';
import { IUserAccount } from '@atonix/shared/api';
import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import * as actions from '../store/account-settings.actions';

export const accountSettingsFeatureKey = 'account-settings';

export interface AccountSettingsState {
  userAccount: IUserAccount;
}

export const initialUserAccount: AccountSettingsState = {
  userAccount: {
    UserName: '',
    Email: '',
    Name: '',
    AccountStatus: '',
    MFAStatus: '',
    EmailVerified: false,
    Phone: '',
    PhoneVerified: false,
    IsUserFederated: false,
  },
};

export const reducer = createReducer(
  initialUserAccount,
  on(actions.getUserAccountSuccess, (state, action): AccountSettingsState => {
    return { ...state, userAccount: action.userAccount };
  }),
  on(actions.changePhoneNo, (state, action): AccountSettingsState => {
    const userAccount = { ...state.userAccount };
    userAccount.Phone = '';
    return { ...state, userAccount: userAccount };
  })
);

const selectAccountSettingsState = createFeatureSelector<AccountSettingsState>(
  accountSettingsFeatureKey
);

export const selectUserAccount = createSelector(
  selectAccountSettingsState,
  (state: AccountSettingsState) => state.userAccount
);

export const selectPhoneVerified = createSelector(
  selectAccountSettingsState,
  (state: AccountSettingsState) => state.userAccount.PhoneVerified
);
