/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountFrameworkService, IUserAccount } from '@atonix/shared/api';
import { AuthService, AuthActions } from '@atonix/shared/state/auth';
import { ToastService } from '@atonix/shared/utils';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, switchMap, map, filter, delay, tap } from 'rxjs/operators';
import * as actions from '../store/account-settings.actions';

@Injectable()
export class AccountSettingsEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private accountFrameworkService: AccountFrameworkService,
    private toastService: ToastService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  getUserAccount$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getUserAccount),
      switchMap(() => this.authService.GetAccessToken()),
      switchMap((token) =>
        this.accountFrameworkService.userAccount(token).pipe(
          map((user) => {
            const isFederated =
              user.AccountStatus.toUpperCase() === 'EXTERNAL_PROVIDER';
            return actions.getUserAccountSuccess({
              userAccount: {
                ...user,
                IsUserFederated: isFederated,
              },
            });
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error: Error) =>
            of(actions.getUserAccountFailure({ error }))
          )
        )
      )
    );
  });

  changePassword$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.changePassword),
      switchMap((action) =>
        this.authService
          .changePassword(action.oldPassword, action.newPassword)
          .pipe(
            map((response) => {
              this.toastService.openSnackBar(
                'Password successfully changed.',
                'success'
              );
            }),
            delay(1000), // This will show the message longer before redirecting
            map((response) => AuthActions.logout()),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error: Error) => {
              this.toastService.openSnackBar(error.message, 'error');
              return of(
                actions.changePasswordFailure({
                  error: error,
                })
              );
            })
          )
      )
    );
  });

  setPhoneNumber$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.setPhoneNumber),

      switchMap((action) =>
        this.authService.setPhoneNo(action.phoneNumber).pipe(
          switchMap(() => {
            this.toastService.openSnackBar(
              'Phone number set, a verification code was sent.',
              'success'
            );
            return [actions.getUserAccount(), actions.sendVerificationCode()];
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((err) =>
            of(actions.setPhoneNumberFailure({ error: err.error }))
          )
        )
      )
    );
  });

  setPhoneNumberFailed$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.setPhoneNumberFailure),
        tap((action) => {
          this.toastService.openSnackBar(action.error, 'error');
          console.log(action.error);
        })
      );
    },
    { dispatch: false }
  );

  verifyPhoneNumber$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.verifyPhoneNumber),

      switchMap((action) =>
        this.authService.verifyPhone(action.verificationCode).pipe(
          map((n) => {
            this.toastService.openSnackBar('Phone number verified', 'success');
            this.router.navigate(['../account-settings'], {
              relativeTo: this.activatedRoute,
            });
            return actions.getUserAccount();
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((err) =>
            of(actions.verifyPhoneNumberFailure({ error: err.error }))
          )
        )
      )
    );
  });

  verifyPhoneNumberFailed$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.verifyPhoneNumberFailure),
        tap((action) => {
          this.toastService.openSnackBar(action.error, 'error');
          console.log(action.error);
        })
      );
    },
    { dispatch: false }
  );

  sendVerificationCode$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.sendVerificationCode),
        tap(() => {
          this.authService.sendVerificationCode().subscribe((res: any) => {
            if (!res.success) {
              const error =
                res.error ?? 'Could not resend phone verification code.';
              this.toastService.openSnackBar(error, 'error');
              console.log(error);
            }
          });
        })
      );
    },
    { dispatch: false }
  );

  resendVerificationCode$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.resendVerificationCode),
        tap(() => {
          this.toastService.openSnackBar(
            'Phone Number Verification Code Sent Again',
            'info'
          );
          this.authService.sendVerificationCode().subscribe((res: any) => {
            if (!res.success) {
              const error =
                res.error ?? 'Could not resend phone verification code.';
              this.toastService.openSnackBar(error, 'error');
              console.log(error);
            }
          });
        })
      );
    },
    { dispatch: false }
  );

  removePhoneNumber$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.removePhoneNumber),
      switchMap(() =>
        this.authService.removePhoneNo().pipe(
          map((res) => {
            this.toastService.openSnackBar('Phone number removed', 'success');
            return actions.getUserAccount();
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error) => {
            return of(actions.removePhoneNumberFailed({ error }));
          })
        )
      )
    );
  });

  removePhoneNumberFailed$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.removePhoneNumberFailed),
        tap((action) => {
          this.toastService.openSnackBar(action.error, 'error');
          console.log(action.error);
        })
      );
    },
    { dispatch: false }
  );

  configureMFAWithSMS$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.configureMFAWithSMS),
      switchMap((action) =>
        this.authService.configureMFAWithSMS().pipe(
          map((response) => {
            this.toastService.openSnackBar(
              'Multi-Factor Authentication set to use SMS',
              'success'
            );
            return actions.getUserAccount();
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error: Error) => {
            this.toastService.openSnackBar('Cannot configure MFA', 'error');
            return of(
              actions.configureMFAWithSMSFailed({
                error,
              })
            );
          })
        )
      )
    );
  });

  disableMFA$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.disableMFA),
      switchMap((action) =>
        this.authService.disableMFA().pipe(
          map((response) => {
            this.toastService.openSnackBar('MFA Cleared', 'success');
            return actions.getUserAccount();
          }),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error: Error) => {
            this.toastService.openSnackBar('Could not clear MFA', 'error');
            return of(
              actions.disableMFAFailed({
                error,
              })
            );
          })
        )
      )
    );
  });
}
