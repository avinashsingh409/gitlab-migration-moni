import { IUserAccount } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const getUserAccount = createAction(
  '[Account Settings] Get User Account'
);
export const getUserAccountSuccess = createAction(
  '[Account Settings] Get User Account Success',
  props<{ userAccount: IUserAccount }>()
);
export const getUserAccountFailure = createAction(
  '[Account Settings] Get User Account Failure',
  props<{ error: Error }>()
);

export const changePassword = createAction(
  '[Account Settings] Change Password',
  props<{ oldPassword: string; newPassword: string }>()
);
export const changePasswordSuccess = createAction(
  '[Account Settings] Change Password Success'
);
export const changePasswordFailure = createAction(
  '[Account Settings] Change Password Failure',
  props<{ error: Error }>()
);

export const setPhoneNumber = createAction(
  '[Account Settings] Set Phone Number',
  props<{ phoneNumber: string }>()
);

export const setPhoneNumberFailure = createAction(
  '[Account Settings] Set Phone Number Failure',
  props<{ error: string }>()
);

export const sendVerificationCode = createAction(
  '[Account Settings] Sending Verification Code'
);

export const resendVerificationCode = createAction(
  '[Account Settings] Resending Verification Code'
);

export const verifyPhoneNumber = createAction(
  '[Account Settings] Verify Phone Number',
  props<{ verificationCode: string }>()
);

export const verifyPhoneNumberFailure = createAction(
  '[Account Settings] Verify Phone Number Failure',
  props<{ error: string }>()
);

export const removePhoneNumber = createAction(
  '[Account Settings] Remove Phone Number'
);

export const removePhoneNumberFailed = createAction(
  '[Account Settings] Remove Phone Number Failed',
  props<{ error: string }>()
);

export const changePhoneNo = createAction(
  '[Account Settings] Change Phone Number'
);

export const configureMFAWithSMS = createAction(
  '[Account Settings] Configure MFA With SMS'
);

export const configureMFAWithSMSFailed = createAction(
  '[Account Settings] Configure MFA With SMS Failed',
  props<{ error: Error }>()
);

export const disableMFA = createAction('[Account Settings] Disable MFA');

export const disableMFAFailed = createAction(
  '[Account Settings] Disable MFA Failed',
  props<{ error: Error }>()
);
