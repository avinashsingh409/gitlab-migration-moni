import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { changePassword } from '../../../store/account-settings.actions';

@Component({
  selector: 'atx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  public unsubscribe$ = new Subject<void>();
  form: UntypedFormGroup = this.formBuilder.group({
    oldPassword: ['', Validators.required],
    password1: [
      '',
      [
        Validators.minLength(10),
        Validators.required,
        Validators.pattern(
          // eslint-disable-next-line no-useless-escape
          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\^$*.\[\]{}\(\)?\-“!@#%&/,><\’:;|_~`])\S{10,99}$/
        ),
      ],
    ],
    password2: ['', Validators.required],
  });
  constructor(
    private store: Store,
    private formBuilder: UntypedFormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((frm) => {
        const password = frm.password1;
        const confirm = frm.password2;
        if (password) {
          if (password.length < 10) {
            this.form.get('password1')?.setErrors({
              length: true,
            });
          } else {
            const re =
              /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\^$*.[\]{}()?\-“!@#%&/,><’:;|_~`])\S{10,99}$/;
            if (!re.test(password)) {
              this.form.get('password1')?.setErrors({
                invalid: true,
              });
            } else {
              this.form.get('password1')?.setErrors(null);
            }
          }
        }
        if (password !== confirm) {
          this.form.get('password2')?.setErrors({ notMatched: true });
        } else {
          this.form.get('password2')?.setErrors(null);
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  submit() {
    this.store.dispatch(
      changePassword({
        oldPassword: this.form.get('oldPassword')?.value,
        newPassword: this.form.get('password1')?.value,
      })
    );
  }

  cancel() {
    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
  }
}
