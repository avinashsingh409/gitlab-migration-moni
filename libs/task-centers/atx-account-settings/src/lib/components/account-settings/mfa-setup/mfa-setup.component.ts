import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  configureMFAWithSMS,
  disableMFA,
} from '../../../store/account-settings.actions';

@Component({
  selector: 'atx-mfa-setup',
  templateUrl: './mfa-setup.component.html',
  styleUrls: ['./mfa-setup.component.scss'],
})
export class MfaSetupComponent {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor(private store: Store) {}

  configureMFAWithSMS() {
    this.store.dispatch(configureMFAWithSMS());
  }

  disableMFA() {
    this.store.dispatch(disableMFA());
  }
}
