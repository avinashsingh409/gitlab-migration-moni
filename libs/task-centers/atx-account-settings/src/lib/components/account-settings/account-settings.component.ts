import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectUserAccount } from '../../store/account-settings.reducer';
import {
  getUserAccount,
  removePhoneNumber,
} from '../../store/account-settings.actions';

@Component({
  selector: 'atx-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss'],
})
export class AccountSettingsComponent implements OnInit {
  readonly userAccount$ = this.store.select(selectUserAccount);

  constructor(private readonly store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(getUserAccount());
  }

  public removePhoneNumber() {
    this.store.dispatch(removePhoneNumber());
  }
}
