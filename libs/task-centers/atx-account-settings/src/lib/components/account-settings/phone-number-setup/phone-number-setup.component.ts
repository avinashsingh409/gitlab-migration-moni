import { Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import {
  PhoneNumber,
  getCountries,
  getExampleNumber,
  isValidPhoneNumber,
  parsePhoneNumber,
} from 'libphonenumber-js';
import examples from 'libphonenumber-js/examples.mobile.json';
import { AuthService } from '@atonix/shared/state/auth';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ToastService } from '@atonix/shared/utils';
import { NavFacade } from '@atonix/atx-navigation';
import { ActivatedRoute, Router } from '@angular/router';
import * as actions from '../../../store/account-settings.actions';
import { Store } from '@ngrx/store';
import {
  selectPhoneVerified,
  selectUserAccount,
} from '../../../store/account-settings.reducer';
import { IUserAccount } from '@atonix/shared/api';
@Component({
  selector: 'atx-phone-number-setup',
  templateUrl: './phone-number-setup.component.html',
  styleUrls: ['./phone-number-setup.component.scss'],
})
export class PhoneNumberSetupComponent implements OnInit, OnDestroy {
  public phoneForm = this.formBuilder.group({
    phoneNumber: ['', Validators.required],
    verificationCode: ['', Validators.required],
    countryCode: [''],
  });
  public samplephoneNumber!: PhoneNumber | undefined;
  public countryCodes: string[] = [];
  onDestroy$ = new Subject<void>();
  readonly userAccount$ = this.store.select(selectUserAccount);
  readonly phoneVerified$ = this.store.select(selectPhoneVerified);

  constructor(
    private formBuilder: UntypedFormBuilder,
    private toastService: ToastService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store,
    public navFacade: NavFacade
  ) {
    this.countryCodes = getCountries();
  }
  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
  ngOnInit(): void {
    this.phoneForm
      .get('countryCode')
      ?.valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((val) => {
        this.samplephoneNumber = getExampleNumber(val, examples);
      });
  }

  public settingUpPhoneNumber(userAccount: IUserAccount) {
    if (userAccount.Phone && userAccount.PhoneVerified) {
      //show setup
      return true;
    }

    if (!userAccount.Phone && !userAccount.PhoneVerified) {
      //show setup
      return true;
    }

    return false;
  }

  public setPhoneNumber() {
    const phoneNumber = `+${this.samplephoneNumber?.countryCallingCode}${
      this.phoneForm.get('phoneNumber')?.value
    }`;

    if (isValidPhoneNumber(phoneNumber)) {
      this.store.dispatch(actions.setPhoneNumber({ phoneNumber: phoneNumber }));
    } else {
      this.toastService.openSnackBar('Phone number invalid.', 'error');
    }
  }

  public resetInput() {
    this.phoneForm.get('phoneNumber')?.patchValue('');
  }

  public completeVerification() {
    const verificationCode = this.phoneForm.get('verificationCode')?.value;
    this.store.dispatch(actions.verifyPhoneNumber({ verificationCode }));
  }

  public resendVerificationCode() {
    this.userAccount$.pipe(take(1)).subscribe((ua) => {
      const phoneNumber = parsePhoneNumber(ua.Phone);
      if (phoneNumber.isValid()) {
        this.store.dispatch(actions.resendVerificationCode());
      } else {
        this.toastService.openSnackBar('Phone number invalid.', 'error');
      }
    });
  }

  public changephoneNumber() {
    this.store.dispatch(actions.changePhoneNo());
  }

  public cancel() {
    this.store.dispatch(actions.getUserAccount());
    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
  }
}
