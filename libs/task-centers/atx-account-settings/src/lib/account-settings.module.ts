import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { SharedApiModule } from '@atonix/shared/api';
import { SharedUiModule } from '@atonix/shared/ui';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NavigationModule } from '@atonix/atx-navigation';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './components/account-settings/change-password/change-password.component';
import { PhoneNumberSetupComponent } from './components/account-settings/phone-number-setup/phone-number-setup.component';
import { MfaSetupComponent } from './components/account-settings/mfa-setup/mfa-setup.component';
import { StoreModule } from '@ngrx/store';
import {
  accountSettingsFeatureKey,
  reducer,
} from './store/account-settings.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AccountSettingsEffects } from './store/account-settings.effects';
import { NumberOnlyDirective } from './components/account-settings/phone-number-setup/only-number.directive';

@NgModule({
  declarations: [
    AccountSettingsComponent,
    ChangePasswordComponent,
    PhoneNumberSetupComponent,
    MfaSetupComponent,
    NumberOnlyDirective,
  ],
  imports: [
    CommonModule,
    NavigationModule,
    AtxMaterialModule,
    SharedUiModule,
    SharedApiModule,
    SharedUtilsModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature(accountSettingsFeatureKey, reducer),
    EffectsModule.forFeature([AccountSettingsEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: AccountSettingsComponent,
        children: [
          {
            path: 'change-password',
            component: ChangePasswordComponent,
          },
          {
            path: 'phone-setup',
            component: PhoneNumberSetupComponent,
          },
          {
            path: 'mfa-setup',
            component: MfaSetupComponent,
          },
        ],
      },
    ]),
  ],
})
export class AccountSettingsModule {}
