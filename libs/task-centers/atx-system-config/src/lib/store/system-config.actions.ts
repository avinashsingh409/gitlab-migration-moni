/* eslint-disable ngrx/prefer-inline-action-props */
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITreePermissions } from '@atonix/atx-core';
import { createAction, props } from '@ngrx/store';

export const systemInitialize = createAction(
  '[SystemConfig] Initialize',
  props<{ asset?: string }>()
);

export const selectAsset = createAction(
  '[SystemConfig] Select Asset',
  props<{ asset: string }>()
);

export const treeStateChange = createAction(
  '[SystemConfig] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[SystemConfig] Tree Data Request Failure',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[SystemConfig] Asset Tree Size Change',
  props<{ value: number }>()
);
export const permissionsRequest = createAction(
  '[SystemConfig] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[SystemConfig] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[SystemConfig] Permissions Request Failure',
  props<Error>()
);
// Asset Tree Dropdown
export const assetTreeDropdownStateChange = createAction(
  '[SystemConfig - Asset Tree Dropdown] Tree State Change',
  props<ITreeStateChange>()
);

export const assetTreeDropdownDataRequestFailure = createAction(
  '[SystemConfig - Asset Tree Dropdown] Tree Data Request Failure',
  props<Error>()
);
