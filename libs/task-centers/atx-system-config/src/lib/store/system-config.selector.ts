import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  createTreeBuilder,
  getDefaultTree,
  getIDFromSelectedAssets,
  getSelectedNodes,
  ITreeState,
  TrayState,
} from '@atonix/atx-asset-tree';
import * as fromNavigation from './system-config.reducer';

const systemConfigState =
  createFeatureSelector<fromNavigation.SystemConfigState>(
    fromNavigation.systemConfigFeatureKey
  );

export const getAssetTreeState = createSelector(
  systemConfigState,
  (state) => state?.AssetTreeState
);
export const leftTraySize = createSelector(
  systemConfigState,
  (n) => n.TreeSize
);
export const getAssetTreeConfiguration = createSelector(
  systemConfigState,
  (state) => state?.AssetTreeState.treeConfiguration
);

export const selectLeftTrayMode = createSelector(
  getAssetTreeConfiguration,
  (state) => {
    const result: TrayState = state?.pin ? 'side' : 'over';
    return result;
  }
);

export const selectAssetTreeState = createSelector(
  systemConfigState,
  (state) => state.AssetTreeState
);

export const selectAssetTreeNodes = createSelector(
  selectAssetTreeState,
  (state) => state.treeNodes
);

export const getSelectedAsset = createSelector(selectAssetTreeNodes, (state) =>
  getIDFromSelectedAssets(getSelectedNodes(state))
);

export const getSelectedAssetTreeNode = createSelector(
  getAssetTreeState,
  getSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);
export const selectApp = (state: {
  nav: fromNavigation.SystemConfigState;
  asset: any;
}) => state;
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);

export const selectAssetTreeDropdownState = createSelector(
  systemConfigState,
  (state) => state?.AssetTreeDropdownState || null
);

export const selectAssetTreeDropdownConfiguration = createSelector(
  selectAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const selectAssetTreeDropdownNode = createSelector(
  selectAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectAssetTreeDropdownName = createSelector(
  selectAssetTreeDropdownNode,
  (state) => state?.nodeAbbrev
);

export const selectAssetTreeDropdownGlobalId = createSelector(
  selectAssetTreeDropdownNode,
  (state) => state?.data?.AssetGuid
);
