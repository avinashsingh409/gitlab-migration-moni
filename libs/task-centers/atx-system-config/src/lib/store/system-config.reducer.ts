/* eslint-disable ngrx/on-function-explicit-return-type */
import {
  alterAssetTreeState,
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import { createReducer, on } from '@ngrx/store';
import * as actions from './system-config.actions';

export const systemConfigFeatureKey = 'systemConfigNavigation';

export interface SystemConfigState {
  AssetTreeState: ITreeState;
  TreeSize: number;
  AssetTreeDropdownState: ITreeState;
}

export const initialSystemConfigState: SystemConfigState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({
      pin: true,
      collapseOthers: false,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  TreeSize: 250,
  AssetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: true }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
};

export const systemConfigReducer = createReducer(
  initialSystemConfigState,
  on(actions.permissionsRequest, (state) => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        null
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestSuccess, (state, payload) => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: payload.canView,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestFailure, (state) => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        null
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.treeStateChange, (state, payload) => {
    const newPayload = treeRetrievedStateChange(payload);
    return {
      ...state,
      AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
    };
  }),
  on(actions.assetTreeDataRequestFailure, (state) => {
    return { ...state };
  }),
  on(actions.treeSizeChange, (state, payload: { value: number }) => {
    return {
      ...state,
      TreeSize: payload.value,
    };
  }),
  on(actions.assetTreeDropdownStateChange, (state, payload) => {
    const newPayload = treeRetrievedStateChange(payload);
    return {
      ...state,
      AssetTreeDropdownState: alterAssetTreeState(
        state.AssetTreeDropdownState,
        newPayload
      ),
    };
  })
);
