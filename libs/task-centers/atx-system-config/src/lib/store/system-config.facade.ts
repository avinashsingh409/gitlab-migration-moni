/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { select, Store } from '@ngrx/store';
import {
  getAssetTreeConfiguration,
  getSelectedAsset,
  leftTraySize,
  selectAssetTreeDropdownConfiguration,
  selectAssetTreeDropdownGlobalId,
  selectAssetTreeDropdownName,
  selectLeftTrayMode,
} from './system-config.selector';

import * as actions from './system-config.actions';

@Injectable()
export class SystemConfigNavigationFacade {
  constructor(private store: Store<any>) {}

  selectedAsset$ = this.store.pipe(select(getSelectedAsset));
  assetTreeConfiguration$ = this.store.pipe(select(getAssetTreeConfiguration));
  leftTrayMode$ = this.store.pipe(select(selectLeftTrayMode));
  leftTraySize$ = this.store.pipe(select(leftTraySize));

  assetTreeDropdownConfig$ = this.store.select(
    selectAssetTreeDropdownConfiguration
  );
  assetTreeDropdownName$ = this.store.select(selectAssetTreeDropdownName);
  assetTreeDropdownGlobalId$ = this.store.select(
    selectAssetTreeDropdownGlobalId
  );

  selectAsset(asset: string) {
    this.store.dispatch(actions.selectAsset({ asset }));
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.treeStateChange(change));
  }
  treeSizeChange(value: number) {
    this.store.dispatch(actions.treeSizeChange({ value }));
  }

  systemInitialize(asset?: string) {
    this.store.dispatch(actions.systemInitialize({ asset }));
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.assetTreeDropdownStateChange(change));
  }
}
