/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import {
  catchError,
  map,
  mergeMap,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import * as actions from './system-config.actions';
import {
  assetState,
  getAssetTreeState,
  getSelectedAsset,
  selectAssetTreeDropdownState,
} from './system-config.selector';
import {
  NavActions,
  updateRouterIdWithoutReloading,
} from '@atonix/atx-navigation';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
@Injectable()
export class SystemConfigEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private assetTreeModel: AssetTreeModel,
    private activatedRoute: ActivatedRoute,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private router: Router
  ) {}

  initializeSystemConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.systemInitialize),
      map((n) => n.asset),
      withLatestFrom(
        this.store.select(getSelectedAsset),
        this.store.select(assetState)
      ),
      map(([oldAsset, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return appAsset?.toString() || newAsset || oldAsset;
      }),
      switchMap((asset) => [
        actions.permissionsRequest(),
        actions.treeStateChange(selectAsset(asset, false)),
        NavActions.taskCenterLoad({
          taskCenterID: 'data-explorer',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: false,
          asset,
        }),
      ])
    )
  );
  getPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) => actions.permissionsRequestSuccess(permissions)),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error) => of(actions.permissionsRequestFailure(error)))
        )
      )
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.treeStateChange),
      withLatestFrom(this.store.pipe(select(getAssetTreeState))),
      mergeMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => actions.treeStateChange(n),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(actions.assetTreeDataRequestFailure(error))
              )
            )
          )
      )
    )
  );

  selectAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectAsset),
      switchMap((action) => {
        setTimeout(() => {
          updateRouterIdWithoutReloading(
            action.asset,
            this.activatedRoute,
            this.router
          );
        }, 100);
        return [NavActions.setApplicationAsset({ asset: action.asset })];
      })
    )
  );

  assetTreeDropdownStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.assetTreeDropdownStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeDropdownState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.assetTreeDropdownStateChange(n)),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) =>
              of(actions.assetTreeDropdownDataRequestFailure(error))
            )
          )
      )
    )
  );
}
