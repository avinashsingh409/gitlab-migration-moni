import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TimeRangeFilterComponent } from './component/time-range-filter/time-range-filter.component';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NavigationModule } from '@atonix/atx-navigation';
import { AtxMaterialModule } from '@atonix/atx-material';
import { TimeFilterFacade } from './services/time-filter-facade';
import { ReactiveFormsModule } from '@angular/forms';
import { SaveChangesModalComponent } from './component/save-changes-modal/save-changes-modal.component';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { StoreModule } from '@ngrx/store';
import * as fromSystemConfig from './store/system-config.reducer';
import { SystemConfigNavigationFacade } from './store/system-config.facade';
import { EffectsModule } from '@ngrx/effects';
import { SystemConfigEffects } from './store/system-config.effects';
import { SharedUiModule } from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import { DeletePinDialogComponent } from './component/time-range-filter/delete-pin-dialog/delete-pin-dialog/delete-pin-dialog.component';

@NgModule({
  declarations: [
    TimeRangeFilterComponent,
    SaveChangesModalComponent,
    DeletePinDialogComponent,
  ],
  imports: [
    CommonModule,
    AssetTreeModule,
    NavigationModule,
    ReactiveFormsModule,
    AtxMaterialModule,
    SharedUtilsModule,
    SharedUiModule,
    SharedApiModule,
    StoreModule.forFeature(
      fromSystemConfig.systemConfigFeatureKey,
      fromSystemConfig.systemConfigReducer
    ),
    EffectsModule.forFeature([SystemConfigEffects]),
    RouterModule.forChild([
      {
        path: 'time-range',
        component: TimeRangeFilterComponent,
        pathMatch: 'full',
      },
    ]),
  ],
  providers: [TimeFilterFacade, SystemConfigNavigationFacade],
})
export class SystemConfigModule {}
