import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { TimeFilterFacade } from './time-filter-facade';
import { combineTime } from '@atonix/shared/utils';

@Injectable({
  providedIn: 'root',
})
export class TimeFilterFormService implements OnDestroy {
  private onDestroy = new Subject<void>();

  private numberRegex = '-?\\d+(?:\\.\\d+)?';
  isNumber = (n: string | number): boolean =>
    !isNaN(parseFloat(String(n))) && isFinite(Number(n));
  constructor(private timeFilterFacade: TimeFilterFacade) {}

  timeFilterTitle(name: string): UntypedFormControl {
    const filterTitle = new UntypedFormControl(name, [Validators.required]);
    filterTitle.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.timeFilterFacade.updateTitle(value);
      });
    return filterTitle;
  }

  timeFilterDescription(description: string): UntypedFormControl {
    const filterDescription = new UntypedFormControl(description, [
      Validators.required,
    ]);
    filterDescription.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.timeFilterFacade.updateDescription(value);
      });
    return filterDescription;
  }
  buildStartTime(): UntypedFormControl {
    const startTime = new UntypedFormControl(null, [Validators.required]);
    startTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.timeFilterFacade.updateStartTime(value);
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.timeFilterFacade.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return startTime;
  }

  checkStartAndEndTimes(
    startDate: Date,
    startTime: string,
    endDate: Date,
    endTime: string
  ) {
    const start: Date = combineTime(startDate, startTime);
    const end: Date = combineTime(endDate, endTime);
    if (start >= end) {
      this.timeFilterFacade.setStartDateAfterEndDate(true);
    } else {
      this.timeFilterFacade.setStartDateAfterEndDate(false);
    }
  }

  buildEndTime(): UntypedFormControl {
    const endTime = new UntypedFormControl(null, [Validators.required]);
    endTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.timeFilterFacade.updateEndTime(value);
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.timeFilterFacade.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return endTime;
  }
  buildOffset(): UntypedFormControl {
    const impactQuantityControl = new UntypedFormControl(0, [
      Validators.pattern(this.numberRegex),
      Validators.required,
    ]);
    impactQuantityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.timeFilterFacade.updateOffset(value);
      });
    return impactQuantityControl;
  }
  buildSpan(): UntypedFormControl {
    const impactQuantityControl = new UntypedFormControl(0, [
      Validators.pattern(this.numberRegex),
      Validators.required,
    ]);
    impactQuantityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.timeFilterFacade.updateSpan(value);
      });
    return impactQuantityControl;
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
