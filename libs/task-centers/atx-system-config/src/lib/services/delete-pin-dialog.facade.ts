import { Injectable, OnDestroy } from '@angular/core';
import { TimeFilter } from '@atonix/atx-core';
import { ProcessDataFrameworkService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { catchError, distinctUntilChanged, map, take } from 'rxjs/operators';

export interface DeletePinDialogState {
  selectedTimeFilter: TimeFilter;
  selectedTimeFilterTrendsPath: string[];
  deleting: boolean;
  deleted: boolean;
  checkingPinOnTrends: boolean;
}

const _initialState: DeletePinDialogState = {
  selectedTimeFilter: null,
  selectedTimeFilterTrendsPath: [],
  deleting: false,
  deleted: false,
  checkingPinOnTrends: false,
};

let _state: DeletePinDialogState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class DeletePinDialogFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<DeletePinDialogState>(_state);
  private state$ = this.store.asObservable();

  private selectedTimeFilter$ = this.state$.pipe(
    map((state) => state.selectedTimeFilter),
    distinctUntilChanged()
  );

  private selectedTimeFilterTrendsPath$ = this.state$.pipe(
    map((state) => state.selectedTimeFilterTrendsPath),
    distinctUntilChanged()
  );

  private deleting$ = this.state$.pipe(
    map((state) => state.deleting),
    distinctUntilChanged()
  );

  private deleted$ = this.state$.pipe(
    map((state) => state.deleted),
    distinctUntilChanged()
  );

  private checkingPinOnTrends = this.state$.pipe(
    map((state) => state.checkingPinOnTrends),
    distinctUntilChanged()
  );

  vm$: Observable<DeletePinDialogState> = combineLatest([
    this.selectedTimeFilter$,
    this.selectedTimeFilterTrendsPath$,
    this.deleting$,
    this.deleted$,
    this.checkingPinOnTrends,
  ]).pipe(
    map(
      ([
        selectedTimeFilter,
        selectedTimeFilterTrendsPath,
        deleting,
        deleted,
        trendsLoading,
      ]) =>
        ({
          selectedTimeFilter,
          selectedTimeFilterTrendsPath,
          deleting,
          deleted,
          checkingPinOnTrends: trendsLoading,
        } as DeletePinDialogState)
    )
  );
  constructor(
    private processDataFrameworkService: ProcessDataFrameworkService
  ) {}

  init(timeFilter: TimeFilter) {
    this.updateState({
      ..._state,
      selectedTimeFilter: timeFilter,
      selectedTimeFilterTrendsPath: [],
      checkingPinOnTrends: true,
    });

    this.processDataFrameworkService
      .getTrendsPathLinkedToCriteriaObject(
        timeFilter.AssetGlobalID,
        timeFilter.CoID
      )
      .pipe(take(1))
      .subscribe((results) => {
        this.updateState({
          ..._state,
          selectedTimeFilterTrendsPath: results,
          checkingPinOnTrends: false,
        });
      });
  }

  deletePin() {
    this.updateState({
      ..._state,
      deleting: true,
    });
    this.selectedTimeFilter$.pipe(take(1)).subscribe((selectedTimeFilter) => {
      this.processDataFrameworkService
        .deleteTimeFilter(selectedTimeFilter)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe((result) => {
          if (result) {
            this.updateState({
              ..._state,
              deleted: true,
            });
          }

          this.updateState({
            ..._state,
            deleting: false,
          });
        });
    });
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  private updateState(newState: DeletePinDialogState) {
    this.store.next((_state = newState));
  }
}
