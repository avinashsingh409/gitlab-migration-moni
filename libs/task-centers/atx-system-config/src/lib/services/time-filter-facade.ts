/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  take,
  takeUntil,
} from 'rxjs/operators';
import * as moment from 'moment';

import {
  checkStartAndEndTimes,
  combineTime,
  ToastService,
} from '@atonix/shared/utils';
import { UntypedFormControl } from '@angular/forms';
import { TimeErrorStateMatcher } from './time-error-state-matcher';
import {
  LegacyDataFilter,
  ReferenceType,
  ShiftType,
  TemporalType,
  TimeFilter,
} from '@atonix/atx-core';
import {
  AssetFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';

export interface TimeFilterState {
  timeFilters: TimeFilter[];
  filteredTimeFilters: TimeFilter[];
  selectedAsset: string;
  searchFilterState: string;
  selectedTimeFilter: TimeFilter;
  selectedTimeFilterID: number;
  savedTimeFilterID: number;
  relativeToTypes: ReferenceType[];
  relativeToID: number;
  startTypes: ShiftType[];
  startTypeID: number;
  temporalTypes: TemporalType[];
  filterTypeID: number;
  offset: number;
  offsetID: number;
  span: number;
  spanID: number;
  title: string;
  description: string;
  originalStartDate: Date;
  originalEndDate: Date;
  startTime: string;
  startDate: Date;
  endTime: string;
  endDate: Date;
  startDateAfterEndDate: boolean;
  startPickerStartAt: Date;
  endPickerStartAt: Date;
  invalidStartDateFormat: boolean;
  invalidEndDateFormat: boolean;
  mockStartTime: Date;
  mockEndTime: Date;
  startMin: Date;
  startMax: Date;
  endMin: Date;
  endMax: Date;
  loading: boolean;
  saving: boolean;
  pristine: boolean;
  noResults: boolean;
  deletable: boolean;
  selectedTimeFilterTrendsPath: string[];
}

const _initialState: TimeFilterState = {
  timeFilters: null,
  filteredTimeFilters: null,
  selectedAsset: null,
  searchFilterState: null,
  selectedTimeFilter: null,
  selectedTimeFilterID: 0,
  savedTimeFilterID: null,
  relativeToTypes: null,
  relativeToID: 0,
  startTypes: null,
  startTypeID: 0,
  temporalTypes: null,
  filterTypeID: 0,
  offset: 0,
  offsetID: 0,
  span: 0,
  spanID: 0,
  title: '',
  description: '',
  originalStartDate: null,
  originalEndDate: null,
  startTime: '',
  startDate: null,
  endTime: '',
  endDate: null,
  startDateAfterEndDate: false,
  startPickerStartAt: null,
  endPickerStartAt: null,
  invalidStartDateFormat: false,
  invalidEndDateFormat: false,
  startMin: null,
  startMax: null,
  mockStartTime: null,
  mockEndTime: null,
  endMin: null,
  endMax: null,
  loading: true,
  saving: false,
  pristine: true,
  noResults: false,
  deletable: false,
  selectedTimeFilterTrendsPath: null,
};
let _state: TimeFilterState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class TimeFilterFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<TimeFilterState>(_state);
  private state$ = this.store.asObservable();
  startDateMatcher: TimeErrorStateMatcher;
  endDateMatcher: TimeErrorStateMatcher;

  private timeFilters$ = this.state$.pipe(
    map((state) => state.timeFilters),
    distinctUntilChanged()
  );
  private filteredTimeFilters$ = this.state$.pipe(
    map((state) => state.filteredTimeFilters),
    distinctUntilChanged()
  );
  private selectedAsset$ = this.state$.pipe(
    map((state) => state.selectedAsset),
    distinctUntilChanged()
  );
  private selectedTimeFilter$ = this.state$.pipe(
    map((state) => state.selectedTimeFilter),
    distinctUntilChanged()
  );
  private selectedTimeFilterID$ = this.state$.pipe(
    map((state) => state.selectedTimeFilterID),
    distinctUntilChanged()
  );
  private savedTimeFilterID$ = this.state$.pipe(
    map((state) => state.savedTimeFilterID),
    distinctUntilChanged()
  );
  private relativeToTypes$ = this.state$.pipe(
    map((state) => state.relativeToTypes),
    distinctUntilChanged()
  );
  private relativeToID$ = this.state$.pipe(
    map((state) => state.relativeToID),
    distinctUntilChanged()
  );
  private temporalTypes$ = this.state$.pipe(
    map((state) => state.temporalTypes),
    distinctUntilChanged()
  );
  private startTypes$ = this.state$.pipe(
    map((state) => state.startTypes),
    distinctUntilChanged()
  );
  private startTypeID$ = this.state$.pipe(
    map((state) => state.startTypeID),
    distinctUntilChanged()
  );
  private offset$ = this.state$.pipe(
    map((state) => state.offset),
    distinctUntilChanged()
  );
  private offsetID$ = this.state$.pipe(
    map((state) => state.offsetID),
    distinctUntilChanged()
  );
  private span$ = this.state$.pipe(
    map((state) => state.span),
    distinctUntilChanged()
  );
  private spanID$ = this.state$.pipe(
    map((state) => state.spanID),
    distinctUntilChanged()
  );
  loading$ = this.state$.pipe(
    map((state) => state.loading),
    distinctUntilChanged()
  );
  private title$ = this.state$.pipe(
    map((state) => state.title),
    distinctUntilChanged()
  );
  private description$ = this.state$.pipe(
    map((state) => state.description),
    distinctUntilChanged()
  );
  private originalStartDate$ = this.state$.pipe(
    map((state) => state.originalStartDate),
    distinctUntilChanged()
  );
  private originalEndDate$ = this.state$.pipe(
    map((state) => state.originalEndDate),
    distinctUntilChanged()
  );
  private startTime$ = this.state$.pipe(
    map((state) => state.startTime),
    distinctUntilChanged()
  );
  private startDate$ = this.state$.pipe(
    map((state) => state.startDate),
    distinctUntilChanged()
  );
  private endTime$ = this.state$.pipe(
    map((state) => state.endTime),
    distinctUntilChanged()
  );
  private endDate$ = this.state$.pipe(
    map((state) => state.endDate),
    distinctUntilChanged()
  );
  private startDateAfterEndDate$ = this.state$.pipe(
    map((state) => state.startDateAfterEndDate),
    distinctUntilChanged()
  );
  private startPickerStartAt$ = this.state$.pipe(
    map((state) => state.startPickerStartAt),
    distinctUntilChanged()
  );
  private endPickerStartAt$ = this.state$.pipe(
    map((state) => state.endPickerStartAt),
    distinctUntilChanged()
  );
  private invalidStartDateFormat$ = this.state$.pipe(
    map((state) => state.invalidStartDateFormat),
    distinctUntilChanged()
  );
  private invalidEndDateFormat$ = this.state$.pipe(
    map((state) => state.invalidEndDateFormat),
    distinctUntilChanged()
  );
  private startMin$ = this.state$.pipe(
    map((state) => state.startMin),
    distinctUntilChanged()
  );
  private startMax$ = this.state$.pipe(
    map((state) => state.startMax),
    distinctUntilChanged()
  );
  private mockStartTime$ = this.state$.pipe(
    map((state) => state.mockStartTime),
    distinctUntilChanged()
  );
  private mockEndTime$ = this.state$.pipe(
    map((state) => state.mockEndTime),
    distinctUntilChanged()
  );
  private endMin$ = this.state$.pipe(
    map((state) => state.endMin),
    distinctUntilChanged()
  );
  private endMax$ = this.state$.pipe(
    map((state) => state.endMax),
    distinctUntilChanged()
  );
  private pristine$ = this.state$.pipe(
    map((state) => state.pristine),
    distinctUntilChanged()
  );
  private filterTypeID$ = this.state$.pipe(
    map((state) => state.filterTypeID),
    distinctUntilChanged()
  );
  private noResults$ = this.state$.pipe(
    map((state) => state.noResults),
    distinctUntilChanged()
  );
  private saving$ = this.state$.pipe(
    map((state) => state.saving),
    distinctUntilChanged()
  );
  private searchFilterState$ = this.state$.pipe(
    map((state) => state.searchFilterState),
    distinctUntilChanged()
  );

  private deletable$ = this.state$.pipe(
    map((state) => state.deletable),
    distinctUntilChanged()
  );

  private selectedTimeFilterTrendsPath$ = this.state$.pipe(
    map((state) => state.selectedTimeFilterTrendsPath),
    distinctUntilChanged()
  );
  vm$: Observable<TimeFilterState> = combineLatest([
    this.timeFilters$,
    this.filteredTimeFilters$,
    this.selectedAsset$,
    this.searchFilterState$,
    this.selectedTimeFilter$,
    this.selectedTimeFilterID$,
    this.savedTimeFilterID$,
    this.loading$,
    this.title$,
    this.description$,
    this.relativeToTypes$,
    this.relativeToID$,
    this.startTypes$,
    this.startTypeID$,
    this.temporalTypes$,
    this.filterTypeID$,
    this.offset$,
    this.offsetID$,
    this.span$,
    this.spanID$,
    this.originalStartDate$,
    this.originalEndDate$,
    this.startTime$,
    this.startDate$,
    this.endTime$,
    this.endDate$,
    this.startDateAfterEndDate$,
    this.startPickerStartAt$,
    this.endPickerStartAt$,
    this.invalidStartDateFormat$,
    this.invalidEndDateFormat$,
    this.mockStartTime$,
    this.mockEndTime$,
    this.startMin$,
    this.startMax$,
    this.endMin$,
    this.endMax$,
    this.noResults$,
    this.pristine$,
    this.saving$,
    this.deletable$,
    this.selectedTimeFilterTrendsPath$,
  ]).pipe(
    map(
      ([
        timeFilters,
        filteredTimeFilters,
        selectedAsset,
        searchFilterState,
        selectedTimeFilter,
        selectedTimeFilterID,
        savedTimeFilterID,
        loading,
        title,
        description,
        relativeToTypes,
        relativeToID,
        startTypes,
        startTypeID,
        temporalTypes,
        filterTypeID,
        offset,
        offsetID,
        span,
        spanID,
        originalStartDate,
        originalEndDate,
        startTime,
        startDate,
        endTime,
        endDate,
        startDateAfterEndDate,
        startPickerStartAt,
        endPickerStartAt,
        invalidStartDateFormat,
        invalidEndDateFormat,
        mockStartTime,
        mockEndTime,
        startMin,
        startMax,
        endMin,
        endMax,
        noResults,
        pristine,
        saving,
        deletable,
        selectedTimeFilterTrendsPath,
      ]) =>
        ({
          timeFilters,
          filteredTimeFilters,
          selectedAsset,
          searchFilterState,
          selectedTimeFilter,
          selectedTimeFilterID,
          savedTimeFilterID,
          loading,
          title,
          description,
          relativeToTypes,
          relativeToID,
          startTypes,
          startTypeID,
          temporalTypes,
          filterTypeID,
          offset,
          offsetID,
          span,
          spanID,
          originalStartDate,
          originalEndDate,
          startTime,
          startDate,
          endTime,
          endDate,
          startDateAfterEndDate,
          startPickerStartAt,
          endPickerStartAt,
          invalidStartDateFormat,
          invalidEndDateFormat,
          mockStartTime,
          mockEndTime,
          startMin,
          startMax,
          endMin,
          endMax,
          noResults,
          pristine,
          saving,
          deletable,
          selectedTimeFilterTrendsPath,
        } as TimeFilterState)
    )
  );

  constructor(
    private snackBarService: ToastService,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private assetFrameworkService: AssetFrameworkService
  ) {}

  init(asset: string) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (asset && vm.selectedAsset !== asset) {
        this.updateState({ ..._state, noResults: false, loading: true });
        this.processDataFrameworkService
          .getCriteriaWithDataTimeFiltersAndParams(asset)
          .pipe(take(1))
          .subscribe((result) => {
            if (result && result.length > 0) {
              let filteredTimeFilters = result;
              if (vm.searchFilterState && vm.searchFilterState !== '') {
                filteredTimeFilters = result.filter(
                  (type) =>
                    type.Title.toLowerCase().indexOf(
                      vm.searchFilterState.toLowerCase()
                    ) >= 0
                );
              }
              this.updateState({
                ..._state,
                timeFilters: result,
                filteredTimeFilters: filteredTimeFilters
                  ? filteredTimeFilters
                  : [],
              });
              this.completeInit();
            } else {
              this.updateState({
                ..._state,
                timeFilters: null,
                filteredTimeFilters: null,
                noResults: true,
                loading: false,
              });
            }
          });
        this.updateState({ ..._state, selectedAsset: asset });
      }
      if (!vm.startTypes) {
        this.processDataFrameworkService
          .getCriteriaObjectTypes()
          .pipe(take(1))
          .subscribe((criteriaObjects) => {
            // criteriaObjects.ShiftTypes.splice(0, 1); // remove reference type
            this.updateState({
              ..._state,
              startTypes: criteriaObjects.ShiftTypes,
              relativeToTypes: criteriaObjects.ReferenceTypes,
              temporalTypes: criteriaObjects.TemporalTypes,
            });
            this.completeInit();
          });
      }
    });
  }

  completeInit() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.relativeToTypes && vm.filteredTimeFilters) {
        this.updateState({ ..._state, loading: false });
      }
    });
  }

  buildSearchTimeFilter(): UntypedFormControl {
    const searchTimeFilter = new UntypedFormControl(null);
    searchTimeFilter.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (value) {
          this.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.timeFilters) {
              const newfiltered = vm.timeFilters.filter(
                (type) =>
                  type.Title.toLowerCase().indexOf(value.toLowerCase()) >= 0
              );
              this.updateState({
                ..._state,
                searchFilterState: value,
                filteredTimeFilters: newfiltered ? newfiltered : [],
              });
            } else {
              this.updateState({
                ..._state,
                searchFilterState: value,
              });
            }
          });
        } else {
          this.vm$.pipe(take(1)).subscribe((vm) => {
            this.updateState({
              ..._state,
              searchFilterState: null,
              filteredTimeFilters:
                vm.timeFilters?.length > 0 ? [...vm.timeFilters] : [],
            });
          });
        }
      });
    return searchTimeFilter;
  }

  setStartDateAfterEndDate(startDateAfterEndDate: boolean) {
    this.updateState({ ..._state, startDateAfterEndDate });
  }
  updateEndTime(endTime: string) {
    this.updateState({ ..._state, endTime, pristine: false });
  }
  updateStartTime(startTime: string) {
    this.updateState({ ..._state, startTime, pristine: false });
  }
  updateFilterType(filterTypeID: number) {
    this.updateState({
      ..._state,
      filterTypeID,
      mockStartTime: null,
      mockEndTime: null,
      pristine: false,
    });
  }
  updateTitle(title: string) {
    this.updateState({ ..._state, title, pristine: false });
  }
  updateDescription(description: string) {
    this.updateState({ ..._state, description, pristine: false });
  }
  updateSelectedTimeFilter(selectedTimeFilter: TimeFilter) {
    this.updateState({ ..._state, selectedTimeFilter, pristine: false });
  }
  updateOffset(offset: number) {
    this.updateState({ ..._state, offset, pristine: false });
  }
  updateSpan(span: number) {
    this.updateState({ ..._state, span, pristine: false });
  }
  updatePristine(pristine: boolean) {
    if (pristine) {
      this.endDateMatcher = new TimeErrorStateMatcher(false);
      this.startDateMatcher = new TimeErrorStateMatcher(false);
    }
    this.updateState({ ..._state, pristine });
  }

  updateTimeFilter(
    title: string,
    description: string,
    startDate: Date,
    endDate: Date,
    startMin: Date,
    startMax: Date,
    endMin: Date,
    endMax: Date,
    startDateAfterEndDate: boolean,
    startTypeID: number,
    relativeToID: number,
    offset: number,
    offsetID: number,
    span: number,
    spanID: number,
    filterTypeID: number
  ) {
    this.updateState({
      ..._state,
      pristine: true,
      title,
      description,
      savedTimeFilterID: null,
      originalStartDate: startDate,
      originalEndDate: endDate,
      startDate: startDate,
      startTime: moment(startDate).format('HH:mm'),
      endDate: endDate,
      endTime: moment(endDate).format('HH:mm'),
      startMin: startMin,
      startMax: startMax,
      endMin: endMin,
      endMax: endMax,
      startDateAfterEndDate,
      startTypeID,
      relativeToID,
      offset,
      offsetID,
      span,
      spanID,
      filterTypeID,
      mockEndTime: null,
      mockStartTime: null,
      invalidEndDateFormat: false,
      invalidStartDateFormat: false,
    });
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  updateStartType(startTypeID: number) {
    this.updateState({ ..._state, startTypeID, pristine: false });
  }
  updateSpanID(spanID: number) {
    this.updateState({ ..._state, spanID, pristine: false });
  }
  updateOffsetID(offsetID: number) {
    this.updateState({ ..._state, offsetID, pristine: false });
  }
  updateRelativeToID(relativeToID: number) {
    this.updateState({ ..._state, relativeToID, pristine: false });
  }
  setTimeFilterID(selectedID: number) {
    if (selectedID === -1) {
      this.updateState({
        ..._state,
        selectedTimeFilter: null,
        selectedTimeFilterID: -1,
        noResults: false, // creating a new Time Filter means there is now a result
        deletable: false,
      });
    } else {
      const selectedTimeFilter = _state.timeFilters.find(
        (i) => i.CoDFID === selectedID
      );
      if (selectedTimeFilter) {
        this.updateState({
          ..._state,
          selectedTimeFilter,
          selectedTimeFilterID: selectedID,
          deletable: false,
        });

        this.assetFrameworkService
          .getAssetFromGuid(selectedTimeFilter.AssetGlobalID)
          .pipe(take(1))
          .subscribe((asset) => {
            if (asset) {
              this.updateState({
                ..._state,
                deletable: true,
              });
            }
          });
      }
    }
  }

  updateAsset(globalId: string) {
    if (
      _state.timeFilters &&
      _state.selectedTimeFilter?.AssetGlobalID &&
      _state.selectedTimeFilter?.AssetGlobalID !== globalId
    ) {
      const newTimeFilters = [..._state.timeFilters]?.map((timeFilter) => {
        const newTimeFilter = { ...timeFilter };
        if (timeFilter.CoDFID === _state.selectedTimeFilterID) {
          newTimeFilter.AssetGlobalID = globalId;
        }
        return newTimeFilter;
      });

      this.updateState({
        ..._state,
        pristine: false,
        timeFilters: newTimeFilters,
      });
    }
  }

  saveChanges(assetGlobalID: any) {
    this.updateState({ ..._state, saving: true });
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const saveObject: TimeFilter = {
        CoID: vm.selectedTimeFilter?.CoID || -1,
        CoDFID: vm.selectedTimeFilter?.CoDFID || -1,
        CoDFTimeFilterID: vm.selectedTimeFilter?.CoDFTimeFilterID || -1,
        IsPrivate: false,
        ExecuteOrder: 1,
        CoDFTimeFilterTypeID: vm.filterTypeID,
        AssetGlobalID: assetGlobalID,
        UniqueKey: null, // not needed for save
        Title: vm.title,
        Description: vm.description,
        TFParams: null,
        ChangedBy: null,
        ChangeDate: null,
      };
      if (vm.filterTypeID === 1) {
        const start: Date = combineTime(vm.startDate, vm.startTime);
        const end: Date = combineTime(vm.endDate, vm.endTime);
        saveObject.TFParams = [
          {
            CoDFTimeParamsID: -1,
            PName: 'AbsoluteStart',
            PValue: start.toISOString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'AbsoluteEnd',
            PValue: end.toISOString(),
          },
        ];
      } else {
        saveObject.TFParams = [
          {
            CoDFTimeParamsID: -1,
            PName: 'offset',
            PValue: vm.offset.toString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'offsettemporaltypeid',
            PValue: vm.offsetID.toString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'referencetypeid',
            PValue: vm.relativeToID.toString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'shifttypeid',
            PValue: vm.startTypeID.toString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'span',
            PValue: vm.span.toString(),
          },
          {
            CoDFTimeParamsID: -1,
            PName: 'spantemporaltypeid',
            PValue: vm.spanID.toString(),
          },
        ];
      }
      this.processDataFrameworkService
        .saveParams(saveObject)
        .pipe(take(1))
        .subscribe(
          (val) => {
            this.snackBarService.openSnackBar('Saved successfully.', 'success');
            this.updateState({
              ..._state,
              savedTimeFilterID: val?.CoDFID,
              saving: false,
              loading: true,
            });
            this.processDataFrameworkService
              .getCriteriaWithDataTimeFiltersAndParams(vm.selectedAsset)
              .pipe(take(1))
              .subscribe((result) => {
                if (result && result.length > 0) {
                  let filteredTimeFilters = result;
                  if (vm.searchFilterState && vm.searchFilterState !== '') {
                    filteredTimeFilters = result.filter(
                      (type) =>
                        type.Title.toLowerCase().indexOf(
                          vm.searchFilterState.toLowerCase()
                        ) >= 0
                    );
                  }
                  this.updateState({
                    ..._state,
                    timeFilters: result,
                    filteredTimeFilters: filteredTimeFilters
                      ? filteredTimeFilters
                      : [],
                  });
                  this.completeInit();
                } else {
                  this.updateState({
                    ..._state,
                    timeFilters: null,
                    filteredTimeFilters: null,
                    noResults: true,
                    loading: false,
                  });
                }
              });
          },
          (error: unknown) => {
            this.snackBarService.openSnackBar('An error occurred.', 'error');
            console.error(JSON.stringify(error));
            this.updateState({ ..._state, saving: false });
          }
        );
    });
  }

  validateTimeFilter() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.filterTypeID === 1) {
        // no validation needed
        return;
        // fixed. 2 TFParams
      } else if (vm.filterTypeID === 2) {
        const legacyObject: LegacyDataFilter = {
          Description: vm.description,
          Title: vm.title,
          CoDFID: -1,
          CreateDate: null,
          ChangeDate: null,
          IsPrivate: false,
          ExcludeDaysFilter: null,
          ExcludeHoursFilter: null,
          AssetID: null,
          TimeFilter: {
            CoDFID: -1,
            CoDFTimeFilterID: -1,
            CoDFTimeFilterTypeID: 2,
            ExecuteOrder: 1,
            TFParams: [
              {
                CoDFTimeParamsID: -1,
                PName: 'offset',
                PValue: vm.offset.toString(),
              },
              {
                CoDFTimeParamsID: -1,
                PName: 'offsettemporaltypeid',
                PValue: vm.offsetID.toString(),
              },
              {
                CoDFTimeParamsID: -1,
                PName: 'referencetypeid',
                PValue: vm.relativeToID.toString(),
              },
              {
                CoDFTimeParamsID: -1,
                PName: 'shifttypeid',
                PValue: vm.startTypeID.toString(),
              },
              {
                CoDFTimeParamsID: -1,
                PName: 'span',
                PValue: vm.span.toString(),
              },
              {
                CoDFTimeParamsID: -1,
                PName: 'spantemporaltypeid',
                PValue: vm.spanID.toString(),
              },
            ],
          },
        };
        this.processDataFrameworkService
          .testEndpoint(legacyObject)
          .pipe(take(1))
          .subscribe((result) => {
            const endTime = new Date(result.End);
            const startTime = new Date(result.Start);
            this.updateState({
              ..._state,
              mockEndTime: endTime,
              mockStartTime: startTime,
            });
          });
        // relative. 6 TFParams
      } else return;
    });
  }
  startDateChanged(startDate: Date) {
    let invalidStartDateFormat = false;
    if (!startDate) {
      invalidStartDateFormat = true;
      this.startDateMatcher = new TimeErrorStateMatcher(true);
    }
    if (
      moment(startDate, 'MM/DD/YYYY', true).isValid() ||
      moment(startDate as unknown as string, 'M/D/YYYY', true).isValid()
    ) {
      this.startDateMatcher = new TimeErrorStateMatcher(false);
      this.updateState({
        ..._state,
        pristine: false,
        endMin: startDate,
        startPickerStartAt: startDate,
        startDate,
        invalidStartDateFormat,
      });
    } else {
      this.updateState({
        ..._state,
        startPickerStartAt: startDate,
        pristine: false,
        startDate,
        invalidStartDateFormat,
      });
    }
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const isValid = checkStartAndEndTimes(
        vm.startDate,
        vm.startTime,
        vm.endDate,
        vm.endTime
      );
      this.setStartDateAfterEndDate(isValid);
    });
  }

  endDateChanged(endDate: Date) {
    let invalidEndDateFormat = false;
    if (!endDate) {
      invalidEndDateFormat = true;
      this.endDateMatcher = new TimeErrorStateMatcher(true);
    }
    if (
      moment(endDate, 'MM/DD/YYYY', true).isValid() ||
      moment(endDate as unknown as string, 'M/D/YYYY', true).isValid()
    ) {
      this.endDateMatcher = new TimeErrorStateMatcher(false);
      this.updateState({
        ..._state,
        pristine: false,
        startMax: endDate,
        endPickerStartAt: endDate,
        endDate,
        invalidEndDateFormat,
      });
    } else {
      this.updateState({
        ..._state,
        endPickerStartAt: endDate,
        pristine: false,
        endDate,
        invalidEndDateFormat,
      });
    }
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const isValid = checkStartAndEndTimes(
        vm.startDate,
        vm.startTime,
        vm.endDate,
        vm.endTime
      );
      this.setStartDateAfterEndDate(isValid);
    });
  }

  loadDateTimeFilters() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedAsset) {
        this.processDataFrameworkService
          .getCriteriaWithDataTimeFiltersAndParams(vm.selectedAsset)
          .pipe(take(1))
          .subscribe((result) => {
            if (result && result.length > 0) {
              let filteredTimeFilters = result;
              if (vm.searchFilterState && vm.searchFilterState !== '') {
                filteredTimeFilters = result.filter(
                  (type) =>
                    type.Title.toLowerCase().indexOf(
                      vm.searchFilterState.toLowerCase()
                    ) >= 0
                );
              }
              this.updateState({
                ..._state,
                timeFilters: result,
                selectedTimeFilter: filteredTimeFilters[0],
                filteredTimeFilters: filteredTimeFilters
                  ? filteredTimeFilters
                  : [],
              });
              this.completeInit();
            } else {
              this.updateState({
                ..._state,
                timeFilters: null,
                filteredTimeFilters: null,
                noResults: true,
                loading: false,
              });
            }
          });
      }
    });
  }

  private updateState(newState: TimeFilterState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
