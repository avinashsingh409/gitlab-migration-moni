/* eslint-disable rxjs/no-nested-subscribe */
import * as moment from 'moment';
import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import {
  TimeFilterFacade,
  TimeFilterState,
} from '../../services/time-filter-facade';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material/select';
import { TimeFilterFormService } from '../../services/time-filter-form.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SaveChangesModalComponent } from '../save-changes-modal/save-changes-modal.component';
import { getADayAgo, getToday } from '@atonix/shared/utils';
import {
  ITreeConfiguration,
  ITreeStateChange,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { SystemConfigNavigationFacade } from '../../store/system-config.facade';
import { getUniqueIdFromRoute, NavFacade } from '@atonix/atx-navigation';
import { ActivatedRoute } from '@angular/router';
import {
  DefaultOffsetID,
  DefaultRelativeToID,
  DefaultSpanID,
  DefaultStartTypeID,
  DefaultTimeFilter,
  TimeFilter,
} from '@atonix/atx-core';
import { DeletePinDialogComponent } from './delete-pin-dialog/delete-pin-dialog/delete-pin-dialog.component';

@Component({
  selector: 'atx-time-range-filter',
  templateUrl: './time-range-filter.component.html',
  styleUrls: ['./time-range-filter.component.scss'],
  providers: [TimeFilterFacade, TimeFilterFormService],
})
export class TimeRangeFilterComponent implements OnDestroy, AfterViewInit {
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public leftTrayMode$: Observable<'over' | 'push' | 'side'>;
  public leftTraySize$: Observable<number>;
  public leftTrayOpen$: Observable<boolean>;
  public asset$: Observable<string>;
  public vm$: Observable<TimeFilterState>;
  public assetTreeDropdownConfiguration$: Observable<ITreeConfiguration>;
  public assetTreeDropdownName$: Observable<string>;
  private onDestroy = new Subject<void>();
  filterForm: UntypedFormGroup;
  searchTimeFilter: UntypedFormControl;
  selectedAsset: string;
  showAssetTreeDropdown = false;

  constructor(
    public timeFilterFacade: TimeFilterFacade,
    private navFacade: NavFacade,
    private dialog: MatDialog,
    private fb: UntypedFormBuilder,
    private activatedRoute: ActivatedRoute,
    public timeFilterFormService: TimeFilterFormService,
    private systemConfigFacade: SystemConfigNavigationFacade
  ) {
    this.searchTimeFilter = timeFilterFacade.buildSearchTimeFilter();
    this.leftTrayMode$ = systemConfigFacade.leftTrayMode$;
    this.leftTraySize$ = systemConfigFacade.leftTraySize$;
    this.leftTrayOpen$ = navFacade.assetTreeSelected$;
    this.assetTreeConfiguration$ = systemConfigFacade.assetTreeConfiguration$;
    this.asset$ = systemConfigFacade.selectedAsset$;
    this.assetTreeDropdownConfiguration$ =
      systemConfigFacade.assetTreeDropdownConfig$;
    this.assetTreeDropdownName$ = systemConfigFacade.assetTreeDropdownName$;

    this.asset$.pipe(takeUntil(this.onDestroy)).subscribe((asset) => {
      if (asset) {
        this.selectedAsset = asset;
        this.systemConfigFacade.selectAsset(asset);
        this.timeFilterFacade.init(asset);
      }
    });

    this.systemConfigFacade.assetTreeDropdownGlobalId$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((globalId) => {
        if (globalId) {
          this.timeFilterFacade.updateAsset(globalId);
        }
      });

    this.filterForm = this.fb.group({
      filterName: this.timeFilterFormService.timeFilterTitle(''),
      filterDescription: this.timeFilterFormService.timeFilterDescription(''),
      filterSpan: this.timeFilterFormService.buildSpan(),
      filterOffset: this.timeFilterFormService.buildOffset(),
      startTime: this.timeFilterFormService.buildStartTime(),
      startDate: new UntypedFormControl('', []),
      endDate: new UntypedFormControl('', []),
      endTime: this.timeFilterFormService.buildEndTime(),
    });
  }

  ngAfterViewInit() {
    const result = getUniqueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.systemConfigFacade.systemInitialize(result);

    this.vm$ = this.timeFilterFacade.vm$;
    this.timeFilterFacade.loading$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((loading) => {
        if (!loading) {
          this.vm$.pipe(take(1)).subscribe((vm) => {
            setTimeout(() => {
              if (vm.timeFilters) {
                if (vm.savedTimeFilterID) {
                  const newTimeFilter = vm.timeFilters.find(
                    (c) => c.CoDFID === vm.savedTimeFilterID
                  );
                  if (newTimeFilter) {
                    this.timeFilterFacade.setTimeFilterID(newTimeFilter.CoDFID);
                    this.updateFormValues(newTimeFilter);
                  } else {
                    // user saved a time filter, but changed the asset
                    // and it is no longer available. First check if there is
                    // a criteria in their search criteria:
                    if (
                      vm.filteredTimeFilters &&
                      vm.filteredTimeFilters.length > 0
                    ) {
                      this.timeFilterFacade.setTimeFilterID(
                        vm.filteredTimeFilters[0].CoDFID
                      );
                      this.updateFormValues(vm.filteredTimeFilters[0]);
                    } else {
                      // nothing there, just give them the first time filter
                      this.timeFilterFacade.setTimeFilterID(
                        vm.timeFilters[0].CoDFID
                      );
                      this.updateFormValues(vm.timeFilters[0]);
                    }
                  }
                } else {
                  if (
                    vm.filteredTimeFilters &&
                    vm.filteredTimeFilters.length > 0
                  ) {
                    this.timeFilterFacade.setTimeFilterID(
                      vm.filteredTimeFilters[0].CoDFID
                    );
                    this.updateFormValues(vm.filteredTimeFilters[0]);
                  } else {
                    this.timeFilterFacade.setTimeFilterID(
                      vm.timeFilters[0].CoDFID
                    );
                    this.updateFormValues(vm.timeFilters[0]);
                  }
                }
              }
              this.timeFilterFacade.updatePristine(true);
            }, 1);
          });
        }
      });
  }

  saveChanges() {
    this.systemConfigFacade.assetTreeDropdownGlobalId$
      .pipe(take(1))
      .subscribe((asset) => {
        this.timeFilterFacade.saveChanges(asset);
      });
  }

  changeStartType(event: MatSelectChange) {
    if (event?.value) {
      this.timeFilterFacade.updateStartType(event.value);
    }
  }

  changeRelativeTo(event: MatSelectChange) {
    if (event?.value) {
      this.timeFilterFacade.updateRelativeToID(event.value);
    }
  }
  changeSpanUnitOfTime(event: MatSelectChange) {
    if (event?.value) {
      this.timeFilterFacade.updateSpanID(event.value);
    }
  }
  changeOffsetUnitOfTime(event: MatSelectChange) {
    if (event?.value) {
      this.timeFilterFacade.updateOffsetID(event.value);
    }
  }

  changeTimeRangeType(event: MatSelectChange) {
    if (event?.value) {
      this.timeFilterFacade.updateFilterType(event.value);
    }
  }

  createNew() {
    this.updateFormValues(DefaultTimeFilter);
    this.timeFilterFacade.setTimeFilterID(-1);
    this.timeFilterFacade.updatePristine(false);
  }

  validateTimeFilter() {
    this.timeFilterFacade.validateTimeFilter();
  }

  updateFormValues(timeFilter: TimeFilter) {
    this.filterForm
      .get('filterName')
      .patchValue(timeFilter?.Title, { emitEvent: false });
    this.filterForm
      .get('filterDescription')
      .patchValue(timeFilter?.Description, {
        emitEvent: false,
      });
    let startDate = getADayAgo();
    let startTime = moment(startDate).format('HH:mm');
    let endDate = getToday();
    let endTime = moment(endDate).format('HH:mm');
    let startTypeID = DefaultStartTypeID;
    let relativeToID = DefaultRelativeToID;
    let offset = 0;
    let span = 0;
    let offsetID = DefaultOffsetID;
    let spanID = DefaultSpanID;
    const filterTypeID = timeFilter?.CoDFTimeFilterTypeID;
    timeFilter?.TFParams.forEach((params) => {
      if (params.PName.toLowerCase() === 'absolutestart') {
        if (moment(params.PValue).isValid()) {
          startDate = moment(params.PValue).toDate();
          startTime = moment(params.PValue).format('HH:mm');
        }
      } else if (params.PName.toLowerCase() === 'absoluteend') {
        if (moment(params.PValue).isValid()) {
          endDate = moment(params.PValue).toDate();
          endTime = moment(params.PValue).format('HH:mm');
        }
      } else if (params.PName.toLowerCase() === 'shifttypeid') {
        startTypeID = Number(params.PValue);
      } else if (params.PName.toLowerCase() === 'referencetypeid') {
        relativeToID = Number(params.PValue);
      } else if (params.PName.toLowerCase() === 'offset') {
        offset = Number(params.PValue);
      } else if (params.PName.toLowerCase() === 'span') {
        span = Number(params.PValue);
      } else if (params.PName.toLowerCase() === 'offsettemporaltypeid') {
        offsetID = Number(params.PValue);
      } else if (params.PName.toLowerCase() === 'spantemporaltypeid') {
        spanID = Number(params.PValue);
      }
    });

    this.filterForm
      .get('filterOffset')
      .patchValue(offset, { emitEvent: false });
    this.filterForm.get('filterSpan').patchValue(span, { emitEvent: false });
    this.filterForm
      .get('startDate')
      .patchValue(startDate, { emitEvent: false });
    this.filterForm
      .get('startTime')
      .patchValue(startTime, { emitEvent: false });
    this.filterForm.get('endDate').patchValue(endDate, { emitEvent: false });
    this.filterForm.get('endTime').patchValue(endTime, { emitEvent: false });
    let startDateAfterEndDate = false;
    if (startDate.getTime() >= endDate.getTime()) {
      startDateAfterEndDate = true;
    }
    const startMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
    const startMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();
    const endMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
    const endMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();
    this.filterForm.markAsPristine();
    this.filterForm.updateValueAndValidity({ emitEvent: false });
    const assetUniqueKey = timeFilter?.UniqueKey
      ? timeFilter?.UniqueKey
      : this.selectedAsset;
    this.systemConfigFacade.assetTreeDropdownStateChange(
      selectAsset(String(assetUniqueKey), false)
    );
    this.timeFilterFacade.updateTimeFilter(
      timeFilter?.Title,
      timeFilter?.Description,
      startDate,
      endDate,
      startMin,
      startMax,
      endMin,
      endMax,
      startDateAfterEndDate,
      startTypeID,
      relativeToID,
      offset,
      offsetID,
      span,
      spanID,
      filterTypeID
    );
  }

  dismissChanges(selectedTimeFilter: TimeFilter) {
    if (!selectedTimeFilter) {
      this.timeFilterFacade.updateFilterType(-1);
      this.timeFilterFacade.updatePristine(true);
    } else {
      this.updateFormValues(selectedTimeFilter);
      this.timeFilterFacade.setTimeFilterID(selectedTimeFilter.CoDFID);
      this.timeFilterFacade.updatePristine(true);
    }
  }

  selectionChange(event: TimeFilter) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (!vm.pristine) {
        const dialogConfig = new MatDialogConfig();
        const dialogRef = this.dialog.open(
          SaveChangesModalComponent,
          dialogConfig
        );
        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            this.timeFilterFacade.setTimeFilterID(event.CoDFID);
            this.timeFilterFacade.updatePristine(true);
            this.updateFormValues(event);
          }
        });
      } else {
        this.timeFilterFacade.setTimeFilterID(event.CoDFID);
        this.updateFormValues(event);
      }
    });
  }
  endDateChanged(event: Date) {
    this.timeFilterFacade.endDateChanged(event);
  }
  startDateChanged(event: Date) {
    this.timeFilterFacade.startDateChanged(event);
  }
  public onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.systemConfigFacade.treeSizeChange(newSize);
    }
  }

  public onAssetTreeStateChange(change: ITreeStateChange) {
    if (change?.event === 'SelectAsset') {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        if (!vm.pristine) {
          const dialogConfig = new MatDialogConfig();
          const dialogRef = this.dialog.open(
            SaveChangesModalComponent,
            dialogConfig
          );
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              this.systemConfigFacade.treeStateChange(change);
              this.timeFilterFacade.updatePristine(true);
            }
          });
        } else {
          this.systemConfigFacade.treeStateChange(change);
        }
      });
    } else {
      this.systemConfigFacade.treeStateChange(change);
    }
  }
  mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  editAsset() {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.timeFilterFacade.updatePristine(false);
    this.systemConfigFacade.assetTreeDropdownStateChange(change);
  }

  closeAssetTreeDropdown() {
    this.showAssetTreeDropdown = false;
  }

  deletePin(): void {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedTimeFilter) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
          timeFilter: vm.selectedTimeFilter,
        };

        const dialogRef = this.dialog.open(
          DeletePinDialogComponent,
          dialogConfig
        );

        dialogRef.afterClosed().subscribe((isDeleted) => {
          if (isDeleted) {
            this.timeFilterFacade.loadDateTimeFilters();
            this.selectionChange(vm.filteredTimeFilters[0]);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
