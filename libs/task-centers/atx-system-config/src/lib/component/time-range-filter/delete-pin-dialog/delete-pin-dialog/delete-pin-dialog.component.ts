import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import {
  DeletePinDialogFacade,
  DeletePinDialogState,
} from '../../../../services/delete-pin-dialog.facade';

@Component({
  selector: 'atx-delete-pin-dialog',
  templateUrl: './delete-pin-dialog.component.html',
  styleUrls: ['./delete-pin-dialog.component.scss'],
})
export class DeletePinDialogComponent {
  public vm$: Observable<DeletePinDialogState>;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DeletePinDialogComponent>,
    private deletePinDialogFacade: DeletePinDialogFacade
  ) {
    this.vm$ = this.deletePinDialogFacade.vm$;

    this.deletePinDialogFacade.init(this.data.timeFilter);
  }
  deletePin() {
    this.deletePinDialogFacade.deletePin();
  }

  closeAfterSuccessfulDelete() {
    this.deletePinDialogFacade.reset();
    this.dialogRef.close(true);
  }
}
