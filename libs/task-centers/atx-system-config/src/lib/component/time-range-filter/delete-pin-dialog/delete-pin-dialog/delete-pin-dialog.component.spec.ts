import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import {
  DeletePinDialogFacade,
  DeletePinDialogState,
} from '../../../../services/delete-pin-dialog.facade';
import { DeletePinDialogComponent } from './delete-pin-dialog.component';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('DeletePinDialogComponent', () => {
  let component: DeletePinDialogComponent;
  let fixture: ComponentFixture<DeletePinDialogComponent>;

  let mockDeletePinDialogFacade: DeletePinDialogFacade;

  const mockDialogRef = {
    close: jest.fn(),
  };
  const state: DeletePinDialogState = {
    selectedTimeFilter: null,
    selectedTimeFilterTrendsPath: [],
    deleting: false,
    deleted: false,
    checkingPinOnTrends: false,
  };

  beforeEach(() => {
    mockDeletePinDialogFacade = createMockWithValues(DeletePinDialogFacade, {
      vm$: new BehaviorSubject<DeletePinDialogState>(state),
      init: jest.fn(),
      deletePin: jest.fn(),
      reset: jest.fn(),
    });
    TestBed.configureTestingModule({
      declarations: [DeletePinDialogComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: DeletePinDialogFacade,
          useValue: mockDeletePinDialogFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: {
              selectedTimeFilter: null,
              selectedTimeFilterTrendsPath: [],
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletePinDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
