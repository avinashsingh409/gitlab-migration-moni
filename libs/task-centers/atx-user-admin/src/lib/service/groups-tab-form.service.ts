import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { produce } from 'immer';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { RolePermission } from '../model/roles';
import { GroupsTabFacade } from './groups-tab.service.facade';
import { UserAdminRxjsFacade } from './user-admin.rxjs.facade';

@Injectable({
  providedIn: 'root',
})
export class GroupsTabFormService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private groupsTabFacade: GroupsTabFacade,
    private userAdminFacade: UserAdminRxjsFacade
  ) {}

  rolePermission(role: RolePermission): UntypedFormControl {
    const rolePermission = new UntypedFormControl(role.Allow);
    rolePermission.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: boolean) => {
        this.groupsTabFacade.query.rolePermissions$
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((rolePermissions: RolePermission[]) => {
            const newState = produce(
              rolePermissions,
              (draftState: RolePermission[]) => {
                draftState.find(
                  (rolePermission: RolePermission) =>
                    rolePermission.Role.Id === role.Role.Id
                ).Allow = value;
              }
            );
            this.groupsTabFacade.setRolePermissions(newState);
            this.groupsTabFacade.setIsRolesSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });
    return rolePermission;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
