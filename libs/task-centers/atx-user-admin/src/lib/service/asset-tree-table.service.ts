import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AssetNode, UserAdminCoreService } from '@atonix/shared/api';
import { throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AssetTreeTableService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getAssetChildren = (
    assetId: string,
    level: number,
    roleId: number,
    customerId: string
  ): Observable<AssetNode[]> => {
    return this.userAdminCoreService
      .getAssetChildren(assetId, customerId, roleId)
      .pipe(
        take(1),
        map((children: AssetNode[]) =>
          children.map((child: AssetNode) => ({
            ...child,
            Level: level + 1,
          }))
        ),
        // eslint-disable-next-line rxjs/no-implicit-any-catch
        catchError((err) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Mesage);
            }
          }
          return throwError('an error occurred');
        })
      );
  };

  findById(id: string, array: AssetNode[]): AssetNode {
    let result: AssetNode;
    array.some(
      (o: AssetNode) =>
        (o.AssetId === id && (result = o)) ||
        (result = this.findById(id, o.Children || []))
    );
    return result;
  }

  flattenItems = (items: AssetNode[], key: string): AssetNode[] => {
    return items.reduce((flattenedItems: AssetNode[], item: AssetNode) => {
      flattenedItems.push(item);
      if (Array.isArray(item[key])) {
        flattenedItems = flattenedItems.concat(
          this.flattenItems(item[key], key)
        );
      }
      return flattenedItems;
    }, []);
  };
}
