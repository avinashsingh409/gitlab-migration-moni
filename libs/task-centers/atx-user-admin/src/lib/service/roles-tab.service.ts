/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import {
  User,
  UserAdminCoreService,
  Customer,
  Resource,
  RoleDetails,
  AssetNode,
  Role,
} from '@atonix/shared/api';
import { Observable, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RolesTabService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getRoles = (customerId: string): Observable<Role[]> =>
    this.userAdminCoreService.getRoles(customerId).pipe(
      take(1),
      map((roles: Role[]) => roles),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getRoleCustomers = (
    customerId: string,
    inclBranch?: boolean
  ): Observable<Customer[]> =>
    this.userAdminCoreService.getRoleCustomers(customerId, inclBranch).pipe(
      take(1),
      map((customers: Customer[]) => customers),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getUsers = (customerId: string): Observable<User[]> =>
    this.userAdminCoreService.getUsers(customerId).pipe(
      take(1),
      map((users: User[]) => users),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getResources = (): Observable<Resource[]> =>
    this.userAdminCoreService.getResources().pipe(
      take(1),
      map((resources: Resource[]) => resources),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getRoleDetails = (
    roleId: number,
    customerId: string
  ): Observable<RoleDetails> =>
    this.userAdminCoreService.getRoleDetails(roleId, customerId).pipe(
      take(1),
      map((roleDetails: RoleDetails) => roleDetails),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getAssets = (roleId: number, customerId: string): Observable<AssetNode[]> =>
    this.userAdminCoreService.getAssets(roleId, customerId).pipe(
      take(1),
      map((users: AssetNode[]) => users),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );
}
