import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { RolesTabFacadeState } from './roles-tab.facade';

export class RolesTabQuery {
  constructor(private store: BehaviorSubject<RolesTabFacadeState>) {}
  private state$: Observable<RolesTabFacadeState> = this.store.asObservable();

  readonly roles$ = this.state$.pipe(
    map((state) => state.roles),
    distinctUntilChanged()
  );

  readonly selectedRole$ = this.state$.pipe(
    map((state) => state.selectedRole),
    distinctUntilChanged()
  );

  readonly filteredMembers$ = this.state$.pipe(
    map((state) => state.filteredMembers),
    distinctUntilChanged()
  );

  readonly resources$ = this.state$.pipe(
    map((state) => state.resources),
    distinctUntilChanged()
  );

  readonly allResourcePermissions$ = this.state$.pipe(
    map((state) => state.allResourcePermissions),
    distinctUntilChanged()
  );

  readonly gridFilters$ = this.state$.pipe(
    map((state) => state.gridFilters),
    distinctUntilChanged()
  );

  readonly assets$ = this.state$.pipe(
    map((state) => state.assets),
    distinctUntilChanged()
  );

  readonly isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  readonly assetPermissions$ = this.state$.pipe(
    map((state) => state.selectedRole?.AssetPermissions),
    distinctUntilChanged()
  );

  readonly nameIsDirty$ = this.state$.pipe(
    map((state) => state.nameIsDirty),
    distinctUntilChanged()
  );

  readonly appPermsIsDirty$ = this.state$.pipe(
    map((state) => state.appPermsIsDirty),
    distinctUntilChanged()
  );

  readonly assetPermsIsDirty$ = this.state$.pipe(
    map((state) => state.assetPermsIsDirty),
    distinctUntilChanged()
  );

  readonly membershipIsDirty$ = this.state$.pipe(
    map((state) => state.membershipIsDirty),
    distinctUntilChanged()
  );

  readonly sharingIsDirty$ = this.state$.pipe(
    map((state) => state.sharingIsDirty),
    distinctUntilChanged()
  );

  readonly roleCustomers$ = this.state$.pipe(
    map((state) => state.roleCustomers),
    distinctUntilChanged()
  );

  readonly filteredCustomers$ = this.state$.pipe(
    map((state) => state.filteredCustomers),
    distinctUntilChanged()
  );

  readonly users$ = this.state$.pipe(
    map((state) => state.users),
    distinctUntilChanged()
  );

  readonly vm$: Observable<RolesTabFacadeState> = combineLatest([
    this.roles$,
    this.selectedRole$,
    this.filteredMembers$,
    this.resources$,
    this.allResourcePermissions$,
    this.gridFilters$,
    this.assets$,
    this.isLoading$,
    this.assetPermissions$,
    this.nameIsDirty$,
    this.appPermsIsDirty$,
    this.assetPermsIsDirty$,
    this.membershipIsDirty$,
    this.sharingIsDirty$,
    this.roleCustomers$,
    this.filteredCustomers$,
    this.users$,
  ]).pipe(
    map(
      ([
        roles,
        selectedRole,
        filteredMembers,
        resources,
        allResourcePermissions,
        gridFilters,
        assets,
        isLoading,
        assetPermissions,
        nameIsDirty,
        appPermsIsDirty,
        assetPermsIsDirty,
        membershipIsDirty,
        sharingIsDirty,
        roleCustomers,
        filteredCustomers,
        users,
      ]) =>
        ({
          roles,
          selectedRole,
          filteredMembers,
          resources,
          allResourcePermissions,
          gridFilters,
          assets,
          isLoading,
          assetPermissions,
          nameIsDirty,
          appPermsIsDirty,
          assetPermsIsDirty,
          membershipIsDirty,
          sharingIsDirty,
          roleCustomers,
          filteredCustomers,
          users,
        } as RolesTabFacadeState)
    )
  );
}
