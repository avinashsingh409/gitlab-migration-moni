import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';
import {
  User,
  Role,
  UserDetails,
  AssetNode,
  Customer,
  Group,
} from '@atonix/shared/api';
import { RolePermission } from '../model/roles';

@Injectable()
export class UserAdminEventBusService {
  private subject$ = new Subject<UserAdminEmitEvent>();

  on(
    event: UserAdminBusEventTypes,
    action: any,
    notifier: Subject<void>
  ): Subscription {
    return this.subject$
      .pipe(
        filter((e: UserAdminEmitEvent) => {
          return e.stateChange.event === event;
        }),
        map((e: UserAdminEmitEvent) => {
          return e.stateChange.value;
        }),
        takeUntil(notifier)
      )
      .subscribe(action);
  }

  emit(event: UserAdminEmitEvent) {
    this.subject$.next(event);
  }
}

export const enum UserAdminBusEventTypes {
  ADD_ROLE_MEMBERS,
  REMOVE_ROLE_MEMBER,
  REMOVE_ALL_ROLE_MEMBERS,
  ADD_ROLE_FILTERED_MEMBERS,
  CHANGE_ROLE_RESOURCE_PERMISSION,
  CLICK_ROLE_LIST_ROW,
  CLICK_USER_LIST_ROW,
  SET_USER_FILTERS,
  REMOVE_USER_FILTER,
  SET_ROLE_FILTERS,
  REMOVE_ROLE_FILTER,
  SET_ASSET_NODES,
  ADD_ROLE_CUSTOMERS,
  REMOVE_ROLE_CUSTOMER,
  REMOVE_ALL_ROLE_CUSTOMERS,
  ADD_ROLE_FILTERED_CUSTOMERS,
  SET_GROUP_FILTERS,
  REMOVE_GROUP_FILTER,
  CLICK_GROUP_LIST_ROW,
  ADD_GROUP_MEMBERS,
  REMOVE_GROUP_MEMBER,
  REMOVE_ALL_GROUP_MEMBERS,
  ADD_GROUP_FILTERED_MEMBERS,
}

export interface UserAdminStateChange {
  event:
    | UserAdminBusEventTypes.ADD_ROLE_MEMBERS // User[]
    | UserAdminBusEventTypes.REMOVE_ROLE_MEMBER // User
    | UserAdminBusEventTypes.REMOVE_ALL_ROLE_MEMBERS // null
    | UserAdminBusEventTypes.ADD_ROLE_FILTERED_MEMBERS // User[]
    | UserAdminBusEventTypes.CLICK_ROLE_LIST_ROW // { role: Role, callbackFn: (value: boolean) => void }
    | UserAdminBusEventTypes.CLICK_USER_LIST_ROW // { user: User; callbackFn: (value: boolean) => void }
    | UserAdminBusEventTypes.SET_USER_FILTERS // any[]
    | UserAdminBusEventTypes.REMOVE_USER_FILTER // { filter: any; callbackFn: (filterName: string) => void }
    | UserAdminBusEventTypes.SET_ROLE_FILTERS // any[]
    | UserAdminBusEventTypes.REMOVE_ROLE_FILTER // { filter: any; callbackFn: (filterName: string) => void }
    | UserAdminBusEventTypes.SET_ASSET_NODES // AssetNode[]
    | UserAdminBusEventTypes.ADD_ROLE_CUSTOMERS // Customer[]
    | UserAdminBusEventTypes.REMOVE_ROLE_CUSTOMER // Customer
    | UserAdminBusEventTypes.REMOVE_ALL_ROLE_CUSTOMERS // null
    | UserAdminBusEventTypes.ADD_ROLE_FILTERED_CUSTOMERS // Customer[]
    | UserAdminBusEventTypes.SET_GROUP_FILTERS // any[]
    | UserAdminBusEventTypes.REMOVE_GROUP_FILTER // { filter: any; callbackFn: (filterName: string) => void }
    | UserAdminBusEventTypes.CLICK_GROUP_LIST_ROW // { group: Group; callbackFn: (value: boolean) => void }
    | UserAdminBusEventTypes.ADD_GROUP_MEMBERS // User[]
    | UserAdminBusEventTypes.REMOVE_GROUP_MEMBER // User
    | UserAdminBusEventTypes.REMOVE_ALL_GROUP_MEMBERS // null
    | UserAdminBusEventTypes.ADD_GROUP_FILTERED_MEMBERS; // User[]
  value?:
    | User
    | User[]
    | { role: Role; callbackFn: (value: boolean) => void }
    | RolePermission
    | UserDetails
    | { user: User; callbackFn: (value: boolean) => void }
    | any[]
    | { filter: any; callbackFn: (filterName: string) => void }
    | AssetNode[]
    | Customer
    | Customer[]
    | { group: Group; callbackFn: (value: boolean) => void };
}

export class UserAdminEmitEvent {
  constructor(public stateChange: UserAdminStateChange) {}
}
