import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, forkJoin, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import produce from 'immer';
import {
  Role,
  RoleDetails,
  User,
  Resource,
  ResourcePermission,
  AssetNode,
  AssetPermission,
  Customer,
} from '@atonix/shared/api';
import {
  AllResourcePermissions,
  RoleResourcePermissionChange,
} from '../model/roles';
import { ResourceAccessTypes } from '../model/resource-access-type-enum';
import { AssetEmitNode } from '../model/asset-emit-node';
import { RolesTabQuery } from './roles-tab.query';
import { RolesTabService } from './roles-tab.service';

export interface RolesTabFacadeState {
  roles: Role[];
  selectedRole: RoleDetails;
  filteredMembers: User[];
  resources: Resource[];
  allResourcePermissions: AllResourcePermissions[];
  gridFilters: any[];
  assets: AssetNode[];
  isLoading?: boolean;
  nameIsDirty: boolean;
  appPermsIsDirty: boolean;
  assetPermsIsDirty: boolean;
  membershipIsDirty: boolean;
  sharingIsDirty: boolean;
  roleCustomers: Customer[];
  filteredCustomers: Customer[];
  users: User[];
}

const _initialState: RolesTabFacadeState = {
  roles: [],
  selectedRole: null,
  filteredMembers: [],
  resources: [],
  allResourcePermissions: [],
  gridFilters: [],
  assets: [],
  isLoading: null,
  nameIsDirty: false,
  appPermsIsDirty: false,
  assetPermsIsDirty: false,
  membershipIsDirty: false,
  sharingIsDirty: false,
  roleCustomers: [],
  filteredCustomers: [],
  users: [],
};

let _state: RolesTabFacadeState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class RolesTabFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<RolesTabFacadeState>(_state);
  public query = new RolesTabQuery(this.store);

  constructor(private rolesTabService: RolesTabService) {}

  addNewRole(newRole: Role): void {
    this.query.roles$.pipe(take(1)).subscribe((roles: Role[]) => {
      // add new role to top of the roles list
      const newRoles: Role[] = [newRole].concat(roles);
      this.updateState({
        ..._state,
        roles: newRoles,
      });
    });
  }

  initializeRoleTabs(customerId: string): void {
    forkJoin({
      roles: this.rolesTabService.getRoles(customerId),
      roleCustomers: this.rolesTabService.getRoleCustomers(customerId, true),
      users: this.rolesTabService.getUsers(customerId),
      resources: this.rolesTabService.getResources(),
    })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        ({
          roles,
          roleCustomers,
          users,
          resources,
        }: {
          roles: Role[];
          roleCustomers: Customer[];
          users: User[];
          resources: Resource[];
        }) => {
          this.updateState({
            ..._state,
            roles,
            roleCustomers,
            users,
            resources,
            selectedRole: null,
            assets: [],
            allResourcePermissions: [],
            isLoading: false,
          });
        }
      );
  }

  updateSelectedRole(selectedRole: Role, customerId: string): void {
    forkJoin({
      selectedRole: this.rolesTabService.getRoleDetails(
        selectedRole.Id,
        customerId
      ),
      assets: this.rolesTabService.getAssets(selectedRole.Id, customerId),
    })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        ({
          selectedRole,
          assets,
        }: {
          selectedRole: RoleDetails;
          assets: AssetNode[];
        }) => {
          this.buildResourcePermissionMatrix(selectedRole, assets);
        }
      );
  }

  buildResourcePermissionMatrix(role: RoleDetails, assets?: AssetNode[]): void {
    // build matrix of available resources and role's resource permissions
    this.query.resources$.pipe(take(1)).subscribe((resources: Resource[]) => {
      const allResources: AllResourcePermissions[] = resources.map(
        (resrc: Resource) => {
          // if a role is selected, look for corresponding permissions on the it and attach accordingly
          const viewPermission: ResourcePermission =
            role?.ResourcePermissions.find(
              (rolePermission: ResourcePermission) => {
                return (
                  rolePermission.ResourceId === resrc.Id &&
                  rolePermission.AccessTypeId === ResourceAccessTypes.View
                );
              }
            );
          const editPermission: ResourcePermission =
            role?.ResourcePermissions.find(
              (rolePermission: ResourcePermission) => {
                return (
                  rolePermission.ResourceId === resrc.Id &&
                  rolePermission.AccessTypeId === ResourceAccessTypes.Edit
                );
              }
            );
          const addPermission: ResourcePermission =
            role?.ResourcePermissions.find(
              (rolePermission: ResourcePermission) => {
                return (
                  rolePermission.ResourceId === resrc.Id &&
                  rolePermission.AccessTypeId === ResourceAccessTypes.Add
                );
              }
            );
          const deletePermission: ResourcePermission =
            role?.ResourcePermissions.find(
              (rolePermission: ResourcePermission) => {
                return (
                  rolePermission.ResourceId === resrc.Id &&
                  rolePermission.AccessTypeId === ResourceAccessTypes.Delete
                );
              }
            );
          const adminPermission: ResourcePermission =
            role?.ResourcePermissions.find(
              (rolePermission: ResourcePermission) => {
                return (
                  rolePermission.ResourceId === resrc.Id &&
                  rolePermission.AccessTypeId === ResourceAccessTypes.BVEdit
                );
              }
            );
          const thisResource: AllResourcePermissions = {
            Resource: resrc,
            ViewPermission: viewPermission,
            EditPermission: editPermission,
            AddPermission: addPermission,
            DeletePermission: deletePermission,
            AdminPermission: adminPermission,
          };
          return thisResource;
        }
      );
      this.updateState({
        ..._state,
        isLoading: false,
        selectedRole: role,
        allResourcePermissions: allResources.filter(
          (d: AllResourcePermissions) =>
            d.ViewPermission?.Allow ||
            d.EditPermission?.Allow ||
            d.AddPermission?.Allow ||
            d.DeletePermission?.Allow ||
            d.AdminPermission?.Allow
        ),
        assets: assets ?? _state.assets,
      });
    });
  }

  setAssets(assets: AssetNode[]): void {
    this.updateState({
      ..._state,
      assets,
    });
  }

  addMembersToRole(members: User[]): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        // add new member to the top of the members list
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Users = members.concat(draftState.Users);
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  removeMemberFromRole(member: User): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        // remove member from the members list
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Users.splice(
              draftState.Users.findIndex((r) => r.Id === member.Id),
              1
            );
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  removeAllMembersFromRole(): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        // remove all members from the members list
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Users = [];
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  addRoleFilteredMembers(members: User[]): void {
    this.updateState({
      ..._state,
      filteredMembers: members,
    });
  }

  changeRoleResourcePermission(
    resourceChange: RoleResourcePermissionChange
  ): void {
    this.query.selectedRole$.pipe(take(1)).subscribe((role: RoleDetails) => {
      const updatedRole = produce(role, (draftState: RoleDetails) => {
        if (resourceChange.Checked) {
          // remove all existing access types for the resource
          const draftResourcePermissions: ResourcePermission[] =
            draftState.ResourcePermissions.filter((r) => {
              return r.ResourceId !== resourceChange.AllPermissions.Resource.Id;
            });
          // add checked access type
          let accessType = '';
          switch (resourceChange.AccessTypeId) {
            case ResourceAccessTypes.View:
              accessType = ResourceAccessTypes[ResourceAccessTypes.View];
              break;
            case ResourceAccessTypes.Edit:
              accessType = ResourceAccessTypes[ResourceAccessTypes.Edit];
              break;
            case ResourceAccessTypes.Add:
              accessType = ResourceAccessTypes[ResourceAccessTypes.Add];
              break;
            case ResourceAccessTypes.Delete:
              accessType = ResourceAccessTypes[ResourceAccessTypes.Delete];
              break;
            case ResourceAccessTypes.BVEdit:
              accessType = ResourceAccessTypes[ResourceAccessTypes.BVEdit];
              break;
          }
          const newResourcePermission: ResourcePermission = {
            ResourceId: resourceChange.AllPermissions.Resource.Id,
            ResourceName: resourceChange.AllPermissions.Resource.Name,
            AccessTypeId: resourceChange.AccessTypeId,
            AccessType: accessType,
            Allow: true,
          };
          draftState.ResourcePermissions = [newResourcePermission].concat(
            draftResourcePermissions
          );
        } else {
          const idx = draftState.ResourcePermissions.findIndex(
            (r: ResourcePermission) => {
              return (
                r.ResourceId === resourceChange.AllPermissions.Resource.Id &&
                r.AccessTypeId === resourceChange.AccessTypeId
              );
            }
          );
          draftState.ResourcePermissions.splice(idx, 1);
        }
      });
      this.buildResourcePermissionMatrix(updatedRole);
    });
  }

  changeRoleAssetPermissions(assetPerms: AssetEmitNode[]): void {
    this.query.selectedRole$.pipe(take(1)).subscribe((role: RoleDetails) => {
      const updatedRole: RoleDetails = produce(
        role,
        (draftState: RoleDetails) => {
          const updatedAssetPerms: AssetPermission[] = assetPerms.map(
            (ap: AssetEmitNode) => ({
              ...ap,
              AssetName: '',
            })
          );
          draftState.AssetPermissions = updatedAssetPerms;
          return draftState;
        }
      );
      this.updateState({
        ..._state,
        selectedRole: updatedRole,
        assetPermsIsDirty: true,
      });
    });
  }

  setFilters(filters: any[]): void {
    this.updateState({
      ..._state,
      gridFilters: filters,
    });
  }

  removeGridFilter(filter: any, callback: (filterName: string) => void): void {
    this.query.gridFilters$.pipe(take(1)).subscribe((f: any[]) => {
      const filters: any[] = [...f];
      const index: number = filters.indexOf(filter);
      if (index >= 0) {
        filters.splice(index, 1);
        this.updateState({
          ..._state,
          gridFilters: filters,
        });
        callback(filter.Name);
      }
    });
  }

  updateRoleName(newName: string): void {
    this.query.selectedRole$.pipe(take(1)).subscribe((role: RoleDetails) => {
      const updatedRole = produce(role, (draftState: RoleDetails) => {
        draftState.Name = newName;
      });
      this.updateState({
        ..._state,
        selectedRole: updatedRole,
      });
    });
  }

  setNameIsDirty(nameIsDirty: boolean): void {
    this.updateState({
      ..._state,
      nameIsDirty,
    });
  }

  setAppPermsIsDirty(appPermsIsDirty: boolean): void {
    this.updateState({
      ..._state,
      appPermsIsDirty,
    });
  }

  setAssetPermsIsDirty(assetPermsIsDirty: boolean): void {
    this.updateState({
      ..._state,
      assetPermsIsDirty,
    });
  }

  setMembershipIsDirty(membershipIsDirty: boolean): void {
    this.updateState({
      ..._state,
      membershipIsDirty,
    });
  }

  setSharingIsDirty(sharingIsDirty: boolean): void {
    this.updateState({
      ..._state,
      sharingIsDirty,
    });
  }

  setLoading(isLoading?: boolean): void {
    this.updateState({
      ..._state,
      isLoading,
    });
  }

  setRolesSubTabsDirty(isDirty: boolean): void {
    this.updateState({
      ..._state,
      nameIsDirty: isDirty,
      appPermsIsDirty: isDirty,
      assetPermsIsDirty: isDirty,
      membershipIsDirty: isDirty,
      sharingIsDirty: isDirty,
    });
  }

  setRoles(roles: Role[]): void {
    this.updateState({ ..._state, roles });
  }

  addCustomersToRole(customers: Customer[]): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Shares = customers.concat(draftState.Shares);
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  removeCustomerFromRole(customer: Customer): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Shares.splice(
              draftState.Shares.findIndex((r) => r.Id === customer.Id),
              1
            );
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  removeAllCustomersFromRole(): void {
    this.query.selectedRole$
      .pipe(take(1))
      .subscribe((selectedRole: RoleDetails) => {
        const updatedSelectedRole = produce(
          selectedRole,
          (draftState: RoleDetails) => {
            draftState.Shares = [];
          }
        );
        this.updateState({
          ..._state,
          selectedRole: updatedSelectedRole,
        });
      });
  }

  addRoleFilteredCustomers(customers: Customer[]): void {
    this.updateState({
      ..._state,
      filteredCustomers: customers,
    });
  }

  reset(): void {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: RolesTabFacadeState): void {
    this.store.next((_state = newState));
  }

  destroyCalls(): void {
    this.onDestroy$.next();
  }

  ngOnDestroy(): void {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
