import { Injectable, OnDestroy } from '@angular/core';
import { isNil } from '@atonix/atx-core';
import {
  CustomerNode,
  Role,
  TopLevelAssetNode,
  User,
  UserAdminCoreService,
} from '@atonix/shared/api';
import produce from 'immer';
import { BehaviorSubject, Subject } from 'rxjs';
import { mergeMap, takeUntil } from 'rxjs/operators';
import { ITab } from '../model/tab';
import { UserAdminRxjsQuery } from './user-admin.rxjs.query';

export interface UserAdminRxjsFacadeState {
  selectedUser: any;
  selectedNotificationGroup: any;
  tabs: ITab[];
  isDirty: boolean;
  customers: CustomerNode[];
  selectedCustomer: CustomerNode;
  customerTopLevelAssets: TopLevelAssetNode[];
}

const _initialState: UserAdminRxjsFacadeState = {
  selectedUser: null,
  selectedNotificationGroup: null,
  tabs: [
    {
      path: 'users',
      label: 'Users',
      isDirty: false,
    },
    {
      path: 'roles',
      label: 'Roles',
      isDirty: false,
    },
    {
      path: 'groups',
      label: 'Groups',
      isDirty: false,
    },
    {
      path: 'customer',
      label: 'Customer Information',
      isDirty: false,
    },
  ],
  isDirty: false,
  customers: [],
  selectedCustomer: null,
  customerTopLevelAssets: [],
};

let _state: UserAdminRxjsFacadeState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class UserAdminRxjsFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<UserAdminRxjsFacadeState>(_state);
  public query = new UserAdminRxjsQuery(this.store);

  constructor(private userAdminCoreService: UserAdminCoreService) {}

  updateSelectedCustomer(selectedCustomer: CustomerNode) {
    this.updateState({
      ..._state,
      selectedCustomer,
    });
  }

  getCustomersAndTopLevelAssets() {
    this.userAdminCoreService
      .getCustomers()
      .pipe(
        mergeMap((customers: CustomerNode[]) => {
          if (!isNil(customers) && customers.length > 0) {
            const selectedCustomer: CustomerNode = customers[0];
            this.updateState({
              ..._state,
              customers,
              selectedCustomer,
            });
            return this.userAdminCoreService.getTopLevelAssets();
          }
        }),
        takeUntil(this.onDestroy$)
      )
      .subscribe((customerTopLevelAssets: TopLevelAssetNode[]) => {
        this.updateState({
          ..._state,
          customerTopLevelAssets,
        });
      });
  }

  setIsDirty(isDirty: boolean, path?: string) {
    const updatedState: UserAdminRxjsFacadeState = produce(
      _state,
      (draftState: UserAdminRxjsFacadeState) => {
        if (!isNil(path)) {
          const index: number = draftState.tabs.findIndex(
            (tab: ITab) => tab.path === path
          );
          draftState.tabs[index].isDirty = isDirty;
        }
        draftState.isDirty = isDirty;
      }
    );
    this.updateState(updatedState);
  }

  private updateState(newState: UserAdminRxjsFacadeState) {
    this.store.next((_state = newState));
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  destroyCalls(): void {
    this.onDestroy$.next();
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
