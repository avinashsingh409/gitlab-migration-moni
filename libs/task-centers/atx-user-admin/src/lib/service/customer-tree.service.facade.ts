import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { CustomerTreeQuery } from './customer-tree.service.query';
import { CustomerTreeService } from './customer-tree.service';
import { CustomerNode } from '@atonix/shared/api';

export interface CustomerTreeState {
  nodes: CustomerNode[];
}

const _initialState: CustomerTreeState = {
  nodes: [],
};

let _state: CustomerTreeState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class CustomerTreeFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<CustomerTreeState>(_state);
  public query = new CustomerTreeQuery(this.store);

  constructor(private customerTreeService: CustomerTreeService) {}

  setNodes(nodes: CustomerNode[]): void {
    this.updateState({
      ..._state,
      nodes,
    });
  }

  setSelectedNode(selectedNodeId: string): void {
    const updatedState: CustomerTreeState = cloneDeep(_state);
    updatedState.nodes.forEach((data: CustomerNode) => {
      data.IsSelected = false;
    });
    updatedState.nodes.find(
      (node: CustomerNode) => node.Id === selectedNodeId
    ).IsSelected = true;
    this.updateState(updatedState);
  }

  setUpdatedName(id: string, name: string) {
    const updatedState: CustomerTreeState = cloneDeep(_state);
    updatedState.nodes.find((node: CustomerNode) => node.Id === id).Name = name;
    this.updateState(updatedState);
  }

  addNewCustomer(newCustomer: CustomerNode) {
    const updatedState: CustomerTreeState = cloneDeep(_state);
    const parentCustomer: CustomerNode = updatedState.nodes.find(
      (node: CustomerNode) => node.Id === newCustomer.ParentCustomerId
    );
    const newCustomerNode: CustomerNode = {
      ...newCustomer,
      Level: parentCustomer.Level + 1,
    };
    const indexOfParentCustomer: number = updatedState.nodes.findIndex(
      (node: CustomerNode) => node.Id === parentCustomer.Id
    );
    if (!parentCustomer.HasChildren) {
      parentCustomer.Children = [];
    } else if (parentCustomer.IsExpanded) {
      updatedState.nodes.splice(indexOfParentCustomer + 1, 0, newCustomerNode);
    }

    this.updateState(updatedState);
  }

  toggleNode(node: CustomerNode, expand: boolean): void {
    const nodes: CustomerNode[] = [...this.query.getNodes()];
    const index: number = nodes.indexOf(node);
    if (index < 0) {
      return;
    }

    if (expand) {
      node.IsLoading = true;
      if (node.Children == null || node.Children.length === 0) {
        this.customerTreeService
          .getChildren(node.Id, node.Level)
          .pipe(take(1))
          .subscribe((children: CustomerNode[]) => {
            node.Children = children;
            nodes.splice(index + 1, 0, ...node.Children);
            node.IsExpanded = true;
            node.IsLoading = false;
            this.updateState({
              ..._state,
              nodes,
            });
          });
      } else {
        node.Children = cloneDeep(node.Children);
        nodes.splice(index + 1, 0, ...node.Children);
        node.IsExpanded = true;
        node.IsLoading = false;
        this.updateState({
          ..._state,
          nodes,
        });
      }
    } else {
      let count = 0;
      for (
        let i = index + 1;
        i < nodes.length && nodes[i].Level > node.Level;
        i++, count++
      ) {
        nodes[i].IsExpanded = false;
      }
      nodes.splice(index + 1, count);
      node.IsExpanded = false;
      this.updateState({
        ..._state,
        nodes,
      });
    }
  }

  private updateState(newState: CustomerTreeState) {
    this.store.next((_state = newState));
  }

  private reset() {
    this.updateState(_initialState);
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
