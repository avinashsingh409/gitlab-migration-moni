import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { AssetTreeTableServiceState } from './asset-tree-table.service.facade';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { AssetNode } from '@atonix/shared/api';
import { AssetEmitNode } from '../model/asset-emit-node';

export class AssetTreeTableServiceQuery {
  constructor(private store: BehaviorSubject<AssetTreeTableServiceState>) {}
  private state$ = this.store.asObservable();

  readonly assetNodes$ = this.state$.pipe(
    map((state) => state.assetNodes),
    distinctUntilChanged()
  );

  readonly emitNodes$ = this.state$.pipe(
    map((state) => state.emitNodes),
    distinctUntilChanged()
  );

  readonly vm$: Observable<AssetTreeTableServiceState> = combineLatest([
    this.assetNodes$,
    this.emitNodes$,
  ]).pipe(
    map(([assetNodes, emitNodes]) => {
      return {
        assetNodes,
        emitNodes,
      } as AssetTreeTableServiceState;
    })
  );

  getAssetNodes = (): AssetNode[] => {
    let assetNodes: AssetNode[] = [];
    this.assetNodes$.pipe(take(1)).subscribe((nodes: AssetNode[]) => {
      assetNodes = nodes;
    });
    return assetNodes;
  };

  getEmitNodes = (): AssetEmitNode[] => {
    let emitNodes: AssetEmitNode[] = [];
    this.emitNodes$.pipe(take(1)).subscribe((nodes: AssetEmitNode[]) => {
      emitNodes = nodes;
    });
    return emitNodes;
  };

  getParentNode = (assetId: string, level: number): AssetNode =>
    this.getAssetNodes().find(
      (node: AssetNode) =>
        node.Level === level - 1 &&
        node.Children != null &&
        node.Children.some((child: AssetNode) => child.AssetId === assetId)
    );
}
