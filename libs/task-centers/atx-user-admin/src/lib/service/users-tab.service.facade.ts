import { Injectable, OnDestroy } from '@angular/core';
import { User, Role, Group, UserDetails } from '@atonix/shared/api';
import { BehaviorSubject, forkJoin, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { UsersTabService } from './users-tab.service';
import { UsersTabServiceQuery } from './users-tab.service.query';
import { RolePermission } from '../model/roles';
import { isNil } from '@atonix/atx-core';
import { GroupPermission } from '../model/users';

export interface UsersTabServiceState {
  users: User[];
  selectedUser?: UserDetails;
  roles: Role[];
  groups: Group[];
  rolePermissions: RolePermission[];
  groupPermissions: GroupPermission[];
  filters: any[];
  isLoading?: boolean;
  isDetailsSubTabDirty: boolean;
  isRolesSubTabDirty: boolean;
  isGroupsSubTabDirty: boolean;
  emailChanged: boolean;
}

const _initialState: UsersTabServiceState = {
  users: [],
  selectedUser: null,
  roles: [],
  groups: [],
  rolePermissions: [],
  groupPermissions: [],
  filters: [],
  isLoading: null,
  isDetailsSubTabDirty: false,
  isRolesSubTabDirty: false,
  isGroupsSubTabDirty: false,
  emailChanged: false,
};

let _state: UsersTabServiceState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class UsersTabFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<UsersTabServiceState>(_state);
  public query = new UsersTabServiceQuery(this.store);

  constructor(private usersTabService: UsersTabService) {}

  getUsersRolesAndGroups(customerId: string): void {
    forkJoin({
      users: this.usersTabService.getUsers(customerId, true, false),
      roles: this.usersTabService.getRoles(customerId),
      groups: this.usersTabService.getGroups(customerId),
    })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        ({
          users,
          roles,
          groups,
        }: {
          users: User[];
          roles: Role[];
          groups: Group[];
        }) => {
          this.updateState({
            ..._state,
            users,
            roles,
            groups,
            selectedUser: null,
            isLoading: false,
          });
        }
      );
  }

  addNewUser(newSelectedUser: User): void {
    this.query.users$.pipe(take(1)).subscribe((users: User[]) => {
      // add new user to top of the users list
      const newUsers: User[] = [newSelectedUser].concat(users);
      this.updateSelectedUser(newSelectedUser, newUsers);
    });
  }

  updateSelectedUser(selectedUser: User, users?: User[]): void {
    if (!isNil(selectedUser)) {
      this.getUserDetails(selectedUser.Id, users);
    } else {
      this.updateState({
        ..._state,
        rolePermissions: this.buildUserRolePermissionMatrix(null),
        groupPermissions: this.buildUserGroupPermissionMatrix(null),
        isLoading: false,
      });
    }
  }

  getUserDetails(userId: string, users?: User[]): void {
    this.usersTabService
      .getUserDetails(userId)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((user: UserDetails) => {
        this.updateState({
          ..._state,
          isLoading: false,
          rolePermissions: this.buildUserRolePermissionMatrix(user),
          groupPermissions: this.buildUserGroupPermissionMatrix(user),
          selectedUser: user,
          users: users ?? _state.users,
        });
      });
  }

  buildUserRolePermissionMatrix(user: UserDetails): RolePermission[] {
    // build matrix of available roles with selected user's permission
    let allRoles: RolePermission[] = [];
    this.query.roles$.pipe(take(1)).subscribe((roles: Role[]) => {
      allRoles = roles.map((resrc: Role) => {
        // if a role is selected, look for corresponding permissions on the list and attach accordingly
        const role: Role = user?.Roles.find((r: Role) => {
          return r.Id === resrc.Id;
        });
        const thisRole: RolePermission = {
          Role: resrc,
          Allow: role ? true : false,
        };
        return thisRole;
      });
    });
    return allRoles;
  }

  buildUserGroupPermissionMatrix(user: UserDetails): GroupPermission[] {
    // build matrix of available groups with selected user's permission
    let optionalGroups: GroupPermission[] = [];
    let userGroups: GroupPermission[] = [];

    this.query.groups$.pipe(take(1)).subscribe((groups: Group[]) => {
      // list of optional active groups that user could participates in
      const filtered_groups = groups.filter((g) => {
        const userGroup: Group = user?.Groups.find((ug: Group) => {
          return g.Id === ug.Id;
        });
        if (userGroup == null && g.Active === true) {
          return g;
        }
      });
      optionalGroups = filtered_groups.map((group: Group) => {
        const thisGroup: GroupPermission = {
          Group: group,
          Allow: false,
        };
        return thisGroup;
      });

      // list of (active and inactive) groups that user currently participates in
      userGroups = user.Groups.map((group: Group) => {
        const thisGroup: GroupPermission = {
          Group: group,
          Allow: true,
        };
        return thisGroup;
      });
    });

    return [...optionalGroups, ...userGroups];
  }

  setLoading(isLoading?: boolean): void {
    this.updateState({
      ..._state,
      isLoading,
    });
  }

  setFilters(filters: any[]): void {
    this.updateState({
      ..._state,
      filters,
    });
  }

  removeFilter(filter: any, callback: (filterName: string) => void): void {
    this.query.filters$.pipe(take(1)).subscribe((f: any[]) => {
      const filters: any[] = [...f];
      const index: number = filters.indexOf(filter);
      if (index >= 0) {
        filters.splice(index, 1);
        this.updateState({
          ..._state,
          filters,
        });
        callback(filter.Name);
      }
    });
  }

  setSelectedUser(selectedUser: UserDetails): void {
    this.updateState({
      ..._state,
      selectedUser,
    });
  }

  setRolePermissions(rolePermissions: RolePermission[]): void {
    this.updateState({
      ..._state,
      rolePermissions,
    });
  }

  setGroupPermissions(groupPermissions: GroupPermission[]): void {
    this.updateState({
      ..._state,
      groupPermissions,
    });
  }

  setIsDetailsSubTabDirty(isDetailsSubTabDirty: boolean): void {
    this.updateState({ ..._state, isDetailsSubTabDirty });
  }

  setIsRolesSubTabDirty(isRolesSubTabDirty: boolean): void {
    this.updateState({ ..._state, isRolesSubTabDirty });
  }

  setIsGroupsSubTabDirty(isGroupsSubTabDirty: boolean): void {
    this.updateState({ ..._state, isGroupsSubTabDirty });
  }

  setUsers(users: User[]): void {
    this.updateState({ ..._state, users });
  }

  setUsersSubTabsDirty(value: boolean): void {
    this.updateState({
      ..._state,
      isDetailsSubTabDirty: value,
      isRolesSubTabDirty: value,
      isGroupsSubTabDirty: value,
      emailChanged: value,
    });
  }

  setEmailChanged(emailChanged: boolean): void {
    this.updateState({
      ..._state,
      emailChanged,
    });
  }

  reset(): void {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: UsersTabServiceState): void {
    this.store.next((_state = newState));
  }

  destroyCalls(): void {
    this.onDestroy$.next();
  }

  ngOnDestroy(): void {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
