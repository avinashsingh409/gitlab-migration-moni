/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import { UserAdminCoreService, CustomerDetails } from '@atonix/shared/api';
import { Observable, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CustomerTabService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getCustomerDetails = (customerId: string): Observable<CustomerDetails> =>
    this.userAdminCoreService.getCustomerDetails(customerId).pipe(
      take(1),
      map((customer: CustomerDetails) => customer),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getCustomerEditPermission = (customerId: string): Observable<boolean> =>
    this.userAdminCoreService.getCustomerEditPermission(customerId).pipe(
      take(1),
      map((canEditCustomer: boolean) => canEditCustomer),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );
}
