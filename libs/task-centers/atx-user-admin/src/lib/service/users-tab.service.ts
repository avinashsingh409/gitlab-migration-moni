/* eslint-disable rxjs/no-implicit-any-catch */
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  User,
  Role,
  Group,
  UserAdminCoreService,
  UserDetails,
} from '@atonix/shared/api';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UsersTabService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getUsers = (
    customerId: string,
    includeDisabled?: boolean,
    includeGroups?: boolean
  ): Observable<User[]> =>
    this.userAdminCoreService
      .getUsers(customerId, includeDisabled, includeGroups)
      .pipe(
        take(1),
        map((users: User[]) => users),
        catchError((err: any) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );

  getUserDetails = (userId: string): Observable<UserDetails> =>
    this.userAdminCoreService.getUserDetails(userId).pipe(
      take(1),
      map((user: UserDetails) => user),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getRoles = (customerId: string): Observable<Role[]> =>
    this.userAdminCoreService.getRoles(customerId).pipe(
      take(1),
      map((roles: Role[]) => roles),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getGroups = (customerId: string): Observable<Group[]> =>
    this.userAdminCoreService.getGroups(customerId, true).pipe(
      take(1),
      map((groups: Group[]) => groups.filter((g: Group) => g.Active)),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  initiatePasswordReset = (userId: string): Observable<string> =>
    this.userAdminCoreService.initiatePasswordReset(userId).pipe(
      take(1),
      map((result: string) => result),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  downloadUserList = (customerId: string): Observable<HttpResponse<any>> =>
    this.userAdminCoreService.downloadUserList(customerId).pipe(
      take(1),
      map((result: HttpResponse<any>) => result),
      catchError((err: HttpErrorResponse) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        } else if (err?.error instanceof Blob) {
          return throwError(err);
        } else {
          return throwError('an error occurred');
        }
      })
    );

  validateEmail(emailAddress: string): boolean {
    let valid = false;
    // source (12/28/2022): https://stackoverflow.com/questions/46155/how-can-i-validate-an-email-address-in-javascript
    // then modified by Schumacher to better handle personal info (left of the @) and domain (right of the @), including ip addresses
    const match = String(emailAddress)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])|([a-zA-Z](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])*(?:\.[a-zA-Z](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])*)+))$/
      );
    if (match) {
      valid = true;
    }
    return valid;
  }
}
