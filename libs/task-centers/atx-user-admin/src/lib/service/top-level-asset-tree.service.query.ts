import { TopLevelAssetNode } from '@atonix/shared/api';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { TopLevelAssetTreeState } from './top-level-asset-tree.service.facade';

export class TopLevelAssetTreeQuery {
  constructor(public store: BehaviorSubject<TopLevelAssetTreeState>) {}
  private state$ = this.store.asObservable();

  readonly nodes$ = this.state$.pipe(
    map((state) => state.nodes),
    distinctUntilChanged()
  );

  readonly vm$: Observable<TopLevelAssetTreeState> = combineLatest([
    this.nodes$,
  ]).pipe(
    map(([nodes]) => {
      return {
        nodes,
      } as TopLevelAssetTreeState;
    })
  );

  getNodes = (): TopLevelAssetNode[] => {
    let nodes: TopLevelAssetNode[] = [];
    this.nodes$.pipe(take(1)).subscribe((queryNodes: TopLevelAssetNode[]) => {
      nodes = queryNodes;
    });
    return nodes;
  };
}
