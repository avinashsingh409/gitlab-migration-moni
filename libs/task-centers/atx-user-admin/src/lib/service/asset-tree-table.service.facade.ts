import { EventEmitter, Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AssetTreeTableServiceQuery } from './asset-tree-table.service.query';
import { AssetTreeTableService } from './asset-tree-table.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { AssetEmitNode } from '../model/asset-emit-node';
import { cloneDeep } from 'lodash';
import { AssetNode, AssetPermission } from '@atonix/shared/api';
import { isNil } from '@atonix/atx-core';

export interface AssetTreeTableServiceState {
  assetNodes: AssetNode[];
  emitNodes: AssetEmitNode[];
}

const _initialState: AssetTreeTableServiceState = {
  assetNodes: [],
  emitNodes: [],
};

let _state: AssetTreeTableServiceState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class AssetTreeTableServiceFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<AssetTreeTableServiceState>(_state);
  public query = new AssetTreeTableServiceQuery(this.store);

  constructor(private assetTreeTableService: AssetTreeTableService) {}

  setAssetNodes(assetNodes: AssetNode[]): void {
    this.updateState({
      ..._state,
      assetNodes,
    });
  }

  setEmitNodes(emitNodes: AssetPermission[]): void {
    this.updateState({
      ..._state,
      emitNodes,
    });
  }

  toggleNode(
    node: AssetNode,
    expand: boolean,
    roleId: number,
    customerId: string
  ): void {
    const nodes: AssetNode[] = [...this.query.getAssetNodes()];
    const index: number = nodes.indexOf(node);
    if (index < 0) {
      return;
    }

    if (expand) {
      node.IsLoading = true;
      if (node.Children == null || node.Children.length === 0) {
        this.assetTreeTableService
          .getAssetChildren(node.AssetId, node.Level, roleId, customerId)
          .pipe(take(1))
          .subscribe((children: AssetNode[]) => {
            [...children].forEach((child: AssetNode) => {
              child.ImplicitAllow =
                child.Allow === false &&
                child.Deny === false &&
                ((node.Allow === true && child.Allow === false) ||
                  node.ImplicitAllow === true);
              child.ImplicitDeny =
                child.Deny === false &&
                child.Allow === false &&
                ((node.Deny === true && child.Deny === false) ||
                  node.ImplicitDeny === true);
            });
            node.Children = children;
            nodes.splice(index + 1, 0, ...node.Children);
            node.IsExpanded = true;
            node.IsLoading = false;
            this.updateState({
              ..._state,
              assetNodes: nodes,
            });
          });
      } else {
        node.Children = cloneDeep(node.Children);
        nodes.splice(index + 1, 0, ...node.Children);
        node.IsExpanded = true;
        node.IsLoading = false;
        this.updateState({
          ..._state,
          assetNodes: nodes,
        });
      }
    } else {
      let count = 0;
      for (
        let i = index + 1;
        i < nodes.length && nodes[i].Level > node.Level;
        i++, count++
      );
      nodes.splice(index + 1, count);
      node.IsExpanded = false;
      this.updateState({
        ..._state,
        assetNodes: nodes,
      });
    }
  }

  tickedAllow(
    data: AssetNode,
    checkbox: MatCheckbox,
    stateChange: EventEmitter<AssetEmitNode[]>
  ): void {
    const node: AssetNode = this.assetTreeTableService.findById(
      data.AssetId,
      this.query.getAssetNodes()
    );
    const flat: AssetNode[] = !isNil(node.Children)
      ? this.assetTreeTableService.flattenItems(node.Children, 'Children')
      : [];
    let isImplicitAllow = false;
    let isImplicitDeny = false;

    if (node.ImplicitAllow === true && node.Allow === false) {
      node.Allow = true;
      node.ImplicitAllow = false;
      checkbox.toggle();
    } else {
      node.Allow = !node.Allow;
    }

    if (node.Allow === true) {
      node.Deny = false;
      node.AscendAllow = false;
      node.ImplicitDeny = false;
      node.AscendDeny = flat.some(
        (fl: AssetNode) => fl.Deny === true || fl.AscendDeny === true
      );
      isImplicitAllow = true;
    } else {
      node.Allow = false;

      const parentNode: AssetNode = this.query.getParentNode(
        node.AssetId,
        node.Level
      );
      isImplicitAllow =
        !isNil(parentNode) &&
        (parentNode.Allow === true || parentNode.ImplicitAllow === true);
      if (isImplicitAllow === true) {
        node.ImplicitAllow = isImplicitAllow;
        checkbox.toggle();
      } else {
        node.AscendAllow = flat.some(
          (fl: AssetNode) => fl.Allow === true || fl.AscendAllow === true
        );
      }

      isImplicitDeny =
        !isNil(parentNode) &&
        (parentNode.Deny === true || parentNode.ImplicitDeny === true);
      if (isImplicitDeny === true) {
        node.ImplicitDeny = isImplicitDeny;
        checkbox.toggle();
      } else {
        node.AscendDeny = flat.some(
          (fl: AssetNode) => fl.Deny === true || fl.AscendDeny === true
        );
      }
    }

    node.Children?.forEach((child: AssetNode) => {
      if (child.Allow === false && child.Deny === false) {
        this.updateChildrenAllow(child, isImplicitAllow, isImplicitDeny);
      }
    });

    this.updateParentNodeAllow(node);
    this.emitNodes(node, stateChange);
  }

  updateParentNodeAllow(data: AssetNode): void {
    if (data.Level > 0) {
      const parentNode: AssetNode = this.query.getParentNode(
        data.AssetId,
        data.Level
      );
      if (parentNode.Allow === false && parentNode.ImplicitAllow === false) {
        parentNode.AscendAllow = parentNode.Children.some(
          (child: AssetNode) =>
            child.Allow === true || child.AscendAllow === true
        );
      }

      const flat: AssetNode[] = this.assetTreeTableService.flattenItems(
        parentNode.Children,
        'Children'
      );
      if (
        flat.some((fl: AssetNode) => fl.Deny === true) &&
        parentNode.Deny === false
      ) {
        parentNode.AscendDeny = true;
      } else if (
        flat.every(
          (fl: AssetNode) => fl.Deny === false && fl.AscendDeny === false
        )
      ) {
        parentNode.AscendDeny = false;
      }

      this.updateParentNodeAllow(parentNode);
    }
  }

  updateChildrenAllow(
    node: AssetNode,
    implicitAllow: boolean,
    implicitDeny: boolean
  ): void {
    const flat: AssetNode[] = !isNil(node.Children)
      ? this.assetTreeTableService.flattenItems(node.Children, 'Children')
      : [];
    node.Allow = false;
    node.ImplicitAllow = implicitAllow;
    node.ImplicitDeny = implicitDeny;
    node.AscendDeny = flat.some(
      (fl: AssetNode) => fl.Deny === true || fl.AscendDeny === true
    );
    node.AscendAllow = flat.some(
      (fl: AssetNode) => fl.Allow === true || fl.AscendAllow === true
    );

    node.Children?.forEach((child: AssetNode) => {
      if (child.Allow === false && child.Deny === false) {
        this.updateChildrenAllow(child, implicitAllow, implicitDeny);
      }
    });
  }

  tickedDeny(
    data: AssetNode,
    checkbox: MatCheckbox,
    stateChange: EventEmitter<AssetEmitNode[]>
  ): void {
    const node: AssetNode = this.assetTreeTableService.findById(
      data.AssetId,
      this.query.getAssetNodes()
    );
    const flat: AssetNode[] = !isNil(node.Children)
      ? this.assetTreeTableService.flattenItems(node.Children, 'Children')
      : [];
    let isImplicitDeny = false;
    let isImplicitAllow = false;

    if (node.ImplicitDeny === true && node.Deny === false) {
      node.Deny = true;
      node.ImplicitDeny = false;
      checkbox.toggle();
    } else {
      node.Deny = !node.Deny;
    }

    if (node.Deny === true) {
      node.Allow = false;
      node.AscendDeny = false;
      node.ImplicitAllow = false;
      node.AscendAllow = flat.some(
        (fl: AssetNode) => fl.Allow === true || fl.AscendAllow === true
      );
      isImplicitDeny = true;
    } else {
      node.Deny = false;

      const parentNode: AssetNode = this.query.getParentNode(
        node.AssetId,
        node.Level
      );
      isImplicitDeny =
        !isNil(parentNode) &&
        (parentNode.Deny === true || parentNode.ImplicitDeny === true);
      if (isImplicitDeny === true) {
        node.ImplicitDeny = isImplicitDeny;
        checkbox.toggle();
      } else {
        node.AscendDeny = flat.some(
          (fl: AssetNode) => fl.Deny === true || fl.AscendDeny === true
        );
      }

      isImplicitAllow =
        !isNil(parentNode) &&
        (parentNode.Allow === true || parentNode.ImplicitAllow === true);
      if (isImplicitAllow === true) {
        node.ImplicitAllow = isImplicitAllow;
        checkbox.toggle();
      } else {
        node.AscendAllow = flat.some(
          (fl: AssetNode) => fl.Allow === true || fl.AscendAllow === true
        );
      }
    }

    node.Children?.forEach((child: AssetNode) => {
      if (child.Deny === false && child.Allow === false) {
        this.updateChildrenDeny(child, isImplicitDeny, isImplicitAllow);
      }
    });

    this.updateParentNodeDeny(node);
    this.emitNodes(node, stateChange);
  }

  updateParentNodeDeny(data: AssetNode): void {
    if (data.Level > 0) {
      const parentNode: AssetNode = this.query.getParentNode(
        data.AssetId,
        data.Level
      );
      if (parentNode.Deny === false && parentNode.ImplicitDeny === false) {
        parentNode.AscendDeny = parentNode.Children.some(
          (child) => child.Deny === true || child.AscendDeny === true
        );
      }

      const flat: AssetNode[] = this.assetTreeTableService.flattenItems(
        parentNode.Children,
        'Children'
      );
      if (
        flat.some((fl: AssetNode) => fl.Allow === true) &&
        parentNode.Allow === false
      ) {
        parentNode.AscendAllow = true;
      } else if (
        flat.every(
          (fl: AssetNode) => fl.Allow === false && fl.AscendAllow === false
        )
      ) {
        parentNode.AscendAllow = false;
      }

      this.updateParentNodeDeny(parentNode);
    }
  }

  updateChildrenDeny(
    node: AssetNode,
    implicitDeny: boolean,
    implicitAllow: boolean
  ): void {
    const flat: AssetNode[] = !isNil(node.Children)
      ? this.assetTreeTableService.flattenItems(node.Children, 'Children')
      : [];
    node.Deny = false;
    node.ImplicitDeny = implicitDeny;
    node.ImplicitAllow = implicitAllow;
    node.AscendAllow = flat.some(
      (fl: AssetNode) => fl.Allow === true || fl.AscendAllow === true
    );
    node.AscendDeny = flat.some(
      (fl: AssetNode) => fl.Deny === true || fl.AscendDeny === true
    );

    node.Children?.forEach((child: AssetNode) => {
      if (child.Deny === false && child.Allow === false) {
        this.updateChildrenDeny(child, implicitDeny, implicitAllow);
      }
    });
  }

  emitNodes(node: AssetNode, stateChange: EventEmitter<AssetEmitNode[]>) {
    const emitNodes: AssetEmitNode[] = [...this.query.getEmitNodes()];
    const index: number = emitNodes.findIndex(
      (emitNode: AssetEmitNode) => emitNode.AssetId === node.AssetId
    );

    if (index < 0) {
      emitNodes.push({
        Allow: node.Allow,
        AssetId: node.AssetId,
      });
    } else {
      if (node.Allow === false && node.Deny === false) {
        emitNodes.splice(index, 1);
      } else {
        emitNodes[index] = {
          ...emitNodes[index],
          Allow: node.Allow,
        };
      }
    }

    stateChange.emit(emitNodes);
  }

  private updateState(newState: AssetTreeTableServiceState) {
    this.store.next((_state = newState));
  }

  private reset() {
    this.updateState(_initialState);
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
