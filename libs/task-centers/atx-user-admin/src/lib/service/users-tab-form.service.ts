/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { UserDetails } from '@atonix/shared/api';
import { produce } from 'immer';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { RolePermission } from '../model/roles';
import { GroupPermission } from '../model/users';
import { UserAdminRxjsFacade } from './user-admin.rxjs.facade';
import { UsersTabFacade } from './users-tab.service.facade';

@Injectable({
  providedIn: 'root',
})
export class UsersTabFormService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private usersTabFacade: UsersTabFacade,
    private userAdminFacade: UserAdminRxjsFacade
  ) {}

  active(): UntypedFormControl {
    const active = new UntypedFormControl(null);
    active.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: boolean) => {
        this.usersTabFacade.query.selectedUser$
          .pipe(take(1))
          .subscribe((selectedUser: UserDetails) => {
            const newState = produce(
              selectedUser,
              (draftState: UserDetails) => {
                draftState.Active = value;
              }
            );
            this.usersTabFacade.setSelectedUser(newState);
            this.usersTabFacade.setIsDetailsSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });

    return active;
  }

  email(): UntypedFormControl {
    const email = new UntypedFormControl(null);
    email.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        this.usersTabFacade.query.selectedUser$
          .pipe(take(1))
          .subscribe((selectedUser: UserDetails) => {
            const newState = produce(
              selectedUser,
              (draftState: UserDetails) => {
                draftState.Email = value;
              }
            );
            this.usersTabFacade.setSelectedUser(newState);
            this.usersTabFacade.setIsDetailsSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
            this.usersTabFacade.setEmailChanged(true);
          });
      });

    return email;
  }

  lastName(): UntypedFormControl {
    const lastName = new UntypedFormControl(null);
    lastName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        this.usersTabFacade.query.selectedUser$
          .pipe(take(1))
          .subscribe((selectedUser: UserDetails) => {
            const newState = produce(
              selectedUser,
              (draftState: UserDetails) => {
                draftState.LastName = value;
              }
            );
            this.usersTabFacade.setSelectedUser(newState);
            this.usersTabFacade.setIsDetailsSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });

    return lastName;
  }

  firstName(): UntypedFormControl {
    const firstName = new UntypedFormControl(null);
    firstName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        this.usersTabFacade.query.selectedUser$
          .pipe(take(1))
          .subscribe((selectedUser: UserDetails) => {
            const newState = produce(
              selectedUser,
              (draftState: UserDetails) => {
                draftState.FirstName = value;
              }
            );
            this.usersTabFacade.setSelectedUser(newState);
            this.usersTabFacade.setIsDetailsSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });

    return firstName;
  }

  rolePermission(role: RolePermission): UntypedFormControl {
    const rolePermission = new UntypedFormControl(role.Allow);
    rolePermission.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: boolean) => {
        this.usersTabFacade.query.rolePermissions$
          .pipe(take(1))
          .subscribe((rolePermissions: RolePermission[]) => {
            const newState = produce(
              rolePermissions,
              (draftState: RolePermission[]) => {
                draftState.find(
                  (rolePermission: RolePermission) =>
                    rolePermission.Role.Id === role.Role.Id
                ).Allow = value;
              }
            );
            this.usersTabFacade.setRolePermissions(newState);
            this.usersTabFacade.setIsRolesSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });

    return rolePermission;
  }

  serviceAccount = (): UntypedFormControl =>
    new UntypedFormControl({ value: null, disabled: true });

  federatedAccount = (): UntypedFormControl =>
    new UntypedFormControl({ value: null, disabled: true });

  groupPermission(group: GroupPermission): UntypedFormControl {
    const groupPermission = new UntypedFormControl({
      value: group.Allow,
      disabled: false,
    });
    groupPermission.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: boolean) => {
        this.usersTabFacade.query.groupPermissions$
          .pipe(take(1))
          .subscribe((groupPermissions: GroupPermission[]) => {
            const newState = produce(
              groupPermissions,
              (draftState: GroupPermission[]) => {
                draftState.find(
                  (groupPermission: GroupPermission) =>
                    groupPermission.Group.Id === group.Group.Id
                ).Allow = value;
              }
            );
            this.usersTabFacade.setGroupPermissions(newState);
            this.usersTabFacade.setIsGroupsSubTabDirty(true);
            this.userAdminFacade.setIsDirty(true);
          });
      });

    return groupPermission;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
