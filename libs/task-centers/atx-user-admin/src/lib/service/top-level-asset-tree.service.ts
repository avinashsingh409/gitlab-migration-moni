import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TopLevelAssetNode, UserAdminCoreService } from '@atonix/shared/api';
import { throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TopLevelAssetTreeService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getChildren = (
    assetId: string,
    level: number
  ): Observable<TopLevelAssetNode[]> => {
    return this.userAdminCoreService.getTopLevelAssets(assetId).pipe(
      take(1),
      map((children: TopLevelAssetNode[]) =>
        children.map((child: TopLevelAssetNode) => ({
          ...child,
          Level: level + 1,
        }))
      ),
      // eslint-disable-next-line rxjs/no-implicit-any-catch
      catchError((err) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Mesage);
          }
        }
        return throwError('an error occurred');
      })
    );
  };
}
