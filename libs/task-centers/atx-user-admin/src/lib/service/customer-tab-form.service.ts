import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { CustomerDetails } from '@atonix/shared/api';
import { produce } from 'immer';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { UserAdminRxjsFacade } from './user-admin.rxjs.facade';
import { CustomerTabFacade } from './customer-tab.service.facade';

@Injectable({
  providedIn: 'root',
})
export class CustomerTabFormService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private customerTabFacade: CustomerTabFacade,
    private userAdminFacade: UserAdminRxjsFacade
  ) {}

  name(value: string, disabled: boolean): UntypedFormControl {
    const name = new UntypedFormControl({ value, disabled });
    name.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: string) => {
        this.customerTabFacade.query.selectedCustomerDetails$
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((selectedCustomerDetails: CustomerDetails) => {
            const newState = produce(
              selectedCustomerDetails,
              (draftState: CustomerDetails) => {
                draftState.Name = value;
              }
            );
            this.customerTabFacade.setSelectedCustomerDetails(newState);
            this.userAdminFacade.setIsDirty(true, 'customer');
          });
      });

    return name;
  }

  emailDomain(disabled: boolean): UntypedFormControl {
    const emailDomain = new UntypedFormControl({ value: null, disabled });
    return emailDomain;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
