import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { UserAdminRxjsFacadeState } from './user-admin.rxjs.facade';

export class UserAdminRxjsQuery {
  constructor(private store: BehaviorSubject<UserAdminRxjsFacadeState>) {}
  private state$ = this.store.asObservable();

  readonly selectedUser$ = this.state$.pipe(
    map((state) => state.selectedUser),
    distinctUntilChanged()
  );

  readonly selectedNotificationGroup$ = this.state$.pipe(
    map((state) => state.selectedNotificationGroup),
    distinctUntilChanged()
  );

  readonly tabs$ = this.state$.pipe(
    map((state) => state.tabs),
    distinctUntilChanged()
  );

  readonly isDirty$ = this.state$.pipe(
    map((state) => state.isDirty),
    distinctUntilChanged()
  );

  readonly customers$ = this.state$.pipe(
    map((state) => state.customers),
    distinctUntilChanged()
  );

  readonly selectedCustomer$ = this.state$.pipe(
    map((state) => state.selectedCustomer),
    distinctUntilChanged()
  );

  readonly customerTopLevelAssets$ = this.state$.pipe(
    map((state) => state.customerTopLevelAssets),
    distinctUntilChanged()
  );

  readonly vm$: Observable<UserAdminRxjsFacadeState> = combineLatest([
    this.selectedUser$,
    this.selectedNotificationGroup$,
    this.tabs$,
    this.isDirty$,
    this.customers$,
    this.selectedCustomer$,
    this.customerTopLevelAssets$,
  ]).pipe(
    map(
      ([
        selectedUser,
        selectedNotificationGroup,
        tabs,
        isDirty,
        customers,
        selectedCustomer,
        customerTopLevelAssets,
      ]) =>
        ({
          selectedUser,
          selectedNotificationGroup,
          tabs,
          isDirty,
          customers,
          selectedCustomer,
          customerTopLevelAssets,
        } as UserAdminRxjsFacadeState)
    )
  );
}
