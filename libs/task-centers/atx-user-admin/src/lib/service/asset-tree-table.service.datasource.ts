import {
  DataSource,
  CollectionViewer,
  SelectionChange,
} from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import {
  AssetNode,
  CUSTOMER_ID,
  RoleDetails,
  ROLE_DETAILS,
} from '@atonix/shared/api';
import { Observable, merge, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AssetTreeTableServiceFacade } from './asset-tree-table.service.facade';

@Injectable({
  providedIn: 'root',
})
export class AssetTreeTableDataSource
  implements DataSource<AssetNode>, OnDestroy
{
  public data: AssetNode[] = [];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private treeControl: FlatTreeControl<AssetNode>,
    private assetTreeTableServiceFacade: AssetTreeTableServiceFacade,
    @Inject(ROLE_DETAILS) private selectedRole: RoleDetails,
    @Inject(CUSTOMER_ID) private customerId: string
  ) {
    assetTreeTableServiceFacade.query.assetNodes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetNodes: AssetNode[]) => {
        this.data = assetNodes;
      });

    this.data
      .filter((assetNode: AssetNode) => assetNode.IsExpanded)
      .forEach((assetNode: AssetNode) => {
        this.treeControl.expand(assetNode);
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<AssetNode[]> {
    this.treeControl.expansionModel.changed.subscribe(
      (change: SelectionChange<AssetNode>) => {
        if (
          (change as SelectionChange<AssetNode>).added ||
          (change as SelectionChange<AssetNode>).removed
        ) {
          this.handleTreeControl(change as SelectionChange<AssetNode>);
        }
      }
    );

    return merge(
      collectionViewer.viewChange,
      this.assetTreeTableServiceFacade.query.assetNodes$
    ).pipe(map((_) => this.data));
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  disconnect(_collectionViewer: CollectionViewer): void {}

  handleTreeControl(change: SelectionChange<AssetNode>): void {
    if (change.added) {
      change.added.forEach((node: AssetNode) =>
        this.assetTreeTableServiceFacade.toggleNode(
          node,
          true,
          this.selectedRole.Id,
          this.customerId
        )
      );
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach((node: AssetNode) => {
          this.assetTreeTableServiceFacade.toggleNode(
            node,
            false,
            this.selectedRole.Id,
            this.customerId
          );
        });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
