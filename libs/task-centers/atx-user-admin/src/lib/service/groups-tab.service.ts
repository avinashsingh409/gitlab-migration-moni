/* eslint-disable rxjs/no-implicit-any-catch */
import { Injectable } from '@angular/core';
import {
  Role,
  Group,
  UserAdminCoreService,
  GroupDetails,
  User,
} from '@atonix/shared/api';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GroupsTabService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getGroups = (customerId: string): Observable<Group[]> =>
    this.userAdminCoreService.getGroups(customerId, false).pipe(
      take(1),
      map((groups: Group[]) => groups),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getGroupDetails = (groupId: string): Observable<GroupDetails> =>
    this.userAdminCoreService.getGroupDetails(groupId).pipe(
      take(1),
      map((group: GroupDetails) => group),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getRoles = (customerId: string): Observable<Role[]> =>
    this.userAdminCoreService.getRoles(customerId).pipe(
      take(1),
      map((roles: Role[]) => roles),
      catchError((err: any) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Message);
          }
        }
        return throwError('an error occurred');
      })
    );

  getUsers = (
    customerId: string,
    includeDisabled?: boolean,
    includeGroups?: boolean,
    includeServiceAccounts?: boolean,
    includeAncestorUsers?: boolean
  ): Observable<User[]> =>
    this.userAdminCoreService
      .getUsers(
        customerId,
        includeDisabled,
        includeGroups,
        includeServiceAccounts,
        includeAncestorUsers
      )
      .pipe(
        take(1),
        map((users: User[]) => users),
        catchError((err: any) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );
}
