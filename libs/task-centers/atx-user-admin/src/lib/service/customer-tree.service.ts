import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerNode, UserAdminCoreService } from '@atonix/shared/api';
import { throwError } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CustomerTreeService {
  constructor(private userAdminCoreService: UserAdminCoreService) {}

  getChildren = (
    requestedCustomerId: string,
    level: number
  ): Observable<CustomerNode[]> => {
    return this.userAdminCoreService.getCustomers(requestedCustomerId).pipe(
      take(1),
      map((children: CustomerNode[]) =>
        children.map((child: CustomerNode) => ({
          ...child,
          Level: level + 1,
        }))
      ),
      // eslint-disable-next-line rxjs/no-implicit-any-catch
      catchError((err) => {
        if (err?.error?.Results?.length > 0) {
          if (err?.error?.Results[0]?.Message) {
            return throwError(err.error.Results[0].Mesage);
          }
        }
        return throwError('an error occurred');
      })
    );
  };
}
