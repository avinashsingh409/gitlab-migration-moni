/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { GroupDetails, User } from '@atonix/shared/api';
import { UserAdminRxjsFacade } from './user-admin.rxjs.facade';
import { isNil } from '@atonix/atx-core';
import { GroupsTabFacade } from './groups-tab.service.facade';

@Injectable({
  providedIn: 'root',
})
export class GroupDetailsTabsFormService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private groupsTabFacade: GroupsTabFacade,
    private userAdminFacade: UserAdminRxjsFacade
  ) {}

  groupNameControl(value: string, disabled: boolean): UntypedFormControl {
    const groupName = new UntypedFormControl({ value, disabled });
    groupName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((newName: string) => {
        this.groupsTabFacade.query.selectedGroup$
          .pipe(take(1))
          .subscribe((selectedGroup: GroupDetails) => {
            if (!isNil(selectedGroup)) {
              this.groupsTabFacade.updateGroupName(newName);
              this.userAdminFacade.setIsDirty(true);
              this.groupsTabFacade.setNameIsDirty(true);
            }
          });
      });
    return groupName;
  }

  isActiveControl(value: boolean): UntypedFormControl {
    const isActive = new UntypedFormControl(value);
    isActive.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((isActive: boolean) => {
        this.groupsTabFacade.query.selectedGroup$
          .pipe(take(1))
          .subscribe((selectedGroup: GroupDetails) => {
            if (!isNil(selectedGroup)) {
              this.groupsTabFacade.updateGroupIsActive(isActive);
              this.userAdminFacade.setIsDirty(true);
              this.groupsTabFacade.setActiveIsDirty(true);
            }
          });
      });
    return isActive;
  }

  selectedMembersControl(
    availableMembers: User[],
    selectedMembers: { members: User[] }
  ): UntypedFormControl {
    const selectedMembersControl = new UntypedFormControl();
    selectedMembersControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((val: string) => {
        if (val && val.length >= 1) {
          this.groupsTabFacade.query.selectedGroup$
            .pipe(take(1))
            .subscribe((selectedGroup: GroupDetails) => {
              if (!isNil(selectedGroup)) {
                const searchRE = new RegExp(val, 'gi');
                let filteredMembers =
                  availableMembers.filter(({ Email }) =>
                    Email?.match(searchRE)
                  ) ?? [];
                const existingEmails: string[] = selectedGroup.Users?.map(
                  ({ Email }) => Email
                );
                existingEmails.push(
                  ...selectedMembers.members.map(({ Email }) => Email)
                );
                const unique: string[] = [...new Set(existingEmails)];
                filteredMembers = filteredMembers.filter(
                  ({ Email }) => !unique.includes(Email)
                );

                this.groupsTabFacade.addGroupFilteredMembers(filteredMembers);
              }
            });
        }
      });
    return selectedMembersControl;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
