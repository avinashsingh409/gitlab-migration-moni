import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { UsersTabServiceState } from './users-tab.service.facade';

export class UsersTabServiceQuery {
  constructor(private store: BehaviorSubject<UsersTabServiceState>) {}
  private state$ = this.store.asObservable();

  readonly users$ = this.state$.pipe(
    map((state) => state.users),
    distinctUntilChanged()
  );

  readonly selectedUser$ = this.state$.pipe(
    map((state) => state.selectedUser),
    distinctUntilChanged()
  );

  readonly roles$ = this.state$.pipe(
    map((state) => state.roles),
    distinctUntilChanged()
  );

  readonly groups$ = this.state$.pipe(
    map((state) => state.groups),
    distinctUntilChanged()
  );

  readonly rolePermissions$ = this.state$.pipe(
    map((state) => state.rolePermissions),
    distinctUntilChanged()
  );

  readonly groupPermissions$ = this.state$.pipe(
    map((state) => state.groupPermissions),
    distinctUntilChanged()
  );

  readonly filters$ = this.state$.pipe(
    map((state) => state.filters),
    distinctUntilChanged()
  );

  readonly isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  readonly isDetailsSubTabDirty$ = this.state$.pipe(
    map((state) => state.isDetailsSubTabDirty),
    distinctUntilChanged()
  );

  readonly isRolesSubTabDirty$ = this.state$.pipe(
    map((state) => state.isRolesSubTabDirty),
    distinctUntilChanged()
  );

  readonly isGroupsSubTabDirty$ = this.state$.pipe(
    map((state) => state.isGroupsSubTabDirty),
    distinctUntilChanged()
  );

  readonly emailChanged$ = this.state$.pipe(
    map((state) => state.emailChanged),
    distinctUntilChanged()
  );

  readonly vm$: Observable<UsersTabServiceState> = combineLatest([
    this.users$,
    this.selectedUser$,
    this.roles$,
    this.groups$,
    this.rolePermissions$,
    this.groupPermissions$,
    this.filters$,
    this.isLoading$,
    this.isDetailsSubTabDirty$,
    this.isRolesSubTabDirty$,
    this.isGroupsSubTabDirty$,
    this.emailChanged$,
  ]).pipe(
    map(
      ([
        users,
        selectedUser,
        roles,
        groups,
        rolePermissions,
        groupPermissions,
        filters,
        isLoading,
        isDetailsSubTabDirty,
        isRolesSubTabDirty,
        isGroupsSubTabDirty,
        emailChanged,
      ]) => {
        return {
          users,
          selectedUser,
          roles,
          groups,
          rolePermissions,
          groupPermissions,
          filters,
          isLoading,
          isDetailsSubTabDirty,
          isRolesSubTabDirty,
          isGroupsSubTabDirty,
          emailChanged,
        } as UsersTabServiceState;
      }
    )
  );
}
