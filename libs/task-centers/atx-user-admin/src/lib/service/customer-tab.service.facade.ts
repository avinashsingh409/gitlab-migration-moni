import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, forkJoin, Subject } from 'rxjs';
import { CustomerTabService } from './customer-tab.service';
import { CustomerTabServiceQuery } from './customer-tab.service.query';
import {
  CustomerDetails,
  TopLevelAsset,
  TopLevelAssetNode,
} from '@atonix/shared/api';
import { takeUntil } from 'rxjs/operators';

export interface CustomerTabServiceState {
  isLoading?: boolean;
  selectedCustomerDetails: CustomerDetails;
  selectedTopLevelAsset: TopLevelAssetNode;
  canEditCustomer?: boolean;
}

const _initialState: CustomerTabServiceState = {
  isLoading: null,
  selectedCustomerDetails: null,
  selectedTopLevelAsset: null,
  canEditCustomer: null,
};

let _state: CustomerTabServiceState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class CustomerTabFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<CustomerTabServiceState>(_state);
  public query = new CustomerTabServiceQuery(this.store);

  constructor(private customerTabService: CustomerTabService) {}

  setLoading(isLoading?: boolean): void {
    this.updateState({
      ..._state,
      isLoading,
    });
  }

  getCustomerDetails(customerId: string): void {
    forkJoin({
      selectedCustomerDetails:
        this.customerTabService.getCustomerDetails(customerId),
      canEditCustomer:
        this.customerTabService.getCustomerEditPermission(customerId),
    })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        ({
          selectedCustomerDetails,
          canEditCustomer,
        }: {
          selectedCustomerDetails: CustomerDetails;
          canEditCustomer: boolean;
        }) => {
          this.updateState({
            ..._state,
            isLoading: false,
            canEditCustomer,
            selectedCustomerDetails,
            selectedTopLevelAsset: null,
          });
        }
      );
  }

  setSelectedCustomerDetails(selectedCustomerDetails: CustomerDetails): void {
    this.updateState({
      ..._state,
      selectedCustomerDetails,
    });
  }

  setSelectedCustomerDetailsEmailDomain(emailDomain: string[]): void {
    this.updateState({
      ..._state,
      selectedCustomerDetails: {
        ..._state.selectedCustomerDetails,
        EmailDomain: emailDomain,
      },
    });
  }

  setSelectedCustomerDetailsTopLevelAssets(
    topLevelAssets: TopLevelAsset[]
  ): void {
    this.updateState({
      ..._state,
      selectedCustomerDetails: {
        ..._state.selectedCustomerDetails,
        TopLevelAssets: topLevelAssets,
      },
    });
  }

  setSelectedTopLevelAssetNode(selectedTopLevelAsset: TopLevelAssetNode): void {
    this.updateState({
      ..._state,
      selectedTopLevelAsset,
    });
  }

  reset(): void {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: CustomerTabServiceState): void {
    this.store.next((_state = newState));
  }

  destroyCalls(): void {
    this.onDestroy$.next();
  }

  ngOnDestroy(): void {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
