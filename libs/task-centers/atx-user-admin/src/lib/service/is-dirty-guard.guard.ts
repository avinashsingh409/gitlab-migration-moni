import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CanDeactivate } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UnsavedChangesDialogComponent } from '../component/unsaved-changes-dialog/unsaved-changes-dialog.component';
import { ComponentCanDeactivate } from '../model/component-can-deactivate';

@Injectable()
export class IsDirtyGuard implements CanDeactivate<ComponentCanDeactivate> {
  constructor(private dialog: MatDialog) {}

  canDeactivate(component: ComponentCanDeactivate): Observable<boolean> {
    return !component.canDeactivate() ? of(true) : this.openConfirmDialog();
  }

  openConfirmDialog(): Observable<any> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    const dialogRef = this.dialog.open(
      UnsavedChangesDialogComponent,
      dialogConfig
    );

    return dialogRef.afterClosed().pipe(
      take(1),
      map((result: boolean) => {
        return result;
      })
    );
  }
}
