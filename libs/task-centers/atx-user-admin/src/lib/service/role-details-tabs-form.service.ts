/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy, EventEmitter } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import {
  AllResourcePermissions,
  RoleResourcePermissionChange,
} from '../model/roles';
import { AssetNode, Customer, RoleDetails, User } from '@atonix/shared/api';
import { UserAdminRxjsFacade } from './user-admin.rxjs.facade';
import { RolesTabFacade } from './roles-tab.facade';
import { AssetTreeTableServiceFacade } from '../service/asset-tree-table.service.facade';
import { ResourceAccessTypes } from '../model/resource-access-type-enum';
import { AssetEmitNode } from '../model/asset-emit-node';
import { isNil } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class RoleDetailsTabsFormService implements OnDestroy {
  private onDestroy$ = new Subject<void>();

  constructor(
    private rolesTabFacade: RolesTabFacade,
    private userAdminFacade: UserAdminRxjsFacade
  ) {}

  roleNameControl(name: string, disabled: boolean): UntypedFormControl {
    const roleName = new UntypedFormControl({ value: name, disabled });
    roleName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((newName: string) => {
        this.rolesTabFacade.query.selectedRole$
          .pipe(take(1))
          .subscribe((selectedRole: RoleDetails) => {
            if (!isNil(selectedRole)) {
              this.rolesTabFacade.updateRoleName(newName);
              this.userAdminFacade.setIsDirty(true);
              this.rolesTabFacade.setNameIsDirty(true);
            }
          });
      });
    return roleName;
  }

  roleAppPermsFormControl(
    accessTypeId: number,
    appPerm: AllResourcePermissions
  ): UntypedFormControl {
    let initialValue = false;
    switch (accessTypeId) {
      case ResourceAccessTypes.View:
        initialValue = appPerm.ViewPermission?.Allow;
        break;
      case ResourceAccessTypes.Edit:
        initialValue = appPerm.EditPermission?.Allow;
        break;
      case ResourceAccessTypes.Add:
        initialValue = appPerm.AddPermission?.Allow;
        break;
      case ResourceAccessTypes.Delete:
        initialValue = appPerm.DeletePermission?.Allow;
        break;
      case ResourceAccessTypes.BVEdit:
        initialValue = appPerm.AdminPermission?.Allow;
        break;
    }
    const roleAppPermission: UntypedFormControl = new UntypedFormControl({
      value: initialValue,
      disabled: true,
    });
    roleAppPermission.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((value: boolean) => {
        const resourceChange: RoleResourcePermissionChange = {
          AccessTypeId: accessTypeId,
          AllPermissions: appPerm,
          Checked: value,
        };
        this.rolesTabFacade.changeRoleResourcePermission(resourceChange);
        this.userAdminFacade.setIsDirty(true);
        this.rolesTabFacade.setAppPermsIsDirty(true);
      });
    return roleAppPermission;
  }

  assetAllowPermFormControl(
    asset: AssetNode,
    disabled: boolean,
    checkbox: MatCheckbox,
    initialValue: boolean,
    stateChange: EventEmitter<AssetEmitNode[]>,
    assetTreeFacade: AssetTreeTableServiceFacade
  ): UntypedFormControl {
    const allowControl: UntypedFormControl = new UntypedFormControl({
      value: initialValue,
      disabled,
    });
    allowControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => {
        assetTreeFacade.tickedAllow(asset, checkbox, stateChange);
      });
    return allowControl;
  }

  assetDenyPermFormControl(
    asset: AssetNode,
    disabled: boolean,
    checkbox: MatCheckbox,
    initialValue: boolean,
    stateChange: EventEmitter<AssetEmitNode[]>,
    assetTreeFacade: AssetTreeTableServiceFacade
  ): UntypedFormControl {
    const allowControl: UntypedFormControl = new UntypedFormControl({
      value: initialValue,
      disabled,
    });
    allowControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => {
        assetTreeFacade.tickedDeny(asset, checkbox, stateChange);
      });
    return allowControl;
  }

  selectedMembersControl(
    availableMembers: User[],
    selectedMembers: { members: User[] }
  ): UntypedFormControl {
    const selectedMembersControl = new UntypedFormControl();
    selectedMembersControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((val: string) => {
        if (val && val.length >= 1) {
          this.rolesTabFacade.query.selectedRole$
            .pipe(take(1))
            .subscribe((selectedRole: RoleDetails) => {
              if (!isNil(selectedRole)) {
                const searchRE = new RegExp(val, 'gi');
                let filteredMembers =
                  availableMembers.filter(({ Email }) =>
                    Email?.match(searchRE)
                  ) ?? [];
                const existingEmails: string[] = selectedRole.Users?.map(
                  ({ Email }) => Email
                );
                existingEmails.push(
                  ...selectedMembers.members.map(({ Email }) => Email)
                );
                const unique: string[] = [...new Set(existingEmails)];
                filteredMembers = filteredMembers.filter(
                  ({ Email }) => !unique.includes(Email)
                );

                this.rolesTabFacade.addRoleFilteredMembers(filteredMembers);
              }
            });
        }
      });
    return selectedMembersControl;
  }

  selectedCustomersControl(
    availableCustomers: Customer[],
    selectedCustomers: { customers: Customer[] }
  ): UntypedFormControl {
    const selectedCustomersControl = new UntypedFormControl();
    selectedCustomersControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((val: string) => {
        if (val && val.length >= 1) {
          this.rolesTabFacade.query.selectedRole$
            .pipe(take(1))
            .subscribe((selectedRole: RoleDetails) => {
              if (!isNil(selectedRole)) {
                const searchRE = new RegExp(val, 'gi');
                let filteredCustomers =
                  availableCustomers.filter(({ Name }) =>
                    Name?.match(searchRE)
                  ) ?? [];
                const existingCustomers: string[] = selectedRole.Shares?.map(
                  ({ Name }) => Name
                );
                existingCustomers.push(
                  ...selectedCustomers.customers.map(({ Name }) => Name)
                );
                const unique: string[] = [...new Set(existingCustomers)];
                filteredCustomers = filteredCustomers.filter(
                  ({ Name }) => !unique.includes(Name)
                );

                this.rolesTabFacade.addRoleFilteredCustomers(filteredCustomers);
              }
            });
        }
      });
    return selectedCustomersControl;
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
