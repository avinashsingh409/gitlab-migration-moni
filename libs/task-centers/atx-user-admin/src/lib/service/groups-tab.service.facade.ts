import { Injectable, OnDestroy } from '@angular/core';
import { Role, Group, GroupDetails, User } from '@atonix/shared/api';
import { BehaviorSubject, forkJoin, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GroupsTabServiceQuery } from './groups-tab.service.query';
import { RolePermission } from '../model/roles';
import { isNil } from '@atonix/atx-core';
import { GroupsTabService } from './groups-tab.service';
import { produce } from 'immer';

export interface GroupsTabServiceState {
  selectedGroup?: GroupDetails;
  groups: Group[];
  roles: Role[];
  rolePermissions: RolePermission[];
  filters: any[];
  isLoading?: boolean;
  isMembersSubTabDirty: boolean;
  isRolesSubTabDirty: boolean;
  nameIsDirty: boolean;
  activeIsDirty: boolean;
  users: User[];
  filteredMembers: User[];
}

const _initialState: GroupsTabServiceState = {
  selectedGroup: null,
  groups: [],
  roles: [],
  rolePermissions: [],
  filters: [],
  isLoading: null,
  isMembersSubTabDirty: false,
  isRolesSubTabDirty: false,
  nameIsDirty: false,
  activeIsDirty: false,
  users: [],
  filteredMembers: [],
};

let _state: GroupsTabServiceState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class GroupsTabFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<GroupsTabServiceState>(_state);
  public query = new GroupsTabServiceQuery(this.store);

  constructor(private groupsTabService: GroupsTabService) {}

  getGroupsRolesAndUsers(customerId: string): void {
    forkJoin({
      groups: this.groupsTabService.getGroups(customerId),
      roles: this.groupsTabService.getRoles(customerId),
      users: this.groupsTabService.getUsers(
        customerId,
        false,
        false,
        false,
        true
      ),
    })
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(
        ({
          groups,
          roles,
          users,
        }: {
          groups: Group[];
          roles: Role[];
          users: User[];
        }) => {
          this.updateState({
            ..._state,
            groups,
            roles,
            users,
            selectedGroup: null,
            isLoading: false,
          });
        }
      );
  }

  addNewGroup(newGroup: Group): void {
    this.query.groups$.pipe(take(1)).subscribe((groups: Group[]) => {
      // add new group to top of the groups list
      const newGroups: Group[] = [newGroup].concat(groups);
      this.updateState({
        ..._state,
        groups: newGroups,
      });
    });
  }

  updateSelectedGroup(selectedGroup: Group): void {
    if (!isNil(selectedGroup)) {
      this.getGroupDetails(selectedGroup.Id);
    } else {
      this.updateState({
        ..._state,
        rolePermissions: this.buildGroupRolePermissionMatrix(null),
        isLoading: false,
      });
    }
  }

  getGroupDetails(groupId: string): void {
    this.groupsTabService
      .getGroupDetails(groupId)
      .subscribe((group: GroupDetails) => {
        this.updateState({
          ..._state,
          isLoading: false,
          rolePermissions: this.buildGroupRolePermissionMatrix(group),
          selectedGroup: group,
        });
      });
  }

  buildGroupRolePermissionMatrix(group: GroupDetails): RolePermission[] {
    // build matrix of available roles with selected group's permission
    let allRoles: RolePermission[] = [];
    this.query.roles$.pipe(take(1)).subscribe((roles: Role[]) => {
      allRoles = roles.map((resrc: Role) => {
        // if a role is selected, look for corresponding permissions on the list and attach accordingly
        const role: Role = group?.Roles.find((r: Role) => {
          return r.Id === resrc.Id;
        });
        const thisRole: RolePermission = {
          Role: resrc,
          Allow: role ? true : false,
        };
        return thisRole;
      });
    });
    return allRoles;
  }

  addMembersToGroup(members: User[]): void {
    this.query.selectedGroup$
      .pipe(take(1))
      .subscribe((selectedGroup: GroupDetails) => {
        // add new member to the top of the members list
        const updatedSelectedGroup = produce(
          selectedGroup,
          (draftState: GroupDetails) => {
            draftState.Users = members.concat(draftState.Users);
          }
        );
        this.updateState({
          ..._state,
          selectedGroup: updatedSelectedGroup,
        });
      });
  }

  removeMemberFromGroup(member: User): void {
    this.query.selectedGroup$
      .pipe(take(1))
      .subscribe((selectedGroup: GroupDetails) => {
        // remove member from the members list
        const updatedSelectedGroup = produce(
          selectedGroup,
          (draftState: GroupDetails) => {
            draftState.Users.splice(
              draftState.Users.findIndex((r) => r.Id === member.Id),
              1
            );
          }
        );
        this.updateState({
          ..._state,
          selectedGroup: updatedSelectedGroup,
        });
      });
  }

  removeAllMembersFromGroup(): void {
    this.query.selectedGroup$
      .pipe(take(1))
      .subscribe((selectedGroup: GroupDetails) => {
        // remove all members from the members list
        const updatedSelectedGroup = produce(
          selectedGroup,
          (draftState: GroupDetails) => {
            draftState.Users = [];
          }
        );
        this.updateState({
          ..._state,
          selectedGroup: updatedSelectedGroup,
        });
      });
  }

  setLoading(isLoading?: boolean): void {
    this.updateState({
      ..._state,
      isLoading,
    });
  }

  setFilters(filters: any[]): void {
    this.updateState({
      ..._state,
      filters,
    });
  }

  removeFilter(filter: any, callback: (filterName: string) => void): void {
    this.query.filters$.pipe(take(1)).subscribe((f: any[]) => {
      const filters: any[] = [...f];
      const index: number = filters.indexOf(filter);
      if (index >= 0) {
        filters.splice(index, 1);
        this.updateState({
          ..._state,
          filters,
        });
        callback(filter.Name);
      }
    });
  }

  setSelectedGroup(selectedGroup: GroupDetails): void {
    this.updateState({
      ..._state,
      selectedGroup,
    });
  }

  setIsMembersSubTabDirty(isMembersSubTabDirty: boolean): void {
    this.updateState({ ..._state, isMembersSubTabDirty });
  }

  setIsRolesSubTabDirty(isRolesSubTabDirty: boolean): void {
    this.updateState({ ..._state, isRolesSubTabDirty });
  }

  setGroups(groups: Group[]): void {
    this.updateState({ ..._state, groups });
  }

  setGroupsSubTabsDirty(value: boolean): void {
    this.updateState({
      ..._state,
      isMembersSubTabDirty: value,
      isRolesSubTabDirty: value,
      nameIsDirty: value,
      activeIsDirty: value,
    });
  }

  setRolePermissions(rolePermissions: RolePermission[]): void {
    this.updateState({
      ..._state,
      rolePermissions,
    });
  }

  updateGroupName(newName: string): void {
    this.query.selectedGroup$.pipe(take(1)).subscribe((group: GroupDetails) => {
      const updatedGroup = produce(group, (draftState: GroupDetails) => {
        draftState.Name = newName;
      });
      this.updateState({
        ..._state,
        selectedGroup: updatedGroup,
      });
    });
  }

  setNameIsDirty(nameIsDirty: boolean): void {
    this.updateState({
      ..._state,
      nameIsDirty,
    });
  }

  updateGroupIsActive(isActive: boolean): void {
    this.query.selectedGroup$.pipe(take(1)).subscribe((group: GroupDetails) => {
      const updatedGroup = produce(group, (draftState: GroupDetails) => {
        draftState.Active = isActive;
      });
      this.updateState({
        ..._state,
        selectedGroup: updatedGroup,
      });
    });
  }

  setActiveIsDirty(activeIsDirty: boolean): void {
    this.updateState({
      ..._state,
      activeIsDirty,
    });
  }

  addGroupFilteredMembers(filteredMembers: User[]): void {
    this.updateState({
      ..._state,
      filteredMembers,
    });
  }

  reset(): void {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: GroupsTabServiceState): void {
    this.store.next((_state = newState));
  }

  destroyCalls(): void {
    this.onDestroy$.next();
  }

  ngOnDestroy(): void {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
