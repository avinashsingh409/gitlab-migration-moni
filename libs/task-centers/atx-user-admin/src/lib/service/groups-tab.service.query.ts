import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { GroupsTabServiceState } from './groups-tab.service.facade';

export class GroupsTabServiceQuery {
  constructor(private store: BehaviorSubject<GroupsTabServiceState>) {}
  private state$ = this.store.asObservable();

  readonly selectedGroup$ = this.state$.pipe(
    map((state) => state.selectedGroup),
    distinctUntilChanged()
  );

  readonly groups$ = this.state$.pipe(
    map((state) => state.groups),
    distinctUntilChanged()
  );

  readonly roles$ = this.state$.pipe(
    map((state) => state.roles),
    distinctUntilChanged()
  );

  readonly rolePermissions$ = this.state$.pipe(
    map((state) => state.rolePermissions),
    distinctUntilChanged()
  );

  readonly filters$ = this.state$.pipe(
    map((state) => state.filters),
    distinctUntilChanged()
  );

  readonly isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  readonly isMembersSubTabDirty$ = this.state$.pipe(
    map((state) => state.isMembersSubTabDirty),
    distinctUntilChanged()
  );

  readonly isRolesSubTabDirty$ = this.state$.pipe(
    map((state) => state.isRolesSubTabDirty),
    distinctUntilChanged()
  );

  readonly nameIsDirty$ = this.state$.pipe(
    map((state) => state.nameIsDirty),
    distinctUntilChanged()
  );

  readonly activeIsDirty$ = this.state$.pipe(
    map((state) => state.activeIsDirty),
    distinctUntilChanged()
  );

  readonly users$ = this.state$.pipe(
    map((state) => state.users),
    distinctUntilChanged()
  );

  readonly filteredMembers$ = this.state$.pipe(
    map((state) => state.filteredMembers),
    distinctUntilChanged()
  );

  readonly vm$: Observable<GroupsTabServiceState> = combineLatest([
    this.selectedGroup$,
    this.groups$,
    this.roles$,
    this.rolePermissions$,
    this.filters$,
    this.isLoading$,
    this.isMembersSubTabDirty$,
    this.isRolesSubTabDirty$,
    this.nameIsDirty$,
    this.activeIsDirty$,
    this.users$,
    this.filteredMembers$,
  ]).pipe(
    map(
      ([
        selectedGroup,
        groups,
        roles,
        rolePermissions,
        filters,
        isLoading,
        isMembersSubTabDirty,
        isRolesSubTabDirty,
        nameIsDirty,
        activeIsDirty,
        users,
        filteredMembers,
      ]) => {
        return {
          selectedGroup,
          groups,
          roles,
          rolePermissions,
          filters,
          isLoading,
          isMembersSubTabDirty,
          isRolesSubTabDirty,
          nameIsDirty,
          activeIsDirty,
          users,
          filteredMembers,
        } as GroupsTabServiceState;
      }
    )
  );
}
