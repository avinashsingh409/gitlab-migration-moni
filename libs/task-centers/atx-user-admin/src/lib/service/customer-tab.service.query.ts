import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { CustomerTabServiceState } from './customer-tab.service.facade';

export class CustomerTabServiceQuery {
  constructor(private store: BehaviorSubject<CustomerTabServiceState>) {}
  private state$ = this.store.asObservable();

  readonly isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  readonly selectedCustomerDetails$ = this.state$.pipe(
    map((state) => state.selectedCustomerDetails),
    distinctUntilChanged()
  );

  readonly selectedTopLevelAsset$ = this.state$.pipe(
    map((state) => state.selectedTopLevelAsset),
    distinctUntilChanged()
  );

  readonly canEditCustomer$ = this.state$.pipe(
    map((state) => state.canEditCustomer),
    distinctUntilChanged()
  );

  readonly vm$: Observable<CustomerTabServiceState> = combineLatest([
    this.isLoading$,
    this.selectedCustomerDetails$,
    this.selectedTopLevelAsset$,
    this.canEditCustomer$,
  ]).pipe(
    map(
      ([
        isLoading,
        selectedCustomerDetails,
        selectedTopLevelAsset,
        canEditCustomer,
      ]) => {
        return {
          isLoading,
          selectedCustomerDetails,
          selectedTopLevelAsset,
          canEditCustomer,
        } as CustomerTabServiceState;
      }
    )
  );
}
