import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { cloneDeep } from 'lodash';
import { TopLevelAssetTreeQuery } from './top-level-asset-tree.service.query';
import { TopLevelAssetTreeService } from './top-level-asset-tree.service';
import { TopLevelAssetNode } from '@atonix/shared/api';

export interface TopLevelAssetTreeState {
  nodes: TopLevelAssetNode[];
}

const _initialState: TopLevelAssetTreeState = {
  nodes: [],
};

let _state: TopLevelAssetTreeState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class TopLevelAssetTreeFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<TopLevelAssetTreeState>(_state);
  public query = new TopLevelAssetTreeQuery(this.store);

  constructor(private topLevelAssetTreeService: TopLevelAssetTreeService) {}

  setNodes(nodes: TopLevelAssetNode[]): void {
    this.updateState({
      ..._state,
      nodes,
    });
  }

  toggleNode(node: TopLevelAssetNode, expand: boolean): void {
    const nodes: TopLevelAssetNode[] = [...this.query.getNodes()];
    const index: number = nodes.indexOf(node);
    if (index < 0) {
      return;
    }

    if (expand) {
      node.IsLoading = true;
      if (node.Children == null || node.Children.length === 0) {
        this.topLevelAssetTreeService
          .getChildren(node.AssetId, node.Level)
          .pipe(take(1))
          .subscribe((children: TopLevelAssetNode[]) => {
            node.Children = children;
            nodes.splice(index + 1, 0, ...node.Children);
            node.IsExpanded = true;
            node.IsLoading = false;
            this.updateState({
              ..._state,
              nodes,
            });
          });
      } else {
        node.Children = cloneDeep(node.Children);
        nodes.splice(index + 1, 0, ...node.Children);
        node.IsExpanded = true;
        node.IsLoading = false;
        this.updateState({
          ..._state,
          nodes,
        });
      }
    } else {
      let count = 0;
      for (
        let i = index + 1;
        i < nodes.length && nodes[i].Level > node.Level;
        i++, count++
      );
      nodes.splice(index + 1, count);
      node.IsExpanded = false;
      this.updateState({
        ..._state,
        nodes,
      });
    }
  }

  private updateState(newState: TopLevelAssetTreeState) {
    this.store.next((_state = newState));
  }

  private reset() {
    this.updateState(_initialState);
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
