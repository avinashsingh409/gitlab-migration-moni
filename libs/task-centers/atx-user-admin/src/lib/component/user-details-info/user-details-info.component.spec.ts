import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  ControlContainer,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { UserDetailsInfoComponent } from './user-details-info.component';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('UserDetailsInfoComponent', () => {
  let component: UserDetailsInfoComponent;
  let mockUsersTabFormService: UsersTabFormService;
  let fixture: ComponentFixture<UserDetailsInfoComponent>;

  beforeEach(() => {
    mockUsersTabFormService = createMockWithValues(UsersTabFormService, {
      active: jest.fn(() => new FormControl(false)),
      email: jest.fn(() => new FormControl('test@test.com')),
      lastName: jest.fn(() => new FormControl('McTester')),
      firstName: jest.fn(() => new FormControl('Tester')),
      serviceAccount: jest.fn(() => new FormControl(true)),
      federatedAccount: jest.fn(() => new FormControl(true)),
    });
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        NoopAnimationsModule,
      ],
      declarations: [UserDetailsInfoComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: UsersTabFormService, useValue: mockUsersTabFormService },
        { provide: APP_CONFIG, useValue: AppConfig },
        UsersTabFormService,
      ],
    }).compileComponents();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(UserDetailsInfoComponent);
    component = fixture.componentInstance;
    component.userDetailsForm = new FormGroup({
      active: new FormControl(false),
      email: new FormControl('test'),
      lastName: new FormControl('test'),
      firstName: new FormControl('test'),
      serviceAccount: new FormControl(true),
      federatedAccount: new FormControl(true),
    });
    fixture.detectChanges();
    await fixture.whenRenderingDone();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
