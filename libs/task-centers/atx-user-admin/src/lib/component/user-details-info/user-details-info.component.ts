import {
  Component,
  OnDestroy,
  Input,
  SimpleChanges,
  OnChanges,
  EventEmitter,
  Output,
} from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { UserDetails } from '@atonix/shared/api';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-user-admin-user-details-info',
  templateUrl: './user-details-info.component.html',
  styleUrls: ['./user-details-info.component.scss'],
})
export class UserDetailsInfoComponent implements OnChanges, OnDestroy {
  @Input() selectedUser?: UserDetails;
  @Input() isLoading: boolean;
  @Output() initiatePasswordReset = new EventEmitter<string>();

  public userDetailsForm: UntypedFormGroup;
  private onDestroy$ = new Subject<void>();

  constructor(private usersTabFormService: UsersTabFormService) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.userDetailsForm = new UntypedFormGroup({
      active: this.usersTabFormService.active(),
      email: this.usersTabFormService.email(),
      lastName: this.usersTabFormService.lastName(),
      firstName: this.usersTabFormService.firstName(),
      serviceAccount: this.usersTabFormService.serviceAccount(),
      federatedAccount: this.usersTabFormService.federatedAccount(),
    });

    if (!isNil(changes) && !isNil(changes.selectedUser)) {
      this.userDetailsForm.patchValue(
        {
          active: this.selectedUser.Active,
          email: this.selectedUser.Email,
          lastName: this.selectedUser.LastName,
          firstName: this.selectedUser.FirstName,
          serviceAccount: this.selectedUser.ServiceAccount,
          federatedAccount: this.selectedUser.FederatedAccount,
        },
        { emitEvent: false }
      );
    }
  }

  onInitiateBtnClick(): void {
    this.initiatePasswordReset.emit(this.selectedUser.Id);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
