import {
  Component,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
  ElementRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import {
  EnterpriseCoreModule,
  GridOptions,
  GridReadyEvent,
  GridApi,
  Module,
  ClientSideRowModelModule,
  SelectionChangedEvent,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import {
  ColDef,
  ColumnApi,
  FilterChangedEvent,
  RowNode,
} from '@ag-grid-community/core';
import { Role, RoleDetails } from '@atonix/shared/api';
import {
  UserAdminEventBusService,
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
  UserAdminStateChange,
} from '../../service/user-admin-event-bus.service';
import { NavFacade } from '@atonix/atx-navigation';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.scss'],
})
export class RolesListComponent implements AfterViewInit, OnChanges, OnDestroy {
  @Input() roles: Role[];
  @Input() isLoading?: boolean;
  @Input() gridFilters: any[];
  @Input() selectedRole: RoleDetails;

  public columnApi: ColumnApi;
  public gridApi: GridApi;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ClientSideRowModelModule,
    SetFilterModule,
  ];
  public gridOptions: GridOptions = {
    headerHeight: 30,
    rowHeight: 30,
    animateRows: true,
    defaultColDef: {
      resizable: true,
      sortable: true,
      editable: false,
      filter: true,
      floatingFilter: true,
    },
    columnDefs: [
      {
        colId: 'RoleID',
        headerName: 'RoleID',
        field: 'Id',
        sortable: false,
        hide: true,
      },
      {
        colId: 'RoleName',
        headerName: 'Role',
        field: 'Name',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
        flex: 1,
      },
      {
        colId: 'RoleType',
        headerName: 'Role Type',
        field: 'RoleType',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Asset', 'Application', 'Asset+Application'],
        },
        flex: 1,
      },
    ],
    floatingFiltersHeight: 30,
    debug: false,
    rowSelection: 'single',
    suppressRowDeselection: true,
    getRowId: (params) => `${params.data.Id}`,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.initializeGrid();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
  };
  private previousRowNodeId: string = null;
  private onDestroy$ = new Subject<void>();

  constructor(
    public navFacade: NavFacade,
    private userAdminEventBus: UserAdminEventBusService,
    private el: ElementRef
  ) {}

  ngAfterViewInit(): void {
    this.el.nativeElement
      .querySelectorAll('.ag-text-field-input')
      ?.forEach((element: HTMLInputElement) => {
        element.autocomplete = 'off';
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(this.gridApi) && !isNil(changes)) {
      if (!isNil(changes.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else if (this.isLoading === false) {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(changes.roles)) {
        this.gridApi.setRowData(this.roles);
        if (
          !isNil(this.selectedRole) &&
          this.gridApi.getSelectedNodes().length === 0
        ) {
          this.gridApi
            .getRowNode(`${this.selectedRole.Id}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  initializeGrid(): void {
    if (!isNil(this.columnApi) && !isNil(this.gridApi)) {
      if (!isNil(this.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else if (this.isLoading === false) {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(this.roles)) {
        this.gridApi.setRowData(this.roles);
        if (
          !isNil(this.selectedRole) &&
          this.gridApi.getSelectedNodes().length === 0
        ) {
          this.gridApi
            .getRowNode(`${this.selectedRole.Id}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  selectionChanged(event: SelectionChangedEvent): void {
    const node: RowNode = event.api.getSelectedNodes()[0];
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.CLICK_ROLE_LIST_ROW,
      value: {
        role: node.data as Role,
        callbackFn: (value: boolean): void => {
          if (value === false) {
            event.api.getRowNode(node.id).setSelected(false, false, true);
            event.api
              .getRowNode(this.previousRowNodeId)
              .setSelected(true, true, true);
            event.api.clearFocusedCell();
          } else {
            this.previousRowNodeId = node.id;
          }
        },
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];

    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef.headerName,
      });
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_ROLE_FILTERS,
      value: filters,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  removeFilter(filter: any): void {
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_ROLE_FILTER,
      value: {
        filter,
        callbackFn: this.postRemoveFilterCallbackFn.bind(this),
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName).setModel(null);
    this.refreshGrid();
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key).setModel(null);
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_ROLE_FILTERS,
      value: [],
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
    this.refreshGrid();
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api.onFilterChanged();
    }
  }

  trackByFn = (_: number, item: any): string => item?.Name;

  ngOnDestroy(): void {
    this.gridApi.destroy();
    this.gridApi = null;

    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
