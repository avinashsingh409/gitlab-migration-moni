import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RoleDetailsTabsComponent } from './role-details-tabs.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { RoleAppPermissionsComponent } from '../role-app-perms/role-app-perms.component';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('RoleDetailsTabsComponent', () => {
  let component: RoleDetailsTabsComponent;
  let fixture: ComponentFixture<RoleDetailsTabsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        AtxMaterialModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [RoleDetailsTabsComponent, RoleAppPermissionsComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleDetailsTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
