import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { isNil } from '@atonix/atx-core';
import {
  AssetNode,
  AssetPermission,
  Customer,
  RoleDetails,
  User,
} from '@atonix/shared/api';
import { AssetEmitNode } from '../../model/asset-emit-node';
import { AllResourcePermissions } from '../../model/roles';
import { RoleDetailsTabsFormService } from '../../service/role-details-tabs-form.service';

@Component({
  selector: 'atx-user-admin-role-details-view-tabs',
  templateUrl: './role-details-tabs.component.html',
  styleUrls: ['./role-details-tabs.component.scss'],
})
export class RoleDetailsTabsComponent implements OnChanges {
  @Input() selectedRole: RoleDetails;
  @Input() selectedCustomer: Customer;
  @Input() users: User[];
  @Input() nameIsDirty: boolean;
  @Input() appPermsIsDirty: boolean;
  @Input() assetPermsIsDirty: boolean;
  @Input() membershipIsDirty: boolean;
  @Input() sharingIsDirty: boolean;
  @Input() allResourcePermissions: AllResourcePermissions[];
  @Input() filteredMembers: User[];
  @Input() assets: AssetNode[];
  @Input() assetPermissions: AssetPermission[];
  @Input() isLoading?: boolean;
  @Input() availableCustomers: Customer[];
  @Input() filteredCustomers: Customer[];
  @Output() assetTreeTableStateChange = new EventEmitter<AssetEmitNode[]>();

  public roleDetailsTabsForm: UntypedFormGroup;
  public selectedIndex = 0;
  public isRoleShareable: boolean;

  constructor(private roleDetailsTabsFormService: RoleDetailsTabsFormService) {
    this.roleDetailsTabsForm = new UntypedFormGroup({
      roleName: this.roleDetailsTabsFormService.roleNameControl(null, true),
    });
  }

  selectTab(tab: any): void {
    if (!isNil(tab?.tabIndex)) {
      this.selectedIndex = tab.tabIndex;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes) && !isNil(changes.selectedRole)) {
      if (!isNil(this.selectedRole)) {
        this.isRoleShareable =
          this.selectedRole.Ownership === 'Owning' &&
          !isNil(this.selectedRole.ResourcePermissions) &&
          this.selectedRole.ResourcePermissions.length > 0 &&
          !isNil(this.selectedRole.AssetPermissions) &&
          this.selectedRole.AssetPermissions.length === 0;
        this.roleDetailsTabsForm = new UntypedFormGroup({
          roleName: this.roleDetailsTabsFormService.roleNameControl(
            this.selectedRole.Name,
            this.selectedRole.Ownership !== 'Owning'
          ),
        });
      } else {
        this.roleDetailsTabsForm = new UntypedFormGroup({
          roleName: this.roleDetailsTabsFormService.roleNameControl(null, true),
        });
      }
    }
  }
}
