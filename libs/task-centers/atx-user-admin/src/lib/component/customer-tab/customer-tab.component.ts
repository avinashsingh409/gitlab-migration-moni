import { Component, HostListener, OnDestroy } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { isNil } from '@atonix/atx-core';
import {
  CustomerNode,
  TopLevelAsset,
  TopLevelAssetNode,
} from '@atonix/shared/api';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { ComponentCanDeactivate } from '../../model/component-can-deactivate';
import { CustomerTabFormService } from '../../service/customer-tab-form.service';
import {
  CustomerTabFacade,
  CustomerTabServiceState,
} from '../../service/customer-tab.service.facade';
import { UserAdminRxjsFacade } from '../../service/user-admin.rxjs.facade';

@Component({
  selector: 'atx-customer-tab',
  templateUrl: './customer-tab.component.html',
  styleUrls: ['./customer-tab.component.scss'],
})
export class CustomerTabComponent implements OnDestroy, ComponentCanDeactivate {
  public vm$ = new Observable<CustomerTabServiceState>();
  public customerDetailsForm: UntypedFormGroup;
  public emailDomainDataSource = new MatTableDataSource<string>();
  public displayedEmailDomainColumns: string[] = ['domain', 'remove'];
  public topLevelAssetsDataSource = new MatTableDataSource<TopLevelAsset>();
  public displayedTopLevelAssetsColumns: string[] = ['asset', 'remove'];
  public canEditCustomer?: boolean;
  public selectedAssetExists?: boolean;
  private onDestroy$ = new Subject<void>();

  constructor(
    public userAdminRxjsFacade: UserAdminRxjsFacade,
    private customerTabFacade: CustomerTabFacade,
    private customerTabFormService: CustomerTabFormService
  ) {
    this.vm$ = this.customerTabFacade.query.vm$;

    this.userAdminRxjsFacade.setIsDirty(false);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((selectedCustomer: CustomerNode) => {
        if (!isNil(selectedCustomer)) {
          this.customerTabFacade.destroyCalls();
          this.customerTabFacade.setLoading(true);
          this.customerTabFacade.getCustomerDetails(selectedCustomer.Id);
        }
      });
    this.customerTabFacade.query.vm$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((vm: CustomerTabServiceState) => {
        if (!isNil(vm.canEditCustomer) && !isNil(vm.selectedCustomerDetails)) {
          this.canEditCustomer = vm.canEditCustomer;
          this.emailDomainDataSource.data = [
            ...vm.selectedCustomerDetails.EmailDomain,
          ];
          this.topLevelAssetsDataSource.data = [
            ...vm.selectedCustomerDetails.TopLevelAssets,
          ];
          this.selectedAssetExists =
            this.topLevelAssetsDataSource.data.findIndex(
              (asset: TopLevelAsset) =>
                asset.AssetId === vm.selectedTopLevelAsset?.AssetId
            ) > -1;
          this.customerDetailsForm = new UntypedFormGroup({
            name: this.customerTabFormService.name(
              vm.selectedCustomerDetails.Name,
              !this.canEditCustomer
            ),
            emailDomain: this.customerTabFormService.emailDomain(
              !this.canEditCustomer
            ),
          });
        }
      });
  }

  canDeactivate(): boolean | Observable<boolean> {
    let value: boolean = null;
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        value = isDirty;
      });

    return value;
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.canDeactivate()) {
      $event.returnValue =
        'You have unsaved changes. Are you sure you want to continue? This action cannot be undone.';
    }
  }

  addAsset(): void {
    this.customerTabFacade.query.selectedTopLevelAsset$
      .pipe(take(1))
      .subscribe((node: TopLevelAssetNode) => {
        if (!isNil(node)) {
          this.topLevelAssetsDataSource.data.push({
            AssetName: node.AssetName,
            AssetId: node.AssetId,
            HasChildren: node.HasChildren,
          });
          this.customerTabFacade.setSelectedCustomerDetailsTopLevelAssets(
            this.topLevelAssetsDataSource.data
          );
          this.userAdminRxjsFacade.setIsDirty(true, 'customer');
        }
      });
  }

  public onNodeClick(node: TopLevelAssetNode): void {
    this.customerTabFacade.setSelectedTopLevelAssetNode(node);
  }

  addDomain(): void {
    const emailDomainValue: string =
      this.customerDetailsForm.controls['emailDomain'].value;
    if (!isNil(emailDomainValue) && emailDomainValue !== '') {
      this.emailDomainDataSource.data.push(emailDomainValue);
      this.customerDetailsForm.patchValue({
        emailDomain: '',
      });
      this.customerTabFacade.setSelectedCustomerDetailsEmailDomain(
        this.emailDomainDataSource.data
      );
      this.userAdminRxjsFacade.setIsDirty(true, 'customer');
    }
  }

  removeSelectedDomain(index: number): void {
    if (index > -1) {
      this.emailDomainDataSource.data.splice(index, 1);
      this.emailDomainDataSource.data = [...this.emailDomainDataSource.data];
      this.customerTabFacade.setSelectedCustomerDetailsEmailDomain(
        this.emailDomainDataSource.data
      );
      this.userAdminRxjsFacade.setIsDirty(true, 'customer');
    }
  }

  removeSelectedAsset(index: number): void {
    if (index > -1) {
      this.topLevelAssetsDataSource.data.splice(index, 1);
      this.topLevelAssetsDataSource.data = [
        ...this.topLevelAssetsDataSource.data,
      ];
      this.customerTabFacade.setSelectedCustomerDetailsTopLevelAssets(
        this.topLevelAssetsDataSource.data
      );
      this.userAdminRxjsFacade.setIsDirty(true, 'customer');
    }
  }

  ngOnDestroy(): void {
    this.userAdminRxjsFacade.setIsDirty(false, 'customer');
    this.customerTabFacade.destroyCalls();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
