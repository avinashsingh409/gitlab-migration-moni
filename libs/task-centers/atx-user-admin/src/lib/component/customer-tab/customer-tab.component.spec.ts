import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { CustomerTabComponent } from './customer-tab.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('CustomerTabComponent', () => {
  let component: CustomerTabComponent;
  let fixture: ComponentFixture<CustomerTabComponent>;
  let mockToastService: ToastService;

  beforeEach(waitForAsync(() => {
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [CustomerTabComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ToastService, useValue: mockToastService },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
