import { FlatTreeControl } from '@angular/cdk/tree';
import {
  Component,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { isNil } from '@atonix/atx-core';
import { CustomerNode } from '@atonix/shared/api';
import { Subject } from 'rxjs';
import { CustomerTreeDataSource } from '../../service/customer-tree.service.datasource';
import { CustomerTreeFacade } from '../../service/customer-tree.service.facade';

@Component({
  selector: 'atx-customer-tree-old',
  templateUrl: './customer-tree-old.component.html',
  styleUrls: ['./customer-tree-old.component.scss'],
})
export class CustomerTreeOldComponent implements OnChanges, OnDestroy {
  @Input() nodes: CustomerNode[];
  @Input() selectedNode: CustomerNode;
  @Output() nodeClicked = new EventEmitter<CustomerNode>();

  public displayedColumns: string[] = ['customers'];
  public treeControl: FlatTreeControl<CustomerNode>;
  public dataSource: CustomerTreeDataSource;
  private onDestroy$ = new Subject<void>();

  constructor(private facade: CustomerTreeFacade) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (!isNil(changes.nodes) && this.nodes.length > 0) {
        this.facade.setNodes(this.nodes);
        this.treeControl = new FlatTreeControl<CustomerNode>(
          (node: CustomerNode) => node.Level,
          (node: CustomerNode) => !isNil(node.Children)
        );
        this.dataSource = new CustomerTreeDataSource(
          this.treeControl,
          this.facade
        );
      }
      if (!isNil(changes.selectedNode) && !isNil(this.dataSource)) {
        this.facade.setSelectedNode(this.selectedNode.Id);
      }
    }
  }

  selectNode(node: CustomerNode): void {
    if (node.Id !== this.selectedNode.Id) {
      this.nodeClicked.emit(node);
    }
  }

  expandNode(event: PointerEvent | MouseEvent, node: CustomerNode): void {
    event.stopPropagation();
    this.treeControl.toggle(node);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
