import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ControlContainer } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';

import { CustomerTreeOldComponent } from './customer-tree-old.component';

describe('CustomerTreeComponent', () => {
  let component: CustomerTreeOldComponent;
  let fixture: ComponentFixture<CustomerTreeOldComponent>;
  let mockToastService: ToastService;

  beforeEach(waitForAsync(() => {
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, HttpClientTestingModule],
      declarations: [CustomerTreeOldComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ToastService, useValue: mockToastService },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerTreeOldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
