import { Component, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { catchError, take } from 'rxjs/operators';
import { UserAdminCoreService } from '@atonix/shared/api';
import { UserDetails } from '@atonix/shared/api';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { throwError } from 'rxjs';
import { AuthFacade } from '@atonix/shared/state/auth';
import { UsersTabService } from '../../service/users-tab.service';

@Component({
  selector: 'atx-user-admin-create-new-user-dialog',
  templateUrl: './create-new-user-dialog.component.html',
  styleUrls: ['./create-new-user-dialog.component.scss'],
})
export class CreateNewUserDialogComponent {
  public newUserForm: UntypedFormGroup = new UntypedFormGroup({
    email: new UntypedFormControl(null),
    lastName: new UntypedFormControl(null),
    firstName: new UntypedFormControl(null),
    serviceAccount: new UntypedFormControl(false),
  });
  public validEmail = true;

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CreateNewUserDialogComponent>,
    private userAdminCoreService: UserAdminCoreService,
    public authFacade: AuthFacade,
    private usersTabService: UsersTabService,
    @Inject(MAT_DIALOG_DATA) private customerId: string
  ) {}

  cancelDialog(): void {
    this.dialogRef.close();
  }

  save(): void {
    const emailAddress: string = this.newUserForm.get('email').value;
    this.validEmail = this.usersTabService.validateEmail(emailAddress);
    if (this.validEmail) {
      this.userAdminCoreService
        .createUser(
          this.newUserForm.get('firstName').value,
          this.newUserForm.get('lastName').value,
          emailAddress,
          this.newUserForm.get('serviceAccount').value,
          true,
          this.customerId
        )
        .pipe(
          take(1),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((err) => {
            let message: string = null;
            if (err?.error?.Results?.length > 0) {
              if (err?.error?.Results[0]?.Message) {
                message = err.error.Results[0].Message;
              }
            }
            this.dialogRef.close({ newUser: null, error: message });
            return throwError(message);
          })
        )
        .subscribe((newUser: UserDetails) => {
          this.dialogRef.close({ newUser });
          this.newUserForm.reset();
        });
    }
  }
}
