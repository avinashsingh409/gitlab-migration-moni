import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-user-admin-unsaved-changes-dialog',
  templateUrl: './unsaved-changes-dialog.component.html',
  styleUrls: ['./unsaved-changes-dialog.component.scss'],
})
export class UnsavedChangesDialogComponent {
  constructor(public dialogRef: MatDialogRef<UnsavedChangesDialogComponent>) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  onContinue(): void {
    this.dialogRef.close(true);
  }
}
