import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { isNil } from '@atonix/atx-core';
import { GroupDetails, User } from '@atonix/shared/api';
import { RolePermission } from '../../model/roles';
import { GroupDetailsTabsFormService } from '../../service/group-details-tabs-form.service';

@Component({
  selector: 'atx-user-admin-group-details-view-tabs',
  templateUrl: './group-details-tabs.component.html',
  styleUrls: ['./group-details-tabs.component.scss'],
})
export class GroupDetailsTabsComponent implements OnChanges {
  @Input() selectedGroup?: GroupDetails;
  @Input() isLoading?: boolean;
  @Input() rolePermissions: RolePermission[];
  @Input() isMembersSubTabDirty: boolean;
  @Input() isRolesSubTabDirty: boolean;
  @Input() nameIsDirty: boolean;
  @Input() activeIsDirty: boolean;
  @Input() users: User[];
  @Input() filteredMembers: User[];
  @Output() initiatePasswordReset = new EventEmitter<string>();

  public groupDetailsTabsForm: UntypedFormGroup;
  public selectedIndex = 0;

  constructor(
    private groupDetailsTabsFormService: GroupDetailsTabsFormService
  ) {
    this.groupDetailsTabsForm = new UntypedFormGroup({
      groupName: this.groupDetailsTabsFormService.groupNameControl('', true),
      isActive: this.groupDetailsTabsFormService.isActiveControl(null),
    });
  }

  selectTab(tab: any): void {
    if (!isNil(tab?.tabIndex)) {
      this.selectedIndex = tab.tabIndex;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes) && !isNil(changes.selectedGroup)) {
      if (!isNil(this.selectedGroup)) {
        this.groupDetailsTabsForm = new UntypedFormGroup({
          groupName: this.groupDetailsTabsFormService.groupNameControl(
            this.selectedGroup.Name,
            false
          ),
          isActive: this.groupDetailsTabsFormService.isActiveControl(
            this.selectedGroup.Active
          ),
        });
      } else {
        this.groupDetailsTabsForm = new UntypedFormGroup({
          groupName: this.groupDetailsTabsFormService.groupNameControl(
            '',
            true
          ),
          isActive: this.groupDetailsTabsFormService.isActiveControl(null),
        });
      }
    }
  }
}
