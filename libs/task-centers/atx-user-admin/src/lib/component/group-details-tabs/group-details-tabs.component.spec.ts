import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { GroupDetailsTabsComponent } from './group-details-tabs.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('GroupDetailsTabsComponent', () => {
  let component: GroupDetailsTabsComponent;
  let fixture: ComponentFixture<GroupDetailsTabsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [GroupDetailsTabsComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupDetailsTabsComponent);
    component = fixture.componentInstance;
    component.selectedGroup = {
      Name: 'Group XYZ',
      RoleIds: [1],
      Roles: null,
      UserIds: ['1'],
      Users: null,
      CustomerName: 'Customer A',
      Id: '',
      CustomerId: '123',
      Active: true,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
