import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ToastService } from '@atonix/shared/utils';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { GroupsTabComponent } from './groups-tab.component';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavFacade } from '@atonix/atx-navigation';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

describe('GroupsTabComponent', () => {
  let component: GroupsTabComponent;
  let fixture: ComponentFixture<GroupsTabComponent>;
  let mockToastService: ToastService;
  let mockNavFacade: NavFacade;
  beforeEach(waitForAsync(() => {
    mockNavFacade = createMock(NavFacade);
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [
        AgGridModule.forRoot(),
        ReactiveFormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [GroupsTabComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: NavFacade, useValue: mockNavFacade },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ToastService, useValue: mockToastService },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
