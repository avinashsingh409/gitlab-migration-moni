import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ToastService } from '@atonix/shared/utils';
import { User, CustomerNode, Group, GroupDetails } from '@atonix/shared/api';
import {
  UserAdminBusEventTypes,
  UserAdminEventBusService,
} from '../../service/user-admin-event-bus.service';
import { UserAdminRxjsFacade } from '../../service/user-admin.rxjs.facade';
import { ComponentCanDeactivate } from '../../model/component-can-deactivate';
import { UnsavedChangesDialogComponent } from '../unsaved-changes-dialog/unsaved-changes-dialog.component';
import { isNil } from '@atonix/atx-core';
import {
  GroupsTabFacade,
  GroupsTabServiceState,
} from '../../service/groups-tab.service.facade';
import { CreateNewGroupDialogComponent } from '../create-new-group-dialog/create-new-group-dialog.component';

@Component({
  selector: 'atx-groups-tab',
  templateUrl: './groups-tab.component.html',
  styleUrls: ['./groups-tab.component.scss'],
})
export class GroupsTabComponent
  implements OnInit, OnDestroy, ComponentCanDeactivate
{
  public vm$ = new Observable<GroupsTabServiceState>();
  private customerId: string;
  private onDestroy$ = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private groupsTabFacade: GroupsTabFacade,
    private snackBarService: ToastService,
    public userAdminRxjsFacade: UserAdminRxjsFacade,
    userAdminEventBus: UserAdminEventBusService
  ) {
    // emitted by GroupsListComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.CLICK_GROUP_LIST_ROW,
      this.groupChange.bind(this),
      this.onDestroy$
    );

    // emitted by GroupsListComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.SET_GROUP_FILTERS,
      this.setFilters.bind(this),
      this.onDestroy$
    );

    // emitted by GroupsListComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_GROUP_FILTER,
      this.removeFilter.bind(this),
      this.onDestroy$
    );

    // emitted by GroupMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_GROUP_MEMBERS,
      this.addMembers.bind(this),
      this.onDestroy$
    );

    // emitted by GroupMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_GROUP_MEMBER,
      this.removeMember.bind(this),
      this.onDestroy$
    );

    // emitted by GroupMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_ALL_GROUP_MEMBERS,
      this.removeAllMembers.bind(this),
      this.onDestroy$
    );

    // emitted by GroupMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_GROUP_FILTERED_MEMBERS,
      this.addGroupFilteredMembers.bind(this),
      this.onDestroy$
    );
  }

  ngOnInit(): void {
    this.vm$ = this.groupsTabFacade.query.vm$;
    this.userAdminRxjsFacade.setIsDirty(false);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((customer: CustomerNode) => {
        if (!isNil(customer)) {
          if (!isNil(this.customerId)) {
            this.groupsTabFacade.destroyCalls();
          }
          this.customerId = customer.Id;
          this.groupsTabFacade.setLoading(true);
          this.groupsTabFacade.getGroupsRolesAndUsers(this.customerId);
        }
      });
    this.groupsTabFacade.setGroupsSubTabsDirty(false);
  }

  private groupChange({
    group,
    callbackFn,
  }: {
    group: Group;
    callbackFn: (value: boolean) => void;
  }): void {
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        if (isDirty === true) {
          this.openUnsavedChangesDialog(group, callbackFn);
        } else {
          callbackFn(true);
          this.updateSelectedGroup(group);
        }
      });
  }

  private setFilters(filters: any[]): void {
    this.groupsTabFacade.setFilters(filters);
  }

  private removeFilter({
    filter,
    callbackFn,
  }: {
    filter: any;
    callbackFn: (filterName: string) => void;
  }): void {
    this.groupsTabFacade.removeFilter(filter, callbackFn);
  }

  private updateSelectedGroup(group: Group): void {
    this.groupsTabFacade.setLoading(true);
    this.userAdminRxjsFacade.setIsDirty(false);
    this.groupsTabFacade.setGroupsSubTabsDirty(false);
    this.groupsTabFacade.updateSelectedGroup(group);
  }

  private addMembers(members: User[]): void {
    if (!isNil(members)) {
      this.groupsTabFacade.addMembersToGroup(members);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.groupsTabFacade.setIsMembersSubTabDirty(true);
    }
  }

  private removeMember(member: User): void {
    if (!isNil(member)) {
      this.groupsTabFacade.removeMemberFromGroup(member);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.groupsTabFacade.setIsMembersSubTabDirty(true);
    }
  }

  private removeAllMembers(): void {
    this.groupsTabFacade.removeAllMembersFromGroup();
    this.userAdminRxjsFacade.setIsDirty(true);
    this.groupsTabFacade.setIsMembersSubTabDirty(true);
  }

  private addGroupFilteredMembers(members: User[]): void {
    this.groupsTabFacade.addGroupFilteredMembers(members);
  }

  openCreateNewGroupDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.customerId;

    const dialogRef = this.dialog.open(
      CreateNewGroupDialogComponent,
      dialogConfig
    );
    dialogRef
      .afterClosed()
      .subscribe(
        ({ newGroup, error }: { newGroup?: GroupDetails; error?: string }) => {
          if (!isNil(newGroup)) {
            this.snackBarService.openSnackBar(
              `Created new group: ${newGroup.Name}`,
              'success'
            );
            this.groupsTabFacade.addNewGroup(newGroup);
          } else if (!isNil(error)) {
            this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
          }
        }
      );
  }

  canDeactivate(): boolean | Observable<boolean> {
    let value: boolean = null;
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        value = isDirty;
      });

    return value;
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.canDeactivate()) {
      $event.returnValue =
        'You have unsaved changes. Are you sure you want to continue? This action cannot be undone.';
    }
  }

  openUnsavedChangesDialog(group: Group, callbackFn: (value: boolean) => void) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    const dialogRef = this.dialog.open(
      UnsavedChangesDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        callbackFn(true);
        this.updateSelectedGroup(group);
      } else {
        callbackFn(false);
      }
    });
  }

  ngOnDestroy() {
    this.groupsTabFacade.destroyCalls();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
