import {
  Component,
  OnDestroy,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Input,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { AllResourcePermissions } from '../../model/roles';
import { ResourceAccessTypes } from '../../model/resource-access-type-enum';
import { RoleDetailsTabsFormService } from '../../service/role-details-tabs-form.service';
import { isNil } from '@atonix/atx-core';
import { RoleDetails } from '@atonix/shared/api';

@Component({
  selector: 'atx-user-admin-role-app-perms',
  templateUrl: './role-app-perms.component.html',
  styleUrls: ['./role-app-perms.component.scss'],
})
export class RoleAppPermissionsComponent implements OnChanges, OnDestroy {
  @Input() isLoading?: boolean;
  @Input() selectedRole: RoleDetails;
  @Input() allResourcePermissions: AllResourcePermissions[];
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  public dataSource = new MatTableDataSource<AllResourcePermissions>();
  public displayedColumns: string[] = [];
  private onDestroy$ = new Subject<void>();

  constructor(private roleDetailsTabsFormService: RoleDetailsTabsFormService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (
      !isNil(changes) &&
      !isNil(changes.allResourcePermissions) &&
      !isNil(this.selectedRole)
    ) {
      this.dataSource.data = [...this.allResourcePermissions];
      this.dataSource.sortingDataAccessor = (data, header) => {
        switch (header) {
          case 'View': {
            return data.ViewPermission;
          }
          case 'Edit': {
            return data.EditPermission;
          }
          case 'Add': {
            return data.AddPermission;
          }
          case 'Delete': {
            return data.DeletePermission;
          }
          case 'Admin': {
            return data.AdminPermission;
          }
          case 'Name': {
            return data.Resource.Name;
          }
          default: {
            return data[header];
          }
        }
      };
      this.filterUnusedResources();
    }
  }

  filterUnusedResources() {
    this.displayedColumns = [];

    if (
      !this.dataSource.data.every((d: AllResourcePermissions) =>
        isNil(d.ViewPermission)
      )
    ) {
      this.displayedColumns.push('View');
    }
    if (
      !this.dataSource.data.every((d: AllResourcePermissions) =>
        isNil(d.EditPermission)
      )
    ) {
      this.displayedColumns.push('Edit');
    }
    if (
      !this.dataSource.data.every((d: AllResourcePermissions) =>
        isNil(d.AddPermission)
      )
    ) {
      this.displayedColumns.push('Add');
    }
    if (
      !this.dataSource.data.every((d: AllResourcePermissions) =>
        isNil(d.DeletePermission)
      )
    ) {
      this.displayedColumns.push('Delete');
    }
    if (
      !this.dataSource.data.every((d: AllResourcePermissions) =>
        isNil(d.AdminPermission)
      )
    ) {
      this.displayedColumns.push('Admin');
    }

    if (this.displayedColumns.length > 0) {
      this.displayedColumns.push('Name');
    } else {
      this.displayedColumns = [
        'View',
        'Edit',
        'Add',
        'Delete',
        'Admin',
        'Name',
      ];
      this.dataSource.data = [];
    }
  }

  appViewPermsFormControl(appPerm: AllResourcePermissions): UntypedFormControl {
    return this.appPermsFormControl(ResourceAccessTypes.View, appPerm);
  }

  appEditPermsFormControl(appPerm: AllResourcePermissions): UntypedFormControl {
    return this.appPermsFormControl(ResourceAccessTypes.Edit, appPerm);
  }

  appAddPermsFormControl(appPerm: AllResourcePermissions): UntypedFormControl {
    return this.appPermsFormControl(ResourceAccessTypes.Add, appPerm);
  }

  appDeletePermsFormControl(
    appPerm: AllResourcePermissions
  ): UntypedFormControl {
    return this.appPermsFormControl(ResourceAccessTypes.Delete, appPerm);
  }

  appAdminPermsFormControl(
    appPerm: AllResourcePermissions
  ): UntypedFormControl {
    return this.appPermsFormControl(ResourceAccessTypes.BVEdit, appPerm);
  }

  private appPermsFormControl(
    accessTypeId: number,
    appPerm: AllResourcePermissions
  ): UntypedFormControl {
    return this.roleDetailsTabsFormService.roleAppPermsFormControl(
      accessTypeId,
      appPerm
    );
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
