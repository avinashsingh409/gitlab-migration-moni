import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { NavFacade } from '@atonix/atx-navigation';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { GroupsListComponent } from './groups-list.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AgGridModule } from '@ag-grid-community/angular';
import {
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { GridApi } from '@ag-grid-community/core';
import { BehaviorSubject } from 'rxjs';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('GroupsListComponent', () => {
  let component: GroupsListComponent;
  let fixture: ComponentFixture<GroupsListComponent>;
  let userAdmin: UserAdminEventBusService;
  let mockNavFacade: NavFacade;

  beforeEach(async () => {
    mockNavFacade = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      configureLayoutButton: jest.fn(),
      toggleNavigation: jest.fn(),
      theme$: new BehaviorSubject<string>('light'),
      breadcrumb$: new BehaviorSubject<any>(null),
    });
    userAdmin = createMockWithValues(UserAdminEventBusService, {
      emit: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        AgGridModule.forRoot(),
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [GroupsListComponent],
      providers: [
        provideMock(GridApi),
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: NavFacade, useValue: mockNavFacade },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: UserAdminEventBusService, useValue: userAdmin },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(async () => {
    fixture = TestBed.createComponent(GroupsListComponent);
    component = fixture.componentInstance;
    component.groups = [];
    component.filters = [];
    component.gridOptions = {};
    component.isLoading = true;
  });

  it('should create', async () => {
    expect(component).toBeTruthy();
  });
});
