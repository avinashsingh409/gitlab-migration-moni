import {
  Component,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges,
  AfterViewInit,
  ElementRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import {
  ClientSideRowModelModule,
  ColDef,
  ColumnApi,
  EnterpriseCoreModule,
  FilterChangedEvent,
  GridApi,
  GridOptions,
  GridReadyEvent,
  Module,
  RowNode,
  SelectionChangedEvent,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import { Group, GroupDetails } from '@atonix/shared/api';
import {
  UserAdminEventBusService,
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
  UserAdminStateChange,
} from '../../service/user-admin-event-bus.service';
import { NavFacade } from '@atonix/atx-navigation';
import { isNil } from '@atonix/atx-core';
import { ActiveFormatterComponent } from '../active-formatter/active-formatter.component';

@Component({
  selector: 'atx-groups-list',
  templateUrl: './groups-list.component.html',
  styleUrls: ['./groups-list.component.scss'],
})
export class GroupsListComponent
  implements AfterViewInit, OnChanges, OnDestroy
{
  @Input() groups: Group[];
  @Input() isLoading?: boolean;
  @Input() filters: any[];
  @Input() selectedGroup: GroupDetails;

  public columnApi: ColumnApi;
  public gridApi: GridApi;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ClientSideRowModelModule,
    SetFilterModule,
  ];
  public gridOptions: GridOptions = {
    defaultColDef: {
      filter: true,
      floatingFilter: true,
      sortable: true,
      resizable: false,
    },
    columnDefs: [
      {
        colId: 'Active',
        headerName: 'Active',
        headerTooltip: 'Active',
        field: 'Active',
        cellRenderer: 'activeRenderer',
        filter: 'agSetColumnFilter',
        hide: false,
        width: 55,
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.Active ?? false);
        },
      },
      {
        colId: 'GroupName',
        headerName: 'Group',
        field: 'Name',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
      },
    ],
    components: {
      activeRenderer: ActiveFormatterComponent,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.initializeGrid();
    },
    floatingFiltersHeight: 30,
    debug: false,
    rowSelection: 'single',
    suppressRowDeselection: true,
    getRowId: (params) => `${params.data.Id}`,
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
  };
  private previousRowNodeId: string = null;
  private unsubscribe$ = new Subject<void>();

  constructor(
    public navFacade: NavFacade,
    private userAdminEventBus: UserAdminEventBusService,
    private el: ElementRef
  ) {}

  ngAfterViewInit(): void {
    this.el.nativeElement
      .querySelectorAll('.ag-text-field-input')
      ?.forEach((element: HTMLInputElement) => {
        element.autocomplete = 'off';
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(this.gridApi) && !isNil(changes)) {
      if (!isNil(changes.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(changes.groups)) {
        this.gridApi.setRowData(this.groups);
        if (
          !isNil(this.selectedGroup) &&
          this.gridApi.getSelectedNodes().length === 0
        ) {
          this.gridApi
            .getRowNode(`${this.selectedGroup.Id}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  selectionChanged(event: SelectionChangedEvent): void {
    const node: RowNode = event.api.getSelectedNodes()[0];
    // responded to by GroupsTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.CLICK_GROUP_LIST_ROW,
      value: {
        group: node.data as Group,
        callbackFn: (value: boolean): void => {
          if (value === false) {
            event.api.getRowNode(node.id).setSelected(false, false, true);
            event.api
              .getRowNode(this.previousRowNodeId)
              .setSelected(true, true, true);
            event.api.clearFocusedCell();
          } else {
            this.previousRowNodeId = node.id;
          }
        },
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];

    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef.headerName,
      });
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_GROUP_FILTERS,
      value: filters,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  removeFilter(filter: any): void {
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_GROUP_FILTER,
      value: {
        filter,
        callbackFn: this.postRemoveFilterCallbackFn.bind(this),
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName).setModel(null);
    this.refreshGrid();
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key).setModel(null);
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_GROUP_FILTERS,
      value: [],
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
    this.refreshGrid();
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api.onFilterChanged();
    }
  }

  trackByFn = (_: number, item: any): string => item?.Name;

  initializeGrid(): void {
    if (!isNil(this.columnApi) && !isNil(this.gridApi)) {
      this.gridApi.sizeColumnsToFit();
      this.gridApi.setFilterModel({
        Active: {
          values: ['true'],
          filterType: 'set',
        },
      });

      if (!isNil(this.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else if (this.isLoading === false) {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(this.groups)) {
        this.gridApi.setRowData(this.groups);
        if (
          !isNil(this.selectedGroup) &&
          this.gridApi.getSelectedNodes().length === 0
        ) {
          this.gridApi
            .getRowNode(`${this.selectedGroup.Id}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  ngOnDestroy(): void {
    if (!isNil(this.gridApi)) {
      this.gridApi.destroy();
      this.gridApi = null;
      this.columnApi = null;
    }

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
