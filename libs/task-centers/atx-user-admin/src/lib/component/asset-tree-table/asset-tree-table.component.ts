import {
  Component,
  EventEmitter,
  Output,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Subject } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { AssetEmitNode } from '../../model/asset-emit-node';
import { MatCheckbox } from '@angular/material/checkbox';
import { AssetTreeTableServiceFacade } from '../../service/asset-tree-table.service.facade';
import { AssetTreeTableDataSource } from '../../service/asset-tree-table.service.datasource';
import {
  AssetNode,
  AssetPermission,
  Customer,
  RoleDetails,
} from '@atonix/shared/api';
import { UntypedFormControl } from '@angular/forms';
import { RoleDetailsTabsFormService } from '../../service/role-details-tabs-form.service';
import { isNil } from '@atonix/atx-core';
import {
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
  UserAdminEventBusService,
  UserAdminStateChange,
} from '../../service/user-admin-event-bus.service';

@Component({
  selector: 'atx-asset-tree-table',
  templateUrl: './asset-tree-table.component.html',
  styleUrls: ['./asset-tree-table.component.scss'],
})
export class AssetTreeTableComponent implements OnChanges, OnDestroy {
  @Input() selectedRole: RoleDetails;
  @Input() selectedCustomer: Customer;
  @Input() assets: AssetNode[];
  @Input() assetPermissions: AssetPermission[];
  @Output() stateChange = new EventEmitter<AssetEmitNode[]>();

  public displayedColumns: string[] = ['assets', 'allow', 'deny'];
  public treeControl: FlatTreeControl<AssetNode>;
  public dataSource: AssetTreeTableDataSource;
  public isReadOnly: boolean;
  private onDestroy$ = new Subject<void>();

  constructor(
    private assetTreeTableServiceFacade: AssetTreeTableServiceFacade,
    private userAdminEventBus: UserAdminEventBusService,
    private roleDetailsTabsFormService: RoleDetailsTabsFormService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (
        !isNil(changes.assets) &&
        !isNil(this.selectedRole) &&
        !isNil(this.selectedCustomer) &&
        this.assets.length > 0
      ) {
        this.isReadOnly = this.selectedRole.Ownership === 'Shared';
        this.assetTreeTableServiceFacade.setAssetNodes(this.assets);
        this.treeControl = new FlatTreeControl<AssetNode>(
          (node: AssetNode) => node.Level,
          (node: AssetNode) => !isNil(node.Children)
        );
        this.dataSource = new AssetTreeTableDataSource(
          this.treeControl,
          this.assetTreeTableServiceFacade,
          this.selectedRole,
          this.selectedCustomer.Id
        );
      }
      if (!isNil(changes.assetPermissions)) {
        this.assetTreeTableServiceFacade.setEmitNodes(this.assetPermissions);
      }
    }
  }

  ascendAllow = (data: AssetNode): boolean =>
    data.AscendAllow === true &&
    data.Allow === false &&
    data.ImplicitAllow === false;

  checkedAllow = (data: AssetNode): boolean =>
    data.Allow === true || data.ImplicitAllow === true;

  assetPermAllowFormControl(
    data: AssetNode,
    checkbox: MatCheckbox
  ): UntypedFormControl {
    return this.roleDetailsTabsFormService.assetAllowPermFormControl(
      data,
      this.isReadOnly,
      checkbox,
      this.checkedAllow(data),
      this.stateChange,
      this.assetTreeTableServiceFacade
    );
  }

  assetPermDenyFormControl(
    data: AssetNode,
    checkbox: MatCheckbox
  ): UntypedFormControl {
    return this.roleDetailsTabsFormService.assetDenyPermFormControl(
      data,
      this.isReadOnly,
      checkbox,
      this.checkedDeny(data),
      this.stateChange,
      this.assetTreeTableServiceFacade
    );
  }

  updateParentNodeAllow(data: AssetNode): void {
    this.assetTreeTableServiceFacade.updateParentNodeAllow(data);
  }

  updateChildrenAllow(
    node: AssetNode,
    implicitAllow: boolean,
    implicitDeny: boolean
  ): void {
    this.assetTreeTableServiceFacade.updateChildrenAllow(
      node,
      implicitAllow,
      implicitDeny
    );
  }

  ascendDeny = (data: AssetNode): boolean =>
    data.AscendDeny === true &&
    data.Deny === false &&
    data.ImplicitDeny === false;

  checkedDeny = (data: AssetNode): boolean =>
    data.Deny === true || data.ImplicitDeny === true;

  updateParentNodeDeny(data: AssetNode): void {
    this.assetTreeTableServiceFacade.updateParentNodeDeny(data);
  }

  updateChildrenDeny(
    node: AssetNode,
    implicitDeny: boolean,
    implicitAllow: boolean
  ): void {
    this.assetTreeTableServiceFacade.updateChildrenDeny(
      node,
      implicitDeny,
      implicitAllow
    );
  }

  getColorAllow = (data: AssetNode): string =>
    data.Allow === false &&
    (data.AscendAllow === true || data.ImplicitAllow === true)
      ? 'primary'
      : 'accent';

  getColorDeny = (data: AssetNode): string =>
    data.Deny === false &&
    (data.AscendDeny === true || data.ImplicitDeny === true)
      ? 'primary'
      : 'accent';

  ngOnDestroy(): void {
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_ASSET_NODES,
      value: this.assetTreeTableServiceFacade.query.getAssetNodes(),
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
