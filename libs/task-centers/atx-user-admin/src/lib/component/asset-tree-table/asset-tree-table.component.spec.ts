import { StoreModule } from '@ngrx/store';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AssetTreeTableComponent } from './asset-tree-table.component';
import { AssetTreeTableServiceFacade } from '../../service/asset-tree-table.service.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { AssetTreeTableServiceQuery } from '../../service/asset-tree-table.service.query';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';

describe('AssetTreeTableComponent', () => {
  let component: AssetTreeTableComponent;
  let fixture: ComponentFixture<AssetTreeTableComponent>;
  let mockAssetTreeTableServiceFacade: AssetTreeTableServiceFacade;
  let mockAssetTreeTableServiceQuery: AssetTreeTableServiceQuery;

  beforeEach(() => {
    mockAssetTreeTableServiceQuery = createMockWithValues(
      AssetTreeTableServiceQuery,
      {
        assetNodes$: new BehaviorSubject<any>(null),
        vm$: new BehaviorSubject<any>(null),
        getAssetNodes: jest.fn(),
      }
    );
    mockAssetTreeTableServiceFacade = createMockWithValues(
      AssetTreeTableServiceFacade,
      {
        toggleNode: jest.fn(),
        tickedAllow: jest.fn(),
        updateParentNodeAllow: jest.fn(),
        updateChildrenAllow: jest.fn(),
        tickedDeny: jest.fn(),
        updateParentNodeDeny: jest.fn(),
        updateChildrenDeny: jest.fn(),
        query: mockAssetTreeTableServiceQuery,
      }
    );

    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot([]),
        AtxMaterialModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
      ],
      declarations: [AssetTreeTableComponent],
      providers: [
        {
          provide: AssetTreeTableServiceFacade,
          useValue: mockAssetTreeTableServiceFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: APP_CONFIG,
          useValue: AppConfig,
        },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTreeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
