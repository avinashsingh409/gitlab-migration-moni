import { Component } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-active-formatter',
  template: '<mat-icon style="color: #5e9a40;padding:6px">{{value}}</mat-icon>',
})
export class ActiveFormatterComponent implements ICellRendererAngularComp {
  value: 'check_circle' | 'circle';

  agInit(params: any) {
    this.value = params?.data?.Active ? 'check_circle' : 'circle';
  }

  refresh(params: any): boolean {
    this.value = params?.data?.Active ? 'check_circle' : 'circle';
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(_?: IAfterGuiAttachedParams): void {}
}
