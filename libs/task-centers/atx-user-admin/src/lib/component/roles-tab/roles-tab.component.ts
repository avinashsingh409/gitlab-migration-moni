import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ToastService } from '@atonix/shared/utils';
import { CreateNewRoleDialogComponent } from '../create-new-role-dialog/create-new-role-dialog.component';
import {
  Role,
  RoleDetails,
  User,
  AssetNode,
  CustomerNode,
  Customer,
} from '@atonix/shared/api';
import {
  UserAdminBusEventTypes,
  UserAdminEventBusService,
} from '../../service/user-admin-event-bus.service';
import {
  RolesTabFacade,
  RolesTabFacadeState,
} from '../../service/roles-tab.facade';
import { UserAdminRxjsFacade } from '../../service/user-admin.rxjs.facade';
import { ComponentCanDeactivate } from '../../model/component-can-deactivate';
import { UnsavedChangesDialogComponent } from '../unsaved-changes-dialog/unsaved-changes-dialog.component';
import { AssetEmitNode } from '../../model/asset-emit-node';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-roles-tab',
  templateUrl: './roles-tab.component.html',
  styleUrls: ['./roles-tab.component.scss'],
})
export class RolesTabComponent
  implements OnInit, OnDestroy, ComponentCanDeactivate
{
  public vm$: Observable<RolesTabFacadeState>;
  private customerId: string;
  private onDestroy$ = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private rolesTabFacade: RolesTabFacade,
    private snackBarService: ToastService,
    public userAdminRxjsFacade: UserAdminRxjsFacade,
    userAdminEventBus: UserAdminEventBusService
  ) {
    // emitted by RolesListComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.CLICK_ROLE_LIST_ROW,
      this.roleChange.bind(this),
      this.onDestroy$
    );

    // emitted by RoleMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_ROLE_MEMBERS,
      this.addMembers.bind(this),
      this.onDestroy$
    );

    // emitted by RoleMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_ROLE_MEMBER,
      this.removeMember.bind(this),
      this.onDestroy$
    );

    // emitted by RoleMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_ALL_ROLE_MEMBERS,
      this.removeAllMembers.bind(this),
      this.onDestroy$
    );

    // emitted by RoleMembershipComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_ROLE_FILTERED_MEMBERS,
      this.addRoleFilteredMembers.bind(this),
      this.onDestroy$
    );

    // emitted by AssetTreeTableComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.SET_ASSET_NODES,
      this.setAssetNodes.bind(this),
      this.onDestroy$
    );

    // emitted by RoleSharingComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_ROLE_CUSTOMERS,
      this.addCustomers.bind(this),
      this.onDestroy$
    );

    // emitted by RoleSharingComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_ROLE_CUSTOMER,
      this.removeCustomer.bind(this),
      this.onDestroy$
    );

    // emitted by RoleSharingComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_ALL_ROLE_CUSTOMERS,
      this.removeAllCustomers.bind(this),
      this.onDestroy$
    );

    // emitted by RoleSharingComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.ADD_ROLE_FILTERED_CUSTOMERS,
      this.addRoleFilteredCustomers.bind(this),
      this.onDestroy$
    );

    this.vm$ = this.rolesTabFacade.query.vm$;
  }

  ngOnInit(): void {
    this.userAdminRxjsFacade.setIsDirty(false);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((customer: CustomerNode) => {
        if (!isNil(customer)) {
          if (!isNil(this.customerId)) {
            this.rolesTabFacade.destroyCalls();
            this.userAdminRxjsFacade.destroyCalls();
          }
          this.customerId = customer.Id;
          this.rolesTabFacade.setLoading(true);
          this.rolesTabFacade.initializeRoleTabs(this.customerId);
        }
      });
    this.rolesTabFacade.setRolesSubTabsDirty(false);
  }

  private roleChange({
    role,
    callbackFn,
  }: {
    role: Role;
    callbackFn: (value: boolean) => void;
  }): void {
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        if (isDirty === true) {
          this.openUnsavedChangesDialog(role, callbackFn);
        } else {
          callbackFn(true);
          this.updateSelectedRole(role);
        }
      });
  }

  private updateSelectedRole(role: Role): void {
    this.rolesTabFacade.setLoading(true);
    this.userAdminRxjsFacade.setIsDirty(false);
    this.rolesTabFacade.setRolesSubTabsDirty(false);
    this.rolesTabFacade.updateSelectedRole(role, this.customerId);
  }

  private addMembers(members: User[]): void {
    if (!isNil(members)) {
      this.rolesTabFacade.addMembersToRole(members);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.rolesTabFacade.setMembershipIsDirty(true);
    }
  }

  private removeMember(member: User): void {
    if (!isNil(member)) {
      this.rolesTabFacade.removeMemberFromRole(member);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.rolesTabFacade.setMembershipIsDirty(true);
    }
  }

  private removeAllMembers(): void {
    this.rolesTabFacade.removeAllMembersFromRole();
    this.userAdminRxjsFacade.setIsDirty(true);
    this.rolesTabFacade.setMembershipIsDirty(true);
  }

  private setAssetNodes(nodes: AssetNode[]): void {
    this.rolesTabFacade.setAssets(nodes);
  }

  private addRoleFilteredMembers(members: User[]): void {
    this.rolesTabFacade.addRoleFilteredMembers(members);
  }

  openCreateNewRoleDialog(): void {
    // open dialog in here similar to what manage issue follower dialog does on onNoClick
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.customerId;

    const dialogRef = this.dialog.open(
      CreateNewRoleDialogComponent,
      dialogConfig
    );
    dialogRef
      .afterClosed()
      .subscribe(
        ({ newRole, error }: { newRole?: RoleDetails; error?: string }) => {
          if (!isNil(newRole)) {
            this.snackBarService.openSnackBar(
              `Created new role: ${newRole.Name}`,
              'success'
            );
            this.rolesTabFacade.addNewRole(newRole);
          } else if (!isNil(error)) {
            this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
          }
        }
      );
  }

  canDeactivate(): boolean | Observable<boolean> {
    let value: boolean = null;
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        value = isDirty;
      });

    return value;
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.canDeactivate()) {
      $event.returnValue =
        'You have unsaved changes. Are you sure you want to continue? This action cannot be undone.';
    }
  }

  openUnsavedChangesDialog(role: Role, callbackFn: (value: boolean) => void) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    const dialogRef = this.dialog.open(
      UnsavedChangesDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        callbackFn(true);
        this.updateSelectedRole(role);
      } else {
        callbackFn(false);
      }
    });
  }

  onAssetTreeTableStateChange(assetPerms: AssetEmitNode[]) {
    this.rolesTabFacade.changeRoleAssetPermissions(assetPerms);
    this.userAdminRxjsFacade.setIsDirty(true);
    this.rolesTabFacade.setAssetPermsIsDirty(true);
  }

  private addCustomers(customers: Customer[]): void {
    if (!isNil(customers)) {
      this.rolesTabFacade.addCustomersToRole(customers);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.rolesTabFacade.setSharingIsDirty(true);
    }
  }

  private removeCustomer(customer: Customer): void {
    if (!isNil(customer)) {
      this.rolesTabFacade.removeCustomerFromRole(customer);
      this.userAdminRxjsFacade.setIsDirty(true);
      this.rolesTabFacade.setSharingIsDirty(true);
    }
  }

  private removeAllCustomers(): void {
    this.rolesTabFacade.removeAllCustomersFromRole();
    this.userAdminRxjsFacade.setIsDirty(true);
    this.rolesTabFacade.setSharingIsDirty(true);
  }

  private addRoleFilteredCustomers(customers: Customer[]): void {
    this.rolesTabFacade.addRoleFilteredCustomers(customers);
  }

  ngOnDestroy() {
    this.rolesTabFacade.destroyCalls();
    this.userAdminRxjsFacade.destroyCalls();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
