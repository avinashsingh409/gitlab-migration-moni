import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catchError, take } from 'rxjs/operators';
import { UserAdminCoreService } from '@atonix/shared/api';
import { RoleDetails } from '@atonix/shared/api';
import { throwError } from 'rxjs';

@Component({
  selector: 'atx-user-admin-create-new-role-dialog',
  templateUrl: './create-new-role-dialog.component.html',
  styleUrls: ['./create-new-role-dialog.component.scss'],
})
export class CreateNewRoleDialogComponent {
  @ViewChild('newRoleNameInput') newRoleNameInput: ElementRef<HTMLInputElement>;
  constructor(
    private dialogRef: MatDialogRef<CreateNewRoleDialogComponent>,
    private userAdminCoreService: UserAdminCoreService,
    @Inject(MAT_DIALOG_DATA) private owningCustomerId: string
  ) {}

  cancelDialog(): void {
    this.dialogRef.close();
  }

  save(): void {
    // hard-coded parent role id is acceptable for MVP,
    // will not be acceptable once tenancy design is implemented

    this.userAdminCoreService
      .createRole(
        this.newRoleNameInput.nativeElement.value,
        5,
        this.owningCustomerId
      )
      .pipe(
        take(1),
        // eslint-disable-next-line rxjs/no-implicit-any-catch
        catchError((err: any) => {
          let message: string = null;
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.dialogRef.close({ newRole: null, error: message });
          return throwError(message);
        })
      )
      .subscribe((newRole: RoleDetails) => {
        this.dialogRef.close({ newRole });
      });
  }
}
