/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-nested-subscribe */
import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { produce } from 'immer';
import {
  UserAdminCoreService,
  RoleDetails,
  User,
  UserDetails,
  Role,
  CustomerNode,
  CustomerDetails,
  TopLevelAsset,
  Customer,
  GroupDetails,
  Group,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import {
  UserAdminRxjsFacade,
  UserAdminRxjsFacadeState,
} from '../../service/user-admin.rxjs.facade';
import {
  RolesTabFacade,
  RolesTabFacadeState,
} from '../../service/roles-tab.facade';
import {
  UsersTabFacade,
  UsersTabServiceState,
} from '../../service/users-tab.service.facade';
import { RolePermission } from '../../model/roles';
import { isNil } from '@atonix/atx-core';
import { CustomerTreeFacade } from '../../service/customer-tree.service.facade';
import { FlatTreeControl } from '@angular/cdk/tree';
import { CustomerTreeDataSource } from '../../service/customer-tree.service.datasource';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UnsavedChangesDialogComponent } from '../unsaved-changes-dialog/unsaved-changes-dialog.component';
import { CreateNewCustomerDialogComponent } from '../create-new-customer-dialog/create-new-customer-dialog.component';
import {
  CustomerTabFacade,
  CustomerTabServiceState,
} from '../../service/customer-tab.service.facade';
import { AuthFacade } from '@atonix/shared/state/auth';
import { GroupPermission } from '../../model/users';
import {
  GroupsTabFacade,
  GroupsTabServiceState,
} from '../../service/groups-tab.service.facade';
import { UsersTabService } from '../../service/users-tab.service';
import { ChangeEmailConfirmationDialogComponent } from '../change-email-confirmation-dialog/change-email-confirmation-dialog.component';

@Component({
  selector: 'atx-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAdminComponent implements OnInit, OnDestroy {
  public vm$: Observable<UserAdminRxjsFacadeState>;
  private onDestroy$ = new Subject<void>();
  public treeControl: FlatTreeControl<CustomerNode>;
  public dataSource: CustomerTreeDataSource;

  constructor(
    private dialog: MatDialog,
    private userAdminRxjsFacade: UserAdminRxjsFacade,
    private usersTabFacade: UsersTabFacade,
    private rolesTabFacade: RolesTabFacade,
    private customerTabFacade: CustomerTabFacade,
    private userAdminCoreService: UserAdminCoreService,
    private router: Router,
    private snackBarService: ToastService,
    private customerTreeFacade: CustomerTreeFacade,
    private groupsTabFacade: GroupsTabFacade,
    private usersTabService: UsersTabService,
    public authFacade: AuthFacade
  ) {}

  ngOnInit(): void {
    this.vm$ = this.userAdminRxjsFacade.query.vm$;
    this.userAdminRxjsFacade.getCustomersAndTopLevelAssets();
  }

  public onNodeClick(node: CustomerNode): void {
    this.vm$.pipe(take(1)).subscribe((vm: UserAdminRxjsFacadeState) => {
      if (vm.isDirty === true) {
        this.openUnsavedChangesDialog(node as CustomerNode);
      } else {
        this.userAdminRxjsFacade.updateSelectedCustomer(node as CustomerNode);
      }
    });
  }

  public createNewCustomer(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;

    const dialogRef = this.dialog.open(
      CreateNewCustomerDialogComponent,
      dialogConfig
    );
    dialogRef
      .afterClosed()
      .subscribe(
        ({
          newCustomer,
          error,
        }: {
          newCustomer: CustomerNode;
          error?: string;
        }) => {
          if (!isNil(newCustomer)) {
            this.snackBarService.openSnackBar(
              `Created new customer: ${newCustomer.Name}`,
              'success'
            );
            this.customerTreeFacade.addNewCustomer(newCustomer);
          } else if (!isNil(error)) {
            this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
          }
        }
      );
  }

  private openUnsavedChangesDialog(node: CustomerNode): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    const dialogRef = this.dialog.open(
      UnsavedChangesDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        this.resetDirtyState();
        this.userAdminRxjsFacade.updateSelectedCustomer(node);
      }
    });
  }

  resetDirtyState(): void {
    const url = this.router.url;
    if (url.endsWith('/users')) {
      this.usersTabFacade.setUsersSubTabsDirty(false);
    } else if (url.endsWith('/roles')) {
      this.rolesTabFacade.setRolesSubTabsDirty(false);
    } else if (url.endsWith('/customer')) {
      this.userAdminRxjsFacade.setIsDirty(false, 'customer');
    } else if (url.endsWith('/groups')) {
      this.groupsTabFacade.setGroupsSubTabsDirty(false);
    }

    this.userAdminRxjsFacade.setIsDirty(false);
  }

  undo(): void {
    const url = this.router.url;
    if (url.endsWith('/users')) {
      this.undoUser();
    } else if (url.endsWith('/roles')) {
      this.undoRole();
    } else if (url.endsWith('/customer')) {
      this.undoCustomer();
    } else if (url.endsWith('/groups')) {
      this.undoGroup();
    }
  }

  undoUser(): void {
    this.usersTabFacade.setLoading(true);
    this.usersTabFacade.query.selectedUser$
      .pipe(take(1))
      .subscribe((selectedUser: UserDetails) => {
        this.userAdminRxjsFacade.setIsDirty(false);
        this.usersTabFacade.updateSelectedUser(selectedUser);
        this.usersTabFacade.setUsersSubTabsDirty(false);
      });
  }

  undoRole(): void {
    this.rolesTabFacade.setLoading(true);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(take(1))
      .subscribe((customer: CustomerNode) => {
        this.rolesTabFacade.query.selectedRole$
          .pipe(take(1))
          .subscribe((selectedRole: RoleDetails) => {
            this.userAdminRxjsFacade.setIsDirty(false);
            this.rolesTabFacade.updateSelectedRole(selectedRole, customer.Id);
            this.rolesTabFacade.setRolesSubTabsDirty(false);
          });
      });
  }

  undoCustomer(): void {
    this.customerTabFacade.setLoading(true);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(take(1))
      .subscribe((customer: CustomerNode) => {
        this.userAdminRxjsFacade.updateSelectedCustomer({ ...customer });
        this.userAdminRxjsFacade.setIsDirty(false, 'customer');
      });
  }

  undoGroup(): void {
    this.groupsTabFacade.setLoading(true);
    this.groupsTabFacade.query.selectedGroup$
      .pipe(take(1))
      .subscribe((selectedGroup: GroupDetails) => {
        this.userAdminRxjsFacade.setIsDirty(false);
        this.groupsTabFacade.updateSelectedGroup(selectedGroup);
        this.groupsTabFacade.setGroupsSubTabsDirty(false);
      });
  }

  save(): void {
    const url = this.router.url;
    if (url.endsWith('/users')) {
      this.saveUser();
    } else if (url.endsWith('/roles')) {
      this.saveRole();
    } else if (url.endsWith('/customer')) {
      this.saveCustomer();
    } else if (url.endsWith('/groups')) {
      this.saveGroup();
    }
  }

  saveUser(): void {
    this.usersTabFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: UsersTabServiceState) => {
        if (vm.emailChanged) {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.disableClose = true;
          const dialogRef = this.dialog.open(
            ChangeEmailConfirmationDialogComponent,
            dialogConfig
          );
          dialogRef.afterClosed().subscribe((exit: boolean) => {
            if (exit === true) {
              this.continueSaveUser();
            }
          });
        } else {
          this.continueSaveUser();
        }
      });
  }

  private continueSaveUser() {
    this.usersTabFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: UsersTabServiceState) => {
        if (!isNil(vm.selectedUser)) {
          const userToEdit = produce(
            vm.selectedUser,
            (draftState: UserDetails) => {
              draftState.RoleIds = vm.rolePermissions.reduce(
                (result: number[], role: RolePermission) => {
                  if (role.Allow === true) {
                    result.push(role.Role.Id);
                  }
                  return result;
                },
                []
              );
              draftState.GroupIds = vm.groupPermissions.reduce(
                (result: string[], group: GroupPermission) => {
                  if (group.Allow === true) {
                    result.push(group.Group.Id);
                  }
                  return result;
                },
                []
              );
            }
          );

          if (!this.usersTabService.validateEmail(userToEdit.Email)) {
            this.snackBarService.openSnackBar(
              'Please enter a valid email address',
              'error'
            );
            return;
          }

          this.userAdminCoreService
            .editUser(userToEdit)
            .pipe(
              take(1),
              catchError((err: any) => {
                let message: string = null;
                if (err?.error?.Results?.length > 0) {
                  if (err?.error?.Results[0]?.Message) {
                    message = err.error.Results[0].Message;
                  }
                }
                this.snackBarService.openSnackBar(`Error: ${message}`, 'error');
                return throwError(message);
              })
            )
            .subscribe((savedUser: UserDetails) => {
              if (!isNil(savedUser)) {
                this.snackBarService.openSnackBar(
                  `Successfully updated user: ${savedUser.Email}`,
                  'success'
                );
                this.userAdminRxjsFacade.setIsDirty(false);
                this.usersTabFacade.setUsersSubTabsDirty(false);
                const updatedUsers = produce(vm.users, (draftState: User[]) => {
                  const index: number = draftState.findIndex(
                    (user: User) => user.Email === savedUser.Email
                  );
                  draftState[index] = savedUser;
                });
                this.usersTabFacade.setUsers(updatedUsers);
              }
            });
        }
      });
  }

  saveRole(): void {
    this.rolesTabFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: RolesTabFacadeState) => {
        if (!isNil(vm.selectedRole)) {
          const roleToEdit = produce(
            vm.selectedRole,
            (draftState: RoleDetails) => {
              draftState.UserIds = draftState.Users.map(
                (user: User) => user.Id
              );
              draftState.SharedCustomerIds = draftState.Shares.map(
                (customer: Customer) => customer.Id
              );
            }
          );

          let customerId: string;
          this.userAdminRxjsFacade.query.selectedCustomer$
            .pipe(take(1))
            .subscribe((selectedCustomer: CustomerNode) => {
              customerId = selectedCustomer.Id;
            });

          this.userAdminCoreService
            .editRole(roleToEdit, customerId)
            .pipe(
              take(1),
              catchError((err: any) => {
                let message: string = null;
                if (err?.error?.Results?.length > 0) {
                  if (err?.error?.Results[0]?.Message) {
                    message = err.error.Results[0].Message;
                  }
                }
                this.snackBarService.openSnackBar(`Error: ${message}`, 'error');
                return throwError(message);
              })
            )
            .subscribe((savedRole: RoleDetails) => {
              if (!isNil(savedRole)) {
                this.snackBarService.openSnackBar(
                  `Saved role: ${savedRole.Name}`,
                  'success'
                );
                this.userAdminRxjsFacade.setIsDirty(false);
                this.rolesTabFacade.setRolesSubTabsDirty(false);
                const updatedRoles = produce(vm.roles, (draftState: Role[]) => {
                  const index: number = draftState.findIndex(
                    (role: Role) => role.Id === savedRole.Id
                  );
                  draftState[index] = savedRole;
                });
                this.rolesTabFacade.setRoles(updatedRoles);
              }
            });
        }
      });
  }

  saveCustomer(): void {
    this.customerTabFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: CustomerTabServiceState) => {
        if (!isNil(vm.selectedCustomerDetails)) {
          const customerToEdit = produce(
            vm.selectedCustomerDetails,
            (draftState: CustomerDetails) => {
              draftState.TopLevelAssetIds = draftState.TopLevelAssets.map(
                (asset: TopLevelAsset) => asset.AssetId
              );
            }
          );

          this.userAdminCoreService
            .editCustomer(customerToEdit)
            .pipe(
              take(1),
              catchError((err: any) => {
                let message: string = null;
                if (err?.error?.Results?.length > 0) {
                  if (err?.error?.Results[0]?.Message) {
                    message = err.error.Results[0].Message;
                  }
                }
                this.snackBarService.openSnackBar(`Error: ${message}`, 'error');
                return throwError(message);
              })
            )
            .subscribe((savedCustomer: CustomerDetails) => {
              if (!isNil(savedCustomer)) {
                this.snackBarService.openSnackBar(
                  `Saved customer: ${savedCustomer.Name}`,
                  'success'
                );
                this.customerTreeFacade.setUpdatedName(
                  savedCustomer.Id,
                  savedCustomer.Name
                );
                this.userAdminRxjsFacade.setIsDirty(false, 'customer');
              }
            });
        }
      });
  }

  saveGroup(): void {
    this.groupsTabFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: GroupsTabServiceState) => {
        if (!isNil(vm.selectedGroup)) {
          const groupToEdit = produce(
            vm.selectedGroup,
            (draftState: GroupDetails) => {
              draftState.RoleIds = vm.rolePermissions.reduce(
                (result: number[], role: RolePermission) => {
                  if (role.Allow === true) {
                    result.push(role.Role.Id);
                  }
                  return result;
                },
                []
              );
              draftState.UserIds = vm.selectedGroup.Users.map(
                (user: User) => user.Id
              );
            }
          );

          this.userAdminCoreService
            .editGroup(groupToEdit)
            .pipe(
              take(1),
              catchError((err: any) => {
                let message: string = null;
                if (err?.error?.Results?.length > 0) {
                  if (err?.error?.Results[0]?.Message) {
                    message = err.error.Results[0].Message;
                  }
                }
                this.snackBarService.openSnackBar(`Error: ${message}`, 'error');
                return throwError(message);
              })
            )
            .subscribe((savedGroup: GroupDetails) => {
              if (!isNil(savedGroup)) {
                this.snackBarService.openSnackBar(
                  `Successfully updated group: ${savedGroup.Name}`,
                  'success'
                );
                this.userAdminRxjsFacade.setIsDirty(false);
                this.groupsTabFacade.setGroupsSubTabsDirty(false);
                const updatedGroups = produce(
                  vm.groups,
                  (draftState: Group[]) => {
                    const index: number = draftState.findIndex(
                      (group: Group) => group.Id === savedGroup.Id
                    );
                    draftState[index] = savedGroup;
                  }
                );
                this.groupsTabFacade.setGroups(updatedGroups);
              }
            });
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
