import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserAdminComponent } from './user-admin.component';
import { FormBuilder } from '@angular/forms';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ToastService } from '@atonix/shared/utils';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { MatDialogRef } from '@angular/material/dialog';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  UserAdminRxjsFacade,
  UserAdminRxjsFacadeState,
} from '../../service/user-admin.rxjs.facade';
import {
  UsersTabFacade,
  UsersTabServiceState,
} from '../../service/users-tab.service.facade';
import {
  RolesTabFacade,
  RolesTabFacadeState,
} from '../../service/roles-tab.facade';
import { AuthFacade } from '@atonix/shared/state/auth';
import { CustomerTreeFacade } from '../../service/customer-tree.service.facade';
import {
  CustomerNode,
  RoleDetails,
  UserAdminCoreService,
  UserDetails,
} from '@atonix/shared/api';
import {
  CustomerTabFacade,
  CustomerTabServiceState,
} from '../../service/customer-tab.service.facade';
import { BehaviorSubject } from 'rxjs';
import { UserAdminRxjsQuery } from '../../service/user-admin.rxjs.query';
import { RolesTabQuery } from '../../service/roles-tab.query';
import { UsersTabServiceQuery } from '../../service/users-tab.service.query';
import { CustomerTabServiceQuery } from '../../service/customer-tab.service.query';
import { AgGridModule } from '@ag-grid-community/angular';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('UserAdminComponent', () => {
  let component: UserAdminComponent;
  let fixture: ComponentFixture<UserAdminComponent>;
  let mockToastService: ToastService;
  let mockRolesTabFacade: RolesTabFacade;
  let mockRolesTabFacadeQuery: RolesTabQuery;
  let mockUserAdminRxjsFacade: UserAdminRxjsFacade;
  let mockUserAdminRxjsQuery: UserAdminRxjsQuery;
  let mockUserTabFacade: UsersTabFacade;
  let mockUserTabFacadeQuery: UsersTabServiceQuery;
  let mockCustomerTabFacade: CustomerTabFacade;
  let mockCustomerTabFacadeQuery: CustomerTabServiceQuery;
  const initialState: UserAdminRxjsFacadeState = {
    selectedUser: null,
    selectedNotificationGroup: null,
    tabs: [
      {
        path: 'users',
        label: 'Users',
        isDirty: false,
      },
      {
        path: 'roles',
        label: 'Roles',
        isDirty: false,
      },
      {
        path: 'customer',
        label: 'Customer Information',
        isDirty: false,
      },
    ],
    isDirty: false,
    customers: [],
    selectedCustomer: null,
    customerTopLevelAssets: [],
  };
  const initialRoleTabsState: RolesTabFacadeState = {
    roles: [],
    selectedRole: null,
    filteredMembers: [],
    resources: [],
    allResourcePermissions: [],
    gridFilters: [],
    assets: [],
    isLoading: null,
    nameIsDirty: false,
    appPermsIsDirty: false,
    assetPermsIsDirty: false,
    membershipIsDirty: false,
    sharingIsDirty: false,
    roleCustomers: [],
    filteredCustomers: [],
    users: [],
  };
  const userTabInitialState: UsersTabServiceState = {
    users: [],
    selectedUser: null,
    roles: [],
    rolePermissions: [],
    filters: [],
    isLoading: null,
    isDetailsSubTabDirty: false,
    isRolesSubTabDirty: false,
    isGroupsSubTabDirty: false,
    groups: [],
    groupPermissions: [],
    emailChanged: false,
  };
  const customerTabInitialState: CustomerTabServiceState = {
    isLoading: null,
    selectedCustomerDetails: null,
    selectedTopLevelAsset: null,
    canEditCustomer: null,
  };
  const mockDialogRef = {
    close: jest.fn,
  };
  beforeEach(() => {
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });
    mockCustomerTabFacadeQuery = createMockWithValues(CustomerTabServiceQuery, {
      vm$: new BehaviorSubject<CustomerTabServiceState>(
        customerTabInitialState
      ),
    });
    mockCustomerTabFacade = createMockWithValues(CustomerTabFacade, {
      query: mockCustomerTabFacadeQuery,
    });
    mockUserTabFacadeQuery = createMockWithValues(UsersTabServiceQuery, {
      vm$: new BehaviorSubject<UsersTabServiceState>(userTabInitialState),
      selectedUser$: new BehaviorSubject<UserDetails>(
        userTabInitialState.selectedUser
      ),
      filters$: new BehaviorSubject<any[]>(userTabInitialState.filters),
    });
    mockUserTabFacade = createMockWithValues(UsersTabFacade, {
      query: mockUserTabFacadeQuery,
    });
    mockUserAdminRxjsQuery = createMockWithValues(UserAdminRxjsQuery, {
      vm$: new BehaviorSubject<UserAdminRxjsFacadeState>(initialState),
      selectedCustomer$: new BehaviorSubject<CustomerNode>(
        initialState.selectedCustomer
      ),
    });
    mockUserAdminRxjsFacade = createMockWithValues(UserAdminRxjsFacade, {
      query: mockUserAdminRxjsQuery,
      updateSelectedCustomer: jest.fn(),
      setIsDirty: jest.fn(),
      getCustomersAndTopLevelAssets: jest.fn(),
    });
    mockRolesTabFacadeQuery = createMockWithValues(RolesTabQuery, {
      vm$: new BehaviorSubject<RolesTabFacadeState>(initialRoleTabsState),
      selectedRole$: new BehaviorSubject<RoleDetails>(
        initialRoleTabsState.selectedRole
      ),
    });
    mockRolesTabFacade = createMockWithValues(RolesTabFacade, {
      query: mockRolesTabFacadeQuery,
      setLoading: jest.fn(),
      updateSelectedRole: jest.fn(),
      setRolesSubTabsDirty: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        AgGridModule,
        NoopAnimationsModule,
        RouterTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UserAdminComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: UserAdminRxjsFacade,
          useValue: mockUserAdminRxjsFacade,
        },
        {
          provide: UsersTabFacade,
          useValue: mockUserTabFacade,
        },
        {
          provide: RolesTabFacade,
          useValue: mockRolesTabFacade,
        },
        provideMock(CustomerTreeFacade),
        {
          provide: CustomerTabFacade,
          useValue: mockCustomerTabFacade,
        },
        provideMock(AuthFacade),
        provideMock(UserAdminCoreService),
        RouterTestingModule,
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        FormBuilder,
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ToastService, useValue: mockToastService },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
