import {
  Component,
  Input,
  OnDestroy,
  ElementRef,
  ViewChild,
  OnChanges,
  SimpleChanges,
  COMPILER_OPTIONS,
} from '@angular/core';
import { EmailValidator, UntypedFormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { User, RoleDetails } from '@atonix/shared/api';
import {
  UserAdminEventBusService,
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
  UserAdminStateChange,
} from '../../service/user-admin-event-bus.service';
import { isNil } from '@atonix/atx-core';
import { RoleDetailsTabsFormService } from '../../service/role-details-tabs-form.service';
import { ToastService } from '@atonix/shared/utils';

@Component({
  selector: 'atx-user-admin-role-members',
  templateUrl: './role-members.component.html',
  styleUrls: ['./role-members.component.scss'],
})
export class RoleMembershipComponent implements OnChanges, OnDestroy {
  @Input() isLoading?: boolean;
  @Input() availableMembers: User[];
  @Input() selectedRole: RoleDetails;
  @Input() filteredMembers: User[];
  @ViewChild('memberSelectionInput')
  memberSelectionInput: ElementRef<HTMLInputElement>;
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  public displayedColumns: string[] = ['Name', 'Email', 'Active', 'Remove'];
  public dataSource = new MatTableDataSource<User>();
  public selectedMembers: { members: User[] } = { members: [] };
  public selectedMembersControl = new UntypedFormControl();
  private onDestroy$ = new Subject<void>();

  constructor(
    private userAdminEventBus: UserAdminEventBusService,
    private roleDetailsTabsFormService: RoleDetailsTabsFormService,
    private snackBarService: ToastService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (!isNil(changes.availableMembers)) {
        this.selectedMembersControl =
          this.roleDetailsTabsFormService.selectedMembersControl(
            this.availableMembers,
            this.selectedMembers
          );
        if (!isNil(this.memberSelectionInput)) {
          this.memberSelectionInput.nativeElement.value = '';
        }
        this.selectedMembersControl.setValue(null);
        this.clearFilteredMembers();
      }

      if (!isNil(changes.selectedRole)) {
        this.dataSource.data = [...this.selectedRole.Users];
        this.dataSource.sortingDataAccessor = (data, header) => {
          switch (header) {
            case 'Name': {
              return `${data['FirstName']} ${data['LastName']}`;
            }
            default: {
              return data[header];
            }
          }
        };
      }
    }
  }

  clearFilteredMembers(): void {
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.ADD_ROLE_FILTERED_MEMBERS,
      value: [],
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  selectMember(event: MatAutocompleteSelectedEvent): void {
    const member = event.option.value as User;
    if (this.selectedMembers.members.includes(member)) {
      return;
    }
    this.selectedMembers.members.push(member);
    this.memberSelectionInput.nativeElement.value = '';
    this.memberSelectionInput.nativeElement.focus();
    this.clearFilteredMembers();
  }

  addMember(_: MatChipInputEvent): void {
    this.memberSelectionInput.nativeElement.value = '';
    this.memberSelectionInput.nativeElement.focus();
    this.clearFilteredMembers();
  }

  addMembers(): void {
    const invalids: string[] = [];

    if (this.memberSelectionInput.nativeElement.value != '') {
      // supported delimitters: semi-colon, comma, and characters considered to be whitespace
      const re = /;|,|\s/;
      const candidates: string[] =
        this.memberSelectionInput.nativeElement.value.split(re);

      candidates.forEach((candidate) => {
        if (candidate === '') {
          return;
        }

        const inputCandidate = candidate;
        candidate = candidate.trim().toLowerCase();

        const found: User = this.availableMembers.find(({ Email }) => {
          if (!isNil(Email)) {
            return Email.toLowerCase() === candidate;
          }
        });

        if (!isNil(found)) {
          const foundUser: User = this.selectedRole.Users.find(({ Email }) => {
            if (!isNil(Email)) {
              return Email.toLowerCase() === candidate;
            }
          });
          if (isNil(foundUser)) {
            if (
              isNil(
                this.selectedMembers.members.find(({ Email }) => {
                  if (!isNil(Email)) {
                    return Email.toLowerCase() === candidate;
                  }
                })
              )
            ) {
              this.selectedMembers.members.push(found);
            }
          }
        } else {
          invalids.push(inputCandidate);
        }
      });

      if (this.selectedMembers.members.length > 0) {
        this.snackBarService.openSnackBar(
          `Added ${this.selectedMembers.members.length} ${
            this.selectedMembers.members.length > 1 ? 'users' : 'user'
          }`,
          'success'
        );
      }

      if (invalids.length > 0) {
        const delay = this.selectedMembers.members.length > 0 ? 2000 : 0;
        setTimeout(() => {
          this.snackBarService.openSnackBar(
            `Rejected ${invalids.length} ${
              invalids.length > 1 ? 'usernames' : 'username'
            }:` + invalids.map((i) => '"' + i + '"').join(', '),
            'error'
          );
        }, delay);
      }
    }

    if (this.selectedMembers.members.length > 0) {
      // responded to by RolesTabComponent
      const userAdminStateChange: UserAdminStateChange = {
        event: UserAdminBusEventTypes.ADD_ROLE_MEMBERS,
        value: this.selectedMembers.members,
      };
      this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
      this.selectedMembers.members = [];
    }

    if (invalids.length == 0) {
      this.memberSelectionInput.nativeElement.value = '';
      this.memberSelectionInput.nativeElement.blur();
    } else {
      this.memberSelectionInput.nativeElement.value = invalids.join(',');
    }
  }

  removeSelectedMember(member: User): void {
    this.selectedMembers.members.splice(
      this.selectedMembers.members.indexOf(member),
      1
    );
  }

  removeMember(member: User): void {
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_ROLE_MEMBER,
      value: member,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  removeAllMembers(): void {
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_ALL_ROLE_MEMBERS,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
