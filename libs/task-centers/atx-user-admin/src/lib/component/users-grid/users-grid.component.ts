import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';
import { ActiveFormatterComponent } from '../active-formatter/active-formatter.component';
import {
  ClientSideRowModelModule,
  ColDef,
  ColumnApi,
  EnterpriseCoreModule,
  FilterChangedEvent,
  GridApi,
  GridOptions,
  GridReadyEvent,
  Module,
  RowNode,
  SelectionChangedEvent,
  SetFilterModule,
  ValueGetterParams,
} from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';
import { User, UserDetails } from '@atonix/shared/api';
import {
  UserAdminEventBusService,
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
  UserAdminStateChange,
} from '../../service/user-admin-event-bus.service';
import { NavFacade } from '@atonix/atx-navigation';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-users-grid',
  templateUrl: './users-grid.component.html',
  styleUrls: ['./users-grid.component.scss'],
})
export class UsersGridComponent implements AfterViewInit, OnChanges, OnDestroy {
  @Input() isLoading?: boolean;
  @Input() users: User[];
  @Input() filters: any[];
  @Input() selectedUser?: UserDetails;

  public columnApi: ColumnApi;
  public gridApi: GridApi;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ClientSideRowModelModule,
    SetFilterModule,
  ];
  public gridOptions: GridOptions = {
    defaultColDef: {
      filter: true,
      floatingFilter: true,
      sortable: true,
      resizable: false,
    },
    columnDefs: [
      {
        colId: 'Active',
        headerName: 'Active',
        headerTooltip: 'Active',
        field: 'Active',
        cellRenderer: 'activeRenderer',
        filter: 'agSetColumnFilter',
        hide: false,
        width: 55,
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.Active ?? false);
        },
      },
      {
        colId: 'Username',
        headerName: 'User Name',
        headerTooltip: 'User Name',
        valueGetter: (params: ValueGetterParams) =>
          `${params.data.FirstName} ${params.data.LastName}`,
        hide: false,
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
      },
      {
        colId: 'Email',
        headerName: 'Email',
        headerTooltip: 'Email',
        field: 'Email',
        hide: false,
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
      },
    ],
    components: {
      activeRenderer: ActiveFormatterComponent,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.initializeGrid();
    },
    floatingFiltersHeight: 30,
    debug: false,
    rowSelection: 'single',
    suppressRowDeselection: true,
    getRowId: (params) => `${params.data?.Id}_${params.data?.Email}`,
    onFilterChanged: (event: FilterChangedEvent) => {
      if (!isNil(event)) {
        this.getFilters();
      }
    },
  };
  private previousRowNodeId: string = null;
  private unsubscribe$ = new Subject<void>();

  constructor(
    public navFacade: NavFacade,
    private userAdminEventBus: UserAdminEventBusService,
    private el: ElementRef
  ) {}

  ngAfterViewInit(): void {
    this.el.nativeElement
      .querySelectorAll('.ag-text-field-input')
      ?.forEach((element: HTMLInputElement) => {
        element.autocomplete = 'off';
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(this.gridApi) && !isNil(changes)) {
      if (!isNil(changes.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else if (this.isLoading === false) {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(changes.users)) {
        this.gridApi.setRowData(this.users);
        if (!isNil(this.selectedUser)) {
          this.gridApi
            .getRowNode(`${this.selectedUser.Id}_${this.selectedUser.Email}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  initializeGrid(): void {
    if (!isNil(this.columnApi) && !isNil(this.gridApi)) {
      this.gridApi.sizeColumnsToFit();
      this.gridApi.setFilterModel({
        Active: {
          values: ['true'],
          filterType: 'set',
        },
      });

      if (!isNil(this.isLoading)) {
        if (this.isLoading === true) {
          this.gridApi.showLoadingOverlay();
        } else if (this.isLoading === false) {
          this.gridApi.hideOverlay();
        }
      }
      if (!isNil(this.users)) {
        this.gridApi.setRowData(this.users);
        if (!isNil(this.selectedUser)) {
          this.gridApi
            .getRowNode(`${this.selectedUser.Id}_${this.selectedUser.Email}`)
            .setSelected(true, true, true);
        }
      }
    }
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api.onFilterChanged();
    }
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();

    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key).setModel(null);
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_USER_FILTERS,
      value: [],
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
    this.refreshGrid();
  }

  removeFilter(filter: any): void {
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_USER_FILTER,
      value: {
        filter,
        callbackFn: this.postRemoveFilterCallbackFn.bind(this),
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName).setModel(null);
    this.refreshGrid();
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];

    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef.headerName,
      });
    });

    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.SET_USER_FILTERS,
      value: filters,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  trackByFn = (_: number, item: any): string => item?.Name;

  ngOnDestroy(): void {
    if (!isNil(this.gridApi)) {
      this.gridApi.destroy();
      this.gridApi = null;
      this.columnApi = null;
    }

    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  selectionChanged(event: SelectionChangedEvent): void {
    const node: RowNode = event.api.getSelectedNodes()[0];
    // responded to by UsersTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.CLICK_USER_LIST_ROW,
      value: {
        user: node.data as User,
        callbackFn: (value: boolean): void => {
          if (value === false) {
            event.api.getRowNode(node.id).setSelected(false, false, true);
            event.api
              .getRowNode(this.previousRowNodeId)
              .setSelected(true, true, true);
            event.api.clearFocusedCell();
          } else if (value === true) {
            this.previousRowNodeId = node.id;
          }
        },
      },
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }
}
