import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { NavFacade } from '@atonix/atx-navigation';
import { UsersGridComponent } from './users-grid.component';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { BehaviorSubject } from 'rxjs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
describe('UsersGridComponent', () => {
  let component: UsersGridComponent;
  let fixture: ComponentFixture<UsersGridComponent>;
  let navFacade: NavFacade;
  let userAdminEventBus: UserAdminEventBusService;
  beforeEach(waitForAsync(() => {
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
    });
    userAdminEventBus = createMockWithValues(UserAdminEventBusService, {
      emit: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [AgGridModule, HttpClientTestingModule],
      declarations: [UsersGridComponent],
      providers: [
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: NavFacade, useValue: navFacade },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: UserAdminEventBusService, useValue: userAdminEventBus },
      ],

      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersGridComponent);
    component = fixture.componentInstance;
    component.filters = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
