import { FlatTreeControl } from '@angular/cdk/tree';
import {
  Component,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { isNil } from '@atonix/atx-core';
import { TopLevelAssetNode } from '@atonix/shared/api';
import { cloneDeep } from 'lodash';
import { Subject } from 'rxjs';
import { TopLevelAssetTreeDataSource } from '../../service/top-level-asset-tree.service.datasource';
import { TopLevelAssetTreeFacade } from '../../service/top-level-asset-tree.service.facade';

@Component({
  selector: 'atx-top-level-asset-tree',
  templateUrl: './top-level-asset-tree.component.html',
  styleUrls: ['./top-level-asset-tree.component.scss'],
})
export class TopLevelAssetTreeComponent implements OnChanges, OnDestroy {
  @Input() nodes: TopLevelAssetNode[];
  @Input() selectedNode: TopLevelAssetNode;
  @Output() nodeClicked = new EventEmitter<TopLevelAssetNode>();

  public displayedColumns: string[] = ['assets'];
  public treeControl: FlatTreeControl<TopLevelAssetNode>;
  public dataSource: TopLevelAssetTreeDataSource;
  private onDestroy$ = new Subject<void>();

  constructor(private facade: TopLevelAssetTreeFacade) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (!isNil(changes.nodes) && this.nodes.length > 0) {
        this.facade.setNodes(cloneDeep(this.nodes));
        this.treeControl = new FlatTreeControl<TopLevelAssetNode>(
          (node: TopLevelAssetNode) => node.Level,
          (node: TopLevelAssetNode) => !isNil(node.Children)
        );
        this.dataSource = new TopLevelAssetTreeDataSource(
          this.treeControl,
          this.facade
        );
      }
      if (
        !isNil(changes.selectedNode) &&
        !isNil(this.selectedNode) &&
        !isNil(this.dataSource)
      ) {
        this.dataSource.data.forEach((data: TopLevelAssetNode) => {
          data.IsSelected = data.AssetId === this.selectedNode.AssetId;
        });
      }
    }
  }

  selectNode(node: TopLevelAssetNode): void {
    if (
      isNil(this.selectedNode) ||
      node.AssetId !== this.selectedNode.AssetId
    ) {
      this.nodeClicked.emit(node);
    }
  }

  expandNode(event: PointerEvent | MouseEvent, node: TopLevelAssetNode): void {
    event.stopPropagation();
    this.treeControl.toggle(node);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
