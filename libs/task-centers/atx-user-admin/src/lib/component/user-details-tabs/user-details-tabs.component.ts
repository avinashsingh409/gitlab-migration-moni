import { Component, EventEmitter, Input, Output } from '@angular/core';
import { isNil } from '@atonix/atx-core';
import { UserDetails } from '@atonix/shared/api';
import { RolePermission } from '../../model/roles';
import { GroupPermission } from '../../model/users';

@Component({
  selector: 'atx-user-admin-user-details-view-tabs',
  templateUrl: './user-details-tabs.component.html',
  styleUrls: ['./user-details-tabs.component.scss'],
})
export class UserDetailsTabsComponent {
  @Input() selectedUser?: UserDetails;
  @Input() isLoading?: boolean;
  @Input() rolePermissions: RolePermission[];
  @Input() isDetailsSubTabDirty: boolean;
  @Input() isRolesSubTabDirty: boolean;
  @Input() isGroupsSubTabDirty: boolean;
  @Input() groupPermissions: GroupPermission[];
  @Output() initiatePasswordReset = new EventEmitter<string>();
  public selectedIndex = 0;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  selectTab(tab: any): void {
    if (!isNil(tab?.tabIndex)) {
      this.selectedIndex = tab.tabIndex;
    }
  }
}
