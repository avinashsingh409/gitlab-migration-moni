import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { UserDetailsTabsComponent } from './user-details-tabs.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('UserDetailsTabsComponent', () => {
  let component: UserDetailsTabsComponent;
  let fixture: ComponentFixture<UserDetailsTabsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UserDetailsTabsComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsTabsComponent);
    component = fixture.componentInstance;
    component.selectedUser = {
      RoleIds: [1],
      Roles: null,
      Id: '',
      FirstName: 'Test',
      LastName: 'McTester',
      Email: 'test@test.com',
      CustomerId: '123',
      Active: true,
      ServiceAccount: true,
      FederatedAccount: true,
      Groups: null,
      GroupIds: ['1'],
      CustomerName: 'Customer A',
      IsGroupSub: false,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
