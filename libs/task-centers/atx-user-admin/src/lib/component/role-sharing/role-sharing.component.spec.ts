import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { RoleSharingComponent } from './role-sharing.component';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('RoleSharingComponent', () => {
  let component: RoleSharingComponent;
  let fixture: ComponentFixture<RoleSharingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      declarations: [RoleSharingComponent],
      providers: [
        ControlContainer,
        { provide: APP_CONFIG, useValue: AppConfig },
        UsersTabFormService,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        UserAdminEventBusService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleSharingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
