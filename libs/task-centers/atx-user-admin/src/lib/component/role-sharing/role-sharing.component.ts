import {
  Component,
  OnDestroy,
  Input,
  SimpleChanges,
  OnChanges,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { isNil } from '@atonix/atx-core';
import { MatSort } from '@angular/material/sort';
import { RoleDetails, Customer } from '@atonix/shared/api';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatTableDataSource } from '@angular/material/table';
import { RoleDetailsTabsFormService } from '../../service/role-details-tabs-form.service';
import {
  UserAdminEventBusService,
  UserAdminStateChange,
  UserAdminBusEventTypes,
  UserAdminEmitEvent,
} from '../../service/user-admin-event-bus.service';

@Component({
  selector: 'atx-user-admin-role-sharing',
  templateUrl: './role-sharing.component.html',
  styleUrls: ['./role-sharing.component.scss'],
})
export class RoleSharingComponent implements OnChanges, OnDestroy {
  @Input() isLoading?: boolean;
  @Input() availableCustomers: Customer[];
  @Input() selectedRole: RoleDetails;
  @Input() filteredCustomers: Customer[];
  @ViewChild('customerSelectionInput')
  customerSelectionInput: ElementRef<HTMLInputElement>;
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }

  public displayedColumns: string[] = ['Name', 'Remove'];
  public dataSource = new MatTableDataSource<Customer>();
  public selectedCustomers: { customers: Customer[] } = { customers: [] };
  public selectedCustomersControl = new UntypedFormControl();
  private onDestroy$ = new Subject<void>();

  constructor(
    private userAdminEventBus: UserAdminEventBusService,
    private roleDetailsTabsFormService: RoleDetailsTabsFormService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (!isNil(changes.availableCustomers)) {
        this.selectedCustomersControl =
          this.roleDetailsTabsFormService.selectedCustomersControl(
            this.availableCustomers,
            this.selectedCustomers
          );
        if (!isNil(this.customerSelectionInput)) {
          this.customerSelectionInput.nativeElement.value = '';
        }
        this.selectedCustomersControl.setValue(null);
        this.clearFilteredCustomers();
      }

      if (!isNil(changes.selectedRole)) {
        this.dataSource.data = [...this.selectedRole.Shares];
      }
    }
  }

  clearFilteredCustomers(): void {
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.ADD_ROLE_FILTERED_CUSTOMERS,
      value: [],
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  selectCustomer(event: MatAutocompleteSelectedEvent): void {
    const customer = event.option.value as Customer;
    if (this.selectedCustomers.customers.includes(customer)) {
      return;
    }
    this.selectedCustomers.customers.push(customer);
    this.customerSelectionInput.nativeElement.value = '';
    this.customerSelectionInput.nativeElement.focus();
    this.clearFilteredCustomers();
  }

  addCustomer(_: MatChipInputEvent): void {
    this.customerSelectionInput.nativeElement.value = '';
    this.customerSelectionInput.nativeElement.focus();
    this.clearFilteredCustomers();
  }

  addCustomers(): void {
    if (this.selectedCustomers.customers.length > 0) {
      // responded to by RolesTabComponent
      const userAdminStateChange: UserAdminStateChange = {
        event: UserAdminBusEventTypes.ADD_ROLE_CUSTOMERS,
        value: this.selectedCustomers.customers,
      };
      this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
      this.customerSelectionInput.nativeElement.value = '';
      this.customerSelectionInput.nativeElement.blur();
      this.selectedCustomers.customers = [];
    }
  }

  removeSelectedCustomer(customer: Customer): void {
    this.selectedCustomers.customers.splice(
      this.selectedCustomers.customers.indexOf(customer),
      1
    );
  }

  removeCustomer(customer: Customer): void {
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_ROLE_CUSTOMER,
      value: customer,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  removeAllCustomers(): void {
    // responded to by RolesTabComponent
    const userAdminStateChange: UserAdminStateChange = {
      event: UserAdminBusEventTypes.REMOVE_ALL_ROLE_CUSTOMERS,
    };
    this.userAdminEventBus.emit(new UserAdminEmitEvent(userAdminStateChange));
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
