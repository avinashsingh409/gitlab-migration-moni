import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CreateNewCustomerDialogComponent } from './create-new-customer-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('CreateNewCustomerDialogComponent', () => {
  let component: CreateNewCustomerDialogComponent;
  let fixture: ComponentFixture<CreateNewCustomerDialogComponent>;
  const mockDialogRef = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      declarations: [CreateNewCustomerDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewCustomerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
