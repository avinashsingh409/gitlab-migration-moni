import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { catchError, take } from 'rxjs/operators';
import { CustomerNode, UserAdminCoreService } from '@atonix/shared/api';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { throwError } from 'rxjs';
import {
  UserAdminRxjsFacade,
  UserAdminRxjsFacadeState,
} from '../../service/user-admin.rxjs.facade';

@Component({
  selector: 'atx-user-admin-create-new-customer-dialog',
  templateUrl: './create-new-customer-dialog.component.html',
  styleUrls: ['./create-new-customer-dialog.component.scss'],
})
export class CreateNewCustomerDialogComponent {
  public newCustomerForm: UntypedFormGroup = new UntypedFormGroup({
    customerName: new UntypedFormControl(null),
  });

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateNewCustomerDialogComponent>,
    private userAdminCoreService: UserAdminCoreService,
    public userAdminRxjsFacade: UserAdminRxjsFacade
  ) {}

  cancelDialog(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.userAdminRxjsFacade.query.vm$
      .pipe(take(1))
      .subscribe((vm: UserAdminRxjsFacadeState) => {
        this.userAdminCoreService
          .createCustomer(
            this.newCustomerForm.get('customerName').value,
            vm.selectedCustomer.Id
          )
          .pipe(
            take(1),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((err: any) => {
              let message: string = null;
              if (err?.error?.Results?.length > 0) {
                if (err?.error?.Results[0]?.Message) {
                  message = err.error.Results[0].Message;
                }
              }
              this.dialogRef.close({ newUser: null, error: message });
              return throwError(message);
            })
          )
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((newCustomer: CustomerNode) => {
            this.dialogRef.close({ newCustomer });
            this.newCustomerForm.reset();
          });
      });
  }
}
