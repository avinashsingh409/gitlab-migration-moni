import {
  Component,
  OnDestroy,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Input,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { UserDetails } from '@atonix/shared/api';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { UntypedFormControl } from '@angular/forms';
import { isNil } from '@atonix/atx-core';
import { GroupPermission } from '../../model/users';

@Component({
  selector: 'atx-user-admin-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.scss'],
})
export class UserGroupsComponent implements OnChanges, OnDestroy {
  @Input() selectedUser?: UserDetails;
  @Input() isLoading: boolean;
  @Input() groupPermissions: GroupPermission[];
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  public displayedColumns: string[] = ['Allow', 'Groups', 'Customer', 'Active'];
  public dataSource = new MatTableDataSource<GroupPermission>();
  public onDestroy$ = new Subject<void>();

  constructor(private usersTabFormService: UsersTabFormService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes) && !isNil(changes.groupPermissions)) {
      this.dataSource.data = [...this.groupPermissions];
      this.dataSource.sortingDataAccessor = (data, header) => {
        switch (header) {
          case 'Allow': {
            return data.Allow;
          }
          case 'Groups': {
            return data.Group.Name;
          }
          case 'Customer': {
            return data.Group.CustomerName;
          }
          case 'Active': {
            return data.Group.Active;
          }
          default: {
            return data[header];
          }
        }
      };
    }
  }

  groupFormControl(group: GroupPermission): UntypedFormControl {
    return this.usersTabFormService.groupPermission(group);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
