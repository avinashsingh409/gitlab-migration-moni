import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserGroupsComponent } from './user-groups.component';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { FormControl } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('UserGroupsComponent', () => {
  let component: UserGroupsComponent;
  let fixture: ComponentFixture<UserGroupsComponent>;
  let mockUsersTabFormService: UsersTabFormService;

  beforeEach(async () => {
    mockUsersTabFormService = createMockWithValues(UsersTabFormService, {
      active: jest.fn(() => new FormControl(false)),
      email: jest.fn(() => new FormControl('test@test.com')),
      lastName: jest.fn(() => new FormControl('McTester')),
      firstName: jest.fn(() => new FormControl('Tester')),
    });
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [UserGroupsComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: UsersTabFormService, useValue: mockUsersTabFormService },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(UserGroupsComponent);
    component = fixture.componentInstance;
    component.isLoading = false;
    component.selectedUser = null;
    fixture.detectChanges();
    await fixture.whenRenderingDone();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
