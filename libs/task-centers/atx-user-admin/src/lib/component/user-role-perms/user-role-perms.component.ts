import {
  Component,
  OnDestroy,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Input,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { UserDetails } from '@atonix/shared/api';
import { RolePermission } from '../../model/roles';
import { UsersTabFormService } from '../../service/users-tab-form.service';
import { UntypedFormControl } from '@angular/forms';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-user-admin-user-role-perms',
  templateUrl: './user-role-perms.component.html',
  styleUrls: ['./user-role-perms.component.scss'],
})
export class UserRolePermissionsComponent implements OnChanges, OnDestroy {
  @Input() selectedUser?: UserDetails;
  @Input() isLoading: boolean;
  @Input() rolePermissions: RolePermission[];
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  public displayedColumns: string[] = ['Allow', 'Name', 'RoleType'];
  public dataSource = new MatTableDataSource<RolePermission>();
  public onDestroy$ = new Subject<void>();

  constructor(private usersTabFormService: UsersTabFormService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes) && !isNil(changes.rolePermissions)) {
      this.dataSource.data = [...this.rolePermissions];
      this.dataSource.sortingDataAccessor = (data, header) => {
        switch (header) {
          case 'Name': {
            return data.Role.Name;
          }
          case 'RoleType': {
            return data.Role.RoleType;
          }
          case 'Allow': {
            return data.Allow;
          }
          default: {
            return data[header];
          }
        }
      };
    }
  }

  roleFormControl(role: RolePermission): UntypedFormControl {
    return this.usersTabFormService.rolePermission(role);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
