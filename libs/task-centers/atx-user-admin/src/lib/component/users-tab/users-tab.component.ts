/* eslint-disable rxjs/no-implicit-any-catch */
import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { ToastService } from '@atonix/shared/utils';
import { CustomerNode, User, UserDetails } from '@atonix/shared/api';
import { UserAdminRxjsFacade } from '../../service/user-admin.rxjs.facade';
import { CreateNewUserDialogComponent } from '../create-new-user-dialog/create-new-user-dialog.component';
import { UnsavedChangesDialogComponent } from '../unsaved-changes-dialog/unsaved-changes-dialog.component';
import {
  UserAdminBusEventTypes,
  UserAdminEventBusService,
} from '../../service/user-admin-event-bus.service';
import {
  UsersTabFacade,
  UsersTabServiceState,
} from '../../service/users-tab.service.facade';
import { isNil } from '@atonix/atx-core';
import { ComponentCanDeactivate } from '../../model/component-can-deactivate';
import { UsersTabService } from '../../service/users-tab.service';
import { HttpResponse } from '@angular/common/http';
import moment from 'moment';

@Component({
  selector: 'atx-users-tab',
  templateUrl: './users-tab.component.html',
  styleUrls: ['./users-tab.component.scss'],
})
export class UsersTabComponent
  implements OnInit, OnDestroy, ComponentCanDeactivate
{
  public vm$ = new Observable<UsersTabServiceState>();
  private customerId: string;
  private onDestroy$ = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private snackBarService: ToastService,
    private userAdminRxjsFacade: UserAdminRxjsFacade,
    private usersTabFacade: UsersTabFacade,
    private usersTabService: UsersTabService,
    userAdminEventBus: UserAdminEventBusService
  ) {
    // emitted by UsersGridComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.CLICK_USER_LIST_ROW,
      this.userChange.bind(this),
      this.onDestroy$
    );

    // emitted by UsersGridComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.SET_USER_FILTERS,
      this.setFilters.bind(this),
      this.onDestroy$
    );

    // emitted by UsersGridComponent
    userAdminEventBus.on(
      UserAdminBusEventTypes.REMOVE_USER_FILTER,
      this.removeFilter.bind(this),
      this.onDestroy$
    );

    this.vm$ = this.usersTabFacade.query.vm$;
  }

  canDeactivate(): boolean | Observable<boolean> {
    let value: boolean = null;
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        value = isDirty;
      });

    return value;
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.canDeactivate()) {
      $event.returnValue =
        'You have unsaved changes. Are you sure you want to continue? This action cannot be undone.';
    }
  }

  ngOnInit(): void {
    this.userAdminRxjsFacade.setIsDirty(false);
    this.userAdminRxjsFacade.query.selectedCustomer$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((customer: CustomerNode) => {
        if (!isNil(customer)) {
          if (!isNil(this.customerId)) {
            this.usersTabFacade.destroyCalls();
          }
          this.customerId = customer.Id;
          this.usersTabFacade.setLoading(true);
          this.usersTabFacade.getUsersRolesAndGroups(this.customerId);
        }
      });
    this.usersTabFacade.setUsersSubTabsDirty(false);
  }

  private userChange({
    user,
    callbackFn,
  }: {
    user: User;
    callbackFn: (value: boolean) => void;
  }): void {
    this.userAdminRxjsFacade.query.isDirty$
      .pipe(take(1))
      .subscribe((isDirty: boolean) => {
        if (isDirty === true) {
          this.openUnsavedChangesDialog(user, callbackFn);
        } else {
          callbackFn(true);
          this.updateSelectedUser(user);
        }
      });
  }

  private setFilters(filters: any[]): void {
    this.usersTabFacade.setFilters(filters);
  }

  private removeFilter({
    filter,
    callbackFn,
  }: {
    filter: any;
    callbackFn: (filterName: string) => void;
  }): void {
    this.usersTabFacade.removeFilter(filter, callbackFn);
  }

  updateSelectedUser(user: User): void {
    this.usersTabFacade.setLoading(true);
    this.userAdminRxjsFacade.setIsDirty(false);
    this.usersTabFacade.setUsersSubTabsDirty(false);
    this.usersTabFacade.updateSelectedUser(user);
  }

  openCreateNewUserDialog(): void {
    // open dialog in here similar to what manage issue follower dialog does on onNoClick
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.customerId;

    const dialogRef = this.dialog.open(
      CreateNewUserDialogComponent,
      dialogConfig
    );
    dialogRef
      .afterClosed()
      .subscribe(
        ({ newUser, error }: { newUser: UserDetails; error?: string }) => {
          if (!isNil(newUser)) {
            this.usersTabFacade.addNewUser(newUser);
            this.snackBarService.openSnackBar(
              `Created new user: ${newUser.Email}`,
              'success'
            );
          } else if (!isNil(error)) {
            this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
          }
        }
      );
  }

  openUnsavedChangesDialog(
    user: User,
    callbackFn: (value: boolean) => void
  ): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    const dialogRef = this.dialog.open(
      UnsavedChangesDialogComponent,
      dialogConfig
    );
    dialogRef.afterClosed().subscribe((exit: boolean) => {
      if (exit === true) {
        callbackFn(true);
        this.updateSelectedUser(user);
      } else {
        callbackFn(false);
      }
    });
  }

  onInitiatePasswordReset(userId: string): void {
    this.usersTabService
      .initiatePasswordReset(userId)
      .pipe(take(1))
      .subscribe(
        (result: string) => {
          this.snackBarService.openSnackBar(result, 'success');
        },
        (error: string) => {
          this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
        }
      );
  }

  download(): void {
    this.usersTabService
      .downloadUserList(this.customerId)
      .pipe(take(1))
      .subscribe(
        (result: HttpResponse<any>) => {
          const date: string = moment().format('M-d-YYYY HH-mm');
          const filename = `User List ${date}.xlsx`;

          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(result.body);
          downloadLink.setAttribute('download', filename);
          document.body.appendChild(downloadLink);
          downloadLink.click();
          downloadLink.remove();

          this.snackBarService.openSnackBar(
            'File downloaded successfully',
            'success'
          );
        },
        (error: any) => {
          if (typeof error === 'string') {
            this.snackBarService.openSnackBar(`Error: ${error}`, 'error');
          } else {
            error.error.text().then((text: string) => {
              const parsed: any = JSON.parse(text);
              this.snackBarService.openSnackBar(
                `Error: ${parsed.detail}`,
                'error'
              );
            });
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.usersTabFacade.destroyCalls();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
