import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { catchError, take } from 'rxjs/operators';
import { GroupDetails, UserAdminCoreService } from '@atonix/shared/api';
import { throwError } from 'rxjs';

@Component({
  selector: 'atx-user-admin-create-new-group-dialog',
  templateUrl: './create-new-group-dialog.component.html',
  styleUrls: ['./create-new-group-dialog.component.scss'],
})
export class CreateNewGroupDialogComponent {
  @ViewChild('newGroupNameInput')
  newGroupNameInput: ElementRef<HTMLInputElement>;
  constructor(
    private dialogRef: MatDialogRef<CreateNewGroupDialogComponent>,
    private userAdminCoreService: UserAdminCoreService,
    @Inject(MAT_DIALOG_DATA) private owningCustomerId: string
  ) {}

  cancelDialog(): void {
    this.dialogRef.close();
  }

  save(): void {
    this.userAdminCoreService
      .createGroup(
        this.newGroupNameInput.nativeElement.value,
        this.owningCustomerId
      )
      .pipe(
        take(1),
        // eslint-disable-next-line rxjs/no-implicit-any-catch
        catchError((err: any) => {
          let message: string = null;
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              message = err.error.Results[0].Message;
            }
          }
          this.dialogRef.close({ newGroup: null, error: message });
          return throwError(message);
        })
      )
      .subscribe((newGroup: GroupDetails) => {
        this.dialogRef.close({ newGroup });
      });
  }
}
