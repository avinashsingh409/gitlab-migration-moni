import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CreateNewGroupDialogComponent } from './create-new-group-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('CreateNewGroupDialogComponent', () => {
  let component: CreateNewGroupDialogComponent;
  let fixture: ComponentFixture<CreateNewGroupDialogComponent>;
  const mockDialogRef = {};
  const data = '';

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatDialogRef, useValue: mockDialogRef },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data,
          },
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      declarations: [CreateNewGroupDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNewGroupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
