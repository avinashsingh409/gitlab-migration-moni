import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-user-admin-change-email-confirmation-dialog',
  templateUrl: './change-email-confirmation-dialog.component.html',
  styleUrls: ['./change-email-confirmation-dialog.component.scss'],
})
export class ChangeEmailConfirmationDialogComponent {
  constructor(
    private dialogRef: MatDialogRef<ChangeEmailConfirmationDialogComponent>
  ) {}

  cancel(): void {
    this.dialogRef.close();
  }

  continue(): void {
    this.dialogRef.close(true);
  }
}
