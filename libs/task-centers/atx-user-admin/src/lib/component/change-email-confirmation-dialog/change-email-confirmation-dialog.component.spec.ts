import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ChangeEmailConfirmationDialogComponent } from './change-email-confirmation-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('ChangeEmailConfirmationDialogComponent', () => {
  let component: ChangeEmailConfirmationDialogComponent;
  let fixture: ComponentFixture<ChangeEmailConfirmationDialogComponent>;
  const mockDialogRef = {};

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [
        { provide: MatDialogRef, useValue: mockDialogRef },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
      declarations: [ChangeEmailConfirmationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeEmailConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
