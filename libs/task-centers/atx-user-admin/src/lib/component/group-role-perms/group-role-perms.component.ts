import {
  Component,
  OnDestroy,
  ViewChild,
  OnChanges,
  SimpleChanges,
  Input,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Subject } from 'rxjs';
import { GroupDetails } from '@atonix/shared/api';
import { RolePermission } from '../../model/roles';
import { UntypedFormControl } from '@angular/forms';
import { isNil } from '@atonix/atx-core';
import { GroupsTabFormService } from '../../service/groups-tab-form.service';

@Component({
  selector: 'atx-user-admin-group-role-perms',
  templateUrl: './group-role-perms.component.html',
  styleUrls: ['./group-role-perms.component.scss'],
})
export class GroupRolePermissionsComponent implements OnChanges, OnDestroy {
  @Input() selectedGroup?: GroupDetails;
  @Input() isLoading: boolean;
  @Input() rolePermissions: RolePermission[];
  @ViewChild(MatSort, { static: false }) set sort(sort: MatSort) {
    this.dataSource.sort = sort;
  }
  public displayedColumns: string[] = ['Allow', 'Name', 'RoleType'];
  public dataSource = new MatTableDataSource<RolePermission>();
  public onDestroy$ = new Subject<void>();

  constructor(private groupsTabFormService: GroupsTabFormService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes) && !isNil(changes.rolePermissions)) {
      this.dataSource.data = [...this.rolePermissions];
      this.dataSource.sortingDataAccessor = (data, header) => {
        switch (header) {
          case 'Name': {
            return data.Role.Name;
          }
          case 'RoleType': {
            return data.Role.RoleType;
          }
          case 'Allow': {
            return data.Allow;
          }
          default: {
            return data[header];
          }
        }
      };
    }
  }

  roleFormControl(role: RolePermission): UntypedFormControl {
    return this.groupsTabFormService.rolePermission(role);
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
