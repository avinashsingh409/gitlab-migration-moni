import { ComponentFixture, TestBed } from '@angular/core/testing';
import { GroupRolePermissionsComponent } from './group-role-perms.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { FormControl } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { GroupsTabFormService } from '../../service/groups-tab-form.service';

describe('GroupRolePermissionsComponent', () => {
  let component: GroupRolePermissionsComponent;
  let fixture: ComponentFixture<GroupRolePermissionsComponent>;
  let mockGroupsTabFormService: GroupsTabFormService;

  beforeEach(async () => {
    mockGroupsTabFormService = createMockWithValues(GroupsTabFormService, {
      rolePermission: jest.fn(() => new FormControl(true)),
    });
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [GroupRolePermissionsComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: GroupsTabFormService, useValue: mockGroupsTabFormService },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(GroupRolePermissionsComponent);
    component = fixture.componentInstance;
    component.isLoading = false;
    component.selectedGroup = null;
    fixture.detectChanges();
    await fixture.whenRenderingDone();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
