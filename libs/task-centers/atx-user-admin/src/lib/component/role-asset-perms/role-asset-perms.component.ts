import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  AssetNode,
  AssetPermission,
  Customer,
  RoleDetails,
} from '@atonix/shared/api';
import { AssetEmitNode } from '../../model/asset-emit-node';

@Component({
  selector: 'atx-user-admin-role-asset-perms',
  templateUrl: './role-asset-perms.component.html',
})
export class RoleAssetPermissionsComponent {
  @Input() isLoading?: boolean;
  @Input() selectedRole: RoleDetails;
  @Input() selectedCustomer: Customer;
  @Input() assets: AssetNode[];
  @Input() assetPermissions: AssetPermission[];
  @Output() assetTreeTableStateChange = new EventEmitter<AssetEmitNode[]>();

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}
}
