import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ControlContainer, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

import { RoleAssetPermissionsComponent } from './role-asset-perms.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AssetTreeTableComponent } from '../asset-tree-table/asset-tree-table.component';
import { provideMock } from '@testing-library/angular/jest-utils';
import { UserAdminEventBusService } from '../../service/user-admin-event-bus.service';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('RoleAssetPermissionsComponent', () => {
  let component: RoleAssetPermissionsComponent;
  let fixture: ComponentFixture<RoleAssetPermissionsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      declarations: [RoleAssetPermissionsComponent, AssetTreeTableComponent],
      providers: [
        provideMock(UserAdminEventBusService),
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAssetPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
