/* eslint-disable max-len */
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/core';
LicenseManager.setLicenseKey(
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
import { CommonModule } from '@angular/common';
import { NavigationModule } from '@atonix/atx-navigation';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedUiModule } from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import {
  UserAdminRoutingModule,
  AppRoutingComponents,
} from './atx-user-admin-routing.module';
import { UserAdminComponent } from './component/user-admin/user-admin.component';
import { UsersTabComponent } from './component/users-tab/users-tab.component';
import { RolesTabComponent } from './component/roles-tab/roles-tab.component';
import { AssetTreeTableComponent } from './component/asset-tree-table/asset-tree-table.component';
import { AssetTreeTableServiceFacade } from './service/asset-tree-table.service.facade';
import { CreateNewRoleDialogComponent } from './component/create-new-role-dialog/create-new-role-dialog.component';
import { RoleDetailsTabsComponent } from './component/role-details-tabs/role-details-tabs.component';
import { UserAdminEventBusService } from './service/user-admin-event-bus.service';
import { RoleAppPermissionsComponent } from './component/role-app-perms/role-app-perms.component';
import { RoleAssetPermissionsComponent } from './component/role-asset-perms/role-asset-perms.component';
import { RoleMembershipComponent } from './component/role-members/role-members.component';
import { RolesListComponent } from './component/roles-list/roles-list.component';
import { UsersGridComponent } from './component/users-grid/users-grid.component';
import { CreateNewUserDialogComponent } from './component/create-new-user-dialog/create-new-user-dialog.component';
import { UserDetailsTabsComponent } from './component/user-details-tabs/user-details-tabs.component';
import { UserDetailsInfoComponent } from './component/user-details-info/user-details-info.component';
import { UserRolePermissionsComponent } from './component/user-role-perms/user-role-perms.component';
import { ActiveFormatterComponent } from './component/active-formatter/active-formatter.component';
import { UnsavedChangesDialogComponent } from './component/unsaved-changes-dialog/unsaved-changes-dialog.component';
import { CustomerTreeFacade } from './service/customer-tree.service.facade';
import { CustomerTreeOldComponent } from './component/customer-tree/customer-tree-old.component';
import { CreateNewCustomerDialogComponent } from './component/create-new-customer-dialog/create-new-customer-dialog.component';
import { CustomerTabComponent } from './component/customer-tab/customer-tab.component';
import { TopLevelAssetTreeComponent } from './component/top-level-asset-tree/top-level-asset-tree.component';
import { RoleSharingComponent } from './component/role-sharing/role-sharing.component';
import { UserGroupsComponent } from './component/user-groups/user-groups.component';
import { GroupsTabComponent } from './component/groups-tab/groups-tab.component';
import { CreateNewGroupDialogComponent } from './component/create-new-group-dialog/create-new-group-dialog.component';
import { GroupsListComponent } from './component/groups-list/groups-list.component';
import { GroupDetailsTabsComponent } from './component/group-details-tabs/group-details-tabs.component';
import { GroupRolePermissionsComponent } from './component/group-role-perms/group-role-perms.component';
import { GroupMembersComponent } from './component/group-members/group-members.component';
import { ChangeEmailConfirmationDialogComponent } from './component/change-email-confirmation-dialog/change-email-confirmation-dialog.component';
import { AtxCustomerTreeModule } from '@atx/atx-customer-tree';

@NgModule({
  declarations: [
    UserAdminComponent,
    UsersTabComponent,
    RolesTabComponent,
    AssetTreeTableComponent,
    AppRoutingComponents,
    CreateNewRoleDialogComponent,
    RoleDetailsTabsComponent,
    RoleAppPermissionsComponent,
    RoleAssetPermissionsComponent,
    RoleMembershipComponent,
    RolesListComponent,
    UsersGridComponent,
    CreateNewUserDialogComponent,
    UserDetailsTabsComponent,
    UserDetailsInfoComponent,
    UserRolePermissionsComponent,
    ActiveFormatterComponent,
    UnsavedChangesDialogComponent,
    CustomerTreeOldComponent,
    CreateNewCustomerDialogComponent,
    CustomerTabComponent,
    TopLevelAssetTreeComponent,
    RoleSharingComponent,
    UserGroupsComponent,
    GroupsTabComponent,
    CreateNewGroupDialogComponent,
    GroupsListComponent,
    GroupDetailsTabsComponent,
    GroupRolePermissionsComponent,
    GroupMembersComponent,
    ChangeEmailConfirmationDialogComponent,
  ],
  imports: [
    AgGridModule.withComponents([]),
    CommonModule,
    NavigationModule,
    AtxMaterialModule,
    SharedUiModule,
    ReactiveFormsModule,
    FormsModule,
    SharedApiModule,
    UserAdminRoutingModule,
    AtxCustomerTreeModule,
  ],
  providers: [
    AssetTreeTableServiceFacade,
    UserAdminEventBusService,
    CustomerTreeFacade,
  ],
  exports: [UserAdminComponent],
})
export class UserAdminModule {}
