export interface AssetEmitNode {
  AssetId: string;
  Allow: boolean;
}
