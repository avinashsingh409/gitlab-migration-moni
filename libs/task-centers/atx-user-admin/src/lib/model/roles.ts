import { Resource, ResourcePermission, Role, Group } from '@atonix/shared/api';

export interface AllResourcePermissions {
  Resource: Resource;
  ViewPermission?: ResourcePermission;
  EditPermission?: ResourcePermission;
  AddPermission?: ResourcePermission;
  DeletePermission?: ResourcePermission;
  AdminPermission?: ResourcePermission;
}

export interface RolePermission {
  Role: Role;
  Allow: boolean;
}

export interface RoleResourcePermissionChange {
  AccessTypeId: number;
  AllPermissions: AllResourcePermissions;
  Checked: boolean;
}
