export enum ResourceAccessTypes {
  View = 1,
  Edit = 2,
  Add = 4,
  Delete = 5,
  BVEdit = 9,
}
