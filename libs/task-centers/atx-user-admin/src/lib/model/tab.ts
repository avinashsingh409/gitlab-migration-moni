export interface ITab {
  path: string;
  label: string;
  isDirty: boolean;
}
