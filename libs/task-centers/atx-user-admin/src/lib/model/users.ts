import { Group } from '@atonix/shared/api';

export interface GroupPermission {
  Group: Group;
  Allow: boolean;
}
