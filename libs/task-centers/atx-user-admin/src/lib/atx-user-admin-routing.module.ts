import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAdminComponent } from './component/user-admin/user-admin.component';
import { UsersTabComponent } from './component/users-tab/users-tab.component';
import { RolesTabComponent } from './component/roles-tab/roles-tab.component';
import { IsDirtyGuard } from './service/is-dirty-guard.guard';
import { CustomerTabComponent } from './component/customer-tab/customer-tab.component';
import { GroupsTabComponent } from './component/groups-tab/groups-tab.component';

const routes: Routes = [
  {
    path: '',
    component: UserAdminComponent,
    children: [
      { path: '', redirectTo: 'users', pathMatch: 'full' },
      {
        path: 'users',
        component: UsersTabComponent,
        canDeactivate: [IsDirtyGuard],
      },
      {
        path: 'roles',
        component: RolesTabComponent,
        canDeactivate: [IsDirtyGuard],
      },
      {
        path: 'groups',
        component: GroupsTabComponent,
        canDeactivate: [IsDirtyGuard],
      },
      {
        path: 'customer',
        component: CustomerTabComponent,
        canDeactivate: [IsDirtyGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [IsDirtyGuard],
})
export class UserAdminRoutingModule {}
export const AppRoutingComponents = [UsersTabComponent];
