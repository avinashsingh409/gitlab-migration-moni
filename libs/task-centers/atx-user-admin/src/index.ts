/*
 * Public API Surface of atx-user-admin
 */

export * from './lib/atx-user-admin.module';
export * from './lib/atx-user-admin-routing.module';
export * from './lib/component/user-admin/user-admin.component';
export * from './lib/component/users-tab/users-tab.component';
export * from './lib/component/roles-tab/roles-tab.component';
export * from './lib/component/asset-tree-table/asset-tree-table.component';
export * from './lib/component/users-grid/users-grid.component';
