import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import { IViewExplorerState, adapter } from '../state/view-explorer-state';
import { numOfWorkers } from '../../model/num-of-workers';
import { IWidget, IWidgetChart } from '@atonix/atx-core';
import { IReportSection } from '@atonix/shared/api';

const { selectAll } = adapter.getSelectors();

export const selectApp = (state: {
  viewExplorer: IViewExplorerState;
  asset: any;
}) => state;

// This looks at the application state and grabs the member
// named viewExplorer.  It isn't doing anything more complex than that.
// This selector gets the state for view explorer. It is of type
// IViewExplorerState, which extends EntityState<IReport>
// It holds all the reports in its entities, and it holds related data
// as well.
export const selectViewExplorerState =
  createFeatureSelector<IViewExplorerState>('viewExplorer');

// This grabs the state of the selected asset
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);

export const selectChartList = createSelector(
  selectViewExplorerState,
  (state) => state.chartSelectList
);

export const selectChartListLoading = createSelector(
  selectViewExplorerState,
  (state) => state.chartSelectLoading
);

export const selectSecondaryAssetTreeState = createSelector(
  selectViewExplorerState,
  (state) => state.secondaryAssetTree
);

export const selectSecondaryAssetTreeConfiguration = createSelector(
  selectSecondaryAssetTreeState,
  (state) => state.treeConfiguration
);

//gets the label for the selected Chart/Table widget
export const selectSelectedChartLabel = createSelector(
  selectViewExplorerState,
  (state) => {
    if (!state.selectedElement) return '';
    const selectedWidget = state.selectedElement as IWidgetChart;
    const selectedWidgetId = selectedWidget.WidgetID;

    const selectedView = state.entities[state.selectedView];
    for (const section of selectedView.Sections) {
      for (const widget of section.Widgets) {
        if (
          widget.WidgetID === selectedWidgetId &&
          (selectedWidget.Kind === 'Chart' || selectedWidget.Kind === 'Table')
        ) {
          const foundWidget = widget as IWidgetChart;
          if (foundWidget.trend && foundWidget.trend.label) {
            return foundWidget.trend.label;
          } else {
            return '';
          }
        }
      }
    }
    return '';
  }
);

export const selectSecondaryAssetTreeNode = createSelector(
  selectSecondaryAssetTreeState,
  (state) =>
    state?.treeConfiguration?.nodes?.find(
      (a) =>
        a?.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state?.treeNodes))
    )
);

export const selectSecondaryAssetTreeNodeAssetId = createSelector(
  selectSecondaryAssetTreeNode,
  (state) => state?.data?.AssetId
);

// For Asset Tree
export const selectAssetTreeState = createSelector(
  selectViewExplorerState,
  (state) => state.assetTreeState
);

export const selectSelectedNode = createSelector(
  selectAssetTreeState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state?.treeNodes))
);

export const selectIsEditMode = createSelector(
  selectViewExplorerState,
  (state) => state.isEditMode
);
export const selectSelectedElement = createSelector(
  selectViewExplorerState,
  (state) => state.selectedElement
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  (state) =>
    state?.treeConfiguration?.nodes?.find(
      (a) =>
        a?.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state?.treeNodes))
    )
);

export const selectSelectedAssetID = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data?.AssetId
);

export const selectSelectedNodeID = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data?.NodeId
);

export const selectSelectedAssetKey = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return (
        data.TreeId +
        '~' +
        data.ParentNodeId +
        '~' +
        data.NodeId +
        '~' +
        data.AssetGuid
      );
    }
    return null;
  }
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration
);

// For Time Slider
export const selectTimeSliderState = createSelector(
  selectViewExplorerState,
  (state) => state.timeSliderState
);

export const selectTimeSelection = createSelector(
  selectTimeSliderState,
  (state) => {
    return { startDate: state?.startDate, endDate: state?.endDate };
  }
);

// Layout
export const selectLeftTraySize = createSelector(
  selectViewExplorerState,
  (s) => s.leftTraySize
);
export const selectLayoutMode = createSelector(
  selectAssetTreeConfiguration,
  (s) => {
    const result: TrayState = s.pin ? 'side' : 'over';
    return result;
  }
);

// export const selectViews = createSelector(selectViewExplorerState, selectAll);
export const selectViews = createSelector(selectViewExplorerState, (state) => {
  const sortedViews = Object.values(state.entities).sort((a, b) => {
    if (a.DisplayOrder > b.DisplayOrder) {
      return 1;
    } else if (a.DisplayOrder < b.DisplayOrder) {
      return -1;
    } else {
      return a.ReportID - b.ReportID;
    }
  });
  return sortedViews;
});

export const selectedViewID = createSelector(
  selectViewExplorerState,
  (state) => state.selectedView
);

export const selectIsViewStateDirty = createSelector(
  selectViewExplorerState,
  (state) => state.isViewStateDirty
);
export const selectViewRequestedInRoute = createSelector(
  selectViewExplorerState,
  (state) => state.viewRequestedInRoute
);

export const selectedView = createSelector(selectViewExplorerState, (state) => {
  return state?.entities[state?.selectedView];
});

// this is used in the effect checkIfWidgetsAreDone$ to determine if all of the widgets are populated.
export const selectNumberOfTrendWidgetsLeftToPopulate = createSelector(
  selectViewExplorerState,
  (state) => state.numberOfTrendWidgetsLeftToPopulate
);

// this selector is used when selecting the next widget to pull.
// we want to pull the widgets section by section so the things
// at the top are loaded first.
export const selectSections = createSelector(
  selectedView,
  (state) => state?.Sections || []
);

//gets the widgets of the currently selected section or widget's section
export const selectWidgetsOfCurrentSection = createSelector(
  selectViewExplorerState,
  (state) => {
    if (!state.selectedElement || state.selectedElement.Kind === 'View') {
      return [];
    }
    const element = state.selectedElement as IWidget | IReportSection;
    const sectionId = element.ReportSectionID;
    const selectedViewId = state.selectedView;
    const sections = state.entities[selectedViewId].Sections;
    for (const section of sections) {
      const widgetIndex = section.Widgets.findIndex(
        (widget) => widget.ReportSectionID === sectionId
      );
      if (widgetIndex !== -1) {
        return section.Widgets;
      }
    }

    return [];
  }
);

// this selector will pull the list of initiated widget pulls.
// it is helpful in the selector "selectNextWidgetToPull",
// because that needs to know which widgets have been initiated.
export const selectListOfInitiatedWidgetPulls = createSelector(
  selectViewExplorerState,
  (state) => state.listOfInitiatedWidgetPulls
);

// This selector will provide the next widget for the supervisor and workers to pull.
// if all the widgets are already pulled, it will return null.
export const selectNextWidgetToPull = createSelector(
  selectSections,
  selectListOfInitiatedWidgetPulls,
  (sections, listOfPulls) => {
    const pulls = new Set(listOfPulls);
    for (const section of sections) {
      for (const widget of section.Widgets) {
        if (!pulls.has(widget.WidgetID)) {
          return widget;
        }
      }
    }
    return null;
  }
);

export const selectFirstWidgetsToPull = createSelector(
  selectSections,
  (sections) => {
    const widgetsList: IWidget[] = [];

    for (const section of sections) {
      for (const widget of section.Widgets) {
        widgetsList.push(widget);
        if (widgetsList.length === numOfWorkers) {
          break;
        }
      }
    }
    return widgetsList;
  }
);

export const selectViewsLoading = createSelector(
  selectViewExplorerState,
  (state) => state.loadingViews
);
