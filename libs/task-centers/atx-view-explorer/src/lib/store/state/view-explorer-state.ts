import { ITimeSliderState } from '@atonix/atx-time-slider';
import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { IReport } from '@atonix/shared/api';
import { SelectableElement } from '../../model/selectable-elements';
import { IProcessedTrend } from '@atonix/atx-core';

export interface IViewExplorerState extends EntityState<IReport> {
  assetTreeState: ITreeState;
  timeSliderState: ITimeSliderState;
  selectedView: number;
  loadingViews?: boolean;
  listOfInitiatedWidgetPulls: string[];
  numberOfTrendWidgetsLeftToPopulate: number;
  leftTraySize: number;
  viewRequestedInRoute: number | undefined;
  isEditMode: boolean;
  selectedElement: SelectableElement;
  secondaryAssetTree: ITreeState | null;
  chartSelectList: IProcessedTrend[];
  chartSelectLoading: boolean;
  isViewStateDirty: boolean;
}

export function selectReportID(report: IReport) {
  return report.ReportID;
}

export function sortReports(a: IReport, b: IReport) {
  return (a.ReportDesc ?? '').localeCompare(b.ReportDesc ?? '');
}

export const adapter: EntityAdapter<IReport> = createEntityAdapter<IReport>({
  selectId: selectReportID,
  sortComparer: sortReports,
});

export const initialViewExplorerState: IViewExplorerState =
  adapter.getInitialState({
    assetTreeState: {
      treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
      treeNodes: createTreeBuilder(),
      hasDefaultSelectedAsset: true,
    },
    timeSliderState: null,
    listOfInitiatedWidgetPulls: [],
    numberOfTrendWidgetsLeftToPopulate: 0,
    selectedView: null,
    leftTraySize: 250,
    viewRequestedInRoute: undefined,
    isEditMode: false,
    selectedElement: null,
    secondaryAssetTree: {
      treeConfiguration: getDefaultTree({
        collapseOthers: false,
        showTreeSelector: false,
      }),
      treeNodes: createTreeBuilder(),
      hasDefaultSelectedAsset: false,
    },
    chartSelectList: [],
    chartSelectLoading: false,
    isViewStateDirty: false,
  });
