import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import * as actions from '../actions/secondary-asset-tree.actions';
import { ModelService } from '@atonix/atx-asset-tree';
import { selectSecondaryAssetTreeState } from '../selectors/view-explorer.selector';

@Injectable()
export class SecondaryAssetTreeEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private assetTreeService: ModelService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  tagListAssetStateChange$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.secondaryAssetTreeStateChange),
      concatLatestFrom(() => this.store.select(selectSecondaryAssetTreeState)),
      switchMap(([change, state]) =>
        this.assetTreeService
          .getAssetTreeData(
            change.treeStateChange,
            state.treeConfiguration,
            state.treeNodes
          )
          .pipe(
            map((n) => {
              return actions.secondaryAssetTreeStateChange({
                treeStateChange: n,
              });
            }),
            catchError((error: unknown) => {
              console.log(error);
              return of(
                actions.secondaryAssetTreeStateChangeFailure({
                  error: error as any,
                })
              );
            })
          )
      )
    );
  });
}
