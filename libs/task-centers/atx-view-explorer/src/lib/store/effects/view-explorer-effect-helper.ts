/* eslint-disable ngrx/select-style */
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  map,
  withLatestFrom,
  filter,
  concatMap,
  switchMap,
  catchError,
} from 'rxjs/operators';
import * as actions from '../actions/view-explorer.actions';
import { Store, Action, select } from '@ngrx/store';
import { IViewExplorerState } from '../state/view-explorer-state';
import { selectTimeSelection } from '../selectors/view-explorer.selector';
import { ModelService as ViewExplorerModelService } from '../../service/model.service';
import { of } from 'rxjs';

export const workerBeginFunction = (
  thisActions: Actions<Action>,
  // eslint-disable-next-line ngrx/no-typed-global-store
  thisStore: Store<IViewExplorerState>,
  thisViewExplorerModel: ViewExplorerModelService,
  workerNumber: number
) =>
  createEffect(() => {
    return thisActions.pipe(
      ofType(actions.workerBegin, actions.workerRedispatch),
      filter((z) => z.workerNumber === workerNumber),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(thisStore.pipe(select(selectTimeSelection)))
        )
      ),
      switchMap(([action, time]) =>
        thisViewExplorerModel
          .getWidgetData(time.startDate, time.endDate, action.widget)
          .pipe(
            map((widget) => actions.workerSuccess({ widget, workerNumber })),
            catchError((error: unknown) =>
              of(
                actions.workerFailure({
                  error,
                  widget: action.widget,
                  workerNumber,
                })
              )
            )
          )
      )
    );
  });
