/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Inject, Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  tap,
  filter,
  concatMap,
  debounceTime,
  startWith,
} from 'rxjs/operators';
import * as actions from '../actions/view-explorer.actions';
import { NavActions } from '@atonix/atx-navigation';
import { AuthSelectors } from '@atonix/shared/state/auth';
import { Store, select, Action } from '@ngrx/store';
import {
  selectNextWidgetToPull,
  selectFirstWidgetsToPull,
  selectSelectedNode,
  selectAssetTreeState,
  selectViews,
  selectSelectedAssetID,
  selectTimeSelection,
  selectNumberOfTrendWidgetsLeftToPopulate,
  selectedViewID,
  selectSelectedAssetKey,
  assetState,
  selectSelectedNodeID,
  selectViewRequestedInRoute,
  selectedView,
  selectSelectedElement,
  selectSelectedAssetTreeNode,
  selectIsViewStateDirty,
} from '../selectors/view-explorer.selector';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { Observable, of } from 'rxjs';
import { workerBeginFunction } from './view-explorer-effect-helper';
import {
  IProcessedTrend,
  isNil,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
  IWidgetTable,
} from '@atonix/atx-core';
import { ModelService as ViewExplorerModelService } from '../../service/model.service';
import { ViewExplorerService } from '../../service/view-explorer.service';
import { numOfWorkers } from '../../model/num-of-workers';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  AuthorizationFrameworkService,
  BusinessIntelligenceFrameworkService,
  IReport,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';
import { ToastService, catchSwitchMapError } from '@atonix/shared/utils';
import { copyToClipboard } from '@atonix/atx-chart';

@Injectable()
export class ViewExplorerEffects {
  constructor(
    private actions$: Actions,
    private businessIntelligenceFrameworkService: BusinessIntelligenceFrameworkService,
    private assetTreeModel: AssetTreeModel,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private viewExplorerModelService: ViewExplorerModelService,
    private biFrameworkService: BusinessIntelligenceFrameworkService,
    private viewExplorerService: ViewExplorerService,
    private processDataService: ProcessDataFrameworkService,
    private store: Store<any>,
    private toastService: ToastService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  createView$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createView),
      withLatestFrom(this.store.select(selectSelectedAssetTreeNode)),
      switchMap(([action, treeNode]) => {
        return this.biFrameworkService.createReport(treeNode.data.Asset);
      }),
      switchMap((res) => {
        return [
          actions.createViewSuccess({ report: res }),
          actions.selectView({ viewID: res.ReportID }),
        ];
      })
    )
  );

  deleteView$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.deleteView),
      withLatestFrom(
        this.store.select(selectedViewID),
        this.store.select(selectViews)
      ),
      switchMap(([action, viewId, views]) => {
        return this.biFrameworkService.deleteReport(viewId).pipe(
          map((res) => {
            return { ...res, viewId, views };
          })
        );
      }),
      switchMap((res) => {
        const deletedViewIndex = res.views.findIndex(
          (view) => view.ReportID === res.viewId
        );
        let nextViewId = res.views.at(0).ReportID;
        if (deletedViewIndex === 0 && res.views.length >= 2) {
          nextViewId = res.views.at(1).ReportID;
        } else if (res.views.length >= 2) {
          nextViewId = res.views.at(deletedViewIndex - 1).ReportID;
        }
        return [
          actions.deleteViewSuccess({ viewId: res.viewId }),
          actions.selectView({ viewID: nextViewId }),
        ];
      }),
      catchSwitchMapError((err) => {
        return of(actions.deleteViewFailure());
      })
    )
  );

  //checks whether or not we need to call updateChartOrTableData that
  //will dispatch the updateSelectedChartOrTableData$ Effect
  checkUpdateChartOrTable$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateSelectedWidget),
      withLatestFrom(
        this.store.select(selectSelectedElement),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, selectedElement, timeSelection]) => {
        if (
          (selectedElement.Kind === 'Chart' ||
            selectedElement.Kind === 'Table') &&
          action.updates.PDTrendID
        ) {
          return [actions.updateChartOrTableData()];
        } else {
          return [];
        }
      })
    )
  );

  //Makes 2 HTTP GET Requests first to get the trend info second to get the trend measurements
  //So we can update a charts trend/measurements
  updateSelectedChartOrTableData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        actions.createChartSuccess,
        actions.createTableSuccess,
        actions.updateChartOrTableData
      ),
      withLatestFrom(
        this.store.select(selectSelectedElement),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, selectedElement, timeSelection]) => {
        const widget = selectedElement as IWidgetChart | IWidgetTable;
        //make PDTrends request to get updated trend
        return this.processDataService
          .getViewExplorerTrend(
            widget,
            widget.AssetID,
            timeSelection.startDate,
            timeSelection.endDate
          )
          .pipe(
            map((res) => {
              return { res, timeSelection };
            })
          );
      }),
      switchMap(({ res, timeSelection }) => {
        //make TagsDataFiltered Request to get updated data
        return this.processDataService
          .getTagsDataFiltered(
            res.trend.trendDefinition,
            timeSelection.startDate,
            timeSelection.endDate,
            0,
            '',
            String(res.widget.AssetID)
          )
          .pipe(
            map((measurements) => {
              return {
                widget: res.widget,
                trend: { ...res.trend, measurements } as IProcessedTrend,
              };
            })
          );
      }),
      switchMap(({ widget, trend }) => {
        //update the widget with new trend/measurements
        return [
          actions.updateWidgetById({
            widgetId: widget.WidgetID,
            sectionId: widget.ReportSectionID,
            updates: {
              trend,
              loading: false,
            },
          }),
        ];
      }),
      catchSwitchMapError((err) => {
        return of(actions.updateChartOrTableDataFailure(err));
      })
    )
  );

  getChartTrendList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getChartSelectList),
      switchMap((action) => {
        return this.processDataService.getTrends(action.assetGuid);
      }),
      switchMap((res) => {
        return [actions.getChartSelectListSuccess({ trends: res })];
      })
    )
  );

  //checks whether or not we need to dispatch updateGaugeOrKPIData that
  //will cause the updateSelectedGaugeOrKPIData$ Effect
  checkUpdateGaugeOrKPI$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateSelectedWidget),
      withLatestFrom(
        this.store.select(selectSelectedElement),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, selectedElement, timeSelection]) => {
        //only dispatch update if a gauge/kpi summaryID is changed
        if (
          (selectedElement.Kind === 'Gauge' ||
            selectedElement.Kind === 'KPI') &&
          (action.updates.SummaryID || action.updates.AssetVariableTypeTagMapID)
        ) {
          return [actions.updateGaugeOrKPIData()];
        } else {
          return [];
        }
      })
    )
  );

  //Makes a HTTP request to get tag data and value for the selected Gauge or KPI widget
  //Runs when a new Gauge/KPI is created or A Gauge/KPI has its SummaryID or AssetVariableTypeTagMapID changed
  updateSelectedGaugeOrKPIData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        actions.createGaugeSuccess,
        actions.createKPISuccess,
        actions.updateGaugeOrKPIData
      ),
      withLatestFrom(
        this.store.select(selectSelectedElement),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, selectedElement, timeSelection]) => {
        const gauge = selectedElement as IWidgetGauge | IWidgetKPI;
        return this.processDataService
          .getGaugeData(
            gauge.AssetVariableTypeTagMapID,
            timeSelection.startDate,
            timeSelection.endDate,
            gauge.SummaryID
          )
          .pipe(
            map((res) => {
              return {
                widgetId: gauge.WidgetID,
                sectionId: gauge.ReportSectionID,
                body: res,
              };
            })
          );
      }),
      switchMap((res) => {
        // using updateWidget by Id; we can't depend on the selected widget still being the widget we want to update
        if (res && res.body && res.widgetId) {
          return [
            actions.updateWidgetById({
              widgetId: res.widgetId,
              sectionId: res.sectionId,
              updates: {
                value: res.body.value,
                units: res.body.units,
                TagName: res.body.tagName,
                TagDesc: res.body.tagDesc,
                TagAssetId: res.body.tagAssetId,
              },
            }),
          ];
        }
        return [];
      }),
      catchSwitchMapError((err) => {
        return of(actions.updateGaugeOrKPIDataFailure(err));
      })
    )
  );

  navigateToDataExplorer$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.navigateToDataExplorer),
        withLatestFrom(this.store.select(selectTimeSelection)),
        tap(([action, time]) => {
          const dataExplorerUrl = `data-explorer?id=${action.assetId}&trend=${
            action.trendId
          }&start=${time.startDate.getTime().toString()}&end=${time.endDate
            .getTime()
            .toString()}`;
          let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
          }

          window.open(url, '_blank').focus();
        })
      ),
    { dispatch: false }
  );

  editViewToAsset$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.editViewToAsset),
        tap((payload) => {
          let url =
            this.appConfig.baseSiteURL +
            '/ViewExplorer/Index.html#!/?key=' +
            payload.key;

          if (payload.view) {
            url += '&view=' + payload.view;
          }

          window.open(url, '_blank').focus();
        })
      ),
    { dispatch: false }
  );

  initializeViewExplorer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.viewExplorerIntialize),
      // https://ngrx.io/guide/effects#incorporating-state
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.select(selectSelectedNode),
            this.store.select(assetState)
          )
        )
      ),
      map(([action, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return {
          asset: action.asset || appAsset?.toString() || newAsset,
          start: action.startDate,
          end: action.endDate,
          reportId: action.reportId,
        };
      }),
      switchMap((action) => [
        actions.initializeTimeSlider({
          startDate: action.start,
          endDate: action.end,
        }),
        actions.permissionsRequest(),
        actions.treeStateChange(selectAsset(action.asset, false)),
        actions.initializeRequestedView({
          viewRequestedInRoute: action.reportId,
        }),
        NavActions.taskCenterLoad({
          taskCenterID: 'viewExplorer',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: true,
          timeSliderOpened: true,
          asset: action.asset,
        }),
      ])
    )
  );

  selectAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectAsset),
      switchMap((action) => {
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          actions.updateRoute(),
        ];
      })
    )
  );

  updateRoute$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateRoute),
      withLatestFrom(
        this.store.select(selectSelectedNode),
        this.store.select(selectTimeSelection),
        this.store.select(selectedViewID)
      ),
      switchMap(([, id, time, viewID]) => {
        const start = time?.startDate?.getTime().toString();
        const end = time?.endDate?.getTime().toString();

        this.viewExplorerService.updateRouteWithoutReloading(
          id,
          start,
          end,
          viewID
        );
        return [
          NavActions.updateNavigationItems({
            urlParams: { id, start, end, reportId: viewID },
          }),
        ];
      })
    )
  );

  workerReDispatcher$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.workerSuccess),
      // ngrx docs suggest to use a flattening operator when using withLatestFrom
      // https://ngrx.io/guide/effects.  It is at the very bottom of the page.

      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(this.store.select(selectNextWidgetToPull))
        )
      ),
      filter(([, nextWidget]) => nextWidget !== null),
      map(([action, widget]) =>
        actions.workerRedispatch({
          workerNumber: action.workerNumber,
          widget,
        })
      )
    )
  );

  // There is a constant "numOfWorkers" in the view explorer helper that we need to change
  // if we change the number of workers here.
  initiateWorkerNumber1$ = workerBeginFunction(
    this.actions$,
    this.store,
    this.viewExplorerModelService,
    1
  );

  // There is a constant "numOfWorkers" in the view explorer helper that we need to change
  // if we change the number of workers here.
  initiateWorkerNumber2$ = workerBeginFunction(
    this.actions$,
    this.store,
    this.viewExplorerModelService,
    2
  );

  // There is a constant "numOfWorkers" in the view explorer helper that we need to change
  // if we change the number of workers here.
  initiateWorkerNumber3$ = workerBeginFunction(
    this.actions$,
    this.store,
    this.viewExplorerModelService,
    3
  );

  // There is a constant "numOfWorkers" in the view explorer helper that we need to change
  // if we change the number of workers here.
  initiateWorkerNumber4$ = workerBeginFunction(
    this.actions$,
    this.store,
    this.viewExplorerModelService,
    4
  );

  // There is a constant "numOfWorkers" in the view explorer helper that we need to change
  // if we change the number of workers here.
  initiateWorkerNumber5$ = workerBeginFunction(
    this.actions$,
    this.store,
    this.viewExplorerModelService,
    5
  );

  getPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) => actions.permissionsRequestSuccess(permissions)),
          catchError((error) => of(actions.permissionsRequestFailure(error)))
        )
      )
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.treeStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.treeStateChange(n)),
            catchError((error) =>
              of(actions.assetTreeDataRequestFailure(error))
            )
          )
      )
    )
  );

  // this appears to run when you select a different asset.
  getViews$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getViews),
      withLatestFrom(
        this.store.pipe(select(selectSelectedNodeID)),
        this.store.pipe(select(selectViewRequestedInRoute))
      ),
      map(([action, node, requestedView]) => {
        return {
          asset: action.asset,
          node: isNil(action.asset) ? node : null,
          requestedView,
        };
      }),
      switchMap((vals) =>
        this.businessIntelligenceFrameworkService
          .getViews(vals.asset, vals.node)
          .pipe(
            map((n) =>
              actions.getViewsSuccess({
                views: n,
                viewRequestedInRoute: vals.requestedView,
              })
            ),
            catchError((error) => of(actions.getViewsFailure({ error })))
          )
      ),
      switchMap((result) => {
        const resultAsAny = result as any;
        let selectedViewID = null;
        if (resultAsAny && resultAsAny.views && resultAsAny.views.length > 0) {
          selectedViewID = resultAsAny.views[0].ReportID;
          if (resultAsAny.viewRequestedInRoute) {
            if (
              resultAsAny.views
                .map((view: IReport) => view.ReportID)
                .includes(resultAsAny.viewRequestedInRoute)
            ) {
              selectedViewID = resultAsAny.viewRequestedInRoute;
            } else {
              this.toastService.openSnackBar(
                'Report ID not found for this asset',
                'warning'
              );
            }
          }
        }
        return [
          result,
          actions.selectView({
            viewID: selectedViewID,
          }),
        ];
      })
    )
  );

  //Save the currently selected Report/View
  saveView$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveView),
      withLatestFrom(this.store.pipe(select(selectedView))),
      switchMap(([, view]) => {
        //make http post request
        return this.biFrameworkService.saveReport(view);
      }),
      switchMap((res) => {
        //do something on success
        this.toastService.openSnackBar('View Saved', 'success');
        return [actions.saveViewSuccess()];
      }),
      catchError((err) => {
        this.toastService.openSnackBar('An error has occured', 'error');
        return of(actions.saveViewFailure());
      })
    )
  );

  saveTextbox$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveTextboxDirectly),
      switchMap((action) => {
        return this.biFrameworkService.saveTextbox(
          action.content,
          action.widgetId
        );
      }),
      switchMap((res) => {
        return [actions.saveTextboxDirectlySuccess()];
      })
    )
  );

  //essential makes a HTTP GET requests to get a guid for the new textbox
  //to work with the existing API
  createNewTextboxWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createTextbox),
      switchMap(() => {
        return this.biFrameworkService.createNewTextboxWidget();
      }),
      switchMap((res) => {
        return [actions.createTextboxSuccess({ widget: res })];
      })
    )
  );

  //essential makes a HTTP GET requests to get a guid for the new widget
  //to work with the existing API
  createNewGaugeWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createGauge),
      switchMap((action) => {
        return this.biFrameworkService.createNewGaugeWidget().pipe(
          map((res) => {
            return {
              ...res,
              AssetVariableTypeTagMapID: action.AssetVariableTypeTagMapID,
              Title: action.Title,
            };
          })
        );
      }),
      switchMap((widget) => {
        return [actions.createGaugeSuccess({ widget })];
      })
    )
  );

  //essential makes a HTTP GET requests to get a guid for the new widget
  //to work with the existing API
  createNewKPIWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createKPI),
      switchMap((action) => {
        return this.biFrameworkService.createNewKPIWidget().pipe(
          map((res) => {
            return {
              ...res,
              AssetVariableTypeTagMapID: action.AssetVariableTypeTagMapID,
              Title: action.Title,
            };
          })
        );
      }),
      switchMap((widget) => {
        return [actions.createKPISuccess({ widget })];
      })
    )
  );

  //essential makes a HTTP GET requests to get a guid for the new widget
  //to work with the existing API
  createNewChartWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createChart),
      switchMap((action) => {
        return this.biFrameworkService.createNewChartWidget().pipe(
          map((res) => {
            return {
              ...res,
              PDTrendID: action.PDTrendID,
              AssetID: action.AssetID,
            };
          })
        );
      }),
      switchMap((res: IWidgetChart) => {
        return [actions.createChartSuccess({ widget: res })];
      })
    )
  );

  //essential makes a HTTP GET requests to get a guid for the new widget
  //to work with the existing API
  createNewTableWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createTable),
      switchMap((action) => {
        return this.biFrameworkService.createNewTableWidget().pipe(
          map((res) => {
            return {
              ...res,
              PDTrendID: action.PDTrendID,
              AssetID: action.AssetID,
            };
          })
        );
      }),
      switchMap((res: IWidgetTable) => {
        return [actions.createTableSuccess({ widget: res })];
      })
    )
  );

  // this is called when the user selects a new view from the dropdown.
  // it also gets called when the user selects a new asset because the default (0th) option
  // in the dropdown gets selected, and this runs whenever a new item in the dropdown is selected.
  // it is an effect beccause of the getReport method, which is an API call.
  // then it has a success or failure.  The success calls a new
  // effect, which is selectViewSuccess
  selectView$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectView),
      filter((n) => !!n.viewID),
      withLatestFrom(this.store.pipe(select(selectViews))),
      switchMap(([action, views]) => {
        const view = views?.find((n) => n.ReportID === action.viewID);
        if (view) {
          // if (view && !view.full) {
          return this.biFrameworkService.getReport(action.viewID).pipe(
            map((view) => {
              const newView = { ...view };
              newView.Kind = 'View';
              newView.Sections = view.Sections.map((section) => {
                const newSection = { ...section };
                newSection.Kind = 'Section';
                return newSection;
              });
              return newView;
            }),
            map((view) => actions.selectViewSuccess({ view })),
            catchError((error) =>
              of(actions.selectViewFailure({ error, viewID: action.viewID }))
            )
          );
        } else {
          return [
            actions.updateRoute(),
            actions.setSelectedElement({ element: view }),
          ];
        }
      })
    )
  );

  // this effect is triggered by the user selecting something new from the dropdown,
  // and if the viewExplorerModel getReport is successful, this is called.
  // if this is successful, it calls the populateTrendWidget
  selectViewSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectViewSuccess),
      switchMap((action) => {
        const widgets: (IWidgetChart | IWidgetTable)[] = [];
        const view: IReport = action?.view;
        for (const section of view?.Sections ?? []) {
          for (const widget of section?.Widgets ?? []) {
            if (widget?.Kind === 'Chart' || widget?.Kind === 'Table') {
              widgets.push(widget as any);
            }
          }
        }

        return [
          widgets.length > 0
            ? actions.populateTrendWidget({ widgets })
            : actions.populateTrendWidgetsDone(),
          actions.updateRoute(),
        ];
      })
    )
  );

  editChart$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.editChart),
        concatMap((action) =>
          of(action).pipe(
            withLatestFrom(this.store.pipe(select(selectTimeSelection)))
          )
        ),
        map(([action, time]) => {
          const widget = action.widget as IWidgetChart;
          const asset = isNil(widget?.AssetID) ? null : String(widget?.AssetID);
          const trendID = isNil(widget?.PDTrendID)
            ? null
            : String(widget?.PDTrendID);
          return {
            asset,
            time,
            trendID,
          };
        }),
        filter((vals) => !isNil(vals.asset) && !isNil(vals.trendID)),
        tap((vals) => {
          const dataExplorerUrl = `data-explorer?id=${vals.asset}&trend=${
            vals.trendID
          }&start=${vals.time.startDate
            .getTime()
            .toString()}&end=${vals.time.endDate.getTime().toString()}`;
          let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
          }

          window.open(url, '_blank').focus();
        })
      ),
    { dispatch: false }
  );

  editView$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.editView),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.select(selectSelectedAssetKey),
            this.store.select(selectedViewID)
          )
        )
      ),
      filter(([action, key, view]) => !isNil(key)),
      switchMap(([action, key, view]) => {
        return [actions.editViewToAsset({ key, view })];
      })
    )
  );

  // this has a getTrends method that is an API call. This is the only place getTrends is called.
  // the getTrends method calls the getTrend method, and the getTrend method is only used there.
  // the getTrend method makes a call to the api and uses "PDTrend" as part of the URL.
  // if the call to getTrends is successful, it calls the populateTrendWidgetSuccess action.
  populateTrendWidget$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.populateTrendWidget),
      withLatestFrom(
        this.store.select(selectSelectedAssetID),
        this.store.select(selectTimeSelection)
      ),
      switchMap(([action, asset, time]) =>
        this.viewExplorerModelService
          .getTrends(action.widgets, asset, time.startDate, time.endDate)
          .pipe(
            map((result) => actions.populateTrendWidgetSuccess(result)),
            catchError((error) =>
              of(actions.populateTrendWidgetFailure({ error }))
            )
          )
      )
    )
  );

  // This is called when the user moves the time slider
  // OR This is called when all of the trend widgets have gotten their trend data.
  newDataPull$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.timeRangeChanged, actions.populateTrendWidgetsDone),
      debounceTime(1000),
      // ngrx docs suggest to use a flattening operator when using withLatestFrom
      // https://ngrx.io/guide/effects.  It is at the very bottom of the page.
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(this.store.select(selectFirstWidgetsToPull))
        )
      ),
      map(([unusedAction, widgetList]) => widgetList),
      switchMap((widgetList) => {
        const result: Action[] = [];
        for (let i = 0; i < numOfWorkers; i++) {
          if (widgetList.length > 0) {
            result.push(
              actions.workerBegin({
                workerNumber: i + 1,
                widget: widgetList.shift(),
              })
            );
          }
        }
        return result;
      })
    )
  );

  reflow$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.reflow, actions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 25);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 50);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 75);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 100);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 150);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 225);
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 400);
        })
      ),
    { dispatch: false }
  );

  reflowOnEditMode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setIsEdit),
      switchMap(() => {
        return [actions.reflow()];
      })
    )
  );

  //reflow when section height changes to update widget displays
  reflowOnSectionHeightChange$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.updateSectionInfo),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 25);
        })
      ),
    { dispatch: false }
  );

  // This effect is called each time a trend widget is successfully populated.
  // When it receives an action, it queries the store for the number of remaining widgets to populate.
  // If there are none remaining, then dispatch the action that we're done populating the trend widgets.
  checkIfWidgetsAreDone$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.populateTrendWidgetSuccess),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.pipe(select(selectNumberOfTrendWidgetsLeftToPopulate))
          )
        )
      ),
      map(([unusedAction, numberOfWidgetsLeft]) => numberOfWidgetsLeft),
      filter((numberOfWidgetsLeft) => numberOfWidgetsLeft === 0),
      map((zero) => actions.populateTrendWidgetsDone())
    )
  );

  copyLink$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.copyLink),
        switchMap((payload) =>
          copyToClipboard(payload.link).pipe(
            map(
              (success: boolean) => {
                if (success) {
                  this.toastService.openSnackBar('Link Copied!', 'success');
                } else {
                  this.toastService.openSnackBar('Error Copying', 'error');
                }
              },
              catchError(() => {
                this.toastService.openSnackBar('Error Copying', 'error');
                return of();
              })
            )
          )
        )
      ),
    { dispatch: false }
  );
}
