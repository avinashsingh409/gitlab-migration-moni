import { TestBed } from '@angular/core/testing';
import { Store, Action } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Subject, Observable, of } from 'rxjs';
import { hot, cold } from 'jest-marbles';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { NavActions } from '@atonix/atx-navigation';
import {
  ModelService as AssetTreeModel,
  ITreeConfiguration,
  ITreeStateChange,
  assetTreeAdapter,
} from '@atonix/atx-asset-tree';

import {
  initialViewExplorerState,
  adapter,
  IViewExplorerState,
} from '../state/view-explorer-state';
import { ViewExplorerEffects } from './view-explorer.effects';
import { ModelService as ViewExplorerModelService } from '../../service/model.service';
import { ViewExplorerService } from '../../service/view-explorer.service';
import * as actions from '../actions/view-explorer.actions';

import {
  GroupedSeriesType,
  IProcessedTrend,
  ITreePermissions,
  IWidget,
  IWidgetChart,
  IWidgetTable,
} from '@atonix/atx-core';
import {
  AuthorizationFrameworkService,
  BusinessIntelligenceFrameworkService,
  IReport,
  IReportSection,
} from '@atonix/shared/api';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { ToastService } from '@atonix/shared/utils';

describe('ViewExplorerEffects', () => {
  let actions$: Observable<Action>;
  let store: MockStore<any>;
  let effects: ViewExplorerEffects;

  let mockViewExplorerModelService: ViewExplorerModelService;
  let mockViewExplorerService: ViewExplorerService;
  let mockAssetTreeModel: AssetTreeModel;
  let mockAuthFrameworkService: AuthorizationFrameworkService;
  let mockBIFrameworkService: BusinessIntelligenceFrameworkService;
  let mockToastService: ToastService;

  const error1: Error = { name: 'name', message: 'something' };

  const myTreeConfiguration: ITreeConfiguration = {
    showTreeSelector: true,
    trees: [],
    selectedTree: '1',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    nodes: [],
    pin: false,
    loadingAssets: false,
    loadingTree: false,
    canDrag: false,
    dropTargets: null,
    canView: true,
    canAdd: true,
    canEdit: true,
    canDelete: true,
    collapseOthers: false,
    hideConfigureButton: false,
  };

  const myReferenceReport: IReport = {
    ReportID: 1,
    ReportDesc: 'desc',
    ReportAbbrev: 'abbrev',
    DisplayOrder: 1,
    AssetID: 1,
    Asset: null,
    Title: 'title',
    Subtitle: 'subtitle',
    CreatedBy: 'test',
    ChangedBy: 'test',
    CreateDate: '',
    ChangeDate: '',
    Sections: [],
    NodeID: 'node',
    Kind: 'View',
  };

  const myReferenceSection: IReportSection = {
    ReportSectionID: 1,
    ReportID: 1,
    Title: 'title',
    Subtitle: 'subtitle',
    Height: '1',
    DisplayOrder: 1,
    Color: 'red',
    Widgets: [],
    sectionHeight: '1',
    Kind: 'Section',
  };

  const myReferenceWidget: IWidget = {
    ReportSectionMapID: 1,
    ReportSectionID: 1,
    WidgetID: 'widgetid',
    Title: 'title',
    Subtitle: 'subtitle',
    Width: 10,
    Kind: 'Chart',
    AssetID: 1,
    DisplayOrder: 1,
  };

  beforeEach(() => {
    const initialState: any = { viewExplorer: initialViewExplorerState };
    actions$ = new Subject<Action>();
    mockAssetTreeModel = createMockWithValues(AssetTreeModel, {
      getAssetData: jest.fn(),
      getAssetTreeData: jest.fn(),
    });

    mockBIFrameworkService = createMockWithValues(
      BusinessIntelligenceFrameworkService,
      {
        getViews: jest.fn(),
        getReport: jest.fn(),
      }
    );
    mockViewExplorerModelService = createMockWithValues(
      ViewExplorerModelService,
      {
        getTrends: jest.fn(),
        getWidgetData: jest.fn(),
      }
    );
    mockViewExplorerService = createMockWithValues(ViewExplorerService, {
      updateRouteWithoutReloading: jest.fn(),
    });
    mockAuthFrameworkService = createMockWithValues(
      AuthorizationFrameworkService,
      {
        getPermissions: jest.fn(),
      }
    );

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        provideMockActions(() => actions$),
        provideMockStore<any>({ initialState }),
        { provide: AssetTreeModel, useValue: mockAssetTreeModel },
        {
          provide: ViewExplorerModelService,
          useValue: mockViewExplorerModelService,
        },
        {
          provide: BusinessIntelligenceFrameworkService,
          useValue: mockBIFrameworkService,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: AuthorizationFrameworkService,
          useValue: mockAuthFrameworkService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ViewExplorerService, useValue: mockViewExplorerService },
        ViewExplorerEffects,
        { provide: ToastService, userValue: mockToastService },
      ],
    });

    store = TestBed.inject<any>(Store);
    effects = TestBed.inject<ViewExplorerEffects>(ViewExplorerEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should select asset', () => {
    actions$ = hot('a', {
      a: actions.selectAsset({ asset: 'firstasset' }),
    });

    const expected = hot('(ab)', {
      a: NavActions.setApplicationAsset({ asset: 'firstasset' }),
      b: actions.updateRoute(),
    });
    expect(effects.selectAsset$).toBeObservable(expected);
  });

  it('should redispatch a worker', () => {
    const targetWidget = { ...myReferenceWidget, WidgetID: 'widget3' };

    const adp = adapter;
    let vState: IViewExplorerState = {
      ...initialViewExplorerState,
      listOfInitiatedWidgetPulls: ['widget1', 'widget2'],
      selectedView: 1,
    };
    vState = adp.setAll(
      [
        {
          ...myReferenceReport,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                targetWidget,
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      vState
    );

    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a', {
      a: actions.workerSuccess({
        workerNumber: 1,
        widget: null,
      }),
    });

    const expected = hot('a', {
      a: actions.workerRedispatch({
        workerNumber: 1,
        widget: targetWidget,
      }),
    });

    expect(effects.workerReDispatcher$).toBeObservable(expected);
  });

  it('should not redispatch a worker when widgets are done', () => {
    const adp = adapter;
    let vState: IViewExplorerState = {
      ...initialViewExplorerState,
      listOfInitiatedWidgetPulls: [
        'widget1',
        'widget2',
        'widget3',
        'widgetA',
        'widgetB',
        'widgetC',
      ],
      selectedView: myReferenceReport.ReportID,
    };
    vState = adp.setAll(
      [
        {
          ...myReferenceReport,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      vState
    );

    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a', {
      a: actions.workerSuccess({
        workerNumber: 1,
        widget: null,
      }),
    });

    const expected = hot('', {});

    expect(effects.workerReDispatcher$).toBeObservable(expected);
  });

  it('should execute worker 1', () => {
    const targetWidget: IWidget = { ...myReferenceWidget, WidgetID: 'widget3' };

    const adp = adapter;
    let vState: IViewExplorerState = {
      ...initialViewExplorerState,
      listOfInitiatedWidgetPulls: [
        'widget1',
        'widget2',
        'widget3',
        'widgetA',
        'widgetB',
        'widgetC',
      ],
      selectedView: 1,
    };
    vState = adp.setAll(
      [
        {
          ...myReferenceReport,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      vState
    );

    store.setState({
      viewExplorer: vState,
    });

    mockViewExplorerModelService.getWidgetData = jest
      .fn()
      .mockReturnValue(of(targetWidget));

    actions$ = hot('a-b', {
      a: actions.workerBegin({
        workerNumber: 1,
        widget: targetWidget,
      }),
      b: actions.workerRedispatch({
        workerNumber: 1,
        widget: targetWidget,
      }),
    });

    const expected = hot('a-b', {
      a: actions.workerSuccess({
        workerNumber: 1,
        widget: targetWidget,
      }),
      b: actions.workerSuccess({
        workerNumber: 1,
        widget: targetWidget,
      }),
    });

    expect(effects.initiateWorkerNumber1$).toBeObservable(expected);
  });

  it('should recover worker 1 from error', () => {
    const targetWidget: IWidget = { ...myReferenceWidget, WidgetID: 'widget3' };

    const adp = adapter;
    let vState: IViewExplorerState = {
      ...initialViewExplorerState,
      listOfInitiatedWidgetPulls: [
        'widget1',
        'widget2',
        'widget3',
        'widgetA',
        'widgetB',
        'widgetC',
      ],
      selectedView: 1,
    };
    vState = adp.setAll(
      [
        {
          ...myReferenceReport,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      vState
    );

    store.setState({
      viewExplorer: vState,
    });

    mockViewExplorerModelService.getWidgetData = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a', { a: targetWidget }));

    actions$ = hot('a-b', {
      a: actions.workerBegin({
        workerNumber: 1,
        widget: targetWidget,
      }),
      b: actions.workerRedispatch({
        workerNumber: 1,
        widget: targetWidget,
      }),
    });

    const expected = hot('a-b', {
      a: actions.workerFailure({
        error: error1,
        workerNumber: 1,
        widget: targetWidget,
      }),
      b: actions.workerSuccess({
        workerNumber: 1,
        widget: targetWidget,
      }),
    });

    expect(effects.initiateWorkerNumber1$).toBeObservable(expected);
  });

  it('should get permissions', () => {
    const result: ITreePermissions = {
      canView: true,
      canAdd: true,
      canEdit: true,
      canDelete: true,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValue(cold('a|', { a: result }));

    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestSuccess(result),
      b: actions.permissionsRequestSuccess(result),
    });

    expect(effects.getPermissions$).toBeObservable(expected);
  });

  it('should get permissions with error', () => {
    const result: ITreePermissions = {
      canView: true,
      canAdd: true,
      canEdit: true,
      canDelete: true,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a|', { a: result }));

    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestFailure(error1),
      b: actions.permissionsRequestSuccess(result),
    });

    expect(effects.getPermissions$).toBeObservable(expected);
  });

  it('should get asset tree data', () => {
    const result: ITreeStateChange = {
      event: 'IconClicked',
    };
    const vState = initialViewExplorerState;
    store.setState({
      viewExplorer: vState,
    });
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValue(cold('a|', { a: result }));
    actions$ = hot('a-b', {
      a: actions.treeStateChange({ event: 'PinToggle' }),
      b: actions.treeStateChange({ event: 'ReloadAssetTree' }),
    });
    const expected = hot('a-b', {
      a: actions.treeStateChange(result),
      b: actions.treeStateChange(result),
    });

    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get asset tree data with error', () => {
    const result: ITreeStateChange = {
      event: 'IconClicked',
    };
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a|', { a: result }));

    actions$ = hot('a-b', {
      a: actions.treeStateChange({ event: 'PinToggle' }),
      b: actions.treeStateChange({ event: 'ReloadAssetTree' }),
    });

    const expected = hot('a-b', {
      a: actions.assetTreeDataRequestFailure(error1),
      b: actions.treeStateChange(result),
    });

    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get views', () => {
    const result: IReport[] = [
      { ...myReferenceReport, ReportID: 1 },
      { ...myReferenceReport, ReportID: 2 },
    ];

    mockBIFrameworkService.getViews = jest
      .fn()
      .mockReturnValue(cold('a', { a: result }));

    actions$ = hot('a----b', {
      a: actions.getViews({ asset: 'asset1' }),
      b: actions.getViews({ asset: 'asset2' }),
    });

    const expected = hot('(ab)-(cd)', {
      a: actions.getViewsSuccess({
        views: result,
        viewRequestedInRoute: undefined,
      }),
      b: actions.selectView({ viewID: 1 }),
      c: actions.getViewsSuccess({
        views: result,
        viewRequestedInRoute: undefined,
      }),
      d: actions.selectView({ viewID: 1 }),
    });

    expect(effects.getViews$).toBeObservable(expected);
  });

  it('should get views with errors', () => {
    const result: IReport[] = [
      { ...myReferenceReport, ReportID: 1 },
      { ...myReferenceReport, ReportID: 2 },
    ];
    mockBIFrameworkService.getViews = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a', { a: result }));

    actions$ = hot('a----b', {
      a: actions.getViews({ asset: 'asset1' }),
      b: actions.getViews({ asset: 'asset2' }),
    });

    const expected = hot('(ab)-(cd)', {
      a: actions.getViewsFailure({ error: error1 }),
      b: actions.selectView({ viewID: null }),
      c: actions.getViewsSuccess({
        views: result,
        viewRequestedInRoute: undefined,
      }),
      d: actions.selectView({ viewID: 1 }),
    });

    expect(effects.getViews$).toBeObservable(expected);
  });

  it('should select view', () => {
    const result: IReport = { ...myReferenceReport, ReportID: 3 };
    mockBIFrameworkService.getReport = jest
      .fn()
      .mockReturnValue(cold('a', { a: result }));

    const vState = adapter.setAll(
      [
        {
          ...myReferenceReport,
          full: false,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      initialViewExplorerState
    );
    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a-b', {
      a: actions.selectView({ viewID: 3 }),
      b: actions.selectView({ viewID: 3 }),
    });

    const expected = hot('a-b', {
      a: actions.selectViewSuccess({ view: result }),
      b: actions.selectViewSuccess({ view: result }),
    });

    expect(effects.selectView$).toBeObservable(expected);
  });

  it('should select view with errors', () => {
    const result: IReport = { ...myReferenceReport, ReportID: 3 };

    const vState = adapter.setAll(
      [
        {
          ...myReferenceReport,
          full: false,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      initialViewExplorerState
    );
    store.setState({
      viewExplorer: vState,
    });
    mockBIFrameworkService.getReport = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a', { a: result }));

    actions$ = hot('a-b', {
      a: actions.selectView({ viewID: 3 }),
      b: actions.selectView({ viewID: 3 }),
    });

    const expected = hot('a-b', {
      a: actions.selectViewFailure({ error: error1, viewID: 3 }),
      b: actions.selectViewSuccess({ view: result }),
    });

    expect(effects.selectView$).toBeObservable(expected);
  });

  it('should select view with view id filter', () => {
    const result: IReport = { ...myReferenceReport, ReportID: 3 };

    const vState = adapter.setAll(
      [
        {
          ...myReferenceReport,
          full: false,
          ReportID: 1,
          Sections: [
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widget1' },
                { ...myReferenceWidget, WidgetID: 'widget2' },
                { ...myReferenceWidget, WidgetID: 'widget3' },
              ],
            },
            {
              ...myReferenceSection,
              Widgets: [
                { ...myReferenceWidget, WidgetID: 'widgetA' },
                { ...myReferenceWidget, WidgetID: 'widgetB' },
                { ...myReferenceWidget, WidgetID: 'widgetC' },
              ],
            },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 2,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
        {
          ...myReferenceReport,
          full: false,
          ReportID: 3,
          Sections: [
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
            { ...myReferenceSection, Widgets: [] },
          ],
        },
      ],
      initialViewExplorerState
    );
    store.setState({
      viewExplorer: vState,
    });
    mockBIFrameworkService.getReport = jest
      .fn()
      .mockReturnValue(cold('a', { a: result }));
    actions$ = hot('a-b', {
      a: actions.selectView({ viewID: undefined }),
      b: actions.selectView({ viewID: 3 }),
    });

    const expected = hot('--b', {
      b: actions.selectViewSuccess({ view: result }),
    });

    expect(effects.selectView$).toBeObservable(expected);
  });

  it('should populate widgets on view load', () => {
    const widgets1: IWidget[] = [
      { ...myReferenceWidget, Kind: 'Chart' },
      { ...myReferenceWidget, Kind: 'Table' },
    ];
    const widgets2: IWidget[] = [
      { ...myReferenceWidget, Kind: 'Gauge' },
      { ...myReferenceWidget, Kind: 'KPI' },
    ];
    const report1: IReport = {
      ...myReferenceReport,
      Sections: [{ ...myReferenceSection, Widgets: widgets1 }],
    };
    const report2: IReport = {
      ...myReferenceReport,
      Sections: [{ ...myReferenceSection, Widgets: widgets2 }],
    };

    actions$ = hot('a---b', {
      a: actions.selectViewSuccess({ view: report1 }),
      b: actions.selectViewSuccess({ view: report2 }),
    });

    const expected = hot('(ab)(cd)', {
      a: actions.populateTrendWidget({ widgets: widgets1 as any }),
      b: actions.updateRoute(),
      c: actions.populateTrendWidgetsDone(),
      d: actions.updateRoute(),
      e: actions.setSelectedElement({ element: report1 }),
      f: actions.setSelectedElement({ element: report2 }),
    });

    expect(effects.selectViewSuccess$).toBeObservable(expected);
  });

  it('should edit the chart with filters', () => {
    const startTime = new Date(2020, 1, 1, 1, 1, 1, 1);
    const endTime = new Date(2020, 2, 2, 2, 2, 2, 2);
    const w1: IWidgetChart = {
      ...myReferenceWidget,
      Kind: 'Chart',
      PDTrendID: null,
      AssetID: 5,
      trend: null,
    };
    const w2: IWidgetChart = {
      ...myReferenceWidget,
      Kind: 'Chart',
      PDTrendID: 14,
      AssetID: null,
      trend: null,
    };

    const vState: IViewExplorerState = {
      ...initialViewExplorerState,
      timeSliderState: {
        dateIndicator: 'Range',
        showCalendarDialog: false,
        startDate: startTime,
        endDate: endTime,
        asOfDate: null,
        isLive: false,
      },
    };
    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a-b', {
      a: actions.editChart({ widget: w1 }),
      b: actions.editChart({ widget: w2 }),
    });

    const expected = hot('', {});

    expect(effects.editChart$).toBeObservable(expected);
  });

  it('should edit the view', () => {
    let treeNodes = {
      ...initialViewExplorerState.assetTreeState.treeNodes,
      rootAssets: ['uniquekey1'],
    };
    treeNodes = assetTreeAdapter.setAll(
      [
        {
          uniqueKey: 'uniquekey1',
          parentUniqueKey: null,
          nodeAbbrev: 'abbrev1',
          displayOrder: 1,
          level: 1,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
          data: {
            TreeId: 't1',
            ParentNodeId: 'p1',
            NodeId: 'n1',
            AssetGuid: 'a1',
          },
        },
      ],
      treeNodes
    );

    const vState: IViewExplorerState = {
      ...initialViewExplorerState,
      selectedView: 12,
      assetTreeState: {
        ...initialViewExplorerState.assetTreeState,
        treeNodes,
        treeConfiguration: {
          ...initialViewExplorerState.assetTreeState.treeConfiguration,
          nodes: [
            {
              uniqueKey: 'uniquekey1',
              parentUniqueKey: null,
              nodeAbbrev: 'abbrev1',
              displayOrder: 1,
              level: 1,
              selected: true,
              retrieved: true,
              symbol: 'nothing',
              data: {
                TreeId: 't1',
                ParentNodeId: 'p1',
                NodeId: 'n1',
                AssetGuid: 'a1',
              },
            },
          ],
        },
      },
    };
    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a-b', {
      a: actions.editView(),
      b: actions.editView(),
    });

    const expected = hot('a-b', {
      a: actions.editViewToAsset({ key: 't1~p1~n1~a1', view: 12 }),
      b: actions.editViewToAsset({ key: 't1~p1~n1~a1', view: 12 }),
    });

    expect(effects.editView$).toBeObservable(expected);
  });

  it('should edit the view with filters', () => {
    let treeNodes = {
      ...initialViewExplorerState.assetTreeState.treeNodes,
      rootAssets: ['uniquekey1'],
    };
    treeNodes = assetTreeAdapter.setAll(
      [
        {
          uniqueKey: 'uniquekey1',
          parentUniqueKey: null,
          nodeAbbrev: 'abbrev1',
          displayOrder: 1,
          level: 1,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
        },
      ],
      treeNodes
    );

    const vState: IViewExplorerState = {
      ...initialViewExplorerState,
      selectedView: 12,
      assetTreeState: {
        ...initialViewExplorerState.assetTreeState,
        treeNodes,
        treeConfiguration: {
          ...initialViewExplorerState.assetTreeState.treeConfiguration,
          nodes: [
            {
              uniqueKey: 'uniquekey1',
              parentUniqueKey: null,
              nodeAbbrev: 'abbrev1',
              displayOrder: 1,
              level: 1,
              selected: true,
              retrieved: true,
              symbol: 'nothing',
            },
          ],
        },
      },
    };
    store.setState({
      viewExplorer: vState,
    });

    actions$ = hot('a-b', {
      a: actions.editView(),
      b: actions.editView(),
    });

    const expected = hot('', {});

    expect(effects.editView$).toBeObservable(expected);
  });

  it('should populate trend widgets', () => {
    const startTime = new Date(2020, 1, 1, 1, 1, 1, 1);
    const endTime = new Date(2020, 2, 2, 2, 2, 2, 2);
    const trend1: IProcessedTrend = {
      id: '1',
      label: '1',
      trendDefinition: null,
      totalSeries: 1,
      groupedSeriesSubType: '',
      groupedSeriesType: GroupedSeriesType.NONE,
    };
    const widgets: (IWidgetChart | IWidgetTable)[] = [
      { ...myReferenceWidget, Kind: 'Chart', PDTrendID: 1, trend: trend1 },
      { ...myReferenceWidget, Kind: 'Table', PDTrendID: 2, trend: trend1 },
    ];

    store.setState({
      viewExplorer: {
        ...initialViewExplorerState,
        timeSliderState: {
          dateIndicator: 'Range',
          showCalendarDialog: false,
          startDate: startTime,
          endDate: endTime,
          asOfDate: null,
          isLive: false,
        },
      },
    });
    mockViewExplorerModelService.getTrends = jest.fn().mockReturnValue(
      cold('ab', {
        a: { widget: widgets[0], trend: trend1 },
        b: { widget: widgets[1], trend: trend1 },
      })
    );

    actions$ = hot('a---b', {
      a: actions.populateTrendWidget({ widgets }),
      b: actions.populateTrendWidget({ widgets }),
    });

    const expected = hot('ab--cd', {
      a: actions.populateTrendWidgetSuccess({
        widget: widgets[0],
        trend: trend1,
      }),
      b: actions.populateTrendWidgetSuccess({
        widget: widgets[1],
        trend: trend1,
      }),
      c: actions.populateTrendWidgetSuccess({
        widget: widgets[0],
        trend: trend1,
      }),
      d: actions.populateTrendWidgetSuccess({
        widget: widgets[1],
        trend: trend1,
      }),
    });
    expect(effects.populateTrendWidget$).toBeObservable(expected);
  });

  it('should populate trend widgets with errors', () => {
    const startTime = new Date(2020, 1, 1, 1, 1, 1, 1);
    const endTime = new Date(2020, 2, 2, 2, 2, 2, 2);
    const trend1: IProcessedTrend = {
      id: '1',
      label: '1',
      trendDefinition: null,
      totalSeries: 1,
      groupedSeriesType: GroupedSeriesType.NONE,
      groupedSeriesSubType: '',
    };
    const widgets: (IWidgetChart | IWidgetTable)[] = [
      { ...myReferenceWidget, Kind: 'Chart', PDTrendID: 1, trend: trend1 },
      { ...myReferenceWidget, Kind: 'Table', PDTrendID: 2, trend: trend1 },
    ];

    store.setState({
      viewExplorer: {
        ...initialViewExplorerState,
        timeSliderState: {
          dateIndicator: 'Range',
          showCalendarDialog: false,
          startDate: startTime,
          endDate: endTime,
          asOfDate: null,
          isLive: false,
        },
      },
    });
    mockViewExplorerModelService.getTrends = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(
        cold('a-b', {
          a: { widget: widgets[0], trend: trend1 },
          b: { widget: widgets[1], trend: trend1 },
        })
      );

    actions$ = hot('a-b', {
      a: actions.populateTrendWidget({ widgets }),
      b: actions.populateTrendWidget({ widgets }),
    });

    const expected = hot('a-c-d', {
      a: actions.populateTrendWidgetFailure({ error: error1 }),
      c: actions.populateTrendWidgetSuccess({
        widget: widgets[0],
        trend: trend1,
      }),
      d: actions.populateTrendWidgetSuccess({
        widget: widgets[1],
        trend: trend1,
      }),
    });
    expect(effects.populateTrendWidget$).toBeObservable(expected);
    // expect(mockViewExplorerModelService.getTrends).toHaveBeenCalledTimes(2);
  });

  it('should check if widgets are done', () => {
    actions$ = hot('a-b', {
      a: actions.populateTrendWidgetSuccess({ widget: null, trend: null }),
      b: actions.populateTrendWidgetSuccess({ widget: null, trend: null }),
    });

    const expected = hot('a-b', {
      a: actions.populateTrendWidgetsDone(),
      b: actions.populateTrendWidgetsDone(),
    });
    expect(effects.checkIfWidgetsAreDone$).toBeObservable(expected);
  });
});
