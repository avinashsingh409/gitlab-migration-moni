/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { ITreeState, ITreeStateChange } from '@atonix/atx-asset-tree';
import { Store, select } from '@ngrx/store';
import {
  secondaryAssetTreeStateChange,
  setSecondaryAssetTreeSetState,
} from '../actions/secondary-asset-tree.actions';
import * as selectors from '../selectors/view-explorer.selector';

@Injectable()
export class SecondaryAssetTreeFacade {
  constructor(private store: Store) {}

  secondaryAssetTreeConfiguration$ = this.store.pipe(
    select(selectors.selectSecondaryAssetTreeConfiguration)
  );
  selectedAsset$ = this.store.pipe(select(selectors.selectSelectedAssetID));
  selectedSecondaryAssetTreeNodeAssetId$ = this.store.pipe(
    select(selectors.selectSecondaryAssetTreeNodeAssetId)
  );

  //the selected node in the secondary tag select asset tree component
  selectedSecondaryAssetTreeNode$ = this.store.pipe(
    select(selectors.selectSecondaryAssetTreeNode)
  );

  secondaryAssetTreeState$ = this.store.pipe(
    select(selectors.selectSecondaryAssetTreeState)
  );

  setSecondaryAssetTreeSetState(treeState: ITreeState) {
    this.store.dispatch(setSecondaryAssetTreeSetState({ treeState }));
  }

  secondaryAssetTreeStateChange(change: ITreeStateChange) {
    this.store.dispatch(
      secondaryAssetTreeStateChange({ treeStateChange: change })
    );
  }
}
