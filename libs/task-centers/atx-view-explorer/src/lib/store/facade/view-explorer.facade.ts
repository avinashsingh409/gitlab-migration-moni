/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import * as selectors from '../selectors/view-explorer.selector';
import * as actions from '../actions/view-explorer.actions';
import { selectTheme } from '@atonix/atx-navigation';
import { IAsset, IWidgetChart } from '@atonix/atx-core';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import { take } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';
import { WidgetUpdates } from '../../model/widget-updates';

@Injectable()
export class ViewExplorerFacade {
  constructor(private store: Store<any>) {}

  assetTreeState$ = this.store.pipe(select(selectors.selectAssetTreeState));

  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  selectedAsset$ = this.store.pipe(select(selectors.selectSelectedAssetID));
  selectedAssetNode$ = this.store.pipe(select(selectors.selectSelectedNode));
  timeSliderState$ = this.store.pipe(select(selectors.selectTimeSliderState));
  timeSelection$ = this.store.pipe(select(selectors.selectTimeSelection));

  views$ = this.store.pipe(select(selectors.selectViews));
  selectedViewID$ = this.store.pipe(select(selectors.selectedViewID));
  selectedView$ = this.store.pipe(select(selectors.selectedView));
  viewsLoading$ = this.store.pipe(select(selectors.selectViewsLoading));

  isEditMode$ = this.store.select(selectors.selectIsEditMode);
  selectedElement$ = this.store.select(selectors.selectSelectedElement);

  isViewStateDirty$ = this.store.select(selectors.selectIsViewStateDirty);

  theme$ = this.store.select(selectTheme);
  selectWidgetsOfCurrentSection$ = this.store.select(
    selectors.selectWidgetsOfCurrentSection
  );
  sections$ = this.store.select(selectors.selectSections);

  chartList$ = this.store.select(selectors.selectChartList);
  chartListLoading$ = this.store.select(selectors.selectChartListLoading);

  selectedWidgetChartLabel$ = this.store.select(
    selectors.selectSelectedChartLabel
  );

  // Configuration of the asset tree
  assetTreeConfiguration$ = this.store.pipe(
    select(selectors.selectAssetTreeConfiguration)
  );
  selectedAssetTreeNode$ = this.store.pipe(
    select(selectors.selectSelectedAssetTreeNode)
  );

  // Grab the layout state from the store.  This will indicate whether
  // the asset tree is open or closed.
  layoutMode$ = this.store.pipe(select(selectors.selectLayoutMode));
  leftTraySize$ = this.store.pipe(select(selectors.selectLeftTraySize));

  // This sets up the summary tab
  initializeViewExplorer(
    asset?: string,
    startDate?: Date,
    endDate?: Date,
    reportId?: number
  ) {
    this.store.dispatch(
      actions.viewExplorerIntialize({ asset, startDate, endDate, reportId })
    );
  }

  deleteSelectedWidget() {
    this.store.dispatch(actions.deleteSelectedWidget());
  }

  deleteSelectedSection() {
    this.store.dispatch(actions.deleteSelectedSection());
  }

  getChartSelectList(assetGuid: string) {
    this.store.dispatch(actions.getChartSelectList({ assetGuid }));
  }

  saveView() {
    this.store.dispatch(actions.saveView());
  }

  deleteView() {
    this.store.dispatch(actions.deleteView());
  }

  createView() {
    this.store.dispatch(actions.createView());
  }

  navigateToDataExplorer(assetId: number, trendId: number) {
    this.store.dispatch(actions.navigateToDataExplorer({ assetId, trendId }));
  }

  updateViewInfo(
    name: string,
    description: string,
    title: string,
    subtitle: string
  ) {
    this.store.dispatch(
      actions.updateViewInfo({ name, description, title, subtitle })
    );
  }

  updateSectionInfo(title: string, subtitle: string, size: string) {
    this.store.dispatch(actions.updateSectionInfo({ title, subtitle, size }));
  }

  updateSectionOrder(order: number, sectionId: number) {
    this.store.dispatch(
      actions.updateSectionDisplayOrder({ order, sectionId })
    );
  }

  setSelectedElement(element: SelectableElement) {
    this.store.dispatch(actions.setSelectedElement({ element }));
  }

  setIsEditMode(isEdit: boolean) {
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.store.dispatch(actions.setIsEdit({ isEdit }));
    this.selectedView$.pipe(take(1)).subscribe((view) => {
      // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
      this.store.dispatch(actions.setSelectedElement({ element: view }));
    });
  }
  stopEditMode() {
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.store.dispatch(actions.setIsEdit({ isEdit: false }));
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.store.dispatch(actions.setSelectedElement({ element: null }));
  }

  createNewSection() {
    this.store.dispatch(actions.createSection());
  }

  createNewTextbox() {
    this.store.dispatch(actions.createTextbox());
  }

  createGauge(AssetVariableTypeTagMapID: number, Title: string) {
    this.store.dispatch(
      actions.createGauge({ AssetVariableTypeTagMapID, Title })
    );
  }

  createKPI(AssetVariableTypeTagMapID: number, Title: string) {
    this.store.dispatch(
      actions.createKPI({ AssetVariableTypeTagMapID, Title })
    );
  }

  createChart(PDTrendID: number, AssetID: number) {
    this.store.dispatch(actions.createChart({ PDTrendID, AssetID }));
  }
  createTable(PDTrendID: number, AssetID: number) {
    this.store.dispatch(actions.createTable({ PDTrendID, AssetID }));
  }

  updateWidgetDisplayOrder(order: number, sectionId: number, widgetId: string) {
    this.store.dispatch(
      actions.updateWidgetDisplayOrder({ order, sectionId, widgetId })
    );
  }

  updateSelectedWidget(updates: WidgetUpdates) {
    this.store.dispatch(actions.updateSelectedWidget({ updates }));
  }

  saveTextboxDirectly(content: string, widgetId: string) {
    this.store.dispatch(actions.saveTextboxDirectly({ content, widgetId }));
  }

  updateTextboxContent(content: string, sectionId: number, widgetId: string) {
    this.store.dispatch(
      actions.updateTextboxContent({ content, sectionId, widgetId })
    );
  }

  // For asset tree
  treeSizeChange(value: number) {
    if (value !== null && value !== undefined && value >= 0) {
      this.store.dispatch(actions.treeSizeChange({ value }));
    }
  }

  selectAsset(asset: string) {
    this.store.dispatch(actions.selectAsset({ asset }));
  }

  updateRoute() {
    this.store.dispatch(actions.updateRoute());
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.treeStateChange(change));
    if (change.event === 'PinToggle') {
      this.reflow();
    }
  }

  timeSliderStateChange(change: ITimeSliderStateChange) {
    this.store.dispatch(actions.timeSliderStateChange(change));
  }

  timeRangeChanged(startDate: Date, endDate: Date) {
    this.store.dispatch(actions.timeRangeChanged({ startDate, endDate }));
  }

  loadViews(asset) {
    this.store.dispatch(actions.getViews({ asset }));
  }

  selectView(viewID: number) {
    this.store.dispatch(actions.selectView({ viewID }));
  }
  updateViewFull() {
    this.store.dispatch(actions.updateViewFull({ isFull: false }));
  }

  updateViewDisplayOrder(displayOrder: number) {
    this.store.dispatch(actions.updateViewDisplayOrder({ displayOrder }));
  }

  resetViewDisplayOrder(displayOrder: number) {
    this.store.dispatch(actions.resetViewDisplayOrder({ displayOrder }));
  }

  discardViewChanges() {
    this.store.dispatch(actions.discardViewChanges());
  }

  refreshSelectedView() {
    this.selectedViewID$.pipe(take(1)).subscribe((viewId) => {
      // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
      this.store.dispatch(actions.updateViewFull({ isFull: false }));
      // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
      this.store.dispatch(actions.selectView({ viewID: viewId }));
    });
  }

  resetStateVariables() {
    this.store.dispatch(actions.resetStateVariables());
  }

  reflow() {
    this.store.dispatch(actions.reflow());
  }

  toggleLabels(widget: IWidgetChart) {
    this.store.dispatch(actions.toggleLabels({ widget }));
  }

  editChart(widget: IWidgetChart) {
    this.store.dispatch(actions.editChart({ widget }));
  }

  updateLimits(widget: IWidgetChart, limitsData: IUpdateLimitsData) {
    this.store.dispatch(actions.updateLimits({ widget, limitsData }));
  }

  resetLimits(widget: IWidgetChart, axisIndex: number) {
    this.store.dispatch(actions.resetLimits({ widget, axisIndex }));
  }

  editView() {
    this.store.dispatch(actions.editView());
  }

  copyLink(link: string) {
    this.store.dispatch(actions.copyLink({ link }));
  }
}
