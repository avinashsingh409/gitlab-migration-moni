import {
  ITreeState,
  ITreeStateChange,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import {
  ITimeSliderStateChange,
  alterTimeSliderState,
  getDefaultTimeSlider,
} from '@atonix/atx-time-slider';
import { createReducer, on } from '@ngrx/store';

import * as actions from '../actions/view-explorer.actions';
import * as secondaryTreeActions from '../actions/secondary-asset-tree.actions';
import {
  adapter,
  initialViewExplorerState,
  IViewExplorerState,
} from '../state/view-explorer-state';

import { Update } from '@ngrx/entity';
import {
  updateWidget,
  processFullReport,
  findWidget,
} from '../../service/utilities.service';
import {
  IAsset,
  IProcessedTrend,
  ITreePermissions,
  IWidget,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
  IWidgetTable,
  IWidgetText,
} from '@atonix/atx-core';
import { IReport, IReportSection } from '@atonix/shared/api';
import { IUpdateLimitsData, resetAxes, updateAxes } from '@atonix/atx-chart';
import { SelectableElement } from '../../model/selectable-elements';
import { WidgetUpdates } from '../../model/widget-updates';

const _viewExplorerReducer = createReducer(
  initialViewExplorerState,
  on(actions.resetStateVariables, resetStateVariables),
  on(actions.permissionsRequest, getPermissions),
  on(actions.permissionsRequestSuccess, getPermissionsSuccess),
  on(actions.permissionsRequestFailure, getPermissionsFailure),
  on(actions.treeStateChange, treeStateChange),
  on(actions.assetTreeDataRequestFailure, treeDataFailed),
  on(actions.treeSizeChange, treeSizeChange),
  on(actions.getViews, getViews),
  on(actions.getViewsSuccess, getViewsSuccess),
  on(actions.getViewsFailure, getViewsFailure),
  on(actions.selectView, selectView),
  on(actions.selectViewSuccess, selectViewSuccess),
  on(actions.selectViewFailure, selectViewFailure),

  on(actions.initializeTimeSlider, initializeTimeSliderState),
  on(actions.timeSliderStateChange, timeSliderStateChange),
  on(actions.toggleLabels, toggleLabels),
  on(actions.populateTrendWidgetSuccess, populateTrendWidgetsSuccess),

  on(actions.timeRangeChanged, timeRangeChangedFunction),
  on(actions.workerBegin, workerBeginOrRedispatch),
  on(actions.workerRedispatch, workerBeginOrRedispatch),
  on(actions.workerSuccess, workerSuccessFunction),
  on(actions.populateTrendWidget, populateTrendWidgetsFunction),
  on(actions.viewExplorerIntialize, initialize),
  on(actions.updateLimits, updateLimits),
  on(actions.resetLimits, resetLimits),
  on(actions.initializeRequestedView, initializeRequestedView),

  on(actions.setIsEdit, setIsEdit),
  on(actions.setSelectedElement, setSelectedElement),

  on(actions.createViewSuccess, createViewSuccess),
  on(actions.updateViewInfo, updateViewInfo),
  on(actions.updateViewFull, updateViewFull),
  on(actions.updateViewDisplayOrder, updateViewDisplayOrder),
  on(actions.resetViewDisplayOrder, resetViewDisplayOrder),
  on(actions.discardViewChanges, discardViewChanges),
  on(actions.saveViewSuccess, saveViewSuccess),
  on(actions.deleteViewSuccess, deleteViewSuccess),

  on(actions.createSection, createSection),
  on(actions.updateSectionInfo, updateSectionInfo),
  on(actions.updateSectionDisplayOrder, updateSectionDisplayOrder),

  on(actions.createTextboxSuccess, createTextboxSuccess),
  on(actions.updateTextboxContent, updateTextboxContent),

  on(actions.updateWidgetDisplayOrder, updateWidgetDisplayOrder),
  on(actions.updateSelectedWidget, updateSelectedWidget),
  on(actions.updateWidgetById, updateWidgetById),

  on(
    secondaryTreeActions.secondaryAssetTreeStateChange,
    secondaryAssetTreeStateChange
  ),
  on(
    secondaryTreeActions.setSecondaryAssetTreeSetState,
    setSecondaryAssetTreeSetState
  ),

  on(actions.createGaugeSuccess, createGaugeSuccess),
  on(actions.createKPISuccess, createKPISuccess),
  on(actions.createChartSuccess, createChartSuccess),
  on(actions.createTableSuccess, createTableSuccess),

  on(actions.getChartSelectList, getChartSelectList),
  on(actions.getChartSelectListSuccess, getChartSelectListSuccess),
  on(actions.getChartSelectListFailure, getChartSelectListFailure),

  on(actions.deleteSelectedWidget, deleteSelectedWidget),
  on(actions.deleteSelectedSection, deleteSelectedSection)
);

export function resetStateVariables(state: IViewExplorerState) {
  return {
    ...state,
    isViewStateDirty: false,
    isEditMode: false,
    selectedElement: null,
    listOfInitiatedWidgetPulls: [],
  };
}

export function saveViewSuccess(state: IViewExplorerState) {
  return {
    ...state,
    isViewStateDirty: false,
  };
}

export function discardViewChanges(state: IViewExplorerState) {
  return {
    ...state,
    isViewStateDirty: false,
    selectedElement: null,
  };
}

//updates the full prop on a view that will allow it to be reloaded when selected
export function updateViewFull(
  state: IViewExplorerState,
  payload: { isFull: boolean }
) {
  const selectedViewId = state.selectedView;
  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        full: payload.isFull,
      },
    },
    listOfInitiatedWidgetPulls: [],
    selectedElement: null,
  };
}

export function updateViewDisplayOrder(
  state: IViewExplorerState,
  payload: { displayOrder: number }
) {
  const selectedViewId = state.selectedView;
  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        DisplayOrder: payload.displayOrder,
      },
    },
    isViewStateDirty: true,
  };
}

export function resetViewDisplayOrder(
  state: IViewExplorerState,
  payload: { displayOrder: number }
) {
  const selectedViewId = state.selectedView;
  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        DisplayOrder: payload.displayOrder,
      },
    },
  };
}

export function createViewSuccess(
  state: IViewExplorerState,
  payload: { report: IReport }
) {
  const newState = adapter.addOne(payload.report, state);

  return { ...newState, isViewStateDirty: false };
}

export function deleteViewSuccess(
  state: IViewExplorerState,
  payload: { viewId: number }
) {
  const newState = adapter.removeOne(String(payload.viewId), state);
  const nextViewId = Number(newState.ids.at(-1));

  return {
    ...newState,
    selectedView: nextViewId ? nextViewId : null,
    isEditMode: false,
    selectedElement: null,
  };
}

export function deleteSelectedSection(state: IViewExplorerState) {
  const selectedViewId = state.selectedView;
  const section = state.selectedElement as IReportSection;
  const sectionId = section.ReportSectionID;

  let orderNum = 1;
  let modifiedSections = state.entities[selectedViewId].Sections.filter(
    (section) => section.ReportSectionID !== sectionId
  );
  modifiedSections = modifiedSections.map((section) => {
    return { ...section, DisplayOrder: orderNum++ };
  });

  return {
    ...state,
    selectedElement: null,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function deleteSelectedWidget(state: IViewExplorerState) {
  const selectedViewId = state.selectedView;
  const widget = state.selectedElement as IWidget;
  const widgetId = widget.WidgetID;
  const sectionId = widget.ReportSectionID;

  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === sectionId) {
        let orderNum = 1;
        let modifiedWidgets = section.Widgets.filter(
          (widget) => widget.WidgetID !== widgetId
        );
        modifiedWidgets = modifiedWidgets.map((widget) => {
          return { ...widget, DisplayOrder: orderNum++ };
        });
        return { ...section, Widgets: modifiedWidgets };
      }
      return section;
    }
  );

  return {
    ...state,
    selectedElement: null,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function getChartSelectList(
  state: IViewExplorerState,
  payload: { assetGuid: string }
) {
  return { ...state, chartSelectLoading: true };
}

export function getChartSelectListSuccess(
  state: IViewExplorerState,
  payload: { trends: IProcessedTrend[] }
) {
  return {
    ...state,
    chartSelectList: payload.trends,
    chartSelectLoading: false,
  };
}
export function getChartSelectListFailure(
  state: IViewExplorerState,
  payload: { error: Error }
) {
  return {
    ...state,
    chartSelectLoading: false,
  };
}

export function setSecondaryAssetTreeSetState(
  state: IViewExplorerState,
  payload: { treeState: ITreeState }
): IViewExplorerState {
  const modifiedTreeState = { ...payload.treeState };
  modifiedTreeState.treeConfiguration = {
    ...payload.treeState.treeConfiguration,
    showTreeSelector: false,
    hideConfigureButton: true,
  };
  return {
    ...state,
    secondaryAssetTree: modifiedTreeState,
  };
}

export function secondaryAssetTreeStateChange(
  state: IViewExplorerState,
  payload: { treeStateChange: ITreeStateChange }
): IViewExplorerState {
  const treeChange = treeRetrievedStateChange(payload.treeStateChange);
  return {
    ...state,
    secondaryAssetTree: alterAssetTreeState(
      state.secondaryAssetTree,
      treeChange
    ),
  };
}

export function updateSelectedWidget(
  state: IViewExplorerState,
  payload: { updates: WidgetUpdates }
) {
  const selectedViewId = state.selectedView;
  const element = state.selectedElement as IWidget;
  const sectionId = element.ReportSectionID;
  const widgetId = element.WidgetID;
  let updatedWidget;

  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === sectionId) {
        const modifiedWidgets = section.Widgets.map((widget) => {
          if (widget.WidgetID === widgetId) {
            updatedWidget = { ...widget, ...payload.updates };
            return {
              ...widget,
              ...payload.updates,
            };
          } else {
            return widget;
          }
        });
        return { ...section, Widgets: modifiedWidgets };
      }
      return section;
    }
  );

  return {
    ...state,
    selectedElement: updatedWidget || state.selectedElement,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function updateWidgetById(
  state: IViewExplorerState,
  payload: { widgetId: string; sectionId: number; updates: WidgetUpdates }
) {
  const selectedViewId = state.selectedView;
  const sectionId = payload.sectionId;
  const widgetId = payload.widgetId;
  let updatedWidget;

  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === sectionId) {
        const modifiedWidgets = section.Widgets.map((widget) => {
          if (widget.WidgetID === widgetId) {
            updatedWidget = { ...widget, ...payload.updates };
            return {
              ...widget,
              ...payload.updates,
            };
          } else {
            return widget;
          }
        });
        return { ...section, Widgets: modifiedWidgets };
      }
      return section;
    }
  );

  const selectedWidget = state.selectedElement as IWidget;

  return {
    ...state,
    //if the widget we are updating is the selected widget
    //update selected widget state aswell
    selectedElement:
      updatedWidget && payload.widgetId === selectedWidget.WidgetID
        ? updatedWidget
        : state.selectedElement,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function updateTextboxContent(
  state: IViewExplorerState,
  payload: { content: string; sectionId: number; widgetId: string }
) {
  const selectedViewId = state.selectedView;

  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === payload.sectionId) {
        const modifiedWidgets = section.Widgets.map((widget) => {
          if (widget.WidgetID === payload.widgetId) {
            return {
              ...widget,
              Content: payload.content,
            };
          } else {
            return widget;
          }
        });
        return { ...section, Widgets: modifiedWidgets };
      }
      return section;
    }
  );

  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

function insertNewWidget(
  state: IViewExplorerState,
  newWidget: IWidget
): IViewExplorerState {
  const selectedViewId = state.selectedView;
  let sectionId;
  const element = state.selectedElement as IReportSection | IWidget;
  if (element.ReportSectionID) {
    sectionId = element.ReportSectionID;
  }
  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === sectionId) {
        newWidget.DisplayOrder = section.Widgets.length + 1;
        newWidget.ReportSectionID = sectionId;
        return { ...section, Widgets: [...section.Widgets, newWidget] };
      }
      return section;
    }
  );

  return {
    ...state,
    selectedElement: newWidget,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function createChartSuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetChart }
) {
  const newChartWidget: IWidgetChart = {
    ...payload.widget, //populates PDTrendID, AssetID, WidgetID Guid
    ReportSectionMapID: 0,
    ReportSectionID: 0,
    Title: '',
    Subtitle: '',
    Width: 6,
    Kind: 'Chart',
    DisplayOrder: 0,
    loading: true,
    trend: {
      id: '0',
      label: 'loading',
      trendDefinition: null,
      totalSeries: null,
    },
  };
  return insertNewWidget(state, newChartWidget);
}

export function createTableSuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetTable }
) {
  const newTableWidget: IWidgetTable = {
    ...payload.widget, //populates PDTrendID, AssetID, WidgetID Guid
    ReportSectionMapID: 0,
    ReportSectionID: 0,
    Title: '',
    Subtitle: '',
    Width: 6,
    Kind: 'Table',
    DisplayOrder: 0,
    loading: true,
    trend: {
      id: '0',
      label: 'loading',
      trendDefinition: null,
      totalSeries: null,
    },
  };

  return insertNewWidget(state, newTableWidget);
}

export function createTextboxSuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetText }
) {
  const selectedViewId = state.selectedView;
  const newTextWidget: IWidgetText = {
    ...payload.widget, //WidgetID Guid
    ReportSectionMapID: 0,
    ReportSectionID: 0, //updated later
    Title: '',
    Subtitle: '',
    Width: 3, // represents 3/12 columns aka 25%
    Kind: 'Text',
    AssetID: state.entities[selectedViewId].AssetID,
    DisplayOrder: 0, //updated later
    Content: '(Save this View and double-click to edit this content)',
    Editable: false,
  };
  return insertNewWidget(state, newTextWidget);
}

export function createGaugeSuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetGauge }
) {
  const selectedViewId = state.selectedView;
  const newWidget: IWidgetGauge = {
    ...payload.widget, //AssetVariableTypeTagMapID, WidgetID Guid, Title
    ReportSectionMapID: 0,
    ReportSectionID: 0, //updated later
    Subtitle: '',
    Width: 3, // represents 3/12 columns aka 25%
    Kind: 'Gauge',
    AssetID: state.entities[selectedViewId].AssetID,
    DisplayOrder: 0, //updated later
    Min: 0,
    Max: 1,
  };
  return insertNewWidget(state, newWidget);
}

export function createKPISuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetKPI }
) {
  const selectedViewId = state.selectedView;
  const newWidget: IWidgetKPI = {
    ...payload.widget, //AssetVariableTypeTagMapID, WidgetID Guid, Title
    ReportSectionMapID: 0,
    ReportSectionID: 0, //updated later
    Subtitle: '',
    Width: 3, // represents 3/12 columns aka 25%
    Kind: 'KPI',
    AssetID: state.entities[selectedViewId].AssetID,
    DisplayOrder: 0, //updated later
  };
  return insertNewWidget(state, newWidget);
}

export function updateWidgetDisplayOrder(
  state: IViewExplorerState,
  payload: { order: number; sectionId: number; widgetId: string }
) {
  const selectedViewId = state.selectedView;
  let selectedWidget;
  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === payload.sectionId) {
        //create copy of widgets array
        let modifiedWidgets = section.Widgets.map((widget) => widget);

        //find index of the widget we want to move
        const widgetIndex = modifiedWidgets.findIndex(
          (widget) => widget.WidgetID === payload.widgetId
        );
        if (widgetIndex === -1) return { ...state };

        //get widget to move
        const widget = modifiedWidgets[widgetIndex];

        //remove from old index
        modifiedWidgets.splice(widgetIndex, 1);

        const minIndex = Math.max(payload.order - 1, 0);
        //insert into new index
        modifiedWidgets.splice(
          Math.min(minIndex, modifiedWidgets.length),
          0,
          widget
        );
        //re asign display order for widgets
        modifiedWidgets = modifiedWidgets.map((widget, index) => {
          if (widget.WidgetID === payload.widgetId) {
            //get copy of updated widget to update selectedWidget state
            selectedWidget = { ...widget, DisplayOrder: index + 1 };
          }
          return { ...widget, DisplayOrder: index + 1 };
        });

        return { ...section, Widgets: modifiedWidgets };
      }
      return section;
    }
  );

  return {
    ...state,
    seletedElement: selectedWidget,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

//Sets the section with SectionReportID == sectionId 's  DisplayOrder === order
//and adjusts the DisplayOrder of all other sections and sorts the array by DisplayOrder
export function updateSectionDisplayOrder(
  state: IViewExplorerState,
  payload: { order: number; sectionId: number }
) {
  const selectedViewId = state.selectedView;
  //make copy of sections array
  let modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => section
  );
  //find index of section to reorder
  const sectionIndex = modifiedSections.findIndex(
    (section) => section.ReportSectionID === payload.sectionId
  );
  if (sectionIndex === -1) return { ...state };
  //get section to reorder
  const section = modifiedSections[sectionIndex];
  //remove from old spot in array
  modifiedSections.splice(sectionIndex, 1);
  //insert into new spot in array
  const minIndex = Math.max(payload.order - 1, 0);
  modifiedSections.splice(
    Math.min(minIndex, modifiedSections.length),
    0,
    section
  );
  //relabel sections with correct displayorder
  modifiedSections = modifiedSections.map((section, index) => {
    return { ...section, DisplayOrder: index + 1 };
  });

  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function updateSectionInfo(
  state: IViewExplorerState,
  payload: { title: string; subtitle: string; size: string }
) {
  const selectedViewId = state.selectedView;
  const section = state.selectedElement as IReportSection;
  const selectedSectionId = section.ReportSectionID;
  const modifiedSections = state.entities[selectedViewId].Sections.map(
    (section) => {
      if (section.ReportSectionID === selectedSectionId) {
        return {
          ...section,
          Title: payload.title,
          Subtitle: payload.subtitle,
          Height: payload.size,
        };
      }
      return section;
    }
  );

  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: modifiedSections,
      },
    },
    isViewStateDirty: true,
  };
}

export function updateViewInfo(
  state: IViewExplorerState,
  payload: {
    name: string;
    description: string;
    title: string;
    subtitle: string;
  }
) {
  const selectedViewId = state.selectedView;
  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        ReportAbbrev: payload.name,
        ReportDesc: payload.description,
        Title: payload.title,
        Subtitle: payload.subtitle,
      },
    },
    isViewStateDirty: true,
  };
}

export function setSelectedElement(
  state: IViewExplorerState,
  payload: { element: SelectableElement }
) {
  return {
    ...state,
    selectedElement: payload.element,
  };
}

//newSectionID is to support highlighting new sections (they need different Section IDs)
//using negative nums to not conflict with existing IDs
let newSectionID = -10000;
export function createSection(state: IViewExplorerState) {
  const selectedViewId = state.selectedView;
  const nextDisplayOrder = state.entities[selectedViewId].Sections.length + 1;
  const newSection = {
    ReportSectionID: newSectionID--,
    ReportID: -1,
    Title: 'New Section',
    Subtitle: '',
    Height: 'Small',
    DisplayOrder: nextDisplayOrder,
    Widgets: [],
    backgroundColor: 'transparent',
    Color: 'transparent',
    sectionHeight: '300px',
    Kind: 'Section',
  };
  return {
    ...state,
    entities: {
      ...state.entities,
      [selectedViewId]: {
        ...state.entities[selectedViewId],
        Sections: state.entities[selectedViewId].Sections.concat(newSection),
      },
    },
    isViewStateDirty: true,
  };
}

export function setIsEdit(
  state: IViewExplorerState,
  payload: { isEdit: boolean }
) {
  return {
    ...state,
    isEditMode: payload.isEdit,
  };
}

// this is called when the following happen:
// the user selects a new view from the dropdown, then
// the selectViewSuccess$ is run.  When that ends, it dispatches the
// populateTrendWidget action, and that is what triggers this reducer.
// this reducer sets the number of trend widgets left to populate.
export function populateTrendWidgetsFunction(
  state: IViewExplorerState,
  payload: { widgets: (IWidgetChart | IWidgetTable)[] }
) {
  return {
    ...state,
    numberOfTrendWidgetsLeftToPopulate: payload.widgets.length,
  };
}

export function workerBeginOrRedispatch(
  state: IViewExplorerState,
  payload: { workerNumber: number; widget: IWidget }
) {
  return {
    ...state,
    listOfInitiatedWidgetPulls: state.listOfInitiatedWidgetPulls.concat(
      payload.widget.WidgetID
    ),
  };
}

export function timeRangeChangedFunction(
  state: IViewExplorerState,
  payload: { startDate?: Date; endDate?: Date }
) {
  let newState = { ...state };
  let view = newState.entities[newState.selectedView] || null;
  if (view) {
    const sections = view.Sections.map((section) => {
      const widgets = section.Widgets.map((widget) => {
        return { ...widget, loading: true };
      });
      return { ...section, Widgets: widgets || [] };
    });

    view = { ...view, Sections: sections || [] };
    newState = adapter.updateOne(
      { id: view.ReportID, changes: view },
      newState
    );
  }

  return {
    ...newState,
    listOfInitiatedWidgetPulls: [],
    timeSliderState: {
      ...state.timeSliderState,
      startDate: payload.startDate ?? state.timeSliderState.startDate,
      endDate: payload.endDate ?? state.timeSliderState.endDate,
    },
  };
}

export function workerSuccessFunction(
  state: IViewExplorerState,
  payload: { widget: IWidget; workerNumber: number }
) {
  let newState = { ...state };
  let view = newState.entities[newState.selectedView] || null;
  if (view) {
    view = updateWidget(view, { ...payload.widget, loading: false });
    newState = adapter.updateOne(
      { id: view.ReportID, changes: view },
      newState
    );
  }
  return { ...newState };
}

export function initialize(state: IViewExplorerState) {
  return { ...state };
}

// Asset Tree
export function getPermissions(state: IViewExplorerState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.assetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.assetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.assetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, assetTreeState: treeState };
}

export function getPermissionsSuccess(
  state: IViewExplorerState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(state.assetTreeState.treeConfiguration, {
      canView: payload.canView,
      canEdit: false,
      canDelete: false,
      canAdd: false,
    }),
    treeNodes: { ...state.assetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.assetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, assetTreeState: treeState };
}

export function getPermissionsFailure(state: IViewExplorerState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state.assetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.assetTreeState.treeNodes },
    hasDefaultSelectedAsset: state.assetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, assetTreeState: treeState };
}

export function treeStateChange(
  state: IViewExplorerState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    assetTreeState: alterAssetTreeState(state.assetTreeState, payload),
  };
}

export function treeDataFailed(state: IViewExplorerState, payload: Error) {
  return { ...state };
}

export function treeSizeChange(
  state: IViewExplorerState,
  payload: { value: number }
) {
  return {
    ...state,
    leftTraySize: payload.value,
  };
}

// Time Slider
export function initializeTimeSliderState(
  state: IViewExplorerState,
  payload: { startDate: Date; endDate: Date }
) {
  return {
    ...state,
    timeSliderState: getDefaultTimeSlider({
      dateIndicator: 'Range',
      startDate: payload.startDate,
      endDate: payload.endDate,
    }),
  };
}

export function timeSliderStateChange(
  state: IViewExplorerState,
  payload: ITimeSliderStateChange
) {
  return {
    ...state,
    timeSliderState: alterTimeSliderState(state.timeSliderState, payload),
  };
}

export function getViews(
  state: IViewExplorerState,
  payload: { asset: string }
): IViewExplorerState {
  return {
    ...adapter.removeAll(state),
    selectedView: null,
    loadingViews: true,
  };
}

export function getViewsSuccess(
  state: IViewExplorerState,
  payload: { views: IReport[]; viewRequestedInRoute: number | undefined }
): IViewExplorerState {
  let selectedView = null;
  if (payload.views && payload.views.length > 0) {
    selectedView = payload.views[0].ReportID;
    if (payload.viewRequestedInRoute) {
      if (
        payload.views
          .map((view: IReport) => view.ReportID)
          .includes(payload.viewRequestedInRoute)
      ) {
        selectedView = payload.viewRequestedInRoute;
      }
    }
  }
  return {
    ...adapter.setAll(payload.views, state),
    loadingViews: false,
    selectedView,
    viewRequestedInRoute: undefined,
    listOfInitiatedWidgetPulls: [], //we need to reset list of pulled widgets
    //because we don't have that widget data anymore
  };
}

export function getViewsFailure(
  state: IViewExplorerState,
  payload: { error: any }
) {
  return { ...adapter.removeAll(state), loadingViews: false };
}

export function selectView(
  state: IViewExplorerState,
  payload: { viewID: number }
) {
  const selectedView = state.entities[payload.viewID];
  const update: Update<IReport> = {
    id: payload.viewID,
    changes: {},
  };
  if (selectedView) {
    update.changes.loading = true;
  }
  return {
    ...adapter.updateOne(update, state),
    selectedView: payload.viewID,
    listOfInitiatedWidgetPulls: [],
  };
}

// This reducer is called when a view is retrieved from the server.
// This looks like all we're doing is running the processFullReport function, then
// adding that to the state.views variable.
export function selectViewSuccess(
  state: IViewExplorerState,
  payload: { view: IReport }
) {
  const myReport: IReport = { ...payload.view, full: true, loading: false };
  const changes: Partial<IReport> = processFullReport(myReport);
  const update: Update<IReport> = { id: payload.view.ReportID, changes };
  return { ...adapter.updateOne(update, state) };
}

export function selectViewFailure(state: IViewExplorerState) {
  return { ...state };
}

export function toggleLabels(
  state: IViewExplorerState,
  payload: { widget: IWidgetChart }
) {
  let view = state.entities[state.selectedView];
  let widget = findWidget(view, payload.widget.WidgetID) as IWidgetChart;
  let trend = { ...widget.trend };
  const labelIndex = (trend.labelIndex ?? 0) + 1;
  trend = { ...trend, labelIndex };
  widget = { ...widget, trend };
  view = updateWidget(view, widget);
  return adapter.updateOne({ id: view.ReportID, changes: view }, state);
}

// this is called each time a trend widget successfully gets its data.
export function populateTrendWidgetsSuccess(
  state: IViewExplorerState,
  payload: { widget: IWidgetChart | IWidgetTable; trend: IProcessedTrend }
) {
  // This gets the active view/report
  let view = state.entities[state.selectedView];
  if (!view) {
    // user has clicked away from this asset.
    // this api return is no longer needed.
    return {
      ...state,
      numberOfTrendWidgetsLeftToPopulate:
        state.numberOfTrendWidgetsLeftToPopulate - 1,
    };
  }
  // This finds the widget referenced in the action/payload
  let widget = findWidget(view, payload.widget.WidgetID) as IWidgetChart;

  //if we don't have a trend because the api returned an empty array with 200 0K
  if (!payload.trend || !payload.trend.trendDefinition) return { ...state };

  const trendDefinition = { ...payload.trend.trendDefinition };
  trendDefinition.ShowSelected = true;
  // Turn off the active selection in the event that pins are present.
  // This might be use in the future when a trend is opened on DE.
  if (
    trendDefinition &&
    trendDefinition.Pins &&
    trendDefinition.Pins.length > 0
  ) {
    trendDefinition.ShowSelected = false;
  }

  // this updates the trend in the widget of concern.
  widget = { ...widget, trend: { ...payload.trend, trendDefinition } };
  // I figure this modifies the widget in the view.  The only update this has is probably the trend from the previous line.
  view = updateWidget(view, widget);

  // this updates the view.
  return {
    ...adapter.updateOne({ id: view.ReportID, changes: view }, state),
    numberOfTrendWidgetsLeftToPopulate:
      state.numberOfTrendWidgetsLeftToPopulate - 1,
  };
}

export function updateLimits(
  state: IViewExplorerState,
  payload: { widget: IWidgetChart; limitsData: IUpdateLimitsData }
) {
  let view = state.entities[state.selectedView];
  let widget = findWidget(view, payload.widget.WidgetID) as IWidgetChart;
  let trend = { ...widget.trend };
  const trendDefinition = {
    ...trend.trendDefinition,
    Axes: updateAxes(trend.trendDefinition, payload.limitsData),
  };
  trend = { ...trend, trendDefinition };
  widget = { ...widget, trend };
  view = updateWidget(view, widget);
  return adapter.updateOne({ id: view.ReportID, changes: view }, state);
}

export function resetLimits(
  state: IViewExplorerState,
  payload: { widget: IWidgetChart; axisIndex: number }
) {
  let view = state.entities[state.selectedView];
  let widget = findWidget(view, payload.widget.WidgetID) as IWidgetChart;
  let trend = { ...widget.trend };
  const trendDefinition = {
    ...trend.trendDefinition,
    Axes: resetAxes(trend.trendDefinition, payload.axisIndex),
  };
  trend = { ...trend, trendDefinition };
  widget = { ...widget, trend };
  view = updateWidget(view, widget);
  return adapter.updateOne({ id: view.ReportID, changes: view }, state);
}

export function initializeRequestedView(
  state: IViewExplorerState,
  payload: { viewRequestedInRoute: number | undefined }
): IViewExplorerState {
  return {
    ...state,
    viewRequestedInRoute: payload.viewRequestedInRoute,
  };
}

export function viewExplorerReducer(state, action) {
  return _viewExplorerReducer(state, action);
}
