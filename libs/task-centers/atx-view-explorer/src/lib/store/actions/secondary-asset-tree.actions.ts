import { ITreeState, ITreeStateChange } from '@atonix/atx-asset-tree';
import { createAction, props } from '@ngrx/store';

// Tag List Side Panel Asset Tree
export const secondaryAssetTreeStateChange = createAction(
  '[Tag Select - Asset Tree] Asset Change',
  props<{ treeStateChange: ITreeStateChange }>()
);

export const secondaryAssetTreeStateChangeFailure = createAction(
  '[Tag Select - Asset Tree] Asset Request Failure',
  props<{ error: Error }>()
);

export const setSecondaryAssetTreeSetState = createAction(
  '[Tag Select - Asset Tree] Set State',
  props<{ treeState: ITreeState }>()
);
