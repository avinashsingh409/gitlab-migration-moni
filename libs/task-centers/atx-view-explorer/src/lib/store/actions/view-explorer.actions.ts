/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';

import {
  IAsset,
  IProcessedTrend,
  ITreePermissions,
  IWidget,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
  IWidgetTable,
  IWidgetText,
} from '@atonix/atx-core';
import { IReport } from '@atonix/shared/api';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import { SelectableElement } from '../../model/selectable-elements';
import { WidgetUpdates } from '../../model/widget-updates';

export const getChartSelectList = createAction(
  '[View Explorer] Get Chart Select List',
  props<{ assetGuid: string }>()
);
export const getChartSelectListSuccess = createAction(
  '[View Explorer] Get Chart Select List Success',
  props<{ trends: IProcessedTrend[] }>()
);
export const getChartSelectListFailure = createAction(
  '[View Explorer] Get Chart Select List Failure',
  props<{ error: Error }>()
);

export const navigateToDataExplorer = createAction(
  '[View Explorer] Navigate to Data Explorer',
  props<{ assetId: number; trendId: number }>()
);

export const setIsEdit = createAction(
  '[View Explorer] Set Is Edit',
  props<{ isEdit: boolean }>()
);

export const setSelectedElement = createAction(
  '[View Explorer] Set Selected Element',
  props<{ element: SelectableElement }>()
);

// this action initiates a worker and carries the worker number with it.
export const workerBegin = createAction(
  '[View Explorer] Initiate Worker',
  props<{ workerNumber: number; widget: IWidget }>()
);

// this action initiates a worker and carries the worker number with it.
export const workerRedispatch = createAction(
  '[View Explorer] Redispatch Worker',
  props<{ workerNumber: number; widget: IWidget }>()
);

export const workerSuccess = createAction(
  '[View Explorer] Worker Success',
  props<{ workerNumber: number; widget: IWidget }>()
);

export const workerFailure = createAction(
  '[View Explorer] Worker Failure',
  props<{ error; workerNumber: number; widget: IWidget }>()
);

export const viewExplorerIntialize = createAction(
  '[View Explorer] View Explorer Initialize',
  props<{ asset?: string; startDate: Date; endDate: Date; reportId: number }>()
);

export const permissionsRequest = createAction(
  '[View Explorer] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[View Explorer] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[View Explorer] Permissions Request Failure',
  props<Error>()
);

export const reflow = createAction('[View Explorer] Reflow');

export const selectAsset = createAction(
  '[View Explorer] Select Asset',
  props<{ asset: string }>()
);
export const updateRoute = createAction('[View Explorer] Update Route');

export const treeStateChange = createAction(
  '[View Explorer] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[View Explorer] Tree Data Request Failure',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[View Explorer] Asset Tree Size Change',
  props<{ value: number }>()
);

// Time Slider
export const timeRangeChanged = createAction(
  '[View Explorer] Time Range Changed',
  props<{ startDate: Date; endDate: Date }>()
);

export const initializeTimeSlider = createAction(
  '[View Explorer] Initialize Time Slider',
  props<{ startDate: Date; endDate: Date }>()
);

export const timeSliderStateChange = createAction(
  '[View Explorer] Time Slider State Change',
  props<ITimeSliderStateChange>()
);

export const createTextbox = createAction('[View Explorer] Create New Textbox');
export const createTextboxSuccess = createAction(
  '[View Explorer] Create New Textbox Success',
  props<{ widget: IWidgetText }>()
);

export const createGauge = createAction(
  '[View Explorer] Create New Gauge',
  props<{ AssetVariableTypeTagMapID: number; Title: string }>()
);
export const createGaugeSuccess = createAction(
  '[View Explorer] Create New Gauge Success',
  props<{ widget: IWidgetGauge }>()
);

export const createChart = createAction(
  '[View Explorer] Create New Chart',
  props<{ PDTrendID: number; AssetID: number }>()
);
export const createChartSuccess = createAction(
  '[View Explorer] Create New Chart Success',
  props<{ widget: IWidgetChart }>()
);

export const createTable = createAction(
  '[View Explorer] Create New Table',
  props<{ PDTrendID: number; AssetID: number }>()
);
export const createTableSuccess = createAction(
  '[View Explorer] Create New Table Success',
  props<{ widget: IWidgetTable }>()
);

export const updateChartOrTableData = createAction(
  '[View Explorer] Update Chart or Table Data'
);

//needs reducer
export const updateChartOrTableDataFailure = createAction(
  '[View Explorer] Update Chart or Table Data Failure',
  props<{ error: Error }>()
);

export const createKPI = createAction(
  '[View Explorer] Create New KPI',
  props<{ AssetVariableTypeTagMapID: number; Title: string }>()
);
export const createKPISuccess = createAction(
  '[View Explorer] Create New KPI Success',
  props<{ widget: IWidgetKPI }>()
);

export const updateGaugeOrKPIData = createAction(
  '[View Explorer] Update Gauge or KPI Data'
);

//needs reducer
export const updateGaugeOrKPIDataFailure = createAction(
  '[View Explorer] Update Gauge or KPI Data Failure',
  props<{ error: Error }>()
);

export const updateSelectedWidget = createAction(
  '[View Explorer] Update Selected Widget',
  props<{ updates: WidgetUpdates }>()
);

export const updateWidgetById = createAction(
  '[View Explorer] Update Widget By Id',
  props<{ widgetId: string; sectionId: number; updates: WidgetUpdates }>()
);

export const updateTextboxContent = createAction(
  '[View Explorer] Update Textbox Content',
  props<{ content: string; sectionId: number; widgetId: string }>()
);

export const saveTextboxDirectly = createAction(
  '[View Explorer] Save Textbox Directly',
  props<{ content: string; widgetId: string }>()
);
export const saveTextboxDirectlySuccess = createAction(
  '[View Explorer] Save Textbox Directly Success'
);

export const updateWidgetDisplayOrder = createAction(
  '[View Explorer] Update Widget Display Order',
  props<{ order: number; sectionId: number; widgetId: string }>()
);

export const deleteSelectedWidget = createAction(
  '[View Explorer] Delete Selected Widget'
);

export const deleteSelectedSection = createAction(
  '[View Explorer] Delete Selected Section'
);

export const createSection = createAction('[View Explorer] Create New Section');

export const getViews = createAction(
  '[View Explorer] Get Views',
  props<{ asset: string }>()
);

export const getViewsSuccess = createAction(
  '[View Explorer] Get Views Success',
  props<{ views: IReport[]; viewRequestedInRoute: number | undefined }>()
);
export const getViewsFailure = createAction(
  '[View Explorer] Get Views Failure',
  props<{ error: any }>()
);

export const selectView = createAction(
  '[View Explorer] Select View',
  props<{ viewID: number }>()
);

export const selectViewSuccess = createAction(
  '[View Explorer] Select View Success',
  props<{ view: IReport }>()
);

export const selectViewFailure = createAction(
  '[View Explorer] Select View Failure',
  props<{ viewID: number; error: any }>()
);

export const updateViewFull = createAction(
  '[View Explorer] Update View Full Value',
  props<{ isFull: boolean }>()
);
export const updateViewDisplayOrder = createAction(
  '[View Explorer] Update View Display Order',
  props<{ displayOrder: number }>()
);

export const resetViewDisplayOrder = createAction(
  '[View Explorer] Reset View Display Order',
  props<{ displayOrder: number }>()
);

export const updateViewInfo = createAction(
  '[View Explorer] Update View Info',
  props<{
    name: string;
    description: string;
    title: string;
    subtitle: string;
  }>()
);

export const updateSectionInfo = createAction(
  '[View Explorer] Update Section Info',
  props<{
    title: string;
    subtitle: string;
    size: string;
  }>()
);

export const updateSectionDisplayOrder = createAction(
  '[View Explorer] Update Section Display Order',
  props<{ order: number; sectionId: number }>()
);

export const saveView = createAction('[View Explorer] Save View');
export const saveViewSuccess = createAction(
  '[View Explorer] Save View Success'
);
export const saveViewFailure = createAction(
  '[View Explorer] Save View Failure'
);

export const discardViewChanges = createAction(
  '[View Explorer] Discard View Changes'
);

export const resetStateVariables = createAction(
  '[View Explorer] Reset State Loading/Edit Variables'
);

export const deleteView = createAction('[View Explorer] Delete View');
export const deleteViewSuccess = createAction(
  '[View Explorer] Delete View Success',
  props<{ viewId: number }>()
);
export const deleteViewFailure = createAction(
  '[View Explorer] Delete View Failure'
);

export const createView = createAction('[View Explorer] Create View');

export const createViewSuccess = createAction(
  '[View Explorer] Create View Success',
  props<{ report: IReport }>()
);

export const toggleLabels = createAction(
  '[View Explorer] Toggle Widget Labels',
  props<{ widget: IWidgetChart }>()
);

export const editChart = createAction(
  '[View Explorer] Edit Chart',
  props<{ widget: IWidgetChart }>()
);

export const updateLimits = createAction(
  '[View Explorer] Update Limits',
  props<{ widget: IWidgetChart; limitsData: IUpdateLimitsData }>()
);

export const resetLimits = createAction(
  '[View Explorer] Reset Limits',
  props<{ widget: IWidgetChart; axisIndex: number }>()
);

export const editView = createAction('[View Explorer] Edit View');

export const editViewToAsset = createAction(
  '[View Explorer] Edit View To Asset',
  props<{ key: string; view: number }>()
);

export const populateTrendWidget = createAction(
  '[View Explorer] Populate Trend Widget',
  props<{ widgets: (IWidgetChart | IWidgetTable)[] }>()
);

export const populateTrendWidgetSuccess = createAction(
  '[View Explorer] Populate Trend Widget Success',
  props<{ widget: IWidgetChart | IWidgetTable; trend: IProcessedTrend }>()
);

export const populateTrendWidgetFailure = createAction(
  '[View Explorer] Populate Trend Widget Failure',
  props<{ error: any }>()
);

// dispatched by the checkIfWidgetsAreDone$ effect if we're done populating the trend widgets
export const populateTrendWidgetsDone = createAction(
  '[View Explorer] Populate Trend Widgets Done'
);

export const initializeRequestedView = createAction(
  '[View Explorer] Initialize Requested View',
  props<{ viewRequestedInRoute: number | undefined }>()
);

export const copyLink = createAction(
  '[View Explorer] Copy Report Link',
  props<{ link: string }>()
);
