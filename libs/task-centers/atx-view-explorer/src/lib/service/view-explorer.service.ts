import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isNil } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class ViewExplorerService {
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}
  // This is a function that will grab the id of the selected node from the route.
  // Different uses of the asset tree will want to put different things on the route
  // and the asset tree itself shouldn't be interested in those things.  Also, different
  // apps may want to make local storage another source for information about selected stuff.
  // So we can't make a one size fits all helper function for this yet.
  public getUniqueIdFromRoute() {
    let result = this.activatedRoute.snapshot.queryParamMap.get('id');
    if (result === 'null' || result === 'undefined' || isNil(result)) {
      result = null;
    }
    return result;
  }

  public getStartFromRoute() {
    let result = this.activatedRoute.snapshot.queryParamMap.get('start');
    if (result === 'null' || result === 'undefined' || isNil(result)) {
      result = null;
    }
    return result;
  }

  public getEndFromRoute() {
    let result = this.activatedRoute.snapshot.queryParamMap.get('end');
    if (result === 'null' || result === 'undefined' || isNil(result)) {
      result = null;
    }
    return result;
  }

  public getReportIdFromRoute() {
    const reportIdParam =
      this.activatedRoute.snapshot.queryParamMap.get('reportId');
    let reportId: number | undefined;
    if (
      reportIdParam === 'null' ||
      reportIdParam === 'undefined' ||
      isNil(reportIdParam)
    ) {
      reportId = undefined;
    } else {
      const reportIdParsed = Number.parseInt(reportIdParam);
      if (!Number.isNaN(reportIdParsed)) {
        reportId = reportIdParsed;
      }
    }
    return reportId;
  }

  // Makes the URL reflect the selected asset.
  public updateRouteWithoutReloading(
    id: string,
    start: string,
    end: string,
    reportId: number | undefined
  ) {
    this.router.navigate([], {
      queryParams: { id, start, end, reportId },
      relativeTo: this.activatedRoute,
    });
  }
}
