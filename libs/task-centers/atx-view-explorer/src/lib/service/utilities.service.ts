import { IWidget } from '@atonix/atx-core';
import { IReport, IReportSection } from '@atonix/shared/api';

export function processFullReport(report: IReport): IReport {
  const sections = report.Sections.map((section) => {
    let sectionHeight = '300px';
    if (section.Height === 'Fill') {
      sectionHeight = '100%';
    } else if (section.Height === 'Medium') {
      sectionHeight = '500px';
    }

    const backgroundColor = section.Color ?? 'white';

    const Widgets = section.Widgets.map((widget) => {
      return {
        ...widget,
        widthPct: Math.round(100 * (widget.Width / 12)),
        loading: true,
      };
    });

    return { ...section, sectionHeight, backgroundColor, Widgets };
  });

  return { ...report, Sections: sections };
}

export function findWidget(report: IReport, widgetID: string) {
  // eslint-disable-next-line no-unsafe-optional-chaining
  for (const section of report?.Sections) {
    // eslint-disable-next-line no-unsafe-optional-chaining
    for (const widget of section?.Widgets) {
      if (widget?.WidgetID === widgetID) {
        return widget;
      }
    }
  }
  return null;
}

export function updateWidget(report: IReport, widget: IWidget): IReport {
  const widgetLocation = findWidgetLocation(report, widget);
  if (widgetLocation) {
    const sections: IReportSection[] = [...report.Sections];
    let section = sections[widgetLocation.sectionIndex];
    const widgets: IWidget[] = [...section.Widgets];
    widgets[widgetLocation.widgetIndex] = widget;
    section = { ...section, Widgets: widgets };
    sections[widgetLocation.sectionIndex] = section;
    report = { ...report, Sections: sections };
  }
  return report;
}

function findWidgetLocation(
  report: IReport,
  widget: IWidget
): { sectionIndex: number; widgetIndex: number } {
  for (let s = 0; s < report?.Sections?.length; s++) {
    for (let w = 0; w < report?.Sections[s]?.Widgets?.length; w++) {
      if (report?.Sections[s]?.Widgets[w]?.WidgetID === widget?.WidgetID) {
        return { sectionIndex: s, widgetIndex: w };
      }
    }
  }
  return null;
}
