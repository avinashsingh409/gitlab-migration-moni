import { Inject, Injectable } from '@angular/core';
import { setDataOnTrend, updateTrendAxes } from '@atonix/atx-chart';
import { of, Observable, from } from 'rxjs';

import { map, mergeMap } from 'rxjs/operators';
import { IReport, ProcessDataFrameworkService } from '@atonix/shared/api';
import {
  IProcessedTrend,
  IWidget,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
  IWidgetTable,
} from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class ModelService {
  constructor(
    private processDataFrameworkService: ProcessDataFrameworkService
  ) {}

  public getTrends(
    widget: (IWidgetChart | IWidgetTable)[],
    asset: string,
    start: Date,
    end: Date
  ): Observable<{
    widget: IWidgetChart | IWidgetTable;
    trend: IProcessedTrend;
  }> {
    return of(...widget).pipe(
      mergeMap((w) =>
        this.processDataFrameworkService.getViewExplorerTrend(
          w,
          w.AssetID,
          start,
          end
        )
      )
    );
  }

  public getWidgetData(start: Date, end: Date, widget: IWidget) {
    let result: Observable<IWidget>;
    if (widget.Kind === 'Gauge' || widget.Kind === 'KPI') {
      const g = widget as IWidgetGauge | IWidgetKPI;
      result = this.processDataFrameworkService
        .getGaugeData(
          g.AssetVariableTypeTagMapID,
          start,
          end,
          g.SummaryID ?? 3,
          g.Scenario
        )
        .pipe(
          map((data) => {
            return {
              ...g,
              value: data.value,
              units: data.units,
              Min: (g as IWidgetGauge)?.Min ?? data.min,
              Max: (g as IWidgetGauge)?.Max ?? data.max,
              TagName: data.tagName,
              TagDesc: data.tagDesc,
              TagAssetId: data.tagAssetId,
            };
          })
        );
    } else if (widget.Kind === 'Chart' || widget.Kind === 'Table') {
      const c = widget as IWidgetChart | IWidgetTable;
      if (c?.trend?.trendDefinition) {
        const newTrend = { ...c.trend, startDate: start, endDate: end };
        result = this.processDataFrameworkService
          .getTagsDataFiltered(
            newTrend.trendDefinition,
            start,
            end,
            0,
            null,
            newTrend.assetID
          )
          .pipe(
            map((measurements) => {
              return setDataOnTrend(newTrend, measurements);
            }),
            map((trend) => {
              return { ...c, trend: updateTrendAxes(trend) };
            })
          );
      } else {
        result = of(widget);
      }
    } else {
      result = of(widget);
    }
    return result;
  }
}
