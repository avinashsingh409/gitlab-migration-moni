import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModelService } from './model.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptorService } from '@atonix/shared/state/auth';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('ModelService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    })
  );

  it('should be created', () => {
    const service: ModelService = TestBed.inject(ModelService);
    expect(service).toBeTruthy();
  });
});
