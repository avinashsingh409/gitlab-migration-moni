import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { IWidget } from '@atonix/atx-core';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, take } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';

@Component({
  selector: 'atx-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetComponent {
  @Input() theme: string;
  @Input() widget: IWidget;
  selectedElement$: Observable<SelectableElement>;
  isEditMode$: Observable<boolean>;

  constructor(private facade: ViewExplorerFacade) {
    this.selectedElement$ = this.facade.selectedElement$;
    this.isEditMode$ = this.facade.isEditMode$;
  }

  onSelectElement(event: Event) {
    event.stopImmediatePropagation();
    this.isEditMode$.pipe(take(1)).subscribe((isEditMode) => {
      if (isEditMode) {
        this.facade.setSelectedElement(this.widget);
      }
    });
  }
}
