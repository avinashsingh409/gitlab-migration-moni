import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';

import { WidgetFixedChartComponent } from './widget-fixed-chart.component';

describe('WidgetFixedChartComponent', () => {
  let component: WidgetFixedChartComponent;
  let fixture: ComponentFixture<WidgetFixedChartComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [WidgetFixedChartComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetFixedChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
