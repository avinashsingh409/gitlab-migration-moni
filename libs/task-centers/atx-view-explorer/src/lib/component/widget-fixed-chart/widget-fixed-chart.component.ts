import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { IWidget } from '@atonix/atx-core';

@Component({
  selector: 'atx-widget-fixed-chart',
  templateUrl: './widget-fixed-chart.component.html',
  styleUrls: ['./widget-fixed-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetFixedChartComponent {
  @Input() widget: IWidget;
}
