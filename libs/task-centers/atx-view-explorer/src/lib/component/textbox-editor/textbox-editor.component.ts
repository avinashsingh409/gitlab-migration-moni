import { Component, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, Subject, distinctUntilChanged, takeUntil } from 'rxjs';
import { IWidgetText } from '@atonix/atx-core';
import { SelectableElement } from '../../model/selectable-elements';

@Component({
  selector: 'atx-textbox-editor',
  templateUrl: './textbox-editor.component.html',
  styleUrls: ['./textbox-editor.component.scss'],
})
export class TextboxEditComponent implements OnDestroy {
  textboxEditableControl = new FormControl(false);

  private unsubscribe$ = new Subject<void>();
  public selectedElement$: Observable<SelectableElement>;
  constructor(private facade: ViewExplorerFacade) {
    this.selectedElement$ = facade.selectedElement$;

    this.selectedElement$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((element: IWidgetText) => {
        if (element && element.Kind === 'Text') {
          this.textboxEditableControl.setValue(element.Editable, {
            emitEvent: false,
          });
        }
      });
  }

  onChangeEditable() {
    this.textboxEditableControl.setValue(!this.textboxEditableControl.value);
    this.facade.updateSelectedWidget({
      Editable: this.textboxEditableControl.value,
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
