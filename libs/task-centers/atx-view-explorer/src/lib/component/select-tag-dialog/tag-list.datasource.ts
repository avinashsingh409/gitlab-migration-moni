import {
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { Injectable } from '@angular/core';
import { DataExplorerCoreService } from '@atonix/shared/api';
import { Subject, takeUntil } from 'rxjs';

@Injectable()
export class TagListDataSource implements IServerSideDatasource {
  private unsubscribe$ = new Subject<void>();

  constructor(private dataExplorerCoreService: DataExplorerCoreService) {}

  getRows(params: IServerSideGetRowsParams) {
    const request = { ...params.request };
    if (Object.keys(request?.filterModel).length > 0) {
      // This will handle the % wildcard
      if (request.filterModel['Units']) {
        request.filterModel['Units'].filter = request.filterModel[
          'Units'
        ].filter.replaceAll('%', '[%]');
      }

      this.dataExplorerCoreService
        .getTagList(request)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe({
          next: (n) =>
            params.success({
              rowData: n.TagMaps,
              rowCount: n.NumTagMaps,
            }),
          error: () => {
            params.fail();
            console.error('Could not retrieve tag list');
          },
        });

      // This will reset the filter back
      if (request.filterModel['Units']) {
        request.filterModel['Units'].filter = request.filterModel[
          'Units'
        ].filter.replaceAll('[%]', '%');
      }
    }
  }

  destroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
