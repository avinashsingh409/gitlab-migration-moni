import {
  ClipboardModule,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuModule,
  Module,
  ServerSideRowModelModule,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import {
  AfterViewInit,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { TagListDataSource } from './tag-list.datasource';
import { NavFacade } from '@atonix/atx-navigation';
import {
  ITreeConfiguration,
  ITreeStateChange,
  getAssetByAssetId,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { SecondaryAssetTreeFacade } from '../../store/facade/secondary-asset-tree.facade';
import {
  Observable,
  Subject,
  distinctUntilChanged,
  take,
  takeUntil,
  tap,
} from 'rxjs';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IAssetVariableTypeTagMap } from '@atonix/atx-core';

@Component({
  selector: 'atx-select-tag-dialog',
  templateUrl: './select-tag-dialog.component.html',
  styleUrls: ['./select-tag-dialog.component.scss'],
  providers: [TagListDataSource],
})
export class SelectTagDialogComponent
  implements AfterViewInit, OnDestroy, OnInit
{
  secondaryAssetTreeConfig$: Observable<ITreeConfiguration>;
  selectedAsset$: Observable<string>;
  selectedSecondaryAssetTreeNode$: Observable<any>;
  asset!: string;
  searchAsset!: number;
  gridApi!: GridApi;
  columnApi!: ColumnApi;
  tagsSelectedAssetOnly = false;
  isNewWidget = false;
  unsubscribe$ = new Subject<void>();

  selectedRow: IAssetVariableTypeTagMap;

  modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    ServerSideRowModelModule,
    SetFilterModule,
  ];
  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: 'partial',
    rowGroupPanelShow: 'never',
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'single',
    headerHeight: 30,
    rowHeight: 30,
    floatingFiltersHeight: 30,
    getContextMenuItems: (params) => {
      return ['copy', 'copyWithHeaders'];
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: false,
    },
    components: {},
    columnDefs: [
      {
        colId: 'AssetID',
        headerName: 'AssetID',
        field: 'AssetID',
        sortable: false,
        hide: true,

        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SearchAssetID',
        headerName: 'SearchAssetID',
        field: 'SearchAssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SelectedAssetOnly',
        headerName: 'SelectedAssetOnly',
        field: 'SelectedAssetOnly',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'Asset.AssetAbbrev',
        filter: 'agTextColumnFilter',
        width: 180,
      },
      {
        colId: 'Variable',
        headerName: 'Variable',
        field: 'VariableType.VariableAbbrev',
        filter: 'agTextColumnFilter',
        width: 175,
      },
      {
        colId: 'Name',
        headerName: 'Name',
        field: 'Tag.TagName',
        filter: 'agTextColumnFilter',
        width: 325,
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Tag.TagDesc',
        filter: 'agTextColumnFilter',
        width: 680,
      },
      {
        colId: 'Units',
        headerName: 'Units',
        field: 'Tag.EngUnits',
        filter: 'agTextColumnFilter',
        width: 90,
      },
    ],
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.gridApi.setServerSideDatasource(this.dataSource);
    },
    getRowId: (params) => {
      if (params.data) {
        return params.data.AssetVariableTypeTagMapID;
      }
      return null;
    },
    onSelectionChanged: (event) => {
      const row = this.gridApi.getSelectedRows().find(() => true);
      this.selectedRow = row;
    },
  };

  updateAssetAndSearchAsset(
    asset: string,
    searchAsset: number,
    selectedAssetOnly: boolean
  ) {
    if (this.gridOptions && this.gridOptions.api && asset && searchAsset) {
      this.gridOptions.api?.setFilterModel(null);

      const assetIdFilter = this.gridOptions.api.getFilterInstance('AssetID');
      const searchAssetIdFilter =
        this.gridOptions?.api?.getFilterInstance('SearchAssetID');
      const selectedAssetOnlyFilter =
        this.gridOptions.api.getFilterInstance('SelectedAssetOnly');
      if (
        asset !== assetIdFilter?.getModel()?.filter ||
        searchAsset !== searchAssetIdFilter?.getModel()?.filter ||
        selectedAssetOnly.toString() !==
          selectedAssetOnlyFilter?.getModel()?.filter
      ) {
        assetIdFilter?.setModel({ type: 'asset', filter: asset });
        searchAssetIdFilter?.setModel({
          type: 'searchAsset',
          filter: searchAsset.toString(),
        });
        selectedAssetOnlyFilter?.setModel({
          type: 'selectedAssetOnly',
          filter: selectedAssetOnly ? 'true' : 'false',
        });
        this.gridOptions.api.onFilterChanged();
      }
    }
  }

  constructor(
    public dataSource: TagListDataSource,
    public navFacade: NavFacade,
    public secondaryAssetTreeFacade: SecondaryAssetTreeFacade,
    private viewExplorerFacade: ViewExplorerFacade,
    public dialogRef: MatDialogRef<SelectTagDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.selectedAsset$ = secondaryAssetTreeFacade.selectedAsset$; //main tree

    this.secondaryAssetTreeConfig$ =
      secondaryAssetTreeFacade.secondaryAssetTreeConfiguration$;
    this.selectedSecondaryAssetTreeNode$ =
      secondaryAssetTreeFacade.selectedSecondaryAssetTreeNode$;

    //initilize secondary asset tree
    this.viewExplorerFacade.assetTreeState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        secondaryAssetTreeFacade.setSecondaryAssetTreeSetState(state);
      });

    //When we select a different asset in the Tag Select Asset Tree
    this.selectedSecondaryAssetTreeNode$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        //update AG grid
        this.asset = asset.uniqueKey;
        this.searchAsset = asset.data.AssetId;
        this.selectedRow = undefined;
        this.updateAssetAndSearchAsset(
          asset.uniqueKey,
          asset.data.AssetId,
          this.tagsSelectedAssetOnly
        );
      });
  }

  ngOnInit(): void {
    //navigate to asset associated with the selected tag's asset in the secondary tree
    if (this.data && this.data.tagAssetId) {
      //grab current state of tree (primary and secondary trees should have equal state during init)
      let treeState;
      this.viewExplorerFacade.assetTreeState$
        .pipe(
          take(1),
          tap((assetTreeState) => {
            treeState = assetTreeState;
          })
        )
        .subscribe();

      //get the asset node in the tree that is associated with this assetId
      const asset = getAssetByAssetId(treeState, this.data.tagAssetId);

      //create a TreeStateChange object with selectAsset helper function passing in unique key
      const change: ITreeStateChange = selectAsset(asset.UniqueKey, false);

      //dispatch TreeStateChange
      this.secondaryAssetTreeFacade.secondaryAssetTreeStateChange(change);
    }

    //mark if we are creating a new widget
    if (this.data && this.data.isNewWidget) {
      this.isNewWidget = true;
    }
  }

  ngAfterViewInit(): void {
    //select the asset associated with the tag that the gauge or kpi is associated with

    //set the initial ag grid state with the currently selected asset
    this.selectedSecondaryAssetTreeNode$.pipe(take(1)).subscribe((asset) => {
      //update AG grid
      this.asset = asset.uniqueKey;
      this.searchAsset = asset.data.AssetId;
      this.updateAssetAndSearchAsset(
        asset.uniqueKey,
        asset.data.AssetId,
        this.tagsSelectedAssetOnly
      );
    });
  }

  tagsPreferenceChange() {
    this.tagsSelectedAssetOnly = !this.tagsSelectedAssetOnly;
    this.updateAssetAndSearchAsset(
      this.asset,
      this.searchAsset,
      this.tagsSelectedAssetOnly
    );
  }

  onSelectTagComplete() {
    //if we have a selected row
    if (this.selectedRow) {
      //if its a new widget pass the selected tag back to parent component
      if (this.isNewWidget) {
        return this.dialogRef.close(this.selectedRow);
      }
      //else update existing widget
      else {
        this.viewExplorerFacade.updateSelectedWidget({
          AssetVariableTypeTagMapID: this.selectedRow.AssetVariableTypeTagMapID,
        });
      }
    }

    this.dialogRef.close();
  }

  secondaryAssetTreeStateChange(change: ITreeStateChange) {
    this.secondaryAssetTreeFacade.secondaryAssetTreeStateChange(change);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
