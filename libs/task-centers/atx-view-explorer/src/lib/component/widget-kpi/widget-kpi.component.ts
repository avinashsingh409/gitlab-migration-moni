import {
  Component,
  Input,
  ElementRef,
  ViewChild,
  OnChanges,
  AfterViewInit,
  HostListener,
  ChangeDetectionStrategy,
} from '@angular/core';
import * as d3 from 'd3';
import { IWidgetKPI, sigFig } from '@atonix/atx-core';

interface KPIElemHeights {
  title: number;
  titleY: number;
  value: number;
  units: number;
}

@Component({
  selector: 'atx-widget-kpi',
  templateUrl: './widget-kpi.component.html',
  styleUrls: ['./widget-kpi.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetKpiComponent implements OnChanges, AfterViewInit {
  @Input() widget: IWidgetKPI;
  @ViewChild('kpi') kpiElement: ElementRef;

  private printHeights: KPIElemHeights = {
    title: 16,
    titleY: 70,
    value: 30,
    units: 16,
  };

  ngAfterViewInit(): void {
    this.refresh();
  }

  ngOnChanges() {
    this.refresh();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.refresh();
  }

  public refresh() {
    // The three elements here are sized dynamically for display, to fit the screen well.
    // However, this makes printing them much more complicated.
    // We're adding a second copy of each element, with hardcoded text sizes. These will be used
    // for printing instead of the dynamically sized elements.

    // Need to position title
    // need to position value
    // need to position units
    if (!this.kpiElement?.nativeElement || !this.widget) {
      return;
    }

    const width = this.kpiElement.nativeElement.clientWidth;
    const height = this.kpiElement.nativeElement.clientHeight;

    const titleHeight: number = height * 0.2;
    const subtitleHeight: number = height * 0.2;
    let valueHeight: number = height * 0.25;
    const unitsHeight: number = height * 0.2;

    const screenText = 'screenText';

    this.kpiElement.nativeElement.innerHTML = '';
    const mySvg = d3
      .select(this.kpiElement.nativeElement)
      .append('svg')
      .attr('height', height + 'px')
      .attr('width', width + 'px')
      .attr('class', 'bvKpi ' + (this.widget?.Background ?? ''))
      .style('z-index', 1);

    let valueOffset = valueHeight / 2.8;
    const valueOffsetPrint = this.printHeights.value / 2.8;
    if (this.widget.value !== null && this.widget.value !== undefined) {
      const formattedVal = sigFig(this.widget.value, 4);

      const gt: any = mySvg
        .append('text')
        .text(formattedVal)
        .attr('class', screenText)
        .attr('x', width / 2)
        .attr('text-anchor', 'middle')
        .attr('y', height / 2)
        .style('font-size', valueHeight + 'px')
        .style('fill', '#79c250')
        .attr('dy', valueOffset);
      let sized = valueHeight;
      while (
        gt &&
        gt.node(0) &&
        gt.node(0).getComputedTextLength &&
        sized > 0
      ) {
        if (gt.node(0).getComputedTextLength() > width) {
          sized = sized - 5;
          valueHeight = sized;
          valueOffset = sized / 2.8;

          gt.style('font-size', sized + 'px');
          gt.attr('dy', valueOffset + 'px');
        } else {
          sized = -5;
        }
      }
    }
  }
}
