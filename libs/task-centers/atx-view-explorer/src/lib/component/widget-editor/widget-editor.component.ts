import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import {
  Observable,
  Subject,
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  tap,
  withLatestFrom,
  take,
} from 'rxjs';
import {
  IWidget,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
} from '@atonix/atx-core';
import { SelectableElement } from '../../model/selectable-elements';
import { dataPoints } from '../../model/data-point';
import { MatDialog } from '@angular/material/dialog';
import { SelectTagDialogComponent } from '../select-tag-dialog/select-tag-dialog.component';
import { SelectChartDialogComponent } from '../select-chart-dialog/select-chart-dialog.component';

@Component({
  selector: 'atx-widget-editor',
  templateUrl: './widget-editor.component.html',
  styleUrls: ['./widget-editor.component.scss'],
})
export class WidgetEditComponent implements OnDestroy {
  widgetForm = this.fb.group({
    title: [''],
    subtitle: [''],
    width: [''],
  });
  dataPointControl = new FormControl(1);
  backgroundControl = new FormControl('');
  minMaxForm = this.fb.group({
    min: [0, Validators.pattern(/^-?\d+\.?\d*$/)], //Valid: Whole numbers (1,2,3), negative numbers(-1,-2,-3),
    max: [1, Validators.pattern(/^-?\d+\.?\d*$/)], // numbers with a decimal point (1.1, -1.1, 0.1, -0.1)
  });

  orderControl = new FormControl(0);
  selectWidgetsOfCurrentSection$: Observable<IWidget[]>;
  dataPoints = dataPoints;

  private unsubscribe$ = new Subject<void>();
  public selectedElement$: Observable<SelectableElement>;
  selectedWidgetChartLabel$: Observable<string>;
  constructor(
    private fb: FormBuilder,
    private facade: ViewExplorerFacade,
    public dialog: MatDialog
  ) {
    this.selectedElement$ = facade.selectedElement$;
    this.selectWidgetsOfCurrentSection$ = facade.selectWidgetsOfCurrentSection$;
    this.selectedWidgetChartLabel$ = facade.selectedWidgetChartLabel$;

    // this.selectedWidgetChartLabel$.subscribe((label) => {
    //   console.log(label);
    // });

    this.selectedElement$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((element: IWidget) => {
        if (!element) return;
        this.widgetForm.setValue(
          {
            title: element.Title,
            subtitle: element.Subtitle,
            width: String(element.Width),
          },
          { emitEvent: false }
        );
        this.orderControl.setValue(element.DisplayOrder, {
          emitEvent: false,
        });
        if (element.Kind === 'Gauge') {
          const gauge = element as IWidgetGauge;
          this.dataPointControl.setValue(gauge.SummaryID, { emitEvent: false });
          this.minMaxForm.setValue(
            { min: gauge.Min, max: gauge.Max },
            { emitEvent: false }
          );
        }
        if (element.Kind === 'KPI') {
          const kpi = element as IWidgetKPI;
          this.dataPointControl.setValue(kpi.SummaryID, { emitEvent: false });
          this.backgroundControl.setValue(kpi.Background, { emitEvent: false });
        }
      });

    this.dataPointControl.valueChanges
      .pipe(
        tap((dataPoint) => {
          this.facade.updateSelectedWidget({ SummaryID: dataPoint });
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    this.backgroundControl.valueChanges
      .pipe(
        tap((background) => {
          this.facade.updateSelectedWidget({ Background: background });
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    this.minMaxForm.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(1000),
        tap(({ min, max }) => {
          if (this.isValidMinMax(min, max)) {
            this.facade.updateSelectedWidget({ Min: +min, Max: +max });
          }
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    this.orderControl.valueChanges
      .pipe(
        debounceTime(100),
        withLatestFrom(this.selectedElement$),
        tap(([order, element]) => {
          if (!order) return;
          const widget = element as IWidget;
          this.facade.updateWidgetDisplayOrder(
            order,
            widget.ReportSectionID,
            widget.WidgetID
          );
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    this.widgetForm.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        tap(({ title, subtitle, width }) => {
          this.facade.updateSelectedWidget({
            Title: title,
            Subtitle: subtitle,
            Width: width,
          });
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();
  }

  onSelectTag() {
    this.selectedElement$
      .pipe(
        take(1),
        tap((selectedElement) => {
          const gauge = selectedElement as IWidgetGauge;
          const dialogRef = this.dialog.open(SelectTagDialogComponent, {
            width: '80%',
            height: 'fit-content',
            data: { tagAssetId: gauge.TagAssetId },
          });
        })
      )
      .subscribe();
  }

  onSelectChart() {
    this.selectedElement$
      .pipe(
        take(1),
        tap((selectedElement) => {
          const chart = selectedElement as IWidgetChart;
          const dialogRef = this.dialog.open(SelectChartDialogComponent, {
            width: '50%',
            height: 'fit-content',
            data: {
              chartAssetId: chart.AssetID,
              isChart: selectedElement.Kind === 'Chart',
            },
          });
        })
      )
      .subscribe();
  }

  private isValidMinMax(min: number, max: number) {
    const minString = min.toString();
    const maxString = max.toString();
    return (
      minString.charAt(minString.length - 1) !== '.' &&
      !isNaN(min) &&
      minString !== '' &&
      minString !== '-0' &&
      maxString.charAt(maxString.length - 1) !== '.' &&
      !isNaN(max) &&
      maxString !== '' &&
      maxString !== '-0'
    );
  }

  onNavigateToDataExplorer() {
    this.selectedElement$.pipe(take(1)).subscribe((selectedElement) => {
      const chart = selectedElement as IWidgetChart;
      this.facade.navigateToDataExplorer(chart.AssetID, chart.PDTrendID);
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
