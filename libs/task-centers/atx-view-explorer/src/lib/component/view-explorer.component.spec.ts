import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ViewExplorerComponent } from './view-explorer.component';
import { ViewExplorerService } from '../service/view-explorer.service';
import { ViewExplorerFacade } from '../store/facade/view-explorer.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { MatCardModule } from '@angular/material/card';
import { NavFacade, NavPaneState } from '@atonix/atx-navigation';
import { ITreeNode } from '@atonix/atx-asset-tree';
import { ActivatedRoute } from '@angular/router';
import { AuthFacade } from '@atonix/shared/state/auth';
import { ISecurityRights } from '@atonix/atx-core';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

describe('ViewExplorerComponent', () => {
  let component: ViewExplorerComponent;
  let fixture: ComponentFixture<ViewExplorerComponent>;
  let viewExplorerFacadeMock: ViewExplorerFacade;
  let viewExplorerServiceMock: ViewExplorerService;
  let navFacadeMock: NavFacade;
  let activatedRouteMock: ActivatedRoute;
  let authFacadeMock: AuthFacade;

  beforeEach(waitForAsync(() => {
    navFacadeMock = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      assetTreeSelected$: new BehaviorSubject<boolean>(false),
      navPaneState$: new BehaviorSubject<NavPaneState>('hidden'),
    });
    viewExplorerFacadeMock = createMockWithValues(ViewExplorerFacade, {
      initializeViewExplorer: jest.fn(),
      selectedAssetTreeNode$: new BehaviorSubject<ITreeNode>(null),
      selectedAssetNode$: new BehaviorSubject<string>('123'),
      selectedAsset$: new BehaviorSubject<any>(null),
      selectedViewID$: new BehaviorSubject<number>(100),
      timeSelection$: new BehaviorSubject<{
        startDate: Date;
        endDate: Date;
      }>(null),
      copyLink: jest.fn(),
    });
    viewExplorerServiceMock = createMockWithValues(ViewExplorerService, {
      getEndFromRoute: jest.fn(),
      getUniqueIdFromRoute: jest.fn(),
      getStartFromRoute: jest.fn(),
      getReportIdFromRoute: jest.fn(),
    });
    activatedRouteMock = createMock(ActivatedRoute);
    authFacadeMock = createMockWithValues(AuthFacade, {
      dashboardsTaskCenterAccess$: new BehaviorSubject<ISecurityRights>(null),
    });

    TestBed.configureTestingModule({
      imports: [MatCardModule, MatDialogModule],
      declarations: [ViewExplorerComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: ViewExplorerFacade, useValue: viewExplorerFacadeMock },
        {
          provide: ViewExplorerService,
          useValue: viewExplorerServiceMock,
        },
        {
          provide: NavFacade,
          useValue: navFacadeMock,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRouteMock,
        },
        {
          provide: AuthFacade,
          useValue: authFacadeMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
