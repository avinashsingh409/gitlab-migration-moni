import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WidgetChartComponent } from './widget-chart.component';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { BehaviorSubject, Subject } from 'rxjs';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('WidgetChartComponent', () => {
  let component: WidgetChartComponent;
  let fixture: ComponentFixture<WidgetChartComponent>;
  let viewExplorerFacadeMock: ViewExplorerFacade;
  let authFacadeMock: AuthFacade;

  beforeEach(waitForAsync(() => {
    viewExplorerFacadeMock = createMockWithValues(ViewExplorerFacade, {
      initializeViewExplorer: jest.fn(),
      selectedAsset$: new BehaviorSubject<any>('123'),
    });

    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [WidgetChartComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: ViewExplorerFacade, useValue: viewExplorerFacadeMock },
        { provide: AuthFacade, useValue: authFacadeMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
