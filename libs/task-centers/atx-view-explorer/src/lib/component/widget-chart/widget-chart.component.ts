import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ChangeDetectionStrategy,
} from '@angular/core';

import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { IBtnGrpStateChange, IUpdateLimitsData } from '@atonix/atx-chart';
import {
  GroupedSeriesType,
  IProcessedTrend,
  IWidgetChart,
} from '@atonix/atx-core';
import { AuthFacade } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-widget-chart',
  templateUrl: './widget-chart.component.html',
  styleUrls: ['./widget-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetChartComponent implements OnChanges {
  @Input() widget: IWidgetChart;
  @Input() theme: string;
  height: string;

  emptyTrend: IProcessedTrend = {
    id: '',
    label: '',
    groupedSeriesSubType: '',
    groupedSeriesType: GroupedSeriesType.NONE,
    trendDefinition: {
      PDTrendID: -1,
      TrendDesc: 'No Data',
      Title: 'No Data',
      LegendVisible: true,
      LegendDock: 0,
      LegendContentLayout: 0,
      IsCustom: true,
      CreatedBy: '',
      IsPublic: true,
      DisplayOrder: 0,
      ChartType: null,
      Series: [],
      Axes: [],
      IsStandardTrend: false,
      Filter: null,
      Pins: [],
      PinTypeID: 0,
      ShowPins: false,
      ShowSelected: true,
      SummaryType: null,
      Categories: [],
      XAxisGridlines: false,
    },
    totalSeries: 0,
  };

  constructor(
    private facade: ViewExplorerFacade,
    public authFacade: AuthFacade
  ) {}

  ngOnChanges() {
    let offset = 0;
    if (this.widget?.Title) {
      offset += 25;
    }
    if (this.widget?.Subtitle) {
      offset += 25;
    }
    this.height = 'calc(100% - ' + String(offset) + 'px)';

    // let myTrend = this.widget?.trend;
    // if (myTrend) {
    //   myTrend = { ...myTrend };
    // }
    // this.trend = { ...(this.widget?.trend ?? this.emptyTrend) };
  }

  onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'ChangeLabels') {
      this.facade.toggleLabels(this.widget);
    } else if (change.event === 'EditChart') {
      this.facade.editChart(this.widget);
    } else if (change.event === 'UpdateLimits') {
      this.facade.updateLimits(
        this.widget,
        change.newValue as IUpdateLimitsData
      );
    } else if (change.event === 'ResetLimits') {
      this.facade.resetLimits(this.widget, change.newValue as number);
    }
  }
}
