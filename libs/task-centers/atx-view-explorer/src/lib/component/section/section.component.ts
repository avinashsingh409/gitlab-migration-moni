import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { IReportSection } from '@atonix/shared/api';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, take } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';
import { IWidget, IWidgetText } from '@atonix/atx-core';

@Component({
  selector: 'atx-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SectionComponent {
  @Input() section: IReportSection;
  @Input() theme: string;
  selectedElement$: Observable<SelectableElement>;
  isEditMode$: Observable<boolean>;
  // isSelected$ = this.selectedElement$.pipe();

  constructor(private facade: ViewExplorerFacade) {
    this.selectedElement$ = this.facade.selectedElement$;
    this.isEditMode$ = this.facade.isEditMode$;
  }

  onSelectElement(event: Event) {
    event.stopImmediatePropagation();
    this.isEditMode$.pipe(take(1)).subscribe((isEditMode) => {
      if (isEditMode) {
        this.facade.setSelectedElement(this.section);
      }
    });
  }

  getSectionHeight() {
    if (this.section.Height === 'Small') {
      return '300px';
    } else if (this.section.Height === 'Medium') {
      return '500px';
    } else {
      return '80vh';
    }
  }

  getWidgetWidth(numColumns: number) {
    const map = {
      3: 25,
      4: 33.33,
      6: 50,
      8: 66.66,
      9: 75,
      12: 100,
    };
    return map[numColumns];
  }

  widgetTrackByFunc(index, widget: IWidget): string {
    if (widget.Kind === 'Text') {
      const textWidget = widget as IWidgetText;
      return textWidget.WidgetID + textWidget.Content;
    }
    return widget.WidgetID + widget.loading;
  }
}
