import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { IReport, IReportSection } from '@atonix/shared/api';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, take } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';

@Component({
  selector: 'atx-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewComponent {
  @Input() view: IReport;
  @Input() theme: string;
  isEditMode$: Observable<boolean>;
  selectedElement$: Observable<SelectableElement>;

  constructor(private facade: ViewExplorerFacade) {
    this.isEditMode$ = facade.isEditMode$;
    this.selectedElement$ = facade.selectedElement$;
  }

  trackByReportSectionID(index: number, section: IReportSection): number {
    return section.ReportSectionID;
  }

  onSelectElement(event: Event) {
    event.stopImmediatePropagation();
    this.isEditMode$.pipe(take(1)).subscribe((isEditMode) => {
      if (isEditMode) {
        this.facade.setSelectedElement(this.view);
      }
    });
  }
}
