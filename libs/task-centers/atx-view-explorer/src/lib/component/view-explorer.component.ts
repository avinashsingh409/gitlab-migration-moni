import {
  AfterViewInit,
  Component,
  OnDestroy,
  HostListener,
  OnInit,
} from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import {
  distinctUntilChanged,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  NavFacade,
  IButtonData,
  getUniqueIdFromRoute,
} from '@atonix/atx-navigation';
import {
  IInfoTrayState,
  ITreeConfiguration,
  ITreeNode,
  ITreeStateChange,
} from '@atonix/atx-asset-tree';
import {
  ITimeSliderStateChange,
  ITimeSliderState,
} from '@atonix/atx-time-slider';
import { ViewExplorerFacade } from '../store/facade/view-explorer.facade';
import { ViewExplorerService } from '../service/view-explorer.service';
import moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { IReport } from '@atonix/shared/api';
import { AuthFacade } from '@atonix/shared/state/auth';
import { SelectableElement } from '../model/selectable-elements';
import { MatDialog } from '@angular/material/dialog';
import { DeleteViewConfirmDialogComponent } from './delete-view-confirm-dialog/delete-view-confirm-dialog.component';
import {
  ComponentCanDeactivate,
  SaveChangesModalComponent,
} from '@atonix/shared/utils';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'atx-view-explorer',
  templateUrl: './view-explorer.component.html',
  styleUrls: ['./view-explorer.component.scss'],
})
export class ViewExplorerComponent
  implements OnDestroy, ComponentCanDeactivate, OnInit
{
  public unsubscribe$ = new Subject<void>();

  public theme$: Observable<string>;
  public navPaneState$: Observable<'hidden' | 'open' | 'expanded'>;
  public assetTreeSelected$: Observable<boolean>;
  public showTimeSlider$: Observable<boolean>;

  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public timeSliderState$: Observable<ITimeSliderState>;

  public assetTreeButton$: Observable<IButtonData>;
  public layoutMode$: Observable<'over' | 'push' | 'side'>;
  public leftTraySize$: Observable<number>;

  public selectedAssetTreeNode$: Observable<ITreeNode>;
  public selectedAsset$: Observable<any>;
  public views$: Observable<IReport[]>;
  public sortedViews: IReport[];

  public selectedViewID$: Observable<number>;
  public selectedView$: Observable<IReport>;
  public viewsLoading$: Observable<boolean>;

  public isEditMode$: Observable<boolean>;
  public selectedElement$: Observable<SelectableElement>;
  public infoTrayState: IInfoTrayState;

  public isViewStateDirty$: Observable<boolean>;

  viewSelector = new UntypedFormControl();
  viewOriginalDisplayOrder: number;

  constructor(
    private navFacade: NavFacade,
    private facade: ViewExplorerFacade,
    private viewService: ViewExplorerService,
    private activatedRoute: ActivatedRoute,
    public authFacade: AuthFacade,
    public dialog: MatDialog
  ) {
    const initialStartFromRoute = this.viewService.getStartFromRoute();
    const initialEndFromRoute = this.viewService.getEndFromRoute();

    this.infoTrayState = {
      showInfoTray: true,
      asset: null,
      assetIDs: [],
      attributes: [],
      selectedTab: null,
    };

    this.facade.initializeViewExplorer(
      this.viewService.getUniqueIdFromRoute(),
      initialStartFromRoute
        ? moment(parseFloat(initialStartFromRoute)).toDate()
        : moment().subtract(1, 'months').toDate(),
      initialEndFromRoute
        ? moment(parseFloat(initialEndFromRoute)).toDate()
        : moment().toDate(),
      this.viewService.getReportIdFromRoute()
    );

    this.theme$ = this.navFacade.theme$;
    this.navPaneState$ = this.navFacade.navPaneState$;
    this.assetTreeSelected$ = this.navFacade.assetTreeSelected$;
    this.showTimeSlider$ = this.navFacade.showTimeSlider$;
    this.layoutMode$ = this.facade.layoutMode$;
    this.leftTraySize$ = this.facade.leftTraySize$;

    this.assetTreeConfiguration$ = this.facade.assetTreeConfiguration$;
    this.timeSliderState$ = this.facade.timeSliderState$;
    this.selectedViewID$ = this.facade.selectedViewID$;
    this.selectedView$ = this.facade.selectedView$;
    this.viewsLoading$ = this.facade.viewsLoading$;
    this.selectedAsset$ = this.facade.selectedAsset$;
    this.selectedAssetTreeNode$ = this.facade.selectedAssetTreeNode$;
    this.isEditMode$ = this.facade.isEditMode$;
    this.selectedElement$ = this.facade.selectedElement$;

    this.isViewStateDirty$ = this.facade.isViewStateDirty$;

    this.views$ = this.facade.views$;

    this.selectedViewID$
      .pipe(
        distinctUntilChanged(),
        withLatestFrom(this.selectedView$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([viewId, view]) => {
        this.viewOriginalDisplayOrder = view.DisplayOrder;
      });

    this.selectedAsset$.pipe(takeUntil(this.unsubscribe$)).subscribe((id) => {
      const uniqueKey = getUniqueIdFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      // eslint-disable-next-line rxjs/no-nested-subscribe
      this.selectedAssetTreeNode$.pipe(take(1)).subscribe((node) => {
        if (id) {
          if (uniqueKey === node?.data?.UniqueKey) {
            return;
          } else {
            this.facade.loadViews(id);
          }
        }
      });
    });

    this.facade.selectedAssetNode$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        this.facade.selectAsset(asset);
      });

    this.facade.timeSelection$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((time) => {
        if (time) {
          this.facade.updateRoute();
        }
      });

    this.assetTreeSelected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        this.facade.reflow();
      });

    this.navPaneState$.pipe(takeUntil(this.unsubscribe$)).subscribe((state) => {
      this.facade.reflow();
    });
  }

  onStartEditMode() {
    this.facade.setIsEditMode(true);
  }

  // This is the thing that actually makes changes to the asset tree state.  We handle what we
  // need to handle and then call the default behavior.
  public onAssetTreeStateChange(change: ITreeStateChange) {
    if (change.event === 'SelectAsset') {
      this.checkDiscardChanges(() => {
        this.facade.treeStateChange(change);
      });
    } else {
      this.facade.treeStateChange(change);
    }
  }

  public onAssetTreeSizeChange(newSize: number) {
    this.facade.treeSizeChange(newSize);
  }

  public onTimeSliderStateChange(change: ITimeSliderStateChange) {
    this.facade.timeSliderStateChange(change);
    if (change.event === 'SelectDateRange') {
      this.facade.timeRangeChanged(
        change.newStartDateValue,
        change.newEndDateValue
      );
    }
  }

  onCreateView() {
    this.facade.createView();
    this.facade.setIsEditMode(true);
  }

  onDeleteView() {
    this.dialog.open(DeleteViewConfirmDialogComponent, {});
  }

  reloadView() {
    this.facade.refreshSelectedView();
  }

  public selectView(event: { value: number }) {
    let isStateDirty: boolean;
    this.isViewStateDirty$.pipe(take(1)).subscribe((isViewStateDirty) => {
      isStateDirty = isViewStateDirty;
    });

    if (isStateDirty) {
      const dialogRef = this.dialog.open(SaveChangesModalComponent);

      dialogRef.afterClosed().subscribe((discard: boolean) => {
        if (discard) {
          this.facade.discardViewChanges();
          this.facade.stopEditMode();
          //undo selectedView's display order change
          this.facade.resetViewDisplayOrder(this.viewOriginalDisplayOrder);
          this.facade.selectView(event.value);
        } else {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.selectedViewID$.pipe(take(1)).subscribe((viewId) => {
            //revert view selector to the currently selected view
            this.viewSelector.setValue(viewId);
          });
        }
      });
    } else {
      this.facade.discardViewChanges();
      this.facade.stopEditMode();
      this.facade.selectView(event.value);
    }
  }

  private checkDiscardChanges(callback: () => void) {
    let isStateDirty: boolean;
    this.isViewStateDirty$.pipe(take(1)).subscribe((isViewStateDirty) => {
      isStateDirty = isViewStateDirty;
    });

    if (isStateDirty) {
      const dialogRef = this.dialog.open(SaveChangesModalComponent, {
        data: { navigatingAway: true },
      });

      dialogRef.afterClosed().subscribe((discard: boolean) => {
        if (discard) {
          this.facade.discardViewChanges();
          this.facade.stopEditMode();
          callback();
        } else {
          //do nothing dialog 'cancel' button was pressed
        }
      });
    } else {
      this.facade.discardViewChanges();
      this.facade.stopEditMode();
      callback();
    }
  }

  public editView() {
    this.facade.editView();
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  public copyReportLink(type: string) {
    let reportLink = window.location.href;
    if (type === 'live') {
      const id = this.viewService.getUniqueIdFromRoute();
      const reportId = this.viewService.getReportIdFromRoute();
      const viewExplorerUrl = `?id=${id}&reportId=${reportId?.toString()}`;
      reportLink = `${window.location.origin}${window.location.pathname}${viewExplorerUrl}`;
    }
    this.facade.copyLink(reportLink);
  }

  //activates route guard when selecting another app that shows the discard changes dialog
  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> {
    return this.isViewStateDirty$.pipe(
      take(1),
      switchMap((isDirty) => {
        if (!isDirty) {
          return of(true);
        }
        return of(false);
      })
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  ngOnInit(): void {
    this.facade.resetStateVariables();
  }
}
