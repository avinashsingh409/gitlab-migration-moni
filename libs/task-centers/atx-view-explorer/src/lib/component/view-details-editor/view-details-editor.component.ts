import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { IReport } from '@atonix/shared/api';
import {
  Observable,
  Subject,
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';

@Component({
  selector: 'atx-view-details-editor',
  templateUrl: './view-details-editor.component.html',
  styleUrls: ['./view-details-editor.component.scss'],
})
export class ViewDetailsEditComponent implements OnDestroy {
  viewInfoForm = this.fb.group({
    name: [''],
    description: [''],
    title: [''],
    subtitle: [''],
  });
  displayOrderControl = new FormControl<number>(0);

  public selectedView$: Observable<IReport>;
  public selectedViewID$: Observable<number>;
  private unsubscribe$ = new Subject<void>();

  constructor(private facade: ViewExplorerFacade, private fb: FormBuilder) {
    this.selectedView$ = facade.selectedView$;
    this.selectedViewID$ = facade.selectedViewID$;

    this.selectedViewID$
      .pipe(withLatestFrom(this.selectedView$), takeUntil(this.unsubscribe$))
      .subscribe(([viewId, view]) => {
        if (view) {
          this.viewInfoForm.setValue(
            {
              name: view.ReportAbbrev,
              description: view.ReportDesc,
              title: view.Title,
              subtitle: view.Subtitle,
            },
            { emitEvent: false }
          );
          this.displayOrderControl.setValue(view.DisplayOrder, {
            emitEvent: false,
          });
        }
      });

    this.viewInfoForm.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        tap(({ name, description, title, subtitle }) => {
          this.facade.updateViewInfo(name, description, title, subtitle);
        })
      )
      .subscribe();

    this.displayOrderControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        tap((displayOrder) => {
          this.facade.updateViewDisplayOrder(displayOrder);
        })
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
