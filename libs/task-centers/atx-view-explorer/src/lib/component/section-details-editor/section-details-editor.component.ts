import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { IReportSection } from '@atonix/shared/api';
import {
  Observable,
  Subject,
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { SelectableElement } from '../../model/selectable-elements';

@Component({
  selector: 'atx-section-details-editor',
  templateUrl: './section-details-editor.component.html',
  styleUrls: ['./section-details-editor.component.scss'],
})
export class SectionDetailsEditComponent implements OnDestroy {
  sectionInfoForm = this.fb.group({
    title: [''],
    subtitle: [''],
    size: [''],
  });
  orderControl = new FormControl(0);

  public selectedElement$: Observable<SelectableElement>;
  public sections$: Observable<IReportSection[]>;
  private unsubscribe$ = new Subject<void>();

  constructor(private facade: ViewExplorerFacade, private fb: FormBuilder) {
    this.selectedElement$ = facade.selectedElement$;
    this.sections$ = facade.sections$;

    this.selectedElement$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((element: IReportSection) => {
        if (element && element.Kind === 'Section') {
          this.sectionInfoForm.setValue(
            {
              title: element.Title,
              subtitle: element.Subtitle,
              size: element.Height,
            },
            { emitEvent: false }
          );
          this.orderControl.setValue(element.DisplayOrder, {
            emitEvent: false,
          });
        }
      });

    this.orderControl.valueChanges
      .pipe(
        withLatestFrom(this.selectedElement$),
        tap(([order, element]) => {
          const section = element as IReportSection;
          this.facade.updateSectionOrder(order, section.ReportSectionID);
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();

    this.sectionInfoForm.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(100),
        tap(({ title, subtitle, size }) => {
          this.facade.updateSectionInfo(title, subtitle, size);
        }),
        takeUntil(this.unsubscribe$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
