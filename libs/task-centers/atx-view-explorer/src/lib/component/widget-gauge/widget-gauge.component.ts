import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  HostListener,
  OnChanges,
  AfterViewInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import * as d3 from 'd3';

import { IWidgetGauge, sigFig } from '@atonix/atx-core';

interface GaugeElemHeights {
  title: number;
  titleY: number;
  subtitle: number;
  center: number;
  subcenter: number;
}

@Component({
  selector: 'atx-widget-gauge',
  templateUrl: './widget-gauge.component.html',
  styleUrls: ['./widget-gauge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetGaugeComponent implements OnChanges, AfterViewInit {
  @Input() widget: IWidgetGauge;
  @ViewChild('gauge') gaugeElement: ElementRef;

  private printHeights: GaugeElemHeights = {
    title: 14,
    titleY: 48,
    subtitle: 10,
    center: 12,
    subcenter: 12,
  };

  ngAfterViewInit(): void {
    this.refresh();
  }

  ngOnChanges() {
    this.refresh();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.refresh();
  }

  public refresh() {
    if (!this.gaugeElement?.nativeElement || !this.widget) {
      return;
    }

    const width = this.gaugeElement.nativeElement.clientWidth;
    const height = this.gaugeElement.nativeElement.clientHeight;

    const radius = Math.min(width, height) / 2.75;
    const gaugeWidth: number = radius * 0.075;

    const leftoverSpace = (height - radius) / 2;

    // var titleHeight: number = Math.floor(leftoverSpace * .6);
    const titleHeight: number = height * 0.2;
    const subtitleHeight: number = Math.floor(leftoverSpace * 0.3);
    const centerHeight = 12;
    const labelHeight = 12;
    let valueHeight: number = height * 0.15;

    const centerText = sigFig(this.widget.value, 4);
    // The text elements here are sized dynamically for display, to fit the screen well.
    // However, this makes printing them much more complicated.
    // We're adding a second copy of each element, with hardcoded text sizes. These will be used
    // for printing instead of the dynamically sized elements.

    const screenText = 'screenText';

    const verticalPosition =
      height - (height - titleHeight - subtitleHeight) / 1.5 + radius / 3;

    this.gaugeElement.nativeElement.innerHTML = '';

    const mySvg = d3
      .select(this.gaugeElement.nativeElement)
      .append('svg')
      .attr('height', height + 'px')
      .attr('width', width + 'px')
      .attr('class', 'bvGauge')
      .style('z-index', 1);

    // Gauge Value
    let valueOffset = valueHeight / 2.8;
    if (centerText !== null && centerText !== undefined) {
      const ct: any = mySvg
        .append('text')
        .text(centerText)
        .attr('class', screenText)
        .attr('x', width / 2)
        .attr('y', verticalPosition)
        .attr('text-anchor', 'middle')
        .style('fill', 'currentColor')
        .style('font-size', valueHeight + 'px');
      const maxWidth = radius * 1.2 - 18;
      let sized = valueHeight;
      while (
        ct &&
        ct.node(0) &&
        ct.node(0).getComputedTextLength &&
        sized > 0
      ) {
        if (ct.node(0).getComputedTextLength() > width) {
          sized = sized - 5;
          valueHeight = sized / 1;
          valueOffset = sized / 1;

          ct.style('font-size', sized + 'px');
          ct.attr('dy', valueOffset + 'px');
        } else {
          sized = -5;
        }
      }
    }

    if (this.widget.Min !== null && this.widget.Min !== undefined) {
      mySvg
        .append('text')
        .text(sigFig(this.widget.Min, 4))
        .attr('x', width / 2 - radius + gaugeWidth / 2)
        .attr('y', verticalPosition + labelHeight)
        .attr('text-anchor', 'middle')
        .attr('class', 'bvGaugeLabel')
        .style('fill', 'currentColor')
        .style('font-size', labelHeight + 'px');
    }

    if (this.widget.Max !== null && this.widget.Max !== undefined) {
      mySvg
        .append('text')
        .text(sigFig(this.widget.Max, 4))
        .attr('x', width / 2 + radius - gaugeWidth / 2)
        .attr('y', verticalPosition + labelHeight)
        .attr('text-anchor', 'middle')
        .attr('class', 'bvGaugeLabel')
        .style('fill', 'currentColor')
        .style('font-size', labelHeight + 'px');
    }

    mySvg
      .append('svg:path')
      .attr('class', 'bvGaugePrimary')
      .style('fill', 'gray')
      .attr(
        'd',
        d3
          .arc()
          .innerRadius(radius - gaugeWidth)
          .outerRadius(radius)
          .startAngle((-1 * Math.PI) / 2)
          .endAngle(Math.PI / 2)
      )
      .attr(
        'transform',
        'translate(' + width / 2 + ',' + verticalPosition + ')'
      );

    const scale = d3
      .scaleLinear()
      .domain([this.widget.Min, this.widget.Max])
      .range([(-1 * Math.PI) / 2, Math.PI / 2]);

    if (this.widget.value !== null && this.widget.value !== undefined) {
      mySvg
        .append('svg:path')
        .attr('class', 'bvGaugeSecondary')
        .style('fill', '#79c250')
        .attr(
          'd',
          d3
            .arc()
            .innerRadius(radius - gaugeWidth)
            .outerRadius(radius)
            .startAngle((-1 * Math.PI) / 2)
            .endAngle(
              d3.min([
                d3.max([scale(this.widget.value), (-1 * Math.PI) / 2]),
                Math.PI / 2,
              ])
            )
        )
        .attr(
          'transform',
          'translate(' + width / 2 + ',' + verticalPosition + ')'
        );
    }
  }
}
