import { Component } from '@angular/core';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, take, tap } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';
import { MatDialog } from '@angular/material/dialog';
import { SaveChangesModalComponent } from '@atonix/shared/utils';

@Component({
  selector: 'atx-dashboard-right-side-nav',
  templateUrl: './dashboard-right-side-nav.component.html',
  styleUrls: ['./dashboard-right-side-nav.component.scss'],
})
export class DashboardRightSideNavComponent {
  public selectedElement$: Observable<SelectableElement>;
  public isViewStateDirty$: Observable<boolean>;

  constructor(public facade: ViewExplorerFacade, public dialog: MatDialog) {
    this.selectedElement$ = this.facade.selectedElement$;
    this.isViewStateDirty$ = this.facade.isViewStateDirty$;
  }

  //On 'X' Button press
  onStopEditMode() {
    let isStateDirty: boolean;
    this.isViewStateDirty$.pipe(take(1)).subscribe((isViewStateDirty) => {
      isStateDirty = isViewStateDirty;
    });

    if (isStateDirty) {
      const dialogRef = this.dialog.open(SaveChangesModalComponent);
      dialogRef.afterClosed().subscribe((discard) => {
        if (discard) {
          this.facade.discardViewChanges();
          this.facade.refreshSelectedView();
          this.facade.stopEditMode();
        } else {
          //do nothing dialog 'cancel' button was pressed
        }
      });
    } else {
      this.facade.discardViewChanges();
      this.facade.stopEditMode();
    }
  }

  onSaveView() {
    this.facade.saveView();
  }

  //on 'Discard Changes' Button press
  onDiscardChanges() {
    const dialogRef = this.dialog.open(SaveChangesModalComponent);
    dialogRef.afterClosed().subscribe((discard) => {
      if (discard) {
        this.facade.discardViewChanges();
        this.facade.refreshSelectedView();
      } else {
        //do nothing
      }
    });
  }

  onDeleteSectionOrWidget() {
    this.selectedElement$.pipe(take(1)).subscribe((element) => {
      if (element.Kind === 'Section') {
        this.facade.deleteSelectedSection();
      } else if (this.isWidget(element.Kind)) {
        this.facade.deleteSelectedWidget();
      }
    });
  }

  isWidget(widgetKind: string) {
    const widgetTypes = [
      'Chart',
      'Table',
      'FixedChart',
      'Gauge',
      'KPI',
      'Text',
    ];
    return widgetTypes.some((type) => type === widgetKind);
  }
}
