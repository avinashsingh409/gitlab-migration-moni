import { LocationStrategy } from '@angular/common';
import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
} from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { IWidgetText } from '@atonix/atx-core';
import { Observable, take } from 'rxjs';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';

@Component({
  selector: 'atx-widget-text',
  templateUrl: './widget-text.component.html',
  styleUrls: ['./widget-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetTextComponent implements OnInit {
  @Input() widget: IWidgetText;
  safeContent: SafeHtml;
  showEditor = false;

  editorForm = new UntypedFormGroup({
    content: new UntypedFormControl(null),
  });

  initSummaryEditor = {
    promotion: false,
    height: '100%',
    plugins: 'autolink link',
    contextmenu: 'false',
    paste_data_images: true,
    convert_urls: false,
    relative_urls: false,
    remove_script_host: false,
    toolbar: 'undo redo | bold italic underline | link | forecolor',
    // menubar: 'edit view format',
    menubar: '',
    block_formats: 'Paragraph=p',
    menu: {
      edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
      view: { title: 'View', items: 'visualaid' },
      format: {
        title: 'Format',
        items: 'bold italic underline | removeformat',
      },
    },
    statusbar: false,
    browser_spellcheck: true,
    suffix: '.min',
    base_url: this.locationStrat.getBaseHref() + 'tinymce',
  };

  isEditMode$: Observable<boolean>;

  constructor(
    private sanitizer: DomSanitizer,
    private locationStrat: LocationStrategy,
    private facade: ViewExplorerFacade
  ) {
    this.isEditMode$ = this.facade.isEditMode$;
  }

  ngOnInit(): void {
    this.safeContent = this.sanitizer.bypassSecurityTrustHtml(
      this.widget?.Content ?? ''
    );
    this.editorForm.setValue(
      { content: this.widget?.Content || '' },
      { emitEvent: false }
    );
  }

  onDoneEdit(event: Event) {
    event.stopImmediatePropagation();
    this.updateContent();
    this.showEditor = false;
  }

  onCancelEdit(event: Event) {
    event.stopImmediatePropagation();
    this.showEditor = false;
  }

  onDoubleClick() {
    this.isEditMode$.pipe(take(1)).subscribe((isEditMode) => {
      if (isEditMode || this.widget.Editable) {
        this.showEditor = !this.showEditor;
        //if we just hid the editor
        if (!this.showEditor) {
          this.updateContent();
        }
      }
    });
  }

  private updateContent() {
    this.isEditMode$.pipe(take(1)).subscribe((isEditMode) => {
      const content = this.editorForm.get('content').value;
      this.facade.updateTextboxContent(
        content,
        this.widget.ReportSectionID,
        this.widget.WidgetID
      );

      if (!isEditMode) {
        this.facade.saveTextboxDirectly(content, this.widget.WidgetID);
      }
    });
  }
}
