import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';

import { WidgetTextComponent } from './widget-text.component';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { createMock } from '@testing-library/angular/jest-utils';

describe('WidgetTextComponent', () => {
  let component: WidgetTextComponent;
  let fixture: ComponentFixture<WidgetTextComponent>;

  beforeEach(waitForAsync(() => {
    const viewExplorerFacadeMock = createMock(ViewExplorerFacade);

    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: ViewExplorerFacade, useValue: viewExplorerFacadeMock },
      ],
      declarations: [WidgetTextComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
