import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SecondaryAssetTreeFacade } from '../../store/facade/secondary-asset-tree.facade';
import {
  ITreeConfiguration,
  ITreeNode,
  ITreeStateChange,
  getAssetByAssetId,
  selectAsset,
} from '@atonix/atx-asset-tree';
import {
  Observable,
  Subject,
  distinctUntilChanged,
  startWith,
  take,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs';
import { IProcessedTrend } from '@atonix/atx-core';
import { MatSelectionListChange } from '@angular/material/list';
import { UntypedFormControl } from '@angular/forms';

//Select Chart/Table Dialog
@Component({
  selector: 'atx-select-chart-dialog',
  templateUrl: './select-chart-dialog.component.html',
  styleUrls: ['./select-chart-dialog.component.scss'],
})
export class SelectChartDialogComponent implements OnInit, OnDestroy {
  isNewWidget = false;
  isChart = false;
  secondaryAssetTreeConfig$: Observable<ITreeConfiguration>;
  unsubscribe$ = new Subject<void>();
  selectedSecondaryAssetTreeNode$: Observable<ITreeNode>;
  chartList$: Observable<IProcessedTrend[]>;
  filteredList: IProcessedTrend[];
  chartListLoading$: Observable<boolean>;

  chartSearchControl = new UntypedFormControl('');
  selectedTrend: IProcessedTrend;

  constructor(
    private viewExplorerFacade: ViewExplorerFacade,
    private secondaryAssetTreeFacade: SecondaryAssetTreeFacade,
    public dialogRef: MatDialogRef<SelectChartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.secondaryAssetTreeConfig$ =
      secondaryAssetTreeFacade.secondaryAssetTreeConfiguration$;
    this.selectedSecondaryAssetTreeNode$ =
      secondaryAssetTreeFacade.selectedSecondaryAssetTreeNode$;

    //List of trends updated when we make HTTP requests for a given asset
    this.chartList$ = this.viewExplorerFacade.chartList$;
    this.chartListLoading$ = this.viewExplorerFacade.chartListLoading$;

    //initilize secondary asset tree
    this.viewExplorerFacade.assetTreeState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((state) => {
        secondaryAssetTreeFacade.setSecondaryAssetTreeSetState(state);
      });

    //When we select a different asset in the Tag Select Asset Tree
    this.selectedSecondaryAssetTreeNode$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        this.viewExplorerFacade.getChartSelectList(asset.data.AssetGuid);
        this.selectedTrend = undefined;
      });

    this.chartList$.pipe(takeUntil(this.unsubscribe$)).subscribe((trends) => {
      this.chartSearchControl.setValue('');
      this.filteredList = this.filterChartsOrTables(trends, this.isChart);
    });

    this.chartSearchControl.valueChanges
      .pipe(
        startWith(''),
        distinctUntilChanged(),
        withLatestFrom(this.chartList$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([val, trends]) => {
        const searchRE = new RegExp(val, 'gi');
        this.filteredList =
          trends.filter((trend) => trend.label.match(searchRE)) ?? [];
        this.filteredList = this.filterChartsOrTables(
          this.filteredList,
          this.isChart
        );
      });
  }

  filterChartsOrTables(
    trends: IProcessedTrend[],
    isChart: boolean
  ): IProcessedTrend[] {
    return trends.filter((trend) => {
      if (isChart) {
        //filter to only display charts
        return trend.trendDefinition.ChartTypeID !== 22;
      }
      //filter to only display tables
      return trend.trendDefinition.ChartTypeID === 22;
    });
  }

  ngOnInit(): void {
    if (this.data && this.data.chartAssetId) {
      //grab current state of tree (primary and secondary trees should have equal state during init)
      let treeState;
      this.viewExplorerFacade.assetTreeState$
        .pipe(
          take(1),
          tap((assetTreeState) => {
            treeState = assetTreeState;
          })
        )
        .subscribe();

      //get the asset node in the tree that is associated with this assetId
      const asset = getAssetByAssetId(treeState, this.data.chartAssetId);

      //create a TreeStateChange object with selectAsset helper function passing in unique key
      const change: ITreeStateChange = selectAsset(asset.UniqueKey, false);

      //dispatch TreeStateChange
      this.secondaryAssetTreeFacade.secondaryAssetTreeStateChange(change);
    }

    //mark if we are creating a new widget
    if (this.data && this.data.isNewWidget) {
      this.isNewWidget = true;
    }

    if (this.data && this.data.isChart) {
      this.isChart = this.data.isChart;
    }
  }

  secondaryAssetTreeStateChange(change: ITreeStateChange) {
    this.secondaryAssetTreeFacade.secondaryAssetTreeStateChange(change);
  }

  onSelectChartComplete() {
    if (!this.selectedTrend) {
      return this.dialogRef.close();
    }

    //grabing the assetId of the selected asset in the secondary node as an indirect way to
    //get the assetId associated with a trend, since IProcessedTrend only contains the assetGuid
    //and we just the numeric assetId so we can update the AssetID in the Widget models
    let selectedAssetId: number;
    this.selectedSecondaryAssetTreeNode$
      .pipe(
        take(1),
        tap((node) => {
          selectedAssetId = node.data.AssetId;
        })
      )
      .subscribe();

    //For new charts/tables
    if (this.isNewWidget) {
      return this.dialogRef.close({
        PDTrendID: this.selectedTrend.trendDefinition.PDTrendID,
        AssetID: selectedAssetId || 0,
      });
    }
    //dispatch update chart/table action with updated PDTrendID, AssetID
    else {
      this.viewExplorerFacade.updateSelectedWidget({
        PDTrendID: this.selectedTrend.trendDefinition.PDTrendID,
        AssetID: selectedAssetId || 0,
        loading: true,
      });
      this.dialogRef.close();
    }
  }

  onChartListSelection(event: MatSelectionListChange) {
    this.selectedTrend = event.source._value[0] as unknown as IProcessedTrend;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
