import {
  Component,
  Input,
  ChangeDetectionStrategy,
  OnChanges,
} from '@angular/core';
import { IProcessedTrend, IWidgetTable } from '@atonix/atx-core';

@Component({
  selector: 'atx-widget-table',
  templateUrl: './widget-table.component.html',
  styleUrls: ['./widget-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WidgetTableComponent implements OnChanges {
  @Input() widget: IWidgetTable;
  @Input() theme: string;
  height: string;
  trend: IProcessedTrend;

  emptyTrend: IProcessedTrend = null;

  ngOnChanges() {
    let offset = 0;
    if (this.widget?.Title) {
      offset += 25;
    }
    if (this.widget?.Subtitle) {
      offset += 25;
    }
    this.height = 'calc(100% - ' + String(offset) + 'px)';

    let myTrend = this.widget?.trend;
    if (myTrend) {
      myTrend = { ...myTrend };
    }
    this.trend = myTrend ?? this.emptyTrend;
  }
}
