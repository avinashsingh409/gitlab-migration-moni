import { Component } from '@angular/core';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { Observable, take, tap } from 'rxjs';
import { SelectableElement } from '../../model/selectable-elements';
import { MatDialog } from '@angular/material/dialog';
import { SelectTagDialogComponent } from '../select-tag-dialog/select-tag-dialog.component';
import { SelectChartDialogComponent } from '../select-chart-dialog/select-chart-dialog.component';

@Component({
  selector: 'atx-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  public selectedElement$: Observable<SelectableElement>;
  constructor(private facade: ViewExplorerFacade, public dialog: MatDialog) {
    this.selectedElement$ = this.facade.selectedElement$;
  }

  onCreateSection() {
    this.facade.createNewSection();
  }

  onCreateTextbox() {
    this.facade.createNewTextbox();
  }

  onCreateGaugeOrKPI(Kind: string) {
    const dialogRef = this.dialog.open(SelectTagDialogComponent, {
      width: '80%',
      height: 'fit-content',
      data: { isNewWidget: true },
    });
    dialogRef.afterClosed().subscribe((result) => {
      let title = result.Tag.TagDesc;
      if (result.VariableType && result.VariableType.VariableAbbrev) {
        title = result.VariableType.VariableAbbrev;
      }
      if (result) {
        if (Kind === 'Gauge') {
          this.facade.createGauge(result.AssetVariableTypeTagMapID, title);
        } else {
          this.facade.createKPI(result.AssetVariableTypeTagMapID, title);
        }
      }
    });
  }

  onCreateTableOrChart(Kind: string) {
    const dialogRef = this.dialog.open(SelectChartDialogComponent, {
      width: '50%',
      height: 'fit-content',
      data: { isNewWidget: true, isChart: Kind === 'Chart' },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        if (Kind === 'Chart') {
          this.facade.createChart(result.PDTrendID, result.AssetID);
        } else {
          this.facade.createTable(result.PDTrendID, result.AssetID);
        }
      }
    });
  }
}
