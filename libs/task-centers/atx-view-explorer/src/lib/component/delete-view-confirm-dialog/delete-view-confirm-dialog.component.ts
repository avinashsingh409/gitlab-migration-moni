import { Component, OnInit } from '@angular/core';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import { IReport } from '@atonix/shared/api';
import { Observable } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-delete-view-confirm-dialog',
  templateUrl: './delete-view-confirm-dialog.component.html',
  styleUrls: ['./delete-view-confirm-dialog.component.scss'],
})
export class DeleteViewConfirmDialogComponent {
  selectedView$: Observable<IReport>;
  constructor(
    private dialogRef: MatDialogRef<DeleteViewConfirmDialogComponent>,
    private facade: ViewExplorerFacade
  ) {
    this.selectedView$ = facade.selectedView$;
  }

  onConfirmDelete() {
    //dispatch some action that deletes the current view
    this.facade.deleteView();
    this.dialogRef.close();
  }
}
