import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DeleteViewConfirmDialogComponent } from './delete-view-confirm-dialog.component';
import { ViewExplorerFacade } from '../../store/facade/view-explorer.facade';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BehaviorSubject } from 'rxjs';
import { IReport } from '@atonix/shared/api';

describe('DeleteViewConfirmDialogComponent', () => {
  let component: DeleteViewConfirmDialogComponent;
  let fixture: ComponentFixture<DeleteViewConfirmDialogComponent>;

  beforeEach(async () => {
    const report: IReport = {
      ReportID: -1,
      ReportDesc: '',
      ReportAbbrev: 'New View',
      DisplayOrder: 0,
      AssetID: 0,
      Asset: null,
      Title: '',
      Subtitle: '',
      CreatedBy: '',
      ChangedBy: '',
      CreateDate: '',
      ChangeDate: '',
      Sections: [],
      NodeID: '',
      Kind: 'View',
    };

    const viewExplorerFacadeMock = createMockWithValues(ViewExplorerFacade, {
      selectedView$: new BehaviorSubject<IReport>(report),
    });
    const MatDialogRefMock = createMock(MatDialogRef);
    await TestBed.configureTestingModule({
      declarations: [DeleteViewConfirmDialogComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: ViewExplorerFacade,
          useValue: viewExplorerFacadeMock,
        },
        { provide: MatDialogRef, useValue: MatDialogRefMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DeleteViewConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
