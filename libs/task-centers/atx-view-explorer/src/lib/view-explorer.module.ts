import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NavigationModule } from '@atonix/atx-navigation';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { ViewExplorerComponent } from './component/view-explorer.component';
import { viewExplorerReducer } from './store/reducers/view-explorer.reducer';
import { ViewExplorerEffects } from './store/effects/view-explorer.effects';
import { ViewComponent } from './component/view/view.component';
import { WidgetComponent } from './component/widget/widget.component';
import { SectionComponent } from './component/section/section.component';
import { WidgetChartComponent } from './component/widget-chart/widget-chart.component';
import { WidgetTableComponent } from './component/widget-table/widget-table.component';
import { WidgetFixedChartComponent } from './component/widget-fixed-chart/widget-fixed-chart.component';
import { WidgetGaugeComponent } from './component/widget-gauge/widget-gauge.component';
import { WidgetKpiComponent } from './component/widget-kpi/widget-kpi.component';
import { WidgetTextComponent } from './component/widget-text/widget-text.component';
import { RouterModule } from '@angular/router';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ViewExplorerFacade } from './store/facade/view-explorer.facade';
import { ToolbarComponent } from './component/toolbar/toolbar.component';
import { DashboardRightSideNavComponent } from './component/dashboard-right-side-nav/dashboard-right-side-nav.component';
import { ViewDetailsEditComponent } from './component/view-details-editor/view-details-editor.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SectionDetailsEditComponent } from './component/section-details-editor/section-details-editor.component';
import { TextboxEditComponent } from './component/textbox-editor/textbox-editor.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { WidgetEditComponent } from './component/widget-editor/widget-editor.component';
import { SelectTagDialogComponent } from './component/select-tag-dialog/select-tag-dialog.component';
import { AgGridModule } from '@ag-grid-community/angular';
import { SharedUiModule } from '@atonix/shared/ui';
import { SecondaryAssetTreeFacade } from './store/facade/secondary-asset-tree.facade';
import { SecondaryAssetTreeEffects } from './store/effects/secondary-asset-tree.effects';
import { SelectChartDialogComponent } from './component/select-chart-dialog/select-chart-dialog.component';
import { DeleteViewConfirmDialogComponent } from './component/delete-view-confirm-dialog/delete-view-confirm-dialog.component';
import { PendingChangesGuard } from '@atonix/shared/utils';
// import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { ChartModule } from '@atonix/atx-chart';

@NgModule({
  declarations: [
    ViewExplorerComponent,
    ViewComponent,
    WidgetComponent,
    SectionComponent,
    WidgetChartComponent,
    WidgetTableComponent,
    WidgetFixedChartComponent,
    WidgetGaugeComponent,
    WidgetKpiComponent,
    WidgetTextComponent,
    ToolbarComponent,
    DashboardRightSideNavComponent,
    ViewDetailsEditComponent,
    SectionDetailsEditComponent,
    TextboxEditComponent,
    WidgetEditComponent,
    SelectTagDialogComponent,
    SelectChartDialogComponent,
    DeleteViewConfirmDialogComponent,
  ],
  imports: [
    CommonModule,
    // AtxChartModuleV2,
    ChartModule,
    AssetTreeModule,
    TimeSliderModule,
    NavigationModule,
    AtxMaterialModule,
    ReactiveFormsModule,
    EditorModule,
    AssetTreeModule,
    SharedUiModule,
    AgGridModule,
    RouterModule.forChild([
      {
        path: '',
        component: ViewExplorerComponent,
        pathMatch: 'full',
        canDeactivate: [PendingChangesGuard],
      },
    ]),
    StoreModule.forFeature('viewExplorer', viewExplorerReducer),
    EffectsModule.forFeature([ViewExplorerEffects, SecondaryAssetTreeEffects]),
  ],
  providers: [ViewExplorerFacade, SecondaryAssetTreeFacade],
  exports: [ViewExplorerComponent],
})
export class ViewExplorerModule {}
