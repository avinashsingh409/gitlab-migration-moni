import { IWidget } from '@atonix/atx-core';
import { IReport, IReportSection } from '@atonix/shared/api';

export type SelectableElement = IWidget | IReportSection | IReport;
