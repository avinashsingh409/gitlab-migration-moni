import { IProcessedTrend } from '@atonix/atx-core';

export interface WidgetUpdates {
  Title?: string;
  Subtitle?: string;
  Width?: string;
  Editable?: boolean;
  Background?: string;
  SummaryID?: number;
  Min?: number;
  Max?: number;
  value?: number;
  units?: string;
  AssetVariableTypeTagMapID?: number;
  TagName?: string;
  TagDesc?: string;
  TagAssetId?: number;
  PDTrendID?: number;
  AssetID?: number;
  loading?: boolean;
  trend?: IProcessedTrend;
}
