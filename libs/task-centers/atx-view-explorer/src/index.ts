/*
 * Public API Surface of atx-view-explorer
 */

export * from './lib/service/view-explorer.service';
export * from './lib/component/view-explorer.component';
export * from './lib/view-explorer.module';

// FIX: adding these actions to the MD app breaks lazy loading
// NEED TO MOVE THESE ACTIONS TO SHARED LOCATION OR INSIDE VIEW EXPLORER MODULE
export { editViewToAsset } from './lib/store/actions/view-explorer.actions';
