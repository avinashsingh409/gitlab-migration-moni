/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
/* eslint-disable ngrx/select-style */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  PowerGenActions,
  PowerGenFeature,
} from '@atonix/shared/state/power-gen';

import {
  getEndFromRoute,
  getStartFromRoute,
  NavFacade,
} from '@atonix/atx-navigation';
import {
  ITimeSliderState,
  ITimeSliderStateChange,
} from '@atonix/atx-time-slider';
import { ActivatedRoute } from '@angular/router';
import { AnalyticsResult, IDashboardData } from '@atonix/shared/api';
@Component({
  selector: 'atx-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnDestroy {
  dashboardData$: Observable<IDashboardData | null>;
  showTimeSlider$: Observable<boolean>;
  timeSliderState$: Observable<ITimeSliderState>;
  timeSelection$: Observable<{
    startDate: Date;
    endDate: Date;
  }>;
  loading$: Observable<boolean>;
  error$: Observable<string>;
  unsubscribe$ = new Subject<void>();

  selectedValue = null;

  private getADayAgo() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1, 0, 0, 0, 0);
  }
  private getToday() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
  }

  constructor(
    private store: Store<any>,
    private navFacade: NavFacade,
    private activatedRoute: ActivatedRoute
  ) {
    this.dashboardData$ = store.select(
      PowerGenFeature.getSelectedDashboardData
    );
    this.showTimeSlider$ = this.navFacade.showTimeSlider$;
    this.timeSliderState$ = this.store.pipe(
      select(PowerGenFeature.selectTimeSliderState)
    );
    this.timeSelection$ = this.store.pipe(
      select(PowerGenFeature.selectTimeSelection)
    );
    this.error$ = this.store.pipe(select(PowerGenFeature.selectError));
    this.loading$ = this.store.pipe(select(PowerGenFeature.selectLoading));
    const start = getStartFromRoute(this.activatedRoute.snapshot.queryParamMap);
    const end = getEndFromRoute(this.activatedRoute.snapshot.queryParamMap);

    this.store.dispatch(
      PowerGenActions.initializeTimeSlider({
        start: start ? new Date(parseFloat(start)) : this.getADayAgo(),
        end: end ? new Date(parseFloat(end)) : this.getToday(),
      })
    );

    this.store.dispatch(
      PowerGenActions.updateAnalytics({
        start: start ? new Date(parseFloat(start)) : this.getADayAgo(),
        end: end ? new Date(parseFloat(end)) : this.getToday(),
      })
    );

    this.store.dispatch(PowerGenActions.getEventTypes());

    this.dashboardData$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (data) {
        this.selectedValue = data.id;
      }
    });

    this.timeSelection$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (data) {
        this.store.dispatch(PowerGenActions.updateRoute());
      }
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(PowerGenActions.resetAllIntervalsAndEvents());
    this.unsubscribe$.next();
  }

  trackByChildAssetID(index: number, child: AnalyticsResult): number {
    return child.AssetID;
  }

  public selectAsset(assetID: number) {
    this.store.dispatch(PowerGenActions.selectAsset({ assetID }));
  }

  public onTimeSliderStateChange(change: ITimeSliderStateChange) {
    this.store.dispatch(PowerGenActions.timeSliderStateChange(change));
    if (change.event === 'SelectDateRange') {
      this.store.dispatch(
        PowerGenActions.updateAnalytics({
          start: change.newStartDateValue,
          end: change.newEndDateValue,
        })
      );

      this.store.dispatch(PowerGenActions.getAllChartData());
      this.store.dispatch(PowerGenActions.getAllIntervals());
      this.store.dispatch(PowerGenActions.getAllEvents());
    }
  }

  public showDetails(
    assetID: number,
    parentID: number,
    showDetailsValue: boolean
  ) {
    if (assetID === null) {
      assetID = this.selectedValue;
    }
    this.store.dispatch(
      PowerGenActions.showDetails({ assetID, parentID, showDetailsValue })
    );
  }

  public intervalChange(assetID: number, parentID: number, interval: string) {
    if (assetID === null) {
      assetID = this.selectedValue;
    }
    this.store.dispatch(
      PowerGenActions.getIntervals({ assetID, parentID, interval })
    );
  }

  public initializeEvents(assetID: number, parentID: number) {
    if (assetID === null) {
      assetID = this.selectedValue;
    }
    this.store.dispatch(PowerGenActions.getEvents({ assetID, parentID }));
  }

  public initializeChart(assetID: number, parentID: number) {
    if (assetID === null) {
      assetID = this.selectedValue;
    }
    this.store.dispatch(PowerGenActions.getChartData({ assetID, parentID }));
  }
}
