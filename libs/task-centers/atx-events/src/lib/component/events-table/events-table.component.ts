import {
  Component,
  Input,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { IEvent } from '@atonix/atx-core';

@Component({
  selector: 'atx-events-table',
  templateUrl: './events-table.component.html',
  styleUrls: ['./events-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsTableComponent implements OnChanges {
  @Input() assetID: number;
  @Input() events: IEvent[];
  @Output() initializeEvents = new EventEmitter();

  displayedColumns: string[] = [
    'Event Type',
    'Impact',
    'Description',
    'Start Date',
    'End Date',
  ];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.assetID?.previousValue !== changes?.assetID?.currentValue) {
      this.initializeEvents.emit();
    }
  }
}
