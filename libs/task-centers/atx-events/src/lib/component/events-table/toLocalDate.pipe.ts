import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'toLocalDate',
})
export class LocalDatePipe implements PipeTransform {
  transform(value) {
    return value ? moment(value).format('MM/DD/YYYY hh:mm A') : 'In Progress';
  }
}
