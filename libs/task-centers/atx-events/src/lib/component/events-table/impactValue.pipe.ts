import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'impactValue',
})
export class ImpactValuePipe implements PipeTransform {
  transform(value) {
    return value ? value.toString() : '--';
  }
}
