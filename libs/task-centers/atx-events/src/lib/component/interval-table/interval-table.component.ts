import {
  Component,
  Input,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { AnalyticsResult } from '@atonix/shared/api';

@Component({
  selector: 'atx-interval-table',
  templateUrl: './interval-table.component.html',
  styleUrls: ['./interval-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntervalTableComponent implements OnChanges {
  @Input() assetID: number;
  @Input() interval: string;
  @Input() intervals: AnalyticsResult[];
  @Output() intervalChange = new EventEmitter<string>();

  displayedColumns: string[] = ['StartTime', 'MW', 'CF', 'EAF', 'EFOR', 'SOF'];
  selectedValue = null;

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes?.assetID?.previousValue !== changes?.assetID?.currentValue ||
      this.selectedValue === null
    ) {
      this.selectedValue = this.interval || 'daily';
      this.selectFrequency(this.selectedValue);
    }
  }

  selectFrequency(value: string) {
    this.intervalChange.emit(value);
  }
}
