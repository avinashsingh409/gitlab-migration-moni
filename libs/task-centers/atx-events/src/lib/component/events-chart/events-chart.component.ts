import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
  HostListener,
  Output,
  EventEmitter,
} from '@angular/core';
import { getNumberBySigFigs, HighchartsService } from '@atonix/atx-chart';
import { getDate } from '@atonix/atx-core';
import { IChartData, ImagesFrameworkService } from '@atonix/shared/api';
import Highcharts from 'highcharts';
import moment from 'moment';

@Component({
  selector: 'atx-events-chart',
  templateUrl: './events-chart.component.html',
  styleUrls: ['./events-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsChartComponent implements AfterViewInit, OnChanges {
  @Input() assetID: number;
  @Input() chartData: IChartData;
  @Input() start: Date;
  @Input() end: Date;
  @Output() initializeChart = new EventEmitter();
  @ViewChild('eventChart') chartRef: ElementRef;
  highcharts;

  constructor(
    highchartsService: HighchartsService,

    private imagesFrameworkService: ImagesFrameworkService
  ) {
    this.highcharts = highchartsService.highcharts();
  }

  ngAfterViewInit(): void {
    this.redraw();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes?.assetID?.previousValue !== changes?.assetID?.currentValue) {
      this.initializeChart.emit();
    }

    this.redraw();
  }

  redraw() {
    let newChartOptions: Highcharts.Options = {
      rangeSelector: {
        selected: 1,
      },
      title: {
        text: '',
      },
      series: [],
      credits: {
        enabled: false,
      },
    };

    if (!this.chartData) {
      newChartOptions.title.text = 'loading ...';
    } else {
      newChartOptions = this.turnDataToHighCharts(this.chartData);
      newChartOptions.title.text = '';
    }

    newChartOptions.exporting = this.imagesFrameworkService.getExportSettings();

    if (this.chartRef) {
      this.highcharts.setOptions({
        global: {
          timezoneOffset: moment().toDate().getTimezoneOffset(),
        },
        colors: [
          '#6ea09d',
          '#79c250',
          '#15afea',
          '#bde4e4',
          '#e16b55',
          '#e2bd68',
          '#437638',
          '#ba3f26',
          '#15afea',
          '#bde4e4',
        ],
      });

      this.highcharts.chart(this.chartRef.nativeElement, newChartOptions);
    }
  }

  turnDataToHighCharts(chartData: IChartData) {
    const newChartOptions: Highcharts.Options = {
      chart: {
        type: 'area',
      },
      credits: {
        enabled: false,
      },
      title: {
        text: '',
      },
      xAxis: {
        type: 'datetime',
        showEmpty: true,
        min: getDate(this.start).getTime(),
        max: getDate(this.end).getTime(),
        title: {
          text: ' ',
        },
        breaks: null,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
      },
      yAxis: {
        showEmpty: true,
        min: 0,
        max: null,
        title: {
          text: ' ',
        },
        breaks: null,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
      },
      plotOptions: {
        area: {
          stacking: 'normal',
          step: 'left',
          turboThreshold: 100000,
        },
      },
      tooltip: {
        formatter: function () {
          // Thursday, July 9, 2020 06:00:PM
          const s = `<span style="font-size: 10px">${moment(
            getDate(this.x)
          ).format('dddd, MMM DD, YYYY h:mm A')}</span><br/>
          <span style="color:${this.color}">● </span><b>${
            this.series.name
          }: ${getNumberBySigFigs(this.y, 4)}</b>`;

          return s;
        },
      },
      series: [
        {
          name: 'EFOR',
          data: chartData.EFOR,
          type: 'area',
        },
        {
          name: 'SOF',
          data: chartData.SOF,
          type: 'area',
        },
        {
          name: 'EAF',
          data: chartData.EAF,
          type: 'area',
        },
        {
          name: 'CF',
          type: 'line',
          pointPlacement: 'on',
          color: 'red',
          data: chartData.CF as any,
        },
      ],
    };

    return newChartOptions;
  }
}
