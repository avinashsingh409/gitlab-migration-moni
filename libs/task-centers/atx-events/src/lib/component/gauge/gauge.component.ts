import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  HostListener,
  OnChanges,
  AfterViewInit,
  ChangeDetectionStrategy,
  SimpleChanges,
} from '@angular/core';
import * as d3 from 'd3';
import { sigFig } from '@atonix/atx-core';

interface GaugeElemHeights {
  title: number;
  titleY: number;
  subtitle: number;
  center: number;
  subcenter: number;
}

@Component({
  selector: 'atx-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GaugeComponent implements OnChanges, AfterViewInit {
  @Input() title: string;
  @Input() value: number;
  @Input() units = '%';
  @Input() max = 100;
  @Input() loading: number;
  @ViewChild('gauge') gaugeElement: ElementRef;

  min = 0;

  private printHeights: GaugeElemHeights = {
    title: 14,
    titleY: 48,
    subtitle: 10,
    center: 12,
    subcenter: 12,
  };

  ngAfterViewInit(): void {
    this.refresh();
  }

  ngOnChanges(changes: SimpleChanges) {
    setTimeout(() => {
      this.refresh();
    }, 0);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.refresh();
  }

  public refresh() {
    if (!this.gaugeElement?.nativeElement) {
      return;
    }

    const width = this.gaugeElement.nativeElement.clientWidth;
    const height = this.gaugeElement.nativeElement.clientHeight;

    const radius = Math.min(width, height) / 2.5;
    const gaugeWidth: number = radius * 0.1;

    const leftoverSpace = (height - radius) / 2;

    const titleHeight: number = height * 0.2;
    const subtitleHeight: number = Math.floor(leftoverSpace * 0.3);
    const centerHeight = 12;
    const labelHeight = 12;

    const centerText = sigFig(this.value, 4);
    // The text elements here are sized dynamically for display, to fit the screen well.
    // However, this makes printing them much more complicated.
    // We're adding a second copy of each element, with hardcoded text sizes. These will be used
    // for printing instead of the dynamically sized elements.

    const screenText = 'screenText';

    const verticalPosition =
      height - (height - titleHeight - subtitleHeight) / 1.5 + radius / 3;

    this.gaugeElement.nativeElement.innerHTML = '';

    const mySvg = d3
      .select(this.gaugeElement.nativeElement)
      .append('svg')
      .attr('height', height + 'px')
      .attr('width', width + 'px')
      .attr('class', 'bvGauge')
      .style('z-index', 1);

    // Gauge Value
    if (centerText !== null && centerText !== undefined) {
      const ct: any = mySvg
        .append('text')
        .text(centerText)
        .attr('class', screenText)
        .attr('x', width / 2)
        .attr('y', verticalPosition)
        .attr('text-anchor', 'middle')
        .style('fill', 'currentColor')
        .style('font-size', titleHeight);
      const maxWidth = radius * 1.2 - 18;
      let sized = titleHeight;
      while (ct?.node(0)?.getComputedTextLength && sized > 0) {
        if (ct.node(0).getComputedTextLength() > maxWidth) {
          sized = sized - 5;
          ct.style('font-size', sized + 'px');
        } else {
          sized = -5;
        }
      }
    }

    if (this.min !== null && this.min !== undefined) {
      mySvg
        .append('text')
        .text(this.min.toLocaleString())
        .attr('x', width / 2 - radius + gaugeWidth / 2)
        .attr('y', verticalPosition + labelHeight)
        .attr('text-anchor', 'middle')
        .attr('class', 'bvGaugeLabel')
        .style('fill', 'currentColor')
        .style('font-size', labelHeight + 'px');
    }

    if (this.max !== null && this.max !== undefined) {
      mySvg
        .append('text')
        .text(this.max.toLocaleString())
        .attr('x', width / 2 + radius - gaugeWidth / 2)
        .attr('y', verticalPosition + labelHeight)
        .attr('text-anchor', 'middle')
        .attr('class', 'bvGaugeLabel')
        .style('fill', 'currentColor')
        .style('font-size', labelHeight + 'px');
    }

    mySvg
      .append('svg:path')
      .attr('class', 'bvGaugePrimary')
      .style('fill', 'gray')
      .attr(
        'd',
        d3
          .arc()
          .innerRadius(radius - gaugeWidth)
          .outerRadius(radius)
          .startAngle((-1 * Math.PI) / 2)
          .endAngle(Math.PI / 2)
      )
      .attr(
        'transform',
        'translate(' + width / 2 + ',' + verticalPosition + ')'
      );

    const scale = d3
      .scaleLinear()
      .domain([this.min, this.max])
      .range([(-1 * Math.PI) / 2, Math.PI / 2]);

    if (this.value !== null && this.value !== undefined) {
      mySvg
        .append('svg:path')
        .attr('class', 'bvGaugeSecondary')
        .style('fill', '#79C250')
        .attr(
          'd',
          d3
            .arc()
            .innerRadius(radius - gaugeWidth)
            .outerRadius(radius)
            .startAngle((-1 * Math.PI) / 2)
            .endAngle(
              d3.min([
                d3.max([scale(this.value), (-1 * Math.PI) / 2]),
                Math.PI / 2,
              ])
            )
        )
        .attr(
          'transform',
          'translate(' + width / 2 + ',' + verticalPosition + ')'
        );
    }
  }
}
