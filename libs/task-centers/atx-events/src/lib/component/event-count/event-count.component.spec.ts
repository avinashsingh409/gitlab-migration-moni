import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { EventCountComponent } from './event-count.component';

describe('EventCountComponent', () => {
  let component: EventCountComponent;
  let fixture: ComponentFixture<EventCountComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [EventCountComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
