import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'atx-event-count',
  templateUrl: './event-count.component.html',
  styleUrls: ['./event-count.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventCountComponent {
  @Input() value: number;
  @Input() loading: number;
}
