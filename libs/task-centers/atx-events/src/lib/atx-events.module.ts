import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighchartsChartModule } from 'highcharts-angular';
import { EventsComponent } from './component/events.component';
import { EventsRoutingModule } from './atx-events-routing.module';
import { GaugeComponent } from './component/gauge/gauge.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { AtxMaterialModule } from '@atonix/atx-material';
import { IntervalTableComponent } from './component/interval-table/interval-table.component';
import { EventCountComponent } from './component/event-count/event-count.component';
import { EventsTableComponent } from './component/events-table/events-table.component';
import { ImpactValuePipe } from './component/events-table/impactValue.pipe';
import { LocalDatePipe } from './component/events-table/toLocalDate.pipe';
import { EventsChartComponent } from './component/events-chart/events-chart.component';
@NgModule({
  declarations: [
    EventsComponent,
    GaugeComponent,
    EventCountComponent,
    IntervalTableComponent,
    EventsTableComponent,
    EventsChartComponent,
    ImpactValuePipe,
    LocalDatePipe,
  ],
  imports: [
    CommonModule,
    AssetTreeModule,
    EventsRoutingModule,
    ReactiveFormsModule,
    AtxMaterialModule,
    FlexLayoutModule,
    TimeSliderModule,
    HighchartsChartModule,
  ],
})
export class EventsModule {}
