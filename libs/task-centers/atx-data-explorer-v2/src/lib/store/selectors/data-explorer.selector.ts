import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  getIDFromSelectedAssets,
  getSelectedNode,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';

import { IDataExplorerState } from '../state/data-explorer-state';
import { adapter } from '../state/chart-state';
import { RouteReducerState } from '../state/router.state';
import { ITagListState } from '../state/tag-list-state';
import { ISaveModelState } from '../state/save-model-state';

// get the selectors
const { selectAll } = adapter.getSelectors();

// This is actually grabbing the application state.  It doesn't really
// know what the full application state is, it
export const selectApp = (state: {
  dataexplorer: IDataExplorerState;
  asset?: any;
}) => state;

export const selectDataExplorerState =
  createFeatureSelector<IDataExplorerState>('dataexplorer');

export const selectAssetTreeState = createSelector(
  selectDataExplorerState,
  (state) => state.AssetTreeState
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration
);

export const selectAssetTreePin = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration.pin
);

export const selectLayoutMode = createSelector(selectAssetTreePin, (pin) => {
  const result: TrayState = pin ? 'side' : 'over';
  return result;
});

export const selectSelectedAsset = createSelector(
  selectAssetTreeState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  selectSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const selectSelectedAssetIds = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetGuid: data.AssetGuid, assetId: data.AssetId };
    }
    return null;
  }
);

export const selectSelectedAssetKey = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return (
        data.TreeId +
        '~' +
        data.ParentNodeId +
        '~' +
        data.NodeId +
        '~' +
        data.AssetGuid
      );
    }
    return null;
  }
);

export const selectTimeSliderState = createSelector(
  selectDataExplorerState,
  (state) => state.TimeSliderState
);

export const selectedTrendID = createSelector(
  selectDataExplorerState,
  (state) => state.selectedTrendID
);

export const selectTimeSliderSelection = createSelector(
  selectTimeSliderState,
  (state) => {
    return { start: state?.startDate, end: state?.endDate };
  }
);

export const selectBeginningDate = createSelector(
  selectTimeSliderState,
  (state) => state.startDate
);

export const selectEndingDate = createSelector(
  selectTimeSliderState,
  (state) => state.endDate
);

export const selectToast = createSelector(
  selectDataExplorerState,
  (state) => state?.Toast
);

export const leftTraySize = createSelector(
  selectDataExplorerState,
  (state) => state.LeftTraySize
);

export const chartHeight = createSelector(
  selectDataExplorerState,
  (state) => state.ChartHeight
);

export const selectRouterReducerState =
  createFeatureSelector<RouteReducerState>('router');

// Router Selectors
export const selectRouterState = createSelector(
  selectRouterReducerState,
  (routerReducerState) => routerReducerState?.state
);

export const selectRouterRoute = createSelector(
  selectRouterState,
  (routerState) => routerState?.url
);

// This grabs the state of the selected asset
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);

export const selectTagListState = createSelector(
  selectDataExplorerState,
  (state: IDataExplorerState) => state?.TagListState
);

export const selectAssetTreeDropdownState = createSelector(
  selectTagListState,
  (state: ITagListState) => state?.AssetTreeDropdownState
);

export const selectAssetTreeDropdownConfiguration = createSelector(
  selectAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const selectAssetTreeDropdownNode = createSelector(
  selectAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectAssetTreeDropdownAssetName = createSelector(
  selectAssetTreeDropdownNode,
  (state) => state?.nodeAbbrev
);

export const selectAssetTreeDropdownSelectedAssetIds = createSelector(
  selectAssetTreeDropdownNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetUniqueKey: data.UniqueKey, assetId: data.AssetId };
    }
    return null;
  }
);

export const selectSaveModelState = createSelector(
  selectDataExplorerState,
  (state: IDataExplorerState) => state?.SaveModelState
);

export const selectSaveModeltAssetTreeDropdownState = createSelector(
  selectSaveModelState,
  (state: ISaveModelState) => state?.AssetTreeDropdownState
);

export const saveModelAssetTreeDropdownConfiguration = createSelector(
  selectSaveModeltAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const saveModelAssetTreeDropdownNode = createSelector(
  selectSaveModeltAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const saveModelAssetTreeDropdownAssetName = createSelector(
  saveModelAssetTreeDropdownNode,
  (state) => state?.nodeAbbrev
);

export const saveModelAssetTreeDropdownSelectedAssetId = createSelector(
  saveModelAssetTreeDropdownNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return data.AssetId;
    }
    return null;
  }
);

export const saveModelAssetTreeDropdownSelectedAssetGuid = createSelector(
  saveModelAssetTreeDropdownNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return data.AssetGuid;
    }
    return null;
  }
);
