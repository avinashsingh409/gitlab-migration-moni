import { TestBed, inject } from '@angular/core/testing';
import { Store, Action } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { hot, cold } from 'jest-marbles';
import {
  ModelService as AssetTreeModel,
  nodesRetrieved,
  ITreeConfiguration,
} from '@atonix/atx-asset-tree';
import { AssetTreeEffects } from './asset-tree.effects';
import { initialDataExplorerState } from '../state/data-explorer-state';
import * as actions from '../actions/asset-tree.actions';
import { ITreePermissions } from '@atonix/atx-core';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';

describe('AssetTreeEffects', () => {
  let actions$: Observable<Action>;
  let store: MockStore<any>;
  let effects: AssetTreeEffects;
  let mockAssetTreeModel: AssetTreeModel;
  let mockAuthFrameworkService: AuthorizationFrameworkService;

  const myTreeConfiguration: ITreeConfiguration = {
    showTreeSelector: true,
    trees: [],
    selectedTree: '1',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    nodes: [],
    pin: false,
    loadingAssets: false,
    loadingTree: false,
    canDrag: false,
    dropTargets: null,
    canView: true,
    canAdd: true,
    canEdit: true,
    canDelete: true,
    collapseOthers: false,
    hideConfigureButton: false,
  };

  beforeEach(() => {
    const initialState: any = { dataexplorer: initialDataExplorerState };
    actions$ = new Subject<Action>();

    mockAssetTreeModel = createMockWithValues(AssetTreeModel, {
      defaultNode$: new BehaviorSubject<string>('test'),
      getAssetData: jest.fn(),
      getAssetTreeData: jest.fn(),
    });

    mockAuthFrameworkService = createMock(AuthorizationFrameworkService);

    TestBed.configureTestingModule({
      providers: [
        provideMockActions(() => actions$),
        provideMockStore<any>({ initialState }),
        {
          provide: AuthorizationFrameworkService,
          useValue: mockAuthFrameworkService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: AssetTreeModel, useValue: mockAssetTreeModel },
        AssetTreeEffects,
      ],
    });
    mockAuthFrameworkService = TestBed.inject(AuthorizationFrameworkService);
    mockAssetTreeModel = TestBed.inject(AssetTreeModel);
    store = TestBed.inject<any>(Store);
    effects = TestBed.inject<AssetTreeEffects>(AssetTreeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should get asset info twice', () => {
    store.setState({
      dataexplorer: {
        AssetTreeState: {
          treeConfiguration: myTreeConfiguration,
          treeNodes: null,
        },
      },
    });
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey1', []) }))
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    actions$ = hot('a-b', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });

    const expected = hot('a-b', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get asset info overlap', () => {
    store.setState({
      dataexplorer: {
        AssetTreeState: {
          treeConfiguration: myTreeConfiguration,
          treeNodes: null,
        },
      },
    });
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(
        cold('--a|', { a: nodesRetrieved('uniquekey1', []) })
      )
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    actions$ = hot('ab', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });

    const expected = hot('-ba', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get permissions', () => {
    const result1: ITreePermissions = {
      canAdd: true,
      canDelete: true,
      canEdit: true,
      canView: true,
    };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: result1 }))
      .mockReturnValueOnce(cold('a|', { a: result2 }));

    actions$ = hot('a-b', {
      a: actions.getPermissions(),
      b: actions.getPermissions(),
    });

    const expected = hot('a-b', {
      a: actions.getPermissionsSuccess(result1),
      b: actions.getPermissionsSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });

  it('should get permissions with error', () => {
    const error1: Error = { name: 'name', message: 'something' };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };
    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a|', { a: result2 }));

    actions$ = hot('a-b', {
      a: actions.getPermissions(),
      b: actions.getPermissions(),
    });

    const expected = hot('a-b', {
      a: actions.getPermissionsFailure(error1),
      b: actions.getPermissionsSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });
});
