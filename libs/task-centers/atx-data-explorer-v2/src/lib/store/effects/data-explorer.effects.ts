/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */

import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import {
  switchMap,
  map,
  withLatestFrom,
  catchError,
  tap,
} from 'rxjs/operators';

import * as dataExplorerActions from '../actions/data-explorer.action';
import * as assetTreeActions from '../actions/asset-tree.actions';
import * as timeSliderStateActions from '../actions/time-slider.actions';
import {
  assetState,
  selectAssetTreeDropdownState,
  selectedTrendID,
  selectSaveModeltAssetTreeDropdownState,
  selectSelectedAsset,
  selectTimeSliderSelection,
} from '../selectors/data-explorer.selector';
import {
  NavActions,
  updateRouteWithoutReloading,
} from '@atonix/atx-navigation';

import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { ActivatedRoute, Router } from '@angular/router';
import { updateSelectedTrend } from '../actions/data-explorer.action';
import { of } from 'rxjs';
import { ChartFacade } from '@atonix/atx-chart-v2';
import * as moment from 'moment';
@Injectable()
export class DataExplorerEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private chartFacade: ChartFacade,
    private activatedRoute: ActivatedRoute,
    private assetTreeModel: AssetTreeModel
  ) {}

  reflowSlower$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(assetTreeActions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 100);
        })
      ),
    { dispatch: false }
  );

  userPreferencesChangedSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NavActions.userPreferencesChangedSuccess),
        tap(() => {
          this.chartFacade.setLoading();
        })
      ),
    {
      dispatch: false,
    }
  );

  initializeSummary$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.dataExplorerInitialize),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(assetState)
      ),
      map(([payload, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return {
          asset: payload.asset || appAsset || newAsset,
          trend: payload.trend,
          start: payload.start,
          end: payload.end,
        };
      }),
      switchMap((vals) => [
        assetTreeActions.getPermissions(),
        assetTreeActions.treeStateChange(selectAsset(vals.asset, false)),
        timeSliderStateActions.initializeTimeSlider({
          start: vals.start,
          end: vals.end,
        }),
        updateSelectedTrend({ trendID: vals.trend }),
        NavActions.taskCenterLoad({
          taskCenterID: 'data-explorer',
          timeSlider: true,
          timeSliderOpened: true,
          assetTree: true,
          assetTreeOpened: false,
          asset: vals.asset,
        }),
      ])
    )
  );

  updateRoute$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.updateRoute),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(selectTimeSliderSelection),
        this.store.select(selectedTrendID)
      ),
      switchMap(([action, id, time, trend]) => {
        const start = time?.start?.getTime().toString();
        const end = time?.end?.getTime().toString();

        updateRouteWithoutReloading(
          id,
          trend,
          start,
          end,
          this.router,
          this.activatedRoute
        );
        return [
          NavActions.updateNavigationItems({
            urlParams: { id, trend, start, end },
          }),
        ];
      })
    )
  );

  assetTreeDropdownStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.assetTreeDropdownStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeDropdownState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => dataExplorerActions.assetTreeDropdownStateChange(n)),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) =>
              of(dataExplorerActions.assetTreeDropdownDataRequestFailure(error))
            )
          )
      )
    )
  );

  saveModeAssetTreeDropdownStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.saveModelAssetTreeDropdownStateChange),
      withLatestFrom(
        this.store.pipe(select(selectSaveModeltAssetTreeDropdownState))
      ),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) =>
              dataExplorerActions.saveModelAssetTreeDropdownStateChange(n)
            ),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) =>
              of(
                dataExplorerActions.saveModelAssetDropdownDataRequestFailure(
                  error
                )
              )
            )
          )
      )
    )
  );
}
