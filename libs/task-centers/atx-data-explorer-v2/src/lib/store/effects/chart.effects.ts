/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { copyToClipboard } from '@atonix/atx-chart';
import {
  copyLink,
  copyLinkFailure,
  copyLinkSuccess,
} from '../actions/chart.actions';

@Injectable()
export class ChartEffects {
  constructor(private actions$: Actions) {}

  copyLink$ = createEffect(() =>
    this.actions$.pipe(
      ofType(copyLink),
      mergeMap((payload) =>
        copyToClipboard(payload.link).pipe(
          map(
            () => copyLinkSuccess(),
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            catchError((error) => of(copyLinkFailure(error)))
          )
        )
      )
    )
  );
}
