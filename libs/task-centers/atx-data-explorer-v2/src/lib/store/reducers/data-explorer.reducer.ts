/* eslint-disable ngrx/on-function-explicit-return-type */
import { Update } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import {
  ITreeState,
  ITreeStateChange,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import {
  ITimeSliderStateChange,
  alterTimeSliderState,
  getDefaultTimeSlider,
} from '@atonix/atx-time-slider';

import * as assetTreeActions from '../actions/asset-tree.actions';
import * as timeSliderActions from '../actions/time-slider.actions';
import * as chartActions from '../actions/chart.actions';
import {
  IDataExplorerState,
  initialDataExplorerState,
} from '../state/data-explorer-state';

import * as dataExplorerActions from '../actions/data-explorer.action';
import { ICriteria, ITreePermissions } from '@atonix/atx-core';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
const _dataExplorerReducer = createReducer(
  initialDataExplorerState,
  on(dataExplorerActions.updateSelectedTrend, (state, action) => {
    return {
      ...state,
      selectedTrendID: action.trendID,
    };
  }),
  on(
    dataExplorerActions.assetTreeDropdownStateChange,
    assetTreeDropdownStateChange
  ),
  on(assetTreeActions.getPermissions, getPermissions),
  on(assetTreeActions.getPermissionsSuccess, getPermissionsSuccess),
  on(assetTreeActions.getPermissionsFailure, getPermissionsFailure),
  on(assetTreeActions.treeStateChange, treeStateChange),
  on(assetTreeActions.getAssetTreeDataFailure, treeDataFailed),
  on(assetTreeActions.treeSizeChange, treeSizeChange),
  on(timeSliderActions.initializeTimeSlider, initializeTimeSliderState),
  on(timeSliderActions.timeSliderStateChange, timeSliderStateChange),
  on(chartActions.copyLinkSuccess, copyLinkSuccess),
  on(chartActions.copyLinkFailure, copyLinkFailure),
  on(dataExplorerActions.maximumNewSeriesReached, (state, action) => {
    return {
      ...state,
      Toast: {
        message: `You have reached the maximum number of series for a single chart (500). Please choose a smaller selection of tags or delete unneeded series from the Series tab.`,
        type: 'info',
      },
    };
  }),
  on(
    dataExplorerActions.saveModelAssetTreeDropdownStateChange,
    saveModelAssetTreeDropdownStateChange
  )
);

// Asset Tree
export function getPermissions(state: IDataExplorerState) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsSuccess(
  state: IDataExplorerState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(state.AssetTreeState.treeConfiguration, {
      canView: payload.canView,
      canEdit: false,
      canDelete: false,
      canAdd: false,
    }),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsFailure(state: IDataExplorerState) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function treeStateChange(
  state: IDataExplorerState,
  payload: ITreeStateChange
) {
  const newPayload = treeRetrievedStateChange(payload);
  return {
    ...state,
    AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
  };
}
export function timeSliderStateChange(
  state: IDataExplorerState,
  payload: ITimeSliderStateChange
) {
  return {
    ...state,
    TimeSliderState: alterTimeSliderState(state.TimeSliderState, payload),
  };
}
export function treeDataFailed(state: IDataExplorerState, payload: Error) {
  return { ...state };
}

export function treeSizeChange(
  state: IDataExplorerState,
  payload: { value: number }
) {
  return { ...state, LeftTraySize: payload.value };
}

// Time Slider
export function initializeTimeSliderState(
  state: IDataExplorerState,
  payload: { start: Date; end: Date }
) {
  return {
    ...state,
    TimeSliderState: getDefaultTimeSlider({
      dateIndicator: 'Range',
      startDate: payload.start,
      endDate: payload.end,
    }),
  };
}

export function assetTreeDropdownStateChange(
  state: IDataExplorerState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    TagListState: {
      ...state.TagListState,
      AssetTreeDropdownState: alterAssetTreeState(
        state.TagListState.AssetTreeDropdownState,
        payload
      ),
    },
  };
}

function copyLinkSuccess(state: IDataExplorerState): IDataExplorerState {
  return {
    ...state,
    Toast: { message: 'Link Copied!', type: 'success' },
  };
}

function copyLinkFailure(state: IDataExplorerState): IDataExplorerState {
  return {
    ...state,
    Toast: { message: 'Error Copying', type: 'error' },
  };
}

export function saveModelAssetTreeDropdownStateChange(
  state: IDataExplorerState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    SaveModelState: {
      ...state.SaveModelState,
      AssetTreeDropdownState: alterAssetTreeState(
        state.SaveModelState.AssetTreeDropdownState,
        payload
      ),
    },
  };
}

export function dataExplorerReducer(state, action) {
  return _dataExplorerReducer(state, action);
}
