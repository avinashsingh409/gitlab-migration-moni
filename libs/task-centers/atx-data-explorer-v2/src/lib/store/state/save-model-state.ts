import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
} from '@atonix/atx-asset-tree';

export interface ISaveModelState {
  AssetTreeDropdownState: ITreeState;
}

export const initialSaveModelState: ISaveModelState = {
  AssetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: true }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
};
