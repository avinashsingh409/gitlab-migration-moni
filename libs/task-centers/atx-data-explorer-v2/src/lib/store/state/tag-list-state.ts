import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
} from '@atonix/atx-asset-tree';

export interface ITagListState {
  AssetTreeDropdownState: ITreeState;
}

export const initialTagListState: ITagListState = {
  AssetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
};
