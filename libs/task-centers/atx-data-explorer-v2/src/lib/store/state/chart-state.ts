import { IProcessedTrend } from '@atonix/atx-core';
import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';

export function sortByName(a: IProcessedTrend, b: IProcessedTrend) {
  return a.label.localeCompare(b.label);
}

export const adapter: EntityAdapter<IProcessedTrend> =
  createEntityAdapter<IProcessedTrend>({
    sortComparer: sortByName,
  });

export interface IChartState extends EntityState<IProcessedTrend> {
  selectedDataExplorerTrendID: string;
  loadingTrends: boolean;
  loadingData: boolean;
}

export const initialChartState: IChartState = adapter.getInitialState({
  selectedDataExplorerTrendID: null,
  loadingTrends: false,
  loadingData: false,
});

export interface IToast {
  message: string;
  type: 'success' | 'error' | 'info';
}
