import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { IChartState, initialChartState, IToast } from './chart-state';
import { ITimeSliderState } from '@atonix/atx-time-slider';
import { initialTagListState, ITagListState } from './tag-list-state';
import { initialSaveModelState, ISaveModelState } from './save-model-state';

export interface IDataExplorerState {
  AssetTreeState: ITreeState;
  Toast: IToast;
  selectedTrendID: string;
  LeftTraySize: number;
  ChartHeight: number;
  TimeSliderState: ITimeSliderState;
  TagListState: ITagListState;
  SaveModelState: ISaveModelState;
}

export const initialDataExplorerState: IDataExplorerState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({
      pin: true,
      collapseOthers: false,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  selectedTrendID: null,
  Toast: null,
  LeftTraySize: 250,
  ChartHeight: 500,
  TimeSliderState: null,
  TagListState: initialTagListState,
  SaveModelState: initialSaveModelState,
};
