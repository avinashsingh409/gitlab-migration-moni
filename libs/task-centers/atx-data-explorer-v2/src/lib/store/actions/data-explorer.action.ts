/* eslint-disable ngrx/prefer-inline-action-props */
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { createAction, props } from '@ngrx/store';

export const dataExplorerInitialize = createAction(
  '[Data Explorer] Initialize',
  props<{ asset?: string; trend?: string; start: Date; end: Date }>()
);

export const updateSelectedTrend = createAction(
  '[Data Explorer] Select Trend',
  props<{ trendID: string }>()
);

export const updateRoute = createAction('[Data Explorer] Update Route');

export const maximumNewSeriesReached = createAction(
  '[Data Explorer - Series Tab] Maximum New Series Reached'
);

// Asset Tree Dropdown
export const assetTreeDropdownStateChange = createAction(
  '[Data Explorer - Asset Tree Dropdown] Tree State Change',
  props<ITreeStateChange>()
);

export const assetTreeDropdownDataRequestFailure = createAction(
  '[Data Explorer - Asset Tree Dropdown] Tree Data Request Failure',
  props<Error>()
);

//Save Model Asset Tree Dropdown
export const saveModelAssetTreeDropdownStateChange = createAction(
  '[Data Explorer - Save Model Asset Tree Dropdown] Tree State Change',
  props<ITreeStateChange>()
);

export const saveModelAssetDropdownDataRequestFailure = createAction(
  '[Data Explorer - Save Model Asset Tree Dropdown] Tree Data Request Failure',
  props<Error>()
);
