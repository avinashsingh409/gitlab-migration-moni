import { createAction, props } from '@ngrx/store';

export const copyLink = createAction(
  '[Charts] Copy Chart Link',
  props<{ link: string }>()
);

export const copyLinkSuccess = createAction('[Charts] Copy Chart Link Success');

export const copyLinkFailure = createAction(
  '[Charts] Copy Chart Link Failure',
  props<{ error: any }>()
);
