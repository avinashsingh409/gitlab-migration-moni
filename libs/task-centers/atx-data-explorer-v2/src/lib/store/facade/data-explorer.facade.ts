/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */

import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  leftTraySize,
  chartHeight,
  selectToast,
  selectLayoutMode,
  selectSelectedAsset,
  selectSelectedAssetIds,
  selectAssetTreeDropdownConfiguration,
  selectAssetTreeDropdownAssetName,
  selectAssetTreeDropdownSelectedAssetIds,
  saveModelAssetTreeDropdownConfiguration,
  saveModelAssetTreeDropdownAssetName,
  saveModelAssetTreeDropdownSelectedAssetId,
  saveModelAssetTreeDropdownSelectedAssetGuid,
} from '../selectors/data-explorer.selector';
import * as actions from '../actions/data-explorer.action';
import { updateSelectedTrend } from '../actions/data-explorer.action';
import { ITreeStateChange } from '@atonix/atx-asset-tree';

@Injectable()
export class DataExplorerFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  layoutMode$ = this.store.pipe(select(selectLayoutMode));
  leftTraySize$ = this.store.pipe(select(leftTraySize));
  selectToast$ = this.store.pipe(select(selectToast));

  chartHeight$ = this.store.pipe(select(chartHeight));
  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  selectedAssetIds$ = this.store.pipe(select(selectSelectedAssetIds));
  selectedAssetUniqueKey$ = this.store.pipe(select(selectSelectedAsset));

  selectAssetTreeDropdownConfiguration$ = this.store.pipe(
    select(selectAssetTreeDropdownConfiguration)
  );

  selectAssetTreeDropdownAssetName$ = this.store.pipe(
    select(selectAssetTreeDropdownAssetName)
  );

  selectAssetTreeDropdownSelectedAssetIds$ = this.store.pipe(
    select(selectAssetTreeDropdownSelectedAssetIds)
  );

  saveModelAssetTreeDropdownConfiguration$ = this.store.pipe(
    select(saveModelAssetTreeDropdownConfiguration)
  );

  saveModelAssetTreeDropdownAssetName$ = this.store.pipe(
    select(saveModelAssetTreeDropdownAssetName)
  );

  saveModelAssetTreeDropdownSelectedAssetId$ = this.store.pipe(
    select(saveModelAssetTreeDropdownSelectedAssetId)
  );

  saveModelAssetTreeDropdownSelectedAssetGuid = this.store.pipe(
    select(saveModelAssetTreeDropdownSelectedAssetGuid)
  );

  initializeDataExplorer(asset: string, trend: string, start: Date, end: Date) {
    this.store.dispatch(
      actions.dataExplorerInitialize({ asset, trend, start, end })
    );
  }

  updateTrendID(selectedTrendID: string) {
    this.store.dispatch(updateSelectedTrend({ trendID: selectedTrendID }));
    this.updateRoute();
  }

  updateRoute() {
    this.store.dispatch(actions.updateRoute());
  }

  maximumNewSeriesReached() {
    this.store.dispatch(actions.maximumNewSeriesReached());
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.assetTreeDropdownStateChange(change));
  }

  saveModelAssetTreeDropdownStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.saveModelAssetTreeDropdownStateChange(change));
  }
}
