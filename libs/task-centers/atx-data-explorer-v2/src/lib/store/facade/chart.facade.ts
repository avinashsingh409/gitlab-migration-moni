/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IDataExplorerState } from '../state/data-explorer-state';

import { copyLink } from '../actions/chart.actions';

@Injectable()
export class ChartActionsFacade {
  constructor(private store: Store<IDataExplorerState>) {}

  copyLink(link: string) {
    this.store.dispatch(copyLink({ link }));
  }
}
