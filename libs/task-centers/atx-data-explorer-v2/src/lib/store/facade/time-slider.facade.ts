/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IDataExplorerState } from '../state/data-explorer-state';
import {
  selectTimeSliderState,
  selectTimeSliderSelection,
  selectEndingDate,
  selectBeginningDate,
} from '../selectors/data-explorer.selector';

import * as timeSliderStateActions from '../actions/time-slider.actions';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';

@Injectable()
export class TimeSliderFacade {
  constructor(private store: Store<IDataExplorerState>) {}

  timeSliderState$ = this.store.pipe(select(selectTimeSliderState));
  timeSliderSelection$ = this.store.pipe(select(selectTimeSliderSelection));
  beginningDate$ = this.store.pipe(select(selectBeginningDate));
  endingDate$ = this.store.pipe(select(selectEndingDate));

  timeSliderStateChange(change: ITimeSliderStateChange) {
    this.store.dispatch(timeSliderStateActions.timeSliderStateChange(change));
  }
  hideTimeSlider() {
    this.store.dispatch(timeSliderStateActions.hideTimeSlider());
  }
}
