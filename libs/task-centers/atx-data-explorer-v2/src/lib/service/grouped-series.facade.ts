import { Injectable, OnDestroy, OnInit } from '@angular/core';
import {
  ChartFacade,
  DataSourceValues,
  SeriesState,
} from '@atonix/atx-chart-v2';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take, takeUntil } from 'rxjs/operators';
import { produce } from 'immer';
import {
  IPDTag,
  IPDTrendBandAxisMeasurement,
  isNil,
  IGSMeasurement,
} from '@atonix/atx-core';
import { DataExplorerCoreService } from '@atonix/shared/api';

export interface GroupedSeriesState {
  series: SeriesState[];
  chartID: string;
  tagIDs: IPDTag[];
  tagsLoaded: boolean;
  isDirty: boolean;
}

const _initialState: GroupedSeriesState = {
  series: [],
  chartID: '',
  isDirty: false,
  tagsLoaded: false,
  tagIDs: [],
};

let _state: GroupedSeriesState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class GroupedSeriesFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<GroupedSeriesState>(_state);
  private state$ = this.store.asObservable();

  constructor(
    private chartFacade: ChartFacade,
    private dataExplorerCoreService: DataExplorerCoreService
  ) {}

  series$ = this.state$.pipe(
    map((state) => state.series),
    distinctUntilChanged()
  );

  chartID$ = this.state$.pipe(
    map((state) => state.chartID),
    distinctUntilChanged()
  );
  isDirty$ = this.state$.pipe(
    map((state) => state.isDirty),
    distinctUntilChanged()
  );

  tagIDs$ = this.state$.pipe(
    map((state) => state.tagIDs),
    distinctUntilChanged()
  );

  tagsLoaded$ = this.state$.pipe(
    map((state) => state.tagsLoaded),
    distinctUntilChanged()
  );

  vm$: Observable<GroupedSeriesState> = combineLatest([
    this.isDirty$,
    this.series$,
    this.chartID$,
    this.tagIDs$,
    this.tagsLoaded$,
  ]).pipe(
    map(
      ([isDirty, series, chartID, tagIDs, tagsLoaded]) =>
        ({
          isDirty,
          series,
          chartID,
          tagIDs,
          tagsLoaded,
        } as GroupedSeriesState)
    )
  );

  tagID1Change(tagID: number, seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.series, (state) => {
        state[seriesIndex].tagID1.currentValue = tagID;
        state[seriesIndex].tagID1.valueChanged = true;
      });
      this.updateState({ ..._state, series: newState, isDirty: true });
    });
  }

  tagID2Change(tagID: number, seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.series, (state) => {
        state[seriesIndex].tagID2.currentValue = tagID;
        state[seriesIndex].tagID2.valueChanged = true;
      });
      this.updateState({ ..._state, series: newState, isDirty: true });
    });
  }

  tagID3Change(tagID: number, seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.series, (state) => {
        state[seriesIndex].tagID3.currentValue = tagID;
        state[seriesIndex].tagID3.valueChanged = true;
      });
      this.updateState({ ..._state, series: newState, isDirty: true });
    });
  }

  tagID4Change(tagID: number, seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.series, (state) => {
        state[seriesIndex].tagID4.currentValue = tagID;
        state[seriesIndex].tagID4.valueChanged = true;
      });
      this.updateState({ ..._state, series: newState, isDirty: true });
    });
  }

  tagID5Change(tagID: number, seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.series, (state) => {
        state[seriesIndex].tagID5.currentValue = tagID;
        state[seriesIndex].tagID5.valueChanged = true;
      });
      this.updateState({ ..._state, series: newState, isDirty: true });
    });
  }

  loadSeries(series: IGSMeasurement[], chartID: string) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      let result = new Set([
        ...new Set(
          series.map((a) => a.TagID1).filter((value) => value !== null)
        ),
        ...new Set(
          series.map((a) => a.TagID2).filter((value) => value !== null)
        ),
        ...new Set(
          series.map((a) => a.TagID3).filter((value) => value !== null)
        ),
        ...new Set(
          series.map((a) => a.TagID4).filter((value) => value !== null)
        ),
        ...new Set(
          series.map((a) => a.TagID5).filter((value) => value !== null)
        ),
      ]);
      let updateTagIDs = true;
      if (vm.chartID === chartID) {
        // they have the same chart as before but they may have added or removed
        // series TagIDs make sure we didn't loose a TagID from the list of options
        const currentTagIDs = vm.tagIDs
          .map((t) => t.PDTagID)
          .filter((value) => value !== null);
        const areSetsEqual = (a, b) =>
          a.size === b.size ? [...a].every((value) => b.has(value)) : false;
        if (areSetsEqual(currentTagIDs, result)) {
          updateTagIDs = false;
        } else {
          result = new Set([...result, ...currentTagIDs]);
        }
      } else {
        this.updateState({ ..._state, isDirty: false });
      }

      this.updateState({ ..._state, chartID, tagsLoaded: false });
      if (updateTagIDs && Array.from(result).length > 0) {
        this.dataExplorerCoreService
          .getTags(Array.from(result))
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe(
            (tagList) => {
              this.updateState({
                ..._state,
                tagIDs: tagList,
                tagsLoaded: true,
              });
            },
            (error: unknown) => {
              console.error(JSON.stringify(error));
              this.updateState({ ..._state, tagsLoaded: true });
            }
          );
      }

      const seriesArray: SeriesState[] = [];
      series.forEach((s, sIndex) => {
        const seriesState: SeriesState = {
          seriesType: {
            previousValue: s.Type,
            currentValue: s.Type,
            valueChanged: false,
          },
          dataSource1: {
            previousValue: !isNil(s.Value1)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            currentValue: !isNil(s.Value1)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            valueChanged: false,
          },
          dataSource2: {
            previousValue: !isNil(s.Value2)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            currentValue: !isNil(s.Value2)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            valueChanged: false,
          },
          dataSource3: {
            previousValue: !isNil(s.Value3)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            currentValue: !isNil(s.Value3)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            valueChanged: false,
          },
          dataSource4: {
            previousValue: !isNil(s.Value4)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            currentValue: !isNil(s.Value4)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            valueChanged: false,
          },
          dataSource5: {
            previousValue: !isNil(s.Value5)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            currentValue: !isNil(s.Value5)
              ? DataSourceValues.Constant.valueOf()
              : DataSourceValues.Tag.valueOf(),
            valueChanged: false,
          },
          aggregationType1: {
            previousValue: s.AggregationType1 ?? 3,
            currentValue: s.AggregationType1 ?? 3,
            valueChanged: false,
          },
          aggregationType2: {
            previousValue: s.AggregationType2 ?? 3,
            currentValue: s.AggregationType2 ?? 3,
            valueChanged: false,
          },
          aggregationType3: {
            previousValue: s.AggregationType3 ?? 3,
            currentValue: s.AggregationType3 ?? 3,
            valueChanged: false,
          },
          aggregationType4: {
            previousValue: s.AggregationType4 ?? 3,
            currentValue: s.AggregationType4 ?? 3,
            valueChanged: false,
          },
          aggregationType5: {
            previousValue: s.AggregationType5 ?? 3,
            currentValue: s.AggregationType5 ?? 3,
            valueChanged: false,
          },
          params1: {
            previousValue: +s.Params1 ?? 0,
            currentValue: +s.Params1 ?? 0,
            valueChanged: false,
          },
          params2: {
            previousValue: +s.Params2 ?? 0,
            currentValue: +s.Params2 ?? 0,
            valueChanged: false,
          },
          params3: {
            previousValue: +s.Params3 ?? 0,
            currentValue: +s.Params3 ?? 0,
            valueChanged: false,
          },
          params4: {
            previousValue: +s.Params4 ?? 0,
            currentValue: +s.Params4 ?? 0,
            valueChanged: false,
          },
          params5: {
            previousValue: +s.Params5 ?? 0,
            currentValue: +s.Params5 ?? 0,
            valueChanged: false,
          },
          value1: {
            previousValue: s.Value1,
            currentValue: s.Value1,
            valueChanged: false,
          },
          value2: {
            previousValue: s.Value2,
            currentValue: s.Value2,
            valueChanged: false,
          },
          value3: {
            previousValue: s.Value3,
            currentValue: s.Value3,
            valueChanged: false,
          },
          value4: {
            previousValue: s.Value4,
            currentValue: s.Value4,
            valueChanged: false,
          },
          value5: {
            previousValue: s.Value5,
            currentValue: s.Value5,
            valueChanged: false,
          },
          tagID1: {
            previousValue: s.TagID1,
            currentValue: s.TagID1,
            valueChanged: false,
          },
          tagID2: {
            previousValue: s.TagID2,
            currentValue: s.TagID2,
            valueChanged: false,
          },
          tagID3: {
            previousValue: s.TagID3,
            currentValue: s.TagID3,
            valueChanged: false,
          },
          tagID4: {
            previousValue: s.TagID4,
            currentValue: s.TagID4,
            valueChanged: false,
          },
          tagID5: {
            previousValue: s.TagID5,
            currentValue: s.TagID5,
            valueChanged: false,
          },
          symbol: {
            previousValue: s.Symbol ?? 'triangle',
            currentValue: s.Symbol ?? 'triangle',
            valueChanged: false,
          },
          outlierType: {
            previousValue: s.Flags ?? '',
            currentValue: s.Flags ?? '',
            valueChanged: false,
          },
          groupName: {
            previousValue: s.BarName,
            currentValue: s.BarName,
            valueChanged: false,
          },
        };
        seriesArray.push(seriesState);
      });
      this.updateState({ ..._state, series: seriesArray });
    });
  }

  updateAndMarkDirty(newValues: Partial<GroupedSeriesState>) {
    this.updateState({
      ..._state,
      ...newValues,
      isDirty: true,
    });
  }

  update(newValues: Partial<GroupedSeriesState>) {
    this.updateState({
      ..._state,
      ...newValues,
    });
  }

  reorderSeries(previousIndex: number, currentIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newSeries = produce(vm.series, (state) => {
        const removedSeries = state.splice(previousIndex, 1);
        state.splice(currentIndex, 0, removedSeries[0]);
      });
      this.updateState({ ..._state, series: newSeries });
    });
  }

  deleteSeries(seriesIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newSeries = produce(vm.series, (state) => {
        if (vm.series.length > seriesIndex) {
          state.splice(seriesIndex, 1);
        }
      });
      this.updateState({ ..._state, series: newSeries });
      this.checkDirty();
    });
  }

  checkDirty() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      let isDirty = false;
      vm.series.forEach((series) => {
        if (isDirty) {
          return;
        }
        if (
          series.aggregationType1.valueChanged ||
          series.aggregationType2.valueChanged ||
          series.aggregationType3.valueChanged ||
          series.aggregationType4.valueChanged ||
          series.tagID1.valueChanged ||
          series.tagID2.valueChanged ||
          series.value1.valueChanged ||
          series.value2.valueChanged
        ) {
          isDirty = true;
        }
      });
      this.updateState({ ..._state, isDirty });
    });
  }
  reset() {
    this.updateState({ ..._initialState });
  }

  clearGroupedSeries() {
    this.updateState({ ..._state, series: [], isDirty: false });
  }

  saveGroupedSeries(chartID: string, tagIDs: IPDTag[], tagsLoaded: boolean) {
    this.updateState({
      ..._initialState,
      chartID,
      tagIDs,
      tagsLoaded,
      isDirty: false,
    });
  }

  private updateState(newState: GroupedSeriesState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
