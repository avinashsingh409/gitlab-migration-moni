import { Inject, Injectable, LOCALE_ID, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  ChartFacade,
  dynamicPinCreate,
  pinNameUpdate,
} from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { produce } from 'immer';
import moment from 'moment';

@Injectable()
export class PinsFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(
    private chartFacade: ChartFacade,
    @Inject(LOCALE_ID) private locale: string
  ) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
    // console.log('destroy service');
  }

  resetPins() {
    this.subs.unsubscribe();
    // console.log('reset service');
  }

  pinName(pinIndex: number, name: string): UntypedFormControl {
    name = name.trim();
    const axisUnits = new UntypedFormControl(name, [
      Validators.required,
      Validators.min(1),
    ]);
    this.subs.add(
      axisUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.pinUpdate(pinIndex, value);
        })
    );
    return axisUnits;
  }

  pinStartDate(startDateVal: string | Date): UntypedFormControl {
    const startDate = new UntypedFormControl(
      moment(startDateVal).format('MM/DD/YY')
    );
    return startDate;
  }
  pinStartTime(startTimeVal: string | Date): UntypedFormControl {
    const startTime = new UntypedFormControl(
      moment(startTimeVal).format('hh:mm a')
    );
    return startTime;
  }
  pinEndDate(endDateVal: string | Date): UntypedFormControl {
    const endDate = new UntypedFormControl(
      moment(endDateVal).format('MM/DD/YY')
    );
    return endDate;
  }
  pinEndTime(endTimeVal: string | Date): UntypedFormControl {
    const endTime = new UntypedFormControl(
      moment(endTimeVal).format('hh:mm a')
    );
    return endTime;
  }

  pinUpdate(pinIndex: number, value: string) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.Pins[pinIndex].NameFormat = value;
        // nameFormat is the raw text
        // which may include special formatting in braces {}
        // name is the rendered string used in the chart
        const newValue = pinNameUpdate(
          value,
          vm.selectedTrend.trendDefinition.Pins[pinIndex].StartTime,
          vm.selectedTrend.trendDefinition.Pins[pinIndex].EndTime,
          this.locale
        );
        state.trendDefinition.Pins[pinIndex].Name = newValue;
        const isDirty = produce(vm.isDirty, (state) => {
          state[vm.selectedTrend.id] = true;
        });
        this.chartFacade.setDirty(isDirty);
      });
      this.chartFacade.setSelectedTrend(newState);
    });
  }

  reorderPin(prevIndex: number, curIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        const removedPin = state.trendDefinition.Pins.splice(prevIndex, 1);
        state.trendDefinition.Pins.splice(curIndex, 0, removedPin[0]);
        state.trendDefinition.Pins = state.trendDefinition.Pins.map(
          (pin, index) => {
            const newPin = { ...pin };
            newPin.DisplayOrder = index + 1;
            return newPin;
          }
        );
      });
      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setPinFormElements(newState.trendDefinition.Pins);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }
}
