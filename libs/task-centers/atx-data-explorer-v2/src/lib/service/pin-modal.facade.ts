import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import * as moment from 'moment';
import {
  checkStartAndEndTimes,
  combineTime,
  TimeErrorStateMatcher,
} from '@atonix/shared/utils';
import { ICriteria } from '@atonix/atx-core';

export interface PinModalState {
  selectedID: number;
  timeFilters: ICriteria[];
  selectedFilter: ICriteria;
  filterTypeID: number;
  startTime: string;
  startDate: Date;
  endTime: string;
  endDate: Date;
  startDateAfterEndDate: boolean;
  startPickerStartAt: Date;
  endPickerStartAt: Date;
  invalidStartDateFormat: boolean;
  invalidEndDateFormat: boolean;
  startMin: Date;
  startMax: Date;
  endMin: Date;
  endMax: Date;
  span: number;
  spanID: string;
  temporalTypes: string[];
}

const _initialState: PinModalState = {
  selectedID: null,
  timeFilters: null,
  selectedFilter: null,
  filterTypeID: 1,
  startTime: null,
  startDate: null,
  endTime: null,
  endDate: null,
  startDateAfterEndDate: false,
  startPickerStartAt: null,
  endPickerStartAt: null,
  invalidStartDateFormat: false,
  invalidEndDateFormat: false,
  startMin: null,
  startMax: null,
  endMin: null,
  endMax: null,
  span: 0,
  spanID: 'Days',
  temporalTypes: ['Years', 'Months', 'Weeks', 'Days', 'Hours'],
};

let _state: PinModalState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class PinModalFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<PinModalState>(_state);
  private state$ = this.store.asObservable();
  startDateMatcher: TimeErrorStateMatcher;
  endDateMatcher: TimeErrorStateMatcher;

  private selectedID$ = this.state$.pipe(
    map((state) => state.selectedID),
    distinctUntilChanged()
  );

  private timeFilters$ = this.state$.pipe(
    map((state) => state.timeFilters),
    distinctUntilChanged()
  );
  private selectedFilter$ = this.state$.pipe(
    map((state) => state.selectedFilter),
    distinctUntilChanged()
  );
  private startTime$ = this.state$.pipe(
    map((state) => state.startTime),
    distinctUntilChanged()
  );
  private startDate$ = this.state$.pipe(
    map((state) => state.startDate),
    distinctUntilChanged()
  );
  private endTime$ = this.state$.pipe(
    map((state) => state.endTime),
    distinctUntilChanged()
  );
  private endDate$ = this.state$.pipe(
    map((state) => state.endDate),
    distinctUntilChanged()
  );
  startDateAfterEndDate$ = this.state$.pipe(
    map((state) => state.startDateAfterEndDate),
    distinctUntilChanged()
  );
  private startPickerStartAt$ = this.state$.pipe(
    map((state) => state.startPickerStartAt),
    distinctUntilChanged()
  );
  private endPickerStartAt$ = this.state$.pipe(
    map((state) => state.endPickerStartAt),
    distinctUntilChanged()
  );
  private invalidStartDateFormat$ = this.state$.pipe(
    map((state) => state.invalidStartDateFormat),
    distinctUntilChanged()
  );
  private invalidEndDateFormat$ = this.state$.pipe(
    map((state) => state.invalidEndDateFormat),
    distinctUntilChanged()
  );
  private startMin$ = this.state$.pipe(
    map((state) => state.startMin),
    distinctUntilChanged()
  );
  private startMax$ = this.state$.pipe(
    map((state) => state.startMax),
    distinctUntilChanged()
  );
  private endMin$ = this.state$.pipe(
    map((state) => state.endMin),
    distinctUntilChanged()
  );
  private endMax$ = this.state$.pipe(
    map((state) => state.endMax),
    distinctUntilChanged()
  );
  private span$ = this.state$.pipe(
    map((state) => state.span),
    distinctUntilChanged()
  );
  filterTypeID$ = this.state$.pipe(
    map((state) => state.filterTypeID),
    distinctUntilChanged()
  );
  private spanID$ = this.state$.pipe(
    map((state) => state.spanID),
    distinctUntilChanged()
  );

  private temporalTypes$ = this.state$.pipe(
    map((state) => state.temporalTypes),
    distinctUntilChanged()
  );

  vm$: Observable<PinModalState> = combineLatest([
    this.selectedID$,
    this.timeFilters$,
    this.selectedFilter$,
    this.startTime$,
    this.startDate$,
    this.endTime$,
    this.endDate$,
    this.startDateAfterEndDate$,
    this.startPickerStartAt$,
    this.endPickerStartAt$,
    this.invalidStartDateFormat$,
    this.invalidEndDateFormat$,
    this.startMin$,
    this.startMax$,
    this.endMin$,
    this.endMax$,
    this.span$,
    this.filterTypeID$,
    this.spanID$,
    this.temporalTypes$,
  ]).pipe(
    map(
      ([
        selectedID,
        timeFilters,
        selectedFilter,
        startTime,
        startDate,
        endTime,
        endDate,
        startDateAfterEndDate,
        startPickerStartAt,
        endPickerStartAt,
        invalidStartDateFormat,
        invalidEndDateFormat,
        startMin,
        startMax,
        endMin,
        endMax,
        span,
        filterTypeID,
        spanID,
        temporalTypes,
      ]) =>
        ({
          selectedID,
          timeFilters,
          selectedFilter,
          startTime,
          startDate,
          endTime,
          endDate,
          startDateAfterEndDate,
          startPickerStartAt,
          endPickerStartAt,
          invalidStartDateFormat,
          invalidEndDateFormat,
          startMin,
          startMax,
          endMin,
          endMax,
          span,
          filterTypeID,
          spanID,
          temporalTypes,
        } as PinModalState)
    )
  );

  init(timeFilters: ICriteria[], startDate: Date, endDate: Date) {
    const selectedFilter = timeFilters.find(
      (timeFilter) => timeFilter.CoID === -1
    );
    this.updateState({
      ..._state,
      timeFilters,
      startDate,
      endDate,
      selectedID: -1,
      selectedFilter,
    });
  }

  initStartEndTimes(
    startDate: Date,
    startTime: string,
    endDate: Date,
    endTime: string
  ) {
    this.updateState({
      ..._state,
      startDate,
      startTime,
      endDate,
      endTime,
    });
  }

  updateSpanID(spanID: string) {
    this.updateState({ ..._state, spanID });
  }

  updateTimeSpan(
    startDate: Date,
    startTime: string,
    endDate: Date,
    endTime: string,
    spanID: string
  ) {
    const start: Date = combineTime(startDate, startTime);
    const end: Date = combineTime(endDate, endTime);
    const combinedStartDate = moment(start);
    const combinedEndDate = moment(end);
    const timeDiff = moment.duration(combinedEndDate.diff(combinedStartDate));
    let timeSpan = 1;
    switch (spanID) {
      case 'Days':
        timeSpan = Math.round(timeDiff.asDays());
        break;
      case 'Hours':
        timeSpan = Math.round(timeDiff.asHours());
        break;
      case 'Weeks':
        timeSpan = Math.round(timeDiff.asWeeks());
        break;
      case 'Months':
        timeSpan = Math.round(timeDiff.asMonths());
        break;
      case 'Years':
        timeSpan = Math.round(timeDiff.asYears());
        break;
    }
    return timeSpan < 0 ? timeSpan * -1 : timeSpan;
  }

  updateEndDate(startDate: Date, spanID: string, span: number) {
    const endDate = moment(startDate);
    switch (spanID) {
      case 'Hours':
        endDate.add(span, 'hours');
        break;
      case 'Weeks':
        endDate.add(span, 'weeks');
        break;
      case 'Months':
        endDate.add(span, 'months');
        break;
      case 'Years':
        endDate.add(span, 'years');
        break;
      default:
      case 'Days':
        endDate.add(span, 'days');
        break;
    }
    return endDate.toDate();
  }

  updateFilterType(filterTypeID: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (filterTypeID === 2 && vm.filterTypeID === 1) {
        // from start end to span
        const timeSpan = this.updateTimeSpan(
          vm.startDate,
          vm.startTime,
          vm.endDate,
          vm.endTime,
          vm.spanID
        );

        this.updateState({
          ..._state,
          span: timeSpan,
          filterTypeID,
        });
      } else if (filterTypeID === 1 && vm.filterTypeID === 2) {
        // from span to start end
        const start: Date = combineTime(vm.startDate, vm.startTime);
        const endDate = this.updateEndDate(start, vm.spanID, vm.span);
        const endDay = moment(endDate).toDate();
        const endTime = moment(endDate).format('HH:mm');
        this.updateState({
          ..._state,
          endTime,
          endDate: endDay,
          filterTypeID,
        });
      }
    });
  }

  selectTimeFilter(selectedCoID: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const selectedFilter = vm.timeFilters.find(
        (timeFilter) => timeFilter.CoID === selectedCoID
      );
      if (selectedFilter) {
        this.updateState({
          ..._state,
          selectedID: selectedCoID,
          selectedFilter,
        });
      }
    });
  }

  updateEndTime(endTime: string) {
    this.updateState({ ..._state, endTime });
  }
  updateStartTime(startTime: string) {
    this.updateState({ ..._state, startTime });
  }
  updateSpan(span: number) {
    this.updateState({ ..._state, span });
  }

  setStartDateAfterEndDate(startDateAfterEndDate: boolean) {
    this.updateState({ ..._state, startDateAfterEndDate });
  }

  startDateChanged(startDate: Date) {
    let invalidStartDateFormat = false;
    if (!startDate) {
      invalidStartDateFormat = true;
      this.startDateMatcher = new TimeErrorStateMatcher(true);
    }
    if (
      moment(startDate, 'MM/DD/YYYY', true).isValid() ||
      moment(startDate as unknown as string, 'M/D/YYYY', true).isValid()
    ) {
      this.startDateMatcher = new TimeErrorStateMatcher(false);
      this.updateState({
        ..._state,
        endMin: startDate,
        startPickerStartAt: startDate,
        startDate,
        invalidStartDateFormat,
      });
    } else {
      this.updateState({
        ..._state,
        startPickerStartAt: startDate,
        startDate,
        invalidStartDateFormat,
      });
    }
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const isValid = checkStartAndEndTimes(
        vm.startDate,
        vm.startTime,
        vm.endDate,
        vm.endTime
      );
      this.setStartDateAfterEndDate(isValid);
    });
  }

  endDateChanged(endDate: Date) {
    let invalidEndDateFormat = false;
    if (!endDate) {
      invalidEndDateFormat = true;
      this.endDateMatcher = new TimeErrorStateMatcher(true);
    }
    if (
      moment(endDate, 'MM/DD/YYYY', true).isValid() ||
      moment(endDate as unknown as string, 'M/D/YYYY', true).isValid()
    ) {
      this.endDateMatcher = new TimeErrorStateMatcher(false);
      this.updateState({
        ..._state,
        startMax: endDate,
        endPickerStartAt: endDate,
        endDate,
        invalidEndDateFormat,
      });
    } else {
      this.updateState({
        ..._state,
        endPickerStartAt: endDate,
        endDate,
        invalidEndDateFormat,
      });
    }
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const isValid = checkStartAndEndTimes(
        vm.startDate,
        vm.startTime,
        vm.endDate,
        vm.endTime
      );
      this.setStartDateAfterEndDate(isValid);
    });
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: PinModalState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
