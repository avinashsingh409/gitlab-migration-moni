import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  ISelectAsset,
  ITreeStateChange,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { ChartFacade } from '@atonix/atx-chart-v2';
import { IAtxTreeRetrieval, IPDTrend, IProcessedTrend } from '@atonix/atx-core';
import { ProcessDataFrameworkService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  map,
  take,
  takeUntil,
} from 'rxjs/operators';
import { AssetTreeFacade } from '../store/facade/asset-tree.facade';
import { DataExplorerFacade } from '../store/facade/data-explorer.facade';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from './data-explorer-event-bus.service';

export interface SaveChartState {
  newTrendName: string;
  errorMessage: string;
  chartSaveComplete: boolean;
  chartSaveInProgress: boolean;
  newChartID: string;
  oldChartID: string;
  isUnique: boolean;
  treeStateChange: ITreeStateChange;
  chartTrends: IProcessedTrend[];
  selectedTrend: IProcessedTrend;
  isLoading: boolean;
}

const initialState: SaveChartState = {
  newTrendName: '',
  errorMessage: '',
  chartSaveComplete: false,
  chartSaveInProgress: false,
  newChartID: null,
  oldChartID: null,
  isUnique: true,
  treeStateChange: null,
  chartTrends: null,
  selectedTrend: null,
  isLoading: false,
};
let _state = initialState;

@Injectable()
export class SaveModalFacade implements OnDestroy {
  constructor(
    private processData: ProcessDataFrameworkService,
    private assetTreeFacade: AssetTreeFacade,
    private dataExplorerFacade: DataExplorerFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private chartFacade: ChartFacade,
    private processDataFxService: ProcessDataFrameworkService,
    private toastService: ToastService
  ) {}

  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<SaveChartState>(_state);
  private state$ = this.store.asObservable();

  private newTrendName$ = this.state$.pipe(
    map((state) => state.newTrendName),
    distinctUntilChanged()
  );

  private errorMessage$ = this.state$.pipe(
    map((state) => state.errorMessage),
    distinctUntilChanged()
  );

  private chartSaveInProgress$ = this.state$.pipe(
    map((state) => state.chartSaveInProgress),
    distinctUntilChanged()
  );

  chartSaveComplete$ = this.state$.pipe(
    map((state) => state.chartSaveComplete),
    distinctUntilChanged()
  );

  private oldChartID$ = this.state$.pipe(
    map((state) => state.oldChartID),
    distinctUntilChanged()
  );

  private newChartID$ = this.state$.pipe(
    map((state) => state.newChartID),
    distinctUntilChanged()
  );

  treeStateChange$ = this.state$.pipe(
    map((state) => state.treeStateChange),
    distinctUntilChanged()
  );

  isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  private isUnique$ = this.state$.pipe(
    map((state) => state.isUnique),
    distinctUntilChanged()
  );

  vm$: Observable<SaveChartState> = combineLatest([
    this.newTrendName$,
    this.errorMessage$,
    this.chartSaveInProgress$,
    this.chartSaveComplete$,
    this.newChartID$,
    this.oldChartID$,
    this.isUnique$,
    this.treeStateChange$,
    this.isUnique$,
    this.isLoading$,
  ]).pipe(
    map(
      ([
        newTrendName,
        errorMessage,
        chartSaveInProgress,
        chartSaveComplete,
        newChartID,
        oldChartID,
        isUnique,
        treeStateChange,
        isLoading,
      ]) => {
        return {
          newTrendName,
          errorMessage,
          chartSaveInProgress,
          chartSaveComplete,
          newChartID,
          oldChartID,
          isUnique,
          treeStateChange,
          isLoading,
        } as SaveChartState;
      }
    )
  );

  chartState$ = this.chartFacade.query.vm$;

  init(): void {
    this.chartState$.pipe(take(1)).subscribe((chartState) => {
      this.updateState({
        ..._state,
        chartTrends: chartState.trends,
        selectedTrend: chartState.selectedTrend,
        newTrendName: chartState.selectedTrend.label,
      });

      this.validateTrendName();
    });
  }

  buildTrendNameFormControl(name: string): UntypedFormControl {
    const ctrl = new UntypedFormControl(name, [Validators.required]);
    ctrl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((val: string) => {
        this.updateState({
          ..._state,
          newTrendName: val,
        });
        const trends = [..._state.chartTrends];
        const selectedTrend = { ..._state.selectedTrend };

        if (trends) {
          const trend = trends.filter(
            (x) => x.label.toLowerCase().trim() === val.toLowerCase().trim()
          )[0];
          if (trend) {
            if (selectedTrend.isNew) {
              this.updateState({
                ..._state,
                isUnique: false,
                errorMessage: `There is already a chart named ${val}`,
              });
            } else {
              if (
                selectedTrend.label.toLowerCase().trim() !==
                val.toLowerCase().trim()
              ) {
                this.updateState({
                  ..._state,
                  isUnique: false,
                  errorMessage: `There is already a chart named ${val}`,
                });
              }
            }
          } else {
            this.updateState({
              ..._state,
              isUnique: true,
              errorMessage: null,
            });
          }
        }
      });

    return ctrl;
  }

  refreshCharts(
    newTrendName: string,
    newChartID: string,
    oldChartID: string
  ): void {
    this.chartFacade.refreshCharts(newTrendName, newChartID, oldChartID);
  }

  saveChart(
    newTrendName: string,
    assetID: number,
    endDate: Date,
    startDate: Date,
    trend: IPDTrend,
    trendID: string
  ) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      // straight from legacy AtonixTrend.ts Save function
      const newTrend = { ...trend };
      if (newTrend.IsStandardTrend) {
        newTrend.ParentTrendID = trend.PDTrendID;
        newTrend.PDTrendID = -1;
        newTrend.IsStandardTrend = false;
        newTrend.IsPublic = true;
      }

      this.updateState({ ..._state, newTrendName, chartSaveInProgress: true });
      this.processData
        .savePDTrendForAsset(assetID, endDate, startDate, newTrend)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe({
          next: (newID) => {
            let newChartID = trendID;
            if (newID !== +trendID) {
              newChartID = newID.toString();
            }
            this.updateState({
              ..._state,
              oldChartID: trendID,
              newChartID,
              chartSaveComplete: true,
              chartSaveInProgress: false,
            });

            if (vm.treeStateChange) {
              const newValue = {
                ...(vm.treeStateChange.newValue as ISelectAsset),
              };

              this.assetTreeFacade.treeStateChange(selectAsset(newValue.ids));
              this.dataExplorerFacade.updateTrendID(newChartID);
              this.dataExplorerEventBus.emit(
                new EmitEvent(
                  BusEventTypes.LOAD_DATA_EXPLORER_CHARTS,
                  newChartID
                )
              );
            }
          },
          error: () => {
            this.updateState({
              ..._state,
              chartSaveComplete: true,
              chartSaveInProgress: false,
            });

            this.toastService.openSnackBar(
              'Error occured while saving!',
              'error'
            );
          },
        });
    });
  }

  treeStateChange(change: ITreeStateChange): void {
    this.updateState({
      ..._state,
      treeStateChange: change,
    });
  }

  validateTrendName(): void {
    this.updateState({
      ..._state,
      isUnique: true,
      errorMessage: null,
    });

    const newChartTrends = [..._state.chartTrends];
    const selectedTrend = { ..._state.selectedTrend };
    const newTrendName = _state.newTrendName;

    const trend = newChartTrends?.filter(
      (x) => x.label.toLowerCase().trim() === newTrendName.toLowerCase().trim()
    )[0];

    if (trend) {
      if (selectedTrend.isNew) {
        this.updateState({
          ..._state,
          isUnique: false,
          errorMessage: `There is already a chart named ${newTrendName}`,
        });
      } else {
        if (
          selectedTrend.label.toLowerCase().trim() !==
          newTrendName.toLowerCase().trim()
        ) {
          this.updateState({
            ..._state,
            isUnique: false,
            errorMessage: `There is already a chart named ${newTrendName}`,
          });
        }
      }
    } else {
      this.updateState({
        ..._state,
        isUnique: true,
        errorMessage: null,
      });
    }
  }

  loadNewTrends(): void {
    this.updateState({ ..._state, isLoading: true });
    this.dataExplorerFacade.saveModelAssetTreeDropdownSelectedAssetGuid
      .pipe(take(1))
      .subscribe({
        next: (guid) => {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.processDataFxService.getTrends(guid).subscribe((trends) => {
            this.updateState({
              ..._state,
              chartTrends: trends,
              isLoading: false,
            });
            this.validateTrendName();
          });
        },
        error: (err: unknown) => {
          console.log(err);
          this.updateState({
            ..._state,
            errorMessage:
              'Something went wrong while getting the asset chart trends',
            isUnique: false,
          });
        },
      });
  }

  reset() {
    this.updateState({ ...initialState });
  }
  private updateState(state: SaveChartState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
