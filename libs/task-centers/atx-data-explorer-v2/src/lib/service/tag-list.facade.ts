import { Injectable, OnDestroy } from '@angular/core';
import { ITreeConfiguration } from '@atonix/atx-asset-tree';
import { AssetFrameworkService } from '@atonix/shared/api';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';

export interface TagListState {
  assetTreeConfiguration: ITreeConfiguration;
  assetName: string;
  assetID: string;
  searchAssetID: number;
}

const _initialState: TagListState = {
  assetTreeConfiguration: null,
  assetName: null,
  assetID: null,
  searchAssetID: null,
};

let _state: TagListState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class TagListFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<TagListState>(_state);
  private state$ = this.store.asObservable();

  constructor(private assetFrameworkService: AssetFrameworkService) {}

  assetTreeConfiguration$ = this.state$.pipe(
    map((state) => state.assetTreeConfiguration),
    distinctUntilChanged()
  );

  assetName$ = this.state$.pipe(
    map((state) => state.assetName),
    distinctUntilChanged()
  );

  assetID$ = this.state$.pipe(
    map((state) => state.assetID),
    distinctUntilChanged()
  );

  searchAssetID$ = this.state$.pipe(
    map((state) => state.searchAssetID),
    distinctUntilChanged()
  );

  vm$: Observable<TagListState> = combineLatest([
    this.assetTreeConfiguration$,
    this.assetName$,
    this.assetID$,
    this.searchAssetID$,
  ]).pipe(
    map(
      ([assetTreeConfiguration, assetName, assetID, searchAssetID]) =>
        ({
          assetTreeConfiguration,
          assetName,
          assetID,
          searchAssetID,
        } as TagListState)
    )
  );

  updateAssetTreeConfiguration(assetTreeConfiguration: ITreeConfiguration) {
    this.updateState({
      ..._state,
      assetTreeConfiguration,
    });
  }

  updateAssetName(assetName: string) {
    this.updateState({
      ..._state,
      assetName,
    });
  }

  updateAssetID(assetID: string) {
    this.updateState({
      ..._state,
      assetID,
    });
  }

  updateSearchAssetID(assetID: number) {
    this.updateState({
      ..._state,
      searchAssetID: null,
    });
    this.assetFrameworkService
      .getAssetAndAncestors(assetID)
      .pipe(take(1))
      .subscribe(
        (data) => {
          this.updateState({
            ..._state,
            searchAssetID: data.Unit?.AssetID || data.Asset?.AssetID,
          });
        },
        (error: unknown) => {
          console.error(error);
        }
      );
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: TagListState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
