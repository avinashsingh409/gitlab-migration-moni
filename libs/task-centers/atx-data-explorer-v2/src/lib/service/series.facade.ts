import { Injectable, OnDestroy } from '@angular/core';
import { SeriesFilterState } from '@atonix/atx-chart-v2';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { IPDTrendSeries, isNil } from '@atonix/atx-core';

export interface SeriesState {
  seriesFilter: SeriesFilterState[];
  isDirty: boolean;
}

const _initialState: SeriesState = {
  seriesFilter: [],
  isDirty: false,
};

let _state: SeriesState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class SeriesFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<SeriesState>(_state);
  private state$ = this.store.asObservable();

  seriesFilter$ = this.state$.pipe(
    map((state) => state.seriesFilter),
    distinctUntilChanged()
  );

  isDirty$ = this.state$.pipe(
    map((state) => state.isDirty),
    distinctUntilChanged()
  );

  vm$: Observable<SeriesState> = combineLatest([
    this.seriesFilter$,
    this.isDirty$,
  ]).pipe(
    map(
      ([seriesFilter, isDirty]) =>
        ({
          seriesFilter,
          isDirty,
        } as SeriesState)
    )
  );

  loadSeries(series: IPDTrendSeries[]) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const seriesFilterArray: SeriesFilterState[] = [];
      series.forEach((s, sIndex) => {
        const seriesFiterState: SeriesFilterState = {
          filterMin: {
            previousValue: s.FilterMin,
            currentValue: s.FilterMin,
            valueChanged: false,
          },
          filterMax: {
            previousValue: s.FilterMax,
            currentValue: s.FilterMax,
            valueChanged: false,
          },
          flatlineThreshold: {
            previousValue: s.FlatlineThreshold,
            currentValue: s.FlatlineThreshold,
            valueChanged: false,
          },
          applyFilterToAll: {
            previousValue: s.ApplyToAll,
            currentValue: s.ApplyToAll,
            valueChanged: false,
          },
          isFilterActive:
            (!isNil(s?.FlatlineThreshold) && s?.FlatlineThreshold !== 0) ||
            !isNil(s?.FilterMax) ||
            !isNil(s?.FilterMin) ||
            s?.ApplyToAll
              ? true
              : false,
        };
        seriesFilterArray.push(seriesFiterState);
      });
      this.updateState({ ..._state, seriesFilter: seriesFilterArray });
    });
  }

  updateAndMarkDirty(newValues: Partial<SeriesState>) {
    this.updateState({
      ..._state,
      ...newValues,
      isDirty: true,
    });
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: SeriesState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
