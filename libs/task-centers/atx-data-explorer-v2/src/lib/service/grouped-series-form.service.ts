/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  AtxAggregationAverage,
  AtxAggregationMinimum,
  AtxAggregationPercentile,
  ChartFacade,
  DataSourceValues,
  getBandAxisMeasurementIndex,
  processBandAxisBarsAndMeasurements,
  reflow,
} from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { produce } from 'immer';
import { GroupedSeriesFacade } from './grouped-series.facade';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from './data-explorer-event-bus.service';
import { GroupedSeriesType, isNil } from '@atonix/atx-core';

@Injectable()
export class GroupedSeriesFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(
    private chartFacade: ChartFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private groupedSeriesFacade: GroupedSeriesFacade
  ) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
    // console.log('destroy service');
  }

  resetSeries() {
    this.subs.unsubscribe();
    // console.log('reset service');
  }

  seriesName(index: number, name: string): UntypedFormControl {
    const seriesName = new UntypedFormControl(name, [Validators.required]);
    this.subs.add(
      seriesName.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.groupSeriesMeasurements[index].Name = value;
              processBandAxisBarsAndMeasurements(state);
            });
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
            this.chartFacade.setSelectedTrend(newState);
          });
        })
    );
    return seriesName;
  }

  groupName(index: number, name: string): UntypedFormControl {
    const groupName = new UntypedFormControl(name);
    this.subs.add(
      groupName.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].groupName.currentValue = value;
              state[index].groupName.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({
              series: newState,
              isDirty: true,
            });
          });
        })
    );
    return groupName;
  }

  axisUnits(index: number, axis: number): UntypedFormControl {
    const axisUnits = new UntypedFormControl(axis, []);
    this.subs.add(
      axisUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.groupSeriesMeasurements[index].Axis = value;
              processBandAxisBarsAndMeasurements(state);
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return axisUnits;
  }

  groupedSeriesType(
    index: number,
    seriesTypeInput: string
  ): UntypedFormControl {
    const seriesType = new UntypedFormControl(seriesTypeInput);
    this.subs.add(
      seriesType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].seriesType.currentValue !== value) {
              const newState = produce(vm.series, (state) => {
                if (value == 'boxandwhiskers') {
                  // user has switched to a box and whiskers chart.
                  let newTagID = vm.series[index].tagID1.currentValue;
                  if (!newTagID) {
                    newTagID =
                      vm.tagIDs && vm.tagIDs.length > 0
                        ? vm.tagIDs[0].PDTagID
                        : null;
                  }

                  if (newTagID) {
                    state[index].tagID1.currentValue = newTagID;
                    state[index].tagID1.valueChanged = true;
                    state[index].tagID2.currentValue = newTagID;
                    state[index].tagID2.valueChanged = true;
                    state[index].tagID3.currentValue = newTagID;
                    state[index].tagID3.valueChanged = true;
                    state[index].tagID4.currentValue = newTagID;
                    state[index].tagID4.valueChanged = true;
                    state[index].tagID5.currentValue = newTagID;
                    state[index].tagID5.valueChanged = true;
                  }
                  state[index].params1.currentValue = 95;
                  state[index].params1.valueChanged = true;
                  state[index].params2.currentValue = 75;
                  state[index].params2.valueChanged = true;
                  state[index].params3.currentValue = 50;
                  state[index].params3.valueChanged = true;
                  state[index].params4.currentValue = 25;
                  state[index].params4.valueChanged = true;
                  state[index].params5.currentValue = 5;
                  state[index].params5.valueChanged = true;
                  state[index].value1.currentValue = null;
                  state[index].value1.valueChanged = true;
                  state[index].value2.currentValue = null;
                  state[index].value2.valueChanged = true;
                  state[index].value3.currentValue = null;
                  state[index].value3.valueChanged = true;
                  state[index].value4.currentValue = null;
                  state[index].value4.valueChanged = true;
                  state[index].value5.currentValue = null;
                  state[index].value5.valueChanged = true;
                  state[index].dataSource1.currentValue = DataSourceValues.Tag;
                  state[index].dataSource1.valueChanged = true;
                  state[index].dataSource2.currentValue = DataSourceValues.Tag;
                  state[index].dataSource2.valueChanged = true;
                  state[index].dataSource3.currentValue = DataSourceValues.Tag;
                  state[index].dataSource3.valueChanged = true;
                  state[index].dataSource4.currentValue = DataSourceValues.Tag;
                  state[index].dataSource4.valueChanged = true;
                  state[index].dataSource5.currentValue = DataSourceValues.Tag;
                  state[index].dataSource5.valueChanged = true;
                  state[index].aggregationType1.currentValue =
                    AtxAggregationPercentile;
                  state[index].aggregationType1.valueChanged = true;
                  state[index].aggregationType2.currentValue =
                    AtxAggregationPercentile;
                  state[index].aggregationType2.valueChanged = true;
                  state[index].aggregationType3.currentValue =
                    AtxAggregationPercentile;
                  state[index].aggregationType3.valueChanged = true;
                  state[index].aggregationType4.currentValue =
                    AtxAggregationPercentile;
                  state[index].aggregationType4.valueChanged = true;
                  state[index].aggregationType5.currentValue =
                    AtxAggregationPercentile;
                  state[index].aggregationType5.valueChanged = true;

                  this.dataExplorerEventBus.emit(
                    new EmitEvent(
                      BusEventTypes.GROUPED_SERIES_BOX_AND_WHISKER,
                      { seriesIndex: index }
                    )
                  );
                } else if (
                  vm.series[index].seriesType.currentValue === 'boxandwhiskers'
                ) {
                  // user leaving box and whiskers to something else
                  state[index].params1.currentValue = null;
                  state[index].params1.valueChanged = true;
                  state[index].params2.currentValue = null;
                  state[index].params2.valueChanged = true;
                  state[index].params3.currentValue = null;
                  state[index].params3.valueChanged = true;
                  state[index].params4.currentValue = null;
                  state[index].params4.valueChanged = true;
                  state[index].params5.currentValue = null;
                  state[index].params5.valueChanged = true;
                  state[index].tagID3.currentValue = null;
                  state[index].tagID3.valueChanged = true;
                  state[index].tagID4.currentValue = null;
                  state[index].tagID4.valueChanged = true;
                  state[index].tagID5.currentValue = null;
                  state[index].tagID5.valueChanged = true;

                  state[index].aggregationType1.currentValue =
                    AtxAggregationAverage;
                  state[index].aggregationType1.valueChanged = true;
                  state[index].aggregationType2.currentValue =
                    AtxAggregationAverage;
                  state[index].aggregationType2.valueChanged = true;
                  state[index].aggregationType3.currentValue = null;
                  state[index].aggregationType3.valueChanged = true;
                  state[index].aggregationType4.currentValue = null;
                  state[index].aggregationType4.valueChanged = true;
                  state[index].aggregationType5.currentValue = null;
                  state[index].aggregationType5.valueChanged = true;
                  state[index].value2.currentValue = 0;
                  state[index].value2.valueChanged = true;
                  state[index].dataSource2.currentValue =
                    DataSourceValues.Constant;
                  state[index].dataSource2.valueChanged = true;
                  this.dataExplorerEventBus.emit(
                    new EmitEvent(
                      BusEventTypes.GROUPED_SERIES_BOX_AND_WHISKER_LEAVE,
                      { seriesIndex: index }
                    )
                  );
                }
                state[index].seriesType.currentValue = value;
                state[index].seriesType.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesType;
  }

  groupedSeriesValue1(index: number, value1?: number): UntypedFormControl {
    const seriesValue1 = new UntypedFormControl(value1);
    this.subs.add(
      seriesValue1.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].value1.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].value1.currentValue = +value;
                state[index].value1.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesValue1;
  }

  groupedSeriesValue2(index: number, value1?: number): UntypedFormControl {
    const seriesValue2 = new UntypedFormControl(value1);
    this.subs.add(
      seriesValue2.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].value2.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].value2.currentValue = +value;
                state[index].value2.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesValue2;
  }

  groupedSeriesValue3(index: number, value3?: number): UntypedFormControl {
    const seriesValue3 = new UntypedFormControl(value3);
    this.subs.add(
      seriesValue3.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].value3.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].value3.currentValue = +value;
                state[index].value3.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesValue3;
  }
  groupedSeriesValue4(index: number, value4?: number): UntypedFormControl {
    const seriesValue4 = new UntypedFormControl(value4);
    this.subs.add(
      seriesValue4.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].value4.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].value4.currentValue = +value;
                state[index].value4.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesValue4;
  }
  groupedSeriesValue5(index: number, value5?: number): UntypedFormControl {
    const seriesValue5 = new UntypedFormControl(value5);
    this.subs.add(
      seriesValue5.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].value5.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].value5.currentValue = +value;
                state[index].value5.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesValue5;
  }

  groupedSeriesAggregationType1(
    index: number,
    aggType?: string
  ): UntypedFormControl {
    const aggregationType1 = new UntypedFormControl(aggType);
    this.subs.add(
      aggregationType1.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].aggregationType1.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].aggregationType1.currentValue = +value;
                state[index].aggregationType1.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return aggregationType1;
  }

  groupedSeriesAggregationType2(
    index: number,
    aggType?: string
  ): UntypedFormControl {
    const aggregationType2 = new UntypedFormControl(aggType);
    this.subs.add(
      aggregationType2.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].aggregationType2.currentValue = +value;
              state[index].aggregationType2.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return aggregationType2;
  }

  groupedSeriesAggregationType3(
    index: number,
    aggType?: string
  ): UntypedFormControl {
    const aggregationType3 = new UntypedFormControl(aggType);
    this.subs.add(
      aggregationType3.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].aggregationType3.currentValue = +value;
              state[index].aggregationType3.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return aggregationType3;
  }

  groupedSeriesAggregationType4(
    index: number,
    aggType?: string
  ): UntypedFormControl {
    const aggregationType4 = new UntypedFormControl(aggType);
    this.subs.add(
      aggregationType4.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].aggregationType4.currentValue = +value;
              state[index].aggregationType4.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return aggregationType4;
  }

  groupedSeriesAggregationType5(
    index: number,
    aggType?: string
  ): UntypedFormControl {
    const aggregationType5 = new UntypedFormControl(aggType);
    this.subs.add(
      aggregationType5.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].aggregationType5.currentValue = +value;
              state[index].aggregationType5.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return aggregationType5;
  }

  groupedSeriesParams1(index: number, params1?: number): UntypedFormControl {
    const seriesParams1 = new UntypedFormControl(params1);
    this.subs.add(
      seriesParams1.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].params1.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].params1.currentValue = +value;
                state[index].params1.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesParams1;
  }
  groupedSeriesParams2(index: number, params2?: number): UntypedFormControl {
    const seriesParams2 = new UntypedFormControl(params2);
    this.subs.add(
      seriesParams2.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].params2.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].params2.currentValue = +value;
                state[index].params2.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesParams2;
  }
  groupedSeriesParams3(index: number, params3?: number): UntypedFormControl {
    const seriesParams3 = new UntypedFormControl(params3);
    this.subs.add(
      seriesParams3.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].params3.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].params3.currentValue = +value;
                state[index].params3.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesParams3;
  }
  groupedSeriesParams4(index: number, params4?: number): UntypedFormControl {
    const seriesParams4 = new UntypedFormControl(params4);
    this.subs.add(
      seriesParams4.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].params4.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].params4.currentValue = +value;
                state[index].params4.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesParams4;
  }
  groupedSeriesParams5(index: number, params5?: number): UntypedFormControl {
    const seriesParams5 = new UntypedFormControl(params5);
    this.subs.add(
      seriesParams5.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].params5.currentValue !== +value) {
              const newState = produce(vm.series, (state) => {
                state[index].params5.currentValue = +value;
                state[index].params5.valueChanged = true;
              });
              this.groupedSeriesFacade.updateAndMarkDirty({
                series: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return seriesParams5;
  }

  groupedSeriesSymbol(index: number, symbolType?: string): UntypedFormControl {
    const symbolValue = new UntypedFormControl(symbolType);
    this.subs.add(
      symbolValue.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].symbol.currentValue = value;
              state[index].symbol.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return symbolValue;
  }

  groupedOutlierType(index: number, outlierType?: string): UntypedFormControl {
    const outlierValue = new UntypedFormControl(outlierType);
    this.subs.add(
      outlierValue.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.series, (state) => {
              state[index].outlierType.currentValue = value;
              state[index].outlierType.valueChanged = true;
            });
            this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
          });
        })
    );
    return outlierValue;
  }

  groupedSeriesDataSource1(
    index: number,
    sourceType: string
  ): UntypedFormControl {
    const dataSourceType = new UntypedFormControl(sourceType);
    this.subs.add(
      dataSourceType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].dataSource1.currentValue !== +value) {
              let aggregationValue = null;
              const newState = produce(vm.series, (state) => {
                state[index].dataSource1.currentValue = +value;
                state[index].dataSource1.valueChanged = true;
                if (+value === DataSourceValues.Tag) {
                  if (vm.tagIDs?.length > 0) {
                    state[index].tagID1.currentValue = vm.tagIDs[0].PDTagID;
                    state[index].tagID1.valueChanged = true;
                  }
                  state[index].value1.currentValue = null;
                  state[index].value1.valueChanged = true;
                  if (
                    vm.series[index].seriesType.currentValue ===
                    'boxandwhiskers'
                  ) {
                    state[index].aggregationType1.currentValue =
                      AtxAggregationPercentile;
                    aggregationValue = AtxAggregationPercentile;
                    state[index].aggregationType1.valueChanged = true;
                  } else {
                    state[index].aggregationType1.currentValue =
                      AtxAggregationAverage;
                    aggregationValue = AtxAggregationAverage;
                    state[index].aggregationType1.valueChanged = true;
                  }
                } else {
                  state[index].tagID1.currentValue = null;
                  state[index].tagID1.valueChanged = true;
                  state[index].value1.currentValue = 0;
                  state[index].value1.valueChanged = true;
                }
              });
              this.dataExplorerEventBus.emit(
                new EmitEvent(BusEventTypes.GROUPED_SERIES_DATA_SOURCE_1, {
                  seriesIndex: index,
                  newValue: +value,
                  aggregationValue,
                })
              );
              this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
            }
          });
        })
    );
    return dataSourceType;
  }

  groupedSeriesDataSource2(
    index: number,
    sourceType: string
  ): UntypedFormControl {
    const dataSourceType = new UntypedFormControl(sourceType);
    this.subs.add(
      dataSourceType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].dataSource2.currentValue !== +value) {
              let aggregationValue = null;
              const newState = produce(vm.series, (state) => {
                state[index].dataSource2.currentValue = +value;
                state[index].dataSource2.valueChanged = true;
                if (+value === DataSourceValues.Tag) {
                  // if tagID1 is set, use the same tagID here
                  if (!isNil(vm.series[index].tagID1.currentValue)) {
                    state[index].tagID2.currentValue =
                      vm.series[index].tagID1.currentValue;
                    state[index].tagID2.valueChanged = true;
                  } else if (vm.tagIDs?.length > 0) {
                    state[index].tagID2.currentValue = vm.tagIDs[0].PDTagID;
                    state[index].tagID2.valueChanged = true;
                  }
                  state[index].value2.currentValue = null;
                  state[index].value2.valueChanged = true;
                  if (
                    vm.series[index].seriesType.currentValue ===
                    'boxandwhiskers'
                  ) {
                    state[index].aggregationType2.currentValue =
                      AtxAggregationPercentile;
                    aggregationValue = AtxAggregationPercentile;
                    state[index].aggregationType2.valueChanged = true;
                  } else {
                    // data source 2 is set to minimum
                    // so that the default column / area chart will not cancel itself out and show no bars
                    state[index].aggregationType2.currentValue =
                      AtxAggregationMinimum;
                    aggregationValue = AtxAggregationMinimum;
                    state[index].aggregationType2.valueChanged = true;
                  }
                } else {
                  state[index].tagID2.currentValue = null;
                  state[index].tagID2.valueChanged = true;
                  state[index].value2.currentValue = 0;
                  state[index].value2.valueChanged = true;
                }
              });
              this.dataExplorerEventBus.emit(
                new EmitEvent(BusEventTypes.GROUPED_SERIES_DATA_SOURCE_2, {
                  seriesIndex: index,
                  newValue: +value,
                  aggregationValue,
                })
              );
              this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
            }
          });
        })
    );
    return dataSourceType;
  }

  groupedSeriesDataSource3(
    index: number,
    sourceType: string
  ): UntypedFormControl {
    const dataSourceType = new UntypedFormControl(sourceType);
    this.subs.add(
      dataSourceType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].dataSource3.currentValue !== +value) {
              let aggregationValue = null;
              const newState = produce(vm.series, (state) => {
                state[index].dataSource3.currentValue = +value;
                state[index].dataSource3.valueChanged = true;
                if (+value === DataSourceValues.Tag) {
                  // if tagID1 is set, use the same tagID here
                  if (!isNil(vm.series[index].tagID1.currentValue)) {
                    state[index].tagID3.currentValue =
                      vm.series[index].tagID1.currentValue;
                    state[index].tagID3.valueChanged = true;
                  } else if (vm.tagIDs?.length > 0) {
                    state[index].tagID3.currentValue = vm.tagIDs[0].PDTagID;
                    state[index].tagID3.valueChanged = true;
                  }
                  state[index].value3.currentValue = null;
                  state[index].value3.valueChanged = true;
                  if (
                    vm.series[index].seriesType.currentValue ===
                    'boxandwhiskers'
                  ) {
                    state[index].aggregationType3.currentValue =
                      AtxAggregationPercentile;
                    aggregationValue = AtxAggregationPercentile;
                    state[index].aggregationType3.valueChanged = true;
                  } else {
                    state[index].aggregationType3.currentValue =
                      AtxAggregationAverage;
                    aggregationValue = AtxAggregationAverage;
                    state[index].aggregationType3.valueChanged = true;
                  }
                } else {
                  state[index].tagID3.currentValue = null;
                  state[index].tagID3.valueChanged = true;
                  state[index].value3.currentValue = 0;
                  state[index].value3.valueChanged = true;
                }
              });
              this.dataExplorerEventBus.emit(
                new EmitEvent(BusEventTypes.GROUPED_SERIES_DATA_SOURCE_3, {
                  seriesIndex: index,
                  newValue: +value,
                  aggregationValue,
                })
              );
              this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
            }
          });
        })
    );
    return dataSourceType;
  }

  groupedSeriesDataSource4(
    index: number,
    sourceType: string
  ): UntypedFormControl {
    const dataSourceType = new UntypedFormControl(sourceType);
    this.subs.add(
      dataSourceType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].dataSource4.currentValue !== +value) {
              let aggregationValue = null;
              const newState = produce(vm.series, (state) => {
                state[index].dataSource4.currentValue = +value;
                state[index].dataSource4.valueChanged = true;
                if (+value === DataSourceValues.Tag) {
                  if (!isNil(vm.series[index].tagID1.currentValue)) {
                    state[index].tagID4.currentValue =
                      vm.series[index].tagID1.currentValue;
                    state[index].tagID4.valueChanged = true;
                  } else if (vm.tagIDs?.length > 0) {
                    state[index].tagID4.currentValue = vm.tagIDs[0].PDTagID;
                    state[index].tagID4.valueChanged = true;
                  }
                  state[index].value4.currentValue = null;
                  state[index].value4.valueChanged = true;
                  if (
                    vm.series[index].seriesType.currentValue ===
                    'boxandwhiskers'
                  ) {
                    state[index].aggregationType4.currentValue =
                      AtxAggregationPercentile;
                    aggregationValue = AtxAggregationPercentile;
                    state[index].aggregationType4.valueChanged = true;
                  } else {
                    state[index].aggregationType4.currentValue =
                      AtxAggregationAverage;
                    aggregationValue = AtxAggregationAverage;
                    state[index].aggregationType4.valueChanged = true;
                  }
                } else {
                  state[index].tagID4.currentValue = null;
                  state[index].tagID4.valueChanged = true;
                  state[index].value4.currentValue = 0;
                  state[index].value4.valueChanged = true;
                }
              });
              this.dataExplorerEventBus.emit(
                new EmitEvent(BusEventTypes.GROUPED_SERIES_DATA_SOURCE_4, {
                  seriesIndex: index,
                  newValue: +value,
                  aggregationValue,
                })
              );
              this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
            }
          });
        })
    );
    return dataSourceType;
  }
  groupedSeriesDataSource5(
    index: number,
    sourceType: string
  ): UntypedFormControl {
    const dataSourceType = new UntypedFormControl(sourceType);
    this.subs.add(
      dataSourceType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.series[index].dataSource5.currentValue !== +value) {
              let aggregationValue = null;
              const newState = produce(vm.series, (state) => {
                state[index].dataSource5.currentValue = +value;
                state[index].dataSource5.valueChanged = true;

                if (+value === DataSourceValues.Tag) {
                  if (!isNil(vm.series[index].tagID1.currentValue)) {
                    state[index].tagID5.currentValue =
                      vm.series[index].tagID1.currentValue;
                    state[index].tagID5.valueChanged = true;
                  } else if (vm.tagIDs?.length > 0) {
                    state[index].tagID5.currentValue = vm.tagIDs[0].PDTagID;
                    state[index].tagID5.valueChanged = true;
                  }
                  state[index].value5.currentValue = null;
                  state[index].value5.valueChanged = true;
                  if (
                    vm.series[index].seriesType.currentValue ===
                    'boxandwhiskers'
                  ) {
                    state[index].aggregationType5.currentValue =
                      AtxAggregationPercentile;
                    aggregationValue = AtxAggregationPercentile;
                    state[index].aggregationType5.valueChanged = true;
                  } else {
                    state[index].aggregationType5.currentValue =
                      AtxAggregationAverage;
                    aggregationValue = AtxAggregationAverage;
                    state[index].aggregationType5.valueChanged = true;
                  }
                } else {
                  state[index].tagID5.currentValue = null;
                  state[index].tagID5.valueChanged = true;
                  state[index].value5.currentValue = 0;
                  state[index].value5.valueChanged = true;
                }
              });
              this.dataExplorerEventBus.emit(
                new EmitEvent(BusEventTypes.GROUPED_SERIES_DATA_SOURCE_5, {
                  seriesIndex: index,
                  newValue: +value,
                  aggregationValue,
                })
              );
              this.groupedSeriesFacade.updateAndMarkDirty({ series: newState });
            }
          });
        })
    );
    return dataSourceType;
  }

  addMeasurement() {
    this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((groupedVm) => {
      let tagID = null;
      if (groupedVm.series.length > 0) {
        if (!isNil(groupedVm.series[0].tagID1.currentValue)) {
          tagID = groupedVm.series[0].tagID1.currentValue;
        } else if (groupedVm.tagIDs?.length > 0) {
          tagID = groupedVm.tagIDs[0].PDTagID;
        }
      }
      this.chartFacade.addMeasurementToChart(tagID);
    });
  }

  removeSeriesFromChart(seriesIndex: number) {
    this.groupedSeriesFacade.deleteSeries(seriesIndex);
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        const measurementToRemove =
          vm.selectedTrend.groupSeriesMeasurements[seriesIndex];
        if (measurementToRemove) {
          state.groupSeriesMeasurements.splice(seriesIndex, 1);
          state.measurements?.Bands.forEach((band) => {
            const idx = getBandAxisMeasurementIndex(
              band.Measurements,
              measurementToRemove
            );
            if (idx !== -1) {
              band.Measurements.splice(idx, 1);
            }
          });
        }
        processBandAxisBarsAndMeasurements(state);
      });

      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setGroupedSeriesFormElement(
        newState.groupSeriesMeasurements
      );
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }

  reorderSeries(prevIndex: number, curIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.groupedSeriesFacade.reorderSeries(prevIndex, curIndex);
      const newState = produce(vm.selectedTrend, (state) => {
        const removedSeries = state.groupSeriesMeasurements.splice(
          prevIndex,
          1
        );
        state.groupSeriesMeasurements.splice(curIndex, 0, removedSeries[0]);
        state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
          (series, index) => {
            const newSeries = { ...series };
            newSeries.DisplayOrder = index + 1;
            return newSeries;
          }
        );

        const measurement = state.groupSeriesMeasurements[curIndex];
        state.measurements?.Bands.forEach((band) => {
          const idx = getBandAxisMeasurementIndex(
            band.Measurements,
            measurement
          );
          const measurementToRemove = band.Measurements.splice(idx, 1);
          band.Measurements.splice(curIndex, 0, measurementToRemove[0]);
          band.Measurements.map((m, idx) => {
            const newMeasurement = produce(m, (measurementState) => {
              measurementState.DisplayOrder = idx + 1;
            });
            return newMeasurement;
          });

          if (state.groupedSeriesType === GroupedSeriesType.CUSTOM) {
            const idx = state.groupSeriesMeasurements.findIndex(
              (gsm) => gsm.BarName === band.Label
            );
            if (idx !== -1) {
              band.DisplayOrder =
                state.groupSeriesMeasurements[idx].DisplayOrder;
            }
          }
        });
        state.measurements?.Bands.sort((a, b) => {
          return a.DisplayOrder - b.DisplayOrder;
        });
        processBandAxisBarsAndMeasurements(state);
      });

      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setGroupedSeriesFormElement(
        newState.groupSeriesMeasurements
      );
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }
}
