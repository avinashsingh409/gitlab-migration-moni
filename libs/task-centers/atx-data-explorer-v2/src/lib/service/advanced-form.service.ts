import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { validateHours } from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { AdvancedFacade } from './advanced.facade';

@Injectable()
export class AdvancedFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(private advancedFacade: AdvancedFacade) {}

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  excludeHours(hours: string): UntypedFormControl {
    const excludeHours = new UntypedFormControl(hours, [
      Validators.pattern('[0-9,()-]*'),
    ]);
    this.advancedFacade.updateFilter({ excludeHours: hours });
    this.subs.add(
      excludeHours.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (validateHours(value)) {
            this.advancedFacade.updateFilter({
              excludeHours: value,
            });
            this.advancedFacade.update({ isDirty: true });
          }
        })
    );
    return excludeHours;
  }

  filterMin(min: string | number): UntypedFormControl {
    const filterMin = new UntypedFormControl(min);
    this.advancedFacade.updateFilter({ FilterMin: min });
    this.subs.add(
      filterMin.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.updateFilter({ FilterMin: value });
          this.advancedFacade.update({ isDirty: true });
        })
    );
    return filterMin;
  }

  filterMax(max: string | number): UntypedFormControl {
    const filterMax = new UntypedFormControl(max);
    this.advancedFacade.updateFilter({ FilterMax: max });
    this.subs.add(
      filterMax.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.updateFilter({ FilterMax: value });
          this.advancedFacade.update({ isDirty: true });
        })
    );
    return filterMax;
  }

  flatlineThreshold(flatline: number): UntypedFormControl {
    const flatlineThreshold = new UntypedFormControl(flatline);
    this.advancedFacade.updateFilter({ FlatlineThreshold: flatline });
    this.subs.add(
      flatlineThreshold.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.updateFilter({ FlatlineThreshold: value });
          this.advancedFacade.update({ isDirty: true });
        })
    );
    return flatlineThreshold;
  }

  dataRetrievalMethod(methodValue: number): UntypedFormControl {
    const dataRetrievalMethod = new UntypedFormControl(methodValue);
    this.advancedFacade.update({ dataRetrievalMethod: methodValue });
    this.subs.add(
      dataRetrievalMethod.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.update({
            dataRetrievalMethod: value,
            isDirty: true,
          });
        })
    );
    return dataRetrievalMethod;
  }

  dataRetrievalMinInterval(minValue: number): UntypedFormControl {
    const dataRetrievalMinInterval = new UntypedFormControl(minValue);
    this.advancedFacade.update({ dataRetrievalMinInterval: minValue });
    this.subs.add(
      dataRetrievalMinInterval.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.update({
            dataRetrievalMinInterval: value,
            isDirty: true,
          });
        })
    );
    return dataRetrievalMinInterval;
  }

  dataRetrievalMinIntervalUnits(minUnits: string): UntypedFormControl {
    const dataRetrievalMinIntervalUnits = new UntypedFormControl(minUnits);
    this.advancedFacade.update({ dataRetrievalMinIntervalUnits: minUnits });
    this.subs.add(
      dataRetrievalMinIntervalUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.update({
            dataRetrievalMinIntervalUnits: value,
            isDirty: true,
          });
        })
    );
    return dataRetrievalMinIntervalUnits;
  }

  dataRetrievalArchive(archive: string): UntypedFormControl {
    const dataRetrievalArchive = new UntypedFormControl(archive);
    this.advancedFacade.update({
      dataRetrievalArchive: archive,
    });
    this.subs.add(
      dataRetrievalArchive.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.advancedFacade.update({
            dataRetrievalArchive: value,
            isDirty: true,
          });
        })
    );
    return dataRetrievalArchive;
  }
}
