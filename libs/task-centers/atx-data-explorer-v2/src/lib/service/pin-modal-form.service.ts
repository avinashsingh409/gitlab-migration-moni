import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { combineTime } from '@atonix/shared/utils';
import { PinModalFacade } from './pin-modal.facade';

@Injectable({
  providedIn: 'root',
})
export class PinModalFormService implements OnDestroy {
  private onDestroy = new Subject<void>();

  private numberRegex = '-?\\d+(?:\\.\\d+)?';
  isNumber = (n: string | number): boolean =>
    !isNaN(parseFloat(String(n))) && isFinite(Number(n));
  constructor(private pinModalFacade: PinModalFacade) {}

  buildStartTime(): UntypedFormControl {
    const startTime = new UntypedFormControl(null, [Validators.required]);
    startTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.pinModalFacade.updateStartTime(value);
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.pinModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return startTime;
  }

  checkStartAndEndTimes(
    startDate: Date,
    startTime: string,
    endDate: Date,
    endTime: string
  ) {
    const start: Date = combineTime(startDate, startTime);
    const end: Date = combineTime(endDate, endTime);
    if (start >= end) {
      this.pinModalFacade.setStartDateAfterEndDate(true);
    } else {
      this.pinModalFacade.setStartDateAfterEndDate(false);
    }
  }

  buildEndTime(): UntypedFormControl {
    const endTime = new UntypedFormControl(null, [Validators.required]);
    endTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.pinModalFacade.updateEndTime(value);
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.pinModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return endTime;
  }

  buildSpan(): UntypedFormControl {
    const impactQuantityControl = new UntypedFormControl(0, [
      Validators.pattern(this.numberRegex),
      Validators.required,
    ]);
    impactQuantityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.pinModalFacade.updateSpan(value);
      });
    return impactQuantityControl;
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
