/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  AtxChartLine,
  AtxScatterPlot,
  ChartFacade,
  reflow,
} from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { produce } from 'immer';
import { combineTime } from '@atonix/shared/utils';
import moment from 'moment';

@Injectable()
export class CurveFormService implements OnDestroy {
  private subs = new SubSink();
  private numberRegex = '-?\\d+(?:\\.\\d+)?';
  isNumber = (n: string | number): boolean =>
    !isNaN(parseFloat(String(n))) && isFinite(Number(n));
  constructor(private chartFacade: ChartFacade) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
    // console.log('destroy service');
  }

  resetSeries() {
    this.subs.unsubscribe();
    // console.log('reset service');
  }

  curveName(index: number, name: string): UntypedFormControl {
    const curveName = new UntypedFormControl(name, [Validators.required]);
    this.subs.add(
      curveName.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.DesignCurves[index].DisplayText = value;
            });
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
            this.chartFacade.setSelectedTrend(newState);
          });
        })
    );
    return curveName;
  }

  axisUnits(index: number, axis: number): UntypedFormControl {
    const axisUnits = new UntypedFormControl(axis, [
      Validators.required,
      Validators.min(1),
    ]);
    this.subs.add(
      axisUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.DesignCurves[index].Axis = value;
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return axisUnits;
  }

  curveType(index: number, type: string): UntypedFormControl {
    const curveTypeControl = new UntypedFormControl(type, [
      Validators.required,
    ]);
    this.subs.add(
      curveTypeControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.DesignCurves[index].Type = value;
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return curveTypeControl;
  }

  curveX(
    curveIndex: number,
    pointIndex: number,
    value: string
  ): UntypedFormControl {
    const curveYControl = new UntypedFormControl(value, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    this.subs.add(
      curveYControl.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let x = Number(d.X);
                  if (pointIndex === currentIndex) {
                    if (!this.isNumber(value)) {
                      value = 0;
                    }
                    x = Number(value);
                  }
                  return {
                    X: x,
                    Y: Number(d.Y),
                    Y1: Number(d.Y1),
                    Y2: Number(d.Y2),
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );
    return curveYControl;
  }

  curveY(
    curveIndex: number,
    pointIndex: number,
    isXY: boolean,
    value: string
  ): UntypedFormControl {
    const curveYControl = new UntypedFormControl(value, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    this.subs.add(
      curveYControl.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let y = Number(d.Y);
                  if (pointIndex === currentIndex) {
                    if (!this.isNumber(value)) {
                      value = 0;
                    }
                    y = Number(value);
                  }
                  return {
                    X: isXY ? Number(d.X) : new Date(d.X),
                    Y: y,
                    Y1: Number(d.Y1),
                    Y2: Number(d.Y2),
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );
    return curveYControl;
  }

  curveY1(
    curveIndex: number,
    pointIndex: number,
    isXY: boolean,
    value: string
  ): UntypedFormControl {
    const curveY1Control = new UntypedFormControl(value, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    this.subs.add(
      curveY1Control.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let y1 = Number(d.Y1);
                  if (pointIndex === currentIndex) {
                    if (!this.isNumber(value)) {
                      value = 0;
                    }
                    y1 = Number(value);
                  }
                  return {
                    X: isXY ? Number(d.X) : new Date(d.X),
                    Y: Number(d.Y),
                    Y1: y1,
                    Y2: Number(d.Y2),
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );
    return curveY1Control;
  }

  curveY2(
    curveIndex: number,
    pointIndex: number,
    isXY: boolean,
    value: string
  ): UntypedFormControl {
    const curveY2Control = new UntypedFormControl(value, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    this.subs.add(
      curveY2Control.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let y2 = Number(d.Y2);
                  if (pointIndex === currentIndex) {
                    if (!this.isNumber(value)) {
                      value = 0;
                    }
                    y2 = Number(value);
                  }
                  return {
                    X: isXY ? Number(d.X) : new Date(d.X),
                    Y: Number(d.Y),
                    Y1: Number(d.Y1),
                    Y2: y2,
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );
    return curveY2Control;
  }

  buildPointTime(
    curveIndex: number,
    pointIndex: number,
    timeString: string
  ): UntypedFormControl {
    const pointTime = new UntypedFormControl(timeString, [Validators.required]);
    this.subs.add(
      pointTime.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let X = new Date(d.X);
                  if (pointIndex === currentIndex) {
                    const pointDate = moment(d.X).toDate();
                    const newDate: Date = combineTime(pointDate, value);
                    X = newDate;
                  }
                  return {
                    X,
                    Y: Number(d.Y),
                    Y1: Number(d.Y1),
                    Y2: Number(d.Y2),
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );
    return pointTime;
  }

  buildPointDate(
    curveIndex: number,
    pointIndex: number,
    timeString: Date
  ): UntypedFormControl {
    const pointDate = new UntypedFormControl(timeString, [Validators.required]);
    this.subs.add(
      pointDate.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            if (
              vm.selectedTrend?.trendDefinition.DesignCurves &&
              vm.selectedTrend?.trendDefinition.DesignCurves.length > curveIndex
            ) {
              const newState = produce(vm.selectedTrend, (state) => {
                const values: {
                  X: any;
                  Y: any;
                  Y1: any;
                  Y2: any;
                }[] = JSON.parse(
                  state.trendDefinition.DesignCurves[curveIndex].Values
                );
                const data = values.map((d, currentIndex) => {
                  let X = new Date(d.X);
                  if (pointIndex === currentIndex) {
                    const pointTime = moment(X).format('HH:mm');
                    const newDate: Date = combineTime(value, pointTime);
                    X = newDate;
                  }
                  return {
                    X,
                    Y: Number(d.Y),
                    Y1: Number(d.Y1),
                    Y2: Number(d.Y2),
                  };
                });
                state.trendDefinition.DesignCurves[curveIndex].Values =
                  JSON.stringify(data);
              });
              this.chartFacade.setSelectedTrend(newState);
              const isDirty = produce(vm.isDirty, (state) => {
                state[vm.selectedTrend.id] = true;
              });
              this.chartFacade.setDirty(isDirty);
            }
          });
        })
    );

    return pointDate;
  }

  curveRepeat(index: number, type: string): UntypedFormControl {
    const curveRepeatControl = new UntypedFormControl(type, []);
    this.subs.add(
      curveRepeatControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.DesignCurves[index].Repeat = value;
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return curveRepeatControl;
  }

  curveColor(index: number, color: string): UntypedFormControl {
    const curveColorControl = new UntypedFormControl(color);
    this.subs.add(
      curveColorControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.DesignCurves[index].Color = value;
            });
            this.chartFacade.setSelectedTrend(newState);
            curveColorControl.setValue(value);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return curveColorControl;
  }

  removeSeriesFromChart(seriesIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (
          vm.selectedTrend.trendDefinition?.Series[seriesIndex].IsXAxis &&
          vm.selectedTrend.trendDefinition.ChartTypeID === AtxScatterPlot
        ) {
          state.trendDefinition.ChartTypeID = AtxChartLine;
          this.chartFacade.setIsXY(false);
        }
        state.trendDefinition.Series.splice(seriesIndex, 1);
      });

      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setSeriesFormElement(newState.trendDefinition.Series);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }
}
