/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { ChartFacade, reflow } from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { produce } from 'immer';
import { isNumber, numberRegex } from '@atonix/shared/utils';

@Injectable()
export class AxisFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(private chartFacade: ChartFacade) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
    // console.log('destroy service');
  }

  resetAxis() {
    this.subs.unsubscribe();
    // console.log('reset service');
  }

  axisUnits(axis: number, name: string): UntypedFormControl {
    const axisUnits = new UntypedFormControl(name, []);
    this.subs.add(
      axisUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.Axes[axis].Title = value;
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return axisUnits;
  }

  axisLowLimits(axis: number, lowLimits: string): UntypedFormControl {
    const lowLimitsControl = new UntypedFormControl(lowLimits, [
      Validators.pattern(numberRegex),
    ]);
    this.subs.add(
      lowLimitsControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (!isNumber(value)) {
            value = null;
          }
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              if (value) {
                state.trendDefinition.Axes[axis].Min = +value;
              } else {
                state.trendDefinition.Axes[axis].Min = null;
              }
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return lowLimitsControl;
  }

  axisHighLimits(axis: number, highLimits: string): UntypedFormControl {
    const highLimitsControl = new UntypedFormControl(highLimits, [
      Validators.pattern(numberRegex),
    ]);
    this.subs.add(
      highLimitsControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (!isNumber(value)) {
            value = null;
          }
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              if (value) {
                state.trendDefinition.Axes[axis].Max = +value;
              } else {
                state.trendDefinition.Axes[axis].Max = null;
              }
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return highLimitsControl;
  }

  axisStep(axis: number, currentStep: string): UntypedFormControl {
    const stepControl = new UntypedFormControl(currentStep, [
      Validators.pattern(numberRegex),
    ]);
    this.subs.add(
      stepControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (!isNumber(value)) {
            value = null;
          }
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              if (value) {
                state.trendDefinition.Axes[axis].Step = +value;
              } else {
                state.trendDefinition.Axes[axis].Step = null;
              }
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return stepControl;
  }

  positionToggle(axisIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        let currentPosition = 0;
        let positionBool = false;
        if (vm.selectedTrend.trendDefinition.Axes[axisIndex].Position === 0) {
          currentPosition = 2;
          positionBool = true;
        }
        state.trendDefinition.Axes[axisIndex].PositionBool = positionBool;
        state.trendDefinition.Axes[axisIndex].Position = currentPosition;
      });
      this.chartFacade.setSelectedTrend(newState);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }

  gridlineToggle(axisIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        let gridlineState = true;
        if (
          vm.selectedTrend.trendDefinition.Axes[axisIndex].GridLine === true
        ) {
          gridlineState = false;
        }
        state.trendDefinition.Axes[axisIndex].GridLine = gridlineState;
      });
      this.chartFacade.setSelectedTrend(newState);
    });
  }

  axisMinorStep(axis: number, minorStep: string): UntypedFormControl {
    const stepControl = new UntypedFormControl(minorStep, [
      Validators.pattern(numberRegex),
    ]);
    this.subs.add(
      stepControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (!isNumber(value)) {
            value = null;
          }
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              if (value) {
                state.trendDefinition.Axes[axis].MinorStep = +value;
              } else {
                state.trendDefinition.Axes[axis].MinorStep = null;
              }
            });
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return stepControl;
  }
}
