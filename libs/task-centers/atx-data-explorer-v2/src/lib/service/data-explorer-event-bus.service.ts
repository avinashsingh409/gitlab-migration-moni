import { filter, map } from 'rxjs/operators';
import { Subject, Subscription, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

export const enum BusEventTypes {
  POSITION_TOGGLE,
  GRIDLINE_TOGGLE,
  GRIDLINE_XAXIS_TOGGLE,
  AXIS_ADD_NEW,
  AXIS_REMOVE,
  PIN_TOGGLE,
  PIN_HIDE_TOGGLE,
  PIN_TYPE_CHANGE,
  PIN_ADD_NEW,
  PIN_REMOVE,
  REORDER_PIN,
  CURVE_ADD_NEW,
  CURVE_REMOVE,
  CURVE_ADD_POINT,
  CURVE_REMOVE_POINT,
  REMOVE_SERIES_FROM_CHART,
  ACTIVE_TOGGLE,
  SERIES_VISIBILITY_TOGGLE,
  SERIES_COLOR,
  SERIES_X_AXIS,
  SERIES_BOLD_TOGGLE,
  SERIES_FILTER_TOGGLE,
  SERIES_SAVE,
  REORDER_SERIES,
  TAG_LIST_ASSET_TREE_DROPDOWN_STATE_CHANGE,
  TAG_LIST_ADD_SERIES_TO_CHART,
  GROUPED_SERIES_CLEAR,
  GROUPED_SERIES_SAVE,
  GROUPED_SERIES_DATA_SOURCE_1,
  GROUPED_SERIES_DATA_SOURCE_2,
  GROUPED_SERIES_DATA_SOURCE_3,
  GROUPED_SERIES_DATA_SOURCE_4,
  GROUPED_SERIES_DATA_SOURCE_5,
  GROUPED_SERIES_BOX_AND_WHISKER,
  GROUPED_SERIES_BOX_AND_WHISKER_LEAVE,
  GROUPED_SERIES_ADD_MEASUREMENT,
  ADVANCED_UPDATE_CHART,
  SETTINGS_SELECT_TAB,
  ERROR,
  ON_SAVE_MODEL_ASSET_TREE_DROPDOWN_STATE_CHANGE,
  LOAD_DATA_EXPLORER_CHARTS,
}

export class EmitEvent {
  constructor(public type: BusEventTypes, public data: any) {}
}

@Injectable()
export class DataExplorerEventBus {
  private emitter = new Subject<EmitEvent>();

  emit(event: EmitEvent) {
    this.emitter.next(event);
  }

  on(
    event: BusEventTypes,
    notify: (data: any) => Record<string, unknown>
  ): Subscription | Observable<any> {
    const watch$ = this.emitter.pipe(
      filter((e: EmitEvent) => e.type === event),
      map((e: EmitEvent) => e.data)
    );

    return !notify ? watch$ : watch$.subscribe(notify);
  }
}
