import { Injectable, OnDestroy } from '@angular/core';
import { IPDTrendFilter } from '@atonix/atx-core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

export interface AdvancedState {
  filter: IPDTrendFilter;
  dataRetrievalMethod: number;
  dataRetrievalMinInterval: number;
  dataRetrievalMinIntervalUnits: string;
  dataRetrievalArchive: string;
  isDirty: boolean;
}

const _initialState: AdvancedState = {
  filter: null,
  dataRetrievalMethod: null,
  dataRetrievalMinInterval: null,
  dataRetrievalMinIntervalUnits: null,
  dataRetrievalArchive: null,
  isDirty: false,
};

let _state: AdvancedState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class AdvancedFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<AdvancedState>(_state);
  private state$ = this.store.asObservable();

  filter$ = this.state$.pipe(
    map((state) => state.filter),
    distinctUntilChanged()
  );

  dataRetrievalMethod$ = this.state$.pipe(
    map((state) => state.dataRetrievalMethod),
    distinctUntilChanged()
  );

  dataRetrievalMinInterval$ = this.state$.pipe(
    map((state) => state.dataRetrievalMinInterval),
    distinctUntilChanged()
  );

  dataRetrievalMinIntervalUnits$ = this.state$.pipe(
    map((state) => state.dataRetrievalMinIntervalUnits),
    distinctUntilChanged()
  );

  dataRetrievalArchive$ = this.state$.pipe(
    map((state) => state.dataRetrievalArchive),
    distinctUntilChanged()
  );

  isDirty$ = this.state$.pipe(
    map((state) => state.isDirty),
    distinctUntilChanged()
  );

  vm$: Observable<AdvancedState> = combineLatest([
    this.filter$,
    this.dataRetrievalMethod$,
    this.dataRetrievalMinInterval$,
    this.dataRetrievalMinIntervalUnits$,
    this.dataRetrievalArchive$,
    this.isDirty$,
  ]).pipe(
    map(
      ([
        filter,
        dataRetrievalMethod,
        dataRetrievalMinInterval,
        dataRetrievalMinIntervalUnits,
        dataRetrievalArchive,
        isDirty,
      ]) =>
        ({
          filter,
          dataRetrievalMethod,
          dataRetrievalMinInterval,
          dataRetrievalMinIntervalUnits,
          dataRetrievalArchive,
          isDirty,
        } as AdvancedState)
    )
  );

  update(newValues: Partial<AdvancedState>) {
    this.updateState({
      ..._state,
      ...newValues,
    });
  }

  updateFilter(newValues: Partial<IPDTrendFilter>) {
    this.updateState({
      ..._state,
      filter: {
        ..._state.filter,
        ...newValues,
      },
    });
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  updateDate(excludeDate: string) {
    this.updateState({
      ..._state,
      filter: {
        ..._state.filter,
        [excludeDate]: !_state.filter[excludeDate],
      },
      isDirty: true,
    });
  }

  private updateState(newState: AdvancedState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
