/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  AtxChartLine,
  AtxScatterPlot,
  ChartFacade,
  reflow,
} from '@atonix/atx-chart-v2';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { produce } from 'immer';
import { SeriesFacade } from './series.facade';
import { isNil } from '@atonix/atx-core';
import { LoggerService } from '@atonix/shared/utils';

@Injectable()
export class SeriesFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(
    private chartFacade: ChartFacade,
    private seriesFacade: SeriesFacade,
    private logger: LoggerService
  ) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
    // console.log('destroy service');
  }

  resetSeries() {
    this.subs.unsubscribe();
    // console.log('reset service');
  }

  seriesName(index: number, name: string): UntypedFormControl {
    const seriesName = new UntypedFormControl(name, [Validators.required]);
    this.subs.add(
      seriesName.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.Series[index].DisplayText = value;
              state.trendDefinition.Series[index].AlternateDisplayTexts[0] =
                value;
            });
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.logger.trackTaskCenterEvent(
              'DataExplorerV2:SeriesName',
              value
            );
            this.chartFacade.setDirty(isDirty);
            this.chartFacade.setSelectedTrend(newState);
          });
        })
    );
    return seriesName;
  }

  tagName(name: string): UntypedFormControl {
    const tagName = new UntypedFormControl({ value: name, disabled: true });
    return tagName;
  }

  axisUnits(index: number, axis: number): UntypedFormControl {
    const axisUnits = new UntypedFormControl(axis, [
      Validators.required,
      Validators.min(1),
    ]);
    this.subs.add(
      axisUnits.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.Series[index].Axis = value;
            });

            const newSeriesFormElements = produce(
              vm.formElements.series,
              (state) => {
                state[index].Axis = value;
              }
            );
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });

            this.chartFacade.setSelectedTrend(newState);
            this.chartFacade.setSeriesFormElement(newSeriesFormElements);
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return axisUnits;
  }

  stackType(index: number, type: string): UntypedFormControl {
    const stackType = new UntypedFormControl(type);
    this.subs.add(
      stackType.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            const newState = produce(vm.selectedTrend, (state) => {
              state.trendDefinition.Series[index].StackType = value;
            });
            const newSeriesFormElements = produce(
              vm.formElements.series,
              (state) => {
                state[index].StackType = value;
              }
            );
            this.chartFacade.setSeriesFormElement(newSeriesFormElements);
            this.chartFacade.setSelectedTrend(newState);
            const isDirty = produce(vm.isDirty, (state) => {
              state[vm.selectedTrend.id] = true;
            });
            this.chartFacade.setDirty(isDirty);
          });
        })
    );
    return stackType;
  }

  filterMin(index: number, min: string | number): UntypedFormControl {
    const filterMin = new UntypedFormControl(min);
    this.subs.add(
      filterMin.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.seriesFilter[index].filterMin.currentValue !== value) {
              const newState = produce(vm.seriesFilter, (state) => {
                state[index].filterMin.currentValue = value;
                state[index].filterMin.valueChanged = true;
                if (
                  isNil(value) &&
                  (state[index].flatlineThreshold.currentValue === 0 ||
                    isNil(state[index].flatlineThreshold.currentValue)) &&
                  isNil(state[index].filterMax.currentValue) &&
                  (state[index].applyFilterToAll.currentValue === false ||
                    isNil(state[index].applyFilterToAll.currentValue))
                ) {
                  state[index].isFilterActive = false;
                } else {
                  state[index].isFilterActive = true;
                }
              });
              this.seriesFacade.updateAndMarkDirty({
                seriesFilter: newState,
                isDirty: true,
              });
            }
          });
        })
    );
    return filterMin;
  }

  filterMax(index: number, max: string | number): UntypedFormControl {
    const filterMax = new UntypedFormControl(max);
    this.subs.add(
      filterMax.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
              if (vm.seriesFilter[index].filterMax.currentValue !== value) {
                const newState = produce(vm.seriesFilter, (state) => {
                  state[index].filterMax.currentValue = value;
                  state[index].filterMax.valueChanged = true;
                  if (
                    isNil(value) &&
                    (state[index].flatlineThreshold.currentValue === 0 ||
                      isNil(state[index].flatlineThreshold.currentValue)) &&
                    isNil(state[index].filterMin.currentValue) &&
                    (state[index].applyFilterToAll.currentValue === false ||
                      isNil(state[index].applyFilterToAll.currentValue))
                  ) {
                    state[index].isFilterActive = false;
                  } else {
                    state[index].isFilterActive = true;
                  }
                });
                this.seriesFacade.updateAndMarkDirty({
                  seriesFilter: newState,
                  isDirty: true,
                });
              }
            });
          });
        })
    );
    return filterMax;
  }

  flatlineThreshold(index: number, flatline: number): UntypedFormControl {
    const flatlineThreshold = new UntypedFormControl(flatline);
    this.subs.add(
      flatlineThreshold.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
              if (
                vm.seriesFilter[index].flatlineThreshold.currentValue !== value
              ) {
                const newState = produce(vm.seriesFilter, (state) => {
                  state[index].flatlineThreshold.currentValue = +value;
                  state[index].flatlineThreshold.valueChanged = true;
                  if (
                    (isNil(value) || value === 0) &&
                    isNil(state[index].filterMin.currentValue) &&
                    isNil(state[index].filterMax.currentValue) &&
                    (state[index].applyFilterToAll.currentValue === false ||
                      isNil(state[index].applyFilterToAll.currentValue))
                  ) {
                    state[index].isFilterActive = false;
                  } else {
                    state[index].isFilterActive = true;
                  }
                });
                this.seriesFacade.updateAndMarkDirty({
                  seriesFilter: newState,
                  isDirty: true,
                });
              }
            });
          });
        })
    );
    return flatlineThreshold;
  }

  applyFilterToAll(index: number, applyToAll: boolean): UntypedFormControl {
    const applyFilterToAll = new UntypedFormControl(applyToAll);
    this.subs.add(
      applyFilterToAll.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
              if (
                vm.seriesFilter[index].applyFilterToAll.currentValue !== value
              ) {
                const newState = produce(vm.seriesFilter, (state) => {
                  state[index].applyFilterToAll.currentValue = value;
                  state[index].applyFilterToAll.valueChanged = true;
                  if (
                    (isNil(value) || value === false) &&
                    (state[index].flatlineThreshold.currentValue === 0 ||
                      isNil(state[index].flatlineThreshold.currentValue)) &&
                    isNil(state[index].filterMin.currentValue) &&
                    isNil(state[index].filterMax.currentValue)
                  ) {
                    state[index].isFilterActive = false;
                  } else {
                    state[index].isFilterActive = true;
                  }
                });
                this.seriesFacade.updateAndMarkDirty({
                  seriesFilter: newState,
                  isDirty: true,
                });
              }
            });
          });
        })
    );
    return applyFilterToAll;
  }

  seriesVisibilityToggle(seriesIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedTrend.trendDefinition.Series[seriesIndex]) {
        const newState = produce(vm.selectedTrend, (state) => {
          if (vm.selectedTrend.trendDefinition.Series[seriesIndex].visible) {
            state.trendDefinition.Series[seriesIndex].visible = false;
          } else {
            state.trendDefinition.Series[seriesIndex].visible = true;
          }
        });
        this.chartFacade.setSelectedTrend(newState);
      }
    });
  }

  seriesFilterToggle(seriesIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedTrend.trendDefinition.Series[seriesIndex]) {
        const newState = produce(vm.selectedTrend, (state) => {
          if (vm.selectedTrend.trendDefinition.Series[seriesIndex].showFilter) {
            state.trendDefinition.Series[seriesIndex].showFilter = false;
          } else {
            state.trendDefinition.Series[seriesIndex].showFilter = true;
          }
        });
        this.chartFacade.setSelectedTrend(newState);
      }
    });
  }

  removeSeriesFromChart(seriesIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (
          vm.selectedTrend.trendDefinition?.Series[seriesIndex].IsXAxis &&
          vm.selectedTrend.trendDefinition.ChartTypeID === AtxScatterPlot
        ) {
          state.trendDefinition.ChartTypeID = AtxChartLine;
          this.chartFacade.setIsXY(false);
        }
        state.trendDefinition.Series.splice(seriesIndex, 1);
      });

      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setSeriesFormElement(newState.trendDefinition.Series);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }

  reorderSeries(prevIndex: number, curIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        const removedSeries = state.trendDefinition.Series.splice(prevIndex, 1);
        state.trendDefinition.Series.splice(curIndex, 0, removedSeries[0]);

        state.trendDefinition.Series = state.trendDefinition.Series.map(
          (series, index) => {
            const newSeries = { ...series };
            newSeries.DisplayOrder = index + 1;
            return newSeries;
          }
        );
      });

      this.chartFacade.setSelectedTrend(newState);
      this.chartFacade.setSeriesFormElement(newState.trendDefinition.Series);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.chartFacade.setDirty(isDirty);
    });
  }
}
