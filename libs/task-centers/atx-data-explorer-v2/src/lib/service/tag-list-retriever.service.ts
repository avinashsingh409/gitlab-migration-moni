import { Injectable } from '@angular/core';
import {
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';
import { DataExplorerCoreService } from '@atonix/shared/api';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TagListRetrieverService implements IServerSideDatasource {
  constructor(private dataExplorerCoreService: DataExplorerCoreService) {}

  private unsubscribe$ = new Subject<void>();

  public cancel() {
    this.unsubscribe$.next();
  }

  public getRows(params: IServerSideGetRowsParams) {
    const request = { ...params.request };

    // This will handle the % wildcard
    if (request.filterModel['Units']) {
      request.filterModel['Units'].filter = request.filterModel[
        'Units'
      ].filter.replaceAll('%', '[%]');
    }

    this.dataExplorerCoreService
      .getTagList(request)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (n) => {
          params.success({
            rowData: n.TagMaps,
            rowCount: n.NumTagMaps,
          });
        },
        () => {
          params.fail();
          console.error('Could not retrieve tag list');
        }
      );

    // This will reset the filter back
    if (request.filterModel['Units']) {
      request.filterModel['Units'].filter = request.filterModel[
        'Units'
      ].filter.replaceAll('[%]', '%');
    }
  }
}
