import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { ProcessDataFrameworkService } from '@atonix/shared/api';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { delay, distinctUntilChanged, map, take } from 'rxjs/operators';

export interface DeleteChartState {
  errorMessage: string;
  deleteComplete: boolean;
  deleteInProgress: boolean;
}

const initialState: DeleteChartState = {
  errorMessage: '',
  deleteComplete: false,
  deleteInProgress: false,
};
let _state = initialState;

@Injectable()
export class DeleteModalFacade implements OnDestroy {
  constructor(private processData: ProcessDataFrameworkService) {}

  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<DeleteChartState>(_state);
  private state$ = this.store.asObservable();

  private errorMessage$ = this.state$.pipe(
    map((state) => state.errorMessage),
    distinctUntilChanged()
  );

  private deleteInProgress$ = this.state$.pipe(
    map((state) => state.deleteInProgress),
    distinctUntilChanged()
  );

  deleteComplete$ = this.state$.pipe(
    map((state) => state.deleteComplete),
    distinctUntilChanged()
  );

  vm$: Observable<DeleteChartState> = combineLatest([
    this.errorMessage$,
    this.deleteInProgress$,
    this.deleteComplete$,
  ]).pipe(
    map(([errorMessage, deleteInProgress, deleteComplete]) => {
      return {
        errorMessage,
        deleteInProgress,
        deleteComplete,
      } as DeleteChartState;
    })
  );

  deleteChart(trendID: string) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.updateState({ ..._state, deleteInProgress: true });
      this.processData
        .deleteTrend(+trendID)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (newID) => {
            this.updateState({
              ..._state,
              deleteComplete: true,
              deleteInProgress: false,
            });
          },
          (error: unknown) => {
            this.updateState({
              ..._state,
              deleteComplete: true,
              deleteInProgress: false,
              errorMessage: 'Delete Error',
            });
          }
        );
    });
  }

  reset() {
    this.updateState({ ...initialState });
  }
  private updateState(state: DeleteChartState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
