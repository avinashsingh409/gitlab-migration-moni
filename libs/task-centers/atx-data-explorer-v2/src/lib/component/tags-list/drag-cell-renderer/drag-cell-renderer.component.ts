import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { ChartFacade } from '@atonix/atx-chart-v2';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'atx-drag-cell-renderer',
  template: `
    <div class="dragTip" #dragTipContainer>
      {{ dragTip }}
    </div>
    <div>
      <div
        class="draggableTag"
        [ngClass]="{ draggable: !trendsLoading }"
        [draggable]="!trendsLoading"
        (dragstart)="onDragStart($event)"
        data-cy="cellText"
      >
        {{ value }}
      </div>
    </div>
  `,
  styleUrls: ['./drag-cell-renderer.component.scss'],
})
export class DragCellRendererComponent
  implements ICellRendererAngularComp, OnDestroy
{
  @ViewChild('dragTipContainer') dragTipContainer: ElementRef;
  public unsubscribe$ = new Subject<void>();
  params;
  value = '';
  dragTip = '';
  trendsLoading = false;

  constructor(private chartFacade: ChartFacade) {
    this.chartFacade.query.trendsLoading$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((loading) => {
        this.trendsLoading = loading;
      });
  }

  agInit(params: any) {
    this.params = params;
    this.value = params.value;
  }

  refresh(params: any): boolean {
    this.params = params;
    return true;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onDragStart(dragEvent: DragEvent) {
    let selectedRows = [];
    const selectedNodes = this.params.api.getSelectedNodes();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;

    if (selectedNodes?.length > 0) {
      selectedNodes.sort((a, b) => {
        return a.rowIndex - b.rowIndex;
      });
      selectedRows = selectedNodes.map((nodes) => nodes.data);
    }

    this.dragTip =
      this.params.data.Tag.TagDesc + ' (' + this.params.data.Tag.TagName + ')';
    if (this.dragTip.length > 20) {
      this.dragTip = this.dragTip.substring(0, 16) + '...';
    }

    // This will set the dragTip
    dragEvent.dataTransfer.setDragImage(
      this.dragTipContainer.nativeElement,
      0,
      0
    );

    // This will unselect all other selected nodes when a new SINGLE(not accumulative) node is selected
    if (
      selectedRows?.length === 0 ||
      selectedRows.findIndex((row) => row.TagID === this.params.data.TagID) ===
        -1
    ) {
      selectedRows = [this.params.data];
      this.params.node.setSelected(true, true);
    }

    // This will send the data to the drop area
    dragEvent.dataTransfer.setData(
      isIE ? 'text' : 'text/plain',
      JSON.stringify(selectedRows)
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
