import { Component, Input, OnDestroy } from '@angular/core';
import {
  GridOptions,
  ColumnApi,
  GridApi,
  ServerSideRowModelModule,
  FiltersToolPanelModule,
  Module,
  ClipboardModule,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  MenuModule,
  SetFilterModule,
  GridReadyEvent,
  ICellRendererParams,
  ServerSideStoreType,
  RowDoubleClickedEvent,
} from '@ag-grid-enterprise/all-modules';
import { TagListRetrieverService } from '../../service/tag-list-retriever.service';
import { DragCellRendererComponent } from './drag-cell-renderer/drag-cell-renderer.component';
import { NavFacade } from '@atonix/atx-navigation';
import { TagListFacade } from '../../service/tag-list.facade';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'atx-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.scss'],
})
export class TagsListComponent implements OnDestroy {
  @Input() tagsSelectedAssetOnly = false;
  public unsubscribe$ = new Subject<void>();
  asset: string = null;
  searchAsset: number = null;
  showAssetTreeDropdown = false;

  gridApi: GridApi;
  columnApi: ColumnApi;
  private storeType: ServerSideStoreType = 'partial';
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    ServerSideRowModelModule,
    SetFilterModule,
  ];

  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    rowGroupPanelShow: 'never',
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    headerHeight: 30,
    rowHeight: 30,
    floatingFiltersHeight: 30,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: false,
    },
    columnDefs: [
      {
        colId: 'AssetID',
        headerName: 'AssetID',
        field: 'AssetID',
        sortable: false,
        hide: true,

        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SearchAssetID',
        headerName: 'SearchAssetID',
        field: 'SearchAssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SelectedAssetOnly',
        headerName: 'SelectedAssetOnly',
        field: 'SelectedAssetOnly',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'Asset.AssetAbbrev',
        filter: 'agTextColumnFilter',
        width: 180,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Asset?.AssetAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Variable',
        headerName: 'Variable',
        field: 'VariableType.VariableAbbrev',
        filter: 'agTextColumnFilter',
        width: 175,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.VariableType?.VariableAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Name',
        headerName: 'Name',
        field: 'Tag.TagName',
        filter: 'agTextColumnFilter',
        width: 325,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.TagName;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Tag.TagDesc',
        filter: 'agTextColumnFilter',
        width: 680,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.TagDesc;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Units',
        headerName: 'Units',
        field: 'Tag.EngUnits',
        filter: 'agTextColumnFilter',
        width: 90,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.EngUnits;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          } as any;
        },
      },
    ],
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;

      this.updateAssetAndSearchAsset(
        this.asset,
        this.searchAsset,
        this.tagsSelectedAssetOnly
      );

      this.gridApi.setServerSideDatasource(this.tagListRetriever);
    },
    getRowId: (params) => {
      if (params.data) {
        return String(params.data.AssetVariableTypeTagMapID);
      }
      return null;
    },
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => {
      this.dataExplorerEventBus.emit(
        new EmitEvent(BusEventTypes.TAG_LIST_ADD_SERIES_TO_CHART, {
          assetVariableTypeTagMaps: [event.data],
        })
      );
    },
    sendToClipboard: (params) => {
      this.processDataToClipboard(params.data);
    },
    components: {
      dragCellRenderer: DragCellRendererComponent,
    },
  };

  constructor(
    private tagListRetriever: TagListRetrieverService,
    public tagListFacade: TagListFacade,
    public navFacade: NavFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private clipboard: Clipboard
  ) {
    this.tagListFacade.assetID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        this.asset = id;
      });

    this.tagListFacade.searchAssetID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        this.searchAsset = id;
        this.updateAssetAndSearchAsset(
          this.asset,
          this.searchAsset,
          this.tagsSelectedAssetOnly
        );
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getContextMenuItems(params) {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }

  updateAssetAndSearchAsset(
    asset: string,
    searchAsset: number,
    selectedAssetOnly: boolean
  ) {
    if (this.gridOptions && this.gridOptions.api && asset && searchAsset) {
      const assetIdFilter = this.gridOptions.api.getFilterInstance('AssetID');
      const searchAssetIdFilter =
        this.gridOptions.api.getFilterInstance('SearchAssetID');
      const selectedAssetOnlyFilter =
        this.gridOptions.api.getFilterInstance('SelectedAssetOnly');
      if (
        asset !== assetIdFilter.getModel()?.filter ||
        searchAsset !== searchAssetIdFilter.getModel()?.filter ||
        selectedAssetOnly.toString() !==
          selectedAssetOnlyFilter.getModel()?.filter
      ) {
        assetIdFilter.setModel({ type: 'asset', filter: asset });
        searchAssetIdFilter.setModel({
          type: 'searchAsset',
          filter: searchAsset.toString(),
        });
        selectedAssetOnlyFilter.setModel({
          type: 'selectedAssetOnly',
          filter: selectedAssetOnly ? 'true' : 'false',
        });
        this.gridOptions.api.onFilterChanged();
      }
    }
  }

  editAsset(): void {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  closeAssetTreeDropdown() {
    this.showAssetTreeDropdown = false;
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.TAG_LIST_ASSET_TREE_DROPDOWN_STATE_CHANGE, {
        change,
      })
    );
  }

  // The built-in 'copy to clipboard' of the ag-grid doesn't respect the
  // selected rows' display orders but rather the selected rows' ids.
  // This method will process the selected rows' data before sending it to the clipboard
  // so that selected rows' display orders will be retained upon copying.
  processDataToClipboard(data: string) {
    let text = data;
    const selectedRows = this.gridApi.getSelectedNodes();
    if (selectedRows?.length > 0) {
      text = '';
      if (data.match(/Asset\tVariable\tName\tDescription\tUnits/gm)) {
        text += 'Asset\tVariable\tName\tDescription\tUnits\n';
      }

      selectedRows.sort((a, b) => {
        return a.rowIndex - b.rowIndex;
      });

      selectedRows.map((row) => {
        const assetAbbrev = row.data.Asset?.AssetAbbrev || '';
        const variableAbbrev = row.data.VariableType?.VariableAbbrev || '';
        const tagName = row.data.Tag?.TagName || '';
        const tagDesc = row.data.Tag?.TagDesc || '';
        const engUnits = row.data.Tag?.EngUnits || '';
        text +=
          assetAbbrev +
          '\t' +
          variableAbbrev +
          '\t' +
          tagName +
          '\t' +
          tagDesc +
          '\t' +
          engUnits +
          '\n';
      });
    }

    this.clipboard.copy(text);
  }

  tagsPreferenceChange() {
    this.tagsSelectedAssetOnly = !this.tagsSelectedAssetOnly;
    this.updateAssetAndSearchAsset(
      this.asset,
      this.searchAsset,
      this.tagsSelectedAssetOnly
    );
  }
}
