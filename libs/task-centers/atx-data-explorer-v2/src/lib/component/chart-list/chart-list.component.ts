import { Component, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatSelectionListChange } from '@angular/material/list';
import {
  ChartFacade,
  ChartState,
  processTrendLabel,
} from '@atonix/atx-chart-v2';
import { IProcessedTrend } from '@atonix/atx-core';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'atx-chart-list',
  templateUrl: './chart-list.component.html',
  styleUrls: ['./chart-list.component.scss'],
})
export class ChartListComponent implements OnInit, OnDestroy {
  chartSearchInputCtrl = new UntypedFormControl();
  filteredTrends: IProcessedTrend[] = [];
  unsubscribe$ = new Subject<void>();
  public vm$: Observable<ChartState>;
  constructor(private chartFacade: ChartFacade) {}
  ngOnInit() {
    this.vm$ = this.chartFacade.query.vm$;

    this.chartSearchInputCtrl.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        this.filteredTrends = [];
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.chartFacade.query.trends$.pipe(take(1)).subscribe((trends) => {
          const searchRE = new RegExp(val, 'gi');
          this.filteredTrends =
            trends.filter((trend) => trend.label.match(searchRE)) ?? [];
        });
      });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  selectionChange(event: MatSelectionListChange) {
    if (event?.options) {
      this.chartFacade.selectTrendDropdown(event.options[0].value);
    }
  }

  getLabel(
    trend: IProcessedTrend,
    selectedTrend: IProcessedTrend,
    dirtyTrends: Map<string, IProcessedTrend>
  ) {
    if (
      selectedTrend?.id === trend?.id &&
      selectedTrend?.label === trend?.label
    ) {
      return processTrendLabel(selectedTrend);
    } else {
      const processedTrend: IProcessedTrend = dirtyTrends?.get(trend.id);
      if (processedTrend) {
        return processTrendLabel(processedTrend);
      } else {
        return processTrendLabel(trend);
      }
    }
  }

  selectChart(event: MatAutocompleteSelectedEvent) {
    const selectedTrend = event.option.value as IProcessedTrend;
    this.chartFacade.selectTrendDropdown(selectedTrend.id);
    this.chartSearchInputCtrl.patchValue(null);
  }

  clearFilters() {
    this.filteredTrends = [];
  }
}
