import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlContainer } from '@angular/forms';

@Component({
  selector: 'atx-edit-view',
  templateUrl: './edit-view.component.html',
  styleUrls: ['./edit-view.component.scss'],
})
export class EditViewComponent implements OnInit {
  public parentForm;
  constructor(private parentControl: ControlContainer) {}
  @Input() asset: string;
  @Input() expanded: boolean;

  @Output() bottomIndicatorClicked = new EventEmitter();

  ngOnInit() {
    this.parentForm = this.parentControl.control;
  }

  bottomIndicatorClick() {
    this.bottomIndicatorClicked.emit();
  }
}
