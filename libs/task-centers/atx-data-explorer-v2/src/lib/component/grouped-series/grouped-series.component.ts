import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, OnDestroy } from '@angular/core';
import {
  ControlContainer,
  UntypedFormArray,
  FormControl,
} from '@angular/forms';
import {
  AtxAggregationAverage,
  AtxAggregationPercentile,
  ChartFacade,
  ChartState,
  DataSourceValues,
} from '@atonix/atx-chart-v2';
import { isNil } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import { produce } from 'immer';
import {
  GroupedSeriesFacade,
  GroupedSeriesState,
} from '../../service/grouped-series.facade';
import { IPDTrendAxis, IPDTrendSeries } from '@atonix/atx-core';

@Component({
  selector: 'atx-grouped-series',
  templateUrl: './grouped-series.component.html',
  styleUrls: ['./grouped-series.component.scss'],
})
export class GroupedSeriesComponent implements OnDestroy {
  public unsubscribe$ = new Subject<void>();
  seriesForm;
  public vm$: Observable<ChartState>;
  public seriesVm$: Observable<GroupedSeriesState>;

  axes: IPDTrendAxis[];
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  outlierTypes = [
    { title: 'None', value: '' },
    { title: 'Outliers Only', value: 'outliers' },
    { title: 'Show All Points', value: 'allpoints' },
  ];
  aggregationTypes = [
    { title: 'Minimum', value: '1' },
    { title: 'Maximum', value: '2' },
    { title: 'Average', value: '3' },
    { title: 'Sum', value: '6' },
    { title: 'First', value: '4' },
    { title: 'Last', value: '5' },
    { title: 'Median', value: '9' },
    { title: 'Mode', value: '10' },
    { title: 'First (Honor Bad Status)', value: '11' },
    { title: 'Last (Honor Bad Status)', value: '12' },
    { title: 'Delta', value: '13' },
    { title: 'Standard Deviation', value: '14' },
    { title: 'Percentile', value: '18' },
  ];
  public customSettingsShow = false;

  constructor(
    private chartFacade: ChartFacade,
    private seriesControl: ControlContainer,
    private groupedSeriesFacade: GroupedSeriesFacade,
    private dataExplorerEventBus: DataExplorerEventBus
  ) {
    this.seriesForm = this.seriesControl.control;
    this.seriesVm$ = this.groupedSeriesFacade.vm$;
    this.vm$ = this.chartFacade.query.vm$;
    this.chartFacade.query.selectedTrendAxes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((axes) => {
        this.axes = axes;
      });
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_DATA_SOURCE_1,
      this.dataSource1Change.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_DATA_SOURCE_2,
      this.dataSource2Change.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_DATA_SOURCE_3,
      this.dataSource3Change.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_DATA_SOURCE_4,
      this.dataSource4Change.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_DATA_SOURCE_5,
      this.dataSource5Change.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_BOX_AND_WHISKER,
      this.boxAndWhisker.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_BOX_AND_WHISKER_LEAVE,
      this.boxAndWhiskerLeave.bind(this)
    );
  }

  get series(): UntypedFormArray {
    return this.seriesForm.get('groupedSeries') as UntypedFormArray;
  }

  tagID1Change(event, scenario) {
    this.groupedSeriesFacade.tagID1Change(event.value, scenario);
  }
  tagID2Change(event, scenario) {
    this.groupedSeriesFacade.tagID2Change(event.value, scenario);
  }
  tagID3Change(event, scenario) {
    this.groupedSeriesFacade.tagID3Change(event.value, scenario);
  }
  tagID4Change(event, scenario) {
    this.groupedSeriesFacade.tagID4Change(event.value, scenario);
  }
  tagID5Change(event, scenario) {
    this.groupedSeriesFacade.tagID5Change(event.value, scenario);
  }

  boxAndWhiskerLeave(event: any) {
    // user has left a box and whisker chart.
    // even though the groupedSeriesFacade reactively tracks
    // the state of these elements, the Form elements have to be
    // manually updated at the component level.
    // These charts have lots of values other charts don't, so we want to clear
    // those values
    if (!isNil(event?.seriesIndex)) {
      this.series
        .at(event.seriesIndex)
        .get('params1')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params2')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params3')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params4')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params5')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType1')
        .patchValue(AtxAggregationAverage.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType2')
        .patchValue(AtxAggregationAverage.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType3')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType4')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType5')
        .patchValue(null, { emitEvent: false });
      // set dataSource2 to constant so that a chart other than boxandwhiskers draws successfully
      this.series
        .at(event.seriesIndex)
        .get('dataSource2')
        .patchValue('1', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value2')
        .patchValue('0', { emitEvent: false });
    }
  }

  boxAndWhisker(event: any) {
    // user has entered box and whisker.
    // even though the groupedSeriesFacade reactively tracks
    // the state of these elements, the Form elements have to be
    // manually updated at the component level
    if (!isNil(event?.seriesIndex)) {
      this.series
        .at(event.seriesIndex)
        .get('params1')
        .patchValue('95', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params2')
        .patchValue('75', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params3')
        .patchValue('50', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params4')
        .patchValue('25', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('params5')
        .patchValue('5', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value1')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value2')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value3')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value4')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('value5')
        .patchValue(null, { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType1')
        .patchValue(AtxAggregationPercentile.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType2')
        .patchValue(AtxAggregationPercentile.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType3')
        .patchValue(AtxAggregationPercentile.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType4')
        .patchValue(AtxAggregationPercentile.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('aggregationType5')
        .patchValue(AtxAggregationPercentile.toString(), { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('dataSource1')
        .patchValue('0', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('dataSource2')
        .patchValue('0', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('dataSource3')
        .patchValue('0', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('dataSource4')
        .patchValue('0', { emitEvent: false });
      this.series
        .at(event.seriesIndex)
        .get('dataSource5')
        .patchValue('0', { emitEvent: false });
    }
  }

  dataSource1Change(event: any) {
    if (!isNil(event?.seriesIndex)) {
      if (event?.newValue === DataSourceValues.Constant) {
        this.series
          .at(event.seriesIndex)
          .get('value1')
          .patchValue('0', { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource1')
          .patchValue('1', { emitEvent: false });
      } else {
        this.series
          .at(event.seriesIndex)
          .get('value1')
          .patchValue(null, { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource1')
          .patchValue('0', { emitEvent: false });
      }
      if (!isNil(event?.aggregationValue)) {
        this.series
          .at(event.seriesIndex)
          .get('aggregationType1')
          .patchValue(event.aggregationValue.toString(), { emitEvent: false });
      }
    }
  }

  dataSource2Change(event: any) {
    if (!isNil(event?.seriesIndex)) {
      if (event?.newValue === DataSourceValues.Constant) {
        this.series
          .at(event.seriesIndex)
          .get('value2')
          .patchValue('0', { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource2')
          .patchValue('1', { emitEvent: false });
      } else {
        this.series
          .at(event.seriesIndex)
          .get('value2')
          .patchValue(null, { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource2')
          .patchValue('0', { emitEvent: false });
      }
      if (!isNil(event?.aggregationValue)) {
        this.series
          .at(event.seriesIndex)
          .get('aggregationType2')
          .patchValue(event.aggregationValue.toString(), { emitEvent: false });
      }
    }
  }

  dataSource3Change(event: any) {
    if (!isNil(event?.seriesIndex)) {
      if (event?.newValue === DataSourceValues.Constant) {
        this.series
          .at(event.seriesIndex)
          .get('value3')
          .patchValue('0', { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource3')
          .patchValue('1', { emitEvent: false });
      } else {
        this.series
          .at(event.seriesIndex)
          .get('value3')
          .patchValue(null, { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource3')
          .patchValue('0', { emitEvent: false });
      }
      if (!isNil(event?.aggregationValue)) {
        this.series
          .at(event.seriesIndex)
          .get('aggregationType3')
          .patchValue(event.aggregationValue.toString(), { emitEvent: false });
      }
    }
  }

  dataSource4Change(event: any) {
    if (!isNil(event?.seriesIndex)) {
      if (event?.newValue === DataSourceValues.Constant) {
        this.series
          .at(event.seriesIndex)
          .get('value4')
          .patchValue('0', { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource4')
          .patchValue('1', { emitEvent: false });
      } else {
        this.series
          .at(event.seriesIndex)
          .get('value4')
          .patchValue(null, { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource4')
          .patchValue('0', { emitEvent: false });
      }
      if (!isNil(event?.aggregationValue)) {
        this.series
          .at(event.seriesIndex)
          .get('aggregationType4')
          .patchValue(event.aggregationValue.toString(), { emitEvent: false });
      }
    }
  }

  dataSource5Change(event: any) {
    if (!isNil(event?.seriesIndex)) {
      if (event?.newValue === DataSourceValues.Constant) {
        this.series
          .at(event.seriesIndex)
          .get('value5')
          .patchValue('0', { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource5')
          .patchValue('1', { emitEvent: false });
      } else {
        this.series
          .at(event.seriesIndex)
          .get('value5')
          .patchValue(null, { emitEvent: false });
        this.series
          .at(event.seriesIndex)
          .get('dataSource5')
          .patchValue('0', { emitEvent: false });
      }
      if (!isNil(event?.aggregationValue)) {
        this.series
          .at(event.seriesIndex)
          .get('aggregationType5')
          .patchValue(event.aggregationValue.toString(), { emitEvent: false });
      }
    }
  }

  seriesVisibilityToggle(seriesIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_VISIBILITY_TOGGLE, { seriesIndex })
    );
  }

  removeSeries(seriesIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.REMOVE_SERIES_FROM_CHART, { seriesIndex })
    );
  }

  updateGroupSeriesSubType(event: any) {
    if (event?.value) {
      this.chartFacade.updateGroupSeriesSubType(event.value);
    }
  }

  updateGroupSeriesType(event: any) {
    if (event?.value) {
      this.chartFacade.updateGroupSeriesType(event.value);
    }
  }

  dataSourceType(value1: any) {
    if (!isNil(value1)) {
      return 'Constant';
    }
    return 'Tag';
  }

  aggregationType(aggregationType?: number) {
    console.log(aggregationType);
    if (!isNil(aggregationType)) {
      return aggregationType.toString();
    } else {
      return '';
    }
  }

  updateChart() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.GROUPED_SERIES_SAVE, {})
    );
  }

  clearChart() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.GROUPED_SERIES_CLEAR, {})
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.REORDER_SERIES, {
        previousIndex: event.previousIndex,
        currentIndex: event.currentIndex,
      })
    );
  }

  addMeasurement() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.GROUPED_SERIES_ADD_MEASUREMENT, {})
    );
  }

  xAxisChecked(seriesIndex: number, series: IPDTrendSeries) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_X_AXIS, {
        seriesIndex,
        series,
      })
    );
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  customSettingsToggle() {
    this.customSettingsShow = !this.customSettingsShow;
  }
}
