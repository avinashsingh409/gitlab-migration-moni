import { formatDate } from '@angular/common';
import {
  Component,
  OnInit,
  Inject,
  OnDestroy,
  LOCALE_ID,
  ViewChild,
  ElementRef,
  Input,
} from '@angular/core';
import {
  ControlContainer,
  UntypedFormArray,
  UntypedFormControl,
} from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import {
  AtxPinTypeOverlay,
  AtxPinTypeUnmodified,
  ChartFacade,
  ChartState,
  colorBrewerDivergent,
  PinType,
  PinTypeMapping,
} from '@atonix/atx-chart-v2';
import { produce } from 'immer';
import { Observable, Subject } from 'rxjs';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { PinModalComponent } from '../modals/pin-modal-add/pin-modal.component';
import { take } from 'rxjs/operators';
import { PinModalEditComponent } from '../modals/pin-modal-edit/pin-modal-edit.component';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ICriteria, IPDTrend } from '@atonix/atx-core';
import { AuthFacade } from '@atonix/shared/state/auth';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
export interface DialogData {
  timeFilters: ICriteria[];
  startDt: Date;
  endDt: Date;
}

@Component({
  selector: 'atx-pins',
  templateUrl: './pins.component.html',
  styleUrls: ['./pins.component.scss'],
})
export class PinsComponent implements OnInit, OnDestroy {
  @ViewChild('pinModal', { static: false }) public pinModal: ElementRef;
  @Input() asset: string;

  public pinHelpShow = false;
  public parentForm;
  public pinTypeStates = Object.keys(PinType).map((itm) => PinType[itm]);
  public pinType = PinType;
  public pinTypeMapping: any = PinTypeMapping;
  timeUnitControl = new UntypedFormControl('hr');

  vm$: Observable<ChartState>;
  public unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    public router: Router,
    private parentControl: ControlContainer,
    private chartFacade: ChartFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    public authFacade: AuthFacade,
    @Inject(LOCALE_ID) private locale: string,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.vm$ = this.chartFacade.query.vm$;
  }

  ngOnInit() {
    this.parentForm = this.parentControl.control;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  pinTypeChange(event: MatSelectChange) {
    if (event?.value) {
      this.dataExplorerEventBus.emit(
        new EmitEvent(BusEventTypes.PIN_TYPE_CHANGE, { pinTypeID: event.value })
      );
    }
  }

  editPin(pinIndex: number) {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.loadingData || vm.trendsLoading || vm.timeFilters?.length < 1) {
        return;
      }
      if (vm.selectedTrend.trendDefinition?.Pins?.length > pinIndex) {
        const rect: DOMRect =
          this.pinModal?.nativeElement?.getBoundingClientRect();
        const dialogRef = this.dialog.open(PinModalEditComponent, {
          width: `${rect.width - 220}px`,
          height: `${rect.height}px`,
          position: {
            left: `${rect.x + 60}px`,
            top: `${rect.y}px`,
          },
          disableClose: true,
          data: {
            timeFilters: vm.timeFilters,
            pin: vm.selectedTrend.trendDefinition.Pins[pinIndex],
          },
        });

        // eslint-disable-next-line rxjs/no-nested-subscribe
        dialogRef.afterClosed().subscribe((result) => {
          if (result?.selectedFilter) {
            const selectedFilter = result.selectedFilter as ICriteria;

            if (
              selectedFilter.CoID !==
                vm.selectedTrend.trendDefinition.Pins[pinIndex]
                  .CriteriaObjectId ||
              selectedFilter.CoID === -1
            ) {
              const modifiedPin = produce(
                vm.selectedTrend.trendDefinition.Pins[pinIndex],
                (state) => {
                  if (selectedFilter.CoID > -1) {
                    state.CriteriaObjectId = selectedFilter.CoID;
                  } else {
                    state.CriteriaObjectId = null;
                    state.StartTime = moment(result.startDate).toDate();
                    state.EndTime = moment(result.endDate).toDate();
                    state.SpanUnits = result.units;
                  }
                }
              );
              this.chartFacade.editPin(pinIndex, modifiedPin);
            }
          }
        });
      }
    });
  }

  addpins() {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.loadingData || vm.trendsLoading || vm.timeFilters?.length < 1) {
        return;
      }
      const rect: DOMRect =
        this.pinModal?.nativeElement?.getBoundingClientRect();
      const dialogRef = this.dialog.open(PinModalComponent, {
        width: `${rect.width - 220}px`,
        height: `${rect.height}px`,
        position: {
          left: `${rect.x + 60}px`,
          top: `${rect.y}px`,
        },
        disableClose: true,
        data: {
          timeFilters: vm.timeFilters,
          startDt: vm.start,
          endDt: vm.end,
        },
      });

      // eslint-disable-next-line rxjs/no-nested-subscribe
      dialogRef.afterClosed().subscribe((result) => {
        if (result?.selectedFilter) {
          const newPin = result.selectedFilter as ICriteria;
          this.dataExplorerEventBus.emit(
            new EmitEvent(BusEventTypes.PIN_ADD_NEW, {
              newPin,
              startDate: result.startDate,
              endDate: result.endDate,
              units: result.units,
            })
          );
        }
      });
    });
  }

  removePin(pinIndex: number) {
    this.chartFacade.removePin(pinIndex);
  }

  activeToggle(): void {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.ACTIVE_TOGGLE, {})
    );
  }

  pinsBtnToggle(): void {
    this.dataExplorerEventBus.emit(new EmitEvent(BusEventTypes.PIN_TOGGLE, {}));
  }

  pinsHideToggle(idx: number): void {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.PIN_HIDE_TOGGLE, { idx })
    );
  }

  get pins(): UntypedFormArray {
    return this.parentForm.get('pins') as UntypedFormArray;
  }

  systemConfig() {
    let url = `${window.location.protocol}//${window.location.host}/system-config/time-range?id=${this.asset}`;
    const hostname = window.location.hostname;
    if (hostname !== 'localhost') {
      url = `${this.appConfig.baseSiteURL}/MD/system-config/time-range?id=${this.asset}`;
    }
    window.open(url, '_blank').focus();
  }
  timeRange(trend: IPDTrend, start, end) {
    if (trend.ShowSelected && trend.PinTypeID === AtxPinTypeUnmodified) {
      return '';
    }
    if (trend.PinTypeID === AtxPinTypeOverlay) {
      return 'Viewing Pin Overlay Mode';
    }
    let startDate = null;
    let endDate = null;
    for (const pin of trend.Pins) {
      if (!pin.Hidden) {
        startDate = moment(pin.StartTime).toDate();
        endDate = moment(pin.EndTime).toDate();
        break;
      }
    }
    trend.Pins.forEach((pin) => {
      if (!pin.Hidden) {
        const compareStart = moment(pin.StartTime).toDate();
        const compareEnd = moment(pin.EndTime).toDate();
        if (compareStart < startDate) {
          startDate = compareStart;
        }
        if (compareEnd > endDate) {
          endDate = compareEnd;
        }
      }
    });
    if (startDate) {
      return `Viewing Pin Range: ${formatDate(
        startDate,
        'M/d/yy, h:mm a',
        this.locale
      )} - ${formatDate(endDate, 'M/d/yy, h:mm a', this.locale)}`;
    }
    return '';
  }

  colorBrewer(idx: number) {
    return colorBrewerDivergent[idx % colorBrewerDivergent.length][0];
  }

  hiddenPin(isHidden: boolean) {
    if (isHidden) {
      return 'rgba(150,150,150,0.5)';
    } else {
      return '#FFFFFF';
    }
  }

  hidePin(ifHidden: boolean, idx: number) {
    if (ifHidden) {
      return 'rgba(60,60,60,0.3)';
    } else {
      return this.colorBrewer(idx);
    }
  }

  pinHelpToggle() {
    this.pinHelpShow = !this.pinHelpShow;
  }

  drop(event: CdkDragDrop<string[]>) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.REORDER_PIN, {
        previousIndex: event.previousIndex,
        currentIndex: event.currentIndex,
      })
    );
  }
}
