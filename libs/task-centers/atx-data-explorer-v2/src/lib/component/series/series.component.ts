import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, OnDestroy } from '@angular/core';
import {
  ControlContainer,
  UntypedFormArray,
  FormControl,
} from '@angular/forms';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { IPDTrendAxis, IPDTrendSeries } from '@atonix/atx-core';
import { LoggerService } from '@atonix/shared/utils';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import { SeriesFacade, SeriesState } from '../../service/series.facade';

@Component({
  selector: 'atx-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss'],
})
export class SeriesComponent implements OnDestroy {
  public unsubscribe$ = new Subject<void>();
  seriesForm;
  public vm$: Observable<ChartState>;
  public seriesVm$: Observable<SeriesState>;
  axes: IPDTrendAxis[];
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

  constructor(
    private chartFacade: ChartFacade,
    private seriesControl: ControlContainer,
    private dataExplorerEventBus: DataExplorerEventBus,
    private seriesFacade: SeriesFacade,
    private logger: LoggerService
  ) {
    this.seriesForm = this.seriesControl.control;
    this.vm$ = this.chartFacade.query.vm$;
    this.seriesVm$ = this.seriesFacade.vm$;
    this.chartFacade.query.selectedTrendAxes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((axes) => {
        this.axes = axes;
      });
  }

  get series(): UntypedFormArray {
    return this.seriesForm.get('series') as UntypedFormArray;
  }

  seriesVisibilityToggle(seriesIndex: number) {
    this.logger.trackTaskCenterEvent(
      'DataExplorerV2:SeriesVisibilityToggle',
      'SeriesTab'
    );
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_VISIBILITY_TOGGLE, { seriesIndex })
    );
  }

  removeSeries(seriesIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.REMOVE_SERIES_FROM_CHART, { seriesIndex })
    );
  }

  filterToggle(seriesIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_FILTER_TOGGLE, { seriesIndex })
    );
  }

  drop(event: CdkDragDrop<string[]>) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.REORDER_SERIES, {
        previousIndex: event.previousIndex,
        currentIndex: event.currentIndex,
      })
    );
  }

  boldToggle(seriesIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_BOLD_TOGGLE, {
        seriesIndex,
      })
    );
  }

  xAxisChecked(seriesIndex: number, series: IPDTrendSeries) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_X_AXIS, {
        seriesIndex,
        series,
      })
    );
  }

  updateChart() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SERIES_SAVE, {})
    );
    this.logger.trackTaskCenterEvent(
      'DataExplorerV2:SeriesTab:UpdateChartButtonClick'
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
