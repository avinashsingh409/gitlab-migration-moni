import { Component, Input, OnInit } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { isNil } from '@atonix/atx-core';
import { Observable } from 'rxjs';
import {
  BusEventTypes,
  DataExplorerEventBus,
} from '../../service/data-explorer-event-bus.service';

@Component({
  selector: 'atx-settings-tabs',
  templateUrl: './settings-tabs.component.html',
  styleUrls: ['./settings-tabs.component.scss'],
})
export class SettingsTabsComponent implements OnInit {
  @Input() asset: string;
  public parentForm;
  public vm$: Observable<ChartState>;
  selectedIndex = 0;
  constructor(
    private parentControl: ControlContainer,
    private chartFacade: ChartFacade,
    private dataExplorerEventBus: DataExplorerEventBus
  ) {
    this.vm$ = this.chartFacade.query.vm$;

    dataExplorerEventBus.on(
      BusEventTypes.SETTINGS_SELECT_TAB,
      this.selectTab.bind(this)
    );
  }

  ngOnInit() {
    this.parentForm = this.parentControl.control;
  }

  selectTab(tab: any) {
    if (!isNil(tab?.tabIndex)) {
      this.selectedIndex = tab.tabIndex;
    }
  }
}
