import { Component } from '@angular/core';
import { ControlContainer, UntypedFormGroup } from '@angular/forms';
import { ChartFacade, ChartState, validateHours } from '@atonix/atx-chart-v2';
import { LoggerService } from '@atonix/shared/utils';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AdvancedFacade, AdvancedState } from '../../service/advanced.facade';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';

@Component({
  selector: 'atx-advanced',
  templateUrl: './advanced.component.html',
  styleUrls: ['./advanced.component.scss'],
})
export class AdvancedComponent {
  public chartVm$: Observable<ChartState>;
  public advancedVm$: Observable<AdvancedState>;
  advancedForm;

  constructor(
    private chartFacade: ChartFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private advancedControl: ControlContainer,
    public advancedFacade: AdvancedFacade,
    private logger: LoggerService
  ) {
    this.advancedForm = this.advancedControl.control;
    this.chartVm$ = this.chartFacade.query.vm$;
    this.advancedVm$ = this.advancedFacade.vm$;
  }

  get advanced(): UntypedFormGroup {
    return this.advancedForm.get('advanced') as UntypedFormGroup;
  }

  excludeDateToggle(excludeDate: string) {
    this.advancedFacade.updateDate(excludeDate);
  }

  validateKeys($event) {
    // This will disable other keys that are not acceptable for this input
    if ([8, 37, 39].indexOf($event.which) === -1) {
      return validateHours($event.key);
    }
    return true;
  }

  updateChart() {
    this.advancedVm$.pipe(take(1)).subscribe((vm) => {
      this.dataExplorerEventBus.emit(
        new EmitEvent(BusEventTypes.ADVANCED_UPDATE_CHART, {
          advancedOptions: {
            filter: vm.filter,
            dataRetrievalMethod: vm.dataRetrievalMethod,
            dataRetrievalMinInterval: vm.dataRetrievalMinInterval,
            dataRetrievalMinIntervalUnits: vm.dataRetrievalMinIntervalUnits,
            dataRetrievalArchive: vm.dataRetrievalArchive,
          },
        })
      );
      this.logger.trackTaskCenterEvent(
        'DataExplorerV2:AdvancedTab:UpdateChartButtonClick'
      );
      this.advancedFacade.update({ isDirty: false });
    });
  }
}
