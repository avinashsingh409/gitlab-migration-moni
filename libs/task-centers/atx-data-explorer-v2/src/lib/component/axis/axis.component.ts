import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ControlContainer, UntypedFormArray } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  DataExplorerEventBus,
  BusEventTypes,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import { AxisModalComponent } from '../modals/axis-modal/axis-modal.component';

export interface DialogData {
  errorMessage: string;
}

@Component({
  selector: 'atx-axis',
  templateUrl: './axis.component.html',
  styleUrls: ['./axis.component.scss'],
})
export class AxisComponent implements OnInit, OnDestroy {
  @ViewChild('axisModal', { static: false }) public pinModal: ElementRef;

  public unsubscribe$ = new Subject<void>();
  public parentForm;
  public vm$: Observable<ChartState>;
  public ghostItems = [1];
  ngOnInit() {
    this.parentForm = this.parentControl.control;
    this.chartFacade.query.formElements$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((formElements) => {
        if (formElements && formElements?.axes) {
          this.ghostItems = formElements.axes.map((x) => 1);
        }
      });
  }

  constructor(
    public dialog: MatDialog,
    private chartFacade: ChartFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private parentControl: ControlContainer
  ) {
    this.vm$ = this.chartFacade.query.vm$;
  }

  get axes(): UntypedFormArray {
    return this.parentForm.get('axes') as UntypedFormArray;
  }

  addAxes() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.AXIS_ADD_NEW, {})
    );
  }

  removeAxis(axisIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      let errorMessage = '';
      for (let i = 0; i < vm.selectedTrend.trendDefinition.Series.length; i++) {
        if (vm.selectedTrend.trendDefinition.Series[i].Axis === axisIndex) {
          errorMessage = `${vm.selectedTrend.trendDefinition.Series[i].DisplayText} uses this Axis.`;
          break;
        }
      }
      if (
        vm.selectedTrend.trendDefinition.DesignCurves &&
        vm.selectedTrend.trendDefinition.DesignCurves.length > 0
      ) {
        for (
          let i = 0;
          i < vm.selectedTrend.trendDefinition.DesignCurves.length;
          i++
        ) {
          if (
            vm.selectedTrend.trendDefinition.DesignCurves[i]?.Axis === axisIndex
          ) {
            errorMessage = `Design Curve ${vm.selectedTrend.trendDefinition.DesignCurves[i]?.DisplayText} uses this Axis.`;
            break;
          }
        }
      }

      if (errorMessage !== '') {
        const rect: DOMRect =
          this.pinModal?.nativeElement?.getBoundingClientRect();
        const dialogRef = this.dialog.open(AxisModalComponent, {
          width: `${rect.width - 220}px`,
          height: `180px`,
          position: {
            left: `${rect.x + 60}px`,
            top: `${rect.y + 60}px`,
          },
          disableClose: true,
          data: {
            errorMessage: errorMessage,
          },
        });
      } else {
        this.dataExplorerEventBus.emit(
          new EmitEvent(BusEventTypes.AXIS_REMOVE, { axisIndex })
        );
      }
    });
  }

  positionToggle(axisIndex: number): void {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.POSITION_TOGGLE, { axisIndex })
    );
  }
  gridlineToggle(axisIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.GRIDLINE_TOGGLE, { axisIndex })
    );
  }

  gridLineXAxisToggle() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.GRIDLINE_XAXIS_TOGGLE, {})
    );
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
