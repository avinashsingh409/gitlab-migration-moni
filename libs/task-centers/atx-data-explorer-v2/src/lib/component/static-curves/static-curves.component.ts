import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  ControlContainer,
  UntypedFormArray,
  UntypedFormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { IPDTrendAxis } from '@atonix/atx-core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';

@Component({
  selector: 'atx-static-curves',
  templateUrl: './static-curves.component.html',
  styleUrls: ['./static-curves.component.scss'],
})
export class StaticCurvesComponent implements OnDestroy {
  public unsubscribe$ = new Subject<void>();
  curvesForm;
  axes: IPDTrendAxis[];
  public vm$: Observable<ChartState>;

  constructor(
    private fb: UntypedFormBuilder,
    private chartFacade: ChartFacade,
    private curvesControl: ControlContainer,
    private dataExplorerEventBus: DataExplorerEventBus
  ) {
    this.vm$ = this.chartFacade.query.vm$;
    this.curvesForm = this.curvesControl.control;
    this.chartFacade.query.selectedTrendAxes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((axes) => {
        this.axes = axes;
      });
  }

  get curves(): UntypedFormArray {
    return this.curvesForm.get('curves') as UntypedFormArray;
  }

  pointsLength(curveIndex: number): number {
    return (this.curves.at(curveIndex).get('points') as UntypedFormArray)
      .length;
  }

  pointsArray(pointIndex: number): UntypedFormArray {
    return this.curves.at(pointIndex).get('points') as UntypedFormArray;
  }

  addcurves() {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.CURVE_ADD_NEW, {})
    );
  }

  removeCurve(curveIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.CURVE_REMOVE, { curveIndex })
    );
  }

  addPoint(curveIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.CURVE_ADD_POINT, { curveIndex })
    );
  }

  removePoint(curveIndex: number, pointIndex: number) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.CURVE_REMOVE_POINT, {
        curveIndex,
        pointIndex,
      })
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
