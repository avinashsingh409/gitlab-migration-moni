/* eslint-disable rxjs/no-nested-subscribe */
import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ViewChildren,
  QueryList,
  HostListener,
} from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { map, switchMap, take, takeUntil } from 'rxjs/operators';
import { ITreeStateChange, selectAsset } from '@atonix/atx-asset-tree';
import {
  ITimeSliderState,
  ITimeSliderStateChange,
} from '@atonix/atx-time-slider';
import {
  NavFacade,
  getTrendIdFromRoute,
  getStartFromRoute,
  getEndFromRoute,
  getUniqueIdFromRoute,
} from '@atonix/atx-navigation';
import { AssetTreeFacade } from '../../store/facade/asset-tree.facade';
import { TimeSliderFacade } from '../../store/facade/time-slider.facade';
import { DataExplorerFacade } from '../../store/facade/data-explorer.facade';
import { ActivatedRoute } from '@angular/router';
import {
  getADayAgo,
  getAMonthAgo,
  getAMonthAgoLive,
  getToday,
  LoggerService,
  ToastService,
} from '@atonix/shared/utils';
import {
  ChartDisplayComponent,
  ChartFacade,
  ChartState,
  DataSourceValues,
  getBandAxisMeasurementIndex,
  IBtnGrpStateChange,
  IUpdateLimitsData,
  processBandAxisBarsAndMeasurements,
  reflow,
  reflowFast,
  SummaryTypes,
} from '@atonix/atx-chart-v2';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  FormControl,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { AxisFormService } from '../../service/axis-form.service';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';
import {
  DesignCurve,
  IAssetVariableTypeTagMap,
  ICriteria,
  isNil,
} from '@atonix/atx-core';
import { MatSelectChange } from '@angular/material/select';
import { PinsFormService } from '../../service/pins-form.service';
import { SeriesFormService } from '../../service/series-form.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SaveChangesModalComponent } from '../modals/save-changes-modal/save-changes-modal.component';
import { TagListFacade } from '../../service/tag-list.facade';
import { SaveModalComponent } from '../modals/save-modal/save-modal.component';
import { DeleteModalComponent } from '../modals/delete-modal/delete-modal.component';
import { SplitAreaDirective, SplitComponent } from 'angular-split';
import { ComponentCanDeactivate } from '../../guard/pending-changes-guard';
import { ChartActionsFacade } from '../../store/facade/chart.facade';
import { CurveFormService } from '../../service/curve-form.service';
import moment from 'moment';
import { AdvancedFormService } from '../../service/advanced-form.service';
import { GroupedSeriesFormService } from '../../service/grouped-series-form.service';
import { AdvancedFacade } from '../../service/advanced.facade';
import { GroupedSeriesFacade } from '../../service/grouped-series.facade';
import { GroupedUpdateComponent } from '../modals/grouped-update/grouped-update.component';
import { SeriesFacade } from '../../service/series.facade';
import produce from 'immer';

@Component({
  selector: 'atx-data-explorer',
  templateUrl: './data-explorer.component.html',
  styleUrls: ['./data-explorer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    AxisFormService,
    PinsFormService,
    SeriesFormService,
    CurveFormService,
    AdvancedFormService,
    GroupedSeriesFormService,
  ],
})
export class DataExplorerComponent
  implements OnInit, OnDestroy, AfterViewInit, ComponentCanDeactivate
{
  @ViewChild('trendView', { static: false }) public trendView: ElementRef;
  @ViewChild(ChartDisplayComponent, { static: false })
  chartDisplay: ChartDisplayComponent;
  @ViewChild(SplitComponent) splitEl: SplitComponent;
  @ViewChildren(SplitAreaDirective) areasEl: QueryList<SplitAreaDirective>;

  public vm$: Observable<ChartState>;
  public unsubscribe$ = new Subject<void>();
  public timeSliderState$: Observable<ITimeSliderState>;
  public timeSliderStateChanges$: Subject<ITimeSliderStateChange>;
  public mainForm: UntypedFormGroup;
  public summaryTypes = SummaryTypes;

  private toggleTop = false;
  private toggleBottom = false;
  topSizePercent = 60;
  bottomSizePercent = 40;
  constructor(
    public navFacade: NavFacade,
    private dialog: MatDialog,
    public assetTreeFacade: AssetTreeFacade,
    public timeSliderFacade: TimeSliderFacade,
    public dataExplorerFacade: DataExplorerFacade,
    private chartFacade: ChartFacade,
    private snackBarService: ToastService,
    private activatedRoute: ActivatedRoute,
    private pinsFormsService: PinsFormService,
    private axisFormService: AxisFormService,
    private dataExplorerEventBus: DataExplorerEventBus,
    private fb: UntypedFormBuilder,
    private chartActionsFacade: ChartActionsFacade,
    private seriesFormService: SeriesFormService,
    private groupedSeriesFormService: GroupedSeriesFormService,
    private curveFormService: CurveFormService,
    private advancedFormService: AdvancedFormService,
    private tagListFacade: TagListFacade,
    private advancedFacade: AdvancedFacade,
    private groupedSeriesFacade: GroupedSeriesFacade,
    private seriesFacade: SeriesFacade,
    private logger: LoggerService
  ) {
    dataExplorerEventBus.on(
      BusEventTypes.GRIDLINE_TOGGLE,
      this.axisGridLineToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GRIDLINE_XAXIS_TOGGLE,
      this.gridLineXAxisToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.AXIS_ADD_NEW,
      this.axisAddNew.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.AXIS_REMOVE,
      this.axisRemove.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.CURVE_ADD_NEW,
      this.curveAddNew.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.CURVE_REMOVE,
      this.curveRemove.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.CURVE_ADD_POINT,
      this.curveAddPoint.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.CURVE_REMOVE_POINT,
      this.curveRemovePoint.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.POSITION_TOGGLE,
      this.positionToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.PIN_TOGGLE,
      this.pinsToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.PIN_HIDE_TOGGLE,
      this.pinHideToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.PIN_TYPE_CHANGE,
      this.pinTypeChange.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.REMOVE_SERIES_FROM_CHART,
      this.removeSeriesFromChart.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.ACTIVE_TOGGLE,
      this.activeToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_VISIBILITY_TOGGLE,
      this.seriesVisibilityToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_FILTER_TOGGLE,
      this.seriesFilterToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_COLOR,
      this.seriesColor.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_X_AXIS,
      this.seriesXAxis.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_BOLD_TOGGLE,
      this.seriesBoldToggle.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.SERIES_SAVE,
      this.saveSeries.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.REORDER_SERIES,
      this.reorderSeries.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.PIN_ADD_NEW,
      this.addNewPin.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.PIN_REMOVE,
      this.removePin.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.REORDER_PIN,
      this.reorderPin.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.TAG_LIST_ASSET_TREE_DROPDOWN_STATE_CHANGE,
      this.assetTreeDropdownStateChange.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.TAG_LIST_ADD_SERIES_TO_CHART,
      this.addSeriesFromTagList.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.ADVANCED_UPDATE_CHART,
      this.advancedUpdateChart.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_CLEAR,
      this.clearGroupedSeries.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_SAVE,
      this.saveGroupedSeries.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.GROUPED_SERIES_ADD_MEASUREMENT,
      this.groupedSeriesAddMeasurement.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.ON_SAVE_MODEL_ASSET_TREE_DROPDOWN_STATE_CHANGE,
      this.saveModelAssetTreeDropdownStateChange.bind(this)
    );
    dataExplorerEventBus.on(
      BusEventTypes.LOAD_DATA_EXPLORER_CHARTS,
      this.loadDataExplorerCharts.bind(this)
    );
    const initialStartFromRoute = getStartFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const initialEndFromRoute = getEndFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const initialTrendFromRoute = getTrendIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.dataExplorerFacade.initializeDataExplorer(
      getUniqueIdFromRoute(this.activatedRoute.snapshot.queryParamMap),
      initialTrendFromRoute ? initialTrendFromRoute : '',
      initialStartFromRoute
        ? new Date(parseFloat(initialStartFromRoute))
        : getAMonthAgoLive(),
      initialEndFromRoute
        ? new Date(parseFloat(initialEndFromRoute))
        : new Date()
    );

    this.loadDataExplorerCharts();

    this.dataExplorerFacade.selectedAssetUniqueKey$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        if (id) {
          this.assetTreeDropdownStateChange({ change: selectAsset(id) });
          this.saveModelAssetTreeDropdownStateChange({
            change: selectAsset(id),
          });
        }
      });

    this.dataExplorerFacade.selectAssetTreeDropdownConfiguration$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetTreeConfiguration) => {
        this.tagListFacade.updateAssetTreeConfiguration(assetTreeConfiguration);
      });

    this.dataExplorerFacade.selectAssetTreeDropdownAssetName$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetName) => {
        if (assetName) {
          this.tagListFacade.updateAssetName(assetName);
        }
      });

    this.dataExplorerFacade.selectAssetTreeDropdownSelectedAssetIds$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((ids) => {
        this.tagListFacade.assetID$
          .pipe(take(1))
          .subscribe((assetID: string) => {
            if (ids && assetID !== ids.assetUniqueKey) {
              this.tagListFacade.updateAssetID(ids.assetUniqueKey);
              this.tagListFacade.updateSearchAssetID(ids.assetId);
            }
          });
      });

    this.timeSliderFacade.timeSliderSelection$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((date) => {
        if (date) {
          this.dataExplorerFacade.updateRoute();
        }
      });
    this.dataExplorerFacade.selectToast$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        if (data) {
          this.snackBarService.openSnackBar(data.message, data.type);
        }
      });
  }

  @HostListener('window:resize', ['$event'])
  onResize(_unusedEvent: any) {
    const rect: DOMRect =
      this.trendView?.nativeElement?.getBoundingClientRect();
    if (rect?.height) {
      this.chartFacade.updateBounds(rect.height, false);
    }
  }

  get seriesArray(): UntypedFormArray {
    return this.mainForm.get('series') as UntypedFormArray;
  }

  get groupedSeriesArray(): UntypedFormArray {
    return this.mainForm.get('groupedSeries') as UntypedFormArray;
  }

  get curvesArray(): UntypedFormArray {
    return this.mainForm.get('curves') as UntypedFormArray;
  }

  get axesArray(): UntypedFormArray {
    return this.mainForm.get('axes') as UntypedFormArray;
  }

  get pinsArray(): UntypedFormArray {
    return this.mainForm.get('pins') as UntypedFormArray;
  }

  get advancedForm(): UntypedFormGroup {
    return this.mainForm.get('advanced') as UntypedFormGroup;
  }

  set advancedForm(form: UntypedFormGroup) {
    this.mainForm.setControl('advanced', form);
  }

  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> {
    return this.vm$.pipe(
      switchMap((vm) => {
        let hasDirtyTrends = false;
        for (const key in vm.isDirty) {
          if (vm.isDirty[key]) {
            hasDirtyTrends = true;
          }
        }
        if (hasDirtyTrends) {
          return of(false);
        }
        return of(true);
      })
    );
  }

  addNewChart(isGroupedSeries: boolean) {
    this.chartFacade.addNewChart(isGroupedSeries);
    this.dataExplorerEventBus.emit(
      new EmitEvent(BusEventTypes.SETTINGS_SELECT_TAB, {
        tabIndex: 1,
      })
    );
  }

  cleanSubscriptions() {
    this.seriesFormService.resetSeries();
    this.seriesArray.clear();
    this.groupedSeriesFormService.resetSeries();
    this.groupedSeriesArray.clear();
    this.curveFormService.resetSeries();
    this.curvesArray.clear();
    this.pinsFormsService.resetPins();
    this.pinsArray.clear();
  }

  ngAfterViewInit() {
    const rect: DOMRect =
      this.trendView?.nativeElement?.getBoundingClientRect();
    if (rect?.height) {
      this.chartFacade.updateBounds(rect.height, false);
    }
  }

  ngOnInit(): void {
    this.logger.trackAppInsightsEvent('DataExplorerV2:Loaded');
    this.chartFacade.query.selectedTrendID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((selectedTrend) => {
        if (selectedTrend) {
          this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.chartID !== selectedTrend) {
              this.groupedSeriesFacade.reset();
            }
          });
        }
      });
    this.chartFacade.query.errorNoData$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((error) => {
        if (error) {
          this.cleanSubscriptions();
        }
      });
    this.mainForm = this.fb.group({
      series: this.fb.array([]),
      axes: this.fb.array([]),
      groupedSeries: this.fb.array([]),
      pins: this.fb.array([]),
      curves: this.fb.array([]),
      advanced: this.fb.group({}),
    });

    this.chartFacade.query.axesFormElement$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((axes) => {
        if (axes) {
          this.axisFormService.resetAxis();
          this.axesArray.clear();

          axes.forEach((axis, axisIndex) => {
            const axisFormGroup = this.fb.group({
              axisUnits: this.axisFormService.axisUnits(axisIndex, axis?.Title),
              axisLowLimits: this.axisFormService.axisLowLimits(
                axisIndex,
                axis?.Min?.toString()
              ),
              axisHighLimits: this.axisFormService.axisHighLimits(
                axisIndex,
                axis?.Max?.toString()
              ),
              axisStep: this.axisFormService.axisStep(
                axisIndex,
                axis?.Step?.toString()
              ),
              axisMinorStep: this.axisFormService.axisMinorStep(
                axisIndex,
                axis?.MinorStep?.toString()
              ),
            });
            this.axesArray.push(axisFormGroup);
          });
        }
      });

    this.chartFacade.query.formElements$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((formElements) => {
        if (formElements) {
          this.cleanSubscriptions();
          formElements.pins.forEach((pins, pinIndex) => {
            const pinsFormGroup = this.fb.group({
              name: this.pinsFormsService.pinName(pinIndex, pins.NameFormat),
              startDate: this.pinsFormsService.pinStartDate(pins.StartTime),
              startTime: this.pinsFormsService.pinStartTime(pins.StartTime),
              endTime: this.pinsFormsService.pinEndTime(pins.EndTime),
              endDate: this.pinsFormsService.pinEndDate(pins.EndTime),
            });
            this.pinsArray.push(pinsFormGroup);
          });

          formElements.curves.forEach((curve: DesignCurve, curveIndex) => {
            const values: { X: any; Y: any; Y1: any; Y2: any }[] = JSON.parse(
              curve.Values
            );
            const data = values.map((d) => {
              return {
                // Normal Plots use dates, scatter plots use numbers.
                x: formElements.isXY ? Number(d.X) : new Date(d.X),
                y: Number(d.Y),
                low: Number(d.Y1),
                high: Number(d.Y2),
              };
            });

            const curveFormGroup = this.fb.group({
              curveName: this.curveFormService.curveName(
                curveIndex,
                curve?.DisplayText
              ),
              axisUnits: this.curveFormService.axisUnits(
                curveIndex,
                curve?.Axis
              ),
              curveColor: this.curveFormService.curveColor(
                curveIndex,
                curve?.Color
              ),
              curveType: this.curveFormService.curveType(
                curveIndex,
                curve?.Type
              ),
              curveRepeat: this.curveFormService.curveRepeat(
                curveIndex,
                curve?.Repeat
              ),
              points: this.fb.array([]),
            });
            data.forEach((dataPoint, i) => {
              let startDate = getADayAgo();
              let startTime = moment(startDate).format('HH:mm');
              if (!formElements.isXY) {
                startDate = new Date(dataPoint.x);
                startTime = moment(dataPoint.x).format('HH:mm');
              }
              const pointsGroup = this.fb.group({
                pointDate: this.curveFormService.buildPointDate(
                  curveIndex,
                  i,
                  startDate
                ),
                pointTime: this.curveFormService.buildPointTime(
                  curveIndex,
                  i,
                  startTime
                ),
                pointX: this.curveFormService.curveX(
                  curveIndex,
                  i,
                  formElements.isXY ? dataPoint.x.toString() : '0'
                ),
                pointY: this.curveFormService.curveY(
                  curveIndex,
                  i,
                  formElements.isXY,
                  dataPoint.y.toString()
                ),
                pointY1: this.curveFormService.curveY1(
                  curveIndex,
                  i,
                  formElements.isXY,
                  dataPoint.low.toString()
                ),
                pointY2: this.curveFormService.curveY2(
                  curveIndex,
                  i,
                  formElements.isXY,
                  dataPoint.high.toString()
                ),
              });
              (curveFormGroup.get('points') as UntypedFormArray).push(
                pointsGroup
              );
            });

            this.curvesArray.push(curveFormGroup);
            // api doesn't need to know about design curves. TODO: remove from API.
            // current fix: Hide them.
          });
          if (formElements.groupedSeries.length > 0) {
            // if groupedSeries Facade is Dirty, we need to use the elements there
            // instead of the elements coming from the formGroup.
            // This is so that reordering the series doesn't reset elements.
            this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
              if (!vm.isDirty) {
                this.groupedSeriesFacade.loadSeries(
                  formElements.groupedSeries,
                  formElements.id
                );
              }

              formElements.groupedSeries.forEach(
                (groupedSeries, seriesIndex) => {
                  let axisExists = false;
                  if (
                    formElements.axes.some(
                      (axis) => axis.Axis === groupedSeries.Axis
                    )
                  ) {
                    axisExists = true;
                  }
                  const seriesFormGroup = this.fb.group({
                    seriesName: this.groupedSeriesFormService.seriesName(
                      seriesIndex,
                      groupedSeries?.Name
                    ),
                    axisUnits: this.groupedSeriesFormService.axisUnits(
                      seriesIndex,
                      axisExists ? groupedSeries?.Axis : 0
                    ),
                    groupName: this.groupedSeriesFormService.groupName(
                      seriesIndex,
                      groupedSeries?.BarName
                    ),
                    // this values are stored in the groupedSeriesFacade until the user presses 'Update Chart'
                    seriesType: this.groupedSeriesFormService.groupedSeriesType(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].seriesType.currentValue
                        : groupedSeries?.Type
                    ),
                    dataSource1:
                      this.groupedSeriesFormService.groupedSeriesDataSource1(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].dataSource1.currentValue ===
                            1
                            ? DataSourceValues.Constant.toString()
                            : DataSourceValues.Tag.toString()
                          : !isNil(groupedSeries.Value1)
                          ? DataSourceValues.Constant.toString()
                          : DataSourceValues.Tag.toString()
                      ),
                    dataSource2:
                      this.groupedSeriesFormService.groupedSeriesDataSource2(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].dataSource2.currentValue ===
                            1
                            ? DataSourceValues.Constant.toString()
                            : DataSourceValues.Tag.toString()
                          : !isNil(groupedSeries.Value2)
                          ? DataSourceValues.Constant.toString()
                          : DataSourceValues.Tag.toString()
                      ),
                    dataSource3:
                      this.groupedSeriesFormService.groupedSeriesDataSource3(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].dataSource3.currentValue == 1
                            ? DataSourceValues.Constant.toString()
                            : DataSourceValues.Tag.toString()
                          : !isNil(groupedSeries.Value3)
                          ? DataSourceValues.Constant.toString()
                          : DataSourceValues.Tag.toString()
                      ),
                    dataSource4:
                      this.groupedSeriesFormService.groupedSeriesDataSource4(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].dataSource4.currentValue == 1
                            ? DataSourceValues.Constant.toString()
                            : DataSourceValues.Tag.toString()
                          : !isNil(groupedSeries.Value4)
                          ? DataSourceValues.Constant.toString()
                          : DataSourceValues.Tag.toString()
                      ),
                    dataSource5:
                      this.groupedSeriesFormService.groupedSeriesDataSource5(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].dataSource5.currentValue == 1
                            ? DataSourceValues.Constant.toString()
                            : DataSourceValues.Tag.toString()
                          : !isNil(groupedSeries.Value5)
                          ? DataSourceValues.Constant.toString()
                          : DataSourceValues.Tag.toString()
                      ),
                    value1: this.groupedSeriesFormService.groupedSeriesValue1(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].value1.currentValue
                        : groupedSeries?.Value1
                    ),
                    value2: this.groupedSeriesFormService.groupedSeriesValue2(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].value2.currentValue
                        : groupedSeries?.Value2
                    ),
                    value3: this.groupedSeriesFormService.groupedSeriesValue3(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].value3.currentValue
                        : groupedSeries?.Value3
                    ),
                    value4: this.groupedSeriesFormService.groupedSeriesValue4(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].value4.currentValue
                        : groupedSeries?.Value4
                    ),
                    value5: this.groupedSeriesFormService.groupedSeriesValue5(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].value5.currentValue
                        : groupedSeries?.Value5
                    ),
                    aggregationType1:
                      this.groupedSeriesFormService.groupedSeriesAggregationType1(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[
                              seriesIndex
                            ].aggregationType1.currentValue.toString()
                          : groupedSeries?.AggregationType1?.toString() ?? '3'
                      ),
                    aggregationType2:
                      this.groupedSeriesFormService.groupedSeriesAggregationType2(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[
                              seriesIndex
                            ].aggregationType2.currentValue.toString()
                          : groupedSeries?.AggregationType2?.toString() ?? '3'
                      ),
                    aggregationType3:
                      this.groupedSeriesFormService.groupedSeriesAggregationType3(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[
                              seriesIndex
                            ].aggregationType3.currentValue.toString()
                          : groupedSeries?.AggregationType3?.toString() ?? '3'
                      ),
                    aggregationType4:
                      this.groupedSeriesFormService.groupedSeriesAggregationType4(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[
                              seriesIndex
                            ].aggregationType4.currentValue.toString()
                          : groupedSeries?.AggregationType4?.toString() ?? '3'
                      ),
                    aggregationType5:
                      this.groupedSeriesFormService.groupedSeriesAggregationType5(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[
                              seriesIndex
                            ].aggregationType5.currentValue.toString()
                          : groupedSeries?.AggregationType5?.toString() ?? '3'
                      ),
                    params1: this.groupedSeriesFormService.groupedSeriesParams1(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].params1.currentValue
                        : +groupedSeries?.Params1 ?? 0
                    ),
                    params2: this.groupedSeriesFormService.groupedSeriesParams2(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].params2.currentValue
                        : +groupedSeries?.Params2 ?? 0
                    ),
                    params3: this.groupedSeriesFormService.groupedSeriesParams3(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].params3.currentValue
                        : +groupedSeries?.Params3 ?? 0
                    ),
                    params4: this.groupedSeriesFormService.groupedSeriesParams4(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].params4.currentValue
                        : +groupedSeries?.Params4 ?? 0
                    ),
                    params5: this.groupedSeriesFormService.groupedSeriesParams4(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].params5.currentValue
                        : +groupedSeries?.Params5 ?? 0
                    ),
                    symbol: this.groupedSeriesFormService.groupedSeriesSymbol(
                      seriesIndex,
                      vm.isDirty
                        ? vm.series[seriesIndex].symbol.currentValue
                        : groupedSeries.Symbol ?? 'triangle'
                    ),
                    outlierType:
                      this.groupedSeriesFormService.groupedOutlierType(
                        seriesIndex,
                        vm.isDirty
                          ? vm.series[seriesIndex].outlierType.currentValue
                          : groupedSeries.Flags ?? ''
                      ),
                  });
                  this.groupedSeriesArray.push(seriesFormGroup);
                }
              );
            });
          } else {
            this.groupedSeriesFacade.reset();
          }

          if (formElements.series.length > 0) {
            this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
              if (!vm.isDirty) {
                this.seriesFacade.loadSeries(formElements.series);
              }

              formElements.series.forEach((series, seriesIndex) => {
                const seriesFormGroup = this.fb.group({
                  seriesName: this.seriesFormService.seriesName(
                    seriesIndex,
                    series?.DisplayText
                  ),
                  tagName: this.seriesFormService.tagName(
                    series?.MapData[0]?.Map?.Tag?.TagName
                  ),
                  axisUnits: this.seriesFormService.axisUnits(
                    seriesIndex,
                    series?.Axis
                  ),
                  stackType: this.seriesFormService.stackType(
                    seriesIndex,
                    series?.StackType || ''
                  ),
                  filterMin: this.seriesFormService.filterMin(
                    seriesIndex,
                    series?.FilterMin
                  ),
                  filterMax: this.seriesFormService.filterMax(
                    seriesIndex,
                    series?.FilterMax
                  ),
                  flatlineThreshold: this.seriesFormService.flatlineThreshold(
                    seriesIndex,
                    series?.FlatlineThreshold
                  ),
                  applyFilterToAll: this.seriesFormService.applyFilterToAll(
                    seriesIndex,
                    series?.ApplyToAll
                  ),
                });
                this.seriesArray.push(seriesFormGroup);
              });
            });
          } else {
            this.seriesFacade.reset();
          }

          this.advancedFacade.update({
            filter: formElements?.advanced?.filter,
          });

          this.advancedForm = this.fb.group({
            excludeHours: this.advancedFormService.excludeHours(
              formElements?.advanced?.filter?.excludeHours
            ),
            filterMin: this.advancedFormService.filterMin(
              formElements?.advanced?.filter?.FilterMin
            ),
            filterMax: this.advancedFormService.filterMax(
              formElements?.advanced?.filter?.FilterMax
            ),
            flatlineThreshold: this.advancedFormService.flatlineThreshold(
              formElements?.advanced?.filter?.FlatlineThreshold
            ),
            dataRetrievalMethod: this.advancedFormService.dataRetrievalMethod(
              formElements?.advanced?.dataRetrievalMethod
            ),
            dataRetrievalMinInterval:
              this.advancedFormService.dataRetrievalMinInterval(
                formElements?.advanced?.dataRetrievalMinInterval
              ),
            dataRetrievalMinIntervalUnits:
              this.advancedFormService.dataRetrievalMinIntervalUnits(
                formElements?.advanced?.dataRetrievalMinIntervalUnits
              ),
            dataRetrievalArchive: this.advancedFormService.dataRetrievalArchive(
              formElements?.advanced?.dataRetrievalArchive
            ),
          });
        }
      });
    this.vm$ = this.chartFacade.query.vm$;

    this.chartFacade.query.selectedTrendID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((selectedTrendID) => {
        if (selectedTrendID) {
          this.dataExplorerFacade.updateTrendID(selectedTrendID);
        }
      });

    // This works because it triggers the Highcharts to reflow.
    // it only relies on the actual window resizing thus manually dispatching this

    this.navFacade.navPaneState$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflow();
      });
    this.navFacade.showTimeSlider$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflow();
      });
    this.navFacade.assetTreeSelected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflow();
      });
    this.dataExplorerFacade.leftTraySize$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflowFast();
      });
    this.dataExplorerFacade.layoutMode$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflow();
      });
    this.chartFacade.query.trendsLoading$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        reflowFast();
      });
  }

  splitDragEnd(e: { gutterNum: number; sizes: Array<number> }) {
    if (e?.sizes) {
      this.topSizePercent = e.sizes[0];
      this.bottomSizePercent = e.sizes[1];
    }
    const rect: DOMRect =
      this.trendView?.nativeElement?.getBoundingClientRect();
    if (rect?.height) {
      this.chartFacade.updateBounds(rect.height, true);
    }
  }

  ngOnDestroy(): void {
    this.cleanSubscriptions();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  loadDataExplorerCharts(newChartID: string = null) {
    this.dataExplorerFacade.selectedAssetIds$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        this.chartFacade.query.assetGuid$
          .pipe(take(1))
          .subscribe((assetGuid: string) => {
            if (id) {
              if (assetGuid === id.assetGuid) {
                return;
              }
              const startFromRoute = getStartFromRoute(
                this.activatedRoute.snapshot.queryParamMap
              );
              const endFromRoute = getEndFromRoute(
                this.activatedRoute.snapshot.queryParamMap
              );
              this.assetTreeFacade.selectAsset(id.assetGuid);
              const trendID =
                newChartID ??
                getTrendIdFromRoute(this.activatedRoute.snapshot.queryParamMap);
              this.chartFacade.getLoadDataExplorerCharts({
                nodeID: id.assetGuid,
                trendID: trendID,
                startDate: startFromRoute
                  ? new Date(parseFloat(startFromRoute))
                  : getAMonthAgo(),
                endDate: endFromRoute
                  ? new Date(parseFloat(endFromRoute))
                  : getToday(),
              });
              this.chartFacade.getCriteriaWithDataTimeFilters({
                assetGuid: id.assetGuid,
                assetID: id.assetId,
              });
            }
          });
      });
  }

  onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.assetTreeFacade.treeSizeChange(newSize);

      setTimeout(() => {
        this.chartFacade.reflow();
      }, 100);
    }
  }

  // This is the thing that actually makes changes to the asset tree state.  We handle what we
  // need to handle and then call the default behavior.
  onAssetTreeStateChange(change: ITreeStateChange) {
    if (change?.event === 'SelectAsset') {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        let hasDirtyTrends = false;
        for (const key in vm.isDirty) {
          if (vm.isDirty[key]) {
            hasDirtyTrends = true;
          }
        }
        if (hasDirtyTrends) {
          const dialogConfig = new MatDialogConfig();
          const dialogRef = this.dialog.open(
            SaveChangesModalComponent,
            dialogConfig
          );
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              this.groupedSeriesFacade.reset();
              this.assetTreeFacade.treeStateChange(change);
            }
          });
        } else {
          this.groupedSeriesFacade.reset();
          this.assetTreeFacade.treeStateChange(change);
        }
      });
    } else {
      this.assetTreeFacade.treeStateChange(change);
    }
  }

  deleteChart() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const dialogRef = this.dialog.open(DeleteModalComponent, {
        disableClose: true,
        data: {
          trendName: vm.selectedTrend.trendDefinition.Title,
          trendID: vm.selectedTrend.id,
        },
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result?.response === 'deleted') {
          let message = 'Chart Deleted';
          if (vm.selectedTrend.trendDefinition.IsStandardTrend) {
            message = 'Chart Reverted';
          }
          this.snackBarService.openSnackBar(message, 'success');
          this.chartFacade.deleteChart(vm.selectedTrend.id);
        } else if (result?.response === 'error') {
          this.snackBarService.openSnackBar('An Error Occurred', 'error');
        }
      });
    });
  }

  clearGroupedSeries() {
    this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.groupedSeriesFacade.clearGroupedSeries();
      this.chartFacade.setSelectedTrend(vm.selectedTrend);
      this.chartFacade.setGroupedSeriesFormElement(
        vm.selectedTrend.groupSeriesMeasurements
      );
    });
  }

  saveGroupedSeries() {
    this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
      // keep our tag IDs, clear the series out
      this.groupedSeriesFacade.saveGroupedSeries(
        vm.chartID,
        vm.tagIDs,
        vm.tagsLoaded
      );
      this.chartFacade.updateGroupedSeriesState(vm.series);
    });
  }

  saveChart() {
    this.groupedSeriesFacade.vm$.pipe(take(1)).subscribe((groupedVm) => {
      if (groupedVm.isDirty) {
        this.dialog.open(GroupedUpdateComponent, {
          disableClose: true,
          width: '440px',
        });
      } else {
        this.vm$.pipe(take(1)).subscribe((vm) => {
          const seriesCount = vm.isGroupedSeries
            ? vm.selectedTrend.groupSeriesMeasurements.length
            : vm.selectedTrend.trendDefinition.Series.length;

          if (seriesCount <= 500) {
            // if it's a new chart the user cannot save over an existing chart.
            // if it's an existing chart a user can save over it
            const trendNames = vm.selectedTrend?.isNew
              ? vm.trends
                  .filter((trend) => !trend?.isNew)
                  .map((trend) => trend.label)
              : vm.trends
                  .filter((trend) => trend.id !== vm.selectedTrend.id)
                  .map((trend) => trend.label);
            const dialogRef = this.dialog.open(SaveModalComponent, {
              disableClose: true,
              width: '440px',
              data: {
                trendName: vm.selectedTrend.trendDefinition.Title,
                trendNames,
              },
            });
            dialogRef.afterClosed().subscribe((result) => {
              if (result) {
                this.snackBarService.openSnackBar('Chart Saved', 'success');
              }
            });
          } else {
            this.dataExplorerFacade.maximumNewSeriesReached();
          }
        });
      }
    });
  }

  onTimeSliderStateChange(change: ITimeSliderStateChange) {
    this.timeSliderFacade.timeSliderStateChange(change);
    if (change.event === 'HideTimeSlider') {
      this.timeSliderFacade.hideTimeSlider();
    } else if (change.event === 'SelectDateRange') {
      this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
        if (change.newEndDateValue && change.newStartDateValue) {
          const currentStart = moment(vm.start).unix();
          const newStart = moment(change.newStartDateValue).unix();
          const currentEnd = moment(vm.end).unix();
          const newEnd = moment(change.newEndDateValue).unix();
          if (currentStart !== newStart || currentEnd !== newEnd) {
            this.chartFacade.updateTimeRange(
              change.newStartDateValue,
              change.newEndDateValue
            );
          }
        }
      });
    }
  }

  onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'UpdateLimits') {
      this.chartFacade.updateChartYAxisLimit(
        change.newValue as IUpdateLimitsData
      );
    }
    if (change.event === 'ResetLimits') {
      this.chartFacade.resetChartYAxisLimit(change.newValue as number);
    }
    if (change.event === 'ChangeLabels') {
      this.chartFacade.changeLabelsClicked(change.newValue as number);
    }
    if (change.event === 'LegendItemToggle') {
      this.logger.trackTaskCenterEvent(
        'DataExplorerV2:SeriesVisibilityToggle',
        'Chart'
      );
      this.seriesVisibilityToggle({
        seriesIndex: change.newValue as number,
      });
    }
    if (change.event === 'DataCursorToggle') {
      this.chartFacade.dataCursorToggle();
    }
  }

  trendSelected(trendId: string) {
    this.chartFacade.selectTrendDropdown(trendId);
  }

  mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  onExpandBottom() {
    this.logger.trackTaskCenterEvent('DataExplorerV2:ExpandChart');
    this.toggleTop = false;
    this.toggleBottom = true;
    this.areasEl.toArray()[0].visible = false;
    this.areasEl.toArray()[0].collapse();
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].expand();
  }

  onExpandTop() {
    this.logger.trackTaskCenterEvent('DataExplorerV2:ExpandConfig');
    this.toggleTop = true;
    this.toggleBottom = false;
    this.areasEl.toArray()[1].visible = false;
    this.areasEl.toArray()[1].collapse();
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].expand();
    setTimeout(() => {
      const rect: DOMRect =
        this.trendView?.nativeElement?.getBoundingClientRect();
      this.chartFacade.updateBounds(rect.height, true);
    }, 300);
  }

  onCollapse() {
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].collapse(this.topSizePercent);
    this.areasEl.toArray()[0].order = 0;
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].collapse(this.bottomSizePercent);
    this.areasEl.toArray()[1].order = 1;
    this.toggleTop = false;
    this.toggleBottom = false;
    setTimeout(() => {
      const rect: DOMRect =
        this.trendView?.nativeElement?.getBoundingClientRect();
      this.chartFacade.updateBounds(rect.height, true);
    }, 300);
  }

  topIndicatorClick() {
    if (!this.toggleTop) {
      this.onExpandTop();
    } else {
      this.onCollapse();
    }
  }

  bottomIndicatorClick() {
    if (!this.toggleBottom) {
      this.onExpandBottom();
    } else {
      this.onCollapse();
    }
  }

  curveAddNew() {
    this.chartFacade.addNewCurve();
  }

  curveRemove(curve: any) {
    if (!isNil(curve?.curveIndex)) {
      this.chartFacade.removeCurve(curve?.curveIndex);
    }
  }

  curveAddPoint(curve: any) {
    if (!isNil(curve?.curveIndex)) {
      this.chartFacade.addCurvePoint(curve?.curveIndex);
    }
  }

  curveRemovePoint(curve: any) {
    if (!isNil(curve?.curveIndex)) {
      this.chartFacade.removeCurvePoint(curve?.curveIndex, curve?.pointIndex);
    }
  }

  axisAddNew() {
    this.chartFacade.addNewAxis();
  }

  axisRemove(axis: any) {
    if (!isNil(axis?.axisIndex)) {
      this.chartFacade.removeAxis(axis?.axisIndex);
    }
  }

  axisGridLineToggle(axis: any) {
    if (!isNil(axis?.axisIndex)) {
      this.axisFormService.gridlineToggle(axis?.axisIndex);
    }
  }

  gridLineXAxisToggle() {
    this.chartFacade.gridLineXAxisToggle();
  }

  positionToggle(axis: any) {
    if (!isNil(axis?.axisIndex)) {
      this.axisFormService.positionToggle(axis?.axisIndex);
    }
  }

  seriesColor(series: {
    seriesIndex: number;
    color: string;
    isGroupedSeries: boolean;
  }) {
    if (
      isNil(series?.seriesIndex) ||
      isNil(series?.color || isNil(series?.isGroupedSeries))
    ) {
      return;
    }
    if (series.isGroupedSeries) {
      this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
        const updateMeasurement =
          vm.selectedTrend.groupSeriesMeasurements[series.seriesIndex];
        if (updateMeasurement) {
          const newState = produce(vm.selectedTrend, (state) => {
            state.groupSeriesMeasurements[series.seriesIndex].FillColor =
              series.color;
            state.groupSeriesMeasurements[series.seriesIndex].Color =
              series.color;
            state.measurements?.Bands.forEach((band) => {
              const idx = getBandAxisMeasurementIndex(
                band.Measurements,
                updateMeasurement
              );
              if (idx !== -1) {
                band.Measurements[idx].Color = series.color;
                band.Measurements[idx].FillColor = series.color;
              }
            });
            processBandAxisBarsAndMeasurements(state);
          });
          this.chartFacade.setSelectedTrend(newState);
        }
        const isDirty = produce(vm.isDirty, (state) => {
          state[vm.selectedTrend.id] = true;
        });
        this.chartFacade.setDirty(isDirty);
      });
    } else {
      this.chartFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.selectedTrend, (state) => {
          state.trendDefinition.Series[series.seriesIndex].SeriesColor =
            series.color;
        });
        vm.selectedTrend.groupedSeriesType;
        this.chartFacade.setSelectedTrend(newState);
        const isDirty = produce(vm.isDirty, (state) => {
          state[vm.selectedTrend.id] = true;
        });
        this.logger.trackTaskCenterEvent(
          'DataExplorerV2:SeriesColor',
          series.color
        );
        this.chartFacade.setDirty(isDirty);
      });
    }
  }

  seriesVisibilityToggle(series: any) {
    if (!isNil(series?.seriesIndex)) {
      this.seriesFormService.seriesVisibilityToggle(series?.seriesIndex);
    }
  }

  seriesFilterToggle(series: any) {
    if (!isNil(series?.seriesIndex)) {
      this.seriesFormService.seriesFilterToggle(series?.seriesIndex);
    }
  }

  seriesXAxis(series: any) {
    if (!isNil(series?.series)) {
      this.chartFacade.seriesXAxisToggle(series.seriesIndex, series.series);
    }
  }

  seriesBoldToggle(series: any) {
    if (!isNil(series?.seriesIndex)) {
      this.chartFacade.seriesBoldToggle(series.seriesIndex);
    }
  }

  saveSeries() {
    this.seriesFacade.vm$.pipe(take(1)).subscribe((vm) => {
      this.seriesFacade.reset();
      this.chartFacade.updateSeriesState(vm.seriesFilter);
    });
  }

  groupedSeriesAddMeasurement() {
    this.groupedSeriesFormService.addMeasurement();
  }

  removeSeriesFromChart(series: any) {
    if (!isNil(series?.seriesIndex)) {
      this.chartFacade.query.isGroupedSeries$
        .pipe(take(1))
        .subscribe((isGroupedSeries) => {
          if (isGroupedSeries) {
            this.groupedSeriesFormService.removeSeriesFromChart(
              series?.seriesIndex
            );
          } else {
            this.seriesFormService.removeSeriesFromChart(series?.seriesIndex);
          }
        });
    }
  }

  reorderSeries(series: any) {
    if (!isNil(series?.previousIndex) && !isNil(series?.currentIndex)) {
      this.chartFacade.query.isGroupedSeries$
        .pipe(take(1))
        .subscribe((isGroupedSeries) => {
          if (isGroupedSeries) {
            this.groupedSeriesFormService.reorderSeries(
              series.previousIndex,
              series.currentIndex
            );
          } else {
            this.seriesFormService.reorderSeries(
              series.previousIndex,
              series.currentIndex
            );
          }
        });
    }
  }

  onDragOverChart(event) {
    const types = event.dataTransfer.types;
    const dragSupported = types.length;
    if (dragSupported) {
      event.dataTransfer.dropEffect = 'move';
    }
    event.preventDefault();
  }

  onDropChart(event) {
    event.preventDefault();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;
    const transferedData = event.dataTransfer.getData(
      isIE ? 'text' : 'text/plain'
    );
    const assetVariableTypeTagMaps: IAssetVariableTypeTagMap[] =
      JSON.parse(transferedData);

    this.addSeriesToChart(assetVariableTypeTagMaps);
  }

  addSeriesToChart(assetVariableTypeTagMaps: IAssetVariableTypeTagMap[]) {
    this.chartFacade.query.isGroupedSeries$
      .pipe(take(1))
      .subscribe((isGroupedSeries) => {
        if (isGroupedSeries) {
          this.groupedSeriesFacade.isDirty$
            .pipe(take(1))
            .subscribe((isDirty) => {
              if (isDirty) {
                this.snackBarService.openSnackBar(
                  'Please clear changes on the Series Tab before adding a series',
                  'error'
                );
              } else {
                this.chartFacade.query.selectedTrendGroupSeriesCount$
                  .pipe(take(1))
                  .subscribe((count) => {
                    if (assetVariableTypeTagMaps?.length > 0) {
                      if (count + assetVariableTypeTagMaps.length <= 500) {
                        this.chartFacade.addBandAxisMeasurementToChart(
                          assetVariableTypeTagMaps
                        );
                      } else {
                        this.dataExplorerFacade.maximumNewSeriesReached();
                      }
                    }
                  });
              }
            });
        } else {
          this.chartFacade.query.selectedTrendNewSeriesCount$
            .pipe(take(1))
            .subscribe((count) => {
              if (assetVariableTypeTagMaps?.length > 0) {
                if (count + assetVariableTypeTagMaps.length <= 500) {
                  this.chartFacade.addSeriesToChart(assetVariableTypeTagMaps);
                } else {
                  this.dataExplorerFacade.maximumNewSeriesReached();
                }
              }
            });
        }
      });
  }

  pinTypeChange(axis: any) {
    if (!isNil(axis?.pinTypeID)) {
      this.chartFacade.pinTypeChange(+axis?.pinTypeID);
    }
  }

  addNewPin(command: any) {
    const newCriteriaPin = command?.newPin as ICriteria;
    this.chartFacade.addNewPin(
      newCriteriaPin,
      command?.startDate,
      command?.endDate,
      command?.units
    );
  }

  removePin(command: any) {
    const pinIndex = command?.pinIndex;
    if (pinIndex) {
      this.chartFacade.removePin(pinIndex);
    }
  }

  reorderPin(pinDragDropEvnt: any) {
    if (
      !isNil(pinDragDropEvnt?.previousIndex) &&
      !isNil(pinDragDropEvnt?.currentIndex)
    ) {
      this.pinsFormsService.reorderPin(
        pinDragDropEvnt.previousIndex,
        pinDragDropEvnt.currentIndex
      );
    }
  }

  assetTreeDropdownStateChange(command: any) {
    const change = command?.change;
    if (change) {
      this.dataExplorerFacade.assetTreeDropdownStateChange(change);
    }
  }

  saveModelAssetTreeDropdownStateChange(command: any) {
    const change = command?.change;
    if (change) {
      this.dataExplorerFacade.saveModelAssetTreeDropdownStateChange(change);
    }
  }

  addSeriesFromTagList(command: any) {
    const assetVariableTypeTagMaps = command?.assetVariableTypeTagMaps;
    if (assetVariableTypeTagMaps) {
      this.addSeriesToChart(assetVariableTypeTagMaps);
    }
  }

  advancedUpdateChart(advanced: any) {
    const advancedOptions = advanced?.advancedOptions;
    if (advancedOptions) {
      this.chartFacade.advancedUpdateChart(advancedOptions);
    }
  }

  activeToggle() {
    this.chartFacade.activeToggle();
  }

  pinHideToggle(pinToggle: any) {
    if (!isNil(pinToggle?.idx)) {
      this.chartFacade.pinHideToggle(+pinToggle.idx);
    }
  }

  pinsToggle() {
    this.chartFacade.pinHideAllToggle();
  }

  aggregationChange(event: MatSelectChange) {
    const summaryIndex = this.summaryTypes.findIndex(
      (summaryType) => event.value === summaryType.SummaryTypeID
    );
    if (summaryIndex > -1) {
      this.chartFacade.changeSummaryType(SummaryTypes[summaryIndex]);
    }
  }

  chartChange(event: MatSelectChange) {
    if (event?.value) {
      this.chartFacade.chartTypeChange(+event.value);
    }
  }

  copyChartLink(type: string) {
    this.logger.trackTaskCenterEvent('DataExplorerV2:CopyChartLink');
    let chartLink = window.location.href;
    if (type === 'live') {
      const id = getUniqueIdFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      const trend = getTrendIdFromRoute(
        this.activatedRoute.snapshot.queryParamMap
      );
      const dataExplorerUrl = `?id=${id}&trend=${trend?.toString()}`;
      chartLink = `${window.location.origin}${window.location.pathname}${dataExplorerUrl}`;
    }
    this.chartActionsFacade.copyLink(chartLink);
  }

  downloadChart() {
    this.logger.trackTaskCenterEvent('DataExplorerV2:DownloadChart');
    const chartConfig = this.chartDisplay.chartConfiguration;
    this.chartFacade.downloadChart(chartConfig);
  }

  setRadarChartOption(clockwise: boolean, angle: number) {
    this.chartFacade.setRadarChartOptions(clockwise, +angle);
  }
}
