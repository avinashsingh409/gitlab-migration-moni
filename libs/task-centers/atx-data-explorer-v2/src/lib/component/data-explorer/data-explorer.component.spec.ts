import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NavFacade } from '@atonix/atx-navigation';
import { AssetTreeModule, ITreeConfiguration } from '@atonix/atx-asset-tree';
import { TimeSliderModule, ITimeSliderState } from '@atonix/atx-time-slider';
import { ChartModule } from '@atonix/atx-chart';
import { DataExplorerComponent } from './data-explorer.component';
import { AssetTreeFacade } from '../../store/facade/asset-tree.facade';
import { TimeSliderFacade } from '../../store/facade/time-slider.facade';
import { DataExplorerFacade } from './../../store/facade/data-explorer.facade';
import { BehaviorSubject } from 'rxjs';
import { IToast } from '../../store/state/chart-state';
import { ToastService } from '@atonix/shared/utils';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DataExplorerEventBus } from '../../service/data-explorer-event-bus.service';
import { ChartActionsFacade } from '../../store/facade/chart.facade';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { APP_BASE_HREF } from '@angular/common';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('DataExplorerComponent', () => {
  let component: DataExplorerComponent;
  let fixture: ComponentFixture<DataExplorerComponent>;
  let assetTreeFacadeMock: AssetTreeFacade;
  let dataExplorerFacadeMock: DataExplorerFacade;
  let timeSliderFacadeMock: TimeSliderFacade;
  let chartFacadeMock: ChartActionsFacade;
  let navFacadeMock: NavFacade;
  let toastServiceMock: ToastService;
  let dataExplorerEventMock: DataExplorerEventBus;

  beforeEach(() => {
    navFacadeMock = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      navPaneState$: new BehaviorSubject<any>(null),
      showTimeSlider$: new BehaviorSubject<boolean>(false),
      assetTreeSelected$: new BehaviorSubject<boolean>(false),
      breadcrumb$: new BehaviorSubject<any>(null),
    });
    dataExplorerEventMock = createMockWithValues(DataExplorerEventBus, {
      on: jest.fn(),
    });
    assetTreeFacadeMock = createMockWithValues(AssetTreeFacade, {
      treeStateChange: jest.fn(),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: [],
        pin: true,
        loadingAssets: false,
        loadingTree: false,
        canDrag: false,
        dropTargets: null,
        canView: true,
        canAdd: false,
        canEdit: false,
        canDelete: false,
        collapseOthers: false,
        hideConfigureButton: false,
      }),
    });
    timeSliderFacadeMock = createMockWithValues(TimeSliderFacade, {
      timeSliderState$: new BehaviorSubject<ITimeSliderState>(null),
      timeSliderSelection$: new BehaviorSubject<{ start: Date; end: Date }>(
        null
      ),
    });
    dataExplorerFacadeMock = createMockWithValues(DataExplorerFacade, {
      initializeDataExplorer: jest.fn(),
      leftTraySize$: new BehaviorSubject<number>(12),
      selectedAssetIds$: new BehaviorSubject<{
        assetGuid: any;
        assetId: any;
      }>({ assetGuid: '12', assetId: 12 }),
      selectToast$: new BehaviorSubject<IToast>(null),
      layoutMode$: new BehaviorSubject<any>(null),
      chartHeight$: new BehaviorSubject<number>(23),
      selectAssetTreeDropdownConfiguration$:
        new BehaviorSubject<ITreeConfiguration>(null),
      selectAssetTreeDropdownAssetName$: new BehaviorSubject<string>('test'),
      selectedAssetUniqueKey$: new BehaviorSubject<string>('test'),
      selectAssetTreeDropdownSelectedAssetIds$: new BehaviorSubject<{
        assetUniqueKey: any;
        assetId: any;
      }>(null),
    });

    chartFacadeMock = createMockWithValues(ChartActionsFacade, {
      copyLink: jest.fn(),
    });
    toastServiceMock = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,

        AssetTreeModule,
        TimeSliderModule,
        ChartModule,

        NoopAnimationsModule,
        AtxMaterialModule,

        StoreModule.forRoot([]),
        EffectsModule.forRoot([]),
        RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: DataExplorerEventBus, useValue: dataExplorerEventMock },
        { provide: NavFacade, useValue: navFacadeMock },
        { provide: AssetTreeFacade, useValue: assetTreeFacadeMock },
        { provide: TimeSliderFacade, useValue: timeSliderFacadeMock },
        { provide: DataExplorerFacade, useValue: dataExplorerFacadeMock },
        { provide: ChartActionsFacade, useValue: chartFacadeMock },
        { provide: ToastService, useValue: toastServiceMock },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      declarations: [DataExplorerComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
