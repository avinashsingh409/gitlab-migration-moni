import { Component, Input } from '@angular/core';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../service/data-explorer-event-bus.service';

@Component({
  selector: 'atx-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
})
export class ColorPickerComponent {
  @Input() seriesColor: string;
  @Input() seriesIndex: number;
  @Input() isGroupedSeries: boolean;
  openPicker = false;

  constructor(private dataExplorerEventBus: DataExplorerEventBus) {}

  updateColor(event: { color: { hex: any } }) {
    if (event?.color?.hex) {
      this.dataExplorerEventBus.emit(
        new EmitEvent(BusEventTypes.SERIES_COLOR, {
          seriesIndex: this.seriesIndex,
          isGroupedSeries: this.isGroupedSeries,
          color: event?.color?.hex,
        })
      );
    }
  }

  showPicker() {
    this.openPicker = true;
  }

  closePicker() {
    this.openPicker = false;
  }
}
