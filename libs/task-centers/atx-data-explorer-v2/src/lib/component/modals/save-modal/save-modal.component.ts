import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormGroup,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ChartState } from '@atonix/atx-chart-v2';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import produce from 'immer';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  BusEventTypes,
  DataExplorerEventBus,
  EmitEvent,
} from '../../../service/data-explorer-event-bus.service';
import {
  SaveChartState,
  SaveModalFacade,
} from '../../../service/save-modal.facade';
import { DataExplorerFacade } from '../../../store/facade/data-explorer.facade';

export interface SaveDialogData {
  trendName: string;
  trendNames: string[];
}

@Component({
  selector: 'atx-save-modal',
  templateUrl: './save-modal.component.html',
  styleUrls: ['./save-modal.component.scss'],
})
export class SaveModalComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject<void>();
  modalSaveForm: UntypedFormGroup;
  trends: string[] = [];
  vm$: Observable<ChartState>;
  modalVm$: Observable<SaveChartState>;
  faSpinner = faSpinner;
  showAssetTreeDropdown = false;

  constructor(
    public dialogRef: MatDialogRef<SaveModalComponent>,
    public saveModalFacade: SaveModalFacade,
    private dataExplorerEventBus: DataExplorerEventBus,
    private dataExplorerFacade: DataExplorerFacade,
    private fb: UntypedFormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: SaveDialogData
  ) {
    this.trends = data.trendNames ?? [];
    this.modalSaveForm = this.fb.group({
      trendName: saveModalFacade.buildTrendNameFormControl(data.trendName),
    });
  }

  static uniqueName(trendNames: string[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value == null || control.value == '') {
        return null;
      }

      const errors: ValidationErrors = {};
      const currentName = control.value;
      if (trendNames.includes(currentName)) {
        errors.isUnique = {
          message: `There is already a chart named ${currentName}`,
        };
      }
      return Object.keys(errors).length ? errors : null;
    };
  }

  ngOnInit() {
    this.vm$ = this.saveModalFacade.chartState$;
    this.modalVm$ = this.saveModalFacade.vm$;
    this.saveModalFacade.init();
    this.saveModalFacade.chartSaveComplete$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((isComplete) => {
        if (isComplete) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.saveModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
            this.saveModalFacade.refreshCharts(
              vm.newTrendName,
              vm.newChartID,
              vm.oldChartID
            );
            this.saveModalFacade.reset();
            this.dialogRef.close(true);
          });
        }
      });

    this.saveModalFacade.treeStateChange$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((treeState) => {
        if (treeState) {
          this.saveModalFacade.loadNewTrends();
        }
      });
  }

  saveChart() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      let newTrendName = '';
      const newTrend = produce(vm.selectedTrend.trendDefinition, (state) => {
        state.Title = this.modalSaveForm.get('trendName').value;
        newTrendName = this.modalSaveForm.get('trendName').value;
      });
      this.dataExplorerFacade.saveModelAssetTreeDropdownSelectedAssetId$
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe((id) => {
          this.saveModalFacade.saveChart(
            newTrendName,
            id,
            vm.end,
            vm.start,
            newTrend,
            vm.selectedTrendID
          );
        });
    });
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.dataExplorerEventBus.emit(
      new EmitEvent(
        BusEventTypes.ON_SAVE_MODEL_ASSET_TREE_DROPDOWN_STATE_CHANGE,
        {
          change,
        }
      )
    );

    this.saveModalFacade.treeStateChange(change);
  }

  editAsset(): void {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  closeAssetTreeDropdown() {
    this.showAssetTreeDropdown = false;
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  onNoClick(): void {
    this.saveModalFacade.reset();
    this.dialogRef.close();
  }
}
