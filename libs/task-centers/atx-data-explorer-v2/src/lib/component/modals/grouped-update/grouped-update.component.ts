import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import produce from 'immer';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

export interface SaveDialogData {
  trendName: string;
  trendNames: string[];
}

@Component({
  selector: 'atx-grouped-update',
  templateUrl: './grouped-update.component.html',
  styleUrls: ['./grouped-update.component.scss'],
})
export class GroupedUpdateComponent {
  faSpinner = faSpinner;
  constructor(public dialogRef: MatDialogRef<GroupedUpdateComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
