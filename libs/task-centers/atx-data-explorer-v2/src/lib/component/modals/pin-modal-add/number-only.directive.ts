import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: 'input[atxNumberOnly]',
})
export class NumberOnlyDirective {
  constructor(private _el: ElementRef) {}

  @HostListener('input', ['$event']) public onInputChange(event) {
    console.log(event);
    const initalValue = this._el.nativeElement.value;
    this._el.nativeElement.value = initalValue.replace(/[^0-9]*/g, '');
    if (initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}
