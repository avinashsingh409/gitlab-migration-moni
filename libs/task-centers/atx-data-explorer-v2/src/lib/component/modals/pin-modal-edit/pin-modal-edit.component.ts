import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  checkStartAndEndTimes,
  combineTime,
  getADayAgo,
  getToday,
} from '@atonix/shared/utils';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  PinModalFacade,
  PinModalState,
} from '../../../service/pin-modal.facade';
import * as moment from 'moment';
import { PinModalFormService } from '../../../service/pin-modal-form.service';
import { MatSelectChange } from '@angular/material/select';
import { ICriteria, IPDTrendPin } from '@atonix/atx-core';

export interface EditDialogData {
  timeFilters: ICriteria[];
  pin: IPDTrendPin;
  startDt: Date;
  endDt: Date;
}

@Component({
  selector: 'atx-pin-modal-edit',
  templateUrl: './pin-modal-edit.component.html',
  styleUrls: ['./pin-modal-edit.component.scss'],
  providers: [PinModalFacade, PinModalFormService],
})
export class PinModalEditComponent implements OnDestroy, OnInit {
  vm$: Observable<PinModalState>;
  public unsubscribe$ = new Subject<void>();
  public mainForm: UntypedFormGroup;
  private pin: IPDTrendPin = null;
  constructor(
    public dialogRef: MatDialogRef<PinModalEditComponent>,
    public pinModalFacade: PinModalFacade,
    public pinModalFormService: PinModalFormService,
    private fb: UntypedFormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: EditDialogData
  ) {
    pinModalFacade.init(data.timeFilters, data.startDt, data.endDt);
    if (data.pin.CriteriaObjectId) {
      this.pinModalFacade.selectTimeFilter(data.pin.CriteriaObjectId);
    } else {
      // they have a fixed pin
      this.pin = data.pin;
    }
  }

  changed(event: number) {
    this.pinModalFacade.selectTimeFilter(event);
  }

  changeTimeRangeType(event: MatSelectChange) {
    if (event?.value) {
      this.pinModalFacade.updateFilterType(event.value);
      if (this.pin) {
        const startDate = moment(this.pin.StartTime).toDate();
        const startTime = moment(startDate).format('HH:mm');
        const endDate = moment(this.pin.EndTime).toDate();
        const endTime = moment(endDate).format('HH:mm');
        this.pinModalFacade.initStartEndTimes(
          startDate,
          startTime,
          endDate,
          endTime
        );

        const isValid = checkStartAndEndTimes(
          startDate,
          startTime,
          endDate,
          endTime
        );
        this.pinModalFacade.setStartDateAfterEndDate(isValid);
      }
    }
  }

  changeSpanUnitOfTime(event: MatSelectChange) {
    if (event?.value) {
      this.pinModalFacade.updateSpanID(event.value);
    }
  }

  addPin() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const start: Date = combineTime(vm.startDate, vm.startTime);
      let end: Date = combineTime(vm.endDate, vm.endTime);
      if (vm.filterTypeID === 2) {
        end = this.pinModalFacade.updateEndDate(start, vm.spanID, vm.span);
      }
      this.dialogRef.close({
        selectedFilter: vm.selectedFilter,
        startDate: start,
        endDate: end,
        units: vm.spanID,
      });
    });
  }

  endDateChanged(event: Date) {
    this.pinModalFacade.endDateChanged(event);
  }
  startDateChanged(event: Date) {
    this.pinModalFacade.startDateChanged(event);
  }

  initForms(): void {
    this.mainForm = this.fb.group({
      span: this.pinModalFormService.buildSpan(),
      startTime: this.pinModalFormService.buildStartTime(),
      startDate: new UntypedFormControl('', []),
      endDate: new UntypedFormControl('', []),
      endTime: this.pinModalFormService.buildEndTime(),
    });
  }

  ngOnInit() {
    this.pinModalFacade.filterTypeID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((filterTypeID) => {
        if (this.mainForm && this.pinModalFacade.vm$) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.vm$.pipe(take(1)).subscribe((vm) => {
            this.mainForm
              .get('startTime')
              .patchValue(vm.startTime, { emitEvent: false });
            this.mainForm
              .get('endTime')
              .patchValue(vm.endTime, { emitEvent: false });
            this.mainForm
              .get('endDate')
              .patchValue(vm.endDate, { emitEvent: false });
            this.mainForm
              .get('startDate')
              .patchValue(vm.startDate, { emitEvent: false });
            this.mainForm.get('span').patchValue(vm.span, { emitEvent: false });
          });
        }
      });

    this.pinModalFacade.startDateAfterEndDate$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((notValid) => {
        if (this.mainForm && this.pinModalFacade.vm$) {
          if (notValid) {
            this.mainForm.setErrors({ invalidDateRange: true });
          } else {
            this.mainForm.setErrors(null);
          }
        }
      });

    this.vm$ = this.pinModalFacade.vm$;
    if (this.pin) {
      const startDate = moment(this.pin.StartTime).toDate();
      const startTime = moment(startDate).format('HH:mm');
      const endDate = moment(this.pin.EndTime).toDate();
      const endTime = moment(endDate).format('HH:mm');
      this.pinModalFacade.initStartEndTimes(
        startDate,
        startTime,
        endDate,
        endTime
      );
      this.pinModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
        this.initForms();
      });
      this.mainForm
        .get('startTime')
        .patchValue(startTime, { emitEvent: false });
      this.mainForm.get('endTime').patchValue(endTime, { emitEvent: false });
      this.mainForm.get('endDate').patchValue(endDate, { emitEvent: false });
      this.mainForm
        .get('startDate')
        .patchValue(startDate, { emitEvent: false });
      this.mainForm
        .get('span')
        .patchValue(this.pin.span ?? 1, { emitEvent: false });
    } else {
      const startDate = getADayAgo();
      const startTime = moment(startDate).format('HH:mm');
      const endDate = getToday();
      const endTime = moment(endDate).format('HH:mm');
      this.pinModalFacade.initStartEndTimes(
        startDate,
        startTime,
        endDate,
        endTime
      );
      this.pinModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
        this.initForms();
      });
      this.mainForm
        .get('startTime')
        .patchValue(startTime, { emitEvent: false });
      this.mainForm.get('endTime').patchValue(endTime, { emitEvent: false });
      this.mainForm.get('endDate').patchValue(endDate, { emitEvent: false });
      this.mainForm
        .get('startDate')
        .patchValue(startDate, { emitEvent: false });
      this.mainForm.get('span').patchValue(1, { emitEvent: false });
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
