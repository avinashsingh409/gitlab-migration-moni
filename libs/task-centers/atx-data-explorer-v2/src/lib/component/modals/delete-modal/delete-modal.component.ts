import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChartFacade, ChartState } from '@atonix/atx-chart-v2';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  DeleteChartState,
  DeleteModalFacade,
} from '../../../service/delete-modal.facade';

export interface DeleteDialogData {
  trendName: string;
  trendID: string;
}

@Component({
  selector: 'atx-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject<void>();
  vm$: Observable<ChartState>;
  modalVm$: Observable<DeleteChartState>;
  faSpinner = faSpinner;
  trendName: string;
  constructor(
    public dialogRef: MatDialogRef<DeleteModalComponent>,
    public chartFacade: ChartFacade,
    public deleteModalFacade: DeleteModalFacade,
    private fb: UntypedFormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: DeleteDialogData
  ) {
    this.trendName = data.trendName;
  }

  ngOnInit() {
    this.vm$ = this.chartFacade.query.vm$;
    this.modalVm$ = this.deleteModalFacade.vm$;
    this.deleteModalFacade.deleteComplete$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((deleteComplete) => {
        if (deleteComplete) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.deleteModalFacade.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.errorMessage !== '') {
              this.dialogRef.close({ response: 'error' });
              this.deleteModalFacade.reset();
            } else {
              this.dialogRef.close({ response: 'deleted' });
              this.deleteModalFacade.reset();
            }
          });
        }
      });
  }

  deleteChart() {
    this.deleteModalFacade.deleteChart(this.data.trendID);
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  onNoClick(): void {
    this.deleteModalFacade.reset();
    this.dialogRef.close();
  }
}
