import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, UntypedFormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import {
  PinModalFacade,
  PinModalState,
} from '../../../service/pin-modal.facade';
import { PinModalFormService } from '../../../service/pin-modal-form.service';
import { DialogData } from '../../axis/axis.component';

@Component({
  selector: 'atx-axis-modal',
  templateUrl: './axis-modal.component.html',
  styleUrls: ['./axis-modal.component.scss'],
  providers: [PinModalFacade, PinModalFormService],
})
export class AxisModalComponent {
  vm$: Observable<PinModalState>;
  public unsubscribe$ = new Subject<void>();
  public mainForm: UntypedFormGroup;

  constructor(
    public dialogRef: MatDialogRef<AxisModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}
}
