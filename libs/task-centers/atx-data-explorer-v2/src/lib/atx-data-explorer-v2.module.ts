import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NavigationModule } from '@atonix/atx-navigation';
import { dataExplorerReducer } from './store/reducers/data-explorer.reducer';
import { AssetTreeFacade } from './store/facade/asset-tree.facade';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { DataExplorerFacade } from './store/facade/data-explorer.facade';
import { TimeSliderFacade } from './store/facade/time-slider.facade';
import { ChartEffects } from './store/effects/chart.effects';
import { ChartActionsFacade } from './store/facade/chart.facade';
import { AssetTreeEffects } from './store/effects/asset-tree.effects';
import { DataExplorerComponent } from './component/data-explorer/data-explorer.component';
import { DataExplorerEffects } from './store/effects/data-explorer.effects';
import { AtxMaterialModule } from '@atonix/atx-material';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { ChartListComponent } from './component/chart-list/chart-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditViewComponent } from './component/edit-view/edit-view.component';
import { TagsListComponent } from './component/tags-list/tags-list.component';
import { SettingsTabsComponent } from './component/settings-tabs/settings-tabs.component';
import { AxisComponent } from './component/axis/axis.component';
import { SeriesComponent } from './component/series/series.component';
import { StaticCurvesComponent } from './component/static-curves/static-curves.component';
import { PinsComponent } from './component/pins/pins.component';
import { AgGridModule } from '@ag-grid-community/angular';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { SharedApiModule } from '@atonix/shared/api';
import { DataExplorerEventBus } from './service/data-explorer-event-bus.service';
import { DragCellRendererComponent } from './component/tags-list/drag-cell-renderer/drag-cell-renderer.component';
import { PinModalComponent } from './component/modals/pin-modal-add/pin-modal.component';
import { PinModalFacade } from './service/pin-modal.facade';
import { AxisModalComponent } from './component/modals/axis-modal/axis-modal.component';
import { SaveChangesModalComponent } from './component/modals/save-changes-modal/save-changes-modal.component';
import { SaveModalComponent } from './component/modals/save-modal/save-modal.component';
import { PinModalEditComponent } from './component/modals/pin-modal-edit/pin-modal-edit.component';
import { DeleteModalComponent } from './component/modals/delete-modal/delete-modal.component';
import { AngularSplitModule } from 'angular-split';
import { SaveModalFacade } from './service/save-modal.facade';
import { DeleteModalFacade } from './service/delete-modal.facade';
import { PendingChangesGuard } from './guard/pending-changes-guard';
import { SharedUiModule } from '@atonix/shared/ui';
import { AdvancedComponent } from './component/advanced/advanced.component';
import { GroupedSeriesComponent } from './component/grouped-series/grouped-series.component';
import { AdvancedFacade } from './service/advanced.facade';
import { GroupedSeriesFacade } from './service/grouped-series.facade';
import { GroupedUpdateComponent } from './component/modals/grouped-update/grouped-update.component';
import { SeriesFacade } from './service/series.facade';
import { NumberOnlyDirective } from './component/modals/pin-modal-add/number-only.directive';
import { ColorPickerComponent } from './component/color-picker/color-picker.component';
import { ColorChromeModule } from 'ngx-color/chrome';

@NgModule({
  declarations: [
    AxisComponent,
    SaveChangesModalComponent,
    SaveModalComponent,
    GroupedUpdateComponent,
    SeriesComponent,
    GroupedSeriesComponent,
    DataExplorerComponent,
    ChartListComponent,
    EditViewComponent,
    TagsListComponent,
    SettingsTabsComponent,
    PinsComponent,
    StaticCurvesComponent,
    DragCellRendererComponent,
    AxisModalComponent,
    PinModalComponent,
    DeleteModalComponent,
    PinModalEditComponent,
    AdvancedComponent,
    NumberOnlyDirective,
    ColorPickerComponent,
  ],
  imports: [
    AngularSplitModule,
    AgGridModule.withComponents([]),
    CommonModule,
    SharedApiModule,
    SharedUiModule,
    AtxChartModuleV2,
    AssetTreeModule,
    NgxSkeletonLoaderModule,
    TimeSliderModule,
    NavigationModule,
    AtxMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUtilsModule,
    ColorChromeModule,
    RouterModule.forChild([
      {
        path: '',
        component: DataExplorerComponent,
        pathMatch: 'full',
        canDeactivate: [PendingChangesGuard],
      },
    ]),
    StoreModule.forFeature('dataexplorer', dataExplorerReducer),
    EffectsModule.forFeature([
      DataExplorerEffects,
      AssetTreeEffects,
      ChartEffects,
    ]),
  ],
  providers: [
    AssetTreeFacade,
    DataExplorerFacade,
    TimeSliderFacade,
    ChartActionsFacade,
    DeleteModalFacade,
    SaveModalFacade,
    DataExplorerEventBus,
    PinModalFacade,
    PendingChangesGuard,
    AdvancedFacade,
    GroupedSeriesFacade,
    SeriesFacade,
  ],
  exports: [DataExplorerComponent],
})
export class DataExplorerModuleV2 {}
