import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssetExplorerComponent } from './component/asset-explorer/asset-explorer.component';
import { InfoTabComponent } from './component/info-tab/info-tab.component';
import { TagsTabComponent } from './component/tags-tab/tags-tab.component';
import { EventsTabComponent } from './component/events-tab/events-tab.component';
import { LocationTabComponent } from './component/location-tab/location-tab.component';
import { BlogTabComponent } from './component/blog-tab/blog-tab.component';
import { AttachmentsTabComponent } from './component/attachments-tab/attachments-tab.component';
import { PDServerResolverService } from './service/pdserver-resolver.service';
import { EventsResolverService } from './service/events-resolver.service';
import { BlogResolverService } from './service/blog-resolver.service';
import { AssetResolverService } from './service/asset-resolver.service';
const routes: Routes = [
  {
    path: '',
    component: AssetExplorerComponent,
    pathMatch: 'full',
  },
  {
    path: ':id',
    component: AssetExplorerComponent,
    children: [
      { path: '', redirectTo: 'info', pathMatch: 'full' },
      {
        path: 'info',
        component: InfoTabComponent,
        resolve: { asset: AssetResolverService },
      },
      { path: 'attachments', component: AttachmentsTabComponent },
      {
        path: 'blog',
        component: BlogTabComponent,
      },
      { path: 'location', component: LocationTabComponent },
      {
        path: 'events',
        component: EventsTabComponent,
        resolve: { events: EventsResolverService },
      },
      {
        path: 'tags',
        component: TagsTabComponent,
        resolve: { servers: PDServerResolverService },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssetExplorerRoutingModule {}
export const AppRoutingComponents = [InfoTabComponent];
