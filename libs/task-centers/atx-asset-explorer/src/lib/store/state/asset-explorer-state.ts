import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { initialInfoTabState, IInfoTabState } from './info-tab-state';
import { ITab } from '../../model/tab';
import { ITagsTabState, initialTagsTabState } from './tags-tab-state';
import { IEventsTabState, initialEventsTabState } from './events-tab-state';
import { IBlogTabState, initialBlogTabState } from './blog-tab-state';

const tabs: ITab[] = [
  {
    path: 'info',
    label: 'Info',
  },
  {
    path: 'attachments',
    label: 'Attachments',
  },
  {
    path: 'blog',
    label: 'Blog',
  },
  {
    path: 'location',
    label: 'Location',
  },
  {
    path: 'events',
    label: 'Events',
  },
  {
    path: 'tags',
    label: 'Tags',
  },
];

export interface IAssetExplorerState {
  AssetTreeState: ITreeState;
  Tabs: ITab[];
  InfoTabState: IInfoTabState;
  BlogTabState: IBlogTabState;
  TagsTabState: ITagsTabState;
  EventsTabState: IEventsTabState;
  LeftTraySize: number;
  Loading?: boolean;
}

export const initialAssetExplorerState: IAssetExplorerState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({
      pin: true,
      canEdit: false,
      canAdd: false,
      canDelete: false,
      collapseOthers: false,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
  Tabs: tabs,
  InfoTabState: initialInfoTabState,
  BlogTabState: initialBlogTabState,
  TagsTabState: initialTagsTabState,
  EventsTabState: initialEventsTabState,
  LeftTraySize: 250,
  Loading: false,
};
