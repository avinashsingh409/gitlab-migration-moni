import {
  IDiscussion,
  IDiscussionAssetMapType,
  IDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';

export interface IBlogTabState {
  discussionAssetTypes: IDiscussionAssetMapType[];
  issuesCount: number;
  discussion: IDiscussion;
  discussionEntry: IDiscussionEntry;
  uploadingAttachments: boolean;
  autoCompleteLoading: boolean;
  autoCompleteKeywords: ITaggingKeyword[];
}

export const initialBlogTabState: IBlogTabState = {
  discussionAssetTypes: null,
  issuesCount: 0,
  discussion: null,
  discussionEntry: null,
  uploadingAttachments: null,
  autoCompleteLoading: null,
  autoCompleteKeywords: null,
};
