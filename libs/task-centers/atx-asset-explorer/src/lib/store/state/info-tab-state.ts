import { EntityAdapter, createEntityAdapter, EntityState } from '@ngrx/entity';
import { IAssetInfoState } from '../../model/asset-info';

export const infoTabAdapter: EntityAdapter<IAssetInfoState> =
  createEntityAdapter<IAssetInfoState>();
export interface IInfoTabState extends EntityState<IAssetInfoState> {
  selectedAssetInfo: string;
}

export const initialInfoTabState: IInfoTabState =
  infoTabAdapter.getInitialState({
    selectedAssetInfo: null,
  });
