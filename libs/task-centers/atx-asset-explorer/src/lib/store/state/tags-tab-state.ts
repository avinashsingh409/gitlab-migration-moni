import { ISecurityRights, IPDTag, IPDServer } from '@atonix/atx-core';

export type TagsDataAvailable = 'loading' | 'nodata' | '';

export interface ITagsTabState {
  rights: ISecurityRights;
  servers: IPDServer[];
  selectedServer: number;
  tags: IPDTag[];
  displayedTags: IPDTag[];
  tagsDataAvailable: TagsDataAvailable;
  showDeletedTags: boolean;
  validationMessage: string;
}

export const initialTagsTabState: ITagsTabState = {
  rights: null,
  servers: null,
  selectedServer: null,
  tags: null,
  displayedTags: null,
  tagsDataAvailable: '',
  showDeletedTags: false,
  validationMessage: null,
};
