import {
  IEvent,
  IEventChange,
  IEventType,
  ISecurityRights,
} from '@atonix/atx-core';
export interface IEventsTabState {
  assetGlobalId: string;
  rights: ISecurityRights;
  events: IEvent[];
  eventTypes: IEventType[];
  eventHistory: IEvent[];
  message: string;
  validationMessage: string;
  newEventValid: boolean;
  saveResult: string;
  includeDescendants: boolean;
  descendentsLoading: boolean;
  changedEvents: IEventChange[];
  changedEventCount: number;
}

export const initialEventsTabState: IEventsTabState = {
  assetGlobalId: null,
  rights: null,
  events: null,
  eventTypes: null,
  eventHistory: null,
  message: null,
  validationMessage: null,
  newEventValid: false,
  saveResult: null,
  includeDescendants: false,
  descendentsLoading: false,
  changedEvents: null,
  changedEventCount: 0,
};
