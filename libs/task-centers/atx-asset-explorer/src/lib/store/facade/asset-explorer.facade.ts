/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ITreeStateChange, selectAsset } from '@atonix/atx-asset-tree';
import {
  selectNavigationState,
  selectSelectedAsset,
  selectAssetTreeConfiguration,
  selectTabs,
  selectSelectedNodeAbbrev,
  selectIsAssetSelected,
  selectTheme,
  selectLeftTrayMode,
  selectLeftTraySize,
  selectIsAdHocTree,
} from '../selectors/asset-explorer.selector';
import * as actions from '../actions/asset-explorer.actions';
import { PowerGenFeature } from '@atonix/shared/state/power-gen';
import { selectAssetTreeId } from '../reducers/asset-explorer.reducer';

@Injectable({
  providedIn: 'root',
})
export class AssetExplorerFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  navigationState$ = this.store.pipe(select(selectNavigationState));
  theme$ = this.store.pipe(select(selectTheme));

  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  selectedAsset$ = this.store.pipe(select(selectSelectedAsset));
  assetTreeId$ = this.store.pipe(select(selectAssetTreeId));

  isAssetSelected$ = this.store.pipe(select(selectIsAssetSelected));

  // Configuration of the asset tree
  assetTreeConfiguration$ = this.store.pipe(
    select(selectAssetTreeConfiguration)
  );
  isAdHocTree$ = this.store.pipe(select(selectIsAdHocTree));
  selectedNodeAbbrev$ = this.store.pipe(select(selectSelectedNodeAbbrev));
  tabs$ = this.store.pipe(select(selectTabs));

  leftTrayMode$ = this.store.pipe(select(selectLeftTrayMode));
  leftTraySize$ = this.store.pipe(select(selectLeftTraySize));

  assetDesc$ = this.store.pipe(
    select(PowerGenFeature.getSelectedDashboardDataTitle)
  );

  // This sets up the summary tab
  initializeAssetExplorer(asset?: string) {
    this.store.dispatch(actions.assetExplorerInitialize({ asset }));
  }

  treeSizeChange(value: number) {
    this.store.dispatch(actions.treeSizeChange({ value }));
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.treeStateChange(change));
  }

  selectAsset(ids: string[] | string, multiSelect: boolean) {
    this.store.dispatch(actions.treeStateChange(selectAsset(ids, multiSelect)));
  }
  routeLoadAttributes(asset: string) {
    this.store.dispatch(actions.routeLoadAttributes({ asset }));
  }
  routeLoadLocation(asset: string) {
    this.store.dispatch(actions.routeLoadLocation({ asset }));
  }
  infoToast(message: string) {
    this.store.dispatch(actions.showInfoMessage({ message }));
  }
  successToast(message: string) {
    this.store.dispatch(actions.showSuccessMessage({ message }));
  }
  warningToast(message: string) {
    this.store.dispatch(actions.showWarningMessage({ message }));
  }
  errorToast(message: string) {
    this.store.dispatch(actions.showErrorMessage({ message }));
  }
}
