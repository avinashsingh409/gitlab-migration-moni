import { TestBed } from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { TagsTabFacade } from './tags-tab.facade';

describe('TagsTabFacade', () => {
  let service: TagsTabFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockStore({})],
    });
    service = TestBed.inject(TagsTabFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
