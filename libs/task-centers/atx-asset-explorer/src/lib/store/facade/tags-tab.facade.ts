/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */

import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  selectTagsPermissions,
  selectTagServers,
  selectTags,
  selectSelectedServer,
  selectTagsState,
  selectShowDeletedTags,
  selectValidationStatus,
  selectCanView,
  selectCanEdit,
  isServerSelected,
} from '../selectors/tags-tab.selector';
import * as actions from '../actions/tags-tab.actions';
import { IPDTag } from '@atonix/atx-core';
import { selectTheme } from '../selectors/asset-explorer.selector';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TagsTabFacade {
  // This uses an explicit type because something about the compilation fails
  // when the type is inferred.
  tagsState$: Observable<any> = this.store.pipe(select(selectTagsState));

  tagsPermissions$ = this.store.pipe(select(selectTagsPermissions));
  canView$ = this.store.pipe(select(selectCanView));
  canEdit$ = this.store.pipe(select(selectCanEdit));
  tagServers$ = this.store.pipe(select(selectTagServers));
  tags$ = this.store.pipe(select(selectTags));
  selectedServer$ = this.store.pipe(select(selectSelectedServer));
  isServerSelected$ = this.store.pipe(select(isServerSelected));
  theme$ = this.store.pipe(select(selectTheme));
  showDeletedTags$ = this.store.pipe(select(selectShowDeletedTags));
  validationStatus$ = this.store.pipe(select(selectValidationStatus));

  constructor(private store: Store<any>) {}

  routeLoadTags(asset: string) {
    this.store.dispatch(actions.routeLoadTags({ asset }));
  }

  selectServer(server: number) {
    this.store.dispatch(actions.getTags({ server }));
  }

  addTag() {
    this.store.dispatch(actions.addTag());
  }

  changeTag(tag: IPDTag) {
    this.store.dispatch(actions.changeTag({ tag }));
  }

  validateTags() {
    this.store.dispatch(actions.validateTags());
  }

  saveTags() {
    this.store.dispatch(actions.saveTags());
  }

  discardTags() {
    this.store.dispatch(actions.discardChanges());
  }

  showDeletedTagsChanged(newValue: boolean) {
    this.store.dispatch(actions.showDeletedTagsChange({ newValue }));
  }

  deleteTag(tagID: number) {
    this.store.dispatch(actions.deleteTag({ tagID }));
  }

  pasteTags(tags: IPDTag[]) {
    this.store.dispatch(actions.pasteTags({ tags }));
  }
}
