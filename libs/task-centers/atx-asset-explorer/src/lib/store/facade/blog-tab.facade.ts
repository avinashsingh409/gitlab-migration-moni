/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as actions from '../actions/blog-tab.actions';
import {
  selectDiscussionAssetTypes,
  selectIssuesCount,
  selectAutoCompleteLoading,
  selectAutoCompleteKeywords,
  selectDiscussionEntry,
} from '../selectors/blog-tab.selector';

@Injectable({
  providedIn: 'root',
})
export class BlogTabFacade {
  constructor(private store: Store<any>) {}

  selectDiscussionAssetTypes$ = this.store.pipe(
    select(selectDiscussionAssetTypes)
  );
  selectIssuesCount$ = this.store.pipe(select(selectIssuesCount));
  selectDiscussionEntry$ = this.store.pipe(select(selectDiscussionEntry));
  autoCompleteLoading$ = this.store.pipe(select(selectAutoCompleteLoading));
  autoCompleteKeywords$ = this.store.pipe(select(selectAutoCompleteKeywords));

  routeLoadBlog(asset: string) {
    this.store.dispatch(actions.routeLoadBlog({ asset }));
  }

  sendNotifications() {
    this.store.dispatch(actions.sendNotifications());
  }

  subscribe(discussionAssetType: string) {
    this.store.dispatch(actions.subscribe({ discussionAssetType }));
  }

  viewOpenIssues() {
    this.store.dispatch(actions.viewOpenIssues());
  }

  updateTitle(title: string) {
    this.store.dispatch(actions.updateDiscussionEntryTitle({ title }));
  }

  updateContent(content: string) {
    this.store.dispatch(actions.updateDiscussionEntryContent({ content }));
  }

  saveDiscussionEntry(
    showAutogenEntries: boolean,
    showedTopic: number,
    entryTopic: number
  ) {
    this.store.dispatch(
      actions.saveDiscussionEntry({
        showAutogenEntries,
        showedTopic,
        entryTopic,
      })
    );
  }

  cancelDiscussionEntry() {
    this.store.dispatch(actions.getNewDiscussionEntry());
  }

  addAttachment(
    discussionID: string,
    entryID: string,
    title: string,
    discussionType: string,
    file: any
  ) {
    this.store.dispatch(
      actions.addAttachment({
        discussionID,
        entryID,
        title,
        discussionType,
        file,
      })
    );
  }

  searchTaggingKeywords(searchExpression: string) {
    this.store.dispatch(actions.searchTaggingKeywords({ searchExpression }));
  }

  addKeyword(tag: string) {
    this.store.dispatch(actions.addKeyword({ tag }));
  }
}
