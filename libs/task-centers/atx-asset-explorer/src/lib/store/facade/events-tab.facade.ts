/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as eventSelectors from '../selectors/events-tab.selector';
import { selectEventsPermissions } from '../selectors/asset-explorer.selector';
import * as actions from '../actions/events-tab.actions';
import { IUserEventWithDescription } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class EventsTabFacade {
  constructor(private store: Store<any>) {}
  eventsPermissions$ = this.store.pipe(select(selectEventsPermissions));
  events$ = this.store.pipe(select(eventSelectors.selectEvents));
  eventTypes$ = this.store.pipe(select(eventSelectors.selectEventTypes));
  eventHistory$ = this.store.pipe(select(eventSelectors.selectEventHistory));
  validationMessage$ = this.store.pipe(
    select(eventSelectors.selectValidationResult)
  );
  changedEvents$ = this.store.pipe(select(eventSelectors.selectChangedEvents));

  changedEventCount$ = this.store.pipe(
    select(eventSelectors.selectChangedEventCount)
  );
  message$ = this.store.pipe(select(eventSelectors.selectMessage));
  newEventInvalid$ = this.store.pipe(
    select(eventSelectors.selectNewEventInvalid)
  );
  saveResult$ = this.store.pipe(select(eventSelectors.selectSaveResult));
  eventsViewable$ = this.store.pipe(
    select(eventSelectors.selectEventsViewable)
  );
  includeDescendants$ = this.store.pipe(
    select(eventSelectors.selectIncludeDescendents)
  );
  descendentsLoading$ = this.store.pipe(
    select(eventSelectors.selectDescendentsLoading)
  );

  routeLoadEvents(asset: string) {
    this.store.dispatch(actions.routeLoadEvents({ asset }));
  }

  clearEvents() {
    this.store.dispatch(actions.clearEvents());
  }

  getEventTypes() {
    this.store.dispatch(actions.getEventTypes());
  }

  getEventHistory(eventID: string) {
    this.store.dispatch(actions.getEventHistory({ eventID }));
  }

  changeEvent(event: IUserEventWithDescription) {
    this.store.dispatch(actions.changeEvent({ event }));
  }

  validateEvent(event: IUserEventWithDescription) {
    this.store.dispatch(actions.validateEvent({ event }));
  }

  saveEvent(event: IUserEventWithDescription) {
    this.store.dispatch(actions.saveEvent({ event }));
  }

  deleteEvent(eventID: number) {
    this.store.dispatch(actions.deleteEvent({ eventID }));
  }

  reloadEvents() {
    this.store.dispatch(actions.reloadEvents());
  }

  // This is called when the input form changed so that we can require the user validate the
  // event before it is saved.
  eventChanged() {
    this.store.dispatch(actions.eventChanged());
  }

  eventDialogOpened() {
    this.store.dispatch(actions.eventsDialogOpened());
  }

  validationDialogReset() {
    this.store.dispatch(actions.validationDialogReset());
  }

  toggleIncludeDescendents(value: boolean) {
    this.store.dispatch(actions.includeDescendantsEvent({ value }));
  }
}
