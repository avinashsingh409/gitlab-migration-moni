/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as actions from '../actions/info-tab.actions';
import {
  selectAssetInfo,
  selectAttributes,
  selectAutoCompleteKeywords,
  selectAssetClassTypes,
  selectAutoCompleteLoading,
  selectAssetClassTypesLoading,
  selectCanChangeType,
} from '../selectors/info-tab.selector';
import { IAttribute } from '../../model/asset-info';

@Injectable({
  providedIn: 'root',
})
export class InfoTabFacade {
  constructor(private store: Store<any>) {}

  assetInfo$ = this.store.pipe(select(selectAssetInfo));
  assetClassTypesLoading$ = this.store.pipe(
    select(selectAssetClassTypesLoading)
  );
  assetClassTypes$ = this.store.pipe(select(selectAssetClassTypes));
  attributes$ = this.store.pipe(select(selectAttributes));
  autoCompleteLoading$ = this.store.pipe(select(selectAutoCompleteLoading));
  autoCompleteKeywords$ = this.store.pipe(select(selectAutoCompleteKeywords));
  canChangeType$ = this.store.pipe(select(selectCanChangeType));

  routeLoadInfo(asset: string) {
    this.store.dispatch(actions.routeLoadInfo({ asset }));
  }

  getAssetClassTypes(asset: string) {
    this.store.dispatch(actions.getAssetClassTypes({ asset }));
  }

  changeAttribute(attribute: IAttribute) {
    this.store.dispatch(actions.changeAttribute({ attribute }));
  }

  searchTaggingKeywords(asset: string, searchExpression: string) {
    this.store.dispatch(
      actions.searchTaggingKeywords({ asset, searchExpression })
    );
  }

  addKeyword(asset: string, tag: string) {
    this.store.dispatch(actions.addKeyword({ asset, tag }));
  }

  editAttribute(attributeID: number) {
    this.store.dispatch(actions.editAttribute({ id: attributeID }));
  }
  favoriteAttribute(attributeID: number, value: boolean) {
    this.store.dispatch(
      actions.favoriteAttribute({ id: attributeID, favorite: value })
    );
  }
}
