import * as selectors from './asset-explorer.selector';
import { INavigationState } from '@atonix/atx-navigation';
import {
  ITreeConfiguration,
  ITreeNodes,
  ITreeState,
} from '@atonix/atx-asset-tree';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import { ITagsTabState } from '../state/tags-tab-state';
import { IEventsTabState } from '../state/events-tab-state';

describe('AssetExplorerSelector', () => {
  const treeConfig: ITreeConfiguration = {
    trees: [],
    nodes: [],
    showTreeSelector: true,
    selectedTree: '123456789',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    pin: true,
    loadingAssets: false,
    loadingTree: false,
    collapseOthers: true,
    canView: true,
    canAdd: true,
    canEdit: false,
    canDelete: false,
    hideConfigureButton: false,
  };

  const treeNodes: ITreeNodes = {
    ids: ['testing123', 'testing456'],
    entities: {
      testing123: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: true,
        symbol: 'collapsed',
      },
      testing456: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: false,
        symbol: 'collapsed',
      },
    },
    rootAssets: ['testing123'],
    childrenDict: null,
    treeID: '123',
    appContext: 'test',
  };

  const mockAssetTreeState: ITreeState = {
    treeConfiguration: treeConfig,
    treeNodes,
    hasDefaultSelectedAsset: true,
  };

  const mockTagsTabState: ITagsTabState = {
    servers: null,
    rights: null,
    selectedServer: null,
    tags: null,
    displayedTags: null,
    tagsDataAvailable: null,
    showDeletedTags: false,
    validationMessage: null,
  };

  const mockEventsTabState: IEventsTabState = {
    assetGlobalId: null,
    rights: null,
    events: null,
    eventTypes: [],
    eventHistory: null,
    message: null,
    validationMessage: null,
    newEventValid: false,
    saveResult: null,
    includeDescendants: false,
    descendentsLoading: false,
    changedEvents: null,
    changedEventCount: null,
  };

  const mockAssetExplorerState: IAssetExplorerState = {
    AssetTreeState: mockAssetTreeState,
    TagsTabState: mockTagsTabState,
    Tabs: null,
    InfoTabState: null,
    BlogTabState: null,
    EventsTabState: mockEventsTabState,
    LeftTraySize: 250,
  };

  const mockINavState: INavigationState = {
    breadcrumbMenu: null,
    asset: null,
    appName: '',
    navPaneState: 'open',
    showUserDialog: false,
    newNotifications: false,
    newNotificationsCount: 0,
    showRightTrayIcon: false,
    rightTrayOpen: false,
    user: {
      firstName: null,
      lastName: null,
      email: null,
    },
    userPreferences: {
      Theme: 'dark',
      Logo: '',
      Title: '',
    },
    navigationItems: [
      {
        id: 'asset_tree',
        order: 1,
        icon: 'testicon',
        text: 'Testing Asset tree',
        hover: 'Testing Asset tree',
        visible: true,
        enabled: true,
        selected: true,
        type: 'asset_button',
      },
    ],
    layoutItems: [],
    showNotifications: false,
    notificationList: [],
    timeSliderPaneState: 'none',
    showAssetTreeNavItemOnly: false,
  };

  const mockAppState: any = {
    nav: mockINavState,
    assetexplorer: mockAssetExplorerState,
  };

  it('selectApp', () => {
    expect(selectors.selectApp(mockAppState)).toEqual(mockAppState);
  });

  it('selectNavigationState', () => {
    expect(selectors.selectNavigationState.projector(mockINavState)).toEqual(
      mockINavState
    );
  });

  it('selectThemeState', () => {
    expect(
      selectors.selectThemeState.projector(
        selectors.selectNavigationState.projector(mockINavState)
      )
    ).toEqual(mockINavState.userPreferences.Theme);
  });

  it('selectAssetExplorerState', () => {
    expect(
      selectors.selectAssetExplorerState.projector(mockAssetExplorerState)
    ).toEqual(mockAssetExplorerState);
  });
});
