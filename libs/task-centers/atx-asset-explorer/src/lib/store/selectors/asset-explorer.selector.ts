import { createSelector, createFeatureSelector } from '@ngrx/store';
import { INavigationState } from '@atonix/atx-navigation';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  getSelectedNode,
  TrayState,
} from '@atonix/atx-asset-tree';

// This is actually grabbing the application state.  It doesn't really
// know what the full application state is, it
export const selectApp = (state: {
  nav: INavigationState;
  assetexplorer: IAssetExplorerState;
}) => state;

// This is grabbing the state of the navigation component.  It assumes the
// parent application will have a thing called 'nav' in its state of type INavigationState
const navigationState = createFeatureSelector<INavigationState>('nav');

// This looks at the application state and grabs the member
// named assetexplorer.  It isn't doing anything more complex than that.
const assetExplorerState =
  createFeatureSelector<IAssetExplorerState>('assetexplorer');

// This is grabbing the navigation state object
export const selectNavigationState = createSelector(
  navigationState,
  (state: INavigationState) => state
);

// This is grabbing the asset explorer state object
export const selectAssetExplorerState = createSelector(
  assetExplorerState,
  (state: IAssetExplorerState) => state
);

export const selectThemeState = createSelector(
  navigationState,
  (state) => state.userPreferences.Theme
);

export const selectTheme = createSelector(selectThemeState, (theme) => {
  return theme === 'light'
    ? 'ag-theme-alpine'
    : theme === 'dark'
    ? 'ag-theme-alpine-dark'
    : 'ag-theme-alpine-dark';
});

export const selectAssetTreeState = createSelector(
  selectAssetExplorerState,
  (state) => state.AssetTreeState
);

export const selectSelectedAsset = createSelector(
  selectAssetExplorerState,
  (state) =>
    getIDFromSelectedAssets(getSelectedNodes(state.AssetTreeState.treeNodes))
);

export const selectIsAssetSelected = createSelector(
  selectSelectedAsset,
  (state) => {
    if (state) {
      return true;
    }
    return false;
  }
);

export const selectSelectedNode = createSelector(
  selectAssetExplorerState,
  (state) => getSelectedNode(state.AssetTreeState.treeNodes)
);

export const selectSelectedNodeAbbrev = createSelector(
  selectSelectedNode,
  (node) => {
    let nodeAbbrev: string = null;

    if (node?.data?.NodeAbbrev) {
      nodeAbbrev = node.data.NodeAbbrev;
    }

    return nodeAbbrev;
  }
);

export const selectIsAdHocTree = createSelector(selectSelectedNode, (node) => {
  if (node?.data?.CameFromDefaultTree) {
    return false;
  }
  return true;
});

export const selectSelectedNodeData = createSelector(
  selectSelectedNode,
  (node) => node?.data
);

export const selectSelectedAssetId = createSelector(
  selectSelectedNode,
  (node) => {
    return node?.data?.AssetId || null;
  }
);

export const selectSelectedGlobalId = createSelector(
  selectSelectedNode,
  (node) => {
    return node?.data?.Asset?.GlobalId || null;
  }
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration
);

export const selectAssetTreePin = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration.pin
);

export const selectLeftTrayMode = createSelector(selectAssetTreePin, (pin) => {
  const result: TrayState = pin ? 'side' : 'over';
  return result;
});

export const selectLeftTraySize = createSelector(
  selectAssetExplorerState,
  (state) => state.LeftTraySize
);

export const selectTabs = createSelector(
  selectAssetExplorerState,
  (state) => state.Tabs
);

export const selectInfoTabState = createSelector(
  selectAssetExplorerState,
  (state) => state.InfoTabState
);

export const selectEventsTab = createSelector(
  selectAssetExplorerState,
  (state) => state.EventsTabState
);
export const selectEventsPermissions = createSelector(
  selectEventsTab,
  (state) => state.rights
);
