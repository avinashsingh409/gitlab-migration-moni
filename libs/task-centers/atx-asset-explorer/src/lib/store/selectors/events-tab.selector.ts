import { selectAssetExplorerState } from './asset-explorer.selector';
import { createSelector } from '@ngrx/store';

export const selectEventsTab = createSelector(
  selectAssetExplorerState,
  (state) => state.EventsTabState
);
export const selectEventAssetGlobalId = createSelector(
  selectEventsTab,
  (state) => state.assetGlobalId
);
export const selectEvents = createSelector(
  selectEventsTab,
  (state) => state.events
);
export const selectEventTypes = createSelector(
  selectEventsTab,
  (state) => state.eventTypes
);
export const selectEventHistory = createSelector(
  selectEventsTab,
  (state) => state.eventHistory
);
export const selectValidationResult = createSelector(
  selectEventsTab,
  (state) => state.validationMessage
);
export const selectMessage = createSelector(
  selectEventsTab,
  (state) => state.message
);
export const selectNewEventInvalid = createSelector(
  selectEventsTab,
  (state) => state.newEventValid
);
export const selectSaveResult = createSelector(
  selectEventsTab,
  (state) => state.saveResult
);
export const selectEventsViewable = createSelector(
  selectEventsTab,
  (state) => state?.rights?.CanView ?? false
);
export const selectIncludeDescendents = createSelector(
  selectEventsTab,
  (state) => state.includeDescendants
);
export const selectDescendentsLoading = createSelector(
  selectEventsTab,
  (state) => state.descendentsLoading
);

export const selectChangedEvents = createSelector(
  selectEventsTab,
  (state) => state.changedEvents
);

export const selectChangedEventCount = createSelector(
  selectEventsTab,
  (state) => state.changedEventCount
);
