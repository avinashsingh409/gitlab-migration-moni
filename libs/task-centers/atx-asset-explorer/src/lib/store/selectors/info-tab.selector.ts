import { createSelector } from '@ngrx/store';
import { selectInfoTabState } from './asset-explorer.selector';

export const selectAssetInfo = createSelector(selectInfoTabState, (s) => {
  if (s?.entities) {
    return s.entities[s.selectedAssetInfo] ?? null;
  }
  return null;
});

export const selectCanChangeType = createSelector(selectAssetInfo, (ai) => {
  return ai?.parent ? true : false;
});

export const selectAssetClassTypesLoading = createSelector(
  selectAssetInfo,
  (s) => {
    if (s && s.assetClassTypesLoading) {
      return s.assetClassTypesLoading;
    }
    return null;
  }
);

export const selectAssetClassTypes = createSelector(selectAssetInfo, (s) => {
  if (s && s.assetClassTypes) {
    return s.assetClassTypes;
  }
  return null;
});

export const selectAttributes = createSelector(selectAssetInfo, (s) => {
  if (s && s.attributes) {
    return s.attributes;
  }
  return null;
});

export const selectTags = createSelector(selectAssetInfo, (s) => {
  if (s && s.tags) {
    return s.tags;
  }
  return null;
});

export const selectAutoCompleteLoading = createSelector(
  selectAssetInfo,
  (s) => {
    if (s && s.autoCompleteLoading) {
      return s.autoCompleteLoading;
    }
    return null;
  }
);

export const selectAutoCompleteKeywords = createSelector(
  selectAssetInfo,
  (s) => {
    if (s && s.autoCompleteKeywords) {
      return s.autoCompleteKeywords;
    }
    return null;
  }
);
