import { createSelector } from '@ngrx/store';
import { selectAssetExplorerState } from './asset-explorer.selector';
import { isNil } from '@atonix/atx-core';

export const selectTagsTab = createSelector(
  selectAssetExplorerState,
  (state) => state.TagsTabState
);
export const selectTagsPermissions = createSelector(
  selectTagsTab,
  (state) => state?.rights
);
export const selectCanView = createSelector(
  selectTagsPermissions,
  (rights) => rights?.CanView
);
export const selectCanEdit = createSelector(
  selectTagsPermissions,
  (rights) => rights?.CanEdit
);
export const selectTagServers = createSelector(
  selectTagsTab,
  (state) => state.servers
);
export const selectTags = createSelector(
  selectTagsTab,
  (state) => state.displayedTags
);
export const selectAllTags = createSelector(
  selectTagsTab,
  (state) => state.tags
);
export const selectSelectedServer = createSelector(
  selectTagsTab,
  (state) => state.selectedServer
);
export const isServerSelected = createSelector(
  selectSelectedServer,
  (state) => {
    return !isNil(state);
  }
);
export const selectTagsState = createSelector(
  selectTagsTab,
  (state) => state.tagsDataAvailable
);
export const selectShowDeletedTags = createSelector(
  selectTagsTab,
  (state) => state.showDeletedTags
);
export const selectValidationStatus = createSelector(
  selectTagsTab,
  (state) => state.validationMessage
);
