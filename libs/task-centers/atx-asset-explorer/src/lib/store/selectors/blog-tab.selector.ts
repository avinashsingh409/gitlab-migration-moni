import { createSelector } from '@ngrx/store';
import { selectAssetExplorerState } from './asset-explorer.selector';

export const selectBlogTabState = createSelector(
  selectAssetExplorerState,
  (state) => state.BlogTabState
);

export const selectDiscussionAssetTypes = createSelector(
  selectBlogTabState,
  (state) => state.discussionAssetTypes
);

export const selectIssuesCount = createSelector(
  selectBlogTabState,
  (state) => state.issuesCount
);

export const selectUploadingAttachments = createSelector(
  selectBlogTabState,
  (state) => state?.uploadingAttachments || false
);

export const selectDiscussion = createSelector(
  selectBlogTabState,
  (state) => state?.discussion || null
);

export const selectDiscussionEntry = createSelector(
  selectBlogTabState,
  (state) => state?.discussionEntry || null
);

export const selectDiscussionEntryAttachments = createSelector(
  selectDiscussionEntry,
  (state) => state?.Attachments || null
);

export const selectNextDisplayOrder = createSelector(
  selectDiscussionEntryAttachments,
  (state) => {
    let displayOrder = 1;
    if (state && state.length > 0) {
      displayOrder = state.length + 1;
    }

    return displayOrder;
  }
);

export const selectDiscussionEntryKeywords = createSelector(
  selectDiscussionEntry,
  (state) => state?.Keywords || null
);

export const selectAutoCompleteLoading = createSelector(
  selectBlogTabState,
  (state) => state?.autoCompleteLoading || null
);

export const selectAutoCompleteKeywords = createSelector(
  selectBlogTabState,
  (state) => state?.autoCompleteKeywords
);
