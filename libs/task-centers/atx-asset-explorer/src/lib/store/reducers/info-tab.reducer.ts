import { createReducer, on } from '@ngrx/store';
import * as infoTabActions from '../actions/info-tab.actions';
import { infoTabAdapter } from '../state/info-tab-state';
import { IAssetInfoState, IAttribute } from '../../model/asset-info';
import { getValueFromAttribute } from '../../service/asset-explorer.helpers';
import { Update } from '@ngrx/entity';
import {
  IAssetAndAttributes,
  IAssetClassType,
  ITaggingKeyword,
} from '@atonix/atx-core';
import { IAssetExplorerState } from '../state/asset-explorer-state';

export const infoTabReducers = [
  on(infoTabActions.routeLoadInfo, routeLoadInfo),
  on(infoTabActions.routeLoadInfoSuccess, routeLoadInfoSuccess),
  on(infoTabActions.routeLoadInfoFailure, routeLoadInfoFailure),
  on(infoTabActions.getAssetClassTypes, getAssetClassTypes),
  on(infoTabActions.getAssetClassTypesSuccess, getAssetClassTypesSuccess),
  on(infoTabActions.getAssetClassTypesFailure, getAssetClassTypesFailure),
  on(infoTabActions.changeAttribute, changeAttribute),
  on(infoTabActions.searchTaggingKeywords, searchTaggingKeywords),
  on(infoTabActions.searchTaggingKeywordsSuccess, searchTaggingKeywordsSuccess),
  on(infoTabActions.searchTaggingKeywordsFailure, searchTaggingKeywordsFailure),
  on(infoTabActions.registerKeywordFailure, registerKeywordFailure),
  on(infoTabActions.updateKeywords, updateKeywords),
  on(infoTabActions.editAttribute, editAttribute),
  on(infoTabActions.favoriteAttribute, favoriteAttribute),
];

export function routeLoadInfo(
  state: IAssetExplorerState,
  payload: { asset: string }
): IAssetExplorerState {
  return {
    ...state,
    InfoTabState: {
      ...state.InfoTabState,
      selectedAssetInfo: payload.asset,
    },
    Loading: true,
  };
}

export function routeLoadInfoSuccess(
  state: IAssetExplorerState,
  payload: IAssetAndAttributes
) {
  let assetInfo: IAssetInfoState;
  let attributes: IAttribute[] = [];
  const sortedAttributes = [...payload.Attributes].sort((a, b) => {
    return a.DisplayOrder - b.DisplayOrder;
  });

  attributes = sortedAttributes.map((attr) => {
    const newAttributeInfo: IAttribute = {
      Id: attr.AssetAttributeID,
      AssetAttribute: attr,
      Favorite: attr.Favorite,
      DisplayOrder: attr.DisplayOrder,
      Attribute: attr.AttributeType.AttributeTypeDesc,
      Value: getValueFromAttribute(attr),
      Units: attr.AttributeType.EngrUnits,
      Category: null,
    };

    return newAttributeInfo;
  });

  // eslint-disable-next-line prefer-const
  assetInfo = {
    id: state.InfoTabState.selectedAssetInfo,
    asset: payload.Asset,
    parent: payload.Parent,
    assetClassTypesLoading: false,
    assetClassTypes: [payload.Asset.AssetClassType],
    attributes,
    tags: payload.Asset.Keywords,
    autoCompleteLoading: false,
    autoCompleteKeywords: null,
  };

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.addOne(assetInfo, state.InfoTabState),
      selectedAssetInfo: assetInfo.id,
    },
  };
}

export function routeLoadInfoFailure(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  return { ...state, Loading: false };
}
export function getAssetClassTypes(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  const updatedAssetInfo = {
    changes: {
      assetClassTypesLoading:
        state.InfoTabState.entities[state.InfoTabState.selectedAssetInfo]
          .assetClassTypes?.length <= 1 || true,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function getAssetClassTypesSuccess(
  state: IAssetExplorerState,
  payload: { assetClassTypes: IAssetClassType[] }
) {
  const updatedAssetInfo = {
    changes: {
      assetClassTypesLoading: false,
      assetClassTypes: payload.assetClassTypes,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function getAssetClassTypesFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  const updatedAssetInfo = {
    changes: {
      assetClassTypesLoading: false,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function changeAttribute(
  state: IAssetExplorerState,
  payload: { attribute: IAttribute }
): IAssetExplorerState {
  const attributes = [
    ...state.InfoTabState.entities[state.InfoTabState.selectedAssetInfo]
      .attributes,
  ];
  const idx = attributes.findIndex((n) => n.Id === payload.attribute.Id);
  attributes.splice(idx, 1, { ...payload.attribute });

  const updatedAssetInfo = {
    changes: {
      attributes,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function searchTaggingKeywords(
  state: IAssetExplorerState,
  payload: { asset: string; searchExpression: string }
) {
  const updatedAssetInfo = {
    changes: {
      autoCompleteLoading:
        payload.searchExpression !== null && payload.searchExpression !== '',
      autoCompleteKeywords: null,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function searchTaggingKeywordsSuccess(
  state: IAssetExplorerState,
  payload: { keywords: ITaggingKeyword[] }
) {
  const updatedAssetInfo = {
    changes: {
      autoCompleteLoading: false,
      autoCompleteKeywords: payload.keywords,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function searchTaggingKeywordsFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  const updatedAssetInfo = {
    changes: {
      autoCompleteLoading: false,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function registerKeywordFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}

export function updateKeywords(
  state: IAssetExplorerState,
  payload: ITaggingKeyword
) {
  const tags = [
    ...state.InfoTabState.entities[state.InfoTabState.selectedAssetInfo].tags,
  ];
  tags.push(payload);

  const updatedAssetInfo = {
    changes: {
      tags,
    },
    id: state.InfoTabState.selectedAssetInfo,
  } as Update<IAssetInfoState>;

  return {
    ...state,
    InfoTabState: {
      ...infoTabAdapter.updateOne(updatedAssetInfo, state.InfoTabState),
    },
  };
}

export function editAttribute(
  state: IAssetExplorerState,
  payload: { id: number }
) {
  return { ...state };
}

export function favoriteAttribute(
  state: IAssetExplorerState,
  payload: { id: number; favorite: boolean }
) {
  const asset =
    state.InfoTabState.entities[state.InfoTabState.selectedAssetInfo];
  let attributes = asset.attributes;
  const idx = attributes.findIndex((n) => n.Id === payload.id);
  if (idx >= 0) {
    attributes = [...attributes];
    attributes.splice(idx, 1, {
      ...attributes[idx],
      Favorite: payload.favorite,
    });
  }
  return {
    ...state,
    InfoTabState: infoTabAdapter.updateOne(
      {
        id: state.InfoTabState.selectedAssetInfo,
        changes: { attributes },
      },
      state.InfoTabState
    ),
  };
}
