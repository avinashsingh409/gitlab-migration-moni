import { on } from '@ngrx/store';
import * as tagsTabActions from '../actions/tags-tab.actions';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import { ISecurityRights, IPDTag, IPDServer } from '@atonix/atx-core';
import { TagsDataAvailable } from '../state/tags-tab-state';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
export const tagsTabReducers = [
  on(tagsTabActions.routeLoadTags, routeLoadTags),
  on(tagsTabActions.routeLoadTagsSuccess, routeLoadTagsSuccess),
  on(tagsTabActions.routeLoadTagsFailure, routeLoadTagsFailure),
  on(tagsTabActions.getTags, getTags),
  on(tagsTabActions.getTagsSuccess, getTagsSuccess),
  on(tagsTabActions.getTagsFailure, getTagsFailure),
  on(tagsTabActions.discardChanges, discardChanges),
  on(tagsTabActions.discardChangesSuccess, discardChangesSuccess),
  on(tagsTabActions.discardChangesFailure, discardChangesFailure),
  on(tagsTabActions.addTag, addTag),
  on(tagsTabActions.changeTag, changeTag),
  on(tagsTabActions.showDeletedTagsChange, showDeletedTagsChange),
  on(tagsTabActions.deleteTag, deleteTag),
  on(tagsTabActions.saveTags, saveTags),
  on(tagsTabActions.saveTagsSuccess, saveTagsSuccess),
  on(tagsTabActions.saveTagsFailure, saveTagsFailure),
  on(tagsTabActions.validateTags, validateTags),
  on(tagsTabActions.validateTagsSuccess, validateTagsSuccess),
  on(tagsTabActions.validateTagsFailure, validateTagsFailure),
  on(tagsTabActions.pasteTags, pasteTags),
];

export function getNextTagID(tags: IPDTag[]) {
  let nextTagID = Math.min(...tags.map((n) => n.PDTagID));
  if (nextTagID >= 0) {
    nextTagID = -1;
  } else {
    nextTagID = nextTagID - 1;
  }
  return nextTagID;
}

export function routeLoadTags(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      tags: null,
      displayedTags: null,
      servers: null,
      selectedServer: null,
      validationMessage: null,
    },
  };
}

export function routeLoadTagsSuccess(
  state: IAssetExplorerState,
  payload: { asset: string; rights: ISecurityRights; servers: IPDServer[] }
) {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      rights: payload.rights,
      servers: payload.servers,
      selectedServer: null,
      tags: null,
      displayedTags: null,
      validationMessage: null,
    },
  };
}

export function routeLoadTagsFailure(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  return { ...state };
}

export function getTags(
  state: IAssetExplorerState,
  payload: { server: number }
): IAssetExplorerState {
  let tagsDataAvailable: TagsDataAvailable = '';
  if (payload.server ?? -1 >= 0) {
    tagsDataAvailable = 'loading';
  }
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      selectedServer: payload.server,
      tags: null,
      displayedTags: null,
      tagsDataAvailable,
    },
  };
}

export function getTagsSuccess(
  state: IAssetExplorerState,
  payload: { server: number; tags: IPDTag[] }
): IAssetExplorerState {
  let tagsDataAvailable: TagsDataAvailable = 'nodata';
  if (payload.tags && payload.tags.length > 0) {
    tagsDataAvailable = '';
  }
  const displayedTags = getSortedFilteredTags(
    payload.tags,
    state.TagsTabState.showDeletedTags
  );
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      selectedServer: payload.server,
      tags: payload.tags,
      displayedTags,
      tagsDataAvailable,
    },
  };
}

export function getTagsFailure(
  state: IAssetExplorerState,
  payload: Error
): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      selectedServer: null,
      tags: null,
      displayedTags: null,
      tagsDataAvailable: '',
    },
  };
}

export function discardChanges(
  state: IAssetExplorerState
): IAssetExplorerState {
  let tagsDataAvailable: TagsDataAvailable = 'loading';
  tagsDataAvailable = 'loading';
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      tags: null,
      displayedTags: null,
      tagsDataAvailable,
    },
  };
}

export function discardChangesSuccess(
  state: IAssetExplorerState,
  payload: { server: number; tags: IPDTag[] }
): IAssetExplorerState {
  let tagsDataAvailable: TagsDataAvailable = 'nodata';
  if (payload.tags && payload.tags.length > 0) {
    tagsDataAvailable = '';
  }
  const displayedTags = getSortedFilteredTags(
    payload.tags,
    state.TagsTabState.showDeletedTags
  );
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      selectedServer: payload.server,
      tags: payload.tags,
      displayedTags,
      tagsDataAvailable,
    },
  };
}

export function discardChangesFailure(
  state: IAssetExplorerState,
  payload: Error
): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      selectedServer: null,
      tags: null,
      displayedTags: null,
      tagsDataAvailable: '',
    },
  };
}

export function addTag(state: IAssetExplorerState): IAssetExplorerState {
  const newTag: IPDTag = {
    PDTagID: getNextTagID(state.TagsTabState.tags),
    TagDesc: 'New Tag',
    TagName: 'New Tag',
    PDServerID: state.TagsTabState.selectedServer,
    EngUnits: '',
    ExistsOnServer: true,
    ExternalID: '',
    Qualifier: '',
    Mapping: '',
    GlobalID: null,
    NDCreated: false,
    NDPath: '',
    NDName: '',
    IsAP: false,
  };
  const tags = changeSingleTag(state.TagsTabState.tags, newTag);
  const displayedTags = getSortedFilteredTags(
    tags,
    state.TagsTabState.showDeletedTags
  );
  return {
    ...state,
    TagsTabState: { ...state.TagsTabState, tags, displayedTags },
  };
}

export function changeTag(
  state: IAssetExplorerState,
  payload: { tag: IPDTag }
): IAssetExplorerState {
  const tags = changeSingleTag(state.TagsTabState.tags, payload.tag);
  const displayedTags = getSortedFilteredTags(
    tags,
    state.TagsTabState.showDeletedTags
  );
  return {
    ...state,
    TagsTabState: { ...state.TagsTabState, tags, displayedTags },
  };
}

export function changeSingleTag(tags: IPDTag[], tag: IPDTag) {
  let result: IPDTag[];
  const idx = tags.findIndex((n) => n.PDTagID === tag.PDTagID);
  if (idx >= 0) {
    result = [...tags];
    result.splice(idx, 1, { ...tag });
  } else {
    result = [tag, ...tags];
  }
  return result;
}

export function showDeletedTagsChange(
  state: IAssetExplorerState,
  payload: { newValue: boolean }
): IAssetExplorerState {
  const displayedTags = getSortedFilteredTags(
    state.TagsTabState.tags,
    payload.newValue
  );
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      showDeletedTags: payload.newValue,
      displayedTags,
    },
  };
}

export function deleteTag(
  state: IAssetExplorerState,
  payload: { tagID: number }
): IAssetExplorerState {
  let tags = state.TagsTabState.tags;
  const index = tags.findIndex((n) => {
    return n.PDTagID === payload.tagID;
  });
  if (index >= 0) {
    if (payload.tagID > 0) {
      const newTag = {
        ...state.TagsTabState.tags[index],
        ExistsOnServer: !state.TagsTabState.tags[index].ExistsOnServer,
      };
      tags = [...state.TagsTabState.tags];
      tags.splice(index, 1, newTag);
    } else {
      tags = [...state.TagsTabState.tags];
      tags.splice(index, 1);
    }
  }

  const displayedTags = getSortedFilteredTags(
    tags,
    state.TagsTabState.showDeletedTags
  );

  return {
    ...state,
    TagsTabState: { ...state.TagsTabState, tags, displayedTags },
  };
}

function getSortedFilteredTags(tags: IPDTag[], showDeletedTags: boolean) {
  let result = [...tags];
  if (!showDeletedTags) {
    result = result.filter((n) => n.ExistsOnServer);
  }
  return result;
}

function saveTags(state: IAssetExplorerState): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: { ...state.TagsTabState, validationMessage: 'Saving Tags' },
  };
}

function saveTagsSuccess(
  state: IAssetExplorerState,
  payload: { result: string | IPDTag[]; server: number }
): IAssetExplorerState {
  if (payload.server === state.TagsTabState.selectedServer) {
    if (typeof payload.result === 'string') {
      return {
        ...state,
        TagsTabState: {
          ...state.TagsTabState,
          validationMessage: payload.result as string,
        },
      };
    } else {
      const displayedTags = getSortedFilteredTags(
        payload.result as IPDTag[],
        state.TagsTabState.showDeletedTags
      );
      return {
        ...state,
        TagsTabState: {
          ...state.TagsTabState,
          tags: payload.result as IPDTag[],
          displayedTags,
          validationMessage: 'Tags Saved Successfully',
        },
      };
    }
  }

  return { ...state };
}

function saveTagsFailure(
  state: IAssetExplorerState,
  payload: Error
): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      validationMessage: 'Tag Save Failed',
    },
  };
}

function validateTags(state: IAssetExplorerState): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      validationMessage: 'Validating Tags',
    },
  };
}

function validateTagsSuccess(
  state: IAssetExplorerState,
  payload: { message: string }
): IAssetExplorerState {
  if (payload.message) {
    return {
      ...state,
      TagsTabState: {
        ...state.TagsTabState,
        validationMessage: payload.message,
      },
    };
  } else {
    return {
      ...state,
      TagsTabState: {
        ...state.TagsTabState,
        validationMessage: 'Tags Validated Successfully',
      },
    };
  }
}

function validateTagsFailure(
  state: IAssetExplorerState,
  payload: Error
): IAssetExplorerState {
  return {
    ...state,
    TagsTabState: {
      ...state.TagsTabState,
      validationMessage: 'Tag Validation Failed',
    },
  };
}

function pasteTags(
  state: IAssetExplorerState,
  payload: { tags: IPDTag[] }
): IAssetExplorerState {
  const existingTagsDict: { [id: number]: IPDTag } = {};
  for (const t of state.TagsTabState.tags) {
    existingTagsDict[t.PDTagID] = t;
  }

  let tags: IPDTag[] = state.TagsTabState.tags;
  for (const t of payload.tags) {
    if (existingTagsDict[t.PDTagID]) {
      tags = changeSingleTag(tags, t);
    } else {
      tags = changeSingleTag(tags, {
        ...t,
        PDTagID: getNextTagID(tags),
        PDServerID: state.TagsTabState.selectedServer,
        ExistsOnServer: true,
        GlobalID: null,
        NDCreated: false,
        IsAP: false,
        ExternalID: '',
      });
    }
  }

  const displayedTags = getSortedFilteredTags(
    tags,
    state.TagsTabState.showDeletedTags
  );
  return {
    ...state,
    TagsTabState: { ...state.TagsTabState, tags, displayedTags },
  };
}
