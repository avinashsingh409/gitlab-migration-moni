import { on } from '@ngrx/store';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import * as blogTabActions from '../actions/blog-tab.actions';
import {
  IDiscussion,
  IDiscussionAssetMapType,
  IDiscussionAttachment,
  IDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';
import {
  processDiscussion,
  processEntry,
} from '../../service/asset-explorer.helpers';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
export const blogTabReducers = [
  on(blogTabActions.routeLoadBlog, routeLoadBlog),
  on(blogTabActions.routeLoadBlogSuccess, routeLoadBlogSuccess),
  on(blogTabActions.routeLoadBlogFailure, routeLoadBlogFailure),
  on(blogTabActions.loadDiscussionSuccess, loadDiscussionSuccess),
  on(blogTabActions.loadDiscussionFailure, loadDiscussionFailure),
  on(blogTabActions.getNewDiscussionEntry, getNewDiscussionEntry),
  on(blogTabActions.getNewDiscussionEntrySuccess, getNewDiscussionEntrySuccess),
  on(blogTabActions.getNewDiscussionEntryFailure, getNewDiscussionEntryFailure),
  on(blogTabActions.updateDiscussionEntryTitle, updateDiscussionEntryTitle),
  on(blogTabActions.updateDiscussionEntryContent, updateDiscussionEntryContent),
  on(blogTabActions.saveDiscussionEntrySuccess, saveDiscussionEntrySuccess),
  on(blogTabActions.saveDiscussionEntryFailure, saveDiscussionEntryFailure),
  on(blogTabActions.addAttachment, addAttachment),
  on(blogTabActions.addAttachmentSuccess, addAttachmentSuccess),
  on(blogTabActions.addAttachmentFailure, addAttachmentFailure),
  on(blogTabActions.searchTaggingKeywords, searchTaggingKeywords),
  on(blogTabActions.searchTaggingKeywordsSuccess, searchTaggingKeywordsSuccess),
  on(blogTabActions.searchTaggingKeywordsFailure, searchTaggingKeywordsFailure),
  on(blogTabActions.registerKeywordFailure, registerKeywordFailure),
  on(blogTabActions.updateKeywords, updateKeywords),
];

export function routeLoadBlog(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  return { ...state };
}

export function routeLoadBlogSuccess(
  state: IAssetExplorerState,
  payload: {
    discussionAssetTypes: IDiscussionAssetMapType[];
    issuesCount: number;
  }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionAssetTypes: payload.discussionAssetTypes,
      issuesCount: payload.issuesCount,
    },
  };
}

export function routeLoadBlogFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}

export function loadDiscussionSuccess(
  state: IAssetExplorerState,
  payload: { discussions: IDiscussion[] }
) {
  const discussion = processDiscussion(payload.discussions);
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussion,
    },
  };
}

export function loadDiscussionFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}

export function getNewDiscussionEntry(state: IAssetExplorerState) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: null,
    },
  };
}

export function getNewDiscussionEntrySuccess(
  state: IAssetExplorerState,
  payload: { discussionEntry: IDiscussionEntry }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: payload.discussionEntry,
    },
  };
}

export function getNewDiscussionEntryFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}

export function updateDiscussionEntryTitle(
  state: IAssetExplorerState,
  payload: { title: string }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: {
        ...state.BlogTabState.discussionEntry,
        Title: payload.title,
      },
    },
  };
}

export function updateDiscussionEntryContent(
  state: IAssetExplorerState,
  payload: { content: string }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: {
        ...state.BlogTabState.discussionEntry,
        Content: payload.content,
      },
    },
  };
}

export function saveDiscussionEntrySuccess(
  state: IAssetExplorerState,
  payload: {
    showedTopic: number;
    entryTopic: number;
    savedDiscussionEntry: IDiscussionEntry;
  }
) {
  const discussion = { ...state.BlogTabState.discussion };
  const savedDiscussionEntry = processEntry(payload.savedDiscussionEntry);

  // check if entry exist in the current discussion
  const entryIndex = discussion.Entries.findIndex(
    (entry) =>
      entry.DiscussionEntryID === savedDiscussionEntry.DiscussionEntryID
  );
  if (
    !payload.entryTopic ||
    payload.showedTopic === payload.entryTopic ||
    payload.showedTopic === 0
  ) {
    if (entryIndex >= 0) {
      discussion.Entries = [...discussion.Entries].splice(
        entryIndex,
        1,
        savedDiscussionEntry
      );
    } else {
      discussion.Entries = [savedDiscussionEntry, ...discussion.Entries];
    }
  } else {
    discussion.Entries = [...discussion.Entries].splice(entryIndex, 1);
  }

  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussion,
    },
  };
}

export function saveDiscussionEntryFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}
export function addAttachment(
  state: IAssetExplorerState,
  payload: {
    discussionID: string;
    entryID: string;
    title: string;
    discussionType: string;
    file: any;
  }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      uploadingAttachments: true,
    },
  };
}

export function addAttachmentSuccess(
  state: IAssetExplorerState,
  payload: { attachment: IDiscussionAttachment }
) {
  // eslint-disable-next-line no-unsafe-optional-chaining
  const attachments = [...state.BlogTabState?.discussionEntry.Attachments];
  attachments.push(payload.attachment);

  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: {
        ...state.BlogTabState.discussionEntry,
        Attachments: attachments,
      },
      uploadingAttachments: false,
    },
  };
}

export function addAttachmentFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      uploadingAttachments: false,
    },
  };
}

export function searchTaggingKeywords(
  state: IAssetExplorerState,
  payload: { searchExpression: string }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      autoCompleteLoading:
        payload.searchExpression !== null && payload.searchExpression !== '',
      autoCompleteKeywords: null,
    },
  };
}

export function searchTaggingKeywordsSuccess(
  state: IAssetExplorerState,
  payload: { keywords: ITaggingKeyword[] }
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      autoCompleteLoading: false,
      autoCompleteKeywords: payload.keywords,
    },
  };
}

export function searchTaggingKeywordsFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      autoCompleteLoading: false,
    },
  };
}

export function registerKeywordFailure(
  state: IAssetExplorerState,
  payload: Error
) {
  return { ...state };
}

export function updateKeywords(
  state: IAssetExplorerState,
  payload: { keyword: ITaggingKeyword }
) {
  const keywords = [...state.BlogTabState.discussionEntry.Keywords];
  keywords.push(payload.keyword);

  return {
    ...state,
    BlogTabState: {
      ...state.BlogTabState,
      discussionEntry: {
        ...state.BlogTabState.discussionEntry,
        Keywords: keywords,
      },
    },
  };
}
