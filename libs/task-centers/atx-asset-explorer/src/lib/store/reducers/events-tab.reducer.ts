import { on } from '@ngrx/store';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import * as eventActions from '../actions/events-tab.actions';
import { applyEventTypesToEvents } from '@atonix/shared/utils';
import { IEvent, IEventChange, IEventType } from '@atonix/atx-core';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
export const eventsTabReducers = [
  on(eventActions.routeLoadEvents, routeLoadEvents),
  on(eventActions.routeLoadEventsSuccess, routeLoadEventsSuccess),
  on(eventActions.routeLoadEventsFailure, routeLoadEventsFailure),
  on(eventActions.clearEvents, clearEvents),
  on(eventActions.reloadEvents, reloadEvents),
  on(eventActions.reloadEventsSuccess, reloadEventsSuccess),
  on(eventActions.reloadEventsFailure, reloadEventsFailure),
  on(eventActions.getEventTypesSuccess, getEventTypesSuccess),
  on(eventActions.getEventHistory, getEventHistory),
  on(eventActions.getEventHistorySuccess, getEventHistorySuccess),
  on(eventActions.getEventHistoryFailure, getEventHistoryFailure),
  on(eventActions.validateEvent, validateEvent),
  on(eventActions.validateEventSuccess, validateEventSuccess),
  on(eventActions.validateEventFailure, validateEventFailure),
  on(eventActions.saveEvent, saveEvent),
  on(eventActions.saveEventSuccess, saveEventSuccess),
  on(eventActions.saveEventFailure, saveEventFailure),
  on(eventActions.changeEvent, changeEvent),
  on(eventActions.changeEventSuccess, changeEventSuccess),
  on(eventActions.changeEventFailure, changEventFailure),
  on(eventActions.deleteEvent, deleteEvent),
  on(eventActions.deleteEventSuccess, deleteEventSuccess),
  on(eventActions.deleteEventFailure, deleteEventFailure),
  on(eventActions.eventChanged, eventChanged),
  on(eventActions.eventsDialogOpened, eventsDialogOpened),
  on(eventActions.validationDialogReset, validationDialogReset),
  on(eventActions.includeDescendantsEvent, includesDescendantsChecked),
];

export function routeLoadEvents(
  state: IAssetExplorerState,
  payload: { asset: string }
) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      assetGlobalId: payload.asset,
      includeDescendants: false,
    },
  };
}

export function routeLoadEventsSuccess(
  state: IAssetExplorerState,
  payload: { events: IEvent[] }
) {
  const events = applyEventTypesToEvents(
    state.EventsTabState.eventTypes,
    payload.events
  );
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      descendentsLoading: false,
      events,
    },
  };
}

export function routeLoadEventsFailure(
  state: IAssetExplorerState,
  payload: { error }
) {
  return { ...state };
}

export function clearEvents(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      events: null,
    },
  };
}

export function reloadEvents(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      events: null,
    },
  };
}

export function reloadEventsSuccess(
  state: IAssetExplorerState,
  payload: { events: IEvent[] }
) {
  const events = applyEventTypesToEvents(
    state.EventsTabState.eventTypes,
    payload.events
  );
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      descendentsLoading: false,
      events,
    },
  };
}

export function reloadEventsFailure(state: IAssetExplorerState, error) {
  return { ...state };
}

export function getEventTypesSuccess(
  state: IAssetExplorerState,
  payload: { eventTypes: IEventType[] }
) {
  const events = applyEventTypesToEvents(
    payload.eventTypes,
    state.EventsTabState.events
  );
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      eventTypes: payload.eventTypes,
      events,
    },
  };
}

export function getEventHistory(
  state: IAssetExplorerState,
  payload: { eventID: string }
) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      eventHistory: null,
    },
  };
}

export function getEventHistorySuccess(
  state: IAssetExplorerState,
  payload: { events: IEvent[] }
) {
  const events = applyEventTypesToEvents(
    state.EventsTabState.eventTypes,
    payload.events
  );

  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      eventHistory: events,
    },
  };
}

export function getEventHistoryFailure(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Getting Event history failed',
    },
  };
}

export function validateEvent(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: 'Validating...',
      newEventValid: false,
    },
  };
}

export function validateEventSuccess(
  state: IAssetExplorerState,
  payload: {
    changedEventCount: number;
    validationMessage: string;
    changedEvents: IEventChange[];
  }
) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: payload.validationMessage,
      newEventValid: true,
      changedEvents: payload.changedEvents,
      changedEventCount: payload.changedEventCount,
    },
  };
}

export function validateEventFailure(
  state: IAssetExplorerState,
  payload: { error }
) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: payload.error.error.Results[0].Message,
      newEventValid: false,
    },
  };
}

export function saveEvent(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: { ...state.EventsTabState, saveResult: null },
  };
}

export function saveEventSuccess(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Event saved successfully.',
    },
  };
}

export function saveEventFailure(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Saving event failed',
    },
  };
}

export function changeEvent(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: { ...state.EventsTabState, saveResult: null },
  };
}

export function changeEventSuccess(
  state: IAssetExplorerState,
  payload: { event: IEvent }
) {
  const events = [...state.EventsTabState.events];
  const idx = events.findIndex((n) => n.EventID === payload.event.EventID);

  if (idx >= 0) {
    events.splice(idx, 1, payload.event);
  } else {
    events.push(payload.event);
  }
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      events,
      saveResult: 'Event updated successfully.',
    },
  };
}

export function changEventFailure(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Update event failed',
    },
  };
}

export function deleteEvent(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: { ...state.EventsTabState, saveResult: null },
  };
}

export function deleteEventSuccess(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Event deleted successfully.',
    },
  };
}

export function deleteEventFailure(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      saveResult: 'Deleting event failed',
    },
  };
}

export function eventChanged(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: null,
      newEventValid: false,
    },
  };
}

export function eventsDialogOpened(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: null,
      newEventValid: false,
      changedEventCount: 0,
      changedEvents: null,
    },
  };
}

export function validationDialogReset(state: IAssetExplorerState) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      validationMessage: null,
      newEventValid: false,
      changedEventCount: 0,
      changedEvents: null,
    },
  };
}

export function includesDescendantsChecked(
  state: IAssetExplorerState,
  payload: { value: boolean }
) {
  return {
    ...state,
    EventsTabState: {
      ...state.EventsTabState,
      includeDescendants: payload.value,
      descendentsLoading: true,
    },
  };
}
