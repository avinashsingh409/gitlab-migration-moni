import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import {
  ITreeStateChange,
  alterAssetTreeState,
  getIDFromSelectedAssets,
  getSelectedNodes,
} from '@atonix/atx-asset-tree';
import * as assetExplorerActions from '../actions/asset-explorer.actions';
import {
  initialAssetExplorerState,
  IAssetExplorerState,
} from '../state/asset-explorer-state';
import { infoTabReducers } from './info-tab.reducer';
import { eventsTabReducers } from './events-tab.reducer';
import { tagsTabReducers } from './tags-tab.reducer';
import { blogTabReducers } from './blog-tab.reducer';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
const _assetExplorerReducer = createReducer(
  initialAssetExplorerState,
  on(assetExplorerActions.treeStateChange, treeStateChange),
  on(assetExplorerActions.assetTreeDataRequestFailure, treeDataFailed),
  on(assetExplorerActions.treeSizeChange, treeSizeChange),
  ...infoTabReducers,
  ...tagsTabReducers,
  ...eventsTabReducers,
  ...blogTabReducers
);

export function treeStateChange(
  state: IAssetExplorerState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    AssetTreeState: alterAssetTreeState(state.AssetTreeState, payload),
  };
}

export function treeDataFailed(state: IAssetExplorerState, payload: Error) {
  return { ...state };
}

export function treeSizeChange(
  state: IAssetExplorerState,
  payload: { value: number }
) {
  return { ...state, LeftTraySize: payload.value };
}

export function assetExplorerReducer(state, action) {
  return _assetExplorerReducer(state, action);
}

export const selectDataExplorerState =
  createFeatureSelector<IAssetExplorerState>('assetexplorer');

export const selectAssetTreeState = createSelector(
  selectDataExplorerState,
  (state: IAssetExplorerState) => state?.AssetTreeState
);

export const selectAssetTreeDropdownNode = createSelector(
  selectAssetTreeState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectAssetTreeId = createSelector(
  selectAssetTreeDropdownNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetUniqueKey: data.UniqueKey, assetId: data.AssetId };
    }
    return null;
  }
);
