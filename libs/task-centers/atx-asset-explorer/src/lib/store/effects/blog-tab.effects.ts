/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  filter,
} from 'rxjs/operators';

import * as actions from '../actions/blog-tab.actions';
import { of, forkJoin, iif } from 'rxjs';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import {
  selectAssetTreeConfiguration,
  selectSelectedGlobalId,
  selectSelectedAssetId,
  selectSelectedAsset,
} from '../selectors/asset-explorer.selector';
import {
  isNil,
  IAWSFileUrl,
  IDiscussion,
  IDiscussionEntry,
  IDiscussionAttachment,
} from '@atonix/atx-core';
import {
  selectAutoCompleteKeywords,
  selectNextDisplayOrder,
  selectDiscussionEntryKeywords,
  selectDiscussionEntry,
  selectUploadingAttachments,
} from '../selectors/blog-tab.selector';

import { validateDiscussionEntry } from '../../service/asset-explorer.helpers';
import { ToastService } from '@atonix/shared/utils';
import {
  AssetFrameworkService,
  DiscussionsFrameworkService,
  ImagesFrameworkService,
  IssuesFrameworkService,
} from '@atonix/shared/api';

@Injectable()
export class BlogTabEffects {
  constructor(
    private actions$: Actions,
    private imagesFrameworkService: ImagesFrameworkService,
    private issuesFrameworkService: IssuesFrameworkService,
    private assetsFrameworkService: AssetFrameworkService,
    private discussionsFrameworkService: DiscussionsFrameworkService,
    private store: Store<IAssetExplorerState>,
    private toastService: ToastService
  ) {}

  routeLoadBlog$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadBlog),
      switchMap((action) =>
        forkJoin([
          this.discussionsFrameworkService.getDiscussionAssetType(action.asset),
          this.issuesFrameworkService.getOpenIssuesCount(action.asset),
        ])
      ),
      switchMap(([discussionAssetTypes, issuesCount]) => [
        actions.routeLoadBlogSuccess({ discussionAssetTypes, issuesCount }),
        actions.getNewDiscussionEntry(),
        actions.loadDiscussion({ showAutogenEntries: false, showedTopic: 1 }),
      ]),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(actions.routeLoadBlogFailure(error));
      })
    )
  );

  loadDiscussion$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadDiscussion),
      withLatestFrom(this.store.pipe(select(selectSelectedAssetId))),
      map(([action, asset]) => {
        return {
          asset,
          showAutogenEntries: action.showAutogenEntries,
          showedTopic: action.showedTopic,
        };
      }),
      switchMap((vals) =>
        this.discussionsFrameworkService
          .getDiscussionForAsset(
            vals.asset,
            true,
            vals.showAutogenEntries,
            vals.showedTopic
          )
          .pipe(
            map(
              (discussions) => actions.loadDiscussionSuccess({ discussions }),
              catchError((error) => of(actions.loadDiscussionFailure(error)))
            )
          )
      )
    )
  );

  getNewDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getNewDiscussionEntry),
      switchMap(() =>
        this.discussionsFrameworkService
          .createDiscussionEntry(null, null, null)
          .pipe(
            map(
              (discussionEntry) =>
                actions.getNewDiscussionEntrySuccess({ discussionEntry }),
              catchError((error) =>
                of(actions.getNewDiscussionEntryFailure(error))
              )
            )
          )
      )
    )
  );

  saveDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveDiscussionEntry),
      withLatestFrom(
        this.store.pipe(select(selectSelectedAssetId)),
        this.store.pipe(select(selectDiscussionEntry)),
        this.store.pipe(select(selectUploadingAttachments))
      ),
      map(([action, asset, entry, uploading]) => {
        const errorMessage = validateDiscussionEntry(entry, uploading);
        if (errorMessage) {
          this.toastService.openSnackBar(errorMessage, 'error');
        }

        return {
          asset,
          showAutogenEntries: action.showAutogenEntries,
          showedTopic: action.showedTopic,
          entryTopic: action.entryTopic,
          entry,
          errorMessage,
        };
      }),
      filter((vals) => isNil(vals.errorMessage)),
      switchMap((vals) =>
        this.discussionsFrameworkService
          .getDiscussionForAsset(
            vals.asset,
            true,
            vals.showAutogenEntries,
            vals.entryTopic
          )
          .pipe(
            map((discussions: IDiscussion[]) => {
              const newEntry = { ...vals.entry };
              if (discussions && discussions.length > 0) {
                newEntry.DiscussionID = discussions[0].DiscussionID;
              }

              return {
                entry: newEntry,
                showedTopic: vals.showedTopic,
                entryTopic: vals.entryTopic,
              };
            })
          )
      ),
      switchMap((vals) =>
        this.discussionsFrameworkService
          .saveDiscussionEntryWithAttachments(
            vals.entry,
            vals.entry.attachmentsToDelete,
            false
          )
          .pipe(
            map((savedDiscussionEntry: IDiscussionEntry) => {
              return {
                showedTopic: vals.showedTopic,
                entryTopic: vals.entryTopic,
                savedDiscussionEntry,
              };
            })
          )
      ),
      switchMap((vals) => {
        this.toastService.openSnackBar('New Entry has been added!', 'success');

        return [
          actions.saveDiscussionEntrySuccess({
            showedTopic: vals.showedTopic,
            entryTopic: vals.entryTopic,
            savedDiscussionEntry: vals.savedDiscussionEntry,
          }),
          actions.getNewDiscussionEntry(),
        ];
      }),
      catchError((error) => {
        this.toastService.openSnackBar('Error adding new Entry!', 'error');
        console.log(JSON.stringify(error));
        return of(actions.saveDiscussionEntryFailure(error));
      })
    )
  );

  viewOpenIssues$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.viewOpenIssues),
      withLatestFrom(this.store.pipe(select(selectSelectedGlobalId))),
      map(([action, globalId]) => {
        return globalId;
      }),
      filter((globalId) => !isNil(globalId)),
      switchMap((globalId) => [
        actions.viewOpenIssuesToAsset({ asset: globalId }),
      ])
    )
  );

  sendNotifications$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.sendNotifications),
      withLatestFrom(this.store.pipe(select(selectSelectedGlobalId))),
      map(([action, globalId]) => {
        return globalId;
      }),
      filter((globalId) => !isNil(globalId)),
      switchMap((globalId) => [
        actions.sendNotificationsToAsset({ asset: globalId }),
      ])
    )
  );

  subscribe$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.subscribe),
        withLatestFrom(this.store.pipe(select(selectSelectedAssetId))),
        map(([action, assetId]) => {
          return { assetId, discussionAssetType: action.discussionAssetType };
        }),
        filter((vals) => !isNil(vals.assetId)),
        switchMap((vals) =>
          this.assetsFrameworkService
            .addCurrentUserToAssetBlog(vals.assetId, vals.discussionAssetType)
            .pipe(
              map(
                (result) => [
                  this.toastService.openSnackBar(
                    'You have been subscribed to this blog.',
                    'success'
                  ),
                ],
                catchError((error) => [
                  this.toastService.openSnackBar(
                    'Unable to add subscription.\r\n' + error,
                    'error'
                  ),
                ])
              )
            )
        )
      ),
    { dispatch: false }
  );

  addAttachment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addAttachment),
      withLatestFrom(
        this.store.pipe(select(selectSelectedAssetId)),
        this.store.pipe(select(selectNextDisplayOrder))
      ),
      map(([action, assetId, displayOrder]) => {
        return {
          assetId,
          discussionID: action.discussionID,
          entryID: action.entryID,
          title: action.title,
          discussionType: action.discussionType,
          file: action.file,
          displayOrder,
        };
      }),
      switchMap((vals) =>
        this.discussionsFrameworkService
          .createDiscussionAttachment(
            vals.discussionID,
            vals.entryID,
            vals.title,
            vals.discussionType,
            vals.displayOrder
          )
          .pipe(
            map((attachment: IDiscussionAttachment) => {
              return {
                assetId: vals.assetId,
                title: vals.title,
                file: vals.file,
                attachment,
              };
            })
          )
      ),
      switchMap((vals) =>
        this.imagesFrameworkService
          .getFileUrl(null, vals.title, true, vals.file.type)
          .pipe(
            map((awsFile: IAWSFileUrl) => {
              return {
                assetId: vals.assetId,
                title: vals.title,
                file: vals.file,
                attachment: vals.attachment,
                awsFile,
              };
            })
          )
      ),
      switchMap((vals) =>
        this.imagesFrameworkService
          .uploadFile(vals.awsFile.Url, vals.file)
          .pipe(
            map((result: boolean) => {
              return {
                assetId: vals.assetId,
                title: vals.title,
                attachment: vals.attachment,
                awsFile: vals.awsFile,
              };
            })
          )
      ),
      switchMap((vals) =>
        this.imagesFrameworkService
          .saveFileInfo(vals.title, vals.assetId, vals.awsFile.ContentID, false)
          .pipe(
            map((saveAwsFile: IAWSFileUrl) => {
              return {
                attachment: vals.attachment,
                awsFile: vals.awsFile,
                saveAwsFile,
              };
            })
          )
      ),
      switchMap((vals) => {
        this.toastService.openSnackBar(
          'Attachment has been uploaded!',
          'success'
        );

        const attachment = vals.attachment as IDiscussionAttachment;
        attachment.complete = true;
        attachment.ContentID = vals.awsFile.ContentID;
        attachment.path = vals.saveAwsFile.Url;

        return [actions.addAttachmentSuccess({ attachment })];
      }),
      catchError((error) => {
        this.toastService.openSnackBar('Error uploading Attachment!', 'error');
        console.log(JSON.stringify(error));
        return of(actions.addAttachmentFailure(error));
      })
    )
  );

  searchTaggingKeywords$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.searchTaggingKeywords),
      withLatestFrom(this.store.pipe(select(selectSelectedAsset))),
      map(([action, asset]) => {
        return { asset, searchExpression: action.searchExpression };
      }),
      filter((vals) => !isNil(vals.asset) && !isNil(vals.searchExpression)),
      switchMap((vals) =>
        this.assetsFrameworkService
          .searchTaggingKeywords(vals.asset, vals.searchExpression)
          .pipe(
            map(
              (keywords) => actions.searchTaggingKeywordsSuccess({ keywords }),
              catchError((error) =>
                of(actions.searchTaggingKeywordsFailure(error))
              )
            )
          )
      )
    )
  );

  addKeyword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addKeyword),
      withLatestFrom(
        this.store.pipe(select(selectSelectedAsset)),
        this.store.pipe(select(selectDiscussionEntryKeywords)),
        this.store.pipe(select(selectAutoCompleteKeywords))
      ),
      map(([action, asset, keywords, autoCompleteKeywords]) => {
        const keyword =
          keywords?.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() === action.tag.toLocaleLowerCase()
          ) || null;
        const autoCompleteKeyword =
          autoCompleteKeywords?.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() === action.tag.toLocaleLowerCase()
          ) || null;

        return { asset, tag: action.tag, keyword, autoCompleteKeyword };
      }),
      filter((vals) => !isNil(vals.asset) && isNil(vals.keyword)),
      switchMap((vals) =>
        this.assetsFrameworkService
          .isReservedKeyword(vals.asset, vals.tag)
          .pipe(
            map((result: boolean) => {
              return {
                asset: vals.asset,
                tag: vals.tag,
                autoCompleteKeyword: vals.autoCompleteKeyword,
                isReservedKeyword: result,
              };
            })
          )
      ),
      switchMap((vals) => {
        if (!vals.isReservedKeyword) {
          if (vals.autoCompleteKeyword?.KeywordId > 0) {
            // Tag is already registered so save it directly together with the other tags
            return [
              actions.updateKeywords({ keyword: vals.autoCompleteKeyword }),
            ];
          }

          // This will register the tag then save with the other tags.
          return this.assetsFrameworkService
            .addTaggingKeyword(vals.asset, vals.tag)
            .pipe(
              map(
                (keywords) => actions.updateKeywords({ keyword: keywords[0] }),
                catchError((error) => of(actions.registerKeywordFailure(error)))
              )
            );
        }
        // This willl be thrown if the keyword is a reserved keyword
        return [actions.addKeywordFailure()];
      })
    )
  );
}
