/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  filter,
} from 'rxjs/operators';

import * as actions from '../actions/tags-tab.actions';
import { of } from 'rxjs';
import { IAssetExplorerState } from '../state/asset-explorer-state';

import * as tagsTabSelectors from '../selectors/tags-tab.selector';
import { isNil } from '@atonix/atx-core';
import {
  AuthorizationFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';

@Injectable()
export class TagsTabEffects {
  constructor(
    private actions$: Actions,
    private processDataModelService: ProcessDataFrameworkService,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private store: Store<IAssetExplorerState>
  ) {}

  routeLoadTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadTags),
      map((payload) => payload.asset),
      filter((asset) => {
        return !isNil(asset);
      }),
      switchMap((asset) =>
        this.authorizationFrameworkService.getTagEditorRights().pipe(
          switchMap((rights) => {
            if (rights.CanView) {
              return this.processDataModelService.getPDServers(asset).pipe(
                map((servers) => {
                  return { asset, rights, servers };
                })
              );
            } else {
              return of({ asset, rights, servers: [] });
            }
          })
        )
      ),
      map(
        (payload) => actions.routeLoadTagsSuccess(payload),
        catchError((error: unknown) =>
          of(actions.routeLoadTagsFailure({ asset: '' }))
        )
      )
    )
  );

  loadTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getTags),
      map((payload) => payload.server),
      switchMap((server) =>
        this.processDataModelService.getTagsForServer(server).pipe(
          map(
            (tags) => actions.getTagsSuccess({ tags, server }),
            catchError((error) => of(actions.getTagsFailure(error)))
          )
        )
      )
    )
  );

  discardTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.discardChanges),
      withLatestFrom(this.store.select(tagsTabSelectors.selectSelectedServer)),
      switchMap(([payload, server]) =>
        this.processDataModelService.getTagsForServer(server).pipe(
          map(
            (tags) => actions.discardChangesSuccess({ tags, server }),
            catchError((error) => of(actions.discardChangesFailure(error)))
          )
        )
      )
    )
  );

  validateTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.validateTags),
      withLatestFrom(
        this.store.select(tagsTabSelectors.selectSelectedServer),
        this.store.select(tagsTabSelectors.selectAllTags)
      ),
      switchMap(([payload, server, tags]) =>
        this.processDataModelService.validateTags(server, tags).pipe(
          map(
            (message) => actions.validateTagsSuccess({ message }),
            catchError((error) => of(actions.validateTagsFailure(error)))
          )
        )
      )
    )
  );

  saveTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveTags),
      withLatestFrom(
        this.store.select(tagsTabSelectors.selectSelectedServer),
        this.store.select(tagsTabSelectors.selectAllTags)
      ),
      switchMap(([payload, server, tags]) =>
        this.processDataModelService.saveTags(server, tags).pipe(
          map(
            (result) => actions.saveTagsSuccess({ result, server }),
            catchError((error) => of(actions.saveTagsFailure(error)))
          )
        )
      )
    )
  );
}
