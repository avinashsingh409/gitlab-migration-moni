/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */

import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  filter,
  concatMap,
  takeUntil,
} from 'rxjs/operators';
import * as actions from '../actions/events-tab.actions';
import { of } from 'rxjs';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import {
  selectSelectedAsset,
  selectSelectedGlobalId,
} from '../selectors/asset-explorer.selector';
import {
  selectEventAssetGlobalId,
  selectIncludeDescendents,
} from '../selectors/events-tab.selector';
import { ToastService } from '@atonix/shared/utils';
import { AssetExplorerConfigService } from '../../service/asset-explorer-config.service';
import { AssetExplorerConfig } from '../../model/asset-explorer.config';
import {
  PowerGenActions,
  PowerGenFeature,
} from '@atonix/shared/state/power-gen';
import { AssetsCoreService } from '@atonix/shared/api';

@Injectable()
export class EventTabEffects {
  constructor(
    private actions$: Actions,
    private assetsCoreService: AssetsCoreService,
    private store: Store<IAssetExplorerState>,
    private snackBarService: ToastService,
    @Inject(AssetExplorerConfigService) public config: AssetExplorerConfig
  ) {}
  routeLoadEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadEvents),
      map((payload) => payload.asset),
      filter((asset) => {
        return asset ? true : false;
      }),
      withLatestFrom(this.store.pipe(select(selectIncludeDescendents))),
      switchMap(([asset, includeDescendants]) => {
        return this.assetsCoreService
          .getEventsAssetExplorer(asset, includeDescendants)
          .pipe(
            map(
              (events) => actions.routeLoadEventsSuccess({ events }),
              catchError((error) =>
                of(actions.routeLoadEventsFailure({ error }))
              )
            )
          );
      }),
      switchMap((action) => [action, actions.getEventTypes()])
    )
  );

  routeLoadSwitcher$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reloadEvents, actions.includeDescendantsEvent),
      switchMap(() => {
        if (this.config.configType === 'AssetExplorer') {
          return of(actions.routeLoadEventsAssetExplorer());
        } else {
          return of(actions.routeLoadEventsPowerGen());
        }
      })
    )
  );

  routeLoadEventsPowerGen$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadEventsPowerGen),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.pipe(select(PowerGenFeature.getSelectedDashboardData)),
            this.store.pipe(select(selectSelectedAsset)),
            this.store.pipe(select(selectIncludeDescendents))
          )
        )
      ),
      switchMap(
        ([action, selectedDashboardData, asset, includeDescendants]) => {
          const globalId = selectedDashboardData.id.toString();
          return this.assetsCoreService
            .getEventsAssetExplorer(globalId, includeDescendants)
            .pipe(
              map(
                (events) => actions.reloadEventsSuccess({ events }),
                catchError((error) =>
                  of(actions.reloadEventsFailure({ error }))
                )
              ),
              takeUntil(this.actions$.pipe(ofType(actions.routeLoadEvents)))
            );
        }
      )
    )
  );

  routeLoadEventsAssetExplorer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadEventsAssetExplorer),
      concatMap((action) =>
        of(action).pipe(
          withLatestFrom(
            this.store.pipe(select(selectSelectedAsset)),
            this.store.pipe(select(selectIncludeDescendents))
          )
        )
      ),
      switchMap(([action, asset, includeDescendants]) =>
        this.assetsCoreService
          .getEventsAssetExplorer(asset, includeDescendants)
          .pipe(
            map(
              (events) => actions.reloadEventsSuccess({ events }),
              catchError((error) => of(actions.reloadEventsFailure({ error })))
            ),
            takeUntil(this.actions$.pipe(ofType(actions.routeLoadEvents)))
          )
      )
    )
  );

  getEventTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getEventTypes),
      switchMap(() =>
        this.assetsCoreService.getEventTypes().pipe(
          map(
            (eventTypes) => actions.getEventTypesSuccess({ eventTypes }),
            catchError((error) => of(actions.getEventTypesFailure(error)))
          )
        )
      )
    )
  );

  getEventHistory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getEventHistory),
      withLatestFrom(
        this.store.pipe(select(selectSelectedGlobalId)),
        this.store.pipe(select(selectEventAssetGlobalId))
      ),
      map(([action, assetGuid, eventAssetGuid]) => {
        return {
          eventID: action.eventID,
          asset: assetGuid || eventAssetGuid || null,
        };
      }),
      filter((vals) => {
        return vals.asset ? true : false;
      }),
      switchMap((vals) =>
        this.assetsCoreService.getEventHistory(vals.asset, vals.eventID).pipe(
          map(
            (events) => actions.getEventHistorySuccess({ events }),
            catchError((error) => of(actions.getEventHistoryFailure(error)))
          )
        )
      )
    )
  );

  validateEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.validateEvent),
      withLatestFrom(
        this.store.pipe(select(selectSelectedGlobalId)),
        this.store.pipe(select(selectEventAssetGlobalId))
      ),
      map(([action, assetGuid, eventAssetGuid]) => {
        return {
          event: action.event,
          asset: assetGuid || eventAssetGuid || null,
        };
      }),
      filter((vals) => {
        return vals.asset ? true : false;
      }),
      switchMap((vals) =>
        this.assetsCoreService.validateEvent(vals.asset, vals.event).pipe(
          map(
            (result) =>
              actions.validateEventSuccess({
                changedEventCount: result.changedEventCount,
                validationMessage: result.validationMessage,
                changedEvents: result.changedEvents,
              }),
            catchError((error) => of(actions.validateEventFailure({ error })))
          )
        )
      ),
      catchError((error) => of(actions.validateEventFailure({ error })))
    )
  );

  createNewEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveEvent),
      withLatestFrom(
        this.store.pipe(select(selectSelectedGlobalId)),
        this.store.pipe(select(selectEventAssetGlobalId))
      ),
      map(([action, assetGuid, eventAssetGuid]) => {
        return {
          event: action.event,
          asset: assetGuid || eventAssetGuid || null,
        };
      }),
      filter((vals) => {
        return vals.asset ? true : false;
      }),
      switchMap((vals) =>
        this.assetsCoreService.createEvent(vals.asset, vals.event).pipe(
          map(
            (result) => {
              this.snackBarService.openSnackBar(
                'New event saved successfully.',
                'success'
              );
              return actions.saveEventSuccess();
            },
            catchError((error) => {
              this.snackBarService.openSnackBar(error, 'error');
              return of(actions.saveEventFailure({ error }));
            })
          )
        )
      ),
      switchMap((action) => [action, actions.reloadEvents()])
    )
  );

  updateEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.changeEvent),
      withLatestFrom(
        this.store.pipe(select(selectSelectedGlobalId)),
        this.store.pipe(select(selectEventAssetGlobalId))
      ),
      map(([action, assetGuid, eventAssetGuid]) => {
        return {
          event: action.event,
          asset: assetGuid || eventAssetGuid || null,
        };
      }),
      filter((vals) => {
        return vals.asset ? true : false;
      }),
      switchMap((vals) =>
        this.assetsCoreService.updateEvent(vals.asset, vals.event).pipe(
          map(
            (result) => {
              this.snackBarService.openSnackBar(
                'Event updated successfully.',
                'success'
              );
              return actions.changeEventSuccess({ event: result[0] });
            },
            catchError((error) => {
              this.snackBarService.openSnackBar(error, 'error');
              return of(actions.changeEventFailure({ error }));
            })
          )
        )
      ),
      switchMap((action) => [action, actions.reloadEvents()])
    )
  );

  deleteEvent$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.deleteEvent),
      withLatestFrom(
        this.store.pipe(select(selectSelectedGlobalId)),
        this.store.pipe(select(selectEventAssetGlobalId))
      ),
      map(([action, assetGuid, eventAssetGuid]) => {
        return {
          eventID: action.eventID,
          asset: assetGuid || eventAssetGuid || null,
        };
      }),
      filter((vals) => {
        return vals.asset ? true : false;
      }),
      switchMap((vals) =>
        this.assetsCoreService.deleteEvent(vals.asset, vals.eventID).pipe(
          map(
            (result) => {
              this.snackBarService.openSnackBar(
                'Event deleted successfully.',
                'success'
              );
              return actions.deleteEventSuccess();
            },
            catchError((error) => {
              this.snackBarService.openSnackBar(error, 'error');
              return of(actions.deleteEventFailure({ error }));
            })
          )
        )
      ),
      switchMap((action) => [action, actions.reloadEvents()])
    )
  );
}
