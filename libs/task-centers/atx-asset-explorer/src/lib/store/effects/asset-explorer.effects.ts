/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect, OnInitEffects } from '@ngrx/effects';
import { Store, select, Action } from '@ngrx/store';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  tap,
  take,
} from 'rxjs/operators';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { NavActions } from '@atonix/atx-navigation';

import * as actions from '../actions/asset-explorer.actions';
import { of } from 'rxjs';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import {
  selectSelectedAsset,
  selectAssetTreeState,
  selectInfoTabState,
} from '../selectors/asset-explorer.selector';
import { isNil } from '@atonix/atx-core';
import { ToastService } from '@atonix/shared/utils';
import { AssetExplorerConfig } from '../../model/asset-explorer.config';
import { AssetExplorerConfigService } from '../../service/asset-explorer-config.service';

@Injectable()
export class AssetExplorerEffects {
  constructor(
    private actions$: Actions,
    private assetTreeModel: AssetTreeModel,
    private store: Store<IAssetExplorerState>,
    private toastService: ToastService,
    @Inject(AssetExplorerConfigService) public config: AssetExplorerConfig
  ) {}

  initializeAssetExplorer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.assetExplorerInitialize),
      map((n) => n.asset),
      withLatestFrom(this.store.select(selectSelectedAsset)),
      map(([oldAsset, newAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return newAsset || oldAsset;
      }),
      switchMap((asset) => [
        actions.treeStateChange(selectAsset(asset)),
        NavActions.taskCenterLoad({
          taskCenterID: 'asset-explorer',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: false,
          asset,
        }),
      ])
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.treeStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => actions.treeStateChange(n),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(actions.assetTreeDataRequestFailure(error))
              )
            )
          )
      )
    )
  );

  reflow$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 100);
        })
      ),
    { dispatch: false }
  );

  showInfoMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showInfoMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'information');
        })
      ),
    { dispatch: false }
  );

  showSuccessMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showSuccessMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'success');
        })
      ),
    { dispatch: false }
  );

  showWarningMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showWarningMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'warning');
        })
      ),
    { dispatch: false }
  );

  showErrorMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showErrorMessage),
        tap((n) => {
          this.toastService.openSnackBar(n.message, 'error');
        })
      ),
    { dispatch: false }
  );
}
