/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  filter,
} from 'rxjs/operators';

import * as actions from '../actions/info-tab.actions';
import { of } from 'rxjs';
import { IAssetExplorerState } from '../state/asset-explorer-state';
import { ModelService as AssetTreeModelService } from '@atonix/atx-asset-tree';
import {
  selectAssetInfo,
  selectTags,
  selectAutoCompleteKeywords,
} from '../selectors/info-tab.selector';
import {
  selectInfoTabState,
  selectSelectedNodeData,
} from '../selectors/asset-explorer.selector';
import { IAtxAssetTreeNode, isNil } from '@atonix/atx-core';
import { AssetFrameworkService } from '@atonix/shared/api';

@Injectable()
export class InfoTabEffects {
  constructor(
    private actions$: Actions,

    private assetTreeModelService: AssetTreeModelService,
    private assetFrameworkService: AssetFrameworkService,
    private store: Store<IAssetExplorerState>
  ) {}
  routeLoadInfo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.routeLoadInfo),
      withLatestFrom(this.store.pipe(select(selectInfoTabState))),
      map(([payload, infoTabState]) => {
        let asset = payload.asset;
        if (asset && infoTabState.entities[asset]) {
          asset = null;
        }
        return asset;
      }),
      filter((asset) => {
        return !isNil(asset);
      }),
      switchMap((asset) =>
        this.assetFrameworkService.getAssetAndAttributes(asset).pipe(
          map(
            (val) => actions.routeLoadInfoSuccess(val),
            catchError(() => of(actions.routeLoadInfoFailure({ asset: '' })))
          )
        )
      )
    )
  );

  getAssetClassTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getAssetClassTypes),
      withLatestFrom(
        this.store.pipe(select(selectAssetInfo)),
        this.store.pipe(select(selectSelectedNodeData))
      ),
      map(([action, assetInfo, node]) => {
        const parentAssetClassTypeID =
          assetInfo?.parent?.AssetClassTypeID.toString() || null;
        return { asset: action.asset, parentAssetClassTypeID, node };
      }),
      filter(
        (vals) => !isNil(vals.asset) && !isNil(vals.parentAssetClassTypeID)
      ),
      switchMap((vals) =>
        this.assetTreeModelService.getChildren(vals.node).pipe(
          map((treeNodes: IAtxAssetTreeNode[]) => {
            let childAssetClassTypeID: string[] = [];
            childAssetClassTypeID = treeNodes?.map((node) => {
              if (
                !childAssetClassTypeID.find(
                  (id) => id === node?.Asset?.AssetClassTypeID.toString()
                )
              ) {
                return node?.Asset?.AssetClassTypeID.toString();
              }
              return null;
            });

            return {
              asset: vals.asset,
              parentAssetClassTypeID: vals.parentAssetClassTypeID,
              childAssetClassTypeID,
            };
          })
        )
      ),
      switchMap((vals) =>
        this.assetFrameworkService
          .getAssetClassTypesForAsset(
            vals.asset,
            vals.parentAssetClassTypeID,
            vals.childAssetClassTypeID
          )
          .pipe(
            map(
              (assetClassTypes) =>
                actions.getAssetClassTypesSuccess({ assetClassTypes }),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(actions.getAssetClassTypesFailure(error))
              )
            )
          )
      )
    )
  );

  searchTaggingKeywords$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.searchTaggingKeywords),
      map((action) => {
        return {
          asset: action.asset,
          searchExpression: action.searchExpression,
        };
      }),
      filter((vals) => !isNil(vals.asset) && !isNil(vals.searchExpression)),
      switchMap((vals) =>
        this.assetFrameworkService
          .searchTaggingKeywords(vals.asset, vals.searchExpression)
          .pipe(
            map(
              (keywords) => actions.searchTaggingKeywordsSuccess({ keywords }),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(actions.searchTaggingKeywordsFailure(error))
              )
            )
          )
      )
    )
  );

  addKeyword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addKeyword),
      withLatestFrom(
        this.store.pipe(select(selectTags)),
        this.store.pipe(select(selectAutoCompleteKeywords))
      ),
      map(([action, keywords, autoCompleteKeywords]) => {
        const keyword =
          keywords.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() === action.tag.toLocaleLowerCase()
          ) || null;
        const autoCompleteKeyword =
          autoCompleteKeywords.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() === action.tag.toLocaleLowerCase()
          ) || null;

        return {
          asset: action.asset,
          tag: action.tag,
          keyword,
          autoCompleteKeyword,
        };
      }),
      filter((vals) => !isNil(vals.asset) && isNil(vals.keyword)),
      switchMap((vals) =>
        this.assetFrameworkService.isReservedKeyword(vals.asset, vals.tag).pipe(
          map((result: boolean) => {
            return {
              asset: vals.asset,
              tag: vals.tag,
              autoCompleteKeyword: vals.autoCompleteKeyword,
              isReservedKeyword: result,
            };
          })
        )
      ),
      switchMap((vals) => {
        if (!vals.isReservedKeyword) {
          if (vals.autoCompleteKeyword?.KeywordId > 0) {
            // Tag is already registered so save it directly together with the other tags
            return [actions.updateKeywords(vals.autoCompleteKeyword)];
          }

          // This will register the tag then save with the other tags.
          return this.assetFrameworkService
            .addTaggingKeyword(vals.asset, vals.tag)
            .pipe(
              map(
                (keywords) => actions.updateKeywords(keywords[0]),
                // eslint-disable-next-line rxjs/no-implicit-any-catch
                catchError((error) => of(actions.registerKeywordFailure(error)))
              )
            );
        }
        // This willl be thrown if the keyword is a reserved keyword
        return [actions.addKeywordFailure()];
      })
    )
  );
}
