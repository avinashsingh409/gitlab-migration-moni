/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITreePermissions } from '@atonix/atx-core';

export const assetExplorerInitialize = createAction(
  '[Asset Explorer] Initialize',
  props<{ asset?: string }>()
);

// Asset Tree
export const permissionsRequest = createAction(
  '[Asset Explorer] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[Asset Explorer] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[Asset Explorer] Permissions Request Failure',
  props<Error>()
);
export const treeStateChange = createAction(
  '[Asset Explorer] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[Asset Explorer] Tree Data Request Failure',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[Asset Explorer] Asset Tree Size Change',
  props<{ value: number }>()
);

export const routeLoadAttributes = createAction(
  '[Asset Explorer] Route Load Attributes',
  props<{ asset: string }>()
);
export const routeLoadLocation = createAction(
  '[Asset Explorer] Route Load Location',
  props<{ asset: string }>()
);

export const showInfoMessage = createAction(
  '[Asset Explorer] Show Info Message',
  props<{ message: string }>()
);
export const showSuccessMessage = createAction(
  '[Asset Explorer] Show Success Message',
  props<{ message: string }>()
);
export const showWarningMessage = createAction(
  '[Asset Explorer] Show Warning Message',
  props<{ message: string }>()
);
export const showErrorMessage = createAction(
  '[Asset Explorer] Show Error Message',
  props<{ message: string }>()
);
