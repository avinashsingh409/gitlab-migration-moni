/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import {
  IDiscussion,
  IDiscussionAssetMapType,
  IDiscussionAttachment,
  IDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';

export const routeLoadBlog = createAction(
  '[Asset Explorer - Blog Tab] Route Blog Info',
  props<{ asset: string }>()
);
export const routeLoadBlogSuccess = createAction(
  '[Asset Explorer - Blog Tab] Route Load Blog Success',
  props<{
    discussionAssetTypes: IDiscussionAssetMapType[];
    issuesCount: number;
  }>()
);
export const routeLoadBlogFailure = createAction(
  '[Asset Explorer - Blog Tab] Route Load Blog Failure',
  props<Error>()
);

export const loadDiscussion = createAction(
  '[Asset Explorer - Blog Tab] Load Discussion',
  props<{ showAutogenEntries: boolean; showedTopic: number }>()
);
export const loadDiscussionSuccess = createAction(
  '[Asset Explorer - Blog Tab] Load Discussion Success',
  props<{ discussions: IDiscussion[] }>()
);
export const loadDiscussionFailure = createAction(
  '[Asset Explorer - Blog Tab] Load Discussion Failure',
  props<Error>()
);

export const getNewDiscussionEntry = createAction(
  '[Asset Explorer - Blog Tab] Get New Discussion Entry'
);
export const getNewDiscussionEntrySuccess = createAction(
  '[Asset Explorer - Blog Tab] Get New Discussion Entry Success',
  props<{ discussionEntry: IDiscussionEntry }>()
);
export const getNewDiscussionEntryFailure = createAction(
  '[Asset Explorer - Blog Tab] Get New Discussion Entry Failure',
  props<Error>()
);

export const updateDiscussionEntryTitle = createAction(
  '[Asset Explorer - Blog Tab] Update Discussion Entry Title',
  props<{ title: string }>()
);

export const updateDiscussionEntryContent = createAction(
  '[Asset Explorer - Blog Tab] Update Discussion Entry Content',
  props<{ content: string }>()
);

export const saveDiscussionEntry = createAction(
  '[Asset Explorer - Blog Tab] Save Discussion Entry',
  props<{
    showAutogenEntries: boolean;
    showedTopic: number;
    entryTopic: number;
  }>()
);
export const saveDiscussionEntrySuccess = createAction(
  '[Asset Explorer - Blog Tab] Save Discussion Entry Success',
  props<{
    showedTopic: number;
    entryTopic: number;
    savedDiscussionEntry: IDiscussionEntry;
  }>()
);
export const saveDiscussionEntryFailure = createAction(
  '[Asset Explorer - Blog Tab] Save Discussion Entry Failure',
  props<Error>()
);

export const viewOpenIssues = createAction(
  '[Asset Explorer - Blog Tab] View Open Issues'
);
export const viewOpenIssuesToAsset = createAction(
  '[Asset Explorer - Blog Tab] View Open Issues To Asset',
  props<{ asset: string }>()
);

export const sendNotifications = createAction(
  '[Asset Explorer - Blog Tab] Send Notifications'
);
export const sendNotificationsToAsset = createAction(
  '[Asset Explorer - Blog Tab] Send Notifications To Asset',
  props<{ asset: string }>()
);

export const subscribe = createAction(
  '[Asset Explorer - Blog Tab] Subscribe',
  props<{ discussionAssetType: string }>()
);

// Attachments
export const addAttachment = createAction(
  '[Asset Explorer - Blog Tab] Add Attachment',
  props<{
    discussionID: string;
    entryID: string;
    title: string;
    discussionType: string;
    file: any;
  }>()
);
export const addAttachmentSuccess = createAction(
  '[Asset Explorer - Blog Tab] Add Attachment Success',
  props<{ attachment: IDiscussionAttachment }>()
);
export const addAttachmentFailure = createAction(
  '[Asset Explorer - Blog Tab] Add Attachment Failure',
  props<Error>()
);

// Search Keywords
export const searchTaggingKeywords = createAction(
  '[Asset Explorer - Blog Tab] Search Tagging Keywords',
  props<{ searchExpression: string }>()
);
export const searchTaggingKeywordsSuccess = createAction(
  '[Asset Explorer - Blog Tab] Search Tagging Keywords Success',
  props<{ keywords: ITaggingKeyword[] }>()
);
export const searchTaggingKeywordsFailure = createAction(
  '[Asset Explorer - Blog Tab] Search Tagging Keywords Failure',
  props<Error>()
);

// Keywords
export const addKeyword = createAction(
  '[Asset Explorer - Blog Tab] Add Keyword',
  props<{ tag: string }>()
);
// This action will be use when added tag is a reserved keyword
export const addKeywordFailure = createAction(
  '[Asset Explorer - Blog Tab] Keyword is not allowed'
);
export const registerKeywordFailure = createAction(
  '[Asset Explorer - Blog Tab] Register Tagging Keyword Failure',
  props<Error>()
);
export const updateKeywords = createAction(
  '[Asset Explorer - Blog Tab] Update Keywords',
  props<{ keyword: ITaggingKeyword }>()
);
