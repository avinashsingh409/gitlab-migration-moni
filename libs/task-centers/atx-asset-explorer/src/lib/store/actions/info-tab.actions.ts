/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { IAttribute } from '../../model/asset-info';
import {
  IAssetAndAttributes,
  IAssetClassType,
  ITaggingKeyword,
} from '@atonix/atx-core';

export const routeLoadInfo = createAction(
  '[Asset Explorer - Info Tab] Route Load Info',
  props<{ asset: string }>()
);
export const routeLoadInfoSuccess = createAction(
  '[Asset Explorer - Info Tab] Route Load Info Success',
  props<IAssetAndAttributes>()
);
export const routeLoadInfoFailure = createAction(
  '[Asset Explorer - Info Tab] Route Load Info Failure',
  props<{ asset: string }>()
);

// Asset Class Types
export const getAssetClassTypes = createAction(
  '[Asset Explorer - Info Tab] Get Asset Class Types',
  props<{ asset: string }>()
);
export const getAssetClassTypesSuccess = createAction(
  '[Asset Explorer - Info Tab] Get Asset Class Types Success',
  props<{ assetClassTypes: IAssetClassType[] }>()
);
export const getAssetClassTypesFailure = createAction(
  '[Asset Explorer - Info Tab] Get Asset Class Types Failure',
  props<Error>()
);

// Attributes
export const changeAttribute = createAction(
  '[Asset Explorer - Info Tab] Change Attribute',
  props<{ attribute: IAttribute }>()
);

// Search Keywords
export const searchTaggingKeywords = createAction(
  '[Asset Explorer - Info Tab] Search Tagging Keywords',
  props<{ asset: string; searchExpression: string }>()
);
export const searchTaggingKeywordsSuccess = createAction(
  '[Asset Explorer - Info Tab] Search Tagging Keywords Success',
  props<{ keywords: ITaggingKeyword[] }>()
);
export const searchTaggingKeywordsFailure = createAction(
  '[Asset Explorer - Info Tab] Search Tagging Keywords Failure',
  props<Error>()
);

// Keywords
export const addKeyword = createAction(
  '[Asset Explorer - Info Tab] Add Keyword',
  props<{ asset: string; tag: string }>()
);
// This action will be use when added tag is a reserved keyword
export const addKeywordFailure = createAction(
  '[Asset Explorer - Info Tab] Keyword is not allowed'
);
export const registerKeywordFailure = createAction(
  '[Asset Explorer - Info Tab] Register Tagging Keyword Failure',
  props<Error>()
);
export const updateKeywords = createAction(
  '[Asset Explorer - Info Tab] Update Keywords',
  props<ITaggingKeyword>()
);
export const updateKeywordsSuccess = createAction(
  '[Asset Explorer - Info Tab] Update Keywords Success',
  props<ITaggingKeyword>()
);
export const updateKeywordsFailure = createAction(
  '[Asset Explorer - Info Tab] Update Keywords Failure',
  props<Error>()
);

export const editAttribute = createAction(
  '[Asset Explorer - Info Tab] Edit Attribute',
  props<{ id: number }>()
);
export const favoriteAttribute = createAction(
  '[Asset Explorer - Info Tab] Favorite Attribute',
  props<{ id: number; favorite: boolean }>()
);
