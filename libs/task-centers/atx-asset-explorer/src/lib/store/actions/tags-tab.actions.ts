/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { IPDServer, IPDTag, ISecurityRights } from '@atonix/atx-core';

export const routeLoadTags = createAction(
  '[Asset Explorer - Tags Tab] Route Load Tags',
  props<{ asset: string }>()
);
export const routeLoadTagsSuccess = createAction(
  '[Asset Explorer - Tags Tab] Route Load Tags Success',
  props<{ asset: string; rights: ISecurityRights; servers: IPDServer[] }>()
);
export const routeLoadTagsFailure = createAction(
  '[Asset Explorer - Tags Tab] Route Load Tags Failure',
  props<{ asset: string }>()
);

export const getTags = createAction(
  '[Asset Explorer - Tags Tab] Get Tags',
  props<{ server: number }>()
);
export const getTagsSuccess = createAction(
  '[Asset Explorer - Tags Tab] Get Tags Success',
  props<{ tags: IPDTag[]; server: number }>()
);
export const getTagsFailure = createAction(
  '[Asset Explorer - Tags Tab] Get Tags Failure',
  props<Error>()
);

export const addTag = createAction('[Asset Explorer - Tags Tab] Add Tag');
export const validateTags = createAction(
  '[Asset Explorer - Tags Tab] Validate Tags'
);
export const validateTagsSuccess = createAction(
  '[Asset Explorer - Tags Tab] Validate Tags Success',
  props<{ message: string }>()
);
export const validateTagsFailure = createAction(
  '[Asset Explorer - Tags Tab] Validate Tags Failure',
  props<Error>()
);
export const saveTags = createAction('[Asset Explorer - Tags Tab] Save Tags');
export const saveTagsSuccess = createAction(
  '[Asset Explorer - Tags Tab] Save Tags Success',
  props<{ result: string | IPDTag[]; server: number }>()
);
export const saveTagsFailure = createAction(
  '[Asset Explorer - Tags Tab] Save Tags Failure',
  props<Error>()
);
export const discardChanges = createAction(
  '[Asset Explorer - Tags Tab] Discard Changes'
);
export const discardChangesSuccess = createAction(
  '[Asset Explorer - Tags Tab] Discard Changes Success',
  props<{ tags: IPDTag[]; server: number }>()
);
export const discardChangesFailure = createAction(
  '[Asset Explorer - Tags Tab] Discard Changes Failure',
  props<Error>()
);
export const changeTag = createAction(
  '[Asset Explorer - Tags Tab] Change Tag',
  props<{ tag: IPDTag }>()
);
export const showDeletedTagsChange = createAction(
  '[Asset Explorer - Tags Tab] Show Deleted Tags Change',
  props<{ newValue: boolean }>()
);
export const deleteTag = createAction(
  '[Asset Explorer - Tags Tab] Delete Tag',
  props<{ tagID: number }>()
);
export const pasteTags = createAction(
  '[Asset Explorer - Tags Tag] Paste Tags',
  props<{ tags: IPDTag[] }>()
);
