import { createAction, props } from '@ngrx/store';

import {
  IEvent,
  IEventChange,
  IEventType,
  IUserEventWithDescription,
} from '@atonix/atx-core';

export const routeLoadEvents = createAction(
  '[Asset Explorer - Event Tab] Route Load Events',
  props<{ asset: string }>()
);
export const routeLoadEventsSuccess = createAction(
  '[Asset Explorer - Event Tab] Route Load Events Success',
  props<{ events: IEvent[] }>()
);
export const routeLoadEventsFailure = createAction(
  '[Asset Explorer - Event Tab] Route Load Events Failure',
  props<{ error }>()
);
export const noop = createAction('[Asset Explorer - Event Tab] Noop');
export const routeLoadEventsPowerGen = createAction(
  '[Asset Explorer - Event Tab] PowerGen Route Load Events'
);

export const routeLoadEventsAssetExplorer = createAction(
  '[Asset Explorer - Event Tab] Route Load Events Asset Explorer'
);

export const clearEvents = createAction(
  '[Asset Explorer - Event Tab] Clear Events'
);

export const getEventTypes = createAction(
  '[Asset Explorer - Event Tab] Get Event Types'
);
export const getEventTypesSuccess = createAction(
  '[Asset Explorer - Event Tab] Get Event Types Success',
  props<{ eventTypes: IEventType[] }>()
);
export const getEventTypesFailure = createAction(
  '[Asset Explorer - Event Tab] Get Event Types Failure',
  props<{ error }>()
);

export const getEventHistory = createAction(
  '[Asset Explorer - EventTab] Get Event History',
  props<{ eventID: string }>()
);
export const getEventHistorySuccess = createAction(
  '[Asset Explorer - EventTab] Get Event History Success',
  props<{ events: IEvent[] }>()
);
export const getEventHistoryFailure = createAction(
  '[Asset Explorer - EventTab] Get Event History Failure',
  props<{ error }>()
);

export const changeEvent = createAction(
  '[Asset Explorer - Event Tab] Change Event',
  props<{ event: IUserEventWithDescription }>()
);
export const changeEventSuccess = createAction(
  '[Asset Explorer - Event Tab] Change Event Success',
  props<{ event: IEvent }>()
);
export const changeEventFailure = createAction(
  '[Asset Explorer - Event Tab] Change Event Failure',
  props<{ error }>()
);

export const deleteEvent = createAction(
  '[Asset Explorer - Event Tab] Delete Event',
  props<{ eventID: number }>()
);
export const deleteEventSuccess = createAction(
  '[Asset Explorer - Event Tab] Delete Event Success'
);
export const deleteEventFailure = createAction(
  '[Asset Explorer - Event Tab] Delete Event Failure',
  props<{ error }>()
);

export const eventChanged = createAction(
  '[Asset Explorer - Event Tab] Event Changed'
);

export const validateEvent = createAction(
  '[Asset Explorer - Event Tab] Validate Event',
  props<{ event: IUserEventWithDescription }>()
);
export const validateEventSuccess = createAction(
  '[Asset Explorer - Event Tab] Validate Event Success',
  props<{
    changedEventCount: number;
    validationMessage: string;
    changedEvents: IEventChange[];
  }>()
);
export const validateEventFailure = createAction(
  '[Asset Explorer - Event Tab] Validate Event Failure',
  props<{ error }>()
);

export const saveEvent = createAction(
  '[Asset Explorer - Event Tab] Save Event',
  props<{ event: IUserEventWithDescription }>()
);
export const saveEventSuccess = createAction(
  '[Asset Explorer - Event Tab] Save Event Success'
);
export const saveEventFailure = createAction(
  '[Asset Explorer - Event Tab] Save Event Failure',
  props<{ error }>()
);

export const reloadEvents = createAction(
  '[Asset Explorer - EventTab] Reload Events'
);
export const reloadEventsSuccess = createAction(
  '[Asset Explorer - EventTab] Reload Events Success',
  props<{ events: IEvent[] }>()
);
export const reloadEventsFailure = createAction(
  '[Asset Explorer - EventTab] Reload Events Failure',
  props<{ error }>()
);
export const eventsDialogOpened = createAction(
  '[Asset Explorer - EventTab] Event Dialog Opened'
);

export const validationDialogReset = createAction(
  '[Asset Explorer - Event Tab] Validation Dialog Closed'
);

export const editEventsDialogOpened = createAction(
  '[Asset Explorer - EventTab] Edit Event Dialog Opened'
);
export const includeDescendantsEvent = createAction(
  '[Asset Explorer - Event Tab] Include Descendants Clicked',
  props<{ value: boolean }>()
);
