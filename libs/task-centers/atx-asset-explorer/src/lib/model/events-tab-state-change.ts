export interface IEventsTabStateChange {
  event: 'NewEventsDialog';
  newValue?: string | boolean;
}
