import { IAssetClassType, ITaggingKeyword } from '@atonix/atx-core';

export interface IAssetUpdate {
  asset: string;
  setAssetAbbrev: string;
  setAssetDesc: string;
  setAssetClassType?: IAssetClassType;
  setKeywords?: ITaggingKeyword[];
}

export function createAssetUpdate(newValues?: Partial<IAssetUpdate>) {
  const result: IAssetUpdate = {
    ...{
      asset: null,
      setAssetAbbrev: null,
      setAssetDesc: null,
      setAssetClassType: null,
      setKeywords: null,
    },
    ...newValues,
  };
  return result;
}
