import {
  IAsset,
  IAssetAttribute,
  IAssetClassType,
  ITaggingKeyword,
} from '@atonix/atx-core';

export interface IAssetInfoState {
  id: string;
  asset: IAsset;
  parent: IAsset;
  assetClassTypesLoading: boolean;
  assetClassTypes: IAssetClassType[];
  attributes: IAttribute[];
  tags: ITaggingKeyword[];
  autoCompleteLoading: boolean;
  autoCompleteKeywords: ITaggingKeyword[];
}

export interface IAttribute {
  Id: number;
  AssetAttribute: IAssetAttribute;
  Favorite: boolean;
  DisplayOrder: number;
  Attribute: string;
  Value: string;
  Units: string;
  Category: string;
}
