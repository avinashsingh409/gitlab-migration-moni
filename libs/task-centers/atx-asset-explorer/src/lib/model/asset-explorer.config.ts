export type AssetExplorerConfigType = 'PowerGen' | 'AssetExplorer';

export interface AssetExplorerConfig {
  configType: AssetExplorerConfigType;
}
