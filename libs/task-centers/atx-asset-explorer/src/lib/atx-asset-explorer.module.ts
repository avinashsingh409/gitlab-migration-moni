import { ModuleWithProviders, NgModule } from '@angular/core';
import { AssetExplorerComponent } from './component/asset-explorer/asset-explorer.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AssetExplorerEffects } from './store/effects/asset-explorer.effects';
import { assetExplorerReducer } from './store/reducers/asset-explorer.reducer';
import { InfoTabFacade } from './store/facade/info-tab.facade';
import { CommonModule } from '@angular/common';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { AgGridModule } from '@ag-grid-community/angular';
import {
  AssetExplorerRoutingModule,
  AppRoutingComponents,
} from './atx-asset-explorer-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfoTabEffects } from './store/effects/info-tab.effects';
import { TagsTabEffects } from './store/effects/tags-tab.effects';
import { EventTabEffects } from './store/effects/events-tab.effects';
import { AssetExplorerFacade } from './store/facade/asset-explorer.facade';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TagsTabComponent } from './component/tags-tab/tags-tab.component';
import { NoAssetComponent } from './component/no-asset/no-asset.component';
import { AttachmentsTabComponent } from './component/attachments-tab/attachments-tab.component';
import { BlogTabComponent } from './component/blog-tab/blog-tab.component';
import { LocationTabComponent } from './component/location-tab/location-tab.component';
import { EventsTabComponent } from './component/events-tab/events-tab.component';
import { NewEventsDialogComponent } from './component/events-tab/new-events-dialog/new-events-dialog.component';
import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { DragDropDirective } from './component/blog-tab/drag-drop';
import { DeleteColumnComponent } from './component/delete-column/delete-column.component';
import { EventsTabFacade } from './store/facade/events-tab.facade';
import { BlogTabFacade } from './store/facade/blog-tab.facade';
import { BlogTabEffects } from './store/effects/blog-tab.effects';
import { EditColumnComponent } from './component/edit-column/edit-column.component';
import { FavoriteColumnComponent } from './component/favorite-column/favorite-column.component';
import { InitialRouteFacade } from './service/initial-route.facade';
import { EventActionComponent } from './component/events-tab/event-action/event-action.component';
import { EventHistoryDialogComponent } from './component/events-tab/event-history-dialog/event-history-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { AssetExplorerConfig } from './model/asset-explorer.config';
import { AssetExplorerConfigService } from './service/asset-explorer-config.service';
import { EventValidationDialogComponent } from './component/events-tab/event-validation-dialog/event-validation-dialog.component';
import { ToastService } from '@atonix/shared/utils';
import { SharedApiModule } from '@atonix/shared/api';

@NgModule({
  declarations: [
    AssetExplorerComponent,
    AppRoutingComponents,
    TagsTabComponent,
    NoAssetComponent,
    AttachmentsTabComponent,
    BlogTabComponent,
    LocationTabComponent,
    EventsTabComponent,
    NewEventsDialogComponent,
    DragDropDirective,
    DeleteColumnComponent,
    EditColumnComponent,
    FavoriteColumnComponent,
    EventActionComponent,
    EventHistoryDialogComponent,
    EventValidationDialogComponent,
  ],
  imports: [
    CommonModule,
    AtxMaterialModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule,
    AssetTreeModule,
    SharedApiModule,
    FormsModule,
    ReactiveFormsModule,
    AssetExplorerRoutingModule,
    AgGridModule.withComponents([]),
    StoreModule.forFeature('assetexplorer', assetExplorerReducer),
    EffectsModule.forFeature([
      AssetExplorerEffects,
      InfoTabEffects,
      BlogTabEffects,
      TagsTabEffects,
      EventTabEffects,
    ]),
  ],
  providers: [
    InfoTabFacade,
    BlogTabFacade,
    EventsTabFacade,
    InitialRouteFacade,
    AssetExplorerFacade,
    ToastService,
    { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' },
  ],
  exports: [AssetExplorerComponent],
})
export class AssetExplorerModule {
  static forRoot(
    config: AssetExplorerConfig
  ): ModuleWithProviders<AssetExplorerModule> {
    return {
      ngModule: AssetExplorerModule,
      providers: [
        InfoTabFacade,
        BlogTabFacade,
        EventsTabFacade,

        {
          provide: AssetExplorerConfigService,
          useValue: config,
        },
        { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' },
      ],
    };
  }
}
