import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { getUniqueIdFromRoute, NavFacade } from '@atonix/atx-navigation';
import { Subject, BehaviorSubject } from 'rxjs';
import { map, distinctUntilChanged, take } from 'rxjs/operators';
import { AssetExplorerFacade } from '../store/facade/asset-explorer.facade';

export interface InitialRouteState {
  isLoggedIn: boolean;
  initialized: boolean;
  defaultNode: string;
}

let _state: InitialRouteState = {
  isLoggedIn: undefined,
  initialized: false,
  defaultNode: '',
};

@Injectable()
export class InitialRouteFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<InitialRouteState>(_state);
  private state$ = this.store.asObservable();

  isLoggedIn$ = this.state$.pipe(
    map((state) => state.isLoggedIn),
    distinctUntilChanged()
  );

  initialized$ = this.state$.pipe(
    map((state) => state.initialized),
    distinctUntilChanged()
  );

  setDefaultNode(
    defaultNode: string,
    queryParamMap: ParamMap,
    router: Router,
    assetExplorerFacade: AssetExplorerFacade
  ) {
    const id = getUniqueIdFromRoute(queryParamMap);
    if (!id) {
      this.state$.pipe(take(1)).subscribe((currentState) => {
        if (currentState.initialized === false && currentState.isLoggedIn) {
          this.updateState({ ..._state, initialized: true, defaultNode });
          assetExplorerFacade.initializeAssetExplorer(defaultNode);
        } else {
          this.updateState({ ..._state, defaultNode });
        }
      });
    }
  }
  setLoggedIn(
    isLoggedIn: boolean,
    queryParamMap: ParamMap,
    router: Router,
    assetExplorerFacade: AssetExplorerFacade
  ) {
    if (isLoggedIn === false) {
      this.updateState({
        ..._state,
        isLoggedIn,
        initialized: false,
        defaultNode: '',
      });
    } else {
      this.state$.pipe(take(1)).subscribe((currentState) => {
        if (
          currentState.defaultNode !== '' &&
          currentState.initialized === false
        ) {
          const id = getUniqueIdFromRoute(queryParamMap);
          if (!id) {
            assetExplorerFacade.initializeAssetExplorer(
              currentState.defaultNode
            );
            this.updateState({ ..._state, isLoggedIn, initialized: true });
          } else {
            this.updateState({ ..._state, isLoggedIn, initialized: true });
          }
        } else {
          this.updateState({ ..._state, isLoggedIn });
        }
      });
    }
  }

  private updateState(state: InitialRouteState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
