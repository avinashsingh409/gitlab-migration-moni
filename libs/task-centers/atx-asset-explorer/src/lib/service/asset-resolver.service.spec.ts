import { TestBed } from '@angular/core/testing';
import { AssetResolverService } from './asset-resolver.service';
import { InfoTabFacade } from '../store/facade/info-tab.facade';
import { createMock } from '@testing-library/angular/jest-utils';

describe('AssetResolverService', () => {
  let service: AssetResolverService;
  let infoTabFacade: InfoTabFacade;
  beforeEach(() => {
    infoTabFacade = createMock(InfoTabFacade);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: InfoTabFacade,
          useValue: infoTabFacade,
        },
      ],
    });
    service = TestBed.inject(AssetResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
