import { IEventsTabStateChange } from '../model/events-tab-state-change';

export function newEventsDialog(newVal?: boolean) {
  const result: IEventsTabStateChange = {
    event: 'NewEventsDialog',
    newValue: newVal,
  };
  return result;
}
