import { TestBed } from '@angular/core/testing';
import { createMock } from '@testing-library/angular/jest-utils';
import { BlogTabFacade } from '../store/facade/blog-tab.facade';
import { BlogResolverService } from './blog-resolver.service';

describe('BlogResolverService', () => {
  let service: BlogResolverService;
  let blogTabFacade: BlogTabFacade;
  beforeEach(() => {
    blogTabFacade = createMock(BlogTabFacade);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: BlogTabFacade,
          useValue: blogTabFacade,
        },
      ],
    });
    service = TestBed.inject(BlogResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
