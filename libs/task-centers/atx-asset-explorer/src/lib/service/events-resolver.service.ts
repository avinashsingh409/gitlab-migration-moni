import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { getAssetExplorerRoute } from '@atonix/atx-navigation';
import { EventsTabFacade } from '../store/facade/events-tab.facade';

@Injectable({
  providedIn: 'root',
})
export class EventsResolverService implements Resolve<any> {
  constructor(private facade: EventsTabFacade) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const id = getAssetExplorerRoute(route.parent);
    if (id) {
      this.facade.routeLoadEvents(id);
    }
    return null;
  }
}
