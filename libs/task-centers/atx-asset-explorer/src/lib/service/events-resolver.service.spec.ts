import { TestBed } from '@angular/core/testing';
import { EventsResolverService } from './events-resolver.service';
import { EventsTabFacade } from '../store/facade/events-tab.facade';
import { createMock } from '@testing-library/angular/jest-utils';

describe('EventsResolverService', () => {
  let service: EventsResolverService;
  let eventsTabFacade: EventsTabFacade;
  beforeEach(() => {
    eventsTabFacade = createMock(EventsTabFacade);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: EventsTabFacade,
          useValue: eventsTabFacade,
        },
      ],
    });
    service = TestBed.inject(EventsResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
