import { InjectionToken } from '@angular/core';
import { AssetExplorerConfig } from '../model/asset-explorer.config';

/**
 *  InjectionToken allows us to import a config object, provided from an App
 */
export const AssetExplorerConfigService =
  new InjectionToken<AssetExplorerConfig>('AssetExplorerConfig');
