import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { getAssetExplorerRoute } from '@atonix/atx-navigation';
import { InfoTabFacade } from '../store/facade/info-tab.facade';

@Injectable({
  providedIn: 'root',
})
export class AssetResolverService implements Resolve<any> {
  constructor(private facade: InfoTabFacade) {}

  resolve(route: ActivatedRouteSnapshot): any {
    const id = getAssetExplorerRoute(route.parent);
    if (id) {
      this.facade.routeLoadInfo(id);
    }
    return null;
  }
}
