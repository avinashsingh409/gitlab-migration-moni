import { Injectable, OnDestroy } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { getAssetExplorerRoute } from '@atonix/atx-navigation';
import { BlogTabFacade } from '../store/facade/blog-tab.facade';

@Injectable({
  providedIn: 'root',
})
export class BlogResolverService implements Resolve<any>, OnDestroy {
  constructor(private facade: BlogTabFacade) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const id = getAssetExplorerRoute(route.parent);
    if (id) {
      return this.facade.routeLoadBlog(id);
    }
    return null;
  }

  ngOnDestroy() {
    console.log('yup');
  }
}
