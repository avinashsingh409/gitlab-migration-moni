import { TestBed } from '@angular/core/testing';

import { PDServerResolverService } from './pdserver-resolver.service';
import { TagsTabFacade } from '../store/facade/tags-tab.facade';
import { createMock } from '@testing-library/angular/jest-utils';

describe('PDServerResolverService', () => {
  let service: PDServerResolverService;
  let tagsTabFacade: TagsTabFacade;

  beforeEach(() => {
    tagsTabFacade = createMock(TagsTabFacade);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: TagsTabFacade,
          useValue: tagsTabFacade,
        },
      ],
    });
    service = TestBed.inject(PDServerResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
