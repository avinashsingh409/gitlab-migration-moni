import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { getAssetExplorerRoute } from '@atonix/atx-navigation';
import { TagsTabFacade } from '../store/facade/tags-tab.facade';

@Injectable({
  providedIn: 'root',
})
export class PDServerResolverService implements Resolve<any> {
  constructor(private facade: TagsTabFacade) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const id = getAssetExplorerRoute(route.parent);
    if (id) {
      this.facade.routeLoadTags(id);
    }
    return null;
  }
}
