import moment from 'moment';
import {
  IAssetAttribute,
  IDiscussion,
  IDiscussionAttachment,
  IDiscussionEntry,
} from '@atonix/atx-core';

// This will get the value of an attribute depending on its type format
export function getValueFromAttribute(assetAttribute: IAssetAttribute) {
  let result = null;
  if (assetAttribute?.AttributeType?.DisplayFormat) {
    switch (assetAttribute?.AttributeType?.DisplayFormat) {
      case 'int':
        result = assetAttribute.Attribute_int;
        break;
      case 'float':
        result = assetAttribute.Attribute_float;
        break;
      case 'date':
        result = assetAttribute.Attribute_date;
        break;
      default:
        result = assetAttribute.Attribute_string;
        break;
    }
  }
  return result;
}

export function validateDiscussionEntry(
  entry: IDiscussionEntry,
  uploadingAttachment: boolean
) {
  let errMsg: string = null;
  if (!entry) {
    errMsg = 'No entry is defined.';
  } else if (!entry.Title || entry.Title === 'null' || entry.Title === '') {
    errMsg = 'New entry must have a title.';
  } else if (!entry.DiscussionID) {
    errMsg = 'No Discussion Defined.';
  } else if (!entry.Content || entry.Content === 'null' || entry.Title === '') {
    errMsg = 'New entry must have content.';
  } else if (uploadingAttachment) {
    errMsg = 'Attachments are still uploading.';
  }

  return errMsg;
}

export function processEntry(entry: IDiscussionEntry) {
  const newEntry = { ...entry } as IDiscussionEntry;

  newEntry.CreateDateZ = new Date(entry.CreateDateZ as any);
  newEntry.ChangeDateZ = new Date(entry.ChangeDateZ as any);
  for (let attachment of newEntry.Attachments) {
    // Should add getting of path here
    const attach = { ...attachment } as IDiscussionAttachment;
    attach.CreateDateZ = new Date(attachment.CreateDateZ as any);
    attach.ChangeDateZ = new Date(attachment.ChangeDateZ as any);
    attach.complete = true;

    attachment = attach;
  }

  newEntry.Attachments = [...newEntry.Attachments].sort((a, b) => {
    return a.DisplayOrder - b.DisplayOrder;
  });
  newEntry.editing = false;
  newEntry.saving = false;

  return newEntry;
}

export function processDiscussion(newDiscussion: IDiscussion[]) {
  if (newDiscussion) {
    const discussion = { ...newDiscussion[0] } as IDiscussion;
    discussion.CreateDateZ = moment(
      newDiscussion[0].CreateDateZ as any
    ).toDate();
    discussion.ChangeDateZ = moment(
      newDiscussion[0].ChangeDateZ as any
    ).toDate();

    for (const entry of discussion.Entries) {
      processEntry(entry);
    }

    discussion.Entries = [...discussion.Entries].sort((a, b) => {
      return (
        new Date(b.ChangeDateZ).getTime() - new Date(a.ChangeDateZ).getTime()
      );
    });

    if (discussion.Entries.length > 5) {
      discussion.AllEntries = discussion.Entries;
      discussion.Entries = discussion.Entries.slice(0, 5);
    }

    discussion.DiscussionType =
      newDiscussion.length > 1 ? 0 : newDiscussion[0].DiscussionType;

    return discussion;
  }
}
