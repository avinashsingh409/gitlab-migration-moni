import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAttribute } from '../../model/asset-info';
import { InfoTabFacade } from '../../store/facade/info-tab.facade';

@Component({
  selector: 'atx-favorite-column',
  templateUrl: './favorite-column.component.html',
  styleUrls: ['./favorite-column.component.scss'],
})
export class FavoriteColumnComponent implements ICellRendererAngularComp {
  public attribute: IAttribute;

  constructor(private facade: InfoTabFacade) {}

  agInit(params: any) {
    this.attribute = params.node.data;
  }

  favorite(value: boolean) {
    this.facade.favoriteAttribute(this.attribute.Id, value);
  }

  refresh(params: any) {
    this.attribute = params?.node?.data ?? this.attribute;
    return true;
  }
}
