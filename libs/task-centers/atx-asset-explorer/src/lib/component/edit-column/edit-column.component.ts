import { Component, OnInit } from '@angular/core';
import { InfoTabFacade } from '../../store/facade/info-tab.facade';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAttribute } from '../../model/asset-info';

@Component({
  selector: 'atx-edit-column',
  templateUrl: './edit-column.component.html',
  styleUrls: ['./edit-column.component.scss'],
})
export class EditColumnComponent implements ICellRendererAngularComp {
  public attribute: IAttribute;

  constructor(private facade: InfoTabFacade) {}

  agInit(params: any) {
    this.attribute = params.node.data;
  }

  edit() {
    this.facade.editAttribute(this.attribute.Id);
  }

  refresh() {
    return false;
  }
}
