import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditColumnComponent } from './edit-column.component';
import { InfoTabFacade } from '../../store/facade/info-tab.facade';
import { createMock } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('EditColumnComponent', () => {
  let component: EditColumnComponent;
  let fixture: ComponentFixture<EditColumnComponent>;
  let infoTabFacade: InfoTabFacade;

  beforeEach(waitForAsync(() => {
    infoTabFacade = createMock(InfoTabFacade);

    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [EditColumnComponent],
      providers: [
        { provide: InfoTabFacade, useValue: infoTabFacade },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
