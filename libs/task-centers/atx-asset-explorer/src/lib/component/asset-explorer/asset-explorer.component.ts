/* eslint-disable ngrx/no-typed-global-store */

import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  AfterViewInit,
  Inject,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil, withLatestFrom } from 'rxjs/operators';
import {
  ITreeStateChange,
  ITreeConfiguration,
  ISelectAsset,
  selectAsset,
  ModelService,
  IAssetData,
} from '@atonix/atx-asset-tree';
import { AssetExplorerFacade } from '../../store/facade/asset-explorer.facade';
import { ITab } from '../../model/tab';
import { Router, ActivatedRoute } from '@angular/router';
import { getAssetExplorerRoute, NavFacade } from '@atonix/atx-navigation';
import { InitialRouteFacade } from '../../service/initial-route.facade';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AssetExplorerConfig } from '../../model/asset-explorer.config';
import { AssetExplorerConfigService } from '../../service/asset-explorer-config.service';
import {
  PowerGenActions,
  PowerGenFeature,
} from '@atonix/shared/state/power-gen';
import { Store } from '@ngrx/store';
import { EventsTabFacade } from '../../store/facade/events-tab.facade';
import { IDashboardData } from '@atonix/shared/api';

@Component({
  selector: 'atx-asset-explorer',
  templateUrl: './asset-explorer.component.html',
  styleUrls: ['./asset-explorer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssetExplorerComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public asset$: Observable<string>;
  public isAssetSelected$: Observable<boolean>;
  public nodeAbbrev$: Observable<string>;
  public tabs$: Observable<ITab[]>;
  public showTimeSlider$: Observable<boolean>;
  public assetTreeSelected$: Observable<boolean>;
  private isLoggedIn$: Observable<boolean>;
  public leftTrayMode$: Observable<'over' | 'push' | 'side'>;
  public leftTraySize$: Observable<number>;
  public unsubscribe$ = new Subject<void>();
  dashboardData$: Observable<IDashboardData | null>;
  selectedValue = null;

  constructor(
    private navFacade: NavFacade,
    private assetExplorerFacade: AssetExplorerFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modelService: ModelService,
    private initialRouteFacade: InitialRouteFacade,
    private authFacade: AuthFacade,
    private eventsTabFacade: EventsTabFacade,
    private store: Store<any>,
    @Inject(AssetExplorerConfigService) public config: AssetExplorerConfig
  ) {
    this.asset$ = assetExplorerFacade.selectedAsset$;
    this.isAssetSelected$ = assetExplorerFacade.isAssetSelected$;
    this.assetTreeConfiguration$ = assetExplorerFacade.assetTreeConfiguration$;

    if (config.configType === 'AssetExplorer') {
      this.nodeAbbrev$ = assetExplorerFacade.selectedNodeAbbrev$;
      this.tabs$ = assetExplorerFacade.tabs$;
      this.isLoggedIn$ = authFacade.isLoggedIn$;
      this.showTimeSlider$ = navFacade.showTimeSlider$;
      this.assetTreeSelected$ = navFacade.assetTreeSelected$;
      this.leftTrayMode$ = assetExplorerFacade.leftTrayMode$;
      this.leftTraySize$ = assetExplorerFacade.leftTraySize$;

      activatedRoute.params
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((value) => {
          if (value.id) {
            const change: ITreeStateChange = selectAsset(value.id, false);
            this.assetExplorerFacade.treeStateChange(change);
          }
        });

      this.asset$.pipe(takeUntil(this.unsubscribe$)).subscribe((asset) => {
        if (asset) {
          this.navigateToSelectedAsset(asset);
        }
      });
    } else if (config.configType === 'PowerGen') {
      this.dashboardData$ = store.select(
        PowerGenFeature.getSelectedDashboardData
      );

      this.dashboardData$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((data) => {
          if (data) {
            modelService
              .getAssetInfo([data.id.toString()])
              .pipe(take(1))
              // eslint-disable-next-line rxjs/no-nested-subscribe
              .subscribe((val: IAssetData) => {
                this.eventsTabFacade.routeLoadEvents(
                  val.assetAttributes.Asset.GlobalId
                );
              });
            this.selectedValue = data.id;
          }
        });
    }
  }

  ngAfterViewInit() {
    if (this.config.configType === 'AssetExplorer') {
      const result = getAssetExplorerRoute(this.activatedRoute.snapshot);
      this.assetExplorerFacade.initializeAssetExplorer(result);
    }
  }

  ngOnInit() {
    if (this.config.configType === 'AssetExplorer') {
      this.isLoggedIn$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((isLoggedIn) => {
          this.initialRouteFacade.setLoggedIn(
            isLoggedIn,
            this.activatedRoute.snapshot.queryParamMap,
            this.router,
            this.assetExplorerFacade
          );
        });
      this.modelService.defaultNode$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((defaultNode) => {
          if (defaultNode !== '') {
            this.initialRouteFacade.setDefaultNode(
              defaultNode,
              this.activatedRoute.snapshot.queryParamMap,
              this.router,
              this.assetExplorerFacade
            );
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  // This is the thing that actually makes changes to the asset tree state.  We handle what we
  // need to handle and then call the default behavior.
  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.assetExplorerFacade.treeStateChange(change);
  }

  public onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.assetExplorerFacade.treeSizeChange(newSize);
    }
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  public navigateToSelectedAsset(asset: string) {
    if (this.config.configType === 'AssetExplorer') {
      const navRoutes: string[] = [asset];

      // This will add the parent path at the beginning of the array
      const parentRoute = this.activatedRoute.snapshot.parent;
      if (parentRoute && parentRoute.url.length > 0) {
        navRoutes.unshift('/' + parentRoute.url[0].path);
      }

      // This will add the child path at the end of the array
      const childRoutes = this.activatedRoute.snapshot.children;
      if (childRoutes && childRoutes.length > 0) {
        navRoutes.push(childRoutes[childRoutes.length - 1].url[0].path);
      }

      this.router.navigate(navRoutes, { relativeTo: this.activatedRoute });
    }
  }

  public selectAsset(assetID: number) {
    if (this.config.configType === 'PowerGen') {
      this.eventsTabFacade.clearEvents();
      this.store.dispatch(PowerGenActions.selectAsset({ assetID }));
    }
  }
}
