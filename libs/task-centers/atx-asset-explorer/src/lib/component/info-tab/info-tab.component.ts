import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AssetExplorerFacade } from '../../store/facade/asset-explorer.facade';
import { takeUntil } from 'rxjs/operators';
import { IAssetInfoState, IAttribute } from '../../model/asset-info';
import { UntypedFormControl, Validators } from '@angular/forms';
import { InfoTabFacade } from '../../store/facade/info-tab.facade';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import {
  GridApi,
  Module,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  ClientSideRowModelModule,
  SetFilterModule,
  GridOptions,
  GridReadyEvent,
} from '@ag-grid-enterprise/all-modules';
import { IAssetClassType, ITaggingKeyword } from '@atonix/atx-core';
import { FavoriteColumnComponent } from '../favorite-column/favorite-column.component';
import { EditColumnComponent } from '../edit-column/edit-column.component';

@Component({
  selector: 'atx-info-tab',
  templateUrl: './info-tab.component.html',
  styleUrls: ['./info-tab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoTabComponent implements OnInit, OnDestroy {
  public asset$: Observable<string>;
  public theme$: Observable<string>;
  public assetInfo$: Observable<IAssetInfoState>;
  public assetClassTypesLoading$: Observable<boolean>;
  public assetClassTypes$: Observable<IAssetClassType[]>;
  public attributes$: Observable<IAttribute[]>;
  public autoCompleteLoading$: Observable<boolean>;
  public autoCompleteKeywords$: Observable<ITaggingKeyword[]>;
  public canChangeType$: Observable<boolean>;
  public unsubscribe$ = new Subject<void>();

  // This will be used to set the row data if the data returns before the grid is initialized
  public rowData: IAttribute[];

  faSpinner = faSpinner;

  selectedAsset: string = null;
  name = new UntypedFormControl(null, [Validators.required]);
  attributeName = new UntypedFormControl({ value: null, disabled: true });
  descriptiveName = new UntypedFormControl(null, [Validators.required]);
  assetType = new UntypedFormControl({ value: null, disabled: true }, [
    Validators.required,
  ]);
  parent: string = null;
  tag = new UntypedFormControl(null);

  gridApi: GridApi;

  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
  ];

  gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    rowSelection: 'single',
    stopEditingWhenCellsLoseFocus: true,
    suppressCellFocus: true,
    suppressRowClickSelection: true,
    components: {
      editButton: EditColumnComponent,
      favoriteButton: FavoriteColumnComponent,
    },
    defaultColDef: {
      resizable: true,
      editable: false,
    },
    columnDefs: [
      {
        headerName: 'Edit',
        colId: 'editButton',
        cellRenderer: 'editButton',
        width: 90,
        sortable: false,
      },
      {
        headerName: 'Favorite',
        colId: 'Favorite',
        cellRenderer: 'favoriteButton',
        width: 90,
        sortable: true,
      },
      {
        colId: 'DisplayOrder',
        headerName: 'Order',
        field: 'DisplayOrder',
        editable: true,
        sortable: true,
        valueSetter: (value: { data: IAttribute; newValue: number }) => {
          if (typeof value.newValue === 'number') {
            this.infoTabFacade.changeAttribute({
              ...value.data,
              DisplayOrder: value.newValue,
            });
          }
          return false;
        },
      },
      {
        colId: 'Attribute',
        headerName: 'Attribute',
        field: 'Attribute',
        sortable: true,
      },
      {
        colId: 'Value',
        headerName: 'Value',
        field: 'Value',
        sortable: true,
      },
      {
        colId: 'Units',
        headerName: 'Units',
        field: 'Units',
        sortable: true,
      },
      {
        colId: 'Category',
        headerName: 'Category',
        field: 'Category',
        sortable: true,
      },
    ],
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      if (this.rowData) {
        this.gridApi.setRowData(this.rowData);
      }
    },
    getRowId: (params) => {
      return params.data.Id;
    },
  };

  constructor(
    private assetExplorerFacade: AssetExplorerFacade,
    private infoTabFacade: InfoTabFacade
  ) {
    this.asset$ = assetExplorerFacade.selectedAsset$;
    this.theme$ = assetExplorerFacade.theme$;
    this.assetInfo$ = infoTabFacade.assetInfo$;
    this.assetClassTypesLoading$ = infoTabFacade.assetClassTypesLoading$;
    this.assetClassTypes$ = infoTabFacade.assetClassTypes$;
    this.attributes$ = infoTabFacade.attributes$;
    this.autoCompleteLoading$ = infoTabFacade.autoCompleteLoading$;
    this.autoCompleteKeywords$ = infoTabFacade.autoCompleteKeywords$;
    this.canChangeType$ = infoTabFacade.canChangeType$;
  }

  ngOnInit(): void {
    this.canChangeType$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((enabled) => {
        if (enabled) {
          this.assetType.enable();
        } else {
          this.assetType.disable();
        }
      });

    this.attributes$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      this.rowData = data;
      if (this.gridApi) {
        this.gridApi.setRowData(data ?? null);
        this.gridApi.refreshCells({ force: true });
      }
    });

    this.asset$.pipe(takeUntil(this.unsubscribe$)).subscribe((asset) => {
      this.selectedAsset = asset;
    });

    this.assetInfo$.pipe(takeUntil(this.unsubscribe$)).subscribe((state) => {
      if (state) {
        this.name.setValue(state.asset?.AssetAbbrev ?? '');
        this.descriptiveName.setValue(state.asset?.AssetDesc ?? '');
        this.assetType.setValue(state.asset?.AssetClassTypeID);
        this.parent = state.parent?.AssetDesc || null;
      } else {
        this.name.setValue('');
        this.descriptiveName.setValue('');
        this.assetType.setValue(null);
        this.parent = null;
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  updateName() {
    if (this.name.valid) {
      // call saving the name to db here
    }
  }

  updateDescriptiveName() {
    if (this.descriptiveName.valid) {
      // call saving the descriptive name to db here
    }
  }

  getAssetClassTypes() {
    this.infoTabFacade.getAssetClassTypes(this.selectedAsset);
  }

  searchTaggingKeywords() {
    this.infoTabFacade.searchTaggingKeywords(
      this.selectedAsset,
      this.tag.value
    );
  }

  addKeyword() {
    if (this.tag.value !== null && this.tag.value !== '') {
      this.infoTabFacade.addKeyword(this.selectedAsset, this.tag.value);
      this.tag.setValue('');
    }
  }
}
