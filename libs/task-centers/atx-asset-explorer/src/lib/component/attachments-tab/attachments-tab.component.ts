import { Component } from '@angular/core';

@Component({
  selector: 'atx-attachments-tab',
  templateUrl: './attachments-tab.component.html',
  styleUrls: ['./attachments-tab.component.scss'],
})
export class AttachmentsTabComponent {}
