import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Clipboard } from '@angular/cdk/clipboard';
import {
  GridOptions,
  GridReadyEvent,
  ClientSideRowModelModule,
  GridApi,
  Module,
  ClipboardModule,
  ProcessDataFromClipboardParams,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  SetFilterModule,
  FiltersToolPanelModule,
} from '@ag-grid-enterprise/all-modules';
import { TagsTabFacade } from '../../store/facade/tags-tab.facade';
import { ISecurityRights, IPDTag, isNil, IPDServer } from '@atonix/atx-core';
import { takeUntil } from 'rxjs/operators';
import { TagsDataAvailable } from '../../store/state/tags-tab-state';
import { DeleteColumnComponent } from '../delete-column/delete-column.component';
import { AssetExplorerFacade } from '../../store/facade/asset-explorer.facade';

@Component({
  selector: 'atx-tags-tab',
  templateUrl: './tags-tab.component.html',
  styleUrls: ['./tags-tab.component.scss'],
})
export class TagsTabComponent implements OnDestroy {
  @ViewChild('gridBody') gridBody: ElementRef;
  public unsubscribe$ = new Subject<void>();
  public tagPermissions$: Observable<ISecurityRights>;
  public canView$: Observable<boolean>;
  public canEdit$: Observable<boolean>;
  public tagServers$: Observable<IPDServer[]>;
  public tags$: Observable<IPDTag[]>;
  public theme$: Observable<string>;
  public selectedServer$: Observable<number>;
  public isServerSelected$: Observable<boolean>;
  public tagsState$: Observable<TagsDataAvailable>;
  public showDeletedTags$: Observable<boolean>;
  public validationStatus$: Observable<string>;
  public columnDefs: any[];
  public gridApi: GridApi;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];

  gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    suppressRowClickSelection: true,
    stopEditingWhenCellsLoseFocus: true,
    components: {
      deleteButton: DeleteColumnComponent,
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
    },
    columnDefs: [
      {
        headerName: '',
        colId: 'deleteButton',
        editable: false,
        cellRenderer: 'deleteButton',
        width: 90,
        sortable: false,
      },
      {
        headerName: 'Server',
        colId: 'PDServerID',
        field: 'PDServerID',
        editable: false,
        hide: true,
      },
      {
        headerName: 'Name',
        colId: 'TagName',
        field: 'TagName',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            TagName: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Description',
        colId: 'TagDesc',
        field: 'TagDesc',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            TagDesc: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Units',
        colId: 'EngUnits',
        field: 'EngUnits',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            EngUnits: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Active',
        colId: 'ExistsOnServer',
        field: 'ExistsOnServer',
        editable: false,
      },
      {
        headerName: 'External ID',
        colId: 'ExternalID',
        field: 'ExternalID',
        editable: true,
        hide: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            ExternalID: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Qualifier',
        colId: 'Qualifier',
        field: 'Qualifier',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            Qualifier: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Mapping',
        colId: 'Mapping',
        field: 'Mapping',
        editable: true,
        filter: 'agTextColumnFilter',
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            Mapping: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'Global ID',
        colId: 'GlobalID',
        field: 'GlobalID',
        editable: false,
        hide: true,
      },
      {
        headerName: 'ND Created',
        colId: 'NDCreated',
        field: 'NDCreated',
        editable: false,
        hide: true,
      },
      {
        headerName: 'ND Path',
        colId: 'NDPath',
        field: 'NDPath',
        editable: true,
        filter: 'agTextColumnFilter',
        hide: true,
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({
            ...value.data,
            NDPath: value.newValue,
          });
          return false;
        },
      },
      {
        headerName: 'ND ID',
        colId: 'NdId',
        field: 'NDId',
        editable: true,
        filter: 'agTextColumnFilter',
        hide: true,
        valueSetter: (value: { data: IPDTag; newValue: string }) => {
          this.tagsTabFacade.changeTag({ ...value.data, NDId: value.newValue });
          return false;
        },
      },
      {
        headerName: 'ID',
        colId: 'PDTagID',
        field: 'PDTagID',
        editable: false,
        filter: 'agNumberColumnFilter',
        hide: true,
      },
    ],
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.gridApi.hideOverlay();
    },
    getRowId: (params) => {
      return params.data.PDTagID;
    },
    processDataFromClipboard: (params: ProcessDataFromClipboardParams) => {
      return this.processDataFromClipboard(params);
    },
    sendToClipboard: (params) => {
      let data = params.data?.replace(/\r\r\n/gm, '\r\n'); // This will negate additional new line

      const matches = data.match(/"[a-zA-Z0-9"]+/gm); // This will search for strings that begins with double quotes
      if (matches?.length > 0) {
        matches.map((match) => {
          data = data.replace(match, '""""' + match.substring(1));
        });
      }

      const copied = this.clipboard.copy(data);
      if (copied) {
        this.assetExplorerFacade.successToast('Tags copied to clipboard.');
      } else {
        this.assetExplorerFacade.errorToast(
          'Copied selection exceeds maximum. Filter a smaller data set and try again.'
        );
      }
    },
  };

  constructor(
    private tagsTabFacade: TagsTabFacade,
    private assetExplorerFacade: AssetExplorerFacade,
    private clipboard: Clipboard
  ) {
    this.tagPermissions$ = tagsTabFacade.tagsPermissions$;
    this.canView$ = tagsTabFacade.canView$;
    this.canEdit$ = tagsTabFacade.canEdit$;
    this.tagServers$ = tagsTabFacade.tagServers$;
    this.tags$ = tagsTabFacade.tags$;
    this.tagsState$ = tagsTabFacade.tagsState$;
    this.selectedServer$ = tagsTabFacade.selectedServer$;
    this.isServerSelected$ = tagsTabFacade.isServerSelected$;
    this.theme$ = tagsTabFacade.theme$;
    this.showDeletedTags$ = tagsTabFacade.showDeletedTags$;
    this.validationStatus$ = tagsTabFacade.validationStatus$;

    this.validationStatus$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        if (data) {
          this.assetExplorerFacade.infoToast(data);
        }
      });

    this.tags$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (this.gridApi) {
        this.gridApi.setRowData(data);
        this.gridApi.refreshCells({ force: true });
      }
    });

    this.tagsState$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (this.gridApi) {
        if (data === 'loading') {
          this.gridApi.showLoadingOverlay();
        } else if (data === 'nodata') {
          this.gridApi.showNoRowsOverlay();
        } else {
          this.gridApi.hideOverlay();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  selectServer(server: any) {
    this.tagsTabFacade.selectServer(server?.source?.value || -1);
  }

  addTag() {
    this.tagsTabFacade.addTag();
  }

  validateTags() {
    this.tagsTabFacade.validateTags();
  }

  saveTags() {
    this.tagsTabFacade.saveTags();
  }

  discardChanges() {
    this.tagsTabFacade.discardTags();
  }

  showDeletedTagsChange(change: { checked: boolean }) {
    this.tagsTabFacade.showDeletedTagsChanged(change.checked);
  }

  copyTags() {
    this.assetExplorerFacade.infoToast('Copying tags to clipboard...');
    this.gridApi.selectAll();
    this.gridApi.copySelectedRowsToClipboard({
      includeHeaders: true,
      columnKeys: [
        'PDTagID',
        'TagName',
        'TagDesc',
        'EngUnits',
        'Qualifier',
        'Mapping',
      ],
    });
    this.gridApi.deselectAll();
  }

  processDataFromClipboard(params: ProcessDataFromClipboardParams): string[][] {
    const tags: IPDTag[] = [];

    for (const row of params.data) {
      if (row[0].toLowerCase() !== 'id' && this.isSomethingPopulated(row)) {
        const id = row[0];
        const existingRow = this.gridApi.getRowNode(id);
        let rowTag: IPDTag;
        if (existingRow) {
          rowTag = existingRow.data as IPDTag;
        } else {
          rowTag = {} as IPDTag;
        }
        rowTag = {
          ...rowTag,
          TagName: row[1],
          TagDesc: row[2],
          EngUnits: row[3],
          Qualifier: row[4],
          Mapping: row[5],
        };
        tags.push(rowTag);
      }
    }

    if (this.areAllItemsUnique(tags.map((n) => n.PDTagID))) {
      this.tagsTabFacade.pasteTags(tags);
    } else {
      this.assetExplorerFacade.warningToast(
        'Duplicate IDs detected. Make IDs unique and try again. '
      );
    }

    return null;
  }

  private areAllItemsUnique(items: any[]) {
    // Want to make sure all items that are not null are unique.
    let total = 0;
    const uniqueValues = new Set();
    for (const n of items) {
      if (n) {
        uniqueValues.add(n);
        total++;
      }
    }
    return total === uniqueValues.size;
  }

  private isSomethingPopulated(data: string[]) {
    for (const s of data) {
      if (s) {
        return true;
      }
    }
    return false;
  }
}
