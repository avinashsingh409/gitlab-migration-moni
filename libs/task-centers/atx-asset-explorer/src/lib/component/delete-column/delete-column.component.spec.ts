import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeleteColumnComponent } from './delete-column.component';
import { TagsTabFacade } from '../../store/facade/tags-tab.facade';
import { createMock } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('DeleteColumnComponent', () => {
  let component: DeleteColumnComponent;
  let fixture: ComponentFixture<DeleteColumnComponent>;
  let tagsTabFacade: TagsTabFacade;

  beforeEach(waitForAsync(() => {
    tagsTabFacade = createMock(TagsTabFacade);

    TestBed.configureTestingModule({
      declarations: [DeleteColumnComponent],
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [
        {
          provide: TagsTabFacade,
          useValue: tagsTabFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
