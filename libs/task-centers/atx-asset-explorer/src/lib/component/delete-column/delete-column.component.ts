import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { TagsTabFacade } from '../../store/facade/tags-tab.facade';
import { IPDTag } from '@atonix/atx-core';

@Component({
  selector: 'atx-delete-column',
  templateUrl: './delete-column.component.html',
  styleUrls: ['./delete-column.component.scss'],
})
export class DeleteColumnComponent implements ICellRendererAngularComp {
  public tag: IPDTag;

  constructor(private facade: TagsTabFacade) {}

  agInit(params: any) {
    this.setTag(params);
  }

  delete() {
    this.facade.deleteTag(this.tag.PDTagID);
  }

  refresh(params: any) {
    this.setTag(params);
    return true;
  }

  setTag(params: any) {
    this.tag = params?.node?.data ?? this.tag ?? null;
  }
}
