import { Component } from '@angular/core';

@Component({
  selector: 'atx-no-asset',
  templateUrl: './no-asset.component.html',
  styleUrls: ['./no-asset.component.scss'],
})
export class NoAssetComponent {}
