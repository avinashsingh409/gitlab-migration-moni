import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NoAssetComponent } from './no-asset.component';

describe('NoAssetComponent', () => {
  let component: NoAssetComponent;
  let fixture: ComponentFixture<NoAssetComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NoAssetComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
