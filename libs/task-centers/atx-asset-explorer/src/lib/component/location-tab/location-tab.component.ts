import { Component } from '@angular/core';

@Component({
  selector: 'atx-location-tab',
  templateUrl: './location-tab.component.html',
  styleUrls: ['./location-tab.component.scss'],
})
export class LocationTabComponent {}
