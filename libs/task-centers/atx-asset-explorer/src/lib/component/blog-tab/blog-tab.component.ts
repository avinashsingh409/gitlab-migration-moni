import { Component, OnInit, OnDestroy } from '@angular/core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import {
  ITaggingKeyword,
  isNil,
  IDiscussionAssetMapType,
  IDiscussionEntry,
} from '@atonix/atx-core';
import { BlogTabFacade } from '../../store/facade/blog-tab.facade';
import { takeUntil } from 'rxjs/operators';
import { AssetExplorerFacade } from '../../store/facade/asset-explorer.facade';

@Component({
  selector: 'atx-blog-tab',
  templateUrl: './blog-tab.component.html',
  styleUrls: ['./blog-tab.component.scss'],
})
export class BlogTabComponent implements OnDestroy {
  public discussionAssetTypes$: Observable<IDiscussionAssetMapType[]>;
  public issuesCount$: Observable<number>;
  public discussionEntry$: Observable<IDiscussionEntry>;
  public autoCompleteLoading$: Observable<boolean>;
  public autoCompleteKeywords$: Observable<ITaggingKeyword[]>;
  public unsubscribe$ = new Subject<void>();

  faSpinner = faSpinner;
  expandAddNewEntry = false;

  blogTabForm = new UntypedFormGroup({
    showTopics: new UntypedFormControl(null, [Validators.required]),
    showAutogenEntries: new UntypedFormControl(false),
    topics: new UntypedFormControl(null, [Validators.required]),
    title: new UntypedFormControl(null, [Validators.required]),
    content: new UntypedFormControl(null, [Validators.required]),
    keyword: new UntypedFormControl(null),
  });

  initEditor = {
    promotion: false,
    plugins: 'image autolink link',
    paste_data_images: true,
    convert_urls: false,
    relative_urls: false,
    remove_script_host: false,
    toolbar:
      'undo redo | bold italic underline | forecolor | fontselect emoticons',
    menubar: 'edit view format',
    menu: {
      edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
      view: { title: 'View', items: 'visualaid' },
      format: {
        title: 'Format',
        items: 'bold italic underline | removeformat',
      },
    },
    statusbar: false,
    browser_spellcheck: true,
    base_url: `/tinymce`,
    suffix: '.min',
  };

  constructor(
    private blogTabFacade: BlogTabFacade,
    assetExplorerFacade: AssetExplorerFacade
  ) {
    this.discussionAssetTypes$ = blogTabFacade.selectDiscussionAssetTypes$;
    this.issuesCount$ = blogTabFacade.selectIssuesCount$;
    this.discussionEntry$ = blogTabFacade.selectDiscussionEntry$;
    this.autoCompleteLoading$ = blogTabFacade.autoCompleteLoading$;
    this.autoCompleteKeywords$ = blogTabFacade.autoCompleteKeywords$;
    assetExplorerFacade.assetTreeId$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        if (id?.assetId) {
          return this.blogTabFacade.routeLoadBlog(id.assetId);
        }
        return null;
      });
    this.discussionAssetTypes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((types) => {
        if (types) {
          this.blogTabForm.patchValue({ showTopics: 1, topics: 1 });
        }
      });

    this.discussionEntry$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((disc) => {
        if (isNil(disc)) {
          this.expandAddNewEntry = false;
          this.blogTabForm.patchValue({
            title: null,
            topics: 1,
            content: null,
            keyword: null,
          });
        }
      });
  }

  ngOnDestroy(): void {
    console.log('hello');
    this.unsubscribe$.next();
  }

  sendNotifications() {
    this.blogTabFacade.sendNotifications();
  }

  subscribe() {
    if (this.blogTabForm.get('showTopics').valid) {
      this.blogTabFacade.subscribe(this.blogTabForm.get('showTopics').value);
    }
  }

  viewOpenIssues() {
    this.blogTabFacade.viewOpenIssues();
  }

  updateTitle() {
    this.blogTabFacade.updateTitle(this.blogTabForm.get('title').value);
  }

  updateContent() {
    this.blogTabFacade.updateContent(this.blogTabForm.get('content').value);
  }

  saveDiscussionEntry() {
    this.blogTabFacade.saveDiscussionEntry(
      this.blogTabForm.get('showAutogenEntries').value,
      this.blogTabForm.get('showTopics').value,
      this.blogTabForm.get('topics').value
    );
  }

  cancelDiscussionEntry() {
    this.blogTabFacade.cancelDiscussionEntry();
  }

  searchTaggingKeywords() {
    this.blogTabFacade.searchTaggingKeywords(
      this.blogTabForm.get('keyword').value
    );
  }

  addKeyword() {
    if (
      this.blogTabForm.get('keyword').value !== null &&
      this.blogTabForm.get('keyword').value !== ''
    ) {
      this.blogTabFacade.addKeyword(this.blogTabForm.get('keyword').value);
      this.blogTabForm.get('keyword').setValue('');
    }
  }

  uploadFile($event) {
    // File uploading and saving here
    this.blogTabFacade.addAttachment(
      undefined,
      undefined,
      $event[0].name,
      undefined,
      $event[0]
    );
  }
}
