import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import {
  IEventChange,
  IEventType,
  IUserEventWithDescription,
} from '@atonix/atx-core';
import moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';

export interface DialogData {
  affectedEvents: IEventChange[];
  currentEvent: IUserEventWithDescription;
  isDeletion: boolean;
}

@Component({
  selector: 'atx-event-validation-dialog',
  templateUrl: './event-validation-dialog.component.html',
  styleUrls: ['./event-validation-dialog.component.scss'],
})
export class EventValidationDialogComponent implements OnDestroy {
  public unsubscribe$: Subject<void> = new Subject<void>();
  public validationMessage$: Observable<string>;
  public changedEvents$: Observable<IEventChange[]>;
  public eventTypes$: Observable<IEventType[]>;
  public newEventInvalid$: Observable<boolean>;
  public changedEventCount$: Observable<number>;
  displayedColumns: string[] = [
    'EventTypeID',
    'Description',
    'StartTime',
    'EndTime',
  ];
  eventTypes: IEventType[] = [];

  constructor(
    public eventsFacade: EventsTabFacade,
    public dialogRef: MatDialogRef<EventValidationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.validationMessage$ = eventsFacade.validationMessage$;
    this.changedEvents$ = eventsFacade.changedEvents$;
    this.eventTypes$ = eventsFacade.eventTypes$;
    this.changedEventCount$ = eventsFacade.changedEventCount$;
    this.eventTypes$.pipe(takeUntil(this.unsubscribe$)).subscribe((types) => {
      this.eventTypes = types;
    });
  }

  confirm() {
    this.dialogRef.close('confirm');
  }

  delete() {
    this.dialogRef.close('delete');
  }
  getEventType(typeID: number) {
    return this.eventTypes.find((x) => x.EventTypeID === typeID).Description;
  }

  parseDate(date: Date) {
    if (date) {
      return moment(date).toDate();
    }
  }
  ngOnDestroy(): void {
    this.eventsFacade.validationDialogReset();
    this.unsubscribe$.next();
  }
}
