import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { BehaviorSubject, Subject } from 'rxjs';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';
import {
  DialogData,
  EventValidationDialogComponent,
} from './event-validation-dialog.component';

describe('EventValidationDialogComponent', () => {
  const dialogData = {
    currentEvent: null,
    affectedEvents: [],
    isDeletion: false,
  } as DialogData;
  const mockDialogRef = {
    close: jest.fn(),
  };

  let mockEventsTabFacade: EventsTabFacade;

  let component: EventValidationDialogComponent;
  let fixture: ComponentFixture<EventValidationDialogComponent>;

  beforeEach(() => {
    mockEventsTabFacade = createMockWithValues(EventsTabFacade, {
      eventTypes$: new BehaviorSubject<any>(null),
      validationMessage$: new BehaviorSubject<any>(null),
      changedEvents$: new BehaviorSubject<any>(null),
      changedEventCount$: new BehaviorSubject<any>(null),
    });

    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: dialogData,
          },
        },
        {
          provide: EventsTabFacade,
          useValue: mockEventsTabFacade,
        },
      ],
      imports: [AtxMaterialModule, NoopAnimationsModule, MatTableModule],
      declarations: [EventValidationDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [EventValidationDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventValidationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
