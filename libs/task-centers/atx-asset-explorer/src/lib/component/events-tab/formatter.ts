import moment from 'moment';
import isNil from 'lodash/isNil';

export function dateFormatter(params) {
  return !isNil(params.value)
    ? moment(params.value).format('MM/DD/YYYY hh:mm:ss A')
    : null;
}
