import {
  Inject,
  Component,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {
  UntypedFormControl,
  FormGroupDirective,
  NgForm,
  Validators,
  UntypedFormGroup,
} from '@angular/forms';
import moment from 'moment';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';
import { Observable, Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';

import { EventValidationDialogComponent } from '../event-validation-dialog/event-validation-dialog.component';
import {
  IEvent,
  IEventChange,
  IEventType,
  IUserEventWithDescription,
} from '@atonix/atx-core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  constructor(public isInvalidDateFormat: boolean) {}

  isErrorState(
    control: UntypedFormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return !!(
      control &&
      (control.invalid || this.isInvalidDateFormat) &&
      (control.dirty || control.touched)
    );
  }
}

@Component({
  selector: 'atx-new-events-dialog',
  templateUrl: './new-events-dialog.component.html',
  styleUrls: ['./new-events-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewEventsDialogComponent implements OnDestroy {
  @ViewChild('startDateInput') startDateInputControl: ElementRef;
  @ViewChild('endDateInput') endDateInputControl: ElementRef;
  public newEventForm: UntypedFormGroup;
  public eventTypes$: Observable<IEventType[]>;
  public validationMessage$: Observable<string>;
  public newEventInvalid$: Observable<boolean>;
  public unsubscribe$: Subject<void> = new Subject<void>();
  public saveResult$: Observable<string>;
  public changedEventCount$: Observable<number>;
  public changedEvents$: Observable<IEventChange[]>;

  startDateMatcher = new MyErrorStateMatcher(false);
  endDateMatcher = new MyErrorStateMatcher(false);
  isInvalidStartDateFormat = false;
  isInvalidEndDateFormat = false;
  startDateInputValue: string = null;
  endDateInputValue: string = null;
  min: Date = null;
  max: Date = null;
  startPickerStartAt: Date = null;
  endPickerStartAt: Date = null;
  isEditing = false;
  isDeleting = false;
  eventID = -1;
  hasChangedEvents = false;

  constructor(
    public dialog: MatDialog,
    public validationDialog: MatDialog,
    public eventsFacade: EventsTabFacade
  ) {
    const referenceDate = new Date();

    eventsFacade.eventDialogOpened();

    this.eventTypes$ = eventsFacade.eventTypes$;
    this.validationMessage$ = eventsFacade.validationMessage$;
    this.newEventInvalid$ = eventsFacade.newEventInvalid$;
    this.saveResult$ = eventsFacade.saveResult$;
    this.eventsFacade.getEventTypes();
    this.changedEventCount$ = eventsFacade.changedEventCount$;
    this.changedEvents$ = eventsFacade.changedEvents$;

    this.newEventForm = new UntypedFormGroup({
      startDate: new UntypedFormControl(
        null,

        [Validators.required]
      ),
      startTime: new UntypedFormControl(null, [Validators.required]),
      endDate: new UntypedFormControl(null, [Validators.required]),
      endTime: new UntypedFormControl(null, [Validators.required]),
      type: new UntypedFormControl(1, [Validators.required]),
      impact: new UntypedFormControl({ value: null, disabled: true }, [
        Validators.required,
      ]),
      description: new UntypedFormControl('New Event', [Validators.required]),
      isInProgress: new UntypedFormControl(),
    });

    this.newEventForm.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.eventsFacade.eventChanged();
      });

    this.newEventForm
      .get('type')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        if (value === 3 || value === 5) {
          this.newEventForm.get('impact').enable();
        } else {
          this.newEventForm.get('impact').setValue(null);
          this.newEventForm.get('impact').disable();
        }
      });

    this.newEventForm
      .get('isInProgress')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        if (value) {
          this.newEventForm.get('endDate').setValue(null);
          this.newEventForm.get('endTime').setValue(null);
          this.newEventForm.get('endDate').disable();
          this.newEventForm.get('endTime').disable();
        } else {
          this.newEventForm.get('endDate').enable();
          this.newEventForm.get('endTime').enable();
        }
      });

    this.newEventForm
      .get('startDate')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        const dateNow = moment().toDate();
        const endDate = this.newEventForm.get('endDate').value;
        const startDateTimeStamp = this.combineTime(
          value,
          this.newEventForm.get('startTime').value
        );
        const endDateTimeStamp = this.combineTime(
          endDate,
          this.newEventForm.get('endTime').value
        );

        const isfutureDate =
          (moment(startDateTimeStamp).isAfter(dateNow) ||
            moment(startDateTimeStamp).isSame(dateNow, 'day')) &&
          moment(endDateTimeStamp).isAfter(dateNow);
        const isAfterEndate =
          moment(startDateTimeStamp).isAfter(endDateTimeStamp);
        const isEqual = moment(startDateTimeStamp).isSame(endDateTimeStamp);
        const isValid =
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid();
        const isValidEndDate =
          moment(endDate, 'MM/DD/YYYY', true).isValid() ||
          moment(endDate as string, 'M/D/YYYY', true).isValid();

        if (isfutureDate || isAfterEndate || isEqual || !isValid) {
          if (isfutureDate) {
            this.newEventForm
              .get('startDate')
              .setErrors({ isFutureTimeStamp: true });
          }

          if (isAfterEndate) {
            this.newEventForm
              .get('startDate')
              .setErrors({ isStartDateGreaterThanEndDate: true });
          }

          if (isEqual) {
            this.newEventForm
              .get('startDate')
              .setErrors({ isStartDateEqualsEndDate: true });
          }

          if (!isValid) {
            this.newEventForm
              .get('startDate')
              .setErrors({ invalidDateFormat: true });
          }
        } else {
          if (!isEqual && isValidEndDate) {
            this.newEventForm.get('startDate').setErrors(null);
            this.newEventForm.get('endDate').setErrors(null);
          } else {
            this.newEventForm.get('startDate').setErrors(null);
          }
        }
      });

    this.newEventForm
      .get('endDate')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        const dateNow = moment().toDate();
        const startDate = this.newEventForm.get('startDate').value;
        const startDateTimeStamp = this.combineTime(
          startDate,
          this.newEventForm.get('startTime').value
        );
        const endDateTimeStamp = this.combineTime(
          value,
          this.newEventForm.get('endTime').value
        );

        const isfutureDate =
          (moment(startDateTimeStamp).isAfter(dateNow) ||
            moment(startDateTimeStamp).isSame(dateNow, 'day')) &&
          moment(endDateTimeStamp).isAfter(dateNow);
        const isBeforeStartDate =
          moment(endDateTimeStamp).isBefore(startDateTimeStamp);
        const isEqual = moment(startDateTimeStamp).isSame(endDateTimeStamp);
        const isValid =
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid();
        const isValidStartDate =
          moment(startDate, 'MM/DD/YYYY', true).isValid() ||
          moment(startDate as string, 'M/D/YYYY', true).isValid();

        if (isfutureDate || isBeforeStartDate || isEqual || !isValid) {
          if (isfutureDate) {
            this.newEventForm
              .get('endDate')
              .setErrors({ isFutureTimeStamp: true });
          }

          if (isBeforeStartDate) {
            this.newEventForm
              .get('endDate')
              .setErrors({ isEndDateGreaterThanStartDate: true });
          }

          if (isEqual) {
            this.newEventForm
              .get('endDate')
              .setErrors({ isStartDateEqualsEndDate: true });
          }

          if (!isValid) {
            this.newEventForm
              .get('endDate')
              .setErrors({ invalidDateFormat: true });
          }
        } else {
          if (!isEqual && isValidStartDate) {
            this.newEventForm.get('startDate').setErrors(null);
            this.newEventForm.get('endDate').setErrors(null);
          } else {
            this.newEventForm.get('endDate').setErrors(null);
          }
        }
      });

    this.newEventForm
      .get('startTime')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        const endTime = this.newEventForm.get('endTime').value;
        const startDate = this.newEventForm.get('startDate').value;
        const endDate = this.newEventForm.get('endDate').value;
        const startDateTimeStamp = this.combineTime(
          this.newEventForm.get('startDate').value,
          value
        );
        const endDateTimeStamp = this.combineTime(
          this.newEventForm.get('endDate').value,
          endTime
        );
        const isEqual = moment(startDateTimeStamp).isSame(endDateTimeStamp);
        const isValidStartDate =
          moment(startDate, 'MM/DD/YYYY', true).isValid() ||
          moment(startDate as string, 'M/D/YYYY', true).isValid();
        const isValidEndDate =
          moment(endDate, 'MM/DD/YYYY', true).isValid() ||
          moment(endDate as string, 'M/D/YYYY', true).isValid();

        if (isEqual) {
          this.newEventForm
            .get('startDate')
            .setErrors({ isStartDateEqualsEndDate: true });
        } else {
          if (isValidStartDate && isValidEndDate) {
            this.newEventForm.get('startDate').setErrors(null);
            this.newEventForm.get('endDate').setErrors(null);
          }
        }
      });

    this.newEventForm
      .get('endTime')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        const startTime = this.newEventForm.get('startTime').value;
        const startDate = this.newEventForm.get('startDate').value;
        const endDate = this.newEventForm.get('endDate').value;
        const startDateTimeStamp = this.combineTime(
          this.newEventForm.get('startDate').value,
          startTime
        );
        const endDateTimeStamp = this.combineTime(
          this.newEventForm.get('endDate').value,
          value
        );
        const isEqual = moment(startDateTimeStamp).isSame(endDateTimeStamp);
        const isValidStartDate =
          moment(startDate, 'MM/DD/YYYY', true).isValid() ||
          moment(startDate as string, 'M/D/YYYY', true).isValid();
        const isValidEndDate =
          moment(endDate, 'MM/DD/YYYY', true).isValid() ||
          moment(endDate as string, 'M/D/YYYY', true).isValid();

        if (isEqual) {
          this.newEventForm
            .get('endDate')
            .setErrors({ isStartDateEqualsEndDate: true });
        } else {
          if (isValidStartDate && isValidEndDate) {
            this.newEventForm.get('startDate').setErrors(null);
            this.newEventForm.get('endDate').setErrors(null);
          }
        }
      });
    this.saveResult$.pipe(takeUntil(this.unsubscribe$)).subscribe((message) => {
      this.dialog.closeAll();
    });

    this.changedEvents$
      ?.pipe(
        withLatestFrom(this.validationMessage$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([events, message]) => {
        if (!this.isDeleting) {
          if (events && events.length > 0) {
            this.hasChangedEvents = true;
            const validationDialogConfig = new MatDialogConfig();
            validationDialogConfig.panelClass = 'validation-custom-dialog';
            validationDialogConfig.data = {
              currentEvent: this.getFormData(),
              affectedEvents: events,
              isDeletion: false,
            };

            const validationDialogRef = this.validationDialog.open(
              EventValidationDialogComponent,
              validationDialogConfig
            );
            // eslint-disable-next-line rxjs/no-nested-subscribe
            validationDialogRef.afterClosed().subscribe((result) => {
              if (!validationDialogRef.componentInstance.data.isDeletion) {
                if (result === 'confirm') {
                  if (this.isEditing) {
                    this.eventsFacade.changeEvent(this.createEventFromInputs());
                  } else {
                    this.eventsFacade.saveEvent(
                      this.createNewEventFromInputs()
                    );
                  }
                }
              }
            });
          } else {
            if (message === 'Event is valid.' && !this.hasChangedEvents) {
              if (this.isEditing) {
                this.eventsFacade.changeEvent(this.createNewEventFromInputs());
              } else {
                this.eventsFacade.saveEvent(this.createNewEventFromInputs());
              }
            }
          }
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  createNewEventFromInputs() {
    return {
      EventID: this.eventID,
      EventTypeID: this.eventType(),
      StartTime: this.startTime(),
      EndTime: this.endTime(),
      Impact: this.impact(),
      Description: this.description(),
    } as IUserEventWithDescription;
  }

  createEventFromInputs() {
    return {
      EventID: this.eventID,
      AssetID: 0,
      EventTypeID: this.eventType(),
      StartTime: this.startTime(),
      EndTime: this.endTime(),
      Impact: this.impact(),
      Description: this.description(),
      ModifiedByEventID: null,
    } as IEvent;
  }

  eventType(): number {
    return this.newEventForm.get('type').value;
  }

  startTime() {
    const d: Date = this.newEventForm.get('startDate').value;
    const t: string = this.newEventForm.get('startTime').value;
    const result = this.combineTime(d, t);
    return result;
  }

  endTime() {
    const d: Date = this.newEventForm.get('endDate').value;
    const t: string = this.newEventForm.get('endTime').value;
    const result = this.combineTime(d, t);
    return result;
  }

  impact() {
    return this.newEventForm.get('impact').value;
  }

  description() {
    return this.newEventForm.get('description').value;
  }

  isInProgress() {
    return this.newEventForm.get('isInProgress').value;
  }

  submitNewEvent() {
    if (this.newEventForm.valid) {
      this.eventsFacade.validateEvent(this.createNewEventFromInputs());
      this.hasChangedEvents = false;
    }
  }

  validate() {
    if (this.newEventForm.valid) {
      this.eventsFacade.validateEvent(this.createEventFromInputs());
    }
  }

  checkStartDateFormat(startValue: string): void {
    this.startDateInputValue = startValue;

    if (
      moment(startValue, 'MM/DD/YYYY', true).isValid() ||
      moment(startValue as string, 'M/D/YYYY', true).isValid()
    ) {
      this.startDateMatcher = new MyErrorStateMatcher(false);
      this.isInvalidStartDateFormat = false;
      this.startPickerStartAt = null;
      this.checkAndCombineStartTime();
    } else {
      this.min = this.newEventForm.value.endDate;
      this.startDateMatcher = new MyErrorStateMatcher(true);
      this.isInvalidStartDateFormat = true;
      this.startPickerStartAt = moment().toDate();
    }
  }

  checkAndCombineStartTime(): void {
    if (this.newEventForm.valid && !this.isInvalidStartDateFormat) {
      this.newEventForm
        .get('startDate')
        .setValue(
          this.combineTime(
            this.newEventForm.value.startDate,
            this.newEventForm.value.startTime
          )
        );
      if (
        !this.isInvalidEndDateFormat &&
        this.newEventForm.value.startDate > this.newEventForm.value.endDate
      ) {
        const start = moment(this.newEventForm.value.startDate).toDate();
        const end = moment(this.newEventForm.value.endDate).toDate();
        this.min = moment(start.setDate(start.getDate() + 1)).toDate();
        this.max = moment(end.setDate(end.getDate() - 1)).toDate();
      } else if (this.isInvalidEndDateFormat) {
        this.max = this.newEventForm.value.startDate;
      } else {
        this.min = this.newEventForm.value.startDate;
        this.max = this.newEventForm.value.endDate;
      }
    } else {
      this.min = this.newEventForm.value.startDate;
      this.max = this.newEventForm.value.endDate;
    }
  }
  // This will set back the elementRef if the value is invalid.
  // This will also negate the auto-parse of the mat-datepicker.
  checkStartDateFormatOnFocusOut(): void {
    this.checkStartDateFormat(this.startDateInputValue);
    if (this.isInvalidStartDateFormat) {
      this.startDateInputControl.nativeElement.value = this.startDateInputValue;
    }
  }

  public combineTime(myDate: Date, time: string): Date {
    let result: Date = null;

    const newTime = moment(time, 'HH:mm:ss');
    if (newTime.isValid) {
      const newVal = moment(myDate)
        .hour(newTime.hour())
        .minute(newTime.minute())
        .second(newTime.second())
        .millisecond(newTime.millisecond());
      result = newVal.toDate();
    }

    return result;
  }

  setFormData(data: IEvent) {
    this.newEventForm.patchValue({
      startDate: moment(data.StartTime).toDate(),
      startTime: moment(data.StartTime).format('HH:mm:ss'),
      endDate: moment(data.EndTime).toDate(),
      endTime: moment(data.EndTime).format('HH:mm:ss'),
      type: data.EventTypeID,
      impact: data.Impact,
      description: data.Description,
      isInProgress: data.EndTime === null ? true : false,
    });

    this.eventID = data.EventID;
  }

  getFormData() {
    return {
      EventID: this.eventID,
      EventTypeID: this.eventType(),
      StartTime: this.startTime(),
      EndTime: this.endTime(),
      Impact: this.impact(),
      Description: this.description(),
    } as IUserEventWithDescription;
  }

  deleteEvent() {
    const validationDialogConfig = new MatDialogConfig();
    validationDialogConfig.panelClass = 'validation-custom-dialog';
    validationDialogConfig.data = {
      event: null,
      affectedEvents: [],
      isDeletion: true,
    };

    this.isDeleting = true;

    const validationDialogRef = this.validationDialog.open(
      EventValidationDialogComponent,
      validationDialogConfig
    );
    validationDialogRef
      .afterClosed()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        if (result === 'delete') {
          this.eventsFacade.deleteEvent(this.eventID);
          this.dialog.closeAll();
        }
      });
  }
}
