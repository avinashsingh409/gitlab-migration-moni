import {
  ComponentFixture,
  TestBed,
  inject,
  waitForAsync,
} from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import moment from 'moment';
import { NewEventsDialogComponent } from './new-events-dialog.component';
import { of, Subject, BehaviorSubject } from 'rxjs';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';
import { AtxMaterialModule } from '@atonix/atx-material';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('NewEventsDialogComponent', () => {
  const calendarDate = {
    startDate: moment().toDate(),
    endDate: moment().toDate(),
  };

  const mockDialogRef = {
    close: jest.fn(),
  };
  let mockEventsTabFacade: EventsTabFacade;

  let component: NewEventsDialogComponent;
  let fixture: ComponentFixture<NewEventsDialogComponent>;

  beforeEach(waitForAsync(() => {
    mockEventsTabFacade = createMockWithValues(EventsTabFacade, {
      eventsPermissions$: new BehaviorSubject<any>(null),
      events$: new BehaviorSubject<any>(null),
      eventTypes$: new BehaviorSubject<any>(null),
      validationMessage$: new BehaviorSubject<any>(null),
      message$: new BehaviorSubject<any>(null),
      newEventInvalid$: new BehaviorSubject<any>(null),
      saveResult$: new BehaviorSubject<any>(null),
    });

    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatchMedia, useClass: MockMatchMedia },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: calendarDate,
          },
        },
        {
          provide: EventsTabFacade,
          useValue: mockEventsTabFacade,
        },
      ],
      imports: [
        FormsModule,
        AtxMaterialModule,
        FlexLayoutModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      declarations: [NewEventsDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEventsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should check start date format in focus out', () => {
    jest.spyOn(component, 'checkStartDateFormat');
    component.isInvalidStartDateFormat = true;
    component.startDateInputValue = '1/1/201';

    component.checkStartDateFormatOnFocusOut();
    expect(component.checkStartDateFormat).toHaveBeenCalledWith(
      component.startDateInputValue
    );
    expect(component.startDateInputControl.nativeElement.value).toEqual(
      component.startDateInputValue
    );
  });
});
