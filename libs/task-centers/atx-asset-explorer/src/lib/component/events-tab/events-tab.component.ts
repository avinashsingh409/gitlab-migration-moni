import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  Inject,
  OnDestroy,
} from '@angular/core';
import { Subject, Observable } from 'rxjs';
import {
  ISecurityRights,
  dateComparator,
  isNil,
  IEvent,
} from '@atonix/atx-core';
import {
  GridOptions,
  GridReadyEvent,
  ClientSideRowModelModule,
  GridApi,
  Module,
  ColumnApi,
  ICellRendererParams,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import { AssetExplorerFacade } from '../../store/facade/asset-explorer.facade';
import { takeUntil } from 'rxjs/operators';
import { IEventsTabStateChange } from '../../model/events-tab-state-change';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NewEventsDialogComponent } from './new-events-dialog/new-events-dialog.component';
import moment from 'moment';
import { EventsTabFacade } from '../../store/facade/events-tab.facade';
import { dateFormatter } from './formatter';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { EventActionComponent } from './event-action/event-action.component';
import { AssetExplorerConfigService } from '../../service/asset-explorer-config.service';
import { AssetExplorerConfig } from '../../model/asset-explorer.config';

@Component({
  selector: 'atx-events-tab',
  templateUrl: './events-tab.component.html',
  styleUrls: ['./events-tab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsTabComponent implements OnDestroy {
  @Output() stateChange = new EventEmitter<IEventsTabStateChange>();

  public theme$: Observable<string>;
  public events$: Observable<IEvent[]>;
  public eventsViewable$: Observable<boolean>;
  public selectedNode$: Observable<string>;
  public isAdHocTree$: Observable<boolean>;
  public includeDescendants$: Observable<boolean>;
  public descendentsLoading$: Observable<boolean>;

  asset: string;
  rowData: IEvent[] = [];
  gridApi: GridApi;
  columnApi: ColumnApi;

  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
  ];

  public unsubscribe$ = new Subject<void>();
  public eventsPermissions$: Observable<ISecurityRights>;
  constructor(
    private assetExplorerFacade: AssetExplorerFacade,
    public dialog: MatDialog,
    private eventsTabFacade: EventsTabFacade,
    @Inject(AssetExplorerConfigService) public config: AssetExplorerConfig
  ) {
    this.eventsPermissions$ = eventsTabFacade.eventsPermissions$;
    this.theme$ = assetExplorerFacade.theme$;
    this.selectedNode$ = assetExplorerFacade.selectedNodeAbbrev$;
    this.isAdHocTree$ = assetExplorerFacade.isAdHocTree$;
    this.events$ = eventsTabFacade.events$;
    this.eventsViewable$ = eventsTabFacade.eventsViewable$;
    this.includeDescendants$ = eventsTabFacade.includeDescendants$;
    this.descendentsLoading$ = eventsTabFacade.descendentsLoading$;

    this.events$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      this.rowData = data;
      if (this.gridApi) {
        this.gridApi.setRowData(data);
      }
    });

    if (config.configType === 'AssetExplorer') {
      this.selectedNode$.pipe(takeUntil(this.unsubscribe$)).subscribe((n) => {
        this.asset = n;
      });
    } else if (config.configType === 'PowerGen') {
      assetExplorerFacade.assetDesc$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((n) => {
          this.asset = n;
        });
    }
  }

  gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    rowSelection: 'single',
    stopEditingWhenCellsLoseFocus: true,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: false,
    },
    components: {
      eventAction: EventActionComponent,
    },
    columnDefs: [
      {
        colId: 'editEvent',
        width: 60,
        cellRendererSelector: (params: ICellRendererParams) => {
          let result = null;
          if (!params?.data?.IsDeleted) {
            result = {
              component: 'eventAction',
              params: {
                actionType: 'editEvent',
                data: params.data,
              },
            };
          }
          return result;
        },
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'AssetPath',
        filter: 'agTextColumnFilter',
        tooltipValueGetter: (params) => {
          if (params.data?.AssetPath) {
            return params.data.AssetPath;
          } else {
            return this.asset;
          }
        },
        valueGetter: (params) => {
          if (params.data?.AssetPath) {
            return this.processPath(params.data?.AssetPath);
          } else {
            return this.asset;
          }
        },
      },
      {
        colId: 'EventType',
        headerName: 'Event Type',
        field: 'eventType',
        filter: true,
      },
      {
        colId: 'Impact',
        headerName: 'Impact',
        field: 'Impact',
        filter: 'agTextColumnFilter',
        valueGetter: (params) => {
          return params?.data?.EventTypeID === 3 ||
            params?.data?.EventTypeID === 5
            ? params?.data?.Impact.toString()
            : '--';
        },
        comparator: (a, b, nodeA, nodeB) => {
          if (a === '--') {
            return -1;
          } else if (b === '--') {
            return 1;
          } else if (
            nodeA?.data?.EventTypeID === 3 ||
            nodeA?.data?.EventTypeID === 5 ||
            nodeB?.data?.EventTypeID === 3 ||
            nodeB?.data?.EventTypeID === 5
          ) {
            return a - b;
          }
        },
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Description',
        width: 400,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'StartTime',
        headerName: 'Start Date/Time',
        field: 'StartTime',
        filter: 'agDateColumnFilter',
        valueFormatter: dateFormatter,
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        colId: 'EndTime',
        headerName: 'End Date/Time',
        field: 'EndTime',
        filter: 'agDateColumnFilter',
        valueGetter: (params) => {
          return !isNil(params.data.EndTime)
            ? moment(params.data.EndTime).format('MM/DD/YYYY hh:mm:ss A')
            : 'In Progress';
        },
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
        comparator: (a, b) => {
          if (a === 'In Progress') {
            return -1;
          } else if (b === 'In Progress') {
            return 1;
          } else {
            return moment(a).toDate().getTime() - moment(b).toDate().getTime();
          }
        },
      },
      {
        colId: 'ChangedBy',
        headerName: 'Modified By',
        field: 'ChangedBy',
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'IsDeleted',
        headerName: 'Deleted',
        field: 'IsDeleted',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        filterValueGetter: (params) => {
          return String(params?.data?.IsDeleted ?? false);
        },
      },
      {
        colId: 'eventHistory',
        width: 60,
        cellRendererSelector: (params: ICellRendererParams) => {
          const result: any = {
            component: 'eventAction',
            params: {
              actionType: 'eventHistory',
              asset: params.data?.AssetPath
                ? this.processPath(params.data?.AssetPath)
                : this.asset,
              data: params.data,
            },
          };
          return result;
        },
      },
    ],
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.columnApi.applyColumnState({
        state: [{ colId: 'StartTime', sort: 'desc' }],
      });
      this.gridApi.setFilterModel({
        IsDeleted: {
          values: ['false'],
          filterType: 'set',
        },
      });
      this.gridOptions.api.setRowData(this.rowData);
    },
    getRowId: (params) => {
      return params.data.EventID;
    },
  };

  newEventsDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog';
    dialogConfig.data = {
      startDate: moment(),
      endDate: moment(),
    };
    const dialogRef = this.dialog.open(NewEventsDialogComponent, dialogConfig);
    // this.stateChange.emit(newEventsDialog(true));
  }
  includeDescendants(value: MatCheckboxChange) {
    this.eventsTabFacade.toggleIncludeDescendents(value.checked);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  processPath(path: string) {
    const parts = path.split('\\');
    const lastIndex = parts
      .map((part: string) => part === this.asset)
      .lastIndexOf(true);
    if (lastIndex === -1) {
      return path.slice(0, -1);
    } else if (parts[0] === this.asset && parts.length === 1) {
      return this.asset;
    }
    return parts.slice(lastIndex, parts.length).join('\\').slice(0, -1);
  }
}
