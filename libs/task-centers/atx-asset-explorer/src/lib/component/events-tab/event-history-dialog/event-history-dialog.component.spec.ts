import { AgGridModule } from '@ag-grid-community/angular';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { IEvent } from '@atonix/atx-core';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { BehaviorSubject, Subject } from 'rxjs';
import { AssetExplorerFacade } from '../../../store/facade/asset-explorer.facade';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';
import { EventHistoryDialogComponent } from './event-history-dialog.component';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
describe('EventHistoryDialogComponent', () => {
  let component: EventHistoryDialogComponent;
  let fixture: ComponentFixture<EventHistoryDialogComponent>;

  let assetExplorerFacadeMock: AssetExplorerFacade;
  let eventsTabFacadeMock: EventsTabFacade;

  beforeEach(() => {
    assetExplorerFacadeMock = createMockWithValues(AssetExplorerFacade, {
      theme$: new BehaviorSubject<'ag-theme-alpine' | 'ag-theme-alpine-dark'>(
        'ag-theme-alpine'
      ),
      selectedNodeAbbrev$: new BehaviorSubject<string>('test'),
    });
    eventsTabFacadeMock = createMockWithValues(EventsTabFacade, {
      eventHistory$: new BehaviorSubject<IEvent[]>(null),
    });

    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        AgGridModule.withComponents([EventHistoryDialogComponent]),
      ],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            eventID: '0000-0000-0000-0000',
          },
        },
        { provide: AssetExplorerFacade, useValue: assetExplorerFacadeMock },
        { provide: EventsTabFacade, useValue: eventsTabFacadeMock },
      ],
      declarations: [EventHistoryDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
