import {
  ClientSideRowModelModule,
  ClipboardModule,
  ColumnApi,
  EnterpriseCoreModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuModule,
  Module,
  RangeSelectionModule,
} from '@ag-grid-enterprise/all-modules';
import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { dateComparator, IEvent, isNil } from '@atonix/atx-core';
import moment from 'moment';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AssetExplorerFacade } from '../../../store/facade/asset-explorer.facade';
import { EventsTabFacade } from '../../../store/facade/events-tab.facade';
import { dateFormatter } from '../formatter';

@Component({
  selector: 'atx-event-history-dialog',
  templateUrl: './event-history-dialog.component.html',
  styleUrls: ['./event-history-dialog.component.scss'],
})
export class EventHistoryDialogComponent implements OnDestroy {
  public theme$: Observable<string>;
  public history$: Observable<IEvent[]>;
  public unsubscribe$: Subject<void> = new Subject<void>();

  asset: string;
  historyData = [];
  gridApi: GridApi;
  columnApi: ColumnApi;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private assetExplorerFacade: AssetExplorerFacade,
    private eventsFacade: EventsTabFacade
  ) {
    eventsFacade.getEventHistory(data.eventID);
    this.asset = data.asset;

    this.theme$ = assetExplorerFacade.theme$;
    this.history$ = eventsFacade.eventHistory$;

    this.history$.pipe(takeUntil(this.unsubscribe$)).subscribe((events) => {
      if (this.gridApi) {
        this.gridApi.setRowData(events);
      }
    });
  }

  public modules: Module[] = [
    EnterpriseCoreModule,
    ClientSideRowModelModule,
    RangeSelectionModule,
    MenuModule,
    ClipboardModule,
  ];

  gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    rowSelection: 'single',
    enableRangeSelection: true,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      sortable: false,
      editable: false,
      suppressMenu: true,
    },
    columnDefs: [
      {
        colId: 'IsDeleted',
        headerName: 'Is Deleted',
        field: 'IsDeleted',
        width: 100,
      },
      {
        colId: 'ChangeDate',
        headerName: 'Modified Date',
        field: 'ChangeDate',
        valueFormatter: dateFormatter,
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        colId: 'ChangedBy',
        headerName: 'Modified By',
        field: 'ChangedBy',
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'AssetPath',
        tooltipValueGetter: (params) => {
          if (params.data?.AssetPath) {
            return params.data.AssetPath;
          } else {
            return this.asset;
          }
        },
        valueGetter: (params) => {
          if (params.data?.AssetPath) {
            const parts = params.data.AssetPath.split('\\');
            const lastIndex = parts
              .map((part: string) => part === this.asset)
              .lastIndexOf(true);
            if (lastIndex === -1) {
              return params.data.AssetPath.slice(0, -1);
            } else if (parts[0] === this.asset && parts.length === 1) {
              return this.asset;
            }
            return parts.slice(lastIndex, parts.length).join('\\').slice(0, -1);
          } else {
            return this.asset;
          }
        },
      },
      {
        colId: 'EventType',
        headerName: 'Event Type',
        field: 'eventType',
      },
      {
        colId: 'Impact',
        headerName: 'Impact',
        field: 'Impact',
        width: 100,
        valueGetter: (params) => {
          return params?.data?.EventTypeID === 3 ||
            params?.data?.EventTypeID === 5
            ? params?.data?.Impact.toString()
            : '--';
        },
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Description',
      },
      {
        colId: 'StartTime',
        headerName: 'Start Date/Time',
        field: 'StartTime',
        valueFormatter: dateFormatter,
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
        },
      },
      {
        colId: 'EndTime',
        headerName: 'End Date/Time',
        field: 'EndTime',
        valueGetter: (params) => {
          return !isNil(params.data.EndTime)
            ? moment(params.data.EndTime).format('MM/DD/YYYY hh:mm:ss A')
            : 'In Progress';
        },
      },
      {
        colId: 'ModifiedByEventID',
        headerName: 'Edit Cause',
        field: 'ModifiedByEventID',
        valueGetter: (params) => {
          return isNil(params.data.ModifiedByEventID)
            ? 'By User'
            : 'Overlapping Event';
        },
      },
    ],
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.columnApi.applyColumnState({
        state: [{ colId: 'ChangeDate', sort: 'desc' }],
      });

      if (!this.historyData) {
        this.gridOptions.suppressNoRowsOverlay = true;
      }
    },
    getRowId: (params) => {
      return params.data.ChangeDate;
    },
  };

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  getContextMenuItems(params) {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }
}
