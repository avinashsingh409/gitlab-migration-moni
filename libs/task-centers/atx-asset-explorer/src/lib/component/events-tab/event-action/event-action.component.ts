import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { IEvent } from '@atonix/atx-core';
import { EventHistoryDialogComponent } from '../event-history-dialog/event-history-dialog.component';
import { NewEventsDialogComponent } from '../new-events-dialog/new-events-dialog.component';

@Component({
  selector: 'atx-event-action',
  templateUrl: './event-action.component.html',
  styleUrls: ['./event-action.component.scss'],
})
export class EventActionComponent {
  actionType = null;
  asset = null;
  eventID = null;
  editEventData: IEvent;

  constructor(public dialog: MatDialog) {}

  agInit(params: any) {
    this.actionType = params.actionType;
    this.asset = params.asset;
    this.eventID = params.data.GlobalID;
    this.editEventData = params.data;
  }

  editEvent(data: IEvent) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog';
    const dialogRef = this.dialog.open(NewEventsDialogComponent, dialogConfig);
    dialogRef.componentInstance.isEditing = true;
    dialogRef.componentInstance.setFormData(data);
  }

  showHistory() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog-history';
    dialogConfig.data = {
      asset: this.asset,
      eventID: this.eventID,
    };
    const dialogRef = this.dialog.open(
      EventHistoryDialogComponent,
      dialogConfig
    );
  }
}
