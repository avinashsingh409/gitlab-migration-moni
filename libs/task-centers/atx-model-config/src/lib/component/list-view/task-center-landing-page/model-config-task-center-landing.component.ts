/* eslint-disable rxjs/no-nested-subscribe */
import {
  AfterViewInit,
  Component,
  HostListener,
  Inject,
  OnDestroy,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { Observable, of, Subject } from 'rxjs';
import {
  getEndFromRoute,
  getStartFromRoute,
  getUniqueIdFromRoute,
  NavFacade,
} from '@atonix/atx-navigation';
import { switchMap, take, takeUntil } from 'rxjs/operators';
import { ModelConfigFacade } from '../../../store/facade/model-config.facade';
import { UploadFileDialogFacade } from '../../../store/services/upload-file-dialog.facade';
import { QuickDeployServiceFacade } from '../../../store/services/quick-deploy.service.facade';
import { QuickDeployDialogComponent } from '../modals/quick-deploy-dialog/quick-deploy.dialog.component';
import {
  ImagesFrameworkService,
  ModelConfigCoreService,
  OPModeTypes,
} from '@atonix/shared/api';
import {
  ComponentCanDeactivate,
  getAMonthAgoLive,
  LoggerService,
} from '@atonix/shared/utils';
import {
  ITimeSliderState,
  ITimeSliderStateChange,
} from '@atonix/atx-time-slider';
import { AlertsActionEvent } from '../../../service/model-actions/model-actions.models';
import {
  ActionsType,
  ModelActionsConfirmationDialogComponent,
} from '../../common/modals/model-actions-confirmation-dialog/model-actions-confirmation-dialog.component';
import * as modelConfigActions from '../../../store/actions/model-config.actions';
import { Store } from '@ngrx/store';
import { UploadFileDialogComponent } from '../modals/upload-file-dialog/upload-file-dialog.component';
import { NewOpModeModalComponent } from '../op-mode-list/modals/new-op-mode-modal/new-op-mode-modal.component';
import { OpModeSummary } from '../../../service/model/op-mode';
import { createNewOpMode } from '../../../store/actions/op-mode.actions';
import { OpModeFacade } from '../../../service/op-mode/op-mode.facade';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { isNil } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-config',
  templateUrl: './model-config-task-center-landing.component.html',
  styleUrls: ['./model-config-task-center-landing.component.scss'],
})
export class ModelConfigTaskCenterLandingPageComponent
  implements AfterViewInit, OnDestroy, ComponentCanDeactivate
{
  public asset$: Observable<string>;
  public assetId$: Observable<number>;
  public assetGuid$: Observable<string>;
  public isUnitLevelAndBelow$: Observable<boolean>;
  public isAssetPdServer$: Observable<boolean>;
  public multipleModelsSelected$: Observable<boolean>;
  public editMultipleModelsEnabled$: Observable<boolean>;
  public hasSelectedModels$: Observable<boolean>;
  public qdLoading$: Observable<boolean>;
  public opModeType = OPModeTypes;
  public showRightTray$: Observable<boolean | null>;
  public timeSliderState$: Observable<ITimeSliderState | null>;
  public hasSelectedTag$: Observable<boolean>;
  public defaultOpMode$: Observable<OPModeTypes>;

  private onDestroy = new Subject<void>();
  private showOverwriteDialog$: Observable<boolean>;
  private successStatWithAssetGuid$;
  showAssetTreeDropdown = false;
  listView: 'model' | 'tag' | 'opmode' = 'model';
  connection: signalR.HubConnection | null = null;
  opModes = [
    { label: 'Startup', value: OPModeTypes.Startup },
    { label: 'Transient', value: OPModeTypes.Transient },
    { label: 'Out of Service', value: OPModeTypes.OOS },
    { label: 'Exclusion Period', value: OPModeTypes.ExclusionPeriod },
  ];

  constructor(
    private readonly store: Store,
    public modelConfigFacade: ModelConfigFacade,
    public navFacade: NavFacade,
    private activatedRoute: ActivatedRoute,
    private uploadFileDialogFacade: UploadFileDialogFacade,
    private quickDeployServiceFacade: QuickDeployServiceFacade,
    private dialog: MatDialog,
    private modelConfigCoreService: ModelConfigCoreService,
    private imagesFrameworkService: ImagesFrameworkService,
    private logger: LoggerService,
    private opModeFacade: OpModeFacade,
    private router: Router
  ) {
    this.showRightTray$ = this.navFacade.showRightTray$;
    this.timeSliderState$ = modelConfigFacade.timeSliderState$;
    this.defaultOpMode$ = modelConfigFacade.defaultOpMode$;

    this.assetId$ = modelConfigFacade.selectedAssetId$;
    this.asset$ = modelConfigFacade.selectedAsset$;
    this.assetGuid$ = modelConfigFacade.selectedAssetGuidId$;
    this.isUnitLevelAndBelow$ = modelConfigFacade.isUnitLevelAndBelow$;
    this.isAssetPdServer$ = uploadFileDialogFacade.isPdServer$;
    this.multipleModelsSelected$ = modelConfigFacade.multipleModelsSelected$;
    this.editMultipleModelsEnabled$ =
      modelConfigFacade.editMultipleModelsEnabled$;
    this.hasSelectedModels$ = modelConfigFacade.hasSelectedModels$;
    this.hasSelectedTag$ = modelConfigFacade.hasSelectedTag$;

    this.showOverwriteDialog$ =
      this.quickDeployServiceFacade.showOverwriteDialog$;
    this.successStatWithAssetGuid$ =
      this.quickDeployServiceFacade.successStatWithAssetGuid$;
    this.qdLoading$ = this.quickDeployServiceFacade.isLoading$;

    this.navFacade.user$.pipe(takeUntil(this.onDestroy)).subscribe((user) => {
      if (user) {
        this.uploadFileDialogFacade.setEmail(user.email);
      }
    });

    this.showOverwriteDialog$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((val) => {
        if (val) {
          //show dialog
          this.quickDeployServiceFacade.qdModelCreationActionResult$
            .pipe(take(1))
            .subscribe((res) => {
              const dialogRef = this.dialog.open(QuickDeployDialogComponent, {
                width: '500px',
                data: res?.OverrideModels,
                disableClose: true,
                hasBackdrop: true,
              });

              dialogRef.afterClosed().subscribe((result) => {
                if (result && result.length > 0) {
                  this.quickDeployServiceFacade.reconcileModels(result);
                }
              });
            });
        }
      });

    this.successStatWithAssetGuid$
      .pipe(takeUntil(this.onDestroy))
      .subscribe(([isSuccessful, assetGuid]) => {
        if (isSuccessful) {
          if (assetGuid) {
            this.modelConfigFacade.selectAsset(assetGuid);
            this.quickDeployServiceFacade.resetState();
          }
        }
      });
  }

  ngAfterViewInit(): void {
    let currentOpenTab = this.activatedRoute.snapshot.queryParamMap.get('tab');
    if (
      currentOpenTab === 'null' ||
      currentOpenTab === 'undefined' ||
      isNil(currentOpenTab)
    ) {
      currentOpenTab = null;
    }
    this.setTabType(currentOpenTab);
    this.changeTabType();
    this.asset$.pipe(takeUntil(this.onDestroy)).subscribe((asset) => {
      if (asset) {
        this.assetGuid$.pipe(take(1)).subscribe((assetGuid) => {
          this.uploadFileDialogFacade.setAsset(assetGuid);
        });

        this.modelConfigFacade.selectAsset(asset);

        this.assetId$.pipe(take(1)).subscribe((assetId) => {
          this.quickDeployServiceFacade.init(assetId, asset);
        });
      }
    });

    const initialId = getUniqueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const initialStartFromRoute = getStartFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const initialEndFromRoute = getEndFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.modelConfigFacade.systemInitialize(
      initialStartFromRoute
        ? new Date(parseFloat(initialStartFromRoute))
        : getAMonthAgoLive(),
      initialEndFromRoute
        ? new Date(parseFloat(initialEndFromRoute))
        : new Date(),
      initialId ? initialId : undefined
    );
  }

  @HostListener('window:beforeunload')
  canDeactivate(): Observable<boolean> {
    return this.opModeFacade.query.isDirty$.pipe(
      take(1),
      switchMap((isDirty) => {
        if (!isDirty) {
          return of(true);
        }
        return of(false);
      })
    );
  }

  onOpModeChange(opMode: OPModeTypes) {
    this.modelConfigFacade.setDefaultOpMode(opMode);
  }

  public toggleListView(event: MatButtonToggleChange) {
    const listView: 'model' | 'tag' | 'opmode' = event?.value || 'model';
    if (listView === 'opmode') {
      this.setTabType(listView);
      this.changeTabType();
    } else {
      this.opModeFacade.query.isDirty$.pipe(take(1)).subscribe((isDirty) => {
        if (isDirty) {
          this.opModeFacade.showOpModeDiscardChangesModal((dialogRef) => {
            dialogRef.afterClosed().subscribe((result) => {
              if (result) {
                this.setTabType(listView);
                this.opModeFacade.command.clearSelectedOpMode();
                this.changeTabType();
              }
              event.source.buttonToggleGroup._buttonToggles.map((button) => {
                if (button.value === this.listView) {
                  button.checked = true;
                }
              });
            });
          });
        } else {
          this.opModeFacade.command.clearSelectedOpMode();
          this.setTabType(listView);
          this.changeTabType();
        }
      });
    }
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  public onAssetTreeStateChange(change: ITreeStateChange) {
    if (change.event === 'SelectAsset') {
      this.opModeFacade.query.isDirty$.pipe(take(1)).subscribe((isDirty) => {
        if (isDirty) {
          this.opModeFacade.showOpModeDiscardChangesModal((dialogRef) => {
            dialogRef.afterClosed().subscribe((result) => {
              if (result) {
                this.opModeFacade.command.clearSelectedOpMode();
                this.modelConfigFacade.treeStateChange(change);
              }
            });
          });
        } else {
          this.modelConfigFacade.treeStateChange(change);
        }
      });
    } else {
      this.modelConfigFacade.treeStateChange(change);
    }
  }

  public onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.modelConfigFacade.treeSizeChange(newSize);
    }
  }

  public importModelFile() {
    this.logger.trackTaskCenterEvent(
      'ModelConfig:ImportCustomModels:ImportModelFile'
    );
    const dialogRef = this.dialog.open(UploadFileDialogComponent, {
      disableClose: true,
      width: '300px',
    });
  }

  public exportModelTemplete() {
    this.logger.trackTaskCenterEvent(
      'ModelConfig:ImportCustomModels:ExportModelFile'
    );
    this.modelConfigCoreService
      .downloadModelTemplate()
      .pipe(take(1))
      .subscribe((url) => {
        this.imagesFrameworkService.downloadFile(
          url.Results[0],
          'AtonixOICustomModelImportTemplate_v1.xlsx'
        );
      });
  }

  public quickDeployModels(opModeTypeID: number): void {
    this.quickDeployServiceFacade.getModelsForQuickDeploy(opModeTypeID);
  }

  public editSelectedModels() {
    this.assetGuid$.pipe(take(1)).subscribe((assetGuid) => {
      this.store.dispatch(modelConfigActions.openModels({ asset: assetGuid }));
    });
  }

  public deleteModels(): void {
    this.modelConfigFacade.selectedModels$.pipe(take(1)).subscribe((models) => {
      const dialogRef = this.dialog.open(
        ModelActionsConfirmationDialogComponent,
        {
          disableClose: true,
          width: '600px',
          data: {
            models: models.map((model) => {
              return { modelId: model.ModelID, modelName: model.ModelName };
            }),
            actionType: ActionsType.Delete,
          },
        }
      );

      dialogRef.afterClosed().subscribe((result) => {
        if (typeof result === 'boolean' && result) {
          this.asset$.pipe(take(1)).subscribe((asset) => {
            if (asset) {
              this.assetGuid$.pipe(take(1)).subscribe((assetGuid) => {
                this.uploadFileDialogFacade.setAsset(assetGuid);
              });
              this.modelConfigFacade.selectAsset(asset);
              this.assetId$.pipe(take(1)).subscribe((assetId) => {
                this.quickDeployServiceFacade.init(assetId, asset);
              });
            }
          });
        }
      });
    });
  }

  onTimeSliderStateChange(change: ITimeSliderStateChange) {
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.modelConfigFacade.timeSliderStateChange(change);
    if (change.event === 'SelectDateRange') {
      if (change?.newStartDateValue && change?.newEndDateValue) {
        // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
        this.modelConfigFacade.updateRouteTimeSlider(
          change.newStartDateValue,
          change.newEndDateValue
        );
      }
    }
  }

  modelActionChange(event: AlertsActionEvent) {
    if (event.event === 'OpenOpModeConfig') {
      this.modelConfigFacade.modelConfigActionsOpenOpMode(event.newValue);
    } else if (event.event === 'OpenDiagnosticDrilldown') {
      const dddParams = event.newValue;
      this.modelConfigFacade.modelConfigActionsOpenDiagnosticDrillDown(
        dddParams.assetGuid,
        dddParams.model,
        dddParams.start,
        dddParams.end,
        dddParams.mtype,
        dddParams.ddurl
      );
    } else if (event.event === 'OpenDataExplorer') {
      this.modelConfigFacade.modelConfigActionsOpenDataExplorer(event.newValue);
    } else if (event.event === 'ShowRelatedModels') {
      this.modelConfigFacade.modelConfigActionsShowRelatedAlerts(
        event.newValue
      );
    } else if (event.event === 'ShowRelatedIssues') {
      this.modelConfigFacade.modelConfigActionsShowRelatedIssues(
        event.newValue
      );
    } else if (event.event === 'NewIssue') {
      this.modelConfigFacade.modelConfigActionsCreateNewIssue(event.newValue);
    }
  }

  deployStandardModels() {
    this.logger.trackTaskCenterEvent('ModelConfig:DeployStandardModels');
  }

  createNewModel() {
    this.modelConfigFacade.createNewModel();
  }

  createNewOpMode() {
    this.opModeFacade.query.isDirty$.pipe(take(1)).subscribe((isDirty) => {
      if (isDirty) {
        this.opModeFacade.showOpModeDiscardChangesModal((dialogRef) => {
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              this.showCreateNewOpModeModal();
            }
          });
        });
      } else {
        this.showCreateNewOpModeModal();
      }
    });
  }

  showCreateNewOpModeModal() {
    const dialogRef = this.dialog.open(NewOpModeModalComponent, {
      width: '500px',
      height: '400px',
      disableClose: true,
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((val: OpModeSummary) => {
      if (val) {
        this.store.dispatch(createNewOpMode({ opModeSummary: val }));
      }
    });
  }

  private setTabType(tabQueryParam: string | null) {
    if (tabQueryParam === 'opmode') {
      this.listView = 'opmode';
    } else if (tabQueryParam === 'tag') {
      this.listView = 'tag';
    } else {
      this.listView = 'model';
    }
  }

  private changeTabType() {
    switch (this.listView) {
      case 'model':
        this.modelConfigFacade.treeDragDrop(false);
        this.navFacade.showRightTray();
        this.modelConfigFacade.resetTagSelection();
        break;
      case 'opmode':
        this.modelConfigFacade.treeDragDrop(true);
        this.navFacade.hideRightTray();
        break;
      case 'tag':
        this.modelConfigFacade.treeDragDrop(false);
        this.navFacade.hideRightTray();
        break;
      default:
        console.error('list type not found');
    }
    this.router.navigate([], {
      queryParams: { tab: this.listView },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
