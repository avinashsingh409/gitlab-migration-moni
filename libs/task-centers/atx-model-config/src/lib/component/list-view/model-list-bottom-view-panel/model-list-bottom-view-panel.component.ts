import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  Input,
  OnChanges,
  SimpleChanges,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { ModelEditFacade } from '../../../service/model-edit.facade';
import { Subject, takeUntil } from 'rxjs';
import { IModelConfigSummary } from '@atonix/shared/api';
import { isNilOrEmptyString } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-list-bottom-view-panel',
  templateUrl: './model-list-bottom-view-panel.component.html',
  styleUrls: ['./model-list-bottom-view-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelListBottomViewPanelComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() modelSummary!: IModelConfigSummary;
  @Input() timeSliderSelection!: any;
  @Input() collapse!: boolean;
  @Output() collapseModelTrendStuffs = new EventEmitter<boolean>();

  readonly vm$ = this.modelEditFacade.query.vm$;
  private unsubscribe$ = new Subject<void>();

  constructor(private modelEditFacade: ModelEditFacade) {}

  ngOnInit(): void {
    this.modelEditFacade.query.predictiveMethodTypeSelected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((methodType) => {
        if (
          methodType !== undefined &&
          methodType !== 'none' &&
          !isNilOrEmptyString(this.modelSummary?.ModelExtID)
        ) {
          this.modelEditFacade.getModelTrend({
            modelId: this.modelSummary?.ModelExtID,
            predictiveMethodType: methodType,
            startDate: this.timeSliderSelection.start,
            endDate: this.timeSliderSelection.end,
          });
        }
      });
    if (!isNilOrEmptyString(this.modelSummary?.ModelExtID)) {
      this.modelEditFacade.getModelMathMessage(this.modelSummary?.ModelExtID);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.modelSummary?.currentValue &&
      changes.modelSummary?.previousValue !== changes.modelSummary?.currentValue
    ) {
      if (
        changes.modelSummary.currentValue?.ModelExtID !==
        changes.modelSummary.previousValue?.ModelExtID
      ) {
        const selectedModel = changes.modelSummary
          ?.currentValue as IModelConfigSummary;
        if (!isNilOrEmptyString(this.modelSummary?.ModelExtID)) {
          this.modelEditFacade.command.resetModelEditState();
          this.modelEditFacade.getModelSummaries({
            modelIds: [selectedModel.ModelExtID],
            refreshTrends: true,
            isMultiModel: false,
          });
          this.modelEditFacade.getModelMathMessage(
            this.modelSummary?.ModelExtID
          );
        }
      }
    }

    if (
      changes.timeSliderSelection?.currentValue &&
      changes.timeSliderSelection?.previousValue !==
        changes.timeSliderSelection?.currentValue
    ) {
      this.modelEditFacade.updateModelTrendTime(
        this.timeSliderSelection.start,
        this.timeSliderSelection.end,
        this.modelSummary?.ModelExtID
      );
    }
  }

  collapseSection(): void {
    this.collapseModelTrendStuffs.emit(this.collapse);
  }

  ngOnDestroy(): void {
    this.modelEditFacade.command.resetModelEditState();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
