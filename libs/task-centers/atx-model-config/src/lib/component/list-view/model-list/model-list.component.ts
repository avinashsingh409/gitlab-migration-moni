import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  OnInit,
  OnDestroy,
} from '@angular/core';
import {
  GridOptions,
  GridReadyEvent,
  GridApi,
  ColumnApi,
  ClipboardModule,
  ClientSideRowModelModule,
  ColDef,
  FilterChangedEvent,
  RangeSelectionModule,
  ColGroupDef,
  RowDoubleClickedEvent,
  ColumnRowGroupChangedEvent,
  SortChangedEvent,
  DragStoppedEvent,
  ColumnVisibleEvent,
  ColumnPinnedEvent,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  MenuModule,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
  RowGroupingModule,
} from '@ag-grid-enterprise/all-modules';
import { GridActionsRendererComponent } from '../../ag-grid-components/grid-renderer/grid-actions-renderer.component';
import { isNil } from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import * as modelListActions from '../../../store/actions/model-list.actions';
import * as modelConfigActions from '../../../store/actions/model-config.actions';
import {
  selectModelListModels,
  selectModelListLoading,
  selectShowModelsForSelectedAssetOnly,
  selectModelListViewModel,
  selectModelListLoadingViewModel,
  selectSelectedAsset,
} from '../../../store/reducers/model-config.reducer';
import { modelListColumnDefs } from './model-list-column-defs';
import { distinctUntilChanged, take, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { dateFormatter, LoggerService } from '@atonix/shared/utils';
import { SavedConfigPanelComponent } from '../../ag-grid-components/saved-config-panel/saved-config-panel.component';
import { ListConfigService } from '../../../store/services/list-config-service';
import { GridIconRendererComponent } from '../../ag-grid-components/grid-renderer/grid-icon-renderer.component';
import {
  ModelConfigBusEventTypes,
  ModelConfigEventBus,
} from '../../../store/services/model-config-event-bus';
import { IModelConfigSummary } from '@atonix/shared/api';
import { NavFacade } from '@atonix/atx-navigation';
import { WatchActionParam } from '../../../service/model-actions/model-actions.models';
import { ModelConfigFacade } from '../../../store/facade/model-config.facade';

@Component({
  selector: 'atx-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelListComponent implements OnInit, OnDestroy {
  @Input() listTheme!: string;
  @ViewChild('modelListGrid') modelListGrid!: ElementRef;

  collapseBottomSection = false;

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;

  readonly vm$ = this.store.select(selectModelListViewModel);
  readonly models$ = this.store.select(selectModelListModels);
  readonly showModelsForSelectedAssetOnly$ = this.store.select(
    selectShowModelsForSelectedAssetOnly
  );
  public multipleModelsSelected$: Observable<boolean>;
  public unsubscribe$ = new Subject<void>();

  public columnDefs: (ColDef | ColGroupDef)[];
  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
    RowGroupingModule,
  ];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading Models</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    enableRangeSelection: true,
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    rowHeight: 30,
    suppressCopyRowsToClipboard: true,
    tooltipShowDelay: 0,
    groupDisplayType: 'singleColumn',
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      cellClass: 'cell-selectable',
      enableRowGroup: false,
    },
    icons: {
      column:
        '<span class="ag-icon ag-icon-column " style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
      'my-views':
        '<span class="ag-icon ag-icon-my-views" style="background: url(assets/my-views.svg) no-repeat center;" data-cy="sideButtonMyViews"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
        {
          id: 'views',
          labelDefault: 'My Views',
          labelKey: 'my-views',
          iconKey: 'my-views',
          toolPanel: 'saveToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    suppressAggFuncInHeader: true,
    components: {
      saveToolPanel: SavedConfigPanelComponent,
      actionsCellRenderer: GridActionsRendererComponent,
      iconCellRenderer: GridIconRendererComponent,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.modelListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-column'
      ).title = 'Columns';
      this.modelListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-filters'
      ).title = 'Filters';
      this.modelListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-my-views'
      ).title = 'My Views';
      if (this.columnApi && this.gridApi) {
        this.listConfigService.getCachedState(this.gridApi, this.columnApi);
      }
    },
    getRowId: (params) => {
      return params.data.ModelID.toString();
    },
    onDragStopped: (event: DragStoppedEvent) => {
      if (event) {
        this.listConfigService.updateCachedState(event);
      }
    },
    onColumnVisible: (event: ColumnVisibleEvent) => {
      if (event) {
        this.listConfigService.updateCachedState(event);
      }
    },
    onColumnPinned: (event: ColumnPinnedEvent) => {
      if (event) {
        this.listConfigService.updateCachedState(event);
      }
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (event) {
        this.getFilters();
        this.listConfigService.updateCachedState(event);
      }
    },
    onModelUpdated: (event: any) => {
      if (event && !isNil(this.gridApi)) {
        this.store
          .select(selectModelListLoading)
          .pipe(take(1))
          .subscribe((isLoading) => {
            if (isLoading) {
              this.gridApi.showLoadingOverlay();
            }
          });
        const rowCount =
          event?.api?.rowModel?.rootNode?.allChildrenCount ??
          this.gridApi.getDisplayedRowCount();
        this.store.dispatch(
          modelListActions.updateTotalModels({
            totalModels: rowCount,
          })
        );
      }
    },
    onSortChanged: (event: SortChangedEvent) => {
      if (event) {
        this.listConfigService.updateCachedState(event);
      }
    },
    onColumnRowGroupChanged: (event: ColumnRowGroupChangedEvent) => {
      if (event.column) {
        this.listConfigService.updateCachedState(event);
      }
    },
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => {
      if (event.data) {
        const modelExtID = event.data?.ModelExtID;
        const assetGUID = event.data?.AssetGUID;
        const isStandard = event.data?.IsStandard;
        if (modelExtID) {
          this.store.dispatch(
            modelConfigActions.openModel({
              model: modelExtID,
              asset: assetGUID,
              isStandard,
            })
          );
        }
      }
    },
    onSelectionChanged: (event) => {
      let selectedModels: IModelConfigSummary[] = [];

      if (this.gridApi.getSelectedNodes()?.length > 0) {
        const selectedNodes = this.gridApi.getSelectedNodes();
        selectedNodes.sort((a: any, b: any) => {
          return a.rowIndex - b.rowIndex;
        });
        selectedModels = selectedNodes.map((nodes: any) => nodes.data);
      }

      this.store.dispatch(
        modelListActions.modelListSelectionChange({ selectedModels })
      );
    },
    onFirstDataRendered: () => {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        if (!isNil(this.gridApi) && vm.selectedModelGuids?.length > 0) {
          this.gridApi.forEachNode((node) => {
            if (vm.selectedModelGuids.indexOf(node.data.ModelExtID) !== -1) {
              node.setSelected(true);
            }
          });
        }
      });
    },
  };

  constructor(
    private readonly store: Store,
    private readonly listConfigService: ListConfigService,
    private modelConfigEventBus: ModelConfigEventBus,
    public navFacade: NavFacade,
    public modelConfigFacade: ModelConfigFacade,
    private logger: LoggerService
  ) {
    this.logger.tabClicked('Model List', 'Model Config');

    this.columnDefs = modelListColumnDefs;
    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.LOAD_SAVED_CONFIG,
      this.loadSavedConfig.bind(this)
    );
    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.RESET_TO_DEFAULTS,
      this.resetToDefaults.bind(this)
    );
    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.MODEL_ACTION_STATE_CHANGE,
      this.updateModels.bind(this)
    );

    this.multipleModelsSelected$ = modelConfigFacade.multipleModelsSelected$;
  }

  resetToDefaults() {
    if (
      this.gridOptions &&
      this.gridOptions.columnApi &&
      this.gridOptions.api
    ) {
      this.gridOptions.columnApi.resetColumnState();
      this.gridOptions.columnApi.applyColumnState({
        state: [{ colId: 'ModelName', sort: 'asc' }],
      });
      this.gridOptions.columnApi.resetColumnGroupState();
      this.gridOptions.api.setFilterModel({
        Active: {
          values: ['true'],
          filterType: 'set',
        },
      });
      this.refreshGrid();
    }
  }

  loadSavedConfig(config: any) {
    if (
      this.gridOptions &&
      this.gridOptions.columnApi &&
      this.gridOptions.api
    ) {
      if (!isNil(config?.listState)) {
        if (config.listState.filter) {
          this.gridOptions.api.setFilterModel(config.listState.filter);
        }
        if (config.listState.columns) {
          this.gridOptions.columnApi.applyColumnState({
            state: config.listState.columns,
            applyOrder: true,
          });
        }
        if (config.listState.groups) {
          this.gridOptions.columnApi.setColumnGroupState(
            config.listState.groups
          );
        }
        if (config.listState.sort) {
          this.gridOptions.columnApi.applyColumnState({
            state: config.listState.sort,
          });
        }
      } else {
        this.gridOptions.api.setFilterModel({
          Active: {
            values: ['true'],
            filterType: 'set',
          },
        });
      }
    }
  }

  ngOnInit(): void {
    this.store
      .select(selectSelectedAsset)
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        if (asset) {
          this.store.dispatch(modelListActions.getModelList());
        }
      });
    this.store
      .select(selectModelListLoadingViewModel)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((vm) => {
        if (!isNil(this.gridApi)) {
          // This will reset grid messages
          this.setNoRowsTemplate('<div></div>');
          this.gridApi.hideOverlay();
          if (vm.loading) {
            this.gridApi.showLoadingOverlay();
          } else {
            const errorMessage = vm.errorMessage;
            if (errorMessage) {
              if (errorMessage === 'Cannot Display At This Level') {
                this.setNoRowsTemplate(
                  '<div class="ag-overlay-loading-center"><span class="ag-icon ag-icon-column select-asset" style="background: url(assets/select-asset.svg) no-repeat center;"></span></span>Cannot display models at this level<br/> Please select an asset lower in the asset tree</div>'
                );
              } else {
                this.setNoRowsTemplate(errorMessage);
              }
              this.gridApi.showNoRowsOverlay();
            }
          }
        }
      });
  }

  setNoRowsTemplate(message: string) {
    (this.gridApi as any).gridOptionsWrapper.setProperty(
      'overlayNoRowsTemplate',
      message
    );
  }

  getContextMenuItems(params: any): string[] {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];
    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef | null = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef?.headerName,
      });
    });
    this.store.dispatch(modelListActions.setModelListFilters({ filters }));
  }

  removeFilter(filter: any): void {
    this.store.dispatch(
      modelListActions.removeModelListFilter({
        filter: filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName)?.setModel(null);
    this.refreshGrid();
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key)?.setModel(null);
    });
    this.store.dispatch(modelListActions.setModelListFilters({ filters: [] }));
    this.refreshGrid();
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  modelListPreferenceChange() {
    this.logger.trackTaskCenterEvent('ModelConfig:ModelList:PreferenceChange');

    this.store.dispatch(modelListActions.modelListPreferenceChange());
  }

  downloadModels() {
    this.logger.trackTaskCenterEvent('ModelConfig:ModelList:DownloadModels');

    const params = {
      fileName: 'ModelList',
      processCellCallback: (cellParams: any) => {
        if (
          cellParams?.column?.colId === 'LastBuildDate' ||
          cellParams?.column?.colId === 'LastUpdateDate'
        ) {
          return dateFormatter(cellParams);
        } else return cellParams.value;
      },
    };
    this.gridApi.exportDataAsCsv(params);
  }

  toggleRightTray() {
    this.navFacade.showRightTray$.pipe(take(1)).subscribe((show) => {
      if (show) {
        this.logger.trackTaskCenterEvent('ModelConfig:ActionPane:Hide');
      } else {
        this.logger.trackTaskCenterEvent('ModelConfig:ActionPane:Expand');
      }
    });

    this.navFacade.toggleRightTray();
    this.modelConfigFacade.treeSizeChange(300);
  }

  collapseModelTrendStuffs(collapse: boolean) {
    this.store.dispatch(modelListActions.collapseBottomSection());
  }

  updateModels(data: WatchActionParam) {
    if (this.gridApi && data && data.models && data.models.length > 0) {
      if (data?.models) {
        for (const i of data.models) {
          const node = this.gridApi.getRowNode(String(i.ModelID));
          if (node) {
            let newData = node.data as IModelConfigSummary;
            if (
              data.actionType === 'savemaintenance' ||
              data.actionType === 'clearmaintenance'
            ) {
              newData = {
                ...newData,
                ActionModelMaintenance: i.ActionModelMaintenance,
              };
            }

            node.setData(newData);
          }
        }
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
