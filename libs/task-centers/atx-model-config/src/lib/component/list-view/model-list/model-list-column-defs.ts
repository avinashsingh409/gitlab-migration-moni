import {
  ColDef,
  ColGroupDef,
  ValueGetterParams,
} from '@ag-grid-enterprise/all-modules';
import { dateComparator } from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';
import { modelBoundValueFormatter } from '../../ag-grid-components/formatter';

export const modelListColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'Active',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/atx-active.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Active',
    headerTooltip: 'Active',
    field: 'Active',
    cellRenderer: 'iconCellRenderer',
    cellRendererParams: {
      type: 'active',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.Active ?? false);
    },
    floatingFilter: false,
  },
  {
    colId: 'Standard',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/atx-std-model.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Standard',
    headerTooltip: 'Standard',
    field: 'IsStandard',
    cellRenderer: 'iconCellRenderer',
    cellRendererParams: {
      type: 'standard',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.IsStandard ?? false);
    },
    floatingFilter: false,
  },
  {
    colId: 'ActionMaintenance',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/emblem-maintainence.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Maintenance',
    headerTooltip: 'Maintenance',
    field: 'ActionModelMaintenance',
    cellRenderer: 'actionsCellRenderer',
    cellRendererParams: {
      tooltip: 'Maintenance',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.ActionModelMaintenance ?? false);
    },
    floatingFilter: false,
  },

  {
    colId: 'MaintenanceAge',
    headerName: 'Age',
    width: 80,
    headerTooltip: 'How many days has this model been flagged for maintenance?',
    field: 'MaintenanceAge',
    sortable: true,
    filter: 'agNumberColumnFilter',
  },
  {
    colId: 'ThirtyDayCountOfActions',
    headerName: 'Actions',
    width: 100,
    headerTooltip:
      'How many user actions have been taken on a model in the past 30 days? Updated daily.',
    field: 'ThirtyDayCountOfActions',
    sortable: true,
    filter: 'agNumberColumnFilter',
  },
  {
    sort: 'asc',
    colId: 'ModelName',
    headerName: 'Model Name',
    width: 620,
    field: 'ModelName',
    sortable: true,
    tooltipField: 'ModelName',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'ModelTypeAbbrev',
    headerName: 'Model Type',
    field: 'ModelTypeAbbrev',
    sortable: true,
    tooltipField: 'ModelTypeAbbrev',
    filter: 'agSetColumnFilter',
    filterParams: {
      values: [
        'APR',
        'Rolling Average',
        'Fixed Limit',
        'Frozen Data',
        'External',
        'Moving Average',
        'Forecast',
        'Rate of Change',
      ],
    },
    enableRowGroup: true,
  },
  {
    colId: 'PrioritizedOpMode',
    headerName: 'Operating Mode',
    field: 'PrioritizedOpMode',
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['Steady State', 'Startup', 'Transient', 'Out of Service'],
    },
    valueGetter: (params: ValueGetterParams) => {
      if (params.data) {
        return params.data['PrioritizedOpMode'].split(', ');
      } else return null;
    },
  },
  {
    colId: 'Score',
    headerName: 'Score',
    field: 'Score',
    width: 90,
    sortable: true,
    filter: 'agNumberColumnFilter',
  },
  {
    colId: 'LowerBoundTolerance',
    headerName: 'Lower Bound Tolerance',
    width: 155,
    field: 'LowerBoundTolerance',
    headerTooltip:
      'Lower Bound Sensitivity = Lower Boundary Distance/(0.5 * Interquartile Range of the Actual Value)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    filterParams: {
      inRangeInclusive: true,
    },
    valueFormatter: modelBoundValueFormatter,
    filterValueGetter: (params: ValueGetterParams) => {
      return !isNaN(params?.data?.LowerBoundTolerance) &&
        params?.data?.LowerBoundTolerance > 0
        ? Number((+params.data.LowerBoundTolerance * 100).toFixed(1))
        : '';
    },
  },
  {
    colId: 'UpperBoundTolerance',
    headerName: 'Upper Bound Tolerance',
    width: 155,
    field: 'UpperBoundTolerance',
    headerTooltip:
      'Upper Bound Sensitivity = Upper Boundary Distance/(0.5 * Interquartile Range of the Actual Value)',
    sortable: true,
    filter: 'agNumberColumnFilter',
    filterParams: {
      inRangeInclusive: true,
    },
    valueFormatter: modelBoundValueFormatter,
    filterValueGetter: (params: ValueGetterParams) => {
      return !isNaN(params?.data?.UpperBoundTolerance) &&
        params?.data?.UpperBoundTolerance > 0
        ? Number((+params.data.UpperBoundTolerance * 100).toFixed(1))
        : '';
    },
  },
  {
    colId: 'LastBuildStatus',
    headerName: 'Last Build Status',
    field: 'LastBuildStatus',
    sortable: true,
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'LastBuildDate',
    headerName: 'Last Build Date',
    field: 'LastBuildDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'LastUpdateDate',
    headerName: 'Last Update Date',
    field: 'LastUpdateDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'TagName',
    headerName: 'Tag Name',
    width: 240,
    field: 'TagName',
    sortable: true,
    tooltipField: 'TagName',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'EngUnits',
    headerName: 'Units',
    width: 106,
    field: 'EngUnits',
    sortable: true,
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'VariableTypeDesc',
    headerName: 'Variable',
    width: 240,
    field: 'VariableTypeDesc',
    sortable: true,
    tooltipField: 'VariableTypeDesc',
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'UnitAbbrev',
    headerName: 'Unit',
    width: 195,
    field: 'UnitAbbrev',
    sortable: true,
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'AssetDesc',
    headerName: 'Asset',
    field: 'AssetDesc',
    width: 255,
    sortable: true,
    tooltipField: 'AssetDesc',
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'AssetTypeDesc',
    headerName: 'Asset Class',
    field: 'AssetTypeDesc',
    width: 255,
    sortable: true,
    tooltipField: 'AssetTypeDesc',
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'AssetCriticality',
    headerName: 'Asset Criticality',
    field: 'AssetCriticality',
    width: 150,
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: [1, 2, 3],
    },
    enableRowGroup: true,
  },
  {
    colId: 'TagGroup',
    headerName: 'Asset Path (Tag Group)',
    field: 'TagGroup',
    sortable: true,
    tooltipField: 'TagGroup',
    filter: 'agTextColumnFilter',
    hide: true,
  },
  {
    colId: 'ExternalID',
    headerName: 'External ID',
    field: 'ModelExtID',
    sortable: false,
    tooltipField: 'ModelExtID',
    filter: 'agTextColumnFilter',
    hide: true,
  },
];
