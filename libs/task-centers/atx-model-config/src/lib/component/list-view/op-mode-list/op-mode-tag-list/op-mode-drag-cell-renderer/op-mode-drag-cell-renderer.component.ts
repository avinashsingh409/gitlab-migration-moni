import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';

@Component({
  selector: 'atx-op-mode-drag-cell-renderer',
  template: `
    <div class="dragTip" #dragTipContainer>
      {{ dragTip }}
    </div>
    <div>
      <div
        class="draggableTag"
        [ngClass]="{ draggable: draggable }"
        [draggable]="draggable"
        (dragstart)="onDragStart($event)"
        data-cy="cellText"
      >
        {{ value }}
      </div>
    </div>
  `,
  styleUrls: ['./op-mode-drag-cell-renderer.component.scss'],
})
export class OpModeDragCellRendererComponent
  implements ICellRendererAngularComp, OnDestroy
{
  @ViewChild('dragTipContainer') dragTipContainer!: ElementRef;
  public unsubscribe$ = new Subject<void>();
  params: any;
  dragName = '';
  value = '';
  dragTip = '';
  draggable = true;

  agInit(params: any) {
    this.params = params;
    this.value = params.value;
    this.dragName = params.dragName;
  }

  refresh(params: any): boolean {
    this.params = params;
    return true;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onDragStart(dragEvent: DragEvent) {
    let selectedRows = [];
    const selectedNodes = this.params.api.getSelectedNodes();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;

    if (selectedNodes?.length > 0) {
      selectedNodes.sort((a: any, b: any) => {
        return a.rowIndex - b.rowIndex;
      });
      selectedRows = selectedNodes.map((nodes: any) => nodes.data);
    }

    this.dragTip = this.params.dragName;
    if (this.dragTip.length > 25) {
      this.dragTip = this.dragTip.substring(0, 20) + '...';
    }

    // This will set the dragTip
    dragEvent?.dataTransfer?.setDragImage(
      this.dragTipContainer.nativeElement,
      0,
      0
    );

    // This will unselect all other selected nodes when a new SINGLE(not accumulative) node is selected
    if (
      selectedRows?.length === 0 ||
      selectedRows.findIndex(
        (row: any) => row.TagID === this.params.data.TagID
      ) === -1
    ) {
      selectedRows = [this.params.data];
      this.params.node.setSelected(true, true);
    }

    // This will send the data to the drop area
    dragEvent.dataTransfer?.setData(
      isIE ? 'text' : 'text/plain',
      JSON.stringify(selectedRows)
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
