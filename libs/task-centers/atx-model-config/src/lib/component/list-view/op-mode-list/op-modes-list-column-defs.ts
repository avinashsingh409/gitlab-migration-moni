import {
  ColDef,
  ColGroupDef,
  ValueGetterParams,
} from '@ag-grid-enterprise/all-modules';
import { dateComparator } from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';

export function getContextMenuItems(params: any): string[] {
  const result = ['copy', 'copyWithHeaders'];
  return result;
}

export const opModeListColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'opModeDefinitionID',
    headerName: 'ID',
    width: 320,
    field: 'opModeDefinitionID',
    flex: 1,
    filter: 'agTextColumnFilter',
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    colId: 'opModeDefinitionTitle',
    headerName: 'Operating Mode Name',
    width: 320,
    field: 'opModeDefinitionTitle',
    sortable: true,
    flex: 1,
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'opModeTypeAbbrev',
    headerName: 'Operating Mode Type',
    field: 'opModeTypeAbbrev',
    sortable: true,
    flex: 2,
    filter: 'agTextColumnFilter',
  },
  {
    sort: 'desc',
    colId: 'createDate',
    headerName: 'Creation Date',
    field: 'createDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'changeDate',
    headerName: 'Update Date',
    field: 'changeDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'opModeDefinitionID',
    headerName: 'OpMode Definition ID',
    field: 'opModeDefinitionID',
    sortable: false,
    filter: 'agTextColumnFilter',
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    colId: 'opModeDefinitionExtID',
    headerName: 'OpMode Definition Ext ID',
    field: 'opModeDefinitionExtID',
    sortable: false,
    filter: 'agTextColumnFilter',
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    colId: 'opModeTypeID',
    headerName: 'OpMode Type ID',
    field: 'opModeTypeID',
    sortable: false,
    filter: 'agTextColumnFilter',
    hide: true,
    suppressColumnsToolPanel: true,
  },
];
