import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  FormArray,
  FormGroup,
} from '@angular/forms';
import { IAssetVariableTypeTagMap } from '@atonix/atx-core';
import { Subject } from 'rxjs';
import {
  CriteriaGroup,
  CriteriaLogic,
} from '../../../../service/model/op-mode';
import { OpModeFacade } from '../../../../service/op-mode/op-mode.facade';
import {
  ELogicalOperators,
  EOpModeCriteriaViewType,
} from '../../../../service/op-mode/op-mode.models';

@Component({
  selector: 'atx-op-mode-criteria',
  templateUrl: './op-mode-criteria.component.html',
  styleUrls: ['./op-mode-criteria.component.scss'],
})
export class OpModeCriteriaComponent implements OnDestroy, OnInit {
  public readonly vm$ = this.opModeFacade.query.vm$;
  @Input() criteriaViewType!: EOpModeCriteriaViewType;
  eCriteriaViewType = EOpModeCriteriaViewType;
  public parentForm: AbstractControl<any, any> | null = null;
  unsubscribe$ = new Subject<void>();
  logicalOperators = Object.keys(ELogicalOperators).map(
    (itm: string) => ELogicalOperators[itm as keyof typeof ELogicalOperators]
  );

  get startCriteriaFormArray(): FormArray {
    return this.parentForm?.get('startCriteria') as FormArray;
  }

  get endCriteriaFormArray(): FormArray {
    return this.parentForm?.get('endCriteria') as FormArray;
  }

  constructor(
    public opModeFacade: OpModeFacade,
    private parentControl: ControlContainer
  ) {}
  ngOnInit(): void {
    this.parentForm = this.parentControl.control;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  logicOperatorChange(val: string, groupIdx: number, logicIdx: number) {
    this.opModeFacade.command.setCriteriaLogicOperator({
      groupIdx,
      logicIdx,
      logicOperator: val as ELogicalOperators,
      criteriaType: this.criteriaViewType,
    });
  }

  addCriteriaLogic(groupIdx: number) {
    this.opModeFacade.command.addCriteriaLogic({
      groupIdx,
      criteriaType: this.criteriaViewType,
    });
  }

  criteriaLogicID(index: number, criteriaLogic: CriteriaLogic) {
    return criteriaLogic.criteriaLogicID;
  }

  criteriaGroupID(index: number, criteriaGroup: CriteriaGroup) {
    return criteriaGroup.criteriaGroupID;
  }

  onDragOver(event: any) {
    const types = event.dataTransfer.types;
    const dragSupported = types.length;
    if (dragSupported) {
      event.dataTransfer.dropEffect = 'move';
    }
    event.preventDefault();
  }

  onDrop(event: any, groupIdx: number, logicIdx: number) {
    event.preventDefault();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;
    const transferedData = event.dataTransfer.getData(
      isIE ? 'text' : 'text/plain'
    );
    const assetVariableTypeTagMaps: IAssetVariableTypeTagMap[] =
      JSON.parse(transferedData);
    if (
      assetVariableTypeTagMaps[0]?.Tag &&
      assetVariableTypeTagMaps[0]?.TagID &&
      assetVariableTypeTagMaps[0]?.Tag?.EngUnits &&
      assetVariableTypeTagMaps[0].AssetVariableTypeTagMapID
    ) {
      this.opModeFacade.command.addCriteriaLogicTag({
        groupIdx,
        logicIdx,
        tagDesc: assetVariableTypeTagMaps[0]?.Tag?.TagDesc,
        tagName: assetVariableTypeTagMaps[0].Tag?.TagName,
        tagID: assetVariableTypeTagMaps[0]?.TagID,
        tagUnits: assetVariableTypeTagMaps[0].Tag.EngUnits,
        assetVariableTypeTagMapID:
          assetVariableTypeTagMaps[0].AssetVariableTypeTagMapID,
        criteriaType: this.criteriaViewType,
      });
    }
  }

  addNewCriteria() {
    this.opModeFacade.command.addCriteria({
      criteriaType: this.criteriaViewType,
    });
  }

  deleteCriteria(idx: number) {
    this.opModeFacade.command.deleteCriteria({
      criteriaType: this.criteriaViewType,
      idx,
    });
  }

  deleteLogic(groupIdx: number, logicIdx: number) {
    this.opModeFacade.command.deleteLogic({
      groupIdx,
      logicIdx,
      criteriaType: this.criteriaViewType,
    });
  }
}
