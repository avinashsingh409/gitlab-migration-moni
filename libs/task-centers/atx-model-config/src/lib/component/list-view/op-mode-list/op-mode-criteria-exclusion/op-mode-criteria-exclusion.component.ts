import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { OpModeFacade } from '../../../../service/op-mode/op-mode.facade';
import { ELogicalOperators } from '../../../../service/op-mode/op-mode.models';

@Component({
  selector: 'atx-op-mode-criteria-exclusion',
  templateUrl: './op-mode-criteria-exclusion.component.html',
  styleUrls: ['./op-mode-criteria-exclusion.component.scss'],
})
export class OpModeCriteriaExclusionComponent implements OnDestroy, OnInit {
  public readonly vm$ = this.opModeFacade.query.vm$;
  public parentForm: AbstractControl<any, any> | null = null;
  unsubscribe$ = new Subject<void>();

  get exclusionCriteriaFormGroup(): FormGroup {
    return this.parentForm?.get('exclusionCriteria') as FormGroup;
  }

  constructor(
    public opModeFacade: OpModeFacade,
    private parentControl: ControlContainer
  ) {}
  ngOnInit(): void {
    this.parentForm = this.parentControl.control;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  startDateChanged(event: Date) {
    this.opModeFacade.command.setExclusionPeriodStartDate({ startDate: event });
  }

  endDateChanged(event: Date) {
    this.opModeFacade.command.setExclusionPeriodEndDate({ endDate: event });
  }
}
