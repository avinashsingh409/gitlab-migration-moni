import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { OpModeFacade } from '../../../../../service/op-mode/op-mode.facade';
import { take, switchMap, map, EMPTY, filter, withLatestFrom } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { OpModeFormService } from '../../../../../service/op-mode/op-mode-form.service';
import { OpModeWithModelsForMaintenanceParam } from '../../../../../service/op-mode/op-mode.models';

@Component({
  selector: 'atx-op-mode-save-dialog',
  templateUrl: './op-mode-save-modal.component.html',
  styleUrls: ['./op-mode-save-modal.component.scss'],
})
export class OpModeSaveModalComponent {
  readonly validationData$ = this.opModeFacade.query.saveValidationResult$;
  displayedColumns: string[] = ['name', 'path'];
  setModelMaintenance = false;

  maintenanceNote: FormControl;
  constructor(
    private opModeFacade: OpModeFacade,
    private opModeFormService: OpModeFormService,
    private dialogRef: MatDialogRef<OpModeSaveModalComponent>
  ) {
    this.maintenanceNote = this.opModeFormService.maintenanceNote();
    this.maintenanceNote.disable();
  }

  setMaintenance() {
    this.setModelMaintenance = !this.setModelMaintenance;
    if (this.setModelMaintenance) {
      this.maintenanceNote.enable();
      this.maintenanceNote.setValidators([Validators.required]);
      this.maintenanceNote.updateValueAndValidity();
    } else {
      this.maintenanceNote.setErrors(null);
      this.maintenanceNote.disable();
    }
  }
  saveOpMode() {
    if (this.setModelMaintenance) {
      this.opModeFacade.query.selectedOpMode$
        .pipe(
          take(1),
          withLatestFrom(this.validationData$),
          map(([opMode, validationData]) => {
            const modelExtIds =
              validationData?.affectedModels?.map((x) => x.modelExtID) ?? [];
            return {
              opMode: opMode,
              maintenanceNote: this.maintenanceNote.value,
              affectedModelGuids: modelExtIds,
            } as OpModeWithModelsForMaintenanceParam;
          })
        )
        .subscribe((data) => {
          this.opModeFacade.command.saveOpModeWithModelsForMaintenance(data);
        });
    } else {
      this.opModeFacade.query.selectedOpMode$
        .pipe(take(1))
        .subscribe((opMode) => {
          if (opMode) {
            this.opModeFacade.command.saveOpMode({ opMode });
          }
        });
    }

    this.dialogRef.close();
  }
}
