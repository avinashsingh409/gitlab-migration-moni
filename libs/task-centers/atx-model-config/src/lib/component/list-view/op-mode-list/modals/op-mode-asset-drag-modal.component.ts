import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-op-mode-asset-drag-modal',
  templateUrl: './op-mode-asset-drag-modal.component.html',
  styleUrls: ['./op-mode-asset-drag-modal.component.scss'],
})
export class OpModeAssetDragModalComponent {
  constructor(public dialogRef: MatDialogRef<OpModeAssetDragModalComponent>) {}

  onNoClick() {
    this.dialogRef.close();
  }

  selectAsset() {
    this.dialogRef.close({ response: 'asset' });
  }

  selectDescendants() {
    this.dialogRef.close({ response: 'descendants' });
  }
}
