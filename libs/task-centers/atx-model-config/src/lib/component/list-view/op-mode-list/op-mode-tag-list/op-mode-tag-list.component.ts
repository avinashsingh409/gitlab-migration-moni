import {
  ClipboardModule,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  ICellRendererParams,
  MenuModule,
  Module,
  ServerSideRowModelModule,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { NavFacade } from '@atonix/atx-navigation';
import { distinctUntilChanged, Subject, takeUntil } from 'rxjs';
import { OpModeTagListDataSource } from './op-mode-tag-list.datasource';
import { OpModeFacade } from '../../../../service/op-mode/op-mode.facade';
import { OpModeDragCellRendererComponent } from './op-mode-drag-cell-renderer/op-mode-drag-cell-renderer.component';

@Component({
  selector: 'atx-op-mode-tag-list',
  templateUrl: './op-mode-tag-list.component.html',
  styleUrls: ['./op-mode-tag-list.component.scss'],
  providers: [OpModeTagListDataSource],
})
export class OpModeTagListComponent implements AfterViewInit, OnDestroy {
  asset!: string;
  searchAsset!: number;
  showAssetTreeDropdown = false;
  tagsSelectedAssetOnly = false;

  readonly assetName$ = this.opModeFacade.query.assetName$;
  readonly assetTreeConfiguration$ =
    this.opModeFacade.query.assetTreeConfiguration$;

  gridApi!: GridApi;
  columnApi!: ColumnApi;
  modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    ServerSideRowModelModule,
    SetFilterModule,
  ];
  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: 'partial',
    rowGroupPanelShow: 'never',
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    headerHeight: 30,
    rowHeight: 30,
    floatingFiltersHeight: 30,
    getContextMenuItems: (params) => {
      return ['copy', 'copyWithHeaders'];
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: false,
    },
    components: {
      dragCellRenderer: OpModeDragCellRendererComponent,
    },
    columnDefs: [
      {
        colId: 'AssetID',
        headerName: 'AssetID',
        field: 'AssetID',
        sortable: false,
        hide: true,

        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SearchAssetID',
        headerName: 'SearchAssetID',
        field: 'SearchAssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SelectedAssetOnly',
        headerName: 'SelectedAssetOnly',
        field: 'SelectedAssetOnly',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'Asset.AssetAbbrev',
        filter: 'agTextColumnFilter',
        width: 180,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Asset?.AssetAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              dragName: params?.data?.Tag?.TagName,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Variable',
        headerName: 'Variable',
        field: 'VariableType.VariableAbbrev',
        filter: 'agTextColumnFilter',
        width: 175,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.VariableType?.VariableAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              dragName: params?.data?.Tag?.TagName,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Name',
        headerName: 'Name',
        field: 'Tag.TagName',
        filter: 'agTextColumnFilter',
        width: 325,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.TagName;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              dragName: params?.data?.Tag?.TagName,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Tag.TagDesc',
        filter: 'agTextColumnFilter',
        width: 680,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.TagDesc;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              dragName: params?.data?.Tag?.TagName,
              type: 'text',
            },
          } as any;
        },
      },
      {
        colId: 'Units',
        headerName: 'Units',
        field: 'Tag.EngUnits',
        filter: 'agTextColumnFilter',
        width: 90,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params?.data?.Tag?.EngUnits;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              dragName: params?.data?.Tag?.TagName,
              type: 'text',
            },
          } as any;
        },
      },
    ],
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.gridApi.setServerSideDatasource(this.gridDataSource);
    },
    getRowId: (params) => {
      if (params.data) {
        return params.data.AssetVariableTypeTagMapID;
      }
      return null;
    },
  };
  unsubscribe$ = new Subject<void>();
  constructor(
    private gridDataSource: OpModeTagListDataSource,
    public opModeFacade: OpModeFacade,
    public navFacade: NavFacade
  ) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngAfterViewInit(): void {
    this.opModeFacade.tagListNavSelectedIds$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((ids) => {
        if (
          ids &&
          this.asset !== ids?.assetUniqueKey &&
          this.searchAsset !== ids?.assetId
        ) {
          this.opModeFacade.command.resetTagListFilterIds();

          this.opModeFacade.updateSearchAssetID({
            assetUniqueKey: String(ids?.assetUniqueKey),
            searchAssetId: Number(ids?.assetId),
          });

          this.asset = String(ids?.assetUniqueKey);
        }
      });

    this.opModeFacade.query.searchAssetID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetId) => {
        if (assetId) {
          this.searchAsset = Number(assetId);
          this.updateAssetAndSearchAsset(
            this.asset,
            this.searchAsset,
            this.tagsSelectedAssetOnly
          );
        }
      });
  }

  updateAssetAndSearchAsset(
    asset: string,
    searchAsset: number,
    selectedAssetOnly: boolean
  ) {
    if (this.gridOptions && this.gridOptions.api && asset && searchAsset) {
      this.gridOptions.api?.setFilterModel(null);

      const assetIdFilter = this.gridOptions.api.getFilterInstance('AssetID');
      const searchAssetIdFilter =
        this.gridOptions?.api?.getFilterInstance('SearchAssetID');
      const selectedAssetOnlyFilter =
        this.gridOptions.api.getFilterInstance('SelectedAssetOnly');
      if (
        asset !== assetIdFilter?.getModel()?.filter ||
        searchAsset !== searchAssetIdFilter?.getModel()?.filter ||
        selectedAssetOnly.toString() !==
          selectedAssetOnlyFilter?.getModel()?.filter
      ) {
        assetIdFilter?.setModel({ type: 'asset', filter: asset });
        searchAssetIdFilter?.setModel({
          type: 'searchAsset',
          filter: searchAsset.toString(),
        });
        selectedAssetOnlyFilter?.setModel({
          type: 'selectedAssetOnly',
          filter: selectedAssetOnly ? 'true' : 'false',
        });
        this.gridOptions.api.onFilterChanged();
      }
    }
  }

  editAsset(): void {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  closeSidePanelAssetTree() {
    this.showAssetTreeDropdown = false;
  }

  sidePanelAssetTreeStateChange(change: ITreeStateChange) {
    this.opModeFacade.tagListSidePanelAssetTreeStateChange(change);
  }

  tagsPreferenceChange() {
    this.tagsSelectedAssetOnly = !this.tagsSelectedAssetOnly;
    this.updateAssetAndSearchAsset(
      this.asset,
      this.searchAsset,
      this.tagsSelectedAssetOnly
    );
  }
}
