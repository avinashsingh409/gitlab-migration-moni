/* eslint-disable rxjs/no-nested-subscribe */
import {
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnResizedEvent,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FilterChangedEvent,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuModule,
  ModelUpdatedEvent,
  RowDataUpdatedEvent,
  RowGroupingModule,
  RowNode,
  SelectionChangedEvent,
  SetFilterModule,
  SideBarModule,
} from '@ag-grid-enterprise/all-modules';
import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { IAssetVariableTypeTagMap, isNil } from '@atonix/atx-core';
import { Store } from '@ngrx/store';
import { delay, distinctUntilChanged, Subject, take, takeUntil } from 'rxjs';
import { opModeListColumnDefs } from './op-modes-list-column-defs';
import * as opModeActions from '../../../store/actions/op-mode.actions';
import * as modelConfigActions from '../../../store/actions/model-config.actions';
import {
  selectOpModetagListSidePanelAssetName,
  selectOpModetagListSidePanelAssetTreeConfiguration,
  selectOpModeList,
  selectOpModeListViewModel,
  selectOpModesListLoading,
  selectNewOpModeId,
  selectSelectedAsset,
} from '../../../store/reducers/model-config.reducer';
import { selectAsset } from '@atonix/atx-asset-tree';
import { OpModeFacade } from '../../../service/op-mode/op-mode.facade';
import { EOpModeCriteriaViewType } from '../../../service/op-mode/op-mode.models';
import {
  AssetTagMap,
  CriteriaLogic,
  OpModeDefinition,
  OpModeSummary,
} from '../../../service/model/op-mode';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import moment from 'moment';
import { OpModeFormService } from '../../../service/op-mode/op-mode-form.service';
import { OpModeSaveModalComponent } from './modals/op-mode-save-modal/op-mode-save-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { OpModeAssetDragModalComponent } from './modals/op-mode-asset-drag-modal.component';
import { MatSelectChange } from '@angular/material/select';
import { OPModeTypes } from '@atonix/shared/api';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteOpModeModalComponent } from './modals/delete-op-mode-modal/delete-op-mode-modal.component';
import { ModelConfigFacade } from '../../../store/facade/model-config.facade';

@Component({
  selector: 'atx-op-mode-list',
  templateUrl: './op-mode-list.component.html',
  styleUrls: ['./op-mode-list.component.scss'],
})
export class OpModeListComponent implements OnInit, OnDestroy {
  @Input() listTheme!: string;
  @ViewChild('opModeListGrid') opModeListGrid!: ElementRef;
  public mainForm!: FormGroup;

  readonly vm$ = this.store.select(selectOpModeListViewModel);
  readonly opModes$ = this.store.select(selectOpModeList);
  readonly opModeVm$ = this.opModeFacade.query.vm$;
  public unsubscribe$ = new Subject<void>();
  public columnDefs: (ColDef | ColGroupDef)[];
  public noSelectedOpModeMessage =
    'Create or Select an Op Mode to define its Criteria';
  public eCriteriaViewType = EOpModeCriteriaViewType;
  public eOpModeTypes = OPModeTypes;
  private gridApi!: GridApi;
  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    RowGroupingModule,
  ];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    rowGroupPanelShow: 'never',
    enableRangeSelection: false,
    animateRows: true,
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading Op Modes</div>',
    overlayNoRowsTemplate: '<div></div>',
    debug: false,
    rowHeight: 30,
    cacheBlockSize: 100,
    rowSelection: 'single',
    suppressCellFocus: true,
    suppressCopyRowsToClipboard: true,
    tooltipShowDelay: 0,
    getContextMenuItems: (params) => {
      const result = ['copy', 'copyWithHeaders'];
      return result;
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      cellClass: 'cell-selectable',
      enableRowGroup: false,
    },
    icons: {
      column:
        '<span class="ag-icon ag-icon-column " style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.opModeListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-column'
      ).title = 'Columns';
      this.opModeListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-filters'
      ).title = 'Filters';
    },
    getRowId: (params) => {
      return params.data.opModeDefinitionID.toString();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (event) {
        this.getFilters();
      }
    },
    onColumnResized: (event: ColumnResizedEvent) => {
      if (!isNil(this.gridApi)) {
        this.gridApi.sizeColumnsToFit();
      }
    },
    onModelUpdated: (event: ModelUpdatedEvent) => {
      this.gridApi = event.api;
      if (event && !isNil(this.gridApi)) {
        this.store
          .select(selectOpModesListLoading)
          .pipe(take(1))
          .subscribe((isLoading) => {
            if (isLoading) {
              this.gridApi.showLoadingOverlay();
            }
          });
        const dataLength = this.gridApi?.getDisplayedRowCount() ?? 0;
        if (dataLength > 0) {
          this.opModeFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
            // if vm.selectedOpMode is null,
            // we need to see if the url has an opmode that needs to be selected
            if (!vm.selectedOpMode) {
              // check if user has opmode in url
              const opModeSelected =
                this.activatedRoute.snapshot.queryParamMap.get('opmode');
              if (!opModeSelected) {
                return;
              }
              // find that op mode and select it
              let opModeID = -1;
              this.gridApi.forEachNode((node) => {
                if (node.data.opModeDefinitionExtID === opModeSelected) {
                  opModeID = node.data.opModeDefinitionID;
                }
              });
              if (opModeID > 0) {
                const currentRow = this.gridApi.getRowNode(
                  String(opModeID)
                ) as RowNode;
                if (currentRow) {
                  this.gridOptions.api?.ensureNodeVisible(currentRow);
                  currentRow.setSelected(true, undefined, true);
                  this.opModeFacade.command.getSelectedOpMode({
                    opModeDefinitionID: opModeID,
                  });
                }
              } else {
                // the opmode was not found in the current list
                // remove it from the url
                this.setSelectedOpMode(null);
              }
            }
          });
        }

        const totalOpModes = this.gridApi?.getDisplayedRowCount() ?? 0;
        this.store.dispatch(
          opModeActions.updateTotalOpModes({
            totalOpModes,
          })
        );
      }
    },
    onSelectionChanged: (event: SelectionChangedEvent) => {
      const selectedOpMode = this.gridApi.getSelectedNodes()[0]
        ?.data as OpModeSummary;
      if (selectedOpMode && selectedOpMode.opModeDefinitionID === -1) {
        const newOpMode = {
          opModeDefinitionID: -1,
          opModeDefinitionTitle: selectedOpMode.opModeDefinitionTitle,
          opModeTypeID: selectedOpMode.opModeTypeID,
          opModeTypeAbbrev: selectedOpMode.opModeTypeAbbrev,
          createDate: selectedOpMode.createDate.toISOString(),
          changeDate: selectedOpMode.changeDate.toISOString(),
          opModeDefinitionExtID: '',
          assetTagMaps: [],
          startCriteria: [],
          endCriteria: [],
          createdByUserID: 0,
          priority: 0,
        } as OpModeDefinition;
        this.opModeFacade.command.setNewOpMode(newOpMode);
      } else {
        this.opModeFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
          if (vm.isDirty) {
            this.opModeFacade.showOpModeDiscardChangesModal((dialogRef) => {
              dialogRef.afterClosed().subscribe((result) => {
                if (result) {
                  // user has discarded changes. get the op mode selected
                  this.opModeFacade.command.getSelectedOpMode({
                    opModeDefinitionID: selectedOpMode.opModeDefinitionID,
                  });
                  // update the url to the newly selected op mode
                  this.setSelectedOpMode(selectedOpMode.opModeDefinitionExtID);
                  if (vm.selectedOpMode.opModeDefinitionID <= 0) {
                    const opModeDefinitionID = String(
                      vm.selectedOpMode.opModeDefinitionID
                    );
                    const newOpMode = this.gridApi.getRowNode(
                      opModeDefinitionID
                    ) as RowNode;
                    this.gridApi.removeItems([newOpMode], true);
                    this.store.dispatch(
                      opModeActions.removeOpMode({
                        opModeDefinitionId: opModeDefinitionID,
                      })
                    );
                  }
                } else {
                  const currentRow = this.gridApi.getRowNode(
                    vm.selectedOpMode.opModeDefinitionID.toString()
                  );
                  if (currentRow) {
                    this.gridOptions.api?.ensureNodeVisible(currentRow);
                    currentRow.setSelected(true, undefined, true);
                  }
                  this.setSelectedOpMode(
                    vm.selectedOpMode.opModeDefinitionExtID
                  );
                  const selectedRow = this.gridApi.getRowNode(
                    selectedOpMode.opModeDefinitionID.toString()
                  );
                  if (selectedRow) {
                    selectedRow.setSelected(false, undefined, true);
                  }
                }
              });
            });
          } else {
            if (selectedOpMode) {
              // no dirty opmode selected
              // just select this op mode
              this.opModeFacade.command.getSelectedOpMode({
                opModeDefinitionID: selectedOpMode.opModeDefinitionID,
              });
              // and update the url
              this.setSelectedOpMode(selectedOpMode?.opModeDefinitionExtID);
            } else {
              this.setSelectedOpMode(null);
            }
          }
        });
      }
    },
  };

  get opModeDetailsFormGroup(): FormGroup {
    return this.mainForm.get('opModeDetails') as FormGroup;
  }

  set opModeDetailsFormGroup(form: FormGroup) {
    this.mainForm.setControl('opModeDetails', form);
  }

  get exclusionCriteriaFormGroup(): FormGroup {
    return this.mainForm.get('exclusionCriteria') as FormGroup;
  }

  set exclusionCriteriaFormGroup(form: FormGroup) {
    this.mainForm.setControl('exclusionCriteria', form);
  }

  get startCriteriaFormArray(): FormArray {
    return this.mainForm.get('startCriteria') as FormArray;
  }

  get endCriteriaFormArray(): FormArray {
    return this.mainForm.get('endCriteria') as FormArray;
  }

  constructor(
    private readonly store: Store,
    private opModeFacade: OpModeFacade,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private opModeFormService: OpModeFormService,
    private modelConfigFacade: ModelConfigFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.columnDefs = opModeListColumnDefs;

    this.store
      .select(selectSelectedAsset)
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        if (asset) {
          this.store.dispatch(
            opModeActions.getOpModeList({ uniqueKey: asset })
          );

          const assetTreeStateChange = selectAsset(asset);
          this.opModeFacade.tagListSidePanelAssetTreeStateChange(
            assetTreeStateChange
          );

          this.opModeFacade.command.clearSelectedOpMode();
        }
      });

    this.store
      .select(selectOpModetagListSidePanelAssetTreeConfiguration)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetTreeConfiguration) => {
        this.opModeFacade.command.setAssetConfiguration(assetTreeConfiguration);
      });

    this.store
      .select(selectOpModetagListSidePanelAssetName)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetName) => {
        if (assetName) {
          this.opModeFacade.command.setAssetName(assetName);
        }
      });

    this.store
      .select(selectNewOpModeId)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((newId) => {
        if (newId) {
          setTimeout(() => {
            const newOpMode = this.gridApi.getRowNode(newId.toString());
            if (newOpMode) {
              this.gridOptions.api?.ensureNodeVisible(newOpMode, 'top');
              newOpMode.setSelected(true);
              // new op modes have an opmode id of -1 and can't be recalled
              // on refresh. Just remove the opmode from the url
              this.setSelectedOpMode(null);
            }
          }, 500);
        }
      });
  }

  ngOnInit(): void {
    this.opModeFacade.query.formElements$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((formElements) => {
        if (!formElements) {
          this.mainForm = this.formBuilder.group({
            opModeDetails: this.formBuilder.group({
              opModeName: new FormControl(null),
            }),
            startCriteria: this.formBuilder.array([]),
            endCriteria: this.formBuilder.array([]),
            exclusionCriteria: this.formBuilder.group({
              startDate: new FormControl(null),
              startTime: new FormControl(null),
              endDate: new FormControl(null),
              endTime: new FormControl(null),
            }),
          });
        } else {
          this.opModeFormService.resetFormService();
          this.startCriteriaFormArray.clear();
          this.endCriteriaFormArray.clear();
          this.resetFormElements();
        }
      });

    this.store
      .select(selectOpModeListViewModel)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((vm) => {
        if (!isNil(this.gridApi)) {
          this.setNoRowsTemplate('<div></div>');
          // this.gridApi.sizeColumnsToFit();
          this.gridApi.hideOverlay();
          if (vm.loading) {
            this.gridApi.showLoadingOverlay();
          } else {
            const errorMessage = vm.errorMessage;
            if (errorMessage) {
              if (errorMessage === 'Cannot Display At This Level') {
                this.setNoRowsTemplate(
                  '<div class="ag-overlay-loading-center"><span class="ag-icon ag-icon-column select-asset" style="background: url(assets/select-asset.svg) no-repeat center;"></span></span>Cannot display Op Modes at this level<br/> Please select an asset lower in the asset tree</div>'
                );
              } else {
                this.setNoRowsTemplate(errorMessage);
              }
              this.gridApi.showNoRowsOverlay();
            }
          }
        }
      });

    this.opModeFacade.query.saveValidationResult$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((result) => {
        if (result && result.affectedModels) {
          if (
            result.affectedModels.length > 0 ||
            result?.suppressAffectedModelList
          ) {
            this.dialog.open(OpModeSaveModalComponent, {
              width: '700px',
              disableClose: true,
              hasBackdrop: true,
            });
          } else {
            this.saveOpModeWithoutValidation();
          }
        }
      });

    this.opModeFacade.query.isNewOpMode$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val.isNewOpMode && Number(val.opMode?.opModeDefinitionID) > 0) {
          const newOpMode = {
            opModeDefinitionID: val.opMode?.opModeDefinitionID,
            opModeDefinitionExtID: val.opMode?.opModeDefinitionExtID,
            opModeDefinitionTitle: val.opMode?.opModeDefinitionTitle,
            opModeTypeID: val.opMode?.opModeTypeID,
            opModeTypeAbbrev: val.opMode?.opModeTypeAbbrev,
            changeDate: moment(val.opMode?.changeDate).toDate(),
            createDate: moment(val.opMode?.createDate).toDate(),
          } as OpModeSummary;
          this.store.dispatch(
            opModeActions.updateNewOpMode({ opModeSummary: newOpMode })
          );
        }
      });

    this.opModeFacade.command.clearSelectedOpMode();
  }

  ngOnDestroy(): void {
    this.setSelectedOpMode(null);
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private resetFormElements() {
    this.opModeFacade.query.selectedOpMode$
      .pipe(take(1))
      .subscribe((selectedOpMode) => {
        this.opModeDetailsFormGroup = this.formBuilder.group({
          opModeName: this.opModeFormService.createOpModeName(
            selectedOpMode?.opModeDefinitionTitle ?? ''
          ),
        });
        if (selectedOpMode?.opModeTypeID === OPModeTypes.ExclusionPeriod) {
          if (selectedOpMode?.startCriteria) {
            const logic = selectedOpMode.startCriteria[0].logic[0];

            if (logic) {
              this.exclusionCriteriaFormGroup = this.formBuilder.group({
                startDate: this.opModeFormService.exclusionStartDate(
                  logic.logicStartDate ?? moment().toDate()
                ),
                startTime: this.opModeFormService.exclusionStartTime(),
                endDate: this.opModeFormService.exclusionEndDate(
                  logic.logicEndDate ?? moment().toDate()
                ),
                endTime: this.opModeFormService.exclusionEndTime(),
              });
              const startDate = moment(logic.logicStartDate).toDate();
              const startTime = moment(startDate).format('HH:mm');
              const endDate = moment(logic.logicEndDate).toDate();
              const endTime = moment(endDate).format('HH:mm');

              this.exclusionCriteriaFormGroup
                ?.get('startTime')
                ?.patchValue(startTime, { emitEvent: false });
              this.exclusionCriteriaFormGroup
                ?.get('endTime')
                ?.patchValue(endTime, { emitEvent: false });
              this.exclusionCriteriaFormGroup
                ?.get('endDate')
                ?.patchValue(endDate, { emitEvent: false });
              this.exclusionCriteriaFormGroup
                ?.get('startDate')
                ?.patchValue(startDate, { emitEvent: false });
            }
          }
        } else {
          selectedOpMode?.startCriteria?.forEach((criteria, groupIndex) => {
            const logicArray = this.formBuilder.array([]);
            criteria.logic.forEach((logic, logicIndex) => {
              const logicForm = this.formBuilder.group({
                logicValue: this.opModeFormService.logicValue(
                  groupIndex,
                  logicIndex,
                  logic.value ? logic.value.toString() : '0',
                  EOpModeCriteriaViewType.StartCriteria
                ),
              });
              (logicArray as FormArray).push(logicForm);
            });
            this.startCriteriaFormArray.push(logicArray);
          });
          selectedOpMode?.endCriteria?.forEach((criteria, criteriaIndex) => {
            const logicArray = this.formBuilder.array([]);
            criteria.logic.forEach((logic, logicIndex) => {
              const logicForm = this.formBuilder.group({
                logicValue: this.opModeFormService.logicValue(
                  criteriaIndex,
                  logicIndex,
                  logic.value ? logic.value.toString() : '0',
                  EOpModeCriteriaViewType.EndCriteria
                ),
              });
              (logicArray as FormArray).push(logicForm);
            });
            this.endCriteriaFormArray.push(logicArray);
          });
          this.exclusionCriteriaFormGroup = this.formBuilder.group({
            startDate: new FormControl(null),
            startTime: new FormControl(null),
            endDate: new FormControl(null),
            endTime: new FormControl(null),
          });
        }
      });
  }

  saveOpMode() {
    this.opModeFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const opMode = vm?.selectedOpMode;
      this.opModeFacade.command.validateOpMode({
        opMode,
      });
    });
  }

  saveOpModeWithoutValidation() {
    this.opModeFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const opMode = vm?.selectedOpMode;
      this.opModeFacade.command.saveOpMode({
        opMode,
      });
    });
  }

  opModeChange(event: MatSelectChange) {
    if (event?.value) {
      this.opModeFacade.command.clearForm();
      this.opModeFacade.command.opModeTypeChange(+event.value);
    }
  }

  onOpModeChartDrop(event: any) {
    event.preventDefault();

    let title = '';
    let nodeData: any;
    let tagData: any;
    if (event.dataTransfer.getData('nodeData')) {
      nodeData = JSON.parse(event.dataTransfer.getData('nodeData'));
      title = nodeData.nodeAbbrev;
    } else {
      const userAgent = window.navigator.userAgent;
      const isIE = userAgent.indexOf('Trident/') >= 0;
      const transferedData = event.dataTransfer.getData(
        isIE ? 'text' : 'text/plain'
      );
      const assetVariableTypeTagMaps = JSON.parse(transferedData);
      title = assetVariableTypeTagMaps[0].Tag.TagName;
      tagData = assetVariableTypeTagMaps[0];
    }
    this.opModeFacade.query.isDirty$.pipe(take(1)).subscribe((isDirty) => {
      if (isDirty) {
        this.opModeFacade.showOpModeDiscardChangesModal((dialogRef) => {
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              //On Dialog Discard Changes
              this.createNewDropOpMode(title, nodeData, tagData);
            }
            //On Dialog Cancel Button
          });
        });
      } else {
        //When theres a clean state
        this.createNewDropOpMode(title, nodeData, tagData);
      }
    });
  }

  createNewDropOpMode(title: any, nodeData?: any, tagData?: any) {
    if (nodeData && nodeData.uniqueKey) {
      this.store.dispatch(
        modelConfigActions.treeStateChange(
          selectAsset(nodeData.uniqueKey, false)
        )
      );
    }
    if (!title) {
      return;
    }
    this.modelConfigFacade.defaultOpMode$
      .pipe(take(1), delay(500))
      .subscribe((defaultOpMode: OPModeTypes) => {
        this.discardChanges();
        this.store.dispatch(
          opModeActions.createNewOpMode({
            opModeSummary: {
              opModeDefinitionExtID: '',
              opModeDefinitionID: -1,
              opModeDefinitionTitle: `${title} ${this.getOpModeTypeLabelMapping(
                defaultOpMode
              )}`,
              opModeTypeAbbrev: this.getOpModeTypeLabelMapping(defaultOpMode),
              opModeTypeID: defaultOpMode,
              changeDate: new Date(),
              createDate: new Date(),
            },
          })
        );
        //adding asset or tag to associated tags has to be delayed or else it will be cleared
        //associated tags are stored in component state that gets cleared when a new opmode to created/loaded
        if (nodeData) {
          setTimeout(() => {
            this.opModeFacade.command.addAssetTagMap({
              assetGuid: nodeData?.data?.AssetGuid,
              assetAbbrev: nodeData?.nodeAbbrev,
              bringDescendants: true,
              include: true,
            });
          }, 750);
        } else if (tagData) {
          setTimeout(() => {
            this.opModeFacade.command.addAssetTagMap({
              tagDesc: tagData?.Tag?.TagDesc,
              tagName: tagData.Tag?.TagName,
              bringDescendants: false,
              include: true,
              assetVariableTypeTagMapID: tagData.AssetVariableTypeTagMapID,
            });
          }, 750);
        }
      });
  }

  private getOpModeTypeLabelMapping(opMode: number) {
    const opModeTypeLabelMapping: { [index: number]: string } = {
      [OPModeTypes.Startup]: 'Startup',
      [OPModeTypes.Transient]: 'Transient',
      [OPModeTypes.OOS]: 'Out of Service',
      [OPModeTypes.SteadyState]: 'Steady State',
      [OPModeTypes.ExclusionPeriod]: 'Exclusion Period',
    };
    return opModeTypeLabelMapping[opMode];
  }

  onDragOver(event: any) {
    const types = event.dataTransfer.types;
    const dragSupported = types.length;
    if (dragSupported) {
      event.dataTransfer.dropEffect = 'move';
    }
    event.preventDefault();
  }

  onDrop(event: any, include: boolean) {
    event.preventDefault();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;
    const transferedData = event.dataTransfer.getData(
      isIE ? 'text' : 'text/plain'
    );
    if (event.dataTransfer.getData('nodeData')) {
      const nodeData = JSON.parse(event.dataTransfer.getData('nodeData'));
      const dialogRef = this.dialog.open(OpModeAssetDragModalComponent);

      dialogRef.afterClosed().subscribe((result) => {
        if (
          result?.response === 'descendants' ||
          result?.response === 'asset'
        ) {
          if (nodeData?.data?.AssetGuid) {
            this.opModeFacade.command.addAssetTagMap({
              assetGuid: nodeData?.data?.AssetGuid,
              assetAbbrev: nodeData?.nodeAbbrev,
              bringDescendants:
                result?.response === 'descendants' ? true : false,
              include,
            });
          }
        } else {
          return;
        }
      });
    } else {
      // Coming from Ag Grid
      const assetVariableTypeTagMaps: IAssetVariableTypeTagMap[] =
        JSON.parse(transferedData);
      if (
        assetVariableTypeTagMaps[0]?.Tag &&
        assetVariableTypeTagMaps[0].AssetVariableTypeTagMapID
      ) {
        this.opModeFacade.command.addAssetTagMap({
          tagDesc: assetVariableTypeTagMaps[0]?.Tag?.TagDesc,
          tagName: assetVariableTypeTagMaps[0].Tag?.TagName,
          bringDescendants: false,
          include,
          assetVariableTypeTagMapID:
            assetVariableTypeTagMaps[0].AssetVariableTypeTagMapID,
        });
      }
    }
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];
    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef | null = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef?.headerName,
      });
    });
    this.store.dispatch(opModeActions.setOpModeListFilters({ filters }));
  }

  removeFilter(filter: any): void {
    this.store.dispatch(
      opModeActions.removeOpModeListFilter({
        filter: filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key)?.setModel(null);
    });
    this.store.dispatch(opModeActions.setOpModeListFilters({ filters: [] }));
    this.refreshGrid();
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName)?.setModel(null);
    this.refreshGrid();
  }
  setNoRowsTemplate(message: string) {
    (this.gridApi as any).gridOptionsWrapper.setProperty(
      'overlayNoRowsTemplate',
      message
    );
  }

  changeAssetTagMapDescendants(assetTagMap: AssetTagMap) {
    this.opModeFacade.command.changeAssetTagMapDescendants(assetTagMap);
  }

  removeAssetTagMap(assetTagMap: AssetTagMap) {
    this.opModeFacade.command.removeAssetTagMap(assetTagMap);
  }

  private setSelectedOpMode(selectedOpModeExtID: string | null) {
    this.router.navigate([], {
      queryParams: { opmode: selectedOpModeExtID },
      relativeTo: this.activatedRoute,
      queryParamsHandling: 'merge',
    });
  }

  discardChanges() {
    this.opModeFacade.query.selectedOpMode$
      .pipe(take(1))
      .subscribe((opMode) => {
        if (opMode) {
          const currentOpMode = this.gridApi.getRowNode(
            opMode.opModeDefinitionID.toString()
          ) as RowNode;
          const opModeDefinitionID =
            currentOpMode?.data.opModeDefinitionID ?? 0;

          if (opModeDefinitionID < 0) {
            this.gridApi.removeItems([currentOpMode], true);
            this.store.dispatch(
              opModeActions.removeOpMode({
                opModeDefinitionId: opModeDefinitionID,
              })
            );

            this.opModeFacade.command.clearSelectedOpMode();
          } else if (opModeDefinitionID > 0) {
            this.opModeFacade.command.getSelectedOpMode({
              opModeDefinitionID: opModeDefinitionID,
            });

            if (currentOpMode) {
              currentOpMode.setSelected(true);
            }
          }
        }
      });
  }

  deleteOpMode() {
    this.dialog
      .open(DeleteOpModeModalComponent)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.opModeFacade.query.selectedOpMode$
            .pipe(take(1))
            .subscribe((opMode) => {
              if (opMode) {
                this.opModeFacade.command.deleteOpMode({
                  opModeDefinitionID: opMode.opModeDefinitionID,
                });
              }
            });
        }
      });
  }
}
