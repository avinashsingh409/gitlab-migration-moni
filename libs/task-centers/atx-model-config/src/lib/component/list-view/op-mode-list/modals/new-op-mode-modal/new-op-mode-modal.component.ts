import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { OPModeTypes } from '@atonix/shared/api';
import { OpModeFacade } from '../../../../../service/op-mode/op-mode.facade';
import { OpModeFormService } from '../../../../../service/op-mode/op-mode-form.service';
import {
  OpModeDefinition,
  OpModeSummary,
} from '../../../../../service/model/op-mode';
import moment from 'moment';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { ModelConfigFacade } from 'libs/task-centers/atx-model-config/src/lib/store/facade/model-config.facade';
import { take } from 'rxjs';

@Component({
  selector: 'atx-new-op-mode-modal',
  templateUrl: './new-op-mode-modal.component.html',
  styleUrls: ['./new-op-mode-modal.component.scss'],
})
export class NewOpModeModalComponent {
  readonly opModeTypes = Object.values(OPModeTypes).filter(
    (type) => typeof type === 'number' && type !== 4
  ) as number[];
  readonly opModeTypeLabelMapping: { [index: number]: string } = {
    [OPModeTypes.Startup]: 'Startup',
    [OPModeTypes.Transient]: 'Transient',
    [OPModeTypes.OOS]: 'Out of Service',
    [OPModeTypes.SteadyState]: 'Steady State',
    [OPModeTypes.ExclusionPeriod]: 'Exclusion Period',
  };

  opModeTitle: FormControl;
  opModeType: FormControl = new FormControl(
    OPModeTypes.OOS,
    Validators.required
  );
  constructor(
    private formService: OpModeFormService,
    private opModeFacade: OpModeFacade,
    private modelConfigFacade: ModelConfigFacade,
    private dialogRef: MatDialogRef<NewOpModeModalComponent>
  ) {
    this.opModeTitle = formService.opModeTitle();
    this.modelConfigFacade.defaultOpMode$.pipe(take(1)).subscribe((opMode) => {
      this.opModeType.setValue(opMode);
    });
  }

  createOpMode() {
    const opModeType = this.opModeType.value as OPModeTypes;
    const currentDate = moment().toDate();
    const newOpMode = {
      opModeDefinitionID: -1,
      opModeDefinitionTitle: this.opModeTitle.value,
      opModeTypeID: opModeType,
      opModeTypeAbbrev: this.opModeTypeLabelMapping[opModeType],
      changeDate: currentDate,
      createDate: currentDate,
    } as OpModeSummary;
    this.dialogRef.close(newOpMode);
  }
}
