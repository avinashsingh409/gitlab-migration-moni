import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { INDModelWithMessage } from '@atonix/shared/api';

@Component({
  selector: 'atx-quick-deploy.dialog',
  templateUrl: './quick-deploy.dialog.component.html',
  styleUrls: ['./quick-deploy.dialog.component.scss'],
})
export class QuickDeployDialogComponent {
  public remainingModelCount = 0;
  public currentIdx = 0;
  public applyAll = false;
  private modelIdsToOverrwrite: string[] = [];

  constructor(
    private dialogRef: MatDialogRef<QuickDeployDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Array<INDModelWithMessage>
  ) {
    this.remainingModelCount = data.length - 1;
  }

  public replace() {
    if (this.applyAll) {
      while (this.currentIdx < this.data.length) {
        const currentModel = this.data[this.currentIdx];
        const msg: string[] = currentModel.Message.split('|');
        const extID: string = msg[1];
        this.modelIdsToOverrwrite.push(
          `${currentModel.Model.ModelExtID}|${extID}`
        );
        this.currentIdx += 1;
      }
      this.dialogRef.close(this.modelIdsToOverrwrite);
    } else {
      const currentModel = this.data[this.currentIdx];
      const msg: string[] = currentModel.Message.split('|');
      const extID: string = msg[1];
      this.modelIdsToOverrwrite.push(
        `${currentModel.Model.ModelExtID}|${extID}`
      );
      if (this.currentIdx === this.data.length - 1) {
        this.dialogRef.close(this.modelIdsToOverrwrite);
      } else {
        this.currentIdx += 1;
        this.remainingModelCount -= 1;
      }
    }
  }

  public doNotReplace() {
    if (this.applyAll) {
      this.dialogRef.close(this.modelIdsToOverrwrite);
    } else {
      if (this.currentIdx === this.data.length - 1) {
        this.dialogRef.close(this.modelIdsToOverrwrite);
      } else {
        this.currentIdx += 1;
        this.remainingModelCount -= 1;
      }
    }
  }
}
