import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IPDServer, IServerResult } from '@atonix/atx-core';
import { Observable } from 'rxjs';
import { UploadFileDialogFacade } from '../../../../store/services/upload-file-dialog.facade';

@Component({
  selector: 'atx-upload-file-dialog',
  templateUrl: './upload-file-dialog.component.html',
  styleUrls: ['./upload-file-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [UploadFileDialogFacade],
})
export class UploadFileDialogComponent {
  public servers$: Observable<IServerResult[]>;

  server = new UntypedFormControl(null, [Validators.required]);
  file!: File;
  limitReached = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private uploadFileDialogFacade: UploadFileDialogFacade
  ) {
    this.servers$ = this.uploadFileDialogFacade.servers$;

    this.uploadFileDialogFacade.getPDServers();
  }

  selectFile($event: FileList) {
    if ($event[0]) {
      if ($event[0].size / 1000 / 1000 > 10) {
        this.limitReached = true;
      } else {
        this.file = $event[0];
        this.limitReached = false;
      }
    }
  }

  uploadFile() {
    this.uploadFileDialogFacade.uploadFile(this.server.value, this.file);
  }
}
