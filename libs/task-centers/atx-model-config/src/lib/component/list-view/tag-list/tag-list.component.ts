import {
  Component,
  Input,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  OnInit,
  OnDestroy,
} from '@angular/core';
import {
  GridOptions,
  GridReadyEvent,
  GridApi,
  ColumnApi,
  ClipboardModule,
  ClientSideRowModelModule,
  ColDef,
  FilterChangedEvent,
  RangeSelectionModule,
  ColGroupDef,
  ModelUpdatedEvent,
  RowDoubleClickedEvent,
  MasterDetailModule,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  MenuModule,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
  SelectionChangedEvent,
  RowDataUpdatedEvent,
} from '@ag-grid-enterprise/all-modules';
import {
  ETagType,
  IModelConfigTagListSummary,
  IModelConfigTagModels,
} from '@atonix/shared/api';
import { GridActionsRendererComponent } from '../../ag-grid-components/grid-renderer/grid-actions-renderer.component';
import { isNil } from '@atonix/atx-core';
import {
  detailColumnDefs,
  getContextMenuItems,
  tagListColumnDefs,
} from './tag-list-column-defs';
import { Store } from '@ngrx/store';
import * as tagListActions from '../../../store/actions/tag-list.actions';
import * as modelConfigActions from '../../../store/actions/model-config.actions';
import { take, takeUntil, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { dateFormatter, LoggerService } from '@atonix/shared/utils';
import {
  selectSelectedAsset,
  selectSelectedTag,
  selectShowTagsForSelectedAssetOnly,
  selectTagListLoading,
  selectTagListLoadingViewModel,
  selectTagListTags,
  selectTagListViewModel,
} from '../../../store/reducers/model-config.reducer';
import { GridIconRendererComponent } from '../../ag-grid-components/grid-renderer/grid-icon-renderer.component';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'atx-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagListComponent implements OnInit, OnDestroy {
  @Input() listTheme!: string;
  @ViewChild('tagListGrid') tagListGrid!: ElementRef;

  readonly vm$ = this.store.select(selectTagListViewModel);
  readonly tags$ = this.store.select(selectTagListTags);
  readonly showTagsForSelectedAssetOnly$ = this.store.select(
    selectShowTagsForSelectedAssetOnly
  );
  readonly hasSelectedTag$ = this.store.select(selectSelectedTag);
  public unsubscribe$ = new Subject<void>();
  public modelCountTypeLabelMapping: Record<ETagType, string> = {
    [ETagType.ModelInputTags]: 'Model Input Tags',
    [ETagType.DependentTags]: 'Dependent Tags',
  };
  public eModelCountType = ETagType;
  public modelCountTypeFrmCtrl = new FormControl<ETagType | null>(null);
  public modelCountTypeStr = '';

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
    MasterDetailModule,
  ];
  public columnDefs: (ColDef | ColGroupDef)[];
  private gridApi!: GridApi;
  private columnApi!: ColumnApi;

  // Details' grid options
  detailGridOptions: GridOptions = {
    columnDefs: detailColumnDefs,
    rowHeight: 30,
    defaultColDef: {
      resizable: true,
    },
    components: {
      actionsCellRenderer: GridActionsRendererComponent,
      iconCellRenderer: GridIconRendererComponent,
    },
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => {
      if (event.data) {
        const modelExtID = (event.data as IModelConfigTagModels)?.ModelExtID;
        const assetGUID =
          (event.data as IModelConfigTagModels)?.AssetGuid || '';
        const isStandard = (event.data as IModelConfigTagModels)?.IsStandard;
        if (modelExtID) {
          this.store.dispatch(
            modelConfigActions.openModel({
              model: modelExtID,
              asset: assetGUID,
              isStandard,
            })
          );
        }
      }
    },
  };

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    rowGroupPanelShow: 'never',
    enableRangeSelection: true,
    animateRows: true,
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading Tags</div>',
    overlayNoRowsTemplate: '<div></div>',
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    suppressCopyRowsToClipboard: true,
    tooltipShowDelay: 0,
    getContextMenuItems: getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      cellClass: 'cell-selectable',
      enableRowGroup: false,
    },
    masterDetail: true,
    isRowMaster: (dataItem: any) => {
      // If there are models under a tag, the row can be collapsible
      return dataItem ? dataItem.ModelsList?.length > 0 : false;
    },
    detailCellRendererParams: {
      detailGridOptions: this.detailGridOptions,
      getDetailRowData: (params: any) => {
        params.successCallback(params.data.ModelsList);
      },
    },
    getRowHeight: (params: any) => {
      // Dynamically get the row's height if it has detail grid.
      // Will show max of 3 rows on the detail grid then vertical scroll will be enabled.
      if (params.node && params.node.detail) {
        const maxRowHeight =
          params.data.ModelsList.length < 3
            ? params.data.ModelsList.length * 30
            : 90;
        return maxRowHeight + 70; // 70 = detail grid padding top and bottom + header height + scroll height
      }
      return 30; // default row height
    },
    icons: {
      column:
        '<span class="ag-icon ag-icon-column " style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.tagListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-column'
      ).title = 'Columns';
      this.tagListGrid.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-filters'
      ).title = 'Filters';
    },
    getRowId: (params) => {
      return params.data.TagMapID.toString();
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (event) {
        this.getFilters();
      }
    },
    onSelectionChanged: (event: SelectionChangedEvent) => {
      const selectedNodes = this.gridApi.getSelectedNodes();
      if (selectedNodes?.length === 1) {
        const selectedTag = selectedNodes[0].data as IModelConfigTagListSummary;
        this.store.dispatch(
          tagListActions.tagListSelectionChange({ selectedTag })
        );
      } else {
        this.store.dispatch(
          tagListActions.tagListSelectionChange({ selectedTag: null })
        );
      }
    },
    onModelUpdated: (event: ModelUpdatedEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;

      if (event && !isNil(this.gridApi)) {
        this.store
          .select(selectTagListLoading)
          .pipe(take(1))
          .subscribe((isLoading) => {
            if (isLoading) {
              this.gridApi.showLoadingOverlay();
            }
          });

        this.store.dispatch(
          tagListActions.updateTotalModels({
            totalModels: this.gridApi?.getDisplayedRowCount(),
          })
        );
      }
    },
    onRowDataUpdated: (event: RowDataUpdatedEvent) => {
      event.api.resetRowHeights();
    },
  };

  constructor(private readonly store: Store, private logger: LoggerService) {
    this.logger.tabClicked('Tag List', 'Model Config');

    this.columnDefs = tagListColumnDefs;

    this.modelCountTypeFrmCtrl.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        this.store.dispatch(
          tagListActions.getTagList({ tagType: val ?? ETagType.DependentTags })
        );
        this.modelCountTypeStr = 'Model count of ';
        if (val === ETagType.ModelInputTags) {
          this.modelCountTypeStr +=
            this.modelCountTypeLabelMapping[
              ETagType.ModelInputTags
            ].toLowerCase();
        } else {
          this.modelCountTypeStr +=
            this.modelCountTypeLabelMapping[
              ETagType.DependentTags
            ].toLowerCase();
        }
      });
  }
  ngOnInit(): void {
    this.store
      .select(selectTagListLoadingViewModel)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((vm) => {
        if (!isNil(this.gridApi)) {
          // This will reset grid messages
          this.setNoRowsTemplate('<div></div>');
          this.gridApi.hideOverlay();
          if (vm.loading) {
            this.gridApi.showLoadingOverlay();
          } else {
            const errorMessage = vm.errorMessage;
            if (errorMessage) {
              if (errorMessage === 'Cannot Display At This Level') {
                this.setNoRowsTemplate(
                  '<div class="ag-overlay-loading-center"><span class="ag-icon ag-icon-column select-asset" style="background: url(assets/select-asset.svg) no-repeat center;"></span></span>Cannot display tags at this level<br/> Please select an asset lower in the asset tree</div>'
                );
              } else {
                this.setNoRowsTemplate(errorMessage);
              }
              this.gridApi.showNoRowsOverlay();
            }
          }
        }
      });

    this.store
      .select(selectSelectedAsset)
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((asset) => {
        if (asset) {
          this.modelCountTypeFrmCtrl.patchValue(ETagType.DependentTags);
        }
      });
  }

  setNoRowsTemplate(message: string) {
    (this.gridApi as any).gridOptionsWrapper.setProperty(
      'overlayNoRowsTemplate',
      message
    );
  }

  getFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    const filters: any[] = [];
    Object.keys(gridFilters).forEach((key: string) => {
      const colDef: ColDef | null = this.gridApi.getColumnDef(key);
      filters.push({
        Name: key,
        Removable: true,
        DisplayName: colDef?.headerName,
      });
    });
    this.store.dispatch(tagListActions.setTagListFilters({ filters }));
  }

  removeFilter(filter: any): void {
    this.store.dispatch(
      tagListActions.removeTagListFilter({
        filter: filter,
        callback: this.postRemoveFilterCallbackFn.bind(this),
      })
    );
  }

  postRemoveFilterCallbackFn(filterName: string): void {
    this.gridApi.getFilterInstance(filterName)?.setModel(null);
    this.refreshGrid();
  }

  clearFilters(): void {
    const gridFilters: { [key: string]: any } = this.gridApi.getFilterModel();
    Object.keys(gridFilters).forEach((key: string) => {
      this.gridApi.getFilterInstance(key)?.setModel(null);
    });
    this.store.dispatch(tagListActions.setTagListFilters({ filters: [] }));
    this.refreshGrid();
  }

  refreshGrid(): void {
    if (!isNil(this.gridOptions) && !isNil(this.gridOptions.api)) {
      this.gridOptions.api?.onFilterChanged();
    }
  }

  tagListPreferenceChange() {
    this.logger.trackTaskCenterEvent('ModelConfig:TagList:PreferenceChange');

    this.store.dispatch(tagListActions.tagListPreferenceChange());
  }

  downloadTags() {
    this.logger.trackTaskCenterEvent('ModelConfig:TagList:DownloadTags');

    const params = {
      fileName: 'TagList',
      processCellCallback: (cellParams: any) => {
        if (
          cellParams?.column?.colId === 'LastBuildDate' ||
          cellParams?.column?.colId === 'LastUpdateDate'
        ) {
          return dateFormatter(cellParams);
        } else if (
          cellParams?.column?.colId === 'VariableDesc' &&
          (isNil(cellParams.value) || cellParams.value === '')
        ) {
          return '{none}';
        } else return cellParams.value;
      },
    };
    this.gridApi.exportDataAsCsv(params);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
