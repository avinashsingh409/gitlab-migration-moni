import {
  ColDef,
  ColGroupDef,
  ValueGetterParams,
} from '@ag-grid-enterprise/all-modules';
import { dateComparator } from '@atonix/atx-core';
import { dateFormatter } from '@atonix/shared/utils';

export function getContextMenuItems(params: any): string[] {
  const result = ['copy', 'copyWithHeaders'];
  return result;
}

export const tagListColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelCount',
    headerName: 'Model Count',
    width: 240,
    sortable: true,
    filter: 'agTextColumnFilter',
    valueGetter: (params: ValueGetterParams) => {
      return params?.data?.ModelsList?.length ?? 0;
    },
    cellRenderer: 'agGroupCellRenderer', // group cell renderer needed for expand / collapse icons
  },
  {
    sort: 'asc',
    colId: 'TagName',
    headerName: 'Tag Name',
    width: 240,
    field: 'TagName',
    sortable: true,
    tooltipField: 'TagName',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'TagDesc',
    headerName: 'Tag Description',
    field: 'TagDesc',
    sortable: true,
    tooltipField: 'TagDesc',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'EngUnits',
    headerName: 'Units',
    width: 106,
    field: 'EngUnits',
    sortable: true,
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'VariableDesc',
    headerName: 'Variable',
    width: 240,
    field: 'VariableDesc',
    sortable: true,
    tooltipField: 'VariableType',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'UnitAbbrev',
    headerName: 'Unit',
    field: 'UnitAbbrev',
    sortable: true,
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'AssetDesc',
    headerName: 'Asset',
    field: 'AssetDesc',
    sortable: true,
    tooltipField: 'AssetDesc',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'AssetTypeDesc',
    headerName: 'Asset Class',
    field: 'AssetTypeDesc',
    sortable: true,
    tooltipField: 'AssetClassType',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'AssetCriticality',
    headerName: 'Asset Criticality',
    field: 'AssetCriticality',
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: [1, 2, 3],
    },
  },
  {
    colId: 'AssetPath',
    headerName: 'Asset Path',
    field: 'AssetPath',
    sortable: false,
    tooltipField: 'AssetPath',
    cellStyle: { textAlign: 'right' },
  },
];

export const detailColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'ModelName',
    sortable: true,
    minWidth: 350,
    tooltipField: 'ModelName',
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'ActionMaintenance',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/emblem-maintainence.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Maintenance',
    headerTooltip: 'Maintenance',
    field: 'ActionModelMaintenance',
    cellRenderer: 'actionsCellRenderer',
    cellRendererParams: {
      tooltip: 'Maintenance',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.ActionModelMaintenance ?? false);
    },
  },
  {
    colId: 'Active',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/atx-active.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Active',
    headerTooltip: 'Active',
    field: 'Active',
    cellRenderer: 'iconCellRenderer',
    cellRendererParams: {
      type: 'active',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.Active ?? false);
    },
  },
  {
    colId: 'Standard',
    maxWidth: 65,
    headerComponentParams: {
      template: `
          <div class="ag-cell-label-container icon-only" role="presentation">
            <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button" style="margin-top: -20px; transform: scale(0.5);"></span>
            <div ref="eLabel" class="ag-header-cell-label" role="presentation">
              <img src="./assets/atx-std-model.svg" />
              <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
              <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
              <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
              <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
              <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
              <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
            </div>
          </div>
          `,
    },
    headerName: 'Standard',
    headerTooltip: 'Standard',
    field: 'IsStandard',
    cellRenderer: 'iconCellRenderer',
    cellRendererParams: {
      type: 'standard',
    },
    sortable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['true', 'false'],
    },
    filterValueGetter: (params) => {
      return String(params?.data?.IsStandard ?? false);
    },
  },
  {
    colId: 'LastBuildStatus',
    headerName: 'Last Build Status',
    field: 'LastBuildStatus',
    sortable: true,
    filter: 'agTextColumnFilter',
  },
  {
    colId: 'LastBuildDate',
    headerName: 'Last Build Date',
    field: 'LastBuildDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'LastUpdateDate',
    headerName: 'Last Update Date',
    field: 'LastUpdateDate',
    sortable: true,
    filter: 'agDateColumnFilter',
    valueFormatter: dateFormatter,
    filterParams: {
      comparator: dateComparator,
      inRangeInclusive: true,
    },
  },
  {
    colId: 'ModelTypeAbbrev',
    headerName: 'Model Type',
    field: 'ModelTypeAbbrev',
    sortable: true,
    tooltipField: 'ModelTypeAbbrev',
    filter: 'agSetColumnFilter',
    filterParams: {
      values: [
        'APR',
        'Rolling Average',
        'Fixed Limit',
        'Frozen Data',
        'External',
        'Moving Average',
        'Forecast',
        'Rate of Change',
      ],
    },
  },
  {
    colId: 'OpModeTypes',
    headerName: 'Operating Mode',
    field: 'OpModeTypes',
    sortable: true,
    filter: 'agTextColumnFilter',
    filterParams: {
      values: ['Steady State', 'Startup', 'Transient', 'Out of Service'],
    },
  },
  {
    colId: 'Score',
    headerName: 'Score',
    field: 'Score',
    width: 90,
    sortable: true,
    filter: 'agNumberColumnFilter',
  },
];
