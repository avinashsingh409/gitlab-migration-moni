import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import {
  EModelTypes,
  EPredictiveTypes,
  PredictiveMethodTypeLabel,
} from '@atonix/shared/api';
import {
  MatCheckboxDefaultOptions,
  MAT_CHECKBOX_DEFAULT_OPTIONS,
} from '@angular/material/checkbox';

@Component({
  selector: 'atx-model-stats',
  templateUrl: './model-stats.component.html',
  styleUrls: ['./model-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions,
    },
  ],
})
export class ModelStatsComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  readonly predictiveMethodTypeLabel = PredictiveMethodTypeLabel;
  public readonly modelTypes = EModelTypes;
  predictiveTypeToggle(methodType: EPredictiveTypes) {
    if (methodType) {
      this.modelEditFacade.command.setPredictiveMethodType(methodType);
    }
  }
  constructor(private modelEditFacade: ModelEditFacade) {}
}
