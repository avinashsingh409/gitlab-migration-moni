import {
  Component,
  ChangeDetectionStrategy,
  Input,
  OnInit,
} from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';
import { isNil } from '@atonix/atx-core';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelectChange } from '@angular/material/select';
import { IProcessedTrend, IPDTrendSeries } from '@atonix/atx-core';
import { AtxScatterPlot, AtxChartLine } from '@atonix/atx-chart-v2';
import { produce } from 'immer';
import { Subject } from 'rxjs';

@Component({
  selector: 'atx-model-edit-tabs-model-context',
  templateUrl: './model-context.component.html',
  styleUrls: ['./model-context.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelEditTabsModelContextComponent implements OnInit {
  @Input() PDNDModelTrendData: any = {};
  @Input() ShowXAxisPicker = false;
  public selectedXAxis = -1;
  private onDestroy = new Subject<void>();
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly modelContextData$ = this.modelEditFacade.query.modelContextData$;
  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService,
    public navFacade: NavFacade
  ) {
    this.logger.tabClicked('Model Context', 'Model Config');
  }

  xAxisChanged(changeEvent: MatSelectChange): void {
    this.modelEditFacade.query.modelContextData$
      .pipe(take(1))
      .subscribe((aTrend: IProcessedTrend | null) => {
        if (aTrend) {
          const newTrend = produce(aTrend, (state) => {
            const seriesIndex = state.trendDefinition.Series.findIndex(
              (s: IPDTrendSeries) => {
                return s.PDTagID === changeEvent.value;
              }
            );
            if (seriesIndex > -1) {
              const series: IPDTrendSeries =
                state.trendDefinition.Series[seriesIndex];
              const addingXAxis =
                series.IsXAxis === null
                  ? true
                  : series.IsXAxis === false
                  ? true
                  : false;
              const isXAxis = state.trendDefinition.Series.some(
                (s) => s.IsXAxis
              );
              if (addingXAxis) {
                state.trendDefinition.Series[seriesIndex].IsXAxis = true;
                if (isXAxis) {
                  state.trendDefinition.Series.forEach((s, i) => {
                    if (s.IsXAxis && i !== seriesIndex) {
                      s.IsXAxis = false;
                    }
                  });
                } else {
                  state.trendDefinition.ChartTypeID = AtxScatterPlot;
                }
              } else {
                state.trendDefinition.Series[seriesIndex].IsXAxis = false;

                if (
                  isXAxis &&
                  state.trendDefinition.ChartTypeID === AtxScatterPlot
                ) {
                  state.trendDefinition.ChartTypeID = AtxChartLine;
                }
              }
            } else {
              state.trendDefinition.Series.forEach((s: IPDTrendSeries) => {
                s.IsXAxis = false;
              });
              state.trendDefinition.ChartTypeID = AtxChartLine;
            }
          });
          this.modelEditFacade.command.setModelContext({ trend: newTrend });
        }
      });
  }

  ngOnInit(): void {
    if (!isNil(this.PDNDModelTrendData?.model)) {
      this.modelEditFacade.getPDNDModelTrends({
        model: this.PDNDModelTrendData.model,
        asset: this.PDNDModelTrendData.asset,
        startDate: this.PDNDModelTrendData.startDate,
        endDate: this.PDNDModelTrendData.endDate,
      });
    }
    this.modelContextData$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((context) => {
        if (context) {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 10);
        }
      });
  }
}
