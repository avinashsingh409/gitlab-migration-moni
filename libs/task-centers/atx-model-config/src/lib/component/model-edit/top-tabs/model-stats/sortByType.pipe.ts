import { Pipe, PipeTransform } from '@angular/core';
import {
  IModelPredictiveTypes,
  PredictiveMethodTypeLabel,
} from '@atonix/shared/api';

@Pipe({
  name: 'sortByType',
})
export class SortByTypePipe implements PipeTransform {
  transform(value: IModelPredictiveTypes[]): IModelPredictiveTypes[] {
    const newValue: IModelPredictiveTypes[] = [...value];
    return newValue.sort((a, b) => {
      if (a.type && b.type) {
        return PredictiveMethodTypeLabel[a.type].localeCompare(
          PredictiveMethodTypeLabel[b.type]
        );
      } else {
        return -1;
      }
    });
  }
}
