import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import {
  EModelTypes,
  EPredictiveTypes,
  PredictiveMethodTypeLabel,
} from '@atonix/shared/api';
import {
  MatCheckboxDefaultOptions,
  MAT_CHECKBOX_DEFAULT_OPTIONS,
} from '@angular/material/checkbox';
import { ModelConfigFacade } from '../../../../store/facade/model-config.facade';
import { take } from 'rxjs';

@Component({
  selector: 'atx-op-mode-details',
  templateUrl: './op-mode-details.component.html',
  styleUrls: ['./op-mode-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions,
    },
  ],
})
export class OpModeDetailsComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  readonly predictiveMethodTypeLabel = PredictiveMethodTypeLabel;
  public readonly modelTypes = EModelTypes;
  predictiveTypeToggle(methodType: EPredictiveTypes) {
    if (methodType) {
      this.modelEditFacade.command.setPredictiveMethodType(methodType);
    }
  }

  openOpMode(opModeDefinitionExtID: string) {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (model) {
          this.modelConfigFacade.modelConfigActionsOpenOpMode(
            model.dependent.assetGuid,
            opModeDefinitionExtID
          );
        }
      });
  }

  constructor(
    private modelEditFacade: ModelEditFacade,
    private modelConfigFacade: ModelConfigFacade
  ) {}
}
