import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  Inject,
  ViewChild,
  ChangeDetectorRef,
  ViewChildren,
  QueryList,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getModelConfigRoute, NavFacade } from '@atonix/atx-navigation';
import { getAMonthAgoLive } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import { filter, take, withLatestFrom } from 'rxjs/operators';
import { ModelEditFacade } from '../../../service/model-edit.facade';
import * as modelEditActions from '../../../store/actions/model-edit.actions';
import {
  selectModelEditAssetTreeDropdownAssetName,
  selectModelEditAssetTreeDropdownConfiguration,
  selectModelEditAssetTreeDropdownSelectedAssetIds,
  selectSelectedModelGuids,
} from '../../../store/reducers/model-config.reducer';
import * as timeSliderActions from '../../../store/actions/time-slider.actions';
import { selectTimeSlider } from '../../../store/reducers/model-config.reducer';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import {
  DataExplorerCoreService,
  EModelTypes,
  EOpModeTypes,
  EPredictiveTypes,
  IModelConfigData,
  ModelTypeLabelMapping,
  OpModeTypeLabelMapping,
} from '@atonix/shared/api';
import { UntypedFormControl } from '@angular/forms';
import { firstValueFrom, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { isNil, isNilOrEmptyString } from '@atonix/atx-core';
import * as signalR from '@microsoft/signalr';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AuthService } from '@atonix/shared/state/auth';
import { CustomRetryPolicy } from '../../../service/signal-r.facade';
import { SingleModelFacade } from '../../../service/single-model.facade';
import { ModelEditTagListFacade } from '../../../service/model-edit-tag-list.facade';
import {
  ModelConfigBusEventTypes,
  ModelConfigEventBus,
} from '../../../store/services/model-config-event-bus';
import { MatDialog } from '@angular/material/dialog';
import { MultiModelNoChangeRebuildComponent } from '../../model-edit/modals/multi-model-no-change-rebuild/multi-model-no-change-rebuild.component';
import { MatTabGroup } from '@angular/material/tabs';
import { MultiModelRebuildRequiredComponent } from '../../model-edit/modals/multi-model-rebuild-required/multi-model-rebuild-required.component';
import { AllModelsLockedDialogComponent } from '../../model-edit/modals/all-models-locked/all-models-locked-dialog.component';
import { ITreeStateChange, selectAsset } from '@atonix/atx-asset-tree';
import { SplitAreaDirective, SplitComponent } from 'angular-split';

@Component({
  selector: 'atx-multiple-model-edit',
  templateUrl: './multiple-model-edit.component.html',
  styleUrls: ['./multiple-model-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultipleModelEditComponent implements OnInit, OnDestroy {
  @ViewChild('bottomTabGroup') public bottomTabGroup!: MatTabGroup;
  @ViewChild(SplitComponent) splitEl: SplitComponent | undefined;
  @ViewChildren(SplitAreaDirective) areasEl!: QueryList<SplitAreaDirective>;
  topSizePercent = 40;
  bottomSizePercent = 60;
  public toggleTop = false;
  public toggleBottom = false;

  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly timeSlider$ = this.store.select(selectTimeSlider);
  readonly selectSelectedModelGuids$ = this.store.select(
    selectSelectedModelGuids
  );
  public readonly modelTypeEnum = EModelTypes;

  readonly modelTypeLabelMapping = ModelTypeLabelMapping;
  readonly modelTypes = Object.values(EModelTypes).filter(
    (value) => typeof value === 'string'
  );
  readonly opModeTypeLabelMapping = OpModeTypeLabelMapping;
  readonly opModeTypes = Object.values(EOpModeTypes).filter(
    (value) => typeof value === 'string'
  );
  private modelsLoaded = new Subject<void>();
  private onDestroy = new Subject<void>();
  connection: signalR.HubConnection | null = null;

  opModesFormCtrl = new UntypedFormControl();

  constructor(
    private activatedRoute: ActivatedRoute,
    public navFacade: NavFacade,
    private authService: AuthService,
    private modelEditFacade: ModelEditFacade,
    private readonly store: Store,
    private matDialog: MatDialog,
    private change: ChangeDetectorRef,
    private dataExplorerCoreService: DataExplorerCoreService,
    private singleModelFacade: SingleModelFacade,
    private modelEditTagListFacade: ModelEditTagListFacade,
    private modelConfigEventBus: ModelConfigEventBus,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.MODEL_EDIT_ASSET_TREE_DROPDOWN_STATE_CHANGE,
      this.assetTreeDropdownStateChange.bind(this)
    );
  }

  ngOnInit(): void {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    const start = modelConfigRoute.start
      ? new Date(parseFloat(modelConfigRoute.start))
      : getAMonthAgoLive();
    const end = modelConfigRoute.end
      ? new Date(parseFloat(modelConfigRoute.end))
      : new Date();

    const asset = modelConfigRoute.asset;

    this.store.dispatch(
      modelEditActions.initializeModelConfigEdit({
        assetID: asset,
        start,
        end,
      })
    );

    this.loadModels();

    this.modelEditFacade.query.reloadModels$
      .pipe(filter((x) => x === true))
      .subscribe(() => {
        this.modelEditFacade.command.resetModelEditState();
        this.loadModels();
      });
  }

  loadModels() {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const modelIds = modelConfigRoute.modelIds.split(',');

    this.launchObservers();

    this.modelEditFacade.getModelSummaries({
      modelIds,
      refreshTrends: true,
      isMultiModel: true,
    });
    this.modelEditFacade.query.modelEntities$
      .pipe(takeUntil(this.modelsLoaded))
      .subscribe((models) => {
        setTimeout(() => {
          this.bottomTabGroup.selectedIndex = 0;
          this.change.markForCheck();
        });

        if (!isNil(models) && Object.keys(models).length > 0) {
          const tagMapIDs = [];
          for (const key in models) {
            tagMapIDs.push(
              models[key]?.dependent.assetVariableTagMapID as number
            );
          }
          this.selectTagListAsset(tagMapIDs);
        }
      });
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((model) => {
        if (model && model.properties?.opModeTypes) {
          this.opModesFormCtrl = new UntypedFormControl();
          this.opModesFormCtrl.patchValue(model.properties?.opModeTypes);
        }
      });

    this.modelEditFacade.query.predictiveMethodTypeSelected$
      .pipe(
        withLatestFrom(this.modelEditFacade.query.selectedModelId$),
        takeUntil(this.onDestroy)
      )
      .subscribe(([methodType, modelId]) => {
        if (methodType && methodType !== 'none') {
          const modelConfigRoute = getModelConfigRoute(
            this.activatedRoute.snapshot.queryParamMap
          );
          const start = modelConfigRoute.start
            ? new Date(parseFloat(modelConfigRoute.start))
            : getAMonthAgoLive();
          const end = modelConfigRoute.end
            ? new Date(parseFloat(modelConfigRoute.end))
            : new Date();
          this.modelEditFacade.getModelTrend({
            modelId,
            predictiveMethodType: methodType,
            startDate: start,
            endDate: end,
          });
        }
      });

    this.modelEditFacade.query.selectedModelId$
      .pipe(
        withLatestFrom(
          this.modelEditFacade.query.predictiveMethodTypeSelected$
        ),
        takeUntil(this.onDestroy)
      )
      .subscribe(([modelId, methodType]) => {
        if (methodType && methodType !== 'none') {
          const modelConfigRoute = getModelConfigRoute(
            this.activatedRoute.snapshot.queryParamMap
          );
          const start = modelConfigRoute.start
            ? new Date(parseFloat(modelConfigRoute.start))
            : getAMonthAgoLive();
          const end = modelConfigRoute.end
            ? new Date(parseFloat(modelConfigRoute.end))
            : new Date();
          this.modelEditFacade.getModelTrend({
            modelId,
            predictiveMethodType: methodType,
            startDate: start,
            endDate: end,
          });
        }
      });
  }

  onExpandTop() {
    this.toggleTop = true;
    this.toggleBottom = false;
    this.areasEl.toArray()[1].visible = false;
    this.areasEl.toArray()[1].collapse();
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].expand();
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }
  splitDragEnd(e: { gutterNum: number; sizes: Array<number> }) {
    if (e?.sizes) {
      this.topSizePercent = e.sizes[0];
      this.bottomSizePercent = e.sizes[1];
    }
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }
  onExpandBottom() {
    this.toggleTop = false;
    this.toggleBottom = true;
    this.areasEl.toArray()[0].visible = false;
    this.areasEl.toArray()[0].collapse();
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].expand();
  }

  onCollapse() {
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].collapse(this.topSizePercent);
    this.areasEl.toArray()[0].order = 0;
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].collapse(this.bottomSizePercent);
    this.areasEl.toArray()[1].order = 1;
    this.toggleTop = false;
    this.toggleBottom = false;
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }

  topIndicatorClick() {
    if (!this.toggleTop) {
      this.onExpandTop();
    } else {
      this.onCollapse();
    }
  }

  bottomIndicatorClick() {
    if (!this.toggleBottom) {
      this.onExpandBottom();
    } else {
      this.onCollapse();
    }
  }
  private selectTagListAsset(tagMapIDs: number[]) {
    this.dataExplorerCoreService
      .assetParamsForTagGrid(tagMapIDs)
      .pipe(take(1))
      .subscribe((assetSelection) => {
        if (assetSelection && assetSelection?.AssetID) {
          this.modelEditTagListFacade.updateAssetID(assetSelection?.AssetID);
          this.modelEditTagListFacade.updateSearchAssetID(
            assetSelection?.SearchAssetID
          );
          this.store.dispatch(
            modelEditActions.tagListAssetStateChange({
              treeStateChange: selectAsset(assetSelection?.AssetID),
            })
          );
        }
      });

    this.modelsLoaded.next();
    this.modelsLoaded.complete();
  }

  private launchObservers() {
    this.signalRConnect();
    this.modelEditFacade.query.reloadModelStateWatcher$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((state) => {
        if (state?.modelRefreshAction) {
          if (state.modelRefreshAction.reloadModel) {
            this.modelEditFacade.multiModelCommand.getLatestModelSummary({
              modelID: state.modelRefreshAction.modelExtID as string,
              refreshTrends:
                state.selectedModelID === state.modelRefreshAction.modelExtID
                  ? true
                  : false,
            });
          }
          if (state.modelRefreshAction.reloadPredictiveMethodsNeeded) {
            this.refreshSelectedModel(
              (
                state.modelRefreshAction.modelExtID as string
              )?.toLocaleLowerCase() || '',
              true
            );
          }
        }
      });

    this.store
      .select(selectModelEditAssetTreeDropdownConfiguration)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((assetTreeConfiguration) => {
        this.modelEditTagListFacade.updateAssetTreeConfiguration(
          assetTreeConfiguration
        );
      });
    this.store
      .select(selectModelEditAssetTreeDropdownAssetName)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((assetName) => {
        if (assetName) {
          this.modelEditTagListFacade.updateAssetName(assetName);
        }
      });
    this.store
      .select(selectModelEditAssetTreeDropdownSelectedAssetIds)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((ids) => {
        this.modelEditTagListFacade.assetID$
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((assetID: string | null) => {
            if (ids && assetID !== ids.assetUniqueKey) {
              this.modelEditTagListFacade.updateAssetID(ids.assetUniqueKey);
              this.modelEditTagListFacade.updateSearchAssetID(ids.assetId);
            }
          });
      });
  }

  private reloadPredictiveMethodType(modelExtID: string) {
    this.modelEditFacade.query.predictiveMethodTypeSelected$
      .pipe(take(1))
      .subscribe((methodType) => {
        if (methodType !== undefined && methodType !== 'none') {
          this.loadPredictiveMethods(methodType, modelExtID);
        }
      });
  }

  loadPredictiveMethods(methodType: EPredictiveTypes, modelExtID: string) {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    if (
      !isNilOrEmptyString(modelExtID) &&
      modelExtID !== '-1' &&
      modelExtID !== '0'
    ) {
      setTimeout(() => {
        this.modelEditFacade.getModelTrend({
          modelId: modelExtID,
          predictiveMethodType: methodType,
          startDate: modelConfigRoute.startDate,
          endDate: modelConfigRoute.endDate,
        });
      }, 2000);
    }
  }

  private refreshSelectedModel(
    modelExtID: string,
    reloadPredictiveMethods: boolean
  ) {
    this.modelEditFacade.query.selectedModelId$
      .pipe(take(1))
      .subscribe((selectedExtID) => {
        if (
          modelExtID.toLocaleLowerCase() === selectedExtID.toLocaleLowerCase()
        ) {
          if (reloadPredictiveMethods) {
            this.reloadPredictiveMethodType(selectedExtID);
          }
          this.modelEditFacade.getModelMathMessage(selectedExtID as string);
        }
      });
  }

  determineModelState(models: any) {
    const modelIds: string[] = [];
    for (const key in models) {
      modelIds.push(models[key]?.modelExtID || '');
    }
    this.modelEditFacade.multiModelCommand
      .getLockedModels(modelIds)
      .pipe(take(1))
      .subscribe((modelRetuns) => {
        const modelsToSave = [];
        const cleanModels = [];
        const dirtyModels = [];
        const lockedModels = [];
        for (const key in models) {
          const modelExtID = (models[key] as IModelConfigData)?.modelExtID;
          const modelReturn = modelRetuns.find(
            (x) => x.modelExtID === modelExtID
          );
          if (modelReturn?.locked) {
            lockedModels.push(models[key]);
          } else {
            const isDirty = models[key]?.isDirty;
            if (isDirty) {
              dirtyModels.push(models[key] as IModelConfigData);
            } else {
              cleanModels.push(models[key] as IModelConfigData);
            }
            modelsToSave.push(models[key] as IModelConfigData);
          }
        }
        if (dirtyModels.length === 0) {
          if (cleanModels.length === 0) {
            this.allModelsLocked();
          } else {
            this.retrainModal(cleanModels, lockedModels);
          }
        } else {
          this.rebuildModal(cleanModels, dirtyModels, lockedModels);
        }
      });
  }

  saveChanges() {
    this.modelEditFacade.query.modelEntities$
      .pipe(take(1))
      .subscribe((models) => {
        if (isNil(models) || !models) {
          return;
        }
        this.determineModelState(models);
      });
  }

  rebuildModal(
    cleanModels: IModelConfigData[],
    dirtyModels: IModelConfigData[],
    lockedModels: IModelConfigData[]
  ) {
    const dialogRef = this.matDialog.open(MultiModelRebuildRequiredComponent, {
      data: {
        dirtyModels: dirtyModels,
        lockedModels: lockedModels,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selectBuildStatusTab(
          // since all models are the same type, it doesn't matter which one we use
          // to determine model type.
          cleanModels[0]?.properties?.modelType?.type ||
            EModelTypes.AdvancedPatternRecognition
        );
        if (result === 'ChangedModels') {
          dirtyModels.forEach((model) => {
            this.modelEditFacade.command.beginSaveModelGetTransform({
              isMulti: true,
              model,
            });
          });
        } else {
          dirtyModels.forEach((model) => {
            this.modelEditFacade.command.beginSaveModelGetTransform({
              isMulti: true,
              model,
            });
          });
          cleanModels.forEach((model) => {
            this.modelEditFacade.command.beginSaveModelGetTransform({
              isMulti: true,
              model,
            });
          });
        }
      } else {
        return;
      }
    });
  }

  allModelsLocked() {
    const dialogRef = this.matDialog.open(AllModelsLockedDialogComponent);
  }

  retrainModal(
    modelsToRetrain: IModelConfigData[],
    lockedModels: IModelConfigData[]
  ) {
    const dialogRef = this.matDialog.open(MultiModelNoChangeRebuildComponent, {
      data: {
        lockedModels: lockedModels,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.selectBuildStatusTab(
          modelsToRetrain[0]?.properties?.modelType?.type ||
            EModelTypes.AdvancedPatternRecognition
        );
        modelsToRetrain.forEach((model) => {
          this.modelEditFacade.command.beginSaveModelGetTransform({
            isMulti: true,
            model,
          });
        });
      } else {
        return;
      }
    });
  }

  onTimeSliderStateChange(change: ITimeSliderStateChange) {
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.store.dispatch(
      timeSliderActions.timeSliderStateChange({ stateChange: change })
    );
    if (change.event === 'SelectDateRange') {
      if (change?.newStartDateValue && change?.newEndDateValue) {
        // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
        this.store.dispatch(
          timeSliderActions.updateRouteTimeSlider({
            start: change.newStartDateValue,
            end: change.newEndDateValue,
          })
        );
        this.updateModelTrendTime(
          change.newStartDateValue,
          change.newEndDateValue
        );
      }
    }
  }

  private updateModelTrendTime(newStartDateValue: Date, newEndDateValue: Date) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.modelEditFacade.updateModelTrendTime(
        newStartDateValue,
        newEndDateValue,
        vm.selectedModelSummary.modelExtID
      );
    });
  }

  assetTreeDropdownStateChange(command: { treeStateChange: ITreeStateChange }) {
    if (command) {
      this.store.dispatch(modelEditActions.tagListAssetStateChange(command));
    }
  }

  discardChanges() {
    this.modelEditFacade.command.discardChanges();
  }

  modelSelectionChange(modelExtID: string) {
    this.modelEditFacade.command.modelSelectionChange(modelExtID);
  }

  opModeChange() {
    console.log(this.opModesFormCtrl.value);
  }

  getDefaultOpMode(opMode: EOpModeTypes) {
    const selectedOpModes = [...this.opModesFormCtrl.value];
    if (selectedOpModes.indexOf(opMode) > 0) {
      return this.opModeTypeLabelMapping[opMode];
    }
    const defaultOpModeType = selectedOpModes[0] as EOpModeTypes;
    return this.opModeTypeLabelMapping[defaultOpModeType];
  }

  getModelName(name: string, length: number) {
    if (length > 1) {
      return `${name} (+ ${length - 1} others)`;
    }
    return name;
  }

  selectBuildStatusTab(modelType: EModelTypes) {
    if (modelType === EModelTypes.Forecast) {
      this.bottomTabGroup.selectedIndex = 1;
    } else if (modelType === EModelTypes.RollingAverage) {
      this.bottomTabGroup.selectedIndex = 3;
    } else if (modelType === EModelTypes.FixedLimit) {
      this.bottomTabGroup.selectedIndex = 2;
    } else if (modelType === EModelTypes.FrozenData) {
      this.bottomTabGroup.selectedIndex = 2;
    } else {
      this.bottomTabGroup.selectedIndex = 4;
    }
    // Mat TabGroup SelectedIndex does not trigger
    // change detection, so force it: https://stackoverflow.com/questions/51342582/mat-tab-material-angular6-selectedindex-doesnt-work-with-ngfor
    this.change.markForCheck();
  }

  signalRConnect() {
    if (!this.connection) {
      this.connection = new signalR.HubConnectionBuilder()
        .configureLogging(signalR.LogLevel.Debug)
        .withUrl(
          `${this.appConfig.baseMicroServicesURL}/api/models/hub/notification`,
          {
            accessTokenFactory: async () =>
              await firstValueFrom(this.authService.GetIdTokenOrWait()),
            transport: signalR.HttpTransportType.WebSockets,
            skipNegotiation: true,
          }
        )
        .withAutomaticReconnect(new CustomRetryPolicy(this.singleModelFacade))
        .build();

      this.connection
        .start()
        .then(() => {
          this.singleModelFacade.connected();
          console.log('SignalR Connected!');
        })
        .catch((err) => {
          this.singleModelFacade.disconnected();
          return console.error(err.toString());
        });
      this.connection.on('signalRModelSend', (value: any) => {
        // console.log(value);
        this.modelEditFacade.query.multiModelIds$
          .pipe(take(1))
          .subscribe((models) => {
            if (isNil(models) || models?.length === 0) {
              return;
            }
            const signalRExtID =
              (value?.modelExtID as string).toLocaleLowerCase() || '';
            const modelIndex = models.indexOf(signalRExtID);

            if (modelIndex > -1) {
              this.modelEditFacade.multiModelCommand.multiSignalRMessage({
                modelExtID: signalRExtID,
                signalRMessage: value?.message,
              });
            }
          });
      });
      this.connection.onreconnected((connectionId) => {
        this.singleModelFacade.connected();
        console.log('reconnected');
      });
      this.connection.onclose((error) => {
        this.singleModelFacade.disconnected();
        console.log(error);
      });
    }
  }

  ngOnDestroy(): void {
    this.modelEditFacade.command.resetModelEditState();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
