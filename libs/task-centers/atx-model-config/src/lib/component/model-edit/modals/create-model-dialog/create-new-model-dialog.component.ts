import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  EModelTypes,
  EOpModeTypes,
  ModelTypeLabelMapping,
  ModelTypeMapping,
  OpModeTypeLabelMapping,
  OpModeTypeMapping,
} from '@atonix/shared/api';
import { distinctUntilChanged, Subject, takeUntil } from 'rxjs';
import { ModelEditFacade } from '../../../../service/model-edit.facade';

export enum EModelCreationSource {
  Template = 0,
  Custom = 1,
}
@Component({
  selector: 'atx-create-new-model-dialog',
  templateUrl: './create-new-model-dialog.component.html',
  styleUrls: ['./create-new-model-dialog.component.scss'],
})
export class CreateNewModelDialogComponent implements OnInit, OnDestroy {
  public opModeTypeFrmControl = new FormControl<string | null>(
    EOpModeTypes.SteadyState,
    [Validators.required]
  );
  public modelNameFrmControl = new FormControl<string>('New Model', [
    Validators.required,
  ]);
  public modelTypeFrmControl = new FormControl<string | null>(
    { value: null, disabled: true },
    [Validators.required]
  );
  public modelCreationSourceFrmControl =
    new FormControl<EModelCreationSource | null>(EModelCreationSource.Template);

  public enableTemplateRadioBtn = true;

  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly model$ = this.modelEditFacade.query.selectedModelSummary$;
  readonly modelTypeLabelMapping = ModelTypeLabelMapping;
  readonly modelTypes = Object.values(EModelTypes).filter(
    (value) => typeof value === 'string'
  );
  readonly opModeTypeLabelMapping = OpModeTypeLabelMapping;
  readonly opModeTypes = Object.values(EOpModeTypes).filter(
    (value) => typeof value === 'string'
  );
  readonly modelCreationSource = EModelCreationSource;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private modelEditFacade: ModelEditFacade,
    public dialogRef: MatDialogRef<CreateNewModelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.modelNameFrmControl.patchValue(
      `${this.data?.tagDesc} - ${
        this.opModeTypeLabelMapping[EOpModeTypes.SteadyState]
      }`
    );

    const opModeType =
      OpModeTypeMapping[this.opModeTypeFrmControl.value as EOpModeTypes];
    this.modelEditFacade.getModelTemplates({
      assetVariableTagMapId: this.data?.tagMapId,
      opModeTypeId: opModeType,
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  ngOnInit(): void {
    this.modelEditFacade.query.modelTemplates$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((modelTemplates) => {
        if (modelTemplates) {
          if (modelTemplates.length > 0) {
            this.enableTemplateRadioBtn = true;
            const modelType = modelTemplates[0]?.properties?.modelType
              ?.type as EModelTypes;
            this.modelTypeFrmControl.patchValue(modelType);
          } else {
            this.enableTemplateRadioBtn = false;
            this.modelCreationSourceFrmControl.patchValue(
              EModelCreationSource.Custom
            );
          }
        }
      });

    this.modelCreationSourceFrmControl.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val === EModelCreationSource.Template) {
          const opModeType =
            OpModeTypeMapping[this.opModeTypeFrmControl.value as EOpModeTypes];
          this.modelEditFacade.getModelTemplates({
            assetVariableTagMapId: this.data?.tagMapId,
            opModeTypeId: opModeType,
          });
          this.modelTypeFrmControl.disable();
        }

        if (val === EModelCreationSource.Custom) {
          this.modelTypeFrmControl.patchValue(
            EModelTypes.AdvancedPatternRecognition
          );
          this.enableTemplateRadioBtn = false;
          this.modelTypeFrmControl.enable();
        }
      });

    this.opModeTypeFrmControl.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        const opMode = val as EOpModeTypes;

        this.modelNameFrmControl.patchValue(
          `${this.data?.tagDesc} - ${this.opModeTypeLabelMapping[opMode]}`
        );

        this.enableTemplateRadioBtn = true;
        this.modelCreationSourceFrmControl.patchValue(
          EModelCreationSource.Template
        );

        const opModeType = OpModeTypeMapping[opMode];
        this.modelEditFacade.getModelTemplates({
          assetVariableTagMapId: this.data?.tagMapId,
          opModeTypeId: opModeType,
        });
      });

    this.modelTypeFrmControl.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (
          this.modelCreationSourceFrmControl.value ===
          EModelCreationSource.Custom
        ) {
          const modelType = val as EModelTypes;
          const opMode = this.opModeTypeFrmControl.value as EOpModeTypes;
          this.modelEditFacade.getBlankModel({
            modelName: this.modelNameFrmControl.value ?? 'New Model',
            assetVariableTagMapId: this.data?.tagMapId,
            opModeTypeId: OpModeTypeMapping[opMode],
            modelTypeId: ModelTypeMapping[modelType],
          });
        }
      });
  }
  public cancel() {
    this.dialogRef.close(null);
  }

  public sortedModels() {
    return this.modelTypes.sort((a, b) => (a > b ? 1 : a === b ? 0 : -1));
  }

  public createModel() {
    const opMode = this.opModeTypeFrmControl.value as EOpModeTypes;
    const modelType = this.modelTypeFrmControl.value as EModelTypes;
    const modelName = this.modelNameFrmControl.value ?? '';

    this.modelEditFacade.command.setOpModeTypes([opMode]);
    this.modelEditFacade.command.setNewModelType(modelType);
    this.modelEditFacade.command.setModelName(modelName);
    this.dialogRef.close(true);
  }
}
