import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IModelConfigData } from '@atonix/shared/api';

export interface MultiModelRebuildRequiredData {
  lockedModels: IModelConfigData[];
}

@Component({
  selector: 'atx-multi-model-no-change-rebuild',
  templateUrl: './multi-model-no-change-rebuild.component.html',
  styleUrls: ['./multi-model-no-change-rebuild.component.scss'],
})
export class MultiModelNoChangeRebuildComponent {
  public dbChanges: string[] = [];
  public lockedModels = '';
  constructor(
    public dialogRef: MatDialogRef<MultiModelNoChangeRebuildComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MultiModelRebuildRequiredData
  ) {
    data.lockedModels.forEach((lockedModel) => {
      if (this.lockedModels === '') {
        this.lockedModels += lockedModel.name;
      } else {
        this.lockedModels += ', ' + lockedModel.name;
      }
    });
  }
}
