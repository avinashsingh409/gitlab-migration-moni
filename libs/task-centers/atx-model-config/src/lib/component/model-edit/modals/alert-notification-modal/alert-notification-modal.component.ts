/* eslint-disable rxjs/no-nested-subscribe */
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { IModelConfigData, ISubscriber } from '@atonix/shared/api';
import { SubSink } from 'subsink';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { debounceTime, distinctUntilChanged, take, filter } from 'rxjs';
import { IAlertNotificationModel } from '../../../../service/model/alert-notification';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { isNil, isNilOrEmptyString } from '@atonix/atx-core';
import { ConfirmationDialogComponent } from '@atonix/shared/utils';

@Component({
  selector: 'atx-alert-notification-modal',
  templateUrl: './alert-notification-modal.component.html',
  styleUrls: ['./alert-notification-modal.component.scss'],
})
export class AlertNotificationModalComponent implements OnInit, OnDestroy {
  @ViewChild('usersInput')
  usersInput!: ElementRef<HTMLInputElement>;

  public readonly alertNotificationViewModel$ =
    this.modelEditFacade.query.alertNotificationModel$;
  public readonly selectedModel$ =
    this.modelEditFacade.query.selectedModelSummary$;

  public isMultiModel = false;
  public usersInputCtrl!: FormControl;
  public defaultMessageCtrl!: FormControl;
  public customMessageCtrl!: FormControl;
  public frequencyFormCtrl!: FormControl;

  private subs = new SubSink();
  constructor(
    private modelEditFacade: ModelEditFacade,
    private dialogRef: MatDialogRef<AlertNotificationModalComponent>,
    private matDialog: MatDialog
  ) {
    this.usersInputCtrl = this.buildUserInputCtrl();
    this.defaultMessageCtrl = this.buildDefaultMessageCtrl();
    this.frequencyFormCtrl = this.buildFrequencyInputCtrl();
    this.customMessageCtrl = this.buildCustomMessageCtrl();
  }

  ngOnInit(): void {
    this.subs.add(
      this.alertNotificationViewModel$
        .pipe(
          filter(
            (v) => !isNil(v) && !isNilOrEmptyString(v.alertNotificationId)
          ),
          take(1)
        )
        .subscribe((vm) => {
          if (
            !vm.isSaved &&
            (!isNil(vm.alertNotificationId) || vm.alertNotificationId !== '-1')
          ) {
            this.defaultMessageCtrl.setValue(vm.defaultMessage, {
              emitEvent: false,
            });
            this.customMessageCtrl.setValue(vm.customMessage, {
              emitEvent: false,
            });
            this.frequencyFormCtrl.setValue(vm.notificationFrequency, {
              emitEvent: false,
            });
          }
        })
    );

    this.modelEditFacade.query.modelSummaries$
      .pipe(take(1))
      .subscribe((summary) => {
        const selectedModels = (summary as IModelConfigData[]) ?? [];
        if (selectedModels.length > 1) {
          this.isMultiModel = true;
        }
      });

    this.subs.add(
      this.alertNotificationViewModel$.subscribe((vm) => {
        if (vm.isSaved) {
          this.modelEditFacade.query.modelSummaries$
            .pipe(take(1))
            .subscribe((summary) => {
              const selectedModels = (summary as IModelConfigData[]) ?? [];
              if (selectedModels.length > 1) {
                this.dialogRef.close(false);
              } else if (selectedModels.length === 1) {
                if (
                  !isNilOrEmptyString(vm.alertNotificationId) ||
                  vm.alertNotificationId !== '-1'
                ) {
                  this.dialogRef.close(!selectedModels[0]?.isDirty);
                }
              }
            });
        }
      })
    );

    this.subs.add(
      this.alertNotificationViewModel$.subscribe((vm) => {
        if (vm.deleted) {
          this.dialogRef.close(true);
        }
      })
    );
  }

  getTooltipMessage(user: ISubscriber) {
    let message = '';

    if (user.IsGroup && user.groupMembers && user.groupMembers?.length > 0) {
      for (const [index, regRec] of user.groupMembers.entries()) {
        if (index < 5) {
          message += `\n${regRec.FriendlyName}`;
        } else {
          message += `\n+${user.groupMembers.length - 5}`;
          break;
        }
      }
    }

    return message;
  }

  showGroupMembers(recipient: ISubscriber) {
    if (recipient.IsGroup) {
      recipient.groupMembers?.forEach((x) =>
        this.modelEditFacade.command.addAlertNotificationRecipient(x)
      );
      this.modelEditFacade.command.removeAlertNotificationRecipient(recipient);
    }
  }

  addUser(event: MatChipInputEvent): void {
    if (event.value) {
      this.alertNotificationViewModel$.pipe(take(1)).subscribe((vm) => {
        if (vm?.filteredUsers) {
          const user = vm?.filteredUsers.find(
            (user) =>
              user.FriendlyName.toLowerCase() === event.value.toLowerCase()
          );
          if (user) {
            this.modelEditFacade.command.addAlertNotificationRecipient(user);
            this.usersInput.nativeElement.value = '';
            this.usersInput.nativeElement.blur();
            this.usersInputCtrl.setValue(null);
          }
        }
      });
    }
  }

  selectUser(event: MatAutocompleteSelectedEvent): void {
    const user = event.option.value as ISubscriber;
    if (user) {
      this.modelEditFacade.command.addAlertNotificationRecipient(user);
      this.usersInput.nativeElement.value = '';
      this.usersInput.nativeElement.blur();
      this.usersInputCtrl.setValue(null);
    }
  }

  removeUser(recipient: ISubscriber): void {
    this.modelEditFacade.command.removeAlertNotificationRecipient(recipient);
    this.usersInput.nativeElement.value = '';
    this.usersInput.nativeElement.blur();
    this.usersInputCtrl.setValue(null);
  }

  loadUsers() {
    this.modelEditFacade.command.setFilteredUsers('');
  }

  saveAlertNotification() {
    this.alertNotificationViewModel$.pipe(take(1)).subscribe((model) => {
      const alertNotification = model as IAlertNotificationModel;
      this.modelEditFacade.command.saveAlertNotification(alertNotification);

      const subscribers =
        alertNotification.recipients?.map((r) => r.EmailAddress) ?? [];
      this.modelEditFacade.command.alertNotificationSubscribeUsers({
        alertStatusTypeId: alertNotification.alertStatusTypeId,
        modelId: alertNotification.modelId,
        subscribers,
      });

      const unsubscribedUsers =
        alertNotification.unsubscribedUsers?.map((r) => r.EmailAddress) ?? [];
      if (unsubscribedUsers.length > 0) {
        this.modelEditFacade.command.alertNotificationUnsubscribeUsers({
          alertStatusTypeId: alertNotification.alertStatusTypeId,
          modelId: alertNotification.modelId,
          subscribers: unsubscribedUsers,
        });
      }
    });
  }

  deleteAlertNotification() {
    const confirmationDialogRef = this.matDialog.open(
      ConfirmationDialogComponent,
      {
        disableClose: true,
        width: '250px',
        data: {
          isYesNo: true,
          title: 'Alert Notification',
          message: 'Are you sure you want to delete this notification?',
        },
      }
    );
    confirmationDialogRef.afterClosed().subscribe((val) => {
      if (val) {
        this.alertNotificationViewModel$.pipe(take(1)).subscribe((model) => {
          this.modelEditFacade.command.deleteAlertNotification({
            modelId: model.modelId ?? 0,
            alertNotificationId: model.alertNotificationId ?? '',
          });
        });
      }
    });
  }

  buildUserInputCtrl() {
    const ctrl = new FormControl();
    this.subs.add(
      ctrl.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((val) => {
          if (typeof val === 'string') {
            this.modelEditFacade.command.setFilteredUsers(val);
          }
        })
    );

    return ctrl;
  }

  buildDefaultMessageCtrl() {
    const ctrl = new FormControl();
    return ctrl;
  }

  buildCustomMessageCtrl() {
    const ctrl = new FormControl();
    this.subs.add(
      ctrl.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((val) => {
          if (this.customMessageCtrl.dirty) {
            this.modelEditFacade.command.setAlertNotificationCustomMessage(val);
          }
        })
    );
    return ctrl;
  }

  buildFrequencyInputCtrl() {
    const ctrl = new FormControl<number>(24, [Validators.required]);
    this.subs.add(
      ctrl.valueChanges.pipe(debounceTime(100)).subscribe((val) => {
        if (this.frequencyFormCtrl.dirty) {
          const frequency = Number(val);
          this.frequencyFormCtrl.setErrors(null);
          if (isNaN(frequency) || frequency <= 0 || frequency > 999) {
            this.frequencyFormCtrl.setErrors({ invalidFrequencyValue: true });
          } else {
            this.modelEditFacade.command.setAlertNotificationFrequency(
              frequency
            );
          }
        }
      })
    );

    return ctrl;
  }

  ngOnDestroy(): void {
    this.modelEditFacade.command.resetAlertNotificationModel();
    this.subs.unsubscribe();
  }
}
