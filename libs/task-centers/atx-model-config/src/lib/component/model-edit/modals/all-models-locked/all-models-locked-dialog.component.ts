import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-all-models-locked-dialog',
  templateUrl: './all-models-locked-dialog.component.html',
  styleUrls: ['./all-models-locked-dialog.component.scss'],
})
export class AllModelsLockedDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<AllModelsLockedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
}
