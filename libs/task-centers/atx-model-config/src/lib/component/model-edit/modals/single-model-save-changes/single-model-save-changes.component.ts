import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface SingleModelSaveData {
  modelName: string;
  validationObject: any[];
}

@Component({
  selector: 'atx-single-model-save-changes',
  templateUrl: './single-model-save-changes.component.html',
  styleUrls: ['./single-model-save-changes.component.scss'],
})
export class SingleModelSaveChangesComponent {
  public dbChanges: string[] = [];
  constructor(
    public dialogRef: MatDialogRef<SingleModelSaveChangesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SingleModelSaveData
  ) {
    if (data?.validationObject?.length > 0) {
      this.dbChanges.push('The following changes will be made:\n');
      data.validationObject.forEach((val) => {
        let dbChange = '';
        if (val?.Change) {
          const newValue = val?.NewValue === '' ? 'Null' : val.NewValue;
          const oldValue = val?.OldValue === '' ? 'Null' : val.OldValue;
          dbChange = `DbChange: ${val.Change} \n NewValue: ${newValue} \n OldValue: \xa0\xa0${oldValue} \n\n`;
          this.dbChanges.push(dbChange);
        }
      });
    }
  }
}
