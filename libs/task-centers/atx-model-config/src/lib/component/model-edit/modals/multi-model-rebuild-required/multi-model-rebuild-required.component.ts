import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IModelConfigData } from '@atonix/shared/api';

export interface MultiModelRebuildRequiredData {
  dirtyModels: IModelConfigData[];
  lockedModels: IModelConfigData[];
}

@Component({
  selector: 'atx-multi-model-rebuild-required',
  templateUrl: './multi-model-rebuild-required.component.html',
  styleUrls: ['./multi-model-rebuild-required.component.scss'],
})
export class MultiModelRebuildRequiredComponent {
  public changedModels = '';
  public lockedModels = '';
  constructor(
    public dialogRef: MatDialogRef<MultiModelRebuildRequiredComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MultiModelRebuildRequiredData
  ) {
    data.dirtyModels.forEach((changedModel) => {
      if (this.changedModels === '') {
        this.changedModels += changedModel.name;
      } else {
        this.changedModels += ', ' + changedModel.name;
      }
    });
    data.lockedModels.forEach((lockedModel) => {
      if (this.lockedModels === '') {
        this.lockedModels += lockedModel.name;
      } else {
        this.lockedModels += ', ' + lockedModel.name;
      }
    });
  }
}
