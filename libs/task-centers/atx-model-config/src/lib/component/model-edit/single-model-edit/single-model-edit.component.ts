import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  Inject,
  ViewChildren,
  QueryList,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getModelConfigRoute, NavFacade } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import * as modelEditActions from '../../../store/actions/model-edit.actions';
import {
  selectModelEditAssetTreeDropdownAssetName,
  selectModelEditAssetTreeDropdownConfiguration,
  selectModelEditAssetTreeDropdownSelectedAssetIds,
} from '../../../store/reducers/model-config.reducer';
import { firstValueFrom, Subject } from 'rxjs';
import {
  ModelTypeLabelMapping,
  ModelTypeMapping,
  EModelTypes,
  OpModeTypeLabelMapping,
  OpModeTypeMapping,
  EOpModeTypes,
  IModelConfigData,
  EPredictiveTypes,
  DataExplorerCoreService,
  AlertsFrameworkService,
} from '@atonix/shared/api';
import { selectTimeSlider } from '../../../store/reducers/model-config.reducer';
import { ModelEditFacade } from '../../../service/model-edit.facade';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  take,
  takeUntil,
} from 'rxjs/operators';
import { ModelEditTagListFacade } from '../../../service/model-edit-tag-list.facade';
import {
  ModelConfigBusEventTypes,
  ModelConfigEventBus,
} from '../../../store/services/model-config-event-bus';
import { UntypedFormControl } from '@angular/forms';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import * as timeSliderActions from '../../../store/actions/time-slider.actions';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertsActionEvent } from '../../../service/model-actions/model-actions.models';
import { ModelConfigFacade } from '../../../store/facade/model-config.facade';
import { SingleModelSaveChangesComponent } from '../modals/single-model-save-changes/single-model-save-changes.component';
import * as signalR from '@microsoft/signalr';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { CustomRetryPolicy } from '../../../service/signal-r.facade';
import { AuthService } from '@atonix/shared/state/auth';
import { SingleModelFacade } from '../../../service/single-model.facade';
import {
  ActionsType,
  ModelActionsConfirmationDialogComponent,
} from '../../common/modals/model-actions-confirmation-dialog/model-actions-confirmation-dialog.component';
import { ModelActionsFacade } from '../../../service/model-actions/model-actions.facade';
import { SubSink } from 'subsink';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSelectChange } from '@angular/material/select';
import { isNil, isNilOrEmptyString } from '@atonix/atx-core';
import { ITreeStateChange, selectAsset } from '@atonix/atx-asset-tree';
import { CreateNewModelDialogComponent } from '../modals/create-model-dialog/create-new-model-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SplitAreaDirective, SplitComponent } from 'angular-split';

@Component({
  selector: 'atx-single-model-edit',
  templateUrl: './single-model-edit.component.html',
  styleUrls: ['./single-model-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingleModelEditComponent implements OnInit, OnDestroy {
  @ViewChild('topSection', { static: false })
  public topSection!: ElementRef;
  @ViewChild('sidenav') public sidenav!: MatSidenav;
  @ViewChild(SplitComponent) splitEl: SplitComponent | undefined;
  @ViewChildren(SplitAreaDirective) areasEl!: QueryList<SplitAreaDirective>;
  topSizePercent = 60;
  bottomSizePercent = 40;
  public toggleTop = false;
  public toggleBottom = false;

  public opModesFormCtrl = new UntypedFormControl();
  public modelNameFormCtrl = new UntypedFormControl();
  public selectedTopIndex = 0;
  public selectedBottomIndex = 0;
  public PDNDModelTrendData: any = {};
  private onDestroy = new Subject<void>();
  private modelsLoaded = new Subject<void>();
  private subs = new SubSink();
  readonly timeSlider$ = this.store.select(selectTimeSlider);
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly modelActionsVm$ = this.modelActionsFacade.query.vm$;
  readonly modelTypeLabelMapping = ModelTypeLabelMapping;
  readonly modelTypes = Object.values(EModelTypes).filter(
    (value) => typeof value === 'string'
  );
  public readonly modelTypeEnum = EModelTypes;

  readonly opModeTypeLabelMapping = OpModeTypeLabelMapping;
  readonly opModeTypes = Object.values(EOpModeTypes).filter(
    (value) => typeof value === 'string'
  );
  readonly privateAlertsMicroService: string = '';

  connection: signalR.HubConnection | null = null;

  constructor(
    public navFacade: NavFacade,
    private activatedRoute: ActivatedRoute,
    private modelEditFacade: ModelEditFacade,
    private modelEditTagListFacade: ModelEditTagListFacade,
    private modelConfigEventBus: ModelConfigEventBus,
    private modelConfigFacade: ModelConfigFacade,
    private readonly store: Store,
    public snackBar: MatSnackBar,
    private authService: AuthService,
    private modelActionsFacade: ModelActionsFacade,
    private singleModelFacade: SingleModelFacade,
    private matDialog: MatDialog,
    private alertsFrameworkService: AlertsFrameworkService,
    private dataExplorerCoreService: DataExplorerCoreService,
    private logger: LoggerService,
    private router: Router,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.logger.tabClicked('Single Model Edit', 'Model Config');
    // this.privateAlertsMicroService = 'https://localhost:46909';
    this.privateAlertsMicroService = this.appConfig.baseMicroServicesURL;
    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.MODEL_EDIT_ASSET_TREE_DROPDOWN_STATE_CHANGE,
      this.assetTreeDropdownStateChange.bind(this)
    );
  }

  ngOnInit(): void {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    const assetTagMapId =
      this.activatedRoute.snapshot.queryParamMap.get('tmid') || 0;
    const tagDesc = this.activatedRoute.snapshot.queryParamMap.get('td') || '';

    this.PDNDModelTrendData = {
      model: modelConfigRoute.model,
      asset: modelConfigRoute.asset,
      startDate: modelConfigRoute.startDate,
      endDate: modelConfigRoute.endDate,
    };
    if (modelConfigRoute.model === '-1') {
      this.openCreateNewModelDialog(+assetTagMapId, tagDesc);
      this.modelEditFacade.command.setShowNewModelDialog(true);
    } else {
      this.loadModel();
    }

    this.modelEditFacade.query.reloadModels$
      .pipe(filter((x) => x === true))
      .subscribe(() => {
        this.modelEditFacade.command.resetModelEditState();
        this.loadModel();
      });
  }

  private initializeNewModel() {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.signalRConnect();
    this.modelEditFacade.command.setModelActiveInactive();
    this.store.dispatch(
      modelEditActions.initializeModelConfigEdit({
        assetID: modelConfigRoute.asset,
        start: modelConfigRoute.startDate,
        end: modelConfigRoute.endDate,
      })
    );
    this.launchObservers();
  }

  private loadModel() {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.signalRConnect();
    this.store.dispatch(
      modelEditActions.initializeModelConfigEdit({
        assetID: modelConfigRoute.asset,
        start: modelConfigRoute.startDate,
        end: modelConfigRoute.endDate,
      })
    );
    this.modelEditFacade.getModelSummaries({
      modelIds: [modelConfigRoute.model],
      refreshTrends: false,
      isMultiModel: false,
    });
    this.modelEditFacade.getModelMathMessage(modelConfigRoute.model);
    this.launchObservers();
  }

  private launchObservers() {
    this.modelEditFacade.query.modelEntities$
      .pipe(takeUntil(this.modelsLoaded))
      .subscribe((models) => {
        if (!isNil(models) && Object.keys(models).length > 0) {
          const tagMapIDs = [];
          const buildDates = [];
          const saveDates = [];
          let newModel = false;
          for (const key in models) {
            if (
              models[key]?.legacy?.modelID === 0 ||
              models[key]?.legacy?.modelID === -1
            ) {
              newModel = true;
            }
            tagMapIDs.push(
              models[key]?.dependent.assetVariableTagMapID as number
            );
            buildDates.push(
              models[key]?.buildProperties?.status?.builtSinceTime
            );
            saveDates.push(models[key]?.buildProperties?.status?.lastSaved);
          }
          if (!newModel) {
            this.selectOpModeDefinition(
              tagMapIDs,
              buildDates as Date[],
              saveDates as Date[]
            );
          }

          this.selectTagListAsset(tagMapIDs);
        }
      });

    this.modelEditFacade.query.updateFormElements$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((formElements) => {
        if (formElements === true) {
          this.getCurrentModelState();
        }
      });

    this.modelEditFacade.query.reloadModelStateWatcher$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((state) => {
        if (state.modelRefreshAction) {
          if (state.modelRefreshAction.reloadModel) {
            if (state.isNewModel) {
              const modelConfigRoute = getModelConfigRoute(
                this.activatedRoute.snapshot.queryParamMap
              );
              // refresh page
              this.router.navigate(['/']).then(() => {
                // Then navigate to desired url
                this.router.navigate(['/model-config/m'], {
                  queryParams: {
                    modelId: state.modelSummary?.modelExtID,
                    aid: modelConfigRoute.asset,
                    start: modelConfigRoute.start,
                    end: modelConfigRoute.end,
                  },
                });
              });
            }
            this.modelEditFacade.getModelSummaries({
              modelIds: [state.modelSummary?.modelExtID as string],
              refreshTrends: true,
              isMultiModel: false,
            });
            this.modelEditFacade.getModelMathMessage(
              state.modelSummary?.modelExtID as string
            );
          } else if (state.modelRefreshAction.reloadPredictiveMethodsNeeded) {
            this.reloadPredictiveMethodType(
              state.modelSummary?.modelExtID as string
            );
          }
        }
      });

    this.modelEditFacade.query.modelValidationReturn$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((validationObject) => {
        if (validationObject) {
          this.openSaveModal(validationObject);
        }
      });

    this.modelEditFacade.query.predictiveMethodTypeSelected$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((methodType) => {
        if (methodType !== undefined && methodType !== 'none') {
          this.selectNewPredictiveType(methodType);
        }
      });
    this.store
      .select(selectModelEditAssetTreeDropdownConfiguration)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((assetTreeConfiguration) => {
        this.modelEditTagListFacade.updateAssetTreeConfiguration(
          assetTreeConfiguration
        );
      });

    this.store
      .select(selectModelEditAssetTreeDropdownAssetName)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((assetName) => {
        if (assetName) {
          this.modelEditTagListFacade.updateAssetName(assetName);
        }
      });
    this.store
      .select(selectModelEditAssetTreeDropdownSelectedAssetIds)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((ids) => {
        this.modelEditTagListFacade.assetID$
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((assetID: string | null) => {
            if (ids && assetID !== ids.assetUniqueKey) {
              this.modelEditTagListFacade.updateAssetID(ids.assetUniqueKey);
              this.modelEditTagListFacade.updateSearchAssetID(ids.assetId);
            }
          });
      });
  }

  selectNewPredictiveType(methodType: EPredictiveTypes) {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((vm) => {
        this.loadPredictiveMethods(methodType, vm?.modelExtID || '');
      });
  }

  selectTopTab(tab: number) {
    this.selectedTopIndex = tab;
  }
  selectBottomTab(tab: number) {
    this.selectedBottomIndex = tab;
  }

  reloadPredictiveMethodType(modelExtID: string) {
    this.modelEditFacade.query.predictiveMethodTypeSelected$
      .pipe(take(1))
      .subscribe((methodType) => {
        if (methodType !== undefined && methodType !== 'none') {
          this.loadPredictiveMethods(methodType, modelExtID);
        }
      });
  }

  loadPredictiveMethods(methodType: EPredictiveTypes, modelExtID: string) {
    const modelConfigRoute = getModelConfigRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    if (
      !isNilOrEmptyString(modelExtID) &&
      modelExtID !== '-1' &&
      modelExtID !== '0'
    ) {
      setTimeout(() => {
        this.modelEditFacade.getModelTrend({
          modelId: modelExtID,
          predictiveMethodType: methodType,
          startDate: modelConfigRoute.startDate,
          endDate: modelConfigRoute.endDate,
        });
      }, 2000);
    }
  }

  signalRConnect() {
    if (!this.connection) {
      this.connection = new signalR.HubConnectionBuilder()
        .configureLogging(signalR.LogLevel.Debug)
        .withUrl(
          `${this.privateAlertsMicroService}/api/models/hub/notification`,
          {
            accessTokenFactory: async () =>
              await firstValueFrom(this.authService.GetIdTokenOrWait()),
            transport: signalR.HttpTransportType.WebSockets,
            skipNegotiation: true,
          }
        )
        .withAutomaticReconnect(new CustomRetryPolicy(this.singleModelFacade))
        .build();

      this.connection
        .start()
        .then(() => {
          this.singleModelFacade.connected();
          // console.log('SignalR Connected!');
        })
        .catch((err) => {
          this.singleModelFacade.disconnected();
          return console.error(err.toString());
        });
      this.connection.on('signalRModelSend', (value: any) => {
        // console.log(value);
        this.modelEditFacade.query.selectedModelSummary$
          .pipe(take(1))
          .subscribe((selectedModelSummary) => {
            if (!isNil(selectedModelSummary?.modelExtID)) {
              const signalRExtID =
                (value?.modelExtID as string).toLocaleLowerCase() || '';
              const selectedModelExtID =
                selectedModelSummary?.modelExtID?.toLocaleLowerCase() || '';
              if (signalRExtID === selectedModelExtID) {
                this.modelEditFacade.command.singleModelSignalRMessage({
                  signalRMessage: value?.message,
                });
              }
            }
          });
      });
      this.connection.onreconnected((connectionId) => {
        this.singleModelFacade.connected();
        console.log('reconnected');
      });
      this.connection.onclose((error) => {
        this.singleModelFacade.disconnected();
        console.log(error);
      });
    }
  }

  openSaveModal(validationObject: any) {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((selectedModelSummary) => {
        if (!validationObject?.NotificationRequired) {
          validationObject.Model.ModelRequest = {
            PostStatus: `${this.privateAlertsMicroService}/api/models/status`,
            PostFailure: `${this.privateAlertsMicroService}/api/models/buildfailure`,
            PostFulfillment: `${this.privateAlertsMicroService}/api/models/admit`,
          };
          validationObject.UserDeclinedRebuild = false;
          this.modelEditFacade.command.modelExecuteSaveActions({
            validationObject,
            isMulti: false,
            message: '',
            modelExtID: null,
          });
          return;
        }
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = {
          modelName: validationObject?.Model?.ModelName,
          validationObject: validationObject?.UpdateDB?.Changes,
        };
        dialogConfig.panelClass = 'custom-model-rebuild-container';

        const dialogRef = this.matDialog.open(
          SingleModelSaveChangesComponent,
          dialogConfig
        );
        // eslint-disable-next-line rxjs/no-nested-subscribe
        dialogRef.afterClosed().subscribe((result) => {
          validationObject.Model.ModelRequest = {
            PostStatus: `${this.privateAlertsMicroService}/api/models/status`,
            PostFailure: `${this.privateAlertsMicroService}/api/models/buildfailure`,
            PostFulfillment: `${this.privateAlertsMicroService}/api/models/admit`,
          };
          if (result) {
            if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.MovingAverage
            ) {
              this.selectBottomTab(4);
            } else if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.ROC
            ) {
              this.selectBottomTab(4);
            } else if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.FrozenData
            ) {
              this.selectBottomTab(2);
            } else if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.FixedLimit
            ) {
              this.selectBottomTab(2);
            } else if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.RollingAverage
            ) {
              this.selectBottomTab(3);
            } else if (
              selectedModelSummary?.properties.modelType?.type ===
              EModelTypes.Forecast
            ) {
              this.selectBottomTab(1);
            } else {
              this.selectBottomTab(4);
            }

            this.selectTopTab(0);
            validationObject.UserDeclinedRebuild = false;
            this.checkSignalRStatus();
          } else {
            validationObject.UserDeclinedRebuild = true;
          }
          this.modelEditFacade.command.modelExecuteSaveActions({
            validationObject,
            isMulti: false,
            message: '',
            modelExtID: null,
          });
        });
      });
  }

  checkSignalRStatus() {
    this.singleModelFacade.isDisconnected$
      .pipe(take(1))
      .subscribe((isDisconnected) => {
        if (isDisconnected) {
          this.connection = null;
          this.signalRConnect();
        }
      });
  }

  modelTypeChange(changeEvent: MatSelectChange) {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (model && model.modelExtID) {
          const modelType = changeEvent.value as EModelTypes;
          const opModeTypes = model.properties.opModeTypes;
          let opMode: EOpModeTypes;
          if (opModeTypes?.includes(EOpModeTypes.SteadyState)) {
            opMode = EOpModeTypes.SteadyState;
          } else if (opModeTypes?.includes(EOpModeTypes.Startup)) {
            opMode = EOpModeTypes.Startup;
          } else if (opModeTypes?.includes(EOpModeTypes.Transient)) {
            opMode = EOpModeTypes.Transient;
          } else {
            opMode = EOpModeTypes.OutOfService;
          }
          this.modelEditFacade.changeModelType({
            active: model.active,
            assetVariableTagMapId: model.dependent.assetVariableTagMapID,
            modelExtID: model.modelExtID,
            modelId: model.legacy.modelID,
            modelName: model.name,
            modelTypeId: ModelTypeMapping[modelType],
            opModeTypeId: OpModeTypeMapping[opMode],
            opModeTypes: model.properties.opModeTypes,
            buildProperties: model.buildProperties,
          });
        }
      });
  }

  getDefaultOpMode(opMode: EOpModeTypes) {
    const selectedOpModes = [...this.opModesFormCtrl.value];
    if (selectedOpModes.indexOf(opMode) > 0) {
      return this.opModeTypeLabelMapping[opMode];
    }
    const defaultOpModeType = selectedOpModes[0] as EOpModeTypes;
    return this.opModeTypeLabelMapping[defaultOpModeType];
  }

  assetTreeDropdownStateChange(command: { treeStateChange: ITreeStateChange }) {
    if (command) {
      this.store.dispatch(modelEditActions.tagListAssetStateChange(command));
    }
  }

  discardChanges() {
    this.logger.trackTaskCenterEvent(
      'ModelConfig:SingleModelEdit:DiscardChanges'
    );
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((selectedModelSummary: unknown) => {
        this.modelEditFacade.command.discardChanges();
        this.modelEditFacade.getModelSummaries({
          modelIds: [(selectedModelSummary as IModelConfigData).modelExtID],
          refreshTrends: true,
          isMultiModel: false,
        });
      });
  }

  saveChanges() {
    this.logger.trackTaskCenterEvent('ModelConfig:SingleModelEdit:SaveModel');
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.modelEditFacade.command.beginSaveModelGetTransform({
        isMulti: false,
        model: vm.selectedModelSummary,
      });
    });
  }

  onTimeSliderStateChange(change: ITimeSliderStateChange) {
    // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
    this.store.dispatch(
      timeSliderActions.timeSliderStateChange({ stateChange: change })
    );
    if (change.event === 'SelectDateRange') {
      if (change?.newStartDateValue && change?.newEndDateValue) {
        // eslint-disable-next-line ngrx/avoid-dispatching-multiple-actions-sequentially
        this.store.dispatch(
          timeSliderActions.updateRouteTimeSlider({
            start: change.newStartDateValue,
            end: change.newEndDateValue,
          })
        );
        const modelConfigRoute = getModelConfigRoute(
          this.activatedRoute.snapshot.queryParamMap
        );
        this.modelEditFacade.updateModelTrendTime(
          change.newStartDateValue,
          change.newEndDateValue,
          modelConfigRoute.model
        );
        this.modelEditFacade.updateModelContextTrendTime(
          change.newStartDateValue,
          change.newEndDateValue,
          modelConfigRoute.asset
        );
        this.PDNDModelTrendData.startDate = change.newStartDateValue;
        this.PDNDModelTrendData.endDate = change.newEndDateValue;
      }
    }
  }

  public trackTopTabClick(event: any) {
    if (!isNil(event?.index)) {
      if (+event.index === 0) {
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:ModelTrendTab'
        );
      } else if (+event.index === 1) {
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:ExpectedVsActualTab'
        );
      } else if (+event.index === 2) {
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:HistoryTab'
        );
      } else if (+event.index === 3) {
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:ContextTab'
        );
      }
    }
  }

  public trackBottomTabClick(event: any) {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (!isNil(event?.index)) {
          if (+event.index === 0) {
            if (model?.properties.modelType.type === EModelTypes.Forecast) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:ForecastTab'
              );
            } else {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:InputsTab'
              );
            }
          } else if (+event.index === 1) {
            if (model?.properties.modelType.type === EModelTypes.Forecast) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:BuildStatusTab'
              );
            }
            this.logger.trackTaskCenterEvent(
              'ModelConfig:SingleModelEdit:TrainingRangeTab'
            );
          } else if (+event.index === 2) {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:SingleModelEdit:SensitivityTab'
            );
          } else if (+event.index === 3) {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:SingleModelEdit:SensitivityTab'
            );
          } else if (+event.index === 4) {
            if (
              model?.properties.modelType.type === EModelTypes.MovingAverage
            ) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:AveragesTab'
              );
            } else if (model?.properties.modelType.type === EModelTypes.ROC) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:RateOfChangeTab'
              );
            } else {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:BuildStatusTab'
              );
            }
          } else if (+event.index === 5) {
            if (
              model?.properties.modelType.type === EModelTypes.MovingAverage ||
              model?.properties.modelType.type === EModelTypes.ROC
            ) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:BuildStatusTab'
              );
            }
          }
        }
      });
  }

  public deleteModels(): void {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (model && model.modelExtID) {
          const dialogRef = this.matDialog.open(
            ModelActionsConfirmationDialogComponent,
            {
              disableClose: true,
              width: '600px',
              data: {
                models: [
                  {
                    modelId: model?.legacy.modelID,
                    modelName: model?.name,
                  },
                ],
                actionType: ActionsType.Delete,
              },
            }
          );
          // eslint-disable-next-line rxjs/no-nested-subscribe
          dialogRef.afterClosed().subscribe((result) => {
            if (typeof result === 'boolean' && result) {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:SingleModelEdit:DeleteModel'
              );
              this.modelEditFacade.command.resetModelEditState();
              this.opModesFormCtrl.patchValue(null);
            }
          });
        }
      });
  }

  public activateDeactivateModel(): void {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (model && model.modelExtID) {
          this.confirmModelActivation(model);
        }
      });
  }

  private getCurrentModelState() {
    this.modelEditFacade.query.selectedModelSummary$
      .pipe(take(1))
      .subscribe((model) => {
        if (model && model.properties?.opModeTypes) {
          this.createOpModeAndModelNameForms(
            model.properties.opModeTypes,
            model.name
          );
        } else {
          this.modelEditFacade.command.updateFormComplete();
        }
      });
  }

  private createOpModeAndModelNameForms(
    opModeTypes: EOpModeTypes[],
    modelName: string
  ) {
    this.subs.unsubscribe();
    this.opModesFormCtrl = new UntypedFormControl(opModeTypes);
    this.modelNameFormCtrl = new UntypedFormControl(modelName);
    this.subs.add(
      this.opModesFormCtrl.valueChanges
        .pipe(distinctUntilChanged())
        .subscribe((value) => {
          this.modelEditFacade.command.setOpModeTypes(value);
        })
    );
    this.subs.add(
      this.modelNameFormCtrl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          this.modelEditFacade.command.setModelName(value);
        })
    );
    this.modelEditFacade.command.updateFormComplete();
  }

  confirmModelActivation(newModel: IModelConfigData) {
    if (newModel.active) {
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:DeactivateModel'
      );
    } else {
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:ActivateModel'
      );
    }
    const dialogRef = this.matDialog.open(
      ModelActionsConfirmationDialogComponent,
      {
        disableClose: true,
        width: '600px',
        data: {
          models: [
            {
              modelName: newModel?.name,
            },
          ],
          actionType: newModel.active
            ? ActionsType.Deactivate
            : ActionsType.Activate,
        },
      }
    );

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.modelEditFacade.command.setModelActiveInactive();
      }
    });
  }

  alertsActionChange(event: AlertsActionEvent) {
    if (event.event === 'ToggleActionFlyout') {
      this.toggleAlertsActionsPane();
    } else if (event.event === 'OpenOpModeConfig') {
      this.modelConfigFacade.modelConfigActionsOpenOpMode(event.newValue);
    } else if (event.event === 'OpenDiagnosticDrilldown') {
      const dddParams = event.newValue;
      this.modelConfigFacade.modelConfigActionsOpenDiagnosticDrillDown(
        dddParams.assetGuid,
        dddParams.model,
        dddParams.start,
        dddParams.end,
        dddParams.mtype,
        dddParams.ddurl
      );
    } else if (event.event === 'OpenDataExplorer') {
      this.modelConfigFacade.modelConfigActionsOpenDataExplorer(event.newValue);
    } else if (event.event === 'ShowRelatedModels') {
      this.modelConfigFacade.modelConfigActionsShowRelatedAlerts(
        event.newValue
      );
    } else if (event.event === 'ShowRelatedIssues') {
      this.modelConfigFacade.modelConfigActionsShowRelatedIssues(
        event.newValue
      );
    } else if (event.event === 'NewIssue') {
      this.modelConfigFacade.modelConfigActionsCreateNewIssue(event.newValue);
    }
  }

  toggleAlertsActionsPane() {
    this.sidenav.toggle();
    this.logger.trackTaskCenterEvent(
      'ModelConfig:SingleModelEdit:ToggleAlertsActionsPane'
    );
    this.modelConfigFacade.treeSizeChange(300);
  }

  openCreateNewModelDialog(tagMapId: number, tagDesc: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.disableClose = true;
    dialogConfig.height = '450px';
    dialogConfig.width = '500px';
    dialogConfig.maxWidth = '500px';
    dialogConfig.panelClass = 'custom-new-model-container';
    dialogConfig.data = { tagMapId, tagDesc };
    const dialogRef = this.matDialog.open(
      CreateNewModelDialogComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        this.modelEditFacade.command.setShowNewModelDialog(false);
        this.initializeNewModel();
      } else {
        self.close();
      }
    });
  }

  private selectOpModeDefinition(
    tagMapIDs: number[],
    buildDates: Date[] | undefined,
    saveDates: Date[] | undefined
  ) {
    if (tagMapIDs && tagMapIDs.length > 0 && buildDates && saveDates)
      this.modelEditFacade.command.selectOpModeDefinition({
        astVarTypeTagMapID: tagMapIDs[0],
        buildDates,
        saveDates,
      });
  }

  private selectTagListAsset(tagMapIDs: number[]) {
    this.dataExplorerCoreService
      .assetParamsForTagGrid(tagMapIDs)
      .pipe(take(1))
      .subscribe((assetSelection) => {
        if (assetSelection && assetSelection?.AssetID) {
          this.modelEditTagListFacade.updateAssetID(assetSelection?.AssetID);
          this.modelEditTagListFacade.updateSearchAssetID(
            assetSelection?.SearchAssetID
          );
          this.store.dispatch(
            modelEditActions.tagListAssetStateChange({
              treeStateChange: selectAsset(assetSelection?.AssetID),
            })
          );
        }
      });

    this.modelsLoaded.next();
    this.modelsLoaded.complete();
  }

  onExpandTop() {
    this.toggleTop = true;
    this.toggleBottom = false;
    this.areasEl.toArray()[1].visible = false;
    this.areasEl.toArray()[1].collapse();
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].expand();
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }
  splitDragEnd(e: { gutterNum: number; sizes: Array<number> }) {
    if (e?.sizes) {
      this.topSizePercent = e.sizes[0];
      this.bottomSizePercent = e.sizes[1];
    }
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }
  onExpandBottom() {
    this.toggleTop = false;
    this.toggleBottom = true;
    this.areasEl.toArray()[0].visible = false;
    this.areasEl.toArray()[0].collapse();
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].expand();
  }

  onCollapse() {
    this.areasEl.toArray()[0].visible = true;
    this.areasEl.toArray()[0].collapse(this.topSizePercent);
    this.areasEl.toArray()[0].order = 0;
    this.areasEl.toArray()[1].visible = true;
    this.areasEl.toArray()[1].collapse(this.bottomSizePercent);
    this.areasEl.toArray()[1].order = 1;
    this.toggleTop = false;
    this.toggleBottom = false;
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 200);
  }

  topIndicatorClick() {
    if (!this.toggleTop) {
      this.onExpandTop();
    } else {
      this.onCollapse();
    }
  }

  bottomIndicatorClick() {
    if (!this.toggleBottom) {
      this.onExpandBottom();
    } else {
      this.onCollapse();
    }
  }

  ngOnDestroy(): void {
    this.modelEditFacade.command.resetModelEditState();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
