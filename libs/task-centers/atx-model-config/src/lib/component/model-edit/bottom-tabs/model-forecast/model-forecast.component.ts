import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditRequestEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GetContextMenuItemsParams,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import {
  SensitivityAnomalyTypeLabel,
  SensitivityAnomalyTypes,
} from '../../../../service/model/sensitivity-chips';

import { EModelTypes, IModelConfigData } from '@atonix/shared/api';
import { forecastColumnDefs } from './model-forecast-column-defs';
import {
  DateCellEditorComponent,
  NullableIntCellEditorComponent,
  NullableNumericCellEditorComponent,
  NumericCellEditorComponent,
  PositiveIntCellEditorComponent,
  RealNumberNumericCellEditorComponent,
} from '@atonix/shared/ui';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-forecast',
  templateUrl: './model-forecast.component.html',
  styleUrls: ['./model-forecast.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelForecastComponent {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  readonly sensitivityAnomalyTypeLabel = SensitivityAnomalyTypeLabel;
  readonly sensitivityAnomalyTypes = Object.values(
    SensitivityAnomalyTypes
  ).filter((value) => typeof value === 'number');
  public readonly modelTypes = EModelTypes;

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  public columnDefs: (ColDef | ColGroupDef)[] = [...forecastColumnDefs];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    enableBrowserTooltips: true,
    components: {
      dateCellEditor: DateCellEditorComponent,
      numericCellEditor: NumericCellEditorComponent,
      positiveIntCellEditor: PositiveIntCellEditorComponent,
      nullableIntCellEditor: NullableIntCellEditorComponent,
      nullableNumericCellEditor: NullableNumericCellEditorComponent,
      realNumberNumericCellEditor: RealNumberNumericCellEditorComponent,
    },
    stopEditingWhenCellsLoseFocus: true,
    debug: false,
    readOnlyEdit: true,
    cacheBlockSize: 100,
    singleClickEdit: true,
    headerHeight: 30,
    rowHeight: 30,
    tooltipShowDelay: 0,
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      filter: false,
      sortable: false,
    },
    suppressAggFuncInHeader: true,
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditRequest: (event: CellEditRequestEvent) => {
      this.cellEditRequest(
        event.data.modelExtID,
        event.colDef.field || '',
        event.newValue
      );
    },
  };

  cellEditRequest(modelExtID: string, columnField: string, newValue: any) {
    if (columnField === 'properties.modelType.properties.horizonType') {
      this.modelEditFacade.command.setForecastHorizonType({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (
      columnField === 'properties.modelType.properties.horizonPeriod'
    ) {
      this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanDuration(
        {
          modelExtID: modelExtID,
          columnField: '',
          newValue: newValue,
        }
      );
    } else if (
      columnField === 'properties.modelType.properties.horizonPeriodUnitOfTime'
    ) {
      this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanUnits({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'fixedEarliestInterceptTime') {
      this.modelEditFacade.command.setForecastFixedHorizonTimeChanged({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'fixedEarliestInterceptDate') {
      this.modelEditFacade.command.setForecastFixedHorizonDateChanged({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: columnField || '',
        newValue: newValue,
      });
    }
  }

  focusOut() {
    this.gridApi.deselectAll();
  }
  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;
    if (this.isMultiModel) {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            const fieldName = colDef.field || '';
            const newValue = params.value;
            params?.api?.forEachNode((rowNode: RowNode) => {
              const currentModel = rowNode.data as IModelConfigData;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                if (
                  fieldName === 'properties.modelType.properties.horizonType'
                ) {
                  this.modelEditFacade.command.setForecastHorizonType({
                    modelExtID: rowNode.data.modelExtID,
                    columnField: '',
                    newValue: newValue,
                  });
                } else if (
                  fieldName === 'properties.modelType.properties.horizonPeriod'
                ) {
                  if (
                    currentModel.properties.modelType.properties
                      ?.horizonType === 'Relative'
                  ) {
                    this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanDuration(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: newValue,
                      }
                    );
                  }
                } else if (
                  fieldName ===
                  'properties.modelType.properties.horizonPeriodUnitOfTime'
                ) {
                  if (
                    currentModel.properties.modelType.properties
                      ?.horizonType === 'Relative'
                  ) {
                    this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanUnits(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: newValue,
                      }
                    );
                  }
                } else if (fieldName === 'fixedEarliestInterceptTime') {
                  if (
                    currentModel.properties.modelType.properties
                      ?.horizonType === 'Fixed'
                  ) {
                    this.modelEditFacade.command.setForecastFixedHorizonTimeChanged(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: newValue,
                      }
                    );
                  }
                } else if (fieldName === 'fixedEarliestInterceptDate') {
                  if (
                    currentModel.properties.modelType.properties
                      ?.horizonType === 'Fixed'
                  ) {
                    this.modelEditFacade.command.setForecastFixedHorizonDateChanged(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: newValue,
                      }
                    );
                  }
                } else {
                  this.cellEditRequest(
                    rowNode.data.modelExtID,
                    fieldName,
                    params.value
                  );
                }
              }
            });
          },
        },
      ];

      if (colDef?.editable) {
        menuItems.push(...customMenuItems);
      }
    }

    return menuItems;
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade
  ) {}
}
