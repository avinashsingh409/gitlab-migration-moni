import { Component } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { SingleModelFacade } from '../../../../service/single-model.facade';

@Component({
  selector: 'atx-build-status',
  templateUrl: './build-status.component.html',
  styleUrls: ['./build-status.component.scss'],
})
export class BuildStatusComponent {
  readonly signalRStatus$ = this.singleModelFacade.isDisconnected$;
  readonly vm$ = this.modelEditFacade.query.vm$;
  constructor(
    private modelEditFacade: ModelEditFacade,
    private singleModelFacade: SingleModelFacade
  ) {}
}
