import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditingStoppedEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import {
  SensitivityAnomalyTypeLabel,
  SensitivityAnomalyTypes,
} from '../../../../service/model/sensitivity-chips';
import { defaultColumnDefs } from './training-column-defs';
import { EModelTypes, IModelConfigData } from '@atonix/shared/api';
import { NumericCellEditorComponent } from '@atonix/shared/ui';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-training-range',
  templateUrl: './model-training-range.component.html',
  styleUrls: ['./model-training-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelTrainingRangeComponent {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  readonly sensitivityAnomalyTypeLabel = SensitivityAnomalyTypeLabel;
  readonly sensitivityAnomalyTypes = Object.values(
    SensitivityAnomalyTypes
  ).filter((value) => typeof value === 'number');
  public readonly modelTypes = EModelTypes;

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  public columnDefs: (ColDef | ColGroupDef)[] = [...defaultColumnDefs];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    stopEditingWhenCellsLoseFocus: true,
    debug: false,
    singleClickEdit: true,
    readOnlyEdit: true,
    cacheBlockSize: 100,
    headerHeight: 30,
    rowHeight: 30,
    tooltipShowDelay: 0,
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      filter: false,
      sortable: false,
    },
    suppressMovableColumns: true,
    suppressAggFuncInHeader: true,
    components: {
      numericCellEditor: NumericCellEditorComponent,
    },
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditingStopped: (event: CellEditingStoppedEvent) => {
      this.applyCellValueChanges(
        event.data.modelExtID,
        event.colDef.field || '',
        event.newValue,
        event.data
      );
    },
  };

  focusOut() {
    this.gridApi.deselectAll();
  }
  getContextMenuItems(params: any): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;

    if (this.isMultiModel) {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            params?.api?.forEachNode((rowNode: RowNode, _: number) => {
              const currentModel = rowNode.data as IModelConfigData;
              const newValue = params.value;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                this.applyCellValueChanges(
                  rowNode.data.modelExtID,
                  colDef.field || '',
                  params.value,
                  rowNode.data
                );
              }
            });
          },
        },
      ];
      if (colDef?.editable) {
        menuItems.push(...customMenuItems);
      }
    }
    return menuItems;
  }

  applyCellValueChanges(
    modelExtID: string,
    columnField: string,
    newValue: any,
    data: any
  ) {
    this.modelEditFacade.command.updateModelSummary({
      modelExtID: modelExtID,
      columnField: columnField,
      newValue: newValue,
    });

    if (
      columnField === 'training.lag' ||
      columnField === 'training.lagUnitOfTime' ||
      columnField === 'training.duration' ||
      columnField === 'training.durationUnitOfTime'
    ) {
      this.modelEditFacade.command.updateProjectedTrainingRange({
        modelExtID: modelExtID,
        columnField: columnField,
        newValue: newValue,
      });
    }

    // Change null values into defaults if needed.
    if (
      columnField === 'TrainingLag' &&
      newValue &&
      data?.training?.lagUnitOfTime === 'N/A'
    ) {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: 'training.lagUnitOfTime',
        newValue: 'Seconds',
      });
    } else if (
      columnField === 'TrainingSampleRate' &&
      newValue &&
      data?.training?.sampleRateUnitOfTime === 'N/A'
    ) {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: 'training.sampleRateUnitOfTime',
        newValue: 'Seconds',
      });
    }
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade
  ) {}
}
