import { Component } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { SingleModelFacade } from '../../../../service/single-model.facade';
import {
  CellEditingStoppedEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuModule,
  RangeSelectionModule,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import { buildStatusColumnDefs } from './build-status-defs';
import { NavFacade } from '@atonix/atx-navigation';

@Component({
  selector: 'atx-build-status-multiple',
  templateUrl: './build-status-multiple.component.html',
  styleUrls: ['./build-status-multiple.component.scss'],
})
export class BuildStatusMultipleComponent {
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  readonly vm$ = this.modelEditFacade.query.vm$;
  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  public columnDefs: (ColDef | ColGroupDef)[] = [...buildStatusColumnDefs];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    stopEditingWhenCellsLoseFocus: true,
    debug: false,
    readOnlyEdit: true,
    cacheBlockSize: 100,
    headerHeight: 30,
    rowHeight: 30,
    tooltipShowDelay: 0,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      filter: false,
      sortable: false,
    },
    suppressAggFuncInHeader: true,

    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditingStopped: (event: CellEditingStoppedEvent) => {
      return;
    },
  };

  focusOut() {
    this.gridApi.deselectAll();
  }
  getContextMenuItems(params: any): string[] {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade
  ) {}
}
