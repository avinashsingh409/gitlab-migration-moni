import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
  Input,
} from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditRequestEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  Column,
  ColumnApi,
  ColumnsToolPanelModule,
  EditableCallbackParams,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GetContextMenuItemsParams,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';
import {
  modelBoundCriticalityColumnDefs,
  defaultColumnDefs,
  relativeModelBounds,
  fixedLimitsColumnDefs,
} from './anomaly-column-defs';
import {
  SensitivityAnomalyTypeLabel,
  SensitivityAnomalyTypes,
  SensitivityAnomalyTypeTooltips,
} from '../../../../service/model/sensitivity-chips';
import { SendMessageIconRendererComponent } from '../../../ag-grid-components/send-message-icon-renderer-component';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { LoggerService } from '@atonix/shared/utils';
import {
  NullableNumericCellEditorComponent,
  NullableRealNumberNumericCellEditorComponent,
  NumericCellEditorComponent,
  PositiveIntCellEditorComponent,
  RealNumberNumericCellEditorComponent,
} from '@atonix/shared/ui';
import { IModelConfigData } from '@atonix/shared/api';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-sensitivity-anomalies',
  templateUrl: './anomalies.component.html',
  styleUrls: ['./anomalies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelSensitivityAnomaliesComponent implements OnDestroy {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  public unsubscribe$ = new Subject<void>();
  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  readonly sensitivityAnomalyTypeLabel = SensitivityAnomalyTypeLabel;
  readonly sensitivityAnomalyTypeTooltips = SensitivityAnomalyTypeTooltips;
  readonly sensitivityAnomalyTypes = Object.values(
    SensitivityAnomalyTypes
  ).filter((value) => typeof value === 'number');
  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];
  public columnDefs: (ColDef | ColGroupDef)[] = [
    ...defaultColumnDefs,
    ...relativeModelBounds,
    ...fixedLimitsColumnDefs,
    ...modelBoundCriticalityColumnDefs,
  ];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    components: {
      intCellEditor: PositiveIntCellEditorComponent,
      sendMessageRenderer: SendMessageIconRendererComponent,
      numericCellEditor: NumericCellEditorComponent,
      nullableRealNumberCellEditor:
        NullableRealNumberNumericCellEditorComponent,
      nullableNumericCellEditor: NullableNumericCellEditorComponent,
      realNumberNumericCellEditor: RealNumberNumericCellEditorComponent,
    },
    rowGroupPanelShow: 'never',
    enableRangeSelection: false,
    stopEditingWhenCellsLoseFocus: true,
    animateRows: true,
    debug: false,
    singleClickEdit: true,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    rowHeight: 30,
    suppressMovableColumns: true,
    suppressRowClickSelection: true,
    suppressCopyRowsToClipboard: true,
    suppressRowDeselection: false,
    tooltipShowDelay: 0,
    readOnlyEdit: true,
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      enableRowGroup: false,
    },
    suppressAggFuncInHeader: true,
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditRequest: (event: CellEditRequestEvent) => {
      this.cellEditRequest(
        event.data.modelExtID,
        event.colDef.field || '',
        event.newValue,
        event.data
      );
    },
  };

  cellEditRequest(
    modelExtID: string,
    columnField: string,
    newValue: any,
    data: any
  ) {
    if (columnField === 'criticalityAnomaly.properties.useDefault') {
      const useDefault = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setModelBoundCriticalityUseDefaults({
        modelExtID: modelExtID,
        columnField: '',
        newValue: useDefault,
      });
    } else if (columnField === 'upperFixedLimitAnomaly.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setUpperFixedLimitAnomalyEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'lowerFixedLimitAnomaly.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setLowerFixedLimitAnomalyEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'dependent.tagUnits') {
      this.modelEditFacade.command.setROCExpressionUnits({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'criticalityAnomaly.properties.upperBound') {
      if (!data?.criticalityAnomaly?.properties?.useDefault) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'criticalityAnomaly.properties.lowerBound') {
      if (!data?.criticalityAnomaly?.properties?.useDefault) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'upperFixedLimitAnomaly.properties.upperFixedLimit'
    ) {
      if (data?.upperFixedLimitAnomaly?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'lowerFixedLimitAnomaly.properties.lowerFixedLimit'
    ) {
      if (data?.lowerFixedLimitAnomaly?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: columnField || '',
        newValue: newValue,
      });
    }
  }

  changeColumns(value: SensitivityAnomalyTypes) {
    if (value === SensitivityAnomalyTypes.All) {
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:AllAnomalies'
      );
      this.columnDefs = [
        ...defaultColumnDefs,
        ...relativeModelBounds,
        ...fixedLimitsColumnDefs,
        ...modelBoundCriticalityColumnDefs,
      ];
    } else if (value === SensitivityAnomalyTypes.FixedLimits) {
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:FixedLimitAnomalies'
      );
      this.columnDefs = [...defaultColumnDefs, ...fixedLimitsColumnDefs];
    } else if (value === SensitivityAnomalyTypes.ModelBoundCriticality) {
      this.columnDefs = [
        ...defaultColumnDefs,
        ...modelBoundCriticalityColumnDefs,
      ];
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:ModelBoundCriticalityAnomalies'
      );
    } else if (value === SensitivityAnomalyTypes.RelativeModelBounds) {
      this.logger.trackTaskCenterEvent(
        'ModelConfig:SingleModelEdit:RelativeModelBoundsAnomalies'
      );
      this.columnDefs = [...defaultColumnDefs, ...relativeModelBounds];
    }

    this.modelEditFacade.command.setAnomaly({
      type: 'anomaly',
      subType: +value,
    });
  }

  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;

    if (this.isMultiModel) {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            const fieldName = colDef.field || '';
            const newValue = params.value;
            params?.api?.forEachNode((rowNode: RowNode) => {
              const currentModel = rowNode.data as IModelConfigData;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                const enabled = newValue === 'Yes' ? true : false;
                // Use Default and enabled dropdowns will set other columns if they are changed.
                // if they are currently set to the same value as the user is applying, do not
                // change these values
                if (fieldName === 'criticalityAnomaly.properties.useDefault') {
                  if (currentModel.criticalityAnomaly.enabled !== enabled)
                    this.modelEditFacade.command.setModelBoundCriticalityUseDefaults(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: enabled,
                      }
                    );
                } else if (fieldName === 'upperFixedLimitAnomaly.enabled') {
                  if (currentModel.upperFixedLimitAnomaly.enabled !== enabled) {
                    this.modelEditFacade.command.setUpperFixedLimitAnomalyEnabled(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: enabled,
                      }
                    );
                  }
                } else if (fieldName === 'lowerFixedLimitAnomaly.enabled') {
                  if (currentModel.lowerFixedLimitAnomaly.enabled !== enabled) {
                    this.modelEditFacade.command.setLowerFixedLimitAnomalyEnabled(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: enabled,
                      }
                    );
                  }
                } else {
                  this.cellEditRequest(
                    rowNode.data.modelExtID,
                    fieldName,
                    params.value,
                    rowNode.data
                  );
                }
              }
            });
          },
        },
      ];

      if (typeof colDef?.editable === 'boolean') {
        if (colDef?.editable) {
          menuItems.push(...customMenuItems);
        }
      } else {
        const callbackParam: EditableCallbackParams = {
          api: params.api,
          colDef: colDef,
          column: params.column as Column,
          columnApi: params.columnApi,
          data: params?.node?.data,
          node: params?.node as RowNode,
          context: params.context,
        };
        if (colDef.editable?.(callbackParam)) {
          menuItems.push(...customMenuItems);
        }
      }
    }

    return menuItems;
  }

  focusOut() {
    this.gridApi.deselectAll();
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
