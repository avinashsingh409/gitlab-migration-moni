import {
  ColDef,
  ColGroupDef,
  ColumnFunctionCallbackParams,
} from '@ag-grid-enterprise/all-modules';
import { MAX_INT_VALUE } from '@atonix/atx-core';
import { EModelTypes } from '@atonix/shared/api';

export const defaultAlertsColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: '',
    children: [
      {
        colId: 'ModelName',
        headerName: 'Model Name',
        width: 420,
        cellClass: 'not-editable',
        field: 'name',
        suppressMenu: true,
        editable: false,
        sortable: true,
        filter: 'agTextColumnFilter',
        cellStyle: { 'font-style': 'italic' },
      },
      {
        colId: 'TagUnits',
        headerName: 'Units',
        suppressMenu: true,
        width: 106,
        field: 'dependent.tagUnits',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.properties.modelType.type === EModelTypes.ROC;
        },
        valueGetter: (params) => {
          return params.data.properties.modelType.type === EModelTypes.ROC
            ? params.data.dependent?.tagUnits +
                '/' +
                params.data.properties.modelType.properties.expressionPeriodUnitOfTime
                  .toLowerCase()
                  .slice(0, -1)
            : params.data.dependent?.tagUnits;
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorSelector: (params) => {
          return {
            component: 'agSelectCellEditor',
            params: {
              values: [
                params.data.dependent?.tagUnits + '/hour',
                params.data.dependent?.tagUnits + '/day',
                params.data.dependent?.tagUnits + '/week',
                params.data.dependent?.tagUnits + '/month',
                params.data.dependent?.tagUnits + '/year',
              ],
            },
          };
        },
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
        cellStyle: { 'font-style': 'italic' },
        cellClassRules: {
          'not-editable': (params) => {
            if (params.data.properties.modelType.type === EModelTypes.ROC) {
              return false;
            }
            return true;
          },
        },
      },
    ],
  },
];

export const averageAnomalyAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Average Anomaly Alert',
    children: [
      {
        colId: 'AverageAnomalyEnabled',
        headerName: 'Enable',
        width: 70,
        suppressMenu: true,
        field: 'averageAnomalyAlert.enabled',
        sortable: true,
        editable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'AverageAnomalyEvalPeriod',
        headerName: 'Evaluation Period',
        field: 'averageAnomalyAlert.properties.averageEvalPeriod',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.averageAnomalyAlert.enabled;
        },
        cellEditor: 'numericCellEditor',
        width: 145,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AverageAnomalyEvalPeriodTime',
        headerName: '',
        suppressMenu: true,
        field: 'averageAnomalyAlert.properties.evaluationPeriodUnitOfTime',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.averageAnomalyAlert.enabled;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['N/A', 'Seconds', 'Minutes', 'Hours'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },

      {
        colId: 'AverageAnomalyWindowTime',
        headerName: 'Averaging Window Time',
        field: 'averageAnomalyAlert.properties.averagingWindowUnitOfTime',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.averageAnomalyAlert.enabled;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['N/A', 'Seconds', 'Minutes', 'Hours'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },
      {
        colId: 'AverageAnomalyWindow',
        headerName: 'Averaging Window',
        field: 'averageAnomalyAlert.properties.averagingWindow',
        sortable: true,
        editable: true,
        suppressMenu: true,
        width: 165,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AverageAnomalyUseMae',
        headerName: 'Use MAE',
        field: 'averageAnomalyAlert.properties.useMeanAbsoluteError',
        sortable: true,
        width: 105,
        suppressMenu: true,
        editable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'AverageAnomalyThreshold',
        headerName: 'Threshold',
        field: 'averageAnomalyAlert.properties.threshold',
        sortable: true,
        width: 125,
        suppressMenu: true,
        editable: true,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AverageAnomalyNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '0', //Not included in UI anymore
            enableNotification:
              params.data?.averageAnomalyAlert?.notificationEnabled || false,
          };
        },
        sortable: false,
        width: 60,
        editable: false,
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const anomalyAreaAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Anomaly Area Alert',
    children: [
      {
        colId: 'AnomalyAreaEnabled',
        headerName: 'Enable',
        width: 80,
        field: 'anomalyAreaAlert.enabled',
        sortable: true,
        editable: true,
        suppressMenu: true,
        filter: 'agSetColumnFilter',
        valueGetter: (params) => {
          return params.data.anomalyAreaAlert.enabled === true ? 'Yes' : 'No';
        },
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'AnomalyAlertFastResponsePeriod',
        headerName: 'Fast Period',
        field: 'anomalyAreaAlert.properties.areaFastResponsePeriod',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
        suppressMenu: true,
        cellEditor: 'numericCellEditor',
        width: 110,
      },
      {
        colId: 'AnomalyAlertFastResponsePeriodUnitOfTime',
        headerName: '',
        field: 'anomalyAreaAlert.properties.areaFastResponsePeriodUnitOfTime',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
        suppressMenu: true,
        width: 100,
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },
      {
        colId: 'AnomalyAlertFastResponseThreshold',
        headerName: 'Fast Threshold',
        suppressMenu: true,
        field: 'anomalyAreaAlert.properties.areaFastResponse',
        sortable: true,
        width: 130,
        cellEditor: 'numericCellEditor',
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
      },
      {
        colId: 'AnomalyAlertSlowResponsePeriod',
        headerName: 'Slow Period',
        field: 'anomalyAreaAlert.properties.areaSlowResponsePeriod',
        sortable: true,
        suppressMenu: true,
        cellEditor: 'numericCellEditor',
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
        width: 110,
      },
      {
        colId: 'AnomalyAlertSlowResponsePeriodUnitOfTime',
        headerName: '',
        suppressMenu: true,
        field: 'anomalyAreaAlert.properties.areaSlowResponsePeriodUnitOfTime',
        sortable: true,
        width: 100,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },

      {
        colId: 'AnomalyAlertSlowResponseThreshold',
        headerName: 'Slow Threshold',
        field: 'anomalyAreaAlert.properties.areaSlowResponse',
        sortable: true,
        width: 130,
        suppressMenu: true,
        cellEditor: 'numericCellEditor',
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyAreaAlert.enabled;
        },
      },
      {
        colId: 'AnomalyAlertNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '3', //Area Alert - Fast Response/Slow Response
            alertStatusTypeName: 'Anomaly Area Alert',
            enableNotification:
              params.data?.anomalyAreaAlert?.notificationEnabled || false,
          };
        },
        sortable: false,
        suppressMenu: true,
        width: 60,
        editable: false,
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const anomalyFrequencyAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Anomaly Persistence Alert',
    children: [
      {
        colId: 'AnomalyFrequencyEnabled',
        headerName: 'Enable',
        field: 'anomalyFrequencyAlert.enabled',
        sortable: true,
        editable: true,
        width: 80,
        suppressMenu: true,
        filter: 'agSetColumnFilter',
        valueGetter: (params) => {
          return params.data.anomalyFrequencyAlert.enabled === true
            ? 'Yes'
            : 'No';
        },
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'AnomalyFrequencyEvalPeriod',
        headerName: 'Period',
        headerTooltip: 'The length of time to evaluate the Alert',
        field: 'anomalyFrequencyAlert.properties.evaluationPeriod',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyFrequencyAlert.enabled;
        },
        cellEditor: 'numericCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.anomalyFrequencyAlert?.enabled === false) {
              return false;
            }
            const minValue = 1;
            let maxValue = 1;
            switch (
              params.data.anomalyFrequencyAlert.properties
                .evaluationPeriodUnitOfTime
            ) {
              case 'Hours':
                maxValue = 6;
                break;
              case 'Minutes':
                maxValue = 360;
                break;
              case 'Seconds':
                maxValue = 21600;
                break;
              default:
                break;
            }
            if (
              params.data.anomalyFrequencyAlert.properties.evaluationPeriod <
                minValue ||
              params.data.anomalyFrequencyAlert.properties.evaluationPeriod >
                maxValue
            ) {
              return true;
            }

            return false;
          },
        },
        width: 90,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AnomalyFrequencyEvalPeriodTime',
        headerName: '',
        field: 'anomalyFrequencyAlert.properties.evaluationPeriodUnitOfTime',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyFrequencyAlert.enabled;
        },
        width: 90,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['N/A', 'Seconds', 'Minutes', 'Hours'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },
      {
        colId: 'AnomalyFrequencyThreshold',
        headerName: 'OOB',
        headerTooltip: 'Threshold (% OOB)',
        field: 'anomalyFrequencyAlert.properties.threshold',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyFrequencyAlert.enabled;
        },
        cellEditor: 'numericCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.anomalyFrequencyAlert?.enabled === false) {
              return false;
            }
            if (
              params.data.anomalyFrequencyAlert.properties.threshold < 1 ||
              params.data.anomalyFrequencyAlert.properties.threshold > 100
            ) {
              return true;
            }
            return false;
          },
        },
        width: 80,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AnomalyFrequencyNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        sortable: false,
        width: 60,
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '8', //Anomaly Frequency
            alertStatusTypeName: 'Anomaly Frequency Alert',
            enableNotification:
              params.data?.anomalyFrequencyAlert?.notificationEnabled || false,
          };
        },
        editable: false,
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const anomalyOscillationAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Anomaly Oscillation Alert',
    children: [
      {
        colId: 'AnomalyOscillationAlertEnabled',
        headerName: 'Enable',
        field: 'anomalyOscillationAlert.enabled',
        sortable: true,
        editable: true,
        suppressMenu: true,
        width: 80,
        filter: 'agSetColumnFilter',
        valueGetter: (params) => {
          return params.data.anomalyOscillationAlert.enabled === true
            ? 'Yes'
            : 'No';
        },
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'AnomalyOscillationAlertEvalPeriod',
        headerName: 'Period',
        headerTooltip: 'The length of time to evaluate the Alert',
        field: 'anomalyOscillationAlert.properties.evaluationPeriod',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyOscillationAlert.enabled;
        },
        cellEditor: 'numericCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.anomalyOscillationAlert?.enabled === false) {
              return false;
            }
            const minValue = 1;
            let maxValue = 1;
            switch (
              params.data.anomalyOscillationAlert.properties
                .evaluationPeriodUnitOfTime
            ) {
              case 'Hours':
                maxValue = 6;
                break;
              case 'Minutes':
                maxValue = 360;
                break;
              case 'Seconds':
                maxValue = 21600;
                break;
              default:
                break;
            }
            if (
              params.data.anomalyOscillationAlert.properties.evaluationPeriod <
                minValue ||
              params.data.anomalyOscillationAlert.properties.evaluationPeriod >
                maxValue
            ) {
              return true;
            }
            return false;
          },
        },

        filter: 'agSetColumnFilter',
      },
      {
        colId: 'AnomalyOscillationAlertEvalPeriodTime',
        headerName: '',
        field: 'anomalyOscillationAlert.properties.evaluationPeriodUnitOfTime',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyOscillationAlert.enabled;
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['N/A', 'Seconds', 'Minutes', 'Hours'],
        },
      },
      {
        colId: 'AnomalyOscillationAlertThreshold',
        headerName: 'Threshold',
        headerTooltip:
          'The threshold for the number of times the actual value crosses the anomaly boundaries.',
        field: 'anomalyOscillationAlert.properties.threshold',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.anomalyOscillationAlert.enabled;
        },
        cellEditor: 'numericCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.anomalyOscillationAlert?.enabled === false) {
              return false;
            }
            if (
              params.data.anomalyOscillationAlert.properties.threshold < 1 ||
              params.data.anomalyOscillationAlert.properties.threshold > 1000
            ) {
              return true;
            }
            return false;
          },
        },
        width: 100,
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'OscillationNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        sortable: false,
        width: 60,
        editable: false,
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '9', //Oscillations
            alertStatusTypeName: 'Anomaly Oscillation Alert',
            enableNotification:
              params.data?.anomalyOscillationAlert?.notificationEnabled ||
              false,
          };
        },
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const highhighAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'High-High Alert',
    children: [
      {
        colId: 'HighHighAlertEnabled',
        headerName: 'Enable',
        field: 'highHighAlert.enabled',
        sortable: true,
        editable: true,
        width: 80,
        suppressMenu: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        valueGetter: (params) => {
          return params.data.highHighAlert.enabled === true ? 'Yes' : 'No';
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'HighHighAlertUseMae',
        headerName: 'Use MAE',
        headerTooltip:
          'Switch to evaluate the alert boundary as the product of the model’s MAE and the Threshold. Defaults to catch extreme anomalies.',
        field: 'highHighAlert.properties.useMeanAbsoluteError',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.highHighAlert.enabled;
        },
        valueGetter: (params) => {
          return params.data.highHighAlert.properties?.useMeanAbsoluteError ===
            true
            ? 'Yes'
            : 'No';
        },
        width: 90,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'HighHighAlertThreshold',
        headerName: 'Threshold',
        width: 100,
        field: 'highHighAlert.properties.threshold',
        sortable: true,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.highHighAlert.enabled;
        },
        cellEditor: 'realNumberNumericCellEditor',
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.highHighAlert?.enabled === false) {
              return false;
            }
            if (
              params.data.highHighAlert.properties.threshold > MAX_INT_VALUE
            ) {
              return true;
            }

            return false;
          },
        },
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'HighHighNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        sortable: false,
        width: 60,
        editable: false,
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '1', //Hi-Hi Alert
            alertStatusTypeName: 'High-High Alert',
            enableNotification:
              params.data?.highHighAlert?.notificationEnabled || false,
          };
        },
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const lowLowAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Low-Low Alert',
    children: [
      {
        colId: 'LowLowAlertEnabled',
        headerName: 'Enable',
        field: 'lowLowAlert.enabled',
        sortable: true,
        editable: true,
        width: 80,
        suppressMenu: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        valueGetter: (params) => {
          return params.data.lowLowAlert.enabled === true ? 'Yes' : 'No';
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'LowLowAlertUseMae',
        headerName: 'Use MAE',
        headerTooltip:
          'Switch to evaluate the alert boundary as the product of the model’s MAE and the Threshold. Defaults to catch extreme anomalies.',
        field: 'lowLowAlert.properties.useMeanAbsoluteError',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.lowLowAlert.enabled;
        },
        valueGetter: (params) => {
          return params.data.lowLowAlert.properties?.useMeanAbsoluteError ===
            true
            ? 'Yes'
            : 'No';
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'LowLowAlertThreshold',
        headerName: 'Threshold',
        field: 'lowLowAlert.properties.threshold',
        sortable: true,
        width: 100,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.lowLowAlert.enabled;
        },
        cellEditor: 'realNumberNumericCellEditor',
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.lowLowAlert?.enabled === false) {
              return false;
            }
            if (params.data.lowLowAlert.properties.threshold > MAX_INT_VALUE) {
              return true;
            }

            return false;
          },
        },
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'LowLowNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        sortable: false,
        width: 60,
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '2', //Low-Low Alert
            alertStatusTypeName: 'Low-Low Alert',
            enableNotification:
              params.data?.lowLowAlert?.notificationEnabled || false,
          };
        },
        editable: false,
        filter: 'agSetColumnFilter',
      },
    ],
  },
];

export const frozenDataAlertColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Frozen Data Check',
    children: [
      {
        colId: 'FrozenDataEnabled',
        headerName: 'Enable',
        field: 'frozenDataCheckAlert.enabled',
        sortable: true,
        editable: true,
        suppressMenu: true,
        width: 80,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        valueGetter: (params) => {
          return params.data.frozenDataCheckAlert.enabled === true
            ? 'Yes'
            : 'No';
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'FrozenDataEvaluationPeriod',
        headerName: 'Period',
        headerTooltip: 'The length of time to evaluate the Alert',
        field: 'frozenDataCheckAlert.properties.evaluationPeriod',
        sortable: true,
        width: 80,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.frozenDataCheckAlert.enabled;
        },
        cellEditor: 'intCellEditor',
        cellClassRules: {
          'invalid-cell': (params) => {
            if (params.data.frozenDataCheckAlert?.enabled === false) {
              return false;
            }
            const minValue = 1;
            let maxValue = 1;
            switch (
              params.data.frozenDataCheckAlert.properties
                .evaluationPeriodUnitOfTime
            ) {
              case 'Hours':
                maxValue = 6;
                break;
              case 'Minutes':
                maxValue = 360;
                break;
              case 'Seconds':
                maxValue = 21600;
                break;
              default:
                break;
            }
            if (
              params.data.frozenDataCheckAlert.properties.evaluationPeriod <
                minValue ||
              params.data.frozenDataCheckAlert.properties.evaluationPeriod >
                maxValue
            ) {
              return true;
            }
            return false;
          },
        },
        filter: 'agSetColumnFilter',
      },
      {
        colId: 'FrozenDataAlertEvalPeriodTime',
        headerName: '',
        field: 'frozenDataCheckAlert.properties.evaluationPeriodUnitOfTime',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.frozenDataCheckAlert.enabled;
        },
        width: 90,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['N/A', 'Minutes', 'Hours', 'Days'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Seconds', 'Minutes', 'Hours'],
        },
      },
      {
        colId: 'FrozenDataNotifications',
        headerName: '',
        cellRenderer: 'sendMessageRenderer',
        sortable: false,
        width: 60,
        suppressMenu: true,
        cellRendererParams: (params: any) => {
          return {
            alertStatusTypeId: '7', //Frozen Data
            alertStatusTypeName: 'Frozen Data Check',
            enableNotification:
              params.data?.frozenDataCheckAlert?.notificationEnabled || false,
          };
        },
        editable: false,
        filter: 'agSetColumnFilter',
      },
    ],
  },
];
