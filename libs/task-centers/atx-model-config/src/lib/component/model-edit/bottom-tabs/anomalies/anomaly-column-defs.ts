import {
  ColDef,
  ColGroupDef,
  ColumnFunctionCallbackParams,
} from '@ag-grid-enterprise/all-modules';
import { EModelTypes } from '@atonix/shared/api';

export const defaultColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: '',
    children: [
      {
        colId: 'ModelName',
        headerName: 'Model Name',
        width: 420,
        cellClass: 'not-editable',
        field: 'name',
        editable: false,
        sortable: true,
        suppressMenu: true,
        cellStyle: { 'font-style': 'italic' },
      },
      {
        colId: 'TagUnits',
        headerName: 'Units',
        width: 106,
        field: 'dependent.tagUnits',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.properties.modelType.type === EModelTypes.ROC;
        },
        valueGetter: (params) => {
          return params.data.properties.modelType.type === EModelTypes.ROC
            ? params.data.dependent?.tagUnits +
                '/' +
                params.data.properties.modelType.properties.expressionPeriodUnitOfTime
                  .toLowerCase()
                  .slice(0, -1)
            : params.data.dependent?.tagUnits;
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorSelector: (params) => {
          return {
            component: 'agSelectCellEditor',
            params: {
              values: [
                params.data.dependent?.tagUnits + '/hour',
                params.data.dependent?.tagUnits + '/day',
                params.data.dependent?.tagUnits + '/week',
                params.data.dependent?.tagUnits + '/month',
                params.data.dependent?.tagUnits + '/year',
              ],
            },
          };
        },
        suppressMenu: true,
        cellStyle: { 'font-style': 'italic' },
        cellClassRules: {
          'not-editable': (params) => {
            if (params.data.properties.modelType.type === EModelTypes.ROC) {
              return false;
            }
            return true;
          },
        },
      },
      {
        colId: 'ModelMAE',
        headerName: 'MAE',
        width: 100,
        suppressMenu: true,
        cellClass: 'not-editable',
        editable: false,
        sortable: true,
        filter: 'agTextColumnFilter',
        cellStyle: { 'font-style': 'italic' },
        cellRenderer: (data: any) => {
          return (
            data.data?.properties?.predictiveTypes?.find(
              (pt: any) =>
                pt.type === data?.data?.properties?.predictiveTypeSelected
            )?.properties?.meanAbsoluteError || 0
          ).toPrecision(4);
        },
      },
    ],
  },
];

export const relativeModelBounds: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Relative Upper Bounds',
    children: [
      {
        colId: 'RelativeModelBoundsUpperMultiplier',
        headerName: 'Multiplier',
        field: 'relativeBoundsAnomaly.properties.upperMultiplier',
        sortable: true,
        width: 95,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          if (
            params.data.properties.modelType.type === EModelTypes.FixedLimit
          ) {
            return false;
          }

          return true;
        },
        cellEditor: 'nullableRealNumberCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
        },
        headerTooltip:
          'Scale the upper bound with a multiple of the model MAE.',
      },
      {
        colId: 'RelativeModelBoundsUpperBias',
        headerName: 'Bias',
        width: 90,
        suppressMenu: true,
        field: 'relativeBoundsAnomaly.properties.upperBias',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          if (
            params.data.properties.modelType.type === EModelTypes.FixedLimit
          ) {
            return false;
          }
          return true;
        },
        cellEditor: 'nullableRealNumberCellEditor',
        cellEditorPopup: true,
        valueFormatter: (params) => {
          if (!params.value) {
            return 0;
          }
          return params.value;
        },
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
        },
        headerTooltip: 'Positive numbers will increase the boundary distance.',
      },
      {
        colId: 'RelativeModelBoundsUpperBound',
        headerName: 'Distance',
        sortable: false,
        width: 110,
        suppressMenu: true,
        editable: false,
        cellRenderer: (params: any) => {
          const mae =
            params.data?.properties?.predictiveTypes?.find(
              (pt: any) =>
                pt.type === params?.data?.properties?.predictiveTypeSelected
            )?.properties?.meanAbsoluteError || 0;
          const upperBoundary =
            (+params?.data?.relativeBoundsAnomaly?.properties.upperMultiplier ||
              0) *
              +mae +
            (+params?.data?.relativeBoundsAnomaly?.properties?.upperBias || 0);

          if (params.data.properties.modelType.type === EModelTypes.ROC) {
            const val = upperBoundary?.toPrecision(4);
            const retVal = `${val} ${
              params.data.dependent?.tagUnits
            }/${params.data.properties?.modelType?.properties?.expressionPeriodUnitOfTime
              .toLowerCase()
              .slice(0, -1)}`;
            return retVal;
          } else {
            return upperBoundary?.toPrecision(4);
          }
        },
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
          'not-editable': (params) => {
            return true;
          },
        },
        headerTooltip: '(Model MAE * Upper Multiplier) + Upper Bias',
      },
    ],
  },
  {
    headerName: 'Relative Lower Bounds',
    children: [
      {
        colId: 'RelativeModelBoundsLowerMultiplier',
        headerName: 'Multiplier',
        width: 95,
        field: 'relativeBoundsAnomaly.properties.lowerMultiplier',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          if (
            params.data.properties.modelType.type === EModelTypes.FixedLimit
          ) {
            return false;
          }
          return true;
        },
        suppressMenu: true,
        cellEditor: 'nullableRealNumberCellEditor',
        cellEditorPopup: true,
        headerTooltip:
          'Scale the lower bound with a multiple of the model MAE.',
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
        },
      },
      {
        colId: 'RelativeModelBoundsLowerBias',
        headerName: 'Bias',
        field: 'relativeBoundsAnomaly.properties.lowerBias',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          if (
            params.data.properties.modelType.type === EModelTypes.FixedLimit
          ) {
            return false;
          }
          return true;
        },
        valueFormatter: (params) => {
          if (!params.value) {
            return 0;
          }
          return params.value;
        },
        cellEditor: 'nullableRealNumberCellEditor',
        cellEditorPopup: true,
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
        },
        headerTooltip: 'Negative numbers will increase the boundary distance.',
      },
      {
        colId: 'RelativeModelBoundsLowerBound',
        headerName: 'Distance',
        sortable: false,
        width: 110,
        suppressMenu: true,
        editable: false,
        cellClassRules: {
          'disabled-cell': (params) => {
            if (
              params.data.properties.modelType.type === EModelTypes.FixedLimit
            ) {
              return true;
            }
            return false;
          },
          'not-editable': (params) => {
            return true;
          },
        },
        cellRenderer: (params: any) => {
          const mae =
            params.data?.properties?.predictiveTypes?.find(
              (pt: any) =>
                pt.type === params?.data?.properties?.predictiveTypeSelected
            )?.properties?.meanAbsoluteError || 0;

          const lowerBoundary =
            (+params?.data?.relativeBoundsAnomaly?.properties
              ?.lowerMultiplier || 0) *
              +mae -
            (+params?.data?.relativeBoundsAnomaly?.properties?.lowerBias || 0);

          if (params.data.properties.modelType.type === EModelTypes.ROC) {
            const val = lowerBoundary?.toPrecision(4);
            const retVal = `${val} ${
              params.data.dependent?.tagUnits
            }/${params.data.properties?.modelType?.properties?.expressionPeriodUnitOfTime
              .toLowerCase()
              .slice(0, -1)}`;
            return retVal;
          } else {
            return lowerBoundary?.toPrecision(4);
          }
        },
        headerTooltip: '(Model MAE * Lower Multiplier) - Lower Bias',
      },
    ],
  },
];

export const modelBoundCriticalityColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Model Bound Criticality',
    children: [
      {
        colId: 'UseDefault',
        headerName: 'Default',
        field: 'criticalityAnomaly.properties.useDefault',
        sortable: true,
        editable: true,
        valueGetter: (params) => {
          return params.data.criticalityAnomaly.properties.useDefault === true
            ? 'Yes'
            : 'No';
        },
        width: 90,
        suppressMenu: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'UpperBound',
        headerName: 'Upper',
        width: 90,
        suppressMenu: true,
        field: 'criticalityAnomaly.properties.upperBound',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return !params.data.criticalityAnomaly.properties.useDefault;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Low', 'Medium', 'High'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Asset', 'Low', 'Medium', 'High'],
        },
      },
      {
        colId: 'LowerBound',
        headerName: 'Lower',
        field: 'criticalityAnomaly.properties.lowerBound',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return !params.data.criticalityAnomaly.properties.useDefault;
        },
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Low', 'Medium', 'High'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Asset', 'Low', 'Medium', 'High'],
        },
      },
    ],
  },
];

export const fixedLimitsColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    headerName: 'Fixed Upper Limit',
    children: [
      {
        colId: 'EnableUpperFixedLimit',
        headerName: 'Enable',
        field: 'upperFixedLimitAnomaly.enabled',
        sortable: true,
        editable: true,
        width: 90,
        suppressMenu: true,
        valueGetter: (params) => {
          return params.data.upperFixedLimitAnomaly.enabled === true
            ? 'Yes'
            : 'No';
        },
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'UpperFixedLimit',
        headerName: 'Limit',
        width: 85,
        suppressMenu: true,
        field: 'upperFixedLimitAnomaly.properties.upperFixedLimit',
        sortable: true,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.upperFixedLimitAnomaly.enabled;
        },
        cellEditor: 'realNumberNumericCellEditor',
        cellEditorPopup: true,
      },
    ],
  },

  {
    headerName: 'Fixed Lower Limit',
    children: [
      {
        colId: 'EnableLowerFixed',
        headerName: 'Enable',
        field: 'lowerFixedLimitAnomaly.enabled',
        sortable: true,
        width: 90,
        suppressMenu: true,
        editable: true,
        valueGetter: (params) => {
          return params.data.lowerFixedLimitAnomaly.enabled === true
            ? 'Yes'
            : 'No';
        },
        filterParams: {
          values: ['Yes', 'No'],
        },
        cellEditor: 'agSelectCellEditor',
        cellEditorParams: {
          values: ['Yes', 'No'],
        },
      },
      {
        colId: 'LowerFixedLimit',
        headerName: 'Limit',
        field: 'lowerFixedLimitAnomaly.properties.lowerFixedLimit',
        sortable: true,
        suppressMenu: true,
        width: 85,
        editable: (params: ColumnFunctionCallbackParams) => {
          return params.data.lowerFixedLimitAnomaly.enabled;
        },
        cellEditor: 'realNumberNumericCellEditor',
        cellEditorPopup: true,
      },
    ],
  },
];
