import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IAssetVariableTypeTagMap } from '@atonix/atx-core';
import { EModelTypes, IModelIndependent } from '@atonix/shared/api';
import { LoggerService } from '@atonix/shared/utils';
import { ModelEditFacade } from '../../../../../service/model-edit.facade';

@Component({
  selector: 'atx-inputs-model-config',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputsComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  public readonly modelTypes = EModelTypes;

  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService
  ) {}

  addModelInputTag(tag: IAssetVariableTypeTagMap) {
    if (tag) {
      this.logger.trackTaskCenterEvent('ModelConfig:SingleModelEdit:AddTag');
      const modelInputs: IModelIndependent = {
        tagGuid: tag?.Tag?.GlobalID,
        tagName: tag?.Tag?.TagName,
        tagDesc: tag?.Tag?.TagDesc,
        forcedInclusion: false,
        assetVariableTagMapID: tag?.AssetVariableTypeTagMapID,
        inUse: false,
      } as IModelIndependent;
      this.modelEditFacade.command.addModelInputTags([modelInputs]);
    }
  }

  onDragOver(event: any) {
    const types = event.dataTransfer.types;
    const dragSupported = types.length;
    if (dragSupported) {
      event.dataTransfer.dropEffect = 'move';
    }
    event.preventDefault();
  }

  onDrop(event: any) {
    event.preventDefault();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;
    const transferedData = event.dataTransfer.getData(
      isIE ? 'text' : 'text/plain'
    );
    const assetVariableTypeTagMaps: IAssetVariableTypeTagMap[] =
      JSON.parse(transferedData);

    assetVariableTypeTagMaps.map((tag) => this.addModelInputTag(tag));
  }
}
