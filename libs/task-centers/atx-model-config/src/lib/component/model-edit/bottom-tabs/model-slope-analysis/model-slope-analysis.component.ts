import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditRequestEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GetContextMenuItemsParams,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import {
  SensitivityAnomalyTypeLabel,
  SensitivityAnomalyTypes,
} from '../../../../service/model/sensitivity-chips';
import { EModelTypes, IModelConfigData } from '@atonix/shared/api';
import { initialColumnDefs } from './model-slope-analysis-column-defs';
import {
  NullableIntCellEditorComponent,
  NullableNumericCellEditorComponent,
  NumericCellEditorComponent,
  RealNumberNumericCellEditorComponent,
} from '@atonix/shared/ui';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-slope-analysis',
  templateUrl: './model-slope-analysis.component.html',
  styleUrls: ['./model-slope-analysis.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelSlopeAnalysisComponent {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  readonly sensitivityAnomalyTypeLabel = SensitivityAnomalyTypeLabel;
  readonly sensitivityAnomalyTypes = Object.values(
    SensitivityAnomalyTypes
  ).filter((value) => typeof value === 'number');
  public readonly modelTypes = EModelTypes;

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  public columnDefs: (ColDef | ColGroupDef)[] = [...initialColumnDefs];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    components: {
      numericCellEditor: NumericCellEditorComponent,
      nullableIntCellEditor: NullableIntCellEditorComponent,
      nullableNumericCellEditor: NullableNumericCellEditorComponent,
      realNumberNumericCellEditor: RealNumberNumericCellEditorComponent,
    },
    stopEditingWhenCellsLoseFocus: true,
    debug: false,
    singleClickEdit: true,
    readOnlyEdit: true,
    cacheBlockSize: 100,
    headerHeight: 30,
    rowHeight: 30,
    tooltipShowDelay: 0,
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      filter: false,
      sortable: false,
    },
    suppressAggFuncInHeader: true,
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditRequest: (event: CellEditRequestEvent) => {
      this.cellEditRequest(
        event.data.modelExtID,
        event.colDef.field || '',
        event.newValue
      );
    },
  };

  cellEditRequest(modelExtID: string, columnField: string, newValue: any) {
    if (columnField === 'properties.modelType.properties.horizonType') {
      this.modelEditFacade.command.setForecastHorizonType({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (
      columnField === 'properties.modelType.properties.horizonPeriod'
    ) {
      this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanDuration(
        {
          modelExtID: modelExtID,
          columnField: '',
          newValue: newValue,
        }
      );
    } else if (
      columnField === 'properties.modelType.properties.horizonPeriodUnitOfTime'
    ) {
      this.modelEditFacade.command.setForecastRelativeInterceptTimeSpanUnits({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'fixedEarliestInterceptTime') {
      this.modelEditFacade.command.setForecastFixedHorizonTimeChanged({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'fixedEarliestInterceptDate') {
      this.modelEditFacade.command.setForecastFixedHorizonDateChanged({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'dependent.tagUnits') {
      this.modelEditFacade.command.setROCExpressionUnits({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: columnField || '',
        newValue: newValue,
      });
    }
  }

  focusOut() {
    this.gridApi.deselectAll();
  }
  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;
    if (this.isMultiModel && colDef?.field !== 'dependent.tagUnits') {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            params?.api?.forEachNode((rowNode: RowNode, _: number) => {
              const currentModel = rowNode.data as IModelConfigData;
              const newValue = params.value;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                this.cellEditRequest(
                  rowNode.data.modelExtID,
                  colDef.field || '',
                  newValue
                );
              }
            });
          },
        },
      ];

      if (colDef?.editable) {
        menuItems.push(...customMenuItems);
      }
    }

    return menuItems;
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade
  ) {}
}
