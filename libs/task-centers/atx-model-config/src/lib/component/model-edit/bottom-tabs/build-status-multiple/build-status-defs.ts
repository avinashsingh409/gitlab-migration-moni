import {
  ColDef,
  ColGroupDef,
  ITooltipParams,
} from '@ag-grid-enterprise/all-modules';
import { isNilOrEmptyString } from '@atonix/atx-core';

export const buildStatusColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'name',
    width: 400,
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'LastBuildStatus',
    headerName: 'Last Build Status',
    width: 400,
    editable: false,
    field: 'buildProperties.status.buildState',
    tooltipValueGetter: (params: ITooltipParams) =>
      params.data.buildProperties?.status?.buildState,
  },
  {
    colId: 'LiveBuildStatus',
    headerName: 'Live Build Status',
    editable: false,
    flex: 1,
    field: 'liveStatus',
    tooltipValueGetter: (params: ITooltipParams) => params.data.liveStatus,
  },
];
