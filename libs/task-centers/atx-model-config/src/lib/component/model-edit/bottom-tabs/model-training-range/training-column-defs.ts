import { ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';
import { isNilOrEmptyString } from '@atonix/atx-core';

export const defaultColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'name',
    width: 400,
    suppressMenu: true,
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'TrainingSavedRange',
    headerName: 'Saved Training Range',
    field: 'savedTrainingRange',
    width: 200,
    suppressMenu: true,
    headerTooltip:
      'Data from this time period was used to train the model the last time it was trained.',
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'TrainingDuration',
    headerName: 'Duration',
    field: 'training.duration',
    suppressMenu: true,
    sortable: true,
    width: 100,
    headerTooltip:
      'Used to define the start of the training window relative to the date the model is trained. Training start date = training date - lag - duration',
    editable: true,
    cellEditor: 'numericCellEditor',
    cellEditorPopup: true,
    cellClass: (params) => {
      if (isNilOrEmptyString(params.data.training.duration)) {
        return 'invalid-cell';
      }
      return '';
    },
  },
  {
    colId: 'TrainingDurationTemporalType',
    headerName: '',
    width: 90,
    sortable: true,
    field: 'training.durationUnitOfTime',
    editable: true,
    suppressMenu: true,
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Seconds', 'Minutes', 'Days', 'Weeks', 'Months', 'Years'],
    },
  },
  {
    colId: 'TrainingLag',
    headerName: 'Lag',
    field: 'training.lag',
    width: 90,
    sortable: true,
    suppressMenu: true,
    headerTooltip:
      'Used to define the end of the training window relative to the date the model is trained. Training end date = training date - lag',
    editable: true,
    cellEditor: 'numericCellEditor',
    cellEditorPopup: true,
  },
  {
    colId: 'TrainingLagTemporalType',
    headerName: '',
    width: 90,
    field: 'training.lagUnitOfTime',
    suppressMenu: true,
    editable: true,
    sortable: true,
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Seconds', 'Minutes', 'Days', 'Weeks', 'Months', 'Years'],
    },
  },
  {
    colId: 'TrainingStartDate',
    headerName: 'Proposed Training Range',
    width: 200,
    field: 'projectedTrainingRange',
    suppressMenu: true,
    headerTooltip:
      'The proposed training window with the current configuration.',
    cellClass: 'not-editable',
  },
  {
    colId: 'TrainingSampleRate',
    headerName: 'Sample Rate',
    width: 120,
    sortable: true,
    field: 'training.sampleRate',
    suppressMenu: true,
    headerTooltip:
      'Used to define how often is data sampled in the training window to train the model.',
    editable: true,
    cellEditor: 'numericCellEditor',
    cellEditorPopup: true,
    cellClass: (params) => {
      if (
        isNilOrEmptyString(params.data.training.sampleRate) ||
        params.data.training.sampleRate == 0
      ) {
        return 'invalid-cell';
      }
      return '';
    },
  },
  {
    colId: 'TrainingSampleRateTemporalType',
    headerName: '',
    field: 'training.sampleRateUnitOfTime',
    editable: true,
    sortable: true,
    width: 90,
    suppressMenu: true,
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Seconds', 'Minutes', 'Hours', 'Days'],
    },
  },
  {
    colId: 'TrainingMinDataPoints',
    headerName: 'Min Data Points',
    field: 'training.minDataPoints',
    width: 140,
    sortable: true,
    suppressMenu: true,
    headerTooltip: 'Used to define how much data is required to train a model.',
    editable: true,
    cellEditor: 'numericCellEditor',
    cellEditorPopup: true,
    cellClass: (params) => {
      if (
        isNilOrEmptyString(params.data.training.minDataPoints) ||
        params.data.training.minDataPoints == 0
      ) {
        return 'invalid-cell';
      }
      return '';
    },
  },
];
