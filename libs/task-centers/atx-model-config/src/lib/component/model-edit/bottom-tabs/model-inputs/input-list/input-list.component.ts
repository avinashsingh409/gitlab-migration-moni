import { Component, ChangeDetectionStrategy } from '@angular/core';
import { LoggerService } from '@atonix/shared/utils';
import { ModelEditFacade } from '../../../../../service/model-edit.facade';

@Component({
  selector: 'atx-input-list',
  templateUrl: './input-list.component.html',
  styleUrls: ['./input-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputListComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService
  ) {}

  forcedInclusionToggle(i: number) {
    this.logger.trackTaskCenterEvent(
      'ModelConfig:SingleModelEdit:ForcedInclusion'
    );
    this.modelEditFacade.command.forcedInclusionToggle(i);
  }

  remove(i: number) {
    this.logger.trackTaskCenterEvent('ModelConfig:SingleModelEdit:RemoveTag');
    this.modelEditFacade.command.removeModelInput(i);
  }
}
