import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditingStoppedEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GetContextMenuItemsParams,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import {
  SensitivityAnomalyTypeLabel,
  SensitivityAnomalyTypes,
} from '../../../../service/model/sensitivity-chips';

import { EModelTypes, IModelConfigData } from '@atonix/shared/api';
import { defaultColumnDefs } from './model-averages-column-defs';
import { PositiveIntCellEditorComponent } from '@atonix/shared/ui';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-averages',
  templateUrl: './model-averages.component.html',
  styleUrls: ['./model-averages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelAveragesComponent {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  readonly sensitivityAnomalyTypeLabel = SensitivityAnomalyTypeLabel;
  readonly sensitivityAnomalyTypes = Object.values(
    SensitivityAnomalyTypes
  ).filter((value) => typeof value === 'number');
  public readonly modelTypes = EModelTypes;

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  public columnDefs: (ColDef | ColGroupDef)[] = [...defaultColumnDefs];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    components: {
      intCellEditor: PositiveIntCellEditorComponent,
    },
    stopEditingWhenCellsLoseFocus: true,
    debug: false,
    readOnlyEdit: true,
    cacheBlockSize: 100,
    headerHeight: 30,
    rowHeight: 30,
    tooltipShowDelay: 0,
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      filter: false,
      sortable: false,
    },
    suppressAggFuncInHeader: true,
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditingStopped: (event: CellEditingStoppedEvent) => {
      console.log('cell editing stopped');
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: event.data.modelExtID,
        columnField: event.colDef.field || '',
        newValue: event.newValue,
      });
    },
  };

  focusOut() {
    this.gridApi.deselectAll();
  }
  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;
    if (this.isMultiModel) {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            const newValue = params.value;
            params?.api?.forEachNode((rowNode: RowNode, _: number) => {
              const currentModel = rowNode.data as IModelConfigData;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                this.modelEditFacade.command.updateModelSummary({
                  modelExtID: rowNode.data.modelExtID,
                  columnField: colDef.field || '',
                  newValue,
                });
              }
            });
          },
        },
      ];

      if (colDef?.editable) {
        menuItems.push(...customMenuItems);
      }
    }

    return menuItems;
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade
  ) {}
}
