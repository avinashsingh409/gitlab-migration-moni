import {
  ClipboardModule,
  ColDef,
  ColumnApi,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  ICellRendererParams,
  MenuModule,
  ModelUpdatedEvent,
  Module,
  RowDoubleClickedEvent,
  ServerSideRowModelModule,
  ServerSideStoreType,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import * as modelEditActions from '../../../../store/actions/model-edit.actions';
import {
  clickIcon,
  ITreeNode,
  ITreeStateChange,
  ModelService,
} from '@atonix/atx-asset-tree';
import { NavFacade } from '@atonix/atx-navigation';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  take,
  takeUntil,
  withLatestFrom,
} from 'rxjs/operators';
import { ModelEditTagListRetrieverService } from '../../../../service/model-edit-tag-list-retriever.service';
import { ModelEditTagListFacade } from '../../../../service/model-edit-tag-list.facade';
import { Clipboard } from '@angular/cdk/clipboard';
import {
  ModelConfigBusEventTypes,
  ModelConfigEmitEvent,
  ModelConfigEventBus,
} from '../../../../store/services/model-config-event-bus';
import { GridIconRendererComponent } from '../../../ag-grid-components/grid-renderer/grid-icon-renderer.component';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import {
  IAssetVariableTypeTagMap,
  isNil,
  TagMapRecommendedType,
} from '@atonix/atx-core';
import { DragCellRendererComponent, LoggerService } from '@atonix/shared/utils';
import { MatDrawer } from '@angular/material/sidenav';
import { IModelIndependent } from '@atonix/shared/api';
import { isEqual } from 'lodash';
import { Store } from '@ngrx/store';

export interface TagListState {
  modelIndependents: IModelIndependent[] | null;
  selectedRows: any;
}

const _initialState: TagListState = {
  modelIndependents: null,
  selectedRows: null,
};

let _state: TagListState = _initialState;

@Component({
  selector: 'atx-model-edit-tag-list',
  templateUrl: './model-edit-tag-list.component.html',
  styleUrls: ['./model-edit-tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelEditTagListComponent implements OnDestroy {
  @Output() public selectedTag = new EventEmitter<IAssetVariableTypeTagMap>();
  @ViewChild('drawer') drawer!: MatDrawer;

  private modelsLoaded = new Subject<void>();
  private tagListStore = new BehaviorSubject<TagListState>(_state);
  private tagListState = this.tagListStore.asObservable();

  modelIndependents$ = this.tagListState.pipe(
    map((state) => state.modelIndependents),
    distinctUntilChanged()
  );

  selectedRows$ = this.tagListState.pipe(
    map((state) => state.selectedRows),
    distinctUntilChanged()
  );

  public unsubscribe$ = new Subject<void>();
  asset!: string | null;
  searchAsset!: number | null;
  showAssetTreeDropdown = false;
  tagsSelectedAssetOnly = false;
  isMultiModel = false;
  stickyRows: number[] = [];
  processDataServerGuid: string | undefined;

  gridApi!: GridApi;
  columnApi!: ColumnApi;
  private storeType: ServerSideStoreType = 'partial';
  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    ServerSideRowModelModule,
    SetFilterModule,
  ];

  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading Tags</div>',
    overlayNoRowsTemplate: '<div></div>',
    rowGroupPanelShow: 'never',
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    headerHeight: 30,
    rowHeight: 30,
    floatingFiltersHeight: 30,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      sortable: true,
      editable: false,
    },
    components: {
      iconCellRenderer: GridIconRendererComponent,
      dragCellRenderer: DragCellRendererComponent,
    },
    columnDefs: [
      {
        colId: 'Recommended',
        maxWidth: 65,
        headerComponentParams: {
          template: `
              <div class="ag-cell-label-container icon-only" role="presentation">
                <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>
                <div ref="eLabel" class="ag-header-cell-label" role="presentation">
                  <img src="./assets/recommended-selected.svg" style="height: 18px; width: 16px;" />
                  <span ref="eText" style="visibility:hidden" class="ag-header-cell-text" role="columnheader"></span>
                  <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>
                  <span ref="eSortOrder" class="ag-header-icon ag-sort-order" ></span>
                  <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon" ></span>
                  <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon" ></span>
                  <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon" ></span>
                </div>
              </div>
              `,
        },
        headerName: 'Recommended',
        headerTooltip: 'Recommended',
        field: 'Recommended',
        cellRenderer: 'iconCellRenderer',
        cellRendererParams: {
          type: 'recommended',
          class: 'grid-icon-small',
        },
        sortable: false,
        tooltipValueGetter: (params) => {
          switch (params?.data?.Recommended) {
            case TagMapRecommendedType.RecommendedSelected:
              return 'Recommended, Selected';
            case TagMapRecommendedType.NotRecommendedSelected:
              return 'Selected';
            case TagMapRecommendedType.RecommendedNotSelected:
              return 'Recommended, Not Selected';
            default:
              return 'Not Selected';
          }
        },
      },
      {
        colId: 'AssetID',
        headerName: 'AssetID',
        field: 'AssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SearchAssetID',
        headerName: 'SearchAssetID',
        field: 'SearchAssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'SelectedAssetOnly',
        headerName: 'SelectedAssetOnly',
        field: 'SelectedAssetOnly',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'StickyRows',
        headerName: 'StickyRows',
        field: 'StickyRows',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'ProcessDataServerGuid',
        headerName: 'ProcessDataServerGuid',
        field: 'ProcessDataServerGuid',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'Asset.AssetAbbrev',
        filter: 'agTextColumnFilter',
        width: 180,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params.data?.Asset?.AssetAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          };
        },
      },
      {
        colId: 'Variable',
        headerName: 'Variable',
        field: 'VariableType.VariableAbbrev',
        filter: 'agTextColumnFilter',
        width: 175,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params.data?.VariableType?.VariableAbbrev;
          return {
            component: 'dragCellRenderer',
            params: {
              value: value ?? '',
              type: 'text',
            },
          };
        },
      },
      {
        colId: 'Name',
        headerName: 'Name',
        field: 'Tag.TagName',
        filter: 'agTextColumnFilter',
        width: 325,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params.data?.Tag?.TagName;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          };
        },
      },
      {
        colId: 'Description',
        headerName: 'Description',
        field: 'Tag.TagDesc',
        filter: 'agTextColumnFilter',
        width: 680,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params.data?.Tag?.TagDesc;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          };
        },
      },
      {
        colId: 'Units',
        headerName: 'Units',
        field: 'Tag.EngUnits',
        filter: 'agTextColumnFilter',
        width: 90,
        cellRendererSelector: (params: ICellRendererParams) => {
          const value = params.data?.Tag?.EngUnits;
          return {
            component: 'dragCellRenderer',
            params: {
              value,
              type: 'text',
            },
          };
        },
      },
    ],
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      if (this.isMultiModel) {
        const columnDefs = this.gridApi
          .getColumnDefs()
          ?.filter((c) => (c as ColDef).colId !== 'Recommended') as ColDef[];
        this.gridApi.setColumnDefs(columnDefs);
      }
      this.gridApi.getFilterInstance('ProcessDataServerGuid')?.setModel({
        type: 'contains',
        filter: this.processDataServerGuid,
      });
      this.updateAssetAndSearchAsset(
        this.asset,
        this.searchAsset,
        this.tagsSelectedAssetOnly,
        this.stickyRows
      );
      this.gridApi.setServerSideDatasource(this.tagListRetriever);
    },
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => {
      this.selectedTag.emit(event.data);
    },
    getRowId: (params) => {
      if (params.data) {
        return String(params.data.AssetVariableTypeTagMapID);
      }
      return '';
    },
    sendToClipboard: (params) => {
      this.processDataToClipboard(params.data);
    },
  };

  constructor(
    private tagListRetriever: ModelEditTagListRetrieverService,
    public tagListFacade: ModelEditTagListFacade,
    private modelConfigEventBus: ModelConfigEventBus,
    private modelEditFacade: ModelEditFacade,
    public navFacade: NavFacade,
    private clipboard: Clipboard,
    private logger: LoggerService,
    private readonly store: Store,
    private modelService: ModelService
  ) {
    this.tagListFacade.assetID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        this.asset = id;
      });

    this.modelService.node$
      .pipe(takeUntil(this.modelsLoaded))
      .subscribe((node) => {
        if (!isNil(node) && node) {
          this.store.dispatch(
            modelEditActions.tagListAssetStateChange({
              treeStateChange: {
                event: 'IconClicked',
                newValue: node,
              },
            })
          );
          this.modelsLoaded.next();
          this.modelsLoaded.complete();
        }
      });
    this.tagListFacade.searchAssetID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        this.searchAsset = id;
        this.updateAssetAndSearchAsset(
          this.asset,
          this.searchAsset,
          this.tagsSelectedAssetOnly,
          this.stickyRows
        );
      });

    this.modelEditFacade.query.selectedModelSummary$
      .pipe(withLatestFrom(this.modelEditFacade.query.multiModelIds$), take(1))
      .subscribe(([summary, multiModelIds]) => {
        this.isMultiModel = multiModelIds && multiModelIds.length > 1;
        if (!this.isMultiModel) {
          this.tagListFacade.updateRecommendedInputs(
            summary?.dependent.assetVariableTagMapID as number,
            summary?.properties.modelType.type as string
          );
        }
        this.processDataServerGuid = summary?.dependent.processDataServerGuid;
      });

    this.modelEditFacade.query.selectedModelSummary$
      .pipe(
        withLatestFrom(this.tagListFacade.recommendedInputs$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([summary, recommended]) => {
        if (!this.isMultiModel) {
          this.checkState(summary?.independents || [], recommended);
        }
      });

    this.tagListFacade.recommendedInputs$
      .pipe(
        withLatestFrom(this.tagListFacade.selectedInputs$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([recommended, selected]) => {
        this.stickyRows = selected.concat(
          recommended.filter((r) => !selected.includes(r))
        ) as number[];

        this.updateAssetAndSearchAsset(
          this.asset,
          this.searchAsset,
          this.tagsSelectedAssetOnly,
          this.stickyRows
        );
      });
  }

  checkState(independents: IModelIndependent[], recommended: number[]) {
    this.modelIndependents$.pipe(take(1)).subscribe((localIndependents) => {
      if (isEqual(localIndependents, independents)) {
        return;
      }
      this.updateState({ ..._state, modelIndependents: independents });

      const selected = independents.map((t) => {
        return t.assetVariableTagMapID;
      }) as number[];

      const previouslySelected = localIndependents?.map((t) => {
        return t.assetVariableTagMapID;
      }) as number[];

      // We have to update the list row by row
      // instead of asking the server for changes.
      // https://www.ag-grid.com/angular-data-grid/server-side-model-updating/#single-row-updates
      if (this.gridApi) {
        this.gridApi.forEachNode((rowNode) => {
          if (selected.indexOf(rowNode.data.AssetVariableTypeTagMapID) >= 0) {
            const updated = rowNode.data;
            const updatedRecommended =
              updated.Recommended as TagMapRecommendedType;

            const recommended =
              updatedRecommended ===
                TagMapRecommendedType.RecommendedNotSelected ||
              updatedRecommended === TagMapRecommendedType.RecommendedSelected
                ? true
                : false;

            updated.Recommended =
              this.tagListRetriever.createRecommendedSelectedIcon(
                recommended,
                true
              );
            // directly update data in rowNode
            rowNode.setData(updated);
          } else if (
            previouslySelected.indexOf(
              rowNode.data.AssetVariableTypeTagMapID
            ) >= 0
          ) {
            if (selected.indexOf(rowNode.data.AssetVariableTypeTagMapID) < 0) {
              // was selected, but not selected now
              const updated = rowNode.data;

              const updatedRecommended =
                updated.Recommended as TagMapRecommendedType;

              const recommended =
                updatedRecommended ===
                  TagMapRecommendedType.RecommendedNotSelected ||
                updatedRecommended === TagMapRecommendedType.RecommendedSelected
                  ? true
                  : false;

              rowNode.data.Recommended =
                this.tagListRetriever.createRecommendedSelectedIcon(
                  recommended,
                  false
                );
              rowNode.setData(updated);
            }
          }
        });
      }
      this.tagListFacade.updateSelectedInputs(selected);
      this.stickyRows = selected.concat(
        recommended.filter((r) => !selected.includes(r))
      ) as number[];
      if (!isNil(selected) && isNil(localIndependents)) {
        this.updateAssetAndSearchAsset(
          this.asset,
          this.searchAsset,
          this.tagsSelectedAssetOnly,
          this.stickyRows
        );
      }
    });
  }

  getContextMenuItems(params: any) {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }

  updateAssetAndSearchAsset(
    asset: string | null,
    searchAsset: number | null,
    selectedAssetOnly: boolean,
    stickyRows: number[]
  ) {
    if (this.gridOptions && this.gridOptions.api && asset && searchAsset) {
      const assetIdFilter = this.gridOptions.api.getFilterInstance('AssetID');
      const searchAssetIdFilter =
        this.gridOptions.api.getFilterInstance('SearchAssetID');
      const selectedAssetOnlyFilter =
        this.gridOptions.api.getFilterInstance('SelectedAssetOnly');
      const stickyRowsFilter =
        this.gridOptions.api.getFilterInstance('StickyRows');
      if (
        asset !== assetIdFilter?.getModel()?.filter ||
        searchAsset !== searchAssetIdFilter?.getModel()?.filter ||
        selectedAssetOnly.toString() !==
          selectedAssetOnlyFilter?.getModel()?.filter
      ) {
        assetIdFilter?.setModel({ type: 'asset', filter: asset });
        searchAssetIdFilter?.setModel({
          type: 'searchAsset',
          filter: searchAsset.toString(),
        });
        selectedAssetOnlyFilter?.setModel({
          type: 'selectedAssetOnly',
          filter: selectedAssetOnly ? 'true' : 'false',
        });
        stickyRowsFilter?.setModel({
          type: 'stickyRows',
          filter:
            stickyRows && stickyRows.length > 0 ? stickyRows.join(',') : '',
        });

        this.gridOptions.api.onFilterChanged();
      }
    }
  }

  drawerChanged() {
    this.logger.trackTaskCenterEvent(
      'ModelConfig:SingleModelEdit:EditAssetButton'
    );
    this.drawer.toggle();
  }

  editAsset(): void {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  closeAssetTreeDropdown() {
    this.showAssetTreeDropdown = false;
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.modelConfigEventBus.emit(
      new ModelConfigEmitEvent(
        ModelConfigBusEventTypes.MODEL_EDIT_ASSET_TREE_DROPDOWN_STATE_CHANGE,
        {
          treeStateChange: change,
        }
      )
    );
  }

  // The built-in 'copy to clipboard' of the ag-grid doesn't respect the
  // selected rows' display orders but rather the selected rows' ids.
  // This method will process the selected rows' data before sending it to the clipboard
  // so that selected rows' display orders will be retained upon copying.
  processDataToClipboard(data: string) {
    let text = data;
    const selectedRows = this.gridApi.getSelectedNodes();
    if (selectedRows?.length > 0) {
      text = '';
      if (data.match(/Asset\tVariable\tName\tDescription\tUnits/gm)) {
        text += 'Asset\tVariable\tName\tDescription\tUnits\n';
      }

      selectedRows.sort((a, b) => {
        if (a.rowIndex && b.rowIndex) {
          return a.rowIndex - b.rowIndex;
        } else {
          return -1;
        }
      });

      selectedRows.map((row) => {
        const assetAbbrev = row.data.Asset?.AssetAbbrev || '';
        const variableAbbrev = row.data.VariableType?.VariableAbbrev || '';
        const tagName = row.data.Tag?.TagName || '';
        const tagDesc = row.data.Tag?.TagDesc || '';
        const engUnits = row.data.Tag?.EngUnits || '';
        text +=
          assetAbbrev +
          '\t' +
          variableAbbrev +
          '\t' +
          tagName +
          '\t' +
          tagDesc +
          '\t' +
          engUnits +
          '\n';
      });
    }

    this.clipboard.copy(text);
  }

  tagsPreferenceChange() {
    this.tagsSelectedAssetOnly = !this.tagsSelectedAssetOnly;
    this.logger.trackTaskCenterEvent(
      'ModelConfig:SingleModelEdit:IncludeTagsForThisAssetOnly'
    );
    this.updateAssetAndSearchAsset(
      this.asset,
      this.searchAsset,
      this.tagsSelectedAssetOnly,
      this.stickyRows
    );
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: TagListState) {
    this.tagListStore.next((_state = newState));
  }

  ngOnDestroy(): void {
    this.reset();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
