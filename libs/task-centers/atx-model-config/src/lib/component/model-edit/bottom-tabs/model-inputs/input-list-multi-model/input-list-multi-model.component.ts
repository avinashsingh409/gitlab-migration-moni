import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LoggerService } from '@atonix/shared/utils';
import { Subject, takeUntil } from 'rxjs';
import { IForceIncludeMultiModelParams } from '../../../../../service/model-edit.actions';
import { ModelEditFacade } from '../../../../../service/model-edit.facade';
import { IModelInput } from '../../../../../service/model-edit.query';

@Component({
  selector: 'atx-input-list-multi-model',
  templateUrl: './input-list-multi-model.component.html',
  styleUrls: ['./input-list-multi-model.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputListMultiModelComponent {
  public modelInputs: IModelInput[] = [];
  readonly vm$ = this.modelEditFacade.query.vm$;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService
  ) {
    this.modelEditFacade.query.selectedModelsInputs$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((inputs) => {
        this.modelInputs = Object.keys(inputs).map((val) => inputs[val]);
      });
  }

  public removeInput(modelInput: IModelInput) {
    this.modelEditFacade.multiModelCommand.removeModelInput(modelInput);
    this.logger.trackTaskCenterEvent('ModelConfig:MultiModelEdit:RemoveTag');
  }

  public forceInclude(modelInput: IModelInput) {
    const forceIncludeParam = {
      forceIncludeAll: !modelInput.input?.forcedInclusion,
      modelInput,
    } as IForceIncludeMultiModelParams;
    this.modelEditFacade.multiModelCommand.forceIncludeTags(forceIncludeParam);
    this.logger.trackTaskCenterEvent(
      'ModelConfig:MultiModelEdit:ForcedInclusion'
    );
  }
}
