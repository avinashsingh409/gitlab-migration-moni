import {
  ColDef,
  ColGroupDef,
  ColumnFunctionCallbackParams,
} from '@ag-grid-enterprise/all-modules';
import moment from 'moment';

export const forecastColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'name',
    width: 400,
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'TagUnits',
    headerName: 'Units',
    cellClass: 'not-editable',
    width: 106,
    field: 'dependent.tagUnits',
    sortable: true,
    editable: false,
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
    cellStyle: { 'font-style': 'italic' },
  },
  {
    colId: 'ForecastHorizonType',
    headerName: 'Earliest Acceptable Intercept Type',
    field: 'properties.modelType.properties.horizonType',
    sortable: true,
    editable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['Relative', 'Fixed'],
    },
    width: 255,

    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Relative', 'Fixed'],
    },
    headerTooltip:
      'The earliest acceptable intercept can be a fixed date or a relative date.',
  },
  {
    colId: 'ForecastRelativeEarliesInterceptDate',
    headerName: 'Relative Earliest Intercept Date',
    field: 'relativeEarliestInterceptDate',
    sortable: true,
    width: 250,
    cellClassRules: {
      'not-editable': (params) => {
        if (
          params.data.properties.modelType.properties.horizonType !== 'Relative'
        ) {
          return false;
        }
        return true;
      },
      locked: (params) => {
        if (
          params.data.properties.modelType.properties.horizonType !== 'Fixed'
        ) {
          return false;
        }
        return true;
      },
    },
    editable: false,
    headerTooltip:
      'How far in the future the relative earliest intercept date is.',
  },

  {
    colId: 'ForecastTimeSpanDuration',
    headerName: 'Time Span Duration',
    field: 'properties.modelType.properties.horizonPeriod',
    sortable: true,
    editable: (params: ColumnFunctionCallbackParams) => {
      return (
        params.data.properties.modelType.properties.horizonType !== 'Fixed'
      );
    },
    cellEditor: 'positiveIntCellEditor',
    cellClassRules: {
      locked: (params) => {
        if (
          params.data.properties.modelType.properties.horizonType !== 'Fixed'
        ) {
          return false;
        }
        return true;
      },
    },
    width: 180,
    headerTooltip:
      'Defines the duration of time for the relative earliest intercept date.',
  },
  {
    colId: 'ForecastTimeSpanDurationUnitOfTime',
    headerName: 'Time Span Units',
    field: 'properties.modelType.properties.horizonPeriodUnitOfTime',
    sortable: true,
    editable: (params: ColumnFunctionCallbackParams) => {
      return (
        params.data.properties.modelType.properties.horizonType !== 'Fixed'
      );
    },
    cellClassRules: {
      locked: (params) => {
        if (
          params.data.properties.modelType.properties.horizonType !== 'Fixed'
        ) {
          return false;
        }
        return true;
      },
    },
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['N/A', 'Days', 'Weeks', 'Months', 'Years'],
    },
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Days', 'Weeks', 'Months', 'Years'],
    },
    width: 150,
    headerTooltip:
      'Defines the unit of time for the relative earliest intercept date.',
  },
  {
    colId: 'ForecastEarliestAcceptableInterceptDate',
    headerName: 'Fixed Earliest Acceptable Intercept Date',
    field: 'fixedEarliestInterceptDate',
    sortable: true,
    editable: (params: ColumnFunctionCallbackParams) => {
      return (
        params.data.properties.modelType.properties.horizonType !== 'Relative'
      );
    },
    cellEditor: 'dateCellEditor',
    cellEditorPopup: true,
    cellClassRules: {
      'invalid-cell': (params) => {
        if (params.data.fixedEarliestInterceptDate === 'invalid date') {
          return true;
        }
        const tempDate = params.data.fixedEarliestInterceptDate
          .substring(0, 10)
          .split('/');

        const forecastDate = moment.utc([
          +tempDate[2],
          +tempDate[0] - 1,
          +tempDate[1],
        ]);

        const today = moment.utc(new Date());
        if (today > forecastDate) {
          return true;
        }
        return false;
      },
      locked: (params) => {
        if (
          params.data.properties.modelType.properties.horizonType !== 'Relative'
        ) {
          return false;
        }
        return true;
      },
    },
    width: 300,
    headerTooltip:
      'The specific earliest acceptable intercept date. Does not change unless changed by a user.',
  },
  {
    colId: 'ForecastThresholdType',
    headerName: 'Threshold Type',
    field: 'properties.modelType.properties.thresholdOperator',
    sortable: true,
    editable: true,
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['Less Than', 'Greater Than'],
    },
    width: 155,
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Less Than', 'Greater Than'],
    },
    headerTooltip:
      'Compare whether the forecasted tag value is above or below the alert threshold.',
  },
  {
    colId: 'ForecastAlertThreshold',
    headerName: 'Alert Threshold',
    field: 'properties.modelType.properties.threshold',
    sortable: true,
    editable: true,
    cellEditor: 'realNumberNumericCellEditor',
    cellEditorPopup: true,
    headerTooltip:
      'Generates an alert if the forecasted tag value is above or below this value. Defined in the same units as the dependent tag.',
  },
  {
    colId: 'ForecastBasis',
    headerName: 'Forecast Basis',
    field: 'properties.modelType.properties.historicalPeriod',
    sortable: true,
    editable: true,
    cellEditor: 'positiveIntCellEditor',
    cellEditorPopup: true,
    headerTooltip:
      "How much historical data is used to calculate the regression equation used to forecast the tag's future value. Shorter basis will be more responsive to changes. Longer basis will be less sensitive to noise.",
  },
  {
    colId: 'ForecastBasisUnits',
    headerName: 'Basis Units',
    field: 'properties.modelType.properties.historicalPeriodUnitOfTime',
    sortable: true,
    editable: true,
    headerTooltip: 'Defines the unit of time for the history basis.',
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Days', 'Months', 'Weeks', 'Years'],
    },
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['N/A', 'Days', 'Months', 'Weeks', 'Years'],
    },
  },
];
