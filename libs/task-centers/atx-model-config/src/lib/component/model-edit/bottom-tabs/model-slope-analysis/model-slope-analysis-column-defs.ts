import {
  ColDef,
  ColGroupDef,
  ColumnFunctionCallbackParams,
} from '@ag-grid-enterprise/all-modules';

export const initialColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'name',
    width: 400,
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'TagUnits',
    headerName: 'Units',
    width: 150,
    field: 'dependent.tagUnits',
    sortable: true,
    editable: true,
    valueGetter: (params) => {
      return (
        params.data.dependent?.tagUnits +
        '/' +
        params.data.properties.modelType.properties.expressionPeriodUnitOfTime
          .toLowerCase()
          .slice(0, -1)
      );
    },
    cellEditorSelector: (params) => {
      return {
        component: 'agSelectCellEditor',
        params: {
          values: [
            params.data.dependent?.tagUnits + '/hour',
            params.data.dependent?.tagUnits + '/day',
            params.data.dependent?.tagUnits + '/week',
            params.data.dependent?.tagUnits + '/month',
            params.data.dependent?.tagUnits + '/year',
          ],
        },
      };
    },
    filter: 'agTextColumnFilter',
    enableRowGroup: true,
  },
  {
    colId: 'ROCActualValueSlopeWindow',
    headerName: 'Actual Value Slope Window',
    field: 'properties.modelType.properties.evaluationPeriod',
    sortable: true,
    editable: true,
    cellEditor: 'realNumberNumericCellEditor',
    cellEditorPopup: true,
    cellClassRules: {
      'invalid-cell': (params) => {
        const minValue = 1;
        let maxValue = 1;
        switch (
          params.data.properties.modelType.properties.evaluationPeriodUnitOfTime
        ) {
          case 'Years':
            maxValue = 1;
            break;
          case 'Months':
            maxValue = 12;
            break;
          case 'Weeks':
            maxValue = 52;
            break;
          case 'Days':
            maxValue = 365;
            break;
          case 'Hours':
            maxValue = 8760;
            break;
          default:
            break;
        }
        if (
          params.data.properties.modelType.properties.evaluationPeriod <
            minValue ||
          params.data.properties.modelType.properties.evaluationPeriod >
            maxValue
        ) {
          return true;
        }

        return false;
      },
    },
    tooltipField: '',
  },
  {
    colId: 'ROCTimeSpan',
    headerName: 'Time Span',
    field: 'properties.modelType.properties.evaluationPeriodUnitOfTime',
    sortable: true,
    editable: true,
    tooltipField: '',
    cellEditor: 'agSelectCellEditor',
    cellEditorParams: {
      values: ['Hours', 'Days', 'Weeks', 'Months', 'Years'],
    },
    filter: 'agSetColumnFilter',
    filterParams: {
      values: ['N/A', 'Hours', 'Days', 'Weeks', 'Months', 'Years'],
    },
  },
];
