import {
  Component,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
  Input,
} from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import { NavFacade } from '@atonix/atx-navigation';
import {
  CellEditRequestEvent,
  ClientSideRowModelModule,
  ClipboardModule,
  ColDef,
  ColGroupDef,
  Column,
  ColumnApi,
  ColumnsToolPanelModule,
  EditableCallbackParams,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GetContextMenuItemsParams,
  GridApi,
  GridOptions,
  GridReadyEvent,
  MenuItemDef,
  MenuModule,
  RangeSelectionModule,
  RowNode,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
} from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';
import {
  SensitivityAlertTypeLabel,
  SensitivityAlertTypes,
  SensitivityAlertTypeTooltips,
} from '../../../../service/model/sensitivity-chips';
import {
  anomalyAreaAlertColumnDefs,
  anomalyFrequencyAlertColumnDefs,
  anomalyOscillationAlertColumnDefs,
  averageAnomalyAlertColumnDefs,
  defaultAlertsColumnDefs,
  frozenDataAlertColumnDefs,
  highhighAlertColumnDefs,
  lowLowAlertColumnDefs,
} from './alerts-column-defs';
import { SendMessageIconRendererComponent } from '../../../ag-grid-components/send-message-icon-renderer-component';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { LoggerService } from '@atonix/shared/utils';
import {
  NullableNumericCellEditorComponent,
  NumericCellEditorComponent,
  PositiveIntCellEditorComponent,
  RealNumberNumericCellEditorComponent,
} from '@atonix/shared/ui';
import { IModelConfigData } from '@atonix/shared/api';
import { agGridFieldChangeDirtiesObject } from '@atonix/atx-core';

@Component({
  selector: 'atx-model-sensitivity-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelSensitivityAlertsComponent implements OnDestroy {
  @Input() isMultiModel = false;
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly models$ = this.modelEditFacade.query.modelSummaries$;
  public unsubscribe$ = new Subject<void>();
  private gridApi!: GridApi;
  private columnApi!: ColumnApi;
  readonly sensitivityAlertTypeLabel = SensitivityAlertTypeLabel;
  readonly sensitivityAlertTypeTooltips = SensitivityAlertTypeTooltips;
  sensitivityAlertTypes = Object.values(SensitivityAlertTypes).filter(
    (value) =>
      typeof value === 'number' &&
      value !== SensitivityAlertTypes.AverageAnomaly
  );

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    ClientSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
  ];

  public columnDefs: (ColDef | ColGroupDef)[] = this.appConfig
    .enableModelConfigSensitivityAvgAnomalyFilter
    ? [
        ...defaultAlertsColumnDefs,
        ...averageAnomalyAlertColumnDefs,
        ...anomalyFrequencyAlertColumnDefs, //Anomaly Persistence Alert
        ...anomalyOscillationAlertColumnDefs,
        ...highhighAlertColumnDefs,
        ...lowLowAlertColumnDefs,
        ...frozenDataAlertColumnDefs,
        ...anomalyAreaAlertColumnDefs,
      ]
    : [
        ...defaultAlertsColumnDefs,
        ...anomalyFrequencyAlertColumnDefs, //Anomaly Persistence Alert
        ...anomalyOscillationAlertColumnDefs,
        ...highhighAlertColumnDefs,
        ...lowLowAlertColumnDefs,
        ...frozenDataAlertColumnDefs,
        ...anomalyAreaAlertColumnDefs,
      ];

  gridOptions: GridOptions = {
    rowModelType: 'clientSide',
    overlayLoadingTemplate:
      '<div class="ag-overlay-loading-center"><div class="loader"></div>Loading</div>',
    overlayNoRowsTemplate: '<div></div>',
    components: {
      intCellEditor: PositiveIntCellEditorComponent,
      sendMessageRenderer: SendMessageIconRendererComponent,
      numericCellEditor: NumericCellEditorComponent,
      nullableNumericCellEditor: NullableNumericCellEditorComponent,
      realNumberNumericCellEditor: RealNumberNumericCellEditorComponent,
    },
    rowGroupPanelShow: 'never',
    enableRangeSelection: false,
    stopEditingWhenCellsLoseFocus: true,
    animateRows: true,
    debug: false,
    singleClickEdit: true,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    rowHeight: 30,
    suppressMovableColumns: true,
    suppressRowClickSelection: true,
    suppressCopyRowsToClipboard: true,
    suppressRowDeselection: false,
    tooltipShowDelay: 0,
    readOnlyEdit: true,
    getContextMenuItems: (params) => this.getContextMenuItems(params),
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      enableRowGroup: false,
    },
    suppressAggFuncInHeader: true,
    getRowId: (params) => {
      return params.data.modelExtID;
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
    },
    onCellEditRequest: (event: CellEditRequestEvent) => {
      this.cellEditRequest(
        event.data.modelExtID,
        event.colDef.field || '',
        event.newValue,
        event.data
      );
    },
  };

  cellEditRequest(
    modelExtID: string,
    columnField: string,
    newValue: any,
    data: any
  ) {
    if (columnField === 'anomalyFrequencyAlert.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setAnomalyFrequencyEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'anomalyOscillationAlert.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setAnomalyOscillationEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'highHighAlert.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setHighHighAlertEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'lowLowAlert.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setLowLowAlertEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'frozenDataCheckAlert.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setFrozenDataCheckAlertEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'upperFixedLimitAnomaly.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setUpperFixedLimitAnomalyEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'lowerFixedLimitAnomaly.enabled') {
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setLowerFixedLimitAnomalyEnabled({
        modelExtID: modelExtID,
        columnField: '',
        newValue: enabled,
      });
    } else if (columnField === 'dependent.tagUnits') {
      this.modelEditFacade.command.setROCExpressionUnits({
        modelExtID: modelExtID,
        columnField: '',
        newValue: newValue,
      });
    } else if (columnField === 'anomalyAreaAlert.enabled') {
      // For High High Alert have to manually change 'Yes'/'No' to true false
      const enabled = newValue === 'Yes' ? true : false;
      this.modelEditFacade.command.setAnomalyAreaAlertEnabled({
        modelExtID: modelExtID,
        columnField: columnField || '',
        newValue: enabled,
      });
    } else if (
      columnField === 'highHighAlert.properties.useMeanAbsoluteError'
    ) {
      if (data?.highHighAlert?.enabled) {
        // For High High Alert have to manually change 'Yes'/'No' to true false
        const enabled = newValue === 'Yes' ? true : false;
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: enabled,
        });
      }
    } else if (columnField === 'lowLowAlert.properties.useMeanAbsoluteError') {
      if (data?.lowLowAlert?.enabled) {
        // For Low Low Alert have to manually change 'Yes'/'No' to true false
        const enabled = newValue === 'Yes' ? true : false;
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: enabled,
        });
      }
    } else if (
      columnField ===
      'averageAnomalyAlert.properties.evaluationPeriodUnitOfTime'
    ) {
      if (data?.averageAnomalyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'averageAnomalyAlert.properties.averageEvalPeriod'
    ) {
      if (data?.averageAnomalyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'averageAnomalyAlert.properties.averagingWindowUnitOfTime'
    ) {
      if (data?.averageAnomalyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField ===
      'anomalyAreaAlert.properties.areaFastResponsePeriodUnitOfTime'
    ) {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'anomalyAreaAlert.properties.areaFastResponsePeriod'
    ) {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'anomalyAreaAlert.properties.areaFastResponse') {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField ===
      'anomalyAreaAlert.properties.areaSlowResponsePeriodUnitOfTime'
    ) {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'anomalyAreaAlert.properties.areaSlowResponsePeriod'
    ) {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'anomalyAreaAlert.properties.areaSlowResponse') {
      if (data?.anomalyAreaAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'anomalyFrequencyAlert.properties.evaluationPeriod'
    ) {
      if (data?.anomalyFrequencyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField ===
      'anomalyFrequencyAlert.properties.evaluationPeriodUnitOfTime'
    ) {
      if (data?.anomalyFrequencyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'anomalyFrequencyAlert.properties.threshold') {
      if (data?.anomalyFrequencyAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'anomalyOscillationAlert.properties.evaluationPeriod'
    ) {
      if (data?.anomalyOscillationAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField ===
      'anomalyOscillationAlert.properties.evaluationPeriodUnitOfTime'
    ) {
      if (data?.anomalyOscillationAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'anomalyOscillationAlert.properties.threshold') {
      if (data?.anomalyOscillationAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'highHighAlert.properties.threshold') {
      if (data?.highHighAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (columnField === 'lowLowAlert.properties.threshold') {
      if (data?.lowLowAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField === 'frozenDataCheckAlert.properties.evaluationPeriod'
    ) {
      if (data?.frozenDataCheckAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else if (
      columnField ===
      'frozenDataCheckAlert.properties.evaluationPeriodUnitOfTime'
    ) {
      if (data?.frozenDataCheckAlert?.enabled) {
        this.modelEditFacade.command.updateModelSummary({
          modelExtID: modelExtID,
          columnField: columnField || '',
          newValue: newValue,
        });
      }
    } else {
      this.modelEditFacade.command.updateModelSummary({
        modelExtID: modelExtID,
        columnField: columnField || '',
        newValue: newValue,
      });
    }
  }

  changeAlertColumns(value: SensitivityAlertTypes) {
    switch (value) {
      case SensitivityAlertTypes.All:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:AllAlerts'
        );
        if (this.appConfig.enableModelConfigSensitivityAvgAnomalyFilter) {
          this.columnDefs = [
            ...defaultAlertsColumnDefs,
            ...averageAnomalyAlertColumnDefs,
            ...anomalyFrequencyAlertColumnDefs, //Anomaly Persistence Alert
            ...anomalyOscillationAlertColumnDefs,
            ...highhighAlertColumnDefs,
            ...lowLowAlertColumnDefs,
            ...frozenDataAlertColumnDefs,
            ...anomalyAreaAlertColumnDefs,
          ];
        } else {
          this.columnDefs = [
            ...defaultAlertsColumnDefs,
            ...anomalyFrequencyAlertColumnDefs, //Anomaly Persistence Alert
            ...anomalyOscillationAlertColumnDefs,
            ...highhighAlertColumnDefs,
            ...lowLowAlertColumnDefs,
            ...frozenDataAlertColumnDefs,
            ...anomalyAreaAlertColumnDefs,
          ];
        }

        break;
      case SensitivityAlertTypes.AnomalyArea:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:AnomalyArea'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...anomalyAreaAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.AnomalyFrequency:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:AnomalyFrequency'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...anomalyFrequencyAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.AnomalyOscillation:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:AnomalyOscillation'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...anomalyOscillationAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.AverageAnomaly:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:AverageAnomaly'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...averageAnomalyAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.FrozenDataCheck:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:FrozenData'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...frozenDataAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.HighHigh:
        this.logger.trackTaskCenterEvent(
          'ModelConfig:SingleModelEdit:HighHigh'
        );
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...highhighAlertColumnDefs,
        ];
        break;
      case SensitivityAlertTypes.LowLow:
        this.logger.trackTaskCenterEvent('ModelConfig:SingleModelEdit:LowLow');
        this.columnDefs = [
          ...defaultAlertsColumnDefs,
          ...lowLowAlertColumnDefs,
        ];
        break;
    }

    this.modelEditFacade.command.setAlert({
      type: 'alert',
      subType: +value,
    });
  }

  getContextMenuItems(
    params: GetContextMenuItemsParams
  ): (string | MenuItemDef)[] {
    const menuItems: (string | MenuItemDef)[] = ['copy', 'copyWithHeaders'];
    const colDef = params.column?.getColDef() as ColDef;

    if (this.isMultiModel) {
      const customMenuItems = [
        'separator',
        {
          checked: true,
          name: 'Apply Value to Column',
          action: () => {
            const fieldName = colDef.field || '';
            const newValue = params.value;
            params?.api?.forEachNode((rowNode: RowNode) => {
              const currentModel = rowNode.data as IModelConfigData;
              const hasChanged = agGridFieldChangeDirtiesObject(
                currentModel,
                colDef.field as string,
                newValue
              );
              if (hasChanged) {
                const enabled = newValue === 'Yes' ? true : false;
                // Use Default and enabled dropdowns will set other columns if they are changed.
                // if they are currently set to the same value as the user is applying, do not
                // change these values
                if (fieldName === 'anomalyAreaAlert.enabled') {
                  if (currentModel.anomalyAreaAlert.enabled !== enabled) {
                    this.modelEditFacade.command.setAnomalyAreaAlertEnabled({
                      modelExtID: rowNode.data.modelExtID,
                      columnField: fieldName,
                      newValue: enabled,
                    });
                  }
                } else if (fieldName === 'anomalyFrequencyAlert.enabled') {
                  if (currentModel.anomalyFrequencyAlert.enabled !== enabled) {
                    this.modelEditFacade.command.setAnomalyFrequencyEnabled({
                      modelExtID: rowNode.data.modelExtID,
                      columnField: '',
                      newValue: enabled,
                    });
                  }
                } else if (fieldName === 'anomalyOscillationAlert.enabled') {
                  if (
                    currentModel.anomalyOscillationAlert.enabled !== enabled
                  ) {
                    this.modelEditFacade.command.setAnomalyOscillationEnabled({
                      modelExtID: rowNode.data.modelExtID,
                      columnField: '',
                      newValue: enabled,
                    });
                  }
                } else if (fieldName === 'highHighAlert.enabled') {
                  if (currentModel.highHighAlert.enabled !== enabled) {
                    this.modelEditFacade.command.setHighHighAlertEnabled({
                      modelExtID: rowNode.data.modelExtID,
                      columnField: '',
                      newValue: enabled,
                    });
                  }
                } else if (fieldName === 'lowLowAlert.enabled') {
                  if (currentModel.lowLowAlert.enabled !== enabled) {
                    this.modelEditFacade.command.setLowLowAlertEnabled({
                      modelExtID: rowNode.data.modelExtID,
                      columnField: '',
                      newValue: enabled,
                    });
                  }
                } else if (fieldName === 'frozenDataCheckAlert.enabled') {
                  if (currentModel.frozenDataCheckAlert.enabled !== enabled) {
                    this.modelEditFacade.command.setFrozenDataCheckAlertEnabled(
                      {
                        modelExtID: rowNode.data.modelExtID,
                        columnField: '',
                        newValue: enabled,
                      }
                    );
                  }
                } else {
                  this.cellEditRequest(
                    rowNode.data.modelExtID,
                    fieldName,
                    params.value,
                    rowNode.data
                  );
                }
              }
            });
          },
        },
      ];

      if (typeof colDef?.editable === 'boolean') {
        if (colDef?.editable) {
          menuItems.push(...customMenuItems);
        }
      } else {
        const callbackParam: EditableCallbackParams = {
          api: params.api,
          colDef: colDef,
          column: params.column as Column,
          columnApi: params.columnApi,
          data: params?.node?.data,
          node: params?.node as RowNode,
          context: params.context,
        };
        if (colDef.editable?.(callbackParam)) {
          menuItems.push(...customMenuItems);
        }
      }
    }

    return menuItems;
  }

  focusOut() {
    this.gridApi.deselectAll();
  }

  constructor(
    public navFacade: NavFacade,
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    if (this.appConfig.enableModelConfigSensitivityAvgAnomalyFilter) {
      this.sensitivityAlertTypes = Object.values(SensitivityAlertTypes).filter(
        (value) => typeof value === 'number'
      );
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
