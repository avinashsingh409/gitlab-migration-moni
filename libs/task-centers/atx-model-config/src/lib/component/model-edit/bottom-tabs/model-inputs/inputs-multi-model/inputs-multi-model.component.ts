import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IAssetVariableTypeTagMap } from '@atonix/atx-core';
import { EModelTypes, IModelIndependent } from '@atonix/shared/api';
import { LoggerService } from '@atonix/shared/utils';
import { ModelEditFacade } from '../../../../../service/model-edit.facade';

@Component({
  selector: 'atx-inputs-multi-model-config',
  templateUrl: './inputs-multi-model.component.html',
  styleUrls: ['./inputs-multi-model.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputsMultiModelComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  public readonly modelTypes = EModelTypes;

  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService
  ) {}

  addModelInputTag(tag: IAssetVariableTypeTagMap) {
    if (tag) {
      this.logger.trackTaskCenterEvent('ModelConfig:MultiModelEdit:AddTag');
      const modelInputs: IModelIndependent = {
        tagGuid: tag?.Tag?.GlobalID,
        tagName: tag?.Tag?.TagName,
        tagDesc: tag?.Tag?.TagDesc,
        forcedInclusion: false,
        assetVariableTagMapID: tag?.AssetVariableTypeTagMapID,
        inUse: false,
      } as IModelIndependent;
      this.modelEditFacade.multiModelCommand.addModelInputTags([modelInputs]);
    }
  }

  addModelInputTags(tags: IAssetVariableTypeTagMap[]) {
    if (tags) {
      this.logger.trackTaskCenterEvent('ModelConfig:MultiModelEdit:AddTag');
      const inputs = tags.map((tag) => {
        return {
          tagGuid: tag?.Tag?.GlobalID,
          tagName: tag?.Tag?.TagName,
          tagDesc: tag?.Tag?.TagDesc,
          forcedInclusion: false,
          assetVariableTagMapID: tag?.AssetVariableTypeTagMapID,
          inUse: false,
        } as IModelIndependent;
      });

      this.modelEditFacade.multiModelCommand.addModelInputTags(inputs);
    }
  }

  onDragOver(event: any) {
    const types = event.dataTransfer.types;
    const dragSupported = types.length;
    if (dragSupported) {
      event.dataTransfer.dropEffect = 'move';
    }
    event.preventDefault();
  }

  onDrop(event: any) {
    event.preventDefault();
    const userAgent = window.navigator.userAgent;
    const isIE = userAgent.indexOf('Trident/') >= 0;
    const transferedData = event.dataTransfer.getData(
      isIE ? 'text' : 'text/plain'
    );
    const assetVariableTypeTagMaps: IAssetVariableTypeTagMap[] =
      JSON.parse(transferedData);

    this.addModelInputTags(assetVariableTypeTagMaps);
  }
}
