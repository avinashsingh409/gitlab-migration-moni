import { ColDef, ColGroupDef } from '@ag-grid-enterprise/all-modules';

export const defaultColumnDefs: (ColDef | ColGroupDef)[] = [
  {
    colId: 'ModelName',
    headerName: 'Model Name',
    field: 'name',
    width: 400,
    editable: false,
    cellClass: 'not-editable',
  },
  {
    colId: 'SmoothingFactorActual',
    headerName: 'Actual Value Average',
    cellEditor: 'intCellEditor',
    field: 'training.smoothingFactorActual',
    editable: true,
    cellClassRules: {
      'invalid-cell': (params) => {
        if (
          params.data.training.smoothingFactorActual < 1 ||
          params.data.training.smoothingFactorActual > 518400
        ) {
          return true;
        }

        return false;
      },
    },
    headerTooltip:
      "Defines how much the raw tag value is averaged to create the model trend's actual value.",
  },
  {
    colId: 'SmoothingFactorActualUnits',
    headerName: 'Average Unit of Time',
    field: 'training.smoothingFactorActualUnits',
    editable: false,
  },
  {
    colId: 'SmoothingFactorExpected',
    headerName: 'Expected Value Average',
    field: 'training.smoothingFactorExpected',
    editable: true,
    cellEditor: 'intCellEditor',
    cellClassRules: {
      'invalid-cell': (params) => {
        if (
          params.data.training.smoothingFactorExpected < 1 ||
          params.data.training.smoothingFactorExpected > 518400
        ) {
          return true;
        }

        return false;
      },
    },
    headerTooltip:
      "Defines how much the raw tag value is averaged to create the model trend's expected value. This must be longer than the actual value average.",
  },
  {
    colId: 'SmoothingFactorExpectedUnits',
    headerName: 'Average Unit of Time',
    field: 'training.smoothingFactorExpectedUnits',
    editable: false,
  },
];
