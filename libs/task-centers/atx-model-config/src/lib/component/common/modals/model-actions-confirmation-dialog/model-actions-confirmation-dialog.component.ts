import {
  Component,
  ChangeDetectionStrategy,
  Inject,
  OnDestroy,
} from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, Observable, of, Subject, takeUntil } from 'rxjs';
import { DeleteModelsService } from '../../../../store/services/delete-models.service';

export interface ActionsConfirmationModel {
  models: any[];
  actionType: ActionsType;
}

export enum ActionsType {
  Delete = 0,
  Activate = 1,
  Deactivate = 2,
}

@Component({
  selector: 'atx-model-actions-confirmation-dialog',
  templateUrl: './model-actions-confirmation-dialog.component.html',
  styleUrls: ['./model-actions-confirmation-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelActionsConfirmationDialogComponent implements OnDestroy {
  private isLoading: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private message: BehaviorSubject<string> = new BehaviorSubject('');
  private unsubscribe$ = new Subject<void>();

  public isLoading$!: Observable<boolean>;
  public message$!: Observable<string>;
  constructor(
    private dialogRef: MatDialogRef<ModelActionsConfirmationDialogComponent>,
    private deleteModelsService: DeleteModelsService,
    private toastService: ToastService,
    @Inject(MAT_DIALOG_DATA) public data: ActionsConfirmationModel
  ) {
    this.deleteModelsService.resetState();

    if (data.actionType === ActionsType.Delete) {
      this.message.next(
        `Are you sure you want to delete ${
          this.data.models.length === 1 ? 'model' : 'models'
        }?`
      );

      deleteModelsService.notDeletedModels$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((models) => {
          if (models && models.length > 0) {
            this.dialogRef.close(data.models.length !== models.length);
            const multiModelStr = models.map((x) => x.modelName).join(', ');
            this.toastService.openSnackBar(
              `${
                models.length >= 1 ? 'Model' : 'Models'
              } ${multiModelStr} not deleted.`,
              'error'
            );
          }
        });

      deleteModelsService.deletionSuccessful$
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((deletionSuccessful) => {
          if (deletionSuccessful) {
            this.dialogRef.close(deletionSuccessful);
            this.deleteModelsService.resetState();
          }
        });
    }

    if (data.actionType === ActionsType.Activate) {
      this.message.next(
        `Activating the model will cause the model to predict expected values and be evaluated for alerts. Are you sure you want to activate the selected model? <br><br> Applying this change will require saving the model.`
      );
    }

    if (data.actionType === ActionsType.Deactivate) {
      this.message.next(
        `Deactivating the model will cause the model to no longer predict expected values nor be evaluated for alerts. Are you sure you want to deactivate the selected model? <br><br> Applying this change will require saving the model.`
      );
    }

    this.message.subscribe((val) => {
      this.message$ = of(val);
    });

    this.isLoading.subscribe((val) => {
      this.isLoading$ = of(val);
    });
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public modelAction() {
    this.isLoading.next(true);
    if (this.data.actionType === ActionsType.Delete) {
      this.deleteModelsService.deleteModels(this.data?.models);
      this.isLoading.next(false);
    }

    if (
      this.data.actionType === ActionsType.Activate ||
      this.data.actionType === ActionsType.Deactivate
    ) {
      this.dialogRef.close(true);
    }
  }

  public cancel() {
    this.dialogRef.close('cancel');
  }
}
