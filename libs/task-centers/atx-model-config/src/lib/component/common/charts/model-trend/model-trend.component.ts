import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ModelEditFacade } from '../../../../service/model-edit.facade';
import {
  MatCheckboxDefaultOptions,
  MAT_CHECKBOX_DEFAULT_OPTIONS,
} from '@angular/material/checkbox';
import { NavFacade } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';

@Component({
  selector: 'atx-model-edit-tabs-model-trend',
  templateUrl: './model-trend.component.html',
  styleUrls: ['./model-trend.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions,
    },
  ],
})
export class ModelEditTabsModelTrendComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;

  constructor(
    private modelEditFacade: ModelEditFacade,
    private logger: LoggerService,
    public navFacade: NavFacade
  ) {
    this.logger.tabClicked('Model Trend', 'Model Config');
  }
}
