import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { sigFig } from '@atonix/atx-core';
import { NavFacade } from '@atonix/atx-navigation';
import { IActionItem, INDModelSummary } from '@atonix/shared/api';
import { AuthFacade } from '@atonix/shared/state/auth';
import { LoggerService, ToastService } from '@atonix/shared/utils';
import moment from 'moment';
import { Observable, Subject, of, switchMap, take, takeUntil } from 'rxjs';
import { ModelActionsFacade } from '../../../service/model-actions/model-actions.facade';
import {
  CriteriaType,
  LowerBoundTypeMapping,
  LowerBoundTypes,
  AlertsActionEvent,
  ModelTypes,
  UpperBoundTypeMapping,
  UpperBoundTypes,
} from '../../../service/model-actions/model-actions.models';
import { ModelActionsState } from '../../../service/model-actions/model-actions.query';
import { ModelConfigFacade } from '../../../store/facade/model-config.facade';

@Component({
  selector: 'atx-model-config-action-pane',
  templateUrl: './model-config-action-pane.component.html',
  styleUrls: ['./model-config-action-pane.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelConfigActionPaneComponent implements OnDestroy, OnChanges {
  @Input() selectedModelIds: string[] = [];
  @Input() hasSelectedMultipleModels = false;
  @Input() showModelInfo = false;
  @Input() newModel = false;
  @Output() modelActionChange = new EventEmitter<AlertsActionEvent>();

  public editingModelDesc = false;

  public modelActionsVm$: Observable<ModelActionsState>;
  public refreshing$: Observable<boolean>;
  public isExternalModelType$: Observable<boolean>;

  public customWatchForm: FormGroup;
  public newDiagnoseForm: FormGroup;
  public newNoteForm: FormGroup;
  public newMaintenanceForm: FormGroup;
  public modelDesc = new FormControl('', {
    nonNullable: true,
    validators: [Validators.maxLength(512)],
  });

  public upperBoundTypes = Object.values(UpperBoundTypes);
  public upperBoundTypeMapping: any = UpperBoundTypeMapping;

  public lowerBoundTypes = Object.values(LowerBoundTypes);
  public lowerBoundTypeMapping: any = LowerBoundTypeMapping;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private navFacade: NavFacade,
    public authFacade: AuthFacade,
    private modelActionsFacade: ModelActionsFacade,
    private modelConfigFacade: ModelConfigFacade,
    private toastService: ToastService,
    private logger: LoggerService
  ) {
    this.modelActionsVm$ = modelActionsFacade.query.vm$;
    this.refreshing$ = modelActionsFacade.query.refreshing$;
    this.isExternalModelType$ = modelActionsFacade.query.isExternalModelType$;

    this.modelActionsFacade.query.selectedModels$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((models) => {
        if (models) {
          this.setFormInitialValues(models);
        }
      });

    this.modelActionsFacade.query.selectedModel$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((model) => {
        if (model) {
          let timestamp = 'No Valid Date';
          if (model.LastNoteDate) {
            timestamp = moment(model.LastNoteDate).format('MM/DD/YYYY, h:mm a');
          }
          this.modelActionsFacade.command.setTimeStamp(timestamp);
        }
      });

    this.customWatchForm = this.modelActionsFacade.buildCustomWatchForm();
    this.newDiagnoseForm = this.modelActionsFacade.buildNewDiagnoseForm();
    this.newMaintenanceForm =
      this.modelActionsFacade.buildNewMaintainanceForm();
    this.newNoteForm = this.modelActionsFacade.buildNewNoteForm();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.newModel?.currentValue) {
      if (changes.selectedModelIds?.currentValue?.length > 0) {
        this.modelActionsFacade.loadModels(
          changes.selectedModelIds?.currentValue
        );

        this.modelActionsFacade.command.setModelIds(
          changes.selectedModelIds?.currentValue
        );
      }
      if (changes.selectedModelIds?.currentValue.length === 0) {
        this.modelActionsFacade.command.resetModelActionsState();
      }
    }
  }
  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onEditModelDesc(description: string | undefined) {
    if (!description && description !== '') {
      return;
    }
    this.editingModelDesc = true;
    this.modelDesc.setValue(description);
  }

  onSaveModelDesc() {
    this.editingModelDesc = false;
    this.modelActionsFacade.updateModelDesc(this.modelDesc.value);
  }

  createActionNote(model: INDModelSummary) {
    let note = '';
    note = 'Actual = ' + sigFig(model?.Actual ?? 0, 4) + ', \n';
    note += 'Expected = ' + sigFig(model?.Expected ?? 0, 4) + ', \n';
    note += 'Lower = ' + sigFig(model?.LowerLimit ?? 0, 4) + ', \n';
    note += 'Upper = ' + sigFig(model?.UpperLimit ?? 0, 4);
    return note;
  }

  public resetForms() {
    this.modelActionsFacade.command.resetForms();

    this.customWatchForm = this.modelActionsFacade.buildCustomWatchForm();
    this.newDiagnoseForm.reset();
    this.newNoteForm.reset();
    this.newMaintenanceForm.reset();

    this.modelActionsFacade.query.selectedModels$
      .pipe(take(1))
      .subscribe((models) => {
        if (models) {
          this.setFormInitialValues(models);
        }
      });
  }

  public hideInfoTray() {
    if (this.showModelInfo) {
      this.modelActionChange.emit({
        event: 'ToggleActionFlyout',
        newValue: true,
      });
    } else {
      this.navFacade.hideRightTray();
      this.modelConfigFacade.treeSizeChange(300);
    }
  }

  public refreshModel($event: MouseEvent) {
    this.modelActionsFacade.query.selectedModelIds$
      .pipe(take(1))
      .subscribe((ids) => {
        if (ids) {
          this.modelActionsFacade.loadModels(ids);
        }
      });
    $event.stopPropagation();
  }

  public clearAlert() {
    this.modelActionsFacade.query.selectedModels$
      .pipe(take(1))
      .subscribe((models) => {
        if (models) {
          this.modelActionsFacade.clearAlerts(models);
        }
      });
  }

  public onWatchChange(data: string) {
    const watchValue = data;
    if (data !== 'custom') {
      this.modelActionsFacade.query.selectedModels$
        .pipe(take(1))
        .subscribe((models) => {
          if (models) {
            if (data === '7') {
              this.logger.trackTaskCenterEvent(
                'ModelConfig:ActionPane:7DayQuickWatch'
              );

              this.modelActionsFacade.setWatch({ hours: '168', models });
            } else {
              if (data === '6') {
                this.logger.trackTaskCenterEvent(
                  'ModelConfig:ActionPane:6HourQuickWatch'
                );
              } else if (data === '24') {
                this.logger.trackTaskCenterEvent(
                  'ModelConfig:ActionPane:24HourQuickWatch'
                );
              }

              this.modelActionsFacade.setWatch({ hours: watchValue, models });
            }
          }
          this.resetForms();
        });
    } else {
      this.resetForms();
      this.modelActionsFacade.command.setShowCustomWatchForm(true);
    }
  }

  public submitCustomWatch() {
    if (this.customWatchForm.valid) {
      this.modelActionsFacade.query.selectedModels$
        .pipe(take(1))
        .subscribe((models) => {
          if (models) {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:ActionPane:CustomWatch'
            );

            const params = {
              NoteText: this.customWatchForm.value.customNote,
              WatchDuration: this.customWatchForm.value.timeSpan,
              WatchDurationTemporalTypeId: this.customWatchForm.value.timeUnit,
              WatchLimit: this.customWatchForm.value.upperUnits,
              WatchLimit2:
                this.customWatchForm.value.lowerControl !==
                  LowerBoundTypes.AbsoluteValue &&
                this.customWatchForm.value.lowerBoundExceeded === true
                  ? +this.customWatchForm.value.lowerUnits * -1
                  : this.customWatchForm.value.lowerUnits,
              WatchEmailIfAlert: this.customWatchForm.value.emailAlert,
              WatchRelativeToExpected:
                this.customWatchForm.value.upperControl ===
                  UpperBoundTypes.AbsoluteValue ||
                this.customWatchForm.value.upperBoundExceeded === false
                  ? false
                  : true,
              WatchRelativeToExpected2:
                this.customWatchForm.value.lowerControl ===
                  LowerBoundTypes.AbsoluteValue ||
                this.customWatchForm.value.lowerBoundExceeded === false
                  ? false
                  : true,
              WatchCriteriaTypeId:
                this.customWatchForm.value.upperBoundExceeded === true
                  ? CriteriaType.Greater
                  : null,
              WatchCriteriaTypeId2:
                this.customWatchForm.value.lowerBoundExceeded === true
                  ? CriteriaType.Lesser
                  : null,
            };
            this.modelActionsFacade.setCustomWatch({
              models,
              actionItem: params as IActionItem,
              actionType: 'savewatch',
            });
          }
        });
    }
    this.resetForms();
  }

  changeUpperBound(value: any) {
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        if (model) {
          let newUpperBound = 0;

          if (value === UpperBoundTypes.Model) {
            const expected =
              model.Expected === null ? Number(0) : Number(model.Expected);
            const curUpperBound =
              model.UpperLimit === null ? Number(0) : Number(model.UpperLimit);
            newUpperBound = curUpperBound - expected;
          }

          this.setUpperValidators();

          this.customWatchForm.patchValue({
            upperControl: value,
            upperUnits: newUpperBound,
          });
        }
      });
  }

  changeLowerBound(value: any) {
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        if (model) {
          let newLowerBound = 0;

          if (value === LowerBoundTypes.Model) {
            const expected =
              model.Expected === null ? Number(0) : Number(model.Expected);
            const curLowerBound =
              model.LowerLimit === null ? Number(0) : Number(model.LowerLimit);
            newLowerBound = expected - curLowerBound;
          }

          this.setLowerValidators();

          this.customWatchForm.patchValue({
            lowerControl: value,
            lowerUnits: newLowerBound,
          });
        }
      });
  }

  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearWatch(models: INDModelSummary[]) {
    const valueArr = models.map((item) => {
      return item.ActionWatch;
    });
    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0].ActionWatch : false;
  }

  isDisabledFixedUpper(
    value: UpperBoundTypes | LowerBoundTypes
  ): Observable<boolean> {
    return this.modelActionsFacade.query.selectedModels$.pipe(
      take(1),
      switchMap((models) => {
        if (
          models &&
          (value === UpperBoundTypes.Relative ||
            value === UpperBoundTypes.Model) &&
          this.hasFixedLimitType(models)
        ) {
          return of(true);
        }
        return of(false);
      })
    );
  }

  setDefaultNote() {
    this.modelActionsFacade.query.defaultNote$
      .pipe(take(1))
      .subscribe((defaultNote) => {
        if (defaultNote) {
          this.customWatchForm.patchValue({ customNote: defaultNote });
        }
      });
  }

  setUpperValidators() {
    const value = this.customWatchForm.value.upperControl;
    const upperCheck = this.customWatchForm.value.upperBoundExceeded;
    if (!upperCheck || value === UpperBoundTypes.Model) {
      this.customWatchForm.controls['upperUnits'].clearValidators();
    } else if (
      value === UpperBoundTypes.Relative ||
      value === UpperBoundTypes.AbsoluteValue
    ) {
      this.customWatchForm.controls['upperUnits'].setValidators([
        Validators.required,
      ]);
    }
    this.customWatchForm.controls['upperUnits'].updateValueAndValidity({
      onlySelf: true,
    });
  }

  setLowerValidators() {
    const value = this.customWatchForm.value.lowerControl;
    const lowerCheck = this.customWatchForm.value.lowerBoundExceeded;
    if (!lowerCheck || value === LowerBoundTypes.Model) {
      this.customWatchForm.controls['lowerUnits'].clearValidators();
    } else if (
      value === LowerBoundTypes.Relative ||
      value === LowerBoundTypes.AbsoluteValue
    ) {
      this.customWatchForm.controls['lowerUnits'].setValidators([
        Validators.required,
      ]);
    }
    this.customWatchForm.controls['lowerUnits'].updateValueAndValidity({
      onlySelf: true,
    });
  }

  hasFixedLimitType(models: INDModelSummary[]) {
    const fixedLimitModel = models.find(
      (mod) => mod.ModelTypeAbbrev === ModelTypes.FL
    );

    if (fixedLimitModel) {
      return true;
    }

    return false;
  }

  submitClearWatch() {
    this.modelActionsFacade.query.selectedModels$
      .pipe(take(1))
      .subscribe((models) => {
        if (models) {
          this.logger.trackTaskCenterEvent('ModelConfig:ActionPane:ClearWatch');

          const params = {
            NoteText: '',
          };

          this.modelActionsFacade.clearWatch({
            models,
            actionItem: params as IActionItem,
            actionType: 'clearwatch',
          });
          this.resetForms();
        }
      });
  }

  submitDiagnose() {
    this.modelActionsFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedModels) {
        if (this.newDiagnoseForm.valid) {
          const params = {
            NoteText: this.newDiagnoseForm.value.diagnoseNote,
          };
          if (vm.isDiagnose || vm.isMultiClearDiagnose) {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:ActionPane:ClearDiagnose'
            );

            this.modelActionsFacade.clearDiagnose({
              models: vm.selectedModels,
              actionItem: params as IActionItem,
              actionType: 'cleardiagnose',
            });
          } else {
            this.logger.trackTaskCenterEvent('ModelConfig:ActionPane:Diagnose');

            this.modelActionsFacade.saveDiagnose({
              models: vm.selectedModels,
              actionItem: params as IActionItem,
              actionType: 'savediagnose',
            });
          }
          this.resetForms();
        }
      }
    });
  }

  submitNote() {
    this.modelActionsFacade.query.selectedModels$
      .pipe(take(1))
      .subscribe((models) => {
        if (models) {
          if (this.newNoteForm.valid) {
            this.logger.trackTaskCenterEvent('ModelConfig:ActionPane:Note');

            const params = {
              NoteText: this.newNoteForm.value.note,
            };
            this.modelActionsFacade.saveNote({
              models,
              actionItem: params as IActionItem,
              actionType: 'savenote',
            });
            this.resetForms();
          }
        }
      });
  }

  submitMaintenance() {
    this.modelActionsFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedModels) {
        if (this.newMaintenanceForm.valid) {
          const params = {
            NoteText: this.newMaintenanceForm.value.maintenanceNote,
          };
          if (vm.isMaintenance || vm.isMultiClearMaintenance) {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:ActionPane:ClearMaintenance'
            );

            this.modelActionsFacade.clearMaintenance({
              models: vm.selectedModels,
              actionItem: params as IActionItem,
              actionType: 'clearmaintenance',
            });
          } else {
            this.logger.trackTaskCenterEvent(
              'ModelConfig:ActionPane:Maintenance'
            );

            this.modelActionsFacade.saveMaintenance({
              models: vm.selectedModels,
              actionItem: params as IActionItem,
              actionType: 'savemaintenance',
            });
          }
          this.resetForms();
        }
      }
    });
  }

  actionForm(action: string) {
    switch (action) {
      case 'diagnose':
        this.modelActionsFacade.command.showDiagnoseForm();
        break;
      case 'note':
        this.modelActionsFacade.command.showAddNoteForm();
        break;
      case 'maintenance':
        this.modelActionsFacade.command.showMaintenanceForm();
        break;
      case 'cleardiagnose':
        this.modelActionsFacade.command.showDiagnoseForm();
        break;
      case 'clearmaintenance':
        this.modelActionsFacade.command.showMaintenanceForm();
        break;
    }
  }

  setFormInitialValues(models: INDModelSummary[]) {
    const defaultBoundType = this.hasFixedLimitType(models)
      ? UpperBoundTypes.AbsoluteValue
      : UpperBoundTypes.Model;
    this.changeLowerBound(defaultBoundType);
    this.changeUpperBound(defaultBoundType);
    this.customWatchForm.patchValue({ upperControl: defaultBoundType });
    this.customWatchForm.patchValue({ lowerControl: defaultBoundType });
    const actionNote =
      models?.length > 1 ? '' : this.createActionNote(models[0]);
    this.newDiagnoseForm.patchValue({ diagnoseNote: actionNote });
    this.newMaintenanceForm.patchValue({ maintenanceNote: actionNote });
  }

  public openOpModeConfig() {
    this.logger.trackAppInsightsEvent('ModelConfig:ActionPane:OPMode', {
      button: 'op-mode',
    });

    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        this.modelActionChange.emit({
          event: 'OpenOpModeConfig',
          newValue: model?.AssetGUID,
        });
      });
  }

  public openDataExplorer() {
    this.logger.trackAppInsightsEvent('ModelConfig:ActionPane:DataExplorer', {
      button: 'data-explorer',
    });

    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        this.modelActionChange.emit({
          event: 'OpenDataExplorer',
          newValue: model?.AssetGUID,
        });
      });
  }

  public openDiagnosticDrilldown() {
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        if (model) {
          const data = {
            assetGuid: model?.AssetGUID,
            model: model?.ModelExtID,
            mtype: model?.ModelTypeAbbrev,
            externalUrl: model?.DiagnosticDrilldownURL,
          };
          if (model?.ModelTypeAbbrev === 'External') {
            this.toastService.openSnackBar(
              'Unable to open Diagnostic Drilldown for models with External model type.',
              'warning'
            );
            return;
          }

          this.logger.trackAppInsightsEvent(
            'ModelConfig:ActionPane:DiagnosticDrilldown',
            {
              button: 'diagnostic-drilldown',
            }
          );

          this.modelActionChange.emit({
            event: 'OpenDiagnosticDrilldown',
            newValue: data,
          });
        }
      });
  }

  public showRelatedIssues() {
    this.logger.trackAppInsightsEvent('ModelConfig:ActionPane:RelatedIssues', {
      button: 'issues',
    });
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        this.modelActionChange.emit({
          event: 'ShowRelatedIssues',
          newValue: model?.AssetGUID,
        });
      });
  }

  public showRelatedAlerts() {
    this.logger.trackAppInsightsEvent('ModelConfig:ActionPane:RelatedModels', {
      button: 'alerts',
    });
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        this.modelActionChange.emit({
          event: 'ShowRelatedModels',
          newValue: model?.AssetGUID,
        });
      });
  }

  public createIssue() {
    this.logger.trackAppInsightsEvent('ModelConfig:ActionPane:CreateIssue', {
      button: 'issues',
    });
    this.modelActionsFacade.query.selectedModel$
      .pipe(take(1))
      .subscribe((model) => {
        this.modelActionChange.emit({
          event: 'NewIssue',
          newValue: model?.AssetGUID,
        });
      });
  }
}
