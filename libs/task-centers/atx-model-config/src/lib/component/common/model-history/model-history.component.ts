/* eslint-disable @typescript-eslint/no-empty-function */
import { Component } from '@angular/core';
import { IModelHistoryEventStateChange } from '@atonix/atx-chart-v2';
import { NavFacade } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import { ModelEditFacade } from '../../../service/model-edit.facade';
import { selectTimeSliderAsOfDate } from '../../../store/reducers/model-config.reducer';
import {
  ModelConfigBusEventTypes,
  ModelConfigEventBus,
} from '../../../store/services/model-config-event-bus';

@Component({
  selector: 'atx-model-edit-tabs-model-history',
  templateUrl: './model-history.component.html',
  styleUrls: ['./model-history.component.scss'],
})
export class ModelEditTabsModelHistoryComponent {
  readonly vm$ = this.modelEditFacade.query.vm$;
  readonly timeSliderAsOfDate$ = this.store.select(selectTimeSliderAsOfDate);
  constructor(
    private modelEditFacade: ModelEditFacade,
    private readonly store: Store,
    private logger: LoggerService,
    public navFacade: NavFacade,
    public modelConfigEventBus: ModelConfigEventBus
  ) {
    this.logger.tabClicked('History', 'Model Config');

    this.modelConfigEventBus.on(
      ModelConfigBusEventTypes.MODEL_ACTION_STATE_CHANGE,
      this.reloadGrid.bind(this)
    );
  }

  onModelHistoryEventChange(eventChange: IModelHistoryEventStateChange) {
    if (eventChange.event === 'setfavorite') {
      this.modelEditFacade.modelHistorySetFavoriteAction(eventChange);
    }

    if (eventChange.event === 'setnote') {
      this.modelEditFacade.modelHistorySetNoteAction(eventChange);
    }
  }

  resetModelHistoryGridState() {
    this.modelEditFacade.command.setModelHistoryReload(false);
  }

  reloadGrid(data: any) {
    this.modelEditFacade.command.setModelHistoryReload(true);
  }
}
