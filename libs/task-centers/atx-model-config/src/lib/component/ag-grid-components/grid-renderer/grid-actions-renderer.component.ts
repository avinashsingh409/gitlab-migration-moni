import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-grid-actions-renderer',
  template: `
    <img
      class="grid-icon"
      [title]="tooltip"
      [ngClass]="{ 'active-icon': active }"
      [src]="imageSrc"
    />
  `,
  styleUrls: ['./grid-actions-renderer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridActionsRendererComponent implements ICellRendererAngularComp {
  tooltip = '';
  active = false;
  imageSrc = '';

  agInit(params: ICellRendererParams & { tooltip: string }) {
    this.tooltip = params?.tooltip ?? '';
    this.active = params?.value ?? false;
    this.imageSrc = './assets/emblem-maintainence.svg';
  }

  refresh(params: ICellRendererParams & { tooltip: string }): boolean {
    return true;
  }
}
