import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { ICellRendererParams } from '@ag-grid-enterprise/all-modules';
import { TagMapRecommendedType } from '@atonix/atx-core';

@Component({
  selector: 'atx-grid-icon-renderer',
  template: '<mat-icon [ngClass]="class" [svgIcon]="svgSrc" ></mat-icon>',
  styleUrls: ['./grid-icon-renderer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridIconRendererComponent implements ICellRendererAngularComp {
  svgSrc = '';
  class = 'grid-icon';

  agInit(params: ICellRendererParams & { type: string; class: string }) {
    if (params.type === 'active') {
      this.svgSrc = params?.value ? 'atx_active' : 'atx_inactive';
    } else if (params.type === 'standard') {
      this.svgSrc = params?.value ? 'atx_std-model' : 'atx_non-std';
    } else if (params.type === 'recommended') {
      switch (params?.value) {
        case TagMapRecommendedType.RecommendedSelected:
          this.svgSrc = 'recommended_selected';
          break;
        case TagMapRecommendedType.NotRecommendedSelected:
          this.svgSrc = 'not_recommended_selected';
          break;
        case TagMapRecommendedType.RecommendedNotSelected:
          this.svgSrc = 'recommended_not_selected';
          break;
        default:
          this.svgSrc = '';
          break;
      }
      this.class = params.class;
    }
  }

  refresh(params: ICellRendererParams & { type: string }): boolean {
    return true;
  }
}
