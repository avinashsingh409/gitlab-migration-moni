export function wholeNumberFormatter(params: any) {
  const input: number = params?.value ?? null;
  const output = Math.round(input);
  return String(output);
}

export function modelBoundValueFormatter(params: any) {
  return !isNaN(params?.value) && params?.value > 0
    ? String((+params.value * 100).toFixed(1) + '%')
    : '';
}
