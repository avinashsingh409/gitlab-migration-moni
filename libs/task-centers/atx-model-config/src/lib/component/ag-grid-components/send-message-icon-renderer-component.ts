import { Component, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { MatDialog } from '@angular/material/dialog';
import { AlertNotificationModalComponent } from '../model-edit/modals/alert-notification-modal/alert-notification-modal.component';
import { IModelConfigData } from '@atonix/shared/api';
import { ModelEditFacade } from '../../service/model-edit.facade';

@Component({
  selector: 'atx-send-message-icon-renderer',
  template: `<span (click)="navigate()"
    ><mat-icon color="accent" [ngClass]="{ disable: isEnabled === false }"
      >send</mat-icon
    ></span
  >`,
  styles: [
    `
      .disable {
        color: #666666 !important;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SendMessageIconRendererComponent
  implements ICellRendererAngularComp
{
  params: any;
  modelExtID = '';
  alertStatusTypeId = 0;
  isEnabled = false;
  data!: IModelConfigData;
  alertStatusTypeName = '';

  constructor(
    private modelEditFacade: ModelEditFacade,
    private dialog: MatDialog
  ) {}

  agInit(params: any) {
    this.isEnabled = params?.enableNotification;
    this.alertStatusTypeId = params?.alertStatusTypeId;

    const modelExtID = params?.data?.modelExtID;
    if (modelExtID) {
      this.modelExtID = modelExtID;
    }

    this.data = params?.data as IModelConfigData;
    this.alertStatusTypeName = params?.alertStatusTypeName;
  }

  refresh(params: any): boolean {
    this.isEnabled = params?.enableNotification;
    return true;
  }

  navigate() {
    const model = this.data as IModelConfigData;
    this.modelEditFacade.command.getAlertNotification({
      alertStatusTypeId: this.alertStatusTypeId,
      modelId: model.legacy.modelID,
      alertStatusTypeName: this.alertStatusTypeName,
      modelName: model.name,
      tagName: model.dependent.tagName,
      tagDescription: model.dependent.tagDesc,
      tagUnits: model.dependent.tagUnits,
      assetPath: model.dependent.assetPath,
    });

    const dialogRef = this.dialog.open(AlertNotificationModalComponent, {
      width: '1000px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((val) => {
      if (val) {
        this.modelEditFacade.command.reloadModels();
      }
    });
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(_?: IAfterGuiAttachedParams): void {}
}
