import { Observable } from 'rxjs';
import { IToolPanelParams } from '@ag-grid-enterprise/all-modules';
import { IToolPanelAngularComp } from '@ag-grid-community/angular';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { NavFacade } from '@atonix/atx-navigation';
import { ISavedIssueList } from '@atonix/shared/api';
import { map } from 'rxjs/operators';
import { ListConfigService } from '../../../store/services/list-config-service';

@Component({
  selector: 'atx-saved-config-panel',
  templateUrl: './saved-config-panel.component.html',
  styleUrls: ['./saved-config-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SavedConfigPanelComponent implements IToolPanelAngularComp {
  private params!: IToolPanelParams;
  public lists$!: Observable<ISavedIssueList[]>;
  public name = new UntypedFormControl('');
  public isLightTheme$: Observable<boolean>;

  agInit(params: IToolPanelParams): void {
    this.params = params;
  }

  constructor(
    private navFacade: NavFacade,
    private listConfig: ListConfigService
  ) {
    this.lists$ = listConfig.savedLists$;
    this.isLightTheme$ = this.navFacade.theme$.pipe(
      map((n) => {
        return n === 'light';
      })
    );
  }

  trackByFn(list: ISavedIssueList) {
    return list.id;
  }

  saveList() {
    if (this.name.value) {
      this.listConfig.saveList(this.name.value);
      this.name.setValue('');
    }
  }

  loadDefault() {
    this.listConfig.resetToDefaultList();
  }

  loadList(list: ISavedIssueList) {
    if (list) {
      this.listConfig.loadList(list.id);
    }
  }

  delete(list: ISavedIssueList) {
    if (list) {
      if (confirm('Are you sure you want to delete this saved view?')) {
        this.listConfig.deleteList(list.id);
      }
    }
  }
}
