import { Injectable } from '@angular/core';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { Store } from '@ngrx/store';
import * as actions from '../actions/model-config.actions';
import * as opModeActions from '../actions/op-mode.actions';
import * as tagListActions from '../actions/tag-list.actions';
import * as timeSliderActions from '../../store/actions/time-slider.actions';
import * as modelListActions from '../../store/actions/model-list.actions';
import {
  selectAssetTreeConfiguration,
  selectIsAboveUnitLevel,
  selectIsUnitLevel,
  selectIsUnitLevelAndBelow,
  selectLeftTrayMode,
  selectLeftTraySize,
  selectSelectedAsset,
  selectSelectedAssetId,
  selectMultipleModelsSelected,
  selectEditMultipleModelsEnabled,
  selectSelectedAssetGuid,
  selectSelectedModels,
  selectHasSelectedModels,
  selectTimeSlider,
  selectSelectedModelIds,
  selectSelectedTag,
  selectHasSelectedTag,
  selectOpModesListLoading,
  selectRestrictCreateNewOpMode,
  selectDefaultOpMode,
} from '../reducers/model-config.reducer';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { OPModeTypes } from '@atonix/shared/api';

@Injectable()
export class ModelConfigFacade {
  constructor(private store: Store) {}

  selectedAsset$ = this.store.select(selectSelectedAsset);
  selectedAssetId$ = this.store.select(selectSelectedAssetId);
  selectedAssetGuidId$ = this.store.select(selectSelectedAssetGuid);
  assetTreeConfiguration$ = this.store.select(selectAssetTreeConfiguration);
  leftTrayMode$ = this.store.select(selectLeftTrayMode);
  leftTraySize$ = this.store.select(selectLeftTraySize);

  isAboveUnitLevel$ = this.store.select(selectIsAboveUnitLevel);
  isUnitLevel$ = this.store.select(selectIsUnitLevel);
  isUnitLevelAndBelow$ = this.store.select(selectIsUnitLevelAndBelow);
  multipleModelsSelected$ = this.store.select(selectMultipleModelsSelected);
  editMultipleModelsEnabled$ = this.store.select(
    selectEditMultipleModelsEnabled
  );
  selectedModels$ = this.store.select(selectSelectedModels);
  hasSelectedModels$ = this.store.select(selectHasSelectedModels);
  selectedModelIds$ = this.store.select(selectSelectedModelIds);
  selectedTag$ = this.store.select(selectSelectedTag);
  hasSelectedTag$ = this.store.select(selectHasSelectedTag);

  timeSliderState$ = this.store.select(selectTimeSlider);
  opModeListLoading$ = this.store.select(selectOpModesListLoading);
  defaultOpMode$ = this.store.select(selectDefaultOpMode);
  restrictCreateNewOpMode$ = this.store.select(selectRestrictCreateNewOpMode);

  getModelList() {
    this.store.dispatch(modelListActions.getModelList());
  }

  setDefaultOpMode(opMode: OPModeTypes) {
    this.store.dispatch(opModeActions.setDefaultOpMode({ opMode }));
  }

  selectAsset(asset: string) {
    this.store.dispatch(actions.selectAsset({ asset }));
    this.clearTags();
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.treeStateChange(change));
  }
  treeSizeChange(value: number) {
    this.store.dispatch(actions.treeSizeChange({ value }));
  }
  treeDragDrop(canDrag: boolean) {
    this.store.dispatch(actions.setAssetTreeDragDrop({ canDrag }));
  }

  systemInitialize(start: Date, end: Date, asset?: string) {
    this.store.dispatch(actions.systemInitialize({ start, end, asset }));
  }

  modelConfigActionsOpenOpMode(
    assetGuid: string,
    opModeDefinitionExtID?: string
  ) {
    this.store.dispatch(
      actions.modelConfigActionsOpenOpMode({ assetGuid, opModeDefinitionExtID })
    );
  }

  modelConfigActionsOpenDataExplorer(assetGuid: string) {
    this.store.dispatch(
      actions.modelConfigActionsOpenDataExplorer({ assetGuid })
    );
  }

  modelConfigActionsOpenDiagnosticDrillDown(
    assetGuid: string,
    model: string,
    start: string,
    end: string,
    mtype: string,
    ddurl: string
  ) {
    this.store.dispatch(
      actions.modelConfigActionsOpenDiagnosticDrilldown({
        assetGuid,
        model,
        start,
        end,
        mtype,
        ddurl,
      })
    );
  }

  modelConfigActionsShowRelatedIssues(assetGuid: string) {
    this.store.dispatch(
      actions.modelConfigActionsShowRelatedIssues({ assetGuid })
    );
  }

  modelConfigActionsShowRelatedAlerts(assetGuid: string) {
    this.store.dispatch(
      actions.modelConfigActionsShowRelatedAlerts({ assetGuid })
    );
  }

  modelConfigActionsCreateNewIssue(assetGuid: string) {
    this.store.dispatch(
      actions.modelConfigActionsCreateNewIssue({ assetGuid })
    );
  }

  timeSliderStateChange(stateChange: ITimeSliderStateChange) {
    this.store.dispatch(
      timeSliderActions.timeSliderStateChange({ stateChange })
    );
  }

  updateRouteTimeSlider(start: Date, end: Date) {
    this.store.dispatch(
      timeSliderActions.updateRouteTimeSlider({ start, end })
    );
  }

  resetTagSelection() {
    this.store.dispatch(
      tagListActions.tagListSelectionChange({ selectedTag: null })
    );
  }

  createNewModel() {
    this.store.dispatch(actions.modelConfigCreateNewModel());
  }

  private clearTags() {
    this.store.dispatch(tagListActions.clearTags());
  }
}
