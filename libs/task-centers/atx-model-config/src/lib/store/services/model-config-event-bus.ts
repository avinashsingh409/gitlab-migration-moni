import { filter, map } from 'rxjs/operators';
import { Subject, Subscription, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

export const enum ModelConfigBusEventTypes {
  RESET_TO_DEFAULTS,
  LOAD_SAVED_CONFIG,
  MODEL_EDIT_ASSET_TREE_DROPDOWN_STATE_CHANGE,
  MODEL_ACTION_STATE_CHANGE,
}

export class ModelConfigEmitEvent {
  constructor(public type: ModelConfigBusEventTypes, public data?: any) {}
}

@Injectable()
export class ModelConfigEventBus {
  private emitter = new Subject<ModelConfigEmitEvent>();

  emit(event: ModelConfigEmitEvent) {
    this.emitter.next(event);
  }

  on(
    event: ModelConfigBusEventTypes,
    notify: (data: any) => void
  ): Subscription | Observable<any> {
    const watch$ = this.emitter.pipe(
      filter((e: ModelConfigEmitEvent) => e.type === event),
      map((e: ModelConfigEmitEvent) => e.data)
    );

    return !notify ? watch$ : watch$.subscribe(notify);
  }
}
