/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable, OnDestroy } from '@angular/core';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { Store } from '@ngrx/store';
import * as modelListActions from '../../store/actions/model-list.actions';
import { BehaviorSubject, forkJoin, of, Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
export interface IDeleteModelState {
  isDeleting: boolean;
  deletionSuccessful: boolean;
  notDeletedModels: any[];
}

const _initialState: IDeleteModelState = {
  isDeleting: false,
  deletionSuccessful: false,
  notDeletedModels: [],
};

let _state: IDeleteModelState = _initialState;

@Injectable({ providedIn: 'root' })
export class DeleteModelsService implements OnDestroy {
  private store = new BehaviorSubject<IDeleteModelState>(_state);
  private state$ = this.store.asObservable();
  private onDestroy: Subject<void> = new Subject<void>();

  public isDeletingModels$ = this.state$.pipe(
    map((state) => state.isDeleting),
    distinctUntilChanged()
  );

  public deletionSuccessful$ = this.state$.pipe(
    map((state) => state.deletionSuccessful),
    distinctUntilChanged()
  );

  public notDeletedModels$ = this.state$.pipe(
    map((state) => state.notDeletedModels),
    distinctUntilChanged()
  );

  constructor(
    private alertsFrameworkService: AlertsFrameworkService,
    private ngrxstore: Store
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  public deleteModels(models: any[]) {
    this.updateState({ ..._state, isDeleting: true });

    const results = forkJoin(
      models.map((x) =>
        this.alertsFrameworkService.deleteModelsByModelId(x.modelId)
      )
    );

    results.pipe(takeUntil(this.onDestroy)).subscribe((result) => {
      // eslint-disable-next-line prefer-const
      let notDeletedModels: any[] = [];
      let successfulDeleteCount = 0;
      result.map((result) => {
        if (!result.isDeleted) {
          const model = models.find((x) => x.modelId === result.modelId);
          if (model) {
            notDeletedModels.push(model);
          }
        } else {
          successfulDeleteCount += 1;
        }
      });
      this.updateState({
        ..._state,
        isDeleting: false,
        deletionSuccessful: successfulDeleteCount > 0,
        notDeletedModels: notDeletedModels,
      });

      //sends a new GET Request to get updated model list
      //doing this here because this service isn't using ngrx and the getModelsList code is a ngrx Effect
      this.ngrxstore.dispatch(modelListActions.getModelList());
    });
  }

  public resetState() {
    this.updateState(_initialState);
  }

  private updateState(newState: IDeleteModelState) {
    this.store.next((_state = newState));
  }
}
