import { Injectable, OnDestroy } from '@angular/core';
import {
  AlertsFrameworkService,
  INDQuickDeployModelCreationResults,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  take,
  withLatestFrom,
} from 'rxjs/operators';
import { ModelConfigFacade } from '../facade/model-config.facade';

export interface IQuickDeployServiceState {
  qdModelCreationActionRes: INDQuickDeployModelCreationResults | null;
  showOverwriteDialog: boolean;
  loading: boolean;
  quickDeploySuccessful: boolean;
  assetId: number;
  assetGuid: string;
}

const _initialState: IQuickDeployServiceState = {
  qdModelCreationActionRes: null,
  showOverwriteDialog: false,
  loading: false,
  quickDeploySuccessful: false,
  assetId: -1,
  assetGuid: String(),
};

let _state: IQuickDeployServiceState = _initialState;

@Injectable()
export class QuickDeployServiceFacade implements OnDestroy {
  private store = new BehaviorSubject<IQuickDeployServiceState>(_state);
  private state$ = this.store.asObservable();
  private onDestroy: Subject<void> = new Subject<void>();

  constructor(
    private alertsFrameworkService: AlertsFrameworkService,
    private toastService: ToastService,
    private modelConfigFacade: ModelConfigFacade
  ) {}

  qdModelCreationActionResult$ = this.state$.pipe(
    map((state) => state.qdModelCreationActionRes),
    distinctUntilChanged()
  );
  showOverwriteDialog$ = this.state$.pipe(
    map((state) => state.showOverwriteDialog),
    distinctUntilChanged()
  );

  quickDeploySuccessful$ = this.state$.pipe(
    map((state) => state.quickDeploySuccessful),
    distinctUntilChanged()
  );

  isLoading$ = this.state$.pipe(
    map((state) => state.loading),
    distinctUntilChanged()
  );

  assetId$ = this.state$.pipe(
    map((state) => state.assetId),
    distinctUntilChanged()
  );

  assetGuid$ = this.state$.pipe(
    map((state) => state.assetGuid),
    distinctUntilChanged()
  );

  successStatWithAssetGuid$ = this.state$.pipe(
    map((state) => state.quickDeploySuccessful),
    distinctUntilChanged(),
    withLatestFrom(this.assetGuid$)
  );

  vm$: Observable<IQuickDeployServiceState> = combineLatest([
    this.qdModelCreationActionResult$,
    this.showOverwriteDialog$,
    this.isLoading$,
    this.quickDeploySuccessful$,
    this.assetId$,
    this.assetGuid$,
  ]).pipe(
    map(
      ([
        qdModelCreationActionRes,
        showOverwriteDialog,
        loading,
        quickDeploySuccessful,
        assetId,
        assetGuid,
      ]) => {
        return {
          qdModelCreationActionRes,
          showOverwriteDialog,
          loading,
          quickDeploySuccessful,
          assetId,
          assetGuid,
        } as IQuickDeployServiceState;
      }
    )
  );
  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  public init(assetId: number, assetGuid: string) {
    this.resetState();
    this.updateState({ ..._state, assetId: assetId, assetGuid: assetGuid });
  }
  public getModelsForQuickDeploy(opModeTypeID: number) {
    this.updateState({ ..._state, loading: true, showOverwriteDialog: false });
    this.assetId$.pipe(take(1)).subscribe((assetId) => {
      this.alertsFrameworkService
        .getAssetModelsForQuickDeploy(assetId, opModeTypeID)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe({
          next: (result) => {
            this.updateState({
              ..._state,
              qdModelCreationActionRes: result,
              loading: false,
            });

            if (result.GoodModels && result.GoodModels.length > 0) {
              if (result.OverrideModels && result.OverrideModels.length > 0) {
                this.updateState({
                  ..._state,
                  showOverwriteDialog: true,
                });
              } else {
                this.reconcileModels();
              }
            } else {
              this.toastService.openSnackBar(
                'No Quick Deploy Models.',
                'warning'
              );
            }
          },
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          error: (err) => {
            this.toastService.openSnackBar(err, 'error');
          },
        });
    });
  }

  public reconcileModels(modelIdsToOverwrite: string[] = []) {
    this.updateState({ ..._state, loading: true });
    this.qdModelCreationActionResult$.pipe(take(1)).subscribe((res) => {
      const actionResult = res as INDQuickDeployModelCreationResults;

      //ids of models to replace
      const modelIds = modelIdsToOverwrite.map((combinedIds) =>
        combinedIds.split('|').at(1)
      );

      //list of models to replace
      let modelsToReplace = actionResult.GoodModels.filter((model) => {
        return modelIds.includes(model.ModelExtID);
      });

      if (modelIdsToOverwrite.length === 0) {
        modelsToReplace = actionResult.GoodModels;
      }

      this.alertsFrameworkService
        .reconcileQuickDeployModels(
          actionResult.QDID,
          actionResult.QDStartTime,
          modelsToReplace,
          modelIdsToOverwrite,
          actionResult.QDAssetGUID,
          actionResult.QDAstVarTypeTagMapID,
          actionResult.OpModeTypeID
        )
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe({
          next: (val) => {
            if (val) {
              this.updateState({
                ..._state,
                loading: false,
                showOverwriteDialog: false,
                quickDeploySuccessful: true,
              });
              this.toastService.openSnackBar('Transport Successful', 'success');
              this.modelConfigFacade.getModelList(); //sends a new GET Request to get updated model list
              //doing this here because this service isn't using ngrx and the getModelsList code is a ngrx Effect
            } else {
              this.updateState({
                ..._state,
                loading: false,
                showOverwriteDialog: false,
                quickDeploySuccessful: false,
              });
              this.toastService.openSnackBar(
                'Error Quick Deploying Models',
                'error'
              );
            }
          },
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          error: (err) => {
            this.toastService.openSnackBar(err, 'error');
            this.resetState();
          },
        });
    });
  }

  public resetState() {
    this.updateState({
      ..._state,
      qdModelCreationActionRes: null,
      showOverwriteDialog: false,
      loading: false,
      quickDeploySuccessful: false,
    });
  }

  private updateState(newState: IQuickDeployServiceState) {
    this.store.next((_state = newState));
  }
}
