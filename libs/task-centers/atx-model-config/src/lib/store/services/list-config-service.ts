import {
  GridApi,
  AgGridEvent,
  ColumnApi,
} from '@ag-grid-enterprise/all-modules';
import { Injectable, OnDestroy } from '@angular/core';
import { isNil } from '@atonix/atx-core';
import {
  IListStateV2,
  ISavedModelConfigList,
  UIConfigFrameworkService,
} from '@atonix/shared/api';
import { produce } from 'immer';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import {
  ModelConfigBusEventTypes,
  ModelConfigEmitEvent,
  ModelConfigEventBus,
} from './model-config-event-bus';

export interface ListConfigState {
  listState: IListStateV2 | null;
  selectedListId: number | null;
  init: boolean;
  savedLists: ISavedModelConfigList[];
}

const _initialState: ListConfigState = {
  listState: null,
  selectedListId: null,
  init: false,
  savedLists: [],
};
let _state: ListConfigState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class ListConfigService implements OnDestroy {
  private listKey = '__ATX_MODEL_LIST__';
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<ListConfigState>(_state);
  private state$ = this.store.asObservable();
  private listState$ = this.state$.pipe(
    map((state) => state.listState),
    distinctUntilChanged()
  );
  public selectedListId$ = this.state$.pipe(
    map((state) => state.selectedListId),
    distinctUntilChanged()
  );
  private init$ = this.state$.pipe(
    map((state) => state.init),
    distinctUntilChanged()
  );
  public savedLists$ = this.state$.pipe(
    map((state) => state.savedLists),
    distinctUntilChanged()
  );

  vm$: Observable<ListConfigState> = combineLatest([
    this.listState$,
    this.selectedListId$,
    this.init$,
    this.savedLists$,
  ]).pipe(
    map(
      ([listState, selectedListId, init, savedLists]) =>
        ({
          listState,
          selectedListId,
          init,
          savedLists,
        } as ListConfigState)
    )
  );

  constructor(
    private uiConfigService: UIConfigFrameworkService,
    private modelConfigEventBus: ModelConfigEventBus
  ) {}

  getListState() {
    let result: IListStateV2 | null = null;
    if (window && window.sessionStorage) {
      const resultString = window.sessionStorage.getItem(this.listKey);
      if (
        resultString &&
        resultString.toLowerCase() !== 'undefined' &&
        resultString.toLowerCase() !== 'null'
      ) {
        result = JSON.parse(resultString) as IListStateV2;
      }
      if (result && result?.selectedListId) {
        this.updateState({ ..._state, selectedListId: result.selectedListId });
      }
    }
    return result;
  }

  setListState(listState: IListStateV2) {
    if (window && window.sessionStorage) {
      if (isNil(listState)) {
        window.sessionStorage.removeItem(this.listKey);
      } else {
        window.sessionStorage.setItem(this.listKey, JSON.stringify(listState));
      }
    }
  }

  updateCachedState(event: AgGridEvent) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (!vm.init) {
        return;
      }
      const sortState = event.columnApi
        .getColumnState()
        .filter((x: any) => !isNil(x.sort));
      const sortModels = sortState.map((state: any) => {
        return {
          colId: state.colId ?? '',
          sort: state.sort ?? '',
        };
      });

      const listState: IListStateV2 = {
        version: 1,
        columns: event.columnApi.getColumnState(),
        sort: sortModels,
        filter: event.api.getFilterModel(),
        groups: event.columnApi.getColumnGroupState(),
        selectedListId: vm.selectedListId,
      };
      const jsonListState = JSON.stringify(listState);
      const changed = JSON.stringify(vm.listState) !== jsonListState;
      if (changed) {
        const savedLists = produce(vm.savedLists, (state) => {
          state.forEach((savedList) => {
            savedList.checked = false;
          });
        });
        this.updateState({
          ..._state,
          listState,
          savedLists,
          selectedListId: null,
        });
        this.setListState(listState);
      }
    });
  }

  getModelConfigLists() {
    this.uiConfigService
      .getModelConfigLists('models')
      .pipe(take(1))
      .subscribe((lists) => {
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.vm$.pipe(take(1)).subscribe((vm) => {
          const saveListIndex = lists.findIndex((savedList) => {
            return savedList.id === vm.selectedListId;
          });
          if (saveListIndex !== -1) {
            lists[saveListIndex].checked = true;
          } else {
            const listState = this.getListState();
            if (listState && listState?.selectedListId) {
              const cacheListIndex = lists.findIndex((savedList) => {
                return savedList.id === listState.selectedListId;
              });
              if (cacheListIndex !== -1) {
                lists[cacheListIndex].checked = true;
              }
            }
          }
        });
        this.updateState({ ..._state, savedLists: lists });
      });
  }

  getCachedState(gridApi: GridApi, columnApi: ColumnApi) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const listState = vm.listState || this.getListState();
      this.updateState({
        ..._state,
        listState,
        init: true,
      });
      this.getModelConfigLists();
      if (listState) {
        if (listState.filter) {
          gridApi.setFilterModel(listState.filter);
        }
        if (listState.columns) {
          columnApi.applyColumnState({
            state: listState.columns,
            applyOrder: true,
          });
        }
        if (listState.groups) {
          columnApi.setColumnGroupState(listState.groups);
        }
        if (listState.sort) {
          columnApi.applyColumnState({ state: listState.sort });
        }
      } else {
        gridApi.setFilterModel({
          Active: {
            values: ['true'],
            filterType: 'set',
          },
        });
      }
    });
  }

  resetToDefaultList() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const savedLists = produce(vm.savedLists, (state) => {
        state.forEach((savedList) => {
          savedList.checked = false;
        });
      });
      this.updateState({ ..._state, savedLists, selectedListId: null });
      this.modelConfigEventBus.emit(
        new ModelConfigEmitEvent(ModelConfigBusEventTypes.RESET_TO_DEFAULTS, {})
      );
    });
  }

  loadList(listId: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.savedLists) {
        const saveListIndex = vm.savedLists.findIndex((savedList) => {
          return savedList.id === listId;
        });
        if (saveListIndex !== -1) {
          const savedLists = produce(vm.savedLists, (state) => {
            state.forEach((savedList) => {
              savedList.checked = savedList.id === listId ? true : false;
            });
          });
          let newListState = vm.savedLists[saveListIndex].state as IListStateV2;
          if (newListState) {
            newListState = { ...newListState, selectedListId: listId };
            this.setListState(newListState);
            this.updateState({
              ..._state,
              listState: newListState,
              savedLists,
              selectedListId: listId,
            });
            if (vm?.savedLists[saveListIndex]?.state) {
              this.modelConfigEventBus.emit(
                new ModelConfigEmitEvent(
                  ModelConfigBusEventTypes.LOAD_SAVED_CONFIG,
                  {
                    listState: newListState,
                  }
                )
              );
            }
          }
        }
      }
    });
  }

  deleteList(listId: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const saveListIndex = vm.savedLists.findIndex((savedList) => {
        return savedList.id === listId;
      });
      if (saveListIndex !== -1) {
        const savedLists = produce(vm.savedLists, (state) => {
          state.splice(saveListIndex, 1);
        });
        const selectedListId =
          vm?.selectedListId &&
          vm.selectedListId === vm.savedLists[saveListIndex].id
            ? null
            : vm.selectedListId;
        this.uiConfigService
          .deleteList(vm.savedLists[saveListIndex].id)
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((retVal) => {
            this.updateState({ ..._state, savedLists, selectedListId });
          });
      }
    });
  }

  saveList(name: string) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.listState) {
        this.uiConfigService
          .saveModelsList(name, vm.listState)
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((newModelEntry) => {
            if (newModelEntry) {
              // eslint-disable-next-line rxjs/no-nested-subscribe
              this.vm$.pipe(take(1)).subscribe((returnVm) => {
                const savedLists = produce(returnVm.savedLists, (state) => {
                  const newEntry: ISavedModelConfigList = {
                    id: newModelEntry.id,
                    name: newModelEntry.name,
                    state: newModelEntry.state,
                    checked: true,
                  };
                  state.push(newEntry);
                });
                const newState = produce(returnVm.listState, (state) => {
                  if (state?.selectedListId) {
                    state.selectedListId = newModelEntry.id;
                  }
                });
                if (returnVm?.listState) {
                  this.setListState(returnVm.listState);
                }
                this.updateState({
                  ..._state,
                  savedLists,
                  listState: newState,
                  selectedListId: newModelEntry.id,
                });
              });
            }
          });
      }
    });
  }

  private updateState(newState: ListConfigState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
