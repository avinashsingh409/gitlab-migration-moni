import { Injectable, OnDestroy } from '@angular/core';
import { IPDServer, IServerResult } from '@atonix/atx-core';
import {
  ImagesFrameworkService,
  ModelConfigCoreService,
  ProcessDataFrameworkService,
  ProcessDataCoreService,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';

export interface IUploadFileDialogState {
  isPdServer: boolean;
  asset: string;
  email: string;
  servers: IServerResult[];
}

const _initialState: IUploadFileDialogState = {
  isPdServer: false,
  asset: '',
  email: '',
  servers: [],
};
let _state: IUploadFileDialogState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class UploadFileDialogFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<IUploadFileDialogState>(_state);
  private state$ = this.store.asObservable();

  public isPdServer$ = this.state$.pipe(
    map((state) => state.isPdServer),
    distinctUntilChanged()
  );

  public asset$ = this.state$.pipe(
    map((state) => state.asset),
    distinctUntilChanged()
  );

  public email$ = this.state$.pipe(
    map((state) => state.email),
    distinctUntilChanged()
  );

  public servers$ = this.state$.pipe(
    map((state) => state.servers),
    distinctUntilChanged()
  );

  vm$: Observable<IUploadFileDialogState> = combineLatest([
    this.isPdServer$,
    this.asset$,
    this.email$,
    this.servers$,
  ]).pipe(
    map(
      ([isPdServer, asset, email, servers]) =>
        ({
          isPdServer,
          asset,
          email,
          servers,
        } as IUploadFileDialogState)
    )
  );

  constructor(
    private processDataFrameworkService: ProcessDataFrameworkService,
    private imagesFrameworkService: ImagesFrameworkService,
    private modelConfigCoreService: ModelConfigCoreService,
    private toastService: ToastService,
    private processDataCoreService: ProcessDataCoreService
  ) {}

  setAsset(assetGuid: string) {
    this.updateState({ ..._state, asset: assetGuid });
    this.getPDServers();
  }

  setEmail(email: string) {
    this.updateState({ ..._state, email });
  }

  getPDServers() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.updateState({ ..._state, servers: [] });
      this.processDataCoreService
        .getServers(vm.asset)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (servers) => {
            let isPdServer = false;
            if (
              servers &&
              servers.length > 0 &&
              servers[0].AssetId === vm.asset
            ) {
              isPdServer = true;
            }
            this.updateState({ ..._state, isPdServer, servers });
          },
          () =>
            this.toastService.openSnackBar(
              'Error retrieving PD Servers!',
              'error'
            )
        );
    });
  }

  uploadFile(serverID: number, file: File) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.imagesFrameworkService
        .getFileUrl(null, file.name, true, file.type, null, 'S3TransientBucket')
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (awsFile) =>
            this.imagesFrameworkService
              .uploadFile(awsFile.Url, file)
              .pipe(take(1))
              // eslint-disable-next-line rxjs/no-nested-subscribe
              .subscribe(
                (uploaded) =>
                  this.modelConfigCoreService
                    .multiModelJobProcessing(serverID, awsFile.ContentID)
                    .pipe(take(1))
                    // eslint-disable-next-line rxjs/no-nested-subscribe
                    .subscribe(
                      () =>
                        this.toastService.openSnackBar(
                          `A confirmation email will be sent to ${vm.email} when the import process is complete.`,
                          'success'
                        ),
                      () =>
                        this.toastService.openSnackBar(
                          'Error uploading file!',
                          'error'
                        )
                    ),
                () =>
                  this.toastService.openSnackBar(
                    'Error uploading file!',
                    'error'
                  )
              ),
          () => this.toastService.openSnackBar('Error uploading file!', 'error')
        );
    });
  }

  private updateState(newState: IUploadFileDialogState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
