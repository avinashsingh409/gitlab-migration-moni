import { isNil } from '@atonix/atx-core';
import { ETagType, IModelConfigTagListSummary } from '@atonix/shared/api';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/tag-list.actions';

export const tagListFeatureKey = 'tagList';

export interface TagListState extends EntityState<IModelConfigTagListSummary> {
  selectedTag: IModelConfigTagListSummary | null;
  totalModels: number;
  filteredColumns: any[];
  showTagsForSelectedAssetOnly: boolean;
  loading: boolean;
  errorMessage: string;
  dependentTags: IModelConfigTagListSummary[];
  inputTags: IModelConfigTagListSummary[];
  tagType: ETagType;
}

export const adapter: EntityAdapter<IModelConfigTagListSummary> =
  createEntityAdapter<IModelConfigTagListSummary>({
    selectId: (modelConfig: IModelConfigTagListSummary) => modelConfig.TagMapID,
  });

export const initialTagListState: TagListState = adapter.getInitialState({
  selectedTag: null,
  totalModels: 0,
  filteredColumns: [],
  showTagsForSelectedAssetOnly: false,
  loading: true,
  errorMessage: '',
  dependentTags: [],
  inputTags: [],
  tagType: ETagType.ModelInputTags,
});

export const reducer = createReducer(
  initialTagListState,
  on(actions.getTagList, (state, action): TagListState => {
    state = {
      ...state,
      loading: true,
      errorMessage: '',
      tagType: action.tagType,
    };
    return adapter.removeAll(state);
  }),
  on(actions.clearTags, (state, action): TagListState => {
    state = {
      ...state,
      dependentTags: [],
      inputTags: [],
    };
    return state;
  }),
  on(actions.getTagListSuccess, (state, action): TagListState => {
    state = {
      ...state,
      loading: false,
      errorMessage: '',
      dependentTags:
        action.tagType === ETagType.DependentTags
          ? action.tags
          : state.dependentTags,
      inputTags:
        action.tagType === ETagType.ModelInputTags
          ? action.tags
          : state.inputTags,
    };
    return adapter.addMany(action.tags, state);
  }),
  on(actions.getTagListFailure, (state, action): TagListState => {
    let errorMessage = 'Error: Cannot Display Tags';
    if (
      !isNil(action?.error) &&
      !isNil((action.error as any)?.Results) &&
      (action.error as any).Results.constructor === Array
    ) {
      const results = (action.error as any).Results;
      if (results?.length > 0) {
        if (results[0]?.Code === 'PayloadTooLarge') {
          errorMessage = 'Cannot Display At This Level';
        }
      }
    }

    if (action?.error) {
      const err = action?.error as any;
      if (err?.code === 'selected_asset_above_station') {
        errorMessage = 'Cannot Display At This Level';
      }
    }
    state = { ...state, loading: false, errorMessage };
    return state;
  }),
  on(actions.updateTotalModels, (state, action): TagListState => {
    return {
      ...state,
      totalModels: action.totalModels,
    };
  }),
  on(actions.setTagListFilters, (state, action): TagListState => {
    return {
      ...state,
      filteredColumns: action.filters,
    };
  }),
  on(actions.removeTagListFilter, (state, action): TagListState => {
    const filteredColumns = [...state.filteredColumns];
    const index = filteredColumns.indexOf(action.filter);
    if (index >= 0) {
      filteredColumns.splice(index, 1);
      action.callback(action.filter.Name);
    }
    return {
      ...state,
      filteredColumns,
    };
  }),
  on(actions.tagListPreferenceChange, (state, action): TagListState => {
    return {
      ...state,
      showTagsForSelectedAssetOnly: !state.showTagsForSelectedAssetOnly,
    };
  }),
  on(actions.tagListSelectionChange, (state, action): TagListState => {
    return {
      ...state,
      selectedTag: action.selectedTag,
    };
  })
);
export const { selectAll } = adapter.getSelectors();
export const selectedTag = (state: TagListState) => state.selectedTag;
export const totalModels = (state: TagListState) => state.totalModels;
export const filteredColumns = (state: TagListState) => state.filteredColumns;
export const showTagsForSelectedAssetOnly = (state: TagListState) =>
  state.showTagsForSelectedAssetOnly;
export const loading = (state: TagListState) => state.loading;
export const errorMessage = (state: TagListState) => state.errorMessage;
export const dependentTags = (state: TagListState) => state.dependentTags;
export const inputTags = (state: TagListState) => state.inputTags;
export const tagType = (state: TagListState) => state.tagType;
