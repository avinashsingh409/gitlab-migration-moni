import { isNil } from '@atonix/atx-core';
import { IModelConfigSummary } from '@atonix/shared/api';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/model-list.actions';

export const modelListFeatureKey = 'modelList';

export interface ModelListState extends EntityState<IModelConfigSummary> {
  totalModels: number;
  filteredColumns: any[];
  showModelsForSelectedAssetOnly: boolean;
  loading: boolean;
  errorMessage: string;
  selectedModels: IModelConfigSummary[];
  collapseBottomSection: boolean;
}

export const adapter: EntityAdapter<IModelConfigSummary> =
  createEntityAdapter<IModelConfigSummary>({
    selectId: (modelConfig: IModelConfigSummary) => modelConfig.ModelID,
  });

export const initialModelListState: ModelListState = adapter.getInitialState({
  totalModels: 0,
  filteredColumns: [],
  selectedModels: [],
  showModelsForSelectedAssetOnly: false,
  loading: true,
  errorMessage: '',
  collapseBottomSection: false,
});

export const reducer = createReducer(
  initialModelListState,
  on(actions.getModelList, (state, action): ModelListState => {
    state = {
      ...state,
      loading: true,
      errorMessage: '',
    };
    return adapter.removeAll(state);
  }),
  on(actions.getModelListSuccess, (state, action): ModelListState => {
    state = {
      ...state,
      loading: false,
      errorMessage: '',
    };
    return adapter.addMany(action.modelConfigSummary, state);
  }),
  on(actions.getModelListFailure, (state, action): ModelListState => {
    let errorMessage = 'Error: Cannot Display Models';
    if (
      !isNil(action?.error) &&
      !isNil((action.error as any)?.Results) &&
      (action.error as any).Results.constructor === Array
    ) {
      const results = (action.error as any).Results;
      if (results?.length > 0) {
        if (results[0]?.Code === 'PayloadTooLarge') {
          errorMessage = 'Cannot Display At This Level';
        }
      }
    }
    state = { ...state, loading: false, errorMessage };
    return state;
  }),
  on(actions.updateTotalModels, (state, action): ModelListState => {
    return {
      ...state,
      totalModels: action.totalModels,
    };
  }),
  on(actions.setModelListFilters, (state, action): ModelListState => {
    return {
      ...state,
      filteredColumns: action.filters,
    };
  }),
  on(actions.removeModelListFilter, (state, action): ModelListState => {
    const filteredColumns = [...state.filteredColumns];
    const index = filteredColumns.indexOf(action.filter);
    if (index >= 0) {
      filteredColumns.splice(index, 1);
      action.callback(action.filter.Name);
    }
    return {
      ...state,
      filteredColumns,
    };
  }),
  on(actions.modelListPreferenceChange, (state, action): ModelListState => {
    return {
      ...state,
      showModelsForSelectedAssetOnly: !state.showModelsForSelectedAssetOnly,
    };
  }),
  on(actions.modelListSelectionChange, (state, action): ModelListState => {
    return {
      ...state,
      selectedModels: action.selectedModels,
    };
  }),
  on(actions.collapseBottomSection, (state, action): ModelListState => {
    return {
      ...state,
      collapseBottomSection: !state.collapseBottomSection,
    };
  })
);
export const { selectAll } = adapter.getSelectors();
export const selectedModelGuids = (state: ModelListState) =>
  state.selectedModels?.map((model) => model.ModelExtID);
export const selectedModels = (state: ModelListState) => state.selectedModels;
export const selectedModelAssetGuid = (state: ModelListState) =>
  state.selectedModels[0].AssetGUID;
export const totalModels = (state: ModelListState) => state.totalModels;
export const filteredColumns = (state: ModelListState) => state.filteredColumns;
export const showModelsForSelectedAssetOnly = (state: ModelListState) =>
  state.showModelsForSelectedAssetOnly;
export const loading = (state: ModelListState) => state.loading;
export const errorMessage = (state: ModelListState) => state.errorMessage;
export const collapseBottomSection = (state: ModelListState) =>
  state.collapseBottomSection;
