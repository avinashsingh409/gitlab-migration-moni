import {
  alterTimeSliderState,
  getDefaultTimeSlider,
  ITimeSliderState,
} from '@atonix/atx-time-slider';
import { createReducer, createSelector, on } from '@ngrx/store';
import * as actions from '../actions/time-slider.actions';
export const timeSliderFeatureKey = 'timeSlider';

export interface TimeSliderState {
  timeSliderState: ITimeSliderState | null;
}

export const initialTimeSliderState: TimeSliderState = {
  timeSliderState: null,
};

export const reducer = createReducer(
  initialTimeSliderState,
  on(actions.initializeTimeSliderState, (state, payload): TimeSliderState => {
    return {
      ...state,
      timeSliderState: getDefaultTimeSlider({
        dateIndicator: 'Range',
        startDate: payload.start,
        endDate: payload.end,
      }),
    };
  }),
  on(actions.timeSliderStateChange, (state, payload): TimeSliderState => {
    if (state?.timeSliderState) {
      return {
        ...state,
        timeSliderState: alterTimeSliderState(
          state.timeSliderState,
          payload?.stateChange
        ),
      };
    } else {
      return {
        ...state,
        timeSliderState: null,
      };
    }
  })
);
