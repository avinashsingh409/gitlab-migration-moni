import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';
import * as fromAssetNav from './asset-nav.reducer';
import * as fromModelList from './model-list.reducer';
import * as fromTagList from './tag-list.reducer';
import * as fromModelEdit from './model-edit.reducer';
import * as fromTimeSlider from './time-slider.reducer';
import * as fromOpModes from './op-mode.reducer';
import { INavigationState } from '@atonix/atx-navigation';
import { ETagType } from '@atonix/shared/api';

export const modelConfigFeatureKey = 'model-config';

export interface ModelConfigState {
  [fromAssetNav.assetNavFeatureKey]: fromAssetNav.AssetNavState;
  [fromModelList.modelListFeatureKey]: fromModelList.ModelListState;
  [fromTagList.tagListFeatureKey]: fromTagList.TagListState;
  [fromModelEdit.modelEditFeatureKey]: fromModelEdit.ModelEditState;
  [fromTimeSlider.timeSliderFeatureKey]: fromTimeSlider.TimeSliderState;
  [fromOpModes.opModeListFeatureKey]: fromOpModes.OpModeListState;
}

export function reducers(state: ModelConfigState | undefined, action: Action) {
  return combineReducers({
    [fromAssetNav.assetNavFeatureKey]: fromAssetNav.reducer,
    [fromModelList.modelListFeatureKey]: fromModelList.reducer,
    [fromTagList.tagListFeatureKey]: fromTagList.reducer,
    [fromModelEdit.modelEditFeatureKey]: fromModelEdit.reducer,
    [fromTimeSlider.timeSliderFeatureKey]: fromTimeSlider.reducer,
    [fromOpModes.opModeListFeatureKey]: fromOpModes.reducer,
  })(state, action);
}

//** */
// Selectors
//** */
const selectNavigationState = createFeatureSelector<INavigationState>('nav');
export const selectUser = createSelector(
  selectNavigationState,
  (state) => state.user
);

const selectModelConfigState = createFeatureSelector<ModelConfigState>(
  modelConfigFeatureKey
);
export const selectAssetNavState = createSelector(
  selectModelConfigState,
  (state) => state.assetNav
);
export const selectModelListState = createSelector(
  selectModelConfigState,
  (state) => state.modelList
);
export const selectTagListState = createSelector(
  selectModelConfigState,
  (state) => state.tagList
);

export const selectModelEditState = createSelector(
  selectModelConfigState,
  (state) => state.modelEdit
);

export const selectOpModesState = createSelector(
  selectModelConfigState,
  (state) => state.opModeList
);

//** */
// Asset Nav Selectors
//** */
export const selectLeftTraySize = createSelector(
  selectAssetNavState,
  fromAssetNav.leftTraySize
);

export const selectAssetTreeState = createSelector(
  selectAssetNavState,
  fromAssetNav.treeState
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetNavState,
  fromAssetNav.treeConfiguration
);

export const selectAssetTreeConfigNodes = createSelector(
  selectAssetTreeConfiguration,
  (state) => state.nodes
);

export const selectLeftTrayMode = createSelector(
  selectAssetTreeConfiguration,
  (state) => {
    const result: TrayState = state?.pin ? 'side' : 'over';
    return result;
  }
);

export const selectSelectedAsset = createSelector(
  selectAssetTreeState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
);

export const selectAssetTreeNodes = createSelector(
  selectAssetTreeState,
  (state) => state.treeNodes
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  selectSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const selectSelectedAssetGuid = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data.AssetGuid
);

export const selectSelectedAssetIds = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetUniqueKey: data.UniqueKey, assetId: data.AssetId };
    }
    return null;
  }
);

export const selectSelectedAssetId = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data.AssetId
);

// Time Slider
export const selectTimeSliderState = createSelector(
  selectModelConfigState,
  (state) => state.timeSlider
);
export const selectTimeSlider = createSelector(
  selectTimeSliderState,
  (state) => state.timeSliderState
);
export const selectTimeSliderSelection = createSelector(
  selectTimeSlider,
  (state) => {
    return { start: state?.startDate, end: state?.endDate };
  }
);

export const selectTimeSliderAsOfDate = createSelector(
  selectTimeSlider,
  (state) => state?.asOfDate
);

//** */
// Model List View Selectors
//** */
export const selectAllModels = createSelector(
  selectModelListState,
  fromModelList.selectAll
);

export const selectAllModelIdAndGuid = createSelector(
  selectAllModels,
  (state) =>
    state.map((model) => {
      return {
        modelId: model.ModelID,
        modelExtId: model.ModelExtID,
      };
    })
);

export const selectSelectedModelGuids = createSelector(
  selectModelListState,
  fromModelList.selectedModelGuids
);

export const selectSelectedModels = createSelector(
  selectModelListState,
  fromModelList.selectedModels
);

export const selectSelectedModelIds = createSelector(
  selectSelectedModels,
  (state) => state.map((model) => model.ModelID.toString())
);

export const selectSelectedModelAssetGuid = createSelector(
  selectModelListState,
  fromModelList.selectedModelAssetGuid
);

export const selectHasSelectedModels = createSelector(
  selectSelectedModels,
  (state) => state?.length > 0
);

export const selectModelListTotalModels = createSelector(
  selectModelListState,
  fromModelList.totalModels
);

export const selectModelListFilteredColumns = createSelector(
  selectModelListState,
  fromModelList.filteredColumns
);

export const selectShowModelsForSelectedAssetOnly = createSelector(
  selectModelListState,
  fromModelList.showModelsForSelectedAssetOnly
);

export const selectModelListLoading = createSelector(
  selectModelListState,
  fromModelList.loading
);

export const selectModelListErrorMessage = createSelector(
  selectModelListState,
  fromModelList.errorMessage
);

export const selectCollapseBottomSection = createSelector(
  selectModelListState,
  fromModelList.collapseBottomSection
);

export const selectMultipleModelsSelected = createSelector(
  selectSelectedModels,
  (state) => state?.length > 1
);

export const selectEditMultipleModelsEnabled = createSelector(
  selectSelectedModels,
  (state) => {
    if (state?.length > 1 && state?.length <= 20) {
      const type = state[0].ModelTypeID;
      return state.findIndex((model) => model.ModelTypeID !== type) === -1;
    }
    return false;
  }
);

export const selectAllModelsForSelectedAssetOnly = createSelector(
  selectSelectedAssetTreeNode,
  selectAllModels,
  (node, models) =>
    models?.filter((model) => model.AssetID === node?.data?.AssetId)
);

export const selectModelListModels = createSelector(
  [
    selectShowModelsForSelectedAssetOnly,
    selectAllModelsForSelectedAssetOnly,
    selectAllModels,
  ],
  (showModelsForSelectedAssetOnly, modelsForSelectedAssetOnly, allModels) => {
    if (showModelsForSelectedAssetOnly) {
      return modelsForSelectedAssetOnly;
    }
    return allModels;
  }
);

export const selectModelListLoadingViewModel = createSelector(
  selectModelListLoading,
  selectModelListErrorMessage,
  (loading, errorMessage) => ({ loading, errorMessage })
);

export const selectModelListViewModel = createSelector(
  selectModelListTotalModels,
  selectModelListFilteredColumns,
  selectShowModelsForSelectedAssetOnly,
  selectModelListLoading,
  selectModelListErrorMessage,
  selectSelectedModelGuids,
  selectSelectedModels,
  selectTimeSliderSelection,
  selectCollapseBottomSection,
  (
    totalModels,
    filteredColumns,
    showModelsForSelectedAssetOnly,
    loading,
    errorMessage,
    selectedModelGuids,
    selectedModels,
    timeSliderSelection,
    collapseBottomSection
  ) => ({
    totalModels,
    filteredColumns,
    showModelsForSelectedAssetOnly,
    loading,
    errorMessage,
    selectedModelGuids,
    selectedModels,
    timeSliderSelection,
    collapseBottomSection,
  })
);

//** */
// Tag List View Selectors
//** */
export const selectAllTags = createSelector(
  selectTagListState,
  fromTagList.selectAll
);

export const selectTagListTotalModels = createSelector(
  selectTagListState,
  fromTagList.totalModels
);

export const selectTagListFilteredColumns = createSelector(
  selectTagListState,
  fromTagList.filteredColumns
);

export const selectShowTagsForSelectedAssetOnly = createSelector(
  selectTagListState,
  fromTagList.showTagsForSelectedAssetOnly
);

export const selectTagListLoading = createSelector(
  selectTagListState,
  fromTagList.loading
);

export const selectTagListErrorMessage = createSelector(
  selectTagListState,
  fromTagList.errorMessage
);

export const selectSelectedTagType = createSelector(
  selectTagListState,
  fromTagList.tagType
);

export const selectDependentTags = createSelector(
  selectTagListState,
  fromTagList.dependentTags
);

export const selectInputTags = createSelector(
  selectTagListState,
  fromTagList.inputTags
);

export const selectAllTagsByType = createSelector(
  selectSelectedTagType,
  selectDependentTags,
  selectInputTags,
  (tagType, dependentTags, inputTags) =>
    tagType === ETagType.ModelInputTags ? inputTags : dependentTags
);

export const selectAllTagsForSelectedAssetOnly = createSelector(
  selectSelectedAssetTreeNode,
  selectAllTagsByType,
  (node, tags) => tags?.filter((tag) => tag.AssetID === node?.data?.AssetId)
);

export const selectTagListTags = createSelector(
  [
    selectShowTagsForSelectedAssetOnly,
    selectAllTagsForSelectedAssetOnly,
    selectAllTagsByType,
  ],
  (showTagsForSelectedAssetOnly, tagsForSelectedAssetOnly, allTags) => {
    if (showTagsForSelectedAssetOnly) {
      return tagsForSelectedAssetOnly;
    }
    return allTags;
  }
);

export const selectSelectedTag = createSelector(
  selectTagListState,
  fromTagList.selectedTag
);
export const selectHasSelectedTag = createSelector(
  selectSelectedTag,
  (selectedTag) => (selectedTag === null ? false : true)
);

export const selectTagListLoadingViewModel = createSelector(
  selectTagListLoading,
  selectTagListErrorMessage,
  (loading, errorMessage) => ({
    loading,
    errorMessage,
  })
);
export const selectTagListViewModel = createSelector(
  selectTagListTotalModels,
  selectTagListFilteredColumns,
  selectShowTagsForSelectedAssetOnly,
  selectTagListLoading,
  selectTagListErrorMessage,
  selectSelectedTag,
  selectHasSelectedTag,
  (
    totalModels,
    filteredColumns,
    showModelsForSelectedAssetOnly,
    loading,
    errorMessage,
    selectedTag,
    hasSelectedTag
  ) => ({
    totalModels,
    filteredColumns,
    showModelsForSelectedAssetOnly,
    loading,
    errorMessage,
    selectedTag,
    hasSelectedTag,
  })
);

export const selectIsAboveUnitLevel = createSelector(
  selectModelListState,
  (listState) => {
    return listState.errorMessage === 'Cannot Display At This Level';
  }
);

export const selectIsUnitLevel = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data?.Asset?.AssetTypeID === 4
);

export const selectIsUnitLevelAndBelow = createSelector(
  selectSelectedAssetTreeNode,
  (state) => state?.data?.Asset?.AssetTypeID >= 4
);

//** */
// Model Edit View Selectors
//** */
export const selectModelEditAssetTreeDropdownState = createSelector(
  selectModelEditState,
  fromModelEdit.assetTreeDropdownState
);

export const selectModelEditAssetTreeDropdownConfiguration = createSelector(
  selectModelEditAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const selectModelEditAssetTreeDropdownNode = createSelector(
  selectModelEditAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectModelEditAssetTreeDropdownAssetName = createSelector(
  selectModelEditAssetTreeDropdownNode,
  (state) => state?.nodeAbbrev
);

export const selectModelEditAssetTreeDropdownSelectedAssetIds = createSelector(
  selectModelEditAssetTreeDropdownNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetUniqueKey: data.UniqueKey, assetId: data.AssetId };
    }
    return null;
  }
);

//** */
// Op Mode List Selectors
//** */
export const selectOpModeList = createSelector(
  selectOpModesState,
  fromOpModes.selectAll
);

export const selectTotalOpModes = createSelector(
  selectOpModesState,
  fromOpModes.totalOpModes
);

export const selectOpModesFilteredColumns = createSelector(
  selectOpModesState,
  fromOpModes.filteredColumns
);

export const selectOpModesListLoading = createSelector(
  selectOpModesState,
  fromOpModes.loading
);
export const selectDefaultOpMode = createSelector(
  selectOpModesState,
  (state) => state.defaultOpMode
);

export const selectOpModesListErrorMessage = createSelector(
  selectOpModesState,
  fromOpModes.errorMessage
);

export const selectNewOpModeId = createSelector(
  selectOpModesState,
  fromOpModes.newOpModeId
);

export const selectRestrictCreateNewOpMode = createSelector(
  selectNewOpModeId,
  (id) => Number(id) <= -1
);

export const selectOpModeListLoadingViewModel = createSelector(
  selectOpModesListLoading,
  selectOpModesListErrorMessage,
  (loading, errorMessage) => ({ loading, errorMessage })
);

export const selectOpModetagListSidePanelAssetTreeState = createSelector(
  selectOpModesState,
  fromOpModes.tagListSidePanelAssetTreeState
);

export const selectOpModetagListSidePanelAssetTreeConfiguration =
  createSelector(
    selectOpModetagListSidePanelAssetTreeState,
    (state) => state.treeConfiguration
  );

export const selectOpModetagListSidePanelAssetNode = createSelector(
  selectOpModetagListSidePanelAssetTreeState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectOpModetagListSidePanelAssetName = createSelector(
  selectOpModetagListSidePanelAssetNode,
  (state) => state?.nodeAbbrev
);

export const selectOpModetagListSidePanelSelectedAssetIds = createSelector(
  selectOpModetagListSidePanelAssetNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return { assetUniqueKey: data.UniqueKey, assetId: data.AssetId };
    }
    return null;
  }
);

export const selectOpModeListViewModel = createSelector(
  selectOpModeList,
  selectTotalOpModes,
  selectOpModesFilteredColumns,
  selectOpModesListLoading,
  selectOpModesListErrorMessage,
  selectOpModetagListSidePanelAssetTreeState,
  selectOpModetagListSidePanelAssetTreeConfiguration,
  selectOpModetagListSidePanelAssetNode,
  selectOpModetagListSidePanelAssetName,
  selectOpModetagListSidePanelSelectedAssetIds,
  (
    opModeList,
    totalOpModes,
    opModesFilteredColums,
    loading,
    errorMessage,
    tagListSidePanelAssetTreeState,
    tagListSidePanelAssetTreeConfiguration,
    tagListAssetTreeSideNode,
    tagListSidePanelAssetTreeAssetName,
    tagListSidePanelAssetTreeSelectedAssetIds
  ) => ({
    opModeList,
    totalOpModes,
    opModesFilteredColums,
    loading,
    errorMessage,
    tagListSidePanelAssetTreeState,
    tagListSidePanelAssetTreeConfiguration,
    tagListAssetTreeSideNode,
    tagListSidePanelAssetTreeAssetName,
    tagListSidePanelAssetTreeSelectedAssetIds,
  })
);
