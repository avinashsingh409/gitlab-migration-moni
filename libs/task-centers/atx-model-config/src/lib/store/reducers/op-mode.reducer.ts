import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
  treeRetrievedStateChange,
  alterAssetTreeState,
} from '@atonix/atx-asset-tree';
import { isNil } from '@atonix/atx-core';
import {
  createEntityAdapter,
  EntityAdapter,
  EntityState,
  Update,
} from '@ngrx/entity';
import { createReducer, createSelector, on } from '@ngrx/store';
import { OpModeSummary } from '../../service/model/op-mode';
import * as actions from '../actions/op-mode.actions';
import { OPModeTypes } from '@atonix/shared/api';

export const opModeListFeatureKey = 'opModeList';

export interface OpModeListState extends EntityState<OpModeSummary> {
  tagListSidePanelAssetTreeState: ITreeState;
  totalOpModes: number;
  filteredColumns: any[];
  loading: boolean;
  errorMessage: string;
  newOpModeId: number | null;
  defaultOpMode: OPModeTypes;
}

export const adapter: EntityAdapter<OpModeSummary> =
  createEntityAdapter<OpModeSummary>({
    selectId: (opMode: OpModeSummary) => opMode.opModeDefinitionID,
  });

export const initialOpModeListState: OpModeListState = adapter.getInitialState({
  selectedOpMode: null,
  totalOpModes: 0,
  filteredColumns: [],
  loading: false,
  errorMessage: '',
  newOpModeId: null,
  defaultOpMode: OPModeTypes.OOS,
  tagListSidePanelAssetTreeState: {
    treeConfiguration: getDefaultTree({
      collapseOthers: false,
      showTreeSelector: false,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
});

export const reducer = createReducer(
  initialOpModeListState,
  on(actions.createNewOpMode, (state, action): OpModeListState => {
    state = {
      ...state,
      loading: false,
      errorMessage: '',
      newOpModeId: -1,
    };
    return adapter.addOne(action.opModeSummary, state);
  }),
  on(actions.updateNewOpMode, (state, action): OpModeListState => {
    let newOpmode = state.entities[-1] as OpModeSummary;
    newOpmode = { ...action.opModeSummary };
    const updatedData = {
      id: -1,
      changes: {
        ...newOpmode,
      },
    } as Update<OpModeSummary>;
    state = {
      ...state,
      newOpModeId: newOpmode.opModeDefinitionID,
    };

    return adapter.updateOne(updatedData, state);
  }),
  on(actions.removeOpMode, (state, action): OpModeListState => {
    state = {
      ...state,
      newOpModeId: null,
    };
    return adapter.removeOne(action.opModeDefinitionId, state);
  }),
  on(actions.refreshOpMode, (state, action): OpModeListState => {
    const newOpmode = {
      opModeDefinitionTitle: action.opModeSummary.opModeDefinitionTitle,
      changeDate: action.opModeSummary.changeDate,
      opModeTypeAbbrev: action.opModeSummary.opModeTypeAbbrev,
      opModeTypeID: action.opModeSummary.opModeTypeID,
    };
    const updatedData = {
      id: action.opModeSummary.opModeDefinitionID,
      changes: {
        ...newOpmode,
      },
    } as Update<OpModeSummary>;
    state = {
      ...state,
      newOpModeId: null,
    };
    return adapter.updateOne(updatedData, state);
  }),
  on(actions.getOpModeList, (state, action): OpModeListState => {
    state = {
      ...state,
      loading: true,
      errorMessage: '',
    };
    return adapter.removeAll(state);
  }),
  on(actions.getOpModeListSuccess, (state, action): OpModeListState => {
    state = {
      ...state,
      loading: false,
      errorMessage: '',
      newOpModeId: null,
    };
    return adapter.addMany(action.opModes, state);
  }),
  on(actions.getOpModeListFailure, (state, action): OpModeListState => {
    let errorMessage = 'Error: Cannot Display Models';
    if (!isNil(action?.error) && !isNil((action.error as any)?.error?.code)) {
      const results = (action.error as any).error?.code;
      if (results === 'selected_asset_above_station') {
        errorMessage = 'Cannot Display At This Level';
      }
    }
    state = { ...state, loading: false, errorMessage };
    return state;
  }),
  on(actions.updateTotalOpModes, (state, action): OpModeListState => {
    state = {
      ...state,
      totalOpModes: action.totalOpModes,
    };
    return state;
  }),
  on(actions.setOpModeListFilters, (state, action): OpModeListState => {
    state = {
      ...state,
      filteredColumns: action.filters,
    };
    return state;
  }),
  on(actions.removeOpModeListFilter, (state, action): OpModeListState => {
    const filteredColumns = [...state.filteredColumns];
    const index = filteredColumns.indexOf(action.filter);
    if (index >= 0) {
      filteredColumns.splice(index, 1);
      action.callback(action.filter.Name);
    }
    return {
      ...state,
      filteredColumns,
    };
  }),
  on(
    actions.tagListSidePanelAssetTreeStateChange,
    (state, payload): OpModeListState => {
      const newPayload = treeRetrievedStateChange(payload.treeStateChange);
      return {
        ...state,
        tagListSidePanelAssetTreeState: alterAssetTreeState(
          state.tagListSidePanelAssetTreeState,
          newPayload
        ),
      };
    }
  ),
  on(actions.setDefaultOpMode, (state, payload): OpModeListState => {
    return {
      ...state,
      defaultOpMode: payload.opMode,
    };
  })
);

export const { selectAll } = adapter.getSelectors();
export const totalOpModes = (state: OpModeListState) => state.totalOpModes;
export const filteredColumns = (state: OpModeListState) =>
  state.filteredColumns;
export const newOpModeId = (state: OpModeListState) => state.newOpModeId;
export const loading = (state: OpModeListState) => state.loading;
export const errorMessage = (state: OpModeListState) => state.errorMessage;

export const tagListSidePanelAssetTreeState = (state: OpModeListState) =>
  state?.tagListSidePanelAssetTreeState;
