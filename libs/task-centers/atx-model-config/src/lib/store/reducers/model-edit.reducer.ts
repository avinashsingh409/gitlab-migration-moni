import {
  alterAssetTreeState,
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/model-edit.actions';

export const modelEditFeatureKey = 'modelEdit';

export interface ModelEditState {
  assetTreeDropdownState: ITreeState;
}

export const initialModelEditState: ModelEditState = {
  assetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
};

export const reducer = createReducer(
  initialModelEditState,
  on(actions.tagListAssetStateChange, (state, payload): ModelEditState => {
    const newPayload = treeRetrievedStateChange(payload.treeStateChange);
    return {
      ...state,
      assetTreeDropdownState: alterAssetTreeState(
        state.assetTreeDropdownState,
        newPayload
      ),
    };
  })
);

export const assetTreeDropdownState = (state: ModelEditState) =>
  state?.assetTreeDropdownState;
