import { createReducer, on } from '@ngrx/store';
import * as actions from '../actions/model-config.actions';
import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';

export const assetNavFeatureKey = 'assetNav';

export interface AssetNavState {
  TreeSize: number;
  AssetTreeState: ITreeState;
}

export const initialModelConfigState: AssetNavState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({
      pin: true,
      collapseOthers: false,
      canDrag: false,
    }),

    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  TreeSize: 250,
};

export const reducer = createReducer(
  initialModelConfigState,
  on(actions.permissionsRequest, (state): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: false,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.setAssetTreeDragDrop, (state, payload): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: {
        ...state.AssetTreeState.treeConfiguration,
        canDrag: payload.canDrag,
      },
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestSuccess, (state, payload): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: payload.canView,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.permissionsRequestFailure, (state): AssetNavState => {
    const treeState: ITreeState = {
      treeConfiguration: setPermissions(
        state.AssetTreeState.treeConfiguration,
        {
          canView: false,
          canEdit: false,
          canDelete: false,
          canAdd: false,
        }
      ),
      treeNodes: { ...state.AssetTreeState.treeNodes },
      hasDefaultSelectedAsset: state.AssetTreeState.hasDefaultSelectedAsset,
    };
    return { ...state, AssetTreeState: treeState };
  }),
  on(actions.treeStateChange, (state, payload): AssetNavState => {
    const newPayload = treeRetrievedStateChange(payload);
    return {
      ...state,
      AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
    };
  }),
  on(actions.treeSizeChange, (state, payload): AssetNavState => {
    return {
      ...state,
      TreeSize: payload.value,
    };
  })
);

export const leftTraySize = (state: AssetNavState) => state?.TreeSize;
export const treeState = (state: AssetNavState) => state?.AssetTreeState;
export const treeConfiguration = (state: AssetNavState) =>
  state?.AssetTreeState?.treeConfiguration;
