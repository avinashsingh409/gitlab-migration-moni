/* eslint-disable ngrx/avoid-cyclic-effects */
import { Injectable } from '@angular/core';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { noop, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import * as modelEditActions from '../actions/model-edit.actions';
import * as timeSliderActions from '../actions/time-slider.actions';

import { NavActions } from '@atonix/atx-navigation';
import { selectModelEditAssetTreeDropdownState } from '../reducers/model-config.reducer';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { dependentTags } from '../reducers/tag-list.reducer';

@Injectable()
export class ModelEditEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetTreeModel: AssetTreeModel
  ) {}

  initializeModelConfigEdit$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(modelEditActions.initializeModelConfigEdit),
      switchMap((action) =>
        // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
        [
          timeSliderActions.initializeTimeSliderState({
            start: action.start,
            end: action.end,
          }),
          NavActions.showTimeSlider(),
          NavActions.taskCenterLoad({
            taskCenterID: 'modelConfig',
            timeSlider: true,
            timeSliderOpened: true,
            assetTree: false,
            assetTreeOpened: false,
            asset: '',
          }),
        ]
      )
    );
  });

  tagListAssetStateChange$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(modelEditActions.tagListAssetStateChange),
      concatLatestFrom(() =>
        this.store.select(selectModelEditAssetTreeDropdownState)
      ),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(
            change.treeStateChange,
            state.treeConfiguration,
            state.treeNodes
          )
          .pipe(
            map((n) =>
              modelEditActions.tagListAssetStateChange({
                treeStateChange: n,
              })
            ),
            catchError((error: unknown) =>
              of(
                modelEditActions.tagListAssetRequestFailure({
                  error: error as any,
                })
              )
            )
          )
      )
    );
  });
}
