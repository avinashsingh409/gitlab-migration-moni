import { Inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  getAssetByAssetId,
  ModelService,
  selectAsset,
} from '@atonix/atx-asset-tree';
import { NavActions, selectNavAsset } from '@atonix/atx-navigation';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  catchError,
  concatMap,
  map,
  mergeMap,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import * as modelConfigActions from '../actions/model-config.actions';
import * as modelListActions from '../actions/model-list.actions';
import {
  selectAssetTreeState,
  selectSelectedAsset,
  selectAssetTreeConfigNodes,
  selectTimeSliderSelection,
  selectSelectedTag,
  selectSelectedModelGuids,
} from '../reducers/model-config.reducer';
import * as timeSliderActions from '../actions/time-slider.actions';
import { isNil } from '@atonix/atx-core';
import moment from 'moment';
import { AuthSelectors } from '@atonix/shared/state/auth';

@Injectable()
export class ModelConfigEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private assetTreeModelService: ModelService,
    private activatedRoute: ActivatedRoute,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private router: Router,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  initializeModelConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(modelConfigActions.systemInitialize),
      concatLatestFrom(() => [
        this.store.select(selectSelectedAsset),
        this.store.select(selectNavAsset),
      ]),
      map(([action, newAsset, appAsset]) => {
        return { asset: action.asset || appAsset || newAsset, action };
      }),
      // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
      switchMap((val) => {
        return [
          modelConfigActions.permissionsRequest(),
          timeSliderActions.initializeTimeSliderState({
            start: val.action.start,
            end: val.action.end,
          }),
          NavActions.showTimeSlider(),
          modelConfigActions.treeStateChange(selectAsset(val.asset, false)),
          NavActions.taskCenterLoad({
            taskCenterID: 'modelConfig',
            assetTree: true,
            assetTreeOpened: true,
            timeSlider: true,
            timeSliderOpened: true,
            asset: val.asset,
          }),
        ];
      })
    );
  });

  updateTimeSliderRoute$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(timeSliderActions.updateRouteTimeSlider),
      concatLatestFrom(() => this.store.select(selectSelectedAsset)),
      concatMap(([action, id]) => {
        const start = moment(action.start).toDate().getTime().toString();
        const end = moment(action.end).toDate().getTime().toString();
        if (!isNil(start) && !isNil(end)) {
          this.router.navigate([], {
            queryParams: { start, end },
            relativeTo: this.activatedRoute,
            queryParamsHandling: 'merge',
          });
        }
        // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
        return [
          NavActions.updateNavigationItems({
            urlParams: { id, start, end },
          }),
        ];
      })
    );
  });

  getPermissions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(modelConfigActions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) =>
            modelConfigActions.permissionsRequestSuccess(permissions)
          ),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error) =>
            of(modelConfigActions.permissionsRequestFailure(error))
          )
        )
      )
    );
  });

  treeStateChange$ = createEffect(() => {
    // eslint-disable-next-line ngrx/avoid-cyclic-effects
    return this.actions$.pipe(
      ofType(modelConfigActions.treeStateChange),
      concatLatestFrom(() => this.store.select(selectAssetTreeState)),
      mergeMap(([change, state]) =>
        this.assetTreeModelService
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => modelConfigActions.treeStateChange(n),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(modelConfigActions.assetTreeDataRequestFailure(error))
              )
            )
          )
      )
    );
  });

  selectAsset$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(modelConfigActions.selectAsset),
      concatLatestFrom(() => this.store.select(selectTimeSliderSelection)),
      switchMap(([action, time]) => {
        setTimeout(() => {
          const start = moment(time.start).toDate().getTime().toString();
          const end = moment(time.end).toDate().getTime().toString();
          this.router.navigate([], {
            queryParams: { id: action.asset, start, end },
            relativeTo: this.activatedRoute,
            queryParamsHandling: 'merge',
          });
        }, 100);
        // eslint-disable-next-line ngrx/no-multiple-actions-in-effects
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          NavActions.updateNavigationItems({
            urlParams: { id: action.asset },
          }),
        ];
      })
    );
  });

  reflow$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 400);
        })
      );
    },
    { dispatch: false }
  );

  // List View
  openModel$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.openModel),
        concatLatestFrom(() => this.store.select(selectTimeSliderSelection)),
        tap(([action, time]) => {
          const start = moment(time.start).toDate().getTime().toString();
          const end = moment(time.end).toDate().getTime().toString();
          let url = window.location.href;
          url = url.substring(0, url.indexOf(this.router.url));
          url = `${url}/model-config/m?modelId=${action.model}&isStandard=${action.isStandard}&aid=${action.asset}&start=${start}&end=${end}`;
          window.open(url, '_blank');
        })
      );
    },
    { dispatch: false }
  );

  openModels$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.openModels),
        concatLatestFrom(() => [
          this.store.select(selectTimeSliderSelection),
          this.store.select(selectSelectedModelGuids),
        ]),
        tap(([action, time, selectedModels]) => {
          if (this.appConfig.enableModelConfigLinks) {
            const start = moment(time.start).toDate().getTime().toString();
            const end = moment(time.end).toDate().getTime().toString();
            let url = window.location.href;
            url = url.substring(0, url.indexOf(this.router.url));
            url = `${url}/model-config/mm?modelIds=${selectedModels}&aid=${action.asset}&start=${start}&end=${end}`;
            window.open(url, '_blank');
          }
        })
      );
    },
    { dispatch: false }
  );

  //Model Actions
  openOpModeConfig$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsOpenOpMode),
        concatLatestFrom(() => [this.store.select(selectTimeSliderSelection)]),
        tap(([payload, timeslider]) => {
          const startTime = String(timeslider.start?.getTime());
          const endTime = String(timeslider.end?.getTime()).toString();
          const opModeQueryParam = isNil(payload.opModeDefinitionExtID)
            ? ''
            : `&opmode=${payload.opModeDefinitionExtID}`;
          let url = `${window.location.protocol}//${window.location.host}/model-config?tab=opmode&id=${payload.assetGuid}${opModeQueryParam}&start=${startTime}&end=${endTime}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/model-config?tab=opmode&id=${payload.assetGuid}${opModeQueryParam}&start=${startTime}&end=${endTime}`;
          }
          window?.open(url)?.focus();
        })
      );
    },
    { dispatch: false }
  );

  openDataExplorer$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsOpenDataExplorer),
        concatLatestFrom(() => [this.store.select(selectTimeSliderSelection)]),
        tap(([action, time]) => {
          const start = moment(time.start).toDate().getTime().toString();
          const end = moment(time.end).toDate().getTime().toString();
          const dataExplorerUrl = `data-explorer?id=${action.assetGuid.toString()}&start=${start}&end=${end}`;
          let url = `${window.location.protocol}//${window.location.host}/${dataExplorerUrl}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/${dataExplorerUrl}`;
          }

          window.open(url, '_blank')?.focus();
        })
      );
    },
    { dispatch: false }
  );

  diagnosticDrilldown$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsOpenDiagnosticDrilldown),
        concatLatestFrom(() => [this.store.select(selectTimeSliderSelection)]),
        tap(([action, time]) => {
          const start = moment(time.start).toDate().getTime().toString();
          const end = moment(time.end).toDate().getTime().toString();
          let baseUrl = `${window.location.protocol}//${window.location.host}`;
          const hostname = window.location.hostname;

          if (hostname !== 'localhost') {
            baseUrl = `${this.appConfig.baseSiteURL}/MD`;
          }

          if (this.appConfig.enableNewDiagnosticDrilldown) {
            const url =
              action.mtype !== 'External'
                ? baseUrl +
                  '/diagnostic-drilldown?model=' +
                  action.model +
                  '&start=' +
                  start +
                  '&end=' +
                  end +
                  '&asset=' +
                  action.assetGuid
                : action.ddurl;
            window.open(url, '_blank')?.focus();
          } else {
            window
              .open(
                this.appConfig.baseSiteURL +
                  '/Alerts/DiagnosticDrilldown.html#!?model=' +
                  action.model +
                  '&start=' +
                  start +
                  '&end=' +
                  end +
                  '&asset=' +
                  action.assetGuid,
                '_blank'
              )
              ?.focus();
          }
        })
      );
    },
    { dispatch: false }
  );

  showRelatedModels$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsShowRelatedAlerts),
        concatLatestFrom(() => [
          this.store.select(selectAssetTreeConfigNodes),
          this.store.select(selectTimeSliderSelection),
        ]),
        switchMap(([action, nodes, time]) => {
          let nodeID: string | null;
          // eslint-disable-next-line prefer-const
          nodeID =
            nodes.find((node) => node.data?.AssetGuid === action.assetGuid)
              ?.uniqueKey || null;

          if (nodeID) {
            return of([nodeID, time.start, time.end]);
          } else {
            return of([action.assetGuid, time.start, time.end]);
          }
        }),
        tap(([id, start, end]) => {
          const tstart = moment(start).toDate().getTime().toString();
          const tend = moment(end).toDate().getTime().toString();
          let url = `${window.location.protocol}//${window.location.host}/alerts?id=${id}&clearFilters=true&start=${tstart}&end=${tend}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/alerts?id=${id}&clearFilters=true`;
          }

          window.open(url, '_blank')?.focus();
        })
      );
    },
    { dispatch: false }
  );

  showRelatedIssues$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsShowRelatedIssues),
        concatLatestFrom(() => [this.store.select(selectAssetTreeConfigNodes)]),
        switchMap(([action, nodes]) => {
          let nodeID: string | null;
          // eslint-disable-next-line prefer-const
          nodeID =
            nodes.find((node) => node.data?.AssetGuid === action.assetGuid)
              ?.uniqueKey || null;

          if (nodeID) {
            return of(nodeID);
          } else {
            return of(action.assetGuid);
          }
        }),
        tap((id) => {
          let url = `${window.location.protocol}//${window.location.host}/issues?id=${id}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues?id=${id}`;
          }

          window.open(url)?.focus();
        })
      );
    },
    { dispatch: false }
  );

  newIssue$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigActionsCreateNewIssue),
        tap((action) => {
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=-1&ast=${action.assetGuid}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=-1&ast=${action.assetGuid}`;
          }

          window.open(url)?.focus();
        })
      );
    },
    { dispatch: false }
  );

  createNewModel$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(modelConfigActions.modelConfigCreateNewModel),
        concatLatestFrom(() => [
          this.store.select(selectTimeSliderSelection),
          this.store.select(selectSelectedTag),
          this.store.select(selectAssetTreeState),
        ]),
        tap(([action, time, selectedTag, treeState]) => {
          const asset = getAssetByAssetId(
            treeState,
            Number(selectedTag?.AssetID)
          );
          const assetGuid = asset?.AssetGUID ?? selectedTag?.AssetGUID;
          const start = moment(time.start).toDate().getTime().toString();
          const end = moment(time.end).toDate().getTime().toString();
          let url = window.location.href;
          const encodedTagDesc = encodeURIComponent(selectedTag?.TagDesc ?? '');
          url = url.substring(0, url.indexOf(this.router.url));
          url = `${url}/model-config/m?modelId=-1&tmid=${selectedTag?.TagMapID}&td=${encodedTagDesc}&aid=${assetGuid}&start=${start}&end=${end}`;
          window.open(url, '_blank');
        })
      );
    },
    { dispatch: false }
  );
}
