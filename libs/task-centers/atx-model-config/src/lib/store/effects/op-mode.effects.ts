import { Injectable } from '@angular/core';
import { ModelService } from '@atonix/atx-asset-tree';
import { PrivateApiAlertsCoreService } from '@atonix/shared/api';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import * as actions from '../actions/op-mode.actions';
import { selectOpModetagListSidePanelAssetTreeState } from '../reducers/model-config.reducer';

@Injectable()
export class OpModeListEffects {
  constructor(
    private actions$: Actions,
    private store: Store,
    private privateApiAlertsCoreService: PrivateApiAlertsCoreService,
    private assetTreeService: ModelService
  ) {}

  getOpModeList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getOpModeList),
      switchMap((action) => {
        return this.privateApiAlertsCoreService
          .getOpModeSummaries(action.uniqueKey)
          .pipe(
            map((opModes) => {
              return actions.getOpModeListSuccess({ opModes: opModes ?? [] });
            }),
            catchError((error: unknown) => {
              return of(
                actions.getOpModeListFailure({ error: error as Error })
              );
            })
          );
      })
    );
  });

  tagListAssetStateChange$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.tagListSidePanelAssetTreeStateChange),
      concatLatestFrom(() =>
        this.store.select(selectOpModetagListSidePanelAssetTreeState)
      ),
      switchMap(([change, state]) =>
        this.assetTreeService
          .getAssetTreeData(
            change.treeStateChange,
            state.treeConfiguration,
            state.treeNodes
          )
          .pipe(
            map((n) =>
              actions.tagListSidePanelAssetTreeStateChange({
                treeStateChange: n,
              })
            ),
            catchError((error: unknown) =>
              of(
                actions.tagListSidePanelAssetTreeRequestFailure({
                  error: error as any,
                })
              )
            )
          )
      )
    );
  });
}
