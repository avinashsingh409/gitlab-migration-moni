import { Injectable } from '@angular/core';
import {
  IModelConfigSummary,
  ModelConfigCoreService,
} from '@atonix/shared/api';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as actions from '../actions/model-list.actions';
import { selectSelectedAsset } from '../reducers/model-config.reducer';

@Injectable()
export class ModelListEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private modelConfigCoreService: ModelConfigCoreService
  ) {}

  getModelList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getModelList),
      concatLatestFrom(() => this.store.select(selectSelectedAsset)),
      switchMap(([action, asset]) =>
        this.modelConfigCoreService.getModelList(asset).pipe(
          map((modelConfigSummary: IModelConfigSummary[]) =>
            actions.getModelListSuccess({ modelConfigSummary })
          ),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error) => of(actions.getModelListFailure(error)))
        )
      )
    );
  });
}
