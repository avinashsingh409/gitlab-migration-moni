import { Injectable } from '@angular/core';
import {
  ETagType,
  IModelConfigTagListSummary,
  ModelConfigCoreService,
} from '@atonix/shared/api';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import * as actions from '../actions/tag-list.actions';
import {
  selectDependentTags,
  selectInputTags,
  selectSelectedAsset,
} from '../reducers/model-config.reducer';
@Injectable()
export class TagListEffects {
  constructor(
    private store: Store,
    private actions$: Actions,
    private modelConfigCoreService: ModelConfigCoreService
  ) {}

  getTagList$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.getTagList),
      concatLatestFrom(() => [
        this.store.select(selectSelectedAsset),
        this.store.select(selectDependentTags),
        this.store.select(selectInputTags),
      ]),
      switchMap(([action, asset, dependentTags, inputTags]) => {
        if (action.tagType === ETagType.ModelInputTags) {
          if (inputTags?.length > 0) {
            return of(
              actions.getTagListSuccess({
                tags: inputTags,
                tagType: action.tagType,
              })
            );
          } else {
            return this.modelConfigCoreService
              .getInputTagsWithModels(asset)
              .pipe(
                map((tags: IModelConfigTagListSummary[]) =>
                  actions.getTagListSuccess({ tags, tagType: action.tagType })
                ),
                catchError((error: unknown) =>
                  of(actions.getTagListFailure(error as any))
                )
              );
          }
        } else {
          if (dependentTags?.length > 0) {
            return of(
              actions.getTagListSuccess({
                tags: dependentTags,
                tagType: action.tagType,
              })
            );
          } else {
            return this.modelConfigCoreService.getDependentTagList(asset).pipe(
              map((tags: IModelConfigTagListSummary[]) =>
                actions.getTagListSuccess({ tags, tagType: action.tagType })
              ),
              catchError((error: unknown) =>
                of(actions.getTagListFailure(error as any))
              )
            );
          }
        }
      })
    );
  });
}
