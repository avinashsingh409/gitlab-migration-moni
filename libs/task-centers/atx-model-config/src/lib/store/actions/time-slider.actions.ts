import { createAction, props } from '@ngrx/store';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';

export const initializeTimeSliderState = createAction(
  '[Model Config TimeSlider] Initialize Time Slider',
  props<{ start: Date; end: Date }>()
);

export const timeSliderStateChange = createAction(
  '[Model Config TimeSlider] Time Slider State Change',
  props<{ stateChange: ITimeSliderStateChange }>()
);

export const updateRouteTimeSlider = createAction(
  '[Model Config TimeSlider] Update Route Time Slider',
  props<{ start: Date; end: Date }>()
);

export const hideTimeSlider = createAction(
  '[Model Config TimeSlider] Hide Time Slider'
);
