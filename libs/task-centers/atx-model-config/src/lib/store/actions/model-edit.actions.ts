import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { createAction, props } from '@ngrx/store';

export const initializeModelConfigEdit = createAction(
  '[Model Config Edit] Initialize Model Config Edit',
  props<{
    assetID: string;
    start: Date;
    end: Date;
  }>()
);

// Tag List Asset Tree
export const tagListAssetStateChange = createAction(
  '[Model Config Edit - Tag List Asset Tree] Asset Change',
  props<{ treeStateChange: ITreeStateChange }>()
);

export const tagListAssetRequestFailure = createAction(
  '[Model Config Edit - Tag List Asset Tree] Asset Request Failure',
  props<{ error: Error }>()
);
