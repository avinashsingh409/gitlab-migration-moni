import { IModelConfigSummary } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const getModelList = createAction(
  '[Model Config - Model List View] Get Model List'
);

export const getModelListSuccess = createAction(
  '[Model Config - Model List View] Get Model List Success',
  props<{ modelConfigSummary: IModelConfigSummary[] }>()
);

export const getModelListFailure = createAction(
  '[Model Config - Model List View] Get Model List Failure',
  props<{ error: Error }>()
);

export const updateTotalModels = createAction(
  '[Model Config - Model List View] Update Total Models',
  props<{ totalModels: number }>()
);

export const setModelListFilters = createAction(
  '[Model Config - Model List View] Set Model List Filters',
  props<{ filters: any[] }>()
);

export const removeModelListFilter = createAction(
  '[Model Config - Model List View] Remove Model List Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);

export const modelListPreferenceChange = createAction(
  '[Model Config - Model List View] Model List Preference Change'
);

export const modelListSelectionChange = createAction(
  '[Model Config - Model List View] Model List Selection Change',
  props<{ selectedModels: IModelConfigSummary[] }>()
);

export const collapseBottomSection = createAction(
  '[Model Config - Model List View] Collapse Bottom Section'
);
