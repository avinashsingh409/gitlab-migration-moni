import { ETagType, IModelConfigTagListSummary } from '@atonix/shared/api';
import { createAction, props } from '@ngrx/store';

export const getTagList = createAction(
  '[Model Config - Tag List View] Get Tag List',
  props<{ tagType: ETagType }>()
);

export const getTagListSuccess = createAction(
  '[Model Config - Tag List View] Get Tag List Success',
  props<{ tags: IModelConfigTagListSummary[]; tagType: ETagType }>()
);

export const getTagListFailure = createAction(
  '[Model Config - Tag List View] Get Tag List Failure',
  props<{ error: Error }>()
);

export const updateTotalModels = createAction(
  '[Model Config - Tag List View] Update Total Models',
  props<{ totalModels: number }>()
);
export const setTagListFilters = createAction(
  '[Model Config - Tag List View] Set Tag List Filters',
  props<{ filters: any[] }>()
);

export const removeTagListFilter = createAction(
  '[Model Config - Tag List View] Remove Tag List Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);

export const tagListPreferenceChange = createAction(
  '[Model Config - Tag List View] Tag List Preference Change'
);
export const tagListSelectionChange = createAction(
  '[Model Config - Tag List View] Tag List Selection Change',
  props<{ selectedTag: IModelConfigTagListSummary | null }>()
);
export const clearTags = createAction(
  '[Model Config - Tag List View] Tag List - Clear Tags'
);
