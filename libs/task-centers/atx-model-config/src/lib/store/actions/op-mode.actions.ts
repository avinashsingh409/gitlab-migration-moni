import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { createAction, props } from '@ngrx/store';
import { OpModeSummary } from '../../service/model/op-mode';
import { OPModeTypes } from '@atonix/shared/api';

export const createNewOpMode = createAction(
  '[Model Config - Op Mode List View] Create New Op Mode',
  props<{ opModeSummary: OpModeSummary }>()
);

export const removeOpMode = createAction(
  '[Model Config - Op Mode List View] Remove Op Mode',
  props<{ opModeDefinitionId: string }>()
);
export const updateNewOpMode = createAction(
  '[Model Config - Op Mode List View] Update Op Mode',
  props<{ opModeSummary: OpModeSummary }>()
);

export const getOpModeList = createAction(
  '[Model Config - Op Mode List View] Get Op Modes List',
  props<{ uniqueKey: string }>()
);

export const getOpModeListSuccess = createAction(
  '[Model Config - Op Mode List View] Get Op Modes List Success',
  props<{ opModes: OpModeSummary[] }>()
);

export const getOpModeListFailure = createAction(
  '[Model Config - Op Mode List View] Get Op Modes List Failure',
  props<{ error: Error }>()
);

export const updateTotalOpModes = createAction(
  '[Model Config - Op Mode List View] Update Total Op Modes',
  props<{ totalOpModes: number }>()
);
export const setOpModeListFilters = createAction(
  '[Model Config - Op Mode List View] Set Op Mode List Filters',
  props<{ filters: any[] }>()
);

export const refreshOpMode = createAction(
  '[Model Config - Op Mode List View] Refresh Op Mode Name and Date',
  props<{
    opModeSummary: OpModeSummary;
  }>()
);

export const removeOpModeListFilter = createAction(
  '[Model Config - Op Mode List View] Remove Op Mode List Filter',
  props<{ filter: any; callback: (filterName: string) => void }>()
);

// Tag List Side Panel Asset Tree
export const tagListSidePanelAssetTreeStateChange = createAction(
  '[Model Config - Op Mode Tag List Asset Tree] Asset Change',
  props<{ treeStateChange: ITreeStateChange }>()
);

export const tagListSidePanelAssetTreeRequestFailure = createAction(
  '[Model Config - Op Mode Tag List Asset Tree] Asset Request Failure',
  props<{ error: Error }>()
);

export const setDefaultOpMode = createAction(
  '[Model Config - Op Mode List View] Set Default Op Mode',
  props<{ opMode: OPModeTypes }>()
);
