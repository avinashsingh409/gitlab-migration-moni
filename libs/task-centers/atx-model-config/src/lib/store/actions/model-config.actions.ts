/* eslint-disable ngrx/prefer-inline-action-props */
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITreePermissions } from '@atonix/atx-core';
import { createAction, props } from '@ngrx/store';

export const systemInitialize = createAction(
  '[Model Config] Initialize',
  props<{ start: Date; end: Date; asset?: string }>()
);

export const selectAsset = createAction(
  '[Model Config] Select Asset',
  props<{ asset: string }>()
);

export const treeStateChange = createAction(
  '[Model Config] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[Model Config] Tree Data Request Failure',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[Model Config] Asset Tree Size Change',
  props<{ value: number }>()
);
export const permissionsRequest = createAction(
  '[Model Config] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[Model Config] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[Model Config] Permissions Request Failure',
  props<Error>()
);

export const setAssetTreeDragDrop = createAction(
  '[Model Config] Set Asset Tree Drag and Drop',
  props<{ canDrag: boolean }>()
);

export const openModel = createAction(
  '[Model Config - List View] Open Model',
  props<{ model: string; asset: string; isStandard: boolean }>()
);

export const openModels = createAction(
  '[Model Config - List View] Open Models',
  props<{ asset: string }>()
);

//Model Config Actions
export const modelConfigActionsOpenOpMode = createAction(
  '[Model Config - Actions] Open OP Mode Config',
  props<{ assetGuid: string; opModeDefinitionExtID?: string }>()
);

export const modelConfigActionsOpenDataExplorer = createAction(
  '[Model Config - Actions] Open Data Explorer',
  props<{ assetGuid: string }>()
);

export const modelConfigActionsOpenDiagnosticDrilldown = createAction(
  '[Model Config - Actions] Open Diagnostic Drilldown',
  props<{
    assetGuid: string;
    model: string;
    start: string;
    end: string;
    mtype: string;
    ddurl: string;
  }>()
);

export const modelConfigActionsShowRelatedIssues = createAction(
  '[Model Config - Actions] Show Related Issues',
  props<{ assetGuid: string }>()
);

export const modelConfigActionsShowRelatedAlerts = createAction(
  '[Model Config - Actions] Show Related Alerts',
  props<{ assetGuid: string }>()
);

export const modelConfigActionsCreateNewIssue = createAction(
  '[Model Config - Actions] Create New Issue',
  props<{ assetGuid: string }>()
);

export const modelConfigCreateNewModel = createAction(
  '[Model Config - Actions] Create New Model'
);
