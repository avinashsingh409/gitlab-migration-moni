import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NavigationModule } from '@atonix/atx-navigation';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  DateCellEditorComponent,
  NullableIntCellEditorComponent,
  NullableNumericCellEditorComponent,
  NullableRealNumberNumericCellEditorComponent,
  NumericCellEditorComponent,
  PositiveIntCellEditorComponent,
  RealNumberNumericCellEditorComponent,
  SharedUiModule,
} from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import { AgGridModule } from '@ag-grid-community/angular';
import { StoreModule } from '@ngrx/store';
import { PendingChangesGuard, SharedUtilsModule } from '@atonix/shared/utils';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { ModelConfigEffects } from './store/effects/model-config.effects';
import { ModelConfigFacade } from './store/facade/model-config.facade';
import {
  modelConfigFeatureKey,
  reducers,
} from './store/reducers/model-config.reducer';
import { GridActionsRendererComponent } from './component/ag-grid-components/grid-renderer/grid-actions-renderer.component';
import { SavedConfigPanelComponent } from './component/ag-grid-components/saved-config-panel/saved-config-panel.component';
import { ListConfigService } from './store/services/list-config-service';
import { ModelListEffects } from './store/effects/model-list.effects';
import { TagListEffects } from './store/effects/tag-list.effects';
import { GridIconRendererComponent } from './component/ag-grid-components/grid-renderer/grid-icon-renderer.component';
import { ModelConfigEventBus } from './store/services/model-config-event-bus';
import { ModelEditEffects } from './store/effects/model-edit.effects';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { ModelEditFacade } from './service/model-edit.facade';
import { ModelEditTabsModelTrendComponent } from './component/common/charts/model-trend/model-trend.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { QuickDeployDialogComponent } from './component/list-view/modals/quick-deploy-dialog/quick-deploy.dialog.component';
import { QuickDeployServiceFacade } from './store/services/quick-deploy.service.facade';
import { ExpectedVsActualComponent } from './component/common/charts/expected-vs-actual/expected-vs-actual.component';
import { ModelEditTagListComponent } from './component/model-edit/bottom-tabs/model-edit-tag-list/model-edit-tag-list.component';
import { ModelEditTabsModelHistoryComponent } from './component/common/model-history/model-history.component';
import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { AngularSplitModule } from 'angular-split';
import { SendMessageIconRendererComponent } from './component/ag-grid-components/send-message-icon-renderer-component';
import { SingleModelEditComponent } from './component/model-edit/single-model-edit/single-model-edit.component';
import { ModelConfigActionPaneComponent } from './component/common/model-config-action-pane/model-config-action-pane.component';
import { ModelListBottomViewPanelComponent } from './component/list-view/model-list-bottom-view-panel/model-list-bottom-view-panel.component';
import { ModelActionsFacade } from './service/model-actions/model-actions.facade';
import { SingleModelSaveChangesComponent } from './component/model-edit/modals/single-model-save-changes/single-model-save-changes.component';
import { BuildStatusComponent } from './component/model-edit/bottom-tabs/build-status/build-status.component';
import { SingleModelFacade } from './service/single-model.facade';
import { ModelAveragesComponent } from './component/model-edit/bottom-tabs/model-averages/model-averages.component';
import { ModelActionsConfirmationDialogComponent } from './component/common/modals/model-actions-confirmation-dialog/model-actions-confirmation-dialog.component';
import { ModelForecastComponent } from './component/model-edit/bottom-tabs/model-forecast/model-forecast.component';
import { ModelSlopeAnalysisComponent } from './component/model-edit/bottom-tabs/model-slope-analysis/model-slope-analysis.component';
import { BuildStatusMultipleComponent } from './component/model-edit/bottom-tabs/build-status-multiple/build-status-multiple.component';
import { MultiModelNoChangeRebuildComponent } from './component/model-edit/modals/multi-model-no-change-rebuild/multi-model-no-change-rebuild.component';
import { MultiModelRebuildRequiredComponent } from './component/model-edit/modals/multi-model-rebuild-required/multi-model-rebuild-required.component';
import { AllModelsLockedDialogComponent } from './component/model-edit/modals/all-models-locked/all-models-locked-dialog.component';
import { ModelSensitivityAlertsComponent } from './component/model-edit/bottom-tabs/alerts/alerts.component';
import { ModelSensitivityAnomaliesComponent } from './component/model-edit/bottom-tabs/anomalies/anomalies.component';
import { OpModeListComponent } from './component/list-view/op-mode-list/op-mode-list.component';
import { ModelConfigTaskCenterLandingPageComponent } from './component/list-view/task-center-landing-page/model-config-task-center-landing.component';
import { ModelEditTabsModelContextComponent } from './component/model-edit/top-tabs/model-context/model-context.component';
import { InputListComponent } from './component/model-edit/bottom-tabs/model-inputs/input-list/input-list.component';
import { InputListMultiModelComponent } from './component/model-edit/bottom-tabs/model-inputs/input-list-multi-model/input-list-multi-model.component';
import { InputsComponent } from './component/model-edit/bottom-tabs/model-inputs/inputs/inputs.component';
import { InputsMultiModelComponent } from './component/model-edit/bottom-tabs/model-inputs/inputs-multi-model/inputs-multi-model.component';
import { ModelListComponent } from './component/list-view/model-list/model-list.component';
import { ModelStatsComponent } from './component/model-edit/top-tabs/model-stats/model-stats.component';
import { SortByTypePipe } from './component/model-edit/top-tabs/model-stats/sortByType.pipe';
import { ModelTrainingRangeComponent } from './component/model-edit/bottom-tabs/model-training-range/model-training-range.component';
import { CreateNewModelDialogComponent } from './component/model-edit/modals/create-model-dialog/create-new-model-dialog.component';
import { TagListComponent } from './component/list-view/tag-list/tag-list.component';
import { UploadFileDialogComponent } from './component/list-view/modals/upload-file-dialog/upload-file-dialog.component';
import { MultipleModelEditComponent } from './component/model-edit/multiple-model-edit/multiple-model-edit.component';
import { OpModeListEffects } from './store/effects/op-mode.effects';
import { OpModeDetailsComponent } from './component/model-edit/top-tabs/op-mode-details/op-mode-details.component';
import { OpModeTagListComponent } from './component/list-view/op-mode-list/op-mode-tag-list/op-mode-tag-list.component';
import { OpModeCriteriaComponent } from './component/list-view/op-mode-list/op-mode-criteria/op-mode-criteria.component';
import { OpModeFacade } from './service/op-mode/op-mode.facade';
import { OpModeFormService } from './service/op-mode/op-mode-form.service';
import { OpModeAssetDragModalComponent } from './component/list-view/op-mode-list/modals/op-mode-asset-drag-modal.component';
import { OpModeDragCellRendererComponent } from './component/list-view/op-mode-list/op-mode-tag-list/op-mode-drag-cell-renderer/op-mode-drag-cell-renderer.component';
import { OpModeSaveModalComponent } from './component/list-view/op-mode-list/modals/op-mode-save-modal/op-mode-save-modal.component';
import { NewOpModeModalComponent } from './component/list-view/op-mode-list/modals/new-op-mode-modal/new-op-mode-modal.component';
import { OpModeCriteriaExclusionComponent } from './component/list-view/op-mode-list/op-mode-criteria-exclusion/op-mode-criteria-exclusion.component';
import { DeleteOpModeModalComponent } from './component/list-view/op-mode-list/modals/delete-op-mode-modal/delete-op-mode-modal.component';
import { AlertNotificationModalComponent } from './component/model-edit/modals/alert-notification-modal/alert-notification-modal.component';

@NgModule({
  declarations: [
    ModelConfigTaskCenterLandingPageComponent,
    ExpectedVsActualComponent,
    ModelStatsComponent,
    OpModeDragCellRendererComponent,
    InputsComponent,
    InputsMultiModelComponent,
    ModelAveragesComponent,
    ModelSlopeAnalysisComponent,
    ModelForecastComponent,
    InputListComponent,
    ModelEditTagListComponent,
    SingleModelEditComponent,
    ModelSensitivityAnomaliesComponent,
    ModelSensitivityAlertsComponent,
    SingleModelSaveChangesComponent,
    MultiModelNoChangeRebuildComponent,
    BuildStatusComponent,
    BuildStatusMultipleComponent,
    MultipleModelEditComponent,
    ModelListComponent,
    TagListComponent,
    OpModeDetailsComponent,
    OpModeCriteriaExclusionComponent,
    ModelEditTabsModelTrendComponent,
    GridActionsRendererComponent,
    GridIconRendererComponent,
    SavedConfigPanelComponent,
    UploadFileDialogComponent,
    AllModelsLockedDialogComponent,
    QuickDeployDialogComponent,
    ModelTrainingRangeComponent,
    ModelEditTabsModelHistoryComponent,
    SendMessageIconRendererComponent,
    ModelConfigActionPaneComponent,
    ModelListBottomViewPanelComponent,
    SortByTypePipe,
    ModelEditTabsModelContextComponent,
    ModelActionsConfirmationDialogComponent,
    MultiModelRebuildRequiredComponent,
    CreateNewModelDialogComponent,
    InputListMultiModelComponent,
    OpModeListComponent,
    OpModeTagListComponent,
    OpModeAssetDragModalComponent,
    OpModeCriteriaComponent,
    OpModeSaveModalComponent,
    NewOpModeModalComponent,
    DeleteOpModeModalComponent,
    AlertNotificationModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AssetTreeModule,
    NavigationModule,
    HighchartsChartModule,
    AngularSplitModule,
    AtxMaterialModule,
    AtxChartModuleV2,
    SharedUiModule,
    SharedApiModule,
    SharedUtilsModule,
    TimeSliderModule,
    AgGridModule.withComponents([
      GridActionsRendererComponent,
      GridIconRendererComponent,
      SavedConfigPanelComponent,
      NumericCellEditorComponent,
      DateCellEditorComponent,
      PositiveIntCellEditorComponent,
      NullableRealNumberNumericCellEditorComponent,
      NullableNumericCellEditorComponent,
      NullableIntCellEditorComponent,
      RealNumberNumericCellEditorComponent,
    ]),
    StoreModule.forFeature(modelConfigFeatureKey, reducers),
    EffectsModule.forFeature([
      ModelConfigEffects,
      ModelEditEffects,
      ModelListEffects,
      TagListEffects,
      OpModeListEffects,
    ]),
    RouterModule.forChild([
      {
        path: '',
        component: ModelConfigTaskCenterLandingPageComponent,
        pathMatch: 'full',
        canDeactivate: [PendingChangesGuard],
      },
      {
        path: 'm',
        component: SingleModelEditComponent,
        pathMatch: 'full',
      },
      {
        path: 'mm',
        component: MultipleModelEditComponent,
        pathMatch: 'full',
      },
    ]),
  ],
  providers: [
    ModelConfigFacade,
    ModelEditFacade,
    ListConfigService,
    ListConfigService,
    SingleModelFacade,
    QuickDeployServiceFacade,
    ModelConfigEventBus,
    ModelActionsFacade,
    OpModeFacade,
    OpModeFormService,
  ],
})
export class ModelConfigModule {}
