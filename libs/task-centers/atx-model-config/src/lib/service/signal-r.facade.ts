import * as signalR from '@microsoft/signalr';
import { RetryContext } from '@microsoft/signalr';
import { SingleModelFacade } from './single-model.facade';

export class CustomRetryPolicy implements signalR.IRetryPolicy {
  readonly singleModelFacade: SingleModelFacade;

  nextRetryDelayInMilliseconds(retryContext: RetryContext): number | null {
    console.log(`Retry :: ${retryContext.retryReason}`);
    this.singleModelFacade.disconnected();
    if (retryContext.previousRetryCount === 30) return null;

    const nextRetry = retryContext.previousRetryCount * 1000 || 1000;
    console.log(`Retry in ${nextRetry} milliseconds`);
    return nextRetry;
  }
  constructor(singleModelFacade: SingleModelFacade) {
    this.singleModelFacade = singleModelFacade;
  }
}
