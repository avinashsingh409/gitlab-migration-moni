import { isNil, isNilOrEmptyString } from '@atonix/atx-core';
import {
  EModelTypes,
  IAlert,
  IAnomaly,
  IAverageAnomalyAlertProperties,
  IForecastModelProperties,
  IHighHighAlertProperties,
  ILowLowAlertProperties,
  IModelConfigData,
  IRawModelConfigData,
  IRelativeBoundsAnomalyProperties,
} from '@atonix/shared/api';
import { combineTime } from '@atonix/shared/utils';
import produce from 'immer';
import moment, { unitOfTime } from 'moment';

export function validateModelReturn(e: unknown): {
  error: boolean;
  message: string;
} {
  if (!isNil((e as any).error)) {
    const errorMessage = (e as any).error?.errors;
    if (isNil(errorMessage)) {
      return {
        error: true,
        message: 'a transform validation error occurred unexpectedly',
      };
    } else {
      if (errorMessage['$.model.training.duration']) {
        return { error: true, message: 'Training Duration is Invalid' };
      } else {
        return {
          error: true,
          message: 'A transform validation error occurred',
        };
      }
    }
  } else {
    return {
      error: true,
      message: 'A validation error occurred',
    };
  }
}

export function validateModel(model: IModelConfigData): {
  error: boolean;
  message: string;
} {
  if (
    model.properties.modelType.type ===
      EModelTypes.AdvancedPatternRecognition &&
    model.independents?.length <= 0
  ) {
    return {
      error: true,
      message: 'Cannot Save. Model Inputs must have at least one tag.',
    };
  }
  if (
    (model.fixedEarliestInterceptChanged &&
      model.fixedEarliestInterceptDate === 'invalid date') ||
    (model.fixedEarliestInterceptChanged &&
      model.fixedEarliestInterceptTime === 'invalid time')
  ) {
    return {
      error: true,
      message:
        'Forecast Model. Fixed Earliest Acceptable Intercept Date Invalid',
    };
  }

  return { error: false, message: '' };
}

export function setModelNonStandard(draftState: IModelConfigData) {
  draftState.standardModel = false;
  if (draftState.legacy.modelID === 0) {
    draftState.legacy.modelID = -1;
  }
}

export function legacyValidationReturn(modelValidationReturn: any): {
  error: boolean;
  message: string;
} {
  if (modelValidationReturn.Locked) {
    return {
      error: true,
      message: 'Model Is Locked',
    };
  }
  if (modelValidationReturn.ValidationResult.IsValid === false) {
    if (modelValidationReturn.ValidationResult.ValidationErrors.length > 0) {
      let errorString =
        modelValidationReturn.ValidationResult.ValidationErrors[0]
          ?.ErrorMessage;
      if (errorString === 'HighHighAlertMAEUse') {
        return {
          error: true,
          message: 'High High Alert Cannot Use MAE',
        };
      }
      if (errorString === 'LowLowAlertMAEUse') {
        return {
          error: true,
          message: 'Low Low Alert Cannot Use MAE',
        };
      }
      const stringArray = errorString?.split(' ');

      if (stringArray.length === 1) {
        // there are errors that will show up as `My_Error` <- this will break them into words
        errorString =
          modelValidationReturn.ValidationResult.ValidationErrors[0].ErrorMessage.match(
            /[A-Z][a-z]+|[0-9]+/g
          ).join(' ');
      }
      return {
        error: true,
        message: errorString,
      };
    } else {
      return {
        error: true,
        message: 'An error occurred',
      };
    }
  }
  return {
    error: false,
    message: '',
  };
}

function convertAgGridBoolean(value: string | boolean | undefined): boolean {
  if (typeof value === 'undefined') {
    return false;
  }
  if (typeof value === 'boolean') {
    return value;
  }
  if (value === 'Yes') {
    return true;
  }
  return false;
}

function convertAgGridUndefinableBoolean(
  value: string | boolean | undefined
): boolean | undefined {
  if (isNil(value)) {
    return undefined;
  }
  if (typeof value === 'boolean') {
    return value;
  }
  if (value === 'Yes') {
    return true;
  }
  return false;
}

function createUseMaeAlert(anomalyAlert: IAlert): IAlert {
  const anomalyProperties = { ...anomalyAlert?.properties };
  if (
    !isNil(
      (
        anomalyProperties as
          | IAverageAnomalyAlertProperties
          | ILowLowAlertProperties
          | IHighHighAlertProperties
      ).useMeanAbsoluteError
    )
  ) {
    (
      anomalyProperties as
        | IAverageAnomalyAlertProperties
        | ILowLowAlertProperties
        | IHighHighAlertProperties
    ).useMeanAbsoluteError = convertAgGridUndefinableBoolean(
      (
        anomalyProperties as
          | IAverageAnomalyAlertProperties
          | ILowLowAlertProperties
          | IHighHighAlertProperties
      ).useMeanAbsoluteError
    );
  }

  return {
    type: anomalyAlert.type,
    enabled: convertAgGridBoolean(anomalyAlert.enabled),
    notificationEnabled: anomalyAlert.notificationEnabled,
    properties: anomalyProperties,
  };
}

function createAnomaly(anomaly: IAnomaly): IAnomaly {
  return {
    type: anomaly.type,
    enabled: convertAgGridBoolean(anomaly.enabled),
    properties: anomaly.properties,
  };
}

function createAlert(alert: IAlert): IAlert {
  return {
    type: alert.type,
    enabled: convertAgGridBoolean(alert.enabled),
    notificationEnabled: alert.notificationEnabled,
    properties: alert.properties,
  };
}

export function createModelToSave(
  model: IModelConfigData
): IRawModelConfigData {
  // Forecast Model
  const newModelProperties = produce(model.properties, (draftState) => {
    if (model.properties.modelType.type === EModelTypes.Forecast) {
      if (!isNilOrEmptyString(model.fixedEarliestInterceptDate)) {
        let forecastDateTimeString = '';
        if (model.fixedEarliestInterceptChanged) {
          const tempDate = model.fixedEarliestInterceptDate
            .substring(0, 10)
            .split('/');
          forecastDateTimeString = moment
            .utc([+tempDate[2], +tempDate[0] - 1, +tempDate[1]])
            .format();
        } else {
          const forecastDate = moment(
            model.fixedEarliestInterceptDate
          ).toDate();
          const forecastDateTime: Date = combineTime(
            forecastDate,
            model.fixedEarliestInterceptTime
          );
          forecastDateTimeString = moment(forecastDateTime).format();
        }
        (
          draftState.modelType.properties as IForecastModelProperties
        ).horizonDate = forecastDateTimeString;
      } else {
        (
          draftState.modelType.properties as IForecastModelProperties
        ).horizonDate = moment(new Date(), 'M/D/YYYY HH:mm')
          .add(3, 'months')
          .format();
      }
      if (model.fixedEarliestInterceptChanged) {
        if (
          (model.properties.modelType.properties as IForecastModelProperties)
            .horizonDate === 'Invalid date' ||
          isNilOrEmptyString(
            (model.properties.modelType.properties as IForecastModelProperties)
              .horizonDate
          )
        ) {
          (
            draftState.modelType.properties as IForecastModelProperties
          ).horizonDate = moment(new Date(), 'M/D/YYYY HH:mm')
            .add(3, 'months')
            .format();
        }
      }
    }
  });

  const alerts: IAlert[] = [];
  alerts.push(createUseMaeAlert(model.averageAnomalyAlert));
  alerts.push(createAlert(model.anomalyFrequencyAlert));
  alerts.push(createAlert(model.anomalyAreaAlert));
  alerts.push(createAlert(model.anomalyOscillationAlert));
  alerts.push(createUseMaeAlert(model.highHighAlert));
  alerts.push(createUseMaeAlert(model.lowLowAlert));
  alerts.push(createAlert(model.frozenDataCheckAlert));
  const anomalies: IAnomaly[] = [];
  const relativeBoundsAnomaly = createAnomaly(model.relativeBoundsAnomaly);
  const newProperties = produce(
    {
      ...relativeBoundsAnomaly.properties,
    } as IRelativeBoundsAnomalyProperties,
    (draftState) => {
      if (isNilOrEmptyString(draftState.upperBias)) {
        draftState.upperBiasEnabled = false;
      } else {
        draftState.upperBiasEnabled = true;
      }
      if (isNilOrEmptyString(draftState.lowerBias)) {
        draftState.lowerBiasEnabled = false;
      } else {
        draftState.lowerBiasEnabled = true;
      }
    }
  );
  const newRelativeBoundsAnomaly = produce(
    relativeBoundsAnomaly,
    (draftState) => {
      draftState.properties = newProperties;
    }
  );
  anomalies.push(newRelativeBoundsAnomaly);
  anomalies.push(createAnomaly(model.upperFixedLimitAnomaly));
  anomalies.push(createAnomaly(model.lowerFixedLimitAnomaly));
  anomalies.push(createAnomaly(model.criticalityAnomaly));

  const modelToSave: IRawModelConfigData = {
    active: model.active,
    alerts: alerts,
    anomalies: anomalies,
    dependent: model.dependent,
    independents: model.independents,
    legacy: model.legacy,
    locked: model.locked,
    modelExtID: model.modelExtID,
    name: model.name,
    path: model.path,
    properties: model.fixedEarliestInterceptChanged
      ? newModelProperties
      : model.properties,
    standardModel: model.standardModel,
    training: model.training,
    buildProperties: model.buildProperties,
  };

  return modelToSave;
}
