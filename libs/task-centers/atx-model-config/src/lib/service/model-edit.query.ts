import { IProcessedTrend } from '@atonix/atx-core';
import {
  ICorrelationData,
  IModelConfigData,
  IModelConfigModelTrend,
  IModelIndependent,
} from '@atonix/shared/api';
import { ComponentStore } from '@ngrx/component-store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { orderBy } from 'lodash';
import { IAlertNotificationModel } from './model/alert-notification';
import { OpMode } from './model/op-mode';
import {
  SelectedChip,
  SensitivityAlertTypes,
  SensitivityAnomalyTypes,
} from './model/sensitivity-chips';

export interface IModelInput {
  assetVariableTagMapID: number;
  modelExtIds: string[];
  input?: IModelIndependent;
  inUseCount: number;
  forcedInclusionCount: number;
  configuredCount: number;
}

export interface ModelRefreshAction {
  reloadModel: boolean; // state and predictive methods
  reloadPredictiveMethodsNeeded: boolean;
  modelExtID?: string; // used only in multimodel
}
export interface ModelEditState extends EntityState<IModelConfigData> {
  selectedModelId: string;
  multiModelIds: string[];
  modelTrendData: IModelConfigModelTrend | null;
  modelTemplates: IModelConfigData[] | null;
  modelValidationReturn: any | null;
  modelTrendLoading: boolean;
  modelTemplatesLoading: boolean;
  modelSummaryLoading: boolean;
  selectedAnomalyChip: SelectedChip;
  selectedAlertChip: SelectedChip;
  modelHistoryReload: boolean;
  modelMathMessage: any;
  toggleModelActionsPane: boolean;
  selectedModelBuildStatus: string;
  modelSaving: boolean;
  modelRefreshAction: ModelRefreshAction;
  updateFormElements: boolean;
  modelContextData: IProcessedTrend | null;
  modelContextLoading: boolean;
  modelContextDataLoading: boolean;
  showNewModelDialog: boolean;
  isNewModel: boolean;
  correlationData: ICorrelationData[] | null;
  hasSamePdServer: boolean;
  opModes: OpMode[] | null;
  alertNotificationModel: IAlertNotificationModel | null;

  reloadModels: boolean;
}

export const adapter: EntityAdapter<IModelConfigData> =
  createEntityAdapter<IModelConfigData>({
    selectId: (modelConfig: IModelConfigData) => modelConfig.modelExtID,
  });

export const { selectAll } = adapter.getSelectors();

export const initialModelEditState: ModelEditState = adapter.getInitialState({
  selectedModelId: '',
  multiModelIds: [],
  modelTrendData: null,
  modelTemplates: null,
  modelValidationReturn: null,
  modelTrendLoading: false,
  modelTemplatesLoading: false,
  modelSummaryLoading: false,
  selectedAlertChip: {
    type: 'alert',
    subType: +SensitivityAlertTypes.All,
  },
  selectedAnomalyChip: {
    type: 'anomaly',
    subType: +SensitivityAnomalyTypes.All,
  },
  modelHistoryReload: false,
  modelMathMessage: null,
  toggleModelActionsPane: true,
  selectedModelBuildStatus: '',
  modelSaving: false,
  modelRefreshAction: {
    reloadModel: false,
    reloadPredictiveMethodsNeeded: false,
  },
  updateFormElements: false,
  modelContextData: null,
  modelContextLoading: false,
  modelContextDataLoading: false,
  showNewModelDialog: false,
  isNewModel: false,
  correlationData: null,
  hasSamePdServer: false,
  opModes: null,
  alertNotificationModel: null,
  reloadModels: false,
});

export class ModelEditQuery {
  constructor(private store: ComponentStore<ModelEditState>) {}

  readonly modelSummaries$ = this.store.select(
    (state) => {
      return Object.values(state.entities);
    },
    { debounce: true }
  );

  readonly modelEntities$ = this.store.select((state) => {
    return state.entities;
  });

  readonly selectedModelId$ = this.store.select(
    (state) => state.selectedModelId
  );

  readonly multiModelIds$ = this.store.select((state) => state.multiModelIds);

  readonly selectedModelSummary$ = this.store.select(
    this.modelEntities$,
    this.selectedModelId$,
    (modelSummaries, modelSummaryID) => modelSummaries[modelSummaryID]
  );

  readonly selectedModelsInputs$ = this.store.select(
    this.modelSummaries$,
    (models) => {
      const modelInputs: Record<string, IModelInput> = {};

      models.forEach((model) =>
        model?.independents.map((i) => {
          const modelInput: IModelInput = {
            assetVariableTagMapID: i.assetVariableTagMapID,
            modelExtIds: [],
            input: undefined,
            configuredCount: 0,
            inUseCount: 0,
            forcedInclusionCount: 0,
          };

          const existingModelInput = modelInputs[i.tagGuid];
          if (existingModelInput) {
            existingModelInput.inUseCount = i.inUse
              ? existingModelInput.inUseCount + 1
              : existingModelInput.inUseCount;
            existingModelInput.forcedInclusionCount = i.forcedInclusion
              ? existingModelInput.forcedInclusionCount + 1
              : existingModelInput.forcedInclusionCount;
            existingModelInput.configuredCount += 1;
            existingModelInput.input = i;

            if (existingModelInput.modelExtIds.indexOf(model.modelExtID) <= 0) {
              existingModelInput.modelExtIds.push(model.modelExtID);
            }

            modelInputs[i.tagGuid] = existingModelInput;
          } else {
            modelInput.inUseCount = i.inUse ? 1 : 0;
            modelInput.forcedInclusionCount = i.forcedInclusion ? 1 : 0;
            modelInput.configuredCount += 1;
            modelInput.modelExtIds = [model.modelExtID];
            modelInput.input = i;
            modelInputs[i.tagGuid] = modelInput;
          }
        })
      );
      return modelInputs;
    }
  );

  readonly predictiveMethodTypeSelected$ = this.store.select((state) => {
    const modelSummary = state.entities[state.selectedModelId];
    return modelSummary?.properties?.predictiveTypeSelected;
  });

  readonly modelTrendData$ = this.store.select((state) => state.modelTrendData);

  readonly modelTemplates$ = this.store.select((state) => state.modelTemplates);

  readonly modelTrendLoading$ = this.store.select(
    (state) => state.modelTrendLoading
  );

  readonly modelTemplatesLoading$ = this.store.select(
    (state) => state.modelTemplatesLoading
  );

  readonly modelSummaryLoading$ = this.store.select(
    (state) => state.modelSummaryLoading,
    {
      debounce: true,
    }
  );
  readonly selectedAnomalyChip$ = this.store.select(
    (state) => state.selectedAnomalyChip,
    {
      debounce: true,
    }
  );
  readonly selectedAlertChip$ = this.store.select(
    (state) => state.selectedAlertChip,
    {
      debounce: true,
    }
  );

  readonly modelHistoryReload$ = this.store.select(
    (state) => state.modelHistoryReload,
    {
      debounce: true,
    }
  );

  readonly modelMathMessage$ = this.store.select(
    (state) => state.modelMathMessage,
    {
      debounce: true,
    }
  );

  readonly toggleModelActionsPane$ = this.store.select(
    (state) => state.toggleModelActionsPane,
    {
      debounce: true,
    }
  );

  readonly modelValidationReturn$ = this.store.select(
    (state) => state.modelValidationReturn,
    {
      debounce: true,
    }
  );

  readonly selectedModelBuildStatus$ = this.store.select(
    (state) => state.selectedModelBuildStatus,
    {
      debounce: true,
    }
  );

  readonly modelSaving$ = this.store.select((state) => state.modelSaving, {
    debounce: true,
  });

  readonly updateFormElements$ = this.store.select(
    (state) => state.updateFormElements,
    {
      debounce: true,
    }
  );

  readonly modelContextData$ = this.store.select(
    (state) => state.modelContextData
  );

  readonly modelContextLoading$ = this.store.select(
    (state) => state.modelContextLoading
  );

  readonly modelContextDataLoading$ = this.store.select(
    (state) => state.modelContextDataLoading
  );

  readonly showNewModelDialog$ = this.store.select(
    (state) => state.showNewModelDialog
  );

  readonly isNewModel$ = this.store.select((state) => state.isNewModel);

  readonly modelRefreshAction$ = this.store.select(
    (state) => state.modelRefreshAction
  );

  readonly correlationData$ = this.store.select(
    (state) => state.correlationData
  );

  readonly reloadModelStateWatcher$ = this.store.select(
    this.modelEntities$,
    this.selectedModelId$,
    this.modelRefreshAction$,
    this.isNewModel$,
    (modelSummaries, selectedModelID, modelRefreshAction, isNewModel) => {
      return {
        modelRefreshAction,
        selectedModelID: selectedModelID,
        modelSummary: modelSummaries[selectedModelID],
        isNewModel,
      };
    }
  );

  readonly hasSamePdServer$ = this.store.select((state) => {
    return state.hasSamePdServer;
  });

  readonly opModes$ = this.store.select((state) => {
    return state.opModes;
  });

  readonly alertNotificationModel$ = this.store.select((state) => {
    const alertNotificationModel = { ...state.alertNotificationModel };
    Object.assign(alertNotificationModel, {
      recipientCount: alertNotificationModel.recipients?.length ?? 0,
      users: orderBy(alertNotificationModel.users, ['FriendlyName'], ['asc']),
      filteredUsers: orderBy(
        alertNotificationModel.filteredUsers,
        ['FriendlyName'],
        ['asc']
      ),
    });

    return alertNotificationModel;
  });

  readonly reloadModels$ = this.store.select((state) => state.reloadModels);

  readonly vm$ = this.store.select(
    this.modelValidationReturn$,
    this.selectedModelBuildStatus$,
    this.modelSaving$,
    this.modelSummaries$,
    this.selectedModelSummary$,
    this.selectedAnomalyChip$,
    this.selectedAlertChip$,
    this.modelTrendData$,
    this.modelTemplates$,
    this.modelTrendLoading$,
    this.modelTemplatesLoading$,
    this.modelSummaryLoading$,
    this.modelHistoryReload$,
    this.modelMathMessage$,
    this.toggleModelActionsPane$,
    this.updateFormElements$,
    this.modelContextData$,
    this.modelContextLoading$,
    this.modelContextDataLoading$,
    this.showNewModelDialog$,
    this.isNewModel$,
    this.correlationData$,
    this.hasSamePdServer$,
    this.opModes$,
    this.selectedModelsInputs$,
    this.alertNotificationModel$,
    this.reloadModels$,
    (
      modelValidationReturn,
      selectedModelBuildStatus,
      modelSaving,
      modelSummaries,
      selectedModelSummary,
      selectedAnomalyChip,
      selectedAlertChip,
      modelTrendData,
      modelTemplates,
      modelTrendLoading,
      modelTemplatesLoading,
      modelSummaryLoading,
      modelHistoryReload,
      modelMathMessage,
      toggleModelActionsPane,
      updateFormElements,
      modelContextData,
      modelContextLoading,
      modelContextDataLoading,
      showNewModelDialog,
      isNewModel,
      correlationData,
      hasSamePdServer,
      opModes,
      selectedModelsInputs,
      alertNotificationModel,
      reloadModels
    ): any => {
      return {
        modelValidationReturn,
        selectedModelBuildStatus,
        modelSaving,
        modelSummaries,
        selectedModelSummary,
        selectedAnomalyChip,
        selectedAlertChip,
        modelTrendData,
        modelTemplates,
        modelTrendLoading,
        modelTemplatesLoading,
        modelSummaryLoading,
        modelHistoryReload,
        modelMathMessage,
        toggleModelActionsPane,
        updateFormElements,
        modelContextData,
        modelContextLoading,
        modelContextDataLoading,
        showNewModelDialog,
        isNewModel,
        correlationData,
        hasSamePdServer,
        opModes,
        selectedModelsInputs,
        alertNotificationModel,
        reloadModels,
      };
    }
  );
}
