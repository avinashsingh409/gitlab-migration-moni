import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';

// Right now this just has the SignalR status for the Single Model Edit Component
// as the behaviors diverge between single model and multimodel efforts we can use
// this facade for additional behavior
export interface SingleModelState {
  isDisconnected: boolean;
}

export const initialSingleModelState: SingleModelState = {
  isDisconnected: true,
};

@Injectable({
  providedIn: 'root',
})
export class SingleModelFacade extends ComponentStore<SingleModelState> {
  constructor() {
    super(initialSingleModelState);
  }

  readonly disconnected = this.updater(
    (state: SingleModelState): SingleModelState => {
      return {
        ...state,
        isDisconnected: true,
      };
    }
  );

  readonly connected = this.updater(
    (state: SingleModelState): SingleModelState => {
      return {
        ...state,
        isDisconnected: false,
      };
    }
  );

  readonly isDisconnected$ = this.select((state) => state.isDisconnected, {
    debounce: true,
  });
}
