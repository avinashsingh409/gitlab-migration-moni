import {
  AlertsCoreService,
  AlertsFrameworkService,
  EModelTypes,
  EOpModeTypes,
  EPredictiveTypes,
  EUnitOfTime,
  IAnomalyAreaAlertProperties,
  IAnomalyFrequencyAlertProperties,
  IAnomalyOscillationAlertProperties,
  ICorrelationData,
  ICriticalityAnomalyProperties,
  IForecastModelProperties,
  IFrozenDataAlertProperties,
  IHighHighAlertProperties,
  ILowerFixedLimitAnomalyProperties,
  ILowLowAlertProperties,
  IModelConfigData,
  IModelConfigModelTrend,
  IModelIndependent,
  IModelTypeProperties,
  IRateOfChangeProperties,
  IRawModelConfigData,
  ISubscriber,
  IUpperFixedLimitAnomalyProperties,
  ModelSummaryDependentMapping,
} from '@atonix/shared/api';
import {
  tap,
  switchMap,
  catchError,
  concatMap,
  map,
  mergeMap,
} from 'rxjs/operators';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  adapter,
  initialModelEditState,
  ModelEditState,
  ModelRefreshAction,
} from './model-edit.query';
import produce from 'immer';
import {
  SelectedChip,
  SensitivityAlertTypes,
  SensitivityAnomalyTypes,
} from './model/sensitivity-chips';
import { Update } from '@ngrx/entity';
import { IModelSummaryUpdate } from './model-edit.actions';
import {
  groupByKey,
  IAssetMeasurementsSet,
  IProcessedTrend,
  isNil,
  isNilOrEmptyString,
  processModelContextTrend,
  transformValueToMilliseconds,
} from '@atonix/atx-core';
import { EMPTY, forkJoin, Observable, of } from 'rxjs';
import { ToastService } from '@atonix/shared/utils';
import moment, { unitOfTime } from 'moment';
import {
  createModelToSave,
  legacyValidationReturn,
  setModelNonStandard,
  validateModel as validateModelClientSide,
  validateModelReturn,
} from './model-edit.utilities';
import { Inject } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  LegacyCriteriaObject,
  LegacyLogicObject,
  LegacyOpModeDefinition,
} from './model/op-mode-legacy';
import { OpMode } from './model/op-mode';
import { IAlertNotificationModel } from './model/alert-notification';
export class ModelEditCommands {
  readonly privateAlertsMicroService: string = '';
  readonly logging: boolean = false;
  constructor(
    private store: ComponentStore<ModelEditState>,
    private alertsCoreService: AlertsCoreService,
    private alertsFrameworkService: AlertsFrameworkService,
    private toastService: ToastService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.logging = false;
    this.privateAlertsMicroService = this.appConfig.baseMicroServicesURL;
  }

  //STORE EFFECTS
  readonly beginSaveModelGetTransform = this.store.effect(
    (modelSave$: Observable<{ isMulti: boolean; model: IModelConfigData }>) => {
      return modelSave$.pipe(
        tap((modelSave) => this.initializeModelSaving(modelSave.isMulti)),
        concatMap((modelSave) => {
          if (isNil(modelSave.model)) {
            return EMPTY;
          }
          if (this.logging) {
            console.log(`${modelSave.model.modelExtID}: begin save`);
          }
          const retString = validateModelClientSide(modelSave.model);
          if (retString?.error) {
            this.setModelSaveComplete({
              isMulti: modelSave.isMulti,
              message: retString.message,
              modelExtID: modelSave.model.modelExtID,
            });
            if (this.logging) {
              console.log(
                `${modelSave.model.modelExtID} - Error. ${retString.message}`
              );
            }
            return EMPTY;
          }
          const modelToSave = createModelToSave(modelSave.model);
          return this.alertsCoreService.getModelTransform(modelToSave).pipe(
            tap({
              next: (modelTransform) => {
                if (this.logging) {
                  console.log(
                    `${modelSave.model.modelExtID}: transform received`
                  );
                }
                return this.legacyValidateModel({
                  model: modelTransform,
                  isMulti: modelSave.isMulti,
                });
              },
              error: (e: unknown) => {
                if (this.logging) {
                  console.log(`${modelSave.model.modelExtID}: transfer eror `);
                  console.log(JSON.stringify(e));
                }
                const retValue = validateModelReturn(e);
                this.setModelSaveComplete({
                  isMulti: modelSave.isMulti,
                  message: retValue.message,
                  modelExtID: modelSave.model.modelExtID,
                });
                console.error(e);
              },
            }),
            catchError(() => EMPTY)
          );
        })
      );
    }
  );

  readonly legacyValidateModel = this.store.effect(
    (
      modelTransform$: Observable<{
        model: IRawModelConfigData;
        isMulti: boolean;
      }>
    ) => {
      return modelTransform$.pipe(
        tap((transform: any) =>
          this.setModelSaving({
            isMulti: transform.isMulti,
            modelExtID: transform.model.ModelExtID,
          })
        ),
        tap((transform: any) =>
          this.updateModelExtID({
            modelExtID: transform.model.ModelExtID,
            isMulti: transform.isMulti,
          })
        ),
        concatMap((modelTransform: { model: any; isMulti: boolean }) => {
          if (isNil(modelTransform)) {
            return EMPTY;
          }
          if (this.logging) {
            console.log(
              `${modelTransform.model.ModelExtID}: transfer complete. evaluate begins`
            );
          }
          return this.alertsFrameworkService
            .modelValidate(modelTransform.model)
            .pipe(
              tap({
                next: (frameworkReturn) => {
                  if (this.logging) {
                    console.log(
                      `${modelTransform.model.ModelExtID}: evaluate returned`
                    );
                  }

                  if (modelTransform.isMulti) {
                    const validationReturn =
                      legacyValidationReturn(frameworkReturn);
                    if (validationReturn.error) {
                      return this.legacyEvaluateError({
                        modelExtID: modelTransform.model.ModelExtID,
                        message: validationReturn.message,
                      });
                    }

                    frameworkReturn.Model.ModelRequest = {
                      PostStatus: `${this.appConfig.baseMicroServicesURL}/api/models/status`,
                      PostFailure: `${this.appConfig.baseMicroServicesURL}/api/models/buildfailure`,
                      PostFulfillment: `${this.appConfig.baseMicroServicesURL}/api/models/admit`,
                    };
                    frameworkReturn.UserDeclinedRebuild = false;
                    return this.modelExecuteSaveActions({
                      validationObject: frameworkReturn,
                      isMulti: true,
                      message: '',
                      modelExtID: modelTransform.model.ModelExtID,
                    });
                  } else {
                    return this.singleModelValidationReturnComplete({
                      modelValidationReturn: frameworkReturn,
                    });
                  }
                },
                error: (e: unknown) => {
                  this.setModelSaveComplete({
                    isMulti: modelTransform.isMulti,
                    message: 'A model validation error occurred',
                    modelExtID: modelTransform.model.modelExtID,
                  });
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly modelExecuteSaveActions = this.store.effect(
    (
      validationObject$: Observable<{
        validationObject: any;
        isMulti: boolean;
        message: string;
        modelExtID: string | null;
      }>
    ) => {
      return validationObject$.pipe(
        concatMap((validationReturn) => {
          if (isNil(validationReturn)) {
            return EMPTY;
          }
          if (this.logging) {
            console.log(
              `${validationReturn.modelExtID}: save. user confirmed or declined. ${validationReturn.message}`
            );
          }
          return this.alertsFrameworkService
            .modeleExecuteSaveActions(validationReturn.validationObject)
            .pipe(
              tap({
                next: (frameworkReturn) =>
                  this.modelExecuteSaveActionsComplete({
                    userPressedCancel:
                      validationReturn?.validationObject.UserDeclinedRebuild ||
                      false,
                    retValue: frameworkReturn,
                    isMulti: validationReturn.isMulti,
                    modelExtID: validationReturn.modelExtID,
                  }),
                error: (e: unknown) => {
                  this.setModelSaveComplete({
                    isMulti: validationReturn.isMulti,
                    message: 'a model save error occurred',
                    modelExtID: validationReturn.modelExtID,
                  });
                  if (this.logging) {
                    console.log(
                      `${validationReturn.modelExtID}: api/Monitoring/ExecuteModelSaveActions error.`
                    );
                  }
                  console.log(JSON.stringify(validationReturn));
                  console.error(e);
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly selectOpModeDefinition = this.store.effect(
    (
      opModeApiCall$: Observable<{
        astVarTypeTagMapID: number;
        buildDates: Date[];
        saveDates: Date[];
      }>
    ) => {
      return opModeApiCall$.pipe(
        switchMap((validationReturn) => {
          return this.alertsFrameworkService
            .prioritizedOpModeDefinitionsForTagMap(
              validationReturn.astVarTypeTagMapID
            )
            .pipe(
              tap({
                next: (frameworkReturn) => {
                  const opModes = frameworkReturn as LegacyOpModeDefinition[];
                  return this.opModeApiCallComplete({
                    opModes,
                    buildDates: validationReturn.buildDates,
                    saveDates: validationReturn.saveDates,
                  });
                },
                error: (e: unknown) => {
                  console.log(JSON.stringify(validationReturn));
                  console.error(e);
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly getAlertNotification = this.store.effect(
    (
      getAlertNotificationParams$: Observable<{
        alertStatusTypeId: number;
        modelId: number;
        alertStatusTypeName?: string;
        modelName?: string;
        tagName?: string;
        tagDescription?: string;
        tagUnits?: string;
        assetPath?: string;
      }>
    ) => {
      return getAlertNotificationParams$.pipe(
        switchMap((params) =>
          forkJoin([
            of(params),
            this.alertsFrameworkService.getAlertNotificationParameters(
              params.alertStatusTypeId,
              params.modelId
            ),
          ])
        ),
        switchMap(([params, users]) =>
          this.alertsFrameworkService
            .getAlertNotification(params.alertStatusTypeId, params.modelId)
            .pipe(
              tapResponse(
                (response) => {
                  const unitName = (params.assetPath ?? '').split('/')[1];
                  let defaultMessage = `${params.modelName} went into ${params.alertStatusTypeName} at {Time}.  The value at {DataDateTime} is {Value} ${params.tagUnits}. Unit Name: ${unitName} Tag Name: ${params.tagName}.`;
                  defaultMessage +=
                    '\nTimestamp: {DataDateTime}, Actual: {Actual}, Expected: {Expected}, Upper: {Upper}, Lower: {Lower}';

                  const alertNotification: IAlertNotificationModel = {
                    alertNotificationId: '-1',
                    alertStatusTypeId: params.alertStatusTypeId,
                    modelId: params.modelId,
                    customMessage: '',
                    alertStatusTypeName: params.alertStatusTypeName,
                    modelName: params.modelName,
                    tagName: params.tagName,
                    tagDescription: params.tagDescription,
                    tagUnits: params.tagUnits,
                    assetPath: params.assetPath,
                    notificationFrequency: 24,
                    defaultMessage: defaultMessage,
                    users: users,
                    recipients: users?.filter((x) => x.IsSubscribed),
                    isSaved: false,
                  };

                  if (response) {
                    alertNotification.alertNotificationId =
                      response.NDAlertNotificationID;
                    alertNotification.customMessage =
                      response.NotificationSummary;
                    // alertNotification.defaultMessage = //Don't update local "Default Message" with the NotificationTitle value saved in the DB
                    //   response.NotificationTitle;
                    alertNotification.notificationFrequency =
                      response.NotificationFrequency;
                  }

                  this.loadAlertNotification(alertNotification);

                  if (users) {
                    const groups = users?.filter((x) => x.IsGroup);
                    for (const grp of groups) {
                      this.getAlertNotificationParamGroupMembers({
                        userName: grp.UserName,
                      });
                    }
                  }
                },
                (e: unknown) => {
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly getAlertNotificationParamGroupMembers = this.store.effect(
    (
      params$: Observable<{
        userName: string;
      }>
    ) => {
      return params$.pipe(
        mergeMap((params) =>
          this.alertsFrameworkService
            .getAlertNotificationParametersGroupMembers(params.userName)
            .pipe(
              tapResponse(
                (response) => {
                  this.setUserGroupMembers({
                    userName: params.userName,
                    members: response,
                  });
                },
                (e: unknown) => {
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly saveAlertNotification = this.store.effect(
    (alertNotification$: Observable<IAlertNotificationModel>) => {
      return alertNotification$.pipe(
        tap(() => this.setAlertNotificationSaving(true)),
        map((alertNotification) => {
          return {
            NDAlertNotificationID:
              alertNotification.alertNotificationId === '-1'
                ? ''
                : alertNotification.alertNotificationId,
            AlertStatusTypeID: alertNotification.alertStatusTypeId,
            ModelID: alertNotification.modelId,
            NotificationFrequency: alertNotification.notificationFrequency,
            NotificationTitle: String(alertNotification.defaultMessage),
            NotificationSummary: alertNotification.customMessage,
            NotificationMode: true,
          };
        }),
        switchMap((alertNotification) =>
          this.alertsFrameworkService
            .saveAlertNotification(alertNotification)
            .pipe(
              tapResponse(
                (response) => {
                  if (response) {
                    this.setAlertNotificationSaving(false);
                    this.setAlertNotificationModelSaved(true);

                    this.toastService.openSnackBar(
                      'Alert Notification saved',
                      'success'
                    );

                    this.resetAlertNotificationModel();
                  }
                },
                (e: unknown) => {
                  this.toastService.openSnackBar(
                    'Alert Notification save failed',
                    'error'
                  );
                  this.setAlertNotificationSaving(false);
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly alertNotificationSubscribeUsers = this.store.effect(
    (
      params$: Observable<{
        alertStatusTypeId: number;
        modelId: number;
        subscribers: string[];
      }>
    ) => {
      return params$.pipe(
        switchMap((params) =>
          this.alertsFrameworkService
            .alertNotificationSubscribeUsers(
              params.alertStatusTypeId,
              params.modelId,
              params.subscribers
            )
            .pipe(
              tapResponse(
                (response) => {
                  if (response.ErrorMessage) {
                    this.toastService.openSnackBar(
                      'Failed to subscribe users for the alert notification',
                      'error'
                    );
                  }
                },
                (e: unknown) => {
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly alertNotificationUnsubscribeUsers = this.store.effect(
    (
      params$: Observable<{
        alertStatusTypeId: number;
        modelId: number;
        subscribers: string[];
      }>
    ) => {
      return params$.pipe(
        switchMap((params) =>
          this.alertsFrameworkService
            .alertNotificationUnsubscribeUsers(
              params.alertStatusTypeId,
              params.modelId,
              params.subscribers
            )
            .pipe(
              tapResponse(
                (response) => {
                  if (response.ErrorMessage) {
                    this.toastService.openSnackBar(
                      'Failed to subscribe users for the alert notification',
                      'error'
                    );
                  }
                },
                (e: unknown) => {
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly deleteAlertNotification = this.store.effect(
    (params$: Observable<{ modelId: number; alertNotificationId: string }>) => {
      return params$.pipe(
        switchMap((params) =>
          this.alertsFrameworkService
            .deleteAlertNotification(params.modelId, params.alertNotificationId)
            .pipe(
              tapResponse(
                (response) => {
                  if (response) {
                    this.toastService.openSnackBar(
                      'Alert Notification deleted',
                      'success'
                    );

                    this.setAlertNotificationModelDeleted(true);
                  }
                },
                (e: unknown) => {
                  this.toastService.openSnackBar(
                    'Alert Notification save failed',
                    'error'
                  );
                  this.setAlertNotificationSaving(false);
                  console.error(e);
                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  //STORE UPDATERS
  readonly resetModelEditState = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      const newState = initialModelEditState;
      return newState;
    }
  );

  readonly opModeApiCallComplete = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        opModes: LegacyOpModeDefinition[];
        buildDates: Date[];
        saveDates: Date[];
      }
    ): ModelEditState => {
      let showWarning = false;
      const modelType = this.getModelType(state);
      //forecast modals are excluded from the last build date check
      if (
        payload.buildDates &&
        payload.buildDates.length > 0 &&
        !isNil(payload.buildDates[0]) &&
        modelType !== 'forecast'
      ) {
        const currentBuildDate = payload.buildDates[0];
        payload.opModes.forEach((opMode) => {
          if (moment(opMode.ChangeDate).isAfter(currentBuildDate)) {
            showWarning = true;
          }
        });
      }

      if (
        payload.saveDates &&
        payload.saveDates.length > 0 &&
        !isNil(payload.saveDates[0])
      ) {
        const currentSaveDate = payload.saveDates[0];
        payload.opModes.forEach((opMode) => {
          if (moment(opMode.ChangeDate).isAfter(currentSaveDate)) {
            showWarning = true;
          }
        });
      }

      if (showWarning) {
        this.toastService.openSnackBar(
          'This model has a related op mode that has changed since its last build or save date and may not reflect the latest criteria defined.  Save this model to reflect the latest op mode criteria.  See the "Context" tab for details.',
          'warning',
          10000,
          ['warning', 'op-mode']
        );
      }
      const newOpModeObject: OpMode[] = [];
      let startupPriority: OpMode | null = null;
      let transientPriority: OpMode | null = null;
      let ooServicePriority: OpMode | null = null;

      payload.opModes.forEach((opMode: LegacyOpModeDefinition) => {
        if (opMode.OpModeType.OpModeTypeAbbrev === 'Exclusion Period') {
          const logicString = `[${moment(
            opMode.StartCriteria[0].Logic[0].LogicStartDate
          ).format('MM/DD/YYYY hh:mm A')} to ${moment(
            opMode.StartCriteria[0].Logic[0].LogicEndDate
          ).format('MM/DD/YYYY hh:mm A')}]`;
          const newOpMode: OpMode = {
            opModeDefinitionExtID: opMode.OpModeDefinitionExtID,
            changeDate: opMode.ChangeDate,
            title: opMode.OpModeDefinitionTitle,
            priority: opMode.Priority,
            logicString,
            typeAbbrev: opMode.OpModeType.OpModeTypeAbbrev,
          };
          newOpModeObject.push(newOpMode);
        } else {
          const criteriaGroups = groupByKey(
            opMode.StartCriteria,
            'CriteriaGroupID'
          );
          let logicString = '';
          if (criteriaGroups?.length > 1) {
            criteriaGroups.forEach((criteriaGroup: unknown, i: number) => {
              if (i > 0) {
                logicString += ' OR ';
              }
              const logicObjects = (criteriaGroup as LegacyCriteriaObject[])[0]
                .Logic;
              logicString += '[';
              logicObjects.forEach(
                (logicObject: LegacyLogicObject, j: number) => {
                  if (j > 0) {
                    logicString += ' AND ';
                  }
                  logicString += `${logicObject.AssetVariableTypeTagMap?.Tag?.TagName} ${logicObject?.LogicOperatorType?.LogicOperatorTypeAbbrev} ${logicObject.Value}`;
                }
              );
              logicString += ']';
            });
          } else {
            const logicObjects = opMode.StartCriteria[0].Logic;
            logicString += '[';
            logicObjects.forEach(
              (logicObject: LegacyLogicObject, j: number) => {
                if (j > 0) {
                  logicString += ' AND ';
                }
                logicString += `${logicObject.AssetVariableTypeTagMap?.Tag?.TagName} ${logicObject?.LogicOperatorType?.LogicOperatorTypeAbbrev} ${logicObject.Value}`;
              }
            );
            logicString += ']';
          }
          if (opMode.OpModeType.OpModeTypeAbbrev === 'Startup') {
            if (
              !startupPriority ||
              startupPriority?.priority > opMode.Priority
            ) {
              startupPriority = {
                opModeDefinitionExtID: opMode.OpModeDefinitionExtID,
                changeDate: opMode.ChangeDate,
                title: opMode.OpModeDefinitionTitle,
                priority: opMode.Priority,
                logicString,
                typeAbbrev: opMode.OpModeType.OpModeTypeAbbrev,
              };
            }
          } else if (opMode.OpModeType.OpModeTypeAbbrev === 'Transient') {
            if (
              !transientPriority ||
              transientPriority?.priority > opMode.Priority
            ) {
              transientPriority = {
                opModeDefinitionExtID: opMode.OpModeDefinitionExtID,
                changeDate: opMode.ChangeDate,
                title: opMode.OpModeDefinitionTitle,
                priority: opMode.Priority,
                logicString,
                typeAbbrev: opMode.OpModeType.OpModeTypeAbbrev,
              };
            }
          } else if (opMode.OpModeType.OpModeTypeAbbrev === 'Out of Service') {
            if (
              !ooServicePriority ||
              ooServicePriority?.priority > opMode.Priority
            ) {
              ooServicePriority = {
                opModeDefinitionExtID: opMode.OpModeDefinitionExtID,
                changeDate: opMode.ChangeDate,
                title: opMode.OpModeDefinitionTitle,
                priority: opMode.Priority,
                logicString,
                typeAbbrev: opMode.OpModeType.OpModeTypeAbbrev,
              };
            }
          }
        }
      });
      if (startupPriority) {
        newOpModeObject.unshift(startupPriority);
      }
      if (transientPriority) {
        newOpModeObject.unshift(transientPriority);
      }
      if (ooServicePriority) {
        newOpModeObject.unshift(ooServicePriority);
      }
      return {
        ...state,
        opModes: newOpModeObject,
      };
    }
  );

  readonly modelExecuteSaveActionsComplete = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        retValue: any;
        userPressedCancel: boolean;
        isMulti: boolean;
        modelExtID: string | null;
      }
    ): ModelEditState => {
      if (payload.userPressedCancel) {
        return {
          ...state,
          modelSaving: false,
        };
      }
      if (this.logging) {
        console.log(`${payload.modelExtID}: model save complete.`);
        console.log(
          `${payload.modelExtID}: notificationRequired: ${payload.retValue?.NotificationRequired}`
        );
      }
      if (payload.retValue?.NotificationRequired === false) {
        if (payload.isMulti) {
          if (payload.modelExtID) {
            const modelSummary = {
              ...state.entities[payload.modelExtID],
            };
            if (this.logging) {
              console.log(
                `${payload.modelExtID}: save complete. No Build Required.`
              );
            }

            modelSummary.liveStatus = 'Save Complete. No Build Required.';
            const updatedData = {
              id: payload.modelExtID,
              changes: {
                ...modelSummary,
              },
            } as Update<IModelConfigData>;
            const needToReloadCharts =
              state.selectedModelId === payload.modelExtID ? true : false;
            const modelRefreshAction = produce(
              state.modelRefreshAction,
              (draftState: ModelRefreshAction) => {
                draftState.reloadModel = true;
                draftState.modelExtID = payload.modelExtID as string;
                draftState.reloadPredictiveMethodsNeeded = needToReloadCharts;
              }
            ) as ModelRefreshAction;
            return adapter.updateOne(updatedData, {
              ...state,
              modelRefreshAction,
            });
          }
        } else {
          this.toastService.openSnackBar(
            'Save Complete. No Build Required.',
            'success'
          );
        }
        const modelRefreshAction = produce(
          state.modelRefreshAction,
          (draftState: ModelRefreshAction) => {
            draftState.reloadModel = true;
            draftState.reloadPredictiveMethodsNeeded = false;
          }
        ) as ModelRefreshAction;

        return {
          ...state,
          modelSaving: true,
          modelRefreshAction,
        };
      }
      if (
        !isNil(payload?.retValue?.ExecutionIncompleteMessage) &&
        payload?.retValue?.ExecutionIncompleteMessage !== ''
      ) {
        const errorMessage = `Error: ${payload?.retValue?.ExecutionIncompleteMessage}`;
        if (this.logging) {
          console.log(
            `${payload.modelExtID}: Error: ${payload?.retValue?.ExecutionIncompleteMessage}`
          );
          console.log(JSON.stringify(payload));
        }
        // todo, is multi
        if (payload.isMulti) {
          if (payload.modelExtID) {
            const modelSummary = {
              ...state.entities[payload.modelExtID],
            };
            modelSummary.liveStatus = 'Error. ' + errorMessage;
            const updatedData = {
              id: payload.modelExtID,
              changes: {
                ...modelSummary,
              },
            } as Update<IModelConfigData>;
            return adapter.updateOne(updatedData, state);
          }
        } else {
          this.toastService.openSnackBar(errorMessage, 'error');
          const modelRefreshAction = produce(
            state.modelRefreshAction,
            (draftState: ModelRefreshAction) => {
              draftState.reloadModel = true;
              draftState.reloadPredictiveMethodsNeeded = false;
            }
          ) as ModelRefreshAction;

          return {
            ...state,
            modelSaving: false,
            modelRefreshAction,
            selectedModelBuildStatus: '',
          };
        }
      }
      if (payload.isMulti) {
        if (this.logging) {
          console.log(`${payload.modelExtID}: model sent to ml engine.`);
        }
        if (payload.modelExtID) {
          const modelSummary = {
            ...state.entities[payload.modelExtID],
          };
          modelSummary.liveStatus = 'Model sent to ML Engine.';
          const updatedData = {
            id: payload.modelExtID,
            changes: {
              ...modelSummary,
            },
          } as Update<IModelConfigData>;

          return adapter.updateOne(updatedData, state);
        } else {
          return state;
        }
      } else {
        const newMessage =
          'Model sent to ML Engine.\n' +
          '==============================================================================';

        return {
          ...state,
          modelSaving: true,
          selectedModelBuildStatus: newMessage,
        };
      }
    }
  );

  readonly legacyEvaluateError = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        message: string;
        modelExtID: string;
      }
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[payload.modelExtID],
      };
      modelSummary.liveStatus = 'Error. ' + payload.message;
      const updatedData = {
        id: payload.modelExtID,
        changes: {
          ...modelSummary,
        },
      } as Update<IModelConfigData>;
      if (this.logging) {
        console.log(`${payload.modelExtID}: error. ${payload.message}`);
      }
      return adapter.updateOne(updatedData, state);
    }
  );

  readonly singleModelValidationReturnComplete = this.store.updater(
    (
      state: ModelEditState,
      payload: { modelValidationReturn: any }
    ): ModelEditState => {
      const validationReturn = legacyValidationReturn(
        payload.modelValidationReturn
      );
      if (validationReturn.error) {
        this.toastService.openSnackBar(validationReturn.message, 'error');
        state.modelSaving = false;
        return state;
      }
      state.modelValidationReturn = payload.modelValidationReturn;
      return state;
    }
  );

  readonly singleModelSignalRMessage = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        signalRMessage: string;
      }
    ): ModelEditState => {
      const breaks =
        '==============================================================================';
      let selectedModelBuildStatus = state?.selectedModelBuildStatus || '';
      let reloadModel = false;
      let modelSaving = state.modelSaving;
      if (payload.signalRMessage.startsWith('Model build failed')) {
        selectedModelBuildStatus += '\n' + payload.signalRMessage;
        selectedModelBuildStatus +=
          '\n' + breaks + '\n' + 'Build completed with errors.';
        modelSaving = false;
        reloadModel = true;
      } else if (payload.signalRMessage.startsWith('Failure')) {
        selectedModelBuildStatus += '\n' + payload.signalRMessage;
        selectedModelBuildStatus +=
          '\n' + breaks + '\n' + 'Build completed with errors.';
        modelSaving = false;
        reloadModel = true;
      } else if (payload.signalRMessage === 'Built') {
        selectedModelBuildStatus += '\n' + breaks + '\n' + 'Build complete.';
        modelSaving = false;
        reloadModel = true;
      } else {
        selectedModelBuildStatus += '\n' + payload.signalRMessage;
      }
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (reloadModel) {
            draftState.reloadModel = true;
            draftState.reloadPredictiveMethodsNeeded = false;
          }
        }
      ) as ModelRefreshAction;
      return {
        ...state,
        modelRefreshAction,
        modelSaving,
        selectedModelBuildStatus,
      };
    }
  );

  readonly loadSummaries = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        modelConfigSummaries: IModelConfigData[];
        refreshTrends: boolean;
        appendBuildStatus: boolean;
      }
    ): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (payload.refreshTrends) {
            // after this is done we want to refresh the trends as well
            draftState.reloadModel = false;
            draftState.reloadPredictiveMethodsNeeded = true;
          }
        }
      ) as ModelRefreshAction;

      const appendTrends =
        state.selectedModelBuildStatus &&
        state.selectedModelBuildStatus.length > 0
          ? true
          : false;
      const breaks =
        '==============================================================================';

      const appendTrendMessage = appendTrends
        ? payload.appendBuildStatus
          ? state.selectedModelBuildStatus +
              '\n' +
              breaks +
              '\n' +
              'Last Build Status: ' +
              payload.modelConfigSummaries[0]?.buildProperties?.status
                ?.buildState || ''
          : state.selectedModelBuildStatus
        : '';

      const hasSamePdServer: boolean = payload.modelConfigSummaries.every(
        (s) =>
          s.dependent.processDataServerGuid ===
          payload.modelConfigSummaries[0].dependent.processDataServerGuid
      );

      const newState: ModelEditState = {
        ...state,
        updateFormElements: true,
        selectedModelId: payload.modelConfigSummaries[0].modelExtID,
        selectedModelBuildStatus: appendTrends
          ? appendTrendMessage
          : payload.modelConfigSummaries[0]?.buildProperties?.status
              ?.buildState || '',
        modelSummaryLoading: false,
        modelSaving: false,
        modelRefreshAction,
        hasSamePdServer: hasSamePdServer,
        isNewModel: payload.modelConfigSummaries[0].legacy.modelID < 1,
      };
      return adapter.upsertMany(payload.modelConfigSummaries, newState);
    }
  );

  readonly setMathMessage = this.store.updater(
    (state: ModelEditState, mathMessage: string): ModelEditState => {
      return {
        ...state,
        modelMathMessage: mathMessage || '',
      };
    }
  );

  readonly updateModelSummary = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = state.entities[modelSummaryUpdate.modelExtID];
      const newModelSummary = produce(modelSummary, (draftState: any) => {
        // This will transform the column field to an object path then update the value
        // Ex. ComputedCriticality.DefaultUpperBound => ComputedCriticality: { DefaultUpperBound: newValue }
        const columnFields = modelSummaryUpdate.columnField.split('.');
        columnFields.reduce(
          (p, c, i) =>
            (p[c] =
              columnFields.length === ++i
                ? modelSummaryUpdate.newValue
                : p[c] || {}),
          draftState
        );
        const columnFieldsJson = JSON.stringify(columnFields) || '';
        if (
          columnFieldsJson ===
            JSON.stringify([
              'relativeBoundsAnomaly',
              'properties',
              'upperMultiplier',
            ]) ||
          columnFieldsJson ===
            JSON.stringify([
              'relativeBoundsAnomaly',
              'properties',
              'lowerMultiplier',
            ]) ||
          columnFieldsJson ===
            JSON.stringify([
              'relativeBoundsAnomaly',
              'properties',
              'upperBias',
            ]) ||
          columnFieldsJson ===
            JSON.stringify([
              'relativeBoundsAnomaly',
              'properties',
              'lowerBias',
            ]) ||
          columnFieldsJson ===
            JSON.stringify([
              'upperFixedLimitAnomaly',
              'properties',
              'upperFixedLimit',
            ]) ||
          columnFieldsJson ===
            JSON.stringify([
              'lowerFixedLimitAnomaly',
              'properties',
              'lowerFixedLimit',
            ])
        ) {
          draftState.reloadTrends = true;
        }

        setModelNonStandard(draftState);
        draftState.isDirty = true;
        // This will update the dependent column of the edited column field when value is 'Yes'
        // This will use the default value of the dependent column
        if (
          ModelSummaryDependentMapping[modelSummaryUpdate.columnField] &&
          modelSummaryUpdate.newValue === 'Yes'
        ) {
          const dependentFields =
            ModelSummaryDependentMapping[modelSummaryUpdate.columnField].split(
              '.'
            );
          dependentFields.reduce(
            (p, c, i) =>
              (p[c] =
                dependentFields.length === ++i
                  ? dependentFields.reduce(
                      (p, c) => (p && p[c]) || null,
                      state.entities[modelSummaryUpdate.modelExtID] as any
                    )
                  : p[c] || {}),
            draftState
          );
        }
      }) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly addModelInputTags = this.store.updater(
    (
      state: ModelEditState,
      modelInputs: IModelIndependent[]
    ): ModelEditState => {
      const modelSummary = { ...state.entities[state.selectedModelId] };
      if (!modelSummary) {
        return state;
      }

      const newModelSummary = produce(modelSummary, (draftState) => {
        modelInputs.map((modelInput) => {
          const dependentTagGuid =
            state.entities[state.selectedModelId]?.dependent?.tagGuid;
          if (dependentTagGuid === modelInput?.tagGuid) {
            this.toastService.openSnackBar(
              `The dependent tag (${modelInput.tagName}) cannot be used as an input.`,
              'warning'
            );
          } else {
            const existingTagIdx =
              draftState?.independents?.findIndex(
                (x) => x.tagGuid === modelInput.tagGuid
              ) ?? -1;

            if (existingTagIdx >= 0) {
              this.toastService.openSnackBar(
                `The model is already configured to use that tag (${modelInput.tagName}).`,
                'warning'
              );
            } else {
              if (draftState.independents) {
                draftState.independents.push(modelInput);
                setModelNonStandard(draftState as IModelConfigData);
                draftState.isDirty = true;
              }
            }
          }
        });
      }) as IModelConfigData;
      const updatedData = {
        id: state.entities[state.selectedModelId]?.modelExtID,
        changes: {
          ...newModelSummary,
        },
      } as Update<IModelConfigData>;
      return adapter.updateOne(updatedData, state);
    }
  );

  readonly updateFormComplete = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      return {
        ...state,
        updateFormElements: false,
      };
    }
  );

  readonly discardChanges = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      const selectedAnomalyChip: SelectedChip = {
        type: 'anomaly',
        subType: SensitivityAnomalyTypes.All,
      };
      const selectedAlertChip: SelectedChip = {
        type: 'alert',
        subType: SensitivityAlertTypes.All,
      };

      return adapter.removeAll({
        ...state,
        selectedModelBuildStatus: '',
        selectedAlertChip,
        selectedAnomalyChip,
        modelTrendData: null,
      });
    }
  );

  readonly removeModelInput = this.store.updater(
    (state: ModelEditState, modelInputIndex: number): ModelEditState => {
      if (
        state?.entities &&
        state.entities[state.selectedModelId]?.independents
      ) {
        const modelSummary = {
          ...state.entities[state.selectedModelId],
        } as IModelConfigData;
        const newModelSummary = produce(modelSummary, (draftState) => {
          draftState?.independents.splice(modelInputIndex, 1);
          setModelNonStandard(draftState);
          draftState.isDirty = true;
        }) as IModelConfigData;

        const updatedData = {
          id: state.entities[state.selectedModelId]?.modelExtID,
          changes: {
            ...newModelSummary,
          },
        } as Update<IModelConfigData>;

        return adapter.updateOne(updatedData, state);
      } else return state;
    }
  );

  readonly forcedInclusionToggle = this.store.updater(
    (state: ModelEditState, modelInputIndex: number): ModelEditState => {
      if (
        state?.entities &&
        state.entities[state.selectedModelId]?.independents
      ) {
        const modelSummary = {
          ...state.entities[state.selectedModelId],
        } as IModelConfigData;

        const newModelSummary = produce(modelSummary, (draftState) => {
          if (draftState?.independents[modelInputIndex]) {
            draftState.independents[modelInputIndex].forcedInclusion =
              !draftState.independents[modelInputIndex].forcedInclusion;
            draftState.isDirty = true;
          }
        });
        const updatedData = {
          id: state.entities[state.selectedModelId]?.modelExtID,
          changes: {
            ...newModelSummary,
          },
        } as Update<IModelConfigData>;

        return adapter.updateOne(updatedData, state);
      } else return state;
    }
  );

  readonly setModelActiveInactive = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      if (
        state?.entities &&
        state.entities[state.selectedModelId]?.independents
      ) {
        const modelSummary = {
          ...state.entities[state.selectedModelId],
        } as IModelConfigData;

        const newModelSummary = produce(modelSummary, (draftState) => {
          draftState.active = !modelSummary.active;
          draftState.isDirty = true;
        });
        const updatedData = {
          id: state.entities[state.selectedModelId]?.modelExtID,
          changes: {
            ...newModelSummary,
          },
        } as Update<IModelConfigData>;

        return adapter.updateOne(updatedData, state);
      } else return state;
    }
  );
  readonly setAnomaly = this.store.updater(
    (
      state: ModelEditState,
      selectedAnomalyChip: SelectedChip
    ): ModelEditState => {
      return {
        ...state,
        selectedAnomalyChip,
      };
    }
  );

  readonly setAlert = this.store.updater(
    (
      state: ModelEditState,
      selectedAlertChip: SelectedChip
    ): ModelEditState => {
      return {
        ...state,
        selectedAlertChip,
      };
    }
  );

  readonly setModelName = this.store.updater(
    (state: ModelEditState, value: string): ModelEditState => {
      const modelSummary = {
        ...state.entities[state.selectedModelId],
      } as IModelConfigData;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.name = value;
          draftState.isDirty = true;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setOpModeTypes = this.store.updater(
    (state: ModelEditState, opModeTypes: EOpModeTypes[]): ModelEditState => {
      const modelSummary = {
        ...state.entities[state.selectedModelId],
      } as IModelConfigData;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.properties.opModeTypes = opModeTypes;
          draftState.isDirty = true;
        }
      );
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setNewModelType = this.store.updater(
    (state: ModelEditState, modelType: EModelTypes): ModelEditState => {
      const modelSummary = {
        ...state.entities[state.selectedModelId],
      } as IModelConfigData;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.properties.modelType.type = modelType;
          draftState.isDirty = true;
        }
      );
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setPredictiveMethodType = this.store.updater(
    (state: ModelEditState, methodType: EPredictiveTypes): ModelEditState => {
      const modelSummary = {
        ...state.entities[state.selectedModelId],
      } as IModelConfigData;

      if (modelSummary?.properties.predictiveTypeSelected === methodType) {
        return state;
      }
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.isDirty = true;
          draftState.properties.predictiveTypeSelected = methodType;
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setLowerFixedLimitAnomalyEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const lowerFixedLimitAnomaly = draftState.lowerFixedLimitAnomaly
            .properties as ILowerFixedLimitAnomalyProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.lowerFixedLimitAnomaly.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            lowerFixedLimitAnomaly.lowerFixedLimit = -1;
            draftState.lowerFixedLimitAnomaly.properties =
              lowerFixedLimitAnomaly;
          } else {
            lowerFixedLimitAnomaly.lowerFixedLimit = undefined;
            draftState.lowerFixedLimitAnomaly.properties =
              lowerFixedLimitAnomaly;
          }
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setUpperFixedLimitAnomalyEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const upperFixedLimitAnomaly = draftState.upperFixedLimitAnomaly
            .properties as IUpperFixedLimitAnomalyProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.upperFixedLimitAnomaly.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            upperFixedLimitAnomaly.upperFixedLimit = 1;
            draftState.upperFixedLimitAnomaly.properties =
              upperFixedLimitAnomaly;
          } else {
            upperFixedLimitAnomaly.upperFixedLimit = undefined;
            draftState.upperFixedLimitAnomaly.properties =
              upperFixedLimitAnomaly;
          }
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly updateProjectedTrainingRange = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;
      if (
        modelSummary &&
        modelSummary?.training &&
        !isNil(modelSummary.training?.duration) &&
        modelSummary.training.durationUnitOfTime
      ) {
        const trainingEndDate =
          moment(moment()).toDate().getTime() -
          transformValueToMilliseconds(
            modelSummary.training.lag || 0,
            modelSummary.training.lagUnitOfTime || EUnitOfTime.Seconds
          );
        const trainingStartDate = modelSummary.training.duration
          ? trainingEndDate -
            transformValueToMilliseconds(
              modelSummary.training.duration,
              modelSummary.training.durationUnitOfTime
            )
          : trainingEndDate;

        const newModelSummary = produce(
          modelSummary,
          (draftState: IModelConfigData) => {
            draftState.projectedTrainingRange = `${moment(
              trainingStartDate
            ).format('MM/DD/YYYY')} - ${moment(trainingEndDate).format(
              'MM/DD/YYYY'
            )}`;
          }
        ) as IModelConfigData;
        return adapter.upsertOne(newModelSummary, state);
      } else {
        return state;
      }
    }
  );

  readonly setHighHighAlertEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const highHighAlertProperties = draftState.highHighAlert
            .properties as IHighHighAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.highHighAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            highHighAlertProperties.threshold = 40;
            highHighAlertProperties.useMeanAbsoluteError = true;
            draftState.highHighAlert.properties = highHighAlertProperties;
          } else {
            highHighAlertProperties.threshold = undefined;
            highHighAlertProperties.useMeanAbsoluteError = undefined;
            draftState.highHighAlert.properties = highHighAlertProperties;
          }
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setAnomalyAreaAlertEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const anomalyAreaAlertProperties = draftState.lowLowAlert
            .properties as IAnomalyAreaAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.anomalyAreaAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            anomalyAreaAlertProperties.areaFastResponse = 1;
            anomalyAreaAlertProperties.areaFastResponsePeriodUnitOfTime =
              EUnitOfTime.Minutes;
            anomalyAreaAlertProperties.areaFastResponsePeriod = 1;
            anomalyAreaAlertProperties.areaSlowResponse = 20;
            anomalyAreaAlertProperties.areaSlowResponsePeriod = 20;
            anomalyAreaAlertProperties.areaSlowResponsePeriodUnitOfTime =
              EUnitOfTime.Minutes;
            draftState.anomalyAreaAlert.properties = anomalyAreaAlertProperties;
          } else {
            anomalyAreaAlertProperties.areaFastResponse = undefined;
            anomalyAreaAlertProperties.areaFastResponsePeriodUnitOfTime =
              undefined;
            anomalyAreaAlertProperties.areaFastResponsePeriod = undefined;
            anomalyAreaAlertProperties.areaSlowResponse = undefined;
            anomalyAreaAlertProperties.areaSlowResponsePeriod = undefined;
            anomalyAreaAlertProperties.areaSlowResponsePeriodUnitOfTime =
              undefined;
            draftState.anomalyAreaAlert.properties = anomalyAreaAlertProperties;
          }
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setFrozenDataCheckAlertEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const lowLowAlertProperties = draftState.frozenDataCheckAlert
            .properties as IFrozenDataAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.frozenDataCheckAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            lowLowAlertProperties.evaluationPeriod = 5;
            lowLowAlertProperties.evaluationPeriodUnitOfTime =
              EUnitOfTime.Hours;
            draftState.lowLowAlert.properties = lowLowAlertProperties;
          } else {
            lowLowAlertProperties.evaluationPeriod = undefined;
            lowLowAlertProperties.evaluationPeriodUnitOfTime = undefined;
            draftState.lowLowAlert.properties = lowLowAlertProperties;
          }
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setLowLowAlertEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const lowLowAlertProperties = draftState.lowLowAlert
            .properties as ILowLowAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.lowLowAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            lowLowAlertProperties.threshold = 40;
            lowLowAlertProperties.useMeanAbsoluteError = true;
            draftState.lowLowAlert.properties = lowLowAlertProperties;
          } else {
            lowLowAlertProperties.threshold = undefined;
            lowLowAlertProperties.useMeanAbsoluteError = undefined;
            draftState.lowLowAlert.properties = lowLowAlertProperties;
          }
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setAnomalyOscillationEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const anomalyOscillationAlertProperties = draftState
            .anomalyOscillationAlert
            .properties as IAnomalyOscillationAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.anomalyOscillationAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            anomalyOscillationAlertProperties.evaluationPeriod = 2;
            anomalyOscillationAlertProperties.evaluationPeriodUnitOfTime =
              EUnitOfTime.Hours;
            anomalyOscillationAlertProperties.threshold = 80;
            draftState.anomalyOscillationAlert.properties =
              anomalyOscillationAlertProperties;
          } else {
            anomalyOscillationAlertProperties.evaluationPeriod = undefined;
            anomalyOscillationAlertProperties.evaluationPeriodUnitOfTime =
              undefined;
            anomalyOscillationAlertProperties.threshold = undefined;
            draftState.anomalyOscillationAlert.properties =
              anomalyOscillationAlertProperties;
          }
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setAnomalyFrequencyEnabled = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const anomalyFrequencyAlertProperties = draftState
            .anomalyFrequencyAlert
            .properties as IAnomalyFrequencyAlertProperties;
          const enabled = modelSummaryUpdate.newValue;
          draftState.anomalyFrequencyAlert.enabled = enabled;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (enabled) {
            anomalyFrequencyAlertProperties.evaluationPeriod = 4;
            anomalyFrequencyAlertProperties.evaluationPeriodUnitOfTime =
              EUnitOfTime.Hours;
            anomalyFrequencyAlertProperties.threshold = 70;
            draftState.anomalyFrequencyAlert.properties =
              anomalyFrequencyAlertProperties;
          } else {
            anomalyFrequencyAlertProperties.evaluationPeriod = undefined;
            anomalyFrequencyAlertProperties.evaluationPeriodUnitOfTime =
              undefined;
            anomalyFrequencyAlertProperties.threshold = undefined;
            draftState.anomalyFrequencyAlert.properties =
              anomalyFrequencyAlertProperties;
          }
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setForecastFixedHorizonDateChanged = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newValue = modelSummaryUpdate.newValue;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.fixedEarliestInterceptChanged = true;
          draftState.fixedEarliestInterceptDate = newValue;
          draftState.fixedEarliestInterceptTime = '00:00';
          setModelNonStandard(draftState);
          draftState.isDirty = true;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setForecastFixedHorizonTimeChanged = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newValue = modelSummaryUpdate.newValue;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.fixedEarliestInterceptChanged = true;
          draftState.fixedEarliestInterceptTime = newValue;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setForecastRelativeInterceptTimeSpanUnits = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.fixedEarliestInterceptChanged = true;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          const modelTypeProperties = draftState.properties.modelType
            .properties as IForecastModelProperties;
          const newValue = modelSummaryUpdate.newValue;
          modelTypeProperties.horizonPeriodUnitOfTime = newValue;

          if (modelTypeProperties.horizonPeriodUnitOfTime) {
            draftState.relativeEarliestInterceptDate = moment(
              new Date(),
              'MM/DD/YYYY HH:mm'
            )
              .add(
                modelTypeProperties.horizonPeriod,
                modelTypeProperties.horizonPeriodUnitOfTime.toLowerCase() as unitOfTime.DurationConstructor
              )
              .format('MM/DD/YYYY');
          }

          ((draftState.properties.modelType as IModelTypeProperties)
            .properties as IForecastModelProperties) = modelTypeProperties;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setForecastRelativeInterceptTimeSpanDuration = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.fixedEarliestInterceptChanged = true;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          const modelTypeProperties = draftState.properties.modelType
            ?.properties as IForecastModelProperties;
          const newValue = modelSummaryUpdate.newValue;
          modelTypeProperties.horizonPeriod = newValue;
          if (modelTypeProperties.horizonPeriodUnitOfTime) {
            draftState.relativeEarliestInterceptDate = moment(
              new Date(),
              'MM/DD/YYYY HH:mm'
            )
              .add(
                modelTypeProperties.horizonPeriod,
                modelTypeProperties.horizonPeriodUnitOfTime.toLowerCase() as unitOfTime.DurationConstructor
              )
              .format('MM/DD/YYYY');
          }

          ((draftState.properties.modelType as IModelTypeProperties)
            .properties as IForecastModelProperties) = modelTypeProperties;
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setForecastHorizonType = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          draftState.fixedEarliestInterceptChanged = true;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          const modelTypeProperties = draftState.properties.modelType
            .properties as IForecastModelProperties;
          const newValue = modelSummaryUpdate.newValue;
          if (newValue === 'Relative') {
            draftState.fixedEarliestInterceptDate = '';
            draftState.fixedEarliestInterceptTime = '';

            modelTypeProperties.horizonType = newValue;
            modelTypeProperties.horizonDate = undefined;
            modelTypeProperties.horizonPeriod = 3;
            modelTypeProperties.horizonPeriodUnitOfTime = EUnitOfTime.Months;
            draftState.relativeEarliestInterceptDate = moment(
              new Date(),
              'M/D/YYYY HH:mm'
            )
              .add(3, 'months')
              .format('MM/DD/YYYY');
          } else {
            modelTypeProperties.horizonType = newValue;
            modelTypeProperties.horizonDate = moment(
              new Date(),
              'M/D/YYYY HH:mm'
            )
              .add(3, 'months')
              .format();

            draftState.fixedEarliestInterceptDate = moment(
              modelTypeProperties.horizonDate
            ).format('MM/DD/YYYY');
            draftState.fixedEarliestInterceptTime = '00:00';

            draftState.relativeEarliestInterceptDate = '';
            modelTypeProperties.horizonPeriod = 0;
            modelTypeProperties.horizonPeriodUnitOfTime = null;
          }
          ((draftState.properties.modelType as IModelTypeProperties)
            .properties as IForecastModelProperties) = modelTypeProperties;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setROCExpressionUnits = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;
      const newValue = modelSummaryUpdate.newValue;
      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const tagUnits = modelSummary?.dependent.tagUnits;
          if (tagUnits && tagUnits?.length > 0) {
            const removeTagUnit = newValue.substring(tagUnits.length + 1) + 's';
            const expressionPeriodUnitOfTime =
              removeTagUnit.charAt(0).toUpperCase() + removeTagUnit.slice(1);
            const modelTypeProperties = draftState.properties.modelType
              .properties as IRateOfChangeProperties;
            let unitOfTime = EUnitOfTime.Days;
            switch (expressionPeriodUnitOfTime) {
              case 'Hours':
                unitOfTime = EUnitOfTime.Hours;
                break;
              case 'Days':
                unitOfTime = EUnitOfTime.Days;
                break;
              case 'Weeks':
                unitOfTime = EUnitOfTime.Weeks;
                break;
              case 'Months':
                unitOfTime = EUnitOfTime.Months;
                break;
              case 'Years':
                unitOfTime = EUnitOfTime.Years;
                break;
              default:
                unitOfTime = EUnitOfTime.Days;
            }
            modelTypeProperties.expressionPeriodUnitOfTime = unitOfTime;
            ((draftState.properties.modelType as IModelTypeProperties)
              .properties as IRateOfChangeProperties) = modelTypeProperties;
          }
          setModelNonStandard(draftState);
          draftState.isDirty = true;
        }
      ) as IModelConfigData;
      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly setModelBoundCriticalityUseDefaults = this.store.updater(
    (
      state: ModelEditState,
      modelSummaryUpdate: IModelSummaryUpdate
    ): ModelEditState => {
      const modelSummary = {
        ...state.entities[modelSummaryUpdate.modelExtID],
      } as IModelConfigData;

      const newModelSummary = produce(
        modelSummary,
        (draftState: IModelConfigData) => {
          const criticalityProperties = draftState.criticalityAnomaly
            .properties as ICriticalityAnomalyProperties;
          const useDefault = modelSummaryUpdate.newValue;
          criticalityProperties.useDefault = useDefault;
          setModelNonStandard(draftState);
          draftState.isDirty = true;
          if (useDefault) {
            criticalityProperties.upperBound =
              criticalityProperties.upperBoundDefault;
            criticalityProperties.lowerBound =
              criticalityProperties.lowerBoundDefault;
            draftState.criticalityAnomaly.properties = criticalityProperties;
          }
        }
      ) as IModelConfigData;

      return adapter.upsertOne(newModelSummary, state);
    }
  );

  readonly modelTrendError = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          draftState.reloadPredictiveMethodsNeeded = false;
        }
      ) as ModelRefreshAction;

      return {
        ...state,
        modelTrendData: null,
        modelTrendLoading: false,
        modelRefreshAction,
      };
    }
  );

  readonly loadModelTrend = this.store.updater(
    (
      state: ModelEditState,
      modelTrend: IModelConfigModelTrend
    ): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          draftState.reloadPredictiveMethodsNeeded = false;
        }
      ) as ModelRefreshAction;

      return {
        ...state,
        modelTrendData: modelTrend,
        modelTrendLoading: false,
        modelRefreshAction,
      };
    }
  );

  readonly setModelSaving = this.store.updater(
    (
      state: ModelEditState,
      payload: { modelExtID: string; isMulti: boolean }
    ): ModelEditState => {
      if (payload.isMulti) {
        // No new models in Multi Model Config
        return state;
      }
      if (
        !isNil(payload.modelExtID) &&
        state.selectedModelId !== payload.modelExtID
      ) {
        // if the modelExtID is -1, this will create a new model
        // from the original model with the new ModelExtID returned
        // from the API.
        const modelSummary = {
          ...state.entities[state.selectedModelId],
        } as IModelConfigData;
        const newModelSummary = produce(
          modelSummary,
          (draftState: IModelConfigData) => {
            draftState.modelExtID = payload.modelExtID;
          }
        );
        return adapter.upsertOne(newModelSummary, state);
      }
      return state;
    }
  );

  readonly initializeModelSaving = this.store.updater(
    (state: ModelEditState, isMulti: boolean): ModelEditState => {
      if (isMulti) {
        return state;
      }
      return {
        ...state,
        modelSaving: true,
      };
    }
  );

  readonly setModelSaveComplete = this.store.updater(
    (
      state: ModelEditState,
      saveValues: {
        isMulti: boolean;
        message: string;
        modelExtID: string | null;
      }
    ): ModelEditState => {
      if (saveValues.isMulti) {
        if (saveValues.modelExtID) {
          const modelSummary = {
            ...state.entities[saveValues.modelExtID],
          };
          modelSummary.liveStatus = saveValues.message;
          const updatedData = {
            id: saveValues.modelExtID,
            changes: {
              ...modelSummary,
            },
          } as Update<IModelConfigData>;
          return adapter.updateOne(updatedData, state);
        } else {
          return state;
        }
      }
      const errorMessage = saveValues.message || 'a model save error occurred';
      this.toastService.openSnackBar(errorMessage, 'error');
      return {
        ...state,
        modelSaving: false,
      };
    }
  );

  readonly setModelLoading = this.store.updater(
    (state: ModelEditState, isLoading: boolean): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (isLoading) {
            draftState.reloadModel = false;
          }
        }
      ) as ModelRefreshAction;
      return {
        ...state,
        modelRefreshAction,
        modelSummaryLoading: isLoading,
      };
    }
  );

  readonly setModelTrendLoading = this.store.updater(
    (state: ModelEditState): ModelEditState => ({
      ...state,
      modelTrendData: null,
      modelTrendLoading: true,
    })
  );

  readonly setModelHistoryReload = this.store.updater(
    (state: ModelEditState, reloadModelHistory: boolean): ModelEditState => ({
      ...state,
      modelHistoryReload: reloadModelHistory,
    })
  );

  readonly toggleModelActionsPane = this.store.updater(
    (
      state: ModelEditState,
      toggleModelActionsPane: boolean
    ): ModelEditState => {
      return {
        ...state,
        toggleModelActionsPane,
      };
    }
  );

  readonly modelSelectionChange = this.store.updater(
    (state: ModelEditState, modelExtId: string): ModelEditState => {
      return {
        ...state,
        selectedModelId: modelExtId,
      };
    }
  );

  readonly updateModelExtID = this.store.updater(
    (
      state: ModelEditState,
      payload: { modelExtID: string; isMulti: boolean }
    ): ModelEditState => {
      if (payload.isMulti) {
        // We do not allow new models in Multi Model currently
        return state;
      }
      if (state.selectedModelId !== payload.modelExtID) {
        const removeID = state.selectedModelId;
        // if the modelExtID is -1 or 0, lets change the selectedModelId and reload.
        return adapter.removeOne(removeID, {
          ...state,
          selectedModelId: payload.modelExtID,
        });
      }
      return state;
    }
  );

  readonly setModelContextLoading = this.store.updater(
    (state: ModelEditState): ModelEditState => ({
      ...state,
      modelContextData: null,
      modelContextLoading: true,
      modelContextDataLoading: false,
    })
  );

  readonly setModelContextDataLoading = this.store.updater(
    (state: ModelEditState): ModelEditState => ({
      ...state,
      modelContextLoading: false,
      modelContextDataLoading: true,
    })
  );

  readonly loadModelContext = this.store.updater(
    (
      state: ModelEditState,
      {
        trend,
        measurementsSet,
        startDate,
        endDate,
        correlationData,
      }: {
        trend: IProcessedTrend;
        measurementsSet: IAssetMeasurementsSet;
        startDate: Date;
        endDate: Date;
        correlationData: ICorrelationData[];
      }
    ): ModelEditState => {
      const newTrend = processModelContextTrend(
        trend,
        measurementsSet,
        startDate,
        endDate
      );

      return {
        ...state,
        modelContextData: newTrend,
        modelContextLoading: false,
        modelContextDataLoading: false,
        correlationData: correlationData,
      };
    }
  );

  readonly setModelContext = this.store.updater(
    (
      state: ModelEditState,
      { trend }: { trend: IProcessedTrend }
    ): ModelEditState => {
      return {
        ...state,
        modelContextData: trend,
      };
    }
  );

  readonly setShowNewModelDialog = this.store.updater(
    (state: ModelEditState, showNewModelDialog: boolean): ModelEditState => {
      return {
        ...state,
        showNewModelDialog: showNewModelDialog,
      };
    }
  );

  readonly setModelTemplatesLoading = this.store.updater(
    (state: ModelEditState, isLoading: boolean): ModelEditState => ({
      ...state,
      modelTemplates: null,
      modelTemplatesLoading: isLoading,
    })
  );

  readonly loadModelTemplates = this.store.updater(
    (
      state: ModelEditState,
      modelTemplates: IModelConfigData[]
    ): ModelEditState => {
      const newState = produce(state, (draftState: ModelEditState) => {
        draftState.modelTemplates = modelTemplates;
        draftState.modelTemplatesLoading = false;
      }) as ModelEditState;

      this.loadSummaries({
        modelConfigSummaries: modelTemplates,
        refreshTrends: true,
        appendBuildStatus: false,
      });

      return newState;
    }
  );

  readonly reloadModels = this.store.updater(
    (state: ModelEditState): ModelEditState => ({
      ...state,
      reloadModels: true,
    })
  );

  readonly loadAlertNotification = this.store.updater(
    (
      state: ModelEditState,
      alertNotificationModel: IAlertNotificationModel
    ): ModelEditState => {
      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setUserGroupMembers = this.store.updater(
    (
      state: ModelEditState,
      params: { userName: string; members: ISubscriber[] }
    ): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          const filteredUserIdx =
            draftState.filteredUsers?.findIndex(
              (x) => x.UserName === params.userName
            ) ?? -1;
          const userIdx =
            draftState.users?.findIndex(
              (x) => x.UserName === params.userName
            ) ?? -1;
          const recipientsIdx =
            draftState.recipients?.findIndex(
              (x) => x.UserName === params.userName
            ) ?? -1;
          if (filteredUserIdx > -1) {
            const filteredUser = draftState.filteredUsers?.find(
              (x) => x.UserName === params.userName
            ) as ISubscriber;

            filteredUser.groupMembers = params.members;
            draftState.filteredUsers?.splice(filteredUserIdx, 1, filteredUser);
          }

          if (userIdx > -1) {
            const user = draftState.users?.find(
              (x) => x.UserName === params.userName
            ) as ISubscriber;

            user.groupMembers = params.members;
            draftState.users?.splice(userIdx, 1, user);
          }

          if (recipientsIdx > -1) {
            const recipient = draftState.recipients?.find(
              (x) => x.UserName === params.userName
            ) as ISubscriber;

            recipient.groupMembers = params.members;
            draftState.recipients?.splice(recipientsIdx, 1, recipient);
          }

          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly addAlertNotificationRecipient = this.store.updater(
    (state: ModelEditState, user: ISubscriber): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          if (!draftState.recipients) {
            draftState.recipients = [];
          }

          const idx =
            draftState.recipients?.findIndex(
              (x) => x.SecurityUserID === user.SecurityUserID
            ) ?? -1;

          if (idx === -1) {
            draftState.recipients.push(user);
          }

          const filteredUserIdx =
            draftState.filteredUsers?.findIndex(
              (x) => x.SecurityUserID === user.SecurityUserID
            ) ?? -1;

          if (filteredUserIdx > -1) {
            draftState.filteredUsers?.splice(filteredUserIdx, 1);
          }

          if (draftState.alertNotificationId !== '-1') {
            const unsubscribedUserIdx =
              draftState.unsubscribedUsers?.findIndex(
                (x) => x.SecurityUserID === user.SecurityUserID
              ) ?? -1;

            if (unsubscribedUserIdx > -1) {
              draftState.unsubscribedUsers?.splice(unsubscribedUserIdx, 1);
            }
          }

          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly removeAlertNotificationRecipient = this.store.updater(
    (state: ModelEditState, user: ISubscriber): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          const idx =
            draftState.recipients?.findIndex(
              (x) => x.SecurityUserID === user.SecurityUserID
            ) ?? -1;

          if (idx > -1) {
            draftState.recipients?.splice(idx, 1);
          }

          const filteredUserIdx =
            draftState.filteredUsers?.findIndex(
              (x) => x.SecurityUserID === user.SecurityUserID
            ) ?? -1;

          if (filteredUserIdx === -1) {
            draftState.filteredUsers?.push(user);
          }

          if (draftState.alertNotificationId !== '-1') {
            draftState.unsubscribedUsers = draftState.unsubscribedUsers ?? [];
            draftState.unsubscribedUsers.push(user);
          }

          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setFilteredUsers = this.store.updater(
    (state: ModelEditState, filterStr: string): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          let filteredUsers: ISubscriber[] = draftState.users as ISubscriber[];
          if (!isNilOrEmptyString(filterStr)) {
            filteredUsers = filteredUsers.filter(
              (user) =>
                user.FriendlyName.toLowerCase().indexOf(
                  filterStr.toLowerCase()
                ) >= 0
            );
          }

          draftState.filteredUsers = filteredUsers.filter(
            (u) =>
              !state.alertNotificationModel?.recipients?.some(
                (x) => x.SecurityUserID === u.SecurityUserID
              )
          );
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationSaving = this.store.updater(
    (state: ModelEditState, isSaving: boolean): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.isSaving = isSaving;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationModelSaved = this.store.updater(
    (state: ModelEditState, isSaved: boolean): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.isSaved = isSaved;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationModelDeleted = this.store.updater(
    (state: ModelEditState, deleted: boolean): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.deleted = deleted;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationCustomMessage = this.store.updater(
    (state: ModelEditState, message: string): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.customMessage = message;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationDefaultMessage = this.store.updater(
    (state: ModelEditState, message: string): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.defaultMessage = message;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly setAlertNotificationFrequency = this.store.updater(
    (state: ModelEditState, frequency: number): ModelEditState => {
      const alertNotificationModel = produce(
        state.alertNotificationModel,
        (draftState: IAlertNotificationModel) => {
          draftState.notificationFrequency = frequency;
          return draftState;
        }
      ) as IAlertNotificationModel;

      return {
        ...state,
        alertNotificationModel,
      };
    }
  );

  readonly resetAlertNotificationModel = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      return {
        ...state,
        reloadModels: false,
        alertNotificationModel: null,
      };
    }
  );

  private getModelType(state: ModelEditState): string {
    const modelSummary = {
      ...state.entities[state.selectedModelId],
    } as IModelConfigData;
    return modelSummary.properties.modelType.type || '';
  }
}
