import { IPDTrend, IProcessedTrend } from '@atonix/atx-core';
import {
  EPredictiveTypes,
  EOpModeTypes,
  IModelBuildProperties,
} from '@atonix/shared/api';
import { IModelInput } from './model-edit.query';

export interface ILoadModelTrendConfiguration {
  modelId: string;
  startDate: Date;
  endDate: Date;
  predictiveMethodType: EPredictiveTypes;
}
export interface IModelSummaryUpdate {
  modelExtID: string;
  columnField: string;
  newValue: any;
}
export interface ILoadPDNDModelTrendsConfiguration {
  model: string;
  asset: string;
  startDate: Date;
  endDate: Date;
}
export interface ILoadModelContextConfiguration {
  asset: string;
  startDate: Date;
  endDate: Date;
  trend: IProcessedTrend;
}

export interface ICreateFromTemplateParams {
  assetVariableTagMapId: number;
  opModeTypeId: number;
}

export interface ICreateCustomModelParams extends ICreateFromTemplateParams {
  modelName: string;
  modelTypeId: number;
}

export interface IChangeModelTypeParams {
  active: boolean;
  assetVariableTagMapId: number;
  modelExtID: string;
  modelId: number;
  modelName: string;
  modelTypeId: number;
  opModeTypeId: number;
  opModeTypes?: EOpModeTypes[];
  buildProperties: IModelBuildProperties;
}

export interface IForceIncludeMultiModelParams {
  modelInput: IModelInput;
  forceIncludeAll: boolean;
}
