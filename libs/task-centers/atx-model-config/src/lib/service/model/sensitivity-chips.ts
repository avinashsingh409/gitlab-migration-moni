export enum SensitivityAnomalyTypes {
  All = 0,
  RelativeModelBounds = 1,
  FixedLimits = 2,
  ModelBoundCriticality = 3,
}

export const SensitivityAnomalyTypeLabel: Record<
  SensitivityAnomalyTypes,
  string
> = {
  [SensitivityAnomalyTypes.All]: 'All',
  [SensitivityAnomalyTypes.ModelBoundCriticality]: 'Model Bound Criticality',
  [SensitivityAnomalyTypes.RelativeModelBounds]: 'Relative Model Bounds',
  [SensitivityAnomalyTypes.FixedLimits]: 'Fixed Limits',
};

export const SensitivityAnomalyTypeTooltips: Record<
  SensitivityAnomalyTypes,
  string
> = {
  [SensitivityAnomalyTypes.All]: '',
  [SensitivityAnomalyTypes.ModelBoundCriticality]:
    'The model bound criticality can be optionally set here and seen in the Alerts screening view to prioritize models with more critical alerts.',
  [SensitivityAnomalyTypes.RelativeModelBounds]:
    "Relative model bounds can be set to specify the difference required between the actual value and the model's expected value to be considered anomalous.",
  [SensitivityAnomalyTypes.FixedLimits]:
    'Users can optionally define the upper and lower bound by a fixed value.  The upper and lower fixed limits can be independently set.',
};

export interface SelectedChip {
  type: 'anomaly' | 'alert';
  subType: number;
}

export enum SensitivityAlertTypes {
  All = 0,
  AverageAnomaly = 1,
  AnomalyFrequency = 2,
  AnomalyOscillation = 3,
  HighHigh = 4,
  LowLow = 5,
  FrozenDataCheck = 6,
  AnomalyArea = 7,
}

export const SensitivityAlertTypeLabel: Record<SensitivityAlertTypes, string> =
  {
    [SensitivityAlertTypes.All]: 'All',
    [SensitivityAlertTypes.AverageAnomaly]: 'Average Anomaly',
    [SensitivityAlertTypes.AnomalyFrequency]: 'Anomaly Persistence',
    [SensitivityAlertTypes.AnomalyOscillation]: 'Anomaly Oscillation',
    [SensitivityAlertTypes.HighHigh]: 'High High',
    [SensitivityAlertTypes.LowLow]: 'Low Low',
    [SensitivityAlertTypes.FrozenDataCheck]: 'Frozen Data Check',
    [SensitivityAlertTypes.AnomalyArea]: 'Anomaly Area',
  };

export const SensitivityAlertTypeTooltips: Record<
  SensitivityAlertTypes,
  string
> = {
  [SensitivityAlertTypes.All]: '',
  [SensitivityAlertTypes.AnomalyArea]:
    'Similar to an integral, this alert type will sum the area between actual value and the model bounds. In general, this alert type will be in alert if the value is well out of bounds over a short period or moderately out of bounds for a longer period.',
  [SensitivityAlertTypes.AverageAnomaly]:
    'Generate an alert if over the evaluation period, there has been any window where the difference between the actual value and the model bound has averaged the threshold value.',
  [SensitivityAlertTypes.AnomalyFrequency]:
    'Generate an alert if the actual value has been out of bounds for more than the percentage threshold over the evaluation period.',
  [SensitivityAlertTypes.AnomalyOscillation]:
    'Generate an alert if the actual value has crossed the model boundary, in either direction, more than the oscillation count threshold over the evaluation period.',
  [SensitivityAlertTypes.HighHigh]:
    'Generate an alert if the actual value crosses a threshold. The threshold can be an absolute value or defined as an MAE multiple difference between the actual and expected values.',
  [SensitivityAlertTypes.LowLow]:
    'Generate an alert if the actual value crosses a threshold. The threshold can be an absolute value or defined as an MAE multiple difference between the actual and expected values.',
  [SensitivityAlertTypes.FrozenDataCheck]:
    'Generate an alert if the value of the data has not changed over the evaluation period.',
};
