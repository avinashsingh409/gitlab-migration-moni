import { ISubscriber } from '@atonix/shared/api';

export interface IAlertNotificationModel {
  alertNotificationId: string;
  alertStatusTypeId: number;
  modelId: number;
  notificationFrequency: number;
  defaultMessage?: string;
  customMessage: string;
  users?: ISubscriber[];
  recipients?: ISubscriber[];
  unsubscribedUsers?: ISubscriber[];

  //view properties
  filteredUsers?: ISubscriber[];
  alertStatusTypeName?: string;
  modelName?: string;
  tagName?: string;
  tagDescription?: string;
  tagUnits?: string;
  assetPath?: string;
  recipientCount?: number;

  isSaving?: boolean;
  isSaved?: boolean;
  deleted?: boolean;
}
