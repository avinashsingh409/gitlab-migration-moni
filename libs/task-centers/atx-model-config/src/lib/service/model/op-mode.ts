export interface OpModeSummary {
  opModeDefinitionID: number;
  opModeDefinitionExtID: string;
  opModeDefinitionTitle: string;
  opModeTypeID: number;
  changeDate: Date;
  createDate: Date;
  opModeTypeAbbrev: string;
}

export interface OpMode {
  opModeDefinitionExtID: string;
  changeDate: Date;
  title: string;
  typeAbbrev: string;
  logicString: string;
  priority: number;
}

export interface OpModeDefinition {
  opModeDefinitionID: number;
  opModeDefinitionExtID?: string;
  opModeDefinitionTitle?: string;
  opModeTypeID: number;
  opModeTypeAbbrev?: string;
  createdByUserID: number;
  createDate: string;
  changeDate: string;
  startCriteria?: CriteriaGroup[];
  endCriteria?: CriteriaGroup[];
  assetTagMaps?: AssetTagMap[];
  priority: number;
}

export interface CriteriaGroup {
  criteriaGroupID: number;
  isStart: boolean;
  displayOrder: number;
  logic: CriteriaLogic[];
}

export interface CriteriaLogic {
  criteriaLogicID: number;
  assetVariableTypeTagMapID?: number;
  logicOperatorTypeID: number;
  value: number | null;
  transiencyStdDevMultiplier?: any;
  transiencyDuration?: any;
  transiencyDurationTemporalTypeID?: any;
  logicStartDate?: Date | null;
  logicEndDate?: Date | null;
  logicOperatorTypeAbbrev: string;
  tagID?: number;
  tagName: string;
  tagDesc: string;
  engUnits: string;
  processDataServerGuid: string | null;
}

export interface AssetTagMap {
  assetAbbrev?: string; // null for tags
  assetGuid?: string; // null for tags
  opModeDefinitionAssetTagMapID: number;
  assetVariableTypeTagMapID?: number; // null for assets
  include: boolean;
  bringDescendants: boolean;
  tagDesc?: string; // null for assets
  tagName?: string; // null for assets
}
