// this is a legacy implementation that will be deprecated.
// only include fields that we use on the front end to this interface
export interface LegacyOpModeDefinition {
  OpModeDefinitionExtID: string;
  ChangeDate: Date;
  OpModeDefinitionTitle: string;
  OpModeType: OpModeType;
  StartCriteria: LegacyCriteriaObject[];
  Priority: number;
}

export interface LegacyCriteriaObject {
  Logic: LegacyLogicObject[];
}

export interface LegacyLogicObject {
  Value: number;
  CriteriaGroupID: number;
  LogicStartDate: Date;
  LogicEndDate: Date;
  LogicOperatorType: LegacyLogicOperatorType;
  AssetVariableTypeTagMap: LegacyAssetVariableTypeTagMap;
}

export interface LegacyLogicOperatorType {
  LogicOperatorTypeAbbrev: string;
}

export interface LegacyTagStuff {
  TagName: string;
}
export interface LegacyAssetVariableTypeTagMap {
  Tag: LegacyTagStuff;
}
export interface OpModeType {
  OpModeTypeAbbrev: string;
}
