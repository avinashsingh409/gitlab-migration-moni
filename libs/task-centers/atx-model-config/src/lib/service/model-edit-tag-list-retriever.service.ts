import { Injectable } from '@angular/core';
import {
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { Subject } from 'rxjs';
import { DataExplorerCoreService } from '@atonix/shared/api';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { TagMapRecommendedType } from '@atonix/atx-core';
import { ModelEditTagListFacade } from '../service/model-edit-tag-list.facade';

@Injectable({
  providedIn: 'root',
})
export class ModelEditTagListRetrieverService implements IServerSideDatasource {
  constructor(
    private dataExplorerCoreService: DataExplorerCoreService,
    public tagListFacade: ModelEditTagListFacade
  ) {}

  private unsubscribe$ = new Subject<void>();

  public cancel() {
    this.unsubscribe$.next();
  }

  public getRows(params: IServerSideGetRowsParams) {
    const request = { ...params.request };

    // Call TagGrid API after filterModel has been initialized and includes search asset
    if (request.filterModel['SearchAssetID']) {
      if (request.filterModel['Units']) {
        // This will handle the % wildcard
        request.filterModel['Units'].filter = request.filterModel[
          'Units'
        ].filter.replaceAll('%', '[%]');
      }

      this.dataExplorerCoreService
        .getTagList(request)
        .pipe(
          withLatestFrom(
            this.tagListFacade.selectedInputs$,
            this.tagListFacade.recommendedInputs$
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe(
          ([response, selectedInputs, recommendedInputs]) => {
            const tagMaps = [...response.TagMaps];

            tagMaps.map((t) => {
              const recommended = recommendedInputs.includes(
                t.AssetVariableTypeTagMapID
              );

              const selected = selectedInputs.includes(
                t.AssetVariableTypeTagMapID
              );
              t.Recommended = this.createRecommendedSelectedIcon(
                recommended,
                selected
              );
            });

            params.success({
              rowData: tagMaps,
              rowCount: response.NumTagMaps,
            });
          },
          () => {
            params.fail();
            console.error('Could not retrieve tag list');
          }
        );

      // This will reset the filter back
      if (request.filterModel['Units']) {
        request.filterModel['Units'].filter = request.filterModel[
          'Units'
        ].filter.replaceAll('[%]', '%');
      }
    }
  }

  public createRecommendedSelectedIcon(
    recommended: boolean,
    selected: boolean
  ): TagMapRecommendedType {
    switch (true) {
      case recommended && selected:
        return TagMapRecommendedType.RecommendedSelected;
      case !recommended && selected:
        return TagMapRecommendedType.NotRecommendedSelected;
      case recommended && !selected:
        return TagMapRecommendedType.RecommendedNotSelected;
      default:
        return TagMapRecommendedType.NotRecommendedNotSelected;
    }
  }
}
