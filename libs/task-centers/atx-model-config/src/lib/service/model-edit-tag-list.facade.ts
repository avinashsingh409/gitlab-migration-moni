import { state } from '@angular/animations';
import { Injectable, OnDestroy } from '@angular/core';
import { ITreeConfiguration } from '@atonix/atx-asset-tree';
import { AlertsCoreService, AssetFrameworkService } from '@atonix/shared/api';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';

export interface ModelEditTagListState {
  assetTreeConfiguration: ITreeConfiguration | null;
  assetName: string | null;
  assetID: string | null;
  searchAssetID: number | null;
  selectedInputs: number[];
  recommendedInputs: number[];
}

const _initialState: ModelEditTagListState = {
  assetTreeConfiguration: null,
  assetName: null,
  assetID: null,
  searchAssetID: null,
  selectedInputs: [],
  recommendedInputs: [],
};

let _state: ModelEditTagListState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class ModelEditTagListFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<ModelEditTagListState>(_state);
  private state$ = this.store.asObservable();

  constructor(
    private assetFrameworkService: AssetFrameworkService,
    private alertsCoreService: AlertsCoreService
  ) {}

  assetTreeConfiguration$ = this.state$.pipe(
    map((state) => state.assetTreeConfiguration),
    distinctUntilChanged()
  );

  assetName$ = this.state$.pipe(
    map((state) => state.assetName),
    distinctUntilChanged()
  );

  assetID$ = this.state$.pipe(
    map((state) => state.assetID),
    distinctUntilChanged()
  );

  searchAssetID$ = this.state$.pipe(
    map((state) => state.searchAssetID),
    distinctUntilChanged()
  );

  selectedInputs$ = this.state$.pipe(
    map((state) => state.selectedInputs),
    distinctUntilChanged()
  );

  recommendedInputs$ = this.state$.pipe(
    map((state) => state.recommendedInputs),
    distinctUntilChanged()
  );

  vm$: Observable<ModelEditTagListState> = combineLatest([
    this.assetTreeConfiguration$,
    this.assetName$,
    this.assetID$,
    this.searchAssetID$,
    this.selectedInputs$,
    this.recommendedInputs$,
  ]).pipe(
    map(
      ([
        assetTreeConfiguration,
        assetName,
        assetID,
        searchAssetID,
        selectedInputs,
        recommendedInputs,
      ]) =>
        ({
          assetTreeConfiguration,
          assetName,
          assetID,
          searchAssetID,
          selectedInputs,
          recommendedInputs,
        } as ModelEditTagListState)
    )
  );

  updateAssetTreeConfiguration(assetTreeConfiguration: ITreeConfiguration) {
    this.updateState({
      ..._state,
      assetTreeConfiguration,
    });
  }

  updateAssetName(assetName: string) {
    this.updateState({
      ..._state,
      assetName,
    });
  }

  updateAssetID(assetID: string) {
    this.updateState({
      ..._state,
      assetID,
    });
  }

  updateSearchAssetID(assetID: number) {
    this.updateState({
      ..._state,
      searchAssetID: null,
    });
    this.assetFrameworkService
      .getAssetAndAncestors(assetID)
      .pipe(take(1))
      .subscribe(
        (data) => {
          this.updateState({
            ..._state,
            searchAssetID: data.Unit?.AssetID || data.Asset?.AssetID,
          });
        },
        (error: unknown) => {
          console.error(error);
        }
      );
  }

  updateSelectedInputs(selectedInputs: number[]) {
    this.updateState({
      ..._state,
      selectedInputs,
    });
  }

  updateRecommendedInputs(tagMapID: number, modelType: string) {
    this.updateState({
      ..._state,
      recommendedInputs: [],
    });
    this.alertsCoreService
      .getRecommendedInputs(tagMapID, modelType)
      .pipe(take(1))
      .subscribe(
        (data) => {
          this.updateState({
            ..._state,
            recommendedInputs: data,
          });
        },
        (error: unknown) => {
          console.error(error);
        }
      );
  }

  reset() {
    this.updateState({ ..._initialState });
  }

  private updateState(newState: ModelEditTagListState) {
    this.store.next((_state = newState));
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
