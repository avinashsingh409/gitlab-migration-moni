import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { eachValueFrom } from 'rxjs-for-await';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { take } from 'rxjs/operators';
import { assetIssueWithOptionsMaintenanceReliability } from './fixtures/get-model-config';

import {
  AlertsCoreService,
  AlertsFrameworkService,
  EOpModeTypes,
  IssuesFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';
import { ModelEditFacade } from './model-edit.facade';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { isNil } from '@atonix/atx-core';

describe('Model Edit Config', () => {
  let toastService: ToastService;
  let alertsFrameworkService: AlertsFrameworkService;
  let alertsCoreService: AlertsCoreService;
  let processDataFrameworkService: ProcessDataFrameworkService;
  let modelEditFacade: ModelEditFacade;
  beforeEach(() => {
    toastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        AlertsFrameworkService,
        AlertsCoreService,
        ProcessDataFrameworkService,
        ModelEditFacade,
        {
          provide: ToastService,
          useValue: toastService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    modelEditFacade = TestBed.inject(ModelEditFacade);
    processDataFrameworkService = TestBed.inject(ProcessDataFrameworkService);
    alertsFrameworkService = TestBed.inject(AlertsFrameworkService);
    alertsCoreService = TestBed.inject(AlertsCoreService);
  });

  it('the model can be deactivated', async () => {
    expect(modelEditFacade).toBeTruthy();
    alertsCoreService.getModelConfig =
      assetIssueWithOptionsMaintenanceReliability;
    modelEditFacade.getModelSummaries({
      modelIds: ['1'],
      refreshTrends: true,
      isMultiModel: false,
    });
    modelEditFacade.command.setModelActiveInactive();

    for await (const model of eachValueFrom(
      modelEditFacade.query.selectedModelSummary$.pipe(take(1))
    )) {
      expect(model.active === false);
    }
  });

  it('opmode types can be edited', async () => {
    expect(modelEditFacade).toBeTruthy();
    alertsCoreService.getModelConfig =
      assetIssueWithOptionsMaintenanceReliability;
    modelEditFacade.getModelSummaries({
      modelIds: ['1'],
      refreshTrends: true,
      isMultiModel: false,
    });
    modelEditFacade.command.setOpModeTypes([EOpModeTypes.OutOfService]);

    for await (const model of eachValueFrom(
      modelEditFacade.query.selectedModelSummary$.pipe(take(1))
    )) {
      expect(model.properties.opModeTypes[0] === EOpModeTypes.OutOfService);
    }
  });

  it('set lower fixed limit anomaly', async () => {
    expect(modelEditFacade).toBeTruthy();
    alertsCoreService.getModelConfig =
      assetIssueWithOptionsMaintenanceReliability;
    modelEditFacade.getModelSummaries({
      modelIds: ['1'],
      refreshTrends: true,
      isMultiModel: false,
    });
    modelEditFacade.command.setLowerFixedLimitAnomalyEnabled({
      modelExtID: '70de0e39-85ae-4309-9d4a-877a99d07284',
      columnField: '',
      newValue: true,
    });

    for await (const model of eachValueFrom(
      modelEditFacade.query.selectedModelSummary$.pipe(take(1))
    )) {
      expect(model.standardModel === false);
    }
  });

  it('can discard model', async () => {
    expect(modelEditFacade).toBeTruthy();
    alertsCoreService.getModelConfig =
      assetIssueWithOptionsMaintenanceReliability;
    modelEditFacade.getModelSummaries({
      modelIds: ['1'],
      refreshTrends: true,
      isMultiModel: false,
    });
    modelEditFacade.command.setLowerFixedLimitAnomalyEnabled({
      modelExtID: '70de0e39-85ae-4309-9d4a-877a99d07284',
      columnField: '',
      newValue: true,
    });
    modelEditFacade.command.discardChanges();
    for await (const model of eachValueFrom(
      modelEditFacade.query.selectedModelSummary$.pipe(take(1))
    )) {
      // Discarding changes from the model will set this to null
      // currently the logic to reload the model from the API
      // is in the single-model-edit.component.ts
      expect(isNil(model) === true);
    }
  });
});
