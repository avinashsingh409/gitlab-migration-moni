import { Inject, Injectable } from '@angular/core';
import { IModelHistoryEventStateChange } from '@atonix/atx-chart-v2';
import {
  getTrendTotalSeries,
  IAssetMeasurementsSet,
  INDModelPDTrendMap,
  IProcessedTrend,
  processPDNDTrends,
} from '@atonix/atx-core';
import {
  AlertsCoreService,
  AlertsFrameworkService,
  INDModelActionItem,
  ProcessDataFrameworkService,
  IModelConfigData,
  EModelTypes,
  ICorrelationData,
  IModelBuildProperties,
  IModelBuildStatus,
  IRawModelConfigData,
  AssetFrameworkService,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { forkJoin, iif, Observable } from 'rxjs';
import { map, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { ModelEditMultiCommands } from './model-edit.commands.multi';
import {
  ILoadModelContextConfiguration,
  ILoadModelTrendConfiguration,
  ILoadPDNDModelTrendsConfiguration,
  ICreateFromTemplateParams,
  ICreateCustomModelParams,
  IChangeModelTypeParams,
} from './model-edit.actions';
import { ModelEditCommands } from './model-edit.commands';
import {
  initialModelEditState,
  ModelEditQuery,
  ModelEditState,
} from './model-edit.query';
import { setModelNonStandard } from './model-edit.utilities';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import produce from 'immer';

@Injectable({
  providedIn: 'root',
})
export class ModelEditFacade extends ComponentStore<ModelEditState> {
  public query = new ModelEditQuery(this);
  public command = new ModelEditCommands(
    this,
    this.alertsCoreService,
    this.alertsFrameworkService,
    this.toastService,
    this.appConfig
  );
  public multiModelCommand = new ModelEditMultiCommands(
    this,
    this.alertsCoreService,
    this.toastService
  );

  constructor(
    private alertsFrameworkService: AlertsFrameworkService,
    private alertsCoreService: AlertsCoreService,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private assetService: AssetFrameworkService,
    private toastService: ToastService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    super(initialModelEditState);
  }

  readonly getModelSummaries = this.effect(
    (
      modelSummary$: Observable<{
        modelIds: string[];
        refreshTrends: boolean;
        isMultiModel: boolean;
      }>
    ) => {
      return modelSummary$.pipe(
        tap(() => this.command.setModelLoading(true)),
        switchMap(
          (req: {
            modelIds: string[];
            refreshTrends: boolean;
            isMultiModel: boolean;
          }) =>
            this.alertsCoreService.getModelConfig(req.modelIds).pipe(
              map((modelConfigSummaries) => {
                if (modelConfigSummaries.length > 1) {
                  modelConfigSummaries.sort(
                    (a, b) =>
                      req.modelIds.indexOf(a.modelExtID) -
                      req.modelIds.indexOf(b.modelExtID)
                  );
                }
                return modelConfigSummaries;
              }),
              tapResponse(
                (modelConfigSummaries) =>
                  req.isMultiModel
                    ? this.multiModelCommand.multipleLoadSummaries({
                        modelConfigSummaries,
                        refreshTrends: req.refreshTrends,
                      })
                    : this.command.loadSummaries({
                        modelConfigSummaries,
                        refreshTrends: req.refreshTrends,
                        appendBuildStatus: true,
                      }),
                (error: string) => {
                  console.error(JSON.stringify(error));
                  this.command.setModelLoading(false);
                }
              )
            )
        )
      );
    }
  );

  readonly getModelMathMessage = this.effect(
    (modelExtId$: Observable<string>) => {
      return modelExtId$.pipe(
        switchMap((req: string) => {
          return this.alertsCoreService.getModelTypeVersion(req).pipe(
            tapResponse(
              (mathMessage) => this.command.setMathMessage(mathMessage),
              (error: string) => {
                console.error(JSON.stringify(error));
              }
            )
          );
        })
      );
    }
  );

  readonly getModelTrend = this.effect(
    (modelTrend$: Observable<ILoadModelTrendConfiguration>) => {
      return modelTrend$.pipe(
        tap(() => this.command.setModelTrendLoading()),
        switchMap((req: ILoadModelTrendConfiguration) => {
          return this.alertsFrameworkService
            .getModelTrend(
              req.modelId,
              req.predictiveMethodType,
              req.startDate,
              req.endDate
            )
            .pipe(
              tapResponse(
                (modelTrend) => {
                  return this.command.loadModelTrend(modelTrend);
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                  return this.command.modelTrendError();
                }
              )
            );
        })
      );
    }
  );

  readonly modelHistorySetFavoriteAction = this.effect(
    (modelHistoryEvent$: Observable<IModelHistoryEventStateChange>) => {
      return modelHistoryEvent$.pipe(
        map((modelHistoryEvent) => {
          return {
            ...modelHistoryEvent.modelAction,
            Favorite: modelHistoryEvent?.value,
          } as INDModelActionItem;
        }),
        switchMap((action) =>
          this.alertsFrameworkService.updateActionItem(action).pipe(
            tapResponse(
              () => {
                this.toastService.openSnackBar('Favorite Changed', 'success');
                return this.command.setModelHistoryReload(true);
              },
              (err: string) => {
                this.command.setModelHistoryReload(false);
                this.toastService.openSnackBar(
                  'Favorite Changed Failed',
                  'error'
                );
                console.error(JSON.stringify(err));
              }
            )
          )
        )
      );
    }
  );

  readonly modelHistorySetNoteAction = this.effect(
    (modelHistoryEvent$: Observable<IModelHistoryEventStateChange>) => {
      return modelHistoryEvent$.pipe(
        map((modelHistoryEvent) => {
          return {
            ...modelHistoryEvent.modelAction,
            NoteText: modelHistoryEvent?.value,
          } as INDModelActionItem;
        }),
        switchMap((action) =>
          this.alertsFrameworkService.updateActionItem(action).pipe(
            tapResponse(
              () => {
                this.toastService.openSnackBar('Note Changed', 'success');
                return this.command.setModelHistoryReload(true);
              },
              (err: string) => {
                this.command.setModelHistoryReload(false);
                this.toastService.openSnackBar('Note Changed Failed', 'error');
                console.error(JSON.stringify(err));
              }
            )
          )
        )
      );
    }
  );

  readonly getPDNDModelTrends = this.effect(
    (modelContext$: Observable<ILoadPDNDModelTrendsConfiguration>) => {
      return modelContext$.pipe(
        tap(() => this.command.setModelContextLoading()),
        switchMap((req: ILoadPDNDModelTrendsConfiguration) => {
          return this.processDataFrameworkService
            .getPDNDModelTrendsWithAssetGuid(
              req.asset,
              req.model,
              true,
              req.startDate,
              req.endDate
            )
            .pipe(
              switchMap((trendsMap: INDModelPDTrendMap[]) => {
                const pdTrend: INDModelPDTrendMap | undefined = trendsMap.find(
                  (tm: INDModelPDTrendMap) => tm.PDTrendID === -1
                );
                if (pdTrend != null) {
                  const trend: IProcessedTrend = {
                    id: `${pdTrend.MappedPDTrend?.PDTrendID}`,
                    assetID: `${pdTrend.AssetID}`,
                    assetGuid: pdTrend.AssetGuid,
                    label: pdTrend.MappedPDTrend?.Title,
                    trendDefinition: pdTrend.MappedPDTrend,
                    totalSeries: getTrendTotalSeries(pdTrend.MappedPDTrend),
                    labelIndex: 0,
                    showDataCursor: true,
                    modelTrendMap: pdTrend,
                  };
                  const processedTrend = processPDNDTrends([trend]);
                  return [
                    this.getModelContext({
                      asset: req.asset,
                      startDate: req.startDate,
                      endDate: req.endDate,
                      trend: processedTrend[0],
                    }),
                  ];
                }
                return [];
              })
            );
        })
      );
    }
  );

  readonly getModelContext = this.effect(
    (modelContext$: Observable<ILoadModelContextConfiguration>) => {
      return modelContext$.pipe(
        withLatestFrom(this.query.selectedModelSummary$),
        switchMap(
          ([req, model]: [
            ILoadModelContextConfiguration,
            IModelConfigData | undefined
          ]) =>
            iif(
              () =>
                req.trend.id === '-1' &&
                (model?.properties.modelType.type === EModelTypes.Forecast ||
                  model?.properties.modelType.type === EModelTypes.ROC),
              forkJoin([
                this.processDataFrameworkService.getTagsDataFiltered(
                  req.trend.trendDefinition,
                  req.startDate,
                  req.endDate,
                  0,
                  '',
                  req.asset
                ),
                this.alertsCoreService.getCorrelationTrend(
                  model?.modelExtID || '',
                  req.startDate,
                  req.endDate
                ),
              ]).pipe(
                tapResponse(
                  ([measurementsSet, correlationData]: [
                    IAssetMeasurementsSet,
                    ICorrelationData[]
                  ]) =>
                    this.command.loadModelContext({
                      trend: req.trend,
                      measurementsSet,
                      startDate: req.startDate,
                      endDate: req.endDate,
                      correlationData,
                    }),
                  (error: string) => {
                    console.error(JSON.stringify(error));
                  }
                )
              ),
              forkJoin([
                this.processDataFrameworkService
                  .getTagsDataFiltered(
                    req.trend.trendDefinition,
                    req.startDate,
                    req.endDate,
                    0,
                    '',
                    req.asset
                  )
                  .pipe(
                    tapResponse(
                      (measurementsSet: IAssetMeasurementsSet) =>
                        this.command.loadModelContext({
                          trend: req.trend,
                          measurementsSet,
                          startDate: req.startDate,
                          endDate: req.endDate,
                          correlationData: [],
                        }),
                      (error: string) => {
                        console.error(JSON.stringify(error));
                      }
                    )
                  ),
              ])
            )
        )
      );
    }
  );

  // TODO: This should only be one call to populate both
  readonly getModelTemplates = this.effect(
    (templateForTagsParams$: Observable<ICreateFromTemplateParams>) => {
      return templateForTagsParams$.pipe(
        tap(() => this.command.setModelTemplatesLoading(true)),
        switchMap((req: ICreateFromTemplateParams) =>
          this.alertsCoreService
            .getModelTemplates(req.assetVariableTagMapId, req.opModeTypeId)
            .pipe(
              tapResponse(
                (modelTemplates) => {
                  if (modelTemplates?.length === 0) {
                    this.toastService.openSnackBar(
                      `No templates available`,
                      'warning'
                    );
                  }
                  this.command.loadModelTemplates(modelTemplates);
                  this.command.setModelTemplatesLoading(false);
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                  this.command.setModelTemplatesLoading(false);
                }
              )
            )
        )
      );
    }
  );

  readonly getBlankModel = this.effect(
    (createModelParams$: Observable<ICreateCustomModelParams>) => {
      return createModelParams$.pipe(
        tap(() => this.command.setModelTemplatesLoading(true)),
        switchMap((req: ICreateCustomModelParams) =>
          this.alertsCoreService
            .getBlankModel(
              req.modelName ?? 'New Model',
              req.assetVariableTagMapId,
              req.opModeTypeId,
              req.modelTypeId
            )
            .pipe(
              tapResponse(
                (model) => {
                  this.command.loadSummaries({
                    modelConfigSummaries: [model],
                    refreshTrends: false,
                    appendBuildStatus: false,
                  });
                  this.command.setModelTemplatesLoading(false);
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                  this.command.setModelTemplatesLoading(false);
                }
              )
            )
        )
      );
    }
  );

  readonly changeModelType = this.effect(
    (createModelParams$: Observable<IChangeModelTypeParams>) => {
      return createModelParams$.pipe(
        tap(() => this.command.setModelTemplatesLoading(true)),
        switchMap((req: IChangeModelTypeParams) =>
          this.alertsCoreService
            .getBlankModel(
              req.modelName,
              req.assetVariableTagMapId,
              req.opModeTypeId,
              req.modelTypeId
            )
            .pipe(
              tapResponse(
                (model: IModelConfigData) => {
                  const newModel = produce(
                    model,
                    (draftState: IModelConfigData) => {
                      draftState.active = req.active;
                      draftState.modelExtID = req.modelExtID;
                      draftState.legacy.modelID = req.modelId;
                      draftState.properties.opModeTypes = req.opModeTypes;
                      setModelNonStandard(draftState);
                      draftState.isDirty = true;
                      const buildPropertiesStatus = produce(
                        req.buildProperties,
                        (draftState: IModelBuildProperties) => {
                          if (req.buildProperties?.status) {
                            draftState.status = req.buildProperties.status;
                          } else {
                            draftState.status = {
                              built: false,
                              lastSaved: new Date(),
                              buildState: '',
                            };
                          }
                        }
                      ) as IModelBuildProperties;
                      draftState.buildProperties = buildPropertiesStatus;
                    }
                  ) as IModelConfigData;

                  this.command.loadSummaries({
                    modelConfigSummaries: [newModel],
                    refreshTrends: false,
                    appendBuildStatus: false,
                  });
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            )
        )
      );
    }
  );

  updateModelTrendTime(start: Date, end: Date, modelId: string) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        vm.selectedModelSummary?.properties?.predictiveTypeSelected !==
        undefined
      ) {
        this.getModelTrend({
          modelId: modelId,
          predictiveMethodType:
            vm.selectedModelSummary.properties.predictiveTypeSelected,
          startDate: start,
          endDate: end,
        });
      }
    });
  }

  updateModelContextTrendTime(start: Date, end: Date, asset: string) {
    this.query.modelContextData$.pipe(take(1)).subscribe((trendDefinition) => {
      if (trendDefinition) {
        this.command.setModelContextDataLoading();
        this.getModelContext({
          asset: asset,
          startDate: start,
          endDate: end,
          trend: trendDefinition,
        });
      }
    });
  }
}
