/* eslint-disable */

import {
  EModelTypes,
  EOpModeTypes,
  IModelConfigData,
} from '@atonix/shared/api';
import { BehaviorSubject } from 'rxjs';

export const assetIssueWithOptionsMaintenanceReliability = jest.fn(
  () =>
    new BehaviorSubject<IModelConfigData[]>([
      {
        name: 'Aarfgh4',
        path: 'Live MD Test Group/Live MD Test Station/Live MD Test Unit/Tux Group 1',
        legacy: {
          modelID: 181,
          modelTemplateID: undefined,
          opModeParamsID: 132,
        },
        modelExtID: '70de0e39-85ae-4309-9d4a-877a99d07284',
        active: true,
        locked: true,
        standardModel: false,
        properties: {
          modelType: {
            type: EModelTypes.AdvancedPatternRecognition,
            properties: {},
          },
        },
        dependent: {
          processDataServerGuid: '015f8ca1-1e4a-4671-80d8-a2e2504c738f',
        },
        independents: [
          {
            tagGuid: 'bbfc08b7-d81c-4916-8931-a1897dc1c8d4',
            tagName: 'tag0004',
            tagDesc: 'tag0004 model 1 input',
            forcedInclusion: false,
            assetVariableTagMapID: 1200000136,
            inUse: false,
            recommendedInputVariableMapID: null,
          },
        ],
        opModeTypes: [EOpModeTypes.Startup],
      },
    ] as any)
);
