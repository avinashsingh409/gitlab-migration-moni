import { INDModelSummary } from '@atonix/shared/api';
import { ComponentStore } from '@ngrx/component-store';

export interface ModelActionsState {
  isMultipleSelect: boolean;
  selectedModel: INDModelSummary | null;
  selectedModels: INDModelSummary[] | null;
  selectedModelIds: string[] | null;
  timeStamp: string | null;
  isActionWatch: boolean;
  isAlert: boolean;
  isDiagnose: boolean;
  isMaintenance: boolean;
  showCustomWatchForm: boolean;
  isMultiClearWatch: boolean;
  isMultiClearMaintenance: boolean;
  isMultiClearDiagnose: boolean;
  defaultNote: string | null;
  showOnlyActionForms: boolean;
  showDiagnoseForm: boolean;
  showAddNoteForm: boolean;
  showMaintenanceForm: boolean;
  upperCheck: boolean;
  lowerCheck: boolean;
  refreshing: boolean;
  isExternalModelType: boolean;
}

export const initialState: ModelActionsState = {
  isMultipleSelect: false,
  selectedModel: null,
  selectedModelIds: null,
  selectedModels: null,
  timeStamp: 'No Valid Date',
  isActionWatch: false,
  isAlert: false,
  isDiagnose: false,
  isMaintenance: false,
  showCustomWatchForm: false,
  isMultiClearWatch: false,
  isMultiClearMaintenance: false,
  isMultiClearDiagnose: false,
  defaultNote: null,
  showOnlyActionForms: false,
  showDiagnoseForm: false,
  showAddNoteForm: false,
  showMaintenanceForm: false,
  upperCheck: true,
  lowerCheck: true,
  refreshing: false,
  isExternalModelType: false,
};

export class ModelActionsQuery {
  constructor(private store: ComponentStore<ModelActionsState>) {}

  readonly hasSelectedMultiple$ = this.store.select(
    (state) => state.isMultipleSelect,
    { debounce: true }
  );
  readonly selectedModel$ = this.store.select((state) => state.selectedModel, {
    debounce: true,
  });
  readonly selectedModelIds$ = this.store.select(
    (state) => state.selectedModelIds,
    {
      debounce: true,
    }
  );
  readonly selectedModels$ = this.store.select(
    (state) => state.selectedModels,
    {
      debounce: true,
    }
  );
  readonly timeStamp$ = this.store.select((state) => state.timeStamp, {
    debounce: true,
  });
  readonly isActionWatch$ = this.store.select((state) => state.isActionWatch, {
    debounce: true,
  });
  readonly isAlert$ = this.store.select((state) => state.isAlert, {
    debounce: true,
  });
  readonly isDiagnose$ = this.store.select((state) => state.isDiagnose, {
    debounce: true,
  });
  readonly isMaintenance$ = this.store.select((state) => state.isMaintenance, {
    debounce: true,
  });
  readonly showCustomWatchForm$ = this.store.select(
    (state) => state.showCustomWatchForm,
    { debounce: true }
  );
  readonly isMultiClearWatch$ = this.store.select(
    (state) => state.isMultiClearWatch,
    { debounce: true }
  );
  readonly isMultiClearMaintenance$ = this.store.select(
    (state) => state.isMultiClearMaintenance,
    { debounce: true }
  );
  readonly isMultiClearDiagnose$ = this.store.select(
    (state) => state.isMultiClearDiagnose,
    { debounce: true }
  );
  readonly defaultNote$ = this.store.select((state) => state.defaultNote, {
    debounce: true,
  });
  readonly showOnlyActionForms$ = this.store.select(
    (state) => state.showOnlyActionForms,
    { debounce: true }
  );
  readonly showDiagnoseForm$ = this.store.select(
    (state) => state.showDiagnoseForm,
    {
      debounce: true,
    }
  );
  readonly showAddNoteForm$ = this.store.select(
    (state) => state.showAddNoteForm,
    {
      debounce: true,
    }
  );
  readonly showMaintenanceForm$ = this.store.select(
    (state) => state.showMaintenanceForm,
    { debounce: true }
  );
  readonly upperCheck$ = this.store.select((state) => state.upperCheck, {
    debounce: true,
  });
  readonly lowerCheck$ = this.store.select((state) => state.lowerCheck, {
    debounce: true,
  });
  readonly refreshing$ = this.store.select((state) => state.refreshing, {
    debounce: true,
  });
  readonly isExternalModelType$ = this.store.select(
    (state) => state.isExternalModelType,
    { debounce: true }
  );

  readonly vm$ = this.store.select(
    this.hasSelectedMultiple$,
    this.selectedModel$,
    this.selectedModelIds$,
    this.selectedModels$,
    this.timeStamp$,
    this.isActionWatch$,
    this.isAlert$,
    this.isDiagnose$,
    this.isMaintenance$,
    this.showCustomWatchForm$,
    this.isMultiClearWatch$,
    this.isMultiClearMaintenance$,
    this.isMultiClearDiagnose$,
    this.defaultNote$,
    this.showOnlyActionForms$,
    this.showDiagnoseForm$,
    this.showAddNoteForm$,
    this.showMaintenanceForm$,
    this.upperCheck$,
    this.lowerCheck$,
    this.refreshing$,
    this.isExternalModelType$,
    (
      hasSelectedMultiple,
      selectedModel,
      selectedModelIds,
      selectedModels,
      timeStamp,
      isActionWatch,
      isAlert,
      isDiagnose,
      isMaintenance,
      showCustomWatchForm,
      isMultiClearWatch,
      isMultiClearMaintenance,
      isMultiClearDiagnose,
      defaultNote,
      showOnlyActionForms,
      showDiagnoseForm,
      showAddNoteForm,
      showMaintenanceForm,
      upperCheck,
      lowerCheck,
      refreshing,
      isExternalModelType
    ): any => {
      return {
        hasSelectedMultiple,
        selectedModel,
        selectedModelIds,
        selectedModels,
        timeStamp,
        isActionWatch,
        isAlert,
        isDiagnose,
        isMaintenance,
        showCustomWatchForm,
        isMultiClearWatch,
        isMultiClearMaintenance,
        isMultiClearDiagnose,
        defaultNote,
        showOnlyActionForms,
        showDiagnoseForm,
        showAddNoteForm,
        showMaintenanceForm,
        upperCheck,
        lowerCheck,
        refreshing,
        isExternalModelType,
      };
    }
  );
}
