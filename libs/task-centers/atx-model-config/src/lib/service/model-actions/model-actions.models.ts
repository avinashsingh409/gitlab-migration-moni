import { IActionItem, INDModelSummary } from '@atonix/shared/api';

export enum ModelTypes {
  APR = 'APR',
  FL = 'Fixed Limit',
  RA = 'Rolling Average',
}

export enum UpperBoundTypes {
  Model = 'Model',
  Relative = 'Relative',
  AbsoluteValue = 'Absolute',
}
export const UpperBoundTypeMapping = {
  [UpperBoundTypes.Model]: 'Model Upper Bound',
  [UpperBoundTypes.Relative]: 'Relative to Expected',
  [UpperBoundTypes.AbsoluteValue]: 'Absolute Value',
};

export enum LowerBoundTypes {
  Model = 'Model',
  Relative = 'Relative',
  AbsoluteValue = 'Absolute',
}
export const LowerBoundTypeMapping = {
  [LowerBoundTypes.Model]: 'Model Lower Bound',
  [LowerBoundTypes.Relative]: 'Relative to Expected',
  [LowerBoundTypes.AbsoluteValue]: 'Absolute Value',
};

export enum CriteriaType {
  Greater = 1,
  Lesser = 2,
  Equal = 3,
}

export interface WatchActionParam {
  hours?: string | '';
  models?: INDModelSummary[];
  actionItem?: IActionItem;
  actionType?: string;
}

export interface AlertsActionEvent {
  event:
    | 'ToggleActionFlyout'
    | 'OpenOpModeConfig'
    | 'OpenDiagnosticDrilldown'
    | 'OpenDataExplorer'
    | 'NewIssue'
    | 'ShowRelatedModels'
    | 'ShowRelatedIssues';

  newValue?: any;
}
