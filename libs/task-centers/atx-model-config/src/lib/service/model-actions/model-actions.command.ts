import { INDModelSummary } from '@atonix/shared/api';
import { ComponentStore } from '@ngrx/component-store';
import { initialState, ModelActionsState } from './model-actions.query';

export class ModelActionsCommands {
  constructor(private store: ComponentStore<ModelActionsState>) {}
  readonly setLoading = this.store.updater(
    (state: ModelActionsState, isLoading: boolean): ModelActionsState => {
      return {
        ...state,
        refreshing: isLoading,
      };
    }
  );

  readonly setModelIds = this.store.updater(
    (state: ModelActionsState, modelIds: string[]): ModelActionsState => {
      return {
        ...state,
        selectedModelIds: modelIds,
      };
    }
  );

  readonly updateModelDesc = this.store.updater(
    (state: ModelActionsState, modelDesc: string): ModelActionsState => {
      if (state.selectedModel) {
        return {
          ...state,
          selectedModel: {
            ...state.selectedModel,
            ModelDesc: modelDesc,
          },
        };
      }
      return state;
    }
  );

  readonly setModel = this.store.updater(
    (
      state: ModelActionsState,
      models: INDModelSummary[]
    ): ModelActionsState => {
      return {
        ...state,
        selectedModel: models[0],
        selectedModels: models,
        selectedModelIds: [models[0]?.ModelID.toString()],
        isExternalModelType: models[0]?.ModelTypeAbbrev === 'External',
        showCustomWatchForm: false,
        showOnlyActionForms: false,
        showDiagnoseForm: false,
        showAddNoteForm: false,
        showMaintenanceForm: false,
        isActionWatch: models[0]?.ActionWatch,
        isDiagnose: models[0]?.ActionDiagnose,
        isAlert: models[0]?.Alert,
        isMaintenance: models[0]?.ActionModelMaintenance,
        isMultiClearDiagnose: this.getIsMultiClearDiagnose(models),
        isMultiClearMaintenance: this.getIsMultiClearMaintenance(models),
        isMultiClearWatch: this.getIsMultiClearWatch(models),
      };
    }
  );

  readonly setShowCustomWatchForm = this.store.updater(
    (
      state: ModelActionsState,
      showCustomWatchForm: boolean
    ): ModelActionsState => {
      return {
        ...state,
        showCustomWatchForm: showCustomWatchForm,
      };
    }
  );

  readonly setDefaultNote = this.store.updater(
    (state: ModelActionsState, defaultNote: string): ModelActionsState => {
      return {
        ...state,
        defaultNote,
      };
    }
  );

  readonly showDiagnoseForm = this.store.updater(
    (state: ModelActionsState): ModelActionsState => {
      return {
        ...state,
        showOnlyActionForms: true,
        showDiagnoseForm: true,
      };
    }
  );

  readonly showAddNoteForm = this.store.updater(
    (state: ModelActionsState): ModelActionsState => {
      return {
        ...state,
        showOnlyActionForms: true,
        showAddNoteForm: true,
      };
    }
  );

  readonly showMaintenanceForm = this.store.updater(
    (state: ModelActionsState): ModelActionsState => {
      return {
        ...state,
        showOnlyActionForms: true,
        showMaintenanceForm: true,
      };
    }
  );

  readonly setTimeStamp = this.store.updater(
    (state: ModelActionsState, timeStamp: string): ModelActionsState => {
      return {
        ...state,
        timeStamp,
      };
    }
  );

  readonly resetForms = this.store.updater(
    (state: ModelActionsState): ModelActionsState => {
      return {
        ...state,
        showCustomWatchForm: false,
        showOnlyActionForms: false,
        showDiagnoseForm: false,
        showAddNoteForm: false,
        showMaintenanceForm: false,
      };
    }
  );

  readonly resetModelActionsState = this.store.updater(
    (state: ModelActionsState): ModelActionsState => {
      return initialState;
    }
  );

  getIsMultiClearWatch(models: INDModelSummary[]) {
    const valueArr = models?.map((item) => {
      return item.ActionWatch;
    });
    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0]?.ActionWatch : false;
  }
  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearMaintenance(models: INDModelSummary[]) {
    const valueArr = models?.map((item) => {
      return item.ActionModelMaintenance;
    });
    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0]?.ActionModelMaintenance : false;
  }
  // Checks if all selected is watch and valid for multiple clear
  getIsMultiClearDiagnose(models: INDModelSummary[]) {
    const valueArr = models?.map((item) => {
      return item.ActionDiagnose;
    });

    const isSame = valueArr.every((val, i, arr) => val === arr[0]);
    return isSame ? models[0]?.ActionDiagnose : false;
  }
}
