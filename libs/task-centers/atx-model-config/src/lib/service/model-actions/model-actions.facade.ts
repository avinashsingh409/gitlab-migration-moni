/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Injectable, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  LowerBoundTypes,
  UpperBoundTypes,
} from '../../service/model-actions/model-actions.models';
import {
  AlertsFrameworkService,
  INDModelSummary,
  ModelConfigCoreService,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Observable, Subject, of, switchMap, tap } from 'rxjs';
import {
  distinctUntilChanged,
  debounceTime,
  takeUntil,
  take,
  catchError,
  withLatestFrom,
} from 'rxjs/operators';
import { WatchActionParam } from '../../service/model-actions/model-actions.models';
import { isNil } from '@atonix/atx-core';
import {
  initialState,
  ModelActionsQuery,
  ModelActionsState,
} from './model-actions.query';
import { ModelActionsCommands } from './model-actions.command';
import {
  ModelConfigBusEventTypes,
  ModelConfigEmitEvent,
  ModelConfigEventBus,
} from '../../store/services/model-config-event-bus';

@Injectable({ providedIn: 'root' })
export class ModelActionsFacade
  extends ComponentStore<ModelActionsState>
  implements OnDestroy
{
  public query = new ModelActionsQuery(this);
  public command = new ModelActionsCommands(this);
  onDestroy$ = new Subject<void>();
  constructor(
    private alertFrameworkService: AlertsFrameworkService,
    private toastService: ToastService,
    private modelConfigEventBus: ModelConfigEventBus,
    private modelConfigService: ModelConfigCoreService
  ) {
    super(initialState);
  }

  readonly loadModels = this.effect((modelIds$: Observable<string[]>) =>
    modelIds$.pipe(
      tap(() => this.command.setLoading(true)),
      switchMap((modelIds: string[]) => {
        return this.alertFrameworkService.getModelSummary(modelIds).pipe(
          tapResponse(
            (model) => {
              this.command.setModel(model);
              this.command.setLoading(false);
              this.emitModelAction([], '');
            },
            (err: string) => {
              this.command.setLoading(false);
              console.log(err);
            }
          )
        );
      })
    )
  );

  readonly setWatch = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .setWatch(watchParam.hours!, watchParam.models!)
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Set Watch Success!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar('Error Watch Set!', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly setCustomWatch = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Set Custom Watch Success!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar(
                    'Set Custom Watch Failed!',
                    'error'
                  );
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly updateModelDesc = this.effect((modelDesc$: Observable<string>) =>
    modelDesc$.pipe(
      withLatestFrom(this.query.selectedModels$),
      switchMap(([modelDesc, selectedModels]) => {
        const modelGuids =
          selectedModels?.map((model) => model.ModelExtID) ?? [];
        return this.modelConfigService
          .patchModelSummary(modelDesc, modelGuids)
          .pipe(
            tap({
              next: () => {
                this.command.updateModelDesc(modelDesc);
              },
              error: (err: unknown) => {
                console.log(err);
              },
            }),
            catchError((err: unknown, caught) => of(console.log(err)))
          );
      })
    )
  );

  readonly saveDiagnose = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Diagnose Note Saved',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar(
                    'Save Diagnose Note Failed!',
                    'error'
                  );
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly saveMaintenance = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Maintenance Note Saved!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar('Failed Saving Note', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly saveNote = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar('Note Saved!', 'success');
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar('Failed Saving Note', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly clearWatch = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Clear Watch Success!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar(
                    'Clear Watch Failed!',
                    'error'
                  );
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly clearAlerts = this.effect((models$: Observable<INDModelSummary[]>) =>
    models$.pipe(
      tap(() => this.command.setLoading(true)),
      switchMap((models: INDModelSummary[]) => {
        return this.alertFrameworkService.clearAlert(models).pipe(
          tapResponse(
            (model) => {
              this.toastService.openSnackBar('Clear Alert Success!', 'success');
              this.loadModels(models.map((m) => m.ModelID.toString()));
              this.command.setLoading(false);
              this.emitModelAction(models, 'alert');
            },
            (err: string) => {
              this.toastService.openSnackBar('Clear Action Failed!', 'error');
              console.log(err);
            }
          )
        );
      })
    )
  );

  readonly clearMaintenance = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Clear Maintenance Success!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar(
                    'Clear Maintenance Failed',
                    'error'
                  );
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly clearDiagnose = this.effect(
    (setWatchParam$: Observable<WatchActionParam>) =>
      setWatchParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((watchParam: WatchActionParam) => {
          return this.alertFrameworkService
            .actionItem(
              watchParam.models!,
              watchParam.actionItem!,
              watchParam.actionType!
            )
            .pipe(
              tapResponse(
                (models) => {
                  this.toastService.openSnackBar(
                    'Clear Diagnose Success!',
                    'success'
                  );
                  this.loadModels(
                    watchParam.models!.map((m) => m.ModelID.toString())
                  );
                  this.command.setLoading(false);
                  this.emitModelAction(models, watchParam.actionType!);
                },
                (err: string) => {
                  this.toastService.openSnackBar(
                    'Clear Diagnose Failed',
                    'error'
                  );
                  console.log(err);
                }
              )
            );
        })
      )
  );

  buildCustomWatchForm() {
    const customWatchForm = new FormGroup({
      timeSpan: new FormControl('', [Validators.required, Validators.min(0)]),
      timeUnit: new FormControl('', [Validators.required]),
      upperUnits: new FormControl(''),
      lowerUnits: new FormControl(''),
      emailAlert: new FormControl(false),
      customNote: new FormControl(''),
      upperControl: new FormControl(''),
      lowerControl: new FormControl(''),
      disableSelect: new FormControl(false),
      upperBoundExceeded: new FormControl(true),
      lowerBoundExceeded: new FormControl(true),
    });

    customWatchForm.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$)
      )
      .subscribe((val) => {
        if (val) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.query.selectedModel$.pipe(take(1)).subscribe((model) => {
            if (model) {
              const defaultNote = this.createDefaultNote(val, model);
              this.command.setDefaultNote(defaultNote);
            }
          });
        }
      });

    return customWatchForm;
  }

  buildNewDiagnoseForm() {
    const newDiagnoseForm = new FormGroup({
      diagnoseNote: new FormControl(''),
    });
    return newDiagnoseForm;
  }

  buildNewNoteForm() {
    const newNoteForm = new FormGroup({
      note: new FormControl(''),
    });

    return newNoteForm;
  }

  buildNewMaintainanceForm() {
    const newMaintenanceForm = new FormGroup({
      maintenanceNote: new FormControl(''),
    });
    return newMaintenanceForm;
  }

  createDefaultNote(watch: any, model?: INDModelSummary): string {
    let newDefaultNote = 'Default Note: Watch expires after ';
    if (watch.timeSpan) {
      newDefaultNote += watch.timeSpan;
    } else {
      newDefaultNote += '{}';
    }

    if (watch.timeUnit) {
      switch (watch.timeUnit) {
        case '1':
          newDefaultNote += ' second';
          break;
        case '2':
          newDefaultNote += ' minute';
          break;
        case '3':
          newDefaultNote += ' hour';
          break;
        case '4':
          newDefaultNote += ' day';
          break;
        case '7':
          newDefaultNote += ' week';
          break;
        default:
          // should this set an invalid flag
          break;
      }
      if (watch.timeSpan > 1) {
        newDefaultNote += 's';
      }
    } else {
      newDefaultNote += ' {}';
    }

    //upper bound default note
    if (watch.upperBoundExceeded) {
      newDefaultNote += ' or is overridden if the actual value is greater than';

      if (
        watch.upperControl === UpperBoundTypes.Model ||
        watch.upperControl === UpperBoundTypes.Relative
      ) {
        newDefaultNote += ' the model’s expected value plus';
      }
      if (!isNil(watch.upperUnits)) {
        if (watch.upperUnits === '') {
          watch.upperUnits = 0;
        }
        newDefaultNote += ' ' + watch.upperUnits.toPrecision(6);
      } else {
        newDefaultNote += ' {}';
      }
      if (model?.EngUnits) {
        newDefaultNote += ' ' + model.EngUnits;
      }
    }

    //lower bound default note
    if (watch.lowerBoundExceeded) {
      if (watch.upperBoundExceeded) {
        newDefaultNote += ' or less than';
      } else {
        newDefaultNote += ' or is overridden if the actual value is less than';
      }

      if (
        watch.lowerControl === LowerBoundTypes.Model ||
        watch.lowerControl === LowerBoundTypes.Relative
      ) {
        newDefaultNote += ' the model’s expected value minus';
      }
      if (!isNil(watch.lowerUnits)) {
        if (watch.lowerUnits === '') {
          watch.lowerUnits = 0;
        }
        newDefaultNote += ' ' + watch.lowerUnits.toPrecision(6);
      } else {
        newDefaultNote += ' {}';
      }
      if (model?.EngUnits) {
        newDefaultNote += ' ' + model.EngUnits;
      }
    }
    return newDefaultNote + '.';
  }

  emitModelAction(models: INDModelSummary[], actionType: string) {
    models = !Array.isArray(models) ? [models] : models;
    this.modelConfigEventBus.emit(
      new ModelConfigEmitEvent(
        ModelConfigBusEventTypes.MODEL_ACTION_STATE_CHANGE,
        {
          models,
          actionType,
        }
      )
    );
  }
  ngOnDestroy(): void {
    this.command.resetModelActionsState();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
