import {
  AlertsCoreService,
  AlertsFrameworkService,
  IModelConfigData,
  IModelIndependent,
} from '@atonix/shared/api';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  adapter,
  IModelInput,
  ModelEditState,
  ModelRefreshAction,
} from './model-edit.query';
import produce from 'immer';
import { Update } from '@ngrx/entity';
import { Observable, switchMap, tap } from 'rxjs';
import { isNil } from '@atonix/atx-core';
import { IForceIncludeMultiModelParams } from './model-edit.actions';
import { ToastService } from '@atonix/shared/utils';
import { setModelNonStandard } from './model-edit.utilities';

export class ModelEditMultiCommands {
  readonly logging: boolean = false;

  constructor(
    private store: ComponentStore<ModelEditState>,
    private alertsCoreService: AlertsCoreService,
    private toastService: ToastService
  ) {
    this.logging = true;
  }

  readonly multipleLoadSummaries = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        modelConfigSummaries: IModelConfigData[];
        refreshTrends: boolean;
      }
    ): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (payload.refreshTrends) {
            // after this is done we want to refresh the trends as well
            draftState.reloadModel = false;
            draftState.reloadPredictiveMethodsNeeded = true;
          }
        }
      ) as ModelRefreshAction;
      const hasSamePdServer: boolean = payload.modelConfigSummaries.every(
        (s) =>
          s.dependent.processDataServerGuid ===
          payload.modelConfigSummaries[0].dependent.processDataServerGuid
      );
      const multiModelIds = payload.modelConfigSummaries.map((m) =>
        m.modelExtID.toLocaleLowerCase()
      );

      const newState: ModelEditState = {
        ...state,
        updateFormElements: true,
        selectedModelId: payload.modelConfigSummaries[0].modelExtID,
        modelSummaryLoading: false,
        modelSaving: false,
        modelRefreshAction,
        multiModelIds,
        hasSamePdServer: hasSamePdServer,
        isNewModel: false, // No new models in multi-model config
      };
      return adapter.upsertMany(payload.modelConfigSummaries, newState);
    }
  );

  readonly refreshSummary = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        modelConfigSummaries: IModelConfigData[];
        refreshTrends: boolean;
      }
    ): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (payload.refreshTrends) {
            // after this is done we want to refresh the trends as well
            draftState.reloadModel = false;
            draftState.reloadPredictiveMethodsNeeded = true;
          }
        }
      ) as ModelRefreshAction;
      const modelExtID = payload.modelConfigSummaries[0].modelExtID || '';
      const modelSummary = {
        ...state.entities[modelExtID],
      } as IModelConfigData;
      const newModelSummary = produce(
        payload.modelConfigSummaries[0],
        (draftState) => {
          draftState.liveStatus = modelSummary.liveStatus;
        }
      );
      const newState: ModelEditState = {
        ...state,
        updateFormElements: true,
        modelSummaryLoading: false,
        modelSaving: false,
        modelRefreshAction,
        isNewModel: false, // No new models in multi-model config
      };
      return adapter.upsertOne(newModelSummary, newState);
    }
  );

  readonly resetReadState = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          draftState.reloadModel = false;
          draftState.reloadPredictiveMethodsNeeded = false;
        }
      ) as ModelRefreshAction;
      return {
        ...state,
        modelRefreshAction,
      };
    }
  );
  readonly getLatestModelSummary = this.store.effect(
    (
      modelSummary$: Observable<{
        modelID: string;
        refreshTrends: boolean;
      }>
    ) => {
      return modelSummary$.pipe(
        tap(() => this.resetReadState()),
        switchMap((req: { modelID: string; refreshTrends: boolean }) => {
          const models = [req.modelID];
          return this.alertsCoreService.getModelConfig(models).pipe(
            tapResponse(
              (modelConfigSummaries) =>
                this.refreshSummary({
                  modelConfigSummaries,
                  refreshTrends: req.refreshTrends,
                }),
              (error: string) => {
                console.error(JSON.stringify(error));
              }
            )
          );
        })
      );
    }
  );

  readonly multiSignalRMessage = this.store.updater(
    (
      state: ModelEditState,
      payload: {
        signalRMessage: string;
        modelExtID: string;
      }
    ): ModelEditState => {
      if (this.logging) {
        console.log(
          `${payload.modelExtID}: Signal R. ${JSON.stringify(
            payload.signalRMessage
          )}`
        );
      }
      let modelSummary = { ...state.entities[payload.modelExtID] };
      if (isNil(modelSummary?.modelExtID)) {
        modelSummary = {
          ...state.entities[payload.modelExtID.toLocaleUpperCase()],
        };
      }
      if (!modelSummary) {
        if (this.logging) {
          console.log(
            `${payload.modelExtID}: Error. Signal R Could Not Find ModelExtID`
          );
        }
        return state;
      }
      let liveStatus = modelSummary?.liveStatus || '';
      let reloadModel = false;
      let modelSaving = state.modelSaving;
      if (payload.signalRMessage.startsWith('Model build failed')) {
        liveStatus = payload.signalRMessage + '. Build completed with errors.';
        modelSaving = false;
        reloadModel = true;
      } else if (payload.signalRMessage.startsWith('Failure')) {
        liveStatus = payload.signalRMessage + '. Build completed with errors.';
        modelSaving = false;
        reloadModel = true;
      } else if (payload.signalRMessage === 'Built') {
        liveStatus = 'Build complete.';
        modelSaving = false;
        reloadModel = true;
      } else {
        liveStatus = payload.signalRMessage;
      }
      const newModelSummary = produce(modelSummary, (draftState) => {
        draftState.liveStatus = liveStatus;
      });
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          if (reloadModel) {
            draftState.modelExtID = payload.modelExtID;
            draftState.reloadModel = true;
            draftState.reloadPredictiveMethodsNeeded = false;
          }
        }
      ) as ModelRefreshAction;
      const updatedData = {
        id: newModelSummary.modelExtID,
        changes: {
          ...newModelSummary,
        },
      } as Update<IModelConfigData>;
      return adapter.updateOne(updatedData, {
        ...state,
        modelRefreshAction,
        modelSaving,
      });
    }
  );

  readonly addModelInputTags = this.store.updater(
    (
      state: ModelEditState,
      modelIndependents: IModelIndependent[]
    ): ModelEditState => {
      const updatedModels: Update<IModelConfigData>[] = [];
      const models =
        Object.keys(state.entities)
          .map((modelId) => state.entities[modelId])
          .filter((x) =>
            state.multiModelIds.includes(
              x?.modelExtID.toLocaleLowerCase() ?? ''
            )
          ) ?? [];

      if (!models) {
        return state;
      }

      if (models?.length > 0) {
        const dependentTagTracker: Record<string, IModelIndependent> = {};
        let allInputsExist = true;
        models.forEach((model) => {
          if (model) {
            const newModelIndependents: IModelIndependent[] = [];
            let modelIsDirty = false;
            modelIndependents.forEach((i) => {
              const inputAlreadyAdded = model.independents.find(
                (x) => x.tagGuid === i.tagGuid
              );
              const isDependentTag = i.tagGuid === model.dependent.tagGuid;
              if (isDependentTag) {
                allInputsExist = false;
                const tag = dependentTagTracker[i.tagGuid];
                if (!tag) {
                  dependentTagTracker[i.tagGuid] = i;
                }
              }
              if (!inputAlreadyAdded && !isDependentTag) {
                allInputsExist = false;
                modelIsDirty = true;
                newModelIndependents.push(i);
              }
            });
            if (modelIsDirty) {
              const newModel = produce(model, (draftState) => {
                if (!draftState?.independents) draftState.independents = [];
                draftState?.independents.push(...newModelIndependents);
                setModelNonStandard(draftState);
                draftState.isDirty = true;
              }) as IModelConfigData;

              const updatedModel = {
                id: model.modelExtID,
                changes: {
                  ...newModel,
                },
              } as Update<IModelConfigData>;
              updatedModels.push(updatedModel);
            }
          }
        });
        if (allInputsExist) {
          this.toastService.openSnackBar(
            `Selected ${
              modelIndependents.length > 1 ? 'tags' : 'tag'
            } already configured as input.`,
            'warning'
          );
          return state;
        }
        const dependentTagCount = Object.keys(dependentTagTracker).length;
        if (dependentTagCount > 0) {
          this.toastService.openSnackBar(
            `${dependentTagCount} ${
              dependentTagCount === 1 ? 'tag' : 'tags'
            } is used as a dependent tag in a model and cannot be used as an input.`,
            'warning'
          );
        }
        return adapter.updateMany(updatedModels, state);
      }
      return state;
    }
  );

  readonly removeModelInput = this.store.updater(
    (state: ModelEditState, modelInput: IModelInput): ModelEditState => {
      if (modelInput.modelExtIds.length === 1) {
        const modelExtId = modelInput.modelExtIds[0];
        if (state?.entities && state.entities[modelExtId]?.independents) {
          const modelSummary = {
            ...state.entities[modelExtId],
          } as IModelConfigData;
          const tagIdx = modelSummary.independents.findIndex(
            (x) => x.tagGuid === modelInput.input?.tagGuid
          );
          const newModelSummary = produce(modelSummary, (draftState) => {
            draftState?.independents.splice(tagIdx, 1);
            setModelNonStandard(draftState);
            draftState.isDirty = true;
          }) as IModelConfigData;

          const updatedData = {
            id: modelExtId,
            changes: {
              ...newModelSummary,
            },
          } as Update<IModelConfigData>;

          return adapter.updateOne(updatedData, state);
        }

        return state;
      } else {
        const updatedModels: Update<IModelConfigData>[] = [];
        const models =
          Object.keys(state.entities)
            .map((modelId) => state.entities[modelId])
            .filter((x) =>
              modelInput.modelExtIds.includes(x?.modelExtID ?? '')
            ) ?? [];

        if (models?.length > 0) {
          models.forEach((model) => {
            if (model) {
              const tagIdx = model.independents.findIndex(
                (x) => x.tagGuid === modelInput.input?.tagGuid
              );
              const newModel = produce(model, (draftState) => {
                draftState?.independents.splice(tagIdx, 1);
                setModelNonStandard(draftState);
                draftState.isDirty = true;
              }) as IModelConfigData;

              const updatedModel = {
                id: model.modelExtID,
                changes: {
                  ...newModel,
                },
              } as Update<IModelConfigData>;
              updatedModels.push(updatedModel);
            }
          });
          return adapter.updateMany(updatedModels, state);
        }

        return state;
      }
    }
  );

  readonly forceIncludeTags = this.store.updater(
    (
      state: ModelEditState,
      forceIncludeParam: IForceIncludeMultiModelParams
    ): ModelEditState => {
      if (forceIncludeParam.modelInput.modelExtIds.length === 1) {
        const modelExtId = forceIncludeParam.modelInput.modelExtIds[0];
        if (state?.entities && state.entities[modelExtId]?.independents) {
          const modelSummary = {
            ...state.entities[modelExtId],
          } as IModelConfigData;
          const tagIdx = modelSummary.independents.findIndex(
            (x) => x.tagGuid === forceIncludeParam.modelInput.input?.tagGuid
          );
          const newModelSummary = produce(modelSummary, (draftState) => {
            draftState.independents[tagIdx].forcedInclusion =
              !draftState.independents[tagIdx].forcedInclusion;
            draftState.isDirty = true;
          }) as IModelConfigData;

          const updatedData = {
            id: modelExtId,
            changes: {
              ...newModelSummary,
            },
          } as Update<IModelConfigData>;

          return adapter.updateOne(updatedData, state);
        }

        return state;
      } else {
        const updatedModels: Update<IModelConfigData>[] = [];
        const models =
          Object.keys(state.entities)
            .map((modelId) => state.entities[modelId])
            .filter((x) =>
              forceIncludeParam.modelInput.modelExtIds.includes(
                x?.modelExtID ?? ''
              )
            ) ?? [];

        if (models?.length > 0) {
          models.forEach((model) => {
            if (model) {
              const tagIdx = model.independents.findIndex(
                (x) => x.tagGuid === forceIncludeParam.modelInput.input?.tagGuid
              );

              const newModel = produce(model, (draftState) => {
                draftState.independents[tagIdx].forcedInclusion =
                  forceIncludeParam.forceIncludeAll;
                draftState.isDirty = true;
              }) as IModelConfigData;

              const updatedModel = {
                id: model.modelExtID,
                changes: {
                  ...newModel,
                },
              } as Update<IModelConfigData>;
              updatedModels.push(updatedModel);
            }
          });
          return adapter.updateMany(updatedModels, state);
        }

        return state;
      }
    }
  );

  readonly reloadModelConfig = this.store.updater(
    (state: ModelEditState): ModelEditState => {
      const modelRefreshAction = produce(
        state.modelRefreshAction,
        (draftState: ModelRefreshAction) => {
          draftState.reloadModel = true;
        }
      ) as ModelRefreshAction;
      return {
        ...state,
        modelRefreshAction,
      };
    }
  );

  getLockedModels(modelIds: string[]) {
    return this.alertsCoreService.getModelConfig(modelIds);
  }
}
