import { ITreeConfiguration } from '@atonix/atx-asset-tree';
import { INDModelSummary } from '@atonix/shared/api';
import { CriteriaGroup, OpModeDefinition } from '../model/op-mode';

export interface OpModeErrorState {
  hasErrors: boolean;
  isValidDateFormatMessage: string;
}
export interface OpModeState {
  assetTreeConfiguration: ITreeConfiguration | null;
  assetName: string | null;
  assetID: string | null;
  searchAssetID: number | null;
  highlightWarning: boolean;
  opMode: OpModeDefinition | null;
  isLoading: boolean;
  isDirty: boolean;
  errorState: OpModeErrorState;
  saveValidationResult: OpModeSaveValidationResult | null;
  isNewOpMode: boolean;
  formElements: OpModeFormElements | null;
}

export interface OpModeFormElements {
  opModeName: string;
  startCriteria: CriteriaGroup[];
  endCriteria: CriteriaGroup[];
}

export interface ExclusionPeriodCriteria {
  startDate: Date | null;
  endDate: Date | null;
}

export interface OpModeSaveValidationResult {
  affectedModels: AffectedModelInfo[];
  affectedModelCount: number;
  requiresBuild: boolean;
  suppressAffectedModelList?: boolean;
}

export interface OpModeWithModelsForMaintenanceParam {
  opMode: OpModeDefinition;
  maintenanceNote: string;
  affectedModelGuids: string[];
}

export interface AffectedModelInfo {
  modelExtID: string;
  name: string;
  path: string;
}

export interface TagListFilterIds {
  assetUniqueKey: string;
  searchAssetId: number;
}

export enum EOpModeCriteriaViewType {
  StartCriteria = 1,
  EndCriteria = 2,
  ExclusionPeriod = 3,
}

export enum ELogicalOperators {
  Equals = '=',
  GreaterThan = '>',
  LesserThan = '<',
  GreaterThanEqual = '>=',
  LesserThanEqual = '<=',
  NotEqual = '<>',
}
