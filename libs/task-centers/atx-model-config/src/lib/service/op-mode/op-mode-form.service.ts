import { Injectable, OnDestroy } from '@angular/core';
import { FormControl, UntypedFormControl, Validators } from '@angular/forms';
import { isNil } from '@atonix/atx-core';
import { isNumber, numberRegex } from '@atonix/shared/utils';
import { OPModeTypes } from '@atonix/shared/api';
import moment from 'moment';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { OpModeFacade } from './op-mode.facade';
import { EOpModeCriteriaViewType } from './op-mode.models';

@Injectable()
export class OpModeFormService implements OnDestroy {
  private subs = new SubSink();

  constructor(private opModeFacade: OpModeFacade) {}
  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  resetFormService() {
    this.subs.unsubscribe();
  }

  exclusionStartDate(startDateVal: string | Date): FormControl {
    const startDate = new FormControl(moment(startDateVal).format('MM/DD/YY'), [
      Validators.required,
    ]);
    return startDate;
  }

  exclusionStartTime(): FormControl {
    const startTime = new FormControl(null, [Validators.required]);
    this.subs.add(
      startTime.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          const currentStartTimeVal = String(value);
          this.opModeFacade.command.setExclusionPeriodStartTime({
            startTime: currentStartTimeVal,
          });
        })
    );
    return startTime;
  }

  exclusionEndDate(endDateVal: string | Date): FormControl {
    const endDate = new FormControl(moment(endDateVal).format('MM/DD/YY'), [
      Validators.required,
    ]);
    return endDate;
  }

  exclusionEndTime(): UntypedFormControl {
    const endTime = new UntypedFormControl(null, [Validators.required]);
    this.subs.add(
      endTime.valueChanges
        .pipe(debounceTime(100), distinctUntilChanged())
        .subscribe((value) => {
          const endTimeValue = String(value);
          this.opModeFacade.command.setExclusionPeriodEndTime({
            endTime: endTimeValue,
          });
        })
    );
    return endTime;
  }

  maintenanceNote(): FormControl {
    return new FormControl('');
  }

  opModeTitle(): FormControl {
    return new FormControl('', Validators.required);
  }

  opModeType(): FormControl {
    return new FormControl(OPModeTypes.OOS, Validators.required);
  }

  createOpModeName(name: string): FormControl {
    const logicFormControl = new FormControl(name, [Validators.required]);
    this.subs.add(
      logicFormControl.valueChanges
        .pipe(debounceTime(50), distinctUntilChanged())
        .subscribe((value) => {
          if (value) {
            this.opModeFacade.command.setOpModeName({
              opModeName: value,
            });
          }
        })
    );
    return logicFormControl;
  }

  logicValue(
    groupIdx: number,
    logicIdx: number,
    logicValue: string | null,
    criteriaType: EOpModeCriteriaViewType
  ): FormControl {
    const logicFormControl = new UntypedFormControl(logicValue, [
      Validators.pattern(numberRegex),
    ]);
    this.subs.add(
      logicFormControl.valueChanges
        .pipe(debounceTime(200), distinctUntilChanged())
        .subscribe((value) => {
          if (!isNumber(value)) {
            value = null;
          }

          this.opModeFacade.command.setCriteriaLogicValue({
            groupIdx,
            logicIdx,
            logicValue: value,
            criteriaType,
          });
        })
    );
    return logicFormControl;
  }
}
