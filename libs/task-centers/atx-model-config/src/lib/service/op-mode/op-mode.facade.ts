import { Injectable, OnDestroy } from '@angular/core';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import {
  AlertsFrameworkService,
  AssetFrameworkService,
  PrivateApiAlertsCoreService,
} from '@atonix/shared/api';
import {
  LoggerService,
  SaveChangesModalComponent,
  ToastService,
} from '@atonix/shared/utils';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { Store } from '@ngrx/store';
import { Subject, switchMap, Observable, catchError, EMPTY } from 'rxjs';
import { tagListSidePanelAssetTreeStateChange } from '../../store/actions/op-mode.actions';
import { OpModeCommands } from './op-mode.commands';
import {
  OpModeErrorState,
  OpModeState,
  TagListFilterIds,
} from './op-mode.models';
import { OpModeQuery } from './op-mode.query';
import {
  selectAllModelIdAndGuid,
  selectOpModetagListSidePanelSelectedAssetIds,
} from './../../store/reducers/model-config.reducer';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

export const _initialErrorState: OpModeErrorState = {
  hasErrors: false,
  isValidDateFormatMessage: '',
};

export const _initialState: OpModeState = {
  assetTreeConfiguration: null,
  assetName: null,
  assetID: null,
  formElements: null,
  searchAssetID: null,
  opMode: null,
  highlightWarning: false,
  errorState: _initialErrorState,
  isDirty: false,
  isLoading: false,
  saveValidationResult: null,
  isNewOpMode: false,
};

@Injectable()
export class OpModeFacade
  extends ComponentStore<OpModeState>
  implements OnDestroy
{
  public query = new OpModeQuery(this);
  public command = new OpModeCommands(
    this,
    this.store,
    this.alertsFrameworkService,
    this.privateApiAlertsCoreService,
    this.toastService,
    this.logger
  );
  public modelListIdsAndGuids$ = this.store.select(selectAllModelIdAndGuid);
  public tagListNavSelectedIds$ = this.store.select(
    selectOpModetagListSidePanelSelectedAssetIds
  );
  unsubscribe$ = new Subject<void>();
  constructor(
    public privateApiAlertsCoreService: PrivateApiAlertsCoreService,
    public alertsFrameworkService: AlertsFrameworkService,
    private assetFrameworkService: AssetFrameworkService,
    private store: Store,
    private toastService: ToastService,
    private logger: LoggerService,
    private matDialog: MatDialog
  ) {
    super(_initialState);
  }

  readonly updateSearchAssetID = this.effect(
    (assetIds$: Observable<TagListFilterIds>) =>
      assetIds$.pipe(
        switchMap((assetIds) =>
          this.assetFrameworkService
            .getAssetAndAncestors(assetIds.searchAssetId)
            .pipe(
              tapResponse(
                (response) => {
                  this.command.setAssetIdsAfterOpModeAssetTreeChange({
                    assetUniqueKey: assetIds.assetUniqueKey,
                    searchAssetId:
                      response.Unit?.AssetID || response.Asset?.AssetID,
                  });
                },
                (err: string) => console.log(err)
              )
            )
        ),
        catchError((error: unknown) => {
          console.log(error);
          return EMPTY;
        })
      )
  );

  tagListSidePanelAssetTreeStateChange(change: ITreeStateChange) {
    this.store.dispatch(
      tagListSidePanelAssetTreeStateChange({ treeStateChange: change })
    );
  }

  showOpModeDiscardChangesModal(
    callback: (dialogRef: MatDialogRef<SaveChangesModalComponent>) => void
  ) {
    const dialogRef = this.matDialog.open(SaveChangesModalComponent);
    callback(dialogRef);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
