import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { OpModeState } from './op-mode.models';

@Injectable()
export class OpModeQuery {
  constructor(private store: ComponentStore<OpModeState>) {}

  readonly selectedOpMode$ = this.store.select((state) => state.opMode);

  readonly formElements$ = this.store.select((state) => state.formElements, {
    debounce: true,
  });

  readonly associatedAssetsAndTags$ = this.store.select(
    (state) => state.opMode?.assetTagMaps?.filter((x) => x.include),
    { debounce: true }
  );

  readonly excludedAssetsAndTags$ = this.store.select(
    (state) => state.opMode?.assetTagMaps?.filter((x) => x.include === false),
    { debounce: true }
  );

  readonly saveValidationResult$ = this.store.select(
    (state) => state.saveValidationResult,
    { debounce: true }
  );

  readonly isNewOpMode$ = this.store.select(
    (state) => {
      return {
        isNewOpMode: state.isNewOpMode,
        opMode: state.opMode,
      };
    },
    { debounce: true }
  );

  readonly isLoading$ = this.store.select((state) => state.isLoading, {
    debounce: true,
  });

  readonly isDirty$ = this.store.select((state) => state.isDirty, {
    debounce: true,
  });

  readonly highlightWarning$ = this.store.select(
    (state) => state.highlightWarning,
    {
      debounce: true,
    }
  );

  readonly hasErrors$ = this.store.select(
    (state) => state.errorState?.hasErrors,
    {
      debounce: true,
    }
  );

  readonly isValidDateFormatMessage$ = this.store.select(
    (state) => state.errorState?.isValidDateFormatMessage,
    { debounce: true }
  );

  readonly assetTreeConfiguration$ = this.store.select(
    (state) => state.assetTreeConfiguration,
    { debounce: true }
  );

  readonly assetName$ = this.store.select((state) => state.assetName, {
    debounce: true,
  });

  readonly assetID$ = this.store.select((state) => state.assetID, {
    debounce: true,
  });

  readonly searchAssetID$ = this.store.select((state) => state.searchAssetID, {
    debounce: true,
  });

  readonly vm$ = this.store.select(
    this.selectedOpMode$,
    this.isLoading$,
    this.isValidDateFormatMessage$,
    this.isDirty$,
    this.hasErrors$,
    this.highlightWarning$,
    this.saveValidationResult$,
    this.isNewOpMode$,
    this.assetTreeConfiguration$,
    this.assetName$,
    this.assetID$,
    this.searchAssetID$,
    this.excludedAssetsAndTags$,
    this.associatedAssetsAndTags$,
    (
      selectedOpMode,
      isLoading,
      isValidDateFormatMessage,
      isDirty,
      hasErrors,
      highlightWarning,
      saveValidationResult,
      isNewOpMode,
      assetTreeConfiguration,
      assetName,
      assetID,
      searchAssetID,
      excludedAssetsAndTags,
      associatedAssetsAndTags
    ): any => {
      return {
        selectedOpMode,
        isLoading,
        isValidDateFormatMessage,
        isDirty,
        hasErrors,
        highlightWarning,
        saveValidationResult,
        isNewOpMode,
        assetTreeConfiguration,
        assetName,
        assetID,
        searchAssetID,
        excludedAssetsAndTags,
        associatedAssetsAndTags,
      };
    }
  );
}
