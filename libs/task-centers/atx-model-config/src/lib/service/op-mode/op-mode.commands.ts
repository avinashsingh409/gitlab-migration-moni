import { Injectable } from '@angular/core';
import { ITreeConfiguration } from '@atonix/atx-asset-tree';
import { isNil } from '@atonix/atx-core';
import {
  AlertsFrameworkService,
  IActionItem,
  INDModelSummary,
  OPModeTypes,
  PrivateApiAlertsCoreService,
} from '@atonix/shared/api';
import { combineTime, LoggerService, ToastService } from '@atonix/shared/utils';
import { ComponentStore } from '@ngrx/component-store';
import moment from 'moment';
import { catchError, concatMap, EMPTY, Observable, tap } from 'rxjs';
import {
  CriteriaGroup,
  AssetTagMap,
  OpModeDefinition,
  CriteriaLogic,
  OpModeSummary,
} from '../model/op-mode';
import {
  ELogicalOperators,
  EOpModeCriteriaViewType,
  TagListFilterIds,
  OpModeErrorState,
  OpModeSaveValidationResult,
  OpModeState,
  OpModeWithModelsForMaintenanceParam,
} from './op-mode.models';
import produce from 'immer';
import { HttpErrorResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import * as opModeActions from '../../store/actions/op-mode.actions';
import { _initialErrorState } from './op-mode.facade';

@Injectable()
export class OpModeCommands {
  private startDateAfterEndDateMessage = 'Start date must be before end date.';
  constructor(
    private componentStore: ComponentStore<OpModeState>,
    private readonly store: Store,
    public alertsFrameworkService: AlertsFrameworkService,
    public privateApiAlertsCoreService: PrivateApiAlertsCoreService,
    private toastService: ToastService,
    private logger: LoggerService
  ) {}

  readonly getSelectedOpMode = this.componentStore.effect(
    (
      httpRequest$: Observable<{
        opModeDefinitionID: number;
      }>
    ) => {
      return httpRequest$.pipe(
        tap((_) => this.setLoading(true)),
        concatMap((getOpMode) => {
          if (isNil(getOpMode)) {
            return EMPTY;
          }
          return this.privateApiAlertsCoreService
            .getOpMode(getOpMode.opModeDefinitionID)
            .pipe(
              tap({
                next: (opModeReturn: OpModeDefinition) => {
                  this.setIsNewOpMode(false);
                  return this.setOpModeDefinition(opModeReturn);
                },
                error: (e: unknown) => {
                  console.error(e);
                  this.toastService.openSnackBar(
                    (e as HttpErrorResponse).error.message,
                    'error'
                  );
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly validateOpMode = this.componentStore.effect(
    (
      httpRequest$: Observable<{
        opMode: OpModeDefinition;
      }>
    ) => {
      return httpRequest$.pipe(
        tap((_) => this.setSaving(true)),
        concatMap((getOpMode) => {
          if (isNil(getOpMode)) {
            return EMPTY;
          }
          return this.privateApiAlertsCoreService
            .validateOpMode(getOpMode.opMode)
            .pipe(
              tap({
                next: (opModeReturn: any) => {
                  return this.setOpModeValidationResult(
                    opModeReturn as OpModeSaveValidationResult
                  );
                },
                error: (e: unknown) => {
                  //This is how we are handling error state side effects in ComponentStore, since it does not have actions and reducers.
                  if (e instanceof HttpErrorResponse) {
                    const err = e as HttpErrorResponse;
                    if (
                      err.error?.code?.toLowerCase() ===
                      'selected_asset_above_unit'
                    ) {
                      return this.setSuppressAffectedModelList(true);
                    } else {
                      this.toastService.openSnackBar(
                        err.error.message,
                        'error'
                      );
                    }
                  }

                  console.error(e);
                  return this.setSaving(false);
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly saveOpMode = this.componentStore.effect(
    (
      httpRequest$: Observable<{
        opMode: OpModeDefinition;
      }>
    ) => {
      return httpRequest$.pipe(
        tap((_) => this.setSaving(true)),
        concatMap((getOpMode) => {
          if (isNil(getOpMode)) {
            return EMPTY;
          }

          return this.privateApiAlertsCoreService
            .saveOpMode(getOpMode.opMode)
            .pipe(
              tap({
                next: (opModeReturn: any) => {
                  this.toastService.openSnackBar('Op Mode Saved', 'success');
                  return this.setOpModeDefinition(opModeReturn?.data);
                },
                error: (e: unknown) => {
                  console.error(e);
                  this.toastService.openSnackBar(
                    (e as HttpErrorResponse).error.message,
                    'error'
                  );
                  return this.setSaving(false);
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly saveOpModeWithModelsForMaintenance = this.componentStore.effect(
    (httpRequest$: Observable<OpModeWithModelsForMaintenanceParam>) => {
      return httpRequest$.pipe(
        tap((_) => this.setSaving(true)),
        concatMap((param) => {
          if (isNil(param)) {
            return EMPTY;
          }

          return this.privateApiAlertsCoreService.saveOpMode(param.opMode).pipe(
            tap({
              next: (opModeReturn: any) => {
                this.toastService.openSnackBar('Op Mode Saved', 'success');
                this.setAffectedModelsForMaintenance({
                  note: param.maintenanceNote,
                  affectedModelGuids: param.affectedModelGuids,
                });
                return this.setOpModeDefinition(opModeReturn?.data);
              },
              error: (e: unknown) => {
                console.error(e);
                this.toastService.openSnackBar(
                  (e as HttpErrorResponse).error.message,
                  'error'
                );
                return this.setSaving(false);
              },
            }),
            catchError(() => EMPTY)
          );
        })
      );
    }
  );

  readonly setAffectedModelsForMaintenance = this.componentStore.effect(
    (
      httpRequest$: Observable<{
        note: string;
        affectedModelGuids: string[];
      }>
    ) => {
      return httpRequest$.pipe(
        concatMap((params) => {
          if (isNil(params)) {
            return EMPTY;
          }
          const actionItem: IActionItem = { NoteText: params.note };

          return this.alertsFrameworkService
            .setActionItemModelMaintenanceWithGuid(
              params.affectedModelGuids,
              actionItem,
              'savemaintenance'
            )
            .pipe(
              tap({
                next: (models: any) => {
                  this.toastService.openSnackBar(
                    'Set Model Maintenance Success!',
                    'success'
                  );
                  this.logger.trackTaskCenterEvent(
                    'ModelConfig:OpMode:Save:SetMaintenanceForAffectedModels'
                  );
                },
                error: (e: unknown) => {
                  console.error(e);
                  this.toastService.openSnackBar(
                    (e as HttpErrorResponse).message,
                    'error'
                  );
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly deleteOpMode = this.componentStore.effect(
    (
      httpRequest$: Observable<{
        opModeDefinitionID: number;
      }>
    ) => {
      return httpRequest$.pipe(
        tap((_) => this.setLoading(true)),
        concatMap((param) => {
          if (isNil(param)) {
            return EMPTY;
          }
          return this.privateApiAlertsCoreService
            .deleteOpMode(param.opModeDefinitionID)
            .pipe(
              tap({
                next: () => {
                  this.store.dispatch(
                    opModeActions.removeOpMode({
                      opModeDefinitionId: String(param.opModeDefinitionID),
                    })
                  );

                  this.clearSelectedOpMode();
                  this.setLoading(false);
                },
                error: (e: unknown) => {
                  this.setLoading(false);

                  const err = e as HttpErrorResponse;
                  let message = '';
                  if (err.error?.code) {
                    message = err.error?.message;
                  } else {
                    message = err.error?.title;
                  }

                  console.error(err);
                  this.toastService.openSnackBar(message, 'error');
                },
              }),
              catchError(() => EMPTY)
            );
        })
      );
    }
  );

  readonly setNewOpMode = this.componentStore.updater(
    (state: OpModeState, newOpMode: OpModeDefinition): OpModeState => {
      const currentDate = moment().seconds(0).milliseconds(0).toDate();
      const startDate = moment(currentDate).subtract(1, 'day').toDate();
      const opModeType = newOpMode.opModeTypeID as OPModeTypes;
      const newCriteria = this.createNewCriteria(
        opModeType,
        currentDate,
        startDate
      );
      newOpMode.startCriteria?.push(newCriteria);

      const errorState = this.validateCriteriaGroups(
        newOpMode?.opModeDefinitionTitle,
        newOpMode?.opModeTypeID as OPModeTypes,
        newOpMode?.startCriteria,
        newOpMode?.endCriteria,
        newOpMode?.assetTagMaps
      );
      return {
        ...state,
        opMode: newOpMode,
        formElements: {
          opModeName: newOpMode?.opModeDefinitionTitle || '',
          startCriteria: newOpMode?.startCriteria || [],
          endCriteria: newOpMode?.endCriteria || [],
        },
        errorState,
        isDirty: true,
        isLoading: false,
        isNewOpMode: true,
      };
    }
  );

  readonly setOpModeDefinition = this.componentStore.updater(
    (state: OpModeState, opMode: OpModeDefinition): OpModeState => {
      const refreshedOpMode: OpModeSummary = {
        opModeDefinitionExtID: opMode?.opModeDefinitionExtID || '',
        opModeDefinitionID: opMode?.opModeDefinitionID || 0,
        opModeDefinitionTitle: opMode?.opModeDefinitionTitle || '',
        opModeTypeAbbrev: opMode?.opModeTypeAbbrev || '',
        opModeTypeID: opMode?.opModeTypeID || 0,
        changeDate: moment(opMode?.changeDate).toDate(),
        createDate: moment(opMode?.createDate).toDate(),
      };
      this.store.dispatch(
        opModeActions.refreshOpMode({ opModeSummary: refreshedOpMode })
      );
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      return {
        ...state,
        opMode,
        formElements: {
          opModeName: opMode?.opModeDefinitionTitle || '',
          startCriteria: opMode?.startCriteria || [],
          endCriteria: opMode?.endCriteria || [],
        },
        highlightWarning: false,
        errorState,
        isDirty: false,
        isLoading: false,
      };
    }
  );

  readonly setOpModeValidationResult = this.componentStore.updater(
    (
      state: OpModeState,
      opModeValidationResult: OpModeSaveValidationResult
    ): OpModeState => {
      opModeValidationResult.affectedModelCount =
        opModeValidationResult.affectedModels.length;
      return {
        ...state,
        isLoading: false,
        saveValidationResult: opModeValidationResult,
      };
    }
  );

  readonly setSuppressAffectedModelList = this.componentStore.updater(
    (state: OpModeState, suppressAffectedModelList: boolean): OpModeState => {
      return {
        ...state,
        isLoading: false,
        saveValidationResult: {
          ...state.saveValidationResult,
          affectedModelCount: 0,
          requiresBuild: false,
          affectedModels: [],
          suppressAffectedModelList: suppressAffectedModelList,
        },
      };
    }
  );

  readonly setAssetIdsAfterOpModeAssetTreeChange = this.componentStore.updater(
    (state: OpModeState, ids: TagListFilterIds): OpModeState => {
      return {
        ...state,
        assetID: ids.assetUniqueKey,
        searchAssetID: ids.searchAssetId,
      };
    }
  );

  readonly resetTagListFilterIds = this.componentStore.updater(
    (state: OpModeState): OpModeState => {
      return {
        ...state,
        assetID: null,
        searchAssetID: null,
      };
    }
  );

  readonly clearForm = this.componentStore.updater(
    (state: OpModeState): OpModeState => {
      return {
        ...state,
        formElements: null,
        isLoading: true,
      };
    }
  );

  readonly opModeTypeChange = this.componentStore.updater(
    (state: OpModeState, opModeID: number): OpModeState => {
      const opModeType = opModeID as OPModeTypes;
      const oldOpModeType = state.opMode?.opModeTypeID;
      let changeCriteria = false;
      if (
        oldOpModeType === OPModeTypes.ExclusionPeriod &&
        opModeType !== OPModeTypes.ExclusionPeriod
      ) {
        changeCriteria = true;
      }
      if (
        oldOpModeType !== OPModeTypes.ExclusionPeriod &&
        opModeType === OPModeTypes.ExclusionPeriod
      ) {
        changeCriteria = true;
      }
      const currentDate = moment().seconds(0).milliseconds(0).toDate();
      const startDate = moment(currentDate).subtract(1, 'day').toDate();
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        const startCriteria: CriteriaGroup[] = [];
        opModeState.opModeTypeID = opModeType;
        opModeState.opModeTypeAbbrev = this.convertOpModeType(opModeType);

        if (changeCriteria) {
          const newCriteria = this.createNewCriteria(
            opModeType,
            currentDate,
            startDate
          );
          startCriteria?.push(newCriteria);
          opModeState.startCriteria = startCriteria;
          const endCriteria: CriteriaGroup[] = [];
          opModeState.endCriteria = endCriteria;
        }
      });

      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      return {
        ...state,
        opMode,
        formElements: {
          opModeName: opMode?.opModeDefinitionTitle || '',
          startCriteria: opMode?.startCriteria || [],
          endCriteria: opMode?.endCriteria || [],
        },
        isLoading: false,
        errorState,
        isDirty: true,
      };
    }
  );

  readonly addAssetTagMap = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        assetVariableTypeTagMapID?: number;
        assetGuid?: string;
        assetAbbrev?: string;
        tagDesc?: string;
        tagName?: string;
        include: boolean;
        bringDescendants: boolean;
      }
    ): OpModeState => {
      if (!state.opMode) {
        return state;
      }

      let trendIndex = 0;
      if (payload.assetVariableTypeTagMapID) {
        // it's a tag
        trendIndex =
          state.opMode?.assetTagMaps?.findIndex(
            (tagMap) =>
              tagMap.assetVariableTypeTagMapID ===
              payload.assetVariableTypeTagMapID
          ) ?? -1;
      } else {
        // it's an asset
        trendIndex =
          state.opMode?.assetTagMaps?.findIndex(
            (tagMap) => tagMap.assetGuid === payload.assetGuid
          ) ?? -1;
      }

      if (typeof trendIndex == 'number' && trendIndex >= 0) {
        this.toastService.openSnackBar(
          'This tag is already included.',
          'error'
        );
        return state;
      } else {
        let opModeIndex = -1;
        const currentTagMaps = state.opMode?.assetTagMaps as AssetTagMap[];
        if (currentTagMaps.length > 0) {
          opModeIndex =
            currentTagMaps.reduce(function (prev, curr) {
              return prev.opModeDefinitionAssetTagMapID <
                curr.opModeDefinitionAssetTagMapID
                ? prev
                : curr;
            }).opModeDefinitionAssetTagMapID ?? 1;

          if (opModeIndex > 0) {
            opModeIndex = -1;
          } else {
            opModeIndex--;
          }
        }

        const assetTagMaps = produce(
          state.opMode?.assetTagMaps,
          (draftState: AssetTagMap[]) => {
            const tagMap: AssetTagMap = {
              assetAbbrev: payload.assetAbbrev,
              opModeDefinitionAssetTagMapID: opModeIndex,
              assetGuid: payload.assetGuid,
              assetVariableTypeTagMapID: payload.assetVariableTypeTagMapID,
              include: payload.include,
              tagDesc: payload.tagDesc,
              tagName: payload.tagName,
              bringDescendants: payload.bringDescendants,
            };
            draftState.push(tagMap);
          }
        ) as AssetTagMap[];
        const errorState = this.validateCriteriaGroups(
          state.opMode?.opModeDefinitionTitle,
          state.opMode?.opModeTypeID as OPModeTypes,
          state.opMode?.startCriteria,
          state.opMode?.endCriteria,
          assetTagMaps
        );
        return {
          ...state,
          opMode: {
            ...state.opMode,
            assetTagMaps,
          },
          isDirty: true,
          errorState,
        };
      }
    }
  );

  readonly removeAssetTagMap = this.componentStore.updater(
    (state: OpModeState, assetTagMap: AssetTagMap): OpModeState => {
      if (state.opMode) {
        const assetTagMaps = produce(
          state.opMode?.assetTagMaps,
          (draftState: AssetTagMap[]) => {
            const assetTagMapIndex: number | undefined =
              state.opMode?.assetTagMaps?.findIndex(
                (atm: AssetTagMap) =>
                  atm.opModeDefinitionAssetTagMapID ===
                  assetTagMap.opModeDefinitionAssetTagMapID
              );
            if (
              typeof assetTagMapIndex === 'number' &&
              assetTagMapIndex !== -1
            ) {
              draftState.splice(assetTagMapIndex, 1);
            }
          }
        ) as AssetTagMap[];
        const errorState = this.validateCriteriaGroups(
          state.opMode?.opModeDefinitionTitle,
          state.opMode?.opModeTypeID as OPModeTypes,
          state.opMode?.startCriteria,
          state.opMode?.endCriteria,
          assetTagMaps
        );
        return {
          ...state,
          opMode: {
            ...state.opMode,
            assetTagMaps,
          },
          errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly changeAssetTagMapDescendants = this.componentStore.updater(
    (state: OpModeState, assetTagMap: AssetTagMap): OpModeState => {
      if (state.opMode && state.opMode.assetTagMaps) {
        const assetTagMaps = produce(
          state.opMode?.assetTagMaps,
          (draftState: AssetTagMap[]) => {
            const assetTagMapIndex: number | undefined =
              state.opMode?.assetTagMaps?.findIndex(
                (atm: AssetTagMap) =>
                  atm.opModeDefinitionAssetTagMapID ===
                  assetTagMap.opModeDefinitionAssetTagMapID
              );
            if (
              state?.opMode?.assetTagMaps &&
              typeof assetTagMapIndex === 'number' &&
              assetTagMapIndex !== -1
            ) {
              draftState[assetTagMapIndex].bringDescendants =
                !state?.opMode?.assetTagMaps[assetTagMapIndex].bringDescendants;
            }
          }
        ) as AssetTagMap[];
        const errorState = this.validateCriteriaGroups(
          state.opMode?.opModeDefinitionTitle,
          state.opMode?.opModeTypeID as OPModeTypes,
          state.opMode?.startCriteria,
          state.opMode?.endCriteria,
          assetTagMaps
        );
        return {
          ...state,
          opMode: {
            ...state.opMode,
            assetTagMaps,
          },
          errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly setAssetConfiguration = this.componentStore.updater(
    (
      state: OpModeState,
      assetTreeConfiguration: ITreeConfiguration
    ): OpModeState => {
      return {
        ...state,
        assetTreeConfiguration,
      };
    }
  );

  readonly setAssetName = this.componentStore.updater(
    (state: OpModeState, assetName: string): OpModeState => {
      return {
        ...state,
        assetName,
      };
    }
  );

  readonly setExclusionPeriodStartDate = this.componentStore.updater(
    (state: OpModeState, payload: { startDate?: Date }): OpModeState => {
      if (payload.startDate && state?.opMode?.startCriteria) {
        const startTime = moment(
          state.opMode?.startCriteria[0].logic[0].logicStartDate
        ).format('HH:mm');
        const newStartDate = combineTime(payload.startDate, startTime);
        const opMode = produce(
          state.opMode,
          (opModeState: OpModeDefinition) => {
            let criteria: CriteriaGroup[] = [];
            criteria = produce(
              state.opMode?.startCriteria as CriteriaGroup[],
              (draftState: CriteriaGroup[]) => {
                draftState[0].logic[0].logicStartDate = newStartDate;
              }
            ) as CriteriaGroup[];
            opModeState.startCriteria = criteria;
          }
        );
        const errorState = this.validateCriteriaGroups(
          opMode?.opModeDefinitionTitle,
          opMode?.opModeTypeID as OPModeTypes,
          opMode?.startCriteria,
          opMode?.endCriteria,
          opMode?.assetTagMaps
        );
        return {
          ...state,
          opMode,
          errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly setExclusionPeriodStartTime = this.componentStore.updater(
    (state: OpModeState, payload: { startTime?: string }): OpModeState => {
      if (payload.startTime && state?.opMode?.startCriteria) {
        const startDate = moment(
          state.opMode?.startCriteria[0].logic[0].logicStartDate
        ).toDate();
        const newStartDate = combineTime(startDate, payload.startTime);
        const opMode = produce(
          state.opMode,
          (opModeState: OpModeDefinition) => {
            let criteria: CriteriaGroup[] = [];
            criteria = produce(
              state.opMode?.startCriteria as CriteriaGroup[],
              (draftState: CriteriaGroup[]) => {
                draftState[0].logic[0].logicStartDate = newStartDate;
              }
            ) as CriteriaGroup[];
            opModeState.startCriteria = criteria;
          }
        );

        const errorState = this.validateCriteriaGroups(
          opMode?.opModeDefinitionTitle,
          opMode?.opModeTypeID as OPModeTypes,
          opMode?.startCriteria,
          opMode?.endCriteria,
          opMode?.assetTagMaps
        );
        return {
          ...state,
          opMode,
          errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly setExclusionPeriodEndDate = this.componentStore.updater(
    (state: OpModeState, payload: { endDate?: Date }): OpModeState => {
      if (payload.endDate && state?.opMode?.startCriteria) {
        const endTime = moment(
          state.opMode?.startCriteria[0].logic[0].logicEndDate
        ).format('HH:mm');
        const newEndDate = combineTime(payload.endDate, endTime);
        const opMode = produce(
          state.opMode,
          (opModeState: OpModeDefinition) => {
            let criteria: CriteriaGroup[] = [];
            criteria = produce(
              state.opMode?.startCriteria as CriteriaGroup[],
              (draftState: CriteriaGroup[]) => {
                draftState[0].logic[0].logicEndDate = newEndDate;
              }
            ) as CriteriaGroup[];
            opModeState.startCriteria = criteria;
          }
        );
        const errorState = this.validateCriteriaGroups(
          opMode?.opModeDefinitionTitle,
          opMode?.opModeTypeID as OPModeTypes,
          opMode?.startCriteria,
          opMode?.endCriteria,
          opMode?.assetTagMaps
        );
        return {
          ...state,
          opMode,
          errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly setExclusionPeriodEndTime = this.componentStore.updater(
    (state: OpModeState, payload: { endTime?: string }): OpModeState => {
      if (payload.endTime && state?.opMode?.startCriteria) {
        const endDate = moment(
          state.opMode?.startCriteria[0].logic[0].logicEndDate
        ).toDate();
        const newEndDate = combineTime(endDate, payload.endTime);
        const opMode = produce(
          state.opMode,
          (opModeState: OpModeDefinition) => {
            let criteria: CriteriaGroup[] = [];
            criteria = produce(
              state.opMode?.startCriteria as CriteriaGroup[],
              (draftState: CriteriaGroup[]) => {
                draftState[0].logic[0].logicEndDate = newEndDate;
              }
            ) as CriteriaGroup[];
            opModeState.startCriteria = criteria;
          }
        );
        const errorState = this.validateCriteriaGroups(
          opMode?.opModeDefinitionTitle,
          opMode?.opModeTypeID as OPModeTypes,
          opMode?.startCriteria,
          opMode?.endCriteria,
          opMode?.assetTagMaps
        );
        return {
          ...state,
          opMode,
          errorState: errorState,
          isDirty: true,
        };
      } else {
        return state;
      }
    }
  );

  readonly setCriteriaLogicOperator = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        groupIdx: number;
        logicIdx: number;
        logicOperator: ELogicalOperators;
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        let criteria: CriteriaGroup[] = [];
        if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
          criteria = produce(
            state.opMode?.startCriteria as CriteriaGroup[],
            (draftState: CriteriaGroup[]) => {
              const logicID = this.convertOpModeLogicAbbrevToID(
                payload.logicOperator
              );
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].logicOperatorTypeID = logicID;
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].logicOperatorTypeAbbrev = payload.logicOperator;
            }
          ) as CriteriaGroup[];
          opModeState.startCriteria = criteria;
        } else if (
          payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
        ) {
          criteria = produce(
            state.opMode?.endCriteria as CriteriaGroup[],
            (draftState: CriteriaGroup[]) => {
              const logicID = this.convertOpModeLogicAbbrevToID(
                payload.logicOperator
              );
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].logicOperatorTypeID = logicID;
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].logicOperatorTypeAbbrev = payload.logicOperator;
            }
          ) as CriteriaGroup[];
          opModeState.endCriteria = criteria;
        }
      });
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          opMode,
          highlightWarning: true,
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode,
        isDirty: true,
        errorState,
      };
    }
  );

  readonly addCriteriaLogicTag = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        groupIdx: number;
        logicIdx: number;
        tagName: string;
        tagDesc: string;
        tagID: number;
        tagUnits: string;
        assetVariableTypeTagMapID: number;
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        let criteria: CriteriaGroup[] = [];
        if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
          criteria = produce(
            state.opMode?.startCriteria,
            (draftState: CriteriaGroup[]) => {
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].assetVariableTypeTagMapID = payload.assetVariableTypeTagMapID;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagDesc =
                payload.tagDesc;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagName =
                payload.tagName;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagID =
                payload.tagID;
              draftState[payload.groupIdx].logic[payload.logicIdx].engUnits =
                payload.tagUnits;
            }
          ) as CriteriaGroup[];
          opModeState.startCriteria = criteria;
        } else if (
          payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
        ) {
          criteria = produce(
            state.opMode?.endCriteria,
            (draftState: CriteriaGroup[]) => {
              draftState[payload.groupIdx].logic[
                payload.logicIdx
              ].assetVariableTypeTagMapID = payload.assetVariableTypeTagMapID;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagDesc =
                payload.tagDesc;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagName =
                payload.tagName;
              draftState[payload.groupIdx].logic[payload.logicIdx].tagID =
                payload.tagID;
              draftState[payload.groupIdx].logic[payload.logicIdx].engUnits =
                payload.tagUnits;
            }
          ) as CriteriaGroup[];
          opModeState.endCriteria = criteria;
        }
      }) as OpModeDefinition;
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          opMode,
          highlightWarning: true,
          formElements: {
            opModeName: opMode?.opModeDefinitionTitle || '',
            startCriteria: opMode?.startCriteria || [],
            endCriteria: opMode?.endCriteria || [],
          },
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode,
        formElements: {
          opModeName: opMode?.opModeDefinitionTitle || '',
          startCriteria: opMode?.startCriteria || [],
          endCriteria: opMode?.endCriteria || [],
        },
        isDirty: true,
        errorState,
      };
    }
  );

  readonly addCriteriaLogic = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        groupIdx: number;
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        const criteriaLogic: CriteriaLogic = {
          criteriaLogicID: -1,
          logicOperatorTypeID: 3,
          assetVariableTypeTagMapID: -1,
          logicOperatorTypeAbbrev: '<',
          value: 0,
          logicStartDate: null,
          logicEndDate: null,
          tagName: '',
          tagDesc: '',
          engUnits: '',
          transiencyStdDevMultiplier: null,
          transiencyDuration: null,
          transiencyDurationTemporalTypeID: null,
          processDataServerGuid: null,
        };
        let criteria: CriteriaGroup[] = [];
        if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
          criteria = produce(
            state.opMode?.startCriteria,
            (draftState: CriteriaGroup[]) => {
              draftState[payload.groupIdx].logic.push(criteriaLogic);
            }
          ) as CriteriaGroup[];
          opModeState.startCriteria = criteria;
        } else if (
          payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
        ) {
          criteria = produce(
            state.opMode?.endCriteria,
            (draftState: CriteriaGroup[]) => {
              draftState[payload.groupIdx].logic.push(criteriaLogic);
            }
          ) as CriteriaGroup[];
          opModeState.endCriteria = criteria;
        }
      });
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          opMode,
          highlightWarning: true,
          formElements: {
            opModeName: opMode?.opModeDefinitionTitle || '',
            startCriteria: opMode?.startCriteria || [],
            endCriteria: opMode?.endCriteria || [],
          },
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode,
        formElements: {
          opModeName: opMode?.opModeDefinitionTitle || '',
          startCriteria: opMode?.startCriteria || [],
          endCriteria: opMode?.endCriteria || [],
        },
        isDirty: true,
        errorState,
      };
    }
  );

  readonly deleteLogic = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        groupIdx: number;
        logicIdx: number;
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      let isLastLogic = false;
      const newOpMode = produce(
        state.opMode,
        (opModeState: OpModeDefinition) => {
          let criteria: CriteriaGroup[] = [];
          if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
            criteria = produce(
              state.opMode?.startCriteria,
              (draftState: CriteriaGroup[]) => {
                draftState[payload.groupIdx].logic.splice(payload.logicIdx, 1);
              }
            ) as CriteriaGroup[];
            opModeState.startCriteria = criteria;
          } else if (
            payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
          ) {
            criteria = produce(
              state.opMode?.endCriteria,
              (draftState: CriteriaGroup[]) => {
                draftState[payload.groupIdx].logic.splice(payload.logicIdx, 1);
              }
            ) as CriteriaGroup[];
            opModeState.endCriteria = criteria;
          }
          isLastLogic = !criteria[payload.groupIdx].logic?.length;
        }
      );

      if (isLastLogic) {
        this.deleteCriteria({
          criteriaType: payload.criteriaType,
          idx: payload.groupIdx,
        });
      }
      const errorState = this.validateCriteriaGroups(
        newOpMode?.opModeDefinitionTitle,
        newOpMode?.opModeTypeID as OPModeTypes,
        newOpMode?.startCriteria,
        newOpMode?.endCriteria,
        newOpMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          highlightWarning: true,
          opMode: newOpMode,
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode: newOpMode,
        isDirty: true,
        errorState,
      };
    }
  );

  readonly setOpModeName = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        opModeName: string;
      }
    ): OpModeState => {
      if (!state?.opMode?.startCriteria || !state?.opMode.endCriteria) {
        return state;
      }
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        opModeState.opModeDefinitionTitle = payload.opModeName;
      });
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      return {
        ...state,
        opMode,
        isDirty: true,
        errorState,
      };
    }
  );

  readonly setCriteriaLogicValue = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        groupIdx: number;
        logicIdx: number;
        logicValue: string | null;
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      const opMode = produce(state.opMode, (opModeState: OpModeDefinition) => {
        let criteria: CriteriaGroup[] = [];

        if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
          criteria = produce(
            state.opMode?.startCriteria,
            (draftState: CriteriaGroup[]) => {
              if (payload.logicValue) {
                draftState[payload.groupIdx].logic[payload.logicIdx].value =
                  +payload?.logicValue;
              } else {
                draftState[payload.groupIdx].logic[payload.logicIdx].value =
                  null;
              }
            }
          ) as CriteriaGroup[];
          opModeState.startCriteria = criteria;
        } else if (
          payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
        ) {
          criteria = produce(
            state.opMode?.endCriteria as CriteriaGroup[],
            (draftState: CriteriaGroup[]) => {
              if (payload.logicValue) {
                draftState[payload.groupIdx].logic[payload.logicIdx].value =
                  +payload.logicValue;
              } else {
                draftState[payload.groupIdx].logic[payload.logicIdx].value =
                  null;
              }
            }
          ) as CriteriaGroup[];
          opModeState.endCriteria = criteria;
        }
      });
      const errorState = this.validateCriteriaGroups(
        opMode?.opModeDefinitionTitle,
        opMode?.opModeTypeID as OPModeTypes,
        opMode?.startCriteria,
        opMode?.endCriteria,
        opMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          highlightWarning: true,
          opMode,
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode,
        isDirty: true,
        errorState,
      };
    }
  );

  readonly addCriteria = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        criteriaType: EOpModeCriteriaViewType;
      }
    ): OpModeState => {
      const newOpMode = produce(
        state.opMode,
        (draftState: OpModeDefinition) => {
          const newCriteria = this.createNewCriteria(draftState.opModeTypeID);
          if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
            draftState.startCriteria = [
              ...(state.opMode?.startCriteria ?? []),
              newCriteria,
            ];
          } else if (
            payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
          ) {
            newCriteria.isStart = false;
            draftState.endCriteria = [
              ...(state.opMode?.endCriteria ?? []),
              newCriteria,
            ];
          }
        }
      );
      const errorState = this.validateCriteriaGroups(
        newOpMode?.opModeDefinitionTitle,
        newOpMode?.opModeTypeID as OPModeTypes,
        newOpMode?.startCriteria,
        newOpMode?.endCriteria,
        newOpMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          opMode: newOpMode,
          formElements: {
            opModeName: newOpMode?.opModeDefinitionTitle || '',
            startCriteria: newOpMode?.startCriteria || [],
            endCriteria: newOpMode?.endCriteria || [],
          },
          highlightWarning: true,
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode: newOpMode,
        formElements: {
          opModeName: newOpMode?.opModeDefinitionTitle || '',
          startCriteria: newOpMode?.startCriteria || [],
          endCriteria: newOpMode?.endCriteria || [],
        },
        isDirty: true,
        errorState,
      };
    }
  );

  readonly deleteCriteria = this.componentStore.updater(
    (
      state: OpModeState,
      payload: {
        criteriaType: EOpModeCriteriaViewType;
        idx: number;
      }
    ): OpModeState => {
      const newOpMode = produce(
        state.opMode,
        (draftState: OpModeDefinition) => {
          if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
            draftState.startCriteria?.splice(payload.idx, 1);
          } else if (
            payload.criteriaType === EOpModeCriteriaViewType.EndCriteria
          ) {
            draftState.endCriteria?.splice(payload.idx, 1);
          }
        }
      );
      const errorState = this.validateCriteriaGroups(
        newOpMode?.opModeDefinitionTitle,
        newOpMode?.opModeTypeID as OPModeTypes,
        newOpMode?.startCriteria,
        newOpMode?.endCriteria,
        newOpMode?.assetTagMaps
      );
      if (payload.criteriaType === EOpModeCriteriaViewType.StartCriteria) {
        return {
          ...state,
          highlightWarning: true,
          opMode: newOpMode,
          isDirty: true,
          errorState,
        };
      }
      return {
        ...state,
        opMode: newOpMode,
        isDirty: true,
        errorState,
      };
    }
  );

  readonly setLoading = this.componentStore.updater(
    (state: OpModeState, isLoading: boolean): OpModeState => {
      return {
        ...state,
        opMode: null,
        formElements: null,
        errorState: _initialErrorState,
        isLoading,
      };
    }
  );

  readonly setSaving = this.componentStore.updater(
    (state: OpModeState, isSaving: boolean): OpModeState => {
      return {
        ...state,
        isLoading: isSaving,
      };
    }
  );

  readonly setIsNewOpMode = this.componentStore.updater(
    (state: OpModeState, isNewOpMode: boolean): OpModeState => {
      return {
        ...state,
        isNewOpMode,
      };
    }
  );

  readonly clearSelectedOpMode = this.componentStore.updater(
    (state: OpModeState): OpModeState => {
      return {
        ...state,
        opMode: null,
        formElements: null,
        saveValidationResult: null,
        errorState: _initialErrorState,
        isDirty: false,
      };
    }
  );

  private validateCriteriaGroups(
    opModeTitle: string | undefined,
    opModeType: OPModeTypes,
    startCriteria: CriteriaGroup[] | undefined,
    endCriteria: CriteriaGroup[] | undefined,
    assetTagMaps: AssetTagMap[] | undefined
  ): OpModeErrorState {
    let hasErrors = false;
    if (opModeType === OPModeTypes.ExclusionPeriod) {
      if (
        startCriteria &&
        startCriteria?.length &&
        startCriteria[0].logic &&
        startCriteria[0].logic.length &&
        startCriteria[0].logic[0].logicStartDate &&
        startCriteria[0].logic[0].logicEndDate
      ) {
        const validateReturn = this.validateExclusionStartEndDates(
          startCriteria[0].logic[0].logicStartDate,
          startCriteria[0].logic[0].logicEndDate
        );
        if (!validateReturn.isValidDateFormat) {
          return {
            hasErrors: true,
            isValidDateFormatMessage: validateReturn.isValidDateFormatMessage,
          };
        }
      } else {
        return {
          hasErrors: true,
          isValidDateFormatMessage: 'Invalid Date',
        };
      }
    } else {
      hasErrors =
        !startCriteria ||
        startCriteria.some((c) =>
          c.logic.some((l) => isNil(l.value) || isNil(l.tagID))
        );
      if (hasErrors) {
        return {
          hasErrors: true,
          isValidDateFormatMessage: '',
        };
      }
      hasErrors =
        !endCriteria ||
        endCriteria.some((c) =>
          c.logic.some((l) => isNil(l.value) || isNil(l.tagID))
        );
      if (hasErrors) {
        return {
          hasErrors: true,
          isValidDateFormatMessage: '',
        };
      }
      if (!startCriteria || startCriteria?.length <= 0) {
        // Must have at least one starting criteria. Can have no ending criteria
        return {
          hasErrors: true,
          isValidDateFormatMessage: '',
        };
      }
    }

    if (
      !assetTagMaps ||
      assetTagMaps?.length <= 0 ||
      !assetTagMaps.some((c) => c.include === true)
    ) {
      return {
        hasErrors: true,
        isValidDateFormatMessage: '',
      };
    }
    if (opModeTitle && opModeTitle.length <= 0) {
      return {
        hasErrors: true,
        isValidDateFormatMessage: '',
      };
    }

    return {
      hasErrors: false,
      isValidDateFormatMessage: '',
    };
  }

  validateExclusionStartEndDates(
    startDate: Date,
    endDate: Date
  ): { isValidDateFormat: boolean; isValidDateFormatMessage: string } {
    if (!startDate || !endDate) {
      return {
        isValidDateFormat: false,
        isValidDateFormatMessage: '',
      };
    }
    if (!moment(endDate).isValid() || !moment(startDate).isValid()) {
      return {
        isValidDateFormat: false,
        isValidDateFormatMessage: '',
      };
    }
    if (moment(startDate).isBefore(moment(endDate))) {
      return {
        isValidDateFormat: true,
        isValidDateFormatMessage: '',
      };
    } else {
      return {
        isValidDateFormat: false,
        isValidDateFormatMessage: this.startDateAfterEndDateMessage,
      };
    }
  }

  private convertOpModeType(opModeType: OPModeTypes): string {
    switch (opModeType) {
      case OPModeTypes.ExclusionPeriod:
        return 'Exclusion Period';
      case OPModeTypes.OOS:
        return 'Out of Service';
      case OPModeTypes.Startup:
        return 'Startup';
      case OPModeTypes.SteadyState:
        return 'Steady State';
      case OPModeTypes.Transient:
        return 'Transient';
      default:
        console.error('op mode type not found');
        return 'not Found';
    }
  }

  private convertOpModeLogicAbbrevToID(logicAbbrev: string): number {
    switch (logicAbbrev) {
      case '=':
        return 1;
      case '>':
        return 2;
      case '<':
        return 3;
      case '>=':
        return 4;
      case '<=':
        return 5;
      case '<>':
        return 6;
      default:
        console.error('logic abbreviation not found');
        return 1;
    }
  }

  private createNewCriteria(
    opModeType: OPModeTypes,
    currentDate?: Date,
    startDate?: Date
  ) {
    const isExlusionPeriod = opModeType === OPModeTypes.ExclusionPeriod;
    const logic = {
      logicStartDate: isExlusionPeriod ? startDate : null,
      logicEndDate: isExlusionPeriod ? currentDate : null,
      criteriaLogicID: -1,
      assetVariableTypeTagMapID: -1,
      logicOperatorTypeID: 3,
      logicOperatorTypeAbbrev: ELogicalOperators.LesserThan,
      value: 0,
    } as CriteriaLogic;

    return {
      criteriaGroupID: -1,
      displayOrder: 0,
      isStart: true,
      logic: [logic],
    } as CriteriaGroup;
  }
}
