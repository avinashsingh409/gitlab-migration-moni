import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, withLatestFrom, take, Subject, takeUntil } from 'rxjs';
import { IssueSnapshotFacade } from '../../store/facade/issue-snapshot.facade';
import { IIssueSnapshotState } from '../../store/state/issue-snapshot-state';
import { IIssueSummaryState } from '../../model/issue-summary-state';
import {
  IWorkRequest,
  IWorkRequestLoading,
  ExternalAsset,
} from '@atonix/shared/api';
import { WorkRequestPriorities } from '../../model/work-request-priority.model';
import {
  ClientSideRowModelModule,
  ClipboardModule,
  ColumnsToolPanelModule,
  EnterpriseCoreModule,
  FiltersToolPanelModule,
  GridApi,
  GridOptions,
  GridReadyEvent,
  Module,
  RowNode,
  SetFilterModule,
} from '@ag-grid-enterprise/all-modules';
import { IAssetResult } from '@atonix/atx-core';
import { MatTabChangeEvent } from '@angular/material/tabs';

export interface ICreateWorkRequestDialogData {
  title: string;
}

@Component({
  selector: 'atx-create-work-order-dialog',
  templateUrl: './create-work-order-dialog.component.html',
  styleUrls: ['./create-work-order-dialog.component.scss'],
})
export class CreateWorkOrderDialogComponent implements OnInit, OnDestroy {
  workOrderForm = this.fb.group({
    title: ['', Validators.required],
    description: ['', Validators.required],
    priority: ['2'],
  });

  private gridApi: GridApi | null = null;
  private workItemsGridApi: GridApi | null = null;

  private unsubscribe$ = new Subject<void>();

  workOrderInfo$: Observable<IWorkRequest>;
  summaryState$: Observable<IIssueSummaryState>;
  selectIssueSnapshotState$: Observable<IIssueSnapshotState>;
  assetName$: Observable<string>;
  theme$: Observable<string>;
  workRequestLoading$: Observable<IWorkRequestLoading>;
  selectedExternalAsset$: Observable<ExternalAsset>;
  externalAssets$: Observable<ExternalAsset[]>;

  WorkRequestPriorities = WorkRequestPriorities;

  public modules: Module[] = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    ClientSideRowModelModule,
    SetFilterModule,
    ClipboardModule,
  ];

  public gridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'single',
    rowHeight: 30,
    headerHeight: 20,
    onSelectionChanged: () => {
      const selectedRow =
        this.gridApi && this.gridApi.getSelectedNodes().length > 0
          ? this.gridApi.getSelectedNodes()[0].data
          : null;
      if (selectedRow) {
        this.issueSnapshotFacade.updateSelectedExternalAsset(selectedRow);
      }
    },
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      filter: 'agTextColumnFilter',
      sortable: true,
      enableRowGroup: false,
      enablePivot: false,
      enableValue: false,
      editable: false,
      width: 360,
    },
    treeData: true,
    groupDefaultExpanded: 0,
    getDataPath: (data: IAssetResult) => data.Hierarchy,
    autoGroupColumnDef: {
      headerName: 'Hierarchy',
      cellRendererParams: {
        suppressCount: true,
      },
      valueFormatter: (row) =>
        `${row.data.ExternalAssetID}: ${row.data.ExternalAssetName}`,
    },
    columnDefs: [
      {
        headerName: 'ID',
        colId: 'ExternalAssetID',
        field: 'ExternalAssetID',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 160,
      },
      {
        headerName: 'Name',
        colId: 'ExternalAssetName',
        field: 'ExternalAssetName',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 200,
      },
      {
        headerName: 'Type',
        colId: 'ExternalAssetType',
        field: 'ExternalAssetType',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 120,
      },
      {
        headerName: 'Alternate Text',
        colId: 'ExternalAssetMeta',
        field: 'ExternalAssetMeta',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
      },
    ],
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.getExternalAssets();
    },
    getRowId: (params) => {
      return params.data.ExternalAssetID;
    },
  };

  public workItemsGridOptions: GridOptions = {
    animateRows: true,
    debug: false,
    stopEditingWhenCellsLoseFocus: true,
    rowSelection: 'single',
    rowHeight: 30,
    headerHeight: 20,
    groupDefaultExpanded: 0,
    defaultColDef: {
      resizable: true,
      floatingFilter: true,
      filter: 'agTextColumnFilter',
      sortable: true,
      enableRowGroup: false,
      enablePivot: false,
      enableValue: false,
      editable: false,
      width: 360,
    },
    columnDefs: [
      {
        headerName: 'Location',
        colId: 'Location',
        field: 'Location',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 160,
      },
      {
        headerName: 'Work Type',
        colId: 'WorkType',
        field: 'WorkType',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 120,
      },
      {
        headerName: 'Short Descripiton',
        colId: 'ShortDescription',
        field: 'ShortDescription',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
      },
      {
        headerName: 'Last Changed',
        colId: 'LastChangedDate',
        field: 'LastChangedDate',
        filter: 'agTextColumnFilter',
        floatingFilter: true,
        width: 120,
      },
      {
        headerName: 'Other Information',
        colId: 'Other',
        field: 'Other',
        filter: 'filter',
        floatingFilter: true,
      },
    ],
    icons: {
      column:
        '<span class="ag-icon ag-icon-column" style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
            suppressRowGroups: true,
            suppressValues: true,
          },
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    onGridReady: (event: GridReadyEvent) => {
      this.workItemsGridApi = event.api;
    },
  };

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateWorkOrderDialogComponent>,
    private issueSnapshotFacade: IssueSnapshotFacade,
    @Inject(MAT_DIALOG_DATA) public data: { title: string }
  ) {
    this.workOrderInfo$ = issueSnapshotFacade.selectWorkRequestInfo$;
    this.summaryState$ = issueSnapshotFacade.selectIssueSummaryState$;
    this.selectIssueSnapshotState$ =
      issueSnapshotFacade.selectIssueSnapshotState$;
    this.assetName$ = issueSnapshotFacade.selectAssetName$;
    this.theme$ = issueSnapshotFacade.theme$;
    this.workRequestLoading$ = issueSnapshotFacade.workRequestLoading$;
    this.externalAssets$ = issueSnapshotFacade.externalAssets$;
    this.selectedExternalAsset$ = issueSnapshotFacade.selectedExternalAsset$;

    this.externalAssets$
      .pipe(
        withLatestFrom(this.selectedExternalAsset$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([externalAssets, selectedExternalAsset]) => {
        if (this.gridApi && externalAssets) {
          this.gridApi.setRowData(externalAssets);
          this.gridApi.refreshCells({ force: true });

          // if we have a selected asset, expand all its parents
          if (selectedExternalAsset?.ExternalAssetID) {
            const node: RowNode = this.gridApi.getRowNode(
              selectedExternalAsset.ExternalAssetID
            );
            if (node) {
              node.setSelected(true);
              this.expandParentNodes(node);
            }
          }
        }
      });

    this.selectedExternalAsset$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((selectedExternalAsset) => {
        this.getWorkItems(selectedExternalAsset);
      });
  }

  ngOnInit(): void {
    this.populateFormDefaults();
  }

  onTabChange(tabChangeEvent: MatTabChangeEvent) {
    if (tabChangeEvent.tab.textLabel === 'Location') {
      this.selectedExternalAsset$
        .pipe(take(1))
        .subscribe((selectedExternalAsset) => {
          const node: RowNode = this.gridApi.getRowNode(
            selectedExternalAsset.ExternalAssetID
          );
          if (node) {
            this.gridApi.ensureNodeVisible(node, 'middle');
          }
        });
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSubmit(): void {
    let issueGuid = '';
    this.summaryState$.pipe(take(1)).subscribe((state) => {
      issueGuid = state.IssueGuid;
    });
    let externalAsset: ExternalAsset;
    this.selectedExternalAsset$
      .pipe(take(1))
      .subscribe((selectedExternalAsset) => {
        externalAsset = selectedExternalAsset;
      });
    this.issueSnapshotFacade.saveWorkRequest(
      issueGuid,
      this.workOrderForm.get('title').value,
      this.workOrderForm.get('description').value,
      externalAsset.ExternalSystemAssetID,
      this.workOrderForm.get('priority').value
    );
    this.workRequestLoading$.subscribe((wrLoading) => {
      if (wrLoading.swrLoading === false && !wrLoading.swrError) {
        this.dialogRef.close();
      }
    });
  }

  private getWorkItems(selectedExternalAsset: ExternalAsset) {
    // getExternalAssetWorkItems(selectedExternalAsset.ExternalAssetID)
    this.workItemsGridApi.showLoadingOverlay();
    setTimeout(() => {
      this.workItemsGridApi.setRowData([
        {
          Location: 'Floc:XYZ-123',
          WorkType: 'Notif',
          ShortDescription: 'Investigate this issue',
          LastChangedDate: '11/1/2023',
          Other: 'other stuff',
        },
        {
          Location: 'Equip:ABC-1',
          WorkType: 'WO',
          ShortDescription: 'Fix this equipment again',
          LastChangedDate: '10/28/2023',
          Other: 'other stuff',
        },
        {
          Location: 'Floc:XYZ-124',
          WorkType: 'Notif',
          ShortDescription: 'Investigate this issue',
          LastChangedDate: '11/1/2023',
          Other: 'other stuff',
        },
        {
          Location: 'Equip:ABC-1',
          WorkType: 'WO',
          ShortDescription: 'Fix this equipment',
          LastChangedDate: '10/25/2023',
          Other: 'other stuff',
        },
      ]);
    }, 2000);
  }

  //gets and formats the summary and discussion text for the issue and sets it as
  //the default value for the form input
  private populateFormDefaults() {
    let summary = '';
    this.selectIssueSnapshotState$
      .pipe(take(1))
      .subscribe(({ SummaryState, DiscussionState }) => {
        summary = '==========Summary==========\n';
        summary += SummaryState.IssueSummary.replace(/<(?:.|\n)*?>/gm, '')
          .replace(/&nbsp;/gm, '')
          .replace(/&ndash;/gm, '-')
          .replace(/&lt;/gm, '<')
          .replace(/&gt;/gm, '>');

        const discussions = DiscussionState.discussion?.Entries;
        summary += '\n\n==========Discussions==========';
        discussions.forEach((discussion) => {
          const hours =
            discussion.CreateDateZ.getHours().toString().length == 1
              ? '0' + discussion.CreateDateZ.getHours().toString()
              : discussion.CreateDateZ.getHours().toString();
          const minutes =
            discussion.CreateDateZ.getMinutes().toString().length == 1
              ? '0' + discussion.CreateDateZ.getMinutes().toString()
              : discussion.CreateDateZ.getMinutes().toString();
          const seconds =
            discussion.CreateDateZ.getSeconds().toString().length == 1
              ? '0' + discussion.CreateDateZ.getSeconds().toString()
              : discussion.CreateDateZ.getSeconds().toString();

          summary += `\n\n=====${discussion.CreateDateZ.toDateString()} ${hours}:${minutes}:${seconds} ${
            discussion.Title
          }=====\n`;
          summary += discussion.Content.replace(/<!--(?:.|\n)*?-->/gm, '')
            .replace(/<(?:.|\n)*?>/gm, '')
            .replace(/&nbsp;/gm, '')
            .replace(/&ndash;/gm, '-')
            .replace(/&lt;/gm, '<')
            .replace(/&gt;/gm, '>');
        });
        this.workOrderForm
          .get('title')
          .setValue(this.data.title || DiscussionState.discussion.Title || '');
        this.workOrderForm.get('description').setValue(summary);
      });
  }

  //Triggers the GET Request to get the External Asset.
  //Called within the onGridReady() function
  private getExternalAssets() {
    this.issueSnapshotFacade.selectAssetGuid$
      .pipe(take(1))
      .subscribe((assetGuid) => {
        this.issueSnapshotFacade.getExternalAssets(assetGuid);
      });
  }

  //expands the parent nodes of the given node
  private expandParentNodes(givenNode: RowNode) {
    let node = givenNode;
    while (node.parent) {
      node.parent.setExpanded(true);
      node = node.parent;
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.issueSnapshotFacade.clearExternalAssets();
  }
}
