import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, Subject } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { ImpactCalculatorComponent } from './impact-calculator.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('ImpactCalculatorComponent', () => {
  let mockNavFacade: NavFacade;
  let component: ImpactCalculatorComponent;
  let fixture: ComponentFixture<ImpactCalculatorComponent>;
  let mockToastService: ToastService;
  const mockDialogRef = {
    close: jest.fn(),
  };

  beforeEach(() => {
    mockNavFacade = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      configureLayoutButton: jest.fn(),
      toggleNavigation: jest.fn(),
      breadcrumb$: new BehaviorSubject<IButtonData>(null),
    });
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: {},
          },
        },
        { provide: ToastService, useValue: mockToastService },
        {
          provide: NavFacade,
          useValue: mockNavFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      declarations: [ImpactCalculatorComponent],
      imports: [
        FormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpactCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
