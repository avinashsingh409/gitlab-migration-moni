/* eslint-disable rxjs/no-nested-subscribe */
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  FormControl,
  UntypedFormGroup,
} from '@angular/forms';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import {
  ImpactCalculatorFacade,
  ImpactCalculatorState,
} from '../../service/impact-calculator/impact-calculator.facade';
import { produce } from 'immer';
import { take, takeUntil } from 'rxjs/operators';
import { ImpactScenario } from '../../model/impact-calculator/scenario';
import { ImpactCalculatorFormService } from '../../service/impact-calculator/impact-calculator-form.service';
import moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SaveChangesModalComponent } from '../save-changes-modal/save-changes-modal.component';
import { ImpactCalculatorInvalidModalComponent } from '../impact-calculator-invalid-modal/impact-calculator-invalid-modal.component';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { LoggerService } from '@atonix/shared/utils';
import { CalculationType } from '@atonix/atx-core';
import { CalculationTypeMapping } from '../../model/impact-calculator/calculation-parameters';
import { getCurrencySymbol } from '@angular/common';

@Component({
  selector: 'atx-impact-calculator',
  templateUrl: './impact-calculator.component.html',
  styleUrls: ['./impact-calculator.component.scss'],
})
export class ImpactCalculatorComponent implements OnInit, OnDestroy {
  public breadcrumb$: Observable<IButtonData>;
  public vm$: Observable<ImpactCalculatorState>;
  public calculationTypeMapping: any = CalculationTypeMapping;
  public calculationType = CalculationType;
  onDestroy = new Subject<void>();
  productionPanelExpanded = false;
  showDates = false;

  efficiencyPanelExpanded = false;
  maintenancePanelExpanded = false;
  otherPanelExpanded = false;
  scenariosForm: UntypedFormGroup;
  currentDate = moment().toDate();
  currencyCode: string;
  currencySymbol: string;

  constructor(
    private fb: UntypedFormBuilder,
    private navFacade: NavFacade,
    private router: Router,
    private dialog: MatDialog,
    private impactCalculatorFacade: ImpactCalculatorFacade,
    private impactCalculatorFormService: ImpactCalculatorFormService,
    private loggerService: LoggerService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.scenariosForm = this.fb.group({
      name: '',
      scenarios: this.fb.array([]),
    });
    this.currencyCode = appConfig.currency;
    this.currencySymbol = getCurrencySymbol(appConfig.currency, 'narrow', 'en');
  }

  changeDaysToDates() {
    this.showDates = !this.showDates;
  }

  ngOnInit(): void {
    this.vm$ = this.impactCalculatorFacade.vm$;
    this.impactCalculatorFacade.isValid$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((isValid) => {
        if (!isValid) {
          this.impactCalculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
            this.loggerService.trackAppInsightsEvent(
              'Issues:Invalid_Calculator',
              {
                assetID: vm.issueID,
                legacyIssue: `${this.appConfig.baseSiteURL}/IssuesManagement/Issue.html#!/snapshot?ac=Issues Management&iid=${vm.issueID}`,
              }
            );
          });
          const dialogConfig = new MatDialogConfig();
          const dialogRef = this.dialog.open(
            ImpactCalculatorInvalidModalComponent,
            dialogConfig
          );
          this.scenariosForm.disable();
          dialogRef.afterClosed().subscribe((result) => {
            if (result) {
              this.impactCalculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
                window
                  .open(
                    `${this.appConfig.baseSiteURL}/IssuesManagement/Issue.html#!/snapshot?ac=Issues Management&iid=${vm.issueID}`,
                    '_blank'
                  )
                  .focus();
              });
            }
          });
        }
      });
    this.impactCalculatorFacade.scenarios$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((impactScenarios: ImpactScenario[]) => {
        if (impactScenarios) {
          if (impactScenarios.length === this.scenarios.length) {
            return;
          }
          const removeList = [];
          this.scenarios.controls.forEach((element, scenarioIndex) => {
            const scenarioFound = impactScenarios.find(
              (i) => i.scenarioGuidID === element.get('scenarioGuidID').value
            );
            if (!scenarioFound) {
              removeList.push(scenarioIndex);
            }
          });
          removeList.forEach((val) => {
            this.scenarios.removeAt(val);
          });

          impactScenarios.forEach((impactScenario) => {
            let isCurrentlyShown = false;
            this.scenarios.controls.forEach((element) => {
              if (
                element.get('scenarioGuidID').value ===
                impactScenario.scenarioGuidID
              ) {
                isCurrentlyShown = true;
              }
            });
            if (!isCurrentlyShown) {
              const scenario = this.fb.group({
                scenarioGuidID: impactScenario.scenarioGuidID,
                description: this.impactCalculatorFormService.scenarioName(
                  impactScenario.scenarioGuidID,
                  impactScenario.scenarioName
                ),
                probability: this.impactCalculatorFormService.probability(
                  impactScenario.scenarioGuidID,
                  impactScenario.percentLikelihood
                ),
                note: this.impactCalculatorFormService.scenarioNotes(
                  impactScenario.scenarioGuidID,
                  impactScenario.scenarioNotes
                ),
                // Gen
                impactQuantity: this.impactCalculatorFormService.impactQuantity(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.impactQuantity
                ),
                daysElapsed: this.impactCalculatorFormService.daysElapsed(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.impactRange
                    ?.DaysElapsed > 0
                    ? impactScenario.generationParameters?.impactRange
                        ?.DaysElapsed
                    : 0
                ),

                daysCurrentDate:
                  impactScenario.generationParameters?.impactRange?.CurrentDate,
                daysPending: this.impactCalculatorFormService.daysPending(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.impactRange
                    ?.DaysPending > 0
                    ? impactScenario.generationParameters?.impactRange
                        ?.DaysPending
                    : 0
                ),

                derateElapsed: this.impactCalculatorFormService.derateElapsed(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.percentTimeDerateInEffect
                    .toDate
                ),
                deratePending: this.impactCalculatorFormService.deratePending(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.percentTimeDerateInEffect
                    .potentialFuture
                ),
                derateOverride: this.impactCalculatorFormService.derateOverride(
                  impactScenario.scenarioGuidID,
                  impactScenario.generationParameters?.derateOverrideValue
                ),
                lostMarginOpportunity:
                  this.impactCalculatorFormService.lostMarginOpportunity(
                    impactScenario.scenarioGuidID,
                    impactScenario.generationParameters?.lostMarginOpportunity
                  ),
                relevantProductionCost:
                  this.impactCalculatorFormService.relevantProductionCost(
                    impactScenario.scenarioGuidID,
                    impactScenario.generationParameters?.relevantProductionCost
                  ),
                // override values
                genTotalToDate:
                  this.impactCalculatorFormService.productionToDate(
                    impactScenario.scenarioGuidID,
                    impactScenario.generationParameters?.totalCost?.toDate
                  ),
                genTotalFuture:
                  this.impactCalculatorFormService.productionFuture(
                    impactScenario.scenarioGuidID,
                    impactScenario.generationParameters?.totalCost
                      ?.potentialFuture
                  ),

                // eff
                effImpactQuantity:
                  this.impactCalculatorFormService.effImpactQuantity(
                    impactScenario.scenarioGuidID,
                    impactScenario.efficiencyParameters?.impactQuantity
                  ),
                effDaysElapsed: this.impactCalculatorFormService.effDaysElapsed(
                  impactScenario.scenarioGuidID,
                  impactScenario.efficiencyParameters?.impactRange
                    ?.DaysElapsed > 0
                    ? impactScenario.efficiencyParameters.impactRange
                        .DaysElapsed
                    : 0
                ),
                effDaysPending: this.impactCalculatorFormService.effDaysPending(
                  impactScenario.scenarioGuidID,
                  impactScenario.efficiencyParameters?.impactRange
                    ?.DaysPending > 0
                    ? impactScenario.efficiencyParameters.impactRange
                        .DaysPending
                    : 0
                ),
                // override values
                effTotalToDate: this.impactCalculatorFormService.effToDate(
                  impactScenario.scenarioGuidID,
                  impactScenario.efficiencyParameters?.totalCost?.toDate
                ),
                effTotalFuture: this.impactCalculatorFormService.effFuture(
                  impactScenario.scenarioGuidID,
                  impactScenario.efficiencyParameters?.totalCost
                    ?.potentialFuture
                ),

                // maintenance
                repairManHoursToDate:
                  this.impactCalculatorFormService.maintenanceLaborHoursElapsed(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.repairManHours?.toDate
                  ),
                repairManHoursFuture:
                  this.impactCalculatorFormService.maintenanceLaborHoursPending(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.repairManHours
                      ?.potentialFuture
                  ),
                laborCostToDate:
                  this.impactCalculatorFormService.maintenanceLaborCostElapsed(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.laborHourlyRate
                      ?.toDate
                  ),
                laborCostFuture:
                  this.impactCalculatorFormService.maintenanceLaborCostPending(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.laborHourlyRate
                      ?.potentialFuture
                  ),
                partsCostToDate:
                  this.impactCalculatorFormService.maintenancePartsCostElapsed(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.repairCost?.toDate
                  ),
                partsCostFuture:
                  this.impactCalculatorFormService.maintenancePartsCostPending(
                    impactScenario.scenarioGuidID,
                    impactScenario.maintenanceParameters?.repairCost
                      ?.potentialFuture
                  ),
                // override values
                mainTotalToDate: this.impactCalculatorFormService.mainToDate(
                  impactScenario.scenarioGuidID,
                  impactScenario.maintenanceParameters?.totalCost?.toDate
                ),
                mainTotalFuture: this.impactCalculatorFormService.mainFuture(
                  impactScenario.scenarioGuidID,
                  impactScenario.maintenanceParameters?.totalCost
                    ?.potentialFuture
                ),

                // other
                otherImpactQuantityToDate:
                  this.impactCalculatorFormService.otherRateElapsed(
                    impactScenario.scenarioGuidID,
                    impactScenario.otherParameters?.impactQuantity?.toDate
                  ),
                otherImpactQuantityFuture:
                  this.impactCalculatorFormService.otherRatePending(
                    impactScenario.scenarioGuidID,
                    impactScenario.otherParameters?.impactQuantity
                      ?.potentialFuture
                  ),
                otherDaysElapsed:
                  this.impactCalculatorFormService.otherDaysElapsed(
                    impactScenario.scenarioGuidID,
                    impactScenario.otherParameters?.impactRange?.DaysElapsed
                  ),
                otherDaysPending:
                  this.impactCalculatorFormService.otherDaysPending(
                    impactScenario.scenarioGuidID,
                    impactScenario.otherParameters?.impactRange?.DaysPending
                  ),
                // override values
                otherTotalToDate: this.impactCalculatorFormService.otherToDate(
                  impactScenario.scenarioGuidID,
                  impactScenario.otherParameters?.totalCost?.toDate
                ),
                otherTotalFuture: this.impactCalculatorFormService.otherFuture(
                  impactScenario.scenarioGuidID,
                  impactScenario.otherParameters?.totalCost?.potentialFuture
                ),
              });
              this.scenarios.push(scenario);
            }
          });
          this.scenarios.markAllAsTouched();
        }
      });
    const breadcrumbMenu: IButtonData = {
      id: 'impact-calculator',
      url: '',
      text: 'Back',
      hover: 'back',
      visible: true,
      type: 'back',
      matIcon: 'keyboard_backspace',
      enabled: true,
    };

    this.navFacade.configure({ breadcrumbMenu });
    this.breadcrumb$ = this.navFacade.breadcrumb$;
    this.navFacade.configureLayoutButton('nNavbar', { visible: false });
    this.navFacade.toggleNavigation('hidden');
  }

  calculationTypeChange(event: CalculationType, scenario: number) {
    event === CalculationType.CalculatedImpact
      ? (this.productionPanelExpanded = true)
      : (this.productionPanelExpanded = false);
    if (!this.scenarios.valid) {
      return;
    }
    this.impactCalculatorFormService.productionImpactCalculationTypeChange(
      event,
      scenario
    );
  }
  expansionPanelsDisabled() {
    if (!this.scenarios.valid) {
      return 'disabled-expansion-panel';
    }
    return '';
  }

  effCalculationTypeChange(event: CalculationType, scenario: number) {
    event === CalculationType.CalculatedImpact
      ? (this.efficiencyPanelExpanded = true)
      : (this.efficiencyPanelExpanded = false);
    if (!this.scenarios.valid) {
      return;
    }
    this.impactCalculatorFormService.efficiencyImpactCalculationTypeChange(
      event,
      scenario
    );
  }
  mainCalculationTypeChange(event: CalculationType, scenario: number) {
    event === CalculationType.CalculatedImpact
      ? (this.maintenancePanelExpanded = true)
      : (this.maintenancePanelExpanded = false);
    if (!this.scenarios.valid) {
      return;
    }
    this.impactCalculatorFormService.maintenanceImpactCalculationTypeChange(
      event,
      scenario
    );
  }

  otherCalculationTypeChange(event: CalculationType, scenario: number) {
    event === CalculationType.CalculatedImpact
      ? (this.otherPanelExpanded = true)
      : (this.otherPanelExpanded = false);
    if (!this.scenarios.valid) {
      return;
    }
    this.impactCalculatorFormService.otherImpactCalculationTypeChange(
      event,
      scenario
    );
  }

  changeElapsedEfficiencyDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.efficiencyCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.dateElapsed = event.value;
          const elapsedDate = moment(event.value);
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newElapsedDays = currentDate.diff(elapsedDate, 'days');
          state[scenarioIndex].calculation.daysElapsed = newElapsedDays;
          this.scenarios
            .at(scenario)
            .get('effDaysElapsed')
            .patchValue(newElapsedDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateEfficiencyFormValue(newState);
      });
    }
  }

  changePendingEfficiencyDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.efficiencyCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.datePending = event.value;
          const pendingDate = moment(
            moment(event.value.toLocaleDateString(), 'MM-DD-YYYY')
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newPendingDays = pendingDate.diff(currentDate, 'days');
          state[scenarioIndex].calculation.daysPending = newPendingDays;
          this.scenarios
            .at(scenario)
            .get('effDaysPending')
            .patchValue(newPendingDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateEfficiencyFormValue(newState);
      });
    }
  }

  changeElapsedProductionDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.productionCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.dateElapsed = event.value;
          const elapsedDate = moment(event.value);
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newElapsedDays = currentDate.diff(elapsedDate, 'days');
          state[scenarioIndex].calculation.daysElapsed = newElapsedDays;
          this.scenarios
            .at(scenario)
            .get('daysElapsed')
            .patchValue(newElapsedDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateProductionFormValue(newState);
      });
    }
  }

  changePendingOtherDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.otherCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const futureDate = moment(currentDate, 'DD-MM-YYYY')
            .add(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.datePending = futureDate;
        });
        this.impactCalculatorFacade.updateOtherCalcValues(newState);
      });
    }
  }

  changeElapsedOtherDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.otherCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const startDate = moment(currentDate, 'DD-MM-YYYY')
            .subtract(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.dateElapsed = startDate;
        });
        this.impactCalculatorFacade.updateOtherCalcValues(newState);
      });
    }
  }

  changePendingOtherDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.otherCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.datePending = event.value;
          const pendingDate = moment(
            moment(event.value.toLocaleDateString(), 'MM-DD-YYYY')
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newPendingDays = pendingDate.diff(currentDate, 'days');
          state[scenarioIndex].calculation.daysPending = newPendingDays;
          this.scenarios
            .at(scenario)
            .get('otherDaysPending')
            .patchValue(newPendingDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateOtherCalcValues(newState);
      });
    }
  }

  changeElapsedOtherDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.otherCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.dateElapsed = event.value;
          const elapsedDate = moment(event.value);
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newElapsedDays = currentDate.diff(elapsedDate, 'days');
          state[scenarioIndex].calculation.daysElapsed = newElapsedDays;
          this.scenarios
            .at(scenario)
            .get('otherDaysElapsed')
            .patchValue(newElapsedDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateOtherCalcValues(newState);
      });
    }
  }

  changePendingProductionDate(
    event: MatDatepickerInputEvent<Date>,
    scenarioGuidID: number,
    scenario: number
  ) {
    if (event.value) {
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.productionCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          state[scenarioIndex].calculation.datePending = event.value;
          const pendingDate = moment(
            moment(event.value.toLocaleDateString(), 'MM-DD-YYYY')
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const newPendingDays = pendingDate.diff(currentDate, 'days');
          state[scenarioIndex].calculation.daysPending = newPendingDays;
          this.scenarios
            .at(scenario)
            .get('daysPending')
            .patchValue(newPendingDays, { emitEvent: false });
        });
        this.impactCalculatorFacade.updateProductionFormValue(newState);
      });
    }
  }

  changePendingEfficiencyDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.efficiencyCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const futureDate = moment(currentDate, 'DD-MM-YYYY')
            .add(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.datePending = futureDate;
        });
        this.impactCalculatorFacade.updateEfficiencyFormValue(newState);
      });
    }
  }

  changeElapsedEfficiencyDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.efficiencyCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const startDate = moment(currentDate, 'DD-MM-YYYY')
            .subtract(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.dateElapsed = startDate;
        });
        this.impactCalculatorFacade.updateEfficiencyFormValue(newState);
      });
    }
  }

  private isNumber = (n: string | number): boolean =>
    !isNaN(parseFloat(String(n))) && isFinite(Number(n));

  changePendingProductionDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.productionCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const futureDate = moment(currentDate, 'DD-MM-YYYY')
            .add(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.datePending = futureDate;
        });
        this.impactCalculatorFacade.updateProductionFormValue(newState);
      });
    }
  }

  toggleDerate(derateOverride: boolean, scenario: number) {
    this.impactCalculatorFormService.toggleProductionDerateOverride(
      derateOverride,
      scenario
    );
  }

  changeElapsedProductionDays(event, scenarioGuidID: number) {
    if (event) {
      if (!this.isNumber(event)) {
        return;
      }
      this.vm$.pipe(take(1)).subscribe((vm) => {
        const newState = produce(vm.productionCalcs, (state) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenarioGuidID
          );
          const currentDate = moment(
            state[scenarioIndex].calculation.currentDate
          );
          const startDate = moment(currentDate, 'DD-MM-YYYY')
            .subtract(+event, 'days')
            .toDate();
          state[scenarioIndex].calculation.dateElapsed = startDate;
        });
        this.impactCalculatorFacade.updateProductionFormValue(newState);
      });
    }
  }

  changeGenCategory(event, scenario) {
    this.impactCalculatorFacade.updateGenerationCategory(event, scenario);
  }
  changeCalculationFactor(event, scenario) {
    this.impactCalculatorFacade.changeCalculationFactor(event, scenario);
  }
  changeEfficiencyCalculationFactor(event, scenario) {
    this.impactCalculatorFacade.changeEfficiencyCalculationFactor(
      event,
      scenario
    );
  }
  calculateRelevantProductionCost(scenario) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const productionCost =
        vm.scenarios[scenario].calculateRelevantProductionCost();
      this.scenarios
        .at(scenario)
        .get('relevantProductionCost')
        .setValue(productionCost);
    });
  }

  get scenarios(): UntypedFormArray {
    return this.scenariosForm.get('scenarios') as UntypedFormArray;
  }

  addscenarios() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.impactCalculatorFacade.addBlankScenario(vm);
    });
  }

  removeScenario(i: number) {
    this.impactCalculatorFacade.removeScenario(i);
  }

  onSubmit() {
    this.impactCalculatorFacade.saveImpacts();
  }

  cancel() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (!vm.pristine && vm.isValid) {
        const dialogConfig = new MatDialogConfig();
        const dialogRef = this.dialog.open(
          SaveChangesModalComponent,
          dialogConfig
        );
        dialogRef.afterClosed().subscribe((result) => {
          if (result) {
            this.impactCalculatorFacade.cancelCalculator();
            this.exitCalculator();
          }
        });
      } else {
        this.exitCalculator();
      }
    });
  }

  exitCalculator() {
    this.impactCalculatorFacade.reset();
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.navigate([], {
      queryParams: {
        aiid: null,
        aid: null,
      },
      queryParamsHandling: 'merge',
      replaceUrl: true,
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
  }
}
