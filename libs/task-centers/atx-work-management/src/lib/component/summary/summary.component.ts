import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { IComponentDonutState, makeId } from '@atonix/atx-chart';
import {
  NavFacade,
  IButtonData,
  getUniqueIdFromRoute,
} from '@atonix/atx-navigation';
import { ITreeStateChange, ITreeConfiguration } from '@atonix/atx-asset-tree';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { IIssueSummary, IGridParameters, IListState } from '@atonix/shared/api';
import { take, takeUntil } from 'rxjs/operators';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import { ActivatedRoute } from '@angular/router';
import { AuthFacade } from '@atonix/shared/state/auth';

@Component({
  selector: 'atx-work-management-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryComponent implements OnDestroy {
  public leftTrayMode$: Observable<'over' | 'push' | 'side'>;
  public leftTraySize$: Observable<number>;
  public leftTrayOpen$: Observable<boolean>;
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public leftDonutState$: Observable<IComponentDonutState>;
  public middleDonutState$: Observable<IComponentDonutState>;
  public rightDonutState$: Observable<IComponentDonutState>;
  public unsubscribe$ = new Subject<void>();
  public asset$: Observable<string>;
  public listState$: Observable<IListState>;
  public theme$: Observable<string>;
  public floatingFilter$: Observable<boolean>;
  public issuesManagementTaskCenterAccess$: Observable<any>;

  constructor(
    private navFacade: NavFacade,
    private summaryFacade: SummaryFacade,
    private authFacade: AuthFacade,
    private activatedRoute: ActivatedRoute
  ) {
    this.summaryFacade.initializeSummary(
      getUniqueIdFromRoute(this.activatedRoute.snapshot.queryParamMap)
    );

    this.assetTreeConfiguration$ = summaryFacade.assetTreeConfiguration$;

    this.leftDonutState$ = summaryFacade.leftDonutState$;
    this.middleDonutState$ = summaryFacade.middleDonutState$;
    this.rightDonutState$ = summaryFacade.rightDonutState$;

    this.asset$ = summaryFacade.selectedAsset$;

    this.leftTrayMode$ = summaryFacade.leftTrayMode$;
    this.leftTraySize$ = summaryFacade.leftTraySize$;
    this.leftTrayOpen$ = navFacade.assetTreeSelected$;

    this.listState$ = summaryFacade.savedListState$;
    this.theme$ = navFacade.theme$;
    this.floatingFilter$ = summaryFacade.floatingFilter$;

    this.issuesManagementTaskCenterAccess$ =
      this.authFacade.issuesManagementTaskCenterAccess$;

    this.asset$.pipe(takeUntil(this.unsubscribe$)).subscribe((asset) => {
      if (asset) {
        this.summaryFacade.selectAsset(asset);
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  // This is the thing that actually makes changes to the asset tree state.  We handle what we
  // need to handle and then call the default behavior.
  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.summaryFacade.treeStateChange(change);
  }

  public onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.summaryFacade.treeSizeChange(newSize);
    }
  }

  public itemSelected(itemID: string) {
    this.summaryFacade.selectWorkItem(itemID);
  }

  public addItem() {
    this.summaryFacade.addWorkItem();
  }

  public listStateChanged(listState: IListState) {
    this.summaryFacade.listStateChanged(listState);
  }

  public filterStateChanged(state: IServerSideGetRowsRequest) {
    this.summaryFacade.updateDonutChart(state);
  }

  public listViewDownload(gridParams: IGridParameters) {
    this.summaryFacade.listViewDownload(gridParams);
  }

  public onLeftDonutOthersSelected() {
    this.summaryFacade.leftDonutOthersSelected(makeId(5));
  }

  public onMiddleDonutOthersSelected() {
    this.summaryFacade.middleDonutOthersSelected(makeId(5));
  }

  public onRightDonutOthersSelected() {
    this.summaryFacade.rightDonutOthersSelected(makeId(5));
  }

  public onLeftDonutRefresh(): void {
    this.summaryFacade.leftDonutRefreshed(makeId(5));
  }

  public onMiddleDonutRefresh(): void {
    this.summaryFacade.middleDonutRefreshed(makeId(5));
  }

  public onRightDonutRefresh(): void {
    this.summaryFacade.rightDonutRefreshed(makeId(5));
  }

  floatingFilterToggle() {
    this.summaryFacade.toggleFloatingFilter();
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }

  public setScorecard(values: { issue: IIssueSummary; scorecard: boolean }) {
    this.summaryFacade.setScorecard(
      values.issue.AssetIssueID,
      values.scorecard
    );
  }

  hasAccessRestriction(): boolean {
    let hasAccessRestriction = true;
    this.authFacade.issuesManagementTaskCenterAccess$
      .pipe(take(1))
      .subscribe((rights) => {
        if (rights?.CanAdmin || rights?.CanEdit) {
          hasAccessRestriction = false;
        }
      });
    return hasAccessRestriction;
  }
}
