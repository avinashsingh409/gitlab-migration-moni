import {
  ComponentFixture,
  TestBed,
  inject,
  waitForAsync,
} from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { AgGridModule } from '@ag-grid-community/angular';
import { of, Subject, BehaviorSubject } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

import { NavFacade } from '@atonix/atx-navigation';
import {
  AssetTreeModule,
  ITreeConfiguration,
  ITreeStateChange,
} from '@atonix/atx-asset-tree';
import { IComponentDonutState } from '@atonix/atx-chart';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { SummaryComponent } from './summary.component';
import { IListState } from '@atonix/shared/api';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AuthFacade } from '@atonix/shared/state/auth';
import { ISecurityRights } from '@atonix/atx-core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;
  let mockSummaryFacade: Partial<SummaryFacade>;
  let mockNavFacade: NavFacade;
  let mockAuthFacade: AuthFacade;
  const mockListState: IListState = {
    columns: null,
    sort: null,
    groups: null,
    filter: null,
  };

  beforeEach(() => {
    mockAuthFacade = createMockWithValues(AuthFacade, {
      issuesManagementTaskCenterAccess$: new BehaviorSubject<ISecurityRights>({
        CanAdd: true,
        CanAdmin: true,
        CanDelete: true,
        CanEdit: true,
        CanView: true,
      }),
    });
    mockSummaryFacade = createMockWithValues(SummaryFacade, {
      initializeSummary: jest.fn(),
      leftTrayMode$: new BehaviorSubject<any>('over'),
      leftTraySize$: new BehaviorSubject<number>(23),

      leftDonutState$: new BehaviorSubject<IComponentDonutState>(null),
      middleDonutState$: new BehaviorSubject<IComponentDonutState>(null),
      rightDonutState$: new BehaviorSubject<IComponentDonutState>(null),
      assetTreeConfiguration$: new BehaviorSubject<ITreeConfiguration>({
        showTreeSelector: true,
        trees: [],
        selectedTree: null,
        autoCompleteValue: null,
        autoCompletePending: false,
        autoCompleteAssets: null,
        nodes: [],
        pin: true,
        loadingAssets: false,
        loadingTree: false,
        canDrag: false,
        dropTargets: null,
        canView: true,
        canAdd: false,
        canEdit: false,
        canDelete: false,
        collapseOthers: true,
        hideConfigureButton: false,
      }),
      floatingFilter$: new BehaviorSubject<boolean>(false),
      savedListState$: new BehaviorSubject<IListState>(null),
      selectedAsset$: new BehaviorSubject<string>('123'),
    });
    mockNavFacade = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      assetTreeSelected$: new BehaviorSubject<boolean>(true),
      theme$: new BehaviorSubject<string>('light'),
    });

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        { provide: SummaryFacade, useValue: mockSummaryFacade },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: NavFacade,
          useValue: mockNavFacade,
        },
        { provide: AuthFacade, useValue: mockAuthFacade },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
      declarations: [SummaryComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change tree state', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      const mockChange: ITreeStateChange = {
        event: 'GetPermissions',
        newValue: null,
      };

      component.onAssetTreeStateChange(mockChange);
      expect(summaryFacade.treeStateChange).toHaveBeenCalledWith(mockChange);
    }
  ));

  it('should change tree size', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.onAssetTreeSizeChange(123);
      expect(summaryFacade.treeSizeChange).toHaveBeenCalledWith(123);
    }
  ));

  it('should select work item', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.itemSelected('4567');
      expect(summaryFacade.selectWorkItem).toHaveBeenCalledWith('4567');
    }
  ));

  it('should add work item', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.addItem();
      expect(summaryFacade.addWorkItem).toHaveBeenCalled();
    }
  ));

  it('should change list state', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.listStateChanged(mockListState);
      expect(summaryFacade.listStateChanged).toHaveBeenCalledWith(
        mockListState
      );
    }
  ));

  it('should change left donut state', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.onLeftDonutRefresh();
      expect(summaryFacade.leftDonutRefreshed).toHaveBeenCalled();

      component.onLeftDonutOthersSelected();
      expect(summaryFacade.leftDonutOthersSelected).toHaveBeenCalled();
    }
  ));

  it('should change middle donut state', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.onMiddleDonutRefresh();
      expect(summaryFacade.middleDonutRefreshed).toHaveBeenCalled();

      component.onMiddleDonutOthersSelected();
      expect(summaryFacade.middleDonutOthersSelected).toHaveBeenCalled();
    }
  ));

  it('should change right donut state', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.onRightDonutRefresh();
      expect(summaryFacade.rightDonutRefreshed).toHaveBeenCalled();

      component.onRightDonutOthersSelected();
      expect(summaryFacade.rightDonutOthersSelected).toHaveBeenCalled();
    }
  ));
});
