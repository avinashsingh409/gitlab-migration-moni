import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { IIssueSummaryState } from '../../model/issue-summary-state';
import { EventBusService, Events } from '../../service/event-bus.service';

@Component({
  selector: 'atx-issue-summary-editor',
  templateUrl: './issue-summary-editor.component.html',
  styleUrls: ['./issue-summary-editor.component.scss'],
})
export class IssueSummaryEditorComponent implements OnChanges {
  @Input() summaryState: IIssueSummaryState;
  @Output() uploadSummaryFile = new EventEmitter<any>();
  @Output() saveSummary = new EventEmitter<string>();
  @Output() cancelEditSummary = new EventEmitter();

  summaryEditorForm = new UntypedFormGroup({
    content: new UntypedFormControl(null),
  });

  initSummaryEditor = {
    promotion: false,
    height: '350',
    plugins: 'image autolink link',
    contextmenu: false,
    text_patterns: false,
    paste_data_images: true,
    convert_urls: false,
    relative_urls: false,
    remove_script_host: false,
    toolbar:
      'undo redo | bold italic underline | link | forecolor | fontselect emoticons',
    menubar: 'edit view format',
    menu: {
      edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
      view: { title: 'View', items: 'visualaid' },
      format: {
        title: 'Format',
        items: 'bold italic underline | removeformat',
      },
    },
    statusbar: false,
    browser_spellcheck: true,
    base_url: this.locationStrat.getBaseHref() + 'tinymce',
    suffix: '.min',
    extended_valid_elements: 'a[href|target=_blank]',
    setup: (editor) => {
      if (
        this.summaryState.IssueGuid === '00000000-0000-0000-0000-000000000000'
      ) {
        editor.on('blur', (e) => {
          this.saveSummary.emit(this.summaryEditorForm.get('content').value);
        });
      }

      editor.on('keyup', (e) => {
        let content: string = e.currentTarget.innerHTML;
        if (content.includes('<img src="blob:')) {
          content = this.summaryEditorForm.get('content').value ?? '';
        }
        this.contentCheck(content);
      });

      editor.on('drop', (e) => {
        let content: string = e.currentTarget.children['tinymce'].innerHTML;
        if (content.includes('img src="blob:')) {
          content = this.summaryEditorForm.get('content').value ?? '';
        }
        const totalSize: number =
          [...e.dataTransfer.files].reduce(
            (sum: number, f: File) =>
              sum +
              (f.size * 1.33 + '<img src="data:image/jpeg;base64, />'.length),
            0
          ) + content.length;
        this.contentCheck(totalSize);
      });

      // This will insert the uploaded file to the tinyMCE
      this.eventbus.on(Events.UploadSummaryFileComplete, (element) => {
        this.isUploading = false;
        this.cdr.detectChanges();
        editor.insertContent(element);

        this.contentCheck(this.summaryEditorForm.get('content').value);
      });

      this.eventbus.on(Events.UploadSummaryFileFailure, (element) => {
        this.isUploading = false;
      });
    },
  };

  isUploading = false;
  exceedsLimit = false;
  private CONTENT_LIMIT = 10000000; // 10MB

  constructor(
    private locationStrat: LocationStrategy,
    private eventbus: EventBusService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.summaryState.currentValue) {
      this.summaryEditorForm.patchValue({
        content: changes.summaryState.currentValue.IssueSummary,
      });
    }
  }

  uploadFile($event) {
    // File uploading and saving here
    if ($event && $event[0]) {
      this.isUploading = true;
      this.uploadSummaryFile.emit($event[0]);
    }
  }

  save() {
    this.saveSummary.emit(this.summaryEditorForm.get('content').value);
  }

  cancel() {
    this.cancelEditSummary.emit();
  }

  contentCheck(content: string | number) {
    this.exceedsLimit = this.contentExceedsLimit(content);
    this.cdr.detectChanges();
  }

  contentExceedsLimit = (
    content: string | number,
    limit: number = this.CONTENT_LIMIT
  ): boolean =>
    typeof content === 'number' ? content > limit : content.length > limit;
}
