import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-pre-cwr-dialog',
  templateUrl: './pre-cwr-dialog.component.html',
  styleUrls: ['./pre-cwr-dialog.component.scss'],
})
export class PreCwrDialogComponent {
  constructor(public dialogRef: MatDialogRef<PreCwrDialogComponent>) {}

  onClose() {
    this.dialogRef.close(false);
  }

  onCreate() {
    this.dialogRef.close(true);
  }
}
