import {
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  Inject,
} from '@angular/core';
import {
  GridOptions,
  DisplayedColumnsChangedEvent,
  RowDoubleClickedEvent,
  GridReadyEvent,
  DragStoppedEvent,
  ColumnVisibleEvent,
  ColumnPinnedEvent,
  SortChangedEvent,
  FilterChangedEvent,
  ColumnRowGroupChangedEvent,
  AgGridEvent,
  GridApi,
  ColumnApi,
  CellDoubleClickedEvent,
  IServerSideGetRowsRequest,
  ClipboardModule,
  ServerSideStoreType,
  ColDef,
  EnterpriseCoreModule,
  ColumnsToolPanelModule,
  FiltersToolPanelModule,
  MenuModule,
  RangeSelectionModule,
  RowGroupingModule,
  ServerSideRowModelModule,
  SetFilterModule,
  SideBarModule,
  StatusBarModule,
  ViewportRowModelModule,
  ColumnMovedEvent,
  ValueFormatterParams,
} from '@ag-grid-enterprise/all-modules';

import {
  IssuesFrameworkService,
  IIssueSummary,
  IGridParameters,
  IListState,
} from '@atonix/shared/api';
import { IssueRetrieverService } from '../../service/issue-retriever.service';
import { SavedToolPanelComponent } from '../saved-tool-panel/saved-tool-panel.component';
import { TimeSpanFormatter, dateFormatter, TimeSpanParser } from './formatters';
import { setListState, getListState } from '../../store/state/persistence';
import { ScorecardFormatterComponent } from './scorecard-formatter/scorecard-formatter.component';
import { Clipboard } from '@angular/cdk/clipboard';
import { isNil } from 'lodash';
import { LoggerService, isNumber } from '@atonix/shared/utils';
import { UntypedFormControl } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { formatCurrency, getCurrencySymbol } from '@angular/common';

@Component({
  selector: 'atx-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListViewComponent implements OnChanges, OnDestroy, AfterViewInit {
  @Input() asset: string;
  @Input() listState: IListState;
  @Input() listTheme: string;
  @Input() floatingFilter: boolean;
  @Output() selected = new EventEmitter<string>();
  @Output() listStateChanged = new EventEmitter<IListState>();
  @Output() filterStateChanged = new EventEmitter<IServerSideGetRowsRequest>();
  @Output() listDownloaded = new EventEmitter<IGridParameters>();
  @Output() floatingFilterToggle = new EventEmitter();
  @Output() setScorecard = new EventEmitter<{
    issue: IIssueSummary;
    scorecard: boolean;
  }>();
  @ViewChild('agGridContainer') agGridContainer: ElementRef;
  @ViewChild('keywordSearchInput')
  keywordSearchInput: ElementRef<HTMLInputElement>;

  searchKeywordFrmCtrl: UntypedFormControl = new UntypedFormControl(null);
  showKeywordSearch = false;
  listViewFilters: any[] = [];
  hiddenFilters = ['AssetID'];
  private onDestroy$ = new Subject<void>();
  private storeType: ServerSideStoreType = 'partial';
  private gridApi: GridApi;
  public columnApi: ColumnApi;
  public keyword$: Observable<string>;

  modules = [
    EnterpriseCoreModule,
    ColumnsToolPanelModule,
    FiltersToolPanelModule,
    MenuModule,
    ClipboardModule,
    RangeSelectionModule,
    RowGroupingModule,
    ServerSideRowModelModule,
    SetFilterModule,
    SideBarModule,
    StatusBarModule,
    ViewportRowModelModule,
  ];

  constructor(
    private issuesFrameworkService: IssuesFrameworkService,
    private issuesRetrieverService: IssueRetrieverService,
    private logger: LoggerService,
    private clipboard: Clipboard,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.keyword$ = issuesRetrieverService.keyword$;
  }

  gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    rowGroupPanelShow: 'never',
    enableRangeSelection: true,
    animateRows: true,
    debug: false,
    cacheBlockSize: 100,
    rowSelection: 'multiple',
    suppressCopyRowsToClipboard: true,
    rowHeight: 30,
    blockLoadDebounceMillis: 500,
    maxConcurrentDatasourceRequests: 2,
    tooltipShowDelay: 0,
    getContextMenuItems: this.getContextMenuItems,
    defaultColDef: {
      resizable: true,
      floatingFilter: false,
      cellClass: 'cell-selectable',
    },
    columnDefs: [
      {
        colId: 'AssetIssueID',
        headerName: 'ID',
        field: 'AssetIssueID',
        sortable: true,
        filter: 'agNumberColumnFilter',
      },
      {
        colId: 'Title',
        headerName: 'Title',
        field: 'IssueTitle',
        tooltipField: 'IssueTitle',
        sortable: true,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'ImpactTotal',
        headerName: 'Impact',
        field: 'ImpactTotal',
        sortable: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        valueFormatter: (params: ValueFormatterParams) => {
          let result = '';
          if (!isNil(params.value)) {
            if (isNumber(params.value) && this.appConfig.currency) {
              result = formatCurrency(
                params.value,
                'en',
                getCurrencySymbol(this.appConfig.currency, 'narrow', 'en') ??
                  '',
                this.appConfig.currency
              );
            } else {
              result = String(params.value);
            }
          }
          return result;
        },
        allowedAggFuncs: ['sum', 'min', 'max', 'count', 'avg'],
      },
      {
        colId: 'ActivityStatus',
        headerName: 'Status',
        field: 'ActivityStatus',
        sortable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Closed', 'Open'],
        },
        enableRowGroup: true,
      },
      {
        colId: 'ResolutionStatus',
        headerName: 'Resolution',
        field: 'ResolutionStatus',
        sortable: true,
        enableRowGroup: true,
        filter: 'agTextColumnFilter',
      },
      {
        colId: 'CreateDate',
        headerName: 'Created',
        field: 'CreateDate',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'AssignedTo',
        headerName: 'Assigned',
        field: 'AssignedTo',
        sortable: true,
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'OpenDuration',
        headerName: 'Age (Days)',
        field: 'OpenDuration',
        sortable: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        valueFormatter: TimeSpanFormatter,
        filterParams: {
          numberParser: TimeSpanParser,
          filterOptions: ['lessThan', 'greaterThan', 'inRange'],
        },
        allowedAggFuncs: ['min', 'max'],
      },
      {
        colId: 'Scorecard',
        headerName: 'Scorecard',
        field: 'Scorecard',
        sortable: true,
        enableValue: true,
        cellRenderer: 'scorecardRenderer',
        allowedAggFuncs: ['count'],
        enableRowGroup: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        onCellDoubleClicked: (event: CellDoubleClickedEvent) => {
          this.scorecardChanged(event);
        },
      },
      {
        colId: 'CategoryDesc',
        headerName: 'Category',
        field: 'CategoryDesc',
        sortable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: (params) => {
            this.issuesFrameworkService
              .getOptions(this.asset, 'CategoryDesc')
              .subscribe((n) => {
                params.success(n);
              });
          },
        },
        enableRowGroup: true,
      },
      {
        colId: 'IssueTypeDesc',
        headerName: 'Type',
        field: 'IssueTypeDesc',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'IssueTypeDesc',
        enableRowGroup: true,
      },
      {
        colId: 'AssetClassTypeDesc',
        headerName: 'Asset Type',
        field: 'AssetClassTypeDesc',
        filter: 'agTextColumnFilter',
        sortable: true,
        enableRowGroup: true,
      },
      {
        colId: 'Priority',
        headerName: 'Priority',
        field: 'Priority',
        sortable: true,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['', 'Low', 'Medium', 'High', 'Critical'],
        },
        enableRowGroup: true,
      },
      {
        colId: 'CreatedBy',
        headerName: 'Creator',
        field: 'CreatedBy',
        sortable: true,
        tooltipField: 'CreatedBy',
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'ChangedBy',
        headerName: 'Changed By',
        field: 'ChangedBy',
        sortable: true,
        tooltipField: 'ChangedBy',
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'ChangeDate',
        headerName: 'Changed',
        field: 'ChangeDate',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'OpenDate',
        headerName: 'Opened',
        field: 'OpenDate',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'CloseDate',
        headerName: 'Closed',
        field: 'CloseDate',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'ResolveBy',
        headerName: 'Resolve By',
        field: 'ResolveBy',
        sortable: true,
        filter: 'agDateColumnFilter',
        enableValue: true,
        allowedAggFuncs: ['min', 'max'],
        valueFormatter: dateFormatter,
      },
      {
        colId: 'IssueSummary',
        headerName: 'Summary',
        field: 'IssueSummary',
        sortable: false,
        // tooltipField: 'IssueSummary',
        filter: 'agTextColumnFilter',
        hide: true,
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'ShortSummary',
        headerName: 'Short Summary',
        field: 'ShortSummary',
        cellClass: ['cell-selectable', 'cell-wrap-text'],
        sortable: true,
        autoHeight: true,
        tooltipField: 'ShortSummary',
        filter: 'agTextColumnFilter',
        hide: true,
        width: 450,
      },
      {
        colId: 'Asset',
        headerName: 'Asset',
        field: 'Asset',
        sortable: true,
        filter: 'agTextColumnFilter',
        tooltipField: 'Asset',
        hide: true,
        enableRowGroup: true,
      },
      {
        colId: 'Confidence_Pct',
        headerName: 'Confidence(%)',
        field: 'Confidence_Pct',
        sortable: true,
        hide: true,
        enableValue: true,
        filter: 'agNumberColumnFilter',
        allowedAggFuncs: ['sum', 'min', 'max', 'count', 'avg'],
      },
      {
        colId: 'IssueClassTypeDesc',
        headerName: 'Class Type',
        field: 'IssueClassTypeDescription',
        sortable: true,
        hide: true,
        filter: 'agTextColumnFilter',
        enableRowGroup: true,
      },
      {
        colId: 'IsSubscribed',
        headerName: 'Subscribed',
        field: 'IsSubscribed',
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['Subscribed', 'Not Subscribed'],
        },
        sortable: true,
        hide: true,
        enableValue: true,
        allowedAggFuncs: ['count'],
        enableRowGroup: true,
      },
      {
        colId: 'AssetID',
        headerName: 'AssetID',
        field: 'AssetID',
        sortable: false,
        hide: true,
        filter: 'agTextColumnFilter',
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'IssueTypeID',
        headerName: 'IssueTypeID',
        field: 'IssueTypeID',
        filter: 'agNumberColumnFilter',
        hide: true,
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'CategoryID',
        headerName: 'CategoryID',
        field: 'CategoryID',
        filter: 'agNumberColumnFilter',
        hide: true,
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'GlobalID',
        headerName: 'GlobalID',
        field: 'GlobalID',
        sortable: false,
        filter: 'agTextColumnFilter',
        hide: true,
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
      {
        colId: 'IssueClassTypeID',
        headerName: 'IssueClassTypeID',
        field: 'IssueClassTypeID',
        filter: 'agNumberColumnFilter',
        hide: true,
        suppressFiltersToolPanel: true,
        suppressColumnsToolPanel: true,
      },
    ],
    icons: {
      column:
        '<span class="ag-icon ag-icon-column " style="background: url(assets/columns.svg) no-repeat center;" data-cy="sideButtonColumn"></span>',
      filters:
        '<span class="ag-icon ag-icon-filters" style="background: url(assets/filters.svg) no-repeat center;" data-cy="sideButtonFilters"></span>',
      'my-views':
        '<span class="ag-icon ag-icon-my-views" style="background: url(assets/my-views.svg) no-repeat center;" data-cy="sideButtonMyViews"></span>',
    },
    sideBar: {
      toolPanels: [
        {
          id: 'columns',
          labelDefault: 'Columns',
          labelKey: 'column',
          iconKey: 'column',
          toolPanel: 'agColumnsToolPanel',
          toolPanelParams: {
            suppressPivots: true,
            suppressPivotMode: true,
          },
        },
        {
          id: 'filters',
          labelDefault: 'Filters',
          labelKey: 'filters',
          iconKey: 'filters',
          toolPanel: 'agFiltersToolPanel',
        },
        {
          id: 'views',
          labelDefault: 'My Views',
          labelKey: 'my-views',
          iconKey: 'my-views',
          toolPanel: 'saveToolPanel',
        },
      ],
      defaultToolPanel: '',
      hiddenByDefault: false,
    },
    components: {
      saveToolPanel: SavedToolPanelComponent,
      scorecardRenderer: ScorecardFormatterComponent,
    },
    // icons: { saved: `<span class="ag-icon ag-icon-custom-stats"></span>` },
    suppressAggFuncInHeader: true,
    onGridReady: (event: GridReadyEvent) => {
      this.gridApi = event.api;
      this.columnApi = event.columnApi;
      this.agGridContainer.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-column'
      ).title = 'Columns';
      this.agGridContainer.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-filters'
      ).title = 'Filters';
      this.agGridContainer.nativeElement.querySelector(
        '.ag-side-button-icon-wrapper .ag-icon-my-views'
      ).title = 'My Views';
      this.initializeGrid();
    },
    getChildCount: (data) => {
      if (data && data.Count) {
        return data.Count;
      }
      return 0;
    },
    getRowId: (params) => {
      if (params?.data) {
        return String(params.data.AssetIssueGUID);
      }
      return null;
    },
    onDisplayedColumnsChanged: (event: DisplayedColumnsChangedEvent) => {
      const hiddenCols: string[] = [
        'AssetID',
        'IssueTypeID',
        'CategoryID',
        'GlobalID',
        'IssueClassTypeID',
      ];
      const colsToHide: string[] = [];

      for (const c of hiddenCols) {
        if (event.columnApi.getColumn(c).isVisible()) {
          colsToHide.push(c);
        }
      }
      if (colsToHide.length > 0) {
        event.columnApi.setColumnsVisible(colsToHide, false);
      }
    },
    onDragStopped: (event: DragStoppedEvent) => {
      this.saveState(event);
    },
    onColumnVisible: (event: ColumnVisibleEvent) => {
      this.saveState(event);
    },
    onColumnPinned: (event: ColumnPinnedEvent) => {
      if (event) {
        this.logger.feature('gridpin', 'workmanagement');
      }
      this.saveState(event);
    },
    onSortChanged: (event: SortChangedEvent) => {
      if (event) {
        this.logger.feature('gridsort', 'workmanagement');
      }
      this.saveState(event);
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      if (event) {
        this.getFilters();
      }
      this.saveState(event);
      this.filterChanged(event);
    },
    onColumnMoved: (event: ColumnMovedEvent) => {
      this.saveState(event);
    },
    onColumnRowGroupChanged: (event: ColumnRowGroupChangedEvent) => {
      // FIXME: Or maybe deprecate. Should be called only when user clicks on filters
      // but is being called on every page load.
      // if (event.column) {
      //   this.logger.feature('gridgroup', 'workmanagement');
      //   this.logger.gridAction(
      //     'draggroup',
      //     event.column.getColId(),
      //     'workmanagement'
      //   );
      // }
      this.saveState(event);
    },
    onRowDoubleClicked: (event: RowDoubleClickedEvent) => {
      let skip = false;
      const typelessEvent = event as any;
      if (typelessEvent?.event?.path) {
        const path = typelessEvent.event.path as any[];
        for (const n of path) {
          if (n.tagName === 'ATX-SCORECARD-FORMATTER') {
            skip = true;
            break;
          }
        }
      }
      if (!skip) {
        this.logger.feature('gridissuepopup', 'workmanagement');
        this.selected.emit(event.data.GlobalID);
      }
    },
  };

  ngAfterViewInit(): void {
    this.listViewRefresh();
  }
  ngOnDestroy(): void {
    if (this.gridApi) {
      this.issuesRetrieverService.cancel();
      this.gridApi.destroy();
      this.gridApi = null;
      this.columnApi = null;
    }
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.asset) {
        this.updateAsset(changes.asset.currentValue);
      }

      if (changes.listState) {
        this.updateListState(changes.listState.currentValue);
      }

      if (changes.floatingFilter && this.gridApi) {
        this.setFloatingFilterValue(changes.floatingFilter.currentValue);
      }
    }
  }

  setFloatingFilterValue(newValue: boolean) {
    if (this.columnApi && this.gridApi) {
      const columnDefs = this.gridApi.getColumnDefs();
      columnDefs.forEach((colDef: ColDef) => {
        colDef.floatingFilter = newValue;
      });
      this.gridApi.setColumnDefs(columnDefs);
    }
  }

  initializeGrid() {
    if (this.columnApi && this.gridApi) {
      const savedList = this.listState || getListState();
      if (savedList) {
        if (savedList.columns) {
          this.columnApi.applyColumnState({
            state: savedList.columns,
            applyOrder: true,
          });
        }
        if (savedList.sort) {
          this.columnApi.applyColumnState({ state: savedList.sort });
        }
        if (savedList.filter) {
          let myFilter = savedList.filter;
          if (!myFilter.AssetID) {
            myFilter = {
              ...myFilter,
              AssetID: {
                filter: this.asset,
                filterType: 'text',
                type: 'contains',
              },
            };
          }
          this.gridApi.setFilterModel(myFilter);
        }
        if (savedList.groups) {
          this.columnApi.setColumnGroupState(savedList.groups);
        }
      } else {
        this.gridApi.setFilterModel({
          AssetID: this.getAssetFilter(),
          ActivityStatus: {
            values: ['Open'],
            filterType: 'set',
          },
        });
      }

      this.gridApi.setServerSideDatasource(this.issuesRetrieverService);
    }
  }

  getContextMenuItems(params) {
    const result = ['copy', 'copyWithHeaders'];
    return result;
  }

  getAssetFilter() {
    return {
      filter: this.asset,
      filterType: 'text',
      type: 'contains',
    };
  }

  public saveState(event: AgGridEvent) {
    const state: IListState = {
      columns: event.columnApi.getColumnState(),
      sort: this.getSortState(event.columnApi),
      filter: event.api.getFilterModel(),
      groups: event.columnApi.getColumnGroupState(),
    };
    setListState(state);
    this.listStateChanged.emit(state);
  }

  public filterChanged(event: FilterChangedEvent) {
    const filterModel = event.api.getFilterModel();
    let state: IServerSideGetRowsRequest = {
      startRow: 0,
      endRow: 100,
      rowGroupCols: [],
      valueCols: [],
      pivotCols: [],
      pivotMode: false,
      groupKeys: [],
      filterModel: filterModel,
      sortModel: [],
    };

    this.keyword$.pipe(take(1)).subscribe((keyword) => {
      if (keyword) {
        state = {
          ...state,
          filterModel: {
            ...filterModel,
            Keyword: {
              filterType: 'set',
              values: [keyword],
            },
          },
        };
      }

      this.filterStateChanged.emit(state);
    });
  }

  listViewDownload() {
    const gridParams: IGridParameters = {
      startRow: 0,
      endRow: 20000,
      rowGroupCols: [],
      valueCols: [],
      pivotCols: [],
      groupKeys: [],
      pivotMode: false,
      filterModel: null,
      sortModel: [],
      displayCols: [],
    };

    const columns = this.columnApi.getAllGridColumns();
    const rowGroupCols = this.columnApi.getRowGroupColumns();
    const valueCols = this.columnApi.getValueColumns();
    const pivotCols = this.columnApi.getPivotColumns();

    gridParams.filterModel = this.gridApi.getFilterModel();
    gridParams.sortModel = this.getSortState(this.columnApi);

    if (columns) {
      columns.map((column) => {
        const colDef = column.getColDef();
        if (column.isVisible()) {
          gridParams.displayCols.push({
            id: colDef.colId,
            displayName: colDef.headerName,
            field: colDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (rowGroupCols) {
      rowGroupCols.map((rowGroupCol) => {
        const groupColDef = rowGroupCol.getColDef();
        if (groupColDef) {
          gridParams.rowGroupCols.push({
            id: groupColDef.colId,
            displayName: groupColDef.headerName,
            field: groupColDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (valueCols) {
      valueCols.map((valueCol) => {
        const valueColDef = valueCol.getColDef();
        if (valueColDef) {
          gridParams.valueCols.push({
            id: valueColDef.colId,
            displayName: valueColDef.headerName,
            field: valueColDef.field,
            aggFunc: null,
          });
        }
      });
    }

    if (pivotCols) {
      pivotCols.map((pivotCol) => {
        const pivotColDef = pivotCol.getColDef();
        if (pivotColDef) {
          gridParams.valueCols.push({
            id: pivotColDef.colId,
            displayName: pivotColDef.headerName,
            field: pivotColDef.field,
            aggFunc: null,
          });
        }
      });
    }
    this.listDownloaded.emit(gridParams);
  }

  public listViewRefresh() {
    if (this.gridOptions && this.gridOptions.api) {
      this.issuesRetrieverService.clearCacher();
      this.gridOptions.api.onFilterChanged();
    }
  }

  updateAsset(asset: string) {
    if (this.gridOptions && this.gridOptions.api && asset) {
      const assetIdFilter = this.gridOptions.api.getFilterInstance('AssetID');
      if (asset !== assetIdFilter.getModel()?.filter) {
        assetIdFilter.setModel({ type: 'asset', filter: asset });
        this.gridOptions.api.onFilterChanged();
      }
    }
  }

  updateListState(listState: IListState) {
    if (
      this.gridOptions &&
      this.gridOptions.columnApi &&
      this.gridOptions.api &&
      listState
    ) {
      if (listState.columns) {
        this.gridOptions.columnApi.applyColumnState({
          state: listState.columns,
          applyOrder: true,
        });
      } else {
        this.gridOptions.columnApi.resetColumnState();
        this.gridOptions.columnApi.applyColumnState({
          applyOrder: true,
        });
      }

      if (listState.sort) {
        this.gridOptions.columnApi.applyColumnState({
          state: listState.sort,
        });
      } else {
        this.gridOptions.columnApi.applyColumnState({
          defaultState: { sort: null },
        });
      }

      if (listState.filter) {
        const myFilter = {
          ...listState.filter,
          AssetID: this.getAssetFilter(),
        };

        this.gridOptions.api.setFilterModel(myFilter);
      } else {
        this.gridOptions.api.setFilterModel(null);
      }

      if (listState.groups) {
        this.gridOptions.columnApi.setColumnGroupState(listState.groups);
      } else {
        this.gridOptions.columnApi.resetColumnGroupState();
      }
    }
  }

  toggleFloatingFilter() {
    this.floatingFilterToggle.emit();
  }

  scorecardChanged(event: CellDoubleClickedEvent) {
    const value: IIssueSummary = event.data;
    this.setScorecard.emit({ issue: value, scorecard: !value.Scorecard });
    event.node.updateData({ ...value, Scorecard: !value.Scorecard });
  }

  getFilters() {
    if (this.gridApi) {
      const filters = this.gridApi.getFilterModel();
      this.listViewFilters = [];
      Object.keys(filters).forEach((key) => {
        if (!this.listViewFilters.includes(key)) {
          const isHidden = this.hiddenFilters.includes(key);
          if (!isHidden) {
            const colDef = this.gridApi.getColumnDef(key);
            this.listViewFilters.push({
              Name: key,
              Removable: true,
              DisplayName: colDef.headerName,
            });
          }
        }
      });
    }
  }

  removeFilter(filter: any) {
    const idx = this.listViewFilters.indexOf(filter);

    if (idx >= 0) {
      this.listViewFilters.splice(idx, 1);
      this.gridApi.getFilterInstance(filter.Name).setModel(null);
      this.listViewRefresh();
    }
  }

  clearFilters($event: MouseEvent) {
    const filters = this.gridApi.getFilterModel();

    Object.keys(filters).forEach((key) => {
      const isHidden = this.hiddenFilters.includes(key);
      if (!isHidden) {
        this.gridApi.getFilterInstance(key).setModel(null);
      }
    });

    this.listViewFilters = [];
    this.listViewRefresh();
  }

  getSortState(columnApi: ColumnApi) {
    const sortState = columnApi.getColumnState().filter((x) => !isNil(x.sort));
    const sortModels = sortState.map(function (state) {
      return {
        colId: state.colId,
        sort: state.sort,
      };
    });

    return sortModels;
  }

  toggleKeywordSearch() {
    this.showKeywordSearch = !this.showKeywordSearch;
    setTimeout(() => {
      this.keywordSearchInput.nativeElement.focus();
    }, 100);
  }
  searchByKeyword() {
    this.issuesRetrieverService.setKeyword(this.searchKeywordFrmCtrl.value);
    this.issuesRetrieverService.clearCacher();
    this.gridApi.onFilterChanged();
    this.toggleKeywordSearch();
  }
  resetSearch() {
    this.showKeywordSearch = false;
    this.searchKeywordFrmCtrl.reset();
    this.issuesRetrieverService.setKeyword(null);
    this.issuesRetrieverService.clearCacher();
    this.gridApi.onFilterChanged();
  }
}
