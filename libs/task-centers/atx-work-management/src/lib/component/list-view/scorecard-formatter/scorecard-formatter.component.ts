import { Component } from '@angular/core';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';

@Component({
  selector: 'atx-scorecard-formatter',
  template: '<mat-icon style="color: #e2bd68;">{{value}}</mat-icon>',
})
export class ScorecardFormatterComponent implements ICellRendererAngularComp {
  value: 'star' | 'star_outline';

  agInit(params: any) {
    this.value = params?.data?.Scorecard ? 'star' : 'star_outline';
  }

  refresh(params: any): boolean {
    this.value = params?.data?.Scorecard ? 'star' : 'star_outline';
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
