import { ValueFormatterParams } from '@ag-grid-enterprise/all-modules';
import moment from 'moment';
import { isNil } from '@atonix/atx-core';

function isNumber(x: any): x is number {
  return typeof x === 'number';
}

export function TimeSpanFormatter(params: ValueFormatterParams) {
  if (!isNil(params.value)) {
    if (isNumber(params.value) && params.value > 0) {
      return String(Math.floor(params.value / 86400));
    } else {
      return '-';
    }
  }
}

export function TimeSpanParser(text: string | null) {
  return !isNil(text) ? +text * 86400 : null;
}

export function dateFormatter(params) {
  return !isNil(params.value)
    ? moment(params.value).format('MM/DD/YYYY')
    : null;
}
