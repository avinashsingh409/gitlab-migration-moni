import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ListViewComponent } from './list-view.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LicenseManager } from '@ag-grid-enterprise/all-modules';
import { mockListState } from '../../data/listview-mocks';
import { IssueRetrieverService } from '../../service/issue-retriever.service';
import { SavedToolPanelComponent } from '../saved-tool-panel/saved-tool-panel.component';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { BehaviorSubject } from 'rxjs';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { AgGridModule } from '@ag-grid-community/angular';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';

LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

describe('list view saved views tests', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;
  let mockSummaryFacade: SummaryFacade;
  let navFacade: NavFacade;
  let issueRetrieverService: IssueRetrieverService;

  beforeEach(() => {
    mockSummaryFacade = createMockWithValues(SummaryFacade, {
      selectedAsset$: new BehaviorSubject<string>('test'),
    });
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
      breadcrumb$: new BehaviorSubject<IButtonData>(null),
    });

    TestBed.configureTestingModule({
      declarations: [ListViewComponent, SavedToolPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FlexLayoutModule,
        AgGridModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        IssueRetrieverService,
        {
          provide: SummaryFacade,
          useValue: mockSummaryFacade,
        },
        {
          provide: MatchMedia,
          useClass: MockMatchMedia,
        },
        {
          provide: MATERIAL_SANITY_CHECKS,
          useValue: false,
        },
        { provide: NavFacade, useValue: navFacade },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
    issueRetrieverService = TestBed.inject(IssueRetrieverService);
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    component.listViewFilters = [];
    fixture.detectChanges();
  });

  it('test rearranging columns, first column is moved to last.', async () => {
    fixture.detectChanges();
    expect(
      component.gridOptions.columnApi.getAllDisplayedColumns()[0].getColId()
    ).toEqual('AssetIssueID');

    //update the state.
    component.updateListState(mockListState);

    expect(
      component.gridOptions.columnApi.getAllDisplayedColumns()[0].getColId()
    ).toEqual('Title');
    expect(
      component.gridOptions.columnApi
        .getAllDisplayedColumns()
        [
          component.gridOptions.columnApi.getAllDisplayedColumns().length - 1
        ].getColId()
    ).toEqual('AssetIssueID');
  });

  it('test hiding a column', async () => {
    fixture.detectChanges();

    expect(
      component.gridOptions.columnApi.getColumn('AssetIssueID').isVisible()
    ).toBe(true);

    component.gridOptions.columnApi.setColumnsVisible(['AssetIssueID'], false);

    expect(
      component.gridOptions.columnApi.getColumn('AssetIssueID').isVisible()
    ).toBe(false);
  });
});
