/* eslint-disable @typescript-eslint/no-empty-function */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ListViewComponent } from './list-view.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SavedToolPanelComponent } from '../saved-tool-panel/saved-tool-panel.component';
import {
  AgGridEvent,
  Column,
  ColumnApi,
  ColumnRowGroupChangedEvent,
  FilterChangedEvent,
  GridApi,
} from '@ag-grid-enterprise/all-modules';
import {
  IIssueSummary,
  IListState,
  IssuesFrameworkService,
} from '@atonix/shared/api';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  FlexLayoutModule,
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
import { AgGridModule } from '@ag-grid-community/angular';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { BehaviorSubject } from 'rxjs';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

describe('ListViewComponent', () => {
  let component: ListViewComponent;
  let fixture: ComponentFixture<ListViewComponent>;
  let mockSummaryFacade: SummaryFacade;
  let navFacade: NavFacade;
  let mockGridApi: GridApi;

  beforeEach(waitForAsync(() => {
    mockSummaryFacade = createMockWithValues(SummaryFacade, {
      selectedAsset$: new BehaviorSubject<string>('test'),
    });
    navFacade = createMockWithValues(NavFacade, {
      theme$: new BehaviorSubject<string>('light'),
      breadcrumb$: new BehaviorSubject<IButtonData>(null),
    });
    mockGridApi = createMockWithValues(GridApi, {
      destroy: jest.fn(),
    });
    TestBed.configureTestingModule({
      declarations: [ListViewComponent, SavedToolPanelComponent],
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        FlexLayoutModule,
        AgGridModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        { provide: MatchMedia, useClass: MockMatchMedia },
        { provide: SummaryFacade, useValue: mockSummaryFacade },
        {
          provide: ActivatedRoute,
          useValue: {},
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: GridApi, useValue: mockGridApi },
        { provide: NavFacade, useValue: navFacade },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListViewComponent);
    component = fixture.componentInstance;
    component.listViewFilters = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should succeed all grid options methods', () => {
    it('onGridReady()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
      };

      const mockColumnApi: any = {
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        applyColumnState(applyColumnState: any) {},
      };

      const mockEvent: AgGridEvent = {
        api: mockApi,
        columnApi: mockColumnApi,
        type: null,
      };

      const mockListState: IListState = {
        columns: ['testing', 'only'],
        sort: [
          {
            colId: 'Test',
            sort: 'asc',
          },
        ],
        groups: [
          {
            groupId: '12345',
            open: true,
          },
        ],
        filter: {
          AssetID: {
            filter: null,
          },
        },
      };
      jest.spyOn(mockEvent.api, 'setFilterModel');
      jest.spyOn(mockEvent.columnApi, 'setColumnGroupState');
      jest.spyOn(mockEvent.api, 'setServerSideDatasource');
      jest.spyOn(mockEvent.columnApi, 'applyColumnState');
      const columnDiv = document.createElement('div');
      columnDiv.className = 'ag-side-button-icon-wrapper';
      const columnSpan = document.createElement('span');
      columnSpan.className = 'ag-icon-column';
      columnDiv.appendChild(columnSpan);

      const filtersDiv = document.createElement('div');
      filtersDiv.className = 'ag-side-button-icon-wrapper';
      const filtersSpan = document.createElement('span');
      filtersSpan.className = 'ag-icon-filters';
      filtersDiv.appendChild(filtersSpan);

      const viewsDiv = document.createElement('div');
      viewsDiv.className = 'ag-side-button-icon-wrapper';
      const viewsSpan = document.createElement('span');
      viewsSpan.className = 'ag-icon-my-views';
      viewsDiv.appendChild(viewsSpan);

      component.agGridContainer.nativeElement.appendChild(columnDiv);
      component.agGridContainer.nativeElement.appendChild(filtersDiv);
      component.agGridContainer.nativeElement.appendChild(viewsDiv);

      component.listState = mockListState;
      component.gridOptions.onGridReady(mockEvent);
      expect(mockEvent.columnApi.applyColumnState).toHaveBeenCalled();
      expect(mockEvent.api.setFilterModel).toHaveBeenCalled();
      expect(mockEvent.columnApi.setColumnGroupState).toHaveBeenCalled();
      expect(mockEvent.api.setServerSideDatasource).toHaveBeenCalled();
      expect(mockEvent.columnApi.applyColumnState).toHaveBeenCalled();
    });

    it('getChildCount()', () => {
      expect(component.gridOptions.getChildCount({ Count: 123 })).toEqual(123);
      expect(component.gridOptions.getChildCount(null)).toEqual(0);
    });

    it('getRowId()', () => {
      const mockIssueSummary: IIssueSummary = {
        AssetIssueGUID: '12345',
        AssetIssueID: 1,
        CategoryDesc: 'Testing Only',
        CategoryID: 2,
        IssueTitle: 'Testing Only',
        IssueSummary: 'Testing Only',
        ShortSummary: 'Testing Only',
        Description: '',
        Client: 'Testind Client',
        StationGroup: 'Testing',
        Station: 'Testing',
        StationAssetAbbrev: 'Testing',
        Unit: 'Test',
        UnitAssetAbbrev: 'Test',
        System: 'Test',
        SystemAssetClassTypeID: 3,
        Asset: '1234567',
        AssetClassTypeID: 4,
        AssetID: 5,
        ImpactsString: 'Testing Only',
        ImpactTotal: 6,
        Confidence_Pct: 7,
        IssueTypeDesc: 'Testing',
        IssueTypeID: 8,
        IssueClassTypeID: 9,
        IssueClassTypeDescription: 'Testing',
        Priority: 'High',
        CreatedBy: 'Tester',
        CreateDate: new Date('1/1/2020'),
        ChangeDate: new Date('2/2/2020'),
        ChangedBy: 'Tester',
        ActivityStatus: 'In Progress',
        ResolutionStatus: 'In Progress',
        CloseDate: new Date('3/3/2020'),
        IsSubscribed: false,
        GlobalID: '123456789',
        OpenDuration: 10,
        OpenDate: new Date('4/4/2020'),
        AssignedTo: 'Tester',
        Scorecard: true,
        ResolveBy: new Date('5/5/2020'),
        AssetClassTypeAbbrev: 'For Testing Purposes',
        AssetClassTypeDesc: 'For Testing Purposes',
        AssetTypeAbbrev: 'For Testing Purposes',
        AssetTypeDesc: 'For Testing Purposes',
        CreatedByUserID: 11,
        CanEdit: false,
        CanDelete: true,
        CanEditIssueStatus: false,
      };

      expect(
        component.gridOptions.getRowId({
          data: mockIssueSummary,
          level: 1,
          api: null,
          columnApi: null,
          context: null,
        })
      ).toEqual(String(mockIssueSummary.AssetIssueGUID));
      expect(component.gridOptions.getRowId(null)).toBeNull();
    });

    it('onDisplayedColumnsChanged()', () => {
      const mockColumnApi: any = {
        getColumn(key: any): Column {
          return null;
        },
        setColumnsVisible(keys: (string | Column)[], visible: boolean) {},
      };

      const mockEvent: AgGridEvent = {
        api: null,
        columnApi: mockColumnApi,
        type: null,
      };

      const mockIsVisible: any = {
        isVisible: () => {
          return true;
        },
      };

      jest
        .spyOn(mockEvent.columnApi, 'getColumn')
        .mockReturnValue(mockIsVisible);
      jest.spyOn(mockEvent.columnApi, 'setColumnsVisible');

      component.gridOptions.onDisplayedColumnsChanged(mockEvent);
      expect(mockEvent.columnApi.getColumn).toHaveBeenCalled();
      expect(mockEvent.columnApi.setColumnsVisible).toHaveBeenCalledWith(
        [
          'AssetID',
          'IssueTypeID',
          'CategoryID',
          'GlobalID',
          'IssueClassTypeID',
        ],
        false
      );
    });

    it('onDragStopped()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');
      const mockEvent = {
        pinned: 'yes',
        columnApi: mockColumnApi,
        api: mockApi,
        column: null,
        columns: null,
        source: null,
        type: 'drag',
      };

      component.gridOptions.onDragStopped(mockEvent as any);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });

    it('onColumnVisible()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');
      const mockEvent = {
        pinned: 'yes',
        columnApi: mockColumnApi,
        api: mockApi,
        column: null,
        columns: null,
        source: null,
        visible: true,
      };

      component.gridOptions.onColumnVisible(mockEvent as any);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });

    it('onColumnPinned()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');
      const mockEvent = {
        pinned: 'yes',
        columnApi: mockColumnApi,
        api: mockApi,
        column: null,
        columns: null,
        source: null,
        type: 'test',
      };

      component.gridOptions.onColumnPinned(mockEvent as any);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });

    it('onSortChanged()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');

      const mockEvent: any = {
        api: mockApi,
        columnApi: mockColumnApi,
      };

      component.gridOptions.onSortChanged(mockEvent);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });

    it('onFilterChanged()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');

      const mockEvent: Partial<FilterChangedEvent> = {
        api: mockApi,
        type: 'filterchanged',
        columnApi: mockColumnApi as ColumnApi,
      };
      component.gridOptions.onFilterChanged(mockEvent as any);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });

    it('onColumnRowGroupChanged()', () => {
      const mockApi: any = {
        setFilterModel(model: any) {},
        setServerSideDatasource(datasource: any) {},
        destroy: jest.fn(),
        getFilterModel: jest.fn(),
      };
      const mockColumnApi: Partial<ColumnApi> = {
        getColumnGroupState: jest.fn(),
        getColumnState: jest.fn(() => []),
        setColumnGroupState(
          stateItems: { groupId: string; open: boolean }[]
        ) {},
        setColumnGroupOpened: jest.fn(),
      };
      jest.spyOn(component, 'saveState');
      const mockEvent: Partial<ColumnRowGroupChangedEvent> = {
        column: null,
        columns: [],
        source: null,
        api: mockApi,
        type: 'filterchanged',
        columnApi: mockColumnApi as ColumnApi,
      };

      component.gridOptions.onColumnRowGroupChanged(mockEvent as any);
      expect(component.saveState).toHaveBeenCalledWith(mockEvent);
    });
  });

  it('should save state', () => {
    jest.spyOn(component.listStateChanged, 'emit');
    const mockApi: any = {
      setFilterModel(model: any) {},
      setServerSideDatasource(datasource: any) {},
      destroy: jest.fn(),
      getFilterModel() {
        return [];
      },
    };
    const mockColumnApi: Partial<ColumnApi> = {
      getColumnGroupState() {
        return [];
      },
      getColumnState() {
        return [];
      },
      setColumnGroupState(stateItems: { groupId: string; open: boolean }[]) {},
      setColumnGroupOpened: jest.fn(),
    };

    const event: AgGridEvent = {
      api: mockApi,
      columnApi: mockColumnApi as ColumnApi,
      type: null,
    };

    component.saveState(event);
    expect(component.listStateChanged.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        columns: [],
        sort: [],
        filter: [],
        groups: [],
      })
    );
  });

  it('should update asset filter', () => {
    const mockApi: any = {
      setFilterModel(model: any) {},
      getFilterInstance(filter: string): any {},
      setServerSideDatasource(datasource: any) {},
      destroy: jest.fn(),
      onFilterChanged() {},
      getFilterModel() {
        return [];
      },
    };

    const mockSetModel: any = {
      getModel: () => {
        return {
          filter: 'something',
        };
      },
      setModel(filters: { type: string; filter: string }) {
        return null;
      },
    };

    component.gridOptions.api = mockApi;
    jest
      .spyOn(component.gridOptions.api, 'getFilterInstance')
      .mockReturnValue(mockSetModel);
    jest.spyOn(component.gridOptions.api, 'onFilterChanged');

    component.updateAsset('1234');
    expect(component.gridOptions.api.getFilterInstance).toHaveBeenCalled();
    expect(component.gridOptions.api.onFilterChanged).toHaveBeenCalled();
  });

  it('should update list state when there are values pass', () => {
    const mockApi: any = {
      setFilterModel(model: any) {},
      applyColumnState(applyColumnState: any) {},
      getFilterInstance(filter: string): any {},
      setServerSideDatasource(datasource: any) {},
      destroy: jest.fn(),
      onFilterChanged() {},
      getFilterModel() {
        return [];
      },
    };

    const mockColumnApi: any = {
      setColumnGroupState(stateItems: any) {},
      applyColumnState(applyColumnState: any) {},
    };

    const mockListState: IListState = {
      columns: ['testing', 'only'],
      sort: [
        {
          colId: 'Test',
          sort: 'asc',
        },
      ],
      groups: [
        {
          groupId: '12345',
          open: true,
        },
      ],
      filter: {
        AssetID: {
          filter: '1234',
          filterType: 'text',
          type: 'contains',
        },
      },
    };

    component.gridOptions.api = mockApi;
    component.gridOptions.columnApi = mockColumnApi;

    jest.spyOn(component.gridOptions.api, 'setFilterModel');
    jest.spyOn(component.gridOptions.columnApi, 'setColumnGroupState');
    jest.spyOn(component.gridOptions.columnApi, 'applyColumnState');

    component.asset = '1234';
    component.updateListState(mockListState);

    expect(
      component.gridOptions.columnApi.applyColumnState
    ).toHaveBeenCalledWith({ applyOrder: true, state: mockListState.columns });

    expect(component.gridOptions.api.setFilterModel).toHaveBeenCalledWith(
      mockListState.filter
    );
    expect(
      component.gridOptions.columnApi.setColumnGroupState
    ).toHaveBeenCalledWith(mockListState.groups);
    expect(
      component.gridOptions.columnApi.applyColumnState
    ).toHaveBeenCalledWith({ state: mockListState.sort });
  });

  it('should update list state when there are no values pass', () => {
    const mockApi: any = {
      setFilterModel(model: any) {},
    };

    const mockColumnApi: any = {
      resetColumnState() {},
      resetColumnGroupState() {},
      applyColumnState(applyColumnState: any) {},
    };

    const mockListState: IListState = {
      columns: null,
      sort: null,
      groups: null,
      filter: null,
    };

    component.gridOptions.api = mockApi;
    component.gridOptions.columnApi = mockColumnApi;
    jest.spyOn(component.gridOptions.columnApi, 'resetColumnState');
    jest.spyOn(component.gridOptions.api, 'setFilterModel');
    jest.spyOn(component.gridOptions.columnApi, 'resetColumnGroupState');
    jest.spyOn(component.gridOptions.columnApi, 'applyColumnState');

    component.asset = '1234';
    component.updateListState(mockListState);

    expect(
      component.gridOptions.columnApi.resetColumnState
    ).toHaveBeenCalledWith();
    expect(component.gridOptions.api.setFilterModel).toHaveBeenCalledWith(null);
    expect(
      component.gridOptions.columnApi.resetColumnGroupState
    ).toHaveBeenCalledWith();
    expect(
      component.gridOptions.columnApi.applyColumnState
    ).toHaveBeenCalledWith({ defaultState: Object({ sort: null }) });
  });
});
