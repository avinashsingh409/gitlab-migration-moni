import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-delete-issue-modal',
  templateUrl: './delete-issue-modal.component.html',
  styleUrls: ['./delete-issue-modal.component.scss'],
})
export class DeleteIssueModalComponent {
  constructor(public dialogRef: MatDialogRef<DeleteIssueModalComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteIssue(): void {
    this.dialogRef.close(true);
  }
}
