import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { Observable, Subject } from 'rxjs';
import { getQuickReplyIdFromRoute, NavFacade } from '@atonix/atx-navigation';
import { ActivatedRoute } from '@angular/router';
import {
  QuickReplyServiceFacade,
  QuickReplyServiceState,
} from '../../service/quick-reply.service.facade';
import { QuickReplyFacade } from '../../store/facade/quick-reply.facade';
import { IQuickReplyState } from '../../store/state/quick-reply-state';
import { UntypedFormControl, Validators } from '@angular/forms';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'atx-quick-reply',
  templateUrl: './quick-reply.component.html',
  styleUrls: ['./quick-reply.component.scss'],
  providers: [QuickReplyServiceFacade],
})
export class QuickReplyComponent implements OnInit, OnDestroy, AfterViewInit {
  public quickReplyState$: Observable<IQuickReplyState>;
  public vm$: Observable<QuickReplyServiceState>;
  private onDestroy = new Subject<void>();
  titleCtrl = new UntypedFormControl(null);
  contentCtrl = new UntypedFormControl(null, [Validators.required]);

  initQuickReplyEditor = {
    promotion: false,
    plugins: 'image autolink link',
    contextmenu: false,
    paste_data_images: true,
    convert_urls: false,
    relative_urls: false,
    remove_script_host: false,
    toolbar:
      'undo redo | bold italic underline | link | forecolor | fontselect emoticons',
    menubar: 'edit view format',
    menu: {
      edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
      view: { title: 'View', items: 'visualaid' },
      format: {
        title: 'Format',
        items: 'bold italic underline | removeformat',
      },
    },
    statusbar: false,
    browser_spellcheck: true,
    base_url: this.locationStrat.getBaseHref() + 'tinymce',
    suffix: '.min',
    extended_valid_elements: 'a[href|target=_blank]',
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private locationStrat: LocationStrategy,
    private quickReplyServiceFacade: QuickReplyServiceFacade,
    private quickReplyFacade: QuickReplyFacade,
    public navFacade: NavFacade
  ) {
    this.navFacade.configure({ navPaneState: 'hidden' });
    this.navFacade.configureLayoutButton('nNavbar', { visible: false });
    this.navFacade.toggleNavigation('hidden');
  }

  ngOnInit() {
    this.quickReplyState$ = this.quickReplyFacade.selectQuickReplyState$;
    this.vm$ = this.quickReplyServiceFacade.vm$;

    this.quickReplyServiceFacade.isSaved$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((saved) => {
        if (saved) {
          this.clearNewEntry();
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngAfterViewInit(): void {
    const quickReplyID = getQuickReplyIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.quickReplyFacade.initializeQuickReply();
    this.navFacade.user$.subscribe((user) => {
      this.quickReplyServiceFacade.init(quickReplyID, user.email);
    });
  }

  toggleNewEntry() {
    this.quickReplyServiceFacade.toggleNewEntry();
  }

  getIssueUrl(issueID: string) {
    let url = `/issues/i?iid=${issueID}`;

    if (window.location.hostname !== 'localhost') {
      url = '/MD' + url;
    }

    return url;
  }

  uploadFile($event: FileList, assetID: number) {
    // File uploading and saving here
    if ($event?.length > 0) {
      this.quickReplyFacade.addQuickReplyAttachments(
        Array.from($event).map((file) => file),
        assetID
      );
    }
  }

  removeAttachments(index: number) {
    this.quickReplyFacade.removeQuickReplyAttachment(index);
  }

  save() {
    this.quickReplyServiceFacade.saveReply(
      this.titleCtrl.value,
      this.contentCtrl.value
    );
  }

  cancel() {
    this.quickReplyServiceFacade.cancelEntry();
    this.clearNewEntry();
  }

  clearNewEntry() {
    this.titleCtrl.setValue(null);
    this.contentCtrl.setValue(null);
    this.quickReplyFacade.clearQuickReplyAttachments();
  }
}
