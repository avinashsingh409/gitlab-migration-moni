import { Observable } from 'rxjs';
import { IToolPanelParams } from '@ag-grid-enterprise/all-modules';
import { IToolPanelAngularComp } from '@ag-grid-community/angular';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { NavFacade } from '@atonix/atx-navigation';
import { ISavedIssueList } from '@atonix/shared/api';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { map } from 'rxjs/operators';

@Component({
  selector: 'atx-saved-tool-panel',
  templateUrl: './saved-tool-panel.component.html',
  styleUrls: ['./saved-tool-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SavedToolPanelComponent implements IToolPanelAngularComp, OnInit {
  private params: IToolPanelParams;
  public lists$: Observable<ISavedIssueList[]>;
  public name = new UntypedFormControl('');
  public isLightTheme$: Observable<boolean>;

  agInit(params: IToolPanelParams): void {
    this.params = params;
  }

  constructor(
    private summaryFacade: SummaryFacade,
    private navFacade: NavFacade
  ) {
    this.lists$ = summaryFacade.savedLists$;
    this.isLightTheme$ = this.navFacade.theme$.pipe(
      map((n) => {
        return n === 'light';
      })
    );
  }

  ngOnInit() {
    this.summaryFacade.loadLists();
  }

  trackByFn(list: ISavedIssueList) {
    return list.id;
  }

  saveList() {
    if (this.name.value) {
      this.summaryFacade.saveList({
        id: -1,
        name: this.name.value,
      });
      this.name.setValue('');
    }
  }

  loadDefault() {
    this.summaryFacade.loadDefaultList();
  }

  loadList(list: ISavedIssueList) {
    if (list) {
      this.summaryFacade.loadList(list.id);
    }
  }

  delete(list: ISavedIssueList) {
    if (list) {
      if (confirm('Are you sure you want to delete this saved view?')) {
        this.summaryFacade.deleteList(list.id);
      }
    }
  }
}
