import {
  ComponentFixture,
  TestBed,
  inject,
  waitForAsync,
} from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { NavFacade } from '@atonix/atx-navigation';

import { SavedToolPanelComponent } from './saved-tool-panel.component';
import { SummaryFacade } from '../../store/facade/summary.facade';
import { ISavedIssueList } from '@atonix/shared/api';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('SavedToolPanelComponent', () => {
  let component: SavedToolPanelComponent;
  let fixture: ComponentFixture<SavedToolPanelComponent>;
  let mockSummaryFacade: SummaryFacade;
  let mockNavFacade: NavFacade;

  beforeEach(waitForAsync(() => {
    mockSummaryFacade = createMockWithValues(SummaryFacade, {
      loadLists: jest.fn(),
      loadList: jest.fn(),
      saveList: jest.fn(),
      loadDefaultList: jest.fn(),
      deleteList: jest.fn(),
      savedLists$: new BehaviorSubject<any>(null),
    });
    mockNavFacade = createMockWithValues(NavFacade, {
      configure: jest.fn(),
      theme$: new BehaviorSubject<string>('light'),
    });

    TestBed.configureTestingModule({
      declarations: [SavedToolPanelComponent],
      imports: [ReactiveFormsModule, AtxMaterialModule, NoopAnimationsModule],
      providers: [
        {
          provide: SummaryFacade,
          useValue: mockSummaryFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: NavFacade,
          useValue: mockNavFacade,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedToolPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.agInit(null);
    expect(component).toBeTruthy();
  });

  it('should track by fn', () => {
    const savedIssueList: ISavedIssueList = {
      id: 1234,
      name: 'Test Only',
      state: null,
      checked: false,
    };

    component.trackByFn(savedIssueList);
    expect(component.trackByFn(savedIssueList)).toEqual(savedIssueList.id);
  });

  it('should save list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      component.name.setValue('Test Only');
      component.saveList();
      expect(summaryFacade.saveList).toHaveBeenCalledWith({
        id: -1,
        name: 'Test Only',
      });
    }
  ));

  it('should load list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      const savedIssueList: ISavedIssueList = {
        id: 1234,
        name: 'Test Only',
        state: null,
        checked: false,
      };

      component.loadList(savedIssueList);
      expect(mockSummaryFacade.loadList).toHaveBeenCalledWith(
        savedIssueList.id
      );
    }
  ));

  it('should delete list', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      const savedIssueList: ISavedIssueList = {
        id: 1234,
        name: 'Test Only',
        state: null,
        checked: false,
      };

      jest.spyOn(window, 'confirm').mockReturnValue(true);
      component.delete(savedIssueList);
      expect(summaryFacade.deleteList).toHaveBeenCalledWith(savedIssueList.id);
    }
  ));
});
