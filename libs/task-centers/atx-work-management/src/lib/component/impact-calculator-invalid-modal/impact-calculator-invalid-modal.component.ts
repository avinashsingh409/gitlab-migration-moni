import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../issue-snapshot/issue-snapshot.component';

@Component({
  selector: 'atx-save-changes-modal',
  templateUrl: './impact-calculator-invalid-modal.component.html',
  styleUrls: ['./impact-calculator-invalid-modal.component.scss'],
})
export class ImpactCalculatorInvalidModalComponent {
  constructor(
    public dialogRef: MatDialogRef<ImpactCalculatorInvalidModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}
}
