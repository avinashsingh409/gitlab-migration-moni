import {
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import {
  SendIssueModalFacade,
  SendIssueModalState,
} from '../../service/send-issue-modal.facade';
import { ISubscriber } from '@atonix/shared/api';
import { take, takeUntil } from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material/checkbox';
export interface ISendIssueDialogData {
  assetIssueGlobalID: string;
  assetIssueID: number;
  issueTitle: string;
  message: string;
}
@Component({
  selector: 'atx-send-issue-dialog',
  templateUrl: './send-issue-dialog.component.html',
  styleUrls: ['./send-issue-dialog.component.scss'],
})
export class SendIssueDialogComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject<void>();
  public vm$: Observable<SendIssueModalState>;
  @ViewChild('registeredRecipientsInput')
  registeredRecipientsInput: ElementRef<HTMLInputElement>;
  @ViewChild('nonRegisteredRecipientsInput')
  nonRegisteredRecipientsInput: ElementRef<HTMLInputElement>;

  registeredRecipients = new UntypedFormControl();
  nonRegisteredRecipients = new UntypedFormControl(null, [Validators.email]);
  message = new UntypedFormControl();
  subscribeUser = true;

  constructor(
    public dialogRef: MatDialogRef<SendIssueDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ISendIssueDialogData,
    private sendIssueModalFacade: SendIssueModalFacade
  ) {
    this.registeredRecipients =
      sendIssueModalFacade.buildRegisteredRecipientsInput();
    this.message = sendIssueModalFacade.buildMessageTextArea(data.message);
  }

  cancel(): void {
    this.sendIssueModalFacade.reset();
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.vm$ = this.sendIssueModalFacade.vm$;
    this.sendIssueModalFacade.init(this.data);

    this.sendIssueModalFacade.successMessage$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((successMessage) => {
        if (successMessage) {
          this.cancel();
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  getTooltipMessage(registeredRecipient: ISubscriber) {
    let message = '';

    if (
      registeredRecipient.IsGroup &&
      registeredRecipient.groupMembers?.length > 0
    ) {
      for (const [
        index,
        regRec,
      ] of registeredRecipient.groupMembers.entries()) {
        if (index < 5) {
          message += `\n${regRec.FriendlyName}`;
        } else {
          message += `\n+${registeredRecipient.groupMembers.length - 5}`;
          break;
        }
      }
    }

    return message;
  }

  addRegisteredUser(event: MatChipInputEvent): void {
    if (event.value) {
      this.sendIssueModalFacade.filteredRegisteredUsers$
        .pipe(take(1))
        .subscribe((filteredUsers) => {
          // This will check if the value exist on the
          // registered user list before adding it.
          const user = filteredUsers.find(
            (user) => user.FriendlyName === event.value
          );
          if (user) {
            this.sendIssueModalFacade.addRegisteredUser(user);
            this.registeredRecipientsInput.nativeElement.value = '';
            this.registeredRecipientsInput.nativeElement.blur();
            this.registeredRecipients.setValue(null);
          }
        });
    }
  }

  selectRegisteredUser(event: MatAutocompleteSelectedEvent): void {
    if (event.option.value) {
      this.sendIssueModalFacade.addRegisteredUser(event.option.value);
      this.registeredRecipientsInput.nativeElement.value = '';
      this.registeredRecipientsInput.nativeElement.blur();
      this.registeredRecipients.setValue(null);
    }
  }

  removeRegisteredRecipient(recipient: ISubscriber): void {
    this.sendIssueModalFacade.removeRegisteredRecipient(recipient);
    this.registeredRecipientsInput.nativeElement.value = '';
    this.registeredRecipientsInput.nativeElement.blur();
    this.registeredRecipients.setValue(null);
  }

  showGroupMemebers(recipient: ISubscriber) {
    this.sendIssueModalFacade.showGroupMembers(recipient);
  }

  addNonRegisteredRecipient(event: MatChipInputEvent): void {
    if (this.nonRegisteredRecipients.valid && event.value) {
      this.sendIssueModalFacade.addNonRegisteredRecipient(event.value);
      this.nonRegisteredRecipientsInput.nativeElement.value = '';
      this.nonRegisteredRecipients.setValue(null);
    }
  }

  removeNonRegisteredRecipient(recipient: string): void {
    this.sendIssueModalFacade.removeNonRegisteredRecipient(recipient);
    this.nonRegisteredRecipientsInput.nativeElement.value = '';
    this.nonRegisteredRecipients.setValue(null);
  }

  subscribeUserChanged(value: MatCheckboxChange) {
    this.subscribeUser = value.checked;
    this.sendIssueModalFacade.setSubscribeUser(this.subscribeUser);
  }

  sendEmail() {
    this.sendIssueModalFacade.sendEmail();
  }
}
