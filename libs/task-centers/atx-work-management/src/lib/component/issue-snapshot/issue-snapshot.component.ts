/* eslint-disable rxjs/no-nested-subscribe */
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Observable, of, Subject } from 'rxjs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
  IDiscussionState,
  IDiscussionStateChange,
  ILoadDiscussion,
} from '@atonix/atx-discussion';
import {
  getAssetIdFromRoute,
  getAssetIssueIdFromRoute,
  getIssueIdFromRoute,
  IButtonData,
  NavFacade,
} from '@atonix/atx-navigation';
import { NewIssueModalComponent } from '../new-issue-modal/new-issue-modal.component';
import {
  IssueSnapshotServiceState,
  IssueSnapshotServiceFacade,
  ModalResponse,
} from '../../service/issue-snapshot.service.facade';
import { IssueSnapshotFacade } from '../../store/facade/issue-snapshot.facade';
import { debounceTime, take, takeUntil, switchMap, skip } from 'rxjs/operators';
import { UntypedFormControl } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import moment from 'moment';
import { IIssueSummaryState } from '../../model/issue-summary-state';
import { ImageViewerDialogComponent, ToastService } from '@atonix/shared/utils';
import { SendIssueDialogComponent } from '../send-issue-dialog/send-issue-dialog.component';
import { ManageFollowersDialogComponent } from '../manage-followers-dialog/manage-followers-dialog.component';
import { ImpactCalculatorFacade } from '../../service/impact-calculator/impact-calculator.facade';
import { UpdateIssueCategoryModalComponent } from '../update-issue-category-modal/update-issue-category-modal.component';
import {
  AssetIssueWithOptions,
  ISelectedExternalAssetLoading,
  IssueCauseType,
  IUser,
  IWorkRequest,
  IWorkRequestLoading,
} from '@atonix/shared/api';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import {
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  ISaveDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';
import {
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
} from '@angular/material/autocomplete';
import { DeleteIssueModalComponent } from '../delete-issue-modal/delete-issue-modal.component';
import { MatChipInputEvent } from '@angular/material/chips';
import { ITreeConfiguration, ITreeStateChange } from '@atonix/atx-asset-tree';
import { AuthFacade } from '@atonix/shared/state/auth';
import { CreateWorkOrderDialogComponent } from '../create-work-order-dialog/create-work-order-dialog.component';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { PreCwrDialogComponent } from '../pre-cwr-dialog/pre-cwr-dialog.component';

export interface DialogData {
  asset: string;
  error?: boolean;
  assetIssue?: AssetIssueWithOptions;
  selectedClass?: number;
  selectedCategory?: number;
}

export interface WorkOrderInfo {
  refId: number;
  title: string;
  status: string;
  link: string;
}

@Component({
  selector: 'atx-issue',
  templateUrl: './issue-snapshot.component.html',
  styleUrls: ['./issue-snapshot.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [IssueSnapshotServiceFacade],
})
export class IssueSnapshotComponent implements OnInit, OnDestroy {
  public vm$: Observable<IssueSnapshotServiceState>;
  public discussionState$: Observable<IDiscussionState>;
  public summaryState$: Observable<IIssueSummaryState>;
  public workRequestLoading$: Observable<IWorkRequestLoading>;
  public workOrderInfo$: Observable<IWorkRequest>;
  public workOrderInfo: IWorkRequest;
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public assetName$: Observable<string>;

  public selectedExternalAssetLoading$: Observable<ISelectedExternalAssetLoading>;

  private unsubscribe$ = new Subject<void>();
  @ViewChild('summaryContent') summaryContent: ElementRef;
  @ViewChild('searchTag') matAutocomplete: MatAutocomplete;
  @ViewChild('issueTypeInput') issueTypeInput: ElementRef<HTMLInputElement>;
  @ViewChild('issueCauseTypesInput')
  issueCauseTypesInput: ElementRef<HTMLInputElement>;

  assignedTo = new UntypedFormControl();
  resolveByDate: UntypedFormControl;
  resolveByStartMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
  resolveByEndMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();

  public breadcrumb$: Observable<IButtonData>;
  public selectedAsset$: Observable<string>;
  breadcrumb: IButtonData;
  shortSummary: UntypedFormControl;
  issueTitle: UntypedFormControl;
  issueTagQuery: UntypedFormControl;
  showCalculator = false;
  assetIssueId = null;
  issueGuid = null;
  assetID = null;
  selectedAsset = null;
  assetIssue = null;
  selectedClass = null;
  selectedCategory = null;
  issueTags: ITaggingKeyword[];
  issueType = new UntypedFormControl();
  issueCauseTypes = new UntypedFormControl();
  showAssetTreeDropdown = false;
  user: IUser;
  currencyCode: string;

  constructor(
    private issueSnapshotServiceFacade: IssueSnapshotServiceFacade,
    private issueSnapshotFacade: IssueSnapshotFacade,
    private impactCalculatorFacade: ImpactCalculatorFacade,
    private activatedRoute: ActivatedRoute,
    private navFacade: NavFacade,
    private dialog: MatDialog,
    private router: Router,
    private snackBarService: ToastService,
    private titleService: Title,
    private location: Location,
    public authFacade: AuthFacade,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.currencyCode = appConfig.currency;

    this.discussionState$ = issueSnapshotFacade.selectDiscussionState$;
    this.summaryState$ = issueSnapshotFacade.selectIssueSummaryState$;
    this.workOrderInfo$ = issueSnapshotFacade.selectWorkRequestInfo$;
    this.workRequestLoading$ = issueSnapshotFacade.workRequestLoading$;
    this.selectedExternalAssetLoading$ =
      issueSnapshotFacade.selectedExternalAssetLoading$;

    issueSnapshotFacade.selectWorkRequestInfo$.subscribe((workOrderInfo) => {
      this.workOrderInfo = workOrderInfo;
    });

    this.assetTreeConfiguration$ =
      issueSnapshotFacade.selectAssetTreeDropdownConfig$;
    this.assetName$ = issueSnapshotFacade.selectAssetName$;
    this.resolveByDate = this.issueSnapshotServiceFacade.buildResolveByDate();
    this.shortSummary = this.issueSnapshotServiceFacade.buildShortSummary();
    this.issueTitle = this.issueSnapshotServiceFacade.buildIssueTitle();
    this.issueTagQuery = this.issueSnapshotServiceFacade.buildIssueTagQuery();
    this.issueType = this.issueSnapshotServiceFacade.buildIssueTypeInput();
    this.issueCauseTypes =
      this.issueSnapshotServiceFacade.buildIssueCauseTypesInput();
    this.assignedTo =
      this.issueSnapshotServiceFacade.buildIssueAssignedToInput();

    //Trigger Opening of Create Work Dialog
    issueSnapshotFacade.selectedExternalAssetLoading$
      .pipe(skip(1), takeUntil(this.unsubscribe$))
      .subscribe((selectedExternalAssetLoading) => {
        //On successful load of associated external asset
        //Successful when: loading == false, error == null
        if (
          !selectedExternalAssetLoading.loading &&
          !selectedExternalAssetLoading.error
        ) {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.autoFocus = false;
          dialogConfig.closeOnNavigation = true;
          dialogConfig.hasBackdrop = true;
          dialogConfig.disableClose = true;
          dialogConfig.data = {
            title: this.issueTitle.value || '',
          };
          const dialogRef = this.dialog.open(
            CreateWorkOrderDialogComponent,
            dialogConfig
          );

          // dialogRef.afterClosed().subscribe((result) => {
          //   //reload discussions, disabled for now
          //   // this.onDiscussionStateChange({
          //   //   event: 'LoadDiscussion',
          //   //   newValue: {
          //   //     ShowAutogenEntries: false,
          //   //     ShowedTopic: 1,
          //   //   },
          //   // });
          // });
        }
      });

    issueSnapshotFacade.selectAssetId$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetId) => {
        if (assetId) {
          this.issueSnapshotServiceFacade.assetChange(assetId);
        }
      });

    this.issueSnapshotFacade.assetUniqueKey$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((key) => {
        if (key) {
          this.issueSnapshotServiceFacade.getRelatedIssueByAssetCount(key);
          this.issueSnapshotServiceFacade.getAlertByOwningAssetCount(key);
        }
      });

    const routeQueryParams$ = activatedRoute.queryParams
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((params) => {
        if (params['iid'] === '-1') {
          if (params['ast']) {
            this.openDialog(params['ast']);
          } else {
            this.openDialog('', true);
          }
        }
      });

    this.location.subscribe((ev) => {
      if (ev.pop) {
        if (ev.url.includes('/issues/i?iid=')) {
          this.navFacade.configureNavigationButton('asset_tree', {
            visible: false,
          });
        }
      }
    });
  }

  ngOnInit() {
    this.vm$ = this.issueSnapshotServiceFacade.query.vm$;
    const issueId = getIssueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.issueSnapshotServiceFacade.init(issueId);

    this.issueSnapshotFacade.loadWorkRequestInfo(issueId);

    this.impactCalculatorFacade.saveUpdates$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((saveUpdates) => {
        if (saveUpdates) {
          // TODO make API Call to update Impact Total and Monthly
          this.issueSnapshotServiceFacade.updateChangedBy(
            saveUpdates.AssetIssueChangedBy,
            saveUpdates.AssetIssueChangeDate,
            saveUpdates.AssetIssueID
          );
        }
      });
    this.activatedRoute.queryParamMap
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        const assetIssueId = getAssetIssueIdFromRoute(val);
        const assetId = getAssetIdFromRoute(val);
        if (assetIssueId) {
          this.impactCalculatorFacade.updateCalculator(
            +assetIssueId,
            +assetId,
            getIssueIdFromRoute(val)
          );
          this.showCalculator = true;
        } else {
          this.showCalculator = false;
        }
      });

    this.navFacade.user$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((user) => {
        this.user = user;
      });

    this.issueSnapshotFacade.selectIssueSummaryContent$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((content) => {
        if (content && content !== '') {
          this.addClickEventToImages();
        }
      });

    this.issueSnapshotServiceFacade.query.assetLoading$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetLoading) => {
        if (assetLoading === false) {
          this.vm$.pipe(take(1)).subscribe((vm) => {
            if (vm.issueWithOptions && vm.issueWithOptions.Issue) {
              if (vm.issueWithOptions.Issue.AssetIssueID !== -1) {
                //set tab title
                this.titleService.setTitle(
                  `Issue ${vm.issueWithOptions?.Issue.AssetIssueID}: ${vm.issueWithOptions?.Issue.IssueTitle}`
                );
              }
            }

            if (vm.assetIssueId && vm.assetIssueId > -1) {
              this.assetIssueId = vm.assetIssueId;

              this.authFacade.issuesManagementTaskCenterAccess$
                .pipe(take(1))
                .subscribe((rights) => {
                  let showConfigButtonsByUser = true;
                  if (!rights?.CanAdmin) {
                    if (rights?.CanView || rights.CanEdit) {
                      showConfigButtonsByUser = false;
                    }
                  }

                  this.issueSnapshotFacade.loadNewDiscussionEntry(
                    vm.assetIssueId,
                    showConfigButtonsByUser
                  );
                });

              this.issueSnapshotServiceFacade.updateImpactCosts(
                vm.assetIssueId.toString()
              );
            }
            if (vm.assetIssueId) {
              if (!vm.issueWithOptions?.Issue?.CanEdit) {
                // reactive form elements have to be disabled here
                this.shortSummary.disable();
                this.issueTitle.disable();
                this.resolveByDate.disable();
                this.assignedTo.disable();
              }
              this.assetIssueId = vm.assetIssueId;
              this.issueGuid = vm.issueWithOptions?.Issue?.GlobalID;
              this.assetID = vm.issueWithOptions?.Issue?.AssetID;

              this.issueTitle.patchValue(
                vm.issueWithOptions?.Issue?.IssueTitle,
                {
                  emitEvent: false,
                }
              );
              this.shortSummary.patchValue(
                vm.issueWithOptions?.Issue?.IssueShortSummary,
                {
                  emitEvent: false,
                }
              );
              if (vm.issueWithOptions?.Issue?.ResolveBy) {
                this.resolveByDate.patchValue(
                  vm.issueWithOptions?.Issue?.ResolveBy,
                  {
                    emitEvent: false,
                  }
                );
              }

              this.assignedTo.patchValue(
                vm.issueWithOptions?.Issue?.AssignedTo,
                { emitEvent: false }
              );

              this.issueSnapshotFacade.loadSummary(
                vm.issueWithOptions?.Issue?.GlobalID,
                vm.issueWithOptions?.Issue?.IssueSummary
              );
            }
          });
        }
      });

    this.issueSnapshotServiceFacade.query.issueWithOptions$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetIssue) => {
        this.assetIssue = assetIssue;
        this.selectedAsset =
          assetIssue?.Issue?.AssetAndParents?.Asset?.GlobalId;
        this.selectedCategory = assetIssue?.Issue?.AssetIssueCategoryTypeID;
      });

    this.issueSnapshotServiceFacade.query.selectedClass$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((selectedClass) => {
        this.selectedClass = selectedClass;
      });

    this.issueSnapshotServiceFacade.query.issueTagQuery$
      .pipe(debounceTime(100), takeUntil(this.unsubscribe$))
      .subscribe((query) => {
        if (query && query.length >= 1) {
          this.issueSnapshotServiceFacade.searchTag(query.toLowerCase());
        }
      });

    //added to handle tab title change after route change
    //when new issue is saved
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.issueSnapshotServiceFacade.query.issueWithOptions$
          .pipe(take(1))
          .subscribe((issueWithOps) => {
            if (issueWithOps) {
              //set tab title
              this.titleService.setTitle(
                `Issue ${issueWithOps?.Issue.AssetIssueID}: ${issueWithOps?.Issue.IssueTitle}`
              );
            }
          });
      }
    });
  }

  onReloadWorkRequest() {
    const issueId = getIssueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.issueSnapshotFacade.loadWorkRequestInfo(issueId, true);
  }

  onClickWRLink(event: Event) {
    event.stopImmediatePropagation();
  }

  openCreateWorkRequestDialog(): void {
    //When opening the dialog we first send a GET request to get the closest external asset
    //for this issue/issue's asset.
    //When the request is done loading and there is no error we open the dialog, else
    //No dialog opens and a toast opens.
    //This happens above in the subscription to issueSnapshotFacade.selectedExternalAssetLoading$

    //if there is already a work order created we first open an overwrite confirmation dialog first
    if (this.workOrderInfo.title) {
      const dialogRef = this.dialog.open(PreCwrDialogComponent, {
        width: '30%',
        maxWidth: '500px',
      });

      dialogRef.afterClosed().subscribe((value) => {
        if (value) {
          this.issueSnapshotFacade.selectAssetGuid$
            .pipe(take(1))
            .subscribe((assetGuid) => {
              this.issueSnapshotFacade.getAssociatedExternalAsset(assetGuid);
            });
        }
      });
    } else {
      this.issueSnapshotFacade.selectAssetGuid$
        .pipe(take(1))
        .subscribe((assetGuid) => {
          this.issueSnapshotFacade.getAssociatedExternalAsset(assetGuid);
        });
    }
  }

  openDialog(asset: string, error: boolean = false): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.disableClose = true;

    dialogConfig.data = { asset, error };
    const dialogRef = this.dialog.open(NewIssueModalComponent, dialogConfig);
    this.issueSnapshotServiceFacade.openModal();
    dialogRef.afterClosed().subscribe((result: ModalResponse) => {
      if (result) {
        this.issueSnapshotServiceFacade.newIssue(result);
        this.titleService.setTitle('New Issue');
      } else {
        self.close();
      }
    });
  }

  sendIssueDialog(): void {
    const title = this.assetIssue?.Issue?.IssueTitle ?? this.issueTitle.value;
    let message = `Issue ${title} has been updated`;

    if (this.user) {
      message += ` by ${this.user.firstName} ${this.user.lastName}`;
    }
    if (this.assetIssue?.Issue?.AssetAndParents?.Unit?.AssetDesc) {
      message += ` for unit ${this.assetIssue?.Issue?.AssetAndParents?.Unit?.AssetDesc}`;
    }

    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '250vw';
    dialogConfig.data = {
      assetIssueGlobalID: this.issueGuid,
      assetIssueID: this.assetIssueId,
      issueTitle: title,
      message,
    };
    dialogConfig.disableClose = true;

    this.dialog.open(SendIssueDialogComponent, dialogConfig);
  }

  manageFollowersDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.width = '250vw';
    dialogConfig.data = {
      issueGuid: this.issueGuid,
      assetIssueId: this.assetIssueId,
    };

    this.dialog.open(ManageFollowersDialogComponent, dialogConfig);
  }

  openUpdateDialog(asset: string, assetIssue: AssetIssueWithOptions): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.disableClose = true;
    dialogConfig.data = {
      asset,
      assetIssue,
      selectedClass: this.selectedClass,
      selectedCategory: this.selectedCategory,
    };
    const dialogRef = this.dialog.open(
      UpdateIssueCategoryModalComponent,
      dialogConfig
    );

    dialogRef.afterClosed().subscribe((result: ModalResponse) => {
      if (result) {
        this.issueSnapshotServiceFacade.updateClassAndCategory(result);
        this.snackBarService.openSnackBar(
          'Changes almost done! Simply hit Save.',
          'snapshot-banner',
          0
        );
      } else {
        dialogRef.close();
        this.issueSnapshotServiceFacade.updateModalState(false);
      }
    });
  }

  deleteIssueDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.closeOnNavigation = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.disableClose = true;

    const dialogRef = this.dialog.open(DeleteIssueModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((res: boolean) => {
      if (res) {
        this.issueSnapshotServiceFacade.deleteAssetIssue();
      }
    });
  }

  checkShouldDisableButton(): Observable<boolean> {
    const issueId = getIssueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    return this.authFacade.alpha2FeatureAccess$.pipe(
      take(1),
      switchMap((rights) => {
        if (issueId !== '-1') {
          return of(false);
        }
        return of(true);
      })
    );
  }

  checkShouldShowButton(): Observable<boolean> {
    //TODO change to the authFacade prop needed, using alpha2 role for testing
    const issueId = getIssueIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    return this.authFacade.alpha2FeatureAccess$.pipe(
      take(1),
      switchMap((rights) => {
        //TODO change to check for the correct permissions
        if (rights.CanView) {
          return of(true);
        }
        return of(false);
      })
    );
  }

  categoryChange(event: MatSelectChange) {
    this.issueSnapshotServiceFacade.categoryChange(+event.value);
  }

  activityStatusChange(event: MatSelectChange) {
    this.issueSnapshotServiceFacade.activityStatusChange(+event.value);
  }

  resolutionStatusChange(event: MatSelectChange) {
    this.issueSnapshotServiceFacade.resolutionStatusChange(+event.value);
  }

  priorityChange(event: MatSelectChange) {
    this.issueSnapshotServiceFacade.priorityChange(+event.value);
  }

  addIssueType(event: MatChipInputEvent) {
    if (event.value) {
      this.issueSnapshotServiceFacade.addIssueType(event.value);
      this.issueTypeInput.nativeElement.value = '';
      this.issueTypeInput.nativeElement.blur();
      this.issueType.setValue(null);
    }
  }

  removeIssueType() {
    this.issueSnapshotServiceFacade.removeIssueType();
  }

  issueTypeChange(event: MatAutocompleteSelectedEvent) {
    this.issueSnapshotServiceFacade.issueTypeChange(event.option.value);
    this.issueTypeInput.nativeElement.value = '';
    this.issueTypeInput.nativeElement.blur();
    this.issueType.setValue(null);
  }

  addPotentialCause(event: MatChipInputEvent) {
    if (event.value) {
      this.issueSnapshotServiceFacade.addIssueCauseType(event.value);
      this.issueCauseTypesInput.nativeElement.value = '';
      this.issueCauseTypesInput.nativeElement.blur();
      this.issueCauseTypes.setValue(null);
    }
  }

  removePotentialCause(causeType: IssueCauseType) {
    this.issueSnapshotServiceFacade.removeIssueCauseType(
      causeType.IssueCauseTypeID
    );
  }

  issueCauseTypeChange(event: MatAutocompleteSelectedEvent) {
    this.issueSnapshotServiceFacade.issueCauseTypeChange(event.option.value);
    this.issueCauseTypesInput.nativeElement.value = '';
    this.issueCauseTypesInput.nativeElement.blur();
    this.issueCauseTypes.setValue(null);
  }

  isScorecardChange(event: MatButtonToggleChange) {
    this.issueSnapshotServiceFacade.isScoreCardChange(event.source.checked);
  }

  save() {
    this.snackBarService.dismiss();
    this.issueSnapshotServiceFacade.save();
  }

  follow(isSubscribed: boolean) {
    if (isSubscribed) {
      this.issueSnapshotServiceFacade.unsubscribeUser();
    } else {
      this.issueSnapshotServiceFacade.subscribeUser();
    }
  }

  send() {
    this.issueSnapshotServiceFacade.send();
  }

  changeCategoryAndClass() {
    this.openUpdateDialog(this.selectedAsset, this.assetIssue);
  }

  deleteSnapshot() {
    this.deleteIssueDialog();
  }

  cancel() {
    self.close();
  }

  // Discussion
  onDiscussionStateChange(change: IDiscussionStateChange) {
    if (change.event === 'LoadDiscussion') {
      const val = change.newValue as ILoadDiscussion;

      this.authFacade.issuesManagementTaskCenterAccess$
        .pipe(take(1))
        .subscribe((rights) => {
          let showConfigButtonsByUser = true;
          if (!rights?.CanAdmin) {
            if (rights?.CanView || rights.CanEdit) {
              showConfigButtonsByUser = false;
            }
          }

          this.issueSnapshotFacade.loadDiscussion(
            this.assetIssueId,
            val.ShowAutogenEntries,
            showConfigButtonsByUser
          );
        });
    } else if (change.event === 'SaveDiscussionEntry') {
      const saveDiscussionEntry = change.newValue as ISaveDiscussionEntry;
      this.authFacade.issuesManagementDiscussionAccess$
        .pipe(take(1))
        .subscribe((rights) => {
          if (rights?.CanAdmin) {
            saveDiscussionEntry.NotifySubscribers = false;
          } else {
            saveDiscussionEntry.NotifySubscribers = true;
          }
        });

      this.issueSnapshotFacade.saveDiscussionEntry(saveDiscussionEntry);
    } else if (change.event === 'CancelDiscussionEntry') {
      this.issueSnapshotFacade.cancelDiscussionEntry(change.newValue as string);
    } else if (change.event === 'EditDiscussionEntry') {
      this.issueSnapshotFacade.editDiscussionEntry(
        change.newValue as IDiscussionEntry
      );
    } else if (change.event === 'DeleteDiscussionEntry') {
      this.issueSnapshotFacade.deleteDiscussionEntry(change.newValue as string);
    } else if (change.event === 'SearchTaggingKeyword') {
      this.issueSnapshotFacade.searchTaggingKeyword(
        change.newValue as IDiscussionEntryKeyword
      );
    } else if (change.event === 'AddDiscussionEntryKeyword') {
      this.issueSnapshotFacade.addDiscussionEntryKeyword(
        change.newValue as IDiscussionEntryKeyword
      );
    } else if (change.event === 'AddDiscussionEntryAttachments') {
      this.issueSnapshotFacade.addDiscussionEntryAttachments(
        change.newValue as IAddDiscussionEntryAttachment[]
      );
    } else if (change.event === 'DeleteDiscussionEntryAttachment') {
      this.issueSnapshotFacade.deleteDiscussionEntryAttachment(
        change.newValue as IDeleteDiscussionEntryAttachment
      );
    } else if (change.event === 'DeleteDiscussionEntryKeyword') {
      this.issueSnapshotFacade.deleteDiscussionEntryKeyword(
        change.newValue as IDeleteDiscussionEntryKeyword
      );
    } else if (change.event === 'ShowMoreEntries') {
      this.issueSnapshotFacade.showMoreEntries();
    }
  }

  // Summary
  addClickEventToImages() {
    setTimeout(() => {
      const images = this.summaryContent?.nativeElement.querySelectorAll('img');
      const imageSources = [];
      images?.forEach((image) => {
        imageSources.push(image.getAttribute('src') || null);
      });

      images?.forEach((image, index) => {
        image.addEventListener('click', (e) => {
          const dialogConfig = new MatDialogConfig();
          dialogConfig.panelClass = 'custom-dialog-image-viewer';
          dialogConfig.data = {
            backdropClass: 'backdropBackground',
            imageSources,
            selectedIndex: index,
          };
          const dialogRef = this.dialog.open(
            ImageViewerDialogComponent,
            dialogConfig
          );
        });
      });
    }, 0);
  }

  editSummary() {
    this.issueSnapshotFacade.editSummary();
  }

  saveSummary(summaryContent: string) {
    this.issueSnapshotServiceFacade.saveSummary(summaryContent);

    if (this.issueGuid !== '00000000-0000-0000-0000-000000000000') {
      this.issueSnapshotFacade.saveSummary(summaryContent);
    }
  }

  cancelEditSummary() {
    this.issueSnapshotFacade.cancelEditSummary();

    if (this.issueGuid === '00000000-0000-0000-0000-000000000000') {
      this.issueSnapshotServiceFacade.saveSummary('');
    }

    this.addClickEventToImages();
  }

  uploadSummaryFile(file: any) {
    this.issueSnapshotFacade.uploadSummaryFile(this.assetID, file);
  }

  openImpactCalculator(assetIssueId: number, assetId: number) {
    this.router.navigate([], {
      queryParams: { aiid: assetIssueId, aid: assetId },
      queryParamsHandling: 'merge',
      replaceUrl: true,
    });
  }

  addTag(): void {
    if (this.issueTagQuery.value && this.issueTagQuery.value !== '') {
      this.issueSnapshotServiceFacade.query.filteredTags$
        .pipe(take(1))
        .subscribe((filteredTags) => {
          const tagQuery = this.issueTagQuery.value as string;

          if (filteredTags && tagQuery) {
            const existingTag = filteredTags.find(
              (x) => x.Text.toLowerCase() === tagQuery.toLowerCase()
            );
            if (existingTag && existingTag.KeywordId > 0) {
              this.issueSnapshotServiceFacade.selectTag(existingTag);
            } else {
              const tag: ITaggingKeyword = {
                KeywordId: -1,
                Text: this.issueTagQuery.value,
                ClientId: 0,
                BackDated: false,
                BackDate: null,
                CreatedByUserID: null,
                DateTime: null,
                IsTheOwner: false,
              };

              this.issueSnapshotServiceFacade.selectTag(tag);
            }
          }

          this.issueTagQuery.setValue(null);
        });
    }
  }

  removeTag(tag: ITaggingKeyword): void {
    this.issueSnapshotServiceFacade.removeTag(tag);
  }

  selectTag(event: MatAutocompleteSelectedEvent): void {
    this.issueSnapshotServiceFacade.selectTag(event.option.value);
    this.issueTagQuery.setValue(null);
  }

  editAsset(): void {
    this.showAssetTreeDropdown = !this.showAssetTreeDropdown;
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.issueSnapshotFacade.assetTreeDropdownStateChange(change);
  }

  closeAssetTreeDropdown() {
    this.showAssetTreeDropdown = false;
  }

  showRelatedIssuesByAsset() {
    this.issueSnapshotFacade.assetUniqueKey$.pipe(take(1)).subscribe((key) => {
      this.issueSnapshotFacade.showRelatedIssuesByAsset(key);
    });
  }

  showRelatedAlertsByAsset() {
    this.issueSnapshotFacade.assetUniqueKey$.pipe(take(1)).subscribe((key) => {
      this.issueSnapshotFacade.showRelatedAlertsByAsset(key);
    });
  }

  issueTypeClick() {
    this.issueTypeInput.nativeElement.focus();
  }

  issueCauseTypesClick() {
    this.issueCauseTypesInput.nativeElement.focus();
  }

  userSelected(event: MatAutocompleteSelectedEvent) {
    this.issueSnapshotServiceFacade.assignedToChange(event.option.value);
  }

  assignedToFocusOut() {
    const value = this.assignedTo.value;
    this.issueSnapshotServiceFacade.query.users$
      .pipe(take(1))
      .subscribe((users) => {
        if (value) {
          const userIsValid = users.some((x) => x.EmailAddress === value);
          if (!userIsValid) {
            if (this.assignedTo.valid) {
              this.assignedTo.setErrors({ invalidUser: true });
            }
            return;
          }
          this.issueSnapshotServiceFacade.assignedToChange(value);
        }
      });
  }

  hasAccessRestriction(): boolean {
    let hasAccessRestriction = true;
    this.authFacade.issuesManagementTaskCenterAccess$
      .pipe(take(1))
      .subscribe((rights) => {
        if (rights?.CanAdmin || rights?.CanEdit) {
          hasAccessRestriction = false;
        }
      });
    return hasAccessRestriction;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.issueSnapshotFacade.clearDiscusson();
    this.navFacade.configureLayoutButton('nNavbar', { visible: true });
    this.navFacade.toggleNavigation('open');
    this.snackBarService.dismiss();
    this.impactCalculatorFacade.reset();
    this.issueSnapshotFacade.clearAssetTreeDropdown();
  }
}
