import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import {
  IssueModalFacade,
  IssueModalState,
} from '../../service/issue-modal.facade';
import { ModalResponse } from '../../service/issue-snapshot.service.facade';
import { DialogData } from '../issue-snapshot/issue-snapshot.component';

@Component({
  selector: 'atx-issue-modal',
  templateUrl: './new-issue-modal.component.html',
  styleUrls: ['./new-issue-modal.component.scss'],
})
export class NewIssueModalComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject<void>();
  vm$: Observable<IssueModalState>;
  faSpinner = faSpinner;
  constructor(
    public dialogRef: MatDialogRef<NewIssueModalComponent>,
    public modalFacade: IssueModalFacade,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  categoryChange(event: MatSelectChange) {
    this.modalFacade.categoryChange(+event.value);
  }
  classChange(event: MatSelectChange) {
    this.modalFacade.classChange(+event.value);
  }
  newIssue() {
    this.modalFacade.createIssue();
  }
  ngOnInit() {
    this.vm$ = this.modalFacade.vm$;
    this.modalFacade.init(this.data);
    this.modalFacade.newIssue$
      .pipe(takeUntil(this.onDestroy))
      .subscribe((newIssue) => {
        if (newIssue) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.vm$.pipe(take(1)).subscribe((vm) => {
            const modalIssue: ModalResponse = {
              newIssue,
              selectedCategory: vm.selectedCategory,
              selectedClass: vm.selectedClass,
              issueClasses: vm.issueClasses,
              issueCategories: vm.issueCategories,
            };
            this.dialogRef.close(modalIssue);
          });
        }
      });
  }

  ngOnDestroy() {
    this.modalFacade.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
