import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NavFacade } from '@atonix/atx-navigation';
import { Observable, Subject, takeUntil, take } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatSelectChange } from '@angular/material/select';
import { FormControl } from '@angular/forms';
import { SendIssueModalState } from '../../service/send-issue-modal.facade';
import { ISubscriber } from '@atonix/shared/api';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Title } from '@angular/platform-browser';
import { MobileIssueSnapshotFacade } from '../../service/issue-snapshot-mobile/issue-snapshot-mobile.facade';
import { MobileIssueSnapshotState } from '../../service/issue-snapshot-mobile/issue-snapshot-mobile.query';
import { MobileIssueFileAttachment } from '../../service/issue-snapshot-mobile/issue-snapshot-mobile.models';
import { replaceHTMLFileElementToBVFiles } from '../../service/utilities';
import { EventBusService, Events } from '../../service/event-bus.service';

@Component({
  selector: 'atx-issue-snapshot-mobile',
  templateUrl: './issue-snapshot-mobile.component.html',
  styleUrls: ['./issue-snapshot-mobile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IssueSnapshotMobileComponent implements OnDestroy, OnInit {
  @ViewChild('registeredRecipientsInput')
  registeredRecipientsInput: ElementRef<HTMLInputElement>;

  public newIssueVm$: Observable<MobileIssueSnapshotState>;

  sendIssueVm$: Observable<SendIssueModalState>;
  issueTitle: FormControl<string | null>;
  details: FormControl<string | null>;
  registeredRecipients: FormControl<string | null>;

  private unsubscribe$ = new Subject<void>();
  private CONTENT_LIMIT = 10000000; // 10MB
  private recentUploadedFile: File = null;
  constructor(
    private navFacade: NavFacade,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private mobileIssueSnapshotFacade: MobileIssueSnapshotFacade,
    private eventbus: EventBusService
  ) {
    this.navFacade.configureLayoutButton('nNavbar', { visible: false });
    this.navFacade.toggleNavigation('hidden');

    this.issueTitle = this.mobileIssueSnapshotFacade.buildIssueTitle();
    this.details = this.mobileIssueSnapshotFacade.buildShortSummary();
    this.registeredRecipients =
      this.mobileIssueSnapshotFacade.buildRegisteredRecipientsInput();

    this.eventbus.on(Events.UploadSummaryFileComplete, (element) => {
      const exeedLimit =
        typeof element === 'number'
          ? element > this.CONTENT_LIMIT
          : element.length > this.CONTENT_LIMIT;
      if (exeedLimit) {
        this.mobileIssueSnapshotFacade.command.setErrorMessage(
          'Unable to save. Message body exceeds 10MB limit. Please reduce large items or add as attachments.'
        );
        return;
      }

      this.mobileIssueSnapshotFacade.command.setLoading(false);
      this.mobileIssueSnapshotFacade.command.addFileAttachment({
        htmlElement: element,
        file: this.recentUploadedFile,
      });
      this.recentUploadedFile = null;
    });

    this.eventbus.on(Events.UploadSummaryFileFailure, (element) => {
      this.mobileIssueSnapshotFacade.command.setLoading(false);
    });

    this.eventbus.on(
      Events.UploadDiscussionAttachmentsComplete,
      (attachments) => {
        this.mobileIssueSnapshotFacade.query.discussionEntry$
          .pipe(take(1))
          .subscribe((entry) => {
            if (entry) {
              const newEntry = { ...entry };
              newEntry.Attachments = attachments;
              this.mobileIssueSnapshotFacade.saveMobileDiscussionEntry(
                newEntry
              );
              this.mobileIssueSnapshotFacade.command.setIssueCreationSuccess();
            }
          });
      }
    );

    this.eventbus.on(Events.UploadDiscussionAttachmentsFailure, (element) => {
      this.mobileIssueSnapshotFacade.command.setLoading(false);
    });
  }
  ngOnInit(): void {
    this.titleService.setTitle('New Issue');
    this.newIssueVm$ = this.mobileIssueSnapshotFacade.query.vm$;
    this.sendIssueVm$ = this.mobileIssueSnapshotFacade.sendIssueVm$;

    this.activatedRoute.queryParams
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((params) => {
        const assetId = params['ast'];
        const iid = params['iid'];
        this.mobileIssueSnapshotFacade.loadIssueClassesAndCategories({
          assetId,
          assetIssueGuid: iid,
        });
      });

    this.mobileIssueSnapshotFacade.query.assetIssueWithOptions$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((assetIssueWithOptions) => {
        if (assetIssueWithOptions && assetIssueWithOptions.Issue) {
          const assetIssue = assetIssueWithOptions?.Issue;
          if (assetIssue.AssetIssueID !== -1) {
            this.titleService.setTitle(
              `Issue ${assetIssue?.AssetIssueID}: ${assetIssue?.IssueTitle}`
            );
            this.issueTitle.patchValue(assetIssue?.IssueTitle);
            this.details.patchValue(assetIssue?.IssueShortSummary);
          }
        }
      });
  }
  ngOnDestroy(): void {
    this.mobileIssueSnapshotFacade.ngOnDestroy();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  categoryChange(event: MatSelectChange) {
    this.mobileIssueSnapshotFacade.command.setCategory(+event.value);
  }

  classChange(event: MatSelectChange) {
    this.mobileIssueSnapshotFacade.command.setClass(+event.value);
  }

  saveIssue() {
    this.mobileIssueSnapshotFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue = vm.assetIssueWithOptions?.Issue;
      const fileElements = vm.fileAttachments.map(
        (attachment) => attachment.htmlElement
      );
      const issueSummary =
        assetIssue.IssueSummary + ' ' + fileElements.join(' ');
      const newAssetIssue = {
        ...assetIssue,
        IssueSummary: replaceHTMLFileElementToBVFiles(issueSummary),
      };

      assetIssue;
      this.mobileIssueSnapshotFacade.saveIssue({
        assetIssue: newAssetIssue,
        assetIssueWithOptions: vm.assetIssueWithOptions,
        assetId: vm.assetId,
      });
    });
  }

  removeRegisteredRecipient(recipient: ISubscriber): void {
    this.mobileIssueSnapshotFacade.removeRegisteredRecipient(recipient);
    this.registeredRecipientsInput.nativeElement.value = '';
    this.registeredRecipientsInput.nativeElement.blur();
    this.registeredRecipients.setValue(null);
  }

  showGroupMembers(recipient: ISubscriber) {
    this.mobileIssueSnapshotFacade.showGroupMembers(recipient);
  }

  addRegisteredUser(event: MatChipInputEvent): void {
    if (event.value) {
      this.mobileIssueSnapshotFacade.filteredRegisteredUsers$
        .pipe(take(1))
        .subscribe((filteredUsers) => {
          const user = filteredUsers.find(
            (user) => user.FriendlyName === event.value
          );
          if (user) {
            this.mobileIssueSnapshotFacade.addRegisteredUser(user);
            this.registeredRecipientsInput.nativeElement.value = '';
            this.registeredRecipientsInput.nativeElement.blur();
            this.registeredRecipients.setValue(null);
          }
        });
    }
  }

  selectRegisteredUser(event: MatAutocompleteSelectedEvent): void {
    if (event.option.value) {
      this.mobileIssueSnapshotFacade.addRegisteredUser(event.option.value);
      this.registeredRecipientsInput.nativeElement.value = '';
      this.registeredRecipientsInput.nativeElement.blur();
      this.registeredRecipients.setValue(null);
    }
  }

  sendEmail() {
    this.mobileIssueSnapshotFacade.sendEmail();
    this.mobileIssueSnapshotFacade.command.resetToIssue();
  }

  public uploadFile(fileList: any) {
    const file = fileList[0];
    if (file) {
      this.recentUploadedFile = file;
      this.mobileIssueSnapshotFacade.uploadFile(file);
    }
  }

  public deleteFile(file: MobileIssueFileAttachment) {
    this.mobileIssueSnapshotFacade.command.removeFileAttachment(file);
  }
}
