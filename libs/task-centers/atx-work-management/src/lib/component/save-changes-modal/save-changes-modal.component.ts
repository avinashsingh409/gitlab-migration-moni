import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../issue-snapshot/issue-snapshot.component';

@Component({
  selector: 'atx-save-changes-modal',
  templateUrl: './save-changes-modal.component.html',
  styleUrls: ['./save-changes-modal.component.scss'],
})
export class SaveChangesModalComponent {
  constructor(
    public dialogRef: MatDialogRef<SaveChangesModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}
}
