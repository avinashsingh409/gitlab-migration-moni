import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-mf-prompt-dialog',
  templateUrl: './mf-prompt-dialog.component.html',
  styleUrls: ['./mf-prompt-dialog.component.scss'],
})
export class MfPromptDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<MfPromptDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  closeManageFollower(): void {
    this.dialogRef.close('yes');
  }

  ok(): void {
    if (this.data?.closeOnOk) {
      this.dialogRef.close('yes');
    } else {
      this.dialogRef.close();
    }
  }
}
