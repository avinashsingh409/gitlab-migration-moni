import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IIssueSubscriber, ISubscriber } from '@atonix/shared/api';
import { Observable, Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import {
  ManageFollowersModalFacade,
  ManageSubscribersModalState,
} from '../../service/manage-followers-modal.facade';
import { MfPromptDialogComponent } from './mf-prompt-dialog/mf-prompt-dialog/mf-prompt-dialog.component';

@Component({
  selector: 'atx-manage-followers-dialog',
  templateUrl: './manage-followers-dialog.component.html',
  styleUrls: ['./manage-followers-dialog.component.scss'],
})
export class ManageFollowersDialogComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('userSelectionInput')
  userSelectionInput: ElementRef<HTMLInputElement>;
  public vm$: Observable<ManageSubscribersModalState>;
  public onDestroy = new Subject<void>();
  selectedUsers: ISubscriber[] = [];
  displayedColumns: string[] = [
    'FriendlyName',
    'Email',
    'UserGroups',
    'IsSubscribed',
  ];
  dataSource = new MatTableDataSource<IIssueSubscriber>();
  selectedUsersControl = new UntypedFormControl();
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ManageFollowersDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private manageFollowerDialogFacade: ManageFollowersModalFacade
  ) {
    this.selectedUsersControl =
      manageFollowerDialogFacade.buildSelectedUsersControl();
  }
  ngAfterViewInit(): void {
    //Disable clear state 'asc' | 'desc' | ''
    // this.sort.disableClear = true;
    this.vm$.pipe(takeUntil(this.onDestroy)).subscribe((vm) => {
      this.dataSource.data = vm.issueSubscribers;
      this.dataSource.sortingDataAccessor = (data, header) => {
        switch (header) {
          case 'Email':
          case 'FriendlyName': {
            return data[header].toLocaleLowerCase();
          }
          default: {
            return data[header];
          }
        }
      };
      this.dataSource.sort = this.sort;
    });
  }
  ngOnInit(): void {
    this.vm$ = this.manageFollowerDialogFacade.vm$;
    this.manageFollowerDialogFacade.init(
      this.data.issueGuid,
      this.data.assetIssueId
    );
  }
  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
    this.manageFollowerDialogFacade.ngOnDestroy();
  }

  onNoClick(): void {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (!vm.pristine) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.width = '250px';
        dialogConfig.data = {
          message:
            'This will clear unsaved changes, are you sure you want to cancel?',
          isWarn: false,
        };

        const promptDialogRef = this.dialog.open(
          MfPromptDialogComponent,
          dialogConfig
        );
        // eslint-disable-next-line rxjs/no-nested-subscribe
        promptDialogRef.afterClosed().subscribe((res) => {
          if (res === 'yes') {
            this.dialogRef.close();
          }
        });
      } else {
        this.dialogRef.close();
      }
    });
  }

  updateSubscriptionStatus(user: IIssueSubscriber): void {
    this.manageFollowerDialogFacade.updateSubscriptionStatus(
      user.SecurityUserId,
      user.IsSubscribed === true ? false : true
    );
  }

  addUser(event: MatChipInputEvent): void {
    this.userSelectionInput.nativeElement.value = '';
    this.userSelectionInput.nativeElement.focus();
  }

  selectUser(event: MatAutocompleteSelectedEvent): void {
    const user = event.option.value as ISubscriber;
    if (this.selectedUsers.includes(user)) {
      return;
    }
    this.selectedUsers.push(event.option.value);
    this.userSelectionInput.nativeElement.value = '';
    this.userSelectionInput.nativeElement.focus();
  }

  removeUser(user: ISubscriber): void {
    this.selectedUsers.splice(this.selectedUsers.indexOf(user), 1);
  }

  addUsers(): void {
    if (this.selectedUsers.length > 0) {
      const users = this.selectedUsers.filter((user) => !user.IsGroup) ?? [];
      const groups = this.selectedUsers.filter((user) => user.IsGroup) ?? [];

      if (users.length > 0) {
        this.manageFollowerDialogFacade.subscribeUsers(users);
      }

      if (groups.length > 0) {
        this.manageFollowerDialogFacade.subscribeGroups(
          groups,
          this.data.issueGuid
        );
      }
      this.userSelectionInput.nativeElement.value = '';
      this.userSelectionInput.nativeElement.blur();
      this.selectedUsers = [];
    }
  }

  removeAllFollowers(): void {
    this.manageFollowerDialogFacade.unsubscribeAll();
  }

  save(close: boolean): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';

    this.manageFollowerDialogFacade
      .saveChanges(this.data.assetIssueId)
      .subscribe(
        (res) => {
          dialogConfig.data = {
            message: 'Your changes to the issue followers have been saved.',
            isWarn: true,
            closeOnOk: close,
          };

          this.manageFollowerDialogFacade.updateSaveState(false);

          if (!close) {
            this.manageFollowerDialogFacade.reset();
            this.manageFollowerDialogFacade.init(
              this.data.issueGuid,
              this.data.assetIssueId
            );
          }

          const promptDialogRef = this.dialog.open(
            MfPromptDialogComponent,
            dialogConfig
          );
          // eslint-disable-next-line rxjs/no-nested-subscribe
          promptDialogRef.afterClosed().subscribe((res) => {
            if (close) {
              this.dialogRef.close();
            }
          });
        },
        // eslint-disable-next-line rxjs/no-implicit-any-catch
        (err) => {
          const error = err.error;
          console.log(error);

          this.manageFollowerDialogFacade.updateSaveState(false);

          const errorMessage = `Unable to complete request. Service Error ${error?.StatusCode}.`;
          dialogConfig.data = {
            message: errorMessage,
            isWarn: true,
          };

          const promptDialogRef = this.dialog.open(
            MfPromptDialogComponent,
            dialogConfig
          );
        }
      );
  }

  isPristine(): Observable<boolean> {
    return this.vm$.pipe(
      take(1),
      map((vm) => vm.pristine)
    );
  }
}
