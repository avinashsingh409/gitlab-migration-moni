export interface IAssetIssueImpactType {
  AssetIssueImpactTypeID: number;
  AssetIssueImpactCategoryTypeID: number;
  AssetIssueImpactTypeDesc: string;
  AssetIssueImpactCategoryTypeDesc: string;
  AssetIssueImpactCategoryTypeCalcDesc: string;
  AssetIssueImpactCategoryAndTypeDesc: string;
  Units: string;
  DisplayOrder: number;
}
