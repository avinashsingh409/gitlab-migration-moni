import { IAssetIssueImpactType } from './asset-issue-impact-type';

export interface ImpactCost {
  AssetIssueImpactType: IAssetIssueImpactType;
  Impact: number;
  Stoplight: number;
}
