export interface IIssueSummaryState {
  IssueGuid: string;
  IssueSummary: string;
  EditingSummary: boolean;
  saving: boolean;
}

export function getDefaultSummary(params?: Partial<IIssueSummaryState>) {
  const result: IIssueSummaryState = {
    ...{
      IssueGuid: null,
      IssueSummary: null,
      EditingSummary: false,
      saving: false,
    },
    ...params,
  };

  return result;
}
