import { CalculationFactors } from '@atonix/atx-core';
import {
  EfficiencyCalculations,
  EfficiencyCalculationsConstructor,
} from './efficiency-calculations';
import { CalculationAttributes } from './scenario';

export interface EfficiencyImpactContent {
  version: number;
  attributes: CalculationAttributes;
  totalCost: {
    toDate: number;
    potentialFuture: number;
    totalAvg: number;
  };
  impactQuantity: number;
  calculationFactorID: number;
  overrideTotal: string;
  errors: string[] | undefined;
}

export class EfficiencyCalculationsV2 extends EfficiencyCalculations {
  productionTimeToDays: number;
  consumptionToCostRate: number;
  productionToCostRate: number;

  constructor(ecc: EfficiencyCalculationsConstructor) {
    super(ecc);
    this.version = 2;
    this.productionTimeToDays = +ecc.attributes['productionTimeToDays'];
    this.consumptionToCostRate = +ecc.attributes['consumptionToCostRate'];
    this.productionToCostRate = +ecc.attributes['productionToCostRate'];
  }

  efficiencyCalculatedCost(
    impactQuantity: number,
    fuelCost: number,
    capacityFactor: number,
    unitFullLoad: number,
    netUnitHeatRate: number,
    daysElapsedOrPending: number
  ) {
    return (
      (((daysElapsedOrPending * this.productionTimeToDays) / 1000) *
        capacityFactor *
        fuelCost *
        impactQuantity *
        unitFullLoad *
        netUnitHeatRate) /
      100
    );
  }

  calculateDerate(calcFactors: CalculationFactors) {
    if (!calcFactors) {
      this.consumptionRateImpact = 0;
      this.costCalculation = {
        elapsed: 0,
        total: 0,
        pending: 0,
      };
      return;
    }
    this.consumptionRateImpact =
      Math.round(
        ((this.impactQuantity * calcFactors.CalculationFactor) /
          calcFactors.CalculationFactorRatio) *
          1000
      ) / 1000;

    // consumptionRateVariance => 2n = calc: if(2m<>0,2m*2l/100,2l)
    this.consumptionRateVariance =
      this.consumptionRateValue === 0
        ? this.consumptionRateImpact
        : (this.consumptionRateImpact / 100) * this.consumptionRateValue;

    if (this.consumptionToCostRate === 0) {
      this.costCalculation.elapsed = 0.0;
      this.costCalculation.pending = 0.0;
    } else {
      this.costCalculation.elapsed =
        this.consumptionRateVariance *
        this.consumableCost *
        (this.productionToCostRate / this.consumptionToCostRate) *
        this.capacityUtilization *
        this.productionCapacity *
        this.productionTimeToDays *
        this.daysElapsed;
      this.costCalculation.pending =
        this.consumptionRateVariance *
        this.consumableCost *
        (this.productionToCostRate / this.consumptionToCostRate) *
        this.capacityUtilization *
        this.productionCapacity *
        this.productionTimeToDays *
        this.daysPending;
    }

    this.costCalculation.total =
      this.costCalculation.elapsed + this.costCalculation.pending;
  }
}
