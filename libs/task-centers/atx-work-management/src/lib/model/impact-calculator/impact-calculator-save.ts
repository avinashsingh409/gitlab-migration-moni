export interface ImpactCalculatorSaveReturn {
  Results: CalculatorResults[];
  Count: number;
}

export interface CalculatorResults {
  ImpactScenarioIDsSaved?: number[];
  ImpactScenarioIDsDeleted?: number[];
  AssetIssueChangeDate: Date;
  AssetIssueChangedBy: string;
}

export interface SaveUpdates {
  AssetIssueChangedBy: string;
  AssetIssueChangeDate: Date;
  AssetIssueID: string;
}

export interface CalculatorTotals {
  TotalImpactCost: number;
  TotalImpactCostMonthly: number;
}

export interface ImpactCalculatorTotals {
  Results: CalculatorTotals[];
  Count: number;
}
