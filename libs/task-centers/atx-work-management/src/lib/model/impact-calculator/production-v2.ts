import { CalculationFactors, CalculationType } from '@atonix/atx-core';
import {
  ProductionCalculations,
  ProductionCalculationsConstructor,
} from './production-calculations';
import { CalculationAttributes } from './scenario';

export interface ProductionImpactContent {
  version: number;
  attributes: CalculationAttributes;
  totalCost: {
    toDate: number;
    potentialFuture: number;
    totalAvg: number;
  };
  impactQuantity: number;
  calculationFactorID: number;
  derate: {
    hasDerateOverride: boolean;
    derateOverrideValue: number;
    percentTimeDerateInEffect: {
      toDate: number;
      potentialFuture: number;
      totalAvg: number;
    };
  };
  overrideTotal: string;
  relevantProductionCost: number;
  lostMarginOpportunity: number;
  errors: string[];
}

export class ProductionCalculationsV2 extends ProductionCalculations {
  productionTimeToDays: number;

  constructor(pcc: ProductionCalculationsConstructor) {
    super(pcc);
    this.version = 2;
    if (pcc.attributes['productionTimeToDays']) {
      this.productionTimeToDays = +pcc.attributes['productionTimeToDays'];
    } else {
      this.productionTimeToDays = 0;
    }
  }

  calculateDerate(calcFacors: CalculationFactors) {
    this.calculationFactors = calcFacors;
    this.unitDeratePercent =
      (this.impactQuantity * calcFacors?.CalculationFactor) /
      calcFacors?.CalculationFactorRatio;
    this.unitDerate =
      Math.round(
        (this.unitDeratePercent / 100) * this.productionCapacity * 100
      ) / 100;

    if (this.hasDerateOverride) {
      this.productionLoss.elapsed =
        this.unitDerate *
        this.daysElapsed *
        this.productionTimeToDays *
        (this.derateOverride / 100);
      this.productionLoss.pending =
        this.unitDerate *
        this.daysPending *
        this.productionTimeToDays *
        (this.derateOverride / 100);
    } else {
      this.productionLoss.elapsed =
        this.unitDerate *
        this.daysElapsed *
        this.productionTimeToDays *
        (this.derateElapsed / 100);
      this.productionLoss.pending =
        this.unitDerate *
        this.daysPending *
        this.productionTimeToDays *
        (this.deratePending / 100);
    }
    this.productionLoss.total =
      this.productionLoss.pending + this.productionLoss.elapsed;
    this.costCalculation.elapsed =
      this.productionLoss.elapsed *
      (this.lostMarginOpportunity - this.relevantProductionCost);
    this.costCalculation.pending =
      this.productionLoss.pending *
      (this.lostMarginOpportunity - this.relevantProductionCost);

    const totalCost =
      this.costCalculation.elapsed + this.costCalculation.pending;
    this.costCalculation.total = totalCost;
  }
}
