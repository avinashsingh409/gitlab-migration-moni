import {
  CalculationFactors,
  CalculationType,
  ImpactDateRange,
  TimeValues,
} from '@atonix/atx-core';
import { CostPeriods } from './calculation-parameters';
import { CalculationAttributes, CalculatorCategory } from './scenario';

export interface ProductionErrors {
  productionCapacity: boolean;
  productionTimeToDays: boolean;
  productionRevenue: boolean;
}

export function createProductionErrors(errors: string[]): ProductionErrors {
  return {
    productionCapacity: errors.includes('ImpactCapacityValue') ? true : false,
    productionTimeToDays: errors.includes('ImpactTimeToDaysConversion')
      ? true
      : false,
    productionRevenue: errors.includes('ImpactProductionRevenueValue')
      ? true
      : false,
  };
}
export interface GenerationParameters {
  version: number;
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  overrideTotal: CalculationType;
  impactCost: number;
  impactCostMonthly: number;
  calculationFactorID: number;
  impactQuantity: number;
  impactRange: ImpactDateRange;
  percentTimeDerateInEffect: TimeValues;
  hasDerateOverride: boolean;
  derateOverrideValue: number;
  totalCost: TimeValues;
  relevantProductionCost: number;
  lostMarginOpportunity: number;
  attributes: CalculationAttributes;
  errors: string[];
  productionErrors: ProductionErrors;
}

export interface ProductionCalculationsConstructor {
  calculatorCategory: CalculatorCategory;
  impactQuantity: number;
  calculationType: CalculationType;
  productionLoss: CostPeriods;
  daysElapsed: number;
  dateElapsed: Date;
  currentDate: Date;
  calculationFactorID: number;
  daysPending: number;
  datePending: Date;
  derateElapsed: number;
  deratePending: number;
  hasDerateOverride: boolean;
  derateOverride: number;
  costCalculation: CostPeriods;
  calculationFactors: CalculationFactors;
  unitDeratePercent: number;
  unitDerate: number;
  productionCapacity: number;
  lostMarginOpportunity: number;
  relevantProductionCost: number;
  attributes: CalculationAttributes;
}

export abstract class ProductionCalculations {
  calculatorCategory: CalculatorCategory;
  calculationType: CalculationType;
  impactQuantity: number;
  productionLoss: CostPeriods;
  daysElapsed: number;
  dateElapsed: Date;
  currentDate: Date;
  calculationFactorID: number;
  daysPending: number;
  datePending: Date;
  derateElapsed: number;
  deratePending: number;
  hasDerateOverride: boolean;
  derateOverride: number;
  costCalculation: CostPeriods;
  calculationFactors: CalculationFactors;
  unitDeratePercent: number;
  unitDerate: number;
  productionCapacity: number;
  lostMarginOpportunity: number;
  relevantProductionCost: number;

  version: number;
  constructor(pcc: ProductionCalculationsConstructor) {
    this.calculatorCategory = pcc.calculatorCategory;
    this.impactQuantity = pcc.impactQuantity;
    this.productionLoss = pcc.productionLoss;
    this.daysElapsed = pcc.daysElapsed;
    this.dateElapsed = pcc.dateElapsed;
    this.currentDate = pcc.currentDate;
    this.calculationFactorID = pcc.calculationFactorID;
    this.daysPending = pcc.daysPending;
    this.datePending = pcc.datePending;
    this.derateElapsed = pcc.derateElapsed;
    this.deratePending = pcc.deratePending;
    this.hasDerateOverride = pcc.hasDerateOverride;
    this.derateOverride = pcc.derateOverride;
    this.costCalculation = pcc.costCalculation;
    this.calculationFactors = pcc.calculationFactors;
    this.unitDeratePercent = pcc.unitDeratePercent;
    this.unitDerate = pcc.unitDerate;
    this.productionCapacity = pcc.productionCapacity;
    this.lostMarginOpportunity = pcc.lostMarginOpportunity;
    this.relevantProductionCost = pcc.relevantProductionCost;
    this.calculationType = pcc.calculationType;
  }
  abstract calculateDerate(calcFacors: CalculationFactors): void;
}
