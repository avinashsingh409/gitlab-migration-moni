import { CalculationFactors } from '@atonix/atx-core';
import {
  EfficiencyCalculations,
  EfficiencyCalculationsConstructor,
} from './efficiency-calculations';

export class LegacyEfficiency extends EfficiencyCalculations {
  efficiencyCalculatedCost(
    impactQuantity: number,
    fuelCost: number,
    capacityFactor: number,
    unitFullLoad: number,
    netUnitHeatRate: number,
    daysElapsedOrPending: number
  ) {
    return (
      (((daysElapsedOrPending * 24) / 1000) *
        capacityFactor *
        fuelCost *
        impactQuantity *
        unitFullLoad *
        netUnitHeatRate) /
      100
    );
  }
  constructor(ecc: EfficiencyCalculationsConstructor) {
    super(ecc);
    this.version = 1;
  }

  calculateDerate(calcFacors: CalculationFactors) {
    this.consumptionRateImpact =
      Math.round(
        ((this.impactQuantity * calcFacors.CalculationFactor) /
          calcFacors.CalculationFactorRatio) *
          1000
      ) / 1000;
    this.consumptionRateVariance =
      (this.consumptionRateImpact / 100) * this.consumptionRateValue;

    this.costCalculation.elapsed = this.efficiencyCalculatedCost(
      this.consumptionRateImpact,
      this.consumableCost,
      this.capacityUtilization,
      this.productionCapacity,
      this.consumptionRateValue,
      this.daysElapsed
    );
    this.costCalculation.pending = this.efficiencyCalculatedCost(
      this.consumptionRateImpact,
      this.consumableCost,
      this.capacityUtilization,
      this.productionCapacity,
      this.consumptionRateValue,
      this.daysPending
    );

    this.costCalculation.total =
      this.costCalculation.elapsed + this.costCalculation.pending;
  }
}
