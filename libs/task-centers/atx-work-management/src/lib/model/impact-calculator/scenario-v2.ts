import { isNil } from '@atonix/atx-core';
import {
  EfficiencyCalculationParameters,
  ProductionCalculationParameters,
} from './calculation-parameters';

import { EfficiencyCalculationsV2 } from './efficiency-v2';
import { ProductionCalculationsV2 } from './production-v2';
import {
  ImpactScenario,
  MissingAttributeMessage,
  ScenarioConstructor,
  setDisplayUnitValues,
} from './scenario';

export class ImpactScenarioV2 extends ImpactScenario {
  productionCapacityUnits: string; // ImpactCapacityUoM
  productionTimeUnits: string; // ImpactUoT
  productionCapacity: number; // ImpactCapacityValue
  productionTimeToDays: number; // ImpactTimeToDaysConversion
  productionRevenue: number; // ImpactProductionRevenueValue
  consumableLabel: string; // ImpactConsumableLabel
  consumableUnits: string; // ImpactConsumableUoM
  consumableCost: number; // ImpactConsumableValue
  consumptionRateLabel: string; // ImpactNomConsRateLabel
  consumptionRateUnits: string; // ImpactNomConsRateUoM
  consumptionRateValue: number; // ImpactNomConsRateValue
  consumptionToCostRate: number; // ImpactConsCostToRateConvert
  productionToCostRate: number; // ImpactProdCostToRateConvert
  capacityUtilization: number; // ImpactNomCapacityUtilization

  constructor(scenarioConstructor: ScenarioConstructor) {
    super(scenarioConstructor);
    this.scenarioVersion = 2;
    if (!scenarioConstructor?.attributes) {
      return;
    }
    this.consumableLabel =
      scenarioConstructor.attributes['consumableLabel'].toString();
    this.consumableUnits =
      scenarioConstructor.attributes['consumableUnits'].toString();
    this.consumptionRateValue =
      +scenarioConstructor.attributes['consumptionRateValue'];
    this.consumptionRateUnits =
      scenarioConstructor.attributes['consumptionRateUnits'].toString();
    this.consumptionToCostRate =
      +scenarioConstructor.attributes['consumptionToCostRate'];
    this.productionToCostRate =
      +scenarioConstructor.attributes['productionToCostRate'];
    this.consumableCost = +scenarioConstructor.attributes['consumableCost'];
    this.consumptionRateLabel =
      scenarioConstructor.attributes['consumptionRateLabel'].toString();
    this.capacityUtilization =
      +scenarioConstructor.attributes['capacityUtilization'];
    this.productionCapacityUnits =
      scenarioConstructor.attributes['productionCapacityUnits'].toString();
    this.productionTimeUnits =
      scenarioConstructor.attributes['productionTimeUnits'].toString();
    this.productionCapacity =
      +scenarioConstructor.attributes['productionCapacity'];
    this.productionTimeToDays =
      +scenarioConstructor.attributes['productionTimeToDays'];
    this.productionRevenue =
      +scenarioConstructor.attributes['productionRevenue'];
  }

  calculateRelevantProductionCost() {
    if (isNil(this.consumptionToCostRate) || isNil(this.productionToCostRate)) {
      return 0.0;
    }
    if (this.consumptionToCostRate !== 0 && this.productionToCostRate !== 0) {
      return (
        (this.consumableCost * this.consumptionRateValue) /
        (this.consumptionToCostRate / this.productionToCostRate)
      );
    } else {
      return 0.0;
    }
  }

  setDefaults() {
    let productionUnits = `${this.productionCapacityUnits}/${this.productionTimeUnits}`;
    if (
      this.productionCapacityUnits === MissingAttributeMessage ||
      this.productionTimeUnits === MissingAttributeMessage
    ) {
      productionUnits = MissingAttributeMessage;
    }

    const consumptionToCostRateLabel =
      isNil(this.consumptionToCostRate) ||
      this.consumptionToCostRate === 0.0 ||
      isNil(this.productionToCostRate) ||
      this.productionToCostRate === 0.0
        ? MissingAttributeMessage
        : (this.consumptionToCostRate / this.productionToCostRate).toString();

    if (
      this.productionCapacityUnits?.toLowerCase() === 'mwh' &&
      this.productionTimeUnits?.toLowerCase() === 'h'
    ) {
      productionUnits = 'MW';
    }
    if (
      this.productionCapacityUnits?.toLowerCase() === 'kwh' &&
      this.productionTimeUnits?.toLowerCase() === 'h'
    ) {
      productionUnits = 'KW';
    }
    this.displayUnits = {
      productionUnits: setDisplayUnitValues(productionUnits),
      lostMarginOpportunityUnits: setDisplayUnitValues(
        this.productionCapacityUnits
      ),
      consumptionRateUnits: setDisplayUnitValues(this.consumptionRateUnits),
      consumibleCost: setDisplayUnitValues(this.consumableUnits),
      consumptionRateLabel: setDisplayUnitValues(this.consumptionRateLabel),
      consumableLabel: setDisplayUnitValues(this.consumableLabel),
      calculatorDenominator: setDisplayUnitValues(consumptionToCostRateLabel),
      utilizationLabel: { label: '%', error: false },
      consumptionRateImpact: setDisplayUnitValues(
        this.consumptionRateLabel,
        'Impact'
      ),
      consumptionRateVariance: setDisplayUnitValues(
        this.consumptionRateLabel,
        'Change'
      ),
    };
  }

  createEfficiencyCalculationParameters(): EfficiencyCalculationParameters {
    const efficiencyCalculations = new EfficiencyCalculationsV2({
      calculationFactorID: this.efficiencyParameters?.calculationFactorID
        ? this.efficiencyParameters.calculationFactorID
        : 64,
      impactQuantity: this.efficiencyParameters?.impactQuantity,
      capacityUtilization: this.capacityUtilization,
      productionCapacity: this.productionCapacity,
      consumptionRateValue: this.consumptionRateValue,
      consumptionRateImpact: 0.0,
      consumptionRateVariance: 0.0,
      consumableCost: this.consumableCost,
      calculationFactors: null,
      daysElapsed: this.efficiencyParameters?.impactRange?.DaysElapsed,
      daysPending: this.efficiencyParameters?.impactRange?.DaysPending,
      currentDate: this.efficiencyParameters?.impactRange?.CurrentDate,
      dateElapsed: this.efficiencyParameters?.impactRange?.StartDate,
      datePending: this.efficiencyParameters?.impactRange?.EndDate,
      costCalculation: {
        elapsed: this.efficiencyParameters?.totalCost?.toDate,
        pending: this.efficiencyParameters?.totalCost?.potentialFuture,
        total:
          this.efficiencyParameters?.totalCost?.toDate +
          this.efficiencyParameters?.totalCost?.potentialFuture,
      },
      attributes: {
        productionTimeToDays: this.productionTimeToDays,
        consumptionToCostRate: this.consumptionToCostRate,
        productionToCostRate: this.productionToCostRate,
      },
    });
    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.efficiencyParameters?.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.efficiencyParameters?.assetIssueImpactTypeID,
      calculationType: this.efficiencyParameters?.overrideTotal,
      costOverride: {
        elapsed: this.efficiencyParameters?.totalCost.toDate,
        pending: this.efficiencyParameters?.totalCost.potentialFuture,
        total:
          this.efficiencyParameters?.totalCost.toDate +
          this.efficiencyParameters?.totalCost.potentialFuture,
      },
      monthlyCost: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      calculation: efficiencyCalculations,
    };
  }

  createProductionCalculationParameters(): ProductionCalculationParameters {
    const productionCalcs = new ProductionCalculationsV2({
      calculatorCategory: this.generationParameters?.assetIssueImpactTypeID,
      impactQuantity: this.generationParameters?.impactQuantity,
      productionLoss: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      daysElapsed: this.generationParameters?.impactRange?.DaysElapsed,
      dateElapsed: this.generationParameters?.impactRange?.StartDate,
      currentDate: this.generationParameters?.impactRange?.CurrentDate,
      calculationFactorID: this.generationParameters.calculationFactorID,
      calculationType: this.generationParameters.overrideTotal,
      daysPending: this.generationParameters?.impactRange?.DaysPending,
      datePending: this.generationParameters?.impactRange?.EndDate,
      derateElapsed:
        this.generationParameters?.percentTimeDerateInEffect.toDate,
      deratePending:
        this.generationParameters?.percentTimeDerateInEffect.potentialFuture,
      hasDerateOverride: this.generationParameters?.hasDerateOverride,
      derateOverride: this.generationParameters?.derateOverrideValue,
      costCalculation: {
        elapsed: this.generationParameters.totalCost.toDate,
        pending: this.generationParameters.totalCost.potentialFuture,
        total:
          this.generationParameters.totalCost.toDate +
          this.generationParameters.totalCost.potentialFuture,
      },
      attributes: {
        productionTimeToDays: this.productionTimeToDays || 0,
      },
      calculationFactors: null,
      unitDeratePercent: 0.0,
      unitDerate: 0.0,
      productionCapacity: this.productionCapacity || 0,
      lostMarginOpportunity:
        +this.generationParameters?.lostMarginOpportunity ||
        +this.productionRevenue,
      relevantProductionCost: this.generationParameters?.relevantProductionCost,
    });

    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.generationParameters.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.generationParameters.assetIssueImpactTypeID,
      calculationType: this.generationParameters.overrideTotal,
      costOverride: {
        elapsed: this.generationParameters.totalCost.toDate,
        pending: this.generationParameters.totalCost.potentialFuture,
        total:
          this.generationParameters.totalCost.toDate +
          this.generationParameters.totalCost.potentialFuture,
      },
      monthlyCost: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      calculation: productionCalcs,
    };
  }
}
