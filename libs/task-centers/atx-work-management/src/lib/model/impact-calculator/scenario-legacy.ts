import {
  EfficiencyCalculationParameters,
  ProductionCalculationParameters,
} from './calculation-parameters';
import { LegacyProductionCalculations } from './production-legacy';
import { ImpactScenario, ScenarioConstructor } from './scenario';
import { LegacyEfficiency } from './efficiency-legacy';

export class LegacyImpactScenario extends ImpactScenario {
  unitCapability: number;
  netUnitHeatRate: number;
  capacityFactor: number;
  fuelCost: number;
  constructor(scenarioConstructor: ScenarioConstructor) {
    super(scenarioConstructor);
    this.scenarioVersion = 1;
    if (scenarioConstructor.attributes['unitCapability']) {
      this.unitCapability = +scenarioConstructor.attributes['unitCapability'];
    }
    if (scenarioConstructor.attributes['netUnitHeatRate']) {
      this.netUnitHeatRate = +scenarioConstructor.attributes['netUnitHeatRate'];
    }
    if (scenarioConstructor.attributes['fuelCost']) {
      this.fuelCost = +scenarioConstructor.attributes['fuelCost'];
    }
    if (scenarioConstructor.attributes['capacityFactor']) {
      this.capacityFactor = +scenarioConstructor.attributes['capacityFactor'];
    }
  }

  calculateRelevantProductionCost() {
    return (this.fuelCost * this.netUnitHeatRate) / 1000;
  }

  setDefaults() {
    if (!this.capacityFactor) {
      this.capacityFactor = 0;
    }
    if (!this.netUnitHeatRate) {
      this.netUnitHeatRate = 0;
    }
    if (!this.fuelCost) {
      this.fuelCost = 0;
    }
    if (
      this.generationParameters &&
      !this.generationParameters.lostMarginOpportunity
    ) {
      this.generationParameters.lostMarginOpportunity = 0;
    }
    if (!this.unitCapability) {
      this.unitCapability = 0;
    }
    this.displayUnits = {
      productionUnits: { label: 'MW', error: false },
      lostMarginOpportunityUnits: { label: 'MWh', error: false },
      consumptionRateUnits: { label: 'Btu/kWh', error: false },
      consumibleCost: { label: '$/Mbtu', error: false },
      consumableLabel: { label: 'Fuel Cost', error: false },
      utilizationLabel: { label: '', error: false },
      calculatorDenominator: { label: '1000', error: false },
      consumptionRateLabel: { label: 'Net Unit Heat Rate', error: false },
      consumptionRateVariance: { label: 'Avg Heat Rate Change', error: false },
      consumptionRateImpact: { label: 'Heat Rate Impact', error: false },
    };
  }

  createEfficiencyCalculationParameters(): EfficiencyCalculationParameters {
    const calculationParameters = new LegacyEfficiency({
      calculationFactorID: this.efficiencyParameters.calculationFactorID,
      impactQuantity: this.efficiencyParameters.impactQuantity,
      capacityUtilization: this.capacityFactor,
      productionCapacity: this.unitCapability,
      consumptionRateValue: this.netUnitHeatRate,
      consumptionRateImpact: 0.0,
      consumptionRateVariance: 0.0,
      consumableCost: this.fuelCost,
      calculationFactors: null,
      daysElapsed: this.efficiencyParameters?.impactRange?.DaysElapsed,
      daysPending: this.efficiencyParameters?.impactRange?.DaysPending,
      currentDate: this.efficiencyParameters?.impactRange?.CurrentDate,
      dateElapsed: this.efficiencyParameters?.impactRange?.StartDate,
      datePending: this.efficiencyParameters?.impactRange?.EndDate,
      costCalculation: {
        elapsed: this.efficiencyParameters?.totalCost.toDate,
        pending: this.efficiencyParameters?.totalCost.potentialFuture,
        total:
          this.efficiencyParameters?.totalCost.toDate +
          this.efficiencyParameters?.totalCost.potentialFuture,
      },
      attributes: {},
    });
    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.efficiencyParameters.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.efficiencyParameters.assetIssueImpactTypeID,
      calculationType: this.efficiencyParameters.overrideTotal,
      costOverride: {
        elapsed: this.efficiencyParameters.totalCost.toDate,
        pending: this.efficiencyParameters.totalCost.potentialFuture,
        total:
          this.efficiencyParameters.totalCost.toDate +
          this.efficiencyParameters.totalCost.potentialFuture,
      },
      monthlyCost: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      calculation: calculationParameters,
    };
  }

  createProductionCalculationParameters(): ProductionCalculationParameters {
    const productionCalcs = new LegacyProductionCalculations({
      calculatorCategory: this.generationParameters.assetIssueImpactTypeID,
      impactQuantity: this.generationParameters.impactQuantity,
      productionLoss: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      daysElapsed: this.generationParameters?.impactRange?.DaysElapsed,
      dateElapsed: this.generationParameters?.impactRange?.StartDate,
      currentDate: this.generationParameters?.impactRange?.CurrentDate,
      calculationFactorID: this.generationParameters.calculationFactorID,
      calculationType: this.generationParameters.overrideTotal,
      daysPending: this.generationParameters?.impactRange?.DaysPending,
      datePending: this.generationParameters?.impactRange?.EndDate,
      derateElapsed:
        this.generationParameters?.percentTimeDerateInEffect.toDate,
      deratePending:
        this.generationParameters?.percentTimeDerateInEffect.potentialFuture,
      hasDerateOverride: this.generationParameters?.hasDerateOverride,
      derateOverride: this.generationParameters?.derateOverrideValue,
      costCalculation: {
        elapsed: this.generationParameters.totalCost.toDate,
        pending: this.generationParameters.totalCost.potentialFuture,
        total:
          this.generationParameters.totalCost.toDate +
          this.generationParameters.totalCost.potentialFuture,
      },
      attributes: {},
      calculationFactors: null,
      unitDeratePercent: 0.0,
      unitDerate: 0.0,
      productionCapacity: this.unitCapability,

      lostMarginOpportunity: this.generationParameters?.lostMarginOpportunity,
      relevantProductionCost: this.generationParameters?.relevantProductionCost,
    });

    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.generationParameters.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.generationParameters.assetIssueImpactTypeID,
      calculationType: this.generationParameters.overrideTotal,
      costOverride: {
        elapsed: this.generationParameters.totalCost.toDate,
        pending: this.generationParameters.totalCost.potentialFuture,
        total:
          this.generationParameters.totalCost.toDate +
          this.generationParameters.totalCost.potentialFuture,
      },
      monthlyCost: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      calculation: productionCalcs,
    };
  }
}
