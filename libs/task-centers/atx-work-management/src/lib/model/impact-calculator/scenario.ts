import {
  EfficiencyCalculationParameters,
  MaintenanceCalculationParameters,
  MaintenanceParameters,
  OtherCalculationParameters,
  OtherParameters,
  ProductionCalculationParameters,
  TotalCalculations,
} from './calculation-parameters';
import { EfficiencyParameters } from './efficiency-calculations';
import { GenerationParameters } from './production-calculations';

export interface CalculationAttributes {
  [attr: string]: string | number;
}

export interface ScenarioConstructor {
  scenarioID: number;
  scenarioGuidID: number;
  scenarioName: string;
  scenarioNotes: string;
  percentLikelihood: number;
  attributes: CalculationAttributes;
  generationParameters: GenerationParameters;
  efficiencyParameters: EfficiencyParameters;
  maintenanceParameters: MaintenanceParameters;
  otherParameters: OtherParameters;
  errors: string[];
}

export interface DisplayUnitValues {
  label: string;
  error: boolean;
}

export const MissingAttributeMessage = 'not configured';

export interface DisplayUnits {
  productionUnits: DisplayUnitValues;
  lostMarginOpportunityUnits: DisplayUnitValues;
  consumptionRateUnits: DisplayUnitValues;
  consumptionRateVariance: DisplayUnitValues;
  consumibleCost: DisplayUnitValues;
  consumableLabel: DisplayUnitValues;
  consumptionRateLabel: DisplayUnitValues;
  consumptionRateImpact: DisplayUnitValues;
  calculatorDenominator: DisplayUnitValues;
  utilizationLabel: DisplayUnitValues;
}

export function setDisplayUnitValues(
  label: string,
  addedText: string = ''
): DisplayUnitValues {
  if (label === MissingAttributeMessage) {
    return {
      label: MissingAttributeMessage,
      error: true,
    };
  } else {
    if (addedText) {
      label += ` ${addedText}`;
    }
    return {
      label,
      error: false,
    };
  }
}

export abstract class ImpactScenario {
  scenarioID: number;
  scenarioGuidID: number;
  scenarioName: string;
  scenarioNotes: string;
  percentLikelihood: number;
  scenarioVersion: number;
  attributes: CalculationAttributes;
  displayUnits: DisplayUnits;
  errors: string[];
  errorMessage: string;

  constructor(scenarioConstructor: ScenarioConstructor) {
    this.scenarioID = scenarioConstructor.scenarioID;
    this.scenarioGuidID = scenarioConstructor.scenarioGuidID;
    this.scenarioName = scenarioConstructor.scenarioName;
    this.scenarioNotes = scenarioConstructor.scenarioNotes;
    this.percentLikelihood = scenarioConstructor.percentLikelihood;
    this.generationParameters = scenarioConstructor.generationParameters;
    this.efficiencyParameters = scenarioConstructor.efficiencyParameters;
    this.maintenanceParameters = scenarioConstructor.maintenanceParameters;

    this.otherParameters = scenarioConstructor.otherParameters;
    this.errors = Array.from(
      new Set([
        ...scenarioConstructor.errors,
        ...scenarioConstructor.efficiencyParameters.errors,
        ...scenarioConstructor.generationParameters.errors,
      ])
    );
    if (this.errors.length > 0) {
      const errorList = this.errors.join(', ');
      this.errorMessage = `The asset configuration is incomplete to accurately calculate this impact. Please update the following attributes: ${errorList}`;
    } else {
      this.errorMessage = '';
    }
  }
  generationParameters: GenerationParameters;
  efficiencyParameters: EfficiencyParameters;
  maintenanceParameters: MaintenanceParameters;
  otherParameters: OtherParameters;

  abstract calculateRelevantProductionCost(): number;
  abstract setDefaults(): void;

  abstract createProductionCalculationParameters(): ProductionCalculationParameters;
  abstract createEfficiencyCalculationParameters(): EfficiencyCalculationParameters;

  createMaintenanceCalculationParameters(): MaintenanceCalculationParameters {
    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.maintenanceParameters.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.maintenanceParameters.assetIssueImpactTypeID,
      calculationType: this.maintenanceParameters.overrideTotal,
      costOverride: {
        elapsed: this.maintenanceParameters.totalCost.toDate,
        pending: this.maintenanceParameters.totalCost.potentialFuture,
        total:
          this.maintenanceParameters.totalCost.toDate +
          this.maintenanceParameters.totalCost.potentialFuture,
      },
      calculation: {
        costCalculation: {
          elapsed: this.maintenanceParameters.totalCost.toDate,
          pending: this.maintenanceParameters.totalCost.potentialFuture,
          total:
            this.maintenanceParameters.totalCost.toDate +
            this.maintenanceParameters.totalCost.potentialFuture,
        },
        partsElapsed: this.maintenanceParameters?.repairCost.toDate,
        partsPending: this.maintenanceParameters?.repairCost.potentialFuture,
        laborHoursElapsed: this.maintenanceParameters?.repairManHours.toDate,
        laborHoursPending:
          this.maintenanceParameters?.repairManHours.potentialFuture,
        laborRateElapsed: this.maintenanceParameters?.laborHourlyRate.toDate,
        laborRatePending:
          this.maintenanceParameters?.laborHourlyRate.potentialFuture,
      },
    };
  }

  createOtherCalculationParameters(): OtherCalculationParameters {
    return {
      assetIssueAssetIssueImpactTypeMapID:
        this.otherParameters.assetIssueAssetIssueImpactTypeMapID,
      assetIssueImpactTypeID: this.otherParameters.assetIssueImpactTypeID,
      calculationType: this.otherParameters.overrideTotal,
      costOverride: {
        elapsed: this.otherParameters?.totalCost.toDate,
        pending: this.otherParameters?.totalCost.potentialFuture,
        total:
          this.otherParameters?.totalCost.toDate +
          this.otherParameters?.totalCost.potentialFuture,
      },
      monthlyCost: {
        elapsed: 0.0,
        pending: 0.0,
        total: 0.0,
      },
      calculation: {
        daysElapsed: this.otherParameters?.impactRange.DaysElapsed,
        daysPending: this.otherParameters?.impactRange.DaysPending,
        currentDate: this.otherParameters?.impactRange?.CurrentDate,
        dateElapsed: this.otherParameters?.impactRange?.StartDate,
        datePending: this.otherParameters?.impactRange?.EndDate,
        rateElapsed: this.otherParameters?.impactQuantity.toDate,
        ratePending: this.otherParameters?.impactQuantity.potentialFuture,
        costCalculation: {
          elapsed: this.otherParameters?.totalCost.toDate,
          pending: this.otherParameters?.totalCost.potentialFuture,
          total:
            this.otherParameters?.totalCost?.toDate +
            this.otherParameters?.totalCost?.potentialFuture,
        },
      },
    };
  }

  createTotalCalculations(): TotalCalculations {
    return {
      name: this.scenarioName,
      notes: this.scenarioNotes,
      percentLikelihood: this.percentLikelihood,
      totalImpact: {
        pending: 0.0,
        elapsed: 0.0,
        total: 0.0,
      },
      probabilityWeightedMonthlyImpact: {
        pending: 0.0,
        elapsed: 0.0,
        total: 0.0,
      },
      monthlyImpact: {
        pending: 0.0,
        elapsed: 0.0,
        total: 0.0,
      },
      probabilityWeightedTotalImpact: {
        pending: 0.0,
        elapsed: 0.0,
        total: 0.0,
      },
    };
  }
}

export enum CalculatorCategory {
  Reliability = 2,
  Derate = 3,
}
