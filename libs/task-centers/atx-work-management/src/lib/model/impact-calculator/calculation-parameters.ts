import {
  CalculationFactors,
  CalculationType,
  ImpactDateRange,
  TimeValues,
} from '@atonix/atx-core';
import { EfficiencyCalculations } from './efficiency-calculations';
import { ProductionCalculations } from './production-calculations';

export const CalculationTypeMapping = {
  [CalculationType.CalculatedImpact]: 'Calculated Impact',
  [CalculationType.UserDefinedImpact]: 'User Defined Impact',
};

export interface CostPeriods {
  elapsed: number;
  pending: number;
  total: number;
}

export interface MaintenanceCalculations {
  partsElapsed: number;
  partsPending: number;
  laborHoursElapsed: number;
  laborHoursPending: number;
  laborRateElapsed: number;
  laborRatePending: number;
  costCalculation: CostPeriods;
}

export interface OtherCalculations {
  daysPending: number;
  daysElapsed: number;
  dateElapsed: Date;
  currentDate: Date;
  datePending: Date;
  rateElapsed: number;
  ratePending: number;
  costCalculation: CostPeriods;
}

export interface ProductionCalculationParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  calculationType: CalculationType;
  costOverride: CostPeriods;
  monthlyCost: CostPeriods;
  calculation: ProductionCalculations;
}

export interface EfficiencyCalculationParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  calculationType: CalculationType;
  costOverride: CostPeriods;
  monthlyCost: CostPeriods;
  calculation: EfficiencyCalculations;
}

export interface MaintenanceCalculationParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  calculationType: CalculationType;
  costOverride: CostPeriods;
  calculation: MaintenanceCalculations;
}

export interface OtherCalculationParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  calculationType: CalculationType;
  costOverride: CostPeriods;
  monthlyCost: CostPeriods;
  calculation: OtherCalculations;
}

export interface TotalCalculations {
  name: string;
  notes: string;
  percentLikelihood: number;
  totalImpact: CostPeriods;
  probabilityWeightedTotalImpact: CostPeriods;
  monthlyImpact: CostPeriods;
  probabilityWeightedMonthlyImpact: CostPeriods;
}

export interface ImpactTotals {
  productionTotal: number;
  productionDuration: number;
  productionMonthly: number;
  efficiencyTotal: number;
  efficiencyDuration: number;
  efficiencyMonthly: number;
  maintenanceTotal: number;
  otherTotal: number;
  otherDuration: number;
  otherMonthly: number;
  totalImpact: number;
  probabilityWeightedTotalImpact: number;
  monthlyImpact: number;
  probabilityWeightedMonthlyImpact: number;
}

export interface MaintenanceParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  overrideTotal: CalculationType;
  repairCost: TimeValues;
  repairManHours: TimeValues;
  laborHourlyRate: TimeValues;
  totalCost: TimeValues;
}

export interface OtherParameters {
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  overrideTotal: CalculationType;
  impactQuantity: TimeValues;
  totalCost: TimeValues;
  impactRange: ImpactDateRange;
}
