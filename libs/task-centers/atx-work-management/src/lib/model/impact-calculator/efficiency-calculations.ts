import {
  CalculationFactors,
  CalculationType,
  ImpactDateRange,
  TimeValues,
} from '@atonix/atx-core';
import { CostPeriods } from './calculation-parameters';
import { CalculationAttributes } from './scenario';

export interface EfficiencyErrors {
  consumableCost: boolean;
  consumptionRateValue: boolean;
  consumptionToCostRate: boolean;
}

export function createEfficiencyErrors(errors: string[]): EfficiencyErrors {
  return {
    consumableCost: errors.includes('ImpactConsumableValue') ? true : false,
    consumptionRateValue: errors.includes('ImpactNomConsRateValue')
      ? true
      : false,

    consumptionToCostRate: errors.includes('ImpactConsCostToRateConvert')
      ? true
      : false,
  };
}

export interface EfficiencyParameters {
  version: number;
  assetIssueAssetIssueImpactTypeMapID: number;
  assetIssueImpactTypeID: number;
  overrideTotal: CalculationType;
  calculationFactorID: number;
  impactQuantity: number;
  impactRange: ImpactDateRange;
  totalCost: TimeValues;
  attributes: CalculationAttributes;
  errors: string[];
  efficiencyErrors: EfficiencyErrors;
}

export interface EfficiencyCalculationsConstructor {
  impactQuantity: number;
  consumptionRateImpact: number;
  consumptionRateVariance: number;
  capacityUtilization: number;
  calculationFactorID: number;
  productionCapacity: number;
  consumptionRateValue: number;
  consumableCost: number;
  daysElapsed: number;
  daysPending: number;
  dateElapsed: Date;
  currentDate: Date;
  datePending: Date;
  costCalculation: CostPeriods;
  calculationFactors: CalculationFactors;
  attributes: CalculationAttributes;
}

export abstract class EfficiencyCalculations {
  impactQuantity: number;
  consumptionRateImpact: number;
  consumptionRateVariance: number;
  capacityUtilization: number;
  calculationFactorID: number;
  productionCapacity: number;
  consumptionRateValue: number;
  consumableCost: number;
  daysElapsed: number;
  daysPending: number;
  dateElapsed: Date;
  currentDate: Date;
  datePending: Date;
  costCalculation: CostPeriods;
  calculationFactors: CalculationFactors;
  version: number;

  constructor(ecc: EfficiencyCalculationsConstructor) {
    this.impactQuantity = ecc.impactQuantity;
    this.consumptionRateImpact = ecc.consumptionRateImpact;
    this.consumptionRateVariance = ecc.consumptionRateVariance;
    this.capacityUtilization = ecc.capacityUtilization;
    this.calculationFactorID = ecc.calculationFactorID;
    this.productionCapacity = ecc.productionCapacity || 0;
    this.consumptionRateValue = ecc.consumptionRateValue;
    this.consumableCost = ecc.consumableCost;
    this.daysElapsed = ecc.daysElapsed;
    this.daysPending = ecc.daysPending;
    this.dateElapsed = ecc.dateElapsed;
    this.currentDate = ecc.currentDate;
    this.datePending = ecc.datePending;
    this.costCalculation = ecc.costCalculation;
    this.calculationFactors = ecc.calculationFactors;
  }

  abstract efficiencyCalculatedCost(
    impactQuantity: number,
    fuelCost: number,
    capacityFactor: number,
    unitFullLoad: number,
    netUnitHeatRate: number,
    daysElapsedOrPending: number
  ): number;

  abstract calculateDerate(calcFacors: CalculationFactors): void;
}
