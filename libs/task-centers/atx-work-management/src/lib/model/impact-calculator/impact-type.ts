export const enum AssetIssueImpactTypeID {
  HeatRate = 1,
  GenerationReliability = 2,
  GenerationDerate = 3,
  OtherCosts = 9,
  Maintenance = 10,
}
