import { CalculationFactors } from '@atonix/atx-core';
import {
  ProductionCalculations,
  ProductionCalculationsConstructor,
} from './production-calculations';

export class LegacyProductionCalculations extends ProductionCalculations {
  constructor(pcc: ProductionCalculationsConstructor) {
    super(pcc);
    this.version = 1;
  }

  calculateDerate(calcFacors: CalculationFactors) {
    this.calculationFactors = calcFacors;
    this.unitDeratePercent =
      (this.impactQuantity * calcFacors.CalculationFactor) /
      calcFacors.CalculationFactorRatio;
    this.unitDerate =
      Math.round(
        (this.unitDeratePercent / 100) * this.productionCapacity * 100
      ) / 100;
    if (this.hasDerateOverride) {
      this.productionLoss.elapsed =
        this.unitDerate * this.daysElapsed * 24 * (this.derateOverride / 100);
      this.productionLoss.pending =
        this.unitDerate * this.daysPending * 24 * (this.derateOverride / 100);
    } else {
      this.productionLoss.elapsed =
        this.unitDerate * this.daysElapsed * 24 * (this.derateElapsed / 100);
      this.productionLoss.pending =
        this.unitDerate * this.daysPending * 24 * (this.deratePending / 100);
    }
    this.productionLoss.total =
      this.productionLoss.pending + this.productionLoss.elapsed;
    this.costCalculation.elapsed =
      this.productionLoss.elapsed *
      (this.lostMarginOpportunity - this.relevantProductionCost);
    this.costCalculation.pending =
      this.productionLoss.pending *
      (this.lostMarginOpportunity - this.relevantProductionCost);

    const totalCost =
      this.costCalculation.elapsed + this.costCalculation.pending;
    this.costCalculation.total = totalCost;
  }
}
