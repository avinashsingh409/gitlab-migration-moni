export interface ICategoryCount {
  Category: string;
  CategoryID: number;
  Count: number;
}
