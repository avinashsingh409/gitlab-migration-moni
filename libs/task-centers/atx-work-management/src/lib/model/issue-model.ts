export interface AssetClassType {
  AssetClassTypeID: number;
  AssetTypeID: number;
  AssetClassTypeKey: string;
  AssetClassTypeAbbrev: string;
  AssetClassTypeDesc: string;
  DisplayOrder: number;
}

export interface AssetType {
  AssetTypeID: number;
  AssetTypeEnum: number;
  AssetTypeAbbrev: string;
  AssetTypeDesc: string;
}

export interface Asset {
  AssetID: number;
  ParentAssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface System {
  AssetID: number;
  ParentAssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface AssetType3 {
  AssetTypeID: number;
  AssetTypeEnum: number;
  AssetTypeAbbrev: string;
  AssetTypeDesc: string;
}

export interface Unit {
  AssetID: number;
  ParentAssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType3;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface Station {
  AssetID: number;
  ParentAssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface StationGroup {
  AssetID: number;
  ParentAssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface Client {
  AssetID: number;
  ParentAssetID?: any;
  AssetAbbrev: string;
  AssetDesc: string;
  AssetClassTypeID: number;
  AssetTreeNodeChildConfigurationTypeID?: any;
  DisplayOrder: number;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  GlobalId: string;
  IsHidden: boolean;
  Track: boolean;
  AssetClassType: AssetClassType;
  AssetType: AssetType;
  NumChildren: number;
  Keywords?: any;
  IsTheOwner: boolean;
  AttributeCategories?: any;
  AttachmentCategories?: any;
}

export interface AssetAndParents {
  Asset: Asset;
  System: System;
  Unit: Unit;
  Station: Station;
  StationGroup: StationGroup;
  Client: Client;
}

export interface VariableType {
  PDVariableID: number;
  VariableDesc: string;
  VariableAbbrev: string;
  VariableName: string;
  EngUnits: string;
  CalcString?: any;
  FunctionTypeID?: number;
  WeightingVariableTypeID?: any;
  DefaultValueTypeID?: number;
}

export interface IssueCauseTypeVariableTypeMap {
  VariableType: VariableType;
  IssueCauseTypeVariableTypeMapID: number;
  IssueCauseTypeID: number;
  VariableTypeID: number;
  ValueTypeID?: any;
  Symptom: string;
  SymptomLevel: number;
  AttributeTypeID?: any;
  AttributeValue: string;
}

export interface ActionPlan {
  AssetIssueActionPlanID: number;
  AssetIssueID: number;
  ActivityNumber: number;
  ResponsibleParty: string;
  ActivityDate: Date;
  Activity: string;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedBy: string;
  ChangedBy: string;
  Status: string;
  CanEdit: boolean;
  CanDelete: boolean;
  CanEditStatus: boolean;
}

export interface CausalFactor {
  AssetIssueCausalFactorID: number;
  AssetIssueID: number;
  Rating: number;
  CausalFactor: string;
  Note: string;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedBy: string;
  ChangedBy: string;
}

export interface PrioritiesSelection {
  id: number;
  name: string;
}

export const DEFAULT_GENERATION_CALCULATION_FACTOR_ID = 65;
export const DEFAULT_EFFICIENCY_CALCULATION_FACTOR_ID = 64;
export enum AssetImpactType {
  Efficiency = 1,
  Reliability = 2,
  Derate = 3,
  Other = 9,
  Maintenance = 10,
}
export enum AssetImpactCategory {
  Efficiency = 1,
  Reliability = 6,
  Derate = 6,
  Other = 5,
  Maintenance = 7,
}
