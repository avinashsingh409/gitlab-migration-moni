export const WorkRequestPriorities = [
  { value: '1', desc: 'Low' },
  { value: '2', desc: 'Medium' },
  { value: '3', desc: 'High' },
  { value: 'E', desc: 'Emergency' },
];
