import { AssetIssue, AssetIssueWithOptions } from '@atonix/shared/api';

export class NewIssueParam {
  assetId: string;
  selectedCategory: number;
}

export class LoadClassesAndCategoriesParam {
  assetId: string;
  assetIssueGuid: string;
}

export class SaveAssetIssueParam {
  assetIssue: AssetIssue;
  assetIssueWithOptions: AssetIssueWithOptions;
  assetId: string;
}

export class MobileIssueFileAttachment {
  htmlElement: string;
  file: File;
}

export class LoadDiscussionParam {
  assetIssueId: number;
  content: string;
}
