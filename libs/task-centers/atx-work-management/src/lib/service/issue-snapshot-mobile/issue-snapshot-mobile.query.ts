import { IDiscussionEntry } from '@atonix/atx-core';
import {
  AssetIssueWithOptions,
  IssueClassesAndCategories,
  IssueClassType,
} from '@atonix/shared/api';
import { ComponentStore } from '@ngrx/component-store';
import { MobileIssueFileAttachment } from './issue-snapshot-mobile.models';

export interface MobileIssueSnapshotState {
  issueClasses: Array<IssueClassType> | null;
  issueCategories: Record<number, IssueClassesAndCategories[] | null>;
  selectedClass: number;
  selectedCategory: number;
  loadingIssueCategoryTypes: boolean;
  assetIssueId: number;
  assetIssueGuid: string;
  assetId: string | null;
  assetName: string | null;
  isLoading: boolean;
  assetIssueWithOptions: AssetIssueWithOptions | null;
  errorMessage: string | null;
  loadSendNotification: boolean | null;
  selectedUsers: string[] | null;
  issueCreationSuccessful: boolean;
  issueNotificationSent: boolean;
  fileAttachments: MobileIssueFileAttachment[];
  fileUploadCompleted: boolean;
  discussionEntry: IDiscussionEntry;
}

export const initialState: MobileIssueSnapshotState = {
  issueClasses: null,
  issueCategories: null,
  selectedClass: null,
  selectedCategory: null,
  loadingIssueCategoryTypes: false,
  assetIssueId: null,
  assetIssueGuid: null,
  assetId: null,
  assetName: null,
  isLoading: false,
  assetIssueWithOptions: null,
  errorMessage: null,
  loadSendNotification: false,
  selectedUsers: [],
  issueCreationSuccessful: false,
  issueNotificationSent: false,
  fileAttachments: [],
  fileUploadCompleted: false,
  discussionEntry: null,
};

export class MobileIssueSnapshotQuery {
  constructor(private store: ComponentStore<MobileIssueSnapshotState>) {}

  readonly loadSendNotification$ = this.store.select(
    (state) => state.loadSendNotification,
    { debounce: true }
  );
  readonly assetIssueWithOptions$ = this.store.select(
    (state) => state.assetIssueWithOptions,
    { debounce: true }
  );
  readonly issueClasses$ = this.store.select((state) => state.issueClasses, {
    debounce: true,
  });
  readonly issueCategories$ = this.store.select(
    (state) => state.issueCategories,
    { debounce: true }
  );
  readonly selectedClass$ = this.store.select((state) => state.selectedClass, {
    debounce: true,
  });
  readonly selectedCategory$ = this.store.select(
    (state) => state.selectedCategory,
    { debounce: true }
  );
  readonly assetIssueId$ = this.store.select((state) => state.assetIssueId, {
    debounce: true,
  });
  readonly assetIssueGuid$ = this.store.select(
    (state) => state.assetIssueGuid,
    {
      debounce: true,
    }
  );
  readonly loadingIssueCategoryTypes$ = this.store.select(
    (state) => state.loadingIssueCategoryTypes,
    { debounce: true }
  );
  readonly assetId$ = this.store.select((state) => state.assetId, {
    debounce: true,
  });
  readonly assetName$ = this.store.select((state) => state.assetName, {
    debounce: true,
  });
  readonly isLoading$ = this.store.select((state) => state.isLoading, {
    debounce: true,
  });
  readonly errorMessage$ = this.store.select((state) => state.errorMessage, {
    debounce: true,
  });
  readonly issueNotificationSent$ = this.store.select(
    (state) => state.issueNotificationSent,
    { debounce: true }
  );
  readonly issueCreationSuccessful$ = this.store.select(
    (state) => state.issueCreationSuccessful,
    { debounce: true }
  );
  readonly fileAttachments$ = this.store.select(
    (state) => state.fileAttachments,
    { debounce: true }
  );
  readonly fileUploadCompleted$ = this.store.select(
    (state) => state.fileUploadCompleted,
    { debounce: true }
  );

  readonly discussionEntry$ = this.store.select(
    (state) => state.discussionEntry,
    { debounce: true }
  );

  readonly vm$ = this.store.select(
    this.issueClasses$,
    this.issueCategories$,
    this.selectedClass$,
    this.selectedCategory$,
    this.assetIssueId$,
    this.assetIssueGuid$,
    this.loadingIssueCategoryTypes$,
    this.assetId$,
    this.assetName$,
    this.isLoading$,
    this.assetIssueWithOptions$,
    this.errorMessage$,
    this.loadSendNotification$,
    this.issueNotificationSent$,
    this.issueCreationSuccessful$,
    this.fileAttachments$,
    this.fileUploadCompleted$,
    this.discussionEntry$,
    (
      issueClasses,
      issueCategories,
      selectedClass,
      selectedCategory,
      assetIssueId,
      assetIssueGuid,
      loadingIssueCategoryTypes,
      assetId,
      assetName,
      isLoading,
      assetIssueWithOptions,
      errorMessage,
      loadSendNotification,
      issueNotificationSent,
      issueCreationSuccessful,
      fileAttachments,
      fileUploadCompleted,
      discussionEntry
    ): any => {
      return {
        issueClasses,
        issueCategories,
        selectedClass,
        selectedCategory,
        assetIssueId,
        assetIssueGuid,
        loadingIssueCategoryTypes,
        assetId,
        assetName,
        isLoading,
        assetIssueWithOptions,
        errorMessage,
        loadSendNotification,
        issueNotificationSent,
        issueCreationSuccessful,
        fileAttachments,
        fileUploadCompleted,
        discussionEntry,
      };
    }
  );
}
