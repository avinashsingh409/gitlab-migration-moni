import { IDiscussionEntry } from '@atonix/atx-core';
import { ComponentStore } from '@ngrx/component-store';
import { MobileIssueFileAttachment } from './issue-snapshot-mobile.models';
import {
  initialState,
  MobileIssueSnapshotState,
} from './issue-snapshot-mobile.query';

export class MobileIssueSnapshotCommand {
  constructor(private store: ComponentStore<MobileIssueSnapshotState>) {}

  readonly setLoading = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      isLoading: boolean
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        isLoading: isLoading,
      };
    }
  );

  readonly setAssetId = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      assetId: string
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        assetId,
      };
    }
  );

  readonly setLoadingIssueCategoryTypes = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      isLoading: boolean
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        loadingIssueCategoryTypes: isLoading,
      };
    }
  );

  readonly setFileUploadCompleted = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      isComplete: boolean
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        fileUploadCompleted: isComplete,
      };
    }
  );

  readonly setCategoriesAndClasses = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      newState: Partial<MobileIssueSnapshotState>
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        issueCategories: newState.issueCategories,
        issueClasses: newState.issueClasses,
        assetIssueId: newState.assetIssueId,
        assetId: newState.assetId,
        assetName: newState.assetName,
        selectedClass: newState.selectedClass,
        selectedCategory: newState.selectedCategory,
        errorMessage: '',
      };
    }
  );

  readonly setAssetIssue = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      newState: Partial<MobileIssueSnapshotState>
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        assetName: newState.assetName,
        assetIssueWithOptions: {
          ...state.assetIssueWithOptions,
          Issue: newState.assetIssueWithOptions.Issue,
        },
      };
    }
  );

  readonly setAssetIssueIds = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      newState: Partial<MobileIssueSnapshotState>
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        assetIssueId: newState.assetIssueId,
        assetIssueGuid: newState.assetIssueGuid,
      };
    }
  );

  readonly setIssueCreationSuccessful = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      issueCreationSuccessful: boolean
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        issueCreationSuccessful,
      };
    }
  );

  readonly setLoadSendNotification = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      loadSendNotification: boolean
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        loadSendNotification,
      };
    }
  );

  readonly setClass = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      selectedClass: number
    ): MobileIssueSnapshotState => {
      const selectedCategory =
        state.issueCategories[selectedClass][0].AssetIssueCategoryTypeID;
      return {
        ...state,
        selectedClass: selectedClass,
        selectedCategory,
      };
    }
  );

  readonly setCategory = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      selectedCategory: number
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        selectedCategory,
      };
    }
  );

  readonly setTitle = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      title: string
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        assetIssueWithOptions: {
          ...state.assetIssueWithOptions,
          Issue: {
            ...state.assetIssueWithOptions.Issue,
            IssueTitle: title,
          },
        },
      };
    }
  );

  readonly setSummary = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      summary: string
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        assetIssueWithOptions: {
          ...state.assetIssueWithOptions,
          Issue: {
            ...state.assetIssueWithOptions.Issue,
            IssueShortSummary: summary,
            IssueSummary: summary,
          },
        },
      };
    }
  );

  readonly setIssueCreationSuccess = this.store.updater(
    (state: MobileIssueSnapshotState): MobileIssueSnapshotState => {
      return {
        ...state,
        issueCreationSuccessful: true,
      };
    }
  );

  readonly setDiscussionEntry = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      discussionEntry: IDiscussionEntry
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        discussionEntry,
      };
    }
  );

  readonly resetToIssue = this.store.updater(
    (state: MobileIssueSnapshotState): MobileIssueSnapshotState => {
      return {
        ...state,
        isLoading: false,
        loadSendNotification: false,
        issueNotificationSent: true,
      };
    }
  );

  readonly setErrorMessage = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      errorMessage: string
    ): MobileIssueSnapshotState => {
      return {
        ...state,
        errorMessage,
      };
    }
  );

  readonly addFileAttachment = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      file: MobileIssueFileAttachment
    ): MobileIssueSnapshotState => {
      const newFileAttachments: MobileIssueFileAttachment[] = [
        ...state.fileAttachments,
      ];
      newFileAttachments.push(file);

      return {
        ...state,
        fileAttachments: newFileAttachments,
      };
    }
  );

  readonly removeFileAttachment = this.store.updater(
    (
      state: MobileIssueSnapshotState,
      file: MobileIssueFileAttachment
    ): MobileIssueSnapshotState => {
      const newFileAttachments: MobileIssueFileAttachment[] = [
        ...state.fileAttachments,
      ];
      const idx = state.fileAttachments.indexOf(file);
      newFileAttachments.splice(idx, 1);
      return {
        ...state,
        fileAttachments: newFileAttachments,
      };
    }
  );

  readonly resetMobileIssueSnapshotState = this.store.updater(
    (): MobileIssueSnapshotState => {
      return initialState;
    }
  );
}
