import { Injectable, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectAsset } from '@atonix/atx-asset-tree';
import { IDiscussionEntry } from '@atonix/atx-core';
import { IAddDiscussionEntryAttachment } from '@atonix/atx-discussion';
import {
  DiscussionsFrameworkService,
  IssueClassesAndCategories,
  IssueClassType,
  IssuesFrameworkService,
  ISubscriber,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  Subject,
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  Observable,
  switchMap,
  tap,
  take,
  forkJoin,
  of,
} from 'rxjs';
import { IssueSnapshotFacade } from '../../store/facade/issue-snapshot.facade';
import {
  SendIssueModalFacade,
  SendIssueModalState,
} from '../send-issue-modal.facade';
import { MobileIssueSnapshotCommand } from './issue-snapshot-mobile.command';
import {
  LoadClassesAndCategoriesParam,
  LoadDiscussionParam,
  NewIssueParam,
  SaveAssetIssueParam,
} from './issue-snapshot-mobile.models';
import {
  initialState,
  MobileIssueSnapshotQuery,
  MobileIssueSnapshotState,
} from './issue-snapshot-mobile.query';

@Injectable()
export class MobileIssueSnapshotFacade
  extends ComponentStore<MobileIssueSnapshotState>
  implements OnDestroy
{
  public query = new MobileIssueSnapshotQuery(this);
  public command = new MobileIssueSnapshotCommand(this);
  public sendIssueVm$: Observable<SendIssueModalState>;
  public filteredRegisteredUsers$: Observable<ISubscriber[]>;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private issuesFrameworkService: IssuesFrameworkService,
    private discussionsFrameworkService: DiscussionsFrameworkService,
    private issueSnapshotFacade: IssueSnapshotFacade,
    private sendIssueFacade: SendIssueModalFacade,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService
  ) {
    super(initialState);

    this.sendIssueVm$ = this.sendIssueFacade.vm$;
    this.filteredRegisteredUsers$ =
      this.sendIssueFacade.filteredRegisteredUsers$;
  }

  readonly loadIssueClassesAndCategories = this.effect(
    (params$: Observable<LoadClassesAndCategoriesParam>) =>
      params$.pipe(
        tap(() => this.command.setLoading(true)),
        tap(() => this.command.setLoadingIssueCategoryTypes(true)),
        switchMap((params) => {
          return this.issuesFrameworkService
            .issueCategoryTypes(params.assetId)
            .pipe(
              tapResponse(
                (issueCategoryTypes) => {
                  const issueClasses = Array<IssueClassType>();
                  const issueCategories: Record<
                    number,
                    IssueClassesAndCategories[] | null
                  > = {};

                  issueCategoryTypes.forEach((categoryType) => {
                    const dictHasValue =
                      issueCategories?.[categoryType.IssueClassTypeID];
                    if (dictHasValue) {
                      dictHasValue.push(categoryType);
                      issueCategories[categoryType.IssueClassTypeID] =
                        dictHasValue;
                    } else {
                      issueClasses.push(categoryType.IssueClassType);
                      issueCategories[categoryType.IssueClassTypeID] = [
                        categoryType,
                      ];
                    }
                  });
                  const selectedClass = issueClasses[0].AssetIssueClassTypeID;
                  const selectedCategory =
                    issueCategories[selectedClass][0].AssetIssueCategoryTypeID;

                  issueClasses.sort((a, b) =>
                    a.Description.localeCompare(b.Description)
                  );
                  issueCategories[selectedClass].sort((a, b) =>
                    a.CategoryDesc.localeCompare(b.CategoryDesc)
                  );

                  this.command.setCategoriesAndClasses({
                    issueCategories,
                    issueClasses,
                    selectedClass,
                    selectedCategory,
                    errorMessage: '',
                  });
                  this.command.setAssetId(params.assetId);
                  this.command.setAssetIssueIds({
                    assetIssueId: 0,
                    assetIssueGuid: params.assetIssueGuid,
                  });
                  this.command.setLoading(false);
                  this.command.setLoadingIssueCategoryTypes(false);

                  if (params.assetIssueGuid === '-1') {
                    this.newIssue({
                      assetId: params.assetId,
                      selectedCategory,
                    });
                  } else {
                    this.loadIssuesWithOptions(params.assetIssueGuid);
                  }
                },
                (err: string) => {
                  this.command.setLoading(false);
                  this.command.setLoadingIssueCategoryTypes(false);
                  this.command.setErrorMessage('An error occurred.');
                  this.toastService.openSnackBar('An error occurred.', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );
  readonly newIssue = this.effect((newIssueParam$: Observable<NewIssueParam>) =>
    newIssueParam$.pipe(
      tap(() => this.command.setLoading(true)),
      switchMap((newIssueParam) => {
        return this.issuesFrameworkService
          .newAssetIssue(newIssueParam.assetId, newIssueParam.selectedCategory)
          .pipe(
            tapResponse(
              (newIssue) => {
                const assetIssue = newIssue?.Issue;
                this.command.setAssetIssue({
                  assetName: assetIssue?.AssetAndParents?.Asset?.AssetDesc,
                  assetIssueWithOptions: {
                    ...newIssue,
                    Issue: {
                      ...assetIssue,
                      Scorecard: false,
                    },
                  },
                });

                this.command.setAssetIssueIds({
                  assetIssueId: assetIssue?.AssetIssueID,
                  assetIssueGuid: assetIssue?.GlobalID,
                });

                this.command.setLoading(false);
              },
              (err: string) => {
                this.command.setLoading(false);
                this.command.setErrorMessage('An error occurred.');
                this.toastService.openSnackBar('An error occurred.', 'error');
                console.log(err);
              }
            )
          );
      })
    )
  );
  readonly loadIssuesWithOptions = this.effect((issueId$: Observable<string>) =>
    issueId$.pipe(
      tap(() => this.command.setLoading(true)),
      switchMap((issueId) => {
        return this.issuesFrameworkService
          .getAssetIssueWithOptions(issueId)
          .pipe(
            tapResponse(
              (assetIssueWithOptions) => {
                this.issueSnapshotFacade.assetTreeDropdownStateChange(
                  selectAsset(
                    String(assetIssueWithOptions?.Issue?.AssetID),
                    false
                  )
                );
                const assetIssue = assetIssueWithOptions?.Issue;
                this.command.setAssetIssue({
                  assetIssueWithOptions: assetIssueWithOptions,
                  errorMessage: '',
                  assetName: assetIssue?.AssetAndParents?.Asset?.AssetDesc,
                  issueCreationSuccessful: true,
                });

                this.command.setAssetIssueIds({
                  assetIssueId: assetIssue?.AssetIssueID,
                  assetIssueGuid: assetIssue?.GlobalID,
                });
                this.command.setLoading(false);
                this.buildIssueTitle(assetIssue?.IssueTitle);
                this.buildShortSummary(assetIssue?.IssueShortSummary);
              },
              (err: string) => {
                this.command.setLoading(false);
                this.command.setErrorMessage('An error occurred.');
                this.toastService.openSnackBar('An error occurred.', 'error');
                console.log(err);
              }
            )
          );
      })
    )
  );
  readonly saveIssue = this.effect(
    (saveAssetIssueParam$: Observable<SaveAssetIssueParam>) =>
      saveAssetIssueParam$.pipe(
        tap(() => this.command.setLoading(true)),
        switchMap((saveAssetIssueParam) => {
          return this.issuesFrameworkService
            .saveAssetIssue(saveAssetIssueParam.assetIssue)
            .pipe(
              tapResponse(
                (newAssetIssue) => {
                  this.createDiscussion({
                    assetIssueId: newAssetIssue.AssetIssueID,
                    content: newAssetIssue.IssueShortSummary,
                  });

                  newAssetIssue.Scorecard = false;
                  this.command.setAssetIssue({
                    assetIssueWithOptions: {
                      ...saveAssetIssueParam.assetIssueWithOptions,
                      Issue: newAssetIssue,
                    },
                  });

                  this.command.setAssetIssueIds({
                    assetIssueId: newAssetIssue?.AssetIssueID,
                    assetIssueGuid: newAssetIssue?.GlobalID,
                  });
                  this.command.setLoadSendNotification(true);
                  this.command.setLoading(false);

                  this.sendIssueFacade.init({
                    issueTitle: newAssetIssue.IssueTitle,
                    assetIssueGlobalID: newAssetIssue.GlobalID,
                    assetIssueID: newAssetIssue.AssetIssueID,
                    message: '',
                  });

                  if (
                    saveAssetIssueParam.assetIssueWithOptions.Issue
                      .AssetIssueID === -1
                  ) {
                    this.router.navigate([], {
                      relativeTo: this.activatedRoute,
                      queryParams: {
                        ast: saveAssetIssueParam.assetId,
                        iid: newAssetIssue.GlobalID,
                      },
                      queryParamsHandling: 'merge',
                      replaceUrl: true,
                    });
                  }
                },
                (err: string) => {
                  this.command.setLoading(false);
                  this.command.setErrorMessage('An error occurred.');
                  this.toastService.openSnackBar('An error occurred.', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );

  readonly createDiscussion = this.effect(
    (loadDiscussionParam$: Observable<LoadDiscussionParam>) =>
      loadDiscussionParam$.pipe(
        switchMap((loadDiscussionParam) =>
          forkJoin([
            this.discussionsFrameworkService.getDiscussionForAssetIssue(
              loadDiscussionParam.assetIssueId,
              null,
              true,
              false
            ),
            of(loadDiscussionParam.content),
          ])
        ),
        switchMap(([discussion, content]) => {
          return this.discussionsFrameworkService
            .createDiscussionEntry(null, null, null)
            .pipe(
              tapResponse(
                (entry) => {
                  const discussionEntry: IDiscussionEntry = { ...entry };
                  discussionEntry.DiscussionID = discussion.DiscussionID;
                  discussionEntry.Content = content;

                  this.addDiscussionEntryAttachments(discussionEntry);
                  this.command.setDiscussionEntry(discussionEntry);
                  this.command.setLoading(false);
                },
                (err: string) => {
                  this.command.setLoading(false);
                  this.command.setErrorMessage('An error occurred.');
                  this.toastService.openSnackBar('An error occurred.', 'error');
                  console.log(err);
                }
              )
            );
        })
      )
  );

  //Forms
  public buildIssueTitle(value?: string): FormControl<string | null> {
    const issueTitle = new FormControl<string | null>(value, [
      Validators.required,
    ]);
    issueTitle.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((value) => {
        this.command.setTitle(value);
      });
    return issueTitle;
  }

  public buildShortSummary(value?: string): FormControl<string | null> {
    const shortSummary = new FormControl<string | null>(value, [
      Validators.required,
    ]);
    shortSummary.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((value) => {
        this.command.setSummary(value);
      });
    return shortSummary;
  }

  public buildRegisteredRecipientsInput() {
    return this.sendIssueFacade.buildRegisteredRecipientsInput();
  }

  //Methods
  public removeRegisteredRecipient(recipient: ISubscriber) {
    this.sendIssueFacade.removeRegisteredRecipient(recipient);
  }

  public showGroupMembers(recipient: ISubscriber) {
    this.sendIssueFacade.showGroupMembers(recipient);
  }

  public addRegisteredUser(user: ISubscriber) {
    this.sendIssueFacade.addRegisteredUser(user);
  }

  public sendEmail() {
    this.sendIssueFacade.sendEmail();
  }

  public uploadFile(file: File) {
    this.query.assetIssueWithOptions$
      .pipe(take(1))
      .subscribe((assetIssueWithOptions) => {
        if (assetIssueWithOptions && assetIssueWithOptions.Issue) {
          this.command.setLoading(true);
          this.issueSnapshotFacade.uploadSummaryFile(
            assetIssueWithOptions.Issue.AssetID,
            file
          );
        }
      });
  }

  public saveMobileDiscussionEntry(entry: IDiscussionEntry) {
    this.issueSnapshotFacade.saveMobileDiscussionEntry(entry);
  }

  private addDiscussionEntryAttachments(entry: IDiscussionEntry) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue = vm?.assetIssueWithOptions?.Issue;
      const attachments = vm?.fileAttachments;
      if (assetIssue) {
        if (attachments?.length > 0) {
          const discussionAttachments: IAddDiscussionEntryAttachment[] =
            attachments?.map((fileAttachment, idx) => {
              return {
                AssetID: assetIssue.AssetID,
                DiscussionID: entry.DiscussionID,
                EntryID: entry.DiscussionEntryID,
                Title: fileAttachment?.file?.name,
                File: fileAttachment.file,
                DisplayOrder: idx,
              } as IAddDiscussionEntryAttachment;
            });

          this.issueSnapshotFacade.addDiscussionEntryAttachments(
            discussionAttachments
          );
        } else {
          this.issueSnapshotFacade.saveMobileDiscussionEntry(entry);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.command.resetMobileIssueSnapshotState();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
