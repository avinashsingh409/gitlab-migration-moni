import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {
  IIssueSubscriber,
  IIssueSubscribersRetrieval,
  IssuesCoreService,
  IssuesFrameworkService,
  ISubscriber,
  ISubscriberGroup,
  IUpdateIssueSubscribersModel,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import {
  BehaviorSubject,
  combineLatest,
  forkJoin,
  Observable,
  Subject,
} from 'rxjs';
import {
  concatMap,
  debounceTime,
  distinctUntilChanged,
  map,
  take,
  takeUntil,
  tap,
} from 'rxjs/operators';
import { MfPromptDialogComponent } from '../component/manage-followers-dialog/mf-prompt-dialog/mf-prompt-dialog/mf-prompt-dialog.component';

export interface ManageSubscribersModalState {
  assetIssueGlobalID: string;
  issueSubscribers: IIssueSubscriber[];
  subscribedGroups: ISubscriber[];
  users: ISubscriber[];
  filteredUsers: ISubscriber[];
  newSubscribers: ISubscriber[];
  unsubscribedUsers: ISubscriber[];
  newGroups: ISubscriber[];
  isLoading: boolean;
  isSaving: boolean;
  pristine: boolean;
}

const initialState: ManageSubscribersModalState = {
  assetIssueGlobalID: null,
  issueSubscribers: [],
  subscribedGroups: [],
  users: [],
  filteredUsers: [],
  newSubscribers: [],
  unsubscribedUsers: [],
  newGroups: [],
  isLoading: false,
  isSaving: false,
  pristine: true,
};

let _state = initialState;

@Injectable()
export class ManageFollowersModalFacade implements OnDestroy {
  constructor(
    private issueFrameworkService: IssuesFrameworkService,
    private issueCoreService: IssuesCoreService,
    private toastService: ToastService,
    public dialog: MatDialog
  ) {}

  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<ManageSubscribersModalState>(_state);
  private state$ = this.store.asObservable();

  private issueSubscribers$ = this.state$.pipe(
    map((state) => state.issueSubscribers),
    distinctUntilChanged()
  );

  private subscribedGroups$ = this.state$.pipe(
    map((state) => state.subscribedGroups),
    distinctUntilChanged()
  );

  private unsubscribedUsers$ = this.state$.pipe(
    map((state) => state.unsubscribedUsers),
    distinctUntilChanged()
  );

  private newSubscribers$ = this.state$.pipe(
    map((state) => state.newSubscribers),
    distinctUntilChanged()
  );

  private isLoading$ = this.state$.pipe(
    map((state) => state.isLoading),
    distinctUntilChanged()
  );

  private users$ = this.state$.pipe(
    map((state) => state.users),
    distinctUntilChanged()
  );

  private filteredUsers$ = this.state$.pipe(
    map((state) => state.filteredUsers),
    distinctUntilChanged()
  );

  private assetIssueGlobalID$ = this.state$.pipe(
    map((state) => state.assetIssueGlobalID),
    distinctUntilChanged()
  );

  private isSaving$ = this.state$.pipe(
    map((state) => state.isSaving),
    distinctUntilChanged()
  );

  private pristine$ = this.state$.pipe(
    map((state) => state.pristine),
    distinctUntilChanged()
  );

  private newGroups$ = this.state$.pipe(
    map((state) => state.newGroups),
    distinctUntilChanged()
  );

  vm$: Observable<ManageSubscribersModalState> = combineLatest([
    this.issueSubscribers$,
    this.subscribedGroups$,
    this.users$,
    this.filteredUsers$,
    this.isLoading$,
    this.unsubscribedUsers$,
    this.newSubscribers$,
    this.isSaving$,
    this.assetIssueGlobalID$,
    this.pristine$,
    this.newGroups$,
  ]).pipe(
    map(
      ([
        issueSubscribers,
        subscribedGroups,
        users,
        filteredUsers,
        isLoading,
        unsubscribedUsers,
        newSubscribers,
        isSaving,
        assetIssueGlobalID,
        pristine,
        newGroups,
      ]) => {
        return {
          issueSubscribers,
          subscribedGroups,
          users,
          filteredUsers,
          unsubscribedUsers,
          isLoading,
          newSubscribers,
          isSaving,
          assetIssueGlobalID,
          pristine,
          newGroups,
        } as ManageSubscribersModalState;
      }
    )
  );

  buildSelectedUsersControl(): UntypedFormControl {
    const ctrl = new UntypedFormControl();
    ctrl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((val: string) => {
        // eslint-disable-next-line rxjs/no-nested-subscribe
        this.users$.pipe(take(1)).subscribe((users) => {
          if (val && val.length >= 1) {
            const searchRE = new RegExp(val, 'gi');
            const filteredUsers =
              users.filter(({ EmailAddress }) =>
                EmailAddress.match(searchRE)
              ) ?? [];
            this.updateState({
              ..._state,
              filteredUsers,
            });
          }
        });
      });

    return ctrl;
  }

  init(assetIssueGlobalID: string, assetIssueId: number): void {
    if (assetIssueGlobalID) {
      this.updateState({
        ..._state,
        isLoading: true,
      });

      this.issueFrameworkService
        .getAssetSubscription(assetIssueGlobalID)
        .pipe(takeUntil(this.onDestroy))
        // eslint-disable-next-line deprecation/deprecation
        .subscribe(
          (users) => {
            users.forEach((u) => {
              u.groups = [];
              if (u.IsGroup) {
                u.EmailAddress = `${u.EmailAddress} (group)`;
              }
            });

            const unsubscribedUsers = users?.filter((x) => !x.IsSubscribed);
            this.updateState({
              ..._state,
              users: unsubscribedUsers,
            });

            this.issueCoreService
              .getIssueFollowers(assetIssueId)
              .pipe(takeUntil(this.onDestroy))
              // eslint-disable-next-line rxjs/no-nested-subscribe
              .subscribe(
                (issueSubscribers) => {
                  // eslint-disable-next-line prefer-const
                  let userGroups: ISubscriberGroup[] = [];
                  if (!issueSubscribers) {
                    this.updateState({
                      ..._state,
                      isLoading: false,
                    });

                    return;
                  }
                  issueSubscribers.forEach((x) => {
                    if (x?.UserGroups) {
                      const newGroup = x.UserGroups.filter(
                        (u) =>
                          !userGroups.find(
                            (n) => n.SecurityGroupId === u.SecurityGroupId
                          )
                      );
                      if (newGroup.length > 0) {
                        userGroups.push(...x.UserGroups);
                      }
                    }
                  });

                  const subscribedGroups = userGroups.map((g) => {
                    const userGroup = users.find(
                      (x) => x.SecurityUserID === g.SecurityGroupId
                    );
                    if (userGroup) {
                      g.Username = userGroup.UserName;
                    }

                    return {
                      FriendlyName: g.Name,
                      EmailAddress: g.Username,
                      UserName: g.Username,
                      SecurityUserID: g.SecurityGroupId,
                      IsGroup: true,
                      DiscussionAssetMap: null,
                      FirstName: '',
                      IsBV: false,
                      IsSubscribed: true,
                      LastName: '',
                      NERCCIP: false,
                      groupMembers: [],
                      groups: [],
                      isChecked: false,
                      SubscriptionTypes: null,
                      ServiceAccount: false,
                    } as ISubscriber;
                  });

                  this.updateState({
                    ..._state,
                    issueSubscribers: issueSubscribers,
                    users: unsubscribedUsers,
                    isLoading: false,
                  });

                  this.subscribeGroups(
                    subscribedGroups,
                    assetIssueGlobalID,
                    false
                  );
                },
                (err: unknown) => {
                  console.log(err);
                  this.updateState({
                    ..._state,
                    isLoading: false,
                  });

                  this.toastService.openSnackBar(
                    'Unable to get subscribed users, something went wrong',
                    'error'
                  );
                }
              );
          },
          (err: unknown) => {
            console.log(err);
            this.updateState({
              ..._state,
              isLoading: false,
            });
            this.toastService.openSnackBar(
              'Unable to get list of users, something went wrong',
              'error'
            );
          }
        );
    }

    this.newSubscribers$
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((newSubscribers) => {
        if (newSubscribers.length > 0) {
          this.updateState({
            ..._state,
            pristine: false,
          });
        } else {
          this.updateState({
            ..._state,
            pristine: true,
          });
        }
      });

    this.unsubscribedUsers$
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((unsubscribedUsers) => {
        if (unsubscribedUsers.length > 0) {
          this.updateState({
            ..._state,
            pristine: false,
          });
        } else {
          this.updateState({
            ..._state,
            pristine: true,
          });
        }
      });

    this.newGroups$
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((newGroups) => {
        if (newGroups.length > 0) {
          this.updateState({
            ..._state,
            pristine: false,
          });
        } else {
          this.updateState({
            ..._state,
            pristine: true,
          });
        }
      });
  }

  subscribeGroups(
    groups: ISubscriber[],
    assetGuid: string,
    includeUsers: boolean = true
  ): void {
    forkJoin(
      groups.map((g) =>
        this.issueFrameworkService
          .getAssetGroupMembersSubscription(g.UserName, assetGuid)
          .pipe(
            tap((members) => {
              g.groupMembers = members;
            })
          )
      )
    )
      .pipe(takeUntil(this.onDestroy))
      .subscribe((vals) => {
        const subscribedGroups = [..._state.subscribedGroups];
        const newGroups = [..._state.newGroups];
        subscribedGroups.push(...groups);

        this.updateState({
          ..._state,
          subscribedGroups: subscribedGroups,
        });

        if (includeUsers) {
          newGroups.push(...groups);
          this.updateState({
            ..._state,
            newGroups: newGroups,
          });

          groups?.forEach((group) => this.subscribeUsers(group.groupMembers));

          this.updateUserGroupMembership();
        }
      });
  }

  subscribeUsers(users: ISubscriber[]): void {
    const issueSubscribers = [..._state.issueSubscribers];
    const existingUsers = issueSubscribers.filter((x) =>
      users.find((n) => n.SecurityUserID === x.SecurityUserId)
    );

    if (existingUsers.length > 0) {
      users = users.filter(
        (x) => !existingUsers.find((n) => n.SecurityUserId === x.SecurityUserID)
      );
    }

    if (users.length > 0) {
      const newUsers = users.map((u) => {
        return {
          Email: u.EmailAddress,
          SecurityUserId: u.SecurityUserID,
          FriendlyName: u.FriendlyName,
          FirstName: u.FirstName,
          LastName: u.LastName,
          IsSubscribed: true,
        } as IIssueSubscriber;
      });
      issueSubscribers.push(...newUsers);

      const newSubscribers = [..._state.newSubscribers];
      newSubscribers.push(...users);

      this.updateState({
        ..._state,
        issueSubscribers: issueSubscribers,
        newSubscribers: newSubscribers,
      });
    }

    if (existingUsers.length > 0) {
      this.promptExistingUsersWarningMessage(existingUsers);
    }
  }

  updateUserGroupMembership(): void {
    const subscribedUsers = [..._state.issueSubscribers];
    const subscribedGroups = [..._state.subscribedGroups];
    subscribedGroups.forEach((group) => {
      subscribedUsers.forEach((user) => {
        const member = group?.groupMembers.find(
          ({ SecurityUserID }) => SecurityUserID === user.SecurityUserId
        );

        if (member) {
          const userGroup: ISubscriberGroup = {
            Name: group.FriendlyName,
            SecurityGroupId: group.SecurityUserID,
            Username: group.UserName,
          };

          if (!user.UserGroups) {
            user.UserGroups = [];
          }

          if (user.UserGroups.length <= 0) {
            user.UserGroups.push(userGroup);
          }

          const existingUserGroup = user.UserGroups.find(
            (x) => x.SecurityGroupId === userGroup.SecurityGroupId
          );
          if (!existingUserGroup) {
            user.UserGroups.push(userGroup);
          }
        }
      });
    });

    this.updateState({
      ..._state,
      issueSubscribers: subscribedUsers,
    });
  }

  updateSubscriptionStatus(userID: number, isSubscribed: boolean): void {
    const subscribedUsers = [..._state.issueSubscribers];
    const unsubscribedUsers = [..._state.unsubscribedUsers];
    subscribedUsers.forEach((u) => {
      if (u.SecurityUserId === userID) {
        u.IsSubscribed = isSubscribed;

        const unsubscribedUser: ISubscriber = {
          FriendlyName: u.FriendlyName,
          EmailAddress: u.Email,
          UserName: u.Email,
          SecurityUserID: u.SecurityUserId,
          IsGroup: false,
          DiscussionAssetMap: null,
          FirstName: u.FirstName,
          IsBV: false,
          IsSubscribed: isSubscribed,
          LastName: u.LastName,
          NERCCIP: false,
          groupMembers: [],
          groups: [],
          isChecked: false,
          SubscriptionTypes: null,
          ServiceAccount: false,
        } as ISubscriber;

        if (!isSubscribed) {
          unsubscribedUsers.push(unsubscribedUser);
        } else {
          const idx = unsubscribedUsers.indexOf(
            unsubscribedUsers.find(
              ({ SecurityUserID }) =>
                SecurityUserID === unsubscribedUser.SecurityUserID
            )
          );
          unsubscribedUsers.splice(idx, 1);
        }
      }
    });

    this.updateState({
      ..._state,
      issueSubscribers: subscribedUsers,
      unsubscribedUsers: unsubscribedUsers,
    });
  }

  unsubscribeAll(): void {
    const issueSubscribers = [..._state.issueSubscribers];
    const unsubscribedUsers = [..._state.unsubscribedUsers];

    issueSubscribers.forEach((u) => {
      u.IsSubscribed = false;

      const unsubscribedUser: ISubscriber = {
        FriendlyName: u.FriendlyName,
        EmailAddress: u.Email,
        UserName: u.Email,
        SecurityUserID: u.SecurityUserId,
        IsGroup: false,
        DiscussionAssetMap: null,
        FirstName: u.FirstName,
        IsBV: false,
        IsSubscribed: false,
        LastName: u.LastName,
        NERCCIP: false,
        groupMembers: [],
        groups: [],
        isChecked: false,
        SubscriptionTypes: null,
        ServiceAccount: false,
      } as ISubscriber;

      const existinUser = unsubscribedUsers.find(
        (x) => x.SecurityUserID === u.SecurityUserId
      );

      if (!existinUser) {
        unsubscribedUsers.push(unsubscribedUser);
      }
    });

    this.updateState({
      ..._state,
      issueSubscribers: issueSubscribers,
      unsubscribedUsers: unsubscribedUsers,
    });
  }

  saveChanges(assetIssueId: number): Observable<IIssueSubscribersRetrieval> {
    this.updateState({
      ..._state,
      isSaving: true,
    });

    const newSubscribers = [..._state.newSubscribers];
    const unsubscribedUsers = [..._state.unsubscribedUsers];
    const subscribedGroups = [..._state.subscribedGroups];

    const newSubscriberEmails =
      newSubscribers.map((user) => user.UserName) ?? [];
    const unsubscribedUserEmails =
      unsubscribedUsers.map((user) => user.UserName) ?? [];

    // eslint-disable-next-line prefer-const
    let groupsToExclude = [];
    subscribedGroups.forEach((g) => {
      unsubscribedUsers.forEach((u) => {
        const user = g.groupMembers.find(
          (x) => x.SecurityUserID === u.SecurityUserID
        );
        if (user) {
          groupsToExclude.push(g);
        }
      });
    });

    if (groupsToExclude.length > 0) {
      groupsToExclude.forEach((g) => {
        const idx = subscribedGroups.indexOf(g);
        subscribedGroups.splice(idx, 1);
      });
    }

    if (subscribedGroups.length > 0) {
      const groupUsernames = subscribedGroups.map((user) => user.UserName);
      newSubscriberEmails.push(...groupUsernames);
    }

    const updateIssueSubscribersModel: IUpdateIssueSubscribersModel = {
      AssetIssueId: assetIssueId,
      SubscriberEmailIds: newSubscriberEmails,
      UnSubscriberEmailIds: unsubscribedUserEmails,
    };

    return this.issueCoreService.updateIssueSubscribers(
      updateIssueSubscribersModel
    );
  }

  updateSaveState(isSaving: boolean): void {
    this.isSaving$.pipe(take(1)).subscribe((isSavingState) => {
      isSavingState = isSaving;

      this.updateState({
        ..._state,
        isSaving: isSavingState,
      });
    });
  }

  reset(): void {
    this.updateState({ ...initialState });
  }
  ngOnDestroy(): void {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  private promptExistingUsersWarningMessage(users: IIssueSubscriber[]): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '350px';
    dialogConfig.data = {
      message: `The following ${
        users.length > 1 ? 'users are' : 'user is'
      } already subscribed.`,
      users: users,
      isWarn: true,
    };

    const dialogRef = this.dialog.open(MfPromptDialogComponent, dialogConfig);
  }

  private updateState(state: ManageSubscribersModalState): void {
    this.store.next((_state = state));
  }
}
