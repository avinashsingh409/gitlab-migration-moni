import { Injectable, OnDestroy } from '@angular/core';
import { IQuickReplyAttachment, IQuickReplyEntry } from '@atonix/atx-core';
import {
  DiscussionsFrameworkService,
  IssuesFrameworkService,
} from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { QuickReplyFacade } from '../store/facade/quick-reply.facade';

export interface QuickReplyServiceState {
  issueID: string;
  issueTitle: string;
  issueIDNumber: number;
  assetID: number;
  email: string;
  errorMessage: string;
  quickReplyEntries: Array<IQuickReplyEntry>;
  isExpanded: boolean;
  isSaving: boolean;
  isSaved: boolean;
}

const initialState: QuickReplyServiceState = {
  issueID: null,
  issueTitle: null,
  issueIDNumber: null,
  assetID: null,
  email: null,
  errorMessage: null,
  quickReplyEntries: [],
  isExpanded: true,
  isSaving: false,
  isSaved: false,
};
let _state = initialState;

@Injectable({
  providedIn: 'root',
})
export class QuickReplyServiceFacade implements OnDestroy {
  private onDestroy = new Subject<void>();

  private store = new BehaviorSubject<QuickReplyServiceState>(_state);
  private state$ = this.store.asObservable();

  constructor(
    private issuesFrameworkService: IssuesFrameworkService,
    private discussionFrameworkService: DiscussionsFrameworkService,
    private toastService: ToastService,
    private quickReplyFacade: QuickReplyFacade
  ) {}

  issueID$ = this.state$.pipe(
    map((state) => state.issueID),
    distinctUntilChanged()
  );
  issueTitle$ = this.state$.pipe(
    map((state) => state.issueTitle),
    distinctUntilChanged()
  );

  quickReplyEntries$ = this.state$.pipe(
    map((state) => state.quickReplyEntries),
    distinctUntilChanged()
  );

  issueIDNumber$ = this.state$.pipe(
    map((state) => state.issueIDNumber),
    distinctUntilChanged()
  );
  assetID$ = this.state$.pipe(
    map((state) => state.assetID),
    distinctUntilChanged()
  );
  errorMessage$ = this.state$.pipe(
    map((state) => state.errorMessage),
    distinctUntilChanged()
  );

  isExpanded$ = this.state$.pipe(
    map((state) => state.isExpanded),
    distinctUntilChanged()
  );

  isSaving$ = this.state$.pipe(
    map((state) => state.isSaving),
    distinctUntilChanged()
  );

  isSaved$ = this.state$.pipe(
    map((state) => state.isSaved),
    distinctUntilChanged()
  );

  vm$: Observable<QuickReplyServiceState> = combineLatest([
    this.issueID$,
    this.issueTitle$,
    this.issueIDNumber$,
    this.assetID$,
    this.errorMessage$,
    this.quickReplyEntries$,
    this.isExpanded$,
    this.isSaving$,
  ]).pipe(
    map(
      ([
        issueID,
        issueTitle,
        issueIDNumber,
        assetID,
        errorMessage,
        quickReplyEntries,
        isExpanded,
        isSaving,
      ]) => {
        return {
          issueID,
          issueTitle,
          issueIDNumber,
          assetID,
          errorMessage,
          quickReplyEntries,
          isExpanded,
          isSaving,
        } as QuickReplyServiceState;
      }
    )
  );

  init(quickReplyID: string, email: string) {
    this.issuesFrameworkService
      .getIssueIdsFromAlias(quickReplyID)
      .pipe(take(1))
      .subscribe(
        (vals) => {
          this.updateState({
            ..._state,
            issueID: vals.IssueID,
            issueTitle: vals.IssueTitle,
            issueIDNumber: vals.AXID,
            assetID: vals.AssetID,
            email,
          });
        },
        (error: unknown) => {
          console.error(error);
          this.updateState({ ..._state, errorMessage: 'asset not found' });
        }
      );
  }

  toggleNewEntry() {
    this.updateState({
      ..._state,
      isExpanded: !_state.isExpanded,
    });
  }

  saveReply(title: string, content: string): void {
    this.updateState({
      ..._state,
      isSaving: true,
      isSaved: false,
    });
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const entries: IQuickReplyEntry[] = [...vm.quickReplyEntries];
      let entry: IQuickReplyEntry = null;

      if (!content) {
        this.toastService.openSnackBar('Reply must have a content', 'error');
        this.updateState({
          ..._state,
          isSaving: false,
        });
        return;
      }

      entry = {
        IssueID: vm.issueID,
        Email: vm.email,
        Title: title ? title : 'New Discussion Post',
        Content: content,
        Attachments: [],
      };

      this.quickReplyFacade.selectQuickReplyAttachements$
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe((files) => {
          const attachements: IQuickReplyAttachment[] = [...files];
          entry.Attachments = attachements;
        });

      // eslint-disable-next-line rxjs/no-nested-subscribe
      this.discussionFrameworkService.saveQuickReply(entry).subscribe(
        () => {
          entries.push(entry);

          this.updateState({
            ..._state,
            quickReplyEntries: entries,
            isExpanded: false,
            isSaving: false,
            isSaved: true,
          });

          this.toastService.openSnackBar('Discussion Entry Saved', 'success');
        },
        (err: unknown) => {
          this.toastService.openSnackBar(
            `Unable to save reply. ${err}`,
            'error'
          );
        }
      );
    });
  }

  cancelEntry() {
    this.updateState({
      ..._state,
      isExpanded: false,
    });
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  reset() {
    this.updateState({ ...initialState });
  }

  private updateState(state: QuickReplyServiceState) {
    this.store.next((_state = state));
  }
}
