import { Inject, Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import {
  map,
  distinctUntilChanged,
  take,
  debounceTime,
  takeUntil,
} from 'rxjs/operators';
import { PrioritiesSelection } from '../model/issue-model';
import {
  AlertsCoreService,
  IssuesCoreService,
  IssuesFrameworkService,
  ActivityStatus,
  AssetIssueWithOptions,
  IssueClassType,
  ResolutionStatus,
  IssueCauseType,
  IssueClassesAndCategories,
  AssetIssue,
  IssueType,
  IGridParameters,
  AssetFrameworkService,
  ISubscriber,
  IUpdateIssueSubscribersModel,
} from '@atonix/shared/api';

import { UntypedFormControl, Validators } from '@angular/forms';
import moment from 'moment';
import { IssueSnapshotServiceQuery } from './issue-snapshot.service.query';
import { ActivatedRoute, Router } from '@angular/router';
import { replaceHTMLFileElementToBVFiles } from './utilities';
import { ToastService } from '@atonix/shared/utils';
import { NavFacade } from '@atonix/atx-navigation';
import produce from 'immer';
import {
  CalculatorTotals,
  ImpactCalculatorTotals,
} from '../model/impact-calculator/impact-calculator-save';
import { isNil, ITaggingKeyword } from '@atonix/atx-core';
import { _STORE_FEATURES } from '@ngrx/store/src/tokens';
import { IssueSnapshotFacade } from '../store/facade/issue-snapshot.facade';
import { selectAsset } from '@atonix/atx-asset-tree';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

export interface ModalResponse {
  newIssue: AssetIssueWithOptions;
  issueClasses: Array<IssueClassType> | null;
  issueCategories: Record<number, IssueClassesAndCategories[] | null>;
  selectedCategory: number;
  selectedClass: number;
}
export interface IssueSnapshotServiceState {
  issueClasses: Array<IssueClassType> | null;
  issueCategories: Record<number, IssueClassesAndCategories[] | null>;
  issueTypes: IssueType[];
  issueTypeCausesDict: Record<number, IssueCauseType[] | null>;
  defaultIssueCauseTypes: Record<number, IssueCauseType[] | null>;
  activityStatus: Array<ActivityStatus> | null;
  resolutionStatus: Array<ResolutionStatus> | null;
  priorities: Array<PrioritiesSelection> | null;
  calculatorTotals: CalculatorTotals;
  calculatorLoading: boolean;
  selectedClass: number;
  selectedPriority: number;
  loadingIssueCategoryTypes: boolean;
  assetId: number | null;
  assetIssueId: number | null;
  globalId: string | null;
  assetErrorMessage: string;
  assetLoading: boolean;
  issueWithOptions: AssetIssueWithOptions | null;
  modalOpen: boolean;
  pristine: boolean;
  invalidResolveBy: boolean;
  isSubscribed: boolean;
  filteredTags: Array<ITaggingKeyword> | null;
  issueTagQuery: string | null;
  loadingIssueTags: boolean;
  issueTaggingErrorMessage: string | null;
  filteredIssueTypes: IssueType[];
  filteredIssueCauseTypes: IssueCauseType[];
  relatedIssueByAssetCount: number | string | null;
  alertByOwningAssetCount: number | string | null;
  users: Array<ISubscriber> | null;
  filteredUsers: Array<ISubscriber> | null;
}

const _initialState: IssueSnapshotServiceState = {
  issueClasses: null,
  issueCategories: null,
  issueTypes: null,
  issueTypeCausesDict: {},
  defaultIssueCauseTypes: null,
  priorities: null,
  calculatorTotals: null,
  calculatorLoading: true,
  activityStatus: null,
  resolutionStatus: null,
  selectedClass: null,
  selectedPriority: null,
  loadingIssueCategoryTypes: true,
  assetId: null,
  assetIssueId: null,
  globalId: null,
  assetErrorMessage: '',
  assetLoading: true,
  issueWithOptions: null,
  modalOpen: false,
  pristine: true,
  invalidResolveBy: false,
  isSubscribed: false,
  issueTagQuery: null,
  loadingIssueTags: false,
  filteredTags: null,
  issueTaggingErrorMessage: null,
  filteredIssueTypes: null,
  filteredIssueCauseTypes: null,
  relatedIssueByAssetCount: 0,
  alertByOwningAssetCount: 0,
  users: null,
  filteredUsers: null,
};

let _state: IssueSnapshotServiceState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class IssueSnapshotServiceFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<IssueSnapshotServiceState>(_state);
  public query = new IssueSnapshotServiceQuery(this.store);
  reset() {
    this.updateState({ ..._initialState });
  }

  openModal() {
    this.updateState({ ..._state, modalOpen: true });
  }

  newIssue(modalResponse: ModalResponse) {
    // TODO: The user just created this new issue. It should come back from the server as true
    modalResponse.newIssue.Issue.CanEdit = true;

    const priorities = Array<PrioritiesSelection>();
    let selectedPriority = 0;
    modalResponse.newIssue.Priorities.forEach((priority, index) => {
      if (modalResponse.newIssue.Issue.Priority === priority) {
        selectedPriority = index;
      }
      priorities.push({ id: index, name: priority });
    });

    const defaultIssueCauseTypes: Record<number, IssueCauseType[] | null> = {};
    if (modalResponse.newIssue.Issue.IssueTypeID) {
      modalResponse.newIssue.Issue.IssueCauseTypes =
        modalResponse.newIssue.Issue.IssueCauseTypes || [];
      defaultIssueCauseTypes[modalResponse.newIssue.Issue.IssueTypeID] =
        modalResponse.newIssue.Issue.IssueCauseTypes;
      this.getIssueTypeCauses(modalResponse.newIssue.Issue);
    }

    this.updateState({
      ..._state,
      assetLoading: false,
      globalId: modalResponse.newIssue.Issue.GlobalID,
      assetIssueId: modalResponse.newIssue.Issue.AssetIssueID,
      assetId: modalResponse.newIssue.Issue.AssetID,
      priorities: priorities,
      selectedPriority: selectedPriority,
      activityStatus: modalResponse.newIssue.ActivityStatuses,
      resolutionStatus: modalResponse.newIssue.ResolutionStatuses,
      selectedClass: modalResponse.selectedClass,
      issueCategories: modalResponse.issueCategories,
      issueClasses: modalResponse.issueClasses,
      issueWithOptions: {
        ...modalResponse.newIssue,
        Issue: {
          ...modalResponse.newIssue.Issue,
          IssueCauseTypes: modalResponse.newIssue.Issue.IssueCauseTypes || [],
        },
      },
      issueTypes: modalResponse.newIssue.IssueTypes,
      filteredIssueTypes: modalResponse.newIssue.IssueTypes,
      defaultIssueCauseTypes,
      loadingIssueCategoryTypes: false,
      modalOpen: false,
      pristine: false,
      assetErrorMessage: '',
    });

    this.loadUsersByAssetGuid(
      modalResponse.newIssue.Issue?.AssetAndParents?.Asset?.GlobalId
    );
  }

  updateClassAndCategory(modalResponse: ModalResponse) {
    const priorities = Array<PrioritiesSelection>();
    let selectedPriority = 0;
    modalResponse.newIssue.Priorities.forEach((priority, index) => {
      if (modalResponse.newIssue.Issue.Priority === priority) {
        selectedPriority = index;
      }
      priorities.push({ id: index, name: priority });
    });

    if (
      !modalResponse.newIssue.Issue.IssueType.IsBVApproved ||
      modalResponse.newIssue.IssueTypes.findIndex(
        (type) => type.IssueTypeID === modalResponse.newIssue.Issue.IssueTypeID
      ) === -1
    ) {
      modalResponse.newIssue.Issue.IssueTypeID = null;
      modalResponse.newIssue.Issue.IssueType = null;
      modalResponse.newIssue.Issue.IssueCauseTypes = [];
    }

    const defaultIssueCauseTypes: Record<number, IssueCauseType[] | null> = {};
    if (modalResponse.newIssue.Issue.IssueTypeID) {
      modalResponse.newIssue.Issue.IssueCauseTypes =
        modalResponse.newIssue.Issue.IssueCauseTypes || [];
      defaultIssueCauseTypes[modalResponse.newIssue.Issue.IssueTypeID] =
        modalResponse.newIssue.Issue.IssueCauseTypes;
      this.getIssueTypeCauses(modalResponse.newIssue.Issue, true);
    }

    this.updateState({
      ..._state,
      assetLoading: false,
      priorities: priorities,
      selectedPriority: selectedPriority,
      activityStatus: modalResponse.newIssue.ActivityStatuses,
      resolutionStatus: modalResponse.newIssue.ResolutionStatuses,
      selectedClass: modalResponse.selectedClass,
      issueCategories: modalResponse.issueCategories,
      issueClasses: modalResponse.issueClasses,
      issueWithOptions: {
        ...modalResponse.newIssue,
        Issue: {
          ...modalResponse.newIssue.Issue,
          AssetIssueCategoryTypeID: modalResponse.selectedCategory,
        },
      },
      issueTypes: modalResponse.newIssue.IssueTypes,
      filteredIssueTypes: modalResponse.newIssue.IssueTypes,
      filteredIssueCauseTypes: [],
      defaultIssueCauseTypes,
      loadingIssueCategoryTypes: false,
      modalOpen: false,
      pristine: false,
      assetErrorMessage: '',
    });
    this.loadUsersByAssetGuid(
      modalResponse.newIssue.Issue?.AssetAndParents?.Asset?.GlobalId
    );
  }

  init(issueId: string) {
    if (issueId === '-1') {
      this.updateState({
        ..._state,
        globalId: '-1',
        assetId: -1,
        assetLoading: true,
      });
      return;
    }
    this.updateState({ ..._state, assetLoading: true });
    this.issuesFrameworkService
      .getAssetIssueWithOptions(issueId)
      .pipe(take(1))
      .subscribe(
        (val) => {
          this.updateState({
            ..._state,
            assetLoading: false,
            globalId: val.Issue.GlobalID,
            assetIssueId: val.Issue.AssetIssueID,
            assetId: val.Issue.AssetID,
            issueWithOptions: val,
            loadingIssueCategoryTypes: true,
            assetErrorMessage: '',
            pristine: true,
          });

          this.issueSnapshotFacade.assetTreeDropdownStateChange(
            selectAsset(String(val.Issue.AssetID), false)
          );

          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.navFacade.user$.subscribe((user) => {
            if (user) {
              this.issuesFrameworkService
                .getAssetSubscription(val.Issue.GlobalID)
                // eslint-disable-next-line rxjs/no-nested-subscribe
                .subscribe((subscribers) => {
                  const subscriber = subscribers.find(
                    (x) => x.EmailAddress === user.email
                  );

                  const validUsers = subscribers.filter(
                    (x) => !x.IsGroup && !x.ServiceAccount
                  );

                  this.updateState({
                    ..._state,
                    users: validUsers,
                    isSubscribed: subscriber.IsSubscribed,
                  });
                });
            }
          });

          this.issuesFrameworkService
            .issueCategoryTypes(val.Issue.AssetID.toString())
            .pipe(take(1))
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe(
              (issueCategoryTypes) => {
                const activityStatus = Array<ActivityStatus>();
                val.ActivityStatuses.forEach((status) => {
                  activityStatus.push(status);
                });

                // Resolution Status
                let resolutionStatus = Array<ResolutionStatus>();
                if (
                  val.ResolutionStatusTransitions &&
                  val.ResolutionStatusTransitions.length > 0
                ) {
                  const currentResolutionStatus = val.Issue.ResolutionStatusID;
                  const relevantTransitions =
                    val.ResolutionStatusTransitions.filter(
                      (t) =>
                        !t.From ||
                        t.From?.AssetIssueResolutionStatusTypeID ===
                          currentResolutionStatus
                    );
                  if (relevantTransitions.every((t) => t.To)) {
                    const statusDict: { [key: number]: ResolutionStatus } = {};
                    for (const n of val.ResolutionStatuses) {
                      statusDict[n?.AssetIssueResolutionStatusTypeID] = n;
                    }
                    resolutionStatus = relevantTransitions.map(
                      (rt) =>
                        statusDict[rt.To?.AssetIssueResolutionStatusTypeID]
                    );
                    resolutionStatus.push(statusDict[currentResolutionStatus]);
                  } else {
                    resolutionStatus = val.ResolutionStatuses;
                  }
                } else {
                  resolutionStatus = val.ResolutionStatuses;
                }

                resolutionStatus.sort(
                  (a, b) => a.DisplayOrder - b.DisplayOrder
                );
                resolutionStatus = [...new Set(resolutionStatus)];
                if (
                  !val.Issue?.ResolutionStatusID &&
                  resolutionStatus.length > 0
                ) {
                  this.resolutionStatusChange(
                    resolutionStatus[0]?.AssetIssueResolutionStatusTypeID
                  );
                }

                // priorities don't have an id
                const priorities = Array<PrioritiesSelection>();
                let selectedPriority = 0;
                val.Priorities.forEach((priority, index) => {
                  if (val.Issue.Priority === priority) {
                    selectedPriority = index;
                  }
                  priorities.push({ id: index, name: priority });
                });
                const issueClasses = Array<IssueClassType>();
                const issueCategories: Record<
                  number,
                  IssueClassesAndCategories[] | null
                > = {};
                let selectedClass = 0;
                issueCategoryTypes.forEach((categoryType) => {
                  if (
                    categoryType.AssetIssueCategoryTypeID ===
                    val.Issue.AssetIssueCategoryTypeID
                  ) {
                    selectedClass = categoryType.IssueClassTypeID;
                  }
                  const dictHasValue =
                    issueCategories?.[categoryType.IssueClassTypeID];
                  if (dictHasValue) {
                    dictHasValue.push(categoryType);
                    issueCategories[categoryType.IssueClassTypeID] =
                      dictHasValue;
                  } else {
                    issueClasses.push(categoryType.IssueClassType);
                    issueCategories[categoryType.IssueClassTypeID] = [
                      categoryType,
                    ];
                  }
                });

                const issueTypes = val.IssueTypes;
                const defaultIssueCauseTypes: Record<
                  number,
                  IssueCauseType[] | null
                > = {};
                if (val.Issue.IssueTypeID) {
                  val.Issue.IssueCauseTypes = val.Issue.IssueCauseTypes || [];
                  defaultIssueCauseTypes[val.Issue.IssueTypeID] =
                    val.Issue.IssueCauseTypes;
                  this.getIssueTypeCauses(val.Issue);
                }

                this.updateState({
                  ..._state,
                  issueCategories,
                  issueClasses,
                  issueTypes,
                  filteredIssueTypes: issueTypes,
                  defaultIssueCauseTypes,
                  activityStatus,
                  resolutionStatus,
                  priorities,
                  selectedClass,
                  selectedPriority,
                  loadingIssueCategoryTypes: false,
                });
              },
              (error: unknown) => {
                console.log(JSON.stringify(error));
                this.updateState({
                  ..._state,
                  assetErrorMessage: 'Categories not found.',
                });
              }
            );
        },
        (error: unknown) => {
          console.error(error);
          this.updateState({ ..._state, assetErrorMessage: 'asset not found' });
        }
      );
  }

  assetChange(assetId: number) {
    if (
      _state?.issueWithOptions?.Issue?.AssetID &&
      _state?.issueWithOptions?.Issue?.AssetID !== assetId
    ) {
      this.updateState({
        ..._state,
        pristine: false,
        assetId,
        issueWithOptions: {
          ..._state.issueWithOptions,
          Issue: {
            ..._state.issueWithOptions.Issue,
            AssetID: assetId,
          },
        },
      });
    }
  }

  categoryChange(event: number) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          AssetIssueCategoryTypeID: event,
        },
      },
    });
  }

  activityStatusChange(event: number) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          ActivityStatusID: event,
        },
      },
    });
  }

  resolutionStatusChange(event: number) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          ResolutionStatusID: event,
        },
      },
    });
  }

  priorityChange(event: number) {
    this.updateState({ ..._state, selectedPriority: event, pristine: false });
  }

  addIssueType(newIssueType: string) {
    const selected =
      _state?.issueTypes.find(
        (type) =>
          type.IssueTypeDesc.toLowerCase() === newIssueType.toLowerCase()
      ) || null;

    if (selected) {
      this.issueTypeChange(selected);
    } else {
      this.issuesFrameworkService
        .validateIssueType(newIssueType)
        .pipe(take(1))
        // eslint-disable-next-line deprecation/deprecation
        .subscribe(
          (val) => {
            if (val) {
              this.updateState({
                ..._state,
                pristine: false,
                issueWithOptions: {
                  ..._state.issueWithOptions,
                  Issue: {
                    ..._state.issueWithOptions.Issue,
                    IssueCauseTypes: [],
                    IssueType: {
                      IssueTypeID: -1,
                      IssueTypeDesc: newIssueType,
                      IssueTypeAbbrev: undefined,
                      AssetIssueClassType: {
                        ..._state.issueWithOptions.Issue.AssetIssueCategoryType
                          .IssueClassType,
                      },
                      ChangeDate: null,
                      ChangedByUserID: null,
                      CreateDate: null,
                      CreatedByUserID: null,
                      IsBVApproved: false,
                      IssueTypeCausesTypes: null,
                      TrendDirection: null,
                    },
                    IssueTypeID: -1,
                  },
                },
                filteredIssueTypes:
                  _state?.issueTypes.length > 0 ? [..._state.issueTypes] : [],
                filteredIssueCauseTypes: [],
              });
            }
          },
          (error: unknown) => {
            console.error(error);
            this.updateState({
              ..._state,
              assetErrorMessage: 'Error adding new issue type!',
            });
          }
        );
    }
  }

  removeIssueType() {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          IssueCauseTypes: [],
          IssueType: null,
          IssueTypeID: null,
        },
      },
      filteredIssueCauseTypes: [],
    });
  }

  issueTypeChange(issueType: IssueType) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          IssueCauseTypes: _state.defaultIssueCauseTypes[issueType.IssueTypeID]
            ? [..._state.defaultIssueCauseTypes[issueType.IssueTypeID]]
            : [],
          IssueType: issueType,
          IssueTypeID: issueType.IssueTypeID,
        },
      },
      filteredIssueTypes:
        _state?.issueTypes.length > 0 ? [..._state.issueTypes] : [],
    });

    if (
      // eslint-disable-next-line no-constant-condition
      { ..._state.issueTypeCausesDict } ||
      ![..._state.issueTypeCausesDict[issueType.IssueTypeID]]
    ) {
      setTimeout(() => {
        this.getIssueTypeCauses({ ..._state.issueWithOptions.Issue });
      }, 100);
    }
  }

  processIssueCauseType(
    issueCauseTypes: IssueCauseType[],
    selectedPotentialCauses: IssueCauseType[]
  ) {
    const newIssueCauseTypes =
      issueCauseTypes?.length > 0 ? [...issueCauseTypes] : [];
    selectedPotentialCauses.map((cause) => {
      const index = newIssueCauseTypes.findIndex(
        (type) => type.IssueCauseTypeID === cause.IssueCauseTypeID
      );

      if (index > -1) {
        newIssueCauseTypes.splice(index, 1);
      }
    });

    return newIssueCauseTypes;
  }

  addIssueCauseType(newIssueCauseType: string) {
    const exists = _state.issueWithOptions.Issue.IssueCauseTypes?.find(
      (type) =>
        type.IssueCauseDesc.toLowerCase() === newIssueCauseType.toLowerCase()
    );

    if (exists) {
      this.snackBarService.openSnackBar('Issue Cause Type Invalid!', 'error');
    } else {
      const selected = _state.issueTypeCausesDict[
        _state.issueWithOptions.Issue.IssueType.IssueTypeID
      ]?.find(
        (type) =>
          type.IssueCauseDesc.toLowerCase() === newIssueCauseType.toLowerCase()
      );

      if (selected) {
        this.issueCauseTypeChange(selected);
      } else {
        this.issuesFrameworkService
          .validateIssueCauseType(newIssueCauseType)
          .pipe(take(1))
          // eslint-disable-next-line deprecation/deprecation
          .subscribe(
            (val) => {
              if (val) {
                const causeType: IssueCauseType = {
                  IssueCauseTypeID: -1,
                  IssueCauseDesc: newIssueCauseType,
                  IssueCauseAbbrev: newIssueCauseType,
                  IssueTypeID:
                    _state.issueWithOptions.Issue.IssueType.IssueTypeID,
                  displayOrder: 0,
                  ChangeDate: null,
                  ChangedByUserID: null,
                  CreateDate: null,
                  CreatedByUserID: null,
                  IsBVApproved: false,
                  IssueCauseTypeVariableTypeMaps: null,
                };

                const IssueCauseTypes = [
                  ..._state.issueWithOptions.Issue.IssueCauseTypes,
                ];
                IssueCauseTypes.push(causeType);

                const filteredIssueCauseTypes = this.processIssueCauseType(
                  _state.issueTypeCausesDict[
                    _state.issueWithOptions.Issue.IssueType.IssueTypeID
                  ],
                  IssueCauseTypes
                );

                this.updateState({
                  ..._state,
                  pristine: false,
                  issueWithOptions: {
                    ..._state.issueWithOptions,
                    Issue: {
                      ..._state.issueWithOptions.Issue,
                      IssueCauseTypes,
                    },
                  },
                  filteredIssueCauseTypes,
                });
              }
            },
            (error: unknown) => {
              console.error(error);
              this.updateState({
                ..._state,
                assetErrorMessage: 'Error adding new issue type!',
              });
            }
          );
      }
    }
  }

  removeIssueCauseType(issueCauseTypeID: number) {
    const IssueCauseTypes = [..._state.issueWithOptions.Issue.IssueCauseTypes];
    const index = IssueCauseTypes.findIndex(
      (type) => type.IssueCauseTypeID === issueCauseTypeID
    );
    IssueCauseTypes.splice(index, 1);

    const filteredIssueCauseTypes = this.processIssueCauseType(
      _state.issueTypeCausesDict[
        _state.issueWithOptions.Issue.IssueType.IssueTypeID
      ],
      IssueCauseTypes
    );

    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          IssueCauseTypes,
        },
      },
      filteredIssueCauseTypes,
    });
  }

  issueCauseTypeChange(newIssueCauseType: IssueCauseType) {
    const IssueCauseTypes = [..._state.issueWithOptions.Issue.IssueCauseTypes];
    IssueCauseTypes.push(newIssueCauseType);

    const filteredIssueCauseTypes = this.processIssueCauseType(
      _state.issueTypeCausesDict[
        _state.issueWithOptions.Issue.IssueType.IssueTypeID
      ],
      IssueCauseTypes
    );

    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          IssueCauseTypes,
        },
      },
      filteredIssueCauseTypes,
    });
  }

  isScoreCardChange(event: boolean) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          Scorecard: event,
        },
      },
    });
  }

  saveSummary(summaryContent: string) {
    this.updateState({
      ..._state,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          IssueSummary: replaceHTMLFileElementToBVFiles(summaryContent),
        },
      },
    });
  }

  getIssueTypeCauses(issue: AssetIssue, validateSelectedCauseTypes?: boolean) {
    const issueTypeCausesDict: Record<number, IssueCauseType[] | null> = {};

    this.issuesFrameworkService
      .getIssueTypeCauses(issue.IssueTypeID)
      .pipe(take(1))
      // eslint-disable-next-line deprecation/deprecation
      .subscribe(
        (val) => {
          issueTypeCausesDict[issue.IssueTypeID] = val;

          const selectedIssueCauseTypes = [...issue.IssueCauseTypes] || [];
          // If true, only those selected cause types that are present on the dropdown will be displayed.
          if (validateSelectedCauseTypes) {
            [...issue.IssueCauseTypes]?.map((t, index) => {
              if (
                (!t.IsBVApproved && t.IssueTypeID !== issue.IssueTypeID) ||
                val.findIndex((v) => v.IssueTypeID === t.IssueTypeID) === -1
              ) {
                selectedIssueCauseTypes.splice(index, 1);
              }
            });
          }

          if (selectedIssueCauseTypes?.length > 0) {
            selectedIssueCauseTypes.map((type) => {
              const foundType = val.findIndex(
                (v) => v.IssueCauseTypeID === type.IssueCauseTypeID
              );
              if (foundType === -1) {
                issueTypeCausesDict[issue.IssueTypeID].push(type);
              }
            });
          }

          const filteredIssueCauseTypes = this.processIssueCauseType(
            issueTypeCausesDict[issue.IssueTypeID],
            selectedIssueCauseTypes
          );

          this.updateState({
            ..._state,
            issueWithOptions: {
              ..._state.issueWithOptions,
              Issue: {
                ..._state.issueWithOptions.Issue,
                IssueCauseTypes: selectedIssueCauseTypes,
              },
            },
            issueTypeCausesDict,
            filteredIssueCauseTypes,
          });
        },
        (error: unknown) => {
          console.error(error);
          this.updateState({
            ..._state,
            assetErrorMessage: 'Potential Causes not found!',
          });
        }
      );
  }

  save() {
    this.updateState({
      ..._state,
      assetLoading: true,
    });

    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;
      // priority is a unique bird, just an array of strings insted of ids
      assetIssue.Priority = vm.priorities[vm.selectedPriority].name;

      this.issuesFrameworkService
        .saveAssetIssue(assetIssue)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (val) => {
            if (val.AssignedTo) {
              const subscriberUsersParam: IUpdateIssueSubscribersModel = {
                AssetIssueId: _state.assetIssueId,
                SubscriberEmailIds: [val.AssignedTo],
                UnSubscriberEmailIds: [],
              };

              this.issuesCoreService
                .updateIssueSubscribers(subscriberUsersParam)
                .pipe(take(1))
                // eslint-disable-next-line rxjs/no-nested-subscribe
                .subscribe(
                  (res) => {
                    if (res.Success) {
                      //do something
                    }
                  },
                  (err: unknown) => {
                    console.log(err);
                  }
                );
            }

            this.snackBarService.openSnackBar('Issue saved', 'success');
            if (val) {
              this.updateState({
                ..._state,
                assetLoading: false,
                globalId: val.GlobalID,
                assetIssueId: val.AssetIssueID,
                assetId: val.AssetID,
                issueWithOptions: {
                  ..._state.issueWithOptions,
                  Issue: val,
                },
                loadingIssueCategoryTypes: false,
                assetErrorMessage: '',
                pristine: true,
              });
              if (vm.assetIssueId === -1) {
                // we just created a new issue, so we need to update the route.
                this.router.navigate([], {
                  relativeTo: this.activatedRoute,
                  queryParams: {
                    ast: null,
                    iid: val.GlobalID,
                    aid: val.AssetID,
                  },
                  queryParamsHandling: 'merge',
                  replaceUrl: true,
                });
              }
            }
          },
          (error: unknown) => {
            console.error(error);
            this.updateState({
              ..._state,
              assetErrorMessage: 'save failed',
              assetLoading: false,
            });
          }
        );
    });
  }

  deleteAssetIssue() {
    this.updateState({
      ..._state,
      assetLoading: true,
    });

    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;

      this.issuesFrameworkService
        .deleteAssetIssue(assetIssue.AssetIssueID)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (val) => {
            if (val) {
              this.snackBarService.openSnackBar(
                'Asset issue deleted.',
                'success'
              );
              self.close();
              this.updateState({
                ..._state,
                assetLoading: false,
                assetErrorMessage: '',
                pristine: true,
              });
            }
          },
          (error: unknown) => {
            console.error(error);
            const err = 'Deletion failed, something went wrong.';
            this.snackBarService.openSnackBar(err, 'error');
            this.updateState({
              ..._state,
              assetErrorMessage: err,
              assetLoading: false,
            });
          }
        );
    });
  }

  subscribeUser() {
    this.updateState({
      ..._state,
      assetLoading: true,
    });

    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;

      this.issuesFrameworkService
        .subscribeUser(assetIssue.AssetIssueID)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (val) => {
            if (val) {
              if (val.SuccessMessage) {
                this.snackBarService.openSnackBar(
                  val.SuccessMessage,
                  'success'
                );

                this.updateState({
                  ..._state,
                  assetLoading: false,
                  pristine: true,
                  isSubscribed: true,
                });
              } else {
                this.snackBarService.openSnackBar(val.SuccessMessage, 'error');
              }
            }
          },
          (error: unknown) => {
            console.error(error);
            const err = 'Subscription failed, something went wrong.';
            this.snackBarService.openSnackBar(err, 'error');
            this.updateState({
              ..._state,
              assetErrorMessage: err,
              assetLoading: false,
            });
          }
        );
    });
  }

  unsubscribeUser() {
    this.updateState({
      ..._state,
      assetLoading: true,
    });

    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;
      // eslint-disable-next-line rxjs/no-nested-subscribe
      this.navFacade.user$.subscribe((user) => {
        if (user) {
          this.issuesFrameworkService
            .getAssetSubscription(assetIssue.GlobalID)
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe((subscribers) => {
              const subscriber = subscribers.find(
                (x) => x.EmailAddress === user.email
              );
              this.issuesFrameworkService
                .unSubscribeUser(assetIssue.GlobalID, [subscriber.EmailAddress])
                .pipe(take(1))
                // eslint-disable-next-line rxjs/no-nested-subscribe
                .subscribe(
                  (val) => {
                    if (val) {
                      if (val.SuccessMessage) {
                        this.snackBarService.openSnackBar(
                          'Successfully unsubscribed user.',
                          'success'
                        );

                        this.updateState({
                          ..._state,
                          assetLoading: false,
                          pristine: true,
                          isSubscribed: false,
                        });
                      } else {
                        this.snackBarService.openSnackBar(
                          val.ErrorMessage,
                          'error'
                        );
                      }
                    }
                  },
                  (error: unknown) => {
                    console.error(error);
                    const err = 'Unsubscription failed, something went wrong.';
                    this.snackBarService.openSnackBar(err, 'error');
                    this.updateState({
                      ..._state,
                      assetErrorMessage: err,
                      assetLoading: false,
                    });
                  }
                );

              this.updateState({
                ..._state,
                isSubscribed: subscriber.IsSubscribed,
              });
            });
        }
      });
    });
  }

  send() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;

      window
        .open(
          this.appConfig.baseSiteURL +
            '/IssuesManagement/SendIssue.html#!?iid=' +
            assetIssue.GlobalID +
            '&ac=Issues%20Management'
        )
        .focus();
    });
  }

  buildIssueTitle(): UntypedFormControl {
    const issueTitle = new UntypedFormControl(null, [Validators.required]);
    issueTitle.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          pristine: false,
          issueWithOptions: {
            ..._state.issueWithOptions,
            Issue: {
              ..._state.issueWithOptions.Issue,
              IssueTitle: value,
            },
          },
        });
      });
    return issueTitle;
  }

  buildShortSummary(): UntypedFormControl {
    const shortSummary = new UntypedFormControl(null, []);
    shortSummary.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          pristine: false,
          issueWithOptions: {
            ..._state.issueWithOptions,
            Issue: {
              ..._state.issueWithOptions.Issue,
              IssueShortSummary: value,
            },
          },
        });
      });
    return shortSummary;
  }

  buildResolveByDate(): UntypedFormControl {
    const startDate = new UntypedFormControl(null, []);
    startDate.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid() ||
          (startDate.valid && value === null)
        ) {
          const resolveByStartMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
          const resolveByEndMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();
          if (
            (value > resolveByStartMin && value < resolveByEndMax) ||
            !value
          ) {
            this.updateState({
              ..._state,
              pristine: false,
              invalidResolveBy: false,
              issueWithOptions: {
                ..._state.issueWithOptions,
                Issue: {
                  ..._state.issueWithOptions.Issue,
                  ResolveBy: value,
                },
              },
            });
          } else {
            this.updateState({
              ..._state,
              invalidResolveBy: true,
            });
          }
        }
      });
    return startDate;
  }

  buildIssueTagQuery(): UntypedFormControl {
    const issueTags = new UntypedFormControl(null, []);
    issueTags.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((value) => {
        this.updateState({
          ..._state,
          loadingIssueTags: true,
          issueTagQuery: value,
        });
      });

    return issueTags;
  }

  buildIssueTypeInput(): UntypedFormControl {
    const issueType = new UntypedFormControl(null);
    issueType.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (value) {
          const newfiltered = _state.issueTypes.filter(
            (type) =>
              type.IssueTypeDesc.toLowerCase().indexOf(value.toLowerCase()) >= 0
          );
          this.updateState({
            ..._state,
            pristine: false,
            filteredIssueTypes: newfiltered ? newfiltered : [],
          });
        } else {
          this.updateState({
            ..._state,
            pristine: false,
            filteredIssueTypes:
              _state?.issueTypes.length > 0 ? [..._state.issueTypes] : [],
          });
        }
      });
    return issueType;
  }

  buildIssueCauseTypesInput(): UntypedFormControl {
    const potentialCauses = new UntypedFormControl(null);
    potentialCauses.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        const filteredIssueCauseTypes = this.processIssueCauseType(
          _state.issueTypeCausesDict[
            _state.issueWithOptions.Issue.IssueType.IssueTypeID
          ],
          _state.issueWithOptions.Issue.IssueCauseTypes
        );

        if (value) {
          const newfiltered = filteredIssueCauseTypes.filter(
            (type) =>
              type.IssueCauseDesc.toLowerCase().indexOf(value.toLowerCase()) >=
              0
          );
          this.updateState({
            ..._state,
            pristine: false,
            filteredIssueCauseTypes: newfiltered ? newfiltered : [],
          });
        } else {
          this.updateState({
            ..._state,
            pristine: false,
            filteredIssueCauseTypes,
          });
        }
      });
    return potentialCauses;
  }

  buildIssueAssignedToInput(val: string = null): UntypedFormControl {
    const assignedTo = new UntypedFormControl(val);
    assignedTo.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((value) => {
        if (value) {
          // eslint-disable-next-line rxjs/no-nested-subscribe
          this.query.users$.pipe(take(1)).subscribe((users) => {
            const filteredUsers =
              users.filter(
                (user) =>
                  user.EmailAddress.toLowerCase().indexOf(
                    value.toLowerCase()
                  ) >= 0
              ) ?? [];

            if (filteredUsers.length <= 0) {
              assignedTo.setErrors({ invalidUser: true });
            }

            this.updateState({
              ..._state,
              filteredUsers,
            });
          });
        } else {
          this.updateState({
            ..._state,
            pristine: false,
            issueWithOptions: {
              ..._state.issueWithOptions,
              Issue: {
                ..._state.issueWithOptions.Issue,
                AssignedTo: null,
              },
            },
          });
        }
      });
    return assignedTo;
  }

  searchTag(tag: string) {
    this.updateState({
      ..._state,
      loadingIssueTags: true,
      issueTaggingErrorMessage: null,
    });
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const assetIssue: AssetIssue = vm.issueWithOptions.Issue;
      this.assetFrameworkService
        .searchTaggingKeywords(assetIssue.AssetID.toString(), tag)
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (tags) => {
            this.updateState({
              ..._state,
              loadingIssueTags: false,
              filteredTags: tags,
            });
          },
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          (error) => {
            console.error(error);
            this.updateState({
              ..._state,
              assetErrorMessage: error,
              assetLoading: false,
            });
          }
        );
    });
  }

  removeTag(tag: ITaggingKeyword) {
    this.query.issueWithOptions$.subscribe((issueWithOptions) => {
      const index = issueWithOptions.Issue.Keywords.indexOf(tag);
      if (index >= 0) {
        const tags = issueWithOptions.Issue.Keywords.slice();
        tags.splice(index, 1);

        this.updateState({
          ..._state,
          pristine: false,
          issueWithOptions: {
            ..._state.issueWithOptions,
            Issue: {
              ..._state.issueWithOptions.Issue,
              Keywords: tags,
            },
          },
        });
      }
    });
  }

  addTag(tag: ITaggingKeyword): Observable<ITaggingKeyword> {
    this.updateState({
      ..._state,
      loadingIssueTags: true,
      issueTaggingErrorMessage: null,
    });

    const issueTag = new Subject<ITaggingKeyword>();
    this.query.issueWithOptions$.pipe(take(1)).subscribe((issueWithOptions) => {
      const assetIssue: AssetIssue = issueWithOptions.Issue;
      this.assetFrameworkService
        .isReservedKeyword(assetIssue.AssetID.toString(), tag.Text)
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (isReservedKeyword) => {
            if (!isReservedKeyword) {
              this.assetFrameworkService
                .addTaggingKeyword(assetIssue.AssetID.toString(), tag.Text)
                .pipe(take(1))
                // eslint-disable-next-line rxjs/no-nested-subscribe
                .subscribe((tags) => {
                  issueTag.next(tags[0]);
                });
            } else {
              this.updateState({
                ..._state,
                issueTaggingErrorMessage: `${tag.Text} is a reserved word.`,
                loadingIssueTags: false,
              });
            }
          },
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          (error) => {
            console.error(error);
            this.updateState({
              ..._state,
              issueTaggingErrorMessage: error,
              loadingIssueTags: false,
            });
          }
        );
    });

    return issueTag.asObservable();
  }

  selectTag(tag: ITaggingKeyword) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const tags = vm.issueWithOptions.Issue.Keywords.slice();
      const existingTag = tags.filter((x) => x.KeywordId === tag.KeywordId);
      if (existingTag && existingTag.length > 0) {
        this.updateState({
          ..._state,
          loadingIssueTags: false,
          filteredTags: null,
          issueTaggingErrorMessage: `${tag.Text} has already been used`,
        });

        return;
      }

      if (tag.KeywordId === -1) {
        this.addTag(tag)
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((issueTag) => {
            tags.push(issueTag);
            this.updateState({
              ..._state,
              pristine: false,
              loadingIssueTags: false,
              issueTagQuery: null,
              filteredTags: null,
              issueWithOptions: {
                ..._state.issueWithOptions,
                Issue: {
                  ..._state.issueWithOptions.Issue,
                  Keywords: tags,
                },
              },
            });
          });
      } else {
        tags.push(tag);
        this.updateState({
          ..._state,
          pristine: false,
          loadingIssueTags: false,
          issueTagQuery: null,
          filteredTags: null,
          issueWithOptions: {
            ..._state.issueWithOptions,
            Issue: {
              ..._state.issueWithOptions.Issue,
              Keywords: tags,
            },
          },
        });
      }
    });
  }

  updateModalState(isOpen: boolean) {
    this.updateState({
      ..._state,
      modalOpen: isOpen,
    });
  }

  updateChangedBy(changedBy: string, changeDate: Date, assetIssueID: string) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const issueWithOptions = produce(vm.issueWithOptions, (draftState) => {
        draftState.Issue.ChangeDate = changeDate;
        draftState.Issue.ChangedBy = changedBy;
      });
      this.updateState({ ..._state, issueWithOptions });
    });
    this.updateImpactCosts(assetIssueID);
  }

  updateImpactCosts(assetIssueID: string) {
    this.updateState({ ..._state, calculatorLoading: true });
    this.issuesFrameworkService
      .updateImpactCosts(assetIssueID)
      .pipe(take(1))
      // eslint-disable-next-line deprecation/deprecation
      .subscribe(
        (costs: ImpactCalculatorTotals) => {
          if (costs?.Results && costs?.Results.length > 0) {
            this.updateState({
              ..._state,
              calculatorTotals: costs.Results[0],
              calculatorLoading: false,
            });
          } else {
            this.updateState({ ..._state, calculatorLoading: false });
          }
        },
        (error: unknown) => {
          console.error(error);
          this.updateState({ ..._state, calculatorLoading: false });
        }
      );
  }

  getRelatedIssueByAssetCount(uniqueKey: string) {
    const gridParams: IGridParameters = {
      startRow: 0,
      endRow: 100,
      rowGroupCols: [],
      valueCols: [],
      pivotCols: [],
      groupKeys: [],
      pivotMode: false,
      filterModel: {
        ActivityStatus: {
          filterType: 'set',
          values: ['Open'],
        },
        AssetID: {
          filter: uniqueKey,
          filterType: 'text',
          type: 'contains',
        },
      },
      sortModel: [],
      displayCols: [],
    };

    this.updateState({
      ..._state,
      relatedIssueByAssetCount: 0,
    });

    this.issuesCoreService
      .getIssues(gridParams)
      .pipe(take(1))
      .subscribe((issues) => {
        this.updateState({
          ..._state,
          relatedIssueByAssetCount: this.convertToShortNumber(
            issues.Results[0].NumIssues
          ),
        });
      });
  }

  getAlertByOwningAssetCount(uniqueKey: string) {
    this.updateState({
      ..._state,
      alertByOwningAssetCount: 0,
    });
    this.alertsCoreService
      .getAlertsCountByAsset(uniqueKey)
      .pipe(take(1))
      .subscribe((alertsCount) => {
        this.updateState({
          ..._state,
          alertByOwningAssetCount: this.convertToShortNumber(alertsCount),
        });
      });
  }

  assignedToChange(userEmail: string) {
    this.updateState({
      ..._state,
      pristine: false,
      issueWithOptions: {
        ..._state.issueWithOptions,
        Issue: {
          ..._state.issueWithOptions.Issue,
          AssignedTo: userEmail,
        },
      },
    });
  }

  loadUsersByAssetGuid(assetGuid: string) {
    this.issuesFrameworkService
      .getAssetSubscription(assetGuid)
      .subscribe((subscribers) => {
        const validUsers = subscribers.filter(
          (x) => !x.IsGroup && !x.ServiceAccount
        );

        this.updateState({
          ..._state,
          users: validUsers,
        });
      });
  }

  constructor(
    private alertsCoreService: AlertsCoreService,
    private issuesCoreService: IssuesCoreService,
    private issuesFrameworkService: IssuesFrameworkService,
    private assetFrameworkService: AssetFrameworkService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBarService: ToastService,
    private navFacade: NavFacade,
    private issueSnapshotFacade: IssueSnapshotFacade,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  private updateState(newState: IssueSnapshotServiceState) {
    this.store.next((_state = newState));
  }

  private convertToShortNumber(number: number) {
    if (isNaN(number)) return null;
    if (number === null) return null;
    if (number === 0) return null;

    let abs = Math.abs(number);
    const isNegative = number < 0;
    let key = '';

    const powers = [
      { key: 'Q', value: Math.pow(10, 15) },
      { key: 'T', value: Math.pow(10, 12) },
      { key: 'B', value: Math.pow(10, 9) },
      { key: 'M', value: Math.pow(10, 6) },
      { key: 'K', value: 1000 },
    ];

    for (let i = 0; i < powers.length; i++) {
      let reduced = abs / powers[i].value;
      const decimals = reduced - Math.floor(reduced);
      reduced = +Math.floor(reduced).toFixed();
      if (reduced >= 1) {
        abs = reduced;
        key = powers[i].key + (+decimals > 0 ? '+' : '');
        break;
      }
    }
    return (isNegative ? '-' : '') + abs + key;
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
