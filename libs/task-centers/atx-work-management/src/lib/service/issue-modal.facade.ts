import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { selectAsset } from '@atonix/atx-asset-tree';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { delay, distinctUntilChanged, map, take } from 'rxjs/operators';
import { DialogData } from '../component/issue-snapshot/issue-snapshot.component';
import {
  IssuesFrameworkService,
  AssetIssueWithOptions,
  IssueClassType,
  IssueClassesAndCategories,
} from '@atonix/shared/api';

import { IssueSnapshotFacade } from '../store/facade/issue-snapshot.facade';

export interface IssueModalState {
  issueClasses: Array<IssueClassType> | null;
  issueCategories: Record<number, IssueClassesAndCategories[] | null>;
  selectedClass: number;
  selectedCategory: number;
  categoryErrorMessage: string;
  loadingIssueCategoryTypes: boolean;
  asset: string | null;
  newIssueLoading: boolean;
  updateIssueLoading: boolean;
  newIssue: AssetIssueWithOptions | null;
}

const initialState: IssueModalState = {
  issueClasses: null,
  issueCategories: null,
  selectedClass: null,
  selectedCategory: null,
  categoryErrorMessage: '',
  loadingIssueCategoryTypes: false,
  asset: null,
  newIssueLoading: false,
  updateIssueLoading: false,
  newIssue: null,
};
let _state = initialState;

@Injectable()
export class IssueModalFacade implements OnDestroy {
  constructor(
    private issuesFrameworkService: IssuesFrameworkService,
    private issueSnapshotFacade: IssueSnapshotFacade
  ) {}

  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<IssueModalState>(_state);
  private state$ = this.store.asObservable();

  private issueClasses$ = this.state$.pipe(
    map((state) => state.issueClasses),
    distinctUntilChanged()
  );
  private issueCategories$ = this.state$.pipe(
    map((state) => state.issueCategories),
    distinctUntilChanged()
  );
  private categoryErrorMessage$ = this.state$.pipe(
    map((state) => state.categoryErrorMessage),
    distinctUntilChanged()
  );
  private selectedClass$ = this.state$.pipe(
    map((state) => state.selectedClass),
    distinctUntilChanged()
  );

  private selectedCategory$ = this.state$.pipe(
    map((state) => state.selectedCategory),
    distinctUntilChanged()
  );

  private loadingIssueCategoryTypes$ = this.state$.pipe(
    map((state) => state.loadingIssueCategoryTypes),
    distinctUntilChanged()
  );

  private asset$ = this.state$.pipe(
    map((state) => state.asset),
    distinctUntilChanged()
  );

  private newIssueLoading$ = this.state$.pipe(
    map((state) => state.newIssueLoading),
    distinctUntilChanged()
  );

  private updateIssueLoading$ = this.state$.pipe(
    map((state) => state.updateIssueLoading),
    distinctUntilChanged()
  );

  newIssue$ = this.state$.pipe(
    map((state) => state.newIssue),
    distinctUntilChanged()
  );

  vm$: Observable<IssueModalState> = combineLatest([
    this.issueClasses$,
    this.issueCategories$,
    this.categoryErrorMessage$,
    this.selectedClass$,
    this.selectedCategory$,
    this.loadingIssueCategoryTypes$,
    this.asset$,
    this.newIssueLoading$,
    this.updateIssueLoading$,
    this.newIssue$,
  ]).pipe(
    map(
      ([
        issueClasses,
        issueCategories,
        categoryErrorMessage,
        selectedClass,
        selectedCategory,
        loadingIssueCategoryTypes,
        asset,
        newIssueLoading,
        updateIssueLoading,
        newIssue,
      ]) => {
        return {
          issueClasses,
          issueCategories,
          categoryErrorMessage,
          selectedClass,
          selectedCategory,
          loadingIssueCategoryTypes,
          asset,
          newIssueLoading,
          updateIssueLoading,
          newIssue,
        } as IssueModalState;
      }
    )
  );

  categoryChange(event: number) {
    this.updateState({ ..._state, selectedCategory: event });
  }

  createIssue() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.updateState({ ..._state, newIssueLoading: true });
      this.issuesFrameworkService
        .newAssetIssue(vm.asset, vm.selectedCategory)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (newIssue) => {
            this.issueSnapshotFacade.assetTreeDropdownStateChange(
              selectAsset(String(newIssue.Issue.AssetID), false)
            );
            this.updateState({
              ..._state,
              newIssueLoading: false,
              newIssue: {
                ...newIssue,
                Issue: {
                  ...newIssue.Issue,
                  Scorecard: false,
                },
              },
            });
          },
          (error: unknown) => {
            console.error(error);
            this.updateState({
              ..._state,
              newIssueLoading: false,
              categoryErrorMessage: 'An error occurred.',
            });
          }
        );
    });
  }

  updateIssue(assetIssue: AssetIssueWithOptions) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      this.updateState({ ..._state, updateIssueLoading: true });
      this.issuesFrameworkService
        .newAssetIssue(vm.asset, vm.selectedCategory)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (newIssue) => {
            this.updateState({
              ..._state,
              updateIssueLoading: false,
              newIssue: {
                ...assetIssue,
                ResolutionStatuses: newIssue.ResolutionStatuses,
                ResolutionStatusTransitions:
                  newIssue.ResolutionStatusTransitions,
                ActivityStatusTransitions: newIssue.ActivityStatusTransitions,
                ActivityStatuses: newIssue.ActivityStatuses,
                Impacts: newIssue.Impacts,
                IssueTypes: newIssue.IssueTypes,
                Issue: {
                  ...assetIssue.Issue,
                  ResolutionStatusID: newIssue.Issue.ResolutionStatusID,
                  ResolutionStatus: newIssue.Issue.ResolutionStatus,
                  ActivityStatus: newIssue.Issue.ActivityStatus,
                  ActivityStatusID: newIssue.Issue.ActivityStatusID,
                },
              },
            });
          },
          (error: unknown) => {
            console.error(error);
            this.updateState({
              ..._state,
              updateIssueLoading: false,
              categoryErrorMessage: 'An error occurred.',
            });
          }
        );
    });
  }

  classChange(event: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const selectedCategory =
        vm.issueCategories[event][0].AssetIssueCategoryTypeID;
      this.updateState({ ..._state, selectedCategory, selectedClass: event });
    });
  }

  init(modalData: DialogData) {
    this.updateState({ ...initialState });
    if (modalData.error) {
      this.updateState({
        ..._state,
        loadingIssueCategoryTypes: false,
        categoryErrorMessage: 'Asset Not Found Error.',
      });
      return;
    }
    this.updateState({
      ..._state,
      loadingIssueCategoryTypes: true,
    });
    this.issuesFrameworkService
      .issueCategoryTypes(modalData.asset)
      .pipe(take(1))
      .subscribe(
        (issueCategoryTypes) => {
          const issueClasses = Array<IssueClassType>();
          const issueCategories: Record<
            number,
            IssueClassesAndCategories[] | null
          > = {};

          issueCategoryTypes.forEach((categoryType) => {
            const dictHasValue =
              issueCategories?.[categoryType.IssueClassTypeID];
            if (dictHasValue) {
              dictHasValue.push(categoryType);
              issueCategories[categoryType.IssueClassTypeID] = dictHasValue;
            } else {
              issueClasses.push(categoryType.IssueClassType);
              issueCategories[categoryType.IssueClassTypeID] = [categoryType];
            }
          });
          const selectedClass = modalData.selectedClass
            ? modalData.selectedClass
            : issueClasses[0].AssetIssueClassTypeID;
          const selectedCategory = modalData.selectedCategory
            ? modalData.selectedCategory
            : issueCategories[selectedClass][0].AssetIssueCategoryTypeID;

          issueClasses.sort((a, b) =>
            a.Description.localeCompare(b.Description)
          );
          issueCategories[selectedClass].sort((a, b) =>
            a.CategoryDesc.localeCompare(b.CategoryDesc)
          );

          this.updateState({
            ..._state,
            issueCategories,
            issueClasses,
            asset: modalData.asset,
            selectedClass,
            selectedCategory,
            loadingIssueCategoryTypes: false,
            categoryErrorMessage: '',
          });
        },
        (error: unknown) => {
          console.log(JSON.stringify(error));
          this.updateState({
            ..._state,
            categoryErrorMessage: 'Categories not found.',
          });
        }
      );
  }

  reset() {
    this.updateState({ ...initialState });
  }
  private updateState(state: IssueModalState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
