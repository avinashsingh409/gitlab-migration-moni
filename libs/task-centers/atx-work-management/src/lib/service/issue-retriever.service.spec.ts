/* eslint-disable @typescript-eslint/no-empty-function */
import { TestBed, inject } from '@angular/core/testing';

import { IssueRetrieverService } from './issue-retriever.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import {
  IServerSideGetRowsRequest,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import {
  IssuesCoreService,
  IIssueDataRetrieval,
  IIssueSummaryGroup,
} from '@atonix/shared/api';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('IssueRetrieverService', () => {
  const dataRetrieval: IIssueDataRetrieval = {
    Count: 200,
    StatusCode: 200,
    Success: true,
    Type: 'type',
    Results: [
      {
        Issues: [
          {
            AssetIssueID: 987654321,
            CategoryDesc: null,
            CategoryID: null,
            IssueTitle: null,
            IssueSummary: null,
            ShortSummary: null,
            Description: null,
            Client: null,
            StationGroup: null,
            Station: null,
            StationAssetAbbrev: null,
            Unit: null,
            UnitAssetAbbrev: null,
            System: null,
            SystemAssetClassTypeID: null,
            Asset: null,
            AssetClassTypeID: null,
            AssetID: null,
            ImpactsString: null,
            ImpactTotal: null,
            Confidence_Pct: null,
            IssueTypeDesc: '',
            IssueTypeID: null,
            IssueClassTypeID: null,
            IssueClassTypeDescription: null,
            Priority: null,
            CreatedBy: null,
            CreateDate: new Date('2/2/2020'),
            ChangeDate: new Date('2/2/2020'),
            ChangedBy: null,
            ActivityStatus: null,
            ResolutionStatus: null,
            CloseDate: new Date('2/2/2020'),
            IsSubscribed: null,
            GlobalID: null,
            OpenDuration: null,
            OpenDate: new Date('2/2/2020'),
            AssignedTo: null,
            Scorecard: null,
            ResolveBy: null,
            AssetClassTypeAbbrev: null,
            AssetClassTypeDesc: null,
            AssetTypeAbbrev: null,
            AssetTypeDesc: null,
            CreatedByUserID: null,
            Count: 1,
          } as IIssueSummaryGroup,
        ],
        NumIssues: 1,
      },
    ],
  };

  const mockModelService = {
    getIssues(params: IServerSideGetRowsRequest) {
      return of(dataRetrieval);
    },
  };

  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {},
        },
        {
          provide: IssuesCoreService,
          useValue: mockModelService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    })
  );

  it('should be created', () => {
    const service: IssueRetrieverService = TestBed.inject(
      IssueRetrieverService
    );
    service.setAssetID('12345');
    expect(service).toBeTruthy();
  });

  it('should get rows', inject(
    [IssueRetrieverService],
    (service: IssueRetrieverService) => {
      const params: IServerSideGetRowsParams = {
        request: {
          startRow: 1,
          endRow: 100,
          rowGroupCols: [
            {
              id: '123',
              displayName: 'Test Only',
              field: 'test',
              aggFunc: null,
            },
          ],
          valueCols: null,
          pivotCols: null,
          pivotMode: false,
          groupKeys: ['test'],
          filterModel: null,
          sortModel: null,
        },
        parentNode: null,
        successCallback: (rowsThisPage: any[], lastRow: number) => {},
        failCallback: () => {},
        api: null,
        columnApi: null,
        success: () => {},
        fail: () => {},
        context: null,
      };
      jest.spyOn(params, 'success');

      service.getRows(params);
      expect(params.success).toHaveBeenCalledWith({
        rowData: dataRetrieval.Results[0].Issues,
        rowCount: dataRetrieval.Results[0].NumIssues,
      });
    }
  ));
});
