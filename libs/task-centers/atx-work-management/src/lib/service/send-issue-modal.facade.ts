import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { IssuesFrameworkService, ISubscriber } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import {
  BehaviorSubject,
  combineLatest,
  forkJoin,
  Observable,
  Subject,
} from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  take,
  takeUntil,
} from 'rxjs/operators';
import { ISendIssueDialogData } from '../component/send-issue-dialog/send-issue-dialog.component';
import escapeChars from 'lodash/escape';

export interface SendIssueModalState {
  assetIssueGlobalID: string;
  assetIssueID: number;
  issueTitle: string;
  allRegisteredUsers: ISubscriber[];
  filteredRegisteredUsers: ISubscriber[];
  registeredRecipients: ISubscriber[];
  nonRegisteredRecipients: string[];
  message: string;
  successMessage: string;
  sendingEmail: boolean;
  isSubscribeUser: boolean;
}

const initialState: SendIssueModalState = {
  assetIssueGlobalID: null,
  assetIssueID: null,
  issueTitle: null,
  allRegisteredUsers: [],
  filteredRegisteredUsers: [],
  registeredRecipients: [],
  nonRegisteredRecipients: [],
  message: null,
  successMessage: null,
  sendingEmail: false,
  isSubscribeUser: true,
};

let _state = initialState;

@Injectable()
export class SendIssueModalFacade implements OnDestroy {
  constructor(
    private issuesFrameworkService: IssuesFrameworkService,
    private snackBarService: ToastService
  ) {}

  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<SendIssueModalState>(_state);
  private state$ = this.store.asObservable();

  private assetIssueGlobalID$ = this.state$.pipe(
    map((state) => state.assetIssueGlobalID),
    distinctUntilChanged()
  );

  private assetIssueID$ = this.state$.pipe(
    map((state) => state.assetIssueID),
    distinctUntilChanged()
  );

  private issueTitle$ = this.state$.pipe(
    map((state) => state.issueTitle),
    distinctUntilChanged()
  );

  private allRegisteredUsers$ = this.state$.pipe(
    map((state) => state.allRegisteredUsers),
    distinctUntilChanged()
  );

  public filteredRegisteredUsers$ = this.state$.pipe(
    map((state) => state.filteredRegisteredUsers),
    distinctUntilChanged()
  );

  private registeredRecipients$ = this.state$.pipe(
    map((state) => state.registeredRecipients),
    distinctUntilChanged()
  );

  private nonRegisteredRecipients$ = this.state$.pipe(
    map((state) => state.nonRegisteredRecipients),
    distinctUntilChanged()
  );

  private message$ = this.state$.pipe(
    map((state) => state.message),
    distinctUntilChanged()
  );

  successMessage$ = this.state$.pipe(
    map((state) => state.successMessage),
    distinctUntilChanged()
  );

  private sendingEmail$ = this.state$.pipe(
    map((state) => state.sendingEmail),
    distinctUntilChanged()
  );

  private isSubscribeUser$ = this.state$.pipe(
    map((state) => state.isSubscribeUser),
    distinctUntilChanged()
  );

  vm$: Observable<SendIssueModalState> = combineLatest([
    this.assetIssueGlobalID$,
    this.assetIssueID$,
    this.issueTitle$,
    this.allRegisteredUsers$,
    this.filteredRegisteredUsers$,
    this.registeredRecipients$,
    this.nonRegisteredRecipients$,
    this.message$,
    this.successMessage$,
    this.sendingEmail$,
    this.isSubscribeUser$,
  ]).pipe(
    map(
      ([
        assetIssueGlobalID,
        assetIssueID,
        issueTitle,
        allRegisteredUsers,
        filteredRegisteredUsers,
        registeredRecipients,
        nonRegisteredRecipients,
        message,
        successMessage,
        sendingEmail,
        isSubscribeUser,
      ]) => {
        return {
          assetIssueGlobalID,
          assetIssueID,
          issueTitle,
          allRegisteredUsers,
          filteredRegisteredUsers,
          registeredRecipients,
          nonRegisteredRecipients,
          message,
          successMessage,
          sendingEmail,
          isSubscribeUser,
        } as SendIssueModalState;
      }
    )
  );

  init(modalData: ISendIssueDialogData) {
    this.issuesFrameworkService
      .getAssetSubscription(modalData.assetIssueGlobalID)
      .subscribe((subscribers) => {
        subscribers.forEach((sub) => {
          if (sub.IsGroup) {
            sub.FriendlyName = sub.FriendlyName + ' (group)';
          } else {
            sub.groups = [];
          }
        });
        subscribers.sort((a, b) => {
          if (a.FriendlyName > b.FriendlyName) {
            return 1;
          } else if (a.FriendlyName < b.FriendlyName) {
            return -1;
          }
          return 0;
        });

        const groupFollowers: ISubscriber[] = subscribers.filter(
          (x) => x.IsGroup === true && x.IsSubscribed
        );
        if (groupFollowers && groupFollowers.length > 0) {
          forkJoin(
            groupFollowers.map((g) =>
              this.issuesFrameworkService
                .getAssetGroupMembersSubscription(
                  g.UserName,
                  modalData.assetIssueGlobalID
                )
                .pipe(
                  map((members) => {
                    const index = subscribers.indexOf(g);
                    if (index > -1) {
                      subscribers[index].groupMembers = [];
                      members = members.map((m) => {
                        const user = this.getUserByEmail(
                          subscribers,
                          m.EmailAddress
                        );
                        if (user) {
                          subscribers[index].groupMembers.push(user);
                        }
                        return user;
                      });
                    }
                    return members.filter((m) => m !== null);
                  })
                )
            )
          )
            .pipe(takeUntil(this.onDestroy))
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe((allMembers) => {
              const allGroupMembers = [].concat(...allMembers);
              const allRegisteredUsers = subscribers;
              let filteredRegisteredUsers = allRegisteredUsers;
              const registeredRecipients = allRegisteredUsers.filter(
                (user) => user.IsSubscribed && !allGroupMembers.includes(user)
              );
              if (registeredRecipients.length > 0) {
                filteredRegisteredUsers = filteredRegisteredUsers.filter(
                  (user) => !registeredRecipients.includes(user)
                );
              }

              this.updateState({
                ..._state,
                allRegisteredUsers,
                filteredRegisteredUsers,
                registeredRecipients,
              });
            });
        } else {
          const allRegisteredUsers = subscribers;
          let filteredRegisteredUsers = allRegisteredUsers;
          const registeredRecipients = allRegisteredUsers.filter(
            (user) => user.IsSubscribed
          );
          if (registeredRecipients.length > 0) {
            filteredRegisteredUsers = filteredRegisteredUsers.filter(
              (user) => !registeredRecipients.includes(user)
            );
          }

          this.updateState({
            ..._state,
            allRegisteredUsers,
            filteredRegisteredUsers,
            registeredRecipients,
          });
        }
      });

    this.updateState({
      ..._state,
      assetIssueGlobalID: modalData.assetIssueGlobalID,
      assetIssueID: modalData.assetIssueID,
      issueTitle: modalData.issueTitle,
      nonRegisteredRecipients: [],
      message: modalData.message,
      successMessage: null,
      sendingEmail: false,
      isSubscribeUser: true,
    });
  }

  addRegisteredUser(user: ISubscriber) {
    const allRegisteredUsers = [..._state.allRegisteredUsers];
    const registeredRecipients = [..._state.registeredRecipients];

    this.assetIssueGlobalID$.pipe(take(1)).subscribe((assetIssueGlobalID) => {
      if (
        user.IsGroup &&
        (!user.groupMembers || user.groupMembers.length === 0)
      ) {
        this.issuesFrameworkService
          .getAssetGroupMembersSubscription(user.UserName, assetIssueGlobalID)
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe((members) => {
            user.groupMembers = [];
            members.map((m) => {
              const userObj = this.getUserByEmail(
                allRegisteredUsers,
                m.EmailAddress
              );
              if (userObj) {
                user.groupMembers.push(userObj);
              }
            });

            registeredRecipients.push(user);

            this.updateState({
              ..._state,
              filteredRegisteredUsers: allRegisteredUsers.filter(
                (user) => !registeredRecipients.includes(user)
              ),
              registeredRecipients,
            });
          });
      } else {
        registeredRecipients.push(user);

        this.updateState({
          ..._state,
          filteredRegisteredUsers: allRegisteredUsers.filter(
            (user) => !registeredRecipients.includes(user)
          ),
          registeredRecipients,
        });
      }
    });
  }

  removeRegisteredRecipient(recipient: ISubscriber) {
    const userIndex = [..._state.registeredRecipients].indexOf(recipient);
    let filteredRegisteredUsers = [..._state.allRegisteredUsers];
    const registeredRecipients = [..._state.registeredRecipients];
    registeredRecipients.splice(userIndex, 1);

    if (registeredRecipients.length > 0) {
      filteredRegisteredUsers = filteredRegisteredUsers.filter(
        (user) => !registeredRecipients.includes(user)
      );
    }

    this.updateState({
      ..._state,
      filteredRegisteredUsers,
      registeredRecipients,
    });
  }

  showGroupMembers(recipient: ISubscriber) {
    const userIndex = [..._state.registeredRecipients].indexOf(recipient);
    let filteredRegisteredUsers = [..._state.allRegisteredUsers];
    const registeredRecipients = [..._state.registeredRecipients];
    const groupMembers = recipient.groupMembers.filter(
      (member) => !registeredRecipients.includes(member)
    );

    registeredRecipients.splice(userIndex, 1, ...groupMembers);

    if (registeredRecipients.length > 0) {
      filteredRegisteredUsers = filteredRegisteredUsers.filter(
        (user) => !registeredRecipients.includes(user)
      );
    }

    this.updateState({
      ..._state,
      filteredRegisteredUsers,
      registeredRecipients,
    });
  }

  getUserByEmail(allRegisteredUsers: ISubscriber[], emailAddress: string) {
    const result = allRegisteredUsers.find(
      (user) => user.EmailAddress.toLowerCase() == emailAddress.toLowerCase()
    );
    if (result) {
      return result;
    }

    return null;
  }

  addNonRegisteredRecipient(recipient: string) {
    const nonRegisteredRecipients = [..._state.nonRegisteredRecipients];

    if (!nonRegisteredRecipients.includes(recipient)) {
      nonRegisteredRecipients.push(recipient);
    }

    this.updateState({
      ..._state,
      nonRegisteredRecipients,
    });
  }

  removeNonRegisteredRecipient(recipient: string) {
    const userIndex = [..._state.nonRegisteredRecipients].indexOf(recipient);
    const nonRegisteredRecipients = [..._state.nonRegisteredRecipients];
    nonRegisteredRecipients.splice(userIndex, 1);

    this.updateState({
      ..._state,
      nonRegisteredRecipients,
    });
  }

  setSubscribeUser(isSubscribe: boolean) {
    this.updateState({
      ..._state,
      isSubscribeUser: isSubscribe,
    });
  }

  sendEmail() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        vm.registeredRecipients.length === 0 &&
        vm.nonRegisteredRecipients.length === 0
      ) {
        this.snackBarService.openSnackBar(
          'Recipients cannot be empty.',
          'info'
        );
      } else {
        this.updateState({
          ..._state,
          sendingEmail: true,
        });

        let recipients = [
          ...vm.registeredRecipients.map((res) => {
            return res.EmailAddress;
          }),
          ...vm.nonRegisteredRecipients,
        ];

        vm.registeredRecipients.map((regRec) => {
          if (regRec.IsGroup && regRec.groupMembers?.length > 0) {
            recipients = [
              ...regRec.groupMembers.map((res) => {
                return res.EmailAddress;
              }),
              ...recipients,
            ];
          }
        });

        // Get unique recipients
        recipients = recipients.filter((item, i, ar) => ar.indexOf(item) === i);

        let sanitizedMessage = escapeChars(vm.message);
        sanitizedMessage = sanitizedMessage
          .replace(/\r\n/g, '<br />')
          .replace(/[\r\n]/g, '<br />');

        this.issuesFrameworkService
          .sendIssue(
            vm.assetIssueGlobalID,
            sanitizedMessage,
            recipients,
            'Issue',
            vm.isSubscribeUser
          )
          .pipe(take(1))
          // eslint-disable-next-line rxjs/no-nested-subscribe
          .subscribe(
            (val) => {
              if (val.ErrorMessage) {
                this.snackBarService.openSnackBar(
                  'Creating Notifications Failed.',
                  'error'
                );
              } else {
                this.updateState({
                  ..._state,
                  successMessage: val.SuccessMessage,
                  sendingEmail: false,
                });
                this.snackBarService.openSnackBar(
                  'Notifications have been submitted to the queue!',
                  'success'
                );
              }
            },
            (error: unknown) => {
              console.error(error);
              this.snackBarService.openSnackBar(
                'Creating Notifications Failed.',
                'error'
              );
            }
          );
      }
    });
  }

  buildRegisteredRecipientsInput(): UntypedFormControl {
    const registeredRecipientsInput = new UntypedFormControl(null);
    registeredRecipientsInput.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (value) {
          let newfiltered = _state.allRegisteredUsers.filter(
            (user) =>
              user.FriendlyName.toLowerCase().indexOf(value.toLowerCase()) >= 0
          );
          if ([..._state.registeredRecipients].length > 0) {
            newfiltered = newfiltered.filter(
              (sub) => ![..._state.registeredRecipients].includes(sub)
            );
          }
          this.updateState({
            ..._state,
            filteredRegisteredUsers: newfiltered ? newfiltered : [],
          });
        } else {
          let newfiltered = [..._state.allRegisteredUsers];
          if ([..._state.registeredRecipients].length > 0) {
            newfiltered = newfiltered.filter(
              (sub) => ![..._state.registeredRecipients].includes(sub)
            );
          }
          this.updateState({
            ..._state,
            filteredRegisteredUsers: newfiltered ? newfiltered : [],
          });
        }
      });
    return registeredRecipientsInput;
  }

  buildMessageTextArea(message: string): UntypedFormControl {
    const messageTextArea = new UntypedFormControl(message, [
      Validators.required,
    ]);
    messageTextArea.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          message: value,
        });
      });
    return messageTextArea;
  }

  reset() {
    this.updateState({ ...initialState });
  }

  private updateState(state: SendIssueModalState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
