import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { IssueSnapshotServiceState } from './issue-snapshot.service.facade';

export class IssueSnapshotServiceQuery {
  constructor(private store: BehaviorSubject<IssueSnapshotServiceState>) {}
  private state$ = this.store.asObservable();

  issueClasses$ = this.state$.pipe(
    map((state) => state.issueClasses),
    distinctUntilChanged()
  );

  issueCategories$ = this.state$.pipe(
    map((state) => state.issueCategories),
    distinctUntilChanged()
  );

  issueTypes$ = this.state$.pipe(
    map((state) => state.issueTypes),
    distinctUntilChanged()
  );

  issueTypeCausesDict$ = this.state$.pipe(
    map((state) => state.issueTypeCausesDict),
    distinctUntilChanged()
  );

  defaultIssueCauseTypes$ = this.state$.pipe(
    map((state) => state.defaultIssueCauseTypes),
    distinctUntilChanged()
  );

  priorities$ = this.state$.pipe(
    map((state) => state.priorities),
    distinctUntilChanged()
  );

  activityStatus$ = this.state$.pipe(
    map((state) => state.activityStatus),
    distinctUntilChanged()
  );

  resolutionStatus$ = this.state$.pipe(
    map((state) => state.resolutionStatus),
    distinctUntilChanged()
  );

  selectedPriority$ = this.state$.pipe(
    map((state) => state.selectedPriority),
    distinctUntilChanged()
  );

  selectedClass$ = this.state$.pipe(
    map((state) => state.selectedClass),
    distinctUntilChanged()
  );

  loadingIssueCategoryTypes$ = this.state$.pipe(
    map((state) => state.loadingIssueCategoryTypes),
    distinctUntilChanged()
  );

  assetId$ = this.state$.pipe(
    map((state) => state.assetId),
    distinctUntilChanged()
  );

  assetIssueId$ = this.state$.pipe(
    map((state) => state.assetIssueId),
    distinctUntilChanged()
  );

  globalId$ = this.state$.pipe(
    map((state) => state.globalId),
    distinctUntilChanged()
  );

  assetErrorMessage$ = this.state$.pipe(
    map((state) => state.assetErrorMessage),
    distinctUntilChanged()
  );

  assetLoading$ = this.state$.pipe(
    map((state) => state.assetLoading),
    distinctUntilChanged()
  );

  issueWithOptions$ = this.state$.pipe(
    map((state) => state.issueWithOptions),
    distinctUntilChanged()
  );

  modalOpen$ = this.state$.pipe(
    map((state) => state.modalOpen),
    distinctUntilChanged()
  );

  pristine$ = this.state$.pipe(
    map((state) => state.pristine),
    distinctUntilChanged()
  );

  invalidResolveBy$ = this.state$.pipe(
    map((state) => state.pristine),
    distinctUntilChanged()
  );

  isSubscribed$ = this.state$.pipe(
    map((state) => state.isSubscribed),
    distinctUntilChanged()
  );

  calculatorTotals$ = this.state$.pipe(
    map((state) => state.calculatorTotals),
    distinctUntilChanged()
  );

  calculatorLoading$ = this.state$.pipe(
    map((state) => state.calculatorLoading),
    distinctUntilChanged()
  );

  issueTagQuery$ = this.state$.pipe(
    map((state) => state.issueTagQuery),
    distinctUntilChanged()
  );

  filteredTags$ = this.state$.pipe(
    map((state) => state.filteredTags),
    distinctUntilChanged()
  );

  loadingIssueTags$ = this.state$.pipe(
    map((state) => state.loadingIssueTags),
    distinctUntilChanged()
  );
  issueTaggingErrorMessage$ = this.state$.pipe(
    map((state) => state.issueTaggingErrorMessage),
    distinctUntilChanged()
  );

  filteredIssueTypes$ = this.state$.pipe(
    map((state) => state?.filteredIssueTypes || []),
    distinctUntilChanged()
  );

  filteredIssueCauseTypes$ = this.state$.pipe(
    map((state) => state?.filteredIssueCauseTypes || []),
    distinctUntilChanged()
  );

  relatedIssueByAssetCount$ = this.state$.pipe(
    map((state) => state.relatedIssueByAssetCount),
    distinctUntilChanged()
  );
  alertByOwningAssetCount$ = this.state$.pipe(
    map((state) => state.alertByOwningAssetCount),
    distinctUntilChanged()
  );

  users$ = this.state$.pipe(
    map((state) => state.users),
    distinctUntilChanged()
  );
  filteredUsers$ = this.state$.pipe(
    map((state) => state.filteredUsers),
    distinctUntilChanged()
  );

  vm$: Observable<IssueSnapshotServiceState> = combineLatest([
    this.issueClasses$,
    this.issueCategories$,
    this.issueTypes$,
    this.issueTypeCausesDict$,
    this.defaultIssueCauseTypes$,
    this.activityStatus$,
    this.resolutionStatus$,
    this.priorities$,
    this.selectedClass$,
    this.selectedPriority$,
    this.loadingIssueCategoryTypes$,
    this.assetId$,
    this.assetIssueId$,
    this.globalId$,
    this.assetErrorMessage$,
    this.assetLoading$,
    this.issueWithOptions$,
    this.modalOpen$,
    this.pristine$,
    this.invalidResolveBy$,
    this.isSubscribed$,
    this.calculatorTotals$,
    this.calculatorLoading$,
    this.issueTagQuery$,
    this.loadingIssueTags$,
    this.filteredTags$,
    this.issueTaggingErrorMessage$,
    this.filteredIssueTypes$,
    this.filteredIssueCauseTypes$,
    this.relatedIssueByAssetCount$,
    this.alertByOwningAssetCount$,
    this.users$,
    this.filteredUsers$,
  ]).pipe(
    map(
      ([
        issueClasses,
        issueCategories,
        issueTypes,
        issueTypeCausesDict,
        defaultIssueCauseTypes,
        activityStatus,
        resolutionStatus,
        priorities,
        selectedClass,
        selectedPriority,
        loadingIssueCategoryTypes,
        assetId,
        assetIssueId,
        globalId,
        assetErrorMessage,
        assetLoading,
        issueWithOptions,
        modalOpen,
        pristine,
        invalidResolveBy,
        isSubscribed,
        calculatorTotals,
        calculatorLoading,
        issueTagQuery,
        loadingIssueTags,
        filteredTags,
        issueTaggingErrorMessage,
        filteredIssueTypes,
        filteredIssueCauseTypes,
        relatedIssueByAssetCount,
        alertByOwningAssetCount,
        subscribedUsers,
        filteredUsers,
      ]) => {
        return {
          issueClasses,
          issueCategories,
          issueTypes,
          issueTypeCausesDict,
          defaultIssueCauseTypes,
          activityStatus,
          resolutionStatus,
          priorities,
          selectedClass,
          selectedPriority,
          loadingIssueCategoryTypes,
          assetId,
          assetIssueId,
          globalId,
          assetErrorMessage,
          assetLoading,
          issueWithOptions,
          modalOpen,
          pristine,
          invalidResolveBy,
          isSubscribed,
          calculatorTotals,
          calculatorLoading,
          issueTagQuery,
          loadingIssueTags,
          filteredTags,
          issueTaggingErrorMessage,
          filteredIssueTypes,
          filteredIssueCauseTypes,
          relatedIssueByAssetCount,
          alertByOwningAssetCount,
          users: subscribedUsers,
          filteredUsers,
        } as IssueSnapshotServiceState;
      }
    )
  );
}
