/* eslint-disable max-len */
import { IFileInfo, isNil } from '@atonix/atx-core';
import moment from 'moment';

export function replaceBVFilesToHTMLFileElement(
  fileInfos: IFileInfo[],
  summaryContent: string,
  matchFiles: string[]
) {
  if (fileInfos?.length > 0) {
    matchFiles?.map((value, index) => {
      let replacement;
      if (fileInfos[index]?.IsImage) {
        replacement = `<img class="bvFile" id="${fileInfos[index].UniqueID}" src="${fileInfos[index].Url}" style="height: 50px; width: 100px; cursor: pointer;" /> `;
      } else {
        replacement = `<a href="${fileInfos[index].Url}"><span class="bvFile" id="${fileInfos[index].UniqueID}"></span>${fileInfos[index].Name}</a> `;
      }
      summaryContent = summaryContent.replace(value, replacement);
    });
  }
  return summaryContent;
}

export function replaceHTMLFileElementToBVFiles(summaryContent: string) {
  const imgPattern = /<img([^>])*class="bvFile"([^>])*\/>/gm;
  const imgMatchFiles = summaryContent.match(imgPattern);
  imgMatchFiles?.map((val) => {
    const guidPatt = /id="((\w+[-]*)+)?"/g;
    const match = guidPatt.exec(val);
    const replacement = '<bv-file file-guid="' + match[1] + '"></bv-file>';
    summaryContent = summaryContent.replace(val, replacement);
  });

  const linkPattern =
    /<a([^>])*><span([^>])*class="bvFile"([^>])*><\/span>([^>])*<\/a>/gm;
  const linkMatchFiles = summaryContent.match(linkPattern);
  linkMatchFiles?.map((val) => {
    const guidPatt = /id="((\w+[-]*)+)?"/g;
    const match = guidPatt.exec(val);
    const replacement = '<bv-file file-guid="' + match[1] + '"></bv-file>';
    summaryContent = summaryContent.replace(val, replacement);
  });

  return summaryContent;
}

export function createHTMLFileElement(newFileInfo: any) {
  let element = null;
  if (newFileInfo.isImage) {
    element = `<img class="bvFile" id="${newFileInfo.contentID}" src="${newFileInfo.path}" style="height: 50px; width: 100px; cursor: pointer;" /> `;
  } else {
    element = `<a href="${newFileInfo.path}"><span class="bvFile" id="${newFileInfo.contentID}"></span>${newFileInfo.fileName}</a> `;
  }
  return element;
}

export function formatCSVFile(csvFile: string) {
  const newLineTokens = csvFile.split('\n');
  const processedResult = [newLineTokens[0]];

  const headers = newLineTokens[0].split(',');
  const idIndex = headers.findIndex((x) => x === '"ID"');
  const ageIndex = headers.findIndex((x) => x === '"Age (Days)"');
  const impactIndex = headers.findIndex((x) => x === '"Impact"');
  const createdIndex = headers.findIndex((x) => x === '"Created"');
  const changedIndex = headers.findIndex((x) => x === '"Changed"');
  const openedIndex = headers.findIndex((x) => x === '"Opened"');
  const closedIndex = headers.findIndex((x) => x === '"Closed"');
  const resolvedByIndex = headers.findIndex((x) => x === '"Resolve By"');

  if (
    ageIndex !== -1 ||
    impactIndex !== -1 ||
    createdIndex !== -1 ||
    changedIndex !== -1 ||
    openedIndex !== -1 ||
    closedIndex !== -1 ||
    resolvedByIndex !== -1
  ) {
    newLineTokens.forEach((row, index) => {
      if (index !== 0 && row) {
        let col = [];
        if (idIndex !== -1) {
          // This will replace the (,) character on the hyperlink text into (;) before splitting things up.
          col = row.replace(/"", /gm, '""; ').split(',');
        } else {
          col = row.split(',');
        }

        if (ageIndex !== -1) {
          col[ageIndex] = getAge(col[ageIndex]);
        }
        if (impactIndex !== -1) {
          col[impactIndex] = getImpact(col[impactIndex]);
        }
        if (createdIndex !== -1) {
          col[createdIndex] = getDate(col[createdIndex]);
        }
        if (changedIndex !== -1) {
          col[changedIndex] = getDate(col[changedIndex]);
        }
        if (openedIndex !== -1) {
          col[openedIndex] = getDate(col[openedIndex]);
        }
        if (closedIndex !== -1) {
          col[closedIndex] = getDate(col[closedIndex]);
        }
        if (resolvedByIndex !== -1) {
          col[resolvedByIndex] = getDate(col[resolvedByIndex]);
        }

        processedResult.push(col.join(',').replace('""; ', '"", '));
      }
    });

    return processedResult.join('\n');
  } else {
    return csvFile;
  }
}

function getAge(value: string) {
  value = value.replace(/"/gm, '');
  if (Number(value) && +value > 0) {
    return String(Math.floor(+value / 86400));
  } else {
    return '-';
  }
}

function getImpact(value: string) {
  let result = '';
  value = value.replace(/"/gm, '');
  if (!isNil(value) && value !== '') {
    if (Number(value)) {
      result = (+value).toFixed(2);
    } else {
      result = String(value);
    }
  }
  return result;
}

function getDate(value: string) {
  value = value.replace(/"/gm, '');
  return !isNil(value) && value !== ''
    ? moment(value).format('MM/DD/YYYY').toString()
    : '';
}
