import { Injectable } from '@angular/core';
import {
  IServerSideGetRowsParams,
  IServerSideDatasource,
} from '@ag-grid-enterprise/all-modules';
import {
  IIssueDataRetrieval,
  filterModelsToStrings,
  sortModelToStrings,
  IssuesCoreService,
  IssuesFrameworkService,
} from '@atonix/shared/api';
import { takeUntil, map, distinctUntilChanged } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';
import {
  IServerSideGetRowsRequest,
  ColumnVO,
} from '@ag-grid-enterprise/all-modules';
import { Cacher, IKeyCreator } from '@atonix/atx-core';
import { Observable, of } from 'rxjs';

function columnToString(value: ColumnVO) {
  return `${value.id}-${value.displayName}-${value.field}-${value.aggFunc}`;
}

const keyCreator: IKeyCreator<IServerSideGetRowsRequest> = {
  createKey(value: IServerSideGetRowsRequest) {
    const parts: string[] = [];
    parts.push(String(value.startRow));
    parts.push(String(value.endRow));
    parts.push(String(value.pivotMode));
    if (value.groupKeys) {
      for (const groupKey of value.groupKeys) {
        parts.push('gk:' + groupKey);
      }
    }
    if (value.rowGroupCols) {
      for (const rowGroupCol of value.rowGroupCols) {
        parts.push(columnToString(rowGroupCol));
      }
    }
    if (value.valueCols) {
      for (const valueCol of value.valueCols) {
        parts.push(columnToString(valueCol));
      }
    }
    if (value.pivotCols) {
      for (const pivotCol of value.pivotCols) {
        parts.push(columnToString(pivotCol));
      }
    }
    if (value.filterModel) {
      parts.push(...filterModelsToStrings(value.filterModel));
    }
    if (value.sortModel) {
      parts.push(...sortModelToStrings(value.sortModel));
    }

    return parts.join('~');
  },
};

@Injectable({
  providedIn: 'root',
})
export class IssueRetrieverService implements IServerSideDatasource {
  cacher: Cacher<IServerSideGetRowsRequest, IIssueDataRetrieval>;

  private keyword = new BehaviorSubject<string>(null);
  public keyword$ = this.keyword.asObservable().pipe(
    map((keyword) => keyword),
    distinctUntilChanged()
  );

  constructor(
    private issuesCoreService: IssuesCoreService,
    private issuesFrameworkService: IssuesFrameworkService
  ) {
    this.cacher = new Cacher<IServerSideGetRowsRequest, IIssueDataRetrieval>(
      keyCreator,
      20
    );
  }

  private assetID: string;
  private unsubscribe$ = new Subject<void>();

  public cancel() {
    this.unsubscribe$.next();
  }

  public setAssetID(newAssetID: string) {
    this.assetID = newAssetID;
  }

  public clearCacher() {
    this.cacher.clear();
  }

  public setScorecard(assetIssueID: number, include: boolean) {
    this.cacher.clear();
    return this.issuesFrameworkService.setScorecard(assetIssueID, include);
  }

  public setKeyword(keyword: string) {
    this.keyword.next(keyword);
  }

  public getRows(params: IServerSideGetRowsParams) {
    const keyword = this.keyword.getValue();
    if (keyword?.length > 0) {
      const fltrModel = { ...params.request.filterModel };
      params.request.filterModel = {
        ...fltrModel,
        Keyword: {
          filterType: 'set',
          values: [keyword],
        },
      };
    }

    (params.request.startRow === 0 && this.cacher.exists(params.request)
      ? of(this.cacher.get(params.request))
      : this.issuesCoreService.getIssues(params.request).pipe(
          map((n) => {
            // Want to cache the first page of data. Don't want to cache
            // subsequent pages because the user won't retrieve them as often.
            if (params.request.startRow === 0) {
              this.cacher.add(params.request, n);
            }
            return n;
          }),
          takeUntil(this.unsubscribe$)
        )
    ).subscribe(
      (n) => {
        params.success({
          rowData: n.Results[0].Issues,
          rowCount: n.Results[0].NumIssues,
        });
      },
      (error: unknown) => {
        params.fail();
        console.error('Could not retrieve issues');
      }
    );
  }
}
