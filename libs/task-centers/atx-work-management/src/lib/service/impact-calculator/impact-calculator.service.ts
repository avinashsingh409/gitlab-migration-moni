import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import moment from 'moment';
import {
  CalculationAttributes,
  ImpactScenario,
  ScenarioConstructor,
} from '../../model/impact-calculator/scenario';
import { ImpactCalculatorState } from './impact-calculator.facade';
import {
  MaintenanceCalculatedCost,
  OtherCalculatedCost,
} from './impact-calculations.service';
import {
  AssetImpactCategory,
  AssetImpactType,
  DEFAULT_EFFICIENCY_CALCULATION_FACTOR_ID,
  DEFAULT_GENERATION_CALCULATION_FACTOR_ID,
} from '../../model/issue-model';
import {
  AssetIssueImpactScenario,
  CalculationType,
  ImpactAttributes,
  ImpactDateRange,
  ImpactTypeMapSave,
  ImpactTypeMapV2Save,
} from '@atonix/atx-core';
import { IssuesFrameworkService } from '@atonix/shared/api';
import {
  EfficiencyCalculationParameters,
  MaintenanceCalculationParameters,
  MaintenanceParameters,
  OtherCalculationParameters,
  OtherParameters,
  ProductionCalculationParameters,
} from '../../model/impact-calculator/calculation-parameters';
import { LegacyImpactScenario } from '../../model/impact-calculator/scenario-legacy';
import { ImpactScenarioV2 } from '../../model/impact-calculator/scenario-v2';
import {
  createProductionErrors,
  GenerationParameters,
  ProductionErrors,
} from '../../model/impact-calculator/production-calculations';
import { ProductionImpactContent } from '../../model/impact-calculator/production-v2';
import {
  createEfficiencyErrors,
  EfficiencyParameters,
} from '../../model/impact-calculator/efficiency-calculations';
import { EfficiencyImpactContent } from '../../model/impact-calculator/efficiency-v2';

@Injectable({
  providedIn: 'root',
})
export class ImpactCalculatorService {
  constructor(private issuesFrameworkService: IssuesFrameworkService) {}

  findCalcKey(
    calcAttributes: ImpactAttributes[],
    key: string,
    defaultValue,
    errors: string[],
    type = 'string' || 'number'
  ) {
    const idx = calcAttributes.findIndex((x) => x.AttrName === key);
    if (idx >= 0 && calcAttributes[idx]?.AttrValue) {
      return type === 'string'
        ? calcAttributes[idx].AttrValue.toString()
        : +calcAttributes[idx].AttrValue;
    }
    errors.push(key);
    return defaultValue;
  }

  createBlankScenario(
    vm: ImpactCalculatorState,
    guiId: number
  ): ImpactScenario {
    const errors: string[] = [];

    // To test errors and warnings, uncomment line below:
    // vm.blankScenario.ImpactAttributesList = [];

    const productionRevenue = this.findCalcKey(
      vm.blankScenario.ImpactAttributesList,
      'ImpactProductionRevenueValue',
      0,
      errors,
      'number'
    );

    const attributes: CalculationAttributes = {
      productionCapacityUnits: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactCapacityUoM',
        'not configured',
        errors,
        'string'
      ),
      productionTimeUnits: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactUoT',
        'not configured',
        errors,
        'string'
      ),
      productionCapacity: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactCapacityValue',
        0,
        errors,
        'number'
      ),
      productionTimeToDays: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactTimeToDaysConversion',
        0,
        errors,
        'number'
      ),
      productionRevenue,
      consumableLabel: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactConsumableLabel',
        'not configured',
        errors,
        'string'
      ),
      consumableUnits: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactConsumableUoM',
        'not configured',
        errors,
        'string'
      ),
      consumableCost: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactConsumableValue',
        0,
        errors,
        'number'
      ),
      consumptionRateLabel: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactNomConsRateLabel',
        'not configured',
        errors,
        'string'
      ),
      consumptionRateUnits: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactNomConsRateUoM',
        'not configured',
        errors,
        'string'
      ),
      consumptionRateValue: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactNomConsRateValue',
        0,
        errors,
        'number'
      ),
      consumptionToCostRate: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactConsCostToRateConvert',
        0,
        errors,
        'number'
      ),
      productionToCostRate: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactProdCostToRateConvert',
        0,
        errors,
        'number'
      ),
      capacityUtilization: this.findCalcKey(
        vm.blankScenario.ImpactAttributesList,
        'ImpactNomCapacityUtilization',
        0,
        errors,
        'number'
      ),
    };
    const generationCalculations = this.generationImpactHelper(
      null,
      null,
      productionRevenue,
      attributes,
      false,
      errors
    );

    const effeciencyCalculations = this.efficiencyImpactHelper(
      null,
      null,
      attributes,
      false,
      errors
    );
    const maintenanceCalculations = this.maintenanceImpactHelper(null, null);
    const otherCalculations = this.otherImpactHelper(null, null);
    const newCalculationConstructor: ScenarioConstructor = {
      scenarioID: vm.blankScenario.AssetIssueImpactScenarioID,
      scenarioGuidID: guiId,
      scenarioName: vm.blankScenario.AssetIssueImpactScenarioName,
      scenarioNotes: '',
      percentLikelihood: vm.blankScenario.PercentLikelihood,
      attributes,
      generationParameters: generationCalculations,
      efficiencyParameters: effeciencyCalculations,
      maintenanceParameters: maintenanceCalculations,
      otherParameters: otherCalculations,
      errors,
    };
    const impactScenario = new ImpactScenarioV2(newCalculationConstructor);
    return impactScenario;
  }

  getImpactCalculationsForImpactCategory(
    assetIssueID: number
  ): Observable<ImpactScenario[]> {
    return this.issuesFrameworkService
      .getImpactScenariosByAssetIssueID(assetIssueID)
      .pipe(
        map((data: AssetIssueImpactScenario[]) => {
          const impactScenarios: ImpactScenario[] = [];
          data.forEach((scenario: AssetIssueImpactScenario, scenarioIndex) => {
            this.impactScenarioRangeHelper(scenario.EfficiencyImpactRange);
            this.impactScenarioRangeHelper(scenario.GenerationImpactRange);
            this.impactScenarioRangeHelper(scenario.MaintenanceImpactRange);
            this.impactScenarioRangeHelper(scenario.OtherCostsImpactRange);

            const generationCalculations = this.generationImpactHelper(
              scenario.GenerationImpact,
              scenario.GenerationImpactRange,
              scenario.CapacityCost,
              null,
              true,
              []
            );
            const effeciencyCalculations = this.efficiencyImpactHelper(
              scenario.EfficiencyImpact,
              scenario.EfficiencyImpactRange,
              null,
              true,
              []
            );
            const maintenanceCalculations = this.maintenanceImpactHelper(
              scenario.MaintenanceImpact,
              scenario.MaintenanceImpactRange
            );
            const otherCalculations = this.otherImpactHelper(
              scenario.OtherCostsImpact,
              scenario.OtherCostsImpactRange
            );
            if (generationCalculations.version === 1) {
              const newCalculationConstructor: ScenarioConstructor = {
                scenarioID: scenario.AssetIssueImpactScenarioID,
                scenarioGuidID: scenarioIndex,
                scenarioName: scenario.AssetIssueImpactScenarioName,
                scenarioNotes: scenario.AssetIssueImpactScenarioNotes,
                percentLikelihood: scenario.PercentLikelihood,
                attributes: {
                  unitCapability: scenario.UnitCapability,
                  netUnitHeatRate: scenario.NetUnitHeatRate,
                  fuelCost: scenario.FuelCost,
                  capacityFactor: scenario.CapacityFactor,
                },
                generationParameters: generationCalculations,
                efficiencyParameters: effeciencyCalculations,
                maintenanceParameters: maintenanceCalculations,
                otherParameters: otherCalculations,
                errors: [],
              };
              const impactScenario: LegacyImpactScenario =
                new LegacyImpactScenario(newCalculationConstructor);
              impactScenarios.push(impactScenario);
            } else {
              const newCalculationConstructor: ScenarioConstructor = {
                scenarioID: scenario.AssetIssueImpactScenarioID,
                scenarioGuidID: scenarioIndex,
                scenarioName: scenario.AssetIssueImpactScenarioName,
                scenarioNotes: scenario.AssetIssueImpactScenarioNotes,
                percentLikelihood: scenario.PercentLikelihood,
                attributes: generationCalculations.attributes
                  ? generationCalculations.attributes
                  : effeciencyCalculations.attributes,
                generationParameters: generationCalculations,
                efficiencyParameters: effeciencyCalculations,
                maintenanceParameters: maintenanceCalculations,
                otherParameters: otherCalculations,
                errors: [],
              };
              const impactScenario: ImpactScenarioV2 = new ImpactScenarioV2(
                newCalculationConstructor
              );
              impactScenarios.push(impactScenario);
            }
          });
          return impactScenarios;
        })
      );
  }

  private impactScenarioRangeHelper(range: ImpactDateRange) {
    if (range) {
      const today = this.createToday();
      let currentDate = moment.utc(today);
      let endDate = moment.utc(today);
      let startDate = moment.utc(today);
      if (range.CurrentDate) {
        range.CurrentDate = new Date(<any>range.CurrentDate);
        currentDate = moment.utc(
          new Date(
            range.CurrentDate.getFullYear(),
            range.CurrentDate.getMonth(),
            range.CurrentDate.getDate(),
            0,
            0,
            0,
            0
          )
        );
      }
      if (range.EndDate) {
        range.EndDate = new Date(<any>range.EndDate);
        endDate = moment.utc(
          new Date(
            range.EndDate.getFullYear(),
            range.EndDate.getMonth(),
            range.EndDate.getDate(),
            0,
            0,
            0,
            0
          )
        );
      }
      if (range.StartDate) {
        range.StartDate = new Date(<any>range.StartDate);
        startDate = moment.utc(
          new Date(
            range.StartDate.getFullYear(),
            range.StartDate.getMonth(),
            range.StartDate.getDate(),
            0,
            0,
            0,
            0
          )
        );
      }
      if (!range.DaysElapsed) {
        range.DaysElapsed = 0;
      }
      if (!range.DaysPending) {
        range.DaysPending = 0;
      }
      if (range.DaysElapsed + range.DaysPending !== range.DaysTotal) {
        // This fixes legacy issue where a total was given
        // but now past or future
        range.DaysElapsed = 0;
        if (range.DaysTotal) {
          range.DaysPending = range.DaysTotal;
        } else {
          range.DaysTotal = 0;
        }
      }
      const daysElapsed = currentDate.diff(startDate, 'days');
      const daysPending = endDate.diff(currentDate, 'days');
      if (
        daysElapsed !== range.DaysElapsed ||
        daysPending !== range.DaysPending
      ) {
        // dates not serialized correctly. Ignore and use current date
        range.CurrentDate = moment.utc(currentDate, 'DD-MM-YYYY').toDate();
        range.StartDate = moment(currentDate, 'DD-MM-YYYY')
          .subtract(range.DaysElapsed, 'days')
          .toDate();
        range.EndDate = moment(currentDate, 'DD-MM-YYYY')
          .add(range.DaysPending, 'days')
          .toDate();
      } else {
        range.CurrentDate = moment(currentDate, 'DD-MM-YYYY').toDate();
        range.StartDate = moment(startDate, 'DD-MM-YYYY').toDate();
        range.EndDate = moment(endDate, 'DD-MM-YYYY').toDate();
      }
      // console.log(range.CurrentDate);
      // console.log(range.StartDate);
      // console.log(range.EndDate);
    }
  }

  private otherImpactHelper(otherImpact, otherImpactRange: ImpactDateRange) {
    const inputArray = otherImpact?.InputString.split(',');
    if (inputArray && inputArray.length > 0) {
      const oImpact: OtherParameters = {
        assetIssueAssetIssueImpactTypeMapID:
          otherImpact.AssetIssueAssetIssueImpactTypeMapID,
        assetIssueImpactTypeID: otherImpact.AssetIssueImpactTypeID,
        overrideTotal: CalculationType.CalculatedImpact,
        impactQuantity: {
          toDate: parseFloat(inputArray[0]),
          potentialFuture: parseFloat(inputArray[1]),
          totalAvg: parseFloat(inputArray[2]),
        },
        totalCost: {
          toDate: parseFloat(inputArray[3]),
          potentialFuture: parseFloat(inputArray[4]),
          totalAvg: parseFloat(inputArray[5]),
        },
        impactRange: otherImpactRange,
      };
      if (
        OtherCalculatedCost(
          oImpact.impactQuantity.toDate,
          oImpact.impactRange.DaysElapsed
        ) !== oImpact.totalCost.toDate
      ) {
        // check if dates are correct
        let startDate = moment(oImpact.impactRange.StartDate);
        let currentDate = moment(oImpact.impactRange.CurrentDate);
        if (!startDate.isValid() || !currentDate.isValid()) {
          currentDate = moment({
            year: moment().year(),
            month: moment().month(),
            day: moment().date(),
          });
          startDate = currentDate.subtract(
            oImpact.impactRange.DaysElapsed,
            'days'
          );
        }
        const daysElapsed = currentDate.diff(startDate, 'days');
        if (
          OtherCalculatedCost(oImpact.impactQuantity.toDate, daysElapsed) !==
          oImpact.totalCost.toDate
        ) {
          oImpact.impactRange.DaysElapsed = daysElapsed;
        } else {
          // Calculation is wrong. Just set to zero
          oImpact.impactRange.DaysElapsed = 0;
        }
      }
      if (
        OtherCalculatedCost(
          oImpact.impactQuantity.potentialFuture,
          oImpact.impactRange.DaysPending
        ) !== oImpact.totalCost.potentialFuture
      ) {
        // check if dates are correct
        let pendingDate = moment(oImpact.impactRange.EndDate);
        let currentDate = moment(oImpact.impactRange.CurrentDate);
        if (!pendingDate.isValid() || !currentDate.isValid()) {
          const currentDay = this.createToday();
          currentDate = moment.utc(currentDay);
          pendingDate = currentDate.subtract(
            oImpact.impactRange.DaysPending,
            'days'
          );
        }
        const daysPending = pendingDate.diff(currentDate, 'days');
        if (
          OtherCalculatedCost(
            oImpact.impactQuantity.potentialFuture,
            daysPending
          ) !== oImpact.totalCost.potentialFuture
        ) {
          oImpact.impactRange.DaysPending = daysPending;
        } else {
          // Calculation is wrong. Just set to zero
          oImpact.impactRange.DaysPending = 0;
        }
      }

      if (otherImpact.Impact_Cost != null) {
        if (
          otherImpact.InputString === '-1,0,0,0,0,0' ||
          otherImpact.InputString.startsWith('0,0,0,')
        ) {
          if (otherImpact.InputString.startsWith('0,0,0,')) {
            // Special special case where override is found in these
            // three values
            oImpact.totalCost.toDate = parseFloat(inputArray[3]);
            oImpact.totalCost.potentialFuture = parseFloat(inputArray[4]);
            oImpact.totalCost.totalAvg = parseFloat(inputArray[5]);
          }
          oImpact.overrideTotal = CalculationType.UserDefinedImpact;
        }
        if (
          oImpact.totalCost.toDate === 0.0 &&
          oImpact.totalCost.potentialFuture === 0.0
        ) {
          oImpact.totalCost.potentialFuture = otherImpact.Impact_Cost;
          oImpact.overrideTotal = CalculationType.UserDefinedImpact;
          if (oImpact.impactRange.DaysTotal <= 0) {
            const currentDay = this.createToday();
            oImpact.impactRange = {
              StartDate: currentDay,
              EndDate: currentDay,
              CurrentDate: currentDay,
              DaysElapsed: 0,
              DaysPending: 0,
              DaysTotal: 0,
            };
          }
        }
        if (
          otherImpact.Impact_Cost === 0.0 &&
          oImpact.totalCost.toDate === 0.0 &&
          oImpact.totalCost.potentialFuture === 0.0
        ) {
          oImpact.overrideTotal = CalculationType.CalculatedImpact;
        }
        oImpact.totalCost.totalAvg = otherImpact.Impact_Cost;
      }
      return oImpact;
    }
    const today = this.createToday();
    const emptyImpact: OtherParameters = {
      assetIssueAssetIssueImpactTypeMapID: -1,
      assetIssueImpactTypeID: AssetImpactType.Other.valueOf(),
      overrideTotal: CalculationType.CalculatedImpact,
      impactQuantity: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
      totalCost: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
      impactRange: {
        StartDate: today,
        CurrentDate: today,
        EndDate: today,
        DaysElapsed: 0,
        DaysPending: 0,
        DaysTotal: 0,
      },
    };
    return emptyImpact;
  }

  private maintenanceImpactHelper(
    maintenanceImpact,
    maintenanceImpactRange: ImpactDateRange
  ) {
    const inputArray = maintenanceImpact?.InputString.split(',');
    if (inputArray && inputArray.length > 0) {
      const mainImpact: MaintenanceParameters = {
        assetIssueAssetIssueImpactTypeMapID:
          maintenanceImpact.AssetIssueAssetIssueImpactTypeMapID,
        assetIssueImpactTypeID: maintenanceImpact.AssetIssueImpactTypeID,
        overrideTotal: CalculationType.CalculatedImpact,
        repairCost: {
          toDate: parseFloat(inputArray[0]),
          potentialFuture: parseFloat(inputArray[1]),
          totalAvg: parseFloat(inputArray[2]),
        },
        repairManHours: {
          toDate: parseInt(inputArray[3], 10),
          potentialFuture: parseInt(inputArray[4], 10),
          totalAvg: parseInt(inputArray[5], 10),
        },
        laborHourlyRate: {
          toDate: parseFloat(inputArray[6]),
          potentialFuture: parseFloat(inputArray[7]),
          totalAvg: parseFloat(inputArray[8]),
        },
        totalCost: {
          toDate: parseFloat(inputArray[9]),
          potentialFuture: parseFloat(inputArray[10]),
          totalAvg: 0.0,
        },
      };
      if (maintenanceImpact.Impact_Cost != null) {
        if (
          maintenanceImpact?.InputString === '-1,0,0,0,0,0,0,0,0,0,0' ||
          maintenanceImpact?.InputString.startsWith('0,0,0,0,0,0,0,0,0,')
        ) {
          if (
            maintenanceImpact.Impact_Cost === 0.0 &&
            mainImpact.totalCost.toDate === 0.0 &&
            mainImpact.totalCost.potentialFuture === 0.0
          ) {
            mainImpact.overrideTotal = CalculationType.CalculatedImpact;
          } else {
            mainImpact.overrideTotal = CalculationType.UserDefinedImpact;
          }
          if (
            mainImpact.totalCost.toDate === 0.0 &&
            mainImpact.totalCost.potentialFuture === 0.0
          ) {
            mainImpact.totalCost.potentialFuture =
              maintenanceImpact.Impact_Cost;
          }
        } else if (maintenanceImpact.Impact_Cost > 0.0) {
          const elapsed = mainImpact.repairCost.toDate;
          const laborHours = mainImpact.repairManHours.toDate;
          const laborRate = mainImpact.laborHourlyRate.toDate;
          const elapsedCost = MaintenanceCalculatedCost(
            elapsed,
            laborHours,
            laborRate
          );
          const pending = mainImpact.repairCost.potentialFuture;
          const laborHPending = mainImpact.repairManHours.potentialFuture;
          const laborRPending = mainImpact.laborHourlyRate.potentialFuture;
          const pendingCost = MaintenanceCalculatedCost(
            pending,
            laborHPending,
            laborRPending
          );
          const totalImpact = elapsedCost + pendingCost;
          const calculationsCorrect =
            Math.abs(totalImpact - maintenanceImpact.Impact_Cost) <= 1;
          if (!calculationsCorrect) {
            // This is a legacy override of total labor hours / hourly rate
            // in order for our changes to be backwards compatible, we'll
            // set everything to 0
            mainImpact.overrideTotal = CalculationType.UserDefinedImpact;
            mainImpact.totalCost.toDate = 0.0;
            mainImpact.totalCost.potentialFuture =
              maintenanceImpact.Impact_Cost;
            mainImpact.totalCost.totalAvg = maintenanceImpact.Impact_Cost;
            mainImpact.repairCost.toDate = 0.0;
            mainImpact.repairCost.potentialFuture = 0.0;
            mainImpact.repairCost.totalAvg = 0.0;
            mainImpact.repairManHours.potentialFuture = 0.0;
            mainImpact.repairManHours.toDate = 0.0;
            mainImpact.repairManHours.totalAvg = 0.0;
            mainImpact.laborHourlyRate.toDate = 0.0;
            mainImpact.laborHourlyRate.potentialFuture = 0.0;
            mainImpact.laborHourlyRate.totalAvg = 0.0;
          }
        }
        mainImpact.totalCost.totalAvg = maintenanceImpact.Impact_Cost;
      }
      return mainImpact;
    }
    const emptyImpact: MaintenanceParameters = {
      assetIssueAssetIssueImpactTypeMapID: -1,
      assetIssueImpactTypeID: AssetImpactType.Maintenance.valueOf(),
      overrideTotal: CalculationType.CalculatedImpact,
      repairCost: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
      repairManHours: {
        toDate: 0,
        potentialFuture: 0,
        totalAvg: 0,
      },
      laborHourlyRate: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
      totalCost: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
    };
    return emptyImpact;
  }

  private efficiencyImpactHelper(
    efficiencyImpact,
    efficiencyImpactRange: ImpactDateRange,
    attributes: CalculationAttributes,
    legacyLoad: boolean,
    newErrors: string[]
  ) {
    if (efficiencyImpact?.ImpactContent) {
      const impactContent = JSON.parse(
        efficiencyImpact.ImpactContent
      ) as EfficiencyImpactContent;
      if (!impactContent.totalCost?.toDate) {
        impactContent.totalCost.toDate = 0;
      }
      if (!impactContent.totalCost?.potentialFuture) {
        impactContent.totalCost.potentialFuture = 0;
      }
      if (!impactContent.totalCost?.totalAvg) {
        impactContent.totalCost.totalAvg =
          impactContent.totalCost.toDate +
          impactContent.totalCost.potentialFuture;
      }
      const errors = impactContent?.errors ? impactContent.errors : [];
      const efficiencyErrors = createEfficiencyErrors(errors);
      const effImpact: EfficiencyParameters = {
        version: 2,
        overrideTotal:
          impactContent.overrideTotal === 'UserDefinedImpact'
            ? CalculationType.UserDefinedImpact
            : CalculationType.CalculatedImpact,
        attributes: impactContent.attributes,
        assetIssueAssetIssueImpactTypeMapID:
          efficiencyImpact.AssetIssueAssetIssueImpactTypeMapID,
        assetIssueImpactTypeID: efficiencyImpact.AssetIssueImpactTypeID,
        calculationFactorID: impactContent.calculationFactorID, // this is dropdown AssetIssueImpactCalculationFactorID
        impactQuantity: impactContent.impactQuantity,
        impactRange: efficiencyImpactRange,
        totalCost: {
          toDate: impactContent.totalCost.toDate,
          potentialFuture: impactContent.totalCost.potentialFuture,
          totalAvg: impactContent.totalCost.totalAvg,
        },
        efficiencyErrors: efficiencyErrors,
        errors: errors,
      };
      if (effImpact.overrideTotal === CalculationType.UserDefinedImpact) {
        const currentDay = moment({
          year: moment().year(),
          month: moment().month(),
          day: moment().date(),
        }).toDate();
        effImpact.impactRange = {
          StartDate: currentDay,
          CurrentDate: currentDay,
          EndDate: currentDay,
          DaysElapsed: 0,
          DaysPending: 0,
          DaysTotal: 0,
        };
      }
      return effImpact;
    } else if (efficiencyImpact?.InputString) {
      const inputArray = efficiencyImpact?.InputString.split(',');
      if (inputArray && inputArray.length > 0) {
        const effImpact: EfficiencyParameters = {
          version: 1,
          assetIssueAssetIssueImpactTypeMapID:
            efficiencyImpact.AssetIssueAssetIssueImpactTypeMapID,
          assetIssueImpactTypeID: efficiencyImpact.AssetIssueImpactTypeID,
          overrideTotal: CalculationType.CalculatedImpact,
          calculationFactorID: parseInt(inputArray[0], 10), // this is dropdown AssetIssueImpactCalculationFactorID
          impactQuantity: parseFloat(inputArray[1]),
          impactRange: efficiencyImpactRange,
          attributes: {},
          totalCost: {
            toDate: parseFloat(inputArray[2]),
            potentialFuture: parseFloat(inputArray[3]),
            totalAvg: 0.0,
          },
          efficiencyErrors: {
            consumableCost: false,
            consumptionRateValue: false,
            consumptionToCostRate: false,
          },
          errors: [],
        };
        if (effImpact.calculationFactorID === -1) {
          // Calculator was saved in uninitialized state on Legacy
          effImpact.calculationFactorID =
            DEFAULT_EFFICIENCY_CALCULATION_FACTOR_ID;
          effImpact.overrideTotal = CalculationType.UserDefinedImpact;
        }

        if (efficiencyImpact.Impact_Cost != null) {
          if (
            efficiencyImpact?.InputString === '0,0,0,0' ||
            efficiencyImpact?.InputString === '-1,0,0,0'
          ) {
            effImpact.overrideTotal = CalculationType.UserDefinedImpact;
            const currentDay = moment({
              year: moment().year(),
              month: moment().month(),
              day: moment().date(),
            }).toDate();
            effImpact.impactRange = {
              StartDate: currentDay,
              CurrentDate: currentDay,
              EndDate: currentDay,
              DaysElapsed: 0,
              DaysPending: 0,
              DaysTotal: 0,
            };
          }
          effImpact.totalCost.totalAvg = efficiencyImpact.Impact_Cost;
        }
        return effImpact;
      }
    }

    const d = new Date();
    const today = new Date(
      d.getFullYear(),
      d.getMonth(),
      d.getDate(),
      0,
      0,
      0,
      0
    );
    const emptyImpact: EfficiencyParameters = {
      version: legacyLoad ? 1 : 2,
      assetIssueAssetIssueImpactTypeMapID: -1,
      assetIssueImpactTypeID: AssetImpactType.Efficiency.valueOf(),
      overrideTotal: CalculationType.CalculatedImpact,
      calculationFactorID: DEFAULT_EFFICIENCY_CALCULATION_FACTOR_ID,
      attributes,
      errors: newErrors,
      efficiencyErrors: createEfficiencyErrors(newErrors),
      impactQuantity: 0.0,
      impactRange: {
        StartDate: today,
        CurrentDate: today,
        EndDate: today,
        DaysElapsed: 0,
        DaysPending: 0,
        DaysTotal: 0,
      },
      totalCost: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
    };
    return emptyImpact;
  }

  private generationImpactHelper(
    generationImpact,
    generationImpactRange: ImpactDateRange,
    scenarioCapacityCost: number,
    attributes: CalculationAttributes,
    legacyLoad: boolean,
    newErrors: string[]
  ) {
    if (generationImpact?.ImpactContent) {
      const impactContent = JSON.parse(
        generationImpact.ImpactContent
      ) as ProductionImpactContent;
      if (!impactContent.totalCost?.toDate) {
        impactContent.totalCost.toDate = 0;
      }
      if (!impactContent.totalCost?.potentialFuture) {
        impactContent.totalCost.potentialFuture = 0;
      }
      if (!impactContent.totalCost?.totalAvg) {
        impactContent.totalCost.totalAvg =
          impactContent.totalCost.toDate +
          impactContent.totalCost.potentialFuture;
      }
      const errors = impactContent?.errors ? impactContent.errors : [];
      const productionErrors = createProductionErrors(errors);
      const genImpact: GenerationParameters = {
        version: 2,
        assetIssueAssetIssueImpactTypeMapID:
          generationImpact.AssetIssueAssetIssueImpactTypeMapID,
        assetIssueImpactTypeID: generationImpact.AssetIssueImpactTypeID,
        overrideTotal:
          impactContent.overrideTotal === 'UserDefinedImpact'
            ? CalculationType.UserDefinedImpact
            : CalculationType.CalculatedImpact,
        impactCost: generationImpact.Impact_Cost,
        impactCostMonthly: generationImpact.Impact_Cost_Monthly,
        impactRange: generationImpactRange,
        calculationFactorID: impactContent.calculationFactorID,
        impactQuantity: impactContent.impactQuantity,
        percentTimeDerateInEffect: {
          toDate: impactContent.derate.percentTimeDerateInEffect.toDate,
          potentialFuture:
            impactContent.derate.percentTimeDerateInEffect.potentialFuture,
          totalAvg: impactContent.derate.percentTimeDerateInEffect.totalAvg,
        },
        attributes: impactContent.attributes,
        hasDerateOverride: impactContent.derate.hasDerateOverride,
        derateOverrideValue: impactContent.derate.derateOverrideValue,
        totalCost: {
          toDate: impactContent.totalCost.toDate,
          potentialFuture: impactContent.totalCost.potentialFuture,
          totalAvg: impactContent.totalCost.totalAvg,
        },
        productionErrors,
        errors: errors,
        relevantProductionCost: impactContent.relevantProductionCost,
        lostMarginOpportunity: impactContent.lostMarginOpportunity,
      };
      if (genImpact.overrideTotal === CalculationType.UserDefinedImpact) {
        genImpact.percentTimeDerateInEffect.toDate = 100.0;
        genImpact.percentTimeDerateInEffect.potentialFuture = 100.0;
        genImpact.percentTimeDerateInEffect.totalAvg = 100.0;
      }
      return genImpact;
    } else if (generationImpact?.InputString) {
      const inputArray = generationImpact.InputString?.split(',');
      if (inputArray && inputArray.length > 0) {
        const genImpact: GenerationParameters = {
          version: 1,
          assetIssueAssetIssueImpactTypeMapID:
            generationImpact.AssetIssueAssetIssueImpactTypeMapID,
          assetIssueImpactTypeID: generationImpact.AssetIssueImpactTypeID,
          overrideTotal: CalculationType.CalculatedImpact,
          impactCost: generationImpact.Impact_Cost,
          impactCostMonthly: generationImpact.Impact_Cost_Monthly,
          impactRange: generationImpactRange,
          calculationFactorID: parseInt(inputArray[0], 10), // // this is dropdown AssetIssueImpactCalculationFactorID
          impactQuantity: parseFloat(inputArray[1]),
          percentTimeDerateInEffect: {
            toDate: parseFloat(inputArray[2]),
            potentialFuture: parseFloat(inputArray[3]),
            totalAvg: parseFloat(inputArray[4]),
          },
          errors: [],
          attributes: {},
          hasDerateOverride: false,
          derateOverrideValue: 100,
          totalCost: {
            toDate: parseFloat(inputArray[5]),
            potentialFuture: parseFloat(inputArray[6]),
            totalAvg: 0.0,
          },
          productionErrors: {
            productionCapacity: false,
            productionTimeToDays: false,
            productionRevenue: false,
          },
          relevantProductionCost: parseFloat(inputArray[7]),
          lostMarginOpportunity:
            inputArray.length > 8
              ? parseFloat(inputArray[8])
              : scenarioCapacityCost,
        };
        if (genImpact.calculationFactorID === -1) {
          // Calculator was saved in uninitialized state on Legacy
          genImpact.calculationFactorID =
            DEFAULT_GENERATION_CALCULATION_FACTOR_ID;
          genImpact.overrideTotal = CalculationType.UserDefinedImpact;
        }
        if (generationImpact.Impact_Cost != null) {
          if (
            generationImpact?.InputString === '-1,0,0,0,0,0,0,0' ||
            generationImpact?.InputString === '0,0,0,0,0,0,0,0' ||
            genImpact.calculationFactorID === -1
          ) {
            genImpact.overrideTotal = CalculationType.UserDefinedImpact;
            genImpact.percentTimeDerateInEffect.toDate = 100.0;
            genImpact.percentTimeDerateInEffect.potentialFuture = 100.0;
            genImpact.percentTimeDerateInEffect.totalAvg = 100.0;
            if (
              genImpact.totalCost.toDate === 0.0 &&
              genImpact.totalCost.potentialFuture === 0.0
            ) {
              genImpact.totalCost.potentialFuture =
                generationImpact.Impact_Cost;
            }
          }
          const derateTotal =
            (genImpact.percentTimeDerateInEffect.toDate +
              genImpact.percentTimeDerateInEffect.potentialFuture) /
            2.0;
          const calcsCorrect =
            Math.abs(
              derateTotal - genImpact.percentTimeDerateInEffect.totalAvg
            ) <= 1;
          if (!calcsCorrect) {
            genImpact.hasDerateOverride = true;
            genImpact.derateOverrideValue =
              genImpact.percentTimeDerateInEffect.totalAvg;
          }
          genImpact.totalCost.totalAvg = generationImpact.Impact_Cost;
        }
        return genImpact;
      }
    }

    // legacy left everything essentially empty
    const emptyImpact: GenerationParameters = {
      version: legacyLoad ? 1 : 2,
      assetIssueAssetIssueImpactTypeMapID: -1,
      assetIssueImpactTypeID: AssetImpactType.Reliability.valueOf(),
      overrideTotal: CalculationType.CalculatedImpact,
      impactCost: 0.0,
      impactCostMonthly: 0.0,
      impactRange: {
        StartDate: moment().toDate(),
        CurrentDate: moment().toDate(),
        EndDate: moment().toDate(),
        DaysElapsed: 0,
        DaysPending: 0,
        DaysTotal: 0,
      },
      attributes,
      errors: newErrors,
      productionErrors: createProductionErrors(newErrors),
      calculationFactorID: DEFAULT_GENERATION_CALCULATION_FACTOR_ID,
      impactQuantity: 0.0,
      percentTimeDerateInEffect: {
        toDate: 100.0,
        potentialFuture: 100.0,
        totalAvg: 100.0,
      },
      hasDerateOverride: false,
      derateOverrideValue: 100.0,
      totalCost: {
        toDate: 0.0,
        potentialFuture: 0.0,
        totalAvg: 0.0,
      },
      lostMarginOpportunity: scenarioCapacityCost,
      relevantProductionCost: 0.0,
    };
    return emptyImpact;
  }

  createProductionCalsForSave(
    productionCalcs: ProductionCalculationParameters,
    generationParameters: GenerationParameters
  ): ImpactTypeMapV2Save {
    let inputString = '';
    let impactContent = '';
    if (generationParameters.version === 1) {
      if (
        productionCalcs.calculationType === CalculationType.CalculatedImpact
      ) {
        inputString =
          productionCalcs.calculation.calculationFactorID.toString() + ',';
        inputString +=
          productionCalcs.calculation.impactQuantity.toString() + ',';
        if (productionCalcs.calculation.hasDerateOverride) {
          inputString += '100,';
          inputString += '100,';
          inputString += productionCalcs.calculation.derateOverride.toString();
        } else {
          inputString +=
            productionCalcs.calculation.derateElapsed.toString() + ',';
          inputString +=
            productionCalcs.calculation.deratePending.toString() + ',';
          inputString += (
            (productionCalcs.calculation.derateElapsed +
              productionCalcs.calculation.deratePending) /
            2
          ).toString();
        }
      } else {
        inputString = '-1,0,0,0,0';
      }
      if (
        productionCalcs.calculationType === CalculationType.CalculatedImpact
      ) {
        inputString +=
          ',' + productionCalcs.calculation.costCalculation.elapsed.toString();
        inputString +=
          ',' + productionCalcs.calculation.costCalculation.pending.toString();
      } else {
        inputString += ',' + productionCalcs.costOverride.elapsed.toString();
        inputString += ',' + productionCalcs.costOverride.pending.toString();
      }
      inputString +=
        ',' + productionCalcs.calculation.relevantProductionCost.toString();
      inputString +=
        ',' + productionCalcs.calculation.lostMarginOpportunity.toString();
    } else {
      const impactToSave: ProductionImpactContent = {
        version: 2,
        errors: generationParameters.errors,
        attributes: generationParameters.attributes,
        totalCost:
          productionCalcs.calculationType === CalculationType.CalculatedImpact
            ? {
                toDate: productionCalcs.calculation.costCalculation.elapsed,
                potentialFuture:
                  productionCalcs.calculation.costCalculation.pending,
                totalAvg: productionCalcs.calculation.costCalculation.total,
              }
            : {
                toDate: productionCalcs.costOverride.elapsed,
                potentialFuture: productionCalcs.costOverride.pending,
                totalAvg: productionCalcs.costOverride.total,
              },
        impactQuantity: productionCalcs.calculation.impactQuantity,
        calculationFactorID: productionCalcs.calculation.calculationFactorID,
        derate: {
          hasDerateOverride: productionCalcs.calculation.hasDerateOverride,
          derateOverrideValue: productionCalcs.calculation.derateOverride,
          percentTimeDerateInEffect: {
            toDate: productionCalcs.calculation.derateElapsed,
            potentialFuture: productionCalcs.calculation.deratePending,
            totalAvg:
              (productionCalcs.calculation.derateElapsed +
                productionCalcs.calculation.deratePending) /
              2,
          },
        },
        relevantProductionCost:
          productionCalcs.calculation.relevantProductionCost,
        lostMarginOpportunity:
          productionCalcs.calculation.lostMarginOpportunity,
        overrideTotal: productionCalcs.calculationType,
      };
      impactContent = JSON.stringify(impactToSave);
    }

    const genImpact: ImpactTypeMapV2Save = {
      InputString: inputString,
      ImpactContent: impactContent,
      Impact: productionCalcs.calculation.unitDerate,
      AssetIssueAssetIssueImpactTypeMapID:
        productionCalcs.assetIssueAssetIssueImpactTypeMapID,
      CategoryID: AssetImpactCategory.Reliability.valueOf(),
      AssetIssueImpactTypeID: productionCalcs.calculation.calculatorCategory,
      Impact_Cost:
        productionCalcs.calculationType === CalculationType.CalculatedImpact
          ? productionCalcs.calculation.costCalculation.total
          : productionCalcs.costOverride.total,
      Impact_Cost_Monthly:
        productionCalcs.calculationType === CalculationType.CalculatedImpact
          ? productionCalcs.monthlyCost.total
          : 0,
    };
    return genImpact;
  }

  createEfficiencyCalcsForSave(
    effCalcs: EfficiencyCalculationParameters,
    efficiencyParameters: EfficiencyParameters
  ): ImpactTypeMapV2Save {
    let inputString = '';
    let impactContent = '';
    if (efficiencyParameters.version === 1) {
      if (effCalcs.calculationType === CalculationType.CalculatedImpact) {
        inputString = effCalcs.calculation.calculationFactorID.toString() + ',';
        inputString += effCalcs.calculation.impactQuantity.toString();
        inputString +=
          ',' + effCalcs.calculation.costCalculation.elapsed.toString();
        inputString +=
          ',' + effCalcs.calculation.costCalculation.pending.toString();
      } else {
        inputString = '-1,0';
        inputString += ',' + effCalcs.costOverride.elapsed.toString();
        inputString += ',' + effCalcs.costOverride.pending.toString();
      }
    } else {
      const effImpact: EfficiencyImpactContent = {
        version: 2,
        attributes: efficiencyParameters.attributes,
        errors: efficiencyParameters.errors,
        totalCost:
          effCalcs.calculationType === CalculationType.CalculatedImpact
            ? {
                toDate: effCalcs.calculation.costCalculation.elapsed,
                potentialFuture: effCalcs.calculation.costCalculation.pending,
                totalAvg: effCalcs.calculation.costCalculation.total,
              }
            : {
                toDate: effCalcs.costOverride.elapsed,
                potentialFuture: effCalcs.costOverride.pending,
                totalAvg: effCalcs.costOverride.total,
              },
        impactQuantity: effCalcs.calculation.impactQuantity,
        calculationFactorID: effCalcs.calculation.calculationFactorID,
        overrideTotal: effCalcs.calculationType,
      };
      impactContent = JSON.stringify(effImpact);
    }

    const effImpact: ImpactTypeMapV2Save = {
      InputString: inputString,
      ImpactContent: impactContent,
      Impact: effCalcs.calculation.consumptionRateImpact,
      AssetIssueAssetIssueImpactTypeMapID:
        effCalcs.assetIssueAssetIssueImpactTypeMapID,
      AssetIssueImpactTypeID: effCalcs.assetIssueImpactTypeID,
      CategoryID: AssetImpactCategory.Efficiency.valueOf(),
      Impact_Cost:
        effCalcs.calculationType === CalculationType.CalculatedImpact
          ? effCalcs.calculation.costCalculation.total
          : effCalcs.costOverride.total,
      Impact_Cost_Monthly:
        effCalcs.calculationType === CalculationType.CalculatedImpact
          ? effCalcs.monthlyCost.total
          : 0,
    };
    return effImpact;
  }

  createMaintenanceCalcsForSave(
    mCalcs: MaintenanceCalculationParameters
  ): ImpactTypeMapSave {
    let inputString = '';
    if (mCalcs.calculationType === CalculationType.CalculatedImpact) {
      inputString = mCalcs.calculation.partsElapsed.toString();
      inputString += ',' + mCalcs.calculation.partsPending.toString();
      inputString +=
        ',' +
        (
          mCalcs.calculation.partsElapsed + mCalcs.calculation.partsPending
        ).toString();
      inputString += ',' + mCalcs.calculation.laborHoursElapsed.toString();
      inputString += ',' + mCalcs.calculation.laborHoursPending.toString();
      inputString +=
        ',' +
        (
          mCalcs.calculation.laborHoursElapsed +
          mCalcs.calculation.laborHoursPending
        ).toString();
      inputString += ',' + mCalcs.calculation.laborRateElapsed.toString();
      inputString += ',' + mCalcs.calculation.laborRatePending.toString();
      inputString +=
        ',' +
        (
          (mCalcs.calculation.laborRateElapsed +
            mCalcs.calculation.laborRatePending) /
          2
        ).toString();
      inputString +=
        ',' + mCalcs.calculation.costCalculation.elapsed.toString();
      inputString +=
        ',' + mCalcs.calculation.costCalculation.pending.toString();
    } else {
      inputString = '0,0,0,0,0,0,0,0,0';
      inputString += ',' + mCalcs.costOverride.elapsed.toString();
      inputString += ',' + mCalcs.costOverride.pending.toString();
    }

    const mainImpact: ImpactTypeMapSave = {
      InputString: inputString,
      Impact: mCalcs.calculation.costCalculation.total,
      AssetIssueAssetIssueImpactTypeMapID:
        mCalcs.assetIssueAssetIssueImpactTypeMapID,
      AssetIssueImpactTypeID: mCalcs.assetIssueImpactTypeID,
      CategoryID: AssetImpactCategory.Maintenance.valueOf(),
      Impact_Cost:
        mCalcs.calculationType === CalculationType.CalculatedImpact
          ? mCalcs.calculation.costCalculation.total
          : mCalcs.costOverride.total,
      Impact_Cost_Monthly: 0,
    };
    return mainImpact;
  }

  createOtherCalcsForSave(
    oCalcs: OtherCalculationParameters
  ): ImpactTypeMapSave {
    let inputString = '';
    if (oCalcs.calculationType === CalculationType.CalculatedImpact) {
      inputString = oCalcs.calculation.rateElapsed.toString();
      inputString += ',' + oCalcs.calculation.ratePending.toString();
      inputString +=
        ',' +
        (
          (oCalcs.calculation.ratePending + oCalcs.calculation.rateElapsed) /
          2
        ).toString();
    } else {
      inputString = '0,0,0';
    }
    if (oCalcs.calculationType === CalculationType.CalculatedImpact) {
      inputString += ',' + oCalcs.calculation.costCalculation.elapsed;
      inputString += ',' + oCalcs.calculation.costCalculation.pending;
      inputString += ',' + oCalcs.calculation.costCalculation.total;
    } else {
      inputString += ',' + oCalcs.costOverride.elapsed;
      inputString += ',' + oCalcs.costOverride.pending;
      inputString += ',' + oCalcs.costOverride.total;
    }

    const otherImpact: ImpactTypeMapSave = {
      InputString: inputString,
      Impact:
        (oCalcs.calculation.ratePending + oCalcs.calculation.rateElapsed) / 2,
      AssetIssueAssetIssueImpactTypeMapID:
        oCalcs.assetIssueAssetIssueImpactTypeMapID,
      AssetIssueImpactTypeID: oCalcs.assetIssueImpactTypeID,
      CategoryID: AssetImpactCategory.Other.valueOf(),
      Impact_Cost:
        oCalcs.calculationType === CalculationType.CalculatedImpact
          ? oCalcs.calculation.costCalculation.total
          : oCalcs.costOverride.total,
      Impact_Cost_Monthly:
        oCalcs.calculationType === CalculationType.CalculatedImpact
          ? oCalcs.monthlyCost.total
          : 0,
    };
    return otherImpact;
  }

  createToday() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
  }
}
