import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { ImpactScenario } from '../../model/impact-calculator/scenario';
import { ImpactCalculatorService } from './impact-calculator.service';
import produce from 'immer';
import {
  MaintenanceCalculatedCost,
  OtherCalculatedCost,
} from './impact-calculations.service';
import { MatSelectChange } from '@angular/material/select';
import {
  ImpactCalculatorSaveReturn,
  SaveUpdates,
} from '../../model/impact-calculator/impact-calculator-save';
import { ToastService } from '@atonix/shared/utils';
import { IssuesFrameworkService } from '@atonix/shared/api';
import {
  AssetImpactType,
  DEFAULT_GENERATION_CALCULATION_FACTOR_ID,
} from '../../model/issue-model';
import { Router } from '@angular/router';
import {
  BlankScenario,
  CalculationFactors,
  CalculationType,
  ImpactCalculatorSave,
  ImpactScenarioSave,
} from '@atonix/atx-core';
import {
  CostPeriods,
  EfficiencyCalculationParameters,
  ImpactTotals,
  MaintenanceCalculationParameters,
  OtherCalculationParameters,
  ProductionCalculationParameters,
  TotalCalculations,
} from '../../model/impact-calculator/calculation-parameters';

export interface ImpactCalculatorState {
  scenarios: ImpactScenario[];
  blankScenario: BlankScenario;
  productionCalcs: ProductionCalculationParameters[];
  efficiencyCalcs: EfficiencyCalculationParameters[];
  maintenanceCalcs: MaintenanceCalculationParameters[];
  otherCalcs: OtherCalculationParameters[];
  saveUpdates: SaveUpdates;
  hasOther: boolean;
  hasMaintenance: boolean;
  hasGeneration: boolean;
  hasEfficiency: boolean;
  hasReliability: boolean;
  hasDerate: boolean;
  accessControlLoaded: boolean;
  scenarioCalcs: TotalCalculations[];
  impactTotals: ImpactTotals;
  productionCalcDropDown: CalculationFactors[];
  efficiencyCalcDropDown: CalculationFactors[];
  assetIssueID: number;
  issueID: string;
  scenarioIDsToDelete: number[];
  loading: boolean;
  saving: boolean;
  pristine: boolean;
  validationComplete: boolean;
  isValid: boolean;
}

const initialState: ImpactCalculatorState = {
  scenarios: null,
  loading: true,
  blankScenario: null,
  scenarioCalcs: null,
  efficiencyCalcs: null,
  productionCalcs: null,
  maintenanceCalcs: null,
  otherCalcs: null,
  hasReliability: false,
  hasDerate: false,
  hasOther: false,
  hasMaintenance: false,
  hasGeneration: false,
  hasEfficiency: false,
  accessControlLoaded: false,
  saveUpdates: null,
  impactTotals: {
    productionTotal: 0.0,
    productionDuration: 0,
    productionMonthly: 0.0,
    efficiencyTotal: 0.0,
    efficiencyDuration: 0,
    efficiencyMonthly: 0.0,
    maintenanceTotal: 0.0,
    otherTotal: 0.0,
    otherDuration: 0,
    otherMonthly: 0.0,
    totalImpact: 0.0,
    probabilityWeightedTotalImpact: 0.0,
    monthlyImpact: 0.0,
    probabilityWeightedMonthlyImpact: 0.0,
  },
  efficiencyCalcDropDown: null,
  productionCalcDropDown: null,
  assetIssueID: -1,
  issueID: null,
  scenarioIDsToDelete: [],
  saving: false,
  pristine: true,
  validationComplete: false,
  isValid: true,
};
let _state = initialState;

@Injectable({
  providedIn: 'root',
})
export class ImpactCalculatorFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  constructor(
    private impactCalculatorService: ImpactCalculatorService,
    private issuesFrameworkService: IssuesFrameworkService,
    private snackBarService: ToastService,
    private router: Router
  ) {}

  private store = new BehaviorSubject<ImpactCalculatorState>(_state);
  private state$ = this.store.asObservable();

  scenarios$ = this.state$.pipe(
    map((state) => state.scenarios),
    distinctUntilChanged()
  );
  loading$ = this.state$.pipe(
    map((state) => state.loading),
    distinctUntilChanged()
  );
  productionCalcs$ = this.state$.pipe(
    map((state) => state.productionCalcs),
    distinctUntilChanged()
  );
  efficiencyCalcs$ = this.state$.pipe(
    map((state) => state.efficiencyCalcs),
    distinctUntilChanged()
  );
  maintenanceCalcs$ = this.state$.pipe(
    map((state) => state.maintenanceCalcs),
    distinctUntilChanged()
  );
  otherCalcs$ = this.state$.pipe(
    map((state) => state.otherCalcs),
    distinctUntilChanged()
  );
  scenarioCalcs$ = this.state$.pipe(
    map((state) => state.scenarioCalcs),
    distinctUntilChanged()
  );
  impactTotals$ = this.state$.pipe(
    map((state) => state.impactTotals),
    distinctUntilChanged()
  );
  efficiencyCalcDropDown$ = this.state$.pipe(
    map((state) => state.efficiencyCalcDropDown),
    distinctUntilChanged()
  );
  productionCalcDropDown$ = this.state$.pipe(
    map((state) => state.productionCalcDropDown),
    distinctUntilChanged()
  );
  blankScenario$ = this.state$.pipe(
    map((state) => state.blankScenario),
    distinctUntilChanged()
  );
  assetIssueID$ = this.state$.pipe(
    map((state) => state.assetIssueID),
    distinctUntilChanged()
  );
  issueID$ = this.state$.pipe(
    map((state) => state.issueID),
    distinctUntilChanged()
  );
  scenarioIDsToDelete$ = this.state$.pipe(
    map((state) => state.scenarioIDsToDelete),
    distinctUntilChanged()
  );
  saving$ = this.state$.pipe(
    map((state) => state.saving),
    distinctUntilChanged()
  );
  hasOther$ = this.state$.pipe(
    map((state) => state.hasOther),
    distinctUntilChanged()
  );
  hasMaintenance$ = this.state$.pipe(
    map((state) => state.hasMaintenance),
    distinctUntilChanged()
  );
  hasGeneration$ = this.state$.pipe(
    map((state) => state.hasGeneration),
    distinctUntilChanged()
  );
  hasEfficiency$ = this.state$.pipe(
    map((state) => state.hasEfficiency),
    distinctUntilChanged()
  );
  accessControlLoaded$ = this.state$.pipe(
    map((state) => state.accessControlLoaded),
    distinctUntilChanged()
  );
  pristine$ = this.state$.pipe(
    map((state) => state.pristine),
    distinctUntilChanged()
  );
  saveUpdates$ = this.state$.pipe(
    map((state) => state.saveUpdates),
    distinctUntilChanged()
  );
  hasDerate$ = this.state$.pipe(
    map((state) => state.hasDerate),
    distinctUntilChanged()
  );
  hasReliability$ = this.state$.pipe(
    map((state) => state.hasReliability),
    distinctUntilChanged()
  );
  validationComplete$ = this.state$.pipe(
    map((state) => state.validationComplete),
    distinctUntilChanged()
  );
  isValid$ = this.state$.pipe(
    map((state) => state.isValid),
    distinctUntilChanged()
  );
  vm$: Observable<ImpactCalculatorState> = combineLatest([
    this.scenarios$,
    this.loading$,
    this.saving$,
    this.productionCalcs$,
    this.efficiencyCalcs$,
    this.maintenanceCalcs$,
    this.otherCalcs$,
    this.scenarioCalcs$,
    this.impactTotals$,
    this.efficiencyCalcDropDown$,
    this.productionCalcDropDown$,
    this.blankScenario$,
    this.assetIssueID$,
    this.issueID$,
    this.scenarioIDsToDelete$,
    this.hasOther$,
    this.hasMaintenance$,
    this.hasGeneration$,
    this.hasEfficiency$,
    this.accessControlLoaded$,
    this.pristine$,
    this.saveUpdates$,
    this.hasReliability$,
    this.hasDerate$,
    this.validationComplete$,
    this.isValid$,
  ]).pipe(
    map(
      ([
        scenarios,
        loading,
        saving,
        productionCalcs,
        efficiencyCalcs,
        maintenanceCalcs,
        otherCalcs,
        scenarioCalcs,
        impactTotals,
        efficiencyCalcDropDown,
        productionCalcDropDown,
        blankScenario,
        assetIssueID,
        issueID,
        scenarioIDsToDelete,
        hasOther,
        hasMaintenance,
        hasGeneration,
        hasEfficiency,
        accessControlLoaded,
        pristine,
        saveUpdates,
        hasReliability,
        hasDerate,
        validationComplete,
        isValid,
      ]) => {
        return {
          scenarios,
          loading,
          saving,
          productionCalcs,
          efficiencyCalcs,
          maintenanceCalcs,
          otherCalcs,
          scenarioCalcs,
          impactTotals,
          efficiencyCalcDropDown,
          productionCalcDropDown,
          blankScenario,
          assetIssueID,
          issueID,
          scenarioIDsToDelete,
          hasOther,
          hasMaintenance,
          hasGeneration,
          hasEfficiency,
          accessControlLoaded,
          pristine,
          saveUpdates,
          hasReliability,
          hasDerate,
          validationComplete,
          isValid,
        } as ImpactCalculatorState;
      }
    )
  );

  reset() {
    this.updateState({ ...initialState });
  }

  updateGenerationCategory(event: MatSelectChange, scenario) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const productionCalcs = produce(vm.productionCalcs, (draftState) => {
        draftState[scenario].calculation.calculationFactorID =
          DEFAULT_GENERATION_CALCULATION_FACTOR_ID;
        draftState[scenario].calculation.calculatorCategory = event.value;
      });
      this.updateState({ ..._state, productionCalcs, pristine: false });
      this.updateTotals();
    });
  }
  changeCalculationFactor(event: MatSelectChange, scenario) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const productionCalcs = produce(vm.productionCalcs, (draftState) => {
        draftState[scenario].calculation.calculationFactorID = event.value;
      });
      this.updateState({ ..._state, productionCalcs, pristine: false });
      this.updateTotals();
    });
  }
  changeEfficiencyCalculationFactor(event: MatSelectChange, scenario) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const efficiencyCalcs = produce(vm.efficiencyCalcs, (draftState) => {
        draftState[scenario].calculation.calculationFactorID = event.value;
      });
      this.updateState({ ..._state, efficiencyCalcs, pristine: false });
      this.updateTotals();
    });
  }

  updateCalculator(assetIssueID: number, assetId: number, issueID: string) {
    this.updateState({
      ..._state,
      assetIssueID,
      issueID,
      scenarios: null,
      loading: true,
      blankScenario: null,
      scenarioCalcs: null,
      efficiencyCalcs: null,
      productionCalcs: null,
      maintenanceCalcs: null,
      otherCalcs: null,
      impactTotals: {
        productionTotal: 0.0,
        productionDuration: 0,
        productionMonthly: 0.0,
        efficiencyTotal: 0.0,
        efficiencyDuration: 0,
        efficiencyMonthly: 0.0,
        maintenanceTotal: 0.0,
        otherTotal: 0.0,
        otherDuration: 0,
        otherMonthly: 0.0,
        totalImpact: 0.0,
        probabilityWeightedTotalImpact: 0.0,
        monthlyImpact: 0.0,
        probabilityWeightedMonthlyImpact: 0.0,
      },
      efficiencyCalcDropDown: null,
      productionCalcDropDown: null,
      saving: false,
      pristine: true,
    });

    this.issuesFrameworkService
      .getBlankScenario(assetId)
      .subscribe((scenarioReturn) => {
        const blankScenario: BlankScenario = {
          AssetIssueImpactScenarioID:
            scenarioReturn.AssetIssueImpactScenarioID || -1,
          AssetIssueImpactScenarioName:
            scenarioReturn.AssetIssueImpactScenarioName || 'New Scenario',
          CreatedByUserID: scenarioReturn.CreatedByUserID,
          NetUnitHeatRate: scenarioReturn.NetUnitHeatRate || 0,
          UnitCapability: scenarioReturn.UnitCapability || 0,
          CapacityFactor: scenarioReturn.CapacityFactor || 0,
          FuelCost: scenarioReturn.FuelCost || 0,
          CapacityCost: scenarioReturn.CapacityCost || 0,
          PercentLikelihood: scenarioReturn.PercentLikelihood || 100,
          ImpactAttributesList: scenarioReturn.ImpactAttributesList,
        };
        this.updateState({ ..._state, blankScenario });
        this.updateTotals();
      });

    this.issuesFrameworkService
      .getAssetIssueWithOptions(issueID)
      .pipe(take(1))
      .subscribe((issue) => {
        let hasOther = false;
        let hasMaintenance = false;
        let hasGeneration = false;
        let hasEfficiency = false;
        let hasReliability = false;
        let hasDerate = false;
        issue.Impacts.forEach((element) => {
          if (element.AssetIssueImpactTypeID === 9) {
            hasOther = true;
          } else if (element.AssetIssueImpactTypeID === 10) {
            hasMaintenance = true;
          } else if (element.AssetIssueImpactTypeID === 2) {
            hasGeneration = true;
            hasReliability = true;
          } else if (element.AssetIssueImpactTypeID === 3) {
            hasGeneration = true;
            hasDerate = true;
          } else if (element.AssetIssueImpactTypeID === 1) {
            hasEfficiency = true;
          }
        });

        this.updateState({
          ..._state,
          hasOther,
          hasMaintenance,
          hasGeneration,
          hasEfficiency,
          hasReliability,
          hasDerate,
          accessControlLoaded: true,
        });
        this.updateTotals();
      });
    this.issuesFrameworkService
      .getImpactCalculationFactorsByImpactCategoryType(1)
      .subscribe((categories) => {
        this.updateState({ ..._state, efficiencyCalcDropDown: categories });
        this.updateTotals();
      });
    this.issuesFrameworkService
      .getImpactCalculationFactorsByImpactCategoryType(3)
      .subscribe((categories) => {
        this.updateState({ ..._state, productionCalcDropDown: categories });
        this.updateTotals();
      });
    this.impactCalculatorService
      .getImpactCalculationsForImpactCategory(assetIssueID)
      .pipe(take(1))
      .subscribe((scenarios: ImpactScenario[]) => {
        this.scenarioUpdater(scenarios);
      });
  }

  scenarioFactory(scenarios: ImpactScenario[]) {
    const scenarioTuple: [
      Array<ProductionCalculationParameters>,
      Array<EfficiencyCalculationParameters>,
      Array<MaintenanceCalculationParameters>,
      Array<OtherCalculationParameters>,
      Array<TotalCalculations>
    ] = [[], [], [], [], []];

    scenarios.forEach((scenario: ImpactScenario) => {
      if (scenario.generationParameters.assetIssueImpactTypeID === 0) {
        // if the calculator was never set, the category needs to be
        // manually set to 'Derate' - category 3
        scenario.generationParameters.assetIssueImpactTypeID = 3;
      }
      scenario.setDefaults();

      scenarioTuple[0].push(scenario.createProductionCalculationParameters());
      scenarioTuple[1].push(scenario.createEfficiencyCalculationParameters());
      scenarioTuple[2].push(scenario.createMaintenanceCalculationParameters());
      scenarioTuple[3].push(scenario.createOtherCalculationParameters());
      scenarioTuple[4].push(scenario.createTotalCalculations());
    });
    return scenarioTuple;
  }

  scenarioUpdater(scenarios: ImpactScenario[]) {
    const scenarioUpdates = this.scenarioFactory(scenarios);
    let pristine = true;
    if (scenarios.length === 0) {
      pristine = false;
    }
    this.updateState({
      ..._state,
      scenarios,
      efficiencyCalcs: scenarioUpdates[1],
      scenarioCalcs: scenarioUpdates[4],
      otherCalcs: scenarioUpdates[3],
      maintenanceCalcs: scenarioUpdates[2],
      productionCalcs: scenarioUpdates[0],
      loading: false,
      pristine,
    });
    this.updateTotals();
  }

  updateProductionFormValue(
    productionCalcs: ProductionCalculationParameters[]
  ) {
    this.updateState({
      ..._state,
      productionCalcs,
      pristine: false,
    });
    this.updateTotals();
  }

  updateEfficiencyFormValue(
    efficiencyCalcs: EfficiencyCalculationParameters[]
  ) {
    this.updateState({
      ..._state,
      efficiencyCalcs,
      pristine: false,
    });
    this.updateTotals();
  }

  updateScenarioCalcValues(
    scenarioCalcs: TotalCalculations[],
    updateTotals: boolean
  ) {
    this.updateState({
      ..._state,
      scenarioCalcs,
      pristine: false,
    });
    if (updateTotals) {
      this.updateTotals();
    }
  }

  updateMaintenanceCalcValues(
    maintenanceCalcs: MaintenanceCalculationParameters[]
  ) {
    this.updateState({
      ..._state,
      maintenanceCalcs,
      pristine: false,
    });
    this.updateTotals();
  }

  updateOtherCalcValues(otherCalcs: OtherCalculationParameters[]) {
    this.updateState({
      ..._state,
      otherCalcs,
      pristine: false,
    });
    this.updateTotals();
  }

  removeScenario(scenarioIndex: number) {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const newScenarios = produce(vm.scenarios, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });
      const efficiencyCalcs = produce(vm.efficiencyCalcs, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });
      const scenarioCalcs = produce(vm.scenarioCalcs, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });
      const otherCalcs = produce(vm.otherCalcs, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });
      const maintenanceCalcs = produce(vm.maintenanceCalcs, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });

      const productionCalcs = produce(vm.productionCalcs, (draftState) => {
        draftState.splice(scenarioIndex, 1);
      });

      const scenarioIDsToDelete = produce(
        vm.scenarioIDsToDelete,
        (draftState) => {
          if (vm.scenarios[scenarioIndex].scenarioID > 0) {
            draftState.push(vm.scenarios[scenarioIndex].scenarioID);
          }
        }
      );

      this.updateState({
        ..._state,
        scenarios: newScenarios,
        efficiencyCalcs,
        scenarioCalcs,
        otherCalcs,
        maintenanceCalcs,
        productionCalcs,
        scenarioIDsToDelete,
        pristine: false,
      });
      this.updateTotals();
    });
  }

  addBlankScenario(vm: ImpactCalculatorState) {
    let newGuid = 0;
    if (vm.scenarios.length > 0) {
      newGuid =
        // eslint-disable-next-line prefer-spread
        Math.max.apply(
          Math,
          vm.scenarios.map((o) => o.scenarioGuidID)
        ) + 1;
    }

    const impactScenario = this.impactCalculatorService.createBlankScenario(
      vm,
      newGuid
    );
    const scenarioUpdates = this.scenarioFactory([impactScenario]);
    const newScenarios = produce(vm.scenarios, (draftState) => {
      draftState.push(impactScenario);
    });
    const efficiencyCalcs = produce(vm.efficiencyCalcs, (draftState) => {
      draftState.push(scenarioUpdates[1][0]);
    });
    const scenarioCalcs = produce(vm.scenarioCalcs, (draftState) => {
      draftState.push(scenarioUpdates[4][0]);
    });
    const otherCalcs = produce(vm.otherCalcs, (draftState) => {
      draftState.push(scenarioUpdates[3][0]);
    });
    const maintenanceCalcs = produce(vm.maintenanceCalcs, (draftState) => {
      draftState.push(scenarioUpdates[2][0]);
    });

    const productionCalcs = produce(vm.productionCalcs, (draftState) => {
      draftState.push(scenarioUpdates[0][0]);
    });
    this.updateState({
      ..._state,
      scenarios: newScenarios,
      efficiencyCalcs,
      scenarioCalcs,
      otherCalcs,
      maintenanceCalcs,
      productionCalcs,
      pristine: false,
    });
    this.updateTotals();
  }
  updateTotals() {
    this.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        !vm.otherCalcs ||
        !vm.efficiencyCalcDropDown ||
        !vm.productionCalcDropDown ||
        !vm.blankScenario ||
        !vm.accessControlLoaded
      ) {
        // need five different api calls before we can calculate this asynchronous puzzle
        return;
      }

      if (!vm.validationComplete) {
        this.updateState({ ..._state, validationComplete: true });
        this.validateCalculator(vm);
        return this.updateTotals();
      }

      if (vm.scenarios.length === 0) {
        this.addBlankScenario(vm);
        return;
      }

      // uses immer to simplify deep updates to an immutable object
      // see: https://immerjs.github.io/immer/docs/produce
      // and: https://medium.com/hackernoon/introducing-immer-immutability-the-easy-way-9d73d8f71cb3
      let productionTotal = 0.0;
      let productionMonthly = 0.0;
      let productionDuration = 0.0;
      let efficiencyTotal = 0.0;
      let efficiencyMonthly = 0.0;
      let efficiencyDuration = 0.0;
      let maintenanceTotal = 0.0;
      let otherTotal = 0.0;
      let otherMonthly = 0.0;
      let otherDuration = 0.0;
      let totalImpact = 0.0;
      let probabilityWeightedTotalImpact = 0.0;
      let monthlyImpact = 0.0;
      let probabilityWeightedMonthlyImpact = 0.0;
      const newOtherCalcState = produce(vm.otherCalcs, (draftState) => {
        vm.otherCalcs.forEach((otherCalcs, scenario) => {
          if (otherCalcs.calculationType === CalculationType.CalculatedImpact) {
            draftState[scenario].calculation.costCalculation.elapsed =
              OtherCalculatedCost(
                otherCalcs.calculation.rateElapsed,
                otherCalcs.calculation.daysElapsed
              );
            draftState[scenario].calculation.costCalculation.pending =
              OtherCalculatedCost(
                otherCalcs.calculation.ratePending,
                otherCalcs.calculation.daysPending
              );

            const totalDays =
              otherCalcs.calculation.daysPending +
              otherCalcs.calculation.daysElapsed;
            const totalCost =
              draftState[scenario].calculation.costCalculation.elapsed +
              draftState[scenario].calculation.costCalculation.pending;
            draftState[scenario].calculation.costCalculation.total = totalCost;

            draftState[scenario].monthlyCost.elapsed =
              otherCalcs.calculation.rateElapsed;

            draftState[scenario].monthlyCost.pending =
              otherCalcs.calculation.ratePending;

            draftState[scenario].monthlyCost.total = draftState[
              scenario
            ].monthlyCost.total =
              (otherCalcs.calculation.rateElapsed +
                otherCalcs.calculation.ratePending) /
              2;
            otherTotal += totalCost;
            totalImpact += totalCost;
            probabilityWeightedTotalImpact +=
              totalCost *
              (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);

            otherDuration += totalDays;
            otherMonthly += draftState[scenario].monthlyCost.total;
            monthlyImpact += draftState[scenario].monthlyCost.total;
            probabilityWeightedMonthlyImpact +=
              draftState[scenario].monthlyCost.total *
              (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
          } else {
            const totalCostOverride =
              otherCalcs.costOverride.elapsed + otherCalcs.costOverride.pending;
            draftState[scenario].costOverride.total = totalCostOverride;
            // override values have no duration or monthly cost
            draftState[scenario].monthlyCost.elapsed = 0.0;
            draftState[scenario].monthlyCost.pending = 0.0;
            draftState[scenario].monthlyCost.total = 0.0;
            otherTotal += totalCostOverride;
            totalImpact += totalCostOverride;
            probabilityWeightedTotalImpact +=
              totalCostOverride *
              (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
          }
        });
      });

      const newMaintenanceCalcState = produce(
        vm.maintenanceCalcs,
        (draftState) => {
          vm.maintenanceCalcs.forEach((maintenanceCalcs, scenario) => {
            if (
              maintenanceCalcs.calculationType ===
              CalculationType.CalculatedImpact
            ) {
              draftState[scenario].calculation.costCalculation.elapsed =
                MaintenanceCalculatedCost(
                  maintenanceCalcs.calculation.partsElapsed,
                  maintenanceCalcs.calculation.laborHoursElapsed,
                  maintenanceCalcs.calculation.laborRateElapsed
                );
              draftState[scenario].calculation.costCalculation.pending =
                MaintenanceCalculatedCost(
                  maintenanceCalcs.calculation.partsPending,
                  maintenanceCalcs.calculation.laborHoursPending,
                  maintenanceCalcs.calculation.laborRatePending
                );
              const totalCost =
                draftState[scenario].calculation.costCalculation.elapsed +
                draftState[scenario].calculation.costCalculation.pending;
              draftState[scenario].calculation.costCalculation.total =
                totalCost;
              maintenanceTotal += totalCost;
              totalImpact += totalCost;
              probabilityWeightedTotalImpact +=
                totalCost *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
            } else {
              const totalCostOverride =
                maintenanceCalcs.costOverride.elapsed +
                maintenanceCalcs.costOverride.pending;
              draftState[scenario].costOverride.total = totalCostOverride;
              maintenanceTotal += totalCostOverride;
              totalImpact += totalCostOverride;
              probabilityWeightedTotalImpact +=
                totalCostOverride *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
            }
          });
        }
      );
      const newEfficiencyCalcState = produce(
        vm.efficiencyCalcs,
        (draftState) => {
          vm.efficiencyCalcs.forEach((efficiencyCalcs, scenario) => {
            if (
              efficiencyCalcs.calculationType ===
              CalculationType.CalculatedImpact
            ) {
              const calcType = vm.efficiencyCalcDropDown.find(
                (i) =>
                  i.AssetIssueImpactCalculationFactorID ===
                  efficiencyCalcs.calculation.calculationFactorID
              );

              draftState[scenario].calculation.calculateDerate(calcType);

              const totalDays =
                efficiencyCalcs.calculation.daysPending +
                efficiencyCalcs.calculation.daysElapsed;
              const totalCost =
                draftState[scenario].calculation.costCalculation.total;

              draftState[scenario].monthlyCost.elapsed =
                efficiencyCalcs.calculation.daysElapsed > 0
                  ? draftState[scenario].calculation.costCalculation.elapsed /
                    (efficiencyCalcs.calculation.daysElapsed / 30)
                  : 0.0;
              draftState[scenario].monthlyCost.pending =
                efficiencyCalcs.calculation.daysPending > 0
                  ? draftState[scenario].calculation.costCalculation.pending /
                    (efficiencyCalcs.calculation.daysPending / 30)
                  : 0.0;
              const monthlyCost =
                totalDays > 0
                  ? (draftState[scenario].calculation.costCalculation.pending +
                      draftState[scenario].calculation.costCalculation
                        .elapsed) /
                    (totalDays / 30)
                  : 0.0;
              draftState[scenario].monthlyCost.total = monthlyCost;
              efficiencyTotal += totalCost;
              totalImpact += totalCost;
              probabilityWeightedTotalImpact +=
                totalCost *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);

              efficiencyDuration += totalDays;
              efficiencyMonthly += monthlyCost;
              monthlyImpact += monthlyCost;
              probabilityWeightedMonthlyImpact +=
                monthlyCost *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
            } else {
              const totalCostOverride =
                efficiencyCalcs.costOverride.elapsed +
                efficiencyCalcs.costOverride.pending;
              draftState[scenario].costOverride.total = totalCostOverride;
              // override values have no duration or monthly cost
              draftState[scenario].monthlyCost.elapsed = 0.0;
              draftState[scenario].monthlyCost.pending = 0.0;
              draftState[scenario].monthlyCost.total = 0.0;
              efficiencyTotal += totalCostOverride;
              totalImpact += totalCostOverride;
              probabilityWeightedTotalImpact +=
                totalCostOverride *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
            }
          });
        }
      );
      const newProductionCalcState = produce(
        vm.productionCalcs,
        (draftState) => {
          vm.productionCalcs.forEach((productionCalcs, scenario) => {
            if (
              productionCalcs.calculationType ===
              CalculationType.CalculatedImpact
            ) {
              const calcType = vm.productionCalcDropDown.find(
                (i) =>
                  i.AssetIssueImpactCalculationFactorID ===
                  productionCalcs.calculation.calculationFactorID
              );

              draftState[scenario].calculation.calculateDerate(calcType);

              const totalDays =
                productionCalcs.calculation.daysPending +
                productionCalcs.calculation.daysElapsed;
              const totalCost =
                draftState[scenario].calculation.costCalculation.total;
              draftState[scenario].monthlyCost.elapsed =
                productionCalcs.calculation.daysElapsed > 0
                  ? draftState[scenario].calculation.costCalculation.elapsed /
                    (productionCalcs.calculation.daysElapsed / 30)
                  : 0.0;
              draftState[scenario].monthlyCost.pending =
                productionCalcs.calculation.daysPending > 0
                  ? draftState[scenario].calculation.costCalculation.pending /
                    (productionCalcs.calculation.daysPending / 30)
                  : 0.0;
              const monthlyCost =
                totalDays > 0
                  ? (draftState[scenario].calculation.costCalculation.pending +
                      draftState[scenario].calculation.costCalculation
                        .elapsed) /
                    (totalDays / 30)
                  : 0.0;
              draftState[scenario].monthlyCost.total = monthlyCost;
              productionTotal += totalCost;
              totalImpact += totalCost;
              probabilityWeightedTotalImpact +=
                totalCost *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);

              productionDuration += totalDays;
              productionMonthly += monthlyCost;
              monthlyImpact += monthlyCost;
              probabilityWeightedMonthlyImpact +=
                monthlyCost *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
            } else {
              const totalCostOverride =
                productionCalcs.costOverride.elapsed +
                productionCalcs.costOverride.pending;
              draftState[scenario].costOverride.total = totalCostOverride;
              // override values have no duration or monthly cost
              draftState[scenario].monthlyCost.elapsed = 0.0;
              draftState[scenario].monthlyCost.pending = 0.0;
              draftState[scenario].monthlyCost.total = 0.0;
              productionTotal += totalCostOverride;
              totalImpact += totalCostOverride;
              probabilityWeightedTotalImpact +=
                totalCostOverride *
                (vm.scenarioCalcs[scenario].percentLikelihood / 100.0);
              // productionMonthly and productionTotal,monthlyImpact and probabilityWeightedMonthlyImpact
              // are also 0 here because duration = 0
            }
          });
        }
      );

      const newScenarioCalcState = produce(vm.scenarioCalcs, (draftState) => {
        vm.scenarioCalcs.forEach((scenarioCalc, scenario) => {
          draftState[scenario].monthlyImpact.elapsed =
            newProductionCalcState[scenario].monthlyCost.elapsed +
            newEfficiencyCalcState[scenario].monthlyCost.elapsed +
            newOtherCalcState[scenario].monthlyCost.elapsed;
          draftState[scenario].monthlyImpact.pending =
            newProductionCalcState[scenario].monthlyCost.pending +
            newEfficiencyCalcState[scenario].monthlyCost.pending +
            newOtherCalcState[scenario].monthlyCost.pending;
          draftState[scenario].monthlyImpact.total =
            newProductionCalcState[scenario].monthlyCost.total +
            newEfficiencyCalcState[scenario].monthlyCost.total +
            newOtherCalcState[scenario].monthlyCost.total;
          draftState[scenario].probabilityWeightedMonthlyImpact.elapsed =
            (draftState[scenario].monthlyImpact.elapsed *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
          draftState[scenario].probabilityWeightedMonthlyImpact.pending =
            (draftState[scenario].monthlyImpact.pending *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
          draftState[scenario].probabilityWeightedMonthlyImpact.total =
            (draftState[scenario].monthlyImpact.total *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
          const scenarioTotalImpact: CostPeriods = {
            pending: 0.0,
            elapsed: 0.0,
            total: 0.0,
          };
          if (
            vm.productionCalcs[scenario].calculationType ===
            CalculationType.CalculatedImpact
          ) {
            scenarioTotalImpact.total +=
              newProductionCalcState[
                scenario
              ].calculation.costCalculation.total;
            scenarioTotalImpact.elapsed +=
              newProductionCalcState[
                scenario
              ].calculation.costCalculation.elapsed;
            scenarioTotalImpact.pending +=
              newProductionCalcState[
                scenario
              ].calculation.costCalculation.pending;
          } else {
            scenarioTotalImpact.total +=
              newProductionCalcState[scenario].costOverride.total;
            scenarioTotalImpact.elapsed +=
              newProductionCalcState[scenario].costOverride.elapsed;
            scenarioTotalImpact.pending +=
              newProductionCalcState[scenario].costOverride.pending;
          }
          if (
            vm.efficiencyCalcs[scenario].calculationType ===
            CalculationType.CalculatedImpact
          ) {
            scenarioTotalImpact.total +=
              newEfficiencyCalcState[
                scenario
              ].calculation.costCalculation.total;
            scenarioTotalImpact.elapsed +=
              newEfficiencyCalcState[
                scenario
              ].calculation.costCalculation.elapsed;
            scenarioTotalImpact.pending +=
              newEfficiencyCalcState[
                scenario
              ].calculation.costCalculation.pending;
          } else {
            scenarioTotalImpact.total +=
              newEfficiencyCalcState[scenario].costOverride.total;
            scenarioTotalImpact.elapsed +=
              newEfficiencyCalcState[scenario].costOverride.elapsed;
            scenarioTotalImpact.pending +=
              newEfficiencyCalcState[scenario].costOverride.pending;
          }
          if (
            vm.maintenanceCalcs[scenario].calculationType ===
            CalculationType.CalculatedImpact
          ) {
            scenarioTotalImpact.total +=
              newMaintenanceCalcState[
                scenario
              ].calculation.costCalculation.total;
            scenarioTotalImpact.elapsed +=
              newMaintenanceCalcState[
                scenario
              ].calculation.costCalculation.elapsed;
            scenarioTotalImpact.pending +=
              newMaintenanceCalcState[
                scenario
              ].calculation.costCalculation.pending;
          } else {
            scenarioTotalImpact.total +=
              newMaintenanceCalcState[scenario].costOverride.total;
            scenarioTotalImpact.elapsed +=
              newMaintenanceCalcState[scenario].costOverride.elapsed;
            scenarioTotalImpact.pending +=
              newMaintenanceCalcState[scenario].costOverride.pending;
          }
          if (
            vm.otherCalcs[scenario].calculationType ===
            CalculationType.CalculatedImpact
          ) {
            scenarioTotalImpact.total +=
              newOtherCalcState[scenario].calculation.costCalculation.total;
            scenarioTotalImpact.elapsed +=
              newOtherCalcState[scenario].calculation.costCalculation.elapsed;
            scenarioTotalImpact.pending +=
              newOtherCalcState[scenario].calculation.costCalculation.pending;
          } else {
            scenarioTotalImpact.total +=
              newOtherCalcState[scenario].costOverride.total;
            scenarioTotalImpact.elapsed +=
              newOtherCalcState[scenario].costOverride.elapsed;
            scenarioTotalImpact.pending +=
              newOtherCalcState[scenario].costOverride.pending;
          }

          draftState[scenario].totalImpact.total = scenarioTotalImpact.total;
          draftState[scenario].totalImpact.pending =
            scenarioTotalImpact.pending;
          draftState[scenario].totalImpact.elapsed =
            scenarioTotalImpact.elapsed;

          draftState[scenario].probabilityWeightedTotalImpact.elapsed =
            (draftState[scenario].totalImpact.elapsed *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
          draftState[scenario].probabilityWeightedTotalImpact.pending =
            (draftState[scenario].totalImpact.pending *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
          draftState[scenario].probabilityWeightedTotalImpact.total =
            (draftState[scenario].totalImpact.total *
              vm.scenarioCalcs[scenario].percentLikelihood) /
            100.0;
        });
      });

      const impactTotals: ImpactTotals = {
        productionTotal: productionTotal,
        productionDuration: productionDuration,
        productionMonthly: productionMonthly,
        efficiencyTotal: efficiencyTotal,
        efficiencyDuration: efficiencyDuration,
        efficiencyMonthly: efficiencyMonthly,
        maintenanceTotal: maintenanceTotal,
        otherTotal: otherTotal,
        otherDuration: otherDuration,
        otherMonthly: otherMonthly,
        totalImpact: totalImpact,
        probabilityWeightedTotalImpact: probabilityWeightedTotalImpact,
        monthlyImpact: monthlyImpact,
        probabilityWeightedMonthlyImpact: probabilityWeightedMonthlyImpact,
      };
      this.updateState({
        ..._state,
        impactTotals,
        maintenanceCalcs: newMaintenanceCalcState,
        efficiencyCalcs: newEfficiencyCalcState,
        otherCalcs: newOtherCalcState,
        scenarioCalcs: newScenarioCalcState,
        productionCalcs: newProductionCalcState,
      });
    });
  }

  saveImpacts() {
    this.updateState({ ..._state, saving: true });
    this.vm$.pipe(take(1)).subscribe((vm) => {
      const scenariosToSave: ImpactScenarioSave[] = [];
      for (let i = 0; i < vm.scenarios.length; i++) {
        const scenario: ImpactScenarioSave = {
          AssetIssueImpactScenarioID: vm.scenarios[i].scenarioID,
          AssetIssueImpactScenarioName: vm.scenarioCalcs[i].name,
          AssetIssueImpactScenarioNotes: vm.scenarioCalcs[i].notes,
          PercentLikelihood: vm.scenarioCalcs[i].percentLikelihood,
          Generation_AssetIssueAssetIssueImpactTypeMapID:
            vm.hasGeneration ||
            vm.productionCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? vm.productionCalcs[i].assetIssueAssetIssueImpactTypeMapID
              : null,
          Efficiency_AssetIssueAssetIssueImpactTypeMapID:
            vm.hasEfficiency ||
            vm.efficiencyCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? vm.efficiencyCalcs[i].assetIssueAssetIssueImpactTypeMapID
              : null,
          Maintenance_AssetIssueAssetIssueImpactTypeMapID:
            vm.hasMaintenance ||
            vm.maintenanceCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? vm.maintenanceCalcs[i].assetIssueAssetIssueImpactTypeMapID
              : null,
          OtherCosts_AssetIssueAssetIssueImpactTypeMapID:
            vm.hasOther ||
            vm.otherCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? vm.otherCalcs[i].assetIssueAssetIssueImpactTypeMapID
              : null,
          GenerationImpactRange:
            vm.hasGeneration ||
            vm.productionCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? {
                  StartDate: vm.productionCalcs[i].calculation.dateElapsed,
                  CurrentDate: vm.productionCalcs[i].calculation.currentDate,
                  EndDate: vm.productionCalcs[i].calculation.datePending,
                  DaysElapsed: vm.productionCalcs[i].calculation.daysElapsed,
                  DaysPending: vm.productionCalcs[i].calculation.daysPending,
                  DaysTotal:
                    vm.productionCalcs[i].calculation.daysElapsed +
                    vm.productionCalcs[i].calculation.daysPending,
                }
              : null,
          EfficiencyImpactRange:
            vm.hasEfficiency ||
            vm.efficiencyCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? {
                  StartDate: vm.efficiencyCalcs[i].calculation.dateElapsed,
                  CurrentDate: vm.efficiencyCalcs[i].calculation.currentDate,
                  EndDate: vm.efficiencyCalcs[i].calculation.datePending,
                  DaysElapsed: vm.efficiencyCalcs[i].calculation.daysElapsed,
                  DaysPending: vm.efficiencyCalcs[i].calculation.daysPending,
                  DaysTotal:
                    vm.efficiencyCalcs[i].calculation.daysElapsed +
                    vm.efficiencyCalcs[i].calculation.daysPending,
                }
              : null,
          MaintenanceImpactRange:
            vm.hasMaintenance ||
            vm.maintenanceCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? {
                  StartDate: null,
                  CurrentDate: null,
                  EndDate: null,
                  DaysElapsed: 0,
                  DaysPending: 0,
                  DaysTotal: 0,
                }
              : null,
          OtherCostsImpactRange:
            vm.hasOther ||
            vm.otherCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? {
                  StartDate: vm.otherCalcs[i].calculation.dateElapsed,
                  CurrentDate: vm.otherCalcs[i].calculation.currentDate,
                  EndDate: vm.otherCalcs[i].calculation.datePending,
                  DaysElapsed: vm.otherCalcs[i].calculation.daysElapsed,
                  DaysPending: vm.otherCalcs[i].calculation.daysPending,
                  DaysTotal:
                    vm.otherCalcs[i].calculation.daysElapsed +
                    vm.otherCalcs[i].calculation.daysPending,
                }
              : null,
          GenerationImpact:
            vm.hasGeneration ||
            vm.productionCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? this.impactCalculatorService.createProductionCalsForSave(
                  vm.productionCalcs[i],
                  vm.scenarios[i]?.generationParameters
                )
              : null,
          EfficiencyImpact:
            vm.hasEfficiency ||
            vm.efficiencyCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? this.impactCalculatorService.createEfficiencyCalcsForSave(
                  vm.efficiencyCalcs[i],
                  vm.scenarios[i]?.efficiencyParameters
                )
              : null,
          MaintenanceImpact:
            vm.hasMaintenance ||
            vm.maintenanceCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? this.impactCalculatorService.createMaintenanceCalcsForSave(
                  vm.maintenanceCalcs[i]
                )
              : null,
          OtherCostsImpact:
            vm.hasOther ||
            vm.otherCalcs[i].assetIssueAssetIssueImpactTypeMapID > -1
              ? this.impactCalculatorService.createOtherCalcsForSave(
                  vm.otherCalcs[i]
                )
              : null,
        };
        scenariosToSave.push(scenario);
      }
      const saveState: ImpactCalculatorSave = {
        AssetIssueID: vm.assetIssueID,
        ImpactScenariosToSave: scenariosToSave,
        ImpactScenarioIDsToDelete: vm.scenarioIDsToDelete,
      };
      this.issuesFrameworkService
        .saveImpactCalculator(saveState)
        .pipe(take(1))
        // eslint-disable-next-line rxjs/no-nested-subscribe
        .subscribe(
          (val: ImpactCalculatorSaveReturn) => {
            if (val.Results.length > 0) {
              const saveUpdates: SaveUpdates = {
                AssetIssueChangeDate: val.Results[0].AssetIssueChangeDate,
                AssetIssueChangedBy: val.Results[0].AssetIssueChangedBy,
                AssetIssueID: vm.assetIssueID.toString(),
              };
              const newScenarioState = produce(vm.scenarios, (draftState) => {
                vm.scenarios.forEach((scenario, index) => {
                  if (
                    scenario.scenarioID < 0 &&
                    val.Results[0].ImpactScenarioIDsSaved?.length > index
                  ) {
                    draftState[index].scenarioID =
                      val.Results[0].ImpactScenarioIDsSaved[index];
                  }
                });
              });
              this.updateState({
                ..._state,
                scenarios: newScenarioState,
                saveUpdates,
              });
            }
            this.snackBarService.openSnackBar('Saved successfully.', 'success');
            this.reset();
            this.router.navigate([], {
              queryParams: {
                aiid: null,
                aid: null,
              },
              queryParamsHandling: 'merge',
              replaceUrl: true,
            });
          },
          (error: unknown) => {
            this.snackBarService.openSnackBar('An error occurred.', 'error');
            console.error(JSON.stringify(error));
            this.updateState({ ..._state, saving: false });
          }
        );
    });
  }

  validateCalculator(vm: ImpactCalculatorState) {
    let isValid = true;
    let hasInvalidGeneration = false;
    let hasInvalidEfficiency = false;
    let hasInvalidMaintenance = false;
    let hasInvalidOther = false;
    if (!vm.hasEfficiency) {
      vm.efficiencyCalcs.forEach((calc) => {
        if (calc.calculation.costCalculation.total > 0) {
          hasInvalidEfficiency = true;
          isValid = false;
        }
      });
    }
    if (!vm.hasMaintenance) {
      vm.maintenanceCalcs.forEach((calc) => {
        if (calc.calculation.costCalculation.total > 0) {
          hasInvalidMaintenance = true;
          isValid = false;
        }
      });
    }
    if (!vm.hasOther) {
      vm.otherCalcs.forEach((calc) => {
        if (calc.calculation.costCalculation.total > 0) {
          hasInvalidOther = true;
          isValid = false;
        }
      });
    }
    if (!vm.hasGeneration) {
      vm.productionCalcs.forEach((calc) => {
        if (calc.calculation.costCalculation.total > 0) {
          hasInvalidGeneration = true;
          isValid = false;
        }
      });
    }
    if (vm.hasReliability || vm.hasDerate) {
      if (!vm.hasReliability || !vm.hasDerate) {
        // user has one category dropdown but not the other. Legacy defect.
        const validCategory = vm.hasReliability
          ? AssetImpactType.Reliability.valueOf()
          : AssetImpactType.Derate.valueOf();
        vm.productionCalcs.forEach((calc) => {
          if (calc.calculation.costCalculation.total > 0) {
            if (calc.calculation.calculatorCategory !== validCategory) {
              hasInvalidGeneration = true;
              isValid = false;
            }
          }
        });
        if (isValid) {
          // if it is valid, the user may have still used the calculator
          // incorrectly. We need to set it to something legit.
          const newProdCalcState = produce(vm.productionCalcs, (draftState) => {
            vm.productionCalcs.forEach((calc, scenario) => {
              draftState[scenario].calculation.calculatorCategory =
                validCategory;
              draftState[scenario].calculation.calculationFactorID =
                DEFAULT_GENERATION_CALCULATION_FACTOR_ID;
            });
          });
          this.updateState({ ..._state, productionCalcs: newProdCalcState });
        }
      }
    }
    if (!isValid) {
      // they will not be able to save, so loosen restrictions.
      this.updateState({
        ..._state,
        isValid: false,
        ...((hasInvalidGeneration && { hasGeneration: true }) || []),
        ...((hasInvalidEfficiency && { hasEfficiency: true }) || []),
        ...((hasInvalidMaintenance && { hasMaintenance: true }) || []),
        ...((hasInvalidOther && { hasOther: true }) || []),
      });
    }
  }

  cancelCalculator() {
    this.updateState({ ..._state, saving: true });
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  private updateState(state: ImpactCalculatorState) {
    this.store.next((_state = state));
  }
}
