import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {
  assetIssueWithOptionsMaintenanceReliability,
  blankScenario1,
  blankScenario2,
  impactCalculationFactorsByImpactCategoryType,
  impactScenario,
  impactScenario2,
} from '../../data/calculator-mocks';
import { eachValueFrom } from 'rxjs-for-await';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { ImpactCalculatorFacade } from './impact-calculator.facade';
import { ImpactCalculatorService } from './impact-calculator.service';
import { take } from 'rxjs/operators';
import { IssuesFrameworkService } from '@atonix/shared/api';
import { LegacyImpactScenario } from '../../model/impact-calculator/scenario-legacy';

describe('Legacy Impact Calculator Scenarios', () => {
  let impactCalculatorFacade: ImpactCalculatorFacade;
  let toastService: ToastService;
  let issuesFrameworkService: IssuesFrameworkService;

  beforeEach(() => {
    toastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        ImpactCalculatorFacade,
        ImpactCalculatorService,
        IssuesFrameworkService,
        {
          provide: ToastService,
          useValue: toastService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    });
    issuesFrameworkService = TestBed.inject(IssuesFrameworkService);
    impactCalculatorFacade = TestBed.inject(ImpactCalculatorFacade);
  });

  it('reads in a calculation problem', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID = impactScenario;
    impactCalculatorFacade.updateCalculator(1, 1, '1');

    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      expect(value.scenarios[0].generationParameters.totalCost).toStrictEqual({
        toDate: 2339.64,
        potentialFuture: 2339.64,
        totalAvg: 4679.28,
      });
    }
  });

  it('can calculate production calcs', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario2;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID = impactScenario2;
    impactCalculatorFacade.updateCalculator(1, 1, '1');

    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      expect(
        value.productionCalcs[0].calculation.costCalculation
      ).toStrictEqual({
        elapsed: 77644.8,
        pending: 77644.8,
        total: 155289.6,
      });
    }
  });

  it('can set relevant production cost', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario2;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID = impactScenario2;
    impactCalculatorFacade.updateCalculator(1, 1, '1');

    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      const legacyProductionCost =
        ((value.scenarios[0] as LegacyImpactScenario).fuelCost *
          (value.scenarios[0] as LegacyImpactScenario).netUnitHeatRate) /
        1000;
      // legacy production cost is (fuelCost * netUnitHeatRate) / 1000
      expect(legacyProductionCost).toStrictEqual(
        value.scenarios[0].calculateRelevantProductionCost()
      );
    }
  });
});
