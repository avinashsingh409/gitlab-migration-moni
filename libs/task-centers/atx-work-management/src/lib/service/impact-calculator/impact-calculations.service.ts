export function MaintenanceCalculatedCost(
  partsElapsedOrPending: number,
  laborHoursElapsedOrPending: number,
  laborRateElapsedOrPending: number
) {
  return (
    partsElapsedOrPending +
    laborHoursElapsedOrPending * laborRateElapsedOrPending
  );
}

export function OtherCalculatedCost(
  rateElapsedOrPending: number,
  daysElapsedOrPending: number
) {
  return rateElapsedOrPending * (daysElapsedOrPending / 30);
}
