/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { produce } from 'immer';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  take,
  takeUntil,
} from 'rxjs/operators';
import { ImpactCalculatorFacade } from './impact-calculator.facade';
import moment from 'moment';
import { CalculationType } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class ImpactCalculatorFormService implements OnDestroy {
  private numberRegex = '-?\\d+(?:\\.\\d+)?';
  onDestroy = new Subject<void>();
  isNumber = (n: string | number): boolean =>
    !isNaN(parseFloat(String(n))) && isFinite(Number(n));

  constructor(private calculatorFacade: ImpactCalculatorFacade) {}

  scenarioName(scenario: number, name: string): UntypedFormControl {
    const scenarioName = new UntypedFormControl(name, []);
    scenarioName.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.scenarioCalcs, (state) => {
            state[scenarioIndex].name = value;
          });
          this.calculatorFacade.updateScenarioCalcValues(newState, false);
        });
      });
    return scenarioName;
  }

  scenarioNotes(scenario: number, notes: string): UntypedFormControl {
    const scenarioNotes = new UntypedFormControl(notes, []);
    scenarioNotes.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.scenarioCalcs, (state) => {
            state[scenarioIndex].notes = value;
          });
          this.calculatorFacade.updateScenarioCalcValues(newState, false);
        });
      });
    return scenarioNotes;
  }

  probability(scenario: number, probability: number): UntypedFormControl {
    const percentLikelihoodControl = new UntypedFormControl(probability, [
      Validators.required,
      Validators.pattern(this.numberRegex),
      Validators.max(100),
      Validators.min(0),
    ]);
    percentLikelihoodControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.scenarioCalcs, (state) => {
            state[scenarioIndex].percentLikelihood = +value;
          });
          this.calculatorFacade.updateScenarioCalcValues(newState, true);
        });
      });
    return percentLikelihoodControl;
  }

  impactQuantity(scenario: number, impactQuantity: number): UntypedFormControl {
    const impactQuantityControl = new UntypedFormControl(impactQuantity, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    impactQuantityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.impactQuantity = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return impactQuantityControl;
  }

  daysElapsed(scenario: number, daysElapsed: number): UntypedFormControl {
    const daysElapsedControl = new UntypedFormControl(daysElapsed, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    daysElapsedControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.daysElapsed = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return daysElapsedControl;
  }

  daysPending(scenario: number, daysPending: number): UntypedFormControl {
    const daysPendingControl = new UntypedFormControl(daysPending, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    daysPendingControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.daysPending = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return daysPendingControl;
  }

  derateElapsed(scenario: number, derateElapsed: number): UntypedFormControl {
    const derateElapsedControl = new UntypedFormControl(derateElapsed, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    derateElapsedControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.derateElapsed = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return derateElapsedControl;
  }

  derateOverride(scenario: number, derateOverride: number): UntypedFormControl {
    const derateElapsedControl = new UntypedFormControl(derateOverride, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    derateElapsedControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.derateOverride = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return derateElapsedControl;
  }

  deratePending(scenario: number, deratePending: number): UntypedFormControl {
    const deratePendingControl = new UntypedFormControl(deratePending, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    deratePendingControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.deratePending = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return deratePendingControl;
  }

  lostMarginOpportunity(
    scenario: number,
    lostMarginOpportunity: number
  ): UntypedFormControl {
    const marginOpportunityControl = new UntypedFormControl(
      lostMarginOpportunity,
      [
        Validators.required,
        Validators.pattern(this.numberRegex),
        Validators.min(0),
      ]
    );
    marginOpportunityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.lostMarginOpportunity = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return marginOpportunityControl;
  }

  relevantProductionCost(
    scenario: number,
    relevantProductionCost: number
  ): UntypedFormControl {
    const relevantProductionControl = new UntypedFormControl(
      relevantProductionCost,
      [
        Validators.required,
        Validators.pattern(this.numberRegex),
        Validators.min(0),
      ]
    );
    relevantProductionControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].calculation.relevantProductionCost = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return relevantProductionControl;
  }

  productionToDate(scenario: number, elapsedCost: number): UntypedFormControl {
    const costElapsedControl = new UntypedFormControl(elapsedCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    costElapsedControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].costOverride.elapsed = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return costElapsedControl;
  }

  productionFuture(
    scenario: number,
    totalFutureCost: number
  ): UntypedFormControl {
    const productionFuture = new UntypedFormControl(totalFutureCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    productionFuture.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.productionCalcs, (state) => {
            state[scenarioIndex].costOverride.pending = +value;
          });
          this.calculatorFacade.updateProductionFormValue(newState);
        });
      });
    return productionFuture;
  }

  productionImpactCalculationTypeChange(
    event: CalculationType,
    scenario: number
  ) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.productionCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculationType = event;
      });
      this.calculatorFacade.updateProductionFormValue(newState);
    });
  }

  // Efficiency Form Controls
  effImpactQuantity(
    scenario: number,
    effImpactQuantity: number
  ): UntypedFormControl {
    const impactQuantityControl = new UntypedFormControl(effImpactQuantity, [
      Validators.required,
      Validators.pattern(this.numberRegex),
      Validators.min(0),
    ]);
    impactQuantityControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.efficiencyCalcs, (state) => {
            state[scenarioIndex].calculation.impactQuantity = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return impactQuantityControl;
  }

  effCapacityFactor(
    scenario: number,
    effCapacityFactor: number
  ): UntypedFormControl {
    const capacityFactor = new UntypedFormControl(effCapacityFactor, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    capacityFactor.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const scenarioIndex = vm.scenarios.findIndex(
            (i) => i.scenarioGuidID === scenario
          );
          const newState = produce(vm.efficiencyCalcs, (state) => {
            state[scenarioIndex].calculation.capacityUtilization = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return capacityFactor;
  }

  effNetUnitHeatRate(
    scenario: number,
    effNetUnitHeatRate: number
  ): UntypedFormControl {
    const netUnitHeatRate = new UntypedFormControl(effNetUnitHeatRate, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    netUnitHeatRate.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.consumptionRateValue = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return netUnitHeatRate;
  }

  effFuelCost(scenario: number, effFuelCost: number): UntypedFormControl {
    const fuelCost = new UntypedFormControl(effFuelCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    fuelCost.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.consumableCost = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return fuelCost;
  }

  effDaysElapsed(scenario: number, effDaysElapsed: number): UntypedFormControl {
    const daysElapsed = new UntypedFormControl(effDaysElapsed, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    daysElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.daysElapsed = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return daysElapsed;
  }

  effDaysPending(scenario: number, effDaysPending: number): UntypedFormControl {
    const daysPending = new UntypedFormControl(effDaysPending, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    daysPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.daysPending = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return daysPending;
  }

  effFuture(scenario: number, effFutureCost: number): UntypedFormControl {
    const effPending = new UntypedFormControl(effFutureCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    effPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.pending = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return effPending;
  }

  effToDate(scenario: number, elapsedCost: number): UntypedFormControl {
    const effElapsed = new UntypedFormControl(elapsedCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    effElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.efficiencyCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.elapsed = +value;
          });
          this.calculatorFacade.updateEfficiencyFormValue(newState);
        });
      });
    return effElapsed;
  }

  efficiencyImpactCalculationTypeChange(
    event: CalculationType,
    scenario: number
  ) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.efficiencyCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculationType = event;
      });
      this.calculatorFacade.updateEfficiencyFormValue(newState);
    });
  }

  // Maintenance
  maintenanceLaborHoursElapsed(
    scenario: number,
    laborHours: number
  ): UntypedFormControl {
    let laborHoursString = '0';
    if (laborHours) {
      laborHoursString = laborHours.toString();
    }
    const laborHoursElapsed = new UntypedFormControl(laborHoursString, [
      Validators.required,
      Validators.pattern('[0-9]+'),
    ]);
    laborHoursElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.laborHoursElapsed = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return laborHoursElapsed;
  }

  maintenanceLaborHoursPending(
    scenario: number,
    laborHours: number
  ): UntypedFormControl {
    let laborHoursString = '0';
    if (laborHours) {
      laborHoursString = laborHours.toString();
    }
    const laborHoursPending = new UntypedFormControl(laborHoursString, [
      Validators.required,
      Validators.pattern('[0-9]+'),
    ]);
    laborHoursPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.laborHoursPending = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return laborHoursPending;
  }

  maintenanceLaborCostElapsed(
    scenario: number,
    laborCost: number
  ): UntypedFormControl {
    const laborRateElapsed = new UntypedFormControl(laborCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    laborRateElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.laborRateElapsed = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return laborRateElapsed;
  }

  maintenanceLaborCostPending(
    scenario: number,
    laborCost: number
  ): UntypedFormControl {
    const laborRatePending = new UntypedFormControl(laborCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    laborRatePending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.laborRatePending = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return laborRatePending;
  }
  maintenancePartsCostPending(
    scenario: number,
    partsCost: number
  ): UntypedFormControl {
    const partsCostPending = new UntypedFormControl(partsCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    partsCostPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.partsPending = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return partsCostPending;
  }

  maintenancePartsCostElapsed(
    scenario: number,
    partsCost: number
  ): UntypedFormControl {
    const partsCostElapsed = new UntypedFormControl(partsCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    partsCostElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.partsElapsed = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return partsCostElapsed;
  }

  mainFuture(scenario: number, effFutureCost: number): UntypedFormControl {
    const mainPending = new UntypedFormControl(effFutureCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    mainPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.pending = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return mainPending;
  }

  mainToDate(scenario: number, elapsedCost: number): UntypedFormControl {
    const effElapsed = new UntypedFormControl(elapsedCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    effElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.maintenanceCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.elapsed = +value;
          });
          this.calculatorFacade.updateMaintenanceCalcValues(newState);
        });
      });
    return effElapsed;
  }

  maintenanceImpactCalculationTypeChange(
    event: CalculationType,
    scenario: number
  ) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.maintenanceCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculationType = event;
      });
      this.calculatorFacade.updateMaintenanceCalcValues(newState);
    });
  }

  toggleProductionDerateOverride(derateOverride: boolean, scenario: number) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.productionCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculation.hasDerateOverride = derateOverride;
      });
      this.calculatorFacade.updateProductionFormValue(newState);
    });
  }

  changeElapsedProductionDate(newDate: Date, scenario: number) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.productionCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculation.dateElapsed = newDate;
        const elapsedDate = moment(newDate);
        const currentDate = moment(
          state[scenarioIndex].calculation.currentDate
        );
        const newElapsedDays = currentDate.diff(elapsedDate, 'days');
        state[scenarioIndex].calculation.daysElapsed = newElapsedDays;
      });
      this.calculatorFacade.updateProductionFormValue(newState);
    });
  }
  changePendingProductionDate(newDate: Date, scenario: number) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.productionCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculation.datePending = newDate;
        const pendingDate = moment(newDate);
        const currentDate = moment(
          state[scenarioIndex].calculation.currentDate
        );
        const newPendingDays = pendingDate.diff(currentDate, 'days');
        state[scenarioIndex].calculation.daysPending = newPendingDays;
      });
      this.calculatorFacade.updateProductionFormValue(newState);
    });
  }
  // other impact
  otherRateElapsed(scenario: number, elapsedRate: number): UntypedFormControl {
    const otherRateElapsed = new UntypedFormControl(elapsedRate, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    otherRateElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.rateElapsed = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherRateElapsed;
  }

  otherRatePending(scenario: number, pendingRate: number): UntypedFormControl {
    const otherRatePending = new UntypedFormControl(pendingRate, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    otherRatePending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.ratePending = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherRatePending;
  }

  otherDaysElapsed(scenario: number, daysElapsed: number): UntypedFormControl {
    const otherDaysElapsed = new UntypedFormControl(daysElapsed, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    otherDaysElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.daysElapsed = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherDaysElapsed;
  }

  otherDaysPending(scenario: number, daysPending: number): UntypedFormControl {
    const otherDaysPending = new UntypedFormControl(daysPending, [
      Validators.required,
      Validators.min(0),
      Validators.pattern(this.numberRegex),
    ]);
    otherDaysPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].calculation.daysPending = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherDaysPending;
  }

  otherFuture(scenario: number, otherFutureCost: number): UntypedFormControl {
    const otherPending = new UntypedFormControl(otherFutureCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    otherPending.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.pending = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherPending;
  }

  otherToDate(scenario: number, elapsedCost: number): UntypedFormControl {
    const otherElapsed = new UntypedFormControl(elapsedCost, [
      Validators.required,
      Validators.pattern(this.numberRegex),
    ]);
    otherElapsed.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        if (!this.isNumber(value)) {
          value = 0;
        }
        this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
          const newState = produce(vm.otherCalcs, (state) => {
            const scenarioIndex = vm.scenarios.findIndex(
              (i) => i.scenarioGuidID === scenario
            );
            state[scenarioIndex].costOverride.elapsed = +value;
          });
          this.calculatorFacade.updateOtherCalcValues(newState);
        });
      });
    return otherElapsed;
  }

  otherImpactCalculationTypeChange(event: CalculationType, scenario: number) {
    this.calculatorFacade.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.otherCalcs, (state) => {
        const scenarioIndex = vm.scenarios.findIndex(
          (i) => i.scenarioGuidID === scenario
        );
        state[scenarioIndex].calculationType = event;
      });
      this.calculatorFacade.updateOtherCalcValues(newState);
    });
  }
  ngOnDestroy() {
    this.onDestroy.next();
  }
}
