import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {
  assetIssueWithOptionsMaintenanceReliability,
  blankScenario1,
  impactCalculationFactorsByImpactCategoryType,
  impactContent,
  impactScenarioV2_Default,
  impactScenarioV2_KAES_Calc,
  impactScenarioV2_KAES_Calc__Efficiency_ImpactContent,
  impactScenarioV2_KAES_Calc__Generation_ImpactContent,
  impactScenarioV2_Override_Totals,
  saveAssetIssue,
} from '../../data/calculator-mocks';
import { eachValueFrom } from 'rxjs-for-await';
import { RouterTestingModule } from '@angular/router/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { ImpactCalculatorFacade } from './impact-calculator.facade';
import { ImpactCalculatorService } from './impact-calculator.service';
import { take } from 'rxjs/operators';
import { IssuesFrameworkService } from '@atonix/shared/api';
import { ImpactCalculatorSave } from '@atonix/atx-core';

describe('V2 Impact Calculator Scenarios', () => {
  let impactCalculatorFacade: ImpactCalculatorFacade;
  let toastService: ToastService;
  let issuesFrameworkService: IssuesFrameworkService;

  beforeEach(() => {
    toastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        ImpactCalculatorFacade,
        ImpactCalculatorService,
        IssuesFrameworkService,
        {
          provide: ToastService,
          useValue: toastService,
        },
        { provide: APP_CONFIG, useValue: AppConfig },
      ],
    });
    issuesFrameworkService = TestBed.inject(IssuesFrameworkService);
    impactCalculatorFacade = TestBed.inject(ImpactCalculatorFacade);
  });

  it('is configured to use derate and generation', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    // console.log(JSON.stringify(impactContent));
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_KAES_Calc;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      expect(value.hasDerate).toEqual(true);
      expect(value.hasGeneration).toEqual(true);
    }
  });

  it('calculates the default problem', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_Default;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      // These values are from Default Calc
      // See Generic Impact Calc SpecV2 and Examples
      // See PBI 48349: https://dev.azure.com/AtonixDigital/Asset360/_boards/board/t/TUX/Backlog%20items/?workitem=48349
      expect(value.productionCalcs[0].calculation.costCalculation.total).toBe(
        840000
      );
      expect(value.productionCalcs[0].monthlyCost.total).toBe(3600000);
    }
  });

  it('can override the total', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_Override_Totals;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      expect(value.productionCalcs[0].calculation.costCalculation.total).toBe(
        1010
      );
      expect(value.efficiencyCalcs[0].calculation.costCalculation.total).toBe(
        41
      );
      // if override is set, the monthly cost will be 0
      expect(value.productionCalcs[0].monthlyCost.total).toBe(0);
    }
  });

  it('calculates the KAES problem production calcs', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    // console.log(JSON.stringify(impactContent));
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_KAES_Calc;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      // These values are from KAES Calc
      // See Generic Impact Calc SpecV2 and Examples
      // See PBI 48349: https://dev.azure.com/AtonixDigital/Asset360/_boards/board/t/TUX/Backlog%20items/?workitem=48349
      expect(value.productionCalcs[0].calculation.costCalculation.total).toBe(
        350
      );
      expect(value.productionCalcs[0].monthlyCost.total).toBe(10500);
    }
  });

  it('calculates the KAES problem efficiency calcs', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    // console.log(JSON.stringify(impactContent));
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_KAES_Calc;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    for await (const value of eachValueFrom(
      impactCalculatorFacade.vm$.pipe(take(1))
    )) {
      // These values are from KAES Calc
      // See Steam Operation Impact Calc SpecV2 and Examples
      // See PBI 48349: https://dev.azure.com/AtonixDigital/Asset360/_boards/board/t/TUX/Backlog%20items/?workitem=48349
      expect(value.efficiencyCalcs[0].calculation.costCalculation.total).toBe(
        2500
      );
      expect(value.efficiencyCalcs[0].monthlyCost.total).toBe(75000);
    }
  });

  it('saves the KAES problem efficiency calcs', async () => {
    expect(impactCalculatorFacade).toBeTruthy();
    issuesFrameworkService.getAssetIssueWithOptions =
      assetIssueWithOptionsMaintenanceReliability;
    issuesFrameworkService.getBlankScenario = blankScenario1;
    issuesFrameworkService.getImpactCalculationFactorsByImpactCategoryType =
      impactCalculationFactorsByImpactCategoryType;
    issuesFrameworkService.getImpactScenariosByAssetIssueID =
      impactScenarioV2_KAES_Calc;
    issuesFrameworkService.saveImpactCalculator = saveAssetIssue;
    impactCalculatorFacade.updateCalculator(1, 1, '1');
    impactCalculatorFacade.saveImpacts();
    const call = (saveAssetIssue.mock.calls[0] as ImpactCalculatorSave[])[0]
      .ImpactScenariosToSave;
    expect(call[0].GenerationImpact.ImpactContent).toEqual(
      impactScenarioV2_KAES_Calc__Generation_ImpactContent
    );
    expect(call[0].EfficiencyImpact.ImpactContent).toEqual(
      impactScenarioV2_KAES_Calc__Efficiency_ImpactContent
    );
  });
});
