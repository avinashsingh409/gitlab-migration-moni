import {
  AssetIssueImpactScenario,
  BlankScenario,
  CalculationFactors,
  CalculationType,
} from '@atonix/atx-core';
import { AssetIssueWithOptions } from '@atonix/shared/api';
import { BehaviorSubject } from 'rxjs';
import { EfficiencyImpactContent } from '../model/impact-calculator/efficiency-v2';
import { AssetIssueImpactTypeID } from '../model/impact-calculator/impact-type';
import { ProductionImpactContent } from '../model/impact-calculator/production-v2';
export const assetIssueWithOptionsMaintenanceReliability = jest.fn(
  () =>
    new BehaviorSubject<AssetIssueWithOptions>({
      Issue: {} as any,
      Impacts: [
        {
          AssetIssueImpactTypeID: AssetIssueImpactTypeID.GenerationDerate, // hasDerate
        },
        {
          AssetIssueImpactTypeID: AssetIssueImpactTypeID.GenerationReliability, // hasGeneration
        },
        {
          AssetIssueImpactTypeID: AssetIssueImpactTypeID.HeatRate, // hasEfficiency
        },
      ],
    } as any) as any
);

export const saveAssetIssue = jest.fn(
  () =>
    new BehaviorSubject<any>({
      Results: [],
    } as any)
);

export const blankScenario1 = jest.fn(
  () =>
    new BehaviorSubject<BlankScenario>({
      NetUnitHeatRate: 10200,
      CreatedByUserID: 23,
      FuelCost: 1.5,
      UnitCapability: 394,
      CapacityCost: 25,
      CapacityFactor: 0.85,
      PercentLikelihood: 95,
    } as any) as any
);

export const blankScenario2 = jest.fn(
  () =>
    new BehaviorSubject<BlankScenario>({
      NetUnitHeatRate: 10400,
      CreatedByUserID: 23,
      FuelCost: 1.55,
      UnitCapability: 158.6,
      CapacityCost: 25,
      CapacityFactor: 0.65,
      PercentLikelihood: 95,
    } as any) as any
);

export const blankScenarioV2 = jest.fn(
  () =>
    new BehaviorSubject<BlankScenario>({
      NetUnitHeatRate: 10400,
      CreatedByUserID: 23,
      FuelCost: 1.55,
      UnitCapability: 158.6,
      CapacityCost: 25,
      CapacityFactor: 0.65,
      PercentLikelihood: 95,
      ImpactAttributesList: [
        { AttrName: 'ImpactCapacityUoM', AttrType: 'string', AttrValue: 'MWh' },
        { AttrName: 'ImpactUoT', AttrType: 'string', AttrValue: 'h' },
        {
          AttrName: 'ImpactCapacityValue',
          AttrType: 'number',
          AttrValue: '660',
        },
        {
          AttrName: 'ImpactTimeToDaysConversion',
          AttrType: 'number',
          AttrValue: '24',
        },
        { AttrName: 'ImpactCurrency', AttrType: 'string', AttrValue: '$' },
        {
          AttrName: 'ImpactCurrencyToDollars',
          AttrType: 'number',
          AttrValue: '1',
        },
        {
          AttrName: 'ImpactProductionRevenueValue',
          AttrType: 'number',
          AttrValue: '40.5',
        },
        {
          AttrName: 'ImpactConsumableLabel',
          AttrType: 'string',
          AttrValue: 'Fuel Cost',
        },
        {
          AttrName: 'ImpactConsumableUoM',
          AttrType: 'string',
          AttrValue: '$/Mbtu',
        },
        {
          AttrName: 'ImpactConsumableValue',
          AttrType: 'number',
          AttrValue: '3.2',
        },
        {
          AttrName: 'ImpactNomConsRateLabel',
          AttrType: 'string',
          AttrValue: 'Net Heat Rate',
        },
        {
          AttrName: 'ImpactNomConsRateUoM',
          AttrType: 'string',
          AttrValue: 'Btu/kWh',
        },
        {
          AttrName: 'ImpactNomConsRateValue',
          AttrType: 'number',
          AttrValue: '9050',
        },
        {
          AttrName: 'ImpactConsCostToRateConvert',
          AttrType: 'number',
          AttrValue: '1000000',
        },
        {
          AttrName: 'ImpactProdCostToRateConvert',
          AttrType: 'number',
          AttrValue: '1000',
        },
        {
          AttrName: 'ImpactNomCapacityUtilization',
          AttrType: 'number',
          AttrValue: '0.8',
        },
      ],
    } as any) as any
);

export const impactCalculationFactorsByImpactCategoryType = jest.fn(
  (categoryType: number) => {
    if (categoryType === AssetIssueImpactTypeID.GenerationDerate) {
      return new BehaviorSubject<CalculationFactors[]>([
        {
          AssetIssueImpactCalculationFactorID: 1,
          AssetIssueImpactCategoryTypeID: 1,
          CalcuationFactorAbbrev: 'Main Steam Temperature : Subcritical',
          CalculationFactor: 0.15,
          CalculationFactorDesc: 'Main Steam Temperature : Subcritical',
          CalculationFactorRatio: 10,
          CalculationFactorUnits: '°F',
        },
        {
          AssetIssueImpactCalculationFactorID: 65,
          AssetIssueImpactCategoryTypeID: 3,
          CalcuationFactorAbbrev: 'Manual Input',
          CalculationFactor: 1,
          CalculationFactorDesc: 'Manual Input',
          CalculationFactorRatio: 1,
          CalculationFactorUnits: ' ',
        },
      ] as any) as any;
    } else if (categoryType === AssetIssueImpactTypeID.HeatRate) {
      return new BehaviorSubject<CalculationFactors[]>([
        {
          AssetIssueImpactCalculationFactorID: 51,
          AssetIssueImpactCategoryTypeID: 3,
          CalcuationFactorAbbrev: 'LP Turbine Efficiency',
          CalculationFactor: 0.45,
          CalculationFactorDesc: 'LP Turbine Efficiency',
          CalculationFactorRatio: 1,
          CalculationFactorUnits: '%',
        },
        {
          AssetIssueImpactCalculationFactorID: 64,
          AssetIssueImpactCategoryTypeID: 1,
          CalcuationFactorAbbrev: 'Manual Input',
          CalculationFactor: 1,
          CalculationFactorDesc: 'Manual Input',
          CalculationFactorRatio: 1,
          CalculationFactorUnits: ' ',
        },
      ] as any) as any;
    }
  }
);

export const impactScenario = jest.fn((assetIssueId: number) => {
  if (assetIssueId === 1) {
    return new BehaviorSubject<AssetIssueImpactScenario[]>([
      {
        AssetIssueImpactScenarioName: 'test1',
        AssetIssueImpactScenarioNotes: 'test tester',
        PercentLikelihood: 13.0,
        GenerationImpactRange: {
          StartDate: new Date('2021-01-17T06:00:00-06:00'),
          CurrentDate: new Date('2021-01-20T06:00:00-06:00'),
          EndDate: new Date('2021-01-23T06:00:00-06:00'),
          DaysElapsed: 3.0,
          DaysPending: 3.0,
          DaysTotal: 6.0,
        },
        CapacityCost: 25.0,
        FuelCost: 1.5,
        CapacityFactor: 0.75,
        UnitCapability: 394.0,
        NetUnitHeatRate: 10200,
        GenerationImpact: {
          AssetIssueAssetIssueImpactTypeMapID: 53344,
          AssetIssueID: 1015558,
          AssetIssueImpactTypeID: 3,
          Impact_Cost: 4679.28,
          InputString: '1,34,100,100,100,2339.64,2339.64,15.3',
          CategoryID: 0,
          Impact_Cost_Monthly: 23396.4,
        },
      } as any,
    ]) as any;
  }
});

export const impactScenario2 = jest.fn((assetIssueId: number) => {
  if (assetIssueId === 1) {
    return new BehaviorSubject<AssetIssueImpactScenario[]>([
      {
        AssetIssueImpactScenarioName: 'Test 2',
        AssetIssueImpactScenarioNotes: 'test tester',
        PercentLikelihood: 13.0,
        GenerationImpactRange: {
          StartDate: null,
          CurrentDate: null,
          EndDate: null,
          DaysElapsed: 5.0,
          DaysPending: 5.0,
          DaysTotal: 10.0,
        },
        CapacityCost: 25.0,
        FuelCost: 1.55,
        CapacityFactor: 0.65,
        UnitCapability: 158.6,
        NetUnitHeatRate: 10200,
        GenerationImpact: {
          ImpactContent: '',
          AssetIssueAssetIssueImpactTypeMapID: 53344,
          AssetIssueID: 1015558,
          AssetIssueImpactTypeID: 3,
          Impact: 3.35,
          Impact_Cost: 155289.6,
          InputString: '65,34,100,100,100,77644.8,77644.8,13,25',
          CategoryID: 0,
          DisplayOrder: 3,
          Impact_Cost_Monthly: 465868.8,
        },
      } as any,
    ]) as any;
  }
});

export const impactScenarioV2_KAES_Calc__Generation_ImpactContent = `{"version":2,"errors":[],"attributes":{"productionCapacityUnits":"Tons","productionTimeUnits":"Day","productionCapacity":100,"productionTimeToDays":1,"productionRevenue":50,"consumableLabel":"Steam Cost","consumableUnits":"lb","consumableCost":1,"consumptionRateLabel":"Nominal Steam Consumption","consumptionRateUnits":"lb/ton","consumptionRateValue":500,"consumptionToCostRate":1,"productionToCostRate":1,"capacityUtilization":1},"totalCost":{"toDate":350,"potentialFuture":0,"totalAvg":350},"impactQuantity":10,"calculationFactorID":65,"derate":{"hasDerateOverride":false,"derateOverrideValue":100,"percentTimeDerateInEffect":{"toDate":100,"potentialFuture":100,"totalAvg":100}},"relevantProductionCost":15,"lostMarginOpportunity":50,"overrideTotal":"CalculatedImpact"}`;
export const impactScenarioV2_KAES_Calc__Efficiency_ImpactContent = `{"version":2,"attributes":{"productionCapacityUnits":"Tons","productionTimeUnits":"Day","productionCapacity":100,"productionTimeToDays":24,"productionRevenue":50,"consumableLabel":"Steam Cost","consumableUnits":"lb","consumableCost":1,"consumptionRateLabel":"Nominal Steam Consumption","consumptionRateUnits":"lb/ton","consumptionRateValue":500,"consumptionToCostRate":1,"productionToCostRate":1,"capacityUtilization":1},"errors":[],"totalCost":{"toDate":2500,"potentialFuture":0,"totalAvg":2500},"impactQuantity":5,"calculationFactorID":64,"overrideTotal":"CalculatedImpact"}`;

export const blankScenarioV2_Generation_ImpactContent = `{"version":2,"attributes":{"productionCapacityUnits":"MWh","productionTimeUnits":"h","productionCapacity":660,"productionTimeToDays":24,"productionRevenue":40.5,"consumableLabel":"Fuel Cost","consumableUnits":"$/Mbtu","consumableCost":3.2,"consumptionRateLabel":"Net Heat Rate","consumptionRateUnits":"Btu/kWh","consumptionRateValue":9050,"consumptionToCostRate":1000000,"productionToCostRate":1000,"capacityUtilization":0.8},"totalCost":{"toDate":0,"potentialFuture":0,"totalAvg":0},"impactQuantity":0,"calculationFactorID":65,"derate":{"hasDerateOverride":false,"derateOverrideValue":100,"percentTimeDerateInEffect":{"toDate":100,"potentialFuture":100,"totalAvg":100}},"relevantProductionCost":0,"lostMarginOpportunity":40.5,"overrideTotal":"CalculatedImpact"}`;
export const blankScenarioV2_Efficiency_ImpactContent = `{"version":2,"attributes":{"productionCapacityUnits":"MWh","productionTimeUnits":"h","productionCapacity":660,"productionTimeToDays":24,"productionRevenue":40.5,"consumableLabel":"Fuel Cost","consumableUnits":"$/Mbtu","consumableCost":3.2,"consumptionRateLabel":"Net Heat Rate","consumptionRateUnits":"Btu/kWh","consumptionRateValue":9050,"consumptionToCostRate":1000000,"productionToCostRate":1000,"capacityUtilization":0.8},"totalCost":{"toDate":0,"potentialFuture":0,"totalAvg":0},"impactQuantity":0,"calculationFactorID":64,"overrideTotal":"CalculatedImpact"}`;

export const impactScenarioV2_KAES_Calc = jest.fn((assetIssueId: number) => {
  if (assetIssueId === 1) {
    return new BehaviorSubject<AssetIssueImpactScenario[]>([
      {
        PercentLikelihood: 100.0,
        GenerationImpact: {
          ImpactContent: impactScenarioV2_KAES_Calc__Generation_ImpactContent,
          InputString: '',
        },
        GenerationImpactRange: {
          StartDate: null,
          CurrentDate: null,
          EndDate: null,
          DaysElapsed: 1,
          DaysPending: 0,
          DaysTotal: 1,
        },
        EfficiencyImpact: {
          ImpactContent: impactScenarioV2_KAES_Calc__Efficiency_ImpactContent,
          InputString: '',
        },
        EfficiencyImpactRange: {
          CurrentDate: null,
          DaysElapsed: 1,
          DaysPending: 0,
          DaysTotal: 1,
          EndDate: null,
          StartDate: null,
        },
      } as any,
    ]) as any;
  }
});

export const impactScenarioV2_Default = jest.fn((assetIssueId: number) => {
  if (assetIssueId === 1) {
    return new BehaviorSubject<AssetIssueImpactScenario[]>([
      {
        PercentLikelihood: 100.0,
        GenerationImpact: {
          ImpactContent: `{"version":2,"attributes":{"productionCapacityUnits":"MWh","productionTimeUnits":"Hour","productionCapacity":394,"productionTimeToDays":24,"productionRevenue":25,"consumableLabel":"Fuel Cost","consumableUnits":"h","consumableCost":1.5,"consumptionRateLabel":"Nominal Unit Heat Rate","consumptionRateUnits":"Btu/kWh","consumptionRateValue":10200,"consumptionToCostRate":1000000,"productionToCostRate":1000,"capacityUtilization":0.75},"totalCost":{"toDate":0,"potentialFuture":0},"impactQuantity":50.7614,"calculationFactorID":65,"derate":{"hasDerateOverride":false,"derateOverrideValue":100,"percentTimeDerateInEffect":{"toDate":100,"potentialFuture":100,"totalAvg":100}},"relevantProductionCost":0,"lostMarginOpportunity":25, "overrideTotal":"CalculatedImpact"}`,
          InputString: '',
        },
        GenerationImpactRange: {
          StartDate: null,
          CurrentDate: null,
          EndDate: null,
          DaysElapsed: 7,
          DaysPending: 0,
          DaysTotal: 7,
        },
        EfficiencyImpact: {
          Impact: 0.64,
          // ImpactContent: `{"version":2,"attributes":{"productionCapacityUnits":"Tons","productionTimeUnits":"Day","productionCapacity":100,"productionTimeToDays":1,"productionRevenue":50,"consumableLabel":"Steam Cost","consumableUnits":"lb","consumableCost":1,"consumptionRateLabel":"Nominal Steam Consumption","consumptionRateUnits":"lb/ton","consumptionRateValue":500,"consumptionToCostRate":1,"productionToCostRate":1,"capacityUtilization":1},"totalCost":{"toDate":0,"potentialFuture":0},"impactQuantity":23,"calculationFactorID":65,"relevantProductionCost":2,"lostMarginOpportunity":23, "overrideTotal":"CalculatedImpact"}`,
          InputString: '51,32,2083.3459199999998,2083.3459199999998',
        },
        EfficiencyImpactRange: {
          CurrentDate: '2021-06-08T05:00:00-05:00',
          DaysElapsed: 3,
          DaysPending: 3,
          DaysTotal: 6,
          EndDate: '2021-06-11T05:00:00-05:00',
          StartDate: '2021-06-05T05:00:00-05:00',
        },
      } as any,
    ]) as any;
  }
});

export const impactScenarioV2_Override_Totals = jest.fn(
  (assetIssueId: number) => {
    if (assetIssueId === 1) {
      return new BehaviorSubject<AssetIssueImpactScenario[]>([
        {
          PercentLikelihood: 100.0,
          GenerationImpact: {
            ImpactContent: `{"version":2,"attributes":{"productionCapacityUnits":"MWh","productionTimeUnits":"Hour","productionCapacity":394,"productionTimeToDays":24,"productionRevenue":25,"consumableLabel":"Fuel Cost","consumableUnits":"h","consumableCost":1.5,"consumptionRateLabel":"Nominal Unit Heat Rate","consumptionRateUnits":"Btu/kWh","consumptionRateValue":10200,"consumptionToCostRate":1000000,"productionToCostRate":1000,"capacityUtilization":0.75},"totalCost":{"toDate":1000,"potentialFuture":10},"impactQuantity":50.7614,"calculationFactorID":65,"derate":{"hasDerateOverride":false,"derateOverrideValue":100,"percentTimeDerateInEffect":{"toDate":100,"potentialFuture":100,"totalAvg":100}},"relevantProductionCost":0,"lostMarginOpportunity":25, "overrideTotal":"UserDefinedImpact"}`,
            InputString: '',
          },
          GenerationImpactRange: {
            StartDate: null,
            CurrentDate: null,
            EndDate: null,
            DaysElapsed: 7,
            DaysPending: 0,
            DaysTotal: 7,
          },
          EfficiencyImpact: {
            ImpactContent: `{"version":2,"attributes":{"productionCapacityUnits":"Tons","productionTimeUnits":"Day",
            "productionCapacity":100,"productionTimeToDays":24,"productionRevenue":50,"consumableLabel":"Steam Cost",
            "consumableUnits":"lb","consumableCost":1,"consumptionRateLabel":"Nominal Steam Consumption","consumptionRateUnits":"lb/ton",
            "consumptionRateValue":500,"consumptionToCostRate":1,"productionToCostRate":1,"capacityUtilization":1},
            "totalCost":{"toDate":20,"potentialFuture":21},"impactQuantity":5,
            "calculationFactorID":64,"overrideTotal":"UserDefinedImpact"}`,
            InputString: '',
          },
          EfficiencyImpactRange: {
            CurrentDate: '2021-06-08T05:00:00-05:00',
            DaysElapsed: 3,
            DaysPending: 3,
            DaysTotal: 6,
            EndDate: '2021-06-11T05:00:00-05:00',
            StartDate: '2021-06-05T05:00:00-05:00',
          },
        } as any,
      ]) as any;
    }
  }
);

export const impactContent: EfficiencyImpactContent = {
  version: 2,
  attributes: {
    productionCapacityUnits: 'Tons',
    productionTimeUnits: 'Day',
    productionCapacity: 100,
    productionTimeToDays: 1,
    productionRevenue: 50,
    consumableLabel: 'Steam Cost',
    consumableUnits: 'lb',
    consumableCost: 1,
    consumptionRateLabel: 'Nominal Steam Consumption',
    consumptionRateUnits: 'lb/ton',
    consumptionRateValue: 500,
    consumptionToCostRate: 1,
    productionToCostRate: 1,
    capacityUtilization: 1,
  },
  errors: [],
  totalCost: {
    toDate: 0.0,
    potentialFuture: 0.0,
    totalAvg: 0.0,
  },
  impactQuantity: 23,
  calculationFactorID: 65,
  overrideTotal: CalculationType.UserDefinedImpact,
};
