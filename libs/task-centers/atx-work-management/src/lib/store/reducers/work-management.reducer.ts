import { ActionReducerMap } from '@ngrx/store';
import { IWorkManagementState } from '../state/work-management-state';
import { issueSnapshotReducer } from './issue-snapshot.reducer';
import { quickReplyReducer } from './quick-reply.reducer';
import { summaryReducer } from './summary.reducer';

export const workManagementReducers: ActionReducerMap<
  IWorkManagementState,
  any
> = {
  summaryState: summaryReducer,
  issueSnapshotState: issueSnapshotReducer,
  quickReplyState: quickReplyReducer,
};
