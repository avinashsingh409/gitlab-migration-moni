import { IQuickReplyAttachment } from '@atonix/atx-core';
import { on, createReducer } from '@ngrx/store';
import * as actions from '../actions/quick-reply.actions';
import {
  initialQuickReplyState,
  IQuickReplyState,
} from '../state/quick-reply-state';

const _quickReplyReducer = createReducer(
  initialQuickReplyState,
  on(actions.initializeQuickReply, initializeQuickReply),
  on(actions.addQuickReplyAttachments, addQuickReplyAttachments),
  on(actions.addQuickReplyAttachmentsSuccess, addQuickReplyAttachmentsSuccess),
  on(actions.addQuickReplyAttachmentsFailure, addQuickReplyAttachmentsFailure),
  on(actions.removeQuickReplyAttachment, removeQuickReplyAttachment),
  on(actions.clearQuickReplyAttachments, clearQuickReplyAttachments)
);

export function initializeQuickReply(state: IQuickReplyState) {
  return {
    ...state,
    Attachments: [],
  };
}

export function addQuickReplyAttachments(
  state: IQuickReplyState,
  payload: { files: any[]; assetID: number }
) {
  return {
    ...state,
    UploadingAttachments: true,
  };
}

export function addQuickReplyAttachmentsSuccess(
  state: IQuickReplyState,
  payload: {
    attachments: IQuickReplyAttachment[];
  }
) {
  const newAttachments = [...state.Attachments];
  payload.attachments.map((att) => newAttachments.push(att));

  return {
    ...state,
    Attachments: newAttachments,
    UploadingAttachments: false,
  };
}

export function addQuickReplyAttachmentsFailure(
  state: IQuickReplyState,
  payload: Error
) {
  return { ...state };
}

export function removeQuickReplyAttachment(
  state: IQuickReplyState,
  payload: { index: number }
) {
  const newAttachments = [...state.Attachments];
  newAttachments.splice(payload.index, 1);

  return {
    ...state,
    Attachments: newAttachments,
  };
}

export function clearQuickReplyAttachments(state: IQuickReplyState) {
  return {
    ...state,
    Attachments: [],
    UploadingAttachments: false,
  };
}

export function quickReplyReducer(state, action) {
  return _quickReplyReducer(state, action);
}
