import sum from 'lodash/sum';
import {
  isNil,
  IAssetIssueDonutData,
  IAtxTreeRetrieval,
  ITreePermissions,
} from '@atonix/atx-core';
import orderBy from 'lodash/orderBy';
import { createReducer, on } from '@ngrx/store';
import isEqual from 'lodash/isEqual';
import {
  ITreeState,
  ITreeStateChange,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import {
  createDefaultDonutState,
  IDonutState,
  IDonutData,
} from '@atonix/atx-chart';

import * as actions from '../actions/summary.actions';
import {
  ISummaryState,
  initialSummaryState,
  savedIssueListAdapter,
} from '../state/summary-state';

import { IListState, ISavedIssueList } from '@atonix/shared/api';
import { Update } from '@ngrx/entity';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import { getCurrencySymbol } from '@angular/common';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
const _summaryReducer = createReducer(
  initialSummaryState,
  on(actions.permissionsRequest, getPermissions),
  on(actions.permissionsRequestSuccess, getPermissionsSuccess),
  on(actions.permissionsRequestFailure, getPermissionsFailure),
  on(actions.treeStateChange, treeStateChange),
  on(actions.assetTreeDataRequestFailure, treeDataFailed),
  on(actions.donutsRequest, getDonutsLoading),
  on(actions.donutsSuccess, getDonutsSuccess),
  on(actions.donutsFailure, getDonutsFailure),
  on(actions.leftDonutRefreshed, leftDonutRefreshed),
  on(actions.leftDonutOthersSelected, leftDonutOthersSelected),
  on(actions.middleDonutRefreshed, middleDonutRefreshed),
  on(actions.middleDonutOthersSelected, middleDonutOthersSelected),
  on(actions.rightDonutRefreshed, rightDonutRefreshed),
  on(actions.rightDonutOthersSelected, rightDonutOthersSelected),
  on(actions.treeSizeChange, treeSizeChange),
  on(actions.listStateChange, listStateChanged),
  on(actions.listSaveSuccess, saveListSuccess),
  on(actions.listsLoadSuccess, loadListsSuccess),
  on(actions.listDeleteSuccess, deleteListSuccess),
  on(actions.loadDefaultList, loadDefaultList),
  on(actions.listLoad, loadList),
  on(actions.toggleFloatingFilter, toggleFloatingFilter)
);

export function getPermissions(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state?.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state?.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state?.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsSuccess(
  state: ISummaryState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(state?.AssetTreeState.treeConfiguration, {
      canView: payload.canView,
      canEdit: false,
      canAdd: false,
      canDelete: false,
    }),
    treeNodes: { ...state?.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state?.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsFailure(state: ISummaryState) {
  const treeState: ITreeState = {
    treeConfiguration: setPermissions(
      state?.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state?.AssetTreeState.treeNodes },
    hasDefaultSelectedAsset: state?.AssetTreeState.hasDefaultSelectedAsset,
  };
  return { ...state, AssetTreeState: treeState };
}

export function treeStateChange(
  state: ISummaryState,
  payload: ITreeStateChange
) {
  const newPayload = treeRetrievedStateChange(payload);
  return {
    ...state,
    AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
  };
}

export function treeDataFailed(state: ISummaryState, payload: Error) {
  return { ...state };
}

// Donut Chart
export function getDonutsLoading(
  state: ISummaryState,
  props: { state: IServerSideGetRowsRequest }
) {
  const subtitle = 'loading...';

  const leftDonutState = {
    ...state.LeftDonutState,
    subtitle,
    drilldownSection: 1,
    AllDonutData: [],
  };
  const middleDonutState = {
    ...state.MiddleDonutState,
    subtitle,
    drilldownSection: 1,
    AllDonutData: [],
  };
  const rightDonutState = {
    ...state.RightDonutState,
    subtitle,
    drilldownSection: 1,
    AllDonutData: [],
  };
  return {
    ...state,
    DonutKey: null,
    LeftDonutState: leftDonutState,
    MiddleDonutState: middleDonutState,
    RightDonutState: rightDonutState,
  };
}

export function getDonutsSuccess(
  state: ISummaryState,
  payload: { values: IAssetIssueDonutData; asset: string; currency: string }
) {
  if (payload && payload.values) {
    // Categories is the left donut
    let categories = payload.values.IssuesByCategory;
    categories = orderBy(
      categories,
      ['SliceSize', 'SliceDescription'],
      ['desc', 'asc']
    );
    const categorySum = sum(categories.map((n) => n.SliceSize));
    const categoryDonutData = categories.map((item) => {
      return {
        name: item.SliceDescription,
        y: item.SliceSize,
        id: String(item.SliceID),
      } as IDonutData;
    });

    const leftDonutState: IDonutState = {
      ...state.LeftDonutState,
      subtitle: String(categorySum),
      AllDonutData: categoryDonutData,
    };

    // Assets is the middle donut
    let assets = payload.values.IssuesByAsset;
    assets = orderBy(
      assets,
      ['SliceSize', 'SliceDescription'],
      ['desc', 'asc']
    );
    const assetsSum = sum(assets.map((n) => n.SliceSize));
    const assetsDonutData = assets.map((item) => {
      return {
        name: item.SliceDescription,
        y: item.SliceSize,
        id: String(item.SliceID),
      };
    });
    const middleDonutState: IDonutState = {
      ...state.MiddleDonutState,
      subtitle: String(assetsSum),
      AllDonutData: assetsDonutData,
    };

    // IMpacts is the right donut
    let impacts = payload.values.Impact;
    impacts = orderBy(
      impacts,
      ['SliceSize', 'SliceDescription'],
      ['desc', 'asc']
    );
    const impactsSum = sum(impacts.map((n) => n.SliceSize));
    const impactsDonutData = impacts.map((item) => {
      return {
        name: item.SliceDescription,
        y: item.SliceSize,
        id: String(item.SliceID),
      };
    });

    const currencySymbol = getCurrencySymbol(payload.currency, 'narrow', 'en');

    let impactsSumString = currencySymbol + '0';
    if (impactsSum >= 1000000 && impactsSum < 100000000) {
      impactsSumString =
        currencySymbol + String((impactsSum / 1000000).toFixed(1)) + 'M';
    } else if (impactsSum >= 100000000) {
      impactsSumString =
        currencySymbol + String(Math.trunc(impactsSum / 1000000)) + 'M';
    } else if (impactsSum < 1000000 && impactsSum > 1000) {
      impactsSumString =
        currencySymbol + String(Math.round(impactsSum / 1000)) + 'K';
    } else {
      impactsSumString = currencySymbol + String(Math.round(impactsSum));
    }

    const rightDonutState: IDonutState = {
      ...state.RightDonutState,
      subtitle: impactsSumString,
      AllDonutData: impactsDonutData,
    };

    return {
      ...state,
      DonutKey: payload.asset,
      LeftDonutState: leftDonutState,
      MiddleDonutState: middleDonutState,
      RightDonutState: rightDonutState,
    };
  } else {
    return { ...state };
  }
}

export function getDonutsFailure(state: ISummaryState, payload: Error) {
  return {
    ...state,
    RightDonutState: createDefaultDonutState({
      title: 'Item Impact',
      subtitle: '$0',
      toolTipFormat: { isRounded: true, decimalPlaces: 0 },
    }),
    MiddleDonutState: createDefaultDonutState({
      title: 'Item Count by Asset',
      subtitle: '0',
      toolTipFormat: { isRounded: true, decimalPlaces: 0 },
    }),
    LeftDonutState: createDefaultDonutState({
      title: 'Item Count by Category',
      subtitle: '0',
      toolTipFormat: {
        format: 'currency',
        locale: 'en-US',
        currency: 'USD',
        isRounded: true,
        decimalPlaces: 0,
      },
    }),
  };
}

// this resets the state of the left donut when the refresh button is pressed
function leftDonutRefreshed(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    LeftDonutState: {
      ...state.LeftDonutState,
      drilldownSection: 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}
// this modifies the state for when the "others" section of the left donut is clicked
function leftDonutOthersSelected(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    LeftDonutState: {
      ...state.LeftDonutState,
      drilldownSection: state.LeftDonutState.drilldownSection + 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}

// this resets the state of the middle donut when the refresh button is pressed
function middleDonutRefreshed(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    MiddleDonutState: {
      ...state.MiddleDonutState,
      drilldownSection: 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}
// this modifies the state for when the "others" section of the middle donut is clicked
function middleDonutOthersSelected(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    MiddleDonutState: {
      ...state.MiddleDonutState,
      drilldownSection: state.MiddleDonutState.drilldownSection + 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}

// this resets the state of the right donut when the refresh button is pressed
function rightDonutRefreshed(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    RightDonutState: {
      ...state.RightDonutState,
      drilldownSection: 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}
// this modifies the state for when the "others" section of the right donut is clicked
function rightDonutOthersSelected(
  state: ISummaryState,
  payload: { IDtoAppend: string }
) {
  return {
    ...state,
    RightDonutState: {
      ...state.RightDonutState,
      drilldownSection: state.RightDonutState.drilldownSection + 1,
      IDappendedToOthers: payload.IDtoAppend,
    },
  };
}

export function treeSizeChange(
  state: ISummaryState,
  payload: { value: number }
) {
  return {
    ...state,
    TreeSize: payload.value,
  };
}

export function listStateChanged(
  state: ISummaryState,
  payload: { listState: IListState }
) {
  return filterChangeSoUncheck(
    { ...state, DisplayedListState: payload.listState, SavedListState: null },
    payload
  );
}

export function loadListsSuccess(
  state: ISummaryState,
  payload: { lists: ISavedIssueList[] }
) {
  return {
    ...state,
    IssueLists: savedIssueListAdapter.addMany(payload.lists, state.IssueLists),
  };
}

export function saveListSuccess(
  state: ISummaryState,
  payload: { list: ISavedIssueList }
) {
  return {
    ...state,
    IssueLists: savedIssueListAdapter.upsertOne(payload.list, state.IssueLists),
  };
}

export function deleteListSuccess(
  state: ISummaryState,
  payload: { id: number }
) {
  return {
    ...state,
    IssueLists: savedIssueListAdapter.removeOne(payload.id, state.IssueLists),
  };
}

export function getDefaultListState(): IListState {
  return {
    columns: null,
    sort: [],
    groups: [],
    filter: {
      ActivityStatus: {
        values: ['Open'],
        filterType: 'set',
      },
    },
  };
}

export function loadDefaultList(state: ISummaryState): ISummaryState {
  const updates: Update<ISavedIssueList>[] = (
    state.IssueLists.ids as number[]
  ).map((id) => {
    return { id, changes: { checked: false } };
  });

  const IssueLists = savedIssueListAdapter.updateMany(
    updates,
    state.IssueLists
  );

  return {
    ...state,
    SavedListState: getDefaultListState(),
    IssueLists,
  };
}

export function loadList(state: ISummaryState, payload: { id: number }) {
  const updates: Update<ISavedIssueList>[] = (
    state.IssueLists.ids as number[]
  ).map((id) => {
    return { id, changes: { checked: id === payload.id } };
  });

  const IssueLists = savedIssueListAdapter.updateMany(
    updates,
    state.IssueLists
  );

  return {
    ...state,
    SavedListState: { ...state.IssueLists.entities[payload.id].state },
    IssueLists,
  };
}

export function toggleFloatingFilter(
  state: ISummaryState,
  payload: { filterOn?: boolean }
): ISummaryState {
  return {
    ...state,
    FloatingFilter: payload.filterOn ?? !state.FloatingFilter,
  };
}

export function summaryReducer(state, action) {
  return _summaryReducer(state, action);
}

function filterChangeSoUncheck(
  state: ISummaryState,
  _: { listState: IListState }
) {
  // The payload argument is not used, but I included it here
  // because the action has a payload, and i didn't want to
  // indicate that there is no payload
  const listOfIssues = [] as Update<ISavedIssueList>[];
  // displayedIssueList is the state of the issue list that is currently displayed
  // Ignore the AssetID filter
  const displayedIssueList = {
    ...state.DisplayedListState,
    filter: { ...state.DisplayedListState.filter, AssetID: null },
  };
  for (const key of state.IssueLists.ids) {
    // iteratedIssueList is the state of the issue list in the loop
    // Ignore the AssetID filter
    const iteratedIssueList = {
      ...state.IssueLists.entities[key].state,
      filter: { ...state.IssueLists.entities[key].state.filter, AssetID: null },
    };
    if (
      iteratedIssueList?.filter?.Priority?.values &&
      displayedIssueList?.filter?.Priority?.values
    ) {
      if (
        isEqual(
          // I had to do this weird method because the filters are in an array in iteratedIssueList certain order,
          // but the order in the array doesn't matter to the user, so I sorted the arrays before comparing them.
          {
            ...iteratedIssueList,
            filter: {
              ...iteratedIssueList.filter,
              Priority: {
                ...iteratedIssueList.filter.Priority,
                values: [...iteratedIssueList.filter.Priority.values].sort(),
              },
            },
          },
          {
            ...displayedIssueList,
            filter: {
              ...displayedIssueList.filter,
              Priority: {
                ...displayedIssueList.filter.Priority,
                values: [...displayedIssueList.filter.Priority.values].sort(),
              },
            },
          }
        )
      ) {
        listOfIssues.push({
          changes: { checked: true },
          id: key,
        } as Update<ISavedIssueList>);
      } else {
        listOfIssues.push({
          changes: { checked: false },
          id: key,
        } as Update<ISavedIssueList>);
      }
    } else {
      if (isEqual(iteratedIssueList, displayedIssueList)) {
        listOfIssues.push({
          changes: { checked: true },
          id: key,
        } as Update<ISavedIssueList>);
      } else {
        listOfIssues.push({
          changes: { checked: false },
          id: key,
        } as Update<ISavedIssueList>);
      }
    }
  }
  return {
    ...state,
    IssueLists: savedIssueListAdapter.updateMany(
      listOfIssues,
      state.IssueLists
    ),
  };
}
