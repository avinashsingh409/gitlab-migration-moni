import { on, createReducer } from '@ngrx/store';
import {
  IIssueSnapshotState,
  initialIssueSnapshotState,
} from '../state/issue-snapshot-state';
import * as actions from '../actions/issue-snapshot.actions';
import {
  EDiscussion,
  getMoreEntries,
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
  processDiscussion,
  processEntry,
  getDefaultDiscussion,
} from '@atonix/atx-discussion';
import {
  IDiscussion,
  IDiscussionAttachment,
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  IImageDetails,
  ISaveDiscussionEntry,
  isNil,
  ITaggingKeyword,
} from '@atonix/atx-core';
import { getDefaultSummary } from '../../model/issue-summary-state';
import {
  alterAssetTreeState,
  createTreeBuilder,
  getDefaultTree,
  ITreeStateChange,
} from '@atonix/atx-asset-tree';
import { ExternalAsset, IWorkRequest } from '@atonix/shared/api';

const _issueSnapshotReducer = createReducer(
  initialIssueSnapshotState,
  on(actions.loadDiscussion, loadDiscussion),
  on(actions.loadDiscussionSuccess, loadDiscussionSuccess),
  on(actions.loadDiscussionFailure, loadDiscussionFailure),
  on(actions.clearState, clearState),
  on(actions.showMoreEntries, showMoreEntries),
  on(actions.getImageAttachmentsSuccess, getImageAttachmentsSuccess),
  on(actions.getImageAttachmentsFailure, getImageAttachmentsFailure),
  on(actions.getNewDiscussionEntry, getNewDiscussionEntry),
  on(actions.getNewDiscussionEntrySuccess, getNewDiscussionEntrySuccess),
  on(actions.getNewDiscussionEntryFailure, getNewDiscussionEntryFailure),
  on(actions.saveDiscussionEntry, saveDiscussionEntry),
  on(actions.saveDiscussionEntrySuccess, saveDiscussionEntrySuccess),
  on(actions.saveDiscussionEntryFailure, saveDiscussionEntryFailure),
  on(actions.cancelDiscussionEntryEdits, cancelDiscussionEntryEdits),
  on(actions.editDiscussionEntryValues, editDiscussionEntryValues),
  on(actions.deleteDiscussionEntrySuccess, deleteDiscussionEntrySuccess),
  on(actions.deleteDiscussionEntryFailure, deleteDiscussionEntryFailure),
  on(actions.addDiscussionEntryAttachments, addDiscussionEntryAttachments),
  on(
    actions.addDiscussionEntryAttachmentsSuccess,
    addDiscussionEntryAttachmentsSuccess
  ),
  on(
    actions.addDiscussionEntryAttachmentsFailure,
    addDiscussionEntryAttachmentsFailure
  ),
  on(actions.searchTaggingKeyword, searchTaggingKeyword),
  on(actions.searchTaggingKeywordSuccess, searchTaggingKeywordSuccess),
  on(actions.searchTaggingKeywordFailure, searchTaggingKeywordFailure),
  on(actions.registerKeywordFailure, registerKeywordFailure),
  on(actions.updateDiscussionEntryKeywords, updateDiscussionEntryKeywords),
  on(actions.deleteDiscussionEntryAttachment, deleteDiscussionEntryAttachment),
  on(actions.deleteDiscussionEntryKeyword, deleteDiscussionEntryKeyword),
  on(actions.loadSummary, loadSummary),
  on(actions.convertBVFilesToHTMLSuccess, convertBVFilesToHTMLSuccess),
  on(actions.editSummary, editSummary),
  on(actions.saveSummary, saveSummary),
  on(actions.saveSummarySuccess, saveSummarySuccess),
  on(actions.cancelEditSummary, cancelEditSummary),
  on(actions.assetTreeDropdownStateChange, assetTreeDropdownStateChange),
  on(actions.clearAssetTreeDropdown, clearAssetTreeDropdown),
  on(actions.loadWorkRequestInfo, loadWorkRequestInfo),
  on(actions.loadWorkRequestInfoSuccess, loadWorkRequestInfoSuccess),
  on(actions.loadWorkRequestInfoFailure, loadWorkRequestInfoFailure),
  on(actions.saveWorkRequest, saveWorkRequest),
  on(actions.saveWorkRequestSuccess, saveWorkRequestSuccess),
  on(actions.saveWorkRequestFailure, saveWorkRequestFailure),
  on(actions.pollWorkRequestInfo, pollWorkRequestInfo),
  on(actions.pollWorkRequestInfoSuccess, pollWorkRequestInfoSuccess),
  on(actions.pollWorkRequestInfoFailure, pollWorkRequestInfoFailure),
  on(actions.getAssociatedExternalAsset, getAssociatedExternalAsset),
  on(
    actions.getAssociatedExternalAssetSuccess,
    getAssociatedExternalAssetSuccess
  ),
  on(
    actions.getAssociatedExternalAssetFailure,
    getAssociatedExternalAssetFailure
  ),
  on(actions.getExternalAssets, getExternalAssets),
  on(actions.getExternalAssetsSuccess, getExternalAssetsSuccess),
  on(actions.getExternalAssetsFailure, getExternalAssetsFailure),
  on(actions.clearExternalAssets, clearExternalAssets),
  on(actions.updateSelectedExternalAsset, updateSelectedExternalAsset)
);

//External Asset Selection
export function clearExternalAssets(state: IIssueSnapshotState) {
  return { ...state, externalAssets: [], selectedExternalAsset: null };
}

export function getExternalAssets(state: IIssueSnapshotState) {
  return { ...state, externalAssetsLoading: { loading: true, error: null } };
}

export function getExternalAssetsSuccess(
  state: IIssueSnapshotState,
  payload: { externalAssets: ExternalAsset[] }
) {
  return {
    ...state,
    externalAssets: payload.externalAssets,
    externalAssetsLoading: { loading: false, error: null },
  };
}

export function getExternalAssetsFailure(
  state: IIssueSnapshotState,
  payload: { error: Error }
) {
  return {
    ...state,
    externalAssetsLoading: { loading: false, error: payload.error },
  };
}

export function getAssociatedExternalAsset(
  state: IIssueSnapshotState,
  payload: { assetGuid: string }
) {
  return {
    ...state,
    selectedExternalAssetLoading: {
      loading: true,
      error: null,
    },
  };
}

export function getAssociatedExternalAssetSuccess(
  state: IIssueSnapshotState,
  payload: { externalAsset: ExternalAsset }
) {
  return {
    ...state,
    selectedExternalAsset: payload.externalAsset,
    selectedExternalAssetLoading: {
      loading: false,
      error: null,
    },
  };
}

export function getAssociatedExternalAssetFailure(
  state: IIssueSnapshotState,
  payload: { error: Error }
) {
  return {
    ...state,
    selectedExternalAssetLoading: {
      loading: false,
      error: payload.error,
    },
  };
}

export function updateSelectedExternalAsset(
  state: IIssueSnapshotState,
  payload: { externalAsset: ExternalAsset }
) {
  return {
    ...state,
    selectedExternalAsset: payload.externalAsset,
  };
}

//WorkRequestInfo
export function pollWorkRequestInfo(
  state: IIssueSnapshotState,
  payload: { issueId: string }
) {
  return {
    ...state,
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      polling: true,
    },
  };
}

export function pollWorkRequestInfoSuccess(
  state: IIssueSnapshotState,
  payload: { workRequestInfo: IWorkRequest }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
      ...payload.workRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      polling: false,
      pollingError: null,
    },
  };
}

export function pollWorkRequestInfoFailure(
  state: IIssueSnapshotState,
  payload: { error: Error }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      polling: false,
      pollingError: payload.error,
    },
  };
}

export function saveWorkRequest(state: IIssueSnapshotState) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      swrLoading: true,
      swrError: null,
    },
  };
}

export function saveWorkRequestSuccess(state: IIssueSnapshotState) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      swrLoading: false,
    },
  };
}

export function saveWorkRequestFailure(
  state: IIssueSnapshotState,
  payload: { error: Error }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      swrLoading: false,
      swrError: new Error('Unable to save work request'),
    },
  };
}

export function loadWorkRequestInfo(
  state: IIssueSnapshotState,
  payload: { issueId: string }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      loading: true,
    },
  };
}

export function loadWorkRequestInfoSuccess(
  state: IIssueSnapshotState,
  payload: { workRequestInfo: IWorkRequest }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
      ...payload.workRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      loading: false,
      error: null,
    },
  };
}

export function loadWorkRequestInfoFailure(
  state: IIssueSnapshotState,
  payload: { error: Error }
) {
  return {
    ...state,
    WorkRequestInfo: {
      ...state.WorkRequestInfo,
    },
    WorkRequestLoading: {
      ...state.WorkRequestLoading,
      loading: false,
      error: payload.error,
    },
  };
}

// Discussion
export function loadDiscussion(
  state: IIssueSnapshotState,
  payload: { showAutogenEntries: boolean }
) {
  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: null,
      discussionEntry: {
        ...state.DiscussionState.discussionEntry,
        saving: false,
      },
    },
  };
}

export function loadDiscussionSuccess(
  state: IIssueSnapshotState,
  payload: { discussion: IDiscussion }
) {
  const discussion = processDiscussion([payload.discussion]);
  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion,
    },
  };
}

export function loadDiscussionFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function clearState(state: IIssueSnapshotState) {
  return {
    ...state,
    DiscussionState: getDefaultDiscussion(),
    SummaryState: getDefaultSummary(),
  };
}

export function showMoreEntries(state: IIssueSnapshotState) {
  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: getMoreEntries({ ...state.DiscussionState.discussion }),
    },
  };
}

export function getImageAttachmentsSuccess(
  state: IIssueSnapshotState,
  payload: { imageDetails: IImageDetails[] }
) {
  let entries = state.DiscussionState?.discussion?.Entries
    ? // eslint-disable-next-line no-unsafe-optional-chaining
      [...state.DiscussionState?.discussion?.Entries]
    : null;
  if (
    !isNil(entries) &&
    entries.length > 0 &&
    payload.imageDetails.length > 0
  ) {
    entries = entries.map((entry) => {
      const ent = { ...entry };
      if (ent.Attachments.length > 0) {
        ent.Attachments = ent.Attachments.map((attachment) => {
          const attach = { ...attachment };
          if (attachment.DiscussionAttachmentType === 1) {
            const details = payload.imageDetails.find(
              (data) => data.ContentID === attachment.ContentID
            );
            if (details) {
              attach.path = details.Url;
              attach.blobURL = details.BlobUrl;
            }
          }
          return attach;
        });
        ent.newAttachments = ent.Attachments;
      }
      return ent;
    });
  }

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
    },
  };
}

export function getImageAttachmentsFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function getNewDiscussionEntry(state: IIssueSnapshotState) {
  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussionEntry: null,
    },
  };
}

export function getNewDiscussionEntrySuccess(
  state: IIssueSnapshotState,
  payload: { discussionEntry: IDiscussionEntry }
) {
  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussionEntry: payload.discussionEntry,
    },
  };
}

export function getNewDiscussionEntryFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function saveDiscussionEntry(
  state: IIssueSnapshotState,
  payload: { saveDetails: ISaveDiscussionEntry }
) {
  const entries = [...state.DiscussionState.discussion.Entries];
  let entry = { ...state.DiscussionState.discussionEntry };

  // check if entry exist in the current discussion
  const entryIndex = entries.findIndex(
    (entry) =>
      entry.DiscussionEntryID === payload.saveDetails.Entry.DiscussionEntryID
  );

  if (entryIndex >= 0) {
    entries.splice(entryIndex, 1, payload.saveDetails.Entry);
  } else {
    entry = payload.saveDetails.Entry;
  }

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
      discussionEntry: entry,
    },
  };
}

export function saveDiscussionEntrySuccess(
  state: IIssueSnapshotState,
  payload: { savedDiscussionEntry: IDiscussionEntry }
) {
  let entries = [...state.DiscussionState.discussion.Entries];
  const savedDiscussionEntry = processEntry(payload.savedDiscussionEntry);
  savedDiscussionEntry.showConfigButtons = true;

  // check if entry exist in the current discussion
  const entryIndex = entries.findIndex(
    (entry) =>
      entry.DiscussionEntryID === savedDiscussionEntry.DiscussionEntryID
  );
  if (entryIndex >= 0) {
    entries.splice(entryIndex, 1, savedDiscussionEntry);
  } else {
    entries = [savedDiscussionEntry, ...entries];
  }

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
      discussionEntry: savedDiscussionEntry,
    },
  };
}

export function saveDiscussionEntryFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  const entries = [...state.DiscussionState.discussion.Entries];
  const entry = { ...state.DiscussionState.discussionEntry };

  entry.saving = false;
  entries
    .filter((e) => e.saving === true)
    .map((entry) => {
      entry.saving = false;
    });

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
      discussionEntry: entry,
    },
  };
}

export function cancelDiscussionEntryEdits(
  state: IIssueSnapshotState,
  payload: { discussionEntryID: string }
) {
  const entries = [...state.DiscussionState.discussion.Entries];
  let newEntry: IDiscussionEntry;
  const entryIndex = entries.findIndex((entry) => {
    if (entry.DiscussionEntryID === payload.discussionEntryID) {
      newEntry = { ...entry };
      newEntry.editing = false;
      newEntry.attachmentsToDelete = [];
      newEntry.newAttachments = [];
      newEntry.newKeywords = [];
      return entry;
    }
  });
  entries.splice(entryIndex, 1, newEntry);

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
    },
  };
}

export function editDiscussionEntryValues(
  state: IIssueSnapshotState,
  payload: {
    discussions: IDiscussion[];
    discussionEntry: IDiscussionEntry;
  }
) {
  const entries = [...state.DiscussionState.discussion.Entries];
  let newEntry: IDiscussionEntry;
  const entryIndex = entries.findIndex((entry) => {
    if (entry.DiscussionEntryID === payload.discussionEntry.DiscussionEntryID) {
      newEntry = { ...entry };
      newEntry.editing = true;
      newEntry.attachmentsToDelete = [];
      newEntry.newAttachments = entry.Attachments;
      newEntry.newKeywords = entry.Keywords;
      return entry;
    }
  });
  entries.splice(entryIndex, 1, newEntry);

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
    },
  };
}

export function deleteDiscussionEntrySuccess(
  state: IIssueSnapshotState,
  payload: { discussionEntryID: string }
) {
  const entries = [...state.DiscussionState.discussion.Entries];
  const entryIndex = entries.findIndex(
    (entry) => entry.DiscussionEntryID === payload.discussionEntryID
  );
  entries.splice(entryIndex, 1);

  return {
    ...state,
    DiscussionState: {
      ...state.DiscussionState,
      discussion: {
        ...state.DiscussionState.discussion,
        Entries: entries,
      },
    },
  };
}

export function deleteDiscussionEntryFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function addDiscussionEntryAttachments(
  state: IIssueSnapshotState,
  payload: {
    attachmentDetails: IAddDiscussionEntryAttachment[];
  }
) {
  let newState = { ...state };
  if (payload.attachmentDetails[0].EntryID === EDiscussion.NewID) {
    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...newState.DiscussionState.discussionEntry,
          uploadingAttachments: true,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (entry.DiscussionEntryID === payload.attachmentDetails[0].EntryID) {
        newEntry = { ...entry };
        newEntry.uploadingAttachments = true;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function addDiscussionEntryAttachmentsSuccess(
  state: IIssueSnapshotState,
  payload: {
    attachments: IDiscussionAttachment[];
    discussionEntryID: string;
  }
) {
  let newState = { ...state };
  if (payload.discussionEntryID === EDiscussion.NewID) {
    const attach = [...newState.DiscussionState.discussionEntry.newAttachments];
    payload.attachments.map((att) => attach.push(att));

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...state.DiscussionState.discussionEntry,
          newAttachments: attach,
          uploadingAttachments: false,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (entry.DiscussionEntryID === payload.discussionEntryID) {
        newEntry = { ...entry };
        const attach = [...newEntry.newAttachments];
        payload.attachments.map((att) => attach.push(att));

        newEntry.newAttachments = attach;
        newEntry.uploadingAttachments = false;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function addDiscussionEntryAttachmentsFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function searchTaggingKeyword(
  state: IIssueSnapshotState,
  payload: { searchDetails: IDiscussionEntryKeyword }
) {
  let newState = { ...state };
  if (
    payload.searchDetails.DiscussionEntry.DiscussionEntryID ===
    EDiscussion.NewID
  ) {
    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...newState.DiscussionState.discussionEntry,
          autoCompleteLoading:
            payload.searchDetails.Keyword !== null &&
            payload.searchDetails.Keyword !== '',
          autoCompleteKeywords: null,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (
        entry.DiscussionEntryID ===
        payload.searchDetails.DiscussionEntry.DiscussionEntryID
      ) {
        newEntry = { ...entry };
        newEntry.autoCompleteLoading =
          payload.searchDetails.Keyword !== null &&
          payload.searchDetails.Keyword !== '';
        newEntry.autoCompleteKeywords = null;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function searchTaggingKeywordSuccess(
  state: IIssueSnapshotState,
  payload: { discussionEntryID: string; keywords: ITaggingKeyword[] }
) {
  let newState = { ...state };
  if (payload.discussionEntryID === EDiscussion.NewID) {
    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...newState.DiscussionState.discussionEntry,
          autoCompleteLoading: false,
          autoCompleteKeywords: payload.keywords,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (entry.DiscussionEntryID === payload.discussionEntryID) {
        newEntry = { ...entry };
        newEntry.autoCompleteLoading = false;
        newEntry.autoCompleteKeywords = payload.keywords;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function searchTaggingKeywordFailure(
  state: IIssueSnapshotState,
  payload: { discussionEntryID: string }
) {
  let newState = { ...state };
  if (payload.discussionEntryID === EDiscussion.NewID) {
    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...newState.DiscussionState.discussionEntry,
          autoCompleteLoading: false,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (entry.DiscussionEntryID === payload.discussionEntryID) {
        newEntry = { ...entry };
        newEntry.autoCompleteLoading = false;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function registerKeywordFailure(
  state: IIssueSnapshotState,
  payload: Error
) {
  return { ...state };
}

export function updateDiscussionEntryKeywords(
  state: IIssueSnapshotState,
  payload: { discussionEntryID: string; keyword: ITaggingKeyword }
) {
  let newState = { ...state };
  if (payload.discussionEntryID === EDiscussion.NewID) {
    const keywords = [...newState.DiscussionState.discussionEntry.newKeywords];
    keywords.push(payload.keyword);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...state.DiscussionState.discussionEntry,
          newKeywords: keywords,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (entry.DiscussionEntryID === payload.discussionEntryID) {
        newEntry = { ...entry };
        const keywords = [...newEntry.newKeywords];
        keywords.push(payload.keyword);

        newEntry.newKeywords = keywords;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function deleteDiscussionEntryAttachment(
  state: IIssueSnapshotState,
  payload: { attachmentDetails: IDeleteDiscussionEntryAttachment }
) {
  let newState = { ...state };
  if (payload.attachmentDetails.DiscussionEntryID === EDiscussion.NewID) {
    const newAttachments = [
      ...newState.DiscussionState.discussionEntry.newAttachments,
    ];
    const idx = newAttachments.findIndex(
      (attach) =>
        attach.ContentID === payload.attachmentDetails.DiscussionContentID
    );
    if (idx >= 0) {
      newAttachments.splice(idx, 1);
    }

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...state.DiscussionState.discussionEntry,
          newAttachments,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (
        entry.DiscussionEntryID === payload.attachmentDetails.DiscussionEntryID
      ) {
        newEntry = { ...entry };

        const attachmentsToDelete = [...newEntry.attachmentsToDelete] || [];
        if (
          payload.attachmentDetails.DiscussionAttachmentID !== EDiscussion.NewID
        ) {
          attachmentsToDelete.push(
            payload.attachmentDetails.DiscussionAttachmentID
          );
        }

        const newAttachments = [...newEntry.newAttachments];
        const idx = newAttachments.findIndex(
          (attach) =>
            attach.ContentID === payload.attachmentDetails.DiscussionContentID
        );
        if (idx >= 0) {
          newAttachments.splice(idx, 1);
        }

        newEntry.attachmentsToDelete = attachmentsToDelete;
        newEntry.newAttachments = newAttachments;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

export function deleteDiscussionEntryKeyword(
  state: IIssueSnapshotState,
  payload: { keywordDetails: IDeleteDiscussionEntryKeyword }
) {
  let newState = { ...state };
  if (payload.keywordDetails.DiscussionEntryID === EDiscussion.NewID) {
    const newKeywords = [
      ...newState.DiscussionState.discussionEntry.newKeywords,
    ];
    const idx = newKeywords.findIndex(
      (key) => key.KeywordId === payload.keywordDetails.DiscussionKeywordID
    );
    if (idx >= 0) {
      newKeywords.splice(idx, 1);
    }

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussionEntry: {
          ...state.DiscussionState.discussionEntry,
          newKeywords,
        },
      },
    };
  } else {
    const entries = [...state.DiscussionState.discussion.Entries];
    let newEntry: IDiscussionEntry;
    const entryIndex = entries.findIndex((entry) => {
      if (
        entry.DiscussionEntryID === payload.keywordDetails.DiscussionEntryID
      ) {
        newEntry = { ...entry };

        const newKeywords = [...newEntry.newKeywords];
        const idx = newKeywords.findIndex(
          (key) => key.KeywordId === payload.keywordDetails.DiscussionKeywordID
        );
        if (idx >= 0) {
          newKeywords.splice(idx, 1);
        }

        newEntry.newKeywords = newKeywords;
        return entry;
      }
    });
    entries.splice(entryIndex, 1, newEntry);

    newState = {
      ...newState,
      DiscussionState: {
        ...state.DiscussionState,
        discussion: {
          ...newState.DiscussionState.discussion,
          Entries: entries,
        },
      },
    };
  }

  return {
    ...newState,
  };
}

// Summary
export function loadSummary(
  state: IIssueSnapshotState,
  payload: { issueGuid: string; issueSummary: string }
) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      IssueGuid: payload.issueGuid,
      IssueSummary: payload.issueSummary,
      EditingSummary: false,
    },
  };
}

export function convertBVFilesToHTMLSuccess(
  state: IIssueSnapshotState,
  payload: { issueSummaryWithFiles: string }
) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      IssueSummary: payload.issueSummaryWithFiles,
    },
  };
}

export function editSummary(state: IIssueSnapshotState) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      EditingSummary: true,
    },
  };
}

export function saveSummary(
  state: IIssueSnapshotState,
  payload: { summaryContent: string }
) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      saving: true,
    },
  };
}

export function saveSummarySuccess(
  state: IIssueSnapshotState,
  payload: { summaryContent: string }
) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      IssueSummary: payload.summaryContent,
      EditingSummary: false,
      saving: false,
    },
  };
}

export function cancelEditSummary(state: IIssueSnapshotState) {
  return {
    ...state,
    SummaryState: {
      ...state.SummaryState,
      EditingSummary: false,
      saving: false,
    },
  };
}

export function assetTreeDropdownStateChange(
  state: IIssueSnapshotState,
  payload: ITreeStateChange
) {
  return {
    ...state,
    AssetTreeDropdownState: alterAssetTreeState(
      state.AssetTreeDropdownState,
      payload
    ),
  };
}

export function clearAssetTreeDropdown(state: IIssueSnapshotState) {
  return {
    ...state,
    AssetTreeDropdownState: {
      ...state.AssetTreeDropdownState,
      treeConfiguration: getDefaultTree({ collapseOthers: false }),
      treeNodes: createTreeBuilder(),
      hasDefaultSelectedAsset: false,
    },
  };
}

export function issueSnapshotReducer(state, action) {
  return _issueSnapshotReducer(state, action);
}
