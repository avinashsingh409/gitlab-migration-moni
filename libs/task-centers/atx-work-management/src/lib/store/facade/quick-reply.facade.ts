/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as quickReplyActions from '../actions/quick-reply.actions';
import { selectAttachments } from '../selectors/quick-reply.selector';
import { selectQuickReplyState } from '../selectors/work-management.selector';

@Injectable()
export class QuickReplyFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  selectQuickReplyState$ = this.store.select(selectQuickReplyState);
  selectQuickReplyAttachements$ = this.store.select(selectAttachments);

  initializeQuickReply() {
    this.store.dispatch(quickReplyActions.initializeQuickReply());
  }

  addQuickReplyAttachments(files: any[], assetID: number) {
    this.store.dispatch(
      quickReplyActions.addQuickReplyAttachments({ files, assetID })
    );
  }

  removeQuickReplyAttachment(index: number) {
    this.store.dispatch(
      quickReplyActions.removeQuickReplyAttachment({ index })
    );
  }

  clearQuickReplyAttachments() {
    this.store.dispatch(quickReplyActions.clearQuickReplyAttachments());
  }
}
