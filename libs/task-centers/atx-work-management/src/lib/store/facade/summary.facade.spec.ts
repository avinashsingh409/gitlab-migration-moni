import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { TestBed, inject, ComponentFixture } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import * as actions from '../actions/summary.actions';
import { SummaryFacade } from './summary.facade';
import { IButtonData } from '@atonix/atx-navigation';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { IListState, ISavedIssueList } from '@atonix/shared/api';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import {
  BrowserAnimationsModule,
  NoopAnimationsModule,
} from '@angular/platform-browser/animations';

describe('SummaryFacade', () => {
  let store: MockStore<any>;
  const initialState = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule],
      providers: [
        provideMockStore({ initialState }),
        SummaryFacade,
        NoopAnimationsModule,
      ],
    });

    store = TestBed.inject<any>(Store);
  });

  it('should dispatch an action that will initialize summary', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.initializeSummary('1234');
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.summaryInitialize({ asset: '1234' })
      );
    }
  ));

  it('should dispatch an action that will select work item', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.selectWorkItem('1234');
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.workItemSelect({ id: '1234' })
      );
    }
  ));

  it('should dispatch an action that will add work item', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.addWorkItem();
      expect(store.dispatch).toHaveBeenCalledWith(actions.workItemAdd());
    }
  ));

  it('should dispatch an action that will change tree size', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.treeSizeChange(123);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.treeSizeChange({ value: 123 })
      );
    }
  ));

  it('should dispatch an action that will change tree state', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const mockChange: ITreeStateChange = {
        event: 'GetPermissions',
        newValue: null,
      };

      summaryFacade.treeStateChange(mockChange);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.treeStateChange(mockChange)
      );
    }
  ));

  it('should dispatch an action that will change tree state', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const mockChange: ITreeStateChange = {
        event: 'GetPermissions',
        newValue: null,
      };

      summaryFacade.treeStateChange(mockChange);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.treeStateChange(mockChange)
      );
    }
  ));

  it('should dispatch an action that will select asset', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.selectAsset('1234');
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.selectAsset({ asset: '1234' })
      );
    }
  ));

  it('should dispatch an action that will update donut chart', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const state: IServerSideGetRowsRequest = {
        startRow: 1,
        endRow: 100,
        rowGroupCols: null,
        valueCols: null,
        pivotCols: null,
        pivotMode: false,
        groupKeys: null,
        filterModel: null,
        sortModel: null,
      };

      summaryFacade.updateDonutChart(state);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.donutsRequest({ state })
      );
    }
  ));

  it('should dispatch an action that will update left donut chart', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.leftDonutOthersSelected(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.leftDonutOthersSelected({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will update middle donut chart', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.middleDonutOthersSelected(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.middleDonutOthersSelected({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will update right donut chart', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.rightDonutOthersSelected(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.rightDonutOthersSelected({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will refresh left donut', inject(
    [SummaryFacade],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.leftDonutRefreshed(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.leftDonutRefreshed({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will refresh middle donut', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.middleDonutRefreshed(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.middleDonutRefreshed({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will refresh right donut', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const IDtoAppend = 'abcde';

      summaryFacade.rightDonutRefreshed(IDtoAppend);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.rightDonutRefreshed({ IDtoAppend })
      );
    }
  ));

  it('should dispatch an action that will refresh right donut', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const mockListState: IListState = {
        columns: null,
        sort: [],
        groups: [],
        filter: null,
      };

      summaryFacade.listStateChanged(mockListState);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.listStateChange({ listState: mockListState })
      );
    }
  ));

  it('should dispatch an action that will load lists', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.loadLists();
      expect(store.dispatch).toHaveBeenCalledWith(actions.listsLoad());
    }
  ));

  it('should dispatch an action that will save list', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      const mockSaveList: ISavedIssueList = {
        id: 123,
        name: 'test',
        state: null,
        checked: false,
      };

      summaryFacade.saveList(mockSaveList);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.listSave({ list: mockSaveList })
      );
    }
  ));

  it('should dispatch an action that will load list', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.loadList(246);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.listLoad({ id: 246 })
      );
    }
  ));

  it('should dispatch an action that will delete list', inject(
    [SummaryFacade, NoopAnimationsModule],
    (summaryFacade: SummaryFacade) => {
      jest.spyOn(store, 'dispatch');

      summaryFacade.deleteList(369);
      expect(store.dispatch).toHaveBeenCalledWith(
        actions.listDelete({ id: 369 })
      );
    }
  ));
});
