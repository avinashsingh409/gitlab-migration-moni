/* eslint-disable ngrx/avoid-dispatching-multiple-actions-sequentially */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectAssetGuid,
  selectAssetId,
  selectAssetName,
  selectAssetTreeDropdownConfiguration,
  selectAssetUniqueKey,
  selectDiscussionEntry,
  selectDiscussionState,
  selectExternalAssets,
  selectExternalAssetsLoading,
  selectIssueSummaryContent,
  selectIssueSummaryState,
  selectSelectedExternalAsset,
  selectSelectedExternalAssetLoading,
  selectWorkRequestInfo,
  selectWorkRequestLoading,
} from '../selectors/issue-snapshot.selector';
import * as issueSnapshotActions from '../actions/issue-snapshot.actions';
import {
  EDiscussion,
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '@atonix/atx-discussion';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import {
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  ISaveDiscussionEntry,
} from '@atonix/atx-core';
import { selectIssueSnapshotState } from '../selectors/work-management.selector';
import { selectTheme } from '@atonix/atx-navigation';
import { ExternalAsset } from '@atonix/shared/api';

@Injectable()
export class IssueSnapshotFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  selectIssueSnapshotState$ = this.store.select(selectIssueSnapshotState);
  selectDiscussionState$ = this.store.select(selectDiscussionState);
  selectDiscussionEntry$ = this.store.select(selectDiscussionEntry);
  selectIssueSummaryState$ = this.store.select(selectIssueSummaryState);
  selectIssueSummaryContent$ = this.store.select(selectIssueSummaryContent);
  selectAssetTreeDropdownConfig$ = this.store.select(
    selectAssetTreeDropdownConfiguration
  );
  selectAssetName$ = this.store.select(selectAssetName);
  selectAssetId$ = this.store.select(selectAssetId);
  selectAssetGuid$ = this.store.select(selectAssetGuid);
  assetUniqueKey$ = this.store.select(selectAssetUniqueKey);
  selectWorkRequestInfo$ = this.store.select(selectWorkRequestInfo);
  theme$ = this.store.select(selectTheme);
  workRequestLoading$ = this.store.select(selectWorkRequestLoading);
  selectedExternalAsset$ = this.store.select(selectSelectedExternalAsset);
  selectedExternalAssetLoading$ = this.store.select(
    selectSelectedExternalAssetLoading
  );
  externalAssets$ = this.store.select(selectExternalAssets);
  externalAssetsLoading$ = this.store.select(selectExternalAssetsLoading);

  getExternalAssets(assetGuid: string) {
    this.store.dispatch(issueSnapshotActions.getExternalAssets({ assetGuid }));
  }

  getAssociatedExternalAsset(assetGuid: string) {
    this.store.dispatch(
      issueSnapshotActions.getAssociatedExternalAsset({ assetGuid })
    );
  }

  clearExternalAssets() {
    this.store.dispatch(issueSnapshotActions.clearExternalAssets());
  }

  updateSelectedExternalAsset(externalAsset: ExternalAsset) {
    this.store.dispatch(
      issueSnapshotActions.updateSelectedExternalAsset({ externalAsset })
    );
  }

  saveWorkRequest(
    issueId: string,
    title: string,
    description: string,
    externalSystemAssetId: number,
    priority: string
  ) {
    this.store.dispatch(
      issueSnapshotActions.saveWorkRequest({
        issueId,
        title,
        description,
        externalSystemAssetId,
        priority,
      })
    );
  }

  loadWorkRequestInfo(issueId: string, forceReload: boolean = false) {
    this.store.dispatch(
      issueSnapshotActions.loadWorkRequestInfo({ issueId, forceReload })
    );
  }

  loadNewDiscussionEntry(
    assetIssueID: number,
    showConfigButtonsByUser?: boolean
  ) {
    this.store.dispatch(issueSnapshotActions.getNewDiscussionEntry());
    this.store.dispatch(
      issueSnapshotActions.loadDiscussion({
        assetIssueID,
        showAutogenEntries: false,
        showConfigButtonsByUser: showConfigButtonsByUser,
      })
    );
  }

  // Dicussion
  loadDiscussion(
    assetIssueID: number,
    showAutogenEntries: boolean,
    showConfigButtonsByUser?: boolean
  ) {
    this.store.dispatch(
      issueSnapshotActions.loadDiscussion({
        assetIssueID,
        showAutogenEntries,
        showConfigButtonsByUser: showConfigButtonsByUser,
      })
    );
  }

  clearDiscusson() {
    this.store.dispatch(issueSnapshotActions.clearState());
  }

  deleteDiscussionEntryAttachment(
    attachmentDetails: IDeleteDiscussionEntryAttachment
  ) {
    this.store.dispatch(
      issueSnapshotActions.deleteDiscussionEntryAttachment({
        attachmentDetails,
      })
    );
  }

  deleteDiscussionEntryKeyword(keywordDetails: IDeleteDiscussionEntryKeyword) {
    this.store.dispatch(
      issueSnapshotActions.deleteDiscussionEntryKeyword({ keywordDetails })
    );
  }

  saveDiscussionEntry(saveDetails: ISaveDiscussionEntry) {
    this.store.dispatch(
      issueSnapshotActions.saveDiscussionEntry({ saveDetails })
    );
  }

  saveMobileDiscussionEntry(discussionEntry: IDiscussionEntry) {
    this.store.dispatch(
      issueSnapshotActions.saveMobileDiscussionEntry({ entry: discussionEntry })
    );
  }

  cancelDiscussionEntry(discussionEntryID: string) {
    if (discussionEntryID === EDiscussion.NewID) {
      this.store.dispatch(issueSnapshotActions.getNewDiscussionEntry());
    } else {
      this.store.dispatch(
        issueSnapshotActions.cancelDiscussionEntryEdits({ discussionEntryID })
      );
    }
  }

  editDiscussionEntry(discussionEntry: IDiscussionEntry) {
    this.store.dispatch(
      issueSnapshotActions.editDiscussionEntry({ discussionEntry })
    );
  }

  deleteDiscussionEntry(discussionEntryID: string) {
    this.store.dispatch(
      issueSnapshotActions.deleteDiscussionEntry({ discussionEntryID })
    );
  }

  addDiscussionEntryAttachments(
    attachmentDetails: IAddDiscussionEntryAttachment[]
  ) {
    this.store.dispatch(
      // Validate file names before attaching any files
      issueSnapshotActions.validateDiscussionEntryAttachments({
        attachmentDetails,
      })
    );
  }

  searchTaggingKeyword(searchDetails: IDiscussionEntryKeyword) {
    this.store.dispatch(
      issueSnapshotActions.searchTaggingKeyword({ searchDetails })
    );
  }

  addDiscussionEntryKeyword(addDetails: IDiscussionEntryKeyword) {
    this.store.dispatch(
      issueSnapshotActions.addDiscussionEntryKeyword({ addDetails })
    );
  }

  showMoreEntries() {
    this.store.dispatch(issueSnapshotActions.showMoreEntries());
    this.store.dispatch(issueSnapshotActions.getImageAttachments());
  }

  // Summary
  loadSummary(issueGuid: string, issueSummary: string) {
    this.store.dispatch(
      issueSnapshotActions.loadSummary({ issueGuid, issueSummary })
    );
    this.store.dispatch(
      issueSnapshotActions.convertBVFilesToHTML({ issueSummary })
    );
  }

  uploadSummaryFile(assetID: number, file: any) {
    this.store.dispatch(
      // Validate file name before uploading file
      issueSnapshotActions.validateSummaryFile({ assetID, file })
    );
  }

  editSummary() {
    this.store.dispatch(issueSnapshotActions.editSummary());
  }

  saveSummary(summaryContent: string) {
    this.store.dispatch(issueSnapshotActions.saveSummary({ summaryContent }));
  }

  cancelEditSummary() {
    this.store.dispatch(issueSnapshotActions.cancelEditSummary());
  }

  assetTreeDropdownStateChange(change: ITreeStateChange) {
    this.store.dispatch(
      issueSnapshotActions.assetTreeDropdownStateChange(change)
    );
  }

  showRelatedIssuesByAsset(assetUniqueKey: string) {
    this.store.dispatch(
      issueSnapshotActions.showRelatedIssuesByAsset({
        assetUniqueKey: assetUniqueKey,
      })
    );
  }

  showRelatedAlertsByAsset(assetUniqueKey: string) {
    this.store.dispatch(
      issueSnapshotActions.showRelatedAlertsByAsset({
        assetUniqueKey: assetUniqueKey,
      })
    );
  }

  clearAssetTreeDropdown() {
    this.store.dispatch(issueSnapshotActions.clearAssetTreeDropdown());
  }
}
