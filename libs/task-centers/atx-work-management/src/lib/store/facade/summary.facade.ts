/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IButtonData } from '@atonix/atx-navigation';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import {
  getSelectedAsset,
  getAssetTreeConfiguration,
  getLeftDonutState,
  getMiddleDonutState,
  getRightDonutState,
  getListState,
  getSavedLists,
  selectFloatingFilter,
  leftTraySize,
  selectLeftTrayMode,
} from '../selectors/summary.selector';

import * as actions from '../actions/summary.actions';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import {
  IGridParameters,
  IListState,
  ISavedIssueList,
} from '@atonix/shared/api';

@Injectable()
export class SummaryFacade implements OnDestroy {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  unsubscribe$ = new Subject<void>();
  selectedAsset$ = this.store.pipe(select(getSelectedAsset));

  // Configuration of the asset tree
  assetTreeConfiguration$ = this.store.pipe(select(getAssetTreeConfiguration));

  // Donut Chart Configuration
  leftDonutState$ = this.store.pipe(select(getLeftDonutState));
  middleDonutState$ = this.store.pipe(select(getMiddleDonutState));
  rightDonutState$ = this.store.pipe(select(getRightDonutState));

  savedLists$ = this.store.pipe(select(getSavedLists));

  savedListState$ = this.store.pipe(select(getListState));

  leftTrayMode$ = this.store.pipe(select(selectLeftTrayMode));
  leftTraySize$ = this.store.pipe(select(leftTraySize));
  floatingFilter$ = this.store.pipe(select(selectFloatingFilter));

  ngOnDestroy() {
    this.unsubscribe$.next();
  }

  // This sets up the summary tab
  initializeSummary(asset?: string) {
    this.store.dispatch(actions.summaryInitialize({ asset }));
  }

  // This will open a new window with the work item selected.
  selectWorkItem(id: string) {
    this.store.dispatch(actions.workItemSelect({ id }));
  }

  // This will open a new window for the snapshot page.
  addWorkItem() {
    this.store.dispatch(actions.workItemAdd());
  }

  treeSizeChange(value: number) {
    this.store.dispatch(actions.treeSizeChange({ value }));
  }

  selectAsset(asset: string) {
    this.store.dispatch(actions.selectAsset({ asset }));
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(actions.treeStateChange(change));
  }

  updateDonutChart(state: IServerSideGetRowsRequest) {
    this.store.dispatch(actions.donutsRequest({ state }));
  }

  leftDonutOthersSelected(IDtoAppend: string) {
    this.store.dispatch(actions.leftDonutOthersSelected({ IDtoAppend }));
  }

  middleDonutOthersSelected(IDtoAppend: string) {
    this.store.dispatch(actions.middleDonutOthersSelected({ IDtoAppend }));
  }

  rightDonutOthersSelected(IDtoAppend: string) {
    this.store.dispatch(actions.rightDonutOthersSelected({ IDtoAppend }));
  }

  leftDonutRefreshed(IDtoAppend: string) {
    this.store.dispatch(actions.leftDonutRefreshed({ IDtoAppend }));
  }

  middleDonutRefreshed(IDtoAppend: string) {
    this.store.dispatch(actions.middleDonutRefreshed({ IDtoAppend }));
  }

  rightDonutRefreshed(IDtoAppend: string) {
    this.store.dispatch(actions.rightDonutRefreshed({ IDtoAppend }));
  }

  listStateChanged(listState: IListState) {
    this.store.dispatch(actions.listStateChange({ listState }));
  }

  loadLists() {
    this.store.dispatch(actions.listsLoad());
  }

  saveList(list: Partial<ISavedIssueList>) {
    this.store.dispatch(actions.listSave({ list }));
  }

  loadList(id: number) {
    this.store.dispatch(actions.listLoad({ id }));
  }

  deleteList(id: number) {
    this.store.dispatch(actions.listDelete({ id }));
  }
  listViewDownload(gridParams: IGridParameters) {
    this.store.dispatch(actions.listViewDownloadRequest({ gridParams }));
  }
  loadDefaultList() {
    this.store.dispatch(actions.loadDefaultList());
  }
  toggleFloatingFilter(filterOn?: boolean) {
    this.store.dispatch(actions.toggleFloatingFilter({ filterOn }));
  }
  setScorecard(assetIssueID: number, include: boolean) {
    this.store.dispatch(actions.setScorecard({ assetIssueID, include }));
  }
}
