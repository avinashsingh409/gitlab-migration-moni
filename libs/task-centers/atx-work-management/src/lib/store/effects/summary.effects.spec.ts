import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, inject } from '@angular/core/testing';
import { Store, Action } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Subject, Observable } from 'rxjs';
import { hot, cold } from 'jest-marbles';
import {
  ModelService as AssetTreeModel,
  ITreeConfiguration,
  nodesRetrieved,
} from '@atonix/atx-asset-tree';
import { RouterTestingModule } from '@angular/router/testing';
import { initialWorkManagementState } from '../state/work-management-state';
import { SummaryEffects } from './summary.effects';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

import * as actions from '../actions/summary.actions';
import { LoggerService, ToastService } from '@atonix/shared/utils';
import { ITreePermissions } from '@atonix/atx-core';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { createMockWithValues } from '@testing-library/angular/jest-utils';

describe('IssuesSummaryEffects', () => {
  let actions$: Observable<Action>;
  let store: MockStore<any>;
  let effects: SummaryEffects;
  let mockAssetTreeModel: AssetTreeModel;
  let mockLoggerService: LoggerService;
  let mockToastService: ToastService;
  let mockAuthFrameworkService: AuthorizationFrameworkService;

  const myTreeConfiguration: ITreeConfiguration = {
    showTreeSelector: true,
    trees: [],
    selectedTree: '1',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    nodes: [],
    pin: false,
    loadingAssets: false,
    loadingTree: false,
    canDrag: false,
    dropTargets: null,
    canView: true,
    canAdd: true,
    canEdit: true,
    canDelete: true,
    collapseOthers: false,
    hideConfigureButton: false,
  };

  beforeEach(() => {
    const initialState: any = { workmanagement: initialWorkManagementState };
    actions$ = new Subject<Action>();

    mockAuthFrameworkService = createMockWithValues(
      AuthorizationFrameworkService,
      {
        getPermissions: jest.fn(),
      }
    );

    mockAssetTreeModel = createMockWithValues(AssetTreeModel, {
      getAssetTreeData: jest.fn(),
    });

    mockLoggerService = createMockWithValues(LoggerService, {
      savedFilter: jest.fn(),
      feature: jest.fn(),
    });

    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        provideMockActions(() => actions$),
        provideMockStore<any>({ initialState }),
        {
          provide: AuthorizationFrameworkService,
          useValue: mockAuthFrameworkService,
        },
        { provide: AssetTreeModel, useValue: mockAssetTreeModel },
        { provide: LoggerService, useValue: mockLoggerService },
        { provide: ToastService, useValue: mockToastService },
        { provide: APP_CONFIG, useValue: AppConfig },
        SummaryEffects,
      ],
    });

    store = TestBed.inject<any>(Store);
    effects = TestBed.inject<SummaryEffects>(SummaryEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should get asset info twice', () => {
    store.setState({
      workmanagement: {
        summaryState: {
          AssetTreeState: {
            treeConfiguration: myTreeConfiguration,
            treeNodes: null,
          },
        },
      },
    });
    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey1', []) }))
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    actions$ = hot('a-b', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });
    jest.spyOn(mockAssetTreeModel, 'getAssetData');
    const expected = hot('a-b', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get asset info overlap', () => {
    store.setState({
      workmanagement: {
        summaryState: {
          AssetTreeState: {
            treeConfiguration: myTreeConfiguration,
            treeNodes: null,
          },
        },
      },
    });

    mockAssetTreeModel.getAssetTreeData = jest
      .fn()
      .mockReturnValueOnce(
        cold('--a|', { a: nodesRetrieved('uniquekey1', []) })
      )
      .mockReturnValueOnce(cold('a|', { a: nodesRetrieved('uniquekey2', []) }));

    actions$ = hot('ab', {
      a: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey1',
      }),
      b: actions.treeStateChange({
        event: 'IconClicked',
        newValue: 'uniquekey2',
      }),
    });

    const expected = hot('-ba', {
      a: actions.treeStateChange(nodesRetrieved('uniquekey1', [])),
      b: actions.treeStateChange(nodesRetrieved('uniquekey2', [])),
    });
    expect(effects.treeStateChange$).toBeObservable(expected);
  });

  it('should get permissions', () => {
    const result1: ITreePermissions = {
      canAdd: true,
      canDelete: true,
      canEdit: true,
      canView: true,
    };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('a|', { a: result1 }))
      .mockReturnValueOnce(cold('a|', { a: result2 }));

    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestSuccess(result1),
      b: actions.permissionsRequestSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });

  it('should get permissions with error', () => {
    const error1: Error = { name: 'name', message: 'something' };
    const result2: ITreePermissions = {
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: false,
    };

    mockAuthFrameworkService.getPermissions = jest
      .fn()
      .mockReturnValueOnce(cold('#', undefined, error1))
      .mockReturnValueOnce(cold('a|', { a: result2 }));

    actions$ = hot('a-b', {
      a: actions.permissionsRequest(),
      b: actions.permissionsRequest(),
    });

    const expected = hot('a-b', {
      a: actions.permissionsRequestFailure(error1),
      b: actions.permissionsRequestSuccess(result2),
    });
    expect(effects.getPermissions$).toBeObservable(expected);
  });
});
