/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/no-dispatch-in-effects */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  withLatestFrom,
  map,
  filter,
  switchMap,
  catchError,
  tap,
  startWith,
  takeUntil,
  mergeMap,
} from 'rxjs/operators';
import { isNil, IAWSFileUrl, IDiscussionAttachment } from '@atonix/atx-core';
import { validateDiscussionEntry } from '@atonix/atx-discussion';
import * as actions from '../actions/issue-snapshot.actions';
import {
  selectAssetTreeDropdownState,
  selectCurrentUser,
  selectDiscussion,
  selectDiscussionEntries,
  selectDiscussions,
  selectIssueSummaryGuid,
  selectIssueSummaryState,
} from '../selectors/issue-snapshot.selector';
import { SafeStyle } from '@angular/platform-browser';
import { IIssueSnapshotState } from '../state/issue-snapshot-state';
import { Observable, Subject, forkJoin, interval, of, timer } from 'rxjs';
import {
  IssuesFrameworkService,
  ImagesFrameworkService,
  DiscussionsFrameworkService,
  AssetFrameworkService,
  IssuesCoreService,
} from '@atonix/shared/api';
import {
  replaceBVFilesToHTMLFileElement,
  createHTMLFileElement,
  replaceHTMLFileElementToBVFiles,
} from '../../service/utilities';
import { IFileInfo } from '@atonix/atx-core';
import {
  EventBusService,
  EmitEvent,
  Events,
} from '../../service/event-bus.service';
import { ToastService, catchSwitchMapError } from '@atonix/shared/utils';
import { ModelService as AssetTreeModel } from '@atonix/atx-asset-tree';
import { Router } from '@angular/router';
import { isValidFilenameForAttachment } from '@atonix/atx-core';

@Injectable()
export class IssueSnapshotEffects {
  constructor(
    private actions$: Actions,
    private discussionsFrameworkService: DiscussionsFrameworkService,
    private issuesFrameworkService: IssuesFrameworkService,
    private imagesFrameworkService: ImagesFrameworkService,
    private assetFrameworkService: AssetFrameworkService,
    private issueCoreService: IssuesCoreService,
    private toastService: ToastService,
    private store: Store<IIssueSnapshotState>,
    private eventbus: EventBusService,
    private assetTreeModel: AssetTreeModel,
    private router: Router
  ) {}

  getAssociatedExternalAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getAssociatedExternalAsset),
      switchMap((action) => {
        return this.issueCoreService.getAssociatedExternalAsset(
          action.assetGuid
        );
      }),
      switchMap((res) => {
        const externalAsset = res.Results[0];
        return [
          actions.getAssociatedExternalAssetSuccess({
            externalAsset: externalAsset,
          }),
        ];
      }),
      catchSwitchMapError((error) => {
        this.toastService.openSnackBar(
          'You do not have access to any external locations for this issue',
          'error'
        );
        return actions.getAssociatedExternalAssetFailure({ error });
      })
    )
  );

  getExternalAssets$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getExternalAssets),
      switchMap((action) => {
        return this.issueCoreService.getExternalAssets(action.assetGuid);
      }),
      switchMap((res) => {
        return [
          actions.getExternalAssetsSuccess({
            externalAssets: res.Results,
          }),
        ];
      }),
      catchSwitchMapError((error) => {
        return actions.getExternalAssetsFailure({ error });
      })
    )
  );

  private readonly pollingIntervalMs = 3000;
  private readonly maxPollingMs = 60000; // milliseconds
  private pollingUntil$: Subject<boolean> = new Subject<boolean>();

  pollWorkRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.pollWorkRequestInfo),
      mergeMap((action) =>
        interval(this.pollingIntervalMs).pipe(
          takeUntil(this.pollingUntil$),
          map(() => action),
          switchMap((action) => {
            return this.issueCoreService.getWorkRequest(action.issueId).pipe(
              tap((res) => {
                if (
                  res.workRequest.lastUpdatedAt.getTime() >
                  res.workRequest.lastQueriedAt.getTime()
                ) {
                  this.pollingUntil$.next(true);
                }
              })
            );
          }),
          takeUntil(
            timer(this.maxPollingMs).pipe(
              tap((timeOut) => {
                if (timeOut === 0) {
                  this.store.dispatch(
                    actions.pollWorkRequestInfoFailure({
                      error: new Error(
                        'Failed to load updated work request. Data may be out of date.'
                      ),
                    })
                  );
                }
              })
            )
          )
        )
      ),
      switchMap((res) => {
        if (
          res.workRequest.lastUpdatedAt.getTime() >
          res.workRequest.lastQueriedAt.getTime()
        ) {
          return [
            actions.pollWorkRequestInfoSuccess({
              workRequestInfo: res.workRequest,
            }),
          ];
        }
        return [];
      }),
      catchSwitchMapError((error) => {
        return actions.pollWorkRequestInfoFailure({
          error: new Error(
            'Failed to load updated work request. Data may be out of date.'
          ),
        });
      })
    )
  );

  saveWorkRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveWorkRequest),
      withLatestFrom(this.store.pipe(select(selectIssueSummaryState))),
      switchMap(([action, state]) => {
        return this.issueCoreService
          .createWorkRequest({
            title: action.title,
            description: action.description,
            assetIssueId: state.IssueGuid,
            externalSystemAssetId: action.externalSystemAssetId,
            priority: action.priority,
          })
          .pipe(
            map((res) => {
              return { res, issueGuid: state.IssueGuid };
            })
          );
      }),
      switchMap(({ res, issueGuid }) => {
        return [
          actions.saveWorkRequestSuccess(),
          actions.loadWorkRequestInfoSuccess({
            workRequestInfo: res.workRequest,
          }),
          actions.pollWorkRequestInfo({
            issueId: issueGuid,
          }),
        ];
      }),
      catchSwitchMapError(() =>
        actions.saveWorkRequestFailure({
          error: new Error('Unable to Save Work Request'),
        })
      )
    )
  );

  loadWorkRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadWorkRequestInfo),
      withLatestFrom(this.store.pipe(select(selectIssueSummaryState))),
      switchMap(([action, state]) => {
        return this.issueCoreService.getWorkRequest(
          action.issueId,
          action.forceReload
        );
      }),
      withLatestFrom(this.store.pipe(select(selectIssueSummaryGuid))),
      switchMap(([res, issueId]) => {
        const actionsList = [];
        actionsList.push(
          actions.loadWorkRequestInfoSuccess({
            workRequestInfo: res.workRequest,
          })
        );

        const lastQueriedAt = res.workRequest.lastQueriedAt;
        const lastUpdatedAt = res.workRequest.lastUpdatedAt;
        if (
          lastQueriedAt &&
          lastUpdatedAt &&
          lastQueriedAt.getTime() > lastUpdatedAt.getTime() &&
          lastQueriedAt.getTime() > Date.now() - 1000 * 60 * 1 //if lastQueried was updated in the last minute
        ) {
          actionsList.push(actions.pollWorkRequestInfo({ issueId }));
        }

        return actionsList;
      }),
      catchSwitchMapError(() => {
        return actions.loadWorkRequestInfoFailure({
          error: new Error('Unable to load Work Request'),
        });
      })
    )
  );

  loadDiscussion$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.loadDiscussion),
      withLatestFrom(this.store.pipe(select(selectCurrentUser))),
      map(([action, currentUser]) => {
        return {
          assetIssueID: action.assetIssueID,
          showAutogenEntries: action.showAutogenEntries,
          currentUser: currentUser.email,
          showConfigButtonsByUser: action.showConfigButtonsByUser,
        };
      }),
      switchMap((vals) =>
        forkJoin([
          this.discussionsFrameworkService
            .getDiscussionForAssetIssue(
              vals.assetIssueID,
              null,
              true,
              vals.showAutogenEntries
            )
            .pipe(
              map((discussion) => {
                const newEntries = [...discussion.Entries];
                newEntries.forEach((entry) => {
                  entry.showConfigButtons = true;
                });
                return {
                  ...discussion,
                  Entries: newEntries,
                };
              })
            ),
          of(vals.currentUser),
          of(vals.showConfigButtonsByUser),
        ])
      ),
      switchMap(([discussion, currentUser, showConfigButtonsByUser]) => {
        const filteredDiscussion = { ...discussion };
        if (!showConfigButtonsByUser) {
          filteredDiscussion.Entries.forEach((e) => {
            if (
              e.CreatedBy.UserName.toLocaleLowerCase() !==
              currentUser.toLocaleLowerCase()
            ) {
              e.showConfigButtons = false;
            }
          });
        }
        return [
          actions.loadDiscussionSuccess({ discussion: filteredDiscussion }),
          actions.getImageAttachments(),
        ];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(actions.loadDiscussionFailure(error));
      })
    )
  );

  getImageAttachments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getImageAttachments),
      withLatestFrom(this.store.pipe(select(selectDiscussionEntries))),
      map(([action, entries]) => {
        const attachments = [];
        entries.map((entry) => {
          if (entry.Attachments.length > 0) {
            entry.Attachments.map((attachment) => {
              if (
                attachment.DiscussionAttachmentType === 1 &&
                !attachment.blobURL
              )
                // Image Attachment
                attachments.push(attachment);
            });
          }
        });
        return attachments;
      }),
      filter((attachments) => !isNil(attachments) && attachments.length > 0),
      switchMap((attacments) =>
        forkJoin(
          attacments.map((attachment) =>
            this.imagesFrameworkService.getFileUrl(
              attachment.ContentID,
              '',
              false,
              undefined,
              500
            )
          )
        )
      ),
      switchMap((awsFileURLs) =>
        forkJoin(
          awsFileURLs.map((fileURL) =>
            this.imagesFrameworkService.convertToImageDetails(fileURL)
          )
        )
      ),
      switchMap((imageDetails) => {
        return [actions.getImageAttachmentsSuccess({ imageDetails })];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(actions.getImageAttachmentsFailure(error));
      })
    )
  );

  getNewDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.getNewDiscussionEntry),
      switchMap(() =>
        this.discussionsFrameworkService
          .createDiscussionEntry(null, null, null)
          .pipe(
            map(
              (discussionEntry) =>
                actions.getNewDiscussionEntrySuccess({ discussionEntry }),
              catchError((error) =>
                of(actions.getNewDiscussionEntryFailure(error))
              )
            )
          )
      )
    )
  );

  saveDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveDiscussionEntry),
      withLatestFrom(this.store.pipe(select(selectDiscussion))),
      map(([action, discussion]) => {
        const errorMessage = validateDiscussionEntry(
          action.saveDetails.Entry,
          action.saveDetails.UploadingAttachments
        );
        if (errorMessage) {
          this.toastService.openSnackBar(errorMessage, 'error');
          this.store.dispatch(
            actions.saveDiscussionEntryFailure({
              name: 'Validation Error',
              message: errorMessage,
            })
          );
        }

        const entry = { ...action.saveDetails.Entry };
        entry.DiscussionID = discussion.DiscussionID;

        const notifySubscribers = action.saveDetails.NotifySubscribers;
        return { entry, errorMessage, notifySubscribers };
      }),
      filter((vals) => isNil(vals.errorMessage)),
      switchMap((vals) =>
        forkJoin([
          this.discussionsFrameworkService.saveDiscussionEntryWithAttachments(
            vals.entry,
            vals.entry.attachmentsToDelete,
            false
          ),
          of(vals.notifySubscribers),
        ])
      ),
      switchMap(([savedDiscussionEntry, notifySubscribers]) => {
        this.toastService.openSnackBar('Discussion entry saved.', 'success');

        if (notifySubscribers) {
          return [
            actions.saveDiscussionEntrySuccess({ savedDiscussionEntry }),
            actions.getNewDiscussionEntry(),
            actions.getImageAttachments(),
            actions.notifyDiscussionSubscribers({
              discussionEntry: savedDiscussionEntry,
            }),
          ];
        } else {
          return [
            actions.saveDiscussionEntrySuccess({ savedDiscussionEntry }),
            actions.getNewDiscussionEntry(),
            actions.getImageAttachments(),
          ];
        }
      }),
      catchError((error) => {
        this.toastService.openSnackBar(
          'Error saving discussion entry.',
          'error'
        );
        console.log(JSON.stringify(error));
        return of(actions.saveDiscussionEntryFailure(error));
      })
    )
  );

  saveMobileDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveMobileDiscussionEntry),
      switchMap((action) =>
        this.discussionsFrameworkService.saveDiscussionEntryWithAttachments(
          action.entry,
          action.entry.attachmentsToDelete,
          false
        )
      ),
      switchMap((savedDiscussionEntry) => {
        this.toastService.openSnackBar('Discussion entry saved.', 'success');
        return of(
          actions.notifyDiscussionSubscribers({
            discussionEntry: savedDiscussionEntry,
          })
        );
      }),
      catchError((error) => {
        this.toastService.openSnackBar(
          'Error saving discussion entry.',
          'error'
        );
        console.log(JSON.stringify(error));
        return of(actions.saveDiscussionEntryFailure(error));
      })
    )
  );

  notifySubscribedUsers$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.notifyDiscussionSubscribers),
        map((action) => action.discussionEntry),
        switchMap((entry) =>
          this.discussionsFrameworkService.notifyDiscussionSubscribers(entry)
        )
      ),
    { dispatch: false }
  );

  editDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.editDiscussionEntry),
      withLatestFrom(this.store.pipe(select(selectDiscussions))),
      map(([action, discussions]) => {
        return { discussions, discussionEntry: action.discussionEntry };
      }),
      switchMap((vals) => [
        actions.editDiscussionEntryValues({
          discussions: vals.discussions,
          discussionEntry: vals.discussionEntry,
        }),
      ])
    )
  );

  deleteDiscussionEntry$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.deleteDiscussionEntry),
      switchMap((action) =>
        this.discussionsFrameworkService
          .deleteDiscussionEntry(action.discussionEntryID)
          .pipe(
            map(
              (isDeleted) =>
                actions.deleteDiscussionEntrySuccess({
                  discussionEntryID: action.discussionEntryID,
                }),
              catchError((error) =>
                of(actions.deleteDiscussionEntryFailure(error))
              )
            )
          )
      )
    )
  );

  validateDiscussionEntryAttachments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.validateDiscussionEntryAttachments),
      withLatestFrom(this.store.pipe(select(selectDiscussion))),
      switchMap(([action, discussion]) => {
        if (
          action.attachmentDetails.findIndex(
            (attachment) => !isValidFilenameForAttachment(attachment.Title)
          ) > -1
        ) {
          this.toastService.openSnackBar(
            'Upload failed, unsupported character in file name.',
            'warning'
          );
          return of(actions.validateDiscussionEntryAttachmentsFailure());
        } else {
          // Happy path
          return of(actions.addDiscussionEntryAttachments(action));
        }
      })
    )
  );

  addDiscussionEntryAttachments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addDiscussionEntryAttachments),
      withLatestFrom(this.store.pipe(select(selectDiscussion))),
      map(([action, discussion]) => {
        const attachments = action.attachmentDetails?.map((attachment) => {
          const assetID =
            discussion === null || discussion === undefined
              ? attachment.AssetID
              : discussion.AssetID;
          return {
            assetID: assetID,
            discussionID: attachment.DiscussionID,
            entryID: attachment.EntryID,
            title: attachment.Title,
            discussionType: attachment.DiscussionType,
            file: attachment.File,
            displayOrder: attachment.DisplayOrder,
          };
        });
        return attachments || [];
      }),
      switchMap((vals) =>
        forkJoin(
          vals.map((attach) =>
            this.discussionsFrameworkService
              .createDiscussionAttachment(
                attach.discussionID,
                attach.entryID,
                attach.title,
                attach.discussionType,
                attach.displayOrder
              )
              .pipe(
                map((attachment: IDiscussionAttachment) => {
                  return {
                    assetID: attach.assetID,
                    entryID: attach.entryID,
                    title: attach.title,
                    file: attach.file,
                    attachment,
                  };
                })
              )
          )
        )
      ),
      switchMap((vals) =>
        forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .getFileUrl(null, attach.title, true, attach.file.type)
              .pipe(
                map((awsFile: IAWSFileUrl) => {
                  return {
                    assetID: attach.assetID,
                    entryID: attach.entryID,
                    title: attach.title,
                    file: attach.file,
                    attachment: attach.attachment,
                    awsFile,
                  };
                })
              )
          )
        )
      ),
      switchMap((vals) =>
        forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .uploadFile(attach.awsFile.Url, attach.file)
              .pipe(
                map((result: boolean) => {
                  return {
                    assetID: attach.assetID,
                    entryID: attach.entryID,
                    title: attach.title,
                    attachment: attach.attachment,
                    awsFile: attach.awsFile,
                  };
                })
              )
          )
        )
      ),
      switchMap((vals) => {
        return forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .saveFileInfo(
                attach.title,
                attach.assetID,
                attach.awsFile.ContentID,
                false
              )
              .pipe(
                map((saveAwsFile: IAWSFileUrl) => {
                  return {
                    entryID: attach.entryID,
                    attachment: attach.attachment,
                    awsFile: attach.awsFile,
                    saveAwsFile,
                  };
                })
              )
          )
        );
      }),
      switchMap((vals) => {
        vals.forEach((val) => {
          val.awsFile.Url = encodeURIComponent(val.awsFile.Url);
        });
        return [
          actions.convertToBlob({
            attachmentDetails: vals,
            entryID: vals[0].entryID,
          }),
        ];
      }),
      catchError((error) => {
        this.toastService.openSnackBar('Error uploading Attachment!', 'error');
        console.log(JSON.stringify(error));
        return of(actions.addDiscussionEntryAttachmentsFailure(error));
      })
    )
  );

  convertToBlob$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.convertToBlob),
      switchMap((action) =>
        forkJoin([
          forkJoin(
            action.attachmentDetails.map((attach) =>
              this.imagesFrameworkService
                .convertToBlob(attach.saveAwsFile)
                .pipe(
                  map((blobURL: SafeStyle) => {
                    const attachment = {
                      ...attach.attachment,
                    } as IDiscussionAttachment;
                    attachment.complete = true;
                    attachment.ContentID = attach.awsFile.ContentID;
                    attachment.path = attach.saveAwsFile.Url;
                    attachment.blobURL = blobURL;
                    return attachment;
                  })
                )
            )
          ),
          of(action.entryID),
        ])
      ),
      switchMap(([attachments, entryID]) => {
        this.toastService.openSnackBar(
          'Attachment(s) has been uploaded!',
          'success'
        );

        this.eventbus.emit(
          new EmitEvent(Events.UploadDiscussionAttachmentsComplete, attachments)
        );
        return [
          actions.addDiscussionEntryAttachmentsSuccess({
            attachments: attachments as IDiscussionAttachment[],
            discussionEntryID: entryID,
          }),
        ];
      }),
      catchError((error) => {
        this.toastService.openSnackBar('Error uploading Attachment!', 'error');
        console.log(JSON.stringify(error));
        this.eventbus.emit(
          new EmitEvent(Events.UploadDiscussionAttachmentsFailure)
        );
        return of(actions.convertToBlobFailure(error));
      })
    )
  );

  searchTaggingKeyword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.searchTaggingKeyword),
      withLatestFrom(this.store.pipe(select(selectDiscussion))),
      map(([action, discussion]) => {
        return {
          asset: discussion.AssetID.toString(),
          searchExpression: action.searchDetails.Keyword,
          discussionEntryID:
            action.searchDetails.DiscussionEntry.DiscussionEntryID,
        };
      }),
      filter((vals) => !isNil(vals.asset) && !isNil(vals.searchExpression)),
      switchMap((vals) =>
        this.assetFrameworkService
          .searchTaggingKeywords(vals.asset, vals.searchExpression)
          .pipe(
            map(
              (keywords) =>
                actions.searchTaggingKeywordSuccess({
                  discussionEntryID: vals.discussionEntryID,
                  keywords,
                }),
              catchError((error) =>
                of(
                  actions.searchTaggingKeywordFailure({
                    discussionEntryID: vals.discussionEntryID,
                    error,
                  })
                )
              )
            )
          )
      )
    )
  );

  addDiscussionEntryKeyword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addDiscussionEntryKeyword),
      withLatestFrom(this.store.pipe(select(selectDiscussion))),
      map(([action, discussion]) => {
        const autoCompleteKeywords =
          action.addDetails.DiscussionEntry.autoCompleteKeywords;
        const keywords = action.addDetails.DiscussionEntry.Keywords;
        const keyword =
          keywords?.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() ===
              action.addDetails.Keyword.toLocaleLowerCase()
          ) || null;
        const autoCompleteKeyword =
          autoCompleteKeywords?.find(
            (tag) =>
              tag.Text.toLocaleLowerCase() ===
              action.addDetails.Keyword.toLocaleLowerCase()
          ) || null;

        return {
          asset: discussion.AssetID.toString(),
          tag: action.addDetails.Keyword,
          keyword,
          autoCompleteKeyword,
          discussionEntryID:
            action.addDetails.DiscussionEntry.DiscussionEntryID,
        };
      }),
      filter((vals) => !isNil(vals.asset) && isNil(vals.keyword)),
      switchMap((vals) =>
        this.assetFrameworkService.isReservedKeyword(vals.asset, vals.tag).pipe(
          map((isReservedKeyword: boolean) => {
            return {
              asset: vals.asset,
              tag: vals.tag,
              autoCompleteKeyword: vals.autoCompleteKeyword,
              discussionEntryID: vals.discussionEntryID,
              isReservedKeyword,
            };
          })
        )
      ),
      switchMap((vals) => {
        if (!vals.isReservedKeyword) {
          if (vals.autoCompleteKeyword?.KeywordId > 0) {
            // Tag is already registered so save it directly together with the other tags
            return [
              actions.updateDiscussionEntryKeywords({
                discussionEntryID: vals.discussionEntryID,
                keyword: vals.autoCompleteKeyword,
              }),
            ];
          }

          // This will register the tag then save with the other tags.
          return this.assetFrameworkService
            .addTaggingKeyword(vals.asset, vals.tag)
            .pipe(
              map(
                (keywords) =>
                  actions.updateDiscussionEntryKeywords({
                    discussionEntryID: vals.discussionEntryID,
                    keyword: keywords[0],
                  }),
                catchError((error) => of(actions.registerKeywordFailure(error)))
              )
            );
        }
        // This willl be thrown if the keyword is a reserved keyword
        return [actions.addDiscussionEntryKeywordFailure()];
      })
    )
  );

  convertBVFilesToHTML$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.convertBVFilesToHTML),
      map((action) => {
        const summaryContent = action.issueSummary;
        const pattern = /(<bv-file file-guid="((\w+[-]*)+)"><\/bv-file>)/gm;
        const matchFiles = summaryContent.match(pattern);
        const fileGUIDs: string[] = matchFiles?.map((val) => {
          const guidPatt = /file-guid="((\w+[-]*)+)"/g;
          const match = guidPatt.exec(val);
          return match[1];
        });
        return { fileGUIDs, summaryContent, matchFiles };
      }),
      filter((vals: any) => vals.fileGUIDs?.length > 0),
      switchMap((vals: any) =>
        forkJoin([
          forkJoin(
            vals.fileGUIDs.map((guid) =>
              this.imagesFrameworkService.getFileInfo(guid)
            )
          ),
          of(vals.summaryContent),
          of(vals.matchFiles),
        ])
      ),
      switchMap(([fileInfos, summaryContent, matchFiles]) => {
        return [
          actions.convertBVFilesToHTMLSuccess({
            issueSummaryWithFiles: replaceBVFilesToHTMLFileElement(
              fileInfos as any,
              summaryContent,
              matchFiles
            ),
          }),
        ];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(actions.convertBVFilesToHTMLFailure(error));
      })
    )
  );

  validateSummaryFile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.validateSummaryFile),
      switchMap((action) => {
        if (!isValidFilenameForAttachment(action.file.name)) {
          this.toastService.openSnackBar(
            'Upload failed, unsupported character in file name.',
            'warning'
          );

          this.eventbus.emit(new EmitEvent(Events.UploadSummaryFileFailure));
          return of(actions.validateSummaryFileFailure());
        } else {
          // Happy path
          return of(actions.uploadSummaryFile(action));
        }
      })
    )
  );

  uploadSummaryFile$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.uploadSummaryFile),
        map((action) => {
          return {
            assetID: action.assetID,
            file: action.file,
          };
        }),
        switchMap((vals) =>
          this.imagesFrameworkService
            .getFileUrl(null, vals.file.name, true, vals.file.type)
            .pipe(
              map((awsFile: IAWSFileUrl) => {
                return {
                  assetID: vals.assetID,
                  file: vals.file,
                  awsFile,
                };
              })
            )
        ),
        switchMap((vals: any) =>
          this.imagesFrameworkService
            .uploadFile(vals.awsFile.Url, vals.file)
            .pipe(
              map((result: boolean) => {
                return {
                  assetID: vals.assetID,
                  file: vals.file,
                  awsFile: vals.awsFile,
                };
              })
            )
        ),
        switchMap((vals: any) =>
          this.imagesFrameworkService
            .saveFileInfo(
              vals.file.name,
              vals.assetID,
              vals.awsFile.ContentID,
              false
            )
            .pipe(
              map((saveAwsFile: IAWSFileUrl) => {
                return {
                  awsFile: vals.awsFile,
                  saveAwsFile,
                };
              })
            )
        ),
        switchMap((vals: any) =>
          this.imagesFrameworkService.getFileInfo(vals.awsFile.ContentID).pipe(
            map((fileInfo: IFileInfo) => {
              this.toastService.openSnackBar(
                'File has been uploaded!',
                'success'
              );

              const newFileInfo = {
                complete: true,
                contentID: vals.awsFile.ContentID,
                path: vals.saveAwsFile.Url,
                progressPercentage: 100,
                fileName: fileInfo.Name,
                isImage: fileInfo.IsImage,
              };

              this.eventbus.emit(
                new EmitEvent(
                  Events.UploadSummaryFileComplete,
                  createHTMLFileElement(newFileInfo)
                )
              );
            })
          )
        )
      ),
    { dispatch: false }
  );

  saveSummary$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.saveSummary),
      withLatestFrom(this.store.pipe(select(selectIssueSummaryGuid))),
      switchMap(([action, issueGuid]) =>
        this.issuesFrameworkService
          .saveIssueSummary(
            issueGuid,
            replaceHTMLFileElementToBVFiles(action.summaryContent)
          )
          .pipe(
            map(
              (discussionEntry) =>
                actions.saveSummarySuccess({
                  summaryContent: action.summaryContent,
                }),
              catchError((error) => of(actions.saveSummaryFailure(error)))
            )
          )
      )
    )
  );

  assetTreeDropdownStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.assetTreeDropdownStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeDropdownState))),
      switchMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map((n) => actions.assetTreeDropdownStateChange(n)),
            catchError((error) =>
              of(actions.assetTreeDropdownDataRequestFailure(error))
            )
          )
      )
    )
  );

  showRelatedIssuesByAsset$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showRelatedIssuesByAsset),
        tap((p) => {
          if (p.assetUniqueKey) {
            let url = window.location.href;
            url = url.substr(0, url.indexOf(this.router.url));
            url = url + '/issues?id=' + p.assetUniqueKey;
            window.open(url, '_blank');
          } else {
            this.toastService.openSnackBar('Could not open related', 'warning');
          }
        })
      ),
    { dispatch: false }
  );

  showRelatedAlertsByAsset$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.showRelatedAlertsByAsset),
        tap((p) => {
          if (p.assetUniqueKey) {
            let url = window.location.href;
            url = url.substr(0, url.indexOf(this.router.url));
            url = url + '/alerts?id=' + p.assetUniqueKey;
            window.open(url, '_blank');
          } else {
            this.toastService.openSnackBar('Could not open related', 'warning');
          }
        })
      ),
    { dispatch: false }
  );
}
