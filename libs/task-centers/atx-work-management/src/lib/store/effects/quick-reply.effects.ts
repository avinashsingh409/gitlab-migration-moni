/* eslint-disable ngrx/no-dispatch-in-effects */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { IAWSFileUrl, IQuickReplyAttachment } from '@atonix/atx-core';
import * as actions from '../actions/quick-reply.actions';
import { forkJoin, of } from 'rxjs';
import { ImagesFrameworkService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { IQuickReplyState } from '../state/quick-reply-state';
import { selectAttachments } from '../selectors/quick-reply.selector';

@Injectable()
export class QuickReplyEffects {
  constructor(
    private actions$: Actions,
    private imagesFrameworkService: ImagesFrameworkService,
    private toastService: ToastService,
    private store: Store<IQuickReplyState>
  ) {}

  addQuickReplyAttachments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addQuickReplyAttachments),
      withLatestFrom(this.store.pipe(select(selectAttachments))),
      map(([action, currentAttachments]) => {
        let displayOrder = currentAttachments.length;
        const attachments = action.files?.map((file) => {
          return {
            assetID: action.assetID,
            displayOrder: displayOrder++,
            file,
          };
        });
        return attachments || [];
      }),
      switchMap((vals) =>
        forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .getQuickReplyFileURL(null, attach.file.name, attach.file.type)
              .pipe(
                map((awsFile: IAWSFileUrl) => {
                  return {
                    assetID: attach.assetID,
                    displayOrder: attach.displayOrder,
                    file: attach.file,
                    awsFile,
                  };
                })
              )
          )
        )
      ),
      switchMap((vals) =>
        forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .uploadFile(attach.awsFile.Url, attach.file)
              .pipe(
                map((result: boolean) => {
                  return {
                    assetID: attach.assetID,
                    displayOrder: attach.displayOrder,
                    file: attach.file,
                    awsFile: attach.awsFile,
                  };
                })
              )
          )
        )
      ),
      switchMap((vals) => {
        return forkJoin(
          vals.map((attach) =>
            this.imagesFrameworkService
              .saveFileInfo(
                attach.file.name,
                attach.assetID,
                attach.awsFile.ContentID,
                true
              )
              .pipe(
                map((saveAwsFile: IAWSFileUrl) => {
                  attach.awsFile = { ...attach.awsFile };
                  attach.awsFile.ContentID = attach.awsFile.ContentID.replace(
                    /\\|"/g,
                    ''
                  );
                  const attachment: IQuickReplyAttachment = {
                    id: attach.awsFile.ContentID,
                    size: attach.file.size,
                    name: attach.file.name,
                    type: attach.file.type,
                    displayOrder: attach.displayOrder,
                  };
                  return attachment;
                })
              )
          )
        );
      }),
      switchMap((attachments) => {
        this.toastService.openSnackBar(
          'Attachment(s) has been uploaded!',
          'success'
        );

        return [actions.addQuickReplyAttachmentsSuccess({ attachments })];
      }),
      // eslint-disable-next-line rxjs/no-implicit-any-catch
      catchError((error) => {
        this.toastService.openSnackBar('Error uploading Attachment!', 'error');
        console.log(JSON.stringify(error));
        return of(actions.addQuickReplyAttachmentsFailure(error));
      })
    )
  );
}
