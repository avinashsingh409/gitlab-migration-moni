/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  filter,
  tap,
  mergeMap,
} from 'rxjs/operators';
import {
  ModelService as AssetTreeModel,
  selectAsset,
} from '@atonix/atx-asset-tree';
import {
  NavActions,
  updateRouterIdWithoutReloading,
} from '@atonix/atx-navigation';
import { AuthSelectors } from '@atonix/shared/state/auth';
import * as actions from '../actions/summary.actions';
import {
  IssuesCoreService,
  UIConfigFrameworkService,
  IGridExportRetrieval,
  ISavedIssueList,
  AuthorizationFrameworkService,
} from '@atonix/shared/api';
import { IssueRetrieverService } from '../../service/issue-retriever.service';
import {
  getAssetTreeState,
  getDonutKey,
  getDisplayedListState,
  getSelectedAsset,
} from '../selectors/summary.selector';
import { assetState } from '../selectors/work-management.selector';
import { ActivatedRoute, Router } from '@angular/router';
import { LoggerService, ToastService } from '@atonix/shared/utils';
import { formatCSVFile } from '../../service/utilities';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Injectable()
export class SummaryEffects {
  constructor(
    private actions$: Actions,
    private issueCoreService: IssuesCoreService,
    private uiConfigFrameworkService: UIConfigFrameworkService,
    private issueRetrieverService: IssueRetrieverService,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private assetTreeModel: AssetTreeModel,
    private store: Store<any>,
    private logger: LoggerService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBarService: ToastService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  initializeSummary$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.summaryInitialize),
      map((n) => n.asset),
      withLatestFrom(
        this.store.select(getSelectedAsset),
        this.store.select(assetState)
      ),
      map(([routeAsset, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return routeAsset || appAsset?.toString() || newAsset;
      }),
      switchMap((asset) => [
        actions.permissionsRequest(),
        actions.treeStateChange(selectAsset(asset, false)),
        NavActions.taskCenterLoad({
          taskCenterID: 'work_management',
          assetTree: true,
          assetTreeOpened: true,
          timeSlider: false,
          asset,
        }),
      ])
    )
  );

  selectWorkItem$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.workItemSelect),
        map((n) => n.id),
        tap((id) => {
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=${id}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=${id}`;
          }

          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  addWorkItemNewLink$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.workItemAddToAsset),
        map((n) => n.asset),
        tap((asset) => {
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=-1&ast=${asset}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=-1&ast=${asset}`;
          }

          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  addWorkItem$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.workItemAdd),
      withLatestFrom(this.store.select(getSelectedAsset)),
      switchMap(([action, asset]) => [actions.workItemAddToAsset({ asset })])
    )
  );

  selectAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.selectAsset),
      switchMap((action) => {
        setTimeout(() => {
          updateRouterIdWithoutReloading(
            action.asset,
            this.activatedRoute,
            this.router
          );
        }, 100);
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          NavActions.updateNavigationItems({
            urlParams: { id: action.asset },
          }),
        ];
      })
    )
  );

  listViewDownload$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listViewDownloadRequest),
      switchMap((action) => {
        this.logger.feature('download', 'workmanagement');
        this.snackBarService.openSnackBar('Downloading issues...', 'info');
        return this.issueCoreService.ListViewServiceDownload(action.gridParams);
      }),
      map((n: IGridExportRetrieval): void => {
        // this function downloads the file.
        // Reference for this implementation: https://ourcodeworld.com/articles/read/
        // 189/how-to-create-a-file-and-generate-a-download-with-javascript-in-the-browser-without-a-server
        const element = document.createElement('a');
        element.setAttribute(
          'href',
          'data:text/plain;charset=utf-8,' +
            encodeURIComponent(formatCSVFile(n.Results[0]))
        );
        element.setAttribute('download', 'WorkItemsDownload.csv');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
      }),
      map(
        () => {
          this.snackBarService.openSnackBar(
            'Issues downloaded sucessfully.',
            'success'
          );
          return actions.listViewDownloadSuccess();
        },
        catchError((error) => {
          this.snackBarService.openSnackBar(error, 'Error downloading issues!');
          return of(actions.listViewDownloadFailure(error));
        })
      )
    )
  );

  getPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.permissionsRequest),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) => actions.permissionsRequestSuccess(permissions)),
          catchError((error) => of(actions.permissionsRequestFailure(error)))
        )
      )
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.treeStateChange),
      withLatestFrom(this.store.pipe(select(getAssetTreeState))),
      mergeMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => actions.treeStateChange(n),
              catchError((error) =>
                of(actions.assetTreeDataRequestFailure(error))
              )
            )
          )
      )
    )
  );

  getDonutData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.donutsRequest),
      filter((payload) => payload?.state?.filterModel?.AssetID),
      switchMap((payload) =>
        this.issueCoreService.getDonutData(payload.state).pipe(
          map(
            (data) =>
              actions.donutsSuccess({
                values: data.Results[0],
                asset: payload.state.filterModel.AssetID,
                currency: this.appConfig.currency,
              }),
            catchError((error) => of(actions.donutsFailure(error)))
          )
        )
      )
    )
  );

  reflowSlower$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.treeSizeChange),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 100);
        })
      ),
    { dispatch: false }
  );

  loadLists = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listsLoad),
      switchMap(() =>
        this.uiConfigFrameworkService
          .getIssueLists()
          .pipe(map((lists) => actions.listsLoadSuccess({ lists })))
      )
    )
  );

  saveList = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listSave),
      tap((action) => {
        this.logger.savedFilter(null, 'workmanagement', 'save');
      }),
      withLatestFrom(this.store.pipe(select(getDisplayedListState))),
      map(([newList, oldState]) => {
        const myList: ISavedIssueList = {
          id: newList.list.id,
          name: newList.list.name,
          state: oldState,
          checked: true,
        };
        return myList;
      }),
      switchMap((list) =>
        this.uiConfigFrameworkService
          .saveIssueList(list)
          .pipe(map((newList) => actions.listSaveSuccess({ list: newList })))
      )
    )
  );

  deleteList = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.listDelete),
      tap((action) => {
        this.logger.savedFilter(null, 'workmanagement', 'delete');
      }),
      map((n) => n.id),
      switchMap((id) =>
        this.uiConfigFrameworkService
          .deleteList(id)
          .pipe(map((result) => actions.listDeleteSuccess({ id })))
      )
    )
  );

  toggleFloatingFilter = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.toggleFloatingFilter),
        tap(() => {
          this.logger.feature('floating_filter', 'workmanagement');
        })
      ),
    { dispatch: false }
  );

  loadList = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.listLoad),
        tap((action) => {
          this.logger.savedFilter(null, 'workmanagement', 'load');
        })
      ),
    { dispatch: false }
  );

  loadDefaultList = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.loadDefaultList),
        tap((action) => {
          this.logger.savedFilter('default', 'workmanagement', 'load');
        })
      ),
    { dispatch: false }
  );

  setScorecard = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.setScorecard),
      switchMap((action) =>
        // This goes through the IssueRetrieverService to manage caching
        this.issueRetrieverService
          .setScorecard(action.assetIssueID, action.include)
          .pipe(
            map(
              (data) =>
                actions.setScorecardSuccess({
                  assetIssueID: action.assetIssueID,
                  include: action.include,
                }),
              catchError((error) => of(actions.setScorecardFailure(error)))
            )
          )
      )
    )
  );
}
