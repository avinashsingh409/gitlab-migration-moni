import { createSelector } from '@ngrx/store';

import { selectQuickReplyState } from './work-management.selector';

export const selectAttachments = createSelector(
  selectQuickReplyState,
  (state) => state.Attachments
);
