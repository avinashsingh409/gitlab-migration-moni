import {
  getIDFromSelectedAssets,
  getSelectedNodes,
} from '@atonix/atx-asset-tree';
import { INavigationState } from '@atonix/atx-navigation';
import { createSelector } from '@ngrx/store';

import { selectIssueSnapshotState, navState } from './work-management.selector';

export const selectDiscussionState = createSelector(
  selectIssueSnapshotState,
  (state) => state.DiscussionState
);

export const selectDiscussion = createSelector(
  selectDiscussionState,
  (state) => state?.discussion || null
);

export const selectDiscussionEntry = createSelector(
  selectDiscussionState,
  (state) => state?.discussionEntry || null
);

export const selectDiscussions = createSelector(
  selectDiscussionState,
  (state) => state?.discussions || null
);

export const selectDiscussionEntries = createSelector(
  selectDiscussionState,
  (state) => state?.discussion?.Entries || null
);

export const selectIssueSummaryState = createSelector(
  selectIssueSnapshotState,
  (state) => state.SummaryState
);

export const selectIssueSummaryGuid = createSelector(
  selectIssueSummaryState,
  (state) => state?.IssueGuid || null
);

export const selectIssueSummaryContent = createSelector(
  selectIssueSummaryState,
  (state) => state?.IssueSummary || null
);

export const selectAssetTreeDropdownState = createSelector(
  selectIssueSnapshotState,
  (state) => state?.AssetTreeDropdownState || null
);

export const selectAssetTreeDropdownConfiguration = createSelector(
  selectAssetTreeDropdownState,
  (state) => state.treeConfiguration
);

export const selectAssetTreeNode = createSelector(
  selectAssetTreeDropdownState,
  (state) =>
    state.treeConfiguration.nodes.find(
      (a) =>
        a.uniqueKey ===
        getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
    )
);

export const selectAssetName = createSelector(
  selectAssetTreeNode,
  (state) => state?.nodeAbbrev
);

export const selectAssetId = createSelector(
  selectAssetTreeNode,
  (state) => state?.data?.AssetId
);

export const selectAssetGuid = createSelector(selectAssetTreeNode, (state) => {
  return state?.data?.AssetGuid;
});

export const selectAssetUniqueKey = createSelector(
  selectAssetTreeDropdownState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
);

export const selectCurrentUser = createSelector(
  navState,
  (state: INavigationState) => state?.user
);

export const selectWorkRequestInfo = createSelector(
  selectIssueSnapshotState,
  (state) => state.WorkRequestInfo
);

export const selectWorkRequestLoading = createSelector(
  selectIssueSnapshotState,
  (state) => state.WorkRequestLoading
);

export const selectSelectedExternalAsset = createSelector(
  selectIssueSnapshotState,
  (state) => state.selectedExternalAsset
);

export const selectSelectedExternalAssetLoading = createSelector(
  selectIssueSnapshotState,
  (state) => state.selectedExternalAssetLoading
);

export const selectExternalAssets = createSelector(
  selectIssueSnapshotState,
  (state) => state.externalAssets
);

export const selectExternalAssetsLoading = createSelector(
  selectIssueSnapshotState,
  (state) => state.externalAssetsLoading
);
