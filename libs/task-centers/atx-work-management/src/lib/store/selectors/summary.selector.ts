import { createSelector } from '@ngrx/store';
import { ISummaryState, savedIssueListAdapter } from '../state/summary-state';

import { selectSummaryState } from './work-management.selector';
import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import {
  convertToComponentData,
  IComponentDonutState,
} from '@atonix/atx-chart';

export const getAppContext = createSelector(
  selectSummaryState,
  (state) => state.AppContext
);

export const getAssetTreeState = createSelector(
  selectSummaryState,
  (state) => state?.AssetTreeState
);

export const getAssetTreeConfiguration = createSelector(
  selectSummaryState,
  (state) => state?.AssetTreeState.treeConfiguration
);

export const selectLeftTrayMode = createSelector(
  getAssetTreeConfiguration,
  (state) => {
    const result: TrayState = state?.pin ? 'side' : 'over';
    return result;
  }
);

export const getLeftDonutState = createSelector(selectSummaryState, (state) => {
  const result: IComponentDonutState = convertToComponentData(
    state.LeftDonutState
  );
  return result;
});

export const getMiddleDonutState = createSelector(
  selectSummaryState,
  (state) => {
    const result: IComponentDonutState = convertToComponentData(
      state.MiddleDonutState
    );
    return result;
  }
);

export const getRightDonutState = createSelector(
  selectSummaryState,
  (state) => {
    const result: IComponentDonutState = convertToComponentData(
      state.RightDonutState
    );
    return result;
  }
);

export const getDonutKey = createSelector(
  selectSummaryState,
  (state) => state.DonutKey
);

export const getListState = createSelector(
  selectSummaryState,
  (state) => state.SavedListState
);

export const getDisplayedListState = createSelector(
  selectSummaryState,
  (state) => state.DisplayedListState
);

const { selectIds, selectEntities, selectAll, selectTotal } =
  savedIssueListAdapter.getSelectors();

export const getListsState = createSelector(
  selectSummaryState,
  (state) => state.IssueLists
);

export const getSavedLists = createSelector(getListsState, selectAll);

export const getSelectedAsset = createSelector(selectSummaryState, (state) =>
  getIDFromSelectedAssets(getSelectedNodes(state?.AssetTreeState.treeNodes))
);

export const getSelectedAssetTreeNode = createSelector(
  getAssetTreeState,
  getSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const getSelectedAssetKey = createSelector(
  getSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return (
        data.TreeId +
        '~' +
        data.ParentNodeId +
        '~' +
        data.NodeId +
        '~' +
        data.AssetGuid
      );
    }
    return null;
  }
);

export const selectFloatingFilter = createSelector(
  selectSummaryState,
  (state) => state.FloatingFilter
);

export const leftTraySize = createSelector(
  selectSummaryState,
  (n) => n.TreeSize
);
