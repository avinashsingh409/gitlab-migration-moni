import { IWorkManagementState } from '../state/work-management-state';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { INavigationState } from '@atonix/atx-navigation';

// This is actually grabbing the application state.  It doesn't really
// know what the full application state is, it
export const selectApp = (state: {
  nav: INavigationState;
  workmanagement: IWorkManagementState;
  asset: any;
}) => state;

// This looks at the application state and grabs the member
// named workmanagement.  It isn't doing anything more complex than that.
const workManagementState =
  createFeatureSelector<IWorkManagementState>('workmanagement');

// This grabs the state of the selected asset
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);

// This is grabbing the applicationstate.workmanagement.summaryState object
export const selectSummaryState = createSelector(
  workManagementState,
  (state: IWorkManagementState) => state?.summaryState
);

// This is grabbing the applicationstate.workmanagement.issueSnapshotState object
export const selectIssueSnapshotState = createSelector(
  workManagementState,
  (state: IWorkManagementState) => state?.issueSnapshotState
);

// This is grabbing the applicationstate.workmanagement.quickReplyState object
export const selectQuickReplyState = createSelector(
  workManagementState,
  (state: IWorkManagementState) => state?.quickReplyState
);

const navigationState = createFeatureSelector<INavigationState>('nav');

export const navState = createSelector(
  navigationState,
  (state: INavigationState) => state
);
