import * as selectors from './summary.selector';
import { ISummaryState } from '../state/summary-state';
import {
  ITreeConfiguration,
  ITreeNodes,
  ITreeState,
} from '@atonix/atx-asset-tree';
import { IDonutState, convertToComponentData } from '@atonix/atx-chart';

describe('SummarySelector', () => {
  const treeConfig: ITreeConfiguration = {
    trees: [],
    nodes: [],
    showTreeSelector: true,
    selectedTree: '123456789',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    pin: true,
    loadingAssets: false,
    loadingTree: false,
    collapseOthers: true,
    canView: true,
    canAdd: true,
    canEdit: false,
    canDelete: false,
    hideConfigureButton: false,
  };

  const treeNodes: ITreeNodes = {
    ids: ['testing123', 'testing456'],
    entities: {
      testing123: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: true,
        symbol: 'collapsed',
      },
      testing456: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: false,
        symbol: 'collapsed',
      },
    },
    rootAssets: ['testing123'],
    childrenDict: null,
    treeID: '123',
    appContext: 'test',
  };

  const mockAssetTreeState: ITreeState = {
    treeConfiguration: treeConfig,
    treeNodes,
    hasDefaultSelectedAsset: true,
  };

  const mockDonutState: IDonutState = {
    title: 'Testing Only',
    subtitle: 'For Testing Purposes',
    IDappendedToOthers: 'test',
    drilldownSection: 1,
    legendWidthLimit: 5,
    AllDonutData: [
      {
        id: '1',
        name: 'TestOne',
        y: 123,
      },
      {
        id: '2',
        name: 'TestTwo',
        y: 456,
      },
    ],
    toolTipFormat: {
      decimalPlaces: 2,
      isRounded: false,
    },
  };

  const mockSummaryState: ISummaryState = {
    AssetTreeState: mockAssetTreeState,
    DonutKey: 'donutTestKey123',
    LeftDonutState: mockDonutState,
    MiddleDonutState: mockDonutState,
    RightDonutState: mockDonutState,
    CollapseOthers: false,
    AppContext: 'testing',
    IssueLists: {
      ids: null,
      entities: null,
    },
    SavedListState: null,
    DisplayedListState: null,
    FloatingFilter: false,
    TreeSize: 102,
  };

  it('getAppContext', () => {
    expect(selectors.getAppContext.projector(mockSummaryState)).toEqual(
      mockSummaryState.AppContext
    );
  });

  it('getAssetTreeState', () => {
    expect(selectors.getAssetTreeState.projector(mockSummaryState)).toEqual(
      mockSummaryState.AssetTreeState
    );
  });

  it('getAssetTreeConfiguration', () => {
    expect(
      selectors.getAssetTreeConfiguration.projector(mockSummaryState)
    ).toEqual(mockSummaryState.AssetTreeState.treeConfiguration);
  });

  it('getLeftDonutState', () => {
    expect(selectors.getLeftDonutState.projector(mockSummaryState)).toEqual(
      convertToComponentData(mockSummaryState.LeftDonutState)
    );
  });

  it('getMiddleDonutState', () => {
    expect(selectors.getMiddleDonutState.projector(mockSummaryState)).toEqual(
      convertToComponentData(mockSummaryState.MiddleDonutState)
    );
  });

  it('getRightDonutState', () => {
    expect(selectors.getRightDonutState.projector(mockSummaryState)).toEqual(
      convertToComponentData(mockSummaryState.RightDonutState)
    );
  });

  it('getDonutKey', () => {
    expect(selectors.getDonutKey.projector(mockSummaryState)).toEqual(
      mockSummaryState.DonutKey
    );
  });

  it('getListState', () => {
    expect(selectors.getListState.projector(mockSummaryState)).toEqual(
      mockSummaryState.SavedListState
    );
  });

  it('getDisplayedListState', () => {
    expect(selectors.getDisplayedListState.projector(mockSummaryState)).toEqual(
      mockSummaryState.DisplayedListState
    );
  });

  it('getListsState', () => {
    expect(selectors.getListsState.projector(mockSummaryState)).toEqual(
      mockSummaryState.IssueLists
    );
  });

  it('getSelectedAsset', () => {
    expect(selectors.getSelectedAsset.projector(mockSummaryState)).toBe(
      'testing123'
    );
  });
});
