import * as selectors from './work-management.selector';
import { INavigationState } from '@atonix/atx-navigation';
import {
  ITreeConfiguration,
  ITreeNodes,
  ITreeState,
} from '@atonix/atx-asset-tree';
import { IDonutState } from '@atonix/atx-chart';
import { ISummaryState } from '../state/summary-state';

describe('WorkManagementSelector', () => {
  const treeConfig: ITreeConfiguration = {
    trees: [],
    nodes: [],
    showTreeSelector: true,
    selectedTree: '123456789',
    autoCompleteValue: null,
    autoCompletePending: false,
    autoCompleteAssets: [],
    pin: true,
    loadingAssets: false,
    loadingTree: false,
    collapseOthers: true,
    canView: true,
    canAdd: true,
    canEdit: false,
    canDelete: false,
    hideConfigureButton: false,
  };

  const treeNodes: ITreeNodes = {
    ids: ['testing123', 'testing456'],
    entities: {
      testing123: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: true,
        symbol: 'collapsed',
      },
      testing456: {
        uniqueKey: 'testing123',
        parentUniqueKey: null,
        nodeAbbrev: 'testing only',
        displayOrder: 10,
        level: 0,
        retrieved: false,
        selected: false,
        symbol: 'collapsed',
      },
    },
    rootAssets: ['testing123'],
    childrenDict: null,
    treeID: '123',
    appContext: 'test',
  };

  const mockAssetTreeState: ITreeState = {
    treeConfiguration: treeConfig,
    treeNodes,
    hasDefaultSelectedAsset: true,
  };

  const mockDonutState: IDonutState = {
    title: 'Testing Only',
    subtitle: 'For Testing Purposes',
    IDappendedToOthers: 'test',
    drilldownSection: 1,
    legendWidthLimit: 5,
    AllDonutData: [
      {
        id: '1',
        name: 'TestOne',
        y: 123,
      },
      {
        id: '2',
        name: 'TestTwo',
        y: 456,
      },
    ],
    toolTipFormat: {
      decimalPlaces: 2,
      isRounded: false,
    },
  };

  const mockSummaryState: ISummaryState = {
    AssetTreeState: mockAssetTreeState,
    DonutKey: 'donutTestKey123',
    LeftDonutState: mockDonutState,
    MiddleDonutState: mockDonutState,
    RightDonutState: mockDonutState,
    CollapseOthers: false,
    AppContext: 'testing',
    IssueLists: {
      ids: null,
      entities: null,
    },
    SavedListState: null,
    DisplayedListState: null,
    FloatingFilter: false,
    TreeSize: 102,
  };

  const mockAppState: any = {
    workmanagement: {
      summaryState: mockSummaryState,
    },
  };

  it('selectApp', () => {
    expect(selectors.selectApp(mockAppState)).toEqual(mockAppState);
  });

  it('selectSummaryState', () => {
    expect(
      selectors.selectSummaryState.projector(mockAppState.workmanagement)
    ).toEqual(mockAppState.workmanagement.summaryState);
  });
});
