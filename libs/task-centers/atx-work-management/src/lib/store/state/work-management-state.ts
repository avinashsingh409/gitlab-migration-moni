import {
  IIssueSnapshotState,
  initialIssueSnapshotState,
} from './issue-snapshot-state';
import { initialQuickReplyState, IQuickReplyState } from './quick-reply-state';
import { ISummaryState, initialSummaryState } from './summary-state';

export interface IWorkManagementState {
  summaryState: ISummaryState;
  issueSnapshotState: IIssueSnapshotState;
  quickReplyState: IQuickReplyState;
  // add details state here
}

export const initialWorkManagementState: IWorkManagementState = {
  summaryState: initialSummaryState,
  issueSnapshotState: initialIssueSnapshotState,
  quickReplyState: initialQuickReplyState,
  // add details state here
};
