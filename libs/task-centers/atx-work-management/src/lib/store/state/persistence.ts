import { isNil } from '@atonix/atx-core';
import { IListState } from '@atonix/shared/api';

const listKey = '__ATX_WORK_MANAGEMENT_LIST__';

export function getListState() {
  let result: IListState = null;
  if (window && window.sessionStorage) {
    const resultString = window.sessionStorage.getItem(listKey);
    if (
      resultString &&
      resultString.toLowerCase() !== 'undefined' &&
      resultString.toLowerCase() !== 'null'
    ) {
      result = JSON.parse(resultString) as IListState;
    }
  }
  return result;
}

export function setListState(listState: IListState) {
  if (window && window.sessionStorage) {
    if (isNil(listState)) {
      window.sessionStorage.removeItem(listKey);
    } else {
      window.sessionStorage.setItem(listKey, JSON.stringify(listState));
    }
  }
}
