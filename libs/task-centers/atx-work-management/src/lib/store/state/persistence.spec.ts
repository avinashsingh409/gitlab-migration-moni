import * as persistence from '../state/persistence';
import { TestBed } from '@angular/core/testing';
import { IListState } from '@atonix/shared/api';
interface Spies {
  [key: string]: jest.SpyInstance;
}
describe('PersistenceFunctions', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [],
    })
  );

  const spies: Spies = {};
  beforeEach(() => {
    ['setItem', 'getItem', 'clear'].forEach((fn: string) => {
      const mock = jest.fn(localStorage[fn]);
      spies[fn] = jest.spyOn(Storage.prototype, fn).mockImplementation(mock);
    });
    window.localStorage.clear();
    window.sessionStorage.clear();
  });
  afterEach(() => {
    Object.keys(spies).forEach((key: string) => spies[key].mockRestore());
  });

  it('should get list state from storage', () => {
    const mockListState: IListState = {
      columns: null,
      sort: [],
      groups: [],
      filter: null,
    };

    window.sessionStorage.setItem(
      '__ATX_WORK_MANAGEMENT_LIST__',
      JSON.stringify(mockListState)
    );
    expect(persistence.getListState()).toEqual(mockListState);
  });

  it('should set list state into the storage', () => {
    const mockListState: IListState = {
      columns: null,
      sort: [],
      groups: [],
      filter: null,
    };

    persistence.setListState(null);
    expect(
      window.sessionStorage.getItem('__ATX_WORK_MANAGEMENT_LIST__')
    ).toBeNull();

    persistence.setListState(mockListState);
    expect(
      window.sessionStorage.getItem('__ATX_WORK_MANAGEMENT_LIST__')
    ).toEqual(JSON.stringify(mockListState));
  });
});
