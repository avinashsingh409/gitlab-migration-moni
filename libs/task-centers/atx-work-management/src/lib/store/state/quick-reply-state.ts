import { IQuickReplyAttachment } from '@atonix/atx-core';

export interface IQuickReplyState {
  Attachments: IQuickReplyAttachment[];
  UploadingAttachments: boolean;
}

export const initialQuickReplyState: IQuickReplyState = {
  Attachments: [],
  UploadingAttachments: false,
};
