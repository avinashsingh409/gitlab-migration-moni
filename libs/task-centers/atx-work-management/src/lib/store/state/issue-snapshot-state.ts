import {
  createTreeBuilder,
  getDefaultTree,
  ITreeState,
} from '@atonix/atx-asset-tree';
import { IDiscussionState, getDefaultDiscussion } from '@atonix/atx-discussion';
import {
  IIssueSummaryState,
  getDefaultSummary,
} from '../../model/issue-summary-state';
import {
  IExternalAssetsLoading,
  IWorkRequest,
  IWorkRequestLoading,
  ExternalAsset,
  ISelectedExternalAssetLoading,
} from '@atonix/shared/api';

export interface IIssueSnapshotState {
  DiscussionState: IDiscussionState;
  SummaryState: IIssueSummaryState;
  AssetTreeDropdownState: ITreeState;
  WorkRequestInfo: IWorkRequest;
  WorkRequestLoading: IWorkRequestLoading;
  selectedExternalAsset: ExternalAsset;
  selectedExternalAssetLoading: ISelectedExternalAssetLoading;
  externalAssets: ExternalAsset[];
  externalAssetsLoading: IExternalAssetsLoading;
}

export const initialIssueSnapshotState: IIssueSnapshotState = {
  DiscussionState: getDefaultDiscussion(),
  SummaryState: getDefaultSummary(),
  AssetTreeDropdownState: {
    treeConfiguration: getDefaultTree({ collapseOthers: true }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
  WorkRequestInfo: {
    title: null,
    description: null,
    location: null,
    equipment: null,
    workRequestId: null,
    workReferenceId: null,
    assetIssueId: null,
    createDate: null,
    status: null,
    link: null,
    priority: '',
    lastUpdatedAt: null,
    lastQueriedAt: null,
  },
  WorkRequestLoading: {
    loading: false,
    error: null,
    swrLoading: false,
    swrError: null,
    polling: false,
    pollingError: null,
  },
  selectedExternalAsset: null,
  selectedExternalAssetLoading: {
    loading: false,
    error: null,
  },
  externalAssets: [],
  externalAssetsLoading: {
    loading: false,
    error: null,
  },
};
