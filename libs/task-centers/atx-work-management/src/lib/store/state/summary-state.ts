import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { IDonutState, createDefaultDonutState } from '@atonix/atx-chart';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { IListState, ISavedIssueList } from '@atonix/shared/api';

export function sortIssueList(a: ISavedIssueList, b: ISavedIssueList) {
  return a.name.localeCompare(b.name);
}

export const savedIssueListAdapter = createEntityAdapter<ISavedIssueList>({
  sortComparer: sortIssueList,
});

export type ISavedIssueListState = EntityState<ISavedIssueList>;

export interface ISummaryState {
  AssetTreeState: ITreeState;
  DonutKey: string;
  LeftDonutState: IDonutState;
  MiddleDonutState: IDonutState;
  RightDonutState: IDonutState;
  CollapseOthers: boolean;
  AppContext: string;

  IssueLists: ISavedIssueListState;

  // Saved columns are the columns from a configuration
  // we want to send to the list view.
  SavedListState: IListState;

  // Displayed columns are the columns the list view
  // is currently displaying.
  DisplayedListState: IListState;

  FloatingFilter: boolean;

  TreeSize: number;
}

export const initialSummaryState: ISummaryState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  DonutKey: null,
  LeftDonutState: createDefaultDonutState({
    title: 'Item Count by Category',
    subtitle: '0',
    toolTipFormat: { isRounded: true, decimalPlaces: 0 },
  }),
  MiddleDonutState: createDefaultDonutState({
    title: 'Item Count by Asset',
    subtitle: '0',
    toolTipFormat: { isRounded: true, decimalPlaces: 0 },
  }),
  RightDonutState: createDefaultDonutState({
    title: 'Item Impact',
    subtitle: '$0',
    toolTipFormat: {
      format: 'currency',
      locale: 'en-US',
      currency: 'USD',
      isRounded: true,
      decimalPlaces: 0,
    },
  }),
  CollapseOthers: true,
  AppContext: null,

  IssueLists: savedIssueListAdapter.getInitialState(),

  SavedListState: null,
  DisplayedListState: null,

  FloatingFilter: false,
  TreeSize: 250,
};
