/* eslint-disable ngrx/prefer-inline-action-props */
import { props, createAction } from '@ngrx/store';
import {
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '@atonix/atx-discussion';
import {
  IAWSFileUrl,
  IDiscussion,
  IDiscussionAttachment,
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  IImageDetails,
  ISaveDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { IWorkRequest, ExternalAsset } from '@atonix/shared/api';

// Discussion
export const loadDiscussion = createAction(
  '[Issue Snapshot - Discussion] Load Discussion',
  props<{
    assetIssueID: number;
    showAutogenEntries: boolean;
    showConfigButtonsByUser?: boolean;
  }>()
);
export const loadDiscussionSuccess = createAction(
  '[Issue Snapshot - Discussion] Load Discussion Success',
  props<{ discussion: IDiscussion }>()
);
export const loadDiscussionFailure = createAction(
  '[Issue Snapshot - Discussion] Load Discussion Failure',
  props<Error>()
);
export const clearState = createAction(
  '[Issue Snapshot - Discussion] Clear State'
);

export const showMoreEntries = createAction(
  '[Issue Snapshot - Discussion] Show More Entries'
);

export const getNewDiscussionEntry = createAction(
  '[Issue Snapshot - Discussion] Get New Discussion Entry'
);
export const getNewDiscussionEntrySuccess = createAction(
  '[Issue Snapshot - Discussion] Get New Discussion Entry Success',
  props<{ discussionEntry: IDiscussionEntry }>()
);
export const getNewDiscussionEntryFailure = createAction(
  '[Issue Snapshot - Discussion] Get New Discussion Entry Failure',
  props<Error>()
);

export const deleteDiscussionEntryAttachment = createAction(
  '[Issue Snapshot - Discussion] Delete Discussion Entry Attachment',
  props<{ attachmentDetails: IDeleteDiscussionEntryAttachment }>()
);

export const deleteDiscussionEntryKeyword = createAction(
  '[Issue Snapshot - Discussion] Delete Discussion Entry Keyword',
  props<{ keywordDetails: IDeleteDiscussionEntryKeyword }>()
);

export const saveDiscussionEntry = createAction(
  '[Issue Snapshot - Discussion] Save Discussion Entry',
  props<{ saveDetails: ISaveDiscussionEntry }>()
);
export const saveDiscussionEntrySuccess = createAction(
  '[Issue Snapshot - Discussion] Save Discussion Entry Success',
  props<{ savedDiscussionEntry: IDiscussionEntry }>()
);
export const saveDiscussionEntryFailure = createAction(
  '[Issue Snapshot - Discussion] Save Discussion Entry Failure',
  props<Error>()
);

export const cancelDiscussionEntryEdits = createAction(
  '[Issue Snapshot - Discussion] Cancel Discussion Entry Edits',
  props<{ discussionEntryID: string }>()
);

export const editDiscussionEntry = createAction(
  '[Issue Snapshot - Discussion] Edit Discussion Entry',
  props<{ discussionEntry: IDiscussionEntry }>()
);
export const editDiscussionEntryValues = createAction(
  '[Issue Snapshot - Discussion] Edit Discussion Entry Values',
  props<{ discussions: IDiscussion[]; discussionEntry: IDiscussionEntry }>()
);

export const deleteDiscussionEntry = createAction(
  '[Issue Snapshot - Discussion] Delete Discussion Entry',
  props<{ discussionEntryID: string }>()
);
export const deleteDiscussionEntrySuccess = createAction(
  '[Issue Snapshot - Discussion] Delete Discussion Entry Success',
  props<{ discussionEntryID: string }>()
);
export const deleteDiscussionEntryFailure = createAction(
  '[Issue Snapshot - Discussion] Delete Discussion Entry Failure',
  props<Error>()
);

export const notifyDiscussionSubscribers = createAction(
  '[Issue Snapshot - Discussion] Notify Discussion Subscribers',
  props<{ discussionEntry: IDiscussionEntry }>()
);

// Attachments
export const addDiscussionEntryAttachments = createAction(
  '[Issue Snapshot - Discussion] Add Discussion Entry Attachments',
  props<{ attachmentDetails: IAddDiscussionEntryAttachment[] }>()
);
export const addDiscussionEntryAttachmentsSuccess = createAction(
  '[Issue Snapshot - Discussion] Add Discussion Entry Attachments Success',
  props<{ attachments: IDiscussionAttachment[]; discussionEntryID: string }>()
);
export const addDiscussionEntryAttachmentsFailure = createAction(
  '[Issue Snapshot - Discussion] Add Discussion Entry Attachments Failure',
  props<Error>()
);
export const validateDiscussionEntryAttachments = createAction(
  '[Issue Snapshot - Discussion] Validate Discussion Entry Attachments',
  props<{ attachmentDetails: IAddDiscussionEntryAttachment[] }>()
);
export const validateDiscussionEntryAttachmentsFailure = createAction(
  '[Issue Snapshot - Discussion] Validate Discussion Entry Attachments Failure'
);

export const convertToBlob = createAction(
  '[Issue Snapshot - Discussion] Convert To Blob',
  props<{
    attachmentDetails: {
      entryID: string;
      attachment: IDiscussionAttachment;
      awsFile: IAWSFileUrl;
      saveAwsFile: IAWSFileUrl;
    }[];
    entryID: string;
  }>()
);
export const convertToBlobFailure = createAction(
  '[Issue Snapshot - Discussion] Convert To Blob Failure',
  props<Error>()
);

// Image Attachments
export const getImageAttachments = createAction(
  '[Issue Snapshot - Discussion] Get Image Attachments'
);
export const getImageAttachmentsSuccess = createAction(
  '[Issue Snapshot - Discussion] Get Image Attachments Success',
  props<{ imageDetails: IImageDetails[] }>()
);
export const getImageAttachmentsFailure = createAction(
  '[Issue Snapshot - Discussion] Get Image Attachment Failure',
  props<Error>()
);

// Search Keywords
export const searchTaggingKeyword = createAction(
  '[Issue Snapshot - Discussion] Search Tagging Keyword',
  props<{ searchDetails: IDiscussionEntryKeyword }>()
);
export const searchTaggingKeywordSuccess = createAction(
  '[Issue Snapshot - Discussion] Search Tagging Keyword Success',
  props<{ discussionEntryID: string; keywords: ITaggingKeyword[] }>()
);
export const searchTaggingKeywordFailure = createAction(
  '[Issue Snapshot - Discussion] Search Tagging Keyword Failure',
  props<{ discussionEntryID: string; error: Error }>()
);

// Keywords
export const addDiscussionEntryKeyword = createAction(
  '[Issue Snapshot - Discussion] Add Discussion Entry Keyword',
  props<{ addDetails: IDiscussionEntryKeyword }>()
);
// This action will be use when added tag is a reserved keyword
export const addDiscussionEntryKeywordFailure = createAction(
  '[Issue Snapshot - Discussion] Keyword is not allowed'
);
export const registerKeywordFailure = createAction(
  '[Issue Snapshot - Discussion] Register Tagging Keyword Failure',
  props<Error>()
);
export const updateDiscussionEntryKeywords = createAction(
  '[Issue Snapshot - Discussion] Update Discussion Entry Keywords',
  props<{ discussionEntryID: string; keyword: ITaggingKeyword }>()
);

// Summary
export const loadSummary = createAction(
  '[Issue Snapshot - Summary] Load Summary',
  props<{ issueGuid: string; issueSummary: string }>()
);

export const convertBVFilesToHTML = createAction(
  '[Issue Snapshot - Summary] Convert BV Files To HTML',
  props<{ issueSummary: string }>()
);

export const convertBVFilesToHTMLSuccess = createAction(
  '[Issue Snapshot - Summary] Convert BV Files To HTML Success',
  props<{ issueSummaryWithFiles: string }>()
);

export const convertBVFilesToHTMLFailure = createAction(
  '[Issue Snapshot - Summary] Convert BV Files To HTML Failure',
  props<Error>()
);

export const uploadSummaryFile = createAction(
  '[Issue Snapshot - Summary] Upload Summary File',
  props<{ assetID: number; file: any }>()
);
export const validateSummaryFile = createAction(
  '[Issue Snapshot - Summary] Validate Summary File',
  props<{ assetID: number; file: any }>()
);
export const validateSummaryFileFailure = createAction(
  '[Issue Snapshot - Summary] Validate Summary File Failure'
);

export const editSummary = createAction(
  '[Issue Snapshot - Summary] Edit Summary'
);

export const cancelEditSummary = createAction(
  '[Issue Snapshot - Summary] Cancel Edit Summary'
);

export const saveSummary = createAction(
  '[Issue Snapshot - Summary] Save Summary',
  props<{ summaryContent: string }>()
);

export const saveSummarySuccess = createAction(
  '[Issue Snapshot - Summary] Save Summary Success',
  props<{ summaryContent: string }>()
);

export const saveSummaryFailure = createAction(
  '[Issue Snapshot - Summary] Save Summary Failure',
  props<Error>()
);

// Asset Tree Dropdown
export const assetTreeDropdownStateChange = createAction(
  '[Issue Snapshot - Asset Tree Dropdown] Tree State Change',
  props<ITreeStateChange>()
);

export const assetTreeDropdownDataRequestFailure = createAction(
  '[Issue Snapshot - Asset Tree Dropdown] Tree Data Request Failure',
  props<Error>()
);

export const showRelatedIssuesByAsset = createAction(
  '[Issue Snapshot - Asset] Show Related Issues By Asset',
  props<{ assetUniqueKey: string }>()
);

export const showRelatedAlertsByAsset = createAction(
  '[Issue Snapshot - Asset] Show Related Alerts By Asset',
  props<{ assetUniqueKey: string }>()
);

export const clearAssetTreeDropdown = createAction(
  '[Issue Snapshot - Asset Tree Dropdown] Clear'
);

//Issue Creation - Mobile Page

export const saveMobileDiscussionEntry = createAction(
  '[Mobile Issue Snapshot - Discussion] Save Discussion Entry',
  props<{ entry: IDiscussionEntry }>()
);

//Work Request Info

export const loadWorkRequestInfo = createAction(
  '[Work Request - Issue] Load Work Request',
  props<{ issueId: string; forceReload: boolean }>()
);

export const loadWorkRequestInfoSuccess = createAction(
  '[Work Request - Issue] Load Work Request Success',
  props<{
    workRequestInfo: IWorkRequest;
  }>()
);

export const loadWorkRequestInfoFailure = createAction(
  '[Work Request - Issue] Load Work Request Failure',
  props<{ error: Error }>()
);

export const saveWorkRequest = createAction(
  '[Work Request - Issue] Save Work Request',
  props<{
    issueId: string;
    title: string;
    description: string;
    externalSystemAssetId: number;
    priority: string;
  }>()
);

export const saveWorkRequestSuccess = createAction(
  '[Work Request - Issue] Save Work Request Success'
);

export const saveWorkRequestFailure = createAction(
  '[Work Request - Issue] Save Work Request Failure',
  props<{ error: Error }>()
);

export const pollWorkRequestInfo = createAction(
  '[Work Request - Issue] Poll Work Request',
  props<{ issueId: string }>()
);

export const pollWorkRequestInfoSuccess = createAction(
  '[Work Request - Issue] Poll Work Request Success',
  props<{
    workRequestInfo: IWorkRequest;
  }>()
);

export const pollWorkRequestInfoFailure = createAction(
  '[Work Request - Issue] Poll Work Request Failure',
  props<{ error: Error }>()
);

//Associated External Asset
export const getAssociatedExternalAsset = createAction(
  '[Work Request - Issue] Get Associated External Asset',
  props<{ assetGuid: string }>()
);

export const getAssociatedExternalAssetSuccess = createAction(
  '[Work Request - Issue] Get Associated External Asset Success',
  props<{ externalAsset: ExternalAsset }>()
);

export const getAssociatedExternalAssetFailure = createAction(
  '[Work Request - Issue] Get Associated External Asset Failure',
  props<{ error: Error }>()
);

//Extenral Assets List/Hierarchy
export const getExternalAssets = createAction(
  '[Work Request - Issue] Get External Assets',
  props<{ assetGuid: string }>()
);

export const getExternalAssetsSuccess = createAction(
  '[Work Request - Issue] Get External Assets Success',
  props<{ externalAssets: ExternalAsset[] }>()
);

export const getExternalAssetsFailure = createAction(
  '[Work Request - Issue] Get External Assets Failure',
  props<{ error: Error }>()
);

export const clearExternalAssets = createAction(
  '[Work Request - Issue] Clear External Assets'
);

export const updateSelectedExternalAsset = createAction(
  '[Work Request - Issue] Update Selected External Asset',
  props<{ externalAsset: ExternalAsset }>()
);
