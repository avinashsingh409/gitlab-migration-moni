/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { IAssetIssueDonutData, ITreePermissions } from '@atonix/atx-core';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import {
  IGridParameters,
  IListState,
  ISavedIssueList,
} from '@atonix/shared/api';

export const summaryInitialize = createAction(
  '[Work Management Summary] Initialize',
  props<{ asset?: string }>()
);

export const workItemSelect = createAction(
  '[Work Management Summary] Work Item Select',
  props<{ id: string }>()
);

export const workItemAdd = createAction(
  '[Work Management Summary] Work Item Add'
);

export const workItemAddToAsset = createAction(
  '[Work Management Summary] Work Item Add To Asset',
  props<{ asset: string }>()
);

export const listViewDownloadRequest = createAction(
  '[Work Management Summary] List View Download Request',
  props<{ gridParams: IGridParameters }>()
);

export const listViewDownloadSuccess = createAction(
  '[Work Management Summary] List View Download Success'
);

export const listViewDownloadFailure = createAction(
  '[Work Management Summary] List View Download Failure',
  props<Error>()
);

export const permissionsRequest = createAction(
  '[Work Management Summary] Permissions Request'
);
export const permissionsRequestSuccess = createAction(
  '[Work Management Summary] Permissions Request Success',
  props<ITreePermissions>()
);
export const permissionsRequestFailure = createAction(
  '[Work Management Summary] Permissions Request Failure',
  props<Error>()
);

export const selectAsset = createAction(
  '[Work Management Summary] Select Asset',
  props<{ asset: string }>()
);

export const treeStateChange = createAction(
  '[Work Management Summary] Tree State Change',
  props<ITreeStateChange>()
);
export const assetTreeDataRequestFailure = createAction(
  '[Work Management Summary] Tree Data Request Failure',
  props<Error>()
);

export const donutsRequest = createAction(
  '[Work Management Summary] Donuts Request',
  props<{ state: IServerSideGetRowsRequest }>()
);
export const donutsSuccess = createAction(
  '[Work Management Summary] Donuts Success',
  props<{ values: IAssetIssueDonutData; asset: string; currency: string }>()
);
export const donutsFailure = createAction(
  '[Work Management Summary] Donuts Failure',
  props<Error>()
);

// This action comes from when the "others" section in the donut is selected
export const leftDonutOthersSelected = createAction(
  '[Work Management Summary] Left Donut Chart "Others" selected',
  props<{ IDtoAppend: string }>()
);
// This action comes from when the "others" section in the donut is selected
export const middleDonutOthersSelected = createAction(
  '[Work Management Summary] Middle Donut Chart "Others" selected',
  props<{ IDtoAppend: string }>()
);
// This action comes from when the "others" section in the donut is selected
export const rightDonutOthersSelected = createAction(
  '[Work Management Summary] Right Donut Chart "Others" selected',
  props<{ IDtoAppend: string }>()
);

export const leftDonutRefreshed = createAction(
  '[Work Management Summary] Left Donut Chart Refreshed',
  props<{ IDtoAppend: string }>()
);

export const middleDonutRefreshed = createAction(
  '[Work Management Summary] Middle Donut Chart Refreshed',
  props<{ IDtoAppend: string }>()
);

export const rightDonutRefreshed = createAction(
  '[Work Management Summary] Right Donut Chart Refreshed',
  props<{ IDtoAppend: string }>()
);

export const treeSizeChange = createAction(
  '[Work Management Summary] Asset Tree Size Change',
  props<{ value: number }>()
);

export const listStateChange = createAction(
  '[Work Management Summary] List State Change',
  props<{ listState: IListState }>()
);

export const listsLoad = createAction('[Work Management Summary] Lists Load');

export const listsLoadSuccess = createAction(
  '[Work Management Summary] Load Lists Success',
  props<{ lists: ISavedIssueList[] }>()
);

export const listsLoadFailure = createAction(
  '[Work Management Summary] Load Lists Failure'
);

export const listSave = createAction(
  '[Work Management Summary] List Save',
  props<{ list: Partial<ISavedIssueList> }>()
);

export const listSaveSuccess = createAction(
  '[Work Management Summary] List Save Success',
  props<{ list: ISavedIssueList }>()
);

export const listSaveFailure = createAction(
  '[Work Management Summary] List Save Failure',
  props<{ list: ISavedIssueList }>()
);

export const listLoad = createAction(
  '[Work Management Summary] List Load',
  props<{ id: number }>()
);

export const listDelete = createAction(
  '[Work Management Summary] List Delete',
  props<{ id: number }>()
);

export const listDeleteSuccess = createAction(
  '[Work Management Summary] List Delete Success',
  props<{ id: number }>()
);

export const listDeleteFailure = createAction(
  '[Work Management Summary] List Delete Failure',
  props<{ id: number }>()
);

export const toggleFloatingFilter = createAction(
  '[Work Management Summary] Toggle Floating Filter',
  props<{ filterOn?: boolean }>()
);
export const loadDefaultList = createAction(
  '[Work Management Summary] Load Default List'
);

export const setScorecard = createAction(
  '[Work Management Summary] Set Scorecard',
  props<{ assetIssueID: number; include: boolean }>()
);
export const setScorecardSuccess = createAction(
  '[Work Management Summary] Set Scorecard Success',
  props<{ assetIssueID: number; include: boolean }>()
);
export const setScorecardFailure = createAction(
  '[Work Management Summary] Set Scorecard Failure',
  props<{ error: Error }>()
);
