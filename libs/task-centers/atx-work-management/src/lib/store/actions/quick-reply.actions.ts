/* eslint-disable ngrx/prefer-inline-action-props */
import { IAWSFileUrl, IQuickReplyAttachment } from '@atonix/atx-core';
import { props, createAction } from '@ngrx/store';

export const initializeQuickReply = createAction('[Quick Reply] Initialize');

// Quick Reply Attachments
export const addQuickReplyAttachments = createAction(
  '[Quick Reply] Add Quick Reply Attachments',
  props<{ files: any[]; assetID: number }>()
);
export const addQuickReplyAttachmentsSuccess = createAction(
  '[Quick Reply] Add Quick Reply Attachments Success',
  props<{
    attachments: IQuickReplyAttachment[];
  }>()
);
export const addQuickReplyAttachmentsFailure = createAction(
  '[Quick Reply] Add Quick Reply Attachments Failure',
  props<Error>()
);

export const removeQuickReplyAttachment = createAction(
  '[Quick Reply] Remove Quick Reply Attachment',
  props<{ index: number }>()
);

export const clearQuickReplyAttachments = createAction(
  '[Quick Reply] Clear Quick Reply Attachments'
);
