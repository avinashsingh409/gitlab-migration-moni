/* eslint-disable max-len */
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LicenseManager } from '@ag-grid-enterprise/core';
import { AgGridModule } from '@ag-grid-community/angular';
LicenseManager.setLicenseKey(
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);
import { CommonModule } from '@angular/common';
import { NavigationModule } from '@atonix/atx-navigation';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { SummaryComponent } from './component/summary/summary.component';
import { ListViewComponent } from './component/list-view/list-view.component';
import { SavedToolPanelComponent } from './component/saved-tool-panel/saved-tool-panel.component';
import { workManagementReducers } from './store/reducers/work-management.reducer';
import { SummaryEffects } from './store/effects/summary.effects';
import { SummaryFacade } from './store/facade/summary.facade';
import { ScorecardFormatterComponent } from './component/list-view/scorecard-formatter/scorecard-formatter.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { RouterModule } from '@angular/router';
import { AtxMaterialModule } from '@atonix/atx-material';
import { IssueSnapshotComponent } from './component/issue-snapshot/issue-snapshot.component';
import { NewIssueModalComponent } from './component/new-issue-modal/new-issue-modal.component';
import { DiscussionModule } from '@atonix/atx-discussion';
import { IssueSnapshotEffects } from './store/effects/issue-snapshot.effects';
import { IssueSnapshotFacade } from './store/facade/issue-snapshot.facade';
import { MomentModule } from 'ngx-moment';
import { IssueModalFacade } from './service/issue-modal.facade';
import { SafeHTMLIssuePipe } from './component/issue-snapshot/safe-html.pipe';
import { IssueSummaryEditorComponent } from './component/issue-summary-editor/issue-summary-editor.component';
import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { DragDropDirective } from './component/issue-summary-editor/drag-drop';
import { ImpactCalculatorComponent } from './component/impact-calculator/impact-calculator.component';
import { UpdateIssueCategoryModalComponent } from './component/update-issue-category-modal/update-issue-category-modal.component';
import { SaveChangesModalComponent } from './component/save-changes-modal/save-changes-modal.component';
import { DeleteIssueModalComponent } from './component/delete-issue-modal/delete-issue-modal.component';
import { ImpactCalculatorInvalidModalComponent } from './component/impact-calculator-invalid-modal/impact-calculator-invalid-modal.component';
import { SharedUiModule } from '@atonix/shared/ui';
import { SharedApiModule } from '@atonix/shared/api';
import { SendIssueDialogComponent } from './component/send-issue-dialog/send-issue-dialog.component';
import { SendIssueModalFacade } from './service/send-issue-modal.facade';
import { ManageFollowersDialogComponent } from './component/manage-followers-dialog/manage-followers-dialog.component';
import { ManageFollowersModalFacade } from './service/manage-followers-modal.facade';
import { MfPromptDialogComponent } from './component/manage-followers-dialog/mf-prompt-dialog/mf-prompt-dialog/mf-prompt-dialog.component';
import { QuickReplyComponent } from './component/quick-reply/quick-reply.component';
import { QuickReplyEffects } from './store/effects/quick-reply.effects';
import { QuickReplyFacade } from './store/facade/quick-reply.facade';
import { AuthFacade } from '@atonix/shared/state/auth';
import { IssueSnapshotMobileComponent } from './component/issue-snapshot-mobile/issue-snapshot-mobile.component';
import { MobileIssueSnapshotFacade } from './service/issue-snapshot-mobile/issue-snapshot-mobile.facade';
import { CreateWorkOrderDialogComponent } from './component/create-work-order-dialog/create-work-order-dialog.component';
import { AtxChartModuleV2 } from '@atonix/atx-chart-v2';
import { PreCwrDialogComponent } from './component/pre-cwr-dialog/pre-cwr-dialog.component';

@NgModule({
  declarations: [
    DragDropDirective,
    SafeHTMLIssuePipe,
    SummaryComponent,
    IssueSnapshotComponent,
    ListViewComponent,
    SavedToolPanelComponent,
    ScorecardFormatterComponent,
    NewIssueModalComponent,
    IssueSummaryEditorComponent,
    ImpactCalculatorComponent,
    QuickReplyComponent,
    UpdateIssueCategoryModalComponent,
    SaveChangesModalComponent,
    DeleteIssueModalComponent,
    ImpactCalculatorInvalidModalComponent,
    SendIssueDialogComponent,
    ManageFollowersDialogComponent,
    MfPromptDialogComponent,
    IssueSnapshotMobileComponent,
    CreateWorkOrderDialogComponent,
    PreCwrDialogComponent,
  ],
  imports: [
    CommonModule,
    NavigationModule,
    AtxMaterialModule,
    AtxChartModuleV2,
    SharedUiModule,
    MomentModule.forRoot({
      relativeTimeThresholdOptions: {
        m: 59,
      },
    }),
    RouterModule.forChild([
      { path: '', component: SummaryComponent, pathMatch: 'full' },
      {
        path: 'i',
        component: IssueSnapshotComponent,
      },
      {
        path: 'ic',
        component: ImpactCalculatorComponent,
      },
      {
        path: 'qr',
        component: QuickReplyComponent,
      },
      {
        path: 'mobile',
        component: IssueSnapshotMobileComponent,
      },
    ]),
    AssetTreeModule,
    ReactiveFormsModule,
    ClipboardModule,
    DiscussionModule,
    AgGridModule.withComponents([
      SavedToolPanelComponent,
      ScorecardFormatterComponent,
    ]),
    StoreModule.forFeature('workmanagement', workManagementReducers),
    EffectsModule.forFeature([
      SummaryEffects,
      IssueSnapshotEffects,
      QuickReplyEffects,
    ]),
    EditorModule,
    SharedApiModule,
  ],
  providers: [
    SummaryFacade,
    IssueSnapshotFacade,
    IssueModalFacade,
    SendIssueModalFacade,
    ManageFollowersModalFacade,
    QuickReplyFacade,
    AuthFacade,
    MobileIssueSnapshotFacade,
    { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' },
  ],
})
export class WorkManagementModule {}
