import { IProcessedTrend } from '@atonix/atx-core';

export function getSelectedTrendID(trends: IProcessedTrend[]) {
  let result: IProcessedTrend = null;
  if (trends && trends.length > 0) {
    result = trends.filter((n) => n.totalSeries > 0)[0] || trends[0];
  }
  return result?.id;
}
