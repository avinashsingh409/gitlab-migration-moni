import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ITreeStateChange, ITreeConfiguration } from '@atonix/atx-asset-tree';
import {
  ITimeSliderState,
  ITimeSliderStateChange,
} from '@atonix/atx-time-slider';
import { IBtnGrpStateChange, IUpdateLimitsData } from '@atonix/atx-chart';
import {
  NavFacade,
  IButtonData,
  getTrendIdFromRoute,
  getStartFromRoute,
  getEndFromRoute,
  getUniqueIdFromRoute,
} from '@atonix/atx-navigation';

import { AssetTreeFacade } from '../../store/facade/asset-tree.facade';
import { TimeSliderFacade } from '../../store/facade/time-slider.facade';
import { DataExplorerFacade } from '../../store/facade/data-explorer.facade';
import { ChartFacade } from '../../store/facade/chart.facade';
import { IToast } from '../../store/state/chart-state';
import { ActivatedRoute } from '@angular/router';
import { LoggerService, ToastService } from '@atonix/shared/utils';
import { IProcessedTrend } from '@atonix/atx-core';

@Component({
  selector: 'atx-data-explorer',
  templateUrl: './data-explorer.component.html',
  styleUrls: ['./data-explorer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataExplorerComponent implements OnInit, OnDestroy {
  public assetTreeConfiguration$: Observable<ITreeConfiguration>;
  public unsubscribe$ = new Subject<void>();
  public navPaneState$: Observable<'hidden' | 'open' | 'expanded'>;
  public toggleTimeSlider$: Observable<'hidden' | 'none' | 'show'>;
  public assetTreeSelected$: Observable<boolean>;
  public layoutMode$: Observable<'over' | 'push' | 'side'>;
  public leftTraySize$: Observable<number>;
  public launchPadItems$: Observable<IButtonData[]>;
  public showLaunchPad$: Observable<boolean>;
  public assetTreeButton$: Observable<IButtonData>;
  public breadcrumbMenu$: Observable<IButtonData>;
  public theme$: Observable<string>;
  public timeSliderState$: Observable<ITimeSliderState>;
  public showTimeSlider$: Observable<boolean>;

  public trendsLoading$: Observable<boolean>;
  public dataLoading$: Observable<boolean>;
  public noDataFound$: Observable<boolean>;

  public dataExplorerTrends$: Observable<IProcessedTrend[]>;
  public selectedTrendID$: Observable<string>;
  public selectedTrend$: Observable<IProcessedTrend>;

  public selectToast$: Observable<IToast>;

  public initialTrendIdFromRoute: string;
  public initialStartFromRoute: string;
  public initialEndFromRoute: string;

  constructor(
    private navFacade: NavFacade,
    private assetTreeFacade: AssetTreeFacade,
    private timeSliderFacade: TimeSliderFacade,
    private dataExplorerFacade: DataExplorerFacade,
    private chartFacade: ChartFacade,
    private snackBarService: ToastService,
    private logger: LoggerService,
    private activatedRoute: ActivatedRoute
  ) {
    this.initialTrendIdFromRoute = getTrendIdFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.initialStartFromRoute = getStartFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );
    this.initialEndFromRoute = getEndFromRoute(
      this.activatedRoute.snapshot.queryParamMap
    );

    this.dataExplorerFacade.initializeDataExplorer(
      getUniqueIdFromRoute(this.activatedRoute.snapshot.queryParamMap),
      this.initialStartFromRoute
        ? new Date(parseFloat(this.initialStartFromRoute))
        : this.getAMonthAgo(),
      this.initialEndFromRoute
        ? new Date(parseFloat(this.initialEndFromRoute))
        : this.getToday()
    );

    this.navPaneState$ = this.navFacade.navPaneState$;
    this.theme$ = this.navFacade.theme$;
    this.assetTreeConfiguration$ = this.assetTreeFacade.assetTreeConfiguration$;
    this.timeSliderState$ = this.timeSliderFacade.timeSliderState$;
    this.trendsLoading$ = this.chartFacade.trendsLoading$;
    this.dataLoading$ = this.chartFacade.dataLoading$;
    this.noDataFound$ = this.chartFacade.noDataFound$;
    this.dataExplorerTrends$ = this.chartFacade.dataExplorerTrends$;
    this.selectedTrendID$ = this.chartFacade.selectedDataExplorerTrendID$;
    this.selectedTrend$ = this.chartFacade.selectedDataExplorerTrend$;
    this.showTimeSlider$ = this.navFacade.showTimeSlider$;
    this.assetTreeSelected$ = this.navFacade.assetTreeSelected$;
    this.layoutMode$ = this.dataExplorerFacade.layoutMode$;
    this.leftTraySize$ = this.dataExplorerFacade.leftTraySize$;
    this.breadcrumbMenu$ = this.navFacade.breadcrumb$;

    this.selectToast$ = this.dataExplorerFacade.selectToast$;
    this.dataExplorerFacade.selectedAsset$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        if (id) {
          this.assetTreeFacade.selectAsset(id);
          this.chartFacade.loadDataExplorerCharts(
            id,
            this.initialTrendIdFromRoute,
            new Date(this.initialStartFromRoute),
            new Date(this.initialEndFromRoute)
          );
          this.initialTrendIdFromRoute = null;
          this.initialStartFromRoute = null;
          this.initialEndFromRoute = null;
        }
      });

    this.chartFacade.selectedDataExplorerTrendID$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((id) => {
        if (id) {
          this.dataExplorerFacade.updateRoute();
        }
      });

    this.timeSliderFacade.timeSliderSelection$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((date) => {
        if (date) {
          this.dataExplorerFacade.updateRoute();
        }
      });

    this.selectToast$.pipe(takeUntil(this.unsubscribe$)).subscribe((data) => {
      if (data) {
        this.snackBarService.openSnackBar(data.message, data.type);
      }
    });
  }

  ngOnInit(): void {
    // This works because it triggers the Highcharts to reflow.
    // it only relies on the actual window resizing thus manually dispatching this
    this.navPaneState$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.chartFacade.reflow();
    });
    this.showTimeSlider$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.chartFacade.reflow();
    });
    this.assetTreeSelected$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.chartFacade.reflow();
    });
    this.layoutMode$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.chartFacade.reflow();
    });
    this.dataLoading$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.chartFacade.reflow();
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  public onAssetTreeSizeChange(newSize: number) {
    if (newSize !== null && newSize !== undefined && newSize >= 0) {
      this.assetTreeFacade.treeSizeChange(newSize);
    }
  }

  // This is the thing that actually makes changes to the asset tree state.  We handle what we
  // need to handle and then call the default behavior.
  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.assetTreeFacade.treeStateChange(change);
  }

  public onTimeSliderStateChange(change: ITimeSliderStateChange) {
    this.timeSliderFacade.timeSliderStateChange(change);
    if (change.event === 'HideTimeSlider') {
      this.timeSliderFacade.hideTimeSlider();
    } else if (change.event === 'SelectDateRange') {
      this.chartFacade.loadData(
        change.newStartDateValue,
        change.newEndDateValue
      );
      this.logger.feature(
        'selectdaterange_' + (change.source ?? 'any'),
        'dataexplorer'
      );
    } else if (change.event === 'ToggleCalendarIndicator') {
      this.logger.feature('togglecalendarindicator', 'dataexplorer');
    } else if (change.event === 'SelectDate') {
      this.logger.feature(
        'selectdate_' + (change.source ?? 'any'),
        'dataexplorer'
      );
    } else if (change.event === 'ToggleLive') {
      this.logger.feature('togglelive', 'dataexplorer');
    } else if (change.event === 'RangeShift') {
      this.logger.feature('rangeshift', 'dataexplorer');
    } else if (change.event === 'Zoom') {
      this.logger.feature('zoom', 'dataexplorer');
    }
  }

  public onBtnGrpStateChange(change: IBtnGrpStateChange) {
    if (change.event === 'ChangeLabels') {
      this.chartFacade.changeLabelsClicked();
    }
    if (change.event === 'CopyLink') {
      const link = change.newValue as string;
      this.chartFacade.copyLink(link);
    }
    if (change.event === 'DownloadChart') {
      const config = change.newConfig as Highcharts.Options;
      this.chartFacade.downloadChart(config);
    }
    if (change.event === 'EditChart') {
      this.chartFacade.editChart();
    }

    if (change.event === 'UpdateLimits') {
      this.chartFacade.updateChartYAxisLimit(
        change.newValue as IUpdateLimitsData
      );
    }
    if (change.event === 'ResetLimits') {
      this.chartFacade.resetChartYAxisLimit(change.newValue as number);
    }
  }

  public trendSelected(trendId: string) {
    this.chartFacade.selectDataExplorerTrend(trendId);
  }

  private getToday() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
  }

  private getAMonthAgo() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth() - 1, d.getDate(), 0, 0, 0, 0);
  }
  public onChangeLabels() {
    this.chartFacade.changeLabelsClicked();
  }

  public mainNavclicked() {
    this.navFacade.configureNavigationButton('asset_tree', {
      selected: false,
    });
  }
}
