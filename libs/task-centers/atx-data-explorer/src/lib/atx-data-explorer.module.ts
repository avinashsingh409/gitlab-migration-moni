import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ChartModule } from '@atonix/atx-chart';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NavigationModule } from '@atonix/atx-navigation';
import { dataExplorerReducer } from './store/reducers/data-explorer.reducer';
import { AssetTreeFacade } from './store/facade/asset-tree.facade';
import { TimeSliderModule } from '@atonix/atx-time-slider';
import { DataExplorerFacade } from './store/facade/data-explorer.facade';
import { TimeSliderFacade } from './store/facade/time-slider.facade';
import { ChartEffects } from './store/effects/chart.effects';
import { ChartFacade } from './store/facade/chart.facade';
import { AssetTreeEffects } from './store/effects/asset-tree.effects';
import { DataExplorerComponent } from './component/data-explorer/data-explorer.component';
import { DataExplorerEffects } from './store/effects/data-explorer.effects';
import { AtxMaterialModule } from '@atonix/atx-material';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [DataExplorerComponent],
  imports: [
    CommonModule,
    ChartModule,
    AssetTreeModule,
    FlexLayoutModule,
    TimeSliderModule,
    NavigationModule,
    AtxMaterialModule,
    StoreModule.forFeature('dataexplorer', dataExplorerReducer),
    RouterModule.forChild([
      {
        path: '',
        component: DataExplorerComponent,
        pathMatch: 'full',
      },
    ]),
    EffectsModule.forFeature([
      DataExplorerEffects,
      AssetTreeEffects,
      ChartEffects,
    ]),
  ],
  providers: [
    AssetTreeFacade,
    DataExplorerFacade,
    TimeSliderFacade,
    ChartFacade,
  ],
  exports: [DataExplorerComponent],
})
export class DataExplorerModule {}
