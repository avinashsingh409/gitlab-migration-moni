import { createSelector, createFeatureSelector } from '@ngrx/store';
import {
  getIDFromSelectedAssets,
  getSelectedNodes,
  TrayState,
} from '@atonix/atx-asset-tree';
import { IDataExplorerState } from '../state/data-explorer-state';
import { adapter } from '../state/chart-state';
import { RouteReducerState } from '../state/router.state';
import { IProcessedTrend } from '@atonix/atx-core';

// get the selectors
const { selectAll } = adapter.getSelectors();

// This is actually grabbing the application state.  It doesn't really
// know what the full application state is, it
export const selectApp = (state: {
  dataexplorer: IDataExplorerState;
  asset?: any;
}) => state;

export const selectDataExplorerState =
  createFeatureSelector<IDataExplorerState>('dataexplorer');

export const selectAssetTreeState = createSelector(
  selectDataExplorerState,
  (state) => state.AssetTreeState
);

export const selectAssetTreeConfiguration = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration
);

export const selectAssetTreePin = createSelector(
  selectAssetTreeState,
  (state) => state.treeConfiguration.pin
);

export const selectLayoutMode = createSelector(selectAssetTreePin, (pin) => {
  const result: TrayState = pin ? 'side' : 'over';
  return result;
});

export const selectSelectedAsset = createSelector(
  selectAssetTreeState,
  (state) => getIDFromSelectedAssets(getSelectedNodes(state.treeNodes))
);

export const selectSelectedAssetTreeNode = createSelector(
  selectAssetTreeState,
  selectSelectedAsset,
  (state, asset) =>
    state?.treeConfiguration?.nodes?.find((a) => a?.uniqueKey === asset)
);

export const selectSelectedAssetKey = createSelector(
  selectSelectedAssetTreeNode,
  (state) => {
    const data = state?.data;
    if (data) {
      return (
        data.TreeId +
        '~' +
        data.ParentNodeId +
        '~' +
        data.NodeId +
        '~' +
        data.AssetGuid
      );
    }
    return null;
  }
);

export const selectTimeSliderState = createSelector(
  selectDataExplorerState,
  (state) => state.TimeSliderState
);

export const selectTimeSliderSelection = createSelector(
  selectTimeSliderState,
  (state) => {
    return { start: state?.startDate, end: state?.endDate };
  }
);

export const selectBeginningDate = createSelector(
  selectTimeSliderState,
  (state) => state.startDate
);

export const selectEndingDate = createSelector(
  selectTimeSliderState,
  (state) => state.endDate
);

export const selectChartState = createSelector(
  selectDataExplorerState,
  (state) => state.ChartState
);

export const selectDataExplorerTrends = createSelector(
  selectChartState,
  selectAll
);

export const selectNoDataFound = createSelector(
  [selectSelectedAsset, selectDataExplorerTrends],
  (assetTreeNodeID: string, trends: IProcessedTrend[]) => {
    if (assetTreeNodeID && (!trends || trends.length === 0)) {
      return true;
    }
    return false;
  }
);

export const selectSelectedDataExplorerTrend = createSelector(
  selectChartState,
  (state) => {
    let selectedTrend: IProcessedTrend = null;
    if (
      state?.selectedDataExplorerTrendID &&
      state?.entities[state.selectedDataExplorerTrendID]
    ) {
      selectedTrend = state.entities[state.selectedDataExplorerTrendID];
    }
    return selectedTrend;
  }
);

export const selectSelectedTrendIsGroupedSeries = createSelector(
  selectSelectedDataExplorerTrend,
  (state) => {
    if (state?.trendDefinition?.BandAxis) {
      return true;
    } else {
      return false;
    }
  }
);

export const selectSelectedTrendID = createSelector(
  selectChartState,
  (state) => state.selectedDataExplorerTrendID
);
export const selectLoadingTrends = createSelector(
  selectChartState,
  (status) => status.loadingTrends
);
export const selectLoadingData = createSelector(
  selectChartState,
  (state) => state.loadingData && !state.loadingTrends
);
export const selectToast = createSelector(
  selectDataExplorerState,
  (state) => state?.Toast
);

export const leftTraySize = createSelector(
  selectDataExplorerState,
  (state) => state.LeftTraySize
);

export const selectRouterReducerState =
  createFeatureSelector<RouteReducerState>('router');

// Router Selectors
export const selectRouterState = createSelector(
  selectRouterReducerState,
  (routerReducerState) => routerReducerState?.state
);

export const selectRouterRoute = createSelector(
  selectRouterState,
  (routerState) => routerState?.url
);

// This grabs the state of the selected asset
export const assetState = createSelector(
  selectApp,
  (state: any) => state?.asset?.asset
);
