/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IDataExplorerState } from '../state/data-explorer-state';
import {
  selectChartState,
  selectDataExplorerTrends,
  selectSelectedDataExplorerTrend,
  selectSelectedTrendID,
  selectLoadingTrends,
  selectLoadingData,
  selectNoDataFound,
} from '../selectors/data-explorer.selector';
import {
  selectDataExplorerTrend,
  loadDataExplorerCharts,
  getMeasurementsSet,
  reflow,
  copyLink,
  downloadChart,
  changeLabelsClicked,
  editChart,
  updateChartYAxisLimit,
  resetChartYAxisLimit,
} from '../actions/chart.actions';
import { IUpdateLimitsData } from '@atonix/atx-chart';

@Injectable()
export class ChartFacade {
  constructor(private store: Store<IDataExplorerState>) {}
  dataExplorerState$ = this.store.pipe(select(selectChartState));
  dataExplorerTrends$ = this.store.pipe(select(selectDataExplorerTrends));
  selectedDataExplorerTrend$ = this.store.pipe(
    select(selectSelectedDataExplorerTrend)
  );
  selectedDataExplorerTrendID$ = this.store.pipe(select(selectSelectedTrendID));
  trendsLoading$ = this.store.pipe(select(selectLoadingTrends));
  dataLoading$ = this.store.pipe(select(selectLoadingData));
  noDataFound$ = this.store.pipe(select(selectNoDataFound));

  selectDataExplorerTrend(trendID: string) {
    this.store.dispatch(selectDataExplorerTrend({ trendID }));
  }
  loadDataExplorerCharts(
    nodeID: string,
    trendID: string,
    startDate?: Date,
    endDate?: Date
  ) {
    this.store.dispatch(
      loadDataExplorerCharts({ nodeID, trendID, startDate, endDate })
    );
  }
  loadData(startDate: Date, endDate: Date) {
    this.store.dispatch(
      getMeasurementsSet({ trendID: null, trend: null, startDate, endDate })
    );
  }
  reflow() {
    this.store.dispatch(reflow());
  }
  copyLink(link: string) {
    this.store.dispatch(copyLink({ link }));
  }
  downloadChart(config: Highcharts.Options) {
    this.store.dispatch(downloadChart({ config }));
  }
  changeLabelsClicked() {
    this.store.dispatch(changeLabelsClicked());
  }
  editChart() {
    this.store.dispatch(editChart());
  }

  updateChartYAxisLimit(limitsData: IUpdateLimitsData) {
    this.store.dispatch(updateChartYAxisLimit({ limitsData }));
  }

  resetChartYAxisLimit(axisIndex: number) {
    this.store.dispatch(resetChartYAxisLimit({ axisIndex }));
  }
}
