/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  leftTraySize,
  selectToast,
  selectLayoutMode,
  selectSelectedAsset,
} from '../selectors/data-explorer.selector';
import * as actions from '../actions/data-explorer.action';

@Injectable()
export class DataExplorerFacade {
  // This needs to be a Store<any> so that it will grab the root asset
  // state rather than the feature state.
  constructor(private store: Store<any>) {}

  layoutMode$ = this.store.pipe(select(selectLayoutMode));
  leftTraySize$ = this.store.pipe(select(leftTraySize));
  selectToast$ = this.store.pipe(select(selectToast));

  // Grab the selected asset from the store.  This actually looks at the
  // data in the asset tree and determines the selected asset.
  selectedAsset$ = this.store.pipe(select(selectSelectedAsset));

  initializeDataExplorer(asset: string, start: Date, end: Date) {
    this.store.dispatch(actions.dataExplorerInitialize({ asset, start, end }));
  }

  updateRoute() {
    this.store.dispatch(actions.updateRoute());
  }
}
