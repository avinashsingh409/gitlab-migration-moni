/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IDataExplorerState } from '../state/data-explorer-state';
import {
  selectAssetTreeConfiguration,
  selectSelectedAsset,
} from '../selectors/data-explorer.selector';
import * as assetTreeActions from '../actions/asset-tree.actions';
import { ITreeStateChange } from '@atonix/atx-asset-tree';

@Injectable()
export class AssetTreeFacade {
  constructor(private store: Store<IDataExplorerState>) {}

  assetTreeConfiguration$ = this.store.pipe(
    select(selectAssetTreeConfiguration)
  );
  selectedAsset$ = this.store.pipe(select(selectSelectedAsset));

  selectAsset(asset: string) {
    this.store.dispatch(assetTreeActions.selectAsset({ asset }));
  }

  treeStateChange(change: ITreeStateChange) {
    this.store.dispatch(assetTreeActions.treeStateChange(change));
  }

  treeSizeChange(value: number) {
    this.store.dispatch(assetTreeActions.treeSizeChange({ value }));
  }
}
