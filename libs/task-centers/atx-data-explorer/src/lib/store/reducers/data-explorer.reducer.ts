import { Update } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import {
  ITreeState,
  ITreeStateChange,
  alterAssetTreeState,
  setPermissions,
  treeRetrievedStateChange,
} from '@atonix/atx-asset-tree';
import {
  ITimeSliderStateChange,
  alterTimeSliderState,
  getDefaultTimeSlider,
  setJumpTo,
} from '@atonix/atx-time-slider';
import { IUpdateLimitsData, resetAxes, updateAxes } from '@atonix/atx-chart';

import * as assetTreeActions from '../actions/asset-tree.actions';
import * as timeSliderActions from '../actions/time-slider.actions';
import * as chartActions from '../actions/chart.actions';
import {
  IDataExplorerState,
  initialDataExplorerState,
} from '../state/data-explorer-state';
import { adapter } from '../state/chart-state';
import { ICriteria, IProcessedTrend, ITreePermissions } from '@atonix/atx-core';

// eslint-disable-next-line @typescript-eslint/naming-convention, no-underscore-dangle, id-blacklist, id-match
const _dataExplorerReducer = createReducer(
  initialDataExplorerState,
  on(assetTreeActions.getPermissions, getPermissions),
  on(assetTreeActions.getPermissionsSuccess, getPermissionsSuccess),
  on(assetTreeActions.getPermissionsFailure, getPermissionsFailure),
  on(assetTreeActions.treeStateChange, treeStateChange),
  on(assetTreeActions.getAssetTreeDataFailure, treeDataFailed),
  on(assetTreeActions.treeSizeChange, treeSizeChange),
  on(timeSliderActions.initializeTimeSlider, initializeTimeSliderState),
  on(timeSliderActions.timeSliderStateChange, timeSliderStateChange),
  on(chartActions.loadDataExplorerCharts, loadDataExplorerCharts),
  on(chartActions.loadDataExplorerChartsSuccess, loadDataExplorerChartsSuccess),
  on(chartActions.loadDataExplorerChartsFailure, loadDataExplorerChartsFailure),
  on(chartActions.selectDataExplorerTrend, selectedDataExplorerTrend),
  on(chartActions.getMeasurementsSet, getMeasurementsSet),
  on(chartActions.getMeasurementsSetSuccess, getMeasurementsSetSuccess),
  on(chartActions.getMeasurementsSetFailure, getMeasurementsSetFailure),
  on(chartActions.copyLinkSuccess, copyLinkSuccess),
  on(chartActions.copyLinkFailure, copyLinkFailure),
  on(chartActions.changeLabelsClicked, changeLabelsClicked),
  on(chartActions.updateChartYAxisLimit, updateChartYAxisLimits),
  on(chartActions.resetChartYAxisLimit, resetChartYAxisLimit)
);

// Asset Tree
export function getPermissions(state: IDataExplorerState) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsSuccess(
  state: IDataExplorerState,
  payload: ITreePermissions
) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(state.AssetTreeState.treeConfiguration, {
      canView: payload.canView,
      canEdit: false,
      canDelete: false,
      canAdd: false,
    }),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function getPermissionsFailure(state: IDataExplorerState) {
  const treeState: ITreeState = {
    ...state.AssetTreeState,
    treeConfiguration: setPermissions(
      state.AssetTreeState.treeConfiguration,
      null
    ),
    treeNodes: { ...state.AssetTreeState.treeNodes },
  };
  return { ...state, AssetTreeState: treeState };
}

export function treeStateChange(
  state: IDataExplorerState,
  payload: ITreeStateChange
) {
  const newPayload = treeRetrievedStateChange(payload);
  return {
    ...state,
    AssetTreeState: alterAssetTreeState(state.AssetTreeState, newPayload),
  };
}
export function timeSliderStateChange(
  state: IDataExplorerState,
  payload: ITimeSliderStateChange
) {
  return {
    ...state,
    TimeSliderState: alterTimeSliderState(state.TimeSliderState, payload),
  };
}
export function treeDataFailed(state: IDataExplorerState, payload: Error) {
  return { ...state };
}

export function treeSizeChange(
  state: IDataExplorerState,
  payload: { value: number }
) {
  return { ...state, LeftTraySize: payload.value };
}

// Time Slider
export function initializeTimeSliderState(
  state: IDataExplorerState,
  payload: { start: Date; end: Date }
) {
  return {
    ...state,
    TimeSliderState: getDefaultTimeSlider({
      dateIndicator: 'Range',
      startDate: payload.start,
      endDate: payload.end,
    }),
  };
}

export function loadDataExplorerCharts(
  state: IDataExplorerState
): IDataExplorerState {
  return {
    ...state,
    ChartState: {
      ...adapter.removeAll(state.ChartState),
      selectedDataExplorerTrendID: null,
      loadingTrends: true,
    },
  };
}

export function loadDataExplorerChartsSuccess(
  state: IDataExplorerState,
  payload: { trends: IProcessedTrend[] }
): IDataExplorerState {
  const trends = payload.trends.map((trend) => {
    const trendDefinition = { ...trend.trendDefinition };

    const Axes = [...trendDefinition.Axes].map((axis) => {
      const newAxis = { ...axis };
      newAxis.OriginalMin = newAxis.Min;
      newAxis.OriginalMax = newAxis.Max;
      return newAxis;
    });

    return {
      ...trend,
      trendDefinition: {
        ...trendDefinition,
        Axes,
      },
    };
  });

  return {
    ...state,
    ChartState: {
      ...adapter.addMany(trends, state.ChartState),
      selectedDataExplorerTrendID: null,
      loadingTrends: false,
    },
  };
}
export function loadDataExplorerChartsFailure(
  state: IDataExplorerState
): IDataExplorerState {
  return {
    ...state,
    ChartState: { ...state.ChartState, loadingTrends: false },
  };
}

export function selectedDataExplorerTrend(
  state: IDataExplorerState,
  payload: { trendID: string }
): IDataExplorerState {
  if (payload.trendID) {
    return {
      ...state,
      ChartState: {
        ...state.ChartState,
        selectedDataExplorerTrendID: payload.trendID,
      },
    };
  } else {
    return {
      ...state,
      ChartState: {
        ...state.ChartState,
        selectedDataExplorerTrendID: null,
        loadingData: false,
      },
    };
  }
}

export function getMeasurementsSet(
  state: IDataExplorerState
): IDataExplorerState {
  return {
    ...state,
    ChartState: {
      ...state.ChartState,
      loadingData: true,
    },
  };
}
export function getMeasurementsSetSuccess(
  state: IDataExplorerState,
  payload: { trend: IProcessedTrend }
): IDataExplorerState {
  const chartState = adapter.upsertOne(payload.trend, state.ChartState);

  return {
    ...state,
    ChartState: {
      ...chartState,
      loadingData: false,
    },
  };
}

export function getMeasurementsSetFailure(
  state: IDataExplorerState
): IDataExplorerState {
  return { ...state, ChartState: { ...state.ChartState, loadingData: false } };
}

function copyLinkSuccess(state: IDataExplorerState): IDataExplorerState {
  return {
    ...state,
    Toast: { message: 'Link Copied!', type: 'success' },
  };
}

function copyLinkFailure(state: IDataExplorerState): IDataExplorerState {
  return {
    ...state,
    Toast: { message: 'Error Copying', type: 'error' },
  };
}

export function changeLabelsClicked(
  state: IDataExplorerState
): IDataExplorerState {
  const selectedTrend =
    state.ChartState.entities[state.ChartState.selectedDataExplorerTrendID];
  const newLabelIndex = (selectedTrend?.labelIndex ?? 0) + 1;
  const change: Partial<IProcessedTrend> = { labelIndex: newLabelIndex };
  const update: Update<IProcessedTrend> = {
    changes: change,
    id: state.ChartState.selectedDataExplorerTrendID,
  };
  return {
    ...state,
    ChartState: adapter.updateOne(update, state.ChartState),
    // we set isToggled to true here because if the user has changed the label index, that is the initial toggle,
    // and then it should stay as true until the user selects a different chart or asset.
    isToggled: true,
    labelIndex: newLabelIndex,
  };
}

export function updateChartYAxisLimits(
  state: IDataExplorerState,
  payload: { limitsData: IUpdateLimitsData }
) {
  const selectedTrend = {
    ...state.ChartState.entities[state.ChartState.selectedDataExplorerTrendID],
  };

  const newTrend = { ...selectedTrend.trendDefinition };

  selectedTrend.trendDefinition = {
    ...newTrend,
    Axes: updateAxes(selectedTrend.trendDefinition, payload.limitsData),
  };

  const update: Update<IProcessedTrend> = {
    changes: selectedTrend,
    id: state.ChartState.selectedDataExplorerTrendID,
  };
  return {
    ...state,
    ChartState: adapter.updateOne(update, state.ChartState),
  };
}

export function resetChartYAxisLimit(
  state: IDataExplorerState,
  payload: { axisIndex: number }
) {
  const selectedTrend = {
    ...state.ChartState.entities[state.ChartState.selectedDataExplorerTrendID],
  };

  const newTrend = { ...selectedTrend.trendDefinition };

  selectedTrend.trendDefinition = {
    ...newTrend,
    Axes: resetAxes(selectedTrend.trendDefinition, payload.axisIndex),
  };

  const update: Update<IProcessedTrend> = {
    changes: selectedTrend,
    id: state.ChartState.selectedDataExplorerTrendID,
  };
  return {
    ...state,
    ChartState: adapter.updateOne(update, state.ChartState),
  };
}

export function dataExplorerReducer(state, action) {
  return _dataExplorerReducer(state, action);
}
