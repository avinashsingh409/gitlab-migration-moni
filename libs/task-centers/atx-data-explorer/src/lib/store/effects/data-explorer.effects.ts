/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, map, withLatestFrom, tap } from 'rxjs/operators';

import * as dataExplorerActions from '../actions/data-explorer.action';
import * as chartActions from '../actions/chart.actions';
import * as assetTreeActions from '../actions/asset-tree.actions';
import * as timeSliderStateActions from '../actions/time-slider.actions';
import {
  assetState,
  selectSelectedAsset,
  selectSelectedTrendID,
  selectTimeSliderSelection,
} from '../selectors/data-explorer.selector';
import {
  NavActions,
  updateRouteWithoutReloading,
} from '@atonix/atx-navigation';

import { selectAsset } from '@atonix/atx-asset-tree';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Injectable()
export class DataExplorerEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  editChart$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(chartActions.editChartToAsset),
        tap((payload) => {
          window
            .open(
              this.appConfig.baseSiteURL +
                '/ChartEditor/#!?asset=' +
                payload.asset +
                '&start=' +
                payload.start +
                '&end=' +
                payload.end +
                '&trend=' +
                payload.trendID,
              '_blank'
            )
            .focus();
        })
      ),
    { dispatch: false }
  );

  initializeSummary$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.dataExplorerInitialize),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(assetState)
      ),
      map(([payload, newAsset, appAsset]) => {
        // If nothing is passed in for the selected asset we want to use
        // the previously selected asset.
        return {
          asset: payload.asset || appAsset || newAsset,
          start: payload.start,
          end: payload.end,
        };
      }),
      switchMap((vals) => [
        assetTreeActions.getPermissions(),
        assetTreeActions.treeStateChange(selectAsset(vals.asset, false)),
        timeSliderStateActions.initializeTimeSlider({
          start: vals.start,
          end: vals.end,
        }),
        NavActions.taskCenterLoad({
          taskCenterID: 'data-explorer',
          timeSlider: true,
          timeSliderOpened: true,
          assetTree: true,
          assetTreeOpened: false,
          asset: vals.asset,
        }),
      ])
    )
  );

  downloadItem$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(chartActions.downloadChartSuccess),
        tap((payload) => {
          window.open(
            this.appConfig.baseServicesURL +
              '/api/Images/TempCSV?id=' +
              payload.id,
            '_blank'
          );
          window.focus();
        })
      ),
    { dispatch: false }
  );

  updateRoute$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dataExplorerActions.updateRoute),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(selectSelectedTrendID),
        this.store.select(selectTimeSliderSelection)
      ),
      switchMap(([action, id, trend, time]) => {
        const start = time?.start?.getTime().toString();
        const end = time?.end?.getTime().toString();

        updateRouteWithoutReloading(
          id,
          trend,
          start,
          end,
          this.router,
          this.activatedRoute
        );
        return [
          NavActions.updateNavigationItems({
            urlParams: { id, trend, start, end },
          }),
        ];
      })
    )
  );
}
