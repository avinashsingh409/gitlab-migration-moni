/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import {
  map,
  switchMap,
  catchError,
  withLatestFrom,
  tap,
  filter,
  mergeMap,
} from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';

import { setDataOnTrend, copyToClipboard } from '@atonix/atx-chart';

import * as chartActions from '../actions/chart.actions';
import * as assetTreeActions from '../actions/asset-tree.actions';
import * as timeSliderActions from '../actions/time-slider.actions';
import {
  selectDataExplorerState,
  selectSelectedDataExplorerTrend,
  selectTimeSliderState,
  selectSelectedTrendID,
  selectSelectedAsset,
  selectSelectedTrendIsGroupedSeries,
} from '../selectors/data-explorer.selector';
import { IChartState } from '../state/chart-state';
import { getSelectedTrendID } from '../../service/data-explorer.helpers';
import {
  copyLink,
  copyLinkFailure,
  copyLinkSuccess,
} from '../actions/chart.actions';
import {
  ImagesFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';
import { IProcessedTrend } from '@atonix/atx-core';

@Injectable()
export class ChartEffects {
  constructor(
    private actions$: Actions,
    private imagesFrameworkService: ImagesFrameworkService,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private store: Store<IChartState>
  ) {}

  loadCharts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chartActions.loadDataExplorerCharts),
      filter((payload) => !!payload),
      withLatestFrom(this.store.pipe(select(selectDataExplorerState))),
      map(([payload, state]) => {
        return {
          ...payload,
          startDate: state?.TimeSliderState?.startDate ?? payload.startDate,
          endDate: state?.TimeSliderState?.endDate ?? payload.endDate,
        };
      }),
      switchMap((payload) =>
        this.processDataFrameworkService.getTrends(payload.nodeID).pipe(
          map(
            (trends) => [trends, payload.trendID],
            catchError((error) =>
              of(chartActions.loadDataExplorerChartsFailure(error))
            )
          )
        )
      ),
      switchMap(([trends, trendID]) => {
        return [
          chartActions.loadDataExplorerChartsSuccess({
            trends: trends as IProcessedTrend[],
          }),
          chartActions.selectDataExplorerTrend({
            trendID: (trendID as string)
              ? (trendID as string)
              : getSelectedTrendID(trends as IProcessedTrend[]),
          }),
        ];
      })
    )
  );

  selectChart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chartActions.selectDataExplorerTrend),
      filter((n) => !!n.trendID),
      map((action) =>
        chartActions.getMeasurementsSet({ trendID: action.trendID })
      )
    )
  );

  getTrendData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chartActions.getMeasurementsSet),
      withLatestFrom(this.store.pipe(select(selectDataExplorerState))),
      map(([payload, state]) => {
        const trendID =
          payload.trendID ?? state.ChartState.selectedDataExplorerTrendID;
        let trend = payload.trend ?? state.ChartState.entities[trendID];
        if (trend) {
          trend = {
            ...trend,
            startDate: payload?.startDate ?? state.TimeSliderState.startDate,
            endDate: payload?.endDate ?? state.TimeSliderState.endDate,
          };
        }
        return trend;
      }),
      filter((trend) => !!trend),
      switchMap((trend) =>
        this.processDataFrameworkService
          .getTagsDataFiltered(
            trend.trendDefinition,
            trend.startDate,
            trend.endDate,
            0,
            null,
            trend.assetID
          )
          .pipe(
            map((measurements) => {
              return setDataOnTrend(trend, measurements);
            }),
            map(
              (result) =>
                chartActions.getMeasurementsSetSuccess({ trend: result }),
              catchError((error) =>
                of(chartActions.getMeasurementsSetFailure(error))
              )
            )
          )
      )
    )
  );
  reflow$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          chartActions.reflow,
          assetTreeActions.treeSizeChange,
          timeSliderActions.hideTimeSlider
        ),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 500);
        })
      ),
    { dispatch: false }
  );

  copyLink$ = createEffect(() =>
    this.actions$.pipe(
      ofType(copyLink),
      mergeMap((payload) =>
        copyToClipboard(payload.link).pipe(
          map(
            () => copyLinkSuccess(),
            catchError((error) => of(copyLinkFailure(error)))
          )
        )
      )
    )
  );
  saveChart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chartActions.downloadChart),
      withLatestFrom(
        this.store.pipe(select(selectSelectedTrendIsGroupedSeries)),
        this.store.pipe(select(selectSelectedDataExplorerTrend))
      ),
      switchMap(([action, isGroupedSeries, trend]) =>
        this.imagesFrameworkService
          .saveTempCSV(isGroupedSeries, trend, action.config)
          .pipe(
            map((id) => chartActions.downloadChartSuccess({ id })),
            catchError((error) => of(chartActions.downloadChartFailure(error)))
          )
      )
    )
  );

  editChart$ = createEffect(() =>
    this.actions$.pipe(
      ofType(chartActions.editChart),
      withLatestFrom(
        this.store.select(selectSelectedAsset),
        this.store.select(selectTimeSliderState),
        this.store.select(selectSelectedTrendID)
      ),
      switchMap(([action, asset, time, trend]) => {
        return [
          chartActions.editChartToAsset({
            asset,
            start: String(time.startDate.getTime()),
            end: String(time.endDate.getTime()),
            trendID: String(trend),
          }),
        ];
      })
    )
  );
}
