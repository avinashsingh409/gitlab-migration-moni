/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/avoid-cyclic-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import {
  switchMap,
  map,
  withLatestFrom,
  catchError,
  mergeMap,
} from 'rxjs/operators';
import { NavActions } from '@atonix/atx-navigation';
import { ModelService as AssetTreeModel } from '@atonix/atx-asset-tree';
import * as assetTreeActions from '../actions/asset-tree.actions';
import * as dataExplorerActions from '../actions/data-explorer.action';
import { IDataExplorerState } from '../state/data-explorer-state';
import { of } from 'rxjs';
import { selectAssetTreeState } from '../selectors/data-explorer.selector';
import { AuthorizationFrameworkService } from '@atonix/shared/api';

@Injectable()
export class AssetTreeEffects {
  constructor(
    private actions$: Actions,
    private store: Store<IDataExplorerState>,
    private assetTreeModel: AssetTreeModel,
    private authorizationFrameworkService: AuthorizationFrameworkService
  ) {}

  getPermissions$ = createEffect(() =>
    this.actions$.pipe(
      ofType(assetTreeActions.getPermissions),
      switchMap(() =>
        this.authorizationFrameworkService.getPermissions().pipe(
          map((permissions) =>
            assetTreeActions.getPermissionsSuccess(permissions)
          ),
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          catchError((error) =>
            of(assetTreeActions.getPermissionsFailure(error))
          )
        )
      )
    )
  );

  treeStateChange$ = createEffect(() =>
    this.actions$.pipe(
      ofType(assetTreeActions.treeStateChange),
      withLatestFrom(this.store.pipe(select(selectAssetTreeState))),
      mergeMap(([change, state]) =>
        this.assetTreeModel
          .getAssetTreeData(change, state.treeConfiguration, state.treeNodes)
          .pipe(
            map(
              (n) => assetTreeActions.treeStateChange(n),
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) =>
                of(assetTreeActions.getAssetTreeDataFailure(error))
              )
            )
          )
      )
    )
  );

  selectAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(assetTreeActions.selectAsset),
      switchMap((action) => {
        return [
          NavActions.setApplicationAsset({ asset: action.asset }),
          dataExplorerActions.updateRoute(),
        ];
      })
    )
  );
}
