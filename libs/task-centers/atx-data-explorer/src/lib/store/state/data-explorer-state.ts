import {
  ITreeState,
  getDefaultTree,
  createTreeBuilder,
} from '@atonix/atx-asset-tree';
import { IChartState, initialChartState, IToast } from './chart-state';
import { ITimeSliderState } from '@atonix/atx-time-slider';

export interface IDataExplorerState {
  AssetTreeState: ITreeState;
  ChartState: IChartState;
  Toast: IToast;
  // isToggled tells whether or not the user has changed the labels in the legend. It defaults
  // to false, and then switches to true if the user changes the labels.
  // if the user selects a different chart or a different asset, this goes back to false.
  isToggled: boolean;
  // in addition to keeping the label index inside each trend, we keep it in the state
  // because if the user changes the date, we want to stay at the same label index,
  // and changing the date runs getTrends, which would overwrite the labelindex
  // if we only kept it with the trends.  So to avoid getting overwritten, we keep it here too.
  labelIndex: number;
  LeftTraySize: number;
  TimeSliderState: ITimeSliderState;
}

export const initialDataExplorerState: IDataExplorerState = {
  AssetTreeState: {
    treeConfiguration: getDefaultTree({ pin: true, collapseOthers: false }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: true,
  },
  ChartState: initialChartState,
  isToggled: false,
  labelIndex: 0,
  Toast: null,
  LeftTraySize: 250,
  TimeSliderState: null,
};
