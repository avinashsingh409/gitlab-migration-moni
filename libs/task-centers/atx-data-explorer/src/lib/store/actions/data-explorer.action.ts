import { createAction, props } from '@ngrx/store';

export const dataExplorerInitialize = createAction(
  '[Data Explorer] Initialize',
  props<{ asset?: string; start: Date; end: Date }>()
);

export const updateRoute = createAction('[Data Explorer] Update Route');
