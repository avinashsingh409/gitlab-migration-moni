/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';

import { ITreeStateChange } from '@atonix/atx-asset-tree';
import { ITreePermissions } from '@atonix/atx-core';

export const getPermissions = createAction(
  '[Data Explorer AssetTree] Get Permissions'
);
export const getPermissionsSuccess = createAction(
  '[Data Explorer AssetTree] Get Permissions Success',
  props<ITreePermissions>()
);
export const getPermissionsFailure = createAction(
  '[Data Explorer AssetTree] Get Permissions Failed',
  props<Error>()
);

export const selectAsset = createAction(
  '[Data Explorer AssetTree] Select Asset',
  props<{ asset: string }>()
);
export const treeStateChange = createAction(
  '[Data Explorer AssetTree] Tree State Change',
  props<ITreeStateChange>()
);
export const getAssetTreeDataFailure = createAction(
  '[Data Explorer AssetTree] Get Tree Data Failed',
  props<Error>()
);
export const treeSizeChange = createAction(
  '[Data Explorer AssetTree] Asset Tree Size Change',
  props<{ value: number }>()
);
