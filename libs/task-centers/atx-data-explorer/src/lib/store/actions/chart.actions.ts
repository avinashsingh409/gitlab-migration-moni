/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { IUpdateLimitsData } from '@atonix/atx-chart';
import { IAssetMeasurementsSet, IProcessedTrend } from '@atonix/atx-core';

export const loadDataExplorerCharts = createAction(
  '[Data Explorer Chart] Load Trends Charts',
  props<{ nodeID: string; trendID: string; startDate?: Date; endDate?: Date }>()
);
export const loadDataExplorerChartsSuccess = createAction(
  '[Data Explorer Chart] Get Trends Success',
  props<{ trends: IProcessedTrend[] }>()
);
export const loadDataExplorerChartsFailure = createAction(
  '[Data Explorer Chart] Get Trends Failed',
  props<Error>()
);

export const selectDataExplorerTrend = createAction(
  '[Data Explorer Chart] Trend Selected',
  props<{ trendID: string; trend?: IProcessedTrend }>()
);

export const getMeasurementsSet = createAction(
  '[Data Explorer Chart] Get MeasurementsSet Data',
  props<{
    trendID?: string;
    trend?: IProcessedTrend;
    startDate?: Date;
    endDate?: Date;
  }>()
);
export const getMeasurementsSetSuccess = createAction(
  '[Data Explorer Chart] Get MeasurementsSet Data Success',
  props<{ trend: IProcessedTrend }>()
);
export const getMeasurementsSetFailure = createAction(
  '[Data Explorer Chart] Get MeasurementsSet Data Failed',
  props<Error>()
);

export const renderModelContextChart = createAction(
  '[Data Explorer Chart] Render Data Trends Charts',
  props<{ trend: IProcessedTrend }>()
);
export const setMeasurementsSet = createAction(
  '[Data Explorer Chart] Set Measurements Set Trends Charts',
  props<{ measurementsSet: IAssetMeasurementsSet }>()
);

export const reflow = createAction('[Data Explorer] Reflow');

// this triggers a pretty standard reducer when the user selects a new legend index.
export const changeLabelsClicked = createAction(
  '[Charts] Change labels clicked'
);

export const copyLink = createAction(
  '[Charts] Copy Chart Link',
  props<{ link: string }>()
);

export const copyLinkSuccess = createAction('[Charts] Copy Chart Link Success');

export const copyLinkFailure = createAction(
  '[Charts] Copy Chart Link Failure',
  props<{ error: any }>()
);

export const downloadChart = createAction(
  '[Charts] Download Chart Content',
  props<{ config: Highcharts.Options }>()
);
export const downloadChartSuccess = createAction(
  '[Charts] Download Chart Content Success',
  props<{ id: string }>()
);

export const downloadChartFailure = createAction(
  '[Charts] Download Chart Content Failure',
  props<{ error: any }>()
);

export const editChart = createAction('[Charts] Edit Charts clicked');

export const editChartToAsset = createAction(
  '[Charts] Edit Chart To Asset',
  props<{ asset: string; start: string; end: string; trendID: string }>()
);

export const updateChartYAxisLimit = createAction(
  '[Charts] Update Y-Axis Limits',
  props<{ limitsData: IUpdateLimitsData }>()
);

export const resetChartYAxisLimit = createAction(
  '[Charts] Reset Y-Axis Limits',
  props<{ axisIndex: number }>()
);
