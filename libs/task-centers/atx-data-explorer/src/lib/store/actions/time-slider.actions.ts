/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';

import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { ICriteria } from '@atonix/atx-core';

export const initializeTimeSlider = createAction(
  '[TimeSlider] Initialize Time Slider',
  props<{ start: Date; end: Date }>()
);
export const timeSliderStateChange = createAction(
  '[TimeSlider] Time Slider State Change',
  props<ITimeSliderStateChange>()
);

export const hideTimeSlider = createAction('[TimeSlider] Hide Time Slider');
