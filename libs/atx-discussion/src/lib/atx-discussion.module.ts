import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EditorModule, TINYMCE_SCRIPT_SRC } from '@tinymce/tinymce-angular';
import { DiscussionComponent } from './component/discussion/discussion.component';
import { DiscussionEntryComponent } from './component/discussion-entry/discussion-entry.component';
import { AttachmentComponent } from './component/attachment/attachment.component';
import { DragDropDirective } from './component/discussion-entry-editor/drag-drop';
import { SafeHTMLPipe } from './component/discussion-entry/safe-html.pipe';
import { DiscussionEntryEditorComponent } from './component/discussion-entry-editor/discussion-entry-editor.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedApiModule } from '@atonix/shared/api';
@NgModule({
  declarations: [
    DiscussionComponent,
    DiscussionEntryComponent,
    DiscussionEntryEditorComponent,
    AttachmentComponent,
    DragDropDirective,
    SafeHTMLPipe,
  ],
  imports: [
    CommonModule,
    AtxMaterialModule,
    FontAwesomeModule,
    SharedApiModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule,
  ],
  providers: [
    { provide: TINYMCE_SCRIPT_SRC, useValue: 'tinymce/tinymce.min.js' },
  ],
  exports: [DiscussionComponent],
})
export class DiscussionModule {}
