import { IDiscussionAssetMapType } from '../models/discussion';
import moment from 'moment';
import flatMap from 'lodash/flatMap';
import find from 'lodash/find';
import {
  IDiscussionEntry,
  IDiscussion,
  IDiscussionAttachment,
} from '@atonix/atx-core';

export function validateDiscussionEntry(
  entry: IDiscussionEntry,
  uploadingAttachment: boolean
) {
  let errMsg: string = null;
  if (!entry) {
    errMsg = 'No entry is defined.';
  } else if (!entry.DiscussionID) {
    errMsg = 'No Discussion Defined.';
  } else if (!entry.Content || entry.Content === 'null') {
    errMsg = 'New entry must have content.';
  } else if (uploadingAttachment) {
    errMsg = 'Attachments are still uploading.';
  }

  return errMsg;
}

export function toDateTimeOffset(myDate: Date) {
  if (moment(myDate).isValid()) {
    let month = String(myDate.getMonth() + 1);
    if (month.length === 1) {
      month = '0' + month;
    }
    let day = String(myDate.getDate());
    if (day.length === 1) {
      day = '0' + day;
    }
    let hour = String(myDate.getHours());
    if (hour.length === 1) {
      hour = '0' + hour;
    }
    let minute = String(myDate.getMinutes());
    if (minute.length === 1) {
      minute = '0' + minute;
    }
    let second = String(myDate.getSeconds());
    if (second.length === 1) {
      second = '0' + second;
    }

    const timeZoneOffset = myDate.getTimezoneOffset();
    let timeZoneHours = String(Math.floor(timeZoneOffset / 60));
    let timeZoneMinutes = String(timeZoneOffset % 60);
    if (timeZoneHours.length === 1) {
      timeZoneHours = '0' + timeZoneHours;
    }
    if (timeZoneMinutes.length === 1) {
      timeZoneMinutes = '0' + timeZoneMinutes;
    }
    return (
      myDate.getFullYear() +
      '-' +
      month +
      '-' +
      day +
      'T' +
      hour +
      ':' +
      minute +
      ':' +
      second +
      '.0000000-' +
      timeZoneHours +
      ':' +
      timeZoneMinutes
    );
  } else {
    return null;
  }
}

export function processEntry(entry: IDiscussionEntry) {
  const newEntry = { ...entry } as IDiscussionEntry;

  newEntry.CreateDateZ = moment(entry.CreateDateZ as any).toDate();
  newEntry.ChangeDateZ = moment(entry.ChangeDateZ as any).toDate();

  newEntry.Attachments = newEntry.Attachments.map((attachment) => {
    // Should add getting of path here
    const attach = { ...attachment } as IDiscussionAttachment;
    attach.CreateDateZ = moment(attachment.CreateDateZ as any).toDate();
    attach.ChangeDateZ = moment(attachment.ChangeDateZ as any).toDate();
    attach.complete = true;

    return attach;
  });

  newEntry.Attachments = [...newEntry.Attachments].sort(
    (a, b) => a.DisplayOrder - b.DisplayOrder
  );
  newEntry.editing = false;
  newEntry.saving = false;

  return newEntry;
}

export function processDiscussion(newDiscussion: IDiscussion[]) {
  if (newDiscussion) {
    const discussion: IDiscussion = { ...newDiscussion[0] };
    discussion.DiscussionID = newDiscussion[0].DiscussionID;
    discussion.AssetID = newDiscussion[0].AssetID;
    discussion.CreateDateZ = moment(
      newDiscussion[0].CreateDateZ as any
    ).toDate();
    discussion.ChangeDateZ = moment(
      newDiscussion[0].ChangeDateZ as any
    ).toDate();

    discussion.Entries = flatMap(newDiscussion, 'Entries');
    discussion.Entries = discussion.Entries.map((entry) => {
      return processEntry(entry);
    });

    discussion.Entries = [...discussion.Entries].sort((a, b) => {
      return (
        new Date(b.CreateDateZ).getTime() - new Date(a.CreateDateZ).getTime()
      );
    });

    if (discussion.Entries.length > 5) {
      discussion.AllEntries = discussion.Entries;
      discussion.Entries = discussion.Entries.slice(0, 5);
    }

    discussion.DiscussionType =
      newDiscussion.length > 1 ? 0 : newDiscussion[0].DiscussionType;

    return discussion;
  }
}

export function getMoreEntries(discussion: IDiscussion) {
  const newDiscussion = { ...discussion } as IDiscussion;
  if (discussion.AllEntries) {
    const itemsToAdd = newDiscussion.AllEntries.slice(
      newDiscussion.Entries.length,
      newDiscussion.Entries.length + 6
    );
    newDiscussion.Entries = newDiscussion.Entries.concat(itemsToAdd);
    if (newDiscussion.Entries.length === newDiscussion.AllEntries.length) {
      newDiscussion.AllEntries = null;
    }
  }

  return newDiscussion;
}

export function getDiscussionEntryTopic(
  discussions: IDiscussion[],
  discussionAssetTypes: IDiscussionAssetMapType[],
  entry: IDiscussionEntry
) {
  let result: IDiscussionAssetMapType;
  const d = find(discussions, ['DiscussionID', entry.DiscussionID]);
  // eslint-disable-next-line prefer-const
  result = find(discussionAssetTypes, [
    'DiscussionAssetMapTypeID',
    d.DiscussionType,
  ]);

  return result;
}

export function getNextAttachmentDisplayOrder(
  attachments: IDiscussionAttachment[]
) {
  let result = 1;
  if (attachments && attachments.length > 0) {
    for (const attachment of attachments) {
      result = Math.max(result, attachment.DisplayOrder);
    }
    result++;
  }
  return result;
}
