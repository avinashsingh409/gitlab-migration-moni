import { IDiscussionStateChange } from '../models/discussion-state-change';
import { IDiscussionState } from '../models/discussion-state';
import {
  ILoadDiscussion,
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '../models/discussion';
import {
  ISaveDiscussionEntry,
  IDiscussionEntryKeyword,
  IDiscussionEntry,
} from '@atonix/atx-core';

export function getDefaultDiscussion(params?: Partial<IDiscussionState>) {
  const result: IDiscussionState = {
    ...{
      discussionAssetTypes: [],
      issuesCount: 0,
      discussion: null,
      discussions: [],
      discussionEntry: null,
    },
    ...params,
  };

  return result;
}

export function loadDiscussion(value: ILoadDiscussion) {
  const result: IDiscussionStateChange = {
    event: 'LoadDiscussion',
    newValue: value,
  };
  return result;
}

export function sendNotifications() {
  const result: IDiscussionStateChange = {
    event: 'SendNotifications',
  };
  return result;
}

export function subscribe(showedTopic: number) {
  const result: IDiscussionStateChange = {
    event: 'Subscribe',
    newValue: showedTopic,
  };
  return result;
}

export function viewOpenIssues() {
  const result: IDiscussionStateChange = {
    event: 'ViewOpenIssues',
  };
  return result;
}

export function deleteDiscussionEntryAttachment(
  attachmentDetails: IDeleteDiscussionEntryAttachment
) {
  const result: IDiscussionStateChange = {
    event: 'DeleteDiscussionEntryAttachment',
    newValue: attachmentDetails,
  };
  return result;
}

export function deleteDiscussionEntryKeyword(
  keywordDetails: IDeleteDiscussionEntryKeyword
) {
  const result: IDiscussionStateChange = {
    event: 'DeleteDiscussionEntryKeyword',
    newValue: keywordDetails,
  };
  return result;
}

export function saveDiscussionEntry(entry: ISaveDiscussionEntry) {
  const result: IDiscussionStateChange = {
    event: 'SaveDiscussionEntry',
    newValue: entry,
  };
  return result;
}

export function cancelDiscussionEntry(discussionEntryID: string) {
  const result: IDiscussionStateChange = {
    event: 'CancelDiscussionEntry',
    newValue: discussionEntryID,
  };
  return result;
}

export function searchTaggingKeyword(keyword: IDiscussionEntryKeyword) {
  const result: IDiscussionStateChange = {
    event: 'SearchTaggingKeyword',
    newValue: keyword,
  };
  return result;
}

export function addDiscussionEntryKeyword(keyword: IDiscussionEntryKeyword) {
  const result: IDiscussionStateChange = {
    event: 'AddDiscussionEntryKeyword',
    newValue: keyword,
  };
  return result;
}

export function addDiscussionEntryAttachments(
  attachmentDetails: IAddDiscussionEntryAttachment[]
) {
  const result: IDiscussionStateChange = {
    event: 'AddDiscussionEntryAttachments',
    newValue: attachmentDetails,
  };
  return result;
}

export function editDiscussionEntry(entry: IDiscussionEntry) {
  const result: IDiscussionStateChange = {
    event: 'EditDiscussionEntry',
    newValue: entry,
  };
  return result;
}

export function deleteDiscussionEntry(discussionEntryID: string) {
  const result: IDiscussionStateChange = {
    event: 'DeleteDiscussionEntry',
    newValue: discussionEntryID,
  };
  return result;
}

export function showMoreEntries() {
  const result: IDiscussionStateChange = {
    event: 'ShowMoreEntries',
  };
  return result;
}
