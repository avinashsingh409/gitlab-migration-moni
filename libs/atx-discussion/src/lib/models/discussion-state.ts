import { IDiscussion, IDiscussionEntry } from '@atonix/atx-core';
import { IDiscussionAssetMapType } from './discussion';

export interface IDiscussionState {
  discussionAssetTypes: IDiscussionAssetMapType[];
  issuesCount: number;
  discussion: IDiscussion;
  discussions: IDiscussion[];
  discussionEntry: IDiscussionEntry;
}
