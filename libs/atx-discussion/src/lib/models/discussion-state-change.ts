import {
  ISaveDiscussionEntry,
  IDiscussionEntry,
  IDiscussionEntryKeyword,
} from '@atonix/atx-core';
import {
  IAddDiscussionEntryAttachment,
  ILoadDiscussion,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from './discussion';

export interface IDiscussionStateChange {
  event:
    | 'LoadDiscussion' // ILoadDiscussion
    | 'SendNotifications' // null
    | 'Subscribe' // number
    | 'ViewOpenIssues' // null
    | 'DeleteDiscussionEntryAttachment' // IDeleteDiscussionEntryAttachment
    | 'DeleteDiscussionEntryKeyword' // IDeleteDiscussionEntryKeyword
    | 'SaveDiscussionEntry' // ISaveDiscussionEntry
    | 'CancelDiscussionEntry' // string
    | 'EditDiscussionEntry' // IDiscussionEntry
    | 'DeleteDiscussionEntry' // string
    | 'SearchTaggingKeyword' // IDiscussionEntryKeyword
    | 'AddDiscussionEntryKeyword' // IDiscussionEntryKeyword
    | 'AddDiscussionEntryAttachments' // IAddDiscussionEntryAttachment[]
    | 'ShowMoreEntries'; // null
  newValue?:
    | string
    | number
    | ILoadDiscussion
    | ISaveDiscussionEntry
    | IDiscussionEntry
    | IDiscussionEntryKeyword
    | IAddDiscussionEntryAttachment[]
    | IDeleteDiscussionEntryAttachment
    | IDeleteDiscussionEntryKeyword;
}
