import { SafeStyle } from '@angular/platform-browser';

export enum DiscussionType {
  All = 0,
  Default = 1,
  FilesAPI = 2,
  RAScenarioCalcs = 3,
}
export interface ISecurityUserLite {
  UserName: string;
  GlobalId: string;
  IsBVEmployee: boolean;
}

export interface IDiscussionAssetMapType {
  DiscussionAssetMapTypeID: number;
  DiscussionAssetMapTypeName: string;
}

export interface IDiscussionEntryChange {
  Timestamp: Date;
  Change: string;
  User: ISecurityUserLite;
}

export interface ILoadDiscussion {
  ShowAutogenEntries: boolean;
  ShowedTopic: number;
}

export interface IAddDiscussionEntryAttachment {
  DiscussionID: string;
  EntryID: string;
  Title: string;
  DiscussionType: string;
  File: any;
  DisplayOrder: number;

  //For mobile issue snapshot page
  AssetID?: number;
}

export interface IDeleteDiscussionEntryAttachment {
  DiscussionAttachmentID: string;
  DiscussionContentID: string;
  DiscussionEntryID: string;
}

export interface IDeleteDiscussionEntryKeyword {
  DiscussionKeywordID: number;
  DiscussionEntryID: string;
}

export enum EDiscussion {
  NewID = '00000000-0000-0000-0000-000000000000',
}
