import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
} from '@angular/core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
} from '@angular/forms';
import {
  ITaggingKeyword,
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  ISaveDiscussionEntry,
} from '@atonix/atx-core';
import {
  IDiscussionAssetMapType,
  IAddDiscussionEntryAttachment,
  EDiscussion,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '../../models/discussion';
import { getNextAttachmentDisplayOrder } from '../../service/utilities';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'atx-discussion-entry-editor',
  templateUrl: './discussion-entry-editor.component.html',
  styleUrls: ['./discussion-entry-editor.component.scss'],
})
export class DiscussionEntryEditorComponent implements OnChanges {
  @Input() entry: IDiscussionEntry;
  @Input() type: 'Asset' | 'Issue';
  @Input() discussionAssetTypes: IDiscussionAssetMapType[];
  @Output() saveDiscussionEntry = new EventEmitter<ISaveDiscussionEntry>();
  @Output() cancelDiscussionEntry = new EventEmitter<string>();
  @Output() searchTaggingKeyword = new EventEmitter<IDiscussionEntryKeyword>();
  @Output() addDiscussionEntryKeyword =
    new EventEmitter<IDiscussionEntryKeyword>();
  @Output() addDiscussionEntryAttachments = new EventEmitter<
    IAddDiscussionEntryAttachment[]
  >();
  @Output() deleteDiscussionEntryKeyword =
    new EventEmitter<IDeleteDiscussionEntryKeyword>();
  @Output() deleteDiscussionEntryAttachment =
    new EventEmitter<IDeleteDiscussionEntryAttachment>();
  faSpinner = faSpinner;

  editorForm = new UntypedFormGroup({
    title: new UntypedFormControl('New Discussion Post'),
    content: new UntypedFormControl(null, [Validators.required]),
    topics: new UntypedFormControl(1, [Validators.required]),
    keyword: new UntypedFormControl(null),
  });

  initEditor = {
    promotion: false,
    height: '350',
    content_style: 'img { max-width: 100%; }',
    plugins: 'image autolink link',
    contextmenu: false,
    text_patterns: false,
    paste_data_images: true,
    convert_urls: false,
    relative_urls: false,
    remove_script_host: false,
    toolbar:
      'undo redo | bold italic underline | link | forecolor | fontselect emoticons',
    menubar: 'edit view format',
    menu: {
      edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
      view: { title: 'View', items: 'visualaid' },
      insert: { title: 'Insert', items: 'image | link' },
      format: {
        title: 'Format',
        items: 'bold italic underline | removeformat',
      },
    },
    statusbar: false,
    browser_spellcheck: true,
    base_url: this.locationStrat.getBaseHref() + 'tinymce',
    suffix: '.min',
    extended_valid_elements: 'a[href|target=_blank]',
    default_link_target: '_blank',
    setup: (editor) => {
      editor.on('keyup', (e) => {
        let content: string = e.currentTarget.innerHTML;
        if (content.includes('<img src="blob:')) {
          content = this.editorForm.get('content').value ?? '';
        }
        this.contentCheck(content);
      });

      editor.on('drop', (e) => {
        let content: string = e.currentTarget.children['tinymce'].innerHTML;
        if (content.includes('img src="blob:')) {
          content = this.editorForm.get('content').value ?? '';
        }
        const totalSize: number =
          [...e.dataTransfer.files].reduce(
            (sum: number, f: File) =>
              sum +
              (f.size * 1.33 + '<img src="data:image/jpeg;base64, />'.length),
            0
          ) + content.length;
        this.contentCheck(totalSize);
      });
    },
  };
  exceedsLimit = false;
  private CONTENT_LIMIT = 10000000; // 10MB

  constructor(
    private locationStrat: LocationStrategy,
    private cdr: ChangeDetectorRef
  ) {}
  ngOnChanges(changes: SimpleChanges) {
    if (
      changes?.entry?.currentValue?.DiscussionEntryID !== EDiscussion.NewID &&
      changes?.entry?.firstChange
    ) {
      this.editorForm.patchValue({
        title: changes?.entry?.currentValue?.Title,
        content: changes?.entry?.currentValue?.Content,
        topics:
          this.type === 'Asset'
            ? changes.entry.currentValue.DiscussionEntryTopic
                ?.DiscussionAssetMapTypeID
            : null,
      });
    }
  }

  saveEntry(entry: IDiscussionEntry) {
    const newEntry = { ...entry };
    newEntry.Title =
      this.editorForm.get('title').value === ''
        ? 'New Discussion Post'
        : this.editorForm.get('title').value;
    newEntry.Content = this.editorForm.get('content').value;
    newEntry.Attachments = newEntry.newAttachments;
    newEntry.Keywords = newEntry.newKeywords;

    this.saveDiscussionEntry.emit({
      ShowAutogenEntries: null,
      ShowedTopic: null,
      Entry: newEntry,
      EntryTopic: +this.editorForm.get('topics').value,
      UploadingAttachments: this.entry.uploadingAttachments,
      NotifySubscribers: true,
    });
  }

  cancelEntry(entry: IDiscussionEntry) {
    this.cancelDiscussionEntry.emit(entry.DiscussionEntryID);
  }

  searchKeyword(entry: IDiscussionEntry) {
    this.searchTaggingKeyword.emit({
      DiscussionEntry: entry,
      Keyword: this.editorForm.get('keyword').value,
    });
  }

  addKeyword(entry: IDiscussionEntry) {
    if (
      this.editorForm.get('keyword').value !== null &&
      this.editorForm.get('keyword').value !== ''
    ) {
      this.addDiscussionEntryKeyword.emit({
        DiscussionEntry: entry,
        Keyword: this.editorForm.get('keyword').value,
      });
      this.editorForm.get('keyword').setValue('');
    }
  }

  uploadFile($event: FileList) {
    // File uploading and saving here
    if ($event.length > 0) {
      const attachments: IAddDiscussionEntryAttachment[] = [];
      let displayOrder = getNextAttachmentDisplayOrder(
        this.entry.newAttachments
      );

      Array.from($event).forEach((attachment) => {
        attachments.push({
          DiscussionID: undefined,
          EntryID: this.entry.DiscussionEntryID,
          Title: attachment?.name,
          DiscussionType: undefined,
          File: attachment,
          DisplayOrder: displayOrder++,
        });
      });

      this.addDiscussionEntryAttachments.emit(attachments);
    }
  }

  deleteKeyword(keyword: ITaggingKeyword, entryID: string) {
    this.deleteDiscussionEntryKeyword.emit({
      DiscussionKeywordID: keyword.KeywordId,
      DiscussionEntryID: entryID,
    });
  }

  deleteAttachment(
    event: { attachmentID: string; contentID: string },
    entryID: string
  ) {
    this.deleteDiscussionEntryAttachment.emit({
      DiscussionAttachmentID: event.attachmentID,
      DiscussionContentID: event.contentID,
      DiscussionEntryID: entryID,
    });
  }

  contentCheck(content: string | number) {
    this.exceedsLimit = this.contentExceedsLimit(content);
    this.cdr.detectChanges();
  }

  contentExceedsLimit = (
    content: string | number,
    limit: number = this.CONTENT_LIMIT
  ): boolean =>
    typeof content === 'number' ? content > limit : content.length > limit;
}
