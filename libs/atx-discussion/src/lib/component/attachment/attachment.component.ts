import { Component, Input, EventEmitter, Output } from '@angular/core';
import { IDiscussionAttachment } from '@atonix/atx-core';
import { AssetFrameworkService } from '@atonix/shared/api';

@Component({
  selector: 'atx-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
})
export class AttachmentComponent {
  @Input() attachment: IDiscussionAttachment;
  @Input() editing = false;
  @Output() deleteAttachment = new EventEmitter<{
    attachmentID: string;
    contentID: string;
  }>();
  @Output() openImageAttachment = new EventEmitter<IDiscussionAttachment>();

  constructor(private assetFrameworkService: AssetFrameworkService) {}

  openImageViewer(attachment: IDiscussionAttachment) {
    this.openImageAttachment.emit(attachment);
  }

  popupAttachment(attachment: IDiscussionAttachment) {
    this.assetFrameworkService
      .getAttachmentPopupLinkFromIDs(
        attachment.DiscussionAttachmentID,
        attachment.ContentID
      )
      .subscribe((path) => {
        window.open(path, '_blank');
      });
  }

  removeAttachment(attachmentID: string, contentID: string) {
    this.deleteAttachment.emit({ attachmentID, contentID });
  }
}
