import {
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  IDiscussionAssetMapType,
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '../../models/discussion';
import {
  IDiscussionAttachment,
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  ISaveDiscussionEntry,
  ITaggingKeyword,
} from '@atonix/atx-core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ImageViewerDialogComponent } from '@atonix/shared/utils';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'atx-discussion-entry',
  templateUrl: './discussion-entry.component.html',
  styleUrls: ['./discussion-entry.component.scss'],
})
export class DiscussionEntryComponent implements OnChanges {
  @Input() entry: IDiscussionEntry;
  @Input() type: 'Asset' | 'Issue';
  @Input() discussionAssetTypes: IDiscussionAssetMapType[];
  @Output() saveDiscussionEntry = new EventEmitter<ISaveDiscussionEntry>();
  @Output() cancelDiscussionEntry = new EventEmitter<string>();
  @Output() searchTaggingKeyword = new EventEmitter<IDiscussionEntryKeyword>();
  @Output() addDiscussionEntryKeyword =
    new EventEmitter<IDiscussionEntryKeyword>();
  @Output() addDiscussionEntryAttachments = new EventEmitter<
    IAddDiscussionEntryAttachment[]
  >();
  @Output() deleteDiscussionEntryKeyword =
    new EventEmitter<IDeleteDiscussionEntryKeyword>();
  @Output() deleteDiscussionEntryAttachment =
    new EventEmitter<IDeleteDiscussionEntryAttachment>();
  @Output() editDiscussionEntry = new EventEmitter<IDiscussionEntry>();
  @Output() deleteDiscussionEntry = new EventEmitter<string>();
  @ViewChild('entryContent') entryContent: ElementRef;
  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private dialog: MatDialog) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.entry.currentValue?.Content !== null) {
      this.addClickEventToImages();
    }
  }

  addClickEventToImages() {
    setTimeout(() => {
      const contentImages =
        this.entryContent?.nativeElement.querySelectorAll('img');

      contentImages?.forEach((image) => {
        image.addEventListener('click', (e) => {
          const blobURL = image.getAttribute('src') || null;
          this.openImageAttachment({ blobURL } as IDiscussionAttachment);
        });
      });
    }, 0);
  }

  tagsToDisplayText(keywords: ITaggingKeyword[]) {
    if (!keywords) {
      return '';
    }

    const texts = [];
    for (const keyword of keywords) {
      texts.push(keyword.Text);
    }
    return texts.join(', ');
  }

  getEntryTitle(entry: IDiscussionEntry) {
    if (entry?.Title && entry?.Title.length > 0) {
      if (entry?.Title.length > 85) {
        return `${entry?.Title.slice(0, 85)}...`;
      }
      return entry?.Title;
    } else {
      return 'Blog Entry';
    }
  }

  getEntryTitleTooltip() {
    return this.entry?.Title || '';
  }

  saveEntry(saveEntryDetails: ISaveDiscussionEntry) {
    this.saveDiscussionEntry.emit(saveEntryDetails);
  }

  cancelEntry(entryID: string) {
    this.cancelDiscussionEntry.emit(entryID);
  }

  searchKeyword(searchDetails: IDiscussionEntryKeyword) {
    this.searchTaggingKeyword.emit(searchDetails);
  }

  addKeyword(addDetails: IDiscussionEntryKeyword) {
    this.addDiscussionEntryKeyword.emit(addDetails);
  }

  addAttachments(attachmentDetails: IAddDiscussionEntryAttachment[]) {
    this.addDiscussionEntryAttachments.emit(attachmentDetails);
  }

  deleteKeyword(keywordDetails: IDeleteDiscussionEntryKeyword) {
    this.deleteDiscussionEntryKeyword.emit(keywordDetails);
  }

  deleteAttachment(attachmentDetails: IDeleteDiscussionEntryAttachment) {
    this.deleteDiscussionEntryAttachment.emit(attachmentDetails);
  }

  editEntry(event: Event, entry: IDiscussionEntry) {
    event.stopImmediatePropagation();
    this.accordion.openAll();
    this.editDiscussionEntry.emit(entry);
  }

  deleteEntry(event: Event, entryID: string) {
    event.stopImmediatePropagation();
    if (confirm('Are you sure you want to delete this entry?')) {
      this.deleteDiscussionEntry.emit(entryID);
    }
  }

  openImageAttachment(attachment: IDiscussionAttachment) {
    const images = this.entry?.Attachments?.filter(
      (attach) => attach.DiscussionAttachmentType === 1
    );

    const imageSources = this.getContentImgSrc();
    images.map((image) => {
      imageSources.push(image.blobURL);
    });

    const dialogConfig = new MatDialogConfig();
    dialogConfig.panelClass = 'custom-dialog-image-viewer';
    dialogConfig.data = {
      imageSources,
      selectedIndex: imageSources.findIndex(
        (src) => src === attachment.blobURL
      ),
    };
    const dialogRef = this.dialog.open(
      ImageViewerDialogComponent,
      dialogConfig
    );
  }

  getContentImgSrc() {
    const srcFiles = [];
    const contentImages =
      this.entryContent?.nativeElement.querySelectorAll('img');
    contentImages?.forEach((image) => {
      srcFiles.push(image.getAttribute('src') || null);
    });

    return srcFiles;
  }
}
