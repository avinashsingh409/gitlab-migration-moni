import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { DiscussionEntryComponent } from './discussion-entry.component';
import { SafeHTMLPipe } from './safe-html.pipe';

describe('DiscussionEntryComponent', () => {
  let component: DiscussionEntryComponent;
  let fixture: ComponentFixture<DiscussionEntryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DiscussionEntryComponent, SafeHTMLPipe],
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
    }).compileComponents();
    fixture = TestBed.createComponent(DiscussionEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
