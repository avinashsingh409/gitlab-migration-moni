import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  UntypedFormControl,
  Validators,
  UntypedFormGroup,
} from '@angular/forms';
import { IDiscussionStateChange } from '../../models/discussion-state-change';
import { IDiscussionState } from '../../models/discussion-state';
import {
  sendNotifications,
  subscribe,
  viewOpenIssues,
  saveDiscussionEntry,
  cancelDiscussionEntry,
  searchTaggingKeyword,
  loadDiscussion,
  editDiscussionEntry,
  deleteDiscussionEntry,
  addDiscussionEntryKeyword,
  addDiscussionEntryAttachments,
  showMoreEntries,
  deleteDiscussionEntryAttachment,
  deleteDiscussionEntryKeyword,
} from '../../service/discussion.service';
import {
  IAddDiscussionEntryAttachment,
  IDeleteDiscussionEntryAttachment,
  IDeleteDiscussionEntryKeyword,
} from '../../models/discussion';
import {
  IDiscussionEntry,
  IDiscussionEntryKeyword,
  ISaveDiscussionEntry,
  isNil,
} from '@atonix/atx-core';

@Component({
  selector: 'atx-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss'],
})
export class DiscussionComponent implements OnChanges {
  @Input() discussionState: IDiscussionState;
  @Input() type: 'Asset' | 'Issue';
  @Output() stateChange = new EventEmitter<IDiscussionStateChange>();

  expandAddNewEntry = false;

  discussionForm = new UntypedFormGroup({
    showAutogenEntries: new UntypedFormControl(false),
    showTopics: new UntypedFormControl(1, [Validators.required]),
  });

  ngOnChanges(changes: SimpleChanges) {
    if (changes.discussionState && changes.discussionState.currentValue) {
      if (
        changes.discussionState.currentValue.discussionAssetTypes?.length ===
          0 &&
        this.type === 'Asset'
      ) {
        this.discussionForm.patchValue({
          showTopics: 1,
          showAutogenEntries: false,
        });
      } else if (isNil(changes.discussionState.currentValue.discussionEntry)) {
        this.expandAddNewEntry = false;
      }
    }
  }

  // This will enable angular to track the references of the entries
  // In this way, re-creating new object references will be disabled when an object is updated
  trackByDiscussionEntryID(index: number, entry: IDiscussionEntry): string {
    return entry.DiscussionEntryID;
  }

  loadDiscussion() {
    this.stateChange.emit(
      loadDiscussion({
        ShowAutogenEntries: this.discussionForm.get('showAutogenEntries').value,
        ShowedTopic: +this.discussionForm.get('showTopics').value,
      })
    );
  }

  sendNotifications() {
    this.stateChange.emit(sendNotifications());
  }

  subscribe() {
    if (this.discussionForm.get('showTopics').valid) {
      this.stateChange.emit(
        subscribe(+this.discussionForm.get('showTopics').value)
      );
    }
  }

  viewOpenIssues() {
    this.stateChange.emit(viewOpenIssues());
  }

  saveDiscussionEntry(saveEntryDetails: ISaveDiscussionEntry) {
    saveEntryDetails.ShowAutogenEntries =
      this.discussionForm.get('showAutogenEntries').value;
    saveEntryDetails.ShowedTopic = +this.discussionForm.get('showTopics').value;
    saveEntryDetails.Entry.saving = true;
    this.stateChange.emit(saveDiscussionEntry(saveEntryDetails));
  }

  cancelDiscussionEntry(discussionEntryID: string) {
    this.stateChange.emit(cancelDiscussionEntry(discussionEntryID));
  }

  searchTaggingKeyword(keyword: IDiscussionEntryKeyword) {
    this.stateChange.emit(searchTaggingKeyword(keyword));
  }

  addDiscussionEntryKeyword(keyword: IDiscussionEntryKeyword) {
    this.stateChange.emit(addDiscussionEntryKeyword(keyword));
  }

  addDiscussionEntryAttachments(
    attachmentDetails: IAddDiscussionEntryAttachment[]
  ) {
    this.stateChange.emit(addDiscussionEntryAttachments(attachmentDetails));
  }

  deleteDiscussionEntryKeyword(keywordDetails: IDeleteDiscussionEntryKeyword) {
    this.stateChange.emit(deleteDiscussionEntryKeyword(keywordDetails));
  }

  deleteDiscussionEntryAttachment(
    attachmentDetails: IDeleteDiscussionEntryAttachment
  ) {
    this.stateChange.emit(deleteDiscussionEntryAttachment(attachmentDetails));
  }

  editDiscussionEntry(entry: IDiscussionEntry) {
    this.stateChange.emit(editDiscussionEntry(entry));
  }

  deleteDiscussionEntry(discussionEntryID: string) {
    this.stateChange.emit(deleteDiscussionEntry(discussionEntryID));
  }

  showMoreEntries() {
    this.stateChange.emit(showMoreEntries());
  }
}
