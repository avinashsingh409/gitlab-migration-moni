import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DiscussionComponent } from './discussion.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('DiscussionComponent', () => {
  let component: DiscussionComponent;
  let fixture: ComponentFixture<DiscussionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatAutocompleteModule,
        ReactiveFormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [DiscussionComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(DiscussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
