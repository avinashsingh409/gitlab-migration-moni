import { HighchartsChartModule } from 'highcharts-angular';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartDisplayComponent } from './component/chart-display/chart-display.component';
import { ChartSelectorComponent } from './component/chart-selector/chart-selector.component';
import { BarChartDisplayComponent } from './component/bar-chart-display/bar-chart-display.component';
import { TableDisplayComponent } from './component/table-display/table-display.component';
import { UpdateLimitsDialogComponent } from './component/update-limits-dialog/update-limits-dialog.component';
import { CommonModule } from '@angular/common';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ChartFacade } from './facade/chart.facade';
import { SharedApiModule } from '@atonix/shared/api';
import { SimpleChartDisplayComponent } from './component/simple-chart-display/simple-chart-display.component';
import { ModelChartFacade } from './facade/model-chart.facade';
import { ModelTrendChartComponent } from './component/model-trend-chart/model-trend-chart.component';
import { ModelHistoryGridComponent } from './component/model-history-grid/model-history-grid.component';
import { FavoriteFormatterComponent } from './component/model-history-grid/formatter/favorite-formatter.component';
import { AgGridModule } from '@ag-grid-community/angular';
import { LicenseManager } from '@ag-grid-enterprise/all-modules';
import { ModelTrendChartDisplayComponent } from './component/model-trend-chart-display/model-trend-chart-display.component';
import { DonutChartComponent } from './component/donut-chart/donut-chart.component';
LicenseManager.setLicenseKey(
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

@NgModule({
  declarations: [
    ChartDisplayComponent,
    SimpleChartDisplayComponent,
    ChartSelectorComponent,
    TableDisplayComponent,
    BarChartDisplayComponent,
    UpdateLimitsDialogComponent,
    ModelTrendChartComponent,
    ModelTrendChartDisplayComponent,
    ModelHistoryGridComponent,
    FavoriteFormatterComponent,
    DonutChartComponent,
  ],
  imports: [
    CommonModule,
    AtxMaterialModule,
    FormsModule,
    SharedApiModule,
    ReactiveFormsModule,
    HighchartsChartModule,
    AgGridModule,
  ],
  providers: [ChartFacade, ModelChartFacade],
  exports: [
    ChartSelectorComponent,
    ChartDisplayComponent,
    ModelTrendChartComponent,
    ModelTrendChartDisplayComponent,
    SimpleChartDisplayComponent,
    ModelHistoryGridComponent,
    DonutChartComponent,
  ],
})
export class AtxChartModuleV2 {}
