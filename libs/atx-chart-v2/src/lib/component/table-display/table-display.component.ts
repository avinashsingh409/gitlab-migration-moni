import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ITableData } from '../../models/bar-chart-data';
import {
  isGroupedSeriesTable,
  isStandardTable,
} from '../../service/chart.service';
import { BarChartService } from '../../service/bar-chart.service';
import { Subject } from 'rxjs';
import { createTableDataSource } from '../../service/table-utilities';
import { IProcessedTrend } from '@atonix/atx-core';

@Component({
  selector: 'atx-table-display',
  templateUrl: './table-display.component.html',
  styleUrls: ['./table-display.component.scss'],
})
export class TableDisplayComponent implements OnChanges {
  @Input() trend: IProcessedTrend;
  @Input() isGroupedSeries: boolean;
  @Input() theme: string;
  @Output() highchartsOptionsUpdated = new EventEmitter<Highcharts.Options>();

  // the indices hold the numbers in the range [0, 1, ... n] where
  // n is the number of columns in the table - 1.  And this is only used
  // if the user wants a table (instead of a chart, etc).
  indices: number[];
  public unsubscribe$ = new Subject<void>();
  tableData: ITableData;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.trend?.currentValue) {
      const selectedTrend = changes.trend
        .currentValue as Readonly<IProcessedTrend>;
      if (isGroupedSeriesTable(selectedTrend)) {
        const data =
          BarChartService.translatePDTrendToBarChartData(selectedTrend);
        const dataSource = BarChartService.getTableData(data);
        // This fills an array with indices for the columns. It is [0, 1 ... n]
        // We use it to iterate through the columns and the data.
        this.indices = Array.from(Array(dataSource.columns.length).keys());
        this.tableData = {
          title: selectedTrend.trendDefinition.Title,
          columns: dataSource.columns,
          // we do this mapping because the far left column label is blank: "",
          // and the mat-table breaks if you feed it a blank column id.
          // so we append some text to make the blank ones not blank.
          columnIds: dataSource.columns.map(
            (columnText, index) => `columnID: ${index}`
          ),
          data: dataSource.data,
          footers: dataSource.footer,
        };
      } else if (isStandardTable(selectedTrend)) {
        const dataSource = createTableDataSource(
          selectedTrend,
          this.isGroupedSeries,
          this.theme,
          this.trend.labelIndex
        );

        this.highchartsOptionsUpdated.emit(dataSource.highchartsOptions);
        // This fills an array with indices for the columns. It is [0, 1 ... n]
        // We use it to iterate through the columns and the data.
        this.indices = Array.from(Array(dataSource.columns.length).keys());
        this.tableData = {
          title: selectedTrend.trendDefinition.Title,
          columns: dataSource.columns,
          // we do this mapping because the far left column label is blank: "",
          // and the mat-table breaks if you feed it a blank column id.
          // so we append some text to make the blank ones not blank.
          columnIds: dataSource.columns.map(
            (columnText) => 'columnID: "' + columnText + '"'
          ),
          data: dataSource.data,
        };
      }
    }
  }
}
