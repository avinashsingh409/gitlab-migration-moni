import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BarChartDisplayComponent } from './bar-chart-display.component';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { HighchartsChartModule } from 'highcharts-angular';

describe('BarChartDisplayComponent', () => {
  let component: BarChartDisplayComponent;
  let fixture: ComponentFixture<BarChartDisplayComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        HttpClientTestingModule,
        HighchartsChartModule,
        NoopAnimationsModule,
      ],
      declarations: [BarChartDisplayComponent],
      providers: [
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarChartDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
