/* eslint-disable @typescript-eslint/no-empty-function */
import {
  CellDoubleClickedEvent,
  CellValueChangedEvent,
  ColumnApi,
  EnterpriseCoreModule,
  FilterChangedEvent,
  GridApi,
  GridOptions,
  GridReadyEvent,
  Module,
  ServerSideRowModelModule,
  ServerSideStoreType,
  SetFilterModule,
  _,
} from '@ag-grid-enterprise/all-modules';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ModelHistoryGridDatasource } from './model-history-grid.data-source';
import { Subject } from 'rxjs';
import { dateComparator, isNil } from '@atonix/atx-core';
import { FavoriteFormatterComponent } from './formatter/favorite-formatter.component';
import { IModelHistoryFilters, INDModelActionItem } from '@atonix/shared/api';
import { dateFormatter } from '@atonix/shared/utils';
import {
  IModelHistoryEventStateChange,
  setFavorite,
  setModelHistoryFilters,
  setNote,
} from '../../models/model-history-grid.state';

@Component({
  selector: 'atx-model-history-grid',
  templateUrl: './model-history-grid.component.html',
  styleUrls: ['./model-history-grid.component.scss'],
  providers: [ModelHistoryGridDatasource],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelHistoryGridComponent implements OnChanges, OnDestroy {
  @Input() modelID: string;
  @Input() modelName: string;
  @Input() modelFilters: IModelHistoryFilters;
  @Input() refreshDate: Date;
  @Input() reloadGrid = false;
  @Input() theme: string;
  @Output() modelHistoryEventChange =
    new EventEmitter<IModelHistoryEventStateChange>();
  @Output() afterGridReload = new EventEmitter();
  public rowCount!: string;
  public modules: Module[] = [
    EnterpriseCoreModule,
    ServerSideRowModelModule,
    SetFilterModule,
  ];
  private gridApi!: GridApi;
  private onDestroy = new Subject<void>();
  private storeType: ServerSideStoreType = 'partial';
  public gridOptions: GridOptions = {
    rowModelType: 'serverSide',
    serverSideStoreType: this.storeType,
    animateRows: true,
    debug: false,
    rowSelection: 'single',
    suppressRowClickSelection: true,
    suppressCellFocus: true,
    suppressMultiSort: true,
    tooltipShowDelay: 0,
    columnDefs: [
      {
        colId: 'ModelID',
        headerName: 'Model',
        field: 'ModelID',
        filter: 'agNumberColumnFilter',
        sortable: false,
        hide: true,
        floatingFilter: false,
        suppressMenu: true,
        filterParams: {
          suppressAndOrCondition: true,
          filterOptions: ['equals'],
        },
      },
      {
        colId: 'Favorite',
        width: 65,
        headerName: 'Favorite',
        headerTooltip: 'Favorite',
        field: 'Favorite',
        cellRenderer: 'favoriteRenderer',
        sortable: true,
        hide: false,
        filter: 'agSetColumnFilter',
        filterParams: {
          values: ['true', 'false'],
        },
        onCellDoubleClicked: (event: CellDoubleClickedEvent) => {
          this.favoriteChanged(event);
        },
      },
      {
        colId: 'History',
        headerName: 'History',
        field: 'ModelActionItemTypeAbbrev',
        sortable: true,
        width: 250,
        filter: 'agSetColumnFilter',
        cellClass: 'cell-selectable',
        filterParams: {
          values: [
            'Diagnose Set',
            'Diagnose Cleared',
            'Watch Set',
            'Watch Cleared',
            'Watch Expiration',
            'Model Maintenance Set',
            'Model Maintenance Cleared',
            'Issue Created',
            'Issue Closed',
            'Note Added',
            'Ignore Set',
            'Clear Alert Status',
            'Ignore Expiration',
            'Watch Override',
            'Stop Ignoring',
            'Quick Watch Set',
          ],
        },
      },
      {
        colId: 'ChangeDate',
        headerName: 'TimeStamp',
        field: 'ChangeDate',
        sortable: true,
        width: 250,
        filter: 'agDateColumnFilter',
        cellClass: 'cell-selectable',
        valueFormatter: dateFormatter,
        filterParams: {
          comparator: dateComparator,
          inRangeInclusive: true,
          suppressAndOrCondition: true,
          filterOptions: ['inRange'],
          debounceMs: 1000,
        },
      },
      {
        colId: 'Executor',
        headerName: 'User',
        field: 'Executor',
        sortable: true,
        width: 250,
        cellClass: 'cell-selectable',
        tooltipField: 'Executor',
        filter: 'agTextColumnFilter',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
      },
      {
        colId: 'Note',
        headerName: 'Note',
        field: 'NoteText',
        sortable: true,
        filter: 'agTextColumnFilter',
        cellClass: ['cell-selectable', 'cell-wrap-text'],
        flex: 1,
        autoHeight: true,
        tooltipField: 'NoteText',
        filterParams: {
          filterOptions: ['contains'],
          suppressAndOrCondition: true,
        },
        editable: true,
        onCellValueChanged: (event: CellValueChangedEvent) => {
          this.noteChanged(event);
        },
      },
    ],
    defaultColDef: {
      resizable: true,
      filter: true,
      floatingFilter: true,
      sortable: true,
    },
    onColumnResized: (params) => {
      params.api.resetRowHeights();
    },
    getRowId: (params) => {
      return (
        params.data?.ModelActionItemArchiveID ??
        params.data?.ModelConditionNoteID
      );
    },
    onGridReady: (event: GridReadyEvent) => {
      event.api.setFilterModel(this.getFilterModel());
      event.api.setServerSideDatasource(this.modelHistoryGridDatasource);
      this.gridApi = event.api;
    },
    components: {
      favoriteRenderer: FavoriteFormatterComponent,
    },
    onFilterChanged: (event: FilterChangedEvent) => {
      this.filterChanged(event);
    },
  };
  constructor(
    private modelHistoryGridDatasource: ModelHistoryGridDatasource,
    private changeDetector: ChangeDetectorRef
  ) {
    modelHistoryGridDatasource.onRowCountChanged = (value: number) => {
      this.rowCount = isNil(value) || value === 0 ? '-' : String(value);
      changeDetector.markForCheck();
    };
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.gridApi && changes?.modelID) {
      this.setModelFilter(changes.modelID.currentValue);
      this.reload();
    }

    if (changes?.reloadGrid.currentValue || changes?.refreshDate.currentValue) {
      this.reload();
    }
  }

  favoriteChanged(event: CellDoubleClickedEvent) {
    const value: INDModelActionItem = event.data;
    this.modelHistoryEventChange.emit(setFavorite(!value.Favorite, value));
    event.node.updateData({ ...value, Favorite: !value.Favorite });
  }

  noteChanged(event: CellValueChangedEvent) {
    const value: INDModelActionItem = event.data;
    this.modelHistoryEventChange.emit(setNote(value.NoteText, value));
  }

  setModelFilter(newValue?: number) {
    const val = newValue ?? this.modelID;

    if (this.gridApi) {
      const filterInstance = this.gridApi.getFilterInstance('ModelID');
      if (filterInstance) {
        const oldModel = filterInstance.getModel();
        if (oldModel?.filter !== val) {
          filterInstance.setModel({ type: 'equals', filter: val });
        }
      }
    }
  }

  getFilterModel() {
    const filterModel: any = {
      ModelID: { type: 'equals', filter: this.modelID },
    };
    if (this.modelFilters) {
      if (this.modelFilters.historyTypes) {
        filterModel.History = {
          values: this.modelFilters.historyTypes,
          filterType: 'set',
        };
      }
      if (this.modelFilters.start && this.modelFilters.end) {
        filterModel.ChangeDate = {
          dateFrom: this.modelFilters.start,
          dateTo: this.modelFilters.end,
          filterType: 'date',
          type: 'inRange',
        };
      }
      if (!isNil(this.modelFilters.favorite)) {
        filterModel.Favorite = {
          values: [this.modelFilters.favorite ? 'true' : 'false'],
          filterType: 'set',
        };
      }
      if (this.modelFilters.note) {
        filterModel.Note = {
          filter: this.modelFilters.note,
          filterType: 'text',
          type: 'contains',
        };
      }
      if (this.modelFilters.user) {
        filterModel.Executor = {
          filter: this.modelFilters.user,
          filterType: 'text',
          type: 'contains',
        };
      }
    } else {
      filterModel.History = {
        values: [
          'Watch Set',
          'Diagnose Set',
          'Diagnose Cleared',
          'Model Maintenance Set',
          'Model Maintenance Cleared',
          'Note Added',
        ],
        filterType: 'set',
      };
    }
    if (this.modelID) {
      filterModel.ModelID = {
        type: 'equals',
        filter: this.modelID,
        filterType: 'number',
      };
    }

    return filterModel;
  }

  filterChanged(event: FilterChangedEvent) {
    const filters = event.api.getFilterModel();
    const newFilters: IModelHistoryFilters = {
      historyTypes: [],
      start: null,
      end: null,
      user: null,
      note: null,
      favorite: null,
    };
    if (filters.Favorite) {
      newFilters.favorite = filters.Favorite.values[0] === 'true';
    }
    if (filters.History) {
      newFilters.historyTypes = filters.History.values;
    }
    if (filters.ChangeDate) {
      newFilters.start = filters.ChangeDate.dateFrom;
      newFilters.end = filters.ChangeDate.dateTo;
    }
    if (filters.Executor) {
      newFilters.user = filters.Executor.filter;
    }
    if (filters.Note) {
      newFilters.note = filters.Note.filter;
    }
    if (!_.jsonEquals(newFilters, this.modelFilters)) {
      this.modelHistoryEventChange.emit(setModelHistoryFilters(newFilters));
    }
  }

  reload() {
    if (this.gridApi) {
      this.gridApi.onFilterChanged();
      this.afterGridReload.emit();
    }
  }
}
