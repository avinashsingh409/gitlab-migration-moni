import {
  IServerSideDatasource,
  IServerSideGetRowsParams,
} from '@ag-grid-enterprise/all-modules';
import { Injectable } from '@angular/core';
import { isNil } from '@atonix/atx-core';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class ModelHistoryGridDatasource implements IServerSideDatasource {
  private unsubscribe$ = new Subject<void>();
  pageSize = 100;
  rowCount?: number;
  onRowCountChanged!: (value: number) => void;
  private historyToNumberDict: { [key: string]: number } = {};

  constructor(private alertsFrameworkService: AlertsFrameworkService) {
    this.historyToNumberDict['Diagnose Set'] = 1;
    this.historyToNumberDict['Diagnose Cleared'] = 2;
    this.historyToNumberDict['Watch Set'] = 3;
    this.historyToNumberDict['Watch Cleared'] = 4;
    this.historyToNumberDict['Watch Expiration'] = 5;
    this.historyToNumberDict['Model Maintenance Set'] = 6;
    this.historyToNumberDict['Model Maintenance Cleared'] = 7;
    this.historyToNumberDict['Issue Created'] = 8;
    this.historyToNumberDict['Issue Closed'] = 9;
    this.historyToNumberDict['Note Added'] = 10;
    this.historyToNumberDict['Ignore Set'] = 11;
    this.historyToNumberDict['Clear Alert Status'] = 12;
    this.historyToNumberDict['Ignore Expiration'] = 13;
    this.historyToNumberDict['Watch Override'] = 14;
    this.historyToNumberDict['Stop Ignoring'] = 15;
    this.historyToNumberDict['Quick Watch Set'] = 16;
  }
  getRows(params: IServerSideGetRowsParams): void {
    const modelID: number = params?.request?.filterModel?.ModelID?.filter;
    if (!isNil(modelID) && !isNaN(modelID)) {
      let favorite!: boolean;
      let actionItemTypeID!: number[];
      let start!: Date;
      let end!: Date;
      let executor!: string;
      let note!: string;

      if (params?.request?.filterModel?.Favorite) {
        favorite = params?.request.filterModel.Favorite.values[0] === 'true';
      }

      if (params?.request?.filterModel.History) {
        actionItemTypeID = [];
        // eslint-disable-next-line no-unsafe-optional-chaining
        for (const n of params?.request.filterModel.History.values) {
          actionItemTypeID.push(this.historyToNumberDict[n]);
        }
      }

      if (
        params?.request?.filterModel?.ChangeDate &&
        params?.request?.filterModel?.ChangeDate.dateFrom &&
        params?.request?.filterModel?.ChangeDate.dateTo
      ) {
        start = new Date(params?.request?.filterModel?.ChangeDate.dateFrom);
        end = new Date(params?.request?.filterModel?.ChangeDate.dateTo);
      }

      if (params?.request?.filterModel?.Executor) {
        executor = params?.request.filterModel.Executor.filter;
      }

      if (params?.request?.filterModel?.Note) {
        note = params?.request.filterModel.Note.filter;
      }

      const page = params?.request.startRow / this.pageSize;
      let sortOrder!: boolean;
      let sort!: string;
      if (params?.request?.sortModel && params?.request.sortModel.length > 0) {
        const sortModel: { colId: string; sort: 'asc' | 'desc' } =
          params?.request.sortModel[0];
        sortOrder = sortModel.sort === 'asc';
        if (sortModel.colId === 'History') {
          sort = 'ActionItemTypeAbbrev';
        } else if (sortModel.colId === 'Favorite') {
          sort = 'Favorite';
        } else if (sortModel.colId === 'ChangeDate') {
          sort = 'HistoryTimestamp';
        } else if (sortModel.colId === 'Executor') {
          sort = 'Executor';
        } else if (sortModel.colId === 'Note') {
          sort = 'NoteText';
        }
      }

      if (page === 0) {
        this.setRowCount(0);
      }

      this.alertsFrameworkService
        .getAlertTimeLine(
          modelID,
          page,
          this.pageSize,
          sort,
          sortOrder,
          favorite,
          actionItemTypeID,
          start,
          end,
          executor,
          note
        )
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (n) => {
            params.success({ rowData: n.Items || [], rowCount: n.TotalItems });
            this.setRowCount(n.TotalItems);
          },
          (error: unknown) => {
            params.fail();
            console.error('Could not retrieve model history');
          }
        );
    } else if (params) {
      params.success({ rowData: [], rowCount: 0 });
    }
  }

  private setRowCount(value: number) {
    this.rowCount = value;
    if (this.onRowCountChanged) {
      this.onRowCountChanged(value);
    }
  }
}
