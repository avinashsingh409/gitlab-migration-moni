import { AgGridModule } from '@ag-grid-community/angular';
import { GridApi, LicenseManager } from '@ag-grid-enterprise/all-modules';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AlertsFrameworkService } from '@atonix/shared/api';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { ModelHistoryGridComponent } from './model-history-grid.component';
LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

describe('ModelHistoryGridComponent', () => {
  let component: ModelHistoryGridComponent;
  let fixture: ComponentFixture<ModelHistoryGridComponent>;
  let mockAlertsFxService: AlertsFrameworkService;
  let mockGridApi: GridApi;
  beforeEach(async () => {
    mockGridApi = createMockWithValues(GridApi, {
      destroy: jest.fn(),
    });

    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AgGridModule],
      declarations: [ModelHistoryGridComponent],
      providers: [
        {
          provide: AlertsFrameworkService,
          useValue: mockAlertsFxService,
        },
        { provide: GridApi, useValue: mockGridApi },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelHistoryGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
