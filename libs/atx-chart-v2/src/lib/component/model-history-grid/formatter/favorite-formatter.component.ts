import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from '@ag-grid-community/angular';
import { IAfterGuiAttachedParams } from '@ag-grid-enterprise/all-modules';

@Component({
  selector: 'atx-favorite-formatter',
  template: '<mat-icon>{{value}}</mat-icon>',
})
export class FavoriteFormatterComponent implements ICellRendererAngularComp {
  value!: 'star' | 'star_border';

  agInit(params: any) {
    this.value = params?.data?.Favorite ? 'star' : 'star_border';
  }

  refresh(params: any): boolean {
    this.value = params?.data?.Favorite ? 'star' : 'star_border';
    return true;
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {}
}
