import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ChangeDetectionStrategy,
  HostListener,
  OnDestroy,
} from '@angular/core';
import { ITableData } from '../../models/bar-chart-data';
import {
  createHighchartsDefinition,
  isBarOrColumnChart,
  isGroupedSeriesChart,
  isGroupedSeriesTable,
  isImage,
  isStandardTable,
} from '../../service/chart.service';
import { HighchartsService } from '../../service/highcharts.service';
import { IBtnGrpStateChange } from '../../models/button-group-state';
import {
  changeLabels,
  editChart,
  removeActiveYAxis,
  updateLimits,
  createInitialRectBounds,
  resetLimits,
  getDisplayedAxes,
  dataCursorToggle,
  legendItemToggle,
} from '../../service/utilities';
import { ExportingOptions, Options } from 'highcharts';
import { MatDialog } from '@angular/material/dialog';
import { UpdateLimitsDialogComponent } from '../update-limits-dialog/update-limits-dialog.component';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import { IAssetVariableTypeTagMap, IProcessedTrend } from '@atonix/atx-core';
import { ImagesFrameworkService, ICorrelationData } from '@atonix/shared/api';
import { DarkUnicaTheme } from '@atonix/shared/utils';
import { createSummaryChart } from '../../service/summary-chart-utilities';
import { LabelIndex, LabelIndexMapping } from '../../models/label-index';
import Highcharts from 'highcharts';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Subject, debounceTime, takeUntil } from 'rxjs';
import { ChartOpModeService } from '../../service/chartopmode.service';
import { AssetTagMap } from '../../models/asset-tag-map';

// ****************************************************************
// this file is much more clear if you view it after doing a "fold all"
// ****************************************************************
@Component({
  selector: 'atx-simple-chart-display',
  templateUrl: './simple-chart-display.component.html',
  styleUrls: ['./simple-chart-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleChartDisplayComponent implements OnChanges, OnDestroy {
  @Input() theme: string;
  @Input() trend: Readonly<IProcessedTrend>;
  @Input() correlationTrend: ICorrelationData[];
  @Input() showToggle: boolean;
  @Input() showEditChart: boolean;
  @Input() showDataCursorToggle: boolean;
  @Input() showCreateOpMode: boolean;
  @Input() allowHiddenLabel: boolean;
  @Input() loading: boolean;
  @Input() isToggled: boolean;
  @Input() hideTitle: boolean;
  @Output() btnGrpStateChange = new EventEmitter<IBtnGrpStateChange>();

  trendDisplayType: '' | 'chart' | 'table' | 'image' | 'multi';
  // data for charts
  highcharts: any;
  chartConfiguration: Highcharts.Options;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)

  // data for image
  imagePath: string;
  ctrlKeyPressed = false;

  //Form values for creating an Op Mode
  //Also act as the State of the selected area
  //The Date form fields time isn't updated when
  //the time field is changed
  titleControl = new FormControl('', [Validators.required]);
  startGroup = new FormGroup({
    date: new FormControl<Date>(null, [Validators.required]),
    time: new FormControl('', [Validators.required]),
  });
  endGroup = new FormGroup({
    date: new FormControl<Date>(null, [Validators.required]),
    time: new FormControl('', [Validators.required]),
  });
  associationControl = new FormControl(null, [Validators.required]);

  //External ID and Associated Asset for the last newly created EP Op Mode
  newOpMode = new BehaviorSubject<{
    extId: string;
    assetGuid: string;
  }>(null);

  dragPosition = { x: -1, y: -364 }; //adjusts op mode menu starting position

  // data for table
  tableData: ITableData;
  // tableSummaryTypes: ISummaryType[]; // TODO: Load this using ngrx global store

  // data for multi-charts (i.e, multiple area trends)
  multiChartData: Highcharts.Options[];
  exportSettings: ExportingOptions;

  isGroupedSeries = false;
  labelDisplays = Object.keys(LabelIndex)
    .filter((key) => !isNaN(Number(LabelIndex[key])))
    .map((itm) => LabelIndex[itm]);
  labelDisplayNames = LabelIndexMapping;

  showCreateOpModeIcon = false;
  showOpModeMenu = false;

  associationsAssets: AssetTagMap[] = [];
  associationsTags: AssetTagMap[] = [];

  unsubscribe$ = new Subject<void>();

  constructor(
    private highchartsService: HighchartsService,
    imagesFrameworkService: ImagesFrameworkService,
    private dialog: MatDialog,
    private chartOpModeService: ChartOpModeService
  ) {
    this.highcharts = highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();

    this.startGroup.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.refreshChart();
      });
    this.endGroup.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.refreshChart();
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.trend && this.showCreateOpMode) {
      //for hiding and showing create excusion period opmode icon
      if (
        changes.trend.currentValue.trendDefinition.ChartTypeID === 1 ||
        changes.trend.currentValue.trendDefinition.ChartTypeID === 2 ||
        changes.trend.currentValue.trendDefinition.ChartTypeID === 3 ||
        changes.trend.currentValue.trendDefinition.ChartTypeID === 4 ||
        changes.trend.currentValue.trendDefinition.Title === 'Model Context'
      ) {
        this.showCreateOpModeIcon = true;
      } else {
        this.showCreateOpModeIcon = false;
      }

      //formats and get potential tag/asset associations for this trend
      //for listing them in menu mat-select
      this.associationsAssets = this.getAssetAssociations(
        changes.trend.currentValue
      );
      this.associationsTags = this.getTagAssociations(
        changes.trend.currentValue
      );
    }

    if (changes.trend?.currentValue) {
      const myTrend = changes.trend.currentValue as Readonly<IProcessedTrend>;
      this.isGroupedSeries = this.trend?.trendDefinition?.BandAxis
        ? true
        : false;
      // The following block resets and clears a few things.
      {
        this.imagePath = null;
        this.chartConfiguration = null;
        this.updateChart = true;
        this.tableData = null;
        this.trendDisplayType = '';
      }
      if (isImage(myTrend)) {
        this.trendDisplayType = 'image';
        this.imagePath = myTrend.trendDefinition.Path;
        if (
          myTrend.trendDefinition.Math?.substr(0, 8).toLowerCase() ===
          'refresh='
        ) {
          this.imagePath += '?cacheBust=1';
          // There is supposed to be some code in here to upate the image on a certain interval.  I am not going to put it in now,
          // since this feature is one that is not ever used.  For future reference the
          // code is in BV.PowerPlantMD.Web.Sites\Common\Charts\hchart.ts
          // lines 115-138 in the configure callback.
        }
      } else if (isGroupedSeriesTable(myTrend) || isStandardTable(myTrend)) {
        this.trendDisplayType = 'table';
      } else if (isGroupedSeriesChart(myTrend)) {
        // The bar-chart-display component will take care of this.
        this.trendDisplayType = 'multi';
      } else if (isBarOrColumnChart(myTrend)) {
        this.trendDisplayType = 'chart';
        const config = createSummaryChart(
          myTrend,
          this.isGroupedSeries,
          this.theme,
          myTrend.labelIndex,
          this.exportSettings
        );
        this.setHighchart(config);
      } else {
        this.trendDisplayType = 'chart';

        const config = createHighchartsDefinition(
          myTrend.trendDefinition,
          this.isGroupedSeries,
          this.theme,
          myTrend.measurements,
          myTrend.startDate,
          myTrend.endDate,
          myTrend.labelIndex,
          this.isToggled,
          this.allowHiddenLabel,
          this.exportSettings,
          myTrend.showDataCursor,
          false,
          this.correlationTrend,
          this.getHighLightBand()
        );

        this.setHighchart(config);
      }
    } else if (
      changes.correlationTrend &&
      changes.correlationTrend.currentValue
    ) {
      this.trendDisplayType = 'chart';
      const myTrend = this.trend;

      const config = createHighchartsDefinition(
        myTrend.trendDefinition,
        this.isGroupedSeries,
        this.theme,
        myTrend.measurements,
        myTrend.startDate,
        myTrend.endDate,
        myTrend.labelIndex,
        this.isToggled,
        this.allowHiddenLabel,
        this.exportSettings,
        myTrend.showDataCursor,
        false,
        this.correlationTrend,
        this.getHighLightBand()
      );

      this.setHighchart(config);
    }
  }

  //listens for control key
  @HostListener('window:keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    this.ctrlKeyPressed = event.ctrlKey;
  }
  @HostListener('window:keyup', ['$event'])
  keyUpEvent(event: KeyboardEvent) {
    this.ctrlKeyPressed = event.ctrlKey;
  }

  setHighchart(config: Options) {
    if (this.hideTitle && config) {
      config.title = undefined;
    }
    if (this.theme === 'dark') {
      config = this.highcharts.merge(DarkUnicaTheme, config);
    }

    config.chart.events = {
      render: this.setOnRender(),
      //replaces default section handler that runs when you click and drag the chart
      selection: (event) => {
        //when ctrl key is pressed we want to select the highlighted area
        if (
          this.ctrlKeyPressed &&
          event.xAxis &&
          event.xAxis[0] &&
          this.showCreateOpMode
        ) {
          const xMin = event.xAxis[0].min;
          const xMax = event.xAxis[0].max;
          this.startGroup.setValue(
            {
              date: new Date(xMin),
              time: new Date(xMin).toTimeString().slice(0, 5),
            },
            { emitEvent: false }
          );
          this.endGroup.setValue(
            {
              date: new Date(xMax),
              time: new Date(xMax).toTimeString().slice(0, 5),
            },
            { emitEvent: false }
          );

          this.refreshChart();
          return false;
        }
        //else we pass true to continue default behavior (zooming)
        else {
          return true;
        }
      },
    };

    config.plotOptions.series.events.legendItemClick = (event) => {
      this.btnGrpStateChange.emit(
        legendItemToggle(event.target.userOptions.legendIndex)
      );
    };

    this.chartConfiguration = config;
    this.updateChart = true;
  }

  onToggleOpModeMenu() {
    this.showOpModeMenu = !this.showOpModeMenu;
    //if toggling off
    if (!this.showOpModeMenu) {
      this.clearFormControls();
    }
  }

  onCreateOpMode() {
    const opModeDefinitionTitle = this.titleControl.value;
    const logicStartDate = this.combineDateAndTime(
      this.startGroup.get('date').value,
      this.startGroup.get('time').value
    );
    const logicEndDate = this.combineDateAndTime(
      this.endGroup.get('date').value,
      this.endGroup.get('time').value
    );
    const associations = this.associationControl.value;

    this.chartOpModeService
      .createOpMode(
        opModeDefinitionTitle,
        logicStartDate,
        logicEndDate,
        associations
      )
      .subscribe({
        next: (res) => {
          let assetGuid = '';
          //finds a assetGuid from the associations for redirecting
          for (const a of associations) {
            if (a.aGuid) {
              assetGuid = a.aGuid;
              break;
            } else if (a.assetGuid) {
              assetGuid = a.assetGuid;
              break;
            }
          }
          this.newOpMode.next({
            extId: res.data.opModeDefinitionExtID,
            assetGuid,
          });
        },
      });
  }

  goToNewOpMode() {
    this.chartOpModeService.openOpMode(
      this.newOpMode.value.extId,
      this.newOpMode.value.assetGuid
    );
  }

  private clearFormControls() {
    this.titleControl.reset();
    this.associationControl.reset();
    this.startGroup.reset();
    this.endGroup.reset();
    this.newOpMode.next(null);
  }

  private refreshChart() {
    const myTrend = this.trend;
    const config = createHighchartsDefinition(
      myTrend.trendDefinition,
      this.isGroupedSeries,
      this.theme,
      myTrend.measurements,
      myTrend.startDate,
      myTrend.endDate,
      myTrend.labelIndex,
      this.isToggled,
      this.allowHiddenLabel,
      this.exportSettings,
      myTrend.showDataCursor,
      false,
      this.correlationTrend,
      this.getHighLightBand()
    );

    this.setHighchart(config);
  }

  //Gets and formats potential asset associations from a trend for op mode creation
  private getAssetAssociations(trend: IProcessedTrend) {
    const series = trend.trendDefinition.Series;
    const associationsAssets: AssetTagMap[] = [];
    series.forEach((tagSeries) => {
      if (tagSeries.MapData[0] && tagSeries.MapData[0].Map) {
        const map: IAssetVariableTypeTagMap = tagSeries.MapData[0].Map;

        const assetAssociation: AssetTagMap = {
          opModeDefinitionAssetTagMapID: -1,
          bringDescendants: true,
          include: true,
          assetAbbrev: map.Asset.AssetAbbrev,
          assetGuid: map.Asset.GlobalId,
        };

        if (
          !associationsAssets.find(
            (association) =>
              association.assetGuid === assetAssociation.assetGuid
          )
        ) {
          associationsAssets.push(assetAssociation);
        }
      }
    });
    return associationsAssets;
  }

  ////Gets and formats potential tag associations from a trend for op mode creation
  private getTagAssociations(trend: IProcessedTrend) {
    const series = trend.trendDefinition.Series;
    const associationsTags: AssetTagMap[] = [];
    series.forEach((tagSeries) => {
      if (tagSeries.MapData[0] && tagSeries.MapData[0].Map) {
        const map: IAssetVariableTypeTagMap = tagSeries.MapData[0].Map;

        const tagAssociation = {
          opModeDefinitionAssetTagMapID: -1,
          bringDescendants: false,
          include: true,
          tagDesc: map.Tag.TagDesc,
          tagName: map.Tag.TagName,
          assetVariableTypeTagMapID: map.AssetVariableTypeTagMapID,
          aGuid: map.Asset.GlobalId, //used later when redirecting to a op mode needs to be a
          //different name than 'assetGuid' so API doesn't think this is an asset association
        };

        if (
          !associationsTags.find(
            (association) =>
              association.assetVariableTypeTagMapID ===
              tagAssociation.assetVariableTypeTagMapID
          )
        ) {
          associationsTags.push(tagAssociation);
        }
      }
    });
    return associationsTags;
  }

  //Gets the highlight section in the correct format to pass to highcharts
  private getHighLightBand(): Highcharts.XAxisPlotBandsOptions {
    if (!this.startGroup.get('date').value || !this.endGroup.get('date').value)
      return {};

    //For the 'date' form fields we only care about the year/month/day, time is in another form field
    const startDate = this.startGroup.get('date').value;
    const startTime = this.startGroup.get('time').value;
    const newStartDate = this.combineDateAndTime(startDate, startTime);

    const endDate = this.endGroup.get('date').value;
    const endTime = this.endGroup.get('time').value; //In form hh:mm
    const newEndDate = this.combineDateAndTime(endDate, endTime);

    const highlight: Highcharts.XAxisPlotBandsOptions = {
      color: 'rgba(255,77,77,0.12)',
      from: newStartDate.getTime(),
      to: newEndDate.getTime(),
      zIndex: -1,
    };

    return highlight;
  }

  //Combines the 'date' and 'time' (HH:MM) form fields
  private combineDateAndTime(date: Date, time: string): Date {
    //we ignore the date's hours/minutes/etc
    const givenDate = date;
    givenDate.setMilliseconds(0);
    givenDate.setSeconds(0);
    givenDate.setMinutes(0);
    givenDate.setHours(0);

    const endTime = time; //In form hh:mm

    const hoursMins = endTime.split(':'); //['hh', 'mm']
    const minMilis = 1000 * 60;
    const hoursMilis = minMilis * 60;

    const newDate = new Date(
      givenDate.getTime() +
        Number(hoursMins[0]) * hoursMilis +
        Number(hoursMins[1]) * minMilis
    );

    return newDate;
  }

  public setOnRender() {
    return () => {
      setTimeout(() => {
        const yAxis: any = Array.from(
          document.querySelectorAll(`.chart-${this.trend.id} .highcharts-yaxis`)
        );
        yAxis.map((y, index) => {
          const rect = createInitialRectBounds(y);
          // This will add click event to the clickable area
          rect.addEventListener('click', (event) => {
            removeActiveYAxis();
            rect.setAttribute('class', 'activeYAxis rectBounds');
            rect.setAttribute(
              'style',
              'fill-opacity: 0; stroke-width: 1px; stroke: #6ea09d;'
            );

            const chartBackground: any = document.querySelector(
              `.chart-${this.trend.id}`
            );
            const chartBounding = chartBackground?.getBoundingClientRect();
            const displayedAxes = getDisplayedAxes(this.trend.trendDefinition);

            const dialogRef = this.dialog.open(UpdateLimitsDialogComponent, {
              width: '400px',
              position: {
                left: `${chartBounding?.x + 10}px`,
                top: `${chartBounding?.y + chartBounding?.height - 90}px`,
              },
              panelClass: 'custom-dialog-container',
              data: {
                AxisIndex: index,
                Min: displayedAxes[index]?.Min,
                Max: displayedAxes[index]?.Max,
                OriginalMin: displayedAxes[index]?.OriginalMin,
                OriginalMax: displayedAxes[index]?.OriginalMax,
              },
            });

            dialogRef.componentInstance.updateValues.subscribe(
              (data: IUpdateLimitsData) => {
                this.updateLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.componentInstance.resetValues.subscribe(
              (data: number) => {
                this.resetLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.afterClosed().subscribe((result) => {
              removeActiveYAxis();
            });
          });

          y.parentElement.appendChild(rect);
        });
      }, 100);
    };
  }

  public changeLabels(index: number) {
    this.btnGrpStateChange.emit(changeLabels(index));
  }

  public editChart() {
    this.btnGrpStateChange.emit(editChart());
  }

  public dataCursorToggle() {
    this.btnGrpStateChange.emit(dataCursorToggle());
  }

  public updateLimits(event: IUpdateLimitsData) {
    this.btnGrpStateChange.emit(updateLimits(event));
  }

  public resetLimits(event: number) {
    this.btnGrpStateChange.emit(resetLimits(event));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
