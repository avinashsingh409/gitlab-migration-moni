import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  turnEmptyModelTrendToHighcharts,
  turnModelTrendToHighcharts,
  getTitleFromModel,
} from '../../service/chart.service';
import { HighchartsService } from '../../service/highcharts.service';
import { IBtnGrpStateChange } from '../../models/button-group-state';
import {
  removeActiveYAxis,
  updateLimits,
  createInitialRectBounds,
  resetLimits,
  copyToClipboard,
} from '../../service/utilities';
import { ExportingOptions, Options } from 'highcharts';
import { MatDialog } from '@angular/material/dialog';
import { UpdateLimitsDialogComponent } from '../update-limits-dialog/update-limits-dialog.component';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import {
  IAssetModelChartingData,
  ImagesFrameworkService,
  INDModel,
  INDModelActionItemAnalysis,
} from '@atonix/shared/api';
import { DarkUnicaTheme } from '@atonix/shared/utils';

@Component({
  selector: 'atx-model-trend-chart-display',
  templateUrl: './model-trend-chart-display.component.html',
  styleUrls: ['./model-trend-chart-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelTrendChartDisplayComponent implements OnChanges {
  @Input() model: INDModel;
  @Input() data: IAssetModelChartingData;
  @Input() actionData: INDModelActionItemAnalysis[];
  @Input() loading: boolean;
  @Input() multiselected: boolean;
  @Input() moveMarkers: boolean;
  @Output() btnGrpStateChange = new EventEmitter<IBtnGrpStateChange>();

  // data for charts
  highcharts: any;
  chartConfiguration: Highcharts.Options;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)

  exportSettings: ExportingOptions;

  titleSelected = false;

  constructor(
    private highchartsService: HighchartsService,
    imagesFrameworkService: ImagesFrameworkService,
    private dialog: MatDialog
  ) {
    this.highcharts = highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.loading?.currentValue ||
      changes.model?.currentValue ||
      changes.data?.currentValue ||
      changes.actionData?.currentValue
    ) {
      const loading = changes.loading?.currentValue ?? this.loading;
      const model = changes.model?.currentValue ?? this.model;
      const data = changes.data?.currentValue ?? this.data;

      let config: Highcharts.Options = {
        title: { text: '', useHTML: true },
        series: [],
        credits: {
          enabled: false,
        },
      };

      this.updateChart = true;

      if (this.multiselected) {
        config.title.text = 'Please select one Alert';
      } else if (loading) {
        config.title.text = '';
      } else if (!model) {
        config.title.text = '';
      } else if (!data) {
        this.updateChart = true;
        config = turnEmptyModelTrendToHighcharts(
          getTitleFromModel(model),
          this.exportSettings
        );
        this.setHighchart(config);
      } else {
        this.updateChart = true;
        config = turnModelTrendToHighcharts(
          getTitleFromModel(model),
          data,
          this.exportSettings,
          this.actionData
        );
        this.setHighchart(config);
      }
    }
  }

  setHighchart(config: Options) {
    this.chartConfiguration = config;
    config.chart.events = {
      render: this.setOnRender(),
    };
  }

  public setOnRender() {
    return () => {
      setTimeout(() => {
        if (this.moveMarkers) {
          this.onMoveMarkers();
        }

        const yAxis: any = Array.from(
          document.querySelectorAll(
            `.chart-${this.model.ModelID} .highcharts-yaxis`
          )
        );
        yAxis.map((y, index) => {
          const rect = createInitialRectBounds(y);
          // This will add click event to the clickable area
          rect.addEventListener('click', (event) => {
            removeActiveYAxis();
            rect.setAttribute('class', 'activeYAxis rectBounds');
            rect.setAttribute(
              'style',
              'fill-opacity: 0; stroke-width: 1px; stroke: #6ea09d;'
            );

            const chartBackground: any = document.querySelector(
              `.chart-${this.model.ModelID}`
            );
            const chartBounding = chartBackground?.getBoundingClientRect();

            const dialogRef = this.dialog.open(UpdateLimitsDialogComponent, {
              width: '400px',
              position: {
                left: `${chartBounding?.x + 10}px`,
                top: `${chartBounding?.y + chartBounding?.height - 90}px`,
              },
              panelClass: 'custom-dialog-container',
              data: {
                AxisIndex: 0,
                Min: this.data.Min,
                Max: this.data.Max,
                OriginalMin: this.data.OriginalMin,
                OriginalMax: this.data.OriginalMax,
              },
            });

            dialogRef.componentInstance.updateValues.subscribe(
              (data: IUpdateLimitsData) => {
                this.updateLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.componentInstance.resetValues.subscribe(
              (data: number) => {
                this.resetLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.afterClosed().subscribe((result) => {
              removeActiveYAxis();
            });
          });

          y.parentElement.appendChild(rect);
        });

        this.renderTitle();
      }, 100);
    };
  }

  private onMoveMarkers() {
    //A Way to move the highchart-markers above the plot
    //trend/grid lines with direct DOM manipulation.
    //Highcharts does not support any way to do this.

    //select markers
    const markers = Array.from(
      document.querySelectorAll('.highcharts-markers')
    );

    //select chart
    const grid = document.querySelector('.highcharts-series-group');

    //We want to insert the elements in reverse order so that the last elements
    //in the original array are also last in the target html node.
    //This will conserve the icon overlap priority
    markers.reverse();

    //for every marker insert after chart
    markers.forEach((marker) => {
      grid.parentNode.insertBefore(marker, grid.nextSibling);
    });
  }

  public renderTitle() {
    this.titleSelected = false;
    const chartRef: any = document.querySelector(
      `.chart-${this.model.ModelID}`
    );

    if (chartRef) {
      if (chartRef && chartRef.querySelector('.highcharts-title')) {
        const titleElement = chartRef.querySelector('.highcharts-title');
        titleElement.addEventListener('click', (event) => {
          let selection = window.getSelection();
          selection.removeAllRanges();

          if (!this.titleSelected) {
            const range = document.createRange();
            range.selectNodeContents(titleElement);
            selection.addRange(range);
            const text = titleElement?.textContent;
            if (text) {
              copyToClipboard(text);
            }
            this.titleSelected = true;
          } else {
            selection = window.getSelection();
            selection.removeAllRanges();
            this.titleSelected = false;
          }
        });
        chartRef.addEventListener('click', (event) => {
          const svgAnimatedString = event.target.className;
          if (
            svgAnimatedString.baseVal === '' ||
            svgAnimatedString.baseVal === titleElement.className
          ) {
            return;
          }

          const selection = window.getSelection();
          selection.removeAllRanges();
          this.titleSelected = false;
        });
      }
    }
  }

  public updateLimits(event: IUpdateLimitsData) {
    this.btnGrpStateChange.emit(updateLimits(event));
  }

  public resetLimits(event: number) {
    this.btnGrpStateChange.emit(resetLimits(event));
  }
}
