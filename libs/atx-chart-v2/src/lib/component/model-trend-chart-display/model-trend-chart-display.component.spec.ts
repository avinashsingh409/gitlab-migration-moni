import { HighchartsChartModule } from 'highcharts-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { ModelTrendChartDisplayComponent } from './model-trend-chart-display.component';
import { AuthService, JwtInterceptorService } from '@atonix/shared/state/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('ModelTrendChartDisplayComponent', () => {
  let component: ModelTrendChartDisplayComponent;
  let fixture: ComponentFixture<ModelTrendChartDisplayComponent>;

  let mockAuthService: AuthService;
  let toastServiceMock: ToastService;

  beforeEach(() => {
    mockAuthService = createMockWithValues(AuthService, {
      GetIdToken: jest.fn(),
    });
    toastServiceMock = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: ToastService, useValue: toastServiceMock },
        { provide: APP_CONFIG, useValue: AppConfig },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
      ],
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModelTrendChartDisplayComponent],
      imports: [HighchartsChartModule, HttpClientTestingModule, MatTableModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelTrendChartDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
