import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  HostListener,
  ViewChild,
  ChangeDetectionStrategy,
} from '@angular/core';
import * as Highcharts from 'highcharts';
import { IComponentDonutState } from '../../models/donut-chart-state';
import { SCREEN_SIZE } from '../../models/screen-size.enum';
import { toolTipFormatter } from '../../service/utilities';
import { MatButton } from '@angular/material/button';
import { isNil } from 'lodash';

@Component({
  selector: 'atx-donut-chart',
  templateUrl: './donut-chart.component.html',
  styleUrls: ['./donut-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DonutChartComponent implements OnChanges {
  // data for charts
  highcharts: typeof Highcharts = Highcharts;
  chartConfiguration: Highcharts.Options;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)
  // callback required to get an instance of the chart object
  chartCallback: Highcharts.ChartCallbackFunction = function (chart) {
    this.chart = chart;
  }.bind(this);
  chart: Highcharts.Chart; // required to access the async data loading functions
  localDonutState: IComponentDonutState;
  canRefresh = false;

  @Input() donutTheme: string;
  @Input() donutChartState: IComponentDonutState;
  // These two output event emitters align with the
  // ngrx actions for this component, which are
  // selecting the "others" on the chart, and refreshing.
  @Output() othersSelected = new EventEmitter();
  @Output() refreshed = new EventEmitter();
  @ViewChild('button') button: MatButton;

  sizes = [
    {
      id: SCREEN_SIZE.XS,
      minSize: 0,
      maxSize: 480,
    },
    {
      id: SCREEN_SIZE.SM,
      minSize: 481,
      maxSize: 768,
    },
    {
      id: SCREEN_SIZE.MD,
      minSize: 769,
      maxSize: 991,
    },
    {
      id: SCREEN_SIZE.LG,
      minSize: 992,
      maxSize: 1291,
    },
    {
      id: SCREEN_SIZE.XL,
      minSize: 1292,
      maxSize: 1920,
    },
    {
      id: SCREEN_SIZE.XXL,
      minSize: 1920,
      maxSize: 10000,
    },
  ];

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.donutChartState &&
      changes.donutChartState.currentValue &&
      changes.donutChartState.currentValue.currentDonutData
    ) {
      this.localDonutState = this.removeBinding(
        changes.donutChartState.currentValue
      );
      this.canRefresh = this.localDonutState.canRefresh;
      this.loadChart(this.localDonutState);
      this.updateTextColor();
    }

    if (
      changes.donutTheme &&
      changes.donutTheme.currentValue &&
      this.chartConfiguration
    ) {
      this.updateTextColor();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(_unusedEvent: any) {
    const currentSize = this.sizes.find(
      (x) => window.innerWidth >= x.minSize && window.innerWidth <= x.maxSize
    );
    const currentState = this.donutChartState;
    if (currentSize !== undefined || currentSize !== null) {
      if (currentSize.id !== undefined || currentSize.id !== null) {
        switch (currentSize.id) {
          case SCREEN_SIZE.XS:
            this.loadChart({
              ...currentState,
              legendWidthLimit: 40,
            });
            break;
          case SCREEN_SIZE.SM:
            this.loadChart({
              ...currentState,
              legendWidthLimit: 60,
            });
            break;
          case SCREEN_SIZE.MD:
            this.loadChart({
              ...currentState,
              legendWidthLimit: 70,
            });
            break;
          case SCREEN_SIZE.LG:
            this.loadChart({
              ...currentState,
              legendWidthLimit: 90,
            });
            break;
          case SCREEN_SIZE.XL:
          case SCREEN_SIZE.XXL:
            this.loadChart({
              ...currentState,
              legendWidthLimit: 100,
            });
        }
      }
    }
  }
  loadChart(donutState: IComponentDonutState) {
    this.chartConfiguration = this.initializeChartOpt(donutState);
    this.updateTextColor();
  }

  initializeChartOpt(state: IComponentDonutState) {
    let chartOpt: Highcharts.Options = null;

    chartOpt = {
      colors: [
        '#6ea09d',
        '#79c250',
        '#15afea',
        '#bde4e4',
        '#e16b55',
        '#e2bd68',
        '#437638',
        '#ba3f26',
        '#15afea',
        '#bde4e4',
      ],
      chart: {
        plotBorderWidth: 0,
        plotShadow: false,
        width: 500,
        height: 250,
        backgroundColor: null,
      },

      credits: {
        enabled: false,
      },
      title: {
        text: state.title,
        align: 'left',
        style: {
          'font-family': 'roboto',
          color: '#ffffff',
        },
        floating: true,
      },
      subtitle: {
        text: state.subtitle,
        align: 'center',
        verticalAlign: 'middle',
        style: {
          'font-family': 'roboto',
          color: '#ffffff',
          'font-size': '24px',
        },
        x: -152,
        y: 40,
      },
      tooltip: {
        pointFormat:
          '<span style="color:{point.color}">\u25AC</span><b>{point.y}</b><br/>',
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          shadow: false,
          borderWidth: 0,
          center: [70, '60%'],
          size: '80%',
          innerSize: '90%',
          dataLabels: {
            enabled: false,
          },
          point: {
            events: {
              legendItemClick: (event) => {
                if (event.target.name === 'Others') {
                  this.othersSelected.emit();
                  this.resetDonutSliceToggle();
                  event.preventDefault();
                } else {
                  // emit data to parent component
                }
              },
            },
          },
        },
        series: {
          tooltip: {
            pointFormatter: () => {
              this.chart = this.highcharts.charts.filter(
                (x) =>
                  x && x.userOptions.title.text === this.donutChartState.title
              )[0];
              const color = this.chart.hoverPoint.color;
              const formattedpoint = toolTipFormatter(
                this.chart.hoverPoint.y,
                state.toolTipFormat
              );
              return (
                '<span style="color:' +
                color +
                '">\u25CF</span><b>' +
                formattedpoint +
                '</b><br/>'
              );
            },
          },
        },
      },
      // Exporting is for the context menu in the top right corner of the charts.
      // we don't want that in the donut charts.
      exporting: { enabled: false },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'middle',
        itemStyle: {
          color: '#ffffff',
          'font-family': 'roboto',
          fontSize: '12px',
          width: state.legendWidthLimit,
          textOverflow: 'ellipsis',
        },
        itemHoverStyle: {
          color: '#A9A9A9',
        },
        width: 0,
        x: 185,
        y: 10,
        floating: true,
      },
      series: [
        {
          id: 'donut-series',
          type: 'pie',
          data: state.currentDonutData,
          showInLegend: true,
        },
      ],
    };

    return chartOpt;
  }

  refresh() {
    this.refreshed.emit();
    if (!this.canRefresh) {
      setTimeout(() => {
        if (this.button !== null || this.button !== undefined) {
          if (
            this.button._elementRef !== null ||
            this.button._elementRef !== undefined
          ) {
            this.button._elementRef.nativeElement.blur();
          }
        }
      }, 1000);
    }
    this.resetDonutSliceToggle();
  }

  // added to remove angular binding of IPreDonutChartState.currentDonutData and IPreDonutChartState.loadedDonutData
  // even if there is no code that binds this 2 properties together
  removeBinding(data: any[]) {
    return JSON.parse(JSON.stringify(data));
  }

  updateTextColor() {
    const newState: Highcharts.Options = { ...this.chartConfiguration };
    let textColor = '#ffffff';
    let fontWeight = 'bold';

    if (this.donutTheme === 'light') {
      textColor = '#000000';
      fontWeight = 'light';
    }

    newState.title.style.color =
      newState.subtitle.style.color =
      newState.legend.itemStyle.color =
        textColor;
    newState.title.style.fontWeight =
      newState.subtitle.style.fontWeight =
      newState.legend.itemStyle.fontWeight =
        fontWeight;
    this.chartConfiguration = newState;
  }

  resetDonutSliceToggle() {
    const charts = this.highcharts.charts.filter((x) => !isNil(x));
    this.chart = charts.filter(
      (x) => x.userOptions.title.text === this.donutChartState.title
    )[0];
    for (const d of this.chart.series[0].data) {
      if (d.selected) {
        d.select(false);
      }
    }

    this.updateChart = true;
  }
}
