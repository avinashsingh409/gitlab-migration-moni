import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { ITableData } from '../../models/bar-chart-data';

import {
  createHighchartsDefinition,
  isGroupedSeriesChart,
  isBarOrColumnChart,
  isStandardTable,
  isImage,
  isGroupedSeriesTable,
} from '../../service/chart.service';

import { HighchartsService } from '../../service/highcharts.service';
import { IBtnGrpStateChange } from '../../models/button-group-state';
import {
  changeLabels,
  editChart,
  removeActiveYAxis,
  updateLimits,
  removeRectBounds,
  createInitialRectBounds,
  getDisplayedAxes,
  resetLimits,
  legendItemToggle,
  dataCursorToggle,
} from '../../service/utilities';
import { ExportingOptions, Options } from 'highcharts';
import { MatDialog } from '@angular/material/dialog';
import { UpdateLimitsDialogComponent } from '../update-limits-dialog/update-limits-dialog.component';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import { ChartFacade } from '../../facade/chart.facade';
import { Observable } from 'rxjs';
import { ChartState } from '../../facade/chart.query';
import { take } from 'rxjs/operators';
import { createSummaryChart } from '../../service/summary-chart-utilities';
import { ImagesFrameworkService } from '@atonix/shared/api';
import { IProcessedTrend } from '@atonix/atx-core';
import { DarkUnicaTheme } from '@atonix/shared/utils';
import { LoggerService } from '@atonix/shared/utils';
import { LabelIndex, LabelIndexMapping } from '../../models/label-index';
import Highcharts from 'highcharts';
@Component({
  selector: 'atx-chart-display',
  templateUrl: './chart-display.component.html',
  styleUrls: ['./chart-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartDisplayComponent implements OnChanges {
  @ViewChild('chart') componentRef;
  @Input() theme: string;
  @Input() trend: Readonly<IProcessedTrend>;
  @Input() showToggle: boolean;
  @Input() showFilter: boolean;
  @Input() showNewChart: boolean;
  @Input() showEditChart: boolean;
  @Input() showCopyLink: boolean;
  @Input() showDownload: boolean;
  @Input() allowHiddenLabel: boolean;
  @Input() loading: boolean;
  @Input() isToggled: boolean;
  @Input() hideTitle: boolean;
  @Input() showDataCursorToggle: boolean;
  @Output() btnGrpStateChange = new EventEmitter<IBtnGrpStateChange>();
  public vm$: Observable<ChartState>;
  trendDisplayType: '' | 'chart' | 'table' | 'image' | 'multi';
  // data for charts
  highcharts: any;
  chartConfiguration: Highcharts.Options;
  chartRef: Highcharts.Chart;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)
  dialogRefId = '';
  reDraw = true;
  // data for image
  imagePath: string;
  labelDisplays = Object.keys(LabelIndex)
    .filter((key) => !isNaN(Number(LabelIndex[key])))
    .map((itm) => LabelIndex[itm]);
  labelDisplayNames = LabelIndexMapping;

  // data for table
  tableData: ITableData;
  // tableSummaryTypes: ISummaryType[]; // TODO: Load this using ngrx global store

  // data for multi-charts (i.e, multiple area trends)
  multiChartData: Highcharts.Options[];
  exportSettings: ExportingOptions;

  chartCallback: Highcharts.ChartCallbackFunction = (chart) => {
    this.chartRef = chart;
  };

  constructor(
    imagesFrameworkService: ImagesFrameworkService,
    private highchartsService: HighchartsService,
    private chartFacade: ChartFacade,
    private dialog: MatDialog,
    private logger: LoggerService
  ) {
    this.vm$ = this.chartFacade.query.vm$;
    // Boost uses WebGL - much faster but line sizes change
    // const h = highchartsService.highcharts();
    // Boost(h);
    // this.highcharts = h;
    this.highcharts = this.highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.loading === true) {
      this.destroyDataCursors();
    } else if (
      changes.trend?.currentValue &&
      this.loading === false &&
      this.reDraw
    ) {
      const myTrend = changes.trend.currentValue as Readonly<IProcessedTrend>;
      this.imagePath = null;
      this.chartConfiguration = null;
      this.updateChart = true;
      this.tableData = null;
      this.trendDisplayType = '';
      if (isImage(myTrend)) {
        this.trendDisplayType = 'image';
        this.imagePath = myTrend.trendDefinition.Path;

        if (
          myTrend.trendDefinition.Math?.substr(0, 8).toLowerCase() ===
          'refresh='
        ) {
          this.imagePath += '?cacheBust=1';
          // There is supposed to be some code in here to upate the image on a certain interval.  I am not going to put it in now,
          // since this feature is one that is not ever used.  For future reference the
          // code is in BV.PowerPlantMD.Web.Sites\Common\Charts\hchart.ts
          // lines 115-138 in the configure callback.
        }
      } else if (isGroupedSeriesTable(myTrend) || isStandardTable(myTrend)) {
        this.trendDisplayType = 'table';
      } else if (isGroupedSeriesChart(myTrend)) {
        // The bar-chart-display component will take care of this.
        this.trendDisplayType = 'multi';
      } else if (isBarOrColumnChart(myTrend)) {
        this.vm$.pipe(take(1)).subscribe((vm) => {
          this.trendDisplayType = 'chart';
          const config = createSummaryChart(
            myTrend,
            vm.isGroupedSeries,
            this.theme,
            vm.labelIndex.valueOf(),
            this.exportSettings
          );
          this.setHighchart(config);
        });
      } else {
        this.vm$.pipe(take(1)).subscribe((vm) => {
          this.trendDisplayType = 'chart';

          const config = createHighchartsDefinition(
            myTrend.trendDefinition,
            vm.isGroupedSeries,
            this.theme,
            myTrend.measurements,
            myTrend.startDate,
            myTrend.endDate,
            vm.labelIndex.valueOf(),
            this.isToggled,
            this.allowHiddenLabel,
            this.exportSettings,
            myTrend.showDataCursor,
            true
          );
          this.setHighchart(config);
        });
      }
    } else {
      this.reDraw = true;
    }
  }

  setHighchart(config: Options) {
    if (this.hideTitle && config) {
      config.title = undefined;
    }
    if (this.theme === 'dark') {
      config = this.highcharts.merge(DarkUnicaTheme, config);
    }

    config.chart.events = {
      render: this.setOnRender(),
      beforePrint: this.printChart(),

      //Selection event fired when we select a section to zoom into
      //we can look at the event and do some side effect
      //returns true to continue default zoom behavior
      // selection: (event) => {
      //   if (event.xAxis)
      //     console.log(
      //       Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].min),
      //       Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', event.xAxis[0].max)
      //     );
      //   return true;
      // },
    };
    config.plotOptions.series.events.legendItemClick = (event) => {
      // This will stop the chart to redraw multiple times
      this.reDraw = false;
      this.btnGrpStateChange.emit(
        legendItemToggle(event.target.userOptions.legendIndex)
      );
    };

    this.chartConfiguration = config;
    this.updateChart = true;
  }

  public printChart() {
    return () => {
      this.logger.trackTaskCenterEvent('DataExplorerV2:PrintChart');
    };
  }

  public setOnRender() {
    return () => {
      setTimeout(() => {
        if (this.dialogRefId !== '') {
          this.dialog.getDialogById(this.dialogRefId)?.close();
        }
        // This will remove all bounds before creating new bounds
        removeRectBounds();

        const yAxis: any = Array.from(
          document.querySelectorAll('.highcharts-yaxis')
        );
        yAxis.map((y, index) => {
          const rect = createInitialRectBounds(y);
          // This will add click event to the clickable area
          rect.addEventListener('click', (event) => {
            removeActiveYAxis();
            rect.setAttribute('class', 'activeYAxis rectBounds');
            rect.setAttribute(
              'style',
              'fill-opacity: 0; stroke-width: 1px; stroke: #6ea09d;'
            );

            const chartBackground: any = document.querySelector(
              '.chartComponentContent'
            );
            const chartBounding = chartBackground.getBoundingClientRect();
            const displayedAxes = getDisplayedAxes(this.trend.trendDefinition);

            const dialogRef = this.dialog.open(UpdateLimitsDialogComponent, {
              width: '400px',
              position: {
                left: `${chartBounding.x + 10}px`,
                top: `${chartBounding.y + chartBounding.height - 90}px`,
              },
              panelClass: 'custom-dialog-container',
              data: {
                AxisIndex: index,
                Min: displayedAxes[index].Min,
                Max: displayedAxes[index].Max,
                OriginalMin: displayedAxes[index]?.OriginalMin,
                OriginalMax: displayedAxes[index]?.OriginalMax,
              },
            });
            this.dialogRefId = dialogRef.id;

            dialogRef.componentInstance.updateValues.subscribe(
              (data: IUpdateLimitsData) => {
                this.updateLimits(data);
              }
            );

            dialogRef.componentInstance.resetValues.subscribe(
              (data: number) => {
                this.resetLimits(data);
              }
            );

            dialogRef.afterClosed().subscribe((result) => {
              removeActiveYAxis();
            });
          });

          y.parentElement.appendChild(rect);
        });
      }, 100);
    };
  }

  destroyDataCursors() {
    if (this.chartRef) {
      // This will unselect points
      const selectedPoints = this.chartRef.getSelectedPoints();
      if (selectedPoints?.length > 0) {
        selectedPoints[0].select(false, false);
      }

      // This will delete all the custom tooltips and plotlines
      this.componentRef.el.nativeElement
        .querySelectorAll('.firstSelectedToolTip')
        ?.forEach((el) => el.remove());
      this.componentRef.el.nativeElement
        .querySelectorAll('.secondSelectedToolTip')
        ?.forEach((el) => el.remove());

      this.chartRef.xAxis[0].removePlotLine('firstPlotLine');
      this.chartRef.xAxis[0].removePlotLine('secondPlotLine');
    }
  }

  // This is use for the Download feature of typed table
  highchartsOptionsUpdated(highchartsOptions: Highcharts.Options) {
    this.chartConfiguration = { ...highchartsOptions };
  }

  public dataCursorToggle() {
    this.btnGrpStateChange.emit(dataCursorToggle());
  }

  public changeLabels(index: number) {
    this.btnGrpStateChange.emit(changeLabels(index));
  }

  public editChart() {
    this.btnGrpStateChange.emit(editChart());
  }

  public dismissError() {
    this.chartFacade.dismissError();
  }

  public updateLimits(event: IUpdateLimitsData) {
    this.btnGrpStateChange.emit(updateLimits(event));
  }

  public resetLimits(event: number) {
    this.btnGrpStateChange.emit(resetLimits(event));
  }
}
