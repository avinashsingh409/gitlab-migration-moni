import {
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { HighchartsService } from '../../service/highcharts.service';
import { ExportingOptions } from 'highcharts';
import {
  ImagesFrameworkService,
  IModelConfigModelTrend,
  INDModelSummary,
} from '@atonix/shared/api';
import { getExpectedVsActualService } from '../../service/expected-vs-actual.service';
import {
  getChartConfigForEmptyResult,
  getChartConfigForResultWithData,
} from '../../service/model-trend.service';
import Highcharts from 'highcharts';

@Component({
  selector: 'atx-model-trend-chart',
  templateUrl: './model-trend-chart.component.html',
  styleUrls: ['./model-trend-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModelTrendChartComponent implements OnChanges {
  @Input() modelTrend!: IModelConfigModelTrend;
  @Input() isExpectedActual!: boolean;
  @Input() loading!: boolean;
  @Input() theme!: string;

  // data for charts
  highcharts: any;
  chartConfiguration: Highcharts.Options;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)

  exportSettings: ExportingOptions;

  constructor(
    private highchartsService: HighchartsService,
    imagesFrameworkService: ImagesFrameworkService
  ) {
    this.highcharts = highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.loading?.currentValue || changes.modelTrend?.currentValue) {
      const modelTrend = changes.modelTrend?.currentValue ?? this.modelTrend;
      const isExpectedActual =
        changes.isExpectedActual?.currentValue ?? this.isExpectedActual;
      const theme = changes.theme?.currentValue ?? this.theme;

      this.chartConfiguration = null;
      this.updateChart = true;

      if (
        modelTrend?.actualValues &&
        modelTrend.actualValues?.length > 0 &&
        modelTrend?.expectedValues &&
        modelTrend.expectedValues?.length > 0
      ) {
        this.chartConfiguration = isExpectedActual
          ? getExpectedVsActualService(modelTrend)
          : getChartConfigForResultWithData(modelTrend, theme);
      } else {
        this.chartConfiguration = isExpectedActual
          ? getExpectedVsActualService(modelTrend)
          : getChartConfigForEmptyResult(
              theme,
              modelTrend?.startDate,
              modelTrend?.endDate
            );
      }

      this.chartConfiguration.exporting = this.exportSettings;

      this.updateChart = true;
    }
  }
}
