import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { IProcessedTrend } from '@atonix/atx-core';

@Component({
  selector: 'atx-chart-selector',
  templateUrl: './chart-selector.component.html',
  styleUrls: ['./chart-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
// export class ChartSelectorComponent {
export class ChartSelectorComponent implements OnChanges {
  @Input() trends: IProcessedTrend[];
  @Input() selected: string;

  @Output() stateChange = new EventEmitter<string>();

  selection = new UntypedFormControl(null);

  ngOnChanges(changes: SimpleChanges) {
    if (changes?.trends) {
      this.selection.setValue(this.selected);
    }
  }

  changed(trendId: string) {
    this.stateChange.emit(trendId);
  }
}
