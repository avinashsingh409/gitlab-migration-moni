import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Inject,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoggerService } from '@atonix/shared/utils';

@Component({
  selector: 'atx-update-limits-dialog',
  templateUrl: './update-limits-dialog.component.html',
  styleUrls: ['./update-limits-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateLimitsDialogComponent {
  public updateValues = new EventEmitter<IUpdateLimitsData>();
  public resetValues = new EventEmitter<number>();

  lowLimit = new UntypedFormControl(0);
  highLimit = new UntypedFormControl(0);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IUpdateLimitsData,
    private logger: LoggerService
  ) {
    this.lowLimit.setValue(data.Min);
    this.highLimit.setValue(data.Max);
  }

  apply() {
    this.logger.trackTaskCenterEvent('DataExplorerV2:ChartAxisCtrl:ApplyBtn');
    this.updateValues.emit({
      AxisIndex: this.data.AxisIndex,
      Min: this.lowLimit.value,
      Max: this.highLimit.value,
    });
  }

  reset() {
    this.resetValues.emit(this.data.AxisIndex);
  }
}
