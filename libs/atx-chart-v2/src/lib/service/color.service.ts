import { isNil } from '@atonix/atx-core';
import * as Highcharts from 'highcharts';
// chroma.js palette helper: https://vis4.net/palettes/#/9|s|6a3d9a|ffffe0,ff005e,93003a|0|0
// root from ColorBrewer: https://colorbrewer2.org/#type=qualitative&scheme=Paired&n=12
export const colorBrewerDivergent = [
  ['#e31a1c', '#ff4435', '#ff654e', '#ff8468'], // red
  ['#1f78b4', '#4793d1', '#68afef', '#88cbff'], // blue
  ['#004e00', '#006900', '#00840a', '#33a02c'], // green
  ['#ff7f00', '#bb4800', '#dd6300', '#ff9b2d'], // flush orange
  ['#766181', '#ad96b9', '#cab2d6', '#917b9d'], // amethyst
  ['#18004a', '#6a3d9a', '#8657b7', '#a371d4'], // dark blue
  ['#a06d1f', '#be873a', '#dda354', '#fdbf6f'], // desert
  ['#923f0e', '#b15928', '#d17441', '#f18f5b'], // korma orange
  ['#a6cee3', '#537a8d', '#6e95a9', '#8ab1c6'], // regis blue
  ['#b2df8a', '#5e893a', '#79a554', '#95c26f'], // feijoa green
];

export class ColorService {
  // Internal method to choose colors in sequence.  If the color is set in the configuration this is not used.
  private nextColorIndex = 0;
  private nextSelectedColorIndex = 0;
  public colors = [
    ['#ff0000', '#9d1d20', '#e60d7a', '#ff530d', '#b20252'],
    ['#0c5ae8', '#00cfff', '#3f3fbc', '#0da2ff', '#0600ff'],
    ['#26b226', '#56ff0d', '#057525', '#71d8a2', '#11efcf'],
    ['#8d0ed8', '#d300ff', '#794abc', '#bb6cd8', '#902c9b'],
    ['#ffa70d', '#bf9136', '#ffcd00', '#ff8212', '#ccbc2b'],
  ];

  private seriesColor = [
    '#6ea09d',
    '#79c250',
    '#15afea',
    '#bde4e4',
    '#e16b55',
    '#e2bd68',
    '#4C6D3D',
    '#ba3f26',
    '#425252',
    '#84ABD8',
    '#A848AC',
    '#CFA81D',
    '#E2145F',
    '#9319D0',
    '#75D0A6',
  ];

  public colorServiceSymbols = [
    'circle',
    'square',
    'diamond',
    'triangle',
    'triangle-down',
  ];

  public getNextColor() {
    if (Highcharts && Highcharts.getOptions) {
      const options = Highcharts.getOptions();

      if (this.nextColorIndex >= this.seriesColor.length) {
        this.nextColorIndex = 0;
      }
      return this.seriesColor[this.nextColorIndex++];
    }
    return '#000000';
  }

  public selectedColor(idx?: number) {
    if (isNil(idx)) {
      idx = this.nextSelectedColorIndex;
      this.nextSelectedColorIndex++;
    }
    const selectedColors = [
      '#929497',
      '#404041',
      '#d0d2d3',
      '#6d6e70',
      '#bbbdbf',
    ];
    return selectedColors[Math.abs(idx) % selectedColors.length];
  }

  public checkAndGetNextColor(existingColors: string[], seriesIndex: number) {
    if (Highcharts && Highcharts.getOptions) {
      const options = Highcharts.getOptions();
      while (
        this.nextColorIndex < this.seriesColor.length &&
        existingColors.indexOf(this.seriesColor[this.nextColorIndex]) !== -1
      ) {
        this.nextColorIndex++;
      }

      if (this.nextColorIndex >= this.seriesColor.length) {
        this.nextColorIndex = 0;
      }

      if (existingColors.length >= this.seriesColor.length) {
        existingColors.splice(0, existingColors.length);
      }

      existingColors.push(this.seriesColor[this.nextColorIndex]);

      return this.seriesColor[this.nextColorIndex];
    }
    return '#000000';
  }

  public getColor(existingColors: string[]) {
    if (existingColors.length > 0) {
      let colors = [...this.seriesColor];
      colors = colors.filter(
        (x) =>
          !existingColors.map((e) => e.toLowerCase()).includes(x.toLowerCase())
      );

      if (colors.length > 0) {
        const color = colors[this.nextColorIndex];
        this.nextColorIndex += 1;
        return color;
      } else {
        colors = [...this.seriesColor];

        if (existingColors.length === this.seriesColor.length) {
          const color = this.seriesColor[this.nextColorIndex];
          this.nextColorIndex += 1;
          return color;
        } else {
          const chunkedArr = [];
          let idx = 0;
          while (existingColors.length) {
            chunkedArr.push(existingColors.splice(0, this.seriesColor.length));
            idx += 1;
          }

          if (this.nextColorIndex > 0) {
            const color = this.seriesColor[this.nextColorIndex];
            this.nextColorIndex += 1;
            return color;
          }

          const lastChunk = chunkedArr[idx - 1] as [];
          const existingColorLastIdx = this.seriesColor.indexOf(
            lastChunk[lastChunk.length - 1]
          );

          this.nextColorIndex = existingColorLastIdx + 1;
          if (this.nextColorIndex >= this.seriesColor.length) {
            this.nextColorIndex = 0;
          }

          const color = this.seriesColor[this.nextColorIndex];
          this.nextColorIndex += 1;
          return color;
        }
      }
    } else {
      if (this.nextColorIndex > this.seriesColor.length - 1) {
        this.nextColorIndex = 0;
      }

      const color = this.seriesColor[this.nextColorIndex];
      this.nextColorIndex += 1;
      return color;
    }
  }
}
