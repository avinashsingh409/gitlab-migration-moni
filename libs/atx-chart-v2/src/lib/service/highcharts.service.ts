import { Injectable } from '@angular/core';
import * as Highcharts from 'highcharts';
import more from 'highcharts/highcharts-more';
import BrokenAxis from 'highcharts/modules/broken-axis';
import Exporting from 'highcharts/modules/exporting';

@Injectable({
  providedIn: 'root',
})
export class HighchartsService {
  constructor() {
    more(Highcharts);
    BrokenAxis(Highcharts);
    // This exporting is for the context menu at the top-right corner of the charts.
    Exporting(Highcharts);
  }

  highcharts() {
    return Highcharts;
  }
}
