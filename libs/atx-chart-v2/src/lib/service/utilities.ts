import { XAxisOptions, YAxisOptions } from 'highcharts';
import {
  getDate,
  GroupedSeriesType,
  IAssetMeasurements,
  IAssetMeasurementsSet,
  IPDTrend,
  IPDTrendAxis,
  IPDTrendBandAxisMeasurement,
  IProcessedTrend,
  sigFig,
} from '@atonix/atx-core';
import { IDataFormatOption } from '../models/data-format-option';
import { IBtnGrpStateChange } from '../models/button-group-state';
import { BehaviorSubject } from 'rxjs';
import { IUpdateLimitsData } from '../models/update-limits-data';
import cloneDeep from 'lodash/cloneDeep';
import { findIndex } from 'lodash';
import { ColorService } from './color.service';

export function toPrecisionCustom(num: number, precision: number): string {
  if (precision <= 0) {
    return '';
  }

  if (num == 0) {
    return '0';
  }

  let numString: string = num.toPrecision(precision);
  let index: number = numString.length - 1;
  let char: string = numString.charAt(index);

  while (char === '0' || char === '.') {
    numString = numString.substring(0, index); //removes the last character in the string
    index = numString.length - 1;
    char = numString.charAt(index); //gets the new last character in the string
  }

  return numString;
}

export function getNumberBySigFigs(input: number, digits: number): number {
  return Number(sigFig(input, digits));
}

export function hexToRGB(hex: string, alpha: number) {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const b = parseInt(hex.slice(5, 7), 16);
  if (alpha) {
    return `rgba(${r}, ${g}, ${b}, ${alpha.toString()})`;
  } else {
    return `rgb(${r}, ${g}, ${b})`;
  }
}

export function toolTipFormatter(
  val: number,
  formatOpt: IDataFormatOption
): string {
  let formattedString = String('');

  if (formatOpt.isRounded) {
    formattedString = `${Math.round(val).toLocaleString(formatOpt.locale, {
      style: formatOpt.format,
      currency: formatOpt.currency,
      minimumFractionDigits: formatOpt.decimalPlaces,
      maximumFractionDigits: 0,
    })}`;
  } else {
    formattedString = `${val.toLocaleString(formatOpt.locale, {
      style: formatOpt.format,
      currency: formatOpt.currency,
      minimumFractionDigits: formatOpt.decimalPlaces,
      maximumFractionDigits: formatOpt.decimalPlaces,
    })} `;
  }
  return formattedString;
}

// put any properties that needs to be cleared from the chart axes here.
export const defaultXAxis: XAxisOptions = {
  min: null,
  max: null,
  endOnTick: null,
  maxPadding: null,
  gridLineWidth: null,
  categories: null,
  showEmpty: null,
  tickLength: null,
};

export const defaultYAxis: YAxisOptions = {
  min: null,
  max: null,
  endOnTick: null,
  maxPadding: null,
  gridLineWidth: null,
  categories: null,
  showEmpty: null,
  tickLength: null,
};

export function getEmptyTrend(title?: string) {
  const result: IProcessedTrend = {
    id: '',
    groupedSeriesType: GroupedSeriesType.NONE,
    groupedSeriesSubType: '',
    label: title ?? '',
    trendDefinition: {
      PDTrendID: -1,
      TrendDesc: title ?? '',
      Title: title ?? '',
      LegendVisible: true,
      LegendDock: 0,
      LegendContentLayout: 0,
      IsCustom: true,
      CreatedBy: '',
      IsPublic: true,
      DisplayOrder: 0,
      ChartType: null,
      Series: [],
      Axes: [],
      IsStandardTrend: false,
      Filter: null,
      Pins: [],
      PinTypeID: 0,
      ShowPins: true,
      ShowSelected: true,
      SummaryType: null,
      Categories: [],
      XAxisGridlines: false,
    },
    totalSeries: 0,
  };
  return result;
}

export function getLoadingDataTrend(defaultValues?: Partial<IProcessedTrend>) {
  const result = getEmptyTrend('Loading ...');
  return { ...result, ...defaultValues };
}

export function getNoDataTrend(defaultValues?: Partial<IProcessedTrend>) {
  const result = getEmptyTrend('No Data Available');
  return { ...result, ...defaultValues };
}

export function setDataOnTrend(
  trend: IProcessedTrend,
  measurements: IAssetMeasurementsSet
) {
  const result = { ...trend, measurements };
  return result;
}

export function changeLabels(newValue: number) {
  const result: IBtnGrpStateChange = {
    event: 'ChangeLabels',
    newValue,
  };
  return result;
}

export function dataCursorToggle() {
  const result: IBtnGrpStateChange = {
    event: 'DataCursorToggle',
  };
  return result;
}

export function editChart() {
  const result: IBtnGrpStateChange = {
    event: 'EditChart',
  };
  return result;
}

export function updateLimits(limitsData: IUpdateLimitsData) {
  const result: IBtnGrpStateChange = {
    event: 'UpdateLimits',
    newValue: limitsData,
  };
  return result;
}

export function resetLimits(axisIndex: number) {
  const result: IBtnGrpStateChange = {
    event: 'ResetLimits',
    newValue: axisIndex,
  };
  return result;
}

export function legendItemToggle(legendItemIndex: number) {
  const result: IBtnGrpStateChange = {
    event: 'LegendItemToggle',
    newValue: legendItemIndex,
  };
  return result;
}

export function copyToClipboard(link: string) {
  let result$: BehaviorSubject<boolean> = null;
  const selBox = document.createElement('textarea');

  selBox.value = link;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  const copy = document.execCommand('copy');
  if (copy) {
    result$ = new BehaviorSubject<boolean>(true);
  } else {
    result$ = new BehaviorSubject<boolean>(false);
  }
  document.body.removeChild(selBox);
  return result$;
}

export function createInitialRectBounds(yAxis) {
  // Standard code to create SVG element
  // https://developer.mozilla.org/en-US/docs/Web/API/Document/createElementNS
  const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
  // This will create the bounds of the clickable area
  if (yAxis) {
    const boundingRect = yAxis.getBBox();
    rect.setAttribute('width', boundingRect.width.toString());
    rect.setAttribute('height', (boundingRect.height + 20).toString());
    rect.setAttribute('x', boundingRect.x.toString());
    rect.setAttribute('y', (boundingRect.y - 10).toString());
    rect.setAttribute('fill', 'white');
    rect.setAttribute('style', 'fill-opacity: 0');
    rect.setAttribute('class', 'rectBounds');
  }

  return rect;
}

export function removeRectBounds() {
  const rectBounds = document.querySelectorAll('.rectBounds');
  rectBounds.forEach((rect) => {
    rect.parentElement.removeChild(rect);
  });
}

export function removeActiveYAxis() {
  const activeYAxis = document.querySelector('.activeYAxis');
  activeYAxis?.removeAttribute('class');
  activeYAxis?.setAttribute('class', 'rectBounds');
  activeYAxis?.setAttribute('style', 'fill-opacity: 0');
}

export function getDisplayedAxes(trendDefinition: IPDTrend) {
  const displayedAxes: IPDTrendAxis[] = [];
  // eslint-disable-next-line no-unsafe-optional-chaining
  const newAxes = [...trendDefinition?.Axes];

  newAxes.map((x) => {
    if (
      trendDefinition.Series.filter((s) => s.Axis === x.Axis && !s.IsXAxis)
        .length > 0
    ) {
      displayedAxes.push(x);
    }
  });
  if (displayedAxes.length === 0) {
    newAxes.map((x) => {
      displayedAxes.push(x);
    });
  }

  return displayedAxes;
}

export function updateAxes(
  trendDefinition: IPDTrend,
  limitsData: IUpdateLimitsData
) {
  const displayedAxes = getDisplayedAxes(trendDefinition);
  const newAxes = [...trendDefinition.Axes];

  const idx = newAxes.findIndex(
    (x) => x.Axis === displayedAxes[limitsData.AxisIndex].Axis
  );
  if (idx >= 0) {
    newAxes.splice(idx, 1, {
      ...newAxes[idx],
      Min: limitsData.Min,
      Max: limitsData.Max,
    });
  }

  return newAxes;
}

export function resetAxes(trendDefinition: IPDTrend, axisIndex: number) {
  const displayedAxes = getDisplayedAxes(trendDefinition);
  const newAxes = [...trendDefinition.Axes];

  const idx = newAxes.findIndex(
    (x) => x.Axis === displayedAxes[axisIndex].Axis
  );
  if (idx >= 0) {
    newAxes.splice(idx, 1, {
      ...newAxes[idx],
      Min: newAxes[idx].OriginalMin,
      Max: newAxes[idx].OriginalMax,
    });
  }

  return newAxes;
}

// Helper functions for creating the x axis in the event of a no-gaps pinning setting.
export function findMin(trendDef: IPDTrend) {
  let myMin: number = null;
  for (const pin of trendDef.Pins) {
    if (myMin === null || myMin > getDate(pin.StartTime).getTime()) {
      myMin = getDate(pin.StartTime).getTime();
    }
  }
  return myMin;
}

export function getRepeating(
  ticks: number,
  myData: any[],
  start: Date,
  stop: Date
) {
  // Get the start of the hour the start time is in.
  let beginningOfTimeStuff = 0;
  if (myData.length > 0) {
    beginningOfTimeStuff =
      myData[0].x.getTime() - (myData[0].x.getTime() % ticks);
  }

  // get the start hour of the target interval
  const beginningOfTargetInterval = start.getTime() - (start.getTime() % ticks);

  // Translate each data point to be relative to the start of the hour.
  // Get rid of all data that is beyond the indicated interval.
  const data = myData
    .map((d) => {
      return { ...d, x: d.x.getTime() - beginningOfTimeStuff };
    })
    .filter((d) => d.x < ticks); // Gets all the data within the indicated interval

  // Create an array to hold the new repeating data.
  let newData = [];

  let intervalIdx = 0;
  while (beginningOfTargetInterval + intervalIdx * ticks < stop.getTime()) {
    newData = [
      ...newData,
      ...data.map((d) => {
        return {
          ...d,
          x: getDate(d.x + beginningOfTargetInterval + intervalIdx * ticks),
        };
      }),
    ];
    intervalIdx++;
  }

  return newData;
}

export function getRepeatingMonth(myData, start, stop) {
  const targetIntervalYear = start.getFullYear();
  let targetIntervalMonth = start.getMonth();

  myData = cloneDeep(myData);

  const threshold = new Date(
    myData[0].x.getFullYear(),
    myData[0].x.getMonth() + 1,
    1
  );
  let indexOfFirstDataOverInterval = -1;
  for (let i = 0; i < myData.length; i++) {
    if (myData[i].x.getTime() > threshold.getTime()) {
      indexOfFirstDataOverInterval = i;
      break;
    }
  }
  if (indexOfFirstDataOverInterval >= 0) {
    myData = myData.slice(0, indexOfFirstDataOverInterval);
  }

  const newData = [];
  while (
    new Date(targetIntervalYear, targetIntervalMonth, 1, 0, 0, 0, 0).getTime() <
    stop.getTime()
  ) {
    const data = cloneDeep(myData);
    for (const d of data) {
      d.x.setFullYear(targetIntervalYear);
      d.x.setMonth(targetIntervalMonth);
      newData.push(d);
    }
    targetIntervalMonth++;
  }
  return newData;
}

export function getRepeatingYear(myData, start, stop) {
  let targetIntervalYear = start.getFullYear();

  myData = cloneDeep(myData);

  const threshold = new Date(myData[0].x.getFullYear() + 1, 0, 1);
  let indexOfFirstDataOverInterval = -1;
  for (let i = 0; i < myData.length; i++) {
    if (myData[i].x.getTime() > threshold.getTime()) {
      indexOfFirstDataOverInterval = i;
      break;
    }
  }
  if (indexOfFirstDataOverInterval >= 0) {
    myData = myData.slice(0, indexOfFirstDataOverInterval);
  }

  const newData = [];
  while (
    new Date(targetIntervalYear, 0, 1, 0, 0, 0, 0).getTime() < stop.getTime()
  ) {
    const data = cloneDeep(myData);
    for (const d of data) {
      d.x.setFullYear(targetIntervalYear);
      newData.push(d);
    }
    targetIntervalYear++;
  }
  return newData;
}

export function dragElement(elmnt, point) {
  let pos1 = 0;
  let pos2 = 0;
  let pos3 = 0;
  let pos4 = 0;
  if (document.getElementById(elmnt.id + 'header')) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + 'header').onmousedown = (e) => {
      dragMouseDown(e, point);
    };
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = (e) => {
      dragMouseDown(e, point);
    };
  }

  function dragMouseDown(e, point) {
    point.series.chart.container.style.pointerEvents = 'none';
    e.cancelBubble = true;
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = (e) => closeDragElement(e, point);
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    const top = elmnt.offsetTop - pos2;
    const left = elmnt.offsetLeft - pos1;
    if (top >= 0) {
      elmnt.style.top = top + 'px';
      elmnt.style.left = left + 'px';
    }
  }

  function closeDragElement(e, point) {
    point.series.chart.container.style.pointerEvents = 'auto';
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

export function getDefaultMeasurement(
  axis: number,
  tagID: number,
  color: string
) {
  const colors = new ColorService();
  const val: IPDTrendBandAxisMeasurement = {
    AggregationType1: 3,
    AggregationType2: null,
    AggregationType3: null,
    AggregationType4: null,
    AggregationType5: null,
    AttributeType1: null,
    AttributeType2: null,
    AttributeType3: null,
    AttributeType4: null,
    AttributeType5: null,
    Axis: axis,
    BarID: null,
    BorderThickness: null,
    Color: color,
    FillColor: color,
    Invert: false,
    MarkerConfigs: [],
    Name: 'New Measurement',
    Offset: 0,
    Params1: null,
    Params2: null,
    Params3: null,
    Params4: null,
    Params5: null,
    PDTrendID: null,
    Result1: null,
    Result2: null,
    Result3: null,
    Result4: null,
    Result5: null,
    Symbol: 'square',
    TagID1: tagID,
    TagID2: null,
    TagID3: null,
    TagID4: null,
    TagID5: null,
    TooltipHideMin: false,
    TooltipStyle: null,
    TrendBandAxisMeasurementID: null,
    Type: 'range',
    Value1: null,
    Value2: 0,
    Value3: null,
    Value4: null,
    Value5: null,
    ValueType1: null,
    ValueType2: null,
    ValueType3: null,
    ValueType4: null,
    ValueType5: null,
    VariableType1: null,
    VariableType2: null,
    VariableType3: null,
    VariableType4: null,
    VariableType5: null,
    IsFiltered: false,
    FilterMin: null,
    FilterMax: null,
    FlatlineThreshold: null,
    Exclude: null,
    ApplyToAll: false,
    UseGlobal: true,
    Flags: null,
    DisplayOrder: null,
  };

  return val;
}

export function getBandAxisMeasurementIndex(
  measurements: IPDTrendBandAxisMeasurement[],
  bandAxisMeasurement: IPDTrendBandAxisMeasurement
) {
  const idx = findIndex(
    measurements,
    (x) =>
      x.TagID1 === bandAxisMeasurement.TagID1 &&
      x.Color === bandAxisMeasurement.Color &&
      x.Type === bandAxisMeasurement.Type
  );

  return idx;
}
