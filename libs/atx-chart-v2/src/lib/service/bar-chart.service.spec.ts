import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BarChartService } from './bar-chart.service';
import { IRangeValueType } from '../models/bar-chart-data';
import { AuthService, JwtInterceptorService } from '@atonix/shared/state/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { provideMock } from '@testing-library/angular/jest-utils';

beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [
      provideMock(AuthService),
      {
        provide: HTTP_INTERCEPTORS,
        useClass: JwtInterceptorService,
        multi: true,
      },
    ],
  });
});

describe('processTrendDefinition', () => {
  it('creates bar chart service', () => {
    const val = BarChartService.createBarChartData({});
    expect(val).toBeTruthy();
  });

  it('gets a marker', () => {
    let result = BarChartService.getMarker('pump', 'red', '');
    expect(result).toBeTruthy();
    expect(result.symbol).toBeTruthy();
    result = BarChartService.getMarker('level', 'blue', '');
    expect(result).toBeTruthy();
    expect(result.symbol).toBeTruthy();
    result = BarChartService.getMarker('circle', 'green', '');
    expect(result).toBeTruthy();
    expect(result.symbol).toBeTruthy();
    expect(result.fillColor).toEqual('green');
  });

  it('creates an area', () => {
    const val = BarChartService.createArea({});
    expect(val).toBeTruthy();
  });

  it('creates a tooltip range', () => {
    const val1 = BarChartService.getTooltipRange({
      TooltipHideMin: true,
    } as IRangeValueType);
    expect(val1).toBeTruthy();
    const val2 = BarChartService.getTooltipRange({
      TooltipHideMin: false,
    } as IRangeValueType);
    expect(val2).toBeTruthy();
  });

  it('creates a Tooltip Box And Whiskers Outlier', () => {
    const val = BarChartService.getTooltipBoxAndWhiskersOutliers();
    expect(val).toBeTruthy();
  });

  it('creates an area', () => {
    const val: HTMLElement = { innerHTML: 'something' } as HTMLElement;
    BarChartService.destroy(val);
    expect(val.innerHTML).toBeFalsy();
  });
});
