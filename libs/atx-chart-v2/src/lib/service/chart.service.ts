/* eslint-disable no-unsafe-optional-chaining */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable max-len */
import * as Highcharts from 'highcharts';
import moment from 'moment';
import merge from 'lodash/merge';
import { colorBrewerDivergent, ColorService } from './color.service';
import { LoggerService } from '@atonix/shared/utils';
import every from 'lodash/every';
import {
  getDate,
  getTrendTotalSeries,
  IAssetMeasurements,
  IAssetMeasurementsSet,
  IPDRecordedPoint,
  IPDTrend,
  IPDTrendSeries,
  IProcessedTrend,
  isNil,
  setTimestampDictionary,
  sigFig,
} from '@atonix/atx-core';
import {
  defaultXAxis,
  defaultYAxis,
  dragElement,
  findMin,
  getNumberBySigFigs,
  getRepeating,
  getRepeatingMonth,
  getRepeatingYear,
  hexToRGB,
} from './utilities';
import {
  IAssetModelChartingData,
  INDModel,
  CorrelationTrendColorMapping,
  CorrelationTrendNameMapping,
  EForecastTrend,
  EROCTrend,
  ICorrelationData,
  ITimeseriesDatapoint,
  INDModelActionItemAnalysis,
  INDModelSummary,
} from '@atonix/shared/api';

export const AtxChartLine = 1;
export const AtxChartArea = 2;
export const AtxChartColumn = 3;
export const AtxChartBar = 4; // Or Trend in Group Series world...
export const AtxChartTrend = 4;
export const AtxChartTable = 22;
export const AtxChartRadar = 23;
export const AtxScatterPlot = 24;

export const AtxPinTypeUnmodified = 1;
export const AtxPinTypeNoGaps = 2;
export const AtxPinTypeOverlay = 3;

export const AtxAggregationAverage = 3;
export const AtxAggregationPercentile = 18;
export const AtxAggregationMinimum = 1;

let enableDataCursor = null;
let firstIndex = null;
let secondIndex = null;
let firstSelectedToolTip = null;
let secondSelectedToolTip = null;

export function isImage(trend: IProcessedTrend) {
  return (trend.trendDefinition.TrendSource || '').toLowerCase() === 'image';
}

export function isGroupedSeriesChart(trend: IProcessedTrend) {
  return (
    !isNil(trend?.trendDefinition?.BandAxis) &&
    trend?.trendDefinition?.ChartTypeID !== AtxChartTable
  );
}

export function isGroupedSeriesTable(trend: IProcessedTrend) {
  return (
    !isNil(trend?.trendDefinition?.BandAxis) &&
    trend?.trendDefinition?.ChartTypeID === AtxChartTable
  );
}

export function isStandardTable(trend: IProcessedTrend) {
  // 22 = table - from the base chart types; not the grouped series
  const result =
    trend?.trendDefinition?.ChartTypeID === AtxChartTable &&
    !isNil(trend.trendDefinition?.SummaryType) &&
    !isNil(trend.trendDefinition?.SummaryType.SummaryDesc);
  return result;
}

export function isBarOrColumnChart(trend: IProcessedTrend) {
  // column (3) /bar (4) - from the base chart types; not the grouped series
  const result =
    (trend.trendDefinition.ChartTypeID === AtxChartColumn ||
      trend.trendDefinition.ChartTypeID === AtxChartBar) &&
    !isNil(trend.trendDefinition.SummaryType) &&
    !isNil(trend.trendDefinition.SummaryType.SummaryDesc);
  return result;
}

export function getPinNumber(id: string) {
  const pin = id.split(':')[3];
  if (pin === '' || isNil(pin)) {
    return null;
  } else {
    return +pin;
  }
}

export function getTagNumber(id: string) {
  return id.split(':')[1];
}

export function getArchive(id: string) {
  return id.split(':')[5];
}

export function getName(id: string) {
  if (id) {
    if (id.lastIndexOf(':') >= 0) {
      // in case name includes :
      id = id.substr(id.lastIndexOf(':') + 1);
    }
    return id.replace(/</g, '_').replace(/>/g, '_');
  }
}

export function createId(tagNumber, pinNumber, archive) {
  if (isNil(pinNumber)) {
    return `tag:${tagNumber}:pin::archive:${archive}`;
  } else {
    return `tag:${tagNumber}:pin:${pinNumber}:archive:${archive}`;
  }
}

export function getActionTypeString(actionTypeId: number) {
  switch (actionTypeId) {
    case 1:
      return 'Diagnose';
    case 3:
    case 11:
    case 16:
      return 'Watch';
    case 6:
      return 'Maintenance';
    case 12:
      return 'Clear';
    default:
      return '';
  }
}

function OverlayTooltipFormatter(chartObject) {
  let tooltipWrap = `<table>`;
  let s = '';
  if (chartObject.points) {
    const sortedPoints = [...chartObject.points].sort((a, b) => {
      return (
        a.series?.userOptions?.legendIndex - b.series?.userOptions?.legendIndex
      );
    });

    for (const p of sortedPoints) {
      if (p.series.type !== 'arearange') {
        if (p.series.state === 'hover') {
          tooltipWrap = `<table class="tooltip" style="border: 1px solid ${p.color}; border-radius:3px;">`;
        }

        const d = moment(getDate(p.point.timestamp));
        s += `<tr>
          <td>
            <div style="height:5px;width:5px;background-color:${
              p.series.color
            };padding-right:5px;"></div>
          </td>
          <td>${p.series.name}</td>
          <td style="padding-right:5px;padding-left:5px;text-align:right;"><b>${getNumberBySigFigs(
            p.y,
            4
          )}</b></td>
          <td >${d.format('YYYY-MM-DD hh:mm:ss a')}</td>
        </tr>`;
      }
    }
  }
  return tooltipWrap + s + '</table>';
}

function defaultChartTooltipFormatter(chartObject, chartTypeID?: number) {
  const d = moment(getDate(chartObject.x));
  let tooltipWrap = '<div>';

  // Thursday, July 9th 2020  18:40:00
  let s = `<span style="font-size: 10px">
    ${d.format('dddd, MMMM Do YYYY h:mm:ss a')}
    </span><br/>`;

  let isHovered: boolean;

  if (chartObject.points) {
    const sortedPoints = [...chartObject.points].sort((a, b) => {
      return (
        a.series?.userOptions?.legendIndex - b.series?.userOptions?.legendIndex
      );
    });

    for (const p of sortedPoints) {
      let units: string;
      units = '';
      if (p?.series?.userOptions?.custom?.units) {
        units = p.series.userOptions.custom.units;
      }
      // determine hovered series by checking the opacity value
      if (p.series.type !== 'arearange') {
        if (p.series.state === 'hover') {
          isHovered = true;
        } else {
          isHovered = false;
        }

        if (isHovered) {
          tooltipWrap = `<div class="tooltip" style="border: 1px solid ${p.color}; border-radius:3px; padding: 3px;">`;
        }

        if (
          (p?.series?.userOptions?.custom?.isBold &&
            (chartTypeID === AtxChartLine || chartTypeID === AtxChartArea)) ||
          (isHovered &&
            (!chartTypeID ||
              chartTypeID === AtxChartColumn ||
              chartTypeID === AtxChartBar))
        ) {
          s += `<span style="color:${p.color}">●</span><b>${
            p.series.name
          }: ${getNumberBySigFigs(p.y, 4)} ${units}</b><br/>`;
        } else {
          s += `<span style="color:${p.color}">●</span>${
            p.series.name
          }: ${getNumberBySigFigs(p.y, 4)} ${units}<br/>`;
        }
      } else {
        s += `<span style="color:${p.color}">●</span>${p.series.name}: <b>
              ${getNumberBySigFigs(p.point.low, 4)}-
              ${getNumberBySigFigs(p.point.high, 4)}</b> ${units}<br/>`;
      }
    }
  } else if (chartObject.point) {
    if (chartObject.point.isAnnotation) {
      s = chartObject.point.text;
    }
    //Tooltip For Action Items
    if (chartObject.point.actionData) {
      const actionData = chartObject.point.actionData;
      const actionString = getActionTypeString(
        actionData.ModelActionItemTypeID
      );
      const diff = actionData.Actual - actionData.Expected;
      s =
        `<span style="font-size: 10px"><b>${actionString}</b></span><br/>` +
        s +
        `<span>Actual - Expected: ${sigFig(diff, 4)},
       Actual Value: ${sigFig(actionData.Actual, 4)}</span>`;
    }
  }
  return tooltipWrap + s + '</div>';
}

function varVsVarChartTooltipFormatter(chartObject) {
  let n = '';
  if (chartObject.series && chartObject.series.name) {
    n = chartObject.series.name;
  }

  let s = `<div class="tooltip" style="border: 1px solid ${chartObject.color}; border-radius:3px; padding: 3px;">`;
  s += `<span style="font-size: 10px">
    ${moment(getDate(chartObject?.point?.timestamp)).format(
      'dddd, MMMM Do YYYY h:mm:ss a'
    )}
    </span><br/>`;
  s +=
    '<span style="color:' +
    chartObject.color +
    '">●</span><b> ' +
    n +
    ' </b><br/>';
  s +=
    '<span style="font-size: 10px">x: <b> ' +
    getNumberBySigFigs(chartObject.x, 4) +
    ' </b></span><br/>';
  s +=
    '<span style="font-size: 10px">y: <b> ' +
    getNumberBySigFigs(chartObject.y, 4) +
    ' </b></span><br/>';
  s += '</div>';

  return s;
}

export function decimalFormatter(value) {
  if (value !== null) {
    if (value % 1 !== 0) {
      return value.toFixed(1).toString();
    } else {
      return value;
    }
  } else {
    return null;
  }
}

function getDesignCurveKey(dc) {
  return 'dc:' + dc.PDTrendDesignCurveID;
}

function getDataFromDesignCurve(dc, isDCaDate, start?, stop?) {
  const values: { X: any; Y: any; Y1: any; Y2: any }[] = JSON.parse(dc.Values);
  let data = values.map((d) => {
    return {
      x: isDCaDate ? new Date(d.X) : Number(d.X),
      y: Number(d.Y),
      low: Number(d.Y1),
      high: Number(d.Y2),
    };
  });

  if (isDCaDate) {
    data.sort((a, b) => {
      return (a.x as Date).getTime() - (b.x as Date).getTime();
    });
  } else {
    data.sort((a, b) => {
      return (a.x as number) - (b.x as number);
    });
  }

  const combinedStartDate = moment(start);
  const combinedEndDate = moment(stop);
  const timeDiff = moment.duration(combinedEndDate.diff(combinedStartDate));
  const possibleValues = ['hour', 'day', 'week', 'month', 'year'];
  let repeatValue = dc.Repeat;
  if (dc.Repeat) {
    const valIndex = possibleValues.findIndex((val) => val === dc.Repeat);
    for (let i = valIndex; i < possibleValues.length; ++i) {
      let timeSpan = 1;
      switch (possibleValues[i]) {
        case 'day':
          timeSpan = Math.round(timeDiff.asDays());
          break;
        case 'hour':
          timeSpan = Math.round(timeDiff.asHours());
          break;
        case 'week':
          timeSpan = Math.round(timeDiff.asWeeks());
          break;
        case 'month':
          timeSpan = Math.round(timeDiff.asMonths());
          break;
        case 'year':
          timeSpan = Math.round(timeDiff.asYears());
          break;
      }
      repeatValue = possibleValues[i];
      // This is to avoid drawing too many design curve points
      // and overloading Highcharts. When that happens, this error appears:
      // Highcharts warning #12: www.highcharts.com/errors/12/
      if (timeSpan < 1260) {
        if (timeSpan < 2 && i === 0) {
          // with high frequency data we are looking at minutes and not hours
          repeatValue = 'minute';
          timeSpan = Math.round(timeDiff.asMinutes());
        }
        break;
      }
    }
  }

  if (isDCaDate && repeatValue && start && stop) {
    if (repeatValue === 'minute') {
      data = getRepeating(1000 * 60, data, start, stop);
    } else if (repeatValue === 'hour') {
      data = getRepeating(1000 * 60 * 60, data, start, stop);
    } else if (repeatValue === 'day') {
      data = getRepeating(1000 * 60 * 60 * 24, data, start, stop);
    } else if (repeatValue === 'week') {
      data = getRepeating(1000 * 60 * 60 * 24 * 7, data, start, stop);
    } else if (repeatValue === 'month') {
      data = getRepeatingMonth(data, start, stop);
    } else if (repeatValue === 'year') {
      data = getRepeatingYear(data, start, stop);
    }
  }
  return data;
}

function transformTimeseriesDatasToCoordinates(data: ITimeseriesDatapoint[]) {
  return data.map((d) => {
    return transformTimeseriesDataToCoordinates({
      Timestamp: d.Timestamp,
      Value: d.Value,
    });
  });
}

function transformTimeseriesDataToCoordinates(data: ITimeseriesDatapoint) {
  return {
    x: moment(data.Timestamp).toDate().getTime(),
    y: Number(data.Value),
  };
}

function getMinTimeSeriesValue(correlationTrend: ICorrelationData[]) {
  let values: number[] = [];
  correlationTrend?.forEach((mcTrend) => {
    switch (mcTrend.Name) {
      case EForecastTrend.Forecast:
      case EForecastTrend.GreaterThanThreshold:
      case EForecastTrend.LessThanThreshold:
      case EROCTrend.Regression:
      case EROCTrend.Expected:
      case EROCTrend.Upper:
      case EROCTrend.Lower:
        if (mcTrend.Data?.length > 0) {
          const dataValues: number[] = mcTrend.Data.map((d) => d.Value);
          values = values.concat(dataValues);
        }
        break;
      case EForecastTrend.LastRegression:
      case EForecastTrend.Horizon:
      case EForecastTrend.Exceedance:
        if (mcTrend.Data?.length > 0) {
          values.push(mcTrend.Data[0].Value);
        }
        break;
    }
  });

  return Math.min(...values);
}

function getMaxTimeSeriesValue(correlationTrend: ICorrelationData[]) {
  let values: number[] = [];
  correlationTrend?.forEach((mcTrend) => {
    switch (mcTrend.Name) {
      case EForecastTrend.Forecast:
      case EForecastTrend.GreaterThanThreshold:
      case EForecastTrend.LessThanThreshold:
      case EROCTrend.Regression:
      case EROCTrend.Expected:
      case EROCTrend.Upper:
      case EROCTrend.Lower:
        if (mcTrend.Data?.length > 0) {
          const dataValues: number[] = mcTrend.Data.map((d) => d.Value);
          values = values.concat(dataValues);
        }
        break;
      case EForecastTrend.LastRegression:
      case EForecastTrend.Horizon:
      case EForecastTrend.Exceedance:
        if (mcTrend.Data?.length > 0) {
          values.push(mcTrend.Data[0].Value);
        }
        break;
    }
  });

  return Math.max(...values);
}

export function getChartType(chartTypeID: number, isGroupedSeries: boolean) {
  let result = null;
  if (!isNil(chartTypeID)) {
    switch (chartTypeID) {
      case AtxChartLine:
        result = 'line';
        break;
      case AtxChartArea:
        result = 'area';
        break;
      case AtxChartColumn:
        result = 'column';
        break;
      case AtxChartTrend:
        if (isGroupedSeries) {
          result = 'trend';
        } else {
          result = 'bar';
        }
        break;
      case AtxChartTable:
        result = 'table';
        break;
      case AtxScatterPlot:
        result = 'scatter';
    }
  }
  return result;
}

function getSeriesKeyFromPdTagId(
  pdTagId: number,
  pinID?: number,
  archive?: string
) {
  if (!isNil(pinID)) {
    return 'tag:' + pdTagId + ':pin:' + pinID + ':archive:' + (archive || '');
  }
  return 'tag:' + pdTagId + ':pin::archive:' + (archive || '');
}

function seriesCompare(a, b) {
  if (a.legendIndex === b.legendIndex) {
    if (a.name > b.name) {
      return 1;
    } else if (b.name > a.name) {
      return -1;
    }
    return 0;
  } else {
    return a.legendIndex - b.legendIndex;
  }
}

function findMax(trendDef: IPDTrend) {
  let myMax: number = null;
  for (const pin of trendDef.Pins) {
    if (myMax === null || myMax < getDate(pin.EndTime).getTime()) {
      myMax = getDate(pin.EndTime).getTime();
    }
  }
  return myMax;
}

function getPointsByTagPinArchive(
  measurements: IAssetMeasurementsSet,
  tagID: number,
  archive: string,
  pinID?: number
) {
  let result: IAssetMeasurements = null;
  if (
    measurements &&
    measurements.Measurements &&
    measurements.Measurements.length > 0
  ) {
    for (const item of measurements.Measurements) {
      if (!isNil(pinID) && pinID != null) {
        if (
          tagID === item.Tag.PDTagID &&
          pinID === item.PinID &&
          archive === item.Archive
        ) {
          result = item;
        }
      } else {
        if (
          tagID === item.Tag.PDTagID &&
          archive === item.Archive &&
          item.PinID == null
        ) {
          result = item;
        }
      }
    }
    if (!result) {
      // try again ignoring archive
      for (const item of measurements.Measurements) {
        if (!isNil(pinID) && pinID != null) {
          if (tagID === item.Tag.PDTagID && pinID === item.PinID) {
            result = item;
          }
        } else {
          if (tagID === item.Tag.PDTagID && item.PinID == null) {
            result = item;
          }
        }
      }
    }
  }
  return result;
}

function getBreaks(trendDef: IPDTrend) {
  let myBreaks = [
    {
      from: findMin(trendDef),
      to: findMax(trendDef),
    },
  ];
  for (const pin of trendDef.Pins) {
    const tempBreaks: { from: number; to: number }[] = [];

    for (const myBreak of myBreaks) {
      // If the break is invalid then skip it
      if (myBreak.from >= myBreak.to) {
        // do nothing to skip the break
      } else if (
        myBreak.from >= getDate(pin.StartTime).getTime() &&
        myBreak.to <= getDate(pin.EndTime).getTime()
      ) {
        // do nothing to skip the break
      } else if (
        getDate(pin.StartTime).getTime() > myBreak.from &&
        getDate(pin.EndTime).getTime() < myBreak.to
      ) {
        tempBreaks.push({
          from: myBreak.from,
          to: getDate(pin.StartTime).getTime(),
        });
        tempBreaks.push({
          from: getDate(pin.EndTime).getTime(),
          to: myBreak.to,
        });
      } else if (
        getDate(pin.StartTime).getTime() <= myBreak.from &&
        getDate(pin.EndTime).getTime() >= myBreak.from
      ) {
        tempBreaks.push({
          from: getDate(pin.EndTime).getTime(),
          to: myBreak.to,
        });
      } else if (
        getDate(pin.StartTime).getTime() <= myBreak.to &&
        getDate(pin.EndTime).getTime() >= myBreak.to
      ) {
        tempBreaks.push({
          from: myBreak.from,
          to: getDate(pin.StartTime).getTime(),
        });
      } else {
        tempBreaks.push(myBreak);
      }
    }

    myBreaks = tempBreaks;
  }
  return myBreaks;
}

export function getDefaultHighchartsOptions(
  options?: Partial<Highcharts.Options>
) {
  return merge(
    {
      colors: [
        '#058DC7',
        '#50B432',
        '#ED561B',
        '#DDDF00',
        '#24CBE5',
        '#64E572',
        '#FF9655',
        '#FFF263',
        '#6AF9C4',
      ],
      chart: {
        backgroundColor: null,
        style: {
          fontFamily: 'Verdana, Arial, Helvetica, sans-serif',
        },
        marginRight: 40,
      },
      title: {
        style: {
          display: 'none',
        },
      },
      subtitle: {
        style: {
          display: 'none',
        },
      },
      tooltip: {
        borderWidth: 0,
      },
      legend: {
        itemStyle: {
          fontWeight: 'bold',
          fontSize: '13px',
        },
      },

      xAxis: {
        labels: {
          style: {
            color: '#6e6e70',
          },
        },
      },
      yAxis: {
        labels: {
          style: {
            color: '#6e6e70',
          },
        },
      },
      plotOptions: {
        series: {
          shadow: true,
        },
        candlestick: {
          lineColor: '#404048',
        },
        map: {
          shadow: false,
        },
      },

      // Highstock specific
      navigator: {
        xAxis: {
          gridLineColor: '#cfcfcf',
        },
      },
      rangeSelector: {
        buttonTheme: {
          fill: 'white',
          stroke: '#C0C0C8',
          'stroke-width': 1,
          states: {
            select: {
              fill: '#cfcfcf',
            },
          },
        },
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8',
      },

      // General
      // background2: '#E0E0E8',
      credits: {
        enabled: false,
        style: {
          color: '#666',
        },
      },
      exporting: {
        allowHTML: true,
        enabled: false,
      },
      time: {
        useUTC: false,
      },
      lang: {
        thousandsSep: ',',
        numericSymbols: ['k', 'M', 'B', 'T', 'P', 'E'],
      },
    },
    options ?? {}
  ) as Highcharts.Options;
}

function getAlternateName(names: string[], index: number) {
  // First make sure the index is valid.  Can't have null, undefined or a negative number.
  index = Math.abs(index ?? 0);

  // The logic here is:
  // Choose a name from the list of names based on the index
  // If the index is one past the number of names in the list then return null so that we can turn off the legend.
  // If the index is too big then bring it down to a reasonable range using the modulus operator.

  let result: string = null;
  if (names && names.length > 0) {
    const i = index % (names.length + 1);
    if (i < names.length) {
      result = names[i];
    } else {
      result = null;
    }
  }

  if (result === ' \\ Generic') {
    result = 'no variable type';
  }

  return result;
}

// highlight y axis labels and title
function highlightYAxis(series, weight) {
  if (Object.prototype.hasOwnProperty.call(series, 'yAxis')) {
    series.yAxis.update({
      title: {
        style: {
          fontWeight: weight,
        },
      },
      labels: {
        style: {
          fontWeight: weight,
        },
      },
    });
  }
}

// This will turn the definition of a trend that comes from the server into an object that can be interpreted by
// the highcharts directive.  If data has been added to the object from the server it will be displayed.  If not
// then data can be added with the setDataOnChartConfiguration method above.
export function createHighchartsDefinition(
  trend: IPDTrend,
  isGroupedSeries: boolean,
  theme: string,
  measurements?: IAssetMeasurementsSet,
  startDate?: Date,
  endDate?: Date,
  labelIndex?: number,
  isToggled?: boolean,
  allowHiddenLabel?: boolean,
  exportSettings?: Highcharts.ExportingOptions,
  showDataCursor?: boolean,
  enablePointSelection?: boolean,
  correlationTrend?: ICorrelationData[],
  highlight?: any
): Highcharts.Options {
  const colors = new ColorService();
  // eslint-disable-next-line no-unsafe-optional-chaining
  const sortedPins = [...trend?.Pins].sort((a, b) => {
    return a.DisplayOrder - b.DisplayOrder;
  });

  if (sortedPins.length > 0 && !trend.ShowSelected) {
    for (const pin of sortedPins) {
      if (!pin.Hidden) {
        let found = false;
        measurements?.Measurements?.some((measurement) => {
          if (measurement.PinID === pin.PDTrendPinID) {
            if (measurement.Data.length > 0) {
              startDate = moment(pin.StartTime).toDate();
              endDate = moment(pin.EndTime).toDate();
              found = true;
              return true;
            }
          }
        });
        if (found) {
          break;
        }
      }
    }

    sortedPins.forEach((pin) => {
      if (!pin.Hidden) {
        measurements?.Measurements?.forEach((measurement) => {
          if (measurement.PinID === pin.PDTrendPinID) {
            if (measurement.Data.length > 0) {
              const compareStart = moment(pin.StartTime).toDate();
              const compareEnd = moment(pin.EndTime).toDate();
              if (compareStart < startDate) {
                startDate = compareStart;
              }
              if (compareEnd > endDate) {
                endDate = compareEnd;
              }
            }
          }
        });
      }
    });
  }

  // Create a result that has the chart definition that can be passed to the chart object.
  const highChartsOptions = getDefaultHighchartsOptions({
    series: [],
  });

  // This will send the pins along to the render function in the event that the user wants to see a table.
  // When pins come from the server they may be in strings instead of Dates.
  // This will turn the strings into dates at the beginning so we don't have to deal with it later.
  const tablePins = sortedPins.map((p, k) => {
    return {
      ...p,
      StartTime: getDate(p.StartTime),
      EndTime: getDate(p.EndTime),
      PinColor: isNil(p.PinColor) ? k : p.PinColor,
    };
  });

  // This will be set to the series object if it is found.  It will remain null if this is not an xy chart.
  let isXY: IPDTrendSeries = null;
  let cntPin = 0;

  // The name lengths are used to format the legend area.
  let maxNameLength = 0;
  const sortedSeries = [...trend.Series].sort((a, b) => {
    return a.DisplayOrder - b.DisplayOrder;
  });

  let countNonEmptyLabels = 0;

  //If there's atleast one series that's bold, fade other series that's not bold.
  const fadeOtherSeries: boolean =
    sortedSeries.findIndex((ser) => ser.isBold === true) >= 0;
  sortedSeries.forEach((series, seriesIndex) => {
    // Grab the X Series if it exists.  This assumes that only one series has the IsXAxis flag set.
    // If more then one has it set it takes the last one.
    if (series.IsXAxis) {
      isXY = series;
    }

    const alternateNames = series.AlternateDisplayTexts || [];

    // A series could have multiple tags associated with it.  This looks through the tags in the
    // series.  In practice this will not be the case and any series will probably only have the one tag.
    for (const mapData of series.MapData) {
      // check trend label values if empty or not
      // return count trend labels that are not empty
      const seriesName = getAlternateName(alternateNames, labelIndex);
      if (seriesName) {
        countNonEmptyLabels++;
      }

      const highChartsSeries: Highcharts.SeriesOptionsType = {
        legendIndex: series.DisplayOrder,
        id: getSeriesKeyFromPdTagId(series.PDTagID, null, series.Archive),
        data: [{}], // This will display the YAxis label even if there's no data on it
        marker: {
          symbol: colors.colorServiceSymbols[0],
        },
        zIndex: 10,
        turboThreshold: 0,
        color: series.SeriesColor || colors.getNextColor(),
        stack: series.Stack || null,
        stacking: (series.StackType as Highcharts.OptionsStackingValue) || null,
        type: getChartType(series.ChartTypeID, isGroupedSeries),
        name: series.DisplayText || '',
        visible: series.visible ?? true,
        lineWidth: 1.5,
        opacity: 1,
        custom: {
          units: '',
          isBold: series.isBold ?? false,
        },
      };

      //For scatter plots hide/don't show trend line
      if (trend.ChartTypeID === 24) {
        highChartsSeries.lineWidth = 0;
      }

      if (
        +trend.ChartTypeID === AtxChartLine ||
        +trend.ChartTypeID === AtxChartArea
      ) {
        highChartsSeries.lineWidth = series.isBold ? 3 : 2;
        if (fadeOtherSeries) {
          highChartsSeries.opacity = series.isBold
            ? 1
            : +trend.ChartTypeID === AtxChartLine
            ? 0.5
            : 0.2;
        } else {
          highChartsSeries.opacity = 1;
        }
      }

      // Name the series.
      if (series.DisplayText) {
        highChartsSeries.name = seriesName || series.DisplayText;
      }
      maxNameLength = Math.max(
        maxNameLength,
        highChartsSeries.name?.length ?? 0
      );

      // Don't process the axis and pins for this tag if the series is the X Axis.
      if (!series.IsXAxis) {
        highChartsSeries.yAxis = 'axis' + series.Axis + ':' + trend.PDTrendID;
        const ax = trend.Axes.find((axis) => axis.Axis === series.Axis);
        if (ax) {
          highChartsSeries.custom.units = ax.Title;
        }

        if (tablePins.length > 0) {
          if (trend.PinTypeID === AtxPinTypeUnmodified && trend.ShowSelected) {
            // In this situation the currently selected time range needs to be shown possibly in addition to the pins.
            const activeSeries = {
              ...highChartsSeries,
              id: getSeriesKeyFromPdTagId(series.PDTagID, null, series.Archive),
            };
            if (trend.ShowPins) {
              activeSeries.name = 'Active: ' + activeSeries.name;
              activeSeries.zIndex = 0;
              // activeSeries.color = activePinColors.selectedColor();
            }

            if (
              every(
                highChartsOptions.series,
                (n) => n.name !== activeSeries.name
              )
            ) {
              highChartsOptions.series.push(activeSeries);
            }
          }
          if (
            trend.PinTypeID === AtxPinTypeNoGaps ||
            trend.PinTypeID === AtxPinTypeOverlay ||
            trend.ShowPins
          ) {
            // Here we need to take the series and add a new series for each pin in the system.
            let i = 0;

            const startIndex = trend.ShowPins ? sortedSeries.length + 1 : 1;
            sortedPins.forEach((pin, pinIndex) => {
              if (!pin.Hidden) {
                const newPin = {
                  ...highChartsSeries,
                  legendIndex:
                    seriesIndex + startIndex + sortedSeries.length * pinIndex,
                  color:
                    colorBrewerDivergent[i % colorBrewerDivergent.length][
                      seriesIndex % 4
                    ],
                  shadow: true,
                  zIndex: 23,
                  marker: {
                    symbol:
                      colors.colorServiceSymbols[
                        i % colors.colorServiceSymbols.length
                      ],
                  },

                  PinColor: isNil(pin.PinColor) ? cntPin : pin.PinColor,
                };

                const pinID = isNil(pin.PDTrendPinID)
                  ? cntPin
                  : pin.PDTrendPinID;

                newPin.id = getSeriesKeyFromPdTagId(
                  series.PDTagID,
                  pinID,
                  series.Archive
                );
                newPin.name = pin.Name.trim() + ': ' + newPin.name;

                if (
                  !highChartsOptions.series.map((n) => n.id).includes(newPin.id)
                ) {
                  highChartsOptions.series.push(newPin);
                }
              }
              i++;
              cntPin++;
            });
          }
        } else {
          highChartsOptions.series.push(highChartsSeries);
        }
      }
    }
  });

  if (!highChartsOptions.legend) {
    highChartsOptions.legend = {};
  }
  // if all trend labels are empty, legend will not be shown in the chart
  // if at least one trend label value is not empty, legend will be shown in the chart
  if (isToggled && allowHiddenLabel) {
    if (countNonEmptyLabels > 0) {
      highChartsOptions.legend.enabled = true;
    } else {
      highChartsOptions.legend.enabled = false;
    }
  }

  highChartsOptions.tooltip = {
    distance: 2,
    snap: 5,
  };

  highChartsOptions.plotOptions = {
    line: {
      events: {
        afterAnimate: function () {
          const chart = this.chart;
          const series = chart.series;

          // set hover event for each legend item
          for (const item of series) {
            if (!item['legendItem']) {
              return;
            }
            const element = item['legendItem'].element;
            const fontWeight = item.userOptions['custom'].isBold
              ? 'bold'
              : 'normal';

            element.onmouseover = function () {
              if (item.visible) {
                //highlight yAxis that corresponds to bold series
                highlightYAxis(item, 'bold');
              }
            };

            element.onmouseout = function () {
              //set yAxis labels and ticks to original font weight
              highlightYAxis(item, fontWeight);
            };
          }
        },
      },
    },
    series: {
      allowPointSelect: false,
      stickyTracking: true,
      point: {
        events: {
          mouseOver() {
            if (
              !isGroupedSeries &&
              (+trend.ChartTypeID === AtxChartColumn ||
                +trend.ChartTypeID === AtxChartBar)
            ) {
              const point = this;
              const series = point.series;
              // highlight hovered series and dim other series
              series.chart.series.forEach((seriesX) => {
                if (seriesX.visible && seriesX.data.length > 0) {
                  if (seriesX.name === series.name) {
                    series.setState('hover');
                  } else {
                    seriesX.setState('inactive');
                  }
                }
              });
            }
          },
          mouseOut() {
            if (
              !isGroupedSeries &&
              (+trend.ChartTypeID === AtxChartColumn ||
                +trend.ChartTypeID === AtxChartBar)
            ) {
              const point = this;
              const series = point.series;
              // set series opacity back to default
              series.chart.series.forEach((seriesX) => {
                if (seriesX.visible && seriesX.data.length > 0) {
                  seriesX.setState('');
                }
              });
            }

            this.series.chart.tooltip.hide();
          },
          select: null,
          unselect: null,
        },
      },
      findNearestPointBy: 'x',
      states: {},
      events: {},
      marker: {},
    },
  };

  if (
    +trend.ChartTypeID === AtxChartLine ||
    +trend.ChartTypeID === AtxChartArea
  ) {
    highChartsOptions.plotOptions.series.states.hover = {
      lineWidthPlus: 0,
    };
  }

  // Scatter plot will be implemented on a separate PBI
  if (!isXY && !isGroupedSeries && enablePointSelection) {
    highChartsOptions.plotOptions.series.allowPointSelect = true;
    highChartsOptions.plotOptions.series.marker = {
      enabled: true,
      radius: 0.1,
      states: {
        hover: {
          enabled: true,
          radius: 6,
        },
        select: {
          enabled: true,
          radius: 0.1,
          fillColor: null,
        },
      },
    };

    if (enablePointSelection) {
      // showDataCursor updated value can't be read properly inside the event
      // so putting it on a global variable will fix it
      enableDataCursor = showDataCursor ?? true;
      highChartsOptions.plotOptions.series.point.events.select = (events) => {
        const point = { ...events.target } as any;
        setTimeout(() => {
          const selectedPoints = point?.series['chart']?.getSelectedPoints();
          if (
            selectedPoints?.length > 2 ||
            (enableDataCursor === false && events.accumulate === true)
          ) {
            // This will disable selecting points if max selected points, which is 2, is reached
            point.series.data[point.index].select(false, true);
          } else if (events.accumulate === false) {
            // This will unselect all selected points if the user is using a single click event
            point.series.data[point.index].select(false, false);
          } else {
            const logger = new LoggerService();
            if (!firstIndex && firstIndex !== 0) {
              logger.trackTaskCenterEvent(
                'DataExplorerV2:DataCursor:ShiftClick',
                'FirstClick'
              );
              const firstPointColor = '#6ea09d';
              // Crosshair will only work on hover. Adding plotline is the alternative way.
              // This will add a vertical line on the selected point
              point.series.xAxis.visible = true;
              point.series.xAxis.addPlotLine({
                value: point.x,
                color: firstPointColor,
                width: 1.7,
                zIndex: -1,
                dashStyle: 'LongDashDot',
                id: 'firstPlotLine',
              });

              firstIndex = point.index;
              // This will clone the tooltip on hover
              firstSelectedToolTip =
                point.series.chart.tooltip.container.lastChild.firstChild.cloneNode(
                  true
                );
              firstSelectedToolTip.classList.add('firstSelectedToolTip');
              // This will set the styles necessary
              firstSelectedToolTip.style.pointerEvents = 'unset';
              firstSelectedToolTip.style.cursor = 'move';
              firstSelectedToolTip.firstChild.style.borderWidth = '2px';
              firstSelectedToolTip.firstChild.style.dashStyle = '';
              firstSelectedToolTip.firstChild.style.borderColor =
                firstPointColor;
              // This will make the cloned tooltip draggable
              firstSelectedToolTip.firstChild.classList.add('draggable');
              dragElement(firstSelectedToolTip, point);
              // This will add the cloned tooltip to the chart
              point.series.chart.container.parentNode.appendChild(
                firstSelectedToolTip
              );
            } else {
              logger.trackTaskCenterEvent(
                'DataExplorerV2:DataCursor:ShiftClick',
                'SecondClick'
              );
              const secondPointColor = '#bde4e4';
              // Crosshair will only work on hover. Adding plotline is the alternative way.
              // This will add a vertical line on the selected point
              point.series.xAxis.visible = true;
              point.series.xAxis.addPlotLine({
                value: point.x,
                color: secondPointColor,
                width: 1.7,
                zIndex: -1,
                dashStyle: 'LongDashDot',
                id: 'secondPlotLine',
              });

              secondIndex = point.index;
              // This will clone the tooltip on hover
              secondSelectedToolTip =
                point.series.chart.tooltip.container.lastChild.firstChild.cloneNode(
                  true
                );
              secondSelectedToolTip.classList.add('secondSelectedToolTip');
              // This will set the styles necessary
              secondSelectedToolTip.style.pointerEvents = 'unset';
              secondSelectedToolTip.style.cursor = 'move';
              secondSelectedToolTip.style.top =
                firstSelectedToolTip.clientHeight + 15 + 'px';
              secondSelectedToolTip.firstChild.style.borderWidth = '2px';
              secondSelectedToolTip.firstChild.style.borderColor =
                secondPointColor;
              // This will make the cloned tooltip draggable
              secondSelectedToolTip.firstChild.classList.add('draggable');
              dragElement(secondSelectedToolTip, point);
              // This will add the cloned tooltip to the chart
              point.series.chart.container.parentNode.appendChild(
                secondSelectedToolTip
              );
            }
          }
        }, 100);
      };

      highChartsOptions.plotOptions.series.point.events.unselect = (events) => {
        const point = { ...events.target } as any;
        setTimeout(() => {
          if (events.accumulate === false) {
            // This will remove all the selected tooltips on the chart
            // and reset all the data back to the original values
            // if the user is using a single click event
            if (
              firstSelectedToolTip &&
              point?.series?.chart?.container?.parentNode?.contains(
                firstSelectedToolTip
              )
            ) {
              point.series.chart.container.parentNode.removeChild(
                firstSelectedToolTip
              );
            }
            if (
              secondSelectedToolTip &&
              point?.series?.chart?.container?.parentNode?.contains(
                secondSelectedToolTip
              )
            ) {
              point.series.chart.container.parentNode.removeChild(
                secondSelectedToolTip
              );
            }

            firstIndex = null;
            secondIndex = null;
            firstSelectedToolTip = null;
            secondSelectedToolTip = null;
            point?.series?.chart?.series?.map((ser) => {
              ser.xAxis.removePlotLine('firstPlotLine');
              ser.xAxis.removePlotLine('secondPlotLine');
            });
          } else if (events.accumulate === true) {
            // This will remove a certain selected tooltip on the chart
            // and reset its data back to the original values
            // if the user unselect a selected point
            if (point?.index === firstIndex) {
              if (
                firstSelectedToolTip &&
                point?.series?.chart?.container?.parentNode?.contains(
                  firstSelectedToolTip
                )
              ) {
                point.series.chart.container.parentNode.removeChild(
                  firstSelectedToolTip
                );
              }

              firstIndex = null;
              firstSelectedToolTip = null;
              point?.series?.chart?.series?.map((ser) => {
                ser.xAxis.removePlotLine('firstPlotLine');
              });
            } else if (point?.index === secondIndex) {
              if (
                secondSelectedToolTip &&
                point?.series?.chart?.container?.parentNode?.contains(
                  secondSelectedToolTip
                )
              ) {
                point.series.chart.container.parentNode.removeChild(
                  secondSelectedToolTip
                );
              }

              secondIndex = null;
              secondSelectedToolTip = null;
              point?.series?.chart?.series?.map((ser) => {
                ser.xAxis.removePlotLine('secondPlotLine');
              });
            }
          }
        }, 100);
      };
    }
  } else {
    highChartsOptions.plotOptions.series.marker = {
      radius: 3.0,
    };
  }
  // Sort the series based on the index and then the name.  The legend should be determined by the legendIndex
  // but there may be some aspects that do not respect that value.  Also, there could be multiple series with
  // the same legend index.  In that case we still want the order to be deterministic.
  highChartsOptions.series.sort(seriesCompare);

  // Set the legend index back to the order.  This is only necessary if there are multiple series with the same index.
  highChartsOptions.series.map((s, i) => {
    s.legendIndex = i;
  });

  // If one of the trends is set to X axis.
  if (isXY) {
    highChartsOptions.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'linear',
      showEmpty: true,
      title: {
        text: isXY.DisplayText,
      },
      min: null,
      max: null,
      breaks: null,
      gridZIndex: -1,
      tickPosition: 'outside',
      tickLength: 4,
      tickWidth: 2,
    };
    highChartsOptions.xAxis.endOnTick = true;

    const myXAxis = trend.Axes.find((axis) => axis.Axis === isXY.Axis);
    if (myXAxis) {
      if (!isNil(myXAxis.Min)) {
        highChartsOptions.xAxis.min = +myXAxis.Min;
      }
      if (!isNil(myXAxis.Max)) {
        highChartsOptions.xAxis.max = +myXAxis.Max;
      }
    }
  } else if (
    trend.ShowPins &&
    trend.PinTypeID === AtxPinTypeUnmodified &&
    !trend.ShowSelected
  ) {
    // If the pin type is 1 and we are not showing the selected range then go from the minimum pin time to the maximum pin time
    if (sortedPins.length > 0) {
      highChartsOptions.xAxis = {
        ...defaultXAxis,
        id: 'xAxis:' + trend.PDTrendID,
        type: 'datetime',
        showEmpty: true,
        min: findMin(trend),
        max: findMax(trend),
        title: {
          text: ' ',
        },
        visible: true,
        labels: {
          enabled: true,
        },
        breaks: null,
        gridZIndex: -1,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
      };
      if (startDate) {
        highChartsOptions.xAxis.min = startDate.getTime();
      }
      if (endDate) {
        highChartsOptions.xAxis.max = endDate.getTime();
      }
    } else {
      highChartsOptions.xAxis = {
        ...defaultXAxis,
        id: 'xAxis:' + trend.PDTrendID,
        type: 'datetime',
        showEmpty: true,
        min: findMin(trend),
        max: findMax(trend),
        title: {
          text: ' ',
        },
        gridZIndex: -1,
        visible: true,
        labels: {
          enabled: true,
        },
        breaks: null,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
      };
    }
    highChartsOptions.tooltip.formatter = null;
  } else if (trend.ShowPins && trend.PinTypeID === AtxPinTypeNoGaps) {
    // If the pin type is 2 then go from the minimum pin time to the maximum pin time but add in breaks
    highChartsOptions.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'datetime',
      showEmpty: true,
      min: findMin(trend),
      max: findMax(trend),
      breaks: getBreaks(trend),
      title: {
        text: ' ',
      },
      visible: true,
      labels: {
        enabled: true,
      },
      gridZIndex: -1,
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
    };
    highChartsOptions.tooltip.formatter = null;
  } else if (trend.ShowPins && trend.PinTypeID === AtxPinTypeOverlay) {
    // If the pin type is 3 them we don't need to worry about the axis
    // because it will be switched to a number axis with a tick for each data point.
    highChartsOptions.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'linear',
      showEmpty: true,
      title: {
        text: ' ',
      },
      visible: false,
      labels: {
        enabled: false,
      },
      breaks: null,
      gridZIndex: -1,
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
    };
    highChartsOptions.tooltip.formatter = null;
  } else {
    // It arrives here if we are not showing pins or we are showing the currently selected time.
    highChartsOptions.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'datetime',
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
      showEmpty: true,
      breaks: null,
      visible: true,
      labels: {
        enabled: true,
      },
      gridZIndex: -1,
      title: {
        text: ' ',
      },
    };

    if (startDate) {
      highChartsOptions.xAxis.min = startDate.getTime();
    }
    if (endDate) {
      highChartsOptions.xAxis.max = endDate.getTime();
    }
  }

  highChartsOptions.xAxis.gridLineWidth = trend.XAxisGridlines ? 1 : 0;

  // Assigning the axis definitions to the chart.
  highChartsOptions.yAxis = [];
  highChartsOptions.chart.marginRight = 40;
  for (const trendAxis of trend.Axes) {
    if (
      trend.Series.filter((s) => s.Axis === trendAxis.Axis && !s.IsXAxis)
        .length > 0
    ) {
      const boldAxis =
        trend.Series.findIndex(
          (ser) =>
            ser.isBold === true && ser.Axis === trendAxis.Axis && !ser.IsXAxis
        ) >= 0;
      const yAxisColor = theme === 'dark' ? '#E0E0E3' : '#666666';

      const highChartsAxis: Highcharts.YAxisOptions = {
        ...defaultYAxis,
        id: 'axis' + trendAxis.Axis + ':' + trend.PDTrendID,
        title: {
          text: trendAxis.Title,
          style: {
            fontWeight: boldAxis ? 'bold' : 'normal',
            color: yAxisColor,
          },
        },
        labels: {
          style: {
            fontWeight: boldAxis ? 'bold' : 'normal',
            color: yAxisColor,
          },
        },
        opposite: trendAxis.Position !== 0,
        showEmpty: false,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
        gridLineWidth: 0,
        gridZIndex: -1,
      };

      if (trendAxis.GridLine) {
        highChartsAxis.gridLineWidth = 1;
        highChartsAxis.tickInterval =
          typeof trendAxis.Step === 'number' ? trendAxis.Step : null;
        highChartsAxis.minorTickInterval =
          typeof trendAxis.MinorStep === 'number' ? trendAxis.MinorStep : null;
      } else {
        highChartsAxis.tickInterval = null;
        highChartsAxis.minorTickInterval = null;
      }

      if (trendAxis.Position !== 0) {
        // if axes are displayed on right, we need to remove margin
        highChartsOptions.chart.marginRight = null;
      }

      if (typeof trendAxis.Min === 'number') {
        highChartsAxis.min = trendAxis.Min;
        highChartsAxis.startOnTick = false;
        highChartsAxis.minPadding = 0;
      } else {
        highChartsAxis.startOnTick = false;
        highChartsAxis.minPadding = 0;
        // For all of the series associated with this axis
        for (const series of trend.Series) {
          if (series.Axis === trendAxis.Axis) {
            for (const seriesMapData of series.MapData) {
              // Find the data
              let min = getPointsByTagPinArchive(
                measurements,
                seriesMapData.Map.TagID,
                series.Archive,
                seriesMapData.PinID
              )?.proposedSeriesMin;

              if (correlationTrend && correlationTrend.length > 0) {
                min = Math.min(min, getMinTimeSeriesValue(correlationTrend));
              }

              if (min) {
                // set the min to the lower of the existing min or the data min
                if (
                  isNil(highChartsAxis.min) ||
                  highChartsAxis.min === undefined
                ) {
                  highChartsAxis.min = min;
                } else {
                  highChartsAxis.min = Math.min(highChartsAxis.min, min);
                }
              }
            }
          }
        }
      }

      if (typeof trendAxis.Max === 'number') {
        highChartsAxis.max = trendAxis.Max;
        highChartsAxis.endOnTick = false;
        highChartsAxis.maxPadding = 0;
      } else {
        highChartsAxis.endOnTick = false;
        highChartsAxis.maxPadding = 0;
        // For all of the series associated with this axis
        for (const s of trend.Series) {
          if (s.Axis === trendAxis.Axis) {
            for (const md of s.MapData) {
              // Find the data
              let max = getPointsByTagPinArchive(
                measurements,
                md.Map.TagID,
                s.Archive,
                md.PinID
              )?.proposedSeriesMax;

              if (correlationTrend && correlationTrend.length > 0) {
                max = Math.max(max, getMaxTimeSeriesValue(correlationTrend));
              }

              if (max) {
                // set the max to the greater of the existing max or the data max
                if (
                  isNil(highChartsAxis.max) ||
                  highChartsAxis.max === undefined
                ) {
                  highChartsAxis.max = max;
                } else {
                  highChartsAxis.max = Math.max(highChartsAxis.max, max);
                }
              }
            }
          }
        }
      }
      highChartsOptions.yAxis.push(highChartsAxis);
    }
  }

  highChartsOptions.tooltip = highChartsOptions.tooltip || {};
  // if tooltip.shared is set to true, hover specific series while other series are fade will not work.
  highChartsOptions.tooltip.shared = true;
  highChartsOptions.tooltip.valueDecimals = 0;
  highChartsOptions.tooltip.useHTML = true;
  highChartsOptions.tooltip.shadow = false;
  highChartsOptions.tooltip.outside = true;
  // result.xAxis.crosshair = true;

  if (
    +trend.ChartTypeID === AtxChartLine ||
    +trend.ChartTypeID === AtxChartArea
  ) {
    if (showDataCursor === false) {
      highChartsOptions.tooltip.formatter = () => {
        return false;
      };
    } else {
      highChartsOptions.tooltip.formatter = null;
    }
  }

  if (isNil(highChartsOptions.tooltip.formatter)) {
    if (
      (highChartsOptions.xAxis as Highcharts.AxisOptions).title.text === ' ' &&
      (highChartsOptions.xAxis as Highcharts.AxisOptions).type === 'datetime'
    ) {
      highChartsOptions.tooltip.formatter = function () {
        return defaultChartTooltipFormatter(this, +trend.ChartTypeID);
      };
      highChartsOptions.tooltip.backgroundColor = 'rgba(255,0,0,0)';
      highChartsOptions.tooltip.borderWidth = 0;
    } else if (
      (highChartsOptions.xAxis as Highcharts.AxisOptions).title.text === ' ' &&
      (highChartsOptions.xAxis as Highcharts.AxisOptions).type === 'linear'
    ) {
      highChartsOptions.tooltip.formatter = function () {
        return OverlayTooltipFormatter(this);
      };
      highChartsOptions.tooltip.backgroundColor = 'rgba(255,0,0,0)';
      highChartsOptions.tooltip.borderWidth = 0;
    } else {
      highChartsOptions.tooltip.formatter = function () {
        return varVsVarChartTooltipFormatter(this);
      };
      highChartsOptions.tooltip.backgroundColor = 'rgba(255,0,0,0)';
      highChartsOptions.tooltip.borderWidth = 0;
    }
  }

  let fixedLabelWidth = 400;

  if (maxNameLength > 0 && maxNameLength * 20 < fixedLabelWidth) {
    fixedLabelWidth = maxNameLength * 20;
  }

  const totalSeries = getTrendTotalSeries(trend);

  if (totalSeries > 3) {
    highChartsOptions.legend = {
      itemStyle: {},
      itemWidth: fixedLabelWidth,
    };
    (highChartsOptions.legend.itemStyle as any).width = fixedLabelWidth - 20;
    if (isToggled && allowHiddenLabel) {
      if (countNonEmptyLabels > 0) {
        highChartsOptions.legend.enabled = true;
      } else {
        highChartsOptions.legend.enabled = false;
      }
    }
  }

  highChartsOptions.chart = highChartsOptions.chart || {};
  highChartsOptions.chart.type =
    getChartType(+trend.ChartTypeID, isGroupedSeries) ||
    (isXY ? 'scatter' : 'line');

  highChartsOptions.chart.zoomType = 'xy';
  //Chart Panning Settings
  highChartsOptions.chart.panning = { enabled: true, type: 'xy' };
  highChartsOptions.chart.panKey = 'shift';

  // Here we process the data so that if data is present it is drawn on the chart
  const dataToPlot: {
    [key: string]: IPDRecordedPoint[];
  } = {};
  for (const series of sortedSeries) {
    for (const data of series.MapData) {
      if (data.Data && data.Data.length > 0) {
        // There is data for at least one chart.
        dataToPlot[
          getSeriesKeyFromPdTagId(series.PDTagID, data.PinID, series.Archive)
        ] = data.Data;
      }
    }
  }
  if (measurements && measurements.Measurements) {
    for (const assetMeasurement of measurements.Measurements) {
      dataToPlot[
        getSeriesKeyFromPdTagId(
          assetMeasurement.Tag.PDTagID,
          assetMeasurement.PinID,
          assetMeasurement.Archive
        )
      ] = assetMeasurement.Data;
    }
  }

  let timestampDictionary: { [key: number]: any } = null;
  if ((isXY?.MapData?.length ?? 0) > 0 && measurements?.Measurements) {
    // This is an XY chart so we need to process the data into appropriate series.
    // Here we need to process the data into a variable vs variable chart.
    for (const mapData of isXY.MapData) {
      const xyTag = mapData.Map.TagID;
      if ((measurements?.Pins?.length ?? 0) > 0 && trend.ShowPins) {
        for (const p of measurements.Pins) {
          const xyData = getPointsByTagPinArchive(
            measurements,
            xyTag,
            isXY.Archive,
            p.PDTrendPinID
          );
          timestampDictionary = setTimestampDictionary(
            timestampDictionary,
            xyData
          );
        }
      }
      if (trend.ShowSelected) {
        const xyData = getPointsByTagPinArchive(
          measurements,
          xyTag,
          isXY.Archive,
          null
        );
        timestampDictionary = setTimestampDictionary(
          timestampDictionary,
          xyData
        );
      }
    }
  }

  let timeInMilliseconds = null;
  let timestampValue = null;
  for (const series of highChartsOptions.series) {
    let seriesDataToPlot = dataToPlot[series.id];
    if (!seriesDataToPlot) {
      if (series.id) {
        const archiveIndex = series.id.lastIndexOf(':') + 1;
        const archiveString = series.id.substring(0, archiveIndex);
        seriesDataToPlot = dataToPlot[archiveString];
      }
    }
    if (seriesDataToPlot && seriesDataToPlot.length > 0) {
      const newSeries = [];
      if (timestampDictionary) {
        // Need to make sure that the timestamp dictionary and value is not null.
        for (const data of seriesDataToPlot) {
          if (
            data.Status !== 255 &&
            data.Value !== null &&
            data.Value !== undefined
          ) {
            timeInMilliseconds = data.Time.getTime();
            timestampValue = timestampDictionary[timeInMilliseconds];
            if (timestampValue !== null && timestampValue !== undefined) {
              if (isXY) {
                newSeries.push({
                  x: timestampValue,
                  y: data.Value,

                  timestamp: moment(data.Time).format('M/D/YY h:mm A'),
                });
              } else {
                newSeries.push({
                  x: timestampValue,
                  y: data.Value,
                });
              }
            } else {
              timestampValue = null;
            }
          }
        }
      } else if (trend.ShowPins && trend.PinTypeID === AtxPinTypeOverlay) {
        // Here the chart is supposed to display pinned data in an overlay.  That means we need to take each series and
        // adjust the timestamp so that is a integer representing the ticks of the date minus the ticks of the first date.
        const startTick = seriesDataToPlot
          .reduce(function (r, a) {
            return r.Time < a.Time ? r : a;
          })
          .Time.getTime();

        for (const data of seriesDataToPlot) {
          if (data.Status !== 255) {
            newSeries.push({
              x: (data.Time.getTime() - startTick) / 10000,
              y: data.Value,
              timestamp: moment(data.Time).format('M/D/YY h:mm A'),
            });
          }
        }
      } else {
        for (let i = 0; i < seriesDataToPlot.length; i += 1) {
          const data = seriesDataToPlot[i];

          if (isXY) {
            newSeries.push({
              x: data.Time.getTime(),
              y: data.Status !== 255 ? data.Value : null,
              timestamp: moment(data.Time).format('M/D/YY h:mm A'),
            });
          }
          //checks if a point is islanded by two 255 status points and enabled a point marker for it
          else if (
            seriesDataToPlot[i - 1]?.Status === 255 &&
            seriesDataToPlot[i + 1]?.Status === 255
          ) {
            newSeries.push({
              x: data.Time.getTime(),
              y: data.Status !== 255 ? data.Value : null,
              marker: {
                enabled: true,
              },
            });
          } else {
            newSeries.push({
              x: data.Time.getTime(),
              y: data.Status !== 255 ? data.Value : null,
            });
          }
        }
      }
      newSeries.sort((a, b) => {
        return a.x - b.x;
      });
      (series as any).data = newSeries;
    }
  }

  if (
    trend.DesignCurves &&
    trend.DesignCurves.length > 0 &&
    trend.PinTypeID !== AtxPinTypeOverlay
  ) {
    for (const designCurve of trend.DesignCurves) {
      const curveData = getDataFromDesignCurve(
        designCurve,
        !isXY,
        startDate,
        endDate
      );
      const newSeries = {
        type: designCurve.Type === 'range' ? 'areasplinerange' : 'spline',
        id: getDesignCurveKey(designCurve),
        color: designCurve.Color,
        data: curveData,
        yAxis: 'axis' + String(designCurve.Axis) + ':' + trend.PDTrendID,
        name: designCurve.DisplayText,
        legendIndex: highChartsOptions.series.length + 1,
        fillOpacity: 0.1,
        fillColor: hexToRGB(designCurve.Color, 0.4),
        marker: {},
        custom: {},
      };
      const hasAxis = highChartsOptions.yAxis.find(
        (axis) => axis.id === newSeries.yAxis
      );
      if (hasAxis) {
        highChartsOptions.series.unshift(newSeries as any);
      }
    }
  }

  if (
    trend.PDTrendID === -1 &&
    correlationTrend &&
    correlationTrend.length > 0
  ) {
    const correlationTrendType =
      correlationTrend?.find((ct) => ct.Name === EROCTrend.Regression)?.Data
        ?.length > 0
        ? 'ROC'
        : 'Forecast';

    correlationTrend?.forEach((mcTrend) => {
      const maxZIndex = Math.max(
        ...highChartsOptions.series.map((s) => {
          return s.zIndex ?? -1;
        })
      );
      switch (mcTrend.Name) {
        case EForecastTrend.Forecast:
        case EForecastTrend.GreaterThanThreshold:
        case EForecastTrend.LessThanThreshold:
        case EROCTrend.Regression:
        case EROCTrend.Expected:
        case EROCTrend.Upper:
        case EROCTrend.Lower:
          if (mcTrend.Data?.length > 0) {
            highChartsOptions.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: 'line',
              legendIndex: highChartsOptions.series.length + 1,
              marker: {
                radius: 0,
              },
              dashStyle: mcTrend.Name === EROCTrend.Expected ? 'Dash' : 'Solid',
              lineWidth:
                mcTrend.Name === EROCTrend.Upper ||
                mcTrend.Name === EROCTrend.Lower
                  ? 1
                  : 2,
              color: CorrelationTrendColorMapping[mcTrend.Name],
              custom: {
                units:
                  trend.Axes.find((axis) => axis.Axis === sortedSeries[0].Axis)
                    ?.Title ?? '',
              },
              data: transformTimeseriesDatasToCoordinates(mcTrend.Data),
              zIndex: maxZIndex + 1,
            } as any);
          }
          break;
        case EForecastTrend.LastRegression:
        case EForecastTrend.Horizon:
          if (mcTrend.Data?.length > 0) {
            highChartsOptions.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: null,
              legendIndex: highChartsOptions.series.length + 1,
              marker: {
                symbol: 'diamond',
                radius: 8,
              },
              color: CorrelationTrendColorMapping[mcTrend.Name],
              lineWidth: 0,
              custom: {
                units:
                  trend.Axes.find((axis) => axis.Axis === sortedSeries[0].Axis)
                    ?.Title ?? '',
              },
              data: [transformTimeseriesDataToCoordinates(mcTrend.Data[0])],
              zIndex: maxZIndex + 1,
            } as any);
          }
          break;
        case EForecastTrend.Exceedance:
          if (mcTrend.Data?.length > 0) {
            highChartsOptions.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: null,
              legendIndex: highChartsOptions.series.length + 1,
              marker: {
                symbol: 'url(./assets/emblem-alerts.svg)',
                width: 15,
                height: 15,
              },
              lineWidth: 0,
              custom: {
                units:
                  trend.Axes.find((axis) => axis.Axis === sortedSeries[0].Axis)
                    ?.Title ?? '',
              },
              data: [transformTimeseriesDataToCoordinates(mcTrend.Data[0])],
              zIndex: maxZIndex + 1,
            } as any);
          }
          break;
      }
    });
  }

  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  highChartsOptions.exporting = exportSettings || highChartsOptions.exporting;

  // Manipulate some of the styling to get more data into view.
  highChartsOptions.credits = {
    enabled: false,
  };
  highChartsOptions.plotOptions.series.shadow = false;
  highChartsOptions.chart.animation = false;

  if (!correlationTrend || correlationTrend.length === 0) {
    if (!isXY) {
      highChartsOptions.plotOptions.series.marker = {
        enabled: false,
      };
    } else {
      highChartsOptions.plotOptions.series.marker = {
        enabled: true,
      };
    }
  } else {
    highChartsOptions.plotOptions.series.states = {
      inactive: { enabled: false },
    };
  }

  highChartsOptions.chart = {
    ...highChartsOptions.chart,
    spacingBottom: 0,
  };

  if (highlight) {
    highChartsOptions.xAxis.plotBands = [highlight];
  }

  return highChartsOptions;
}

// Model Trend Configurations
export function getModelChartConfigForEmptyResult(start?: Date, end?: Date) {
  const chartConfig = getModelChartConfigForResultWithData(
    [],
    [],
    [],
    '',
    3600,
    [],
    [],
    start,
    end,
    null,
    null
  );
  chartConfig.title.text = 'No data found.';
  return chartConfig;
}

export function turnEmptyModelTrendToHighcharts(
  title: string,
  exportSettings?: Highcharts.ExportingOptions
) {
  const result = getModelChartConfigForEmptyResult();
  result.title.text = title;
  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  result.exporting = exportSettings || result.exporting;
  return result;
}

export function turnModelTrendToHighcharts(
  title: string,
  modelTrend: IAssetModelChartingData,
  exportSettings?: Highcharts.ExportingOptions,
  actionData?: INDModelActionItemAnalysis[]
) {
  let result = getModelChartConfigForEmptyResult(
    modelTrend?.start,
    modelTrend?.end
  );

  const measurements = modelTrend;
  const ranges: {
    // name: string;
    x: number;
    low: number;
    high: number;
  }[] = [];
  const actuals = [];
  const expecteds = [];
  const bands = [];
  const annotations = [];

  let startingTick = 0;
  let endingTick = 0;
  let tickInterval = 3600; // 1 hour by default
  let minValueEncountered = Number.MAX_VALUE;
  let maxValueEncountered = Number.MIN_VALUE;

  if ((measurements?.SeriesList?.length ?? 0) > 0) {
    const series = measurements.SeriesList;
    startingTick = new Date(series[0].TimeStamp).getTime();
    endingTick = new Date(series[series.length - 1].TimeStamp).getTime();
    for (let i = 0; i < series.length; i++) {
      const element = series[i];
      const date = new Date(element.TimeStamp).getTime();
      const dataPoints = [
        element.Min,
        element.Max,
        element.Actual,
        element.Expected,
      ];
      const minValueForThisElement = Math.min(...dataPoints);
      if (minValueForThisElement < minValueEncountered) {
        minValueEncountered = minValueForThisElement;
      }

      const maxValueForThisElement = Math.max(...dataPoints);
      if (maxValueForThisElement > maxValueEncountered) {
        maxValueEncountered = maxValueForThisElement;
      }

      // check if surrounding points are null.
      let surroundedByNulls = element.EvaluationStatus === 0; // only true if current point is valid
      if (surroundedByNulls && i > 0) {
        surroundedByNulls = series[i - 1].EvaluationStatus !== 0; // and if previous point is null
      }
      if (surroundedByNulls && i < series.length - 1) {
        surroundedByNulls = series[i + 1].EvaluationStatus !== 0; // and if next point is null
      }

      ranges.push({
        // name: String(date),
        x: date,
        low: element.EvaluationStatus === 0 ? element.Min : null,
        high: element.EvaluationStatus === 0 ? element.Max : null,
      });

      if (surroundedByNulls) {
        actuals.push({
          // name: date,
          x: date,
          y: element.Actual,
          marker: {
            enabled: true,
            radius: 2,
            states: {
              hover: {
                radius: 6,
              },
            },
          },
        });
        expecteds.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Expected : null,
          marker: {
            enabled: true,
            radius: 2,
            states: {
              hover: {
                radius: 6,
              },
            },
          },
        });
      } else {
        actuals.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Actual : null,
        });
        expecteds.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Expected : null,
        });
      }
    }

    if (endingTick > 0) {
      tickInterval = (endingTick - startingTick) / 60;
    }

    for (const outOfBoundDate of measurements.OutOfBoundDates) {
      const dateFrom = new Date(outOfBoundDate.From).getTime();
      const dateTo = new Date(outOfBoundDate.To).getTime();
      bands.push({
        color: 'rgba(255,217,142,0.80)',
        from: dateFrom,
        to: dateTo,
        zIndex: -1,
      });
    }

    for (const alertEvent of measurements.Events) {
      if (alertEvent.TimeStamp) {
        const alertDate = new Date(alertEvent.TimeStamp).getTime();
        annotations.push({
          x: alertDate,
          y:
            minValueEncountered -
            (maxValueEncountered - minValueEncountered) * 0.17,
          name: alertEvent.Text,
          marker: {
            radius: 7,
          },
          text: alertEvent.Text,
          eventType: alertEvent.EventType,
        });
      }
    }
    // configure chart and then get the minimum y value so that we can plot events there
    result = getModelChartConfigForResultWithData(
      ranges,
      actuals,
      expecteds,
      modelTrend?.Model?.DependentVariable?.Tag?.EngUnits,
      tickInterval,
      bands,
      annotations,
      modelTrend.start,
      modelTrend.end,
      modelTrend.Min,
      modelTrend.Max
    );
  }

  if (result && title) {
    result.title = {
      text: title,
    };
  }

  //Action Items
  actionData = actionData?.filter((item) => {
    const time = new Date(item.ChangeDate).getTime();
    return (
      time >= modelTrend?.start.getTime() && time <= modelTrend?.end.getTime()
    );
  });

  const watch = [];
  const clear = [];
  const maintenance = [];
  const diagnose = [];

  //Grouping Action items into series data
  //A data point needs a X,Y + we are adding our custom
  //prop actionData to use later for tooltip creation
  for (const item of actionData ?? []) {
    switch (+item.ModelActionItemTypeID) {
      case 1: // diagnose
        diagnose.push({
          x: new Date(item.ChangeDate).getTime(),
          y: 0,
          actionData: item,
        });
        break;
      case 3: // watch set
      case 11: // ignore set
      case 16: // quick watch
        watch.push({
          x: new Date(item.ChangeDate).getTime(),
          y: 0,
          actionData: item,
        });
        break;
      case 6: // maintenance
        maintenance.push({
          x: new Date(item.ChangeDate).getTime(),
          y: 0,
          actionData: item,
        });
        break;
      case 12: // clear alert
        clear.push({
          x: new Date(item.ChangeDate).getTime(),
          y: 0,
          actionData: item,
        });
        break;
    }
  }

  //Adding Action Items series to chart config
  //the order we push the series determines how the markers overlap each other
  //Series added first are show behind series added after.
  if (clear.length > 0) {
    result.series.push({
      name: 'Clear',
      data: clear,
      type: 'scatter',
      yAxis: 1,
      marker: {
        symbol: 'url(./assets/emblem-clear.svg)',
        width: 12,
        height: 12,
      },
    });
  }
  if (watch.length > 0) {
    result.series.push({
      name: 'Watch',
      data: watch,
      type: 'scatter',
      yAxis: 1,
      marker: {
        symbol: 'url(./assets/emblem-watch.svg)',
        width: 12,
        height: 12,
      },
    });
  }
  if (diagnose.length > 0) {
    result.series.push({
      name: 'Diagnose',
      data: diagnose,
      type: 'scatter',
      yAxis: 1,
      marker: {
        symbol: 'url(./assets/emblem-diagnose.svg)',
        width: 12,
        height: 12,
      },
    });
  }
  if (maintenance.length > 0) {
    result.series.push({
      name: 'Maintenance',
      data: maintenance,
      type: 'scatter',
      yAxis: 1,
      marker: {
        symbol: 'url(./assets/emblem-maintainence.svg)',
        width: 12,
        height: 12,
      },
    });
  }

  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  result.exporting = exportSettings || result.exporting;

  //Chart Panning Settings
  result.chart.panning = { enabled: true, type: 'xy' };
  result.chart.panKey = 'shift';

  return result;
}

export function getModelChartConfigForResultWithData(
  ranges,
  actuals,
  expecteds,
  yAxisUnit,
  tickInterval,
  bands,
  flags,
  start: Date,
  end: Date,
  min: number,
  max: number
): Highcharts.Options {
  let startTicks: number = null;
  let endTicks: number = null;
  if (start) {
    startTicks = start.getTime();
  }
  if (end) {
    endTicks = end.getTime();
  }

  const chartConfig: Highcharts.Options = getDefaultHighchartsOptions({
    chart: {
      marginRight: 40,
      zoomType: 'xy',
    },
    plotOptions: {
      series: {
        turboThreshold: 0,
        states: {
          //removes the fade effect for background series when hovering another series.
          inactive: {
            opacity: 1,
          },
        },
      },
    },
    credits: {
      enabled: false,
    },
    title: {
      text: 'Testing',
    },
    xAxis: {
      type: 'datetime',
      plotBands: bands,
      min: startTicks,
      max: endTicks,
    },
    tooltip: {
      enabled: true,
      shared: true,
      valueDecimals: 0,
    },
    yAxis: [
      {
        title: {
          text: yAxisUnit,
        },
        labels: {},
        min,
        max,
        startOnTick: true,
        endOnTick: true,
      },
      //Second yAxis for Action Markers
      {
        visible: false,
      },
    ],
    series: [
      {
        name: 'Actual',
        data: actuals,
        zIndex: 2,
        color: 'blue',
        lineWidth: 2,
        type: null,
      },
      {
        name: 'Expected',
        data: expecteds,
        zIndex: 3,
        color: 'red',
        lineWidth: 2,
        type: null,
      },
      {
        name: 'Range',
        data: ranges,
        type: 'arearange',
        lineWidth: 0,
        linkedTo: ':previous',
        fillColor: 'rgba(189, 228, 228, 0.4)',
        marker: {
          enabled: false,
          states: {
            hover: {
              enabled: false,
            },
          },
        },
        zIndex: 1,
      },
      {
        name: 'Event',
        data: flags,
        type: 'scatter',
        color: 'red',
        tooltip: {
          // eslint-disable-next-line object-shorthand
          pointFormatter: function () {
            return String(this.x);
          },
        },
      },
    ],
  });

  //For custom yAxis min/max values we want the startOnTick & endOnTick props = false
  //so odd min/max values won't cause the yAxis to be extended because it needs
  //to start/end on a tick
  if (
    typeof chartConfig.yAxis[0].min === 'number' &&
    typeof chartConfig.yAxis[0].max === 'number'
  ) {
    chartConfig.yAxis[0] = {
      ...chartConfig.yAxis[0],
      startOnTick: false,
      endOnTick: false,
      minPadding: 0,
    };
  }

  chartConfig.tooltip.formatter = function () {
    return defaultChartTooltipFormatter(this);
  };

  return chartConfig;
}

export function getTitleFromModel(model: INDModel) {
  let result = model?.ModelName ?? '';
  if (model?.DependentVariable?.Tag?.TagName) {
    result = result + ' (' + model.DependentVariable.Tag.TagName + ')';
  }
  //sometimes we are passing INDModelSummary to Model Trend Chart Display
  //because Alerts uses INDModelSummary
  const modelSummary = model as unknown as INDModelSummary;
  if (modelSummary.TagName) {
    result = result + ' (' + modelSummary.TagName + ')';
  }
  return result;
}
