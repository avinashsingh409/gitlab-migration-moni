import { IProcessedTrend } from '@atonix/atx-core';
import { LabelIndex } from '../models/label-index';
import {
  createHighchartsDefinition,
  createId,
  getArchive,
  getName,
  getPinNumber,
  getTagNumber,
} from './chart.service';
import { colorBrewerDivergent, ColorService } from './color.service';
import { defaultYAxis, getNumberBySigFigs } from './utilities';

export function createSummaryChart(
  myTrend: IProcessedTrend,
  isGroupedSeries: boolean,
  theme: string,
  labelIndex: number,
  exportSettings: Highcharts.ExportingOptions
) {
  const baseSumInfo = createHighChartSummaryTableColumnOrBar(
    myTrend,
    isGroupedSeries,
    theme,
    labelIndex,
    exportSettings
  );

  const newHighchartsConfiguration = {
    ...baseSumInfo.highchartsOptions,
  } as Highcharts.Options;

  // Array containing the ids of each pin
  const pinList = [];
  const pinNames = [];

  for (const pin of myTrend.trendDefinition.Pins) {
    if (!pin.Hidden) {
      pinNames.push(pin.Name.replace(/</g, '_').replace(/>/g, '_'));
      pinList.push(pin.PDTrendPinID);
    }
  }

  let noPins = false;
  if (!pinList || pinList.length === 0) {
    // No legend
    if (!newHighchartsConfiguration.legend) {
      newHighchartsConfiguration.legend = {};
    }
    newHighchartsConfiguration.legend.enabled = false;
    noPins = true;
  } else {
    newHighchartsConfiguration.legend.enabled = true;
  }

  const xAxis = newHighchartsConfiguration.xAxis as Highcharts.XAxisOptions;
  xAxis.type = 'category';
  xAxis.visible = true;
  xAxis.max = baseSumInfo.seriesSettings.length - 1;
  xAxis.min = 0;
  xAxis.categories = [];
  newHighchartsConfiguration.series = [];
  newHighchartsConfiguration.tooltip = {
    shared: true,
    useHTML: true,
    headerFormat: '<small>{point.key}</small><br/>',
    pointFormat:
      '<span style="color: {series.color}">●</span> {series.name}: <b>{point.y}</b><br/>',
    footerFormat: '',
  };
  // rebuild series data and categories for summary rendering
  for (const so of baseSumInfo.seriesSettings) {
    // categories are sets of same tag
    xAxis.categories.push(baseSumInfo.seriesDict[so.seriesOrder]);
  }

  // see below - because each series will possibly have different UOM
  // and we can't associate with multiple axes per series
  const yAxes = newHighchartsConfiguration.yAxis as Highcharts.YAxisOptions[];
  if (yAxes.length > 1) {
    // the product owner requested that only a
    // single Y axis be displayed, but have all UOM for various tags
    let title = yAxes[0].title.text;
    for (let a = 1; a < yAxes.length; a++) {
      title += ' | ' + yAxes[a].title.text;
      yAxes[a].visible = false;
    }
    yAxes[0].title.text = title;
    yAxes[0].gridLineColor = '#cfcfcf';
    yAxes[0].title.style.color = theme === 'dark' ? '#E0E0E3' : '#cfcfcf';
    yAxes[0].labels.style.color = theme === 'dark' ? '#E0E0E3' : '#cfcfcf';
    yAxes[0].gridZIndex = -1;
  } else if (yAxes.length === 1) {
    yAxes[0].gridLineColor = '#cfcfcf';
    yAxes[0].gridZIndex = -1;
    yAxes[0].title.style.color = theme === 'dark' ? '#E0E0E3' : '#cfcfcf';
    yAxes[0].labels.style.color = theme === 'dark' ? '#E0E0E3' : '#cfcfcf';
  }
  if (baseSumInfo.hasNonPin) {
    const colors = new ColorService();
    const newSer = {
      name: noPins ? ' ' : 'Active', // if no pins, leave series names blank
      data: [],
      color: colors.getNextColor(),
    };
    for (let n = 0; n < baseSumInfo.seriesSettings.length; n++) {
      newSer.data.push({
        x: n,
        y: baseSumInfo.valuesDict[baseSumInfo.seriesSettings[n].seriesOrder],
      });
    }

    newHighchartsConfiguration.series.push(newSer as any);
  }
  for (let j = 0; j < pinList.length; j++) {
    const newSer = {
      // each series has pin name
      name: pinNames[j],
      data: [],
      color: colorBrewerDivergent[j % colorBrewerDivergent.length][0],
    };
    for (let i = 0; i < baseSumInfo.seriesSettings.length; i++) {
      const tagId = getTagNumber(baseSumInfo.seriesSettings[i].seriesOrder);
      const archive = getArchive(baseSumInfo.seriesSettings[i].seriesOrder);
      // and data values in array in category (tag names) order
      newSer.data.push({
        x: i,
        y: baseSumInfo.valuesDict[createId(tagId, pinList[j], archive)],
      });
    }
    newHighchartsConfiguration.series.push(newSer as any);
  }

  // TJC 2/21/18
  // each series is now named by Pin and has a point for each tag - this breaks the Highcharts assumption of the same UOM for a series
  // this is not ideal if the various tag UOM have order of magnitude different scales (0.35 vs 5,000)
  // can't find a Highcharts way around this other than to reverse categories and series (categories are pins and series are tags)
  // maybe each tag's points could be scaled for a relative proportion of the display and the tooltip displays the real value
  // each axis would be displayed and therefore line up with the tooltip/actual value

  newHighchartsConfiguration.chart = newHighchartsConfiguration.chart || {};
  return newHighchartsConfiguration;
}

export function createHighChartSummaryTableColumnOrBar(
  myTrend: IProcessedTrend,
  isGroupedSeries: boolean,
  theme: string,
  labelIndex: number,
  exportSettings?: Highcharts.ExportingOptions
) {
  const highchartsOptions = createHighchartsDefinition(
    myTrend.trendDefinition,
    isGroupedSeries,
    theme,
    myTrend.measurements,
    myTrend.startDate,
    myTrend.endDate,
    labelIndex,
    null,
    null,
    exportSettings
  );

  // this holds the summary types, keyed like the other dictionaries below
  const summaryTypesDict = {};

  // This will hold the actual values to put in the table.
  // It is a dictionary with the id as the key from the series
  const valuesDict: { [key: string]: number } = {};

  let hasNonPin = false;

  // This is an array where each entry is the id of the series
  const seriesSettings: {
    seriesOrder: string;
    seriesAxis: string | number;
  }[] = [];

  // This is a dictionary used to make sure that we don't add a tag to the array twice.
  // It is also used to apply the series label to the table row
  const seriesDict = {};
  for (const s of highchartsOptions.series) {
    if (s.id) {
      if (getPinNumber(s.id) === null) {
        hasNonPin = true;
      }
      const tagId = getTagNumber(s.id);
      const archive = getArchive(s.id);
      const key = createId(tagId, null, archive);
      if (seriesDict[key] === undefined) {
        seriesSettings.push({ seriesOrder: key, seriesAxis: s.yAxis });
        seriesDict[key] = getName(s.name);
      }
      if (s && (s as any).data && (s as any).data[0]) {
        valuesDict[s.id] = getNumberBySigFigs((s as any).data[0].y, 4);
      } else {
        valuesDict[s.id] = null;
      }
      if (s && (s as any).summaryType && (s as any).summaryType.SummaryDesc) {
        summaryTypesDict[key] = (s as any).summaryType.SummaryDesc;
      } else if (
        myTrend.trendDefinition.SummaryType &&
        myTrend.trendDefinition.SummaryType.SummaryDesc
      ) {
        summaryTypesDict[key] = myTrend.trendDefinition.SummaryType.SummaryDesc;
      } else {
        summaryTypesDict[key] = '-';
      }
    }
  }

  return {
    hasNonPin,
    seriesSettings,
    seriesDict,
    summaryTypesDict,
    valuesDict,
    highchartsOptions,
  };
}
