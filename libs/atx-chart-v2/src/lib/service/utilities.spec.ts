import { TestBed } from '@angular/core/testing';
import { getNumberBySigFigs, toPrecisionCustom } from './utilities';

describe('Utilities', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('getNumberBySigFigs', () => {
    expect(getNumberBySigFigs(12.345678, 2)).toEqual(12);
    expect(getNumberBySigFigs(0, 2)).toEqual(0);
  });

  it('toPrecisionCustom() should return not return trailing 0s', () => {
    expect(toPrecisionCustom(6, 4)).toEqual('6');
    expect(toPrecisionCustom(6.0, 4)).toEqual('6');
    expect(toPrecisionCustom(6.0001, 4)).toEqual('6');
    expect(toPrecisionCustom(0, 10)).toEqual('0');
    expect(toPrecisionCustom(4.234242342, 3)).toEqual('4.23');
    expect(toPrecisionCustom(-8573.129, 5)).toEqual('-8573.1');
    expect(toPrecisionCustom(129.32, -4)).toEqual('');
  });
});
