import { IModelConfigModelTrend } from '@atonix/shared/api';
import moment from 'moment';
import { getDate } from '@atonix/atx-core';
import { getNumberBySigFigs } from './utilities';
// import * as d3 from 'd3';

export function getChartConfigForEmptyResult(
  theme: string,
  start: Date,
  end: Date
) {
  const noData: IModelConfigModelTrend = {
    expectedValues: [],
    actualValues: [],
    expectedRange: [],
    timeStamps: [],
    startDate: start,
    endDate: end,
    breaks: [],
    tagName: '',
    tagUnits: '',
    yAxisMax: 10,
    yAxisMin: 0,
  };
  const chartConfig = getChartConfigForResultWithData(noData, theme);
  chartConfig.title.text = 'No data found.';

  return chartConfig;
}

export function getChartConfigForResultWithData(
  trend: IModelConfigModelTrend,
  theme: string
): Highcharts.Options {
  let startTicks: number = null;
  let endTicks: number = null;
  if (trend.startDate) {
    startTicks = trend.startDate.getTime();
  }
  if (trend.endDate) {
    endTicks = trend.endDate.getTime();
  }
  if (trend.expectedRange?.length > 0) {
    startTicks = trend.expectedRange[0][0];
    endTicks = trend.expectedRange[trend.expectedRange.length - 1][0];
  }
  const tickInterval = Math.floor(trend.actualValues.length / 5);
  // const pointVal = d3
  //   .scaleLinear()
  //   .domain([0, 2419200000, 315360000000])
  //   .range([2, 1.5, 0.01]);
  // const tVal = pointVal(endTicks - startTicks);
  // let tickInterval = 2 * 3600 * 1000;
  // if (endTicks > 0) {
  //   if (trend.breaks?.length > 0) {
  //     tickInterval = (endTicks - startTicks) / 8;
  //   } else {
  //     tickInterval = (endTicks - startTicks) / 5;
  //   }
  // }
  const chartConfig: Highcharts.Options = {
    chart: {
      spacingBottom: 0,
      backgroundColor: 'white',
      zoomType: 'xy',
      animation: false,
    },
    legend: {
      enabled: true,
      symbolHeight: 10,
      symbolRadius: 10,
      symbolWidth: 10,
      margin: 0,
    },
    plotOptions: {
      series: {
        shadow: true,
        turboThreshold: 0,
        animation: false,
      },
    },
    credits: {
      enabled: false,
    },
    title: {
      text: null,
    },
    xAxis: {
      tickInterval: tickInterval,
      categories: trend.timeStamps,
    },
    yAxis: [
      {
        title: {
          text: trend.tagUnits,
          style: {
            color: '#222222',
          },
        },
        labels: {
          style: {
            color: '#222222',
          },
        },
        min: trend.yAxisMin,
        max: trend.yAxisMax,
      },
    ],

    tooltip: {
      shared: true,
      valueDecimals: 2,
    },
    series: [
      {
        name: 'Actual',
        data: trend.actualValues,
        zIndex: 3,
        lineWidth: 1,
        color: '#1e4e97',

        type: 'line',
        shadow: true,
      },
      {
        name: 'Expected',
        data: trend.expectedValues,
        zIndex: 2,
        color: '#9b2b00',
        lineWidth: 1,
        type: 'line',
        shadow: true,
      },
    ],
  };
  const tCount = trend.expectedRange[0]?.length || 0;
  const uCount = trend.expectedRange[1]?.length || 0;
  const lCount = trend.expectedRange[2]?.length || 0;
  if (
    tCount > 0 &&
    uCount > 0 &&
    lCount > 0 &&
    tCount == uCount &&
    uCount == lCount
  ) {
    const rangeSer: Highcharts.SeriesOptionsType = {
      name: 'Expected Range',
      data: trend.expectedRange,
      type: 'arearange',
      color: '#d47629',
      fillOpacity: 0.3,
      zIndex: 0,
      lineWidth: 0,
      shadow: false,
      marker: {
        enabled: false,
        states: {
          hover: {
            enabled: false,
          },
        },
      },
    };

    chartConfig.series.push(rangeSer);
  }
  chartConfig.tooltip = chartConfig.tooltip || {};
  chartConfig.tooltip.formatter = function () {
    return toolTipFormatter(this);
  };
  chartConfig.tooltip.useHTML = true;
  return chartConfig;
}

function toolTipFormatter(chartObject) {
  const d = moment(getDate(chartObject.x));
  let s = `<div><div>${d.format('dddd, MMMM Do YYYY h:mm a')}</div>`;
  if (chartObject?.points) {
    for (const p of chartObject.points) {
      if (p?.point?.low && p?.point?.high) {
        const low = Math.min(p.point.high, p.point.low);
        const high = Math.max(p.point.high, p.point.low);
        s += `<span style="color:${p.color}">● </span><b>${
          p.series.name
        }: ${getNumberBySigFigs(low, 4)} - ${getNumberBySigFigs(
          high,
          4
        )}</b><br/>`;
      } else {
        s += `<span style="color:${p.color}">● </span><b>${
          p.series.name
        }: ${getNumberBySigFigs(p.y, 4)}</b><br/>`;
      }
    }
  }
  s += '</div>';
  return s;
}

export function modelTrendDiscontinuity(Highcharts) {
  const H = Highcharts;
  H.wrap(
    H.Axis.prototype,
    'getLinePath',
    function (proceed: any, lineWidth: number) {
      // For Highcharts / Angular interop we need a reference to this.
      // See: https://stackoverflow.com/questions/63672410/extending-highcharts-in-angular/63682392#63682392
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      const axis = this;
      const brokenAxis = axis.brokenAxis;
      // Run the original proceed method
      const path = proceed.call(this, lineWidth);
      const start = path[0];
      let x = start[1];
      let y = start[2];

      (brokenAxis.breakArray || []).forEach(
        (brk: { from: number; to: number; len: number }) => {
          if (axis.horiz) {
            x = axis.toPixels(brk.from);
            path.splice(
              1,
              0,
              ['L', x - 4, y],
              ['M', x - 9, y + 5],
              ['L', x + 1, y - 5],
              ['M', x - 1, y + 5],
              ['L', x + 9, y - 5],
              ['M', x + 4, y]
            );
          } else {
            y = axis.toPixels(brk.from);
            path.splice(
              1,
              0,
              ['L', x, y - 4],
              ['M', x + 5, y - 9],
              ['L', x - 5, y + 1],
              ['M', x + 5, y - 1],
              ['L', x - 5, y + 9],
              ['M', x, y + 4]
            );
          }
        }
      );
      return path;
    }
  );
}
