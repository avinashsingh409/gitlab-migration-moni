import { IDonutState, IComponentDonutState } from '../models/donut-chart-state';
import { IDonutData } from '../models/donut-data';

export function convertToComponentData(
  xDonutState: IDonutState
): IComponentDonutState {
  return {
    title: xDonutState.title,
    subtitle: xDonutState.subtitle,
    legendWidthLimit: xDonutState.legendWidthLimit,
    toolTipFormat: xDonutState.toolTipFormat,
    currentDonutData: xDonutState.AllDonutData.slice(
      (xDonutState.drilldownSection - 1) * 5,
      xDonutState.drilldownSection * 5
    ).concat(getOthersSection(xDonutState)),
    canRefresh: xDonutState.drilldownSection !== 1,
  };
}

export function getOthersSection(xDonutState: IDonutState): IDonutData[] {
  if (xDonutState.AllDonutData.length - xDonutState.drilldownSection * 5 <= 0) {
    return [];
  } else {
    return [
      {
        name: 'Others',
        id: 'Others' + xDonutState.IDappendedToOthers,
        y: xDonutState.AllDonutData.slice(
          xDonutState.drilldownSection * 5,
          xDonutState.AllDonutData.length
        )
          .map((x) => x.y)
          .reduce((a, b) => a + b, 0),
      },
    ];
  }
}

// This is used to generate an ID to append to the ID of the "Others" section of the pie chart.
// the reason for this is explained in the type definition of IDonutState
export function makeId(length: number): string {
  let result = '';
  const characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
