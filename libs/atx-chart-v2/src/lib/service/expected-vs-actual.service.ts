import { IModelConfigModelTrend } from '@atonix/shared/api';

export function getExpectedVsActualService(
  trend: IModelConfigModelTrend
): Highcharts.Options {
  const scatterPlotData = [];
  if (trend.actualValues?.length > 0) {
    for (let i = 0; i < trend.actualValues.length; i++) {
      scatterPlotData.push([
        trend.actualValues[i].y,
        trend.expectedValues[i].y,
      ]);
    }
  }
  const scatterPlot: Highcharts.Options = {
    chart: {
      type: 'scatter',
      zoomType: 'xy',
      spacingBottom: 0,
    },
    title: {
      style: {
        display: 'none',
      },
    },
    credits: {
      enabled: false,
    },
    xAxis: {
      title: {
        text: 'Actual',
      },
    },
    yAxis: {
      title: {
        text: 'Predicted',
      },
    },
    series: [
      {
        animation: false,
        name: trend.tagUnits,
        color: '#336699',
        data: scatterPlotData,
        type: 'scatter',
        marker: {
          enabled: true,
          symbol: 'diamond',
        },
        tooltip: {
          pointFormat:
            'Predicted: {point.y} ' +
            trend.tagUnits +
            '<br />' +
            'Actual: {point.x} ' +
            trend.tagUnits,
        },
      },
    ],
    legend: {
      enabled: false,
    },
    tooltip: {
      headerFormat: '',
      pointFormat: '',
      valueDecimals: 2,
    },
  };

  return scatterPlot;
}
