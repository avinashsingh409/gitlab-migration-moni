export default function (Highcharts) {
  const H = Highcharts;
  H.wrap(
    H.Tooltip.prototype,
    'move',
    function (proceed, x, y, anchorX, anchorY) {
      // eslint-disable-next-line @typescript-eslint/no-this-alias
      const tooltip = this,
        now = tooltip.now,
        animate =
          tooltip.options.animation !== false &&
          !tooltip.isHidden &&
          // When we get close to the target position, abort animation and land on the right place (#3056)
          (Math.abs(x - now.x) > 1 || Math.abs(y - now.y) > 1);
      let skipAnchor = tooltip.followPointer || tooltip.len > 1;

      skipAnchor = false;

      // Get intermediate values for animation
      H.extend(now, {
        x: animate ? (2 * now.x + x) / 3 : x,
        y: animate ? (now.y + y) / 2 : y,
        anchorX: skipAnchor
          ? undefined
          : animate
          ? (2 * now.anchorX + anchorX) / 3
          : anchorX,
        anchorY: skipAnchor
          ? undefined
          : animate
          ? (now.anchorY + anchorY) / 2
          : anchorY,
      });

      // Move to the intermediate value
      tooltip.getLabel().attr(now);

      // Run on next tick of the mouse tracker
      if (animate) {
        // Never allow two timeouts
        clearTimeout(this.tooltipTimeout);

        // Set the fixed interval ticking for the smooth tooltip
        this.tooltipTimeout = setTimeout(function () {
          // The interval function may still be running during destroy,
          // so check that the chart is really there before calling.
          if (tooltip) {
            tooltip.move(x, y, anchorX, anchorY);
          }
        }, 32);
      }
    }
  );
}
