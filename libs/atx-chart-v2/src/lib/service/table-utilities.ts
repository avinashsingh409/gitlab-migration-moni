import { IProcessedTrend } from '@atonix/atx-core';
import { LabelIndex } from '../models/label-index';
import {
  createId,
  decimalFormatter,
  getArchive,
  getTagNumber,
} from './chart.service';
import { createHighChartSummaryTableColumnOrBar } from './summary-chart-utilities';
import { toPrecisionCustom } from './utilities';

export function createTableDataSource(
  myTrend: IProcessedTrend,
  isGroupedSeries: boolean,
  theme: string,
  labelIndex: LabelIndex
): { columns: string[]; data: any[]; highchartsOptions: Highcharts.Options } {
  if (labelIndex === LabelIndex.NoLegend) {
    labelIndex = LabelIndex.SeriesName; // for tables, no legend is not an option
  }

  const baseSumInfo = createHighChartSummaryTableColumnOrBar(
    myTrend,
    isGroupedSeries,
    theme,
    labelIndex as number
  );

  const measurements = myTrend.measurements;

  const columns = ['Series'];
  columns.push('Aggregation');
  columns.push('Units');
  if (baseSumInfo.hasNonPin) {
    columns.push('Values');
  }

  // Array containing the ids of each pin
  const pinList = [];

  for (const pin of myTrend.trendDefinition.Pins) {
    if (!pin.Hidden) {
      columns.push(pin.Name.replace(/</g, '_').replace(/>/g, '_'));
      pinList.push(pin.PDTrendPinID);
    }
  }

  const measurementValuesByTagPinId: any[] = [];

  if (measurements) {
    measurements.Measurements.map((m) => {
      if (m.Data && m.Data.length > 0) {
        measurementValuesByTagPinId[
          createId(m.Tag.PDTagID, m.PinID, m.Archive)
        ] = m.Data[0].Value;
      }
    });
  }

  // We now have an ordered list of pins, an ordered list of tags and a way to get the series from the pins and tags
  const data = baseSumInfo.seriesSettings.map((setting) => {
    const tagId = getTagNumber(setting.seriesOrder);
    const archive = getArchive(setting.seriesOrder);
    const row = [baseSumInfo.seriesDict[setting.seriesOrder]];
    row.push(baseSumInfo.summaryTypesDict[setting.seriesOrder]);
    row.push(
      findUnits(setting.seriesAxis, baseSumInfo.highchartsOptions.yAxis)
    );
    if (baseSumInfo.hasNonPin) {
      const v = measurementValuesByTagPinId[setting.seriesOrder];
      if (v) {
        row.push(toPrecisionCustom(v, 4));
      } else {
        row.push(
          toPrecisionCustom(baseSumInfo.valuesDict[setting.seriesOrder], 4)
        );
      }
    }
    const pinData = pinList.map((pin) => {
      const id = createId(tagId, pin, archive);
      const v = measurementValuesByTagPinId[id];
      if (v) {
        return v;
      } else {
        return baseSumInfo.valuesDict[id];
      }
    });
    return [...row, ...pinData];
  });

  return { columns, data, highchartsOptions: baseSumInfo.highchartsOptions };
}

export function findUnits(
  yAxis: string | number,
  axes: Highcharts.YAxisOptions | Highcharts.YAxisOptions[]
) {
  if (Array.isArray(axes)) {
    const ax = axes.find((n) => n.id === yAxis);
    return ax ? ax.title.text : '';
  } else {
    return axes.id === yAxis ? axes.title.text : '';
  }
}
