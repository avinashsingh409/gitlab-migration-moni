import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { PrivateApiAlertsCoreService } from '@atonix/shared/api';
import { ToastService } from '@atonix/shared/utils';
import { take, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ChartOpModeService {
  constructor(
    private alertsService: PrivateApiAlertsCoreService,
    private toastService: ToastService,
    private router: Router
  ) {}

  //creates Exclusion Period Op Mode for
  createOpMode(
    opModeDefinitionTitle: string,
    logicStartDate: Date,
    logicEndDate: Date,
    associations: any[]
  ) {
    const opModeDefinition = {
      opModeDefinitionTitle,
      opModeTypeID: 5,
      assetTagMaps: associations,
      startCriteria: [
        {
          criteriaGroupID: -1,
          displayOrder: 0,
          isStart: true,
          logic: [
            {
              logicStartDate,
              logicEndDate,
              assetVariableTypeTagMapID: -1,
              logicOperatorTypeID: 3,
              value: 0,
            },
          ],
        },
      ],
    };

    return this.alertsService.saveOpMode(opModeDefinition).pipe(
      tap({
        next: () => {
          this.toastService.openSnackBar('Op Mode Created', 'success', 1000);
        },
        error: (e: unknown) => {
          const message = (e as HttpErrorResponse).error.message;
          this.toastService.openSnackBar(message, 'error', 1000);
        },
      })
    );
  }

  openOpMode(opModeDefinitionExtID: string, assetGuid: string) {
    const baseUrl = window.location.href.replace(this.router.url, '');
    let params = this.router.url.replace(/^.*\?/, '').split('&');
    params = params.map((param) => {
      if (param.slice(0, 3) == 'id=' && assetGuid) {
        return 'id=' + assetGuid;
      } else return param;
    });

    const route =
      '/model-config?' +
      params
        .join('&')
        .concat('&tab=opmode')
        .concat(`&opmode=${opModeDefinitionExtID}`);

    window.open(baseUrl + route, '_blank');
  }
}
