import { TestBed } from '@angular/core/testing';
import { HighchartsService } from './highcharts.service';
import { AuthService } from '@atonix/shared/state/auth';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { createMockWithValues } from '@testing-library/angular/jest-utils';

describe('HighchartsService', () => {
  let service: HighchartsService;
  let mockAuthService: AuthService;
  beforeEach(() => {
    mockAuthService = createMockWithValues(AuthService, {
      GetIdToken: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
      ],
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HighchartsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
