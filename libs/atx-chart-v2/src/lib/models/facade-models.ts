import { IProcessedTrend } from '@atonix/atx-core';
import { Options } from 'highcharts';

export interface LoadDataExplorerCharts {
  nodeID: string;
  trendID: string;
  startDate?: Date;
  endDate?: Date;
}

export interface RefreshDataExplorerCharts {
  nodeID: string;
  newTrendID: string;
  oldTrendID: string;
  newTrendName: string;
  startDate?: Date;
  endDate?: Date;
  trends: IProcessedTrend[];
  isDirty: Record<string, boolean>;
  dirtyTrends: Map<string, IProcessedTrend>;
}

export interface RefreshStandardChart {
  trends: IProcessedTrend[];
  selectedTrend: IProcessedTrend;
  nodeID: string;
  startDate?: Date;
  endDate?: Date;
  isDirty: Record<string, boolean>;
  dirtyTrends: Map<string, IProcessedTrend>;
}

export interface LoadDataExplorerTrends {
  trendID: string;
  trend: IProcessedTrend;
  startDate: Date;
  endDate: Date;
  archive: string;
}

export interface SetDirty {
  trendID: string;
  trend: IProcessedTrend;
  startDate: Date;
  endDate: Date;
  archive: string;
}

export interface TrendsLoaded {
  isDirty: Record<string, boolean>;
  selectedTrendID: string;
  uniqueID: string;
  start: Date;
  end: Date;
  trends: IProcessedTrend[];
}
