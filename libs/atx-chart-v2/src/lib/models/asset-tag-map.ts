//copy of assetTagMap model present in opmodes
export interface AssetTagMap {
  assetAbbrev?: string; // null for tags
  assetGuid?: string; // null for tags
  opModeDefinitionAssetTagMapID: number;
  assetVariableTypeTagMapID?: number; // null for assets
  include: boolean;
  bringDescendants: boolean;
  tagDesc?: string; // null for assets
  tagName?: string; // null for assets
}
