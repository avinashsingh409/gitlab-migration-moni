import { ISummaryType } from '@atonix/atx-core';

export const SummaryTypes: ISummaryType[] = [
  {
    SummaryTypeID: 0,
    SummaryDesc: 'None Selected',
    SummaryAbbrev: 'None',
    DisplayOrder: 0,
  },
  {
    SummaryTypeID: 1,
    SummaryDesc: 'Minimum Value Over Range',
    SummaryAbbrev: 'Min',
    DisplayOrder: 1,
  },
  {
    SummaryTypeID: 2,
    SummaryDesc: 'Maximum Value Over Range',
    SummaryAbbrev: 'Max',
    DisplayOrder: 2,
  },
  {
    SummaryTypeID: 3,
    SummaryDesc: 'Average Value Over Range',
    SummaryAbbrev: 'Average',
    DisplayOrder: 3,
  },
  {
    SummaryTypeID: 4,
    SummaryDesc: 'First Value In Range (Ignore Status)',
    SummaryAbbrev: 'First',
    DisplayOrder: 4,
  },
  {
    SummaryTypeID: 5,
    SummaryDesc: 'Last Value In Range (Ignore Status)',
    SummaryAbbrev: 'Last',
    DisplayOrder: 5,
  },
  {
    SummaryTypeID: 6,
    SummaryDesc: 'Sum of Values In Range',
    SummaryAbbrev: 'Sum',
    DisplayOrder: 6,
  },
  {
    SummaryTypeID: 9,
    SummaryDesc: 'Median Value Over Range',
    SummaryAbbrev: 'Median',
    DisplayOrder: 9,
  },
  {
    SummaryTypeID: 10,
    SummaryDesc: 'Mode Value Over Range',
    SummaryAbbrev: 'Mode',
    DisplayOrder: 10,
  },
  {
    SummaryTypeID: 11,
    SummaryDesc: 'First Value In Range (Honor Status)',
    SummaryAbbrev: 'FirstHonorBadStatus',
    DisplayOrder: 11,
  },
  {
    SummaryTypeID: 12,
    SummaryDesc: 'Last Value In Range (Honor Status)',
    SummaryAbbrev: 'LastHonorBadStatus',
    DisplayOrder: 12,
  },
  {
    SummaryTypeID: 13,
    SummaryDesc: 'Delta',
    SummaryAbbrev: 'Delta',
    DisplayOrder: 13,
  },
  {
    SummaryTypeID: 14,
    SummaryDesc: 'First Standard Deviation Above',
    SummaryAbbrev: 'FirstStdAbove',
    DisplayOrder: 14,
  },
  {
    SummaryTypeID: 15,
    SummaryDesc: 'Second Standard Deviation Above',
    SummaryAbbrev: 'SecondStdAbove',
    DisplayOrder: 15,
  },
  {
    SummaryTypeID: 16,
    SummaryDesc: 'First Standard Deviation Below',
    SummaryAbbrev: 'FirstStdBelow',
    DisplayOrder: 16,
  },
  {
    SummaryTypeID: 17,
    SummaryDesc: 'Second Standard Deviation Below',
    SummaryAbbrev: 'SecondStdBelow',
    DisplayOrder: 17,
  },
  {
    SummaryTypeID: 18,
    SummaryDesc: 'Percentile',
    SummaryAbbrev: 'Percentile',
    DisplayOrder: 18,
  },
];
