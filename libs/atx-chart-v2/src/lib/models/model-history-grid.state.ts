import { IModelHistoryFilters, INDModelActionItem } from '@atonix/shared/api';

export interface IModelHistoryEventStateChange {
  event: 'setfavorite' | 'setnote' | 'setmodelfilters';
  value?: boolean | string | IModelHistoryFilters;
  modelAction?: INDModelActionItem;
}

export function setFavorite(
  isFavorite: boolean,
  modelAction: INDModelActionItem
) {
  const result: IModelHistoryEventStateChange = {
    modelAction: modelAction,
    event: 'setfavorite',
    value: isFavorite,
  };

  return result;
}

export function setNote(note: string, modelAction: INDModelActionItem) {
  const result: IModelHistoryEventStateChange = {
    modelAction: modelAction,
    event: 'setnote',
    value: note,
  };

  return result;
}

export function setModelHistoryFilters(
  modelHistoryFilters: IModelHistoryFilters
) {
  const result: IModelHistoryEventStateChange = {
    event: 'setmodelfilters',
    value: modelHistoryFilters,
  };

  return result;
}
