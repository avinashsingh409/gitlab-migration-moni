import {
  DesignCurve,
  IPDTrendAxis,
  IPDTrendBandAxis,
  IPDTrendBandAxisBar,
  IPDTrendBandAxisMeasurement,
  IPDTrendPin,
  IPDTrendSeries,
  IGSMeasurement,
} from '@atonix/atx-core';
import { IAdvancedOptions } from './advanced-options';

export interface ChartFormElements {
  id: string;
  series: IPDTrendSeries[];
  groupedSeriesAxis: IPDTrendBandAxis;
  groupedSeriesBars: IPDTrendBandAxisBar[];
  groupedSeries: IGSMeasurement[];
  axes: IPDTrendAxis[];
  pins: IPDTrendPin[];
  curves: DesignCurve[];
  advanced: IAdvancedOptions;
  title: string;
  isXY: boolean;
}
