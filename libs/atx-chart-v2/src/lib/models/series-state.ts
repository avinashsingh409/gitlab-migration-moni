export interface SeriesState {
  seriesType: RequestStringState;
  aggregationType1: RequestState;
  aggregationType2: RequestState;
  aggregationType3: RequestState;
  aggregationType4: RequestState;
  aggregationType5: RequestState;
  symbol: RequestStringState;
  dataSource1: RequestState;
  dataSource2: RequestState;
  dataSource3: RequestState;
  dataSource4: RequestState;
  dataSource5: RequestState;
  outlierType: RequestStringState;
  value1: RequestState;
  value2: RequestState;
  value3: RequestState;
  value4: RequestState;
  value5: RequestState;
  params1: RequestState;
  params2: RequestState;
  params3: RequestState;
  params4: RequestState;
  params5: RequestState;
  tagID1: RequestState;
  tagID2: RequestState;
  tagID3: RequestState;
  tagID4: RequestState;
  tagID5: RequestState;
  groupName: RequestStringState;
}

export enum DataSourceValues {
  Tag = 0,
  Constant = 1,
}

export interface RequestStringState {
  previousValue: string | null;
  currentValue: string | null;
  valueChanged: boolean;
}
export interface RequestState {
  previousValue: number | null;
  currentValue: number | null;
  valueChanged: boolean;
}
