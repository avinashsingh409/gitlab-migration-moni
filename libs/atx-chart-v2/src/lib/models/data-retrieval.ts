export enum DataRetrievalType {
  Default = 1,
  Archive = 2,
  MinDataInterval = 3,
}
export const DataRetrievalTypeMapping = {
  [DataRetrievalType.Default]: 'Use Default',
  [DataRetrievalType.Archive]: 'Archive Selection',
  [DataRetrievalType.MinDataInterval]: 'Minimum Data Interval',
};
export interface DataRetrieval {
  method: DataRetrievalType;
  description: string;
  archive: string;
  allArchives: string[];
  minInterval: number;
  minIntervalUnits: string;
}
