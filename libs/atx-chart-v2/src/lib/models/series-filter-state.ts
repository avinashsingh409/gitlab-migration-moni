export interface SeriesFilterState {
  filterMin: RequestStringNumberState;
  filterMax: RequestStringNumberState;
  flatlineThreshold: RequestNumberState;
  applyFilterToAll: RequestBooleanState;
  isFilterActive: boolean;
}

export interface RequestStringNumberState {
  previousValue: string | number;
  currentValue: string | number;
  valueChanged: boolean;
}
export interface RequestNumberState {
  previousValue: number;
  currentValue: number;
  valueChanged: boolean;
}

export interface RequestBooleanState {
  previousValue: boolean;
  currentValue: boolean;
  valueChanged: boolean;
}
