import { IDonutData } from './donut-data';
import { IDataFormatOption } from './data-format-option';

// all the data for the chart, including data that is not currently displayed in the drill down.
export interface IDonutState extends IPreDonutChartState {
  AllDonutData: IDonutData[];
  // this is a 5 character string appended to the ID of the 'Others' partition
  // It is created by the makeId function
  // the reason we need an ID appended to the the "Others" section is that if the Id remains
  // the same before and after a drilldown, highcharts assumes it is the same data, so it puts
  // the "others" section first instead of last. So we needed to make a new ID for each
  // time we drill down.
  IDappendedToOthers: string;
  // this shows the position in the drilldown into "Others".  It starts at 1 and increases each
  // time "Others" is hit. It goes back to 1 when refreshed.
  drilldownSection: number;
}

// This interface is extended by INgrxDonutState and IComponentDonutState.
export interface IPreDonutChartState {
  title: string;
  subtitle: string;
  legendWidthLimit: number;
  toolTipFormat: IDataFormatOption;
}

// This interface holds the data for the donut chart.
// the currentDonutData property holds the data that is currently displayed in the chart.
export interface IComponentDonutState extends IPreDonutChartState {
  currentDonutData: IDonutData[];
  canRefresh: boolean;
}

export function createDefaultDonutState(newValues?: Partial<IDonutState>) {
  const result: IDonutState = {
    ...{
      AllDonutData: [],
      IDappendedToOthers: null,
      drilldownSection: 1,
      title: null,
      subtitle: null,
      legendWidthLimit: null,
      toolTipFormat: null,
    },
    ...newValues,
  };
  return result;
}
