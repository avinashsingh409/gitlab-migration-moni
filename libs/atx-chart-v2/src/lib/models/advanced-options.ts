import { IPDTrendFilter } from '@atonix/atx-core';

export interface IAdvancedOptions {
  filter: IPDTrendFilter;
  dataRetrievalMethod: number; // 0 = default, 1 = archive name, 2 = interval & units
  dataRetrievalMinInterval: number;
  dataRetrievalMinIntervalUnits: string;
  dataRetrievalArchive: string;
}
