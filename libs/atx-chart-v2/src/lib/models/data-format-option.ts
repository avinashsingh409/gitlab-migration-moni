export interface IDataFormatOption {
  currency?: string;
  decimalPlaces: number;
  isRounded: boolean;
  locale?: string;
  format?: string;
}
