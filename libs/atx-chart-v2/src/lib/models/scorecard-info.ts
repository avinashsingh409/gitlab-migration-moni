export interface IStatusCount {
  Status: string;
  StatusID: number;
  Count: number;
  Percent: number;
}

export interface ICategoryCount {
  Category: string;
  CategoryID: number;
  Count: number;
}

export interface ImpactCost {
  AssetIssueImpactType: IAssetIssueImpactType;
  Impact: number;
  Stoplight: number;
}

export interface IAssetIssueImpactType {
  AssetIssueImpactTypeID: number;
  AssetIssueImpactCategoryTypeID: number;
  AssetIssueImpactTypeDesc: string;
  AssetIssueImpactCategoryTypeDesc: string;
  AssetIssueImpactCategoryTypeCalcDesc: string;
  AssetIssueImpactCategoryAndTypeDesc: string;
  Units: string;
  DisplayOrder: number;
}
