export enum LabelIndex {
  SeriesName = 0,
  TagName = 1,
  TagDescription = 2,
  TagNameDescription = 3,
  VariableType = 4,
  NoLegend = 5,
}
export const LabelIndexMapping = {
  [LabelIndex.SeriesName]: 'Series Name',
  [LabelIndex.TagName]: 'Tag Name',
  [LabelIndex.TagDescription]: 'Tag Description',
  [LabelIndex.TagNameDescription]: '[Tag Name] Tag Description',
  [LabelIndex.VariableType]: 'Variable Type',
  [LabelIndex.NoLegend]: 'No Legend',
};
