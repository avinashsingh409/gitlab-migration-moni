import { IUpdateLimitsData } from './update-limits-data';

export interface IBtnGrpStateChange {
  event:
    | 'ChangeLabels'
    | 'EditChart'
    | 'UpdateLimits'
    | 'ResetLimits'
    | 'LegendItemToggle'
    | 'DataCursorToggle';
  newValue?: boolean | string | number | IUpdateLimitsData;
  newConfig?: Highcharts.Options;
}
