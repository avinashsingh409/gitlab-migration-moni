export enum PinType {
  Unmodified = 1,
  NoGaps = 2,
  Overlay = 3,
}
export const PinTypeMapping = {
  [PinType.Unmodified]: 'Unmodified',
  [PinType.NoGaps]: 'No Gaps',
  [PinType.Overlay]: 'Overlay',
};

export interface FormPin {
  pinType: string;
  pinID: number;
  startDate: Date;
  startTime: string;
  endDate: Date;
  endTime: string;
}
