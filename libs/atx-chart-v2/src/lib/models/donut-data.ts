export interface IDonutData {
  id?: string;
  name: string; // label of the item
  y: number; // value of the item,
  legendIndex?: number;
}
