import { ICriteria, IProcessedTrend } from '@atonix/atx-core';
import { ComponentStore } from '@ngrx/component-store';
import { ChartFormElements } from '../models/chart-form-elements';
import { DataRetrieval } from '../models/data-retrieval';
import { LabelIndex } from '../models/label-index';
import { FormPin, PinType } from '../models/pin-types';

export interface ChartState {
  assetID: number; // needed for legacy save call
  assetGuid: string;
  trends: IProcessedTrend[];
  isDirty: Record<string, boolean>;
  dirtyTrends: Map<string, IProcessedTrend>;
  selectedTrend: IProcessedTrend;
  uniqueID: string;
  formElements: ChartFormElements;
  timeFilters: ICriteria[];
  pinNames: string[];
  selectedTrendID: string;
  isGroupedSeries: boolean;
  isXY: boolean; // chart has at least one X-axis series *can be scatter plot
  pinTypeID: PinType;
  pins: FormPin[];
  dataRetrievalMethod: DataRetrieval;
  labelIndex: LabelIndex;
  trendsLoading: boolean;
  loadingData: boolean;
  errorNoData: boolean;
  chartError: string;
  chartHeight: number;
  start: Date;
  end: Date;
}
export const initialChartState: ChartState = {
  assetID: null,
  assetGuid: null,
  trends: null,
  isDirty: {},
  dirtyTrends: new Map<string, null>(),
  selectedTrendID: null,
  isGroupedSeries: false,
  isXY: false,
  selectedTrend: null,
  uniqueID: null,
  formElements: null,
  timeFilters: null,
  pinNames: null,
  pinTypeID: null,
  pins: null,
  labelIndex: LabelIndex.SeriesName,
  dataRetrievalMethod: null,
  trendsLoading: true,
  loadingData: true,
  errorNoData: false,
  chartError: null,
  chartHeight: null,
  start: null,
  end: null,
};
export class ChartQuery {
  constructor(private store: ComponentStore<ChartState>) {}
  readonly assetID$ = this.store.select((state) => state.assetID, {
    debounce: true,
  });
  readonly assetGuid$ = this.store.select((state) => state.assetGuid, {
    debounce: true,
  });
  readonly trends$ = this.store.select((state) => state.trends, {
    debounce: true,
  });
  readonly dirtyTrends$ = this.store.select((state) => state.dirtyTrends, {
    debounce: true,
  });
  readonly selectedTrend$ = this.store.select((state) => state.selectedTrend, {
    debounce: true,
  });
  readonly selectedTrendID$ = this.store.select(
    (state) => state.selectedTrendID,
    {
      debounce: true,
    }
  );
  readonly formElements$ = this.store.select((state) => state.formElements, {
    debounce: true,
  });
  readonly axesFormElement$ = this.store.select(
    (state) => state?.formElements?.axes || [],
    {
      debounce: true,
    }
  );
  readonly pinTypeID$ = this.store.select((state) => state.pinTypeID, {
    debounce: true,
  });
  readonly trendsLoading$ = this.store.select((state) => state.trendsLoading, {
    debounce: true,
  });
  readonly labelIndex$ = this.store.select((state) => state.labelIndex);
  readonly loadingData$ = this.store.select((state) => state.loadingData, {
    debounce: true,
  });
  private readonly start$ = this.store.select((state) => state.start, {
    debounce: true,
  });
  private readonly end$ = this.store.select((state) => state.end, {
    debounce: true,
  });
  readonly errorNoData$ = this.store.select((state) => state.errorNoData, {
    debounce: true,
  });
  readonly dataRetrievalMethod$ = this.store.select(
    (state) => state.dataRetrievalMethod,
    {
      debounce: true,
    }
  );
  readonly uniqueID$ = this.store.select((state) => state.uniqueID, {
    debounce: true,
  });
  readonly chartError$ = this.store.select((state) => state.chartError, {
    debounce: true,
  });
  readonly isDirty$ = this.store.select((state) => state.isDirty, {
    debounce: true,
  });
  readonly selectedTrendSeries$ = this.store.select(
    (state) => state?.selectedTrend?.trendDefinition?.Series || [],
    {
      debounce: true,
    }
  );
  readonly timeFilters$ = this.store.select((state) => state.timeFilters, {
    debounce: true,
  });
  readonly isXY$ = this.store.select((state) => state.isXY, { debounce: true });
  readonly chartHeight$ = this.store.select((state) => state.chartHeight, {
    debounce: true,
  });
  readonly isGroupedSeries$ = this.store.select(
    (state) => state.isGroupedSeries,
    {
      debounce: true,
    }
  );
  readonly selectedTrendAxes$ = this.store.select(
    (state) => state?.selectedTrend?.trendDefinition?.Axes || [],
    {
      debounce: true,
    }
  );
  readonly selectedTrendNewSeriesCount$ = this.store.select(
    (state) => {
      const newSeries = state?.selectedTrend?.trendDefinition?.Series.filter(
        (s) => s.PDTrendSeriesID === null
      );
      if (newSeries?.length > 0) {
        return newSeries.length;
      } else {
        return 0;
      }
    },
    {
      debounce: true,
    }
  );
  readonly selectedTrendGroupSeriesCount$ = this.store.select(
    (state) => {
      return state?.selectedTrend?.groupSeriesMeasurements.length ?? 0;
    },
    {
      debounce: true,
    }
  );
  readonly pinNames$ = this.store.select(
    (state) => {
      const pinNames = [];
      if (state?.pins && state.timeFilters) {
        state.pins.forEach((pin) => {
          let pinName = pin.pinType;
          if (pin.pinID > 0) {
            const timeFilter = state.timeFilters.find(
              (filter) => filter.CoID === pin.pinID
            );
            if (timeFilter) {
              pinName = timeFilter.CoTitle;
            }
          }
          pinNames.push(pinName);
        });
      }
      return pinNames;
    },
    { debounce: true }
  );

  readonly selectedTrendSeriesAxes$ = this.store.select(
    this.selectedTrend$,
    this.selectedTrendSeries$,
    this.selectedTrendAxes$,
    this.start$,
    this.end$,
    (
      selectedTrend,
      selectedTrendSeries,
      selectedTrendAxes,
      start,
      end
    ): any => {
      return {
        selectedTrend,
        selectedTrendSeries,
        selectedTrendAxes,
        start,
        end,
      };
    }
  );
  readonly pins$ = this.store.select((state) => state.pins, {
    debounce: true,
  });

  readonly vm$ = this.store.select(
    this.assetID$,
    this.assetGuid$,
    this.trends$,
    this.isDirty$,
    this.dirtyTrends$,
    this.selectedTrend$,
    this.selectedTrendID$,
    this.isGroupedSeries$,
    this.isXY$,
    this.uniqueID$,
    this.formElements$,
    this.timeFilters$,
    this.pinNames$,
    this.trendsLoading$,
    this.labelIndex$,
    this.loadingData$,
    this.pinTypeID$,
    this.pins$,
    this.dataRetrievalMethod$,
    this.chartError$,
    this.chartHeight$,
    this.start$,
    this.end$,
    this.errorNoData$,
    (
      assetID,
      assetGuid,
      trends,
      isDirty,
      dirtyTrends,
      selectedTrend,
      selectedTrendID,
      isGroupedSeries,
      isXY,
      uniqueID,
      formElements,
      timeFilters,
      pinNames,
      trendsLoading,
      labelIndex,
      loadingData,
      pinTypeID,
      pins,
      dataRetrievalMethod,
      chartError,
      chartHeight,
      start,
      end,
      errorNoData
    ): ChartState => {
      return {
        assetID,
        assetGuid,
        trends,
        isDirty,
        dirtyTrends,
        selectedTrend,
        selectedTrendID,
        isGroupedSeries,
        isXY,
        uniqueID,
        formElements,
        timeFilters,
        pinNames,
        trendsLoading,
        labelIndex,
        loadingData,
        pinTypeID,
        pins,
        dataRetrievalMethod,
        chartError,
        chartHeight,
        start,
        end,
        errorNoData,
      };
    }
  );
}
