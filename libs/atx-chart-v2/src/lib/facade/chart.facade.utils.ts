import { formatDate } from '@angular/common';
import moment from 'moment';
import {
  GroupedSeriesType,
  IPDTrendAxis,
  IPDTrendBandAxisBar,
  IPDTrendBandAxisMeasurement,
  IPDTrendFilter,
  IPDTrendSeries,
  IProcessedTrend,
} from '@atonix/atx-core';

export const hexColors = {
  // from https://css-tricks.com/snippets/css/named-colors-and-hex-equivalents/
  aliceblue: '#f0f8ff',
  antiquewhite: '#faebd7',
  aqua: '#00ffff',
  aquamarine: '#7fffd4',
  azure: '#f0ffff',
  beige: '#f5f5dc',
  bisque: '#ffe4c4',
  black: '#000000',
  blanchedalmond: '#ffebcd',
  blue: '#0000ff',
  blueviolet: '#8a2be2',
  brown: '#a52a2a',
  burlywood: '#deb887',
  cadetblue: '#5f9ea0',
  chartreuse: '#7fff00',
  chocolate: '#d2691e',
  coral: '#ff7f50',
  cornflowerblue: '#6495ed',
  cornsilk: '#fff8dc',
  crimson: '#dc143c',
  cyan: '#00ffff',
  darkblue: '#00008b',
  darkcyan: '#008b8b',
  darkgoldenrod: '#b8860b',
  darkgray: '#a9a9a9',
  darkgreen: '#006400',
  darkkhaki: '#bdb76b',
  darkmagenta: '#8b008b',
  darkolivegreen: '#556b2f',
  darkorange: '#ff8c00',
  darkorchid: '#9932cc',
  darkred: '#8b0000',
  darksalmon: '#e9967a',
  darkseagreen: '#8fbc8f',
  darkslateblue: '#483d8b',
  darkslategray: '#2f4f4f',
  darkturquoise: '#00ced1',
  darkviolet: '#9400d3',
  deeppink: '#ff1493',
  deepskyblue: '#00bfff',
  dimgray: '#696969',
  dodgerblue: '#1e90ff',
  firebrick: '#b22222',
  floralwhite: '#fffaf0',
  forestgreen: '#228b22',
  fuchsia: '#ff00ff',
  gainsboro: '#dcdcdc',
  ghostwhite: '#f8f8ff',
  gold: '#ffd700',
  goldenrod: '#daa520',
  gray: '#808080',
  green: '#008000',
  greenyellow: '#adff2f',
  honeydew: '#f0fff0',
  hotpink: '#ff69b4',
  indianred: '#cd5c5c',
  indigo: '#4b0082',
  ivory: '#fffff0',
  khaki: '#f0e68c',
  lavender: '#e6e6fa',
  lavenderblush: '#fff0f5',
  lawngreen: '#7cfc00',
  lemonchiffon: '#fffacd',
  lightblue: '#add8e6',
  lightcoral: '#f08080',
  lightcyan: '#e0ffff',
  lightgoldenrodyellow: '#fafad2',
  lightgrey: '#d3d3d3',
  lightgreen: '#90ee90',
  lightpink: '#ffb6c1',
  lightsalmon: '#ffa07a',
  lightseagreen: '#20b2aa',
  lightskyblue: '#87cefa',
  lightslategray: '#778899',
  lightsteelblue: '#b0c4de',
  lightyellow: '#ffffe0',
  lime: '#00ff00',
  limegreen: '#32cd32',
  linen: '#faf0e6',
  magenta: '#ff00ff',
  maroon: '#800000',
  mediumaquamarine: '#66cdaa',
  mediumblue: '#0000cd',
  mediumorchid: '#ba55d3',
  mediumpurple: '#9370d8',
  mediumseagreen: '#3cb371',
  mediumslateblue: '#7b68ee',
  mediumspringgreen: '#00fa9a',
  mediumturquoise: '#48d1cc',
  mediumvioletred: '#c71585',
  midnightblue: '#191970',
  mintcream: '#f5fffa',
  mistyrose: '#ffe4e1',
  moccasin: '#ffe4b5',
  navajowhite: '#ffdead',
  navy: '#000080',
  oldlace: '#fdf5e6',
  olive: '#808000',
  olivedrab: '#6b8e23',
  orange: '#ffa500',
  orangered: '#ff4500',
  orchid: '#da70d6',
  palegoldenrod: '#eee8aa',
  palegreen: '#98fb98',
  paleturquoise: '#afeeee',
  palevioletred: '#d87093',
  papayawhip: '#ffefd5',
  peachpuff: '#ffdab9',
  peru: '#cd853f',
  pink: '#ffc0cb',
  plum: '#dda0dd',
  powderblue: '#b0e0e6',
  purple: '#800080',
  rebeccapurple: '#663399',
  red: '#ff0000',
  rosybrown: '#bc8f8f',
  royalblue: '#4169e1',
  saddlebrown: '#8b4513',
  salmon: '#fa8072',
  sandybrown: '#f4a460',
  seagreen: '#2e8b57',
  seashell: '#fff5ee',
  sienna: '#a0522d',
  silver: '#c0c0c0',
  skyblue: '#87ceeb',
  slateblue: '#6a5acd',
  slategray: '#708090',
  snow: '#fffafa',
  springgreen: '#00ff7f',
  steelblue: '#4682b4',
  tan: '#d2b48c',
  teal: '#008080',
  thistle: '#d8bfd8',
  tomato: '#ff6347',
  turquoise: '#40e0d0',
  violet: '#ee82ee',
  wheat: '#f5deb3',
  white: '#ffffff',
  whitesmoke: '#f5f5f5',
  yellow: '#ffff00',
  yellowgreen: '#9acd32',
};

export function updateTrendAxes(trends: IProcessedTrend[]) {
  const newTrends = trends.map((trend) => {
    const trendDefinition = { ...trend.trendDefinition };
    const Axes = [...trendDefinition.Axes].map((axis) => {
      const newAxis = { ...axis };
      newAxis.OriginalMin = newAxis.Min;
      newAxis.OriginalMax = newAxis.Max;
      return newAxis;
    });
    return {
      ...trend,
      trendDefinition: {
        ...trendDefinition,
        Axes,
      },
    };
  });
  return newTrends;
}

export function updateTrendDefinition(trends: IProcessedTrend[]) {
  const newTrends = trends.map((trend) => {
    const defaultDataRetrieval = {
      Method: 0, //0 = default, 1 = archive name, 2 = interval & units
      MinInterval: 1,
      MinIntervalUnits: 'minutes',
      Archive: '',
      Period: 0,
    };
    const trendDefinition = { ...trend.trendDefinition };
    trendDefinition.DataRetrieval = trendDefinition.DataRetrieval
      ? trendDefinition.DataRetrieval
      : defaultDataRetrieval;
    const Axes = [...trendDefinition.Axes].map((axis) => {
      const newAxis = { ...axis };
      newAxis.OriginalMin = newAxis.Min;
      newAxis.OriginalMax = newAxis.Max;
      return newAxis;
    });
    return {
      ...trend,
      isNew: false,
      trendDefinition: {
        ...trendDefinition,
        Axes,
      },
    };
  });
  return newTrends;
}

export function getSelectedTrendID(trends: IProcessedTrend[]) {
  let result: IProcessedTrend = null;
  if (trends && trends.length > 0) {
    result = trends.filter((n) => n.totalSeries > 0)[0] || trends[0];
  }
  return result?.id;
}

export function findTrend(
  trendID: string,
  trends: IProcessedTrend[]
): [boolean, IProcessedTrend] {
  if (!trendID) {
    return [false, null];
  }
  const calcType = trends.find((i) => i.id === trendID);
  if (calcType) {
    return [true, calcType];
  }
  return [false, null];
}

export function findSeries(tagID: number, addedSeries: IPDTrendSeries[]) {
  const result = addedSeries.find((s) => {
    return s.MapData[0].Map.TagID === tagID;
  });

  return result;
}

export function findBandAxisMeasurement(
  tagID: number,
  addedBandAxisMeasurements: IPDTrendBandAxisMeasurement[]
) {
  const result = addedBandAxisMeasurements.find((m) => {
    return m.TagID1 === tagID;
  });

  return result;
}

export function findAxis(units: string, addedAxes: IPDTrendAxis[]) {
  let result: IPDTrendAxis = null;
  units = (units || '').trim().toLowerCase();
  result = addedAxes.find((a) => {
    return (a.Title || '').trim().toLowerCase() === units;
  });
  return result;
}

export function createNewAxis(title: string, addedAxes: IPDTrendAxis[]) {
  let nextAxis = 1;
  let minAxis = 0;
  if (addedAxes.length > 0) {
    for (const a of addedAxes) {
      nextAxis = Math.max(a.Axis, nextAxis);
      minAxis = Math.min(a.PDTrendAxisID, minAxis);
    }
    nextAxis++;
  }
  minAxis--;
  const result: IPDTrendAxis = {
    Title: title,
    Axis: nextAxis,
    GridLine: false,
    IsDefault: false,
    Max: null,
    Min: null,
    MinorStep: null,
    PDTrendAxisID: minAxis,
    PDTrendID: -1,
    Position: 0,
    OriginalMin: null,
    OriginalMax: null,
    Step: null,
  };
  return result;
}

export function createNewGroupedSeries(
  newAxis,
  assetVariableTypeTagMap,
  color: string,
  minID: number,
  displayOrder: number
) {
  return {
    AggregationType1: 3,
    AggregationType2: null,
    AggregationType3: null,
    AggregationType4: null,
    AggregationType5: null,
    AttributeType1: null,
    AttributeType2: null,
    AttributeType3: null,
    AttributeType4: null,
    AttributeType5: null,
    Axis: newAxis.Axis || 1,
    BarID: null,
    BorderThickness: 0,
    Color: color,
    FillColor: color,
    Invert: false,
    MarkerConfigs: [],
    Name: assetVariableTypeTagMap.Tag.TagName || '',
    Offset: 0,
    Params1: '',
    Params2: '',
    Params3: '',
    Params4: '',
    Params5: '',
    PDTrendID: null,
    Result1: null,
    Result2: null,
    Result3: null,
    Result4: null,
    Result5: null,
    Symbol: 'square',
    TagID1: assetVariableTypeTagMap.Tag.PDTagID || null,
    TagID2: null,
    TagID3: null,
    TagID4: null,
    TagID5: null,
    TooltipHideMin: null,
    TooltipStyle: '',
    TrendBandAxisMeasurementID: minID,
    Type: 'range',
    Value1: null,
    Value2: 0,
    Value3: null,
    Value4: null,
    Value5: null,
    ValueType1: null,
    ValueType2: null,
    ValueType3: null,
    ValueType4: null,
    ValueType5: null,
    VariableType1: null,
    VariableType2: null,
    VariableType3: null,
    VariableType4: null,
    VariableType5: null,
    IsFiltered: false,
    FilterMin: null,
    FilterMax: null,
    FlatlineThreshold: null,
    Exclude: '',
    ApplyToAll: false,
    UseGlobal: true,
    Flags: '',
    DisplayOrder: displayOrder,
  };
}

export function createNewSeries(assetVariableTypeTagMap, seriesLength: number) {
  return {
    AlternateDisplayTexts: [],
    ApplyToAll: false,
    Archive: '',
    ArchiveExt: null,
    AssetClassTypeID: null,
    availableArchives: null,
    availArchives: null,
    Axis: null,
    CalcDeviation: null,
    ChartType: null,
    ChartTypeID: null,
    DisplayOrder: seriesLength, // series.length + 1,
    DisplayText:
      assetVariableTypeTagMap.Tag.TagDesc ||
      assetVariableTypeTagMap.Tag.TagName ||
      'Series',
    Exclude: null,
    excludeFriday: null,
    excludeHours: null,
    excludeMonday: null,
    excludeSaturday: null,
    excludeSunday: null,
    excludeThursday: null,
    excludeTuesday: null,
    excludeWednesday: null,
    FilterMax: null,
    FilterMin: null,
    FlatlineThreshold: null,
    IncludeAllAssets: null,
    IncludeAncestors: null,
    IncludeChildren: null,
    IncludeDescendants: null,
    IncludeParents: null,
    IncludeCousins: null,
    IncludeSecondCousins: null,
    IncludeSelf: null,
    IncludeSiblings: null,
    IncludeUnit: null,
    IncludeUnitDescendants: null,
    isAP: false,
    IsFiltered: null,
    IsMain: null,
    IsXAxis: null,
    IsZAxis: null,
    MapData: [
      {
        Map: assetVariableTypeTagMap,
        Data: [],
        PinID: null,
      },
    ],
    PDTagID: assetVariableTypeTagMap.Tag.PDTagID,
    PDTrendID: null,
    PDTrendSeriesID: null,
    PDVariableID: assetVariableTypeTagMap.VariableTypeID,
    ScaleDecimals: null,
    ScaleMax: null,
    ScaleMin: null,
    SeriesColor: null,
    ShowBestFitLine: null,
    showDataRetrieval: null,
    showFilter: null,
    Stack: null,
    StackType: null,
    SummaryType: null,
    SummaryTypeID: null,
    UseDisplayText: null,
    UseGlobalDataRetrieval: true,
    UserVariableID2: null,
    UseVariableID1: null,
    ValueTypeID: null,
  };
}

export function addAxis(
  units: string,
  useExisting: boolean = true,
  axes: IPDTrendAxis[]
) {
  let newAxis: IPDTrendAxis;
  if (useExisting) {
    newAxis = findAxis(units, axes);
  }

  if (!newAxis) {
    newAxis = createNewAxis(units, axes);
    if (axes.length == 0) {
      newAxis.GridLine = true;
    }
    axes.push(newAxis);
  }

  return newAxis;
}

// setTimeOut. These aren't ideal, since it happens in milliseconds
// after called, instead of synchronously after an action.
export function reflow() {
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'));
  }, 500);
}

export function reflowFast() {
  setTimeout(() => {
    window.dispatchEvent(new Event('resize'));
  }, 10);
}

export function pinNameUpdate(
  value: string,
  startDate: string | Date,
  endDate: string | Date,
  locale: string
) {
  const chars = [...value];
  let startFirstIndex = -1;
  let startLastIndex = -1;
  let endFirstIndex = -1;
  let endLastIndex = -1;
  let isFirstBeforeLast = true;
  const stack = [];
  let invalid = false;
  chars.forEach((char, i) => {
    if (char === '{') {
      if (stack.length > 0) {
        invalid = true;
      }
      if (i > 0 && value[i - 1] === 'S') {
        if (startFirstIndex > -1) {
          invalid = true;
        }
        startFirstIndex = i;
        console.log(`start index ${i}`);
        stack.push('start');
      } else if (i > 0 && value[i - 1] === 'E') {
        if (endFirstIndex > -1) {
          invalid = true;
        }
        endFirstIndex = i;
        console.log(`end index ${i}`);
        stack.push('end');
      }
    }
    if (char === '}') {
      if (stack.length > 0) {
        if (stack[0] === 'start') {
          startLastIndex = i;
        } else {
          endLastIndex = i;
        }
        stack.pop();
      } else {
        invalid = true;
      }
    }
  });
  if (
    startFirstIndex > -1 &&
    endFirstIndex > -1 &&
    startFirstIndex > endFirstIndex
  ) {
    isFirstBeforeLast = false;
  }
  if (!invalid) {
    if (startFirstIndex > -1 && startLastIndex > -1) {
      const momentFormatStart = moment(startDate).toDate();
      const commandString = dynamicPinCreate(
        value.substring(startFirstIndex + 1, startLastIndex),
        momentFormatStart,
        locale
      );
      value =
        value.substring(0, startFirstIndex - 1) +
        commandString +
        value.substring(startLastIndex + 1);
      if (isFirstBeforeLast) {
        endFirstIndex += -3;
        endLastIndex += -3;
      }
    }
    if (endFirstIndex > -1 && endLastIndex > -1) {
      const momentFormatEnd = moment(endDate).toDate();
      const commandString = dynamicPinCreate(
        value.substring(endFirstIndex + 1, endLastIndex),
        momentFormatEnd,
        locale
      );
      value =
        value.substring(0, endFirstIndex - 1) +
        commandString +
        value.substring(endLastIndex + 1);
    }
  }
  return value;
}

export function dynamicPinCreate(
  braceCommand: string,
  braceDate: Date,
  locale: string
) {
  if (braceCommand === 'Q' || braceCommand === 'QQ' || braceCommand === 'q') {
    const month = braceDate.getMonth() + 1;
    const quarter = Math.ceil(month / 3.0);
    if (braceCommand === 'Q') {
      switch (quarter) {
        case 1:
          return 'First';
        case 2:
          return 'Second';
        case 3:
          return 'Third';
        default:
        case 4:
          return 'Fourth';
      }
    } else if (braceCommand === 'QQ') {
      switch (quarter) {
        case 1:
          return '1st';
        case 2:
          return '2nd';
        case 3:
          return '3rd';
        default:
        case 4:
          return '4th';
      }
    } else {
      return quarter.toString();
    }
  } else {
    // C# Datetime is similar, but a is tt
    braceCommand = braceCommand.replace('tt', 'a');
    const braceDateTransformation = formatDate(braceDate, braceCommand, locale);
    if (braceDateTransformation) {
      return braceDateTransformation;
    }
  }
}

export function processFilter(filter: IPDTrendFilter) {
  const newFilter: IPDTrendFilter = { ...filter };
  const excludeString: string = newFilter.Exclude;
  if (excludeString !== null && excludeString.search('<months>') >= 0) {
    let months = excludeString.slice(excludeString.search('<months>') + 8);
    months = months.slice(0, months.indexOf('<'));
    if (months.search('january') >= 0) {
      newFilter.excludeJanuary = true;
    }
    if (months.search('february') >= 0) {
      newFilter.excludeFebruary = true;
    }
    if (months.search('march') >= 0) {
      newFilter.excludeMarch = true;
    }
    if (months.search('april') >= 0) {
      newFilter.excludeApril = true;
    }
    if (months.search('may') >= 0) {
      newFilter.excludeMay = true;
    }
    if (months.search('june') >= 0) {
      newFilter.excludeJune = true;
    }
    if (months.search('july') >= 0) {
      newFilter.excludeJuly = true;
    }
    if (months.search('august') >= 0) {
      newFilter.excludeAugust = true;
    }
    if (months.search('september') >= 0) {
      newFilter.excludeSeptember = true;
    }
    if (months.search('october') >= 0) {
      newFilter.excludeOctober = true;
    }
    if (months.search('november') >= 0) {
      newFilter.excludeNovember = true;
    }
    if (months.search('december') >= 0) {
      newFilter.excludeDecember = true;
    }
  }
  if (excludeString !== null && excludeString.search('<days>') >= 0) {
    let days = excludeString.slice(excludeString.search('<days>') + 6);
    days = days.slice(0, days.indexOf('<'));
    if (days.search('monday') >= 0) {
      newFilter.excludeMonday = true;
    }
    if (days.search('tuesday') >= 0) {
      newFilter.excludeTuesday = true;
    }
    if (days.search('wednesday') >= 0) {
      newFilter.excludeWednesday = true;
    }
    if (days.search('thursday') >= 0) {
      newFilter.excludeThursday = true;
    }
    if (days.search('friday') >= 0) {
      newFilter.excludeFriday = true;
    }
    if (days.search('saturday') >= 0) {
      newFilter.excludeSaturday = true;
    }
    if (days.search('sunday') >= 0) {
      newFilter.excludeSunday = true;
    }
  }

  if (excludeString !== null && excludeString.search('<hours>') >= 0) {
    let hours = excludeString.slice(excludeString.search('<hours>') + 7);
    hours = hours.slice(0, hours.indexOf('<'));
    if (validateHours(hours)) {
      newFilter.excludeHours = hours || '';
    } else {
      newFilter.excludeHours = '';
    }
  }
  return newFilter;
}

export function finishFilterProcessing(seriesFilter: any) {
  const newFilter = { ...seriesFilter };
  if (newFilter.FilterMax === '') {
    newFilter.FilterMax = null;
  }
  if (newFilter.FilterMin === '') {
    newFilter.FilterMin = null;
  }

  let exclude = '<filter><months>';
  if (newFilter.excludeJanuary) {
    exclude += 'january,';
  }
  if (newFilter.excludeFebruary) {
    exclude += 'february,';
  }
  if (newFilter.excludeMarch) {
    exclude += 'march,';
  }
  if (newFilter.excludeApril) {
    exclude += 'april,';
  }
  if (newFilter.excludeMay) {
    exclude += 'may,';
  }
  if (newFilter.excludeJune) {
    exclude += 'june,';
  }
  if (newFilter.excludeJuly) {
    exclude += 'july,';
  }
  if (newFilter.excludeAugust) {
    exclude += 'august,';
  }
  if (newFilter.excludeSeptember) {
    exclude += 'september,';
  }
  if (newFilter.excludeOctober) {
    exclude += 'october,';
  }
  if (newFilter.excludeNovember) {
    exclude += 'november,';
  }
  if (newFilter.excludeDecember) {
    exclude += 'december,';
  }
  exclude += '</months><days>';
  if (newFilter.excludeMonday) {
    exclude += 'monday,';
  }
  if (newFilter.excludeTuesday) {
    exclude += 'tuesday,';
  }
  if (newFilter.excludeWednesday) {
    exclude += 'wednesday,';
  }
  if (newFilter.excludeThursday) {
    exclude += 'thursday,';
  }
  if (newFilter.excludeFriday) {
    exclude += 'friday,';
  }
  if (newFilter.excludeSaturday) {
    exclude += 'saturday,';
  }
  if (newFilter.excludeSunday) {
    exclude += 'sunday,';
  }
  exclude += '</days><hours>';

  if (validateHours(newFilter.excludeHours)) {
    exclude += newFilter.excludeHours || '';
  }

  exclude += '</hours></filter>';
  newFilter.Exclude = exclude;
  if (
    newFilter.FilterMax !== null ||
    newFilter.FilterMin !== null ||
    newFilter.FlatlineThreshold !== 0 ||
    exclude !== '<filter><months></months><days></days><hours></hours></filter>'
  ) {
    newFilter.IsFiltered = true;
  }

  return newFilter;
}

export function convertTextColorToHex(color) {
  if (typeof hexColors[color.toLowerCase()] != 'undefined') {
    return hexColors[color.toLowerCase()];
  }
  return color;
}

export function validateHours(hours: string) {
  if (hours && hours.search(/[^0-9,()-]/) >= 0) {
    return false;
  }
  return true;
}

export function getDefaultBar(
  assetID?: number,
  startDate?: Date,
  endDate?: Date
) {
  const val: IPDTrendBandAxisBar = {
    AssetClassTypeID: null,
    AssetID: assetID || null,
    Context: null,
    CriteriaObjectID: null,
    Derived: null,
    DisplayOrder: null,
    EndTime: null,
    IncludeAllAssets: null,
    IncludeAncestors: null,
    IncludeChildren: null,
    IncludeCousins: null,
    IncludeDescendants: null,
    IncludeParents: null,
    IncludeSecondCousins: null,
    IncludeSelf: true,
    IncludeSiblings: null,
    IncludeUnit: null,
    IncludeUnitDescendants: null,
    Label: null,
    Measurements: [],
    PDTrendBandAxisBarID: null,
    PDTrendID: null,
    StartTime: null,
    TagID: null,
    TimeDivision: null,
    ValueType: null,
    VariableTypeID: null,
  };

  if (startDate !== undefined && endDate !== undefined) {
    const diff = Math.abs(endDate.getTime() - startDate.getTime());
    if (diff < 1000 * 60 * 60 * 24 * 7) {
      //If the time range is less than 1 week default to 1 day
      val.TimeDivision = 'day';
    } else if (diff < 1000 * 60 * 60 * 24 * 7 * 4) {
      //If the time range is less than 4
      val.TimeDivision = 'week';
    } else if (diff < 1000 * 60 * 60 * 24 * 7 * 4 * 104) {
      //If the time range is less than 104 weeks default to 1 month
      val.TimeDivision = 'month';
    } else {
      //If the time range is greater than 104 weeks (2 years) default to 1 year.
      val.TimeDivision = 'year';
    }
  }

  return val;
}

// This will add the values to its corresponding arrays.
// TIME and PINS group type values are added on the BandAxisMeasurements
// CUSTOM and ASSET group type values are added on the BandAxisBars
export function processBandAxisBarsAndMeasurements(state: IProcessedTrend) {
  state.trendDefinition.BandAxisBars = [];
  state.trendDefinition.BandAxisMeasurements = [];

  state.groupSeriesMeasurements?.map((gs) => {
    if (state.groupedSeriesType === GroupedSeriesType.PINS) {
      let foundBar = state.trendDefinition.BandAxisBars.find((b) => {
        return b.TimeDivision === 'pins';
      });
      if (!foundBar) {
        foundBar = getDefaultBar();
        foundBar.Label = '';
        foundBar.TimeDivision = 'pins';
        state.trendDefinition.BandAxisBars.push(foundBar);
      }

      gs.BarID = null;
      state.trendDefinition.BandAxisMeasurements =
        state.trendDefinition.BandAxisMeasurements || [];
      state.trendDefinition.BandAxisMeasurements.push({ ...gs });
    } else if (state.groupedSeriesType === GroupedSeriesType.TIME) {
      let foundBar = state.trendDefinition.BandAxisBars.find((b) => {
        return b.TimeDivision === state.groupedSeriesSubType;
      });
      if (!foundBar) {
        foundBar = getDefaultBar();
        foundBar.Label = '';
        foundBar.TimeDivision = state.groupedSeriesSubType;
        state.trendDefinition.BandAxisBars.push(foundBar);
      }

      gs.BarID = null;
      state.trendDefinition.BandAxisMeasurements =
        state.trendDefinition.BandAxisMeasurements || [];
      state.trendDefinition.BandAxisMeasurements.push({ ...gs });
    }
  });

  if (state.groupedSeriesType === GroupedSeriesType.TIME) {
    const timeDivision = state.groupedSeriesSubType ?? 'week';
    state.trendDefinition.BandAxisBars =
      state.trendDefinition.BandAxisBars?.map((bar) => {
        const newBar = { ...bar };
        newBar.TimeDivision = timeDivision;
        return newBar;
      });
    state.groupedSeriesSubType = timeDivision;
  } else if (state.groupedSeriesType === GroupedSeriesType.PINS) {
    state.trendDefinition.BandAxisBars =
      state.trendDefinition.BandAxisBars?.map((bar) => {
        const newBar = { ...bar };
        newBar.TimeDivision = 'pins';
        return newBar;
      });
    state.groupedSeriesSubType = null;
  } else {
    processCustomAndAssetGroupTypes(state);
    state.trendDefinition.BandAxisBars =
      state.trendDefinition.BandAxisBars?.map((bar) => {
        const newBar = { ...bar };
        newBar.TimeDivision = null;
        return newBar;
      });
    state.groupedSeriesSubType = null;
  }
}

// This will handle CUSTOM and ASSET group types
// CUSTOM AND ASSET group types needs special processing
export function processCustomAndAssetGroupTypes(state: IProcessedTrend) {
  const groupOrder = [];
  let seriesOrder = []; //fill with first group ordering to keep other groups the same order
  const extraOrder = []; //series doesn't exist in first group, so add to the end
  let firstGroup = null;
  let matchGroup = null;
  let displayOrderNum = 1;

  state.groupSeriesMeasurements?.forEach((gs) => {
    let foundBar;
    if (state.groupedSeriesType === GroupedSeriesType.ASSET && gs.AssetID) {
      matchGroup = gs.AssetID;
      foundBar = state.trendDefinition.BandAxisBars.find((b) => {
        return b.AssetID === matchGroup;
      });
    } else if (
      state.groupedSeriesType === GroupedSeriesType.CUSTOM &&
      gs.BarName
    ) {
      matchGroup = gs.BarName;
      foundBar = state.trendDefinition.BandAxisBars.find((b) => {
        return b.Label === matchGroup;
      });
    } else {
      gs.BarID = null; //empty custom group name = add measurement to all groups
      state.trendDefinition.BandAxisMeasurements =
        state.trendDefinition.BandAxisMeasurements || [];
      state.trendDefinition.BandAxisMeasurements.push({ ...gs });
      return;
    }
    if (!foundBar) {
      if (state.groupedSeriesType === GroupedSeriesType.ASSET) {
        foundBar = getDefaultBar(matchGroup);
      } else {
        foundBar = getDefaultBar();
        foundBar.Label = matchGroup;
      }
      foundBar.DisplayOrder = displayOrderNum; //without this, group labels will be alpha ordered
      displayOrderNum++;
      state.trendDefinition.BandAxisBars.push(foundBar);
      groupOrder.push(matchGroup);
      if (!firstGroup) {
        firstGroup = matchGroup;
      }
    }
    if (matchGroup == firstGroup) {
      seriesOrder.push(gs.Name);
      if (extraOrder) {
        const n = extraOrder.indexOf(gs.Name);
        if (n > -1) {
          delete extraOrder[n]; //series exists in first group, but already appeared in another group - remove
        }
      }
    } else if (
      (!seriesOrder || seriesOrder.indexOf(gs.Name) == -1) &&
      (!extraOrder || extraOrder.indexOf(gs.Name) == -1)
    ) {
      extraOrder.push(gs.Name); //series not (yet) in first group, so add to bottom of order
    }
  });
  seriesOrder = seriesOrder.concat(extraOrder); //should have distinct series order now

  groupOrder.map((curGroup) => {
    seriesOrder.map((curSeries) => {
      let foundMeasure;
      let foundBar;
      if (state.groupedSeriesType === GroupedSeriesType.ASSET) {
        foundMeasure = state.groupSeriesMeasurements.find((m) => {
          return m.Name === curSeries && m.AssetID === curGroup;
        });
        if (foundMeasure) {
          foundBar = state.trendDefinition.BandAxisBars.find((b) => {
            return b.AssetID === curGroup;
          });
        }
      } else {
        //  'custom'
        foundMeasure = state.groupSeriesMeasurements.find((m) => {
          return m.Name === curSeries && m.BarName === curGroup;
        });
        if (foundMeasure) {
          foundBar = state.trendDefinition.BandAxisBars.find((b) => {
            return b.Label === curGroup;
          });
        }
      }
      if (foundMeasure && foundBar) {
        foundBar.Measurements = foundBar.Measurements || [];
        foundBar.Measurements.push(foundMeasure);
      }
    });
  });
}

export function processTrendLabel(trend: IProcessedTrend) {
  const result = trend.label;
  let cnt = 0;

  if (trend?.trendDefinition?.Series?.length > 0) {
    cnt = trend.trendDefinition.Series.length;
  } else {
    if (trend?.trendDefinition?.BandAxisMeasurements?.length > 0) {
      cnt = trend.trendDefinition.BandAxisMeasurements.length;
    }

    if (trend?.trendDefinition?.BandAxisBars?.length > 0) {
      for (const bandAxisBars of trend.trendDefinition.BandAxisBars) {
        cnt += bandAxisBars.Measurements.length;
      }
    }
  }

  return result + ' (' + cnt + ')';
}
