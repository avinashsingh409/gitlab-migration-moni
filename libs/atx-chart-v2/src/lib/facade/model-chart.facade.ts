import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { INDModelSummary } from '@atonix/shared/api';
import { ComponentStore } from '@ngrx/component-store';
import { HighchartsService } from '../service/highcharts.service';

export interface ModelChartState {
  assetID: number; // needed for legacy save call
  assetGuid: string;
  uniqueID: string;
  model: INDModelSummary;
  modelLoading: boolean;
  loadingData: boolean;
  errorNoData: boolean;
  error: string;
  height: number;
  start: Date;
  end: Date;
}

export const initialModelChartState: ModelChartState = {
  assetID: null,
  assetGuid: null,
  uniqueID: null,
  model: null,
  modelLoading: true,
  loadingData: false,
  errorNoData: false,
  error: '',
  height: null,
  start: null,
  end: null,
};

@Injectable({
  providedIn: 'root',
})
export class ModelChartFacade extends ComponentStore<ModelChartState> {
  highcharts: any;

  constructor(@Inject(APP_CONFIG) private appConfig: AppConfig) {
    super(initialModelChartState);
  }
}
