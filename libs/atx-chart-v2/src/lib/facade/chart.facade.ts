/* eslint-disable ngrx/updater-explicit-return-type */
import { Inject, Injectable, LOCALE_ID, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  filter,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  getDefaultMeasurement,
  resetAxes,
  setDataOnTrend,
  updateAxes,
} from '../service/utilities';
import {
  addAxis,
  convertTextColorToHex,
  createNewAxis,
  createNewGroupedSeries,
  createNewSeries,
  findBandAxisMeasurement,
  findSeries,
  findTrend,
  finishFilterProcessing,
  getDefaultBar,
  getSelectedTrendID,
  pinNameUpdate,
  processBandAxisBarsAndMeasurements,
  processFilter,
  reflow,
  reflowFast,
  updateTrendAxes,
  updateTrendDefinition,
} from './chart.facade.utils';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  LoadDataExplorerCharts,
  LoadDataExplorerTrends,
  RefreshDataExplorerCharts,
  RefreshStandardChart,
  TrendsLoaded,
} from '../models/facade-models';
import { DataRetrievalTypeMapping } from '../models/data-retrieval';
import { ChartQuery, ChartState, initialChartState } from './chart.query';
import { IUpdateLimitsData } from '../models/update-limits-data';
import { castDraft, enableMapSet, produce } from 'immer';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  DesignCurve,
  GroupedSeriesType,
  IAssetMeasurementsSet,
  IAssetVariableTypeTagMap,
  ICriteria,
  IGSMeasurement,
  IPDTrend,
  IPDTrendAxis,
  IPDTrendBandAxisBar,
  IPDTrendBandAxisMeasurement,
  IPDTrendFilter,
  IPDTrendPin,
  IPDTrendSeries,
  IProcessedTrend,
  ISummaryType,
} from '@atonix/atx-core';
import { ChartFormElements } from '../models/chart-form-elements';
import {
  AtxChartLine,
  AtxChartRadar,
  AtxChartTable,
  AtxChartTrend,
  AtxPinTypeUnmodified,
  AtxScatterPlot,
} from '../service/chart.service';
import { isNil } from '@atonix/atx-core';
import { SummaryTypes } from '../models/summary-types';
import { FormPin } from '../models/pin-types';
import {
  DataExplorerCoreService,
  ImagesFrameworkService,
  ProcessDataFrameworkService,
} from '@atonix/shared/api';
import moment from 'moment';

import { ColorService } from '../service/color.service';
import { getADayAgo, LoggerService } from '@atonix/shared/utils';
import { IAdvancedOptions } from '../models/advanced-options';
import { DataSourceValues, SeriesState } from '../models/series-state';
import { SeriesFilterState } from '../models/series-filter-state';
import { LabelIndex } from '../models/label-index';
enableMapSet();
@Injectable({
  providedIn: 'root',
})
export class ChartFacade
  extends ComponentStore<ChartState>
  implements OnDestroy
{
  private onDestroy = new Subject<void>();
  public query = new ChartQuery(this);
  location: any;

  constructor(
    private dataExplorerCoreService: DataExplorerCoreService,
    private imagesFrameworkService: ImagesFrameworkService,
    private processDataFrameworkService: ProcessDataFrameworkService,
    private logger: LoggerService,

    @Inject(LOCALE_ID) private locale: string,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    super(initialChartState);
  }
  readonly dismissError = this.updater((state: ChartState) => {
    return {
      ...state,
      chartError: null,
    };
  });
  readonly setLoading = this.updater((state: ChartState) => ({
    ...state,
    trends: null,
    selectedTrendID: null,
    trendsLoading: true,
    loadingData: true,
    errorNoData: false,
  }));
  readonly setDirty = this.updater(
    (state: ChartState, isDirty: Record<string, boolean>) => ({
      ...state,
      isDirty,
    })
  );
  readonly setAssetID = this.updater(
    (
      state: ChartState,
      assetObject: { assetID: number; assetGuid: string }
    ) => ({
      ...state,
      assetID: assetObject.assetID,
      assetGuid: assetObject.assetGuid,
      // timeFilters: null,
    })
  );
  readonly setLoadingTrends = this.updater((state: ChartState) => {
    return {
      ...state,

      loadingData: false,
      trendsLoading: true,
    };
  });
  readonly setSeriesFormElement = this.updater(
    (state: ChartState, series: IPDTrendSeries[]) => {
      return {
        ...state,
        formElements: {
          ...state.formElements,
          series,
        },
      };
    }
  );
  readonly setGroupedSeriesFormElement = this.updater(
    (state: ChartState, groupedSeries: IPDTrendBandAxisMeasurement[]) => {
      return {
        ...state,
        formElements: {
          ...state.formElements,
          groupedSeries,
        },
      };
    }
  );
  readonly setPinFormElements = this.updater(
    (state: ChartState, pins: IPDTrendPin[]) => {
      return {
        ...state,
        formElements: {
          ...state.formElements,
          pins,
        },
      };
    }
  );
  readonly setIsXY = this.updater((state: ChartState, isXY: boolean) => {
    return {
      ...state,
      isXY,
    };
  });
  readonly setSelectedTrend = this.updater(
    (state: ChartState, selectedTrend: IProcessedTrend) => {
      return {
        ...state,
        selectedTrend,
        trendsLoading: false,
      };
    }
  );
  private readonly setFormElements = this.updater(
    (state: ChartState, formElements: ChartFormElements) => {
      return {
        ...state,
        formElements,
        trendsLoading: false,
      };
    }
  );
  private readonly chartHeight = this.updater(
    (state: ChartState, chartHeight: number) => ({
      ...state,
      chartHeight,
    })
  );

  readonly setError = this.updater((state: ChartState, error: any) => {
    return {
      ...state,
      loadingData: false,
      trendsLoading: false,
      errorNoData: true,
      isGroupedSeries: false,
    };
  });
  readonly updateTrends = this.updater(
    (state: ChartState, trends: IProcessedTrend[]) => {
      return {
        ...state,
        trends,
      };
    }
  );

  private readonly convertVariablesToTags = this.effect(
    (
      series$: Observable<{
        trend: IProcessedTrend;
        archive: string;
        start: Date;
        end: Date;
      }>
    ) => {
      return series$.pipe(
        tap(() =>
          this.updater((state: ChartState) => {
            return {
              ...state,
              trendsLoading: true,
            };
          })
        ),
        switchMap((tagObject) => {
          return this.processDataFrameworkService
            .convertVariablesToTags(tagObject.trend.trendDefinition.Series)
            .pipe(
              tapResponse(
                (data: IPDTrendSeries[]) => {
                  if (data) {
                    const newSeries = produce(
                      tagObject.trend.trendDefinition.Series,
                      (draftState) => {
                        draftState.forEach((list, x) => {
                          const items = data.find(
                            (d) => d.PDTagID == list.PDTagID
                          ).AlternateDisplayTexts; //get build alternate display texts
                          if (items) {
                            draftState[x].AlternateDisplayTexts = castDraft(
                              draftState[x].AlternateDisplayTexts
                            );
                            if (
                              !draftState[x].AlternateDisplayTexts ||
                              draftState[x].AlternateDisplayTexts.length < 1
                            ) {
                              draftState[x].AlternateDisplayTexts = [];
                              items.forEach((prefix) => {
                                draftState[x].AlternateDisplayTexts.push(
                                  prefix
                                );
                              });
                              draftState[x].DisplayText =
                                draftState[x].AlternateDisplayTexts[0];
                            }
                          }
                        });
                      }
                    );
                    const newTrend = produce(tagObject.trend, (draftState) => {
                      draftState.trendDefinition.Series = newSeries;
                    });
                    this.setTrendDirty({
                      trend: newTrend,
                      trendID: newTrend.id,
                      archive: tagObject.archive,
                      startDate: tagObject.start,
                      endDate: tagObject.end,
                    });
                  }
                },
                (error: string) => this.setError(error)
              )
            );
        })
      );
    }
  );
  private readonly setTrends = this.updater(
    (state: ChartState, trendReturn: TrendsLoaded) => ({
      ...state,
      dirtyTrends: new Map<string, null>(),
      isDirty: trendReturn.isDirty,
      uniqueID: trendReturn.uniqueID,
      trends: trendReturn.trends,
      selectedTrendID: trendReturn.selectedTrendID,
      start: trendReturn.start,
      end: trendReturn.end,
      loadingData: false,
    })
  );
  private readonly setTimeSeriesOnTrend = this.updater(
    (state: ChartState, selectedTrend: IProcessedTrend) => {
      let retrievalDescription = '';
      let isGroupedSeries = false;
      if (selectedTrend?.trendDefinition?.BandAxis) {
        isGroupedSeries = true;
      }
      let isXY = false;
      if (selectedTrend.trendDefinition?.Series) {
        selectedTrend.trendDefinition.Series.forEach((series) => {
          if (series.IsXAxis) {
            isXY = true;
          }
        });
      }

      let error = null;
      if (
        selectedTrend?.measurements.Errors &&
        selectedTrend?.measurements?.Errors?.length > 0
      ) {
        if (selectedTrend.measurements.Errors[0].ErrorType === 0) {
          error = `<b>Can't Pull Data</b><br />The following tags are not available in the selected archives:`;
        } else {
          error = `<b>Can't Pull Data</b><br />Chart span exceeds maximum allowable data for the following tags:`;
        }
        const tagNames = new Set();
        selectedTrend.measurements.Errors[0].TagNames.forEach((tagName) => {
          tagNames.add(tagName);
        });
        tagNames.forEach((tagName) => {
          error += `<br />${tagName}`;
        });
        if (selectedTrend.measurements.Errors[0].ErrorType === 0) {
          error += '<br />Please change the data retrieval settings.';
        } else {
          error +=
            '<br /><br />Please select a shorter time range or change the data retrieval settings.';
        }
      }
      if (selectedTrend?.trendDefinition?.DataRetrieval?.Method > -1) {
        const method = selectedTrend.trendDefinition.DataRetrieval?.Method;
        if (method > -1 && method < 4) {
          retrievalDescription = `${DataRetrievalTypeMapping[method + 1]}. `;
          if (method === 2) {
            retrievalDescription +=
              selectedTrend?.trendDefinition?.DataRetrieval?.Archive;
          } else if (method === 3) {
            // eslint-disable-next-line max-len
            retrievalDescription += `${selectedTrend?.trendDefinition?.DataRetrieval?.MinInterval} ${selectedTrend?.trendDefinition?.DataRetrieval?.MinIntervalUnits}`;
          }
        }
      }
      const pins: FormPin[] = [];
      const newTrend = produce(selectedTrend, (state) => {
        if (
          isNil(selectedTrend.trendDefinition.SummaryType) ||
          isNil(selectedTrend.trendDefinition.SummaryTypeID)
        ) {
          // null means none selected
          state.trendDefinition.SummaryType = null;
          state.trendDefinition.SummaryTypeID = SummaryTypes[0].SummaryTypeID;
        }
        if (isNil(selectedTrend.trendDefinition.DesignCurves)) {
          state.trendDefinition.DesignCurves = [];
        }
        if (isGroupedSeries) {
          // There are 4 types of series group: TIME, PINS, CUSTOM, and ASSET
          // TIME and PINS values are from BandAxisMeasurements
          // CUSTOM and ASSET values are from BandAxisBars
          // groupSeriesMeasurements array holds one of those group type data
          // so that it is more easy to manipulate when changing between different types
          state.groupSeriesMeasurements =
            state.trendDefinition.BandAxisMeasurements?.filter(
              (bandAxisMeasurements) =>
                bandAxisMeasurements.DisplayOrder === null ||
                bandAxisMeasurements.DisplayOrder === 0
            ) || []; //if not defined or 0, put at top of list and draw first

          if (state.trendDefinition.BandAxisMeasurements?.length > 0) {
            for (const bandAxisMeasurements of state.trendDefinition
              .BandAxisMeasurements) {
              if (
                bandAxisMeasurements.DisplayOrder &&
                bandAxisMeasurements.DisplayOrder !== 0
              ) {
                state.groupSeriesMeasurements.push({
                  ...bandAxisMeasurements,
                });
              }
            }
          }

          state.groupedSeriesType = GroupedSeriesType.CUSTOM;
          if (state.trendDefinition.BandAxisBars?.length > 0) {
            for (const bandAxisBars of state.trendDefinition.BandAxisBars) {
              if (bandAxisBars.Measurements) {
                for (const m of bandAxisBars.Measurements) {
                  (<IGSMeasurement>m).AssetID = bandAxisBars.AssetID;
                  (<IGSMeasurement>m).BarName = bandAxisBars.Label;
                  m.Params1 = +m.Params1;
                  m.Params2 = +m.Params2;
                  m.Params3 = +m.Params3;
                  m.Params4 = +m.Params4;
                  m.Params5 = +m.Params5;
                  state.groupSeriesMeasurements.push({
                    ...m,
                  });
                }
              }
            }

            if (
              state.trendDefinition.BandAxisBars.every((c) => !!c.TimeDivision)
            ) {
              state.groupedSeriesType = GroupedSeriesType.TIME;
            }
            if (
              state.trendDefinition.BandAxisBars.every(
                (c) => c.TimeDivision === 'pins'
              )
            ) {
              state.groupedSeriesType = GroupedSeriesType.PINS;
            }
            if (state.groupedSeriesType === GroupedSeriesType.TIME) {
              state.groupedSeriesSubType = 'week';
              if (
                state.trendDefinition.BandAxisBars &&
                state.trendDefinition.BandAxisBars.length > 0
              ) {
                if (state.trendDefinition.BandAxisBars[0].TimeDivision) {
                  state.groupedSeriesSubType =
                    state.trendDefinition.BandAxisBars[0].TimeDivision;
                }
              }
            }
            if (state.trendDefinition.BandAxisBars.every((c) => !!c.AssetID)) {
              error = `Asset Group Type Not Supported`;
            }
          }

          if (state.groupSeriesMeasurements?.length > 0) {
            state.groupSeriesMeasurements.sort((a, b) => {
              return a.DisplayOrder - b.DisplayOrder;
            });

            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (series, index) => {
                const newSeries = { ...series };
                newSeries.DisplayOrder = index + 1;
                return newSeries;
              }
            );
          }
        } else {
          state.groupedSeriesType = GroupedSeriesType.NONE;
          state.groupedSeriesSubType = '';
        }

        if (!selectedTrend.trendDefinition?.ChartTypeID) {
          if (isGroupedSeries) {
            state.trendDefinition.ChartTypeID = AtxChartTrend;
          } else {
            if (isXY) {
              state.trendDefinition.ChartTypeID = AtxScatterPlot;
            } else {
              state.trendDefinition.ChartTypeID = AtxChartLine;
            }
          }
        }
        if (selectedTrend?.trendDefinition?.Pins) {
          if (selectedTrend.trendDefinition.Pins.length > 0) {
            state.trendDefinition.Pins = state.trendDefinition.Pins.map(
              (pin, index) => {
                const newPin = { ...pin };
                newPin.DisplayOrder = index + 1;
                return newPin;
              }
            );
            // this is to fix the startTime / end time pin bug

            // For each pin we have defined on the chart, look for the return
            // pin if one was given, and update the start and end times to match
            // for criteria pins
            selectedTrend.trendDefinition.Pins.forEach((pin, pinIndex) => {
              const serverPin = selectedTrend.measurements.Pins.find(
                (mPin) => mPin.PDTrendPinID === pin.PDTrendPinID
              );
              let startDateTime = pin.StartTime;
              let endDateTime = pin.EndTime;
              if (serverPin) {
                startDateTime = serverPin.StartTime;
                endDateTime = serverPin.EndTime;
                state.trendDefinition.Pins[pinIndex].StartTime =
                  serverPin.StartTime;
                state.trendDefinition.Pins[pinIndex].EndTime =
                  serverPin.EndTime;
              }
              const newValue = pinNameUpdate(
                pin.NameFormat,
                startDateTime,
                endDateTime,
                this.locale
              );
              state.trendDefinition.Pins[pinIndex].Name = newValue;
              // The form elements need Time and Date separated
              let startDate = null;
              let startTime = '';
              let endDate = null;
              let endTime = '';
              if (moment(startDateTime).isValid()) {
                startDate = moment(startDateTime).toDate();
                startTime = moment(startDateTime).format('HH:mm');
              }
              if (moment(endDateTime).isValid()) {
                endDate = moment(endDateTime).toDate();
                endTime = moment(endDateTime).format('HH:mm');
              }
              if (pin.CriteriaObjectId) {
                pins.push({
                  pinID: pin.CriteriaObjectId,
                  pinType: 'Time Filter',
                  startDate,
                  startTime,
                  endDate,
                  endTime,
                });
              } else {
                pins.push({
                  pinID: -1,
                  pinType: 'Fixed',
                  startDate,
                  startTime,
                  endDate,
                  endTime,
                });
              }
            });
          }
        }

        if (selectedTrend?.trendDefinition?.Series?.length > 0) {
          const tagIds = selectedTrend?.trendDefinition?.Series.map(
            (ser) => ser.PDTagID
          );
          this.getAllArchives({ tagIds });

          state.trendDefinition.Series = [
            ...selectedTrend.trendDefinition.Series,
          ].sort((a, b) => {
            return a.DisplayOrder - b.DisplayOrder;
          });

          state.trendDefinition.Series = state.trendDefinition.Series.map(
            (series, index) => {
              const newSeries = { ...series };
              newSeries.DisplayOrder = index + 1;
              newSeries.visible = newSeries.visible ?? true;
              if (series?.SeriesColor) {
                newSeries.SeriesColor = convertTextColorToHex(
                  series.SeriesColor
                );
              }
              return newSeries;
            }
          );

          // This will check the colors if they already exist then will get another color.
          // In the event where the highcharts built-in colors are all used, it will set the first color back.
          const existingColors: string[] = state.trendDefinition.Series.filter(
            (s) => s.SeriesColor
          ).map((s) => s.SeriesColor);

          const colors = new ColorService();
          state.trendDefinition.Series = state.trendDefinition.Series.map(
            (series, index) => {
              const newSeries = { ...series };
              newSeries.SeriesColor =
                newSeries.SeriesColor || colors.getColor(existingColors);
              return newSeries;
            }
          );
        } else {
          this.setAllArchives([]);
        }

        if (selectedTrend?.trendDefinition?.Filter) {
          state.trendDefinition.Filter = processFilter(
            state.trendDefinition.Filter
          );
        }
      });

      const dirtyState = produce(state, (draftState) => {
        // https://github.com/immerjs/immer/issues/393
        draftState.isDirty = castDraft(draftState.isDirty);
        if (selectedTrend?.isDirty) {
          draftState.isDirty[newTrend.id] = true;
        }
      });

      return {
        ...state,
        isDirty: selectedTrend?.isDirty ? dirtyState.isDirty : state.isDirty,
        start: newTrend?.startDate,
        end: newTrend?.endDate,
        chartError: error,
        isGroupedSeries,
        isXY,
        pins,
        errorNoData: false,
        trendsLoading: false,
        selectedTrendID: newTrend.id,
        // updating the FormElements refreshes the form controls with latest data
        // will cause them to loose user focus
        formElements: {
          id: newTrend.id,
          title: newTrend?.trendDefinition?.Title,
          series: newTrend?.trendDefinition?.Series,
          groupedSeries: newTrend?.groupSeriesMeasurements ?? [],
          groupedSeriesAxis: newTrend?.trendDefinition?.BandAxis,
          groupedSeriesBars: newTrend?.trendDefinition?.BandAxisBars ?? [],
          axes: newTrend?.trendDefinition?.Axes,
          pins: newTrend?.trendDefinition?.Pins,
          curves: newTrend?.trendDefinition?.DesignCurves
            ? newTrend?.trendDefinition?.DesignCurves
            : [],
          advanced: {
            filter: newTrend?.trendDefinition?.Filter,
            dataRetrievalMethod:
              newTrend?.trendDefinition?.DataRetrieval?.Method,
            dataRetrievalMinInterval:
              newTrend?.trendDefinition?.DataRetrieval?.MinInterval,
            dataRetrievalMinIntervalUnits:
              newTrend?.trendDefinition?.DataRetrieval?.MinIntervalUnits,
            dataRetrievalArchive:
              newTrend.trendDefinition?.DataRetrieval?.Archive,
          },
          isXY,
        },
        pinTypeID: newTrend?.trendDefinition?.PinTypeID,
        dataRetrievalMethod: {
          ...state.dataRetrievalMethod,
          description: retrievalDescription,
          method: newTrend?.trendDefinition?.DataRetrieval?.Method,
          minInterval: newTrend?.trendDefinition?.DataRetrieval?.MinInterval,
          minIntervalUnits:
            newTrend.trendDefinition?.DataRetrieval?.MinIntervalUnits,
          archive: newTrend.trendDefinition?.DataRetrieval?.Archive,
        },
        selectedTrend: newTrend,
      };
    }
  );

  private readonly setTimeFilters = this.updater(
    (state: ChartState, timeFilters: ICriteria[]) => ({
      ...state,
      timeFilters,
    })
  );

  readonly setAllArchives = this.updater(
    (state: ChartState, allArchives: string[]) => ({
      ...state,
      dataRetrievalMethod: {
        ...state.dataRetrievalMethod,
        allArchives,
      },
    })
  );

  readonly getCriteriaWithDataTimeFilters = this.effect(
    (charts$: Observable<{ assetGuid: string; assetID: number }>) => {
      return charts$.pipe(
        tap((chartAsset) =>
          this.setAssetID({
            assetID: chartAsset.assetID,
            assetGuid: chartAsset.assetGuid,
          })
        ),
        switchMap((chartAsset) =>
          this.processDataFrameworkService
            .getCriteriaWithDataTimeFilters(chartAsset.assetGuid)
            .pipe(
              tapResponse(
                (response) => {
                  response.unshift({
                    CoDFID: -1,
                    CoID: -1,
                    GlobalID: '',
                    CoTitle: 'Fixed (date/time entry)',
                    CoDescription: '',
                    CreatedBy: '',
                    ChangedBy: '',
                    CreateDate: null,
                    ChangeDate: null,
                    RangeFlag: false,
                    IndicatorFlag: false,
                  });
                  this.setTimeFilters(response);
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            )
        )
      );
    }
  );

  readonly getAllArchives = this.effect(
    (charts$: Observable<{ tagIds: number[] }>) => {
      return charts$.pipe(
        switchMap((chartAsset) =>
          this.processDataFrameworkService
            .getArchivesExtFromTagId(chartAsset.tagIds)
            .pipe(
              tapResponse(
                (response) => {
                  const allArchives = response.map((data) => data.Archive);
                  this.setAllArchives(allArchives);
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            )
        )
      );
    }
  );

  readonly refreshChartsAfterSave = this.effect(
    (charts$: Observable<RefreshDataExplorerCharts>) => {
      return charts$.pipe(
        tap(() => this.setLoading()),
        switchMap((chartInfo: RefreshDataExplorerCharts) =>
          this.processDataFrameworkService.getTrends(chartInfo.nodeID).pipe(
            tap(() => this.setLoadingTrends()),
            tapResponse(
              (response) => {
                const trends = updateTrendAxes(response);
                const isDirty = this.updateDirtyList(
                  response,
                  chartInfo.isDirty,
                  [chartInfo.oldTrendID, chartInfo.newTrendID]
                );
                const dirtyTrends = produce(chartInfo, (draftState) => {
                  draftState.dirtyTrends = castDraft(draftState.dirtyTrends);
                  for (const key in chartInfo.isDirty) {
                    if (key in [chartInfo.oldTrendID, chartInfo.newTrendID]) {
                      if (!isNil(chartInfo.dirtyTrends[key])) {
                        draftState.dirtyTrends.delete(key);
                      }
                    }
                  }
                });
                //Here we modify the state by adding a new trend or updating an existing trend in the array.
                const updatedNewTrends = produce(chartInfo.trends, (state) => {
                  state = this.replaceTrend(
                    chartInfo.newTrendID,
                    chartInfo.trends,
                    trends,
                    state
                  );
                });
                //Here we modify the state by removing the trend with id -1. and finally sorting the array.
                const newTrends = produce(updatedNewTrends, (state) => {
                  state = this.replaceTrend(
                    chartInfo.oldTrendID,
                    updatedNewTrends,
                    trends,
                    state
                  );
                  state.sort((a, b) => a.label.localeCompare(b.label));
                });

                this.patchState((state) => ({
                  trends: newTrends,
                  isDirty,
                  dirtyTrends: dirtyTrends.dirtyTrends,
                  uniqueID: chartInfo.nodeID,
                  selectedTrendID: chartInfo.newTrendID,
                  start: chartInfo.startDate,
                  end: chartInfo.endDate,
                }));
                const trend = findTrend(chartInfo.newTrendID, newTrends);
                if (trend[0]) {
                  this.getTrendData({
                    trend: trend[1],
                    trendID: chartInfo.newTrendID,
                    archive: '',
                    startDate: chartInfo.startDate,
                    endDate: chartInfo.endDate,
                  });
                } else if (newTrends && newTrends?.length > 0) {
                  this.getTrendData({
                    trend: newTrends[0],
                    trendID: newTrends[0].id,
                    archive: '',
                    startDate: chartInfo.startDate,
                    endDate: chartInfo.endDate,
                  });
                } else {
                  this.patchState((state) => ({
                    selectedTrend: null,
                    trends: null,
                    isDirty: {},
                    formElements: {
                      id: '',
                      title: '',
                      series: [],
                      groupedSeries: [],
                      groupedSeriesAxis: null,
                      groupedSeriesBars: [],
                      axes: [],
                      pins: [],
                      curves: [],
                      advanced: null,
                      isXY: false,
                    }, // tabs needs to update
                  }));
                  this.setError('No Data');
                }
              },
              (error: string) => this.setError(error)
            )
          )
        )
      );
    }
  );

  readonly refreshStandardChartAfterRevert = this.effect(
    (charts$: Observable<RefreshStandardChart>) => {
      return charts$.pipe(
        tap(() => this.setLoading()),
        switchMap((chartInfo: RefreshStandardChart) =>
          this.processDataFrameworkService.getTrends(chartInfo.nodeID).pipe(
            tap(() => this.setLoadingTrends()),
            tapResponse(
              (response) => {
                const trends = updateTrendDefinition(response);
                // Using the label to locate trend because the
                // modified and the original standard trend will have different ids
                const newStandardTrend = trends.find(
                  (trend) => trend.label === chartInfo.selectedTrend.label
                );

                const isDirty = this.updateDirtyList(
                  trends,
                  chartInfo.isDirty,
                  [chartInfo.selectedTrend.id, newStandardTrend.id]
                );
                const dirtyTrends = produce(chartInfo.dirtyTrends, (state) => {
                  for (const key in chartInfo.isDirty) {
                    if (
                      key in [chartInfo.selectedTrend.id, newStandardTrend.id]
                    ) {
                      if (!isNil(chartInfo.dirtyTrends[key])) {
                        state.delete(key);
                      }
                    }
                  }
                });

                //Here we modify the state by updating an existing trend in the array.
                const updatedNewTrends = produce(chartInfo.trends, (state) => {
                  state = this.replaceTrend(
                    newStandardTrend.id,
                    chartInfo.trends,
                    trends,
                    state
                  );
                });

                this.patchState((state) => ({
                  trends: updatedNewTrends,
                  isDirty,
                  dirtyTrends,
                  uniqueID: chartInfo.nodeID,
                  selectedTrendID: newStandardTrend.id,
                  start: chartInfo.startDate,
                  end: chartInfo.endDate,
                }));

                if (newStandardTrend) {
                  this.getTrendData({
                    trend: newStandardTrend,
                    trendID: newStandardTrend.id,
                    archive: '',
                    startDate: chartInfo.startDate,
                    endDate: chartInfo.endDate,
                  });
                }
              },
              (error: string) => this.setError(error)
            )
          )
        )
      );
    }
  );

  readonly getLoadDataExplorerCharts = this.effect(
    (charts$: Observable<LoadDataExplorerCharts>) => {
      return charts$.pipe(
        tap(() => this.setLoading()),
        switchMap((chartInfo: LoadDataExplorerCharts) =>
          this.processDataFrameworkService.getTrends(chartInfo.nodeID).pipe(
            tap(() => this.setLoadingTrends()),
            tapResponse(
              (response) => {
                let trends = updateTrendDefinition(response);
                let trendID = chartInfo?.trendID;
                if (!trendID) {
                  trendID = getSelectedTrendID(trends);
                } else if (trends && !trends.find((t) => t.id === trendID)) {
                  trendID = getSelectedTrendID(trends);
                }
                const isDirty: Record<string, boolean> = {};
                trends = produce(trends, (state) => {
                  state.forEach((trend, index) => {
                    if (trend?.id) {
                      isDirty[trend.id] = false;

                      if (+trend?.id === -1) {
                        isDirty[trend.id] = true;
                        state[index].isNew = true;
                      }
                    }
                  });
                });
                this.setTrends({
                  trends,
                  isDirty,
                  uniqueID: chartInfo.nodeID,
                  selectedTrendID: trendID,
                  start: chartInfo.startDate,
                  end: chartInfo.endDate,
                });

                const trend = findTrend(trendID, trends);
                if (trend[0]) {
                  this.getTrendData({
                    trend: trend[1],
                    trendID,
                    archive: '',
                    startDate: chartInfo.startDate,
                    endDate: chartInfo.endDate,
                  });
                } else if (trends && trends?.length > 0) {
                  this.getTrendData({
                    trend: trends[0],
                    trendID: trends[0].id,
                    archive: '',
                    startDate: chartInfo.startDate,
                    endDate: chartInfo.endDate,
                  });
                } else {
                  this.patchState((state) => ({
                    selectedTrend: null,
                    trends: null,
                    isDirty: {},
                    formElements: {
                      id: '',
                      title: '',
                      series: [],
                      groupedSeries: [],
                      groupedSeriesAxis: null,
                      groupedSeriesBars: [],
                      axes: [],
                      pins: [],
                      curves: [],
                      advanced: null,
                      isXY: false,
                    }, // tabs needs to update
                  }));
                  this.setError('No Data');
                }
              },
              (error: string) => this.setError(error)
            )
          )
        )
      );
    }
  );

  readonly changeLabelsClicked = this.effect((index$: Observable<number>) => {
    return index$.pipe(
      withLatestFrom(this.query.vm$),
      tap(([index, vm]: [number, ChartState]) => {
        const newState = produce(vm.selectedTrend, (state) => {
          state.labelIndex = index;
        });
        this.patchState((state) => ({
          selectedTrend: newState,
          labelIndex: index as LabelIndex,
        }));
      })
    );
  });

  readonly downloadChart = this.effect(
    (chartOptions$: Observable<Highcharts.Options>) => {
      return chartOptions$.pipe(
        withLatestFrom(this.query.isGroupedSeries$, this.query.selectedTrend$),
        switchMap(([chartOptions, isGroupedSeries, trend]) => {
          return this.imagesFrameworkService
            .saveTempCSV(isGroupedSeries, trend, chartOptions)
            .pipe(
              tapResponse(
                (response) => {
                  window.open(
                    this.appConfig.baseServicesURL +
                      '/api/Images/TempCSV?id=' +
                      response,
                    '_blank'
                  );
                  window.focus();
                },
                (error: string) => {
                  console.error(JSON.stringify(error));
                }
              )
            );
        })
      );
    }
  );

  readonly editChartLegacy = this.effect((chart$) => {
    return chart$.pipe(
      withLatestFrom(this.query.vm$),
      tap(([_, vm]: [any, ChartState]) => {
        const start = String(vm.start.getTime());
        const end = String(vm.end.getTime());
        window
          .open(
            this.appConfig.baseSiteURL +
              '/ChartEditor/#!?asset=' +
              vm.uniqueID +
              '&start=' +
              start +
              '&end=' +
              end +
              '&trend=' +
              vm.selectedTrendID,
            '_blank'
          )
          .focus();
      })
    );
  });

  readonly updateChartYAxisLimit = this.effect(
    (limits$: Observable<IUpdateLimitsData>) => {
      return limits$.pipe(
        withLatestFrom(
          this.query.selectedTrend$,
          this.query.formElements$,
          this.query.isDirty$
        ),
        tap(
          ([limits, selectedTrend, formElements, isDirty]: [
            IUpdateLimitsData,
            IProcessedTrend,
            ChartFormElements,
            Record<string, boolean>
          ]) => {
            const axes = updateAxes(selectedTrend?.trendDefinition, limits);
            const newState = produce(selectedTrend, (state) => {
              state.trendDefinition.Axes = axes;
            });
            const newAxesFormElements = produce(formElements.axes, (state) => {
              state = axes;
            });
            const dirtyState = produce(isDirty, (state) => {
              state[selectedTrend.id] = true;
            });
            this.patchState((state) => ({
              selectedTrend: newState,
              isDirty: dirtyState,
              formElements: {
                ...state.formElements,
                axes: newAxesFormElements,
              }, // axis tab needs to update
            }));
          }
        )
      );
    }
  );

  readonly resetChartYAxisLimit = this.effect(
    (axisIndex$: Observable<number>) => {
      return axisIndex$.pipe(
        withLatestFrom(this.query.selectedTrend$, this.query.formElements$),
        tap(
          ([axisIndex, selectedTrend, formElements]: [
            number,
            IProcessedTrend,
            ChartFormElements
          ]) => {
            const axes = resetAxes(selectedTrend?.trendDefinition, axisIndex);
            const newState = produce(selectedTrend, (state) => {
              state.trendDefinition.Axes = axes;
            });
            const newFormElements = produce(formElements, (state) => {
              state.axes = axes;
            });
            this.patchState((state) => ({
              selectedTrend: newState,
              formElements: newFormElements, // axis tab needs to update
            }));
          }
        )
      );
    }
  );

  readonly setTrendDirty = this.effect(
    (trend$: Observable<LoadDataExplorerTrends>) => {
      return trend$.pipe(
        debounceTime(500),
        switchMap((req: LoadDataExplorerTrends) => {
          this.setLoadingTrends();
          return this.processDataFrameworkService
            .getTagsDataFiltered(
              req.trend.trendDefinition,
              req.startDate,
              req.endDate,
              0,
              req.archive,
              req.trend.assetID
            )
            .pipe(
              tapResponse(
                (response: IAssetMeasurementsSet) => {
                  const selectedTrend: IProcessedTrend = setDataOnTrend(
                    req.trend,
                    response
                  );
                  selectedTrend.startDate = req.startDate;
                  selectedTrend.endDate = req.endDate;
                  selectedTrend.isDirty = true;
                  this.setTimeSeriesOnTrend(selectedTrend);
                },
                (error: string) => this.setError(error)
              )
            );
        })
      );
    }
  );

  readonly getTrendData = this.effect(
    (trend$: Observable<LoadDataExplorerTrends>) => {
      return trend$.pipe(
        // tap((trend) => updateTrend(trend.trendID, this.router)),
        tap(() =>
          this.updater((state: ChartState) => {
            console.log(state);

            return {
              ...state,
              trendsLoading: true,
            };
          })
        ),
        filter(
          (req: LoadDataExplorerTrends) => req.trend?.trendDefinition !== null
        ),
        debounceTime(500),
        switchMap((req: LoadDataExplorerTrends) => {
          this.setLoadingTrends();
          return this.processDataFrameworkService
            .getTagsDataFiltered(
              req.trend.trendDefinition,
              req.startDate,
              req.endDate,
              0,
              req.archive,
              req.trend.assetID
            )
            .pipe(
              tapResponse(
                (response: IAssetMeasurementsSet) => {
                  const selectedTrend: IProcessedTrend = setDataOnTrend(
                    req.trend,
                    response
                  );
                  selectedTrend.startDate = req.startDate;
                  selectedTrend.endDate = req.endDate;
                  this.setTimeSeriesOnTrend(selectedTrend);
                },
                (error: string) => this.setError(error)
              )
            );
        })
      );
    }
  );

  private readonly reset = this.updater((state: ChartState) => ({
    ...state,
    initialChartState,
  }));

  dataCursorToggle() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (vm.selectedTrend.showDataCursor !== false) {
          state.showDataCursor = false;
          window.sessionStorage.setItem(
            'ATX_SHOW_DATA_CURSOR',
            JSON.stringify(false)
          );
          this.logger.trackTaskCenterEvent('DataExplorerV2:HideDataCursor');
        } else {
          state.showDataCursor = true;
          window.sessionStorage.setItem(
            'ATX_SHOW_DATA_CURSOR',
            JSON.stringify(true)
          );
        }
      });
      this.setSelectedTrend(newState);
    });
  }

  refreshCharts(newTrendName: string, newTrendID: string, oldTrendID: string) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.refreshAfterSave({
        newTrendName,
        nodeID: vm.assetGuid,
        newTrendID,
        oldTrendID,
        startDate: vm.start,
        endDate: vm.end,
        trends: vm.trends,
        dirtyTrends: vm.dirtyTrends,
        isDirty: vm.isDirty,
      });
    });
  }

  refreshAfterSave(saveState: RefreshDataExplorerCharts) {
    // We need to match legacy behavior for now
    // It gets complicated depending on what the user just did
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newChartCreated =
        saveState.oldTrendID !== saveState.newTrendID ? true : false;

      if (newChartCreated) {
        // punt.. grab the new state from the server
        this.refreshChartsAfterSave(saveState);
      } else {
        // added to force reload chart data from server
        this.refreshChartsAfterSave(saveState);
        const isDirty = this.updateDirtyList(vm.trends, vm.isDirty, [
          saveState.newTrendID,
        ]);
        // update trend in trend list with selected trend:
        const newTrends = produce(vm.trends, (state) => {
          const trendIndex = vm.trends.findIndex(
            (trend) => trend.id === saveState.newTrendID
          );
          if (trendIndex !== -1) {
            state[trendIndex] = vm.selectedTrend;
          }
        });
        const dirtyTrends = produce(vm.dirtyTrends, (state) => {
          state.delete(vm.selectedTrendID);
        });
        this.patchState((state) => ({
          isDirty: isDirty,
          trends: newTrends,
          dirtyTrends,
        }));
      }
    });
  }

  deleteChart(chartID: string) {
    if (chartID) {
      this.query.vm$.pipe(take(1)).subscribe((vm) => {
        if (!vm.selectedTrend.trendDefinition.IsStandardTrend) {
          let updateSelectedID = false;
          const newTrends = produce(vm.trends, (state) => {
            const trendIndex = vm.trends.findIndex(
              (trend) => trend.id === chartID
            );
            if (trendIndex !== -1) {
              state.splice(trendIndex, 1);
              updateSelectedID = true;
            }
          });
          const isDirty = this.updateDirtyList(vm.trends, vm.isDirty, [
            chartID,
          ]);
          const dirtyTrends = produce(vm.dirtyTrends, (state) => {
            if (vm.dirtyTrends[chartID]) {
              state.delete(chartID);
            }
          });

          if (updateSelectedID) {
            let selectedTrend =
              newTrends && newTrends.length > 0 ? newTrends[0] : null;
            if (vm.isDirty[selectedTrend.id]) {
              selectedTrend = vm.dirtyTrends.get(selectedTrend.id);
            }
            let formElements = {
              id: '',
              title: '',
              series: [],
              groupedSeries: [],
              groupedSeriesAxis: null,
              groupedSeriesBars: [],
              axes: [],
              pins: [],
              curves: [],
              advanced: null,
              isXY: vm.isXY,
            };
            let errorNoData = true;
            if (newTrends && newTrends.length > 0) {
              formElements = {
                id: selectedTrend.id,
                title: selectedTrend?.trendDefinition?.Title,
                series: selectedTrend?.trendDefinition?.Series,
                groupedSeries:
                  selectedTrend?.trendDefinition?.BandAxisMeasurements ?? [],
                groupedSeriesAxis: selectedTrend?.trendDefinition?.BandAxis,
                groupedSeriesBars:
                  selectedTrend?.trendDefinition?.BandAxisBars ?? [],
                axes: selectedTrend?.trendDefinition?.Axes,
                pins: selectedTrend?.trendDefinition?.Pins,
                curves: selectedTrend?.trendDefinition?.DesignCurves
                  ? selectedTrend.trendDefinition.DesignCurves
                  : [],
                advanced: {
                  filter: selectedTrend?.trendDefinition?.Filter,
                  dataRetrievalMethod:
                    selectedTrend?.trendDefinition?.DataRetrieval?.Method,
                  dataRetrievalMinInterval:
                    selectedTrend?.trendDefinition?.DataRetrieval?.MinInterval,
                  dataRetrievalMinIntervalUnits:
                    selectedTrend?.trendDefinition?.DataRetrieval
                      ?.MinIntervalUnits,
                  dataRetrievalArchive:
                    selectedTrend?.trendDefinition?.DataRetrieval?.Archive,
                },
                isXY: vm.isXY,
              };
              errorNoData = false;
            }
            this.patchState((state) => ({
              selectedTrend,
              selectedTrendID: selectedTrend?.id ? selectedTrend.id : null,
              trends: newTrends,
              errorNoData,
              dirtyTrends,
              isDirty,
              formElements, // tabs needs to update
            }));
            if (!errorNoData) {
              this.setLoadingTrends();
              this.getTrendData({
                trend: selectedTrend,
                trendID: selectedTrend?.id,
                archive: '',
                startDate: vm.start,
                endDate: vm.end,
              });
            } else {
              this.patchState((state) => ({
                isGroupedSeries: false,
              }));
            }
          }
        } else {
          this.refreshStandardChartAfterRevert({
            nodeID: vm.assetGuid,
            selectedTrend: vm.selectedTrend,
            startDate: vm.start,
            endDate: vm.end,
            trends: vm.trends,
            dirtyTrends: vm.dirtyTrends,
            isDirty: vm.isDirty,
          });
        }
      });
    }
  }

  addNewChart(isGroupedSeries: boolean) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      let trendID = vm.trends
        ? // eslint-disable-next-line no-unsafe-optional-chaining
          Math.min(...vm.trends?.map((trend) => +trend.id))
        : 0;
      if (trendID < 0) {
        trendID -= 1;
      } else {
        trendID = -1;
      }
      const newTrendNumber = Math.abs(trendID);
      const newTrend: IPDTrend = {
        PDTrendID: null,
        TrendDesc: `New Trend ${newTrendNumber}`,
        Title: `New Trend ${newTrendNumber}`,
        LegendVisible: null,
        LegendDock: null,
        LegendContentLayout: null,
        IsCustom: true,
        CreatedBy: '',
        IsPublic: true,
        DisplayOrder: 10,
        ChartTypeID: AtxChartLine,
        ChartType: null,
        Series: [],
        Axes: [],
        IsStandardTrend: false,
        Filter: null,
        DataRetrieval: {
          Method: 0, //0 = default, 1 = archive name, 2 = interval & units
          MinInterval: 1,
          MinIntervalUnits: 'minutes',
          Archive: '',
          Period: 0,
        },
        Pins: [],
        PinTypeID: AtxPinTypeUnmodified,
        ShowPins: false,
        ShowSelected: true,
        SummaryType: null,
        Categories: [],
        XAxisGridlines: false,
      };
      let groupedSeriesType = GroupedSeriesType.NONE;
      let groupedSeriesSubType = '';
      if (isGroupedSeries) {
        const bandAxis: IPDTrendBandAxisBar = {
          PDTrendBandAxisBarID: -1,
          PDTrendID: null,
          AssetID: null,
          Derived: false,
          Label: '',
          TagID: 0,
          ValueType: null,
          TimeDivision: 'month',
          Measurements: [],
        };
        groupedSeriesType = GroupedSeriesType.TIME;
        if (vm.start && vm.end) {
          const diff = Math.abs(vm.end.getTime() - vm.start.getTime());
          if (diff < 1000 * 60 * 60 * 24 * 7) {
            //If the time range is less than 1 week default to 1 day
            bandAxis.TimeDivision = 'day';
            groupedSeriesSubType = 'day';
          } else if (diff < 1000 * 60 * 60 * 24 * 7 * 4) {
            //If the time range is less than 4
            bandAxis.TimeDivision = 'week';
            groupedSeriesSubType = 'week';
          } else if (diff < 1000 * 60 * 60 * 24 * 7 * 4 * 104) {
            //If the time range is less than 416 weeks default to 1 month
            bandAxis.TimeDivision = 'month';
            groupedSeriesSubType = 'month';
          } else {
            //If the time range is greater than 416 weeks (8 years) default to 1 year.
            bandAxis.TimeDivision = 'year';
            groupedSeriesSubType = 'year';
          }
        }
        newTrend.ChartTypeID = AtxChartTrend;
        newTrend.BandAxisBars = [bandAxis];
        newTrend.BandAxis = {
          BarWidth: null,
          IsVertical: false,
          Label: '',
          Opposite: false,
          PDTrendBandAxisID: -1,
          PDTrendID: null,
          RenderAs: '',
          Summarize: '',
          Clockwise: null,
          StartAngle: null,
          EndAngle: null,
        };
        newTrend.BandAxisMeasurements = [];
      }
      const newChart: IProcessedTrend = {
        id: trendID.toString(),
        label: `New Trend ${newTrendNumber}`,
        trendDefinition: newTrend,
        groupSeriesMeasurements: [],
        groupedSeriesType,
        groupedSeriesSubType,
        totalSeries: 0,
        isNew: true,
        startDate: vm.start,
        endDate: vm.end,
        assetID: vm.assetGuid,
      };
      let newTrends = [newChart];
      if (vm.trends) {
        newTrends = produce(vm.trends, (state) => {
          state.push(newChart);
        });
        if (vm.isDirty[vm.selectedTrendID]) {
          const dirtyTrends = produce(vm.dirtyTrends, (state) => {
            const trendNoMeasurements = produce(vm.selectedTrend, (state) => {
              state.measurements = null;
            });
            state.set(vm.selectedTrendID, trendNoMeasurements);
          });
          this.patchState((state) => ({
            dirtyTrends,
          }));
        }
      }
      const isDirty = produce(vm.isDirty, (state) => {
        state[trendID.toString()] = true;
      });

      this.patchState((state) => ({
        trends: newTrends,
        formElements: {
          id: newChart.id,
          title: newChart.trendDefinition.Title,
          series: newChart.trendDefinition.Series,
          groupedSeries: newChart?.groupSeriesMeasurements ?? [],
          groupedSeriesAxis: newChart?.trendDefinition?.BandAxis,
          groupedSeriesBars: newChart?.trendDefinition?.BandAxisBars ?? [],
          axes: newChart.trendDefinition.Axes,
          pins: newChart.trendDefinition.Pins,
          curves: newChart.trendDefinition.DesignCurves
            ? newChart.trendDefinition.DesignCurves
            : [],
          advanced: {
            filter: newChart?.trendDefinition?.Filter,
            dataRetrievalMethod:
              newChart?.trendDefinition?.DataRetrieval?.Method,
            dataRetrievalMinInterval:
              newChart?.trendDefinition?.DataRetrieval?.MinInterval,
            dataRetrievalMinIntervalUnits:
              newChart?.trendDefinition?.DataRetrieval?.MinIntervalUnits,
            dataRetrievalArchive:
              newChart?.trendDefinition?.DataRetrieval?.Archive,
          },
          isXY: false,
        },
        selectedTrend: newChart,
        isDirty,
        chartError: null,
        errorNoData: false,
        selectedTrendID: trendID.toString(),
      }));
      this.getTrendData({
        trend: newChart,
        trendID: trendID.toString(),
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  addNewCurve() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        !Array.isArray(vm.selectedTrend.trendDefinition.DesignCurves) ||
        vm.selectedTrend.trendDefinition.Axes.length < 1
      ) {
        return;
      }
      const newState = produce(vm.selectedTrend, (state) => {
        let x: number | Date = 0;
        if (!vm.isXY) {
          x = getADayAgo();
        }
        const curveValues = [
          {
            X: x,
            Y: 0,
            Y1: 0,
            Y2: 0,
          },
        ];
        let curveID = vm.selectedTrend.trendDefinition.DesignCurves
          ? Math.min(
              ...vm.selectedTrend.trendDefinition.DesignCurves.map(
                (trend) => +trend.PDTrendDesignCurveID
              )
            )
          : 0;
        if (curveID < 0) {
          curveID -= 1;
        } else {
          curveID = -1;
        }
        const newCurve: DesignCurve = {
          Axis: vm.selectedTrend.trendDefinition.Axes[0].Axis,
          DisplayText: 'New Design Curve',
          Color: '#FF0000',
          PDTrendDesignCurveID: curveID,
          PDTrendID: vm.selectedTrend.trendDefinition.PDTrendID,
          Repeat: '',
          Type: 'line',
          Values: JSON.stringify(curveValues),
        };
        state.trendDefinition.DesignCurves.push(newCurve);
      });

      const newFormElements = produce(vm.formElements, (state) => {
        state.curves = newState.trendDefinition.DesignCurves;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
        formElements: newFormElements, // curves tab needs to update
      }));
    });
  }

  removeCurve(curveIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        !Array.isArray(vm.selectedTrend.trendDefinition.DesignCurves) ||
        vm.selectedTrend.trendDefinition.DesignCurves.length < curveIndex + 1
      ) {
        return;
      }
      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.DesignCurves.splice(curveIndex, 1);
      });
      const newFormElements = produce(vm.formElements, (state) => {
        state.curves = newState.trendDefinition.DesignCurves;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
        formElements: newFormElements, // curves tab needs to update
      }));
    });
  }

  addCurvePoint(curveIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        !Array.isArray(vm.selectedTrend.trendDefinition.DesignCurves) ||
        vm.selectedTrend.trendDefinition.DesignCurves.length < curveIndex + 1
      ) {
        return;
      }
      let x: number | Date = 0;
      if (!vm.isXY) {
        x = getADayAgo();
      }
      const curveValues = {
        X: x,
        Y: 0,
        Y1: 0,
        Y2: 0,
      };
      const newState = produce(vm.selectedTrend, (state) => {
        const values: {
          X: number | Date;
          Y: any;
          Y1: any;
          Y2: any;
        }[] = JSON.parse(
          vm.selectedTrend.trendDefinition.DesignCurves[curveIndex].Values
        );
        values.push(curveValues);
        state.trendDefinition.DesignCurves[curveIndex].Values =
          JSON.stringify(values);
      });

      const newFormElements = produce(vm.formElements, (state) => {
        state.curves = newState.trendDefinition.DesignCurves;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
        formElements: newFormElements, // curves tab needs to update
      }));
    });
  }

  removeCurvePoint(curveIndex: number, pointIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (
        !Array.isArray(vm.selectedTrend.trendDefinition.DesignCurves) ||
        vm.selectedTrend.trendDefinition.DesignCurves.length < curveIndex + 1
      ) {
        return;
      }
      const newState = produce(vm.selectedTrend, (state) => {
        const values: {
          X: number | Date;
          Y: any;
          Y1: any;
          Y2: any;
        }[] = JSON.parse(
          vm.selectedTrend.trendDefinition.DesignCurves[curveIndex].Values
        );
        if (values.length > 1) {
          values.splice(pointIndex, 1);
        }
        state.trendDefinition.DesignCurves[curveIndex].Values =
          JSON.stringify(values);
      });

      const newFormElements = produce(vm.formElements, (state) => {
        state.curves = newState.trendDefinition.DesignCurves;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
        formElements: newFormElements, // curves tab needs to update
      }));
    });
  }

  removeAxis(axisIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const axisToDelete = vm.selectedTrend.trendDefinition.Axes.findIndex(
        (axis) => {
          return axis.Axis === axisIndex;
        }
      );
      if (axisToDelete > -1) {
        const newState = produce(vm.selectedTrend, (state) => {
          state.trendDefinition.Axes.splice(axisToDelete, 1);
        });
        const newFormElements = produce(vm.formElements, (state) => {
          state.axes = newState.trendDefinition.Axes;
        });
        const isDirty = produce(vm.isDirty, (state) => {
          state[vm.selectedTrend.id] = true;
        });
        this.patchState((state) => ({
          selectedTrend: newState,
          isDirty,
          formElements: newFormElements, // axis tab needs to update
        }));
      }
    });
  }

  addNewAxis() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newAxis = createNewAxis(
        'New Axis',
        vm.selectedTrend?.trendDefinition.Axes
      );
      const newState = produce(vm.selectedTrend, (state) => {
        if (state.trendDefinition.Axes.length == 0) {
          newAxis.GridLine = true;
        }
        state.trendDefinition.Axes.push(newAxis);
      });
      const newFormElements = produce(vm.formElements, (state) => {
        state.axes = newState.trendDefinition.Axes;
        state.series = newState?.trendDefinition?.Series;
        state.groupedSeries = newState?.groupSeriesMeasurements ?? [];
        state.groupedSeriesAxis = newState?.trendDefinition?.BandAxis;
        state.groupedSeriesBars = newState?.trendDefinition?.BandAxisBars ?? [];
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
        formElements: newFormElements, // axis tab needs to update
      }));
    });
  }

  gridLineXAxisToggle() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.XAxisGridlines =
          !state.trendDefinition.XAxisGridlines;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        selectedTrend: newState,
        isDirty,
      }));
    });
  }

  updateTimeRange(start: Date, end: Date) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();
      this.getTrendData({
        trend: vm.selectedTrend,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: start,
        endDate: end,
      });
    });
  }

  refreshTrends(selectedTrend: IProcessedTrend) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setSelectedTrend(selectedTrend);
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.setLoadingTrends();
      this.getTrendData({
        trend: selectedTrend,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  selectTrendDropdown(trendID: string) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();
      if (vm.isDirty[vm.selectedTrendID]) {
        const dirtyTrends = produce(vm.dirtyTrends, (state) => {
          const trendNoMeasurements = produce(vm.selectedTrend, (state) => {
            state.measurements = null;
          });
          state.set(vm.selectedTrendID, trendNoMeasurements);
        });
        this.patchState((state) => ({
          dirtyTrends,
        }));
      }
      let processedTrend: IProcessedTrend = vm.dirtyTrends?.get(trendID);
      if (!processedTrend) {
        const trend = findTrend(trendID, vm.trends);
        processedTrend = trend[1];
      }
      if (processedTrend) {
        processedTrend = { ...processedTrend };
        if (window.sessionStorage.getItem('ATX_SHOW_DATA_CURSOR')) {
          processedTrend.showDataCursor = JSON.parse(
            window.sessionStorage.getItem('ATX_SHOW_DATA_CURSOR')
          );
        } else {
          processedTrend.showDataCursor = true;
        }

        this.updater((state: ChartState) => {
          return {
            ...state,
            selectedTrendID: trendID,
          };
        });
        this.getTrendData({
          trend: processedTrend,
          trendID,
          archive: '',
          startDate: vm.start,
          endDate: vm.end,
        });
      }
    });
  }

  seriesXAxisToggle(seriesIndex: number, series: IPDTrendSeries) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedTrend?.trendDefinition) {
        let scatterPlot = true;
        const selectedTrend = produce(vm.selectedTrend, (state) => {
          const addingXAxis =
            series.IsXAxis === null
              ? true
              : series.IsXAxis === false
              ? true
              : false;
          const isXAxis = vm.selectedTrend.trendDefinition.Series.some(
            (s) => s.IsXAxis
          );
          if (addingXAxis) {
            state.trendDefinition.Series[seriesIndex].IsXAxis = true;
            if (isXAxis) {
              state.trendDefinition.Series.forEach((s, i) => {
                if (s.IsXAxis && i !== seriesIndex) {
                  s.IsXAxis = false;
                }
              });
            } else {
              state.trendDefinition.ChartTypeID = AtxScatterPlot;
            }
          } else {
            state.trendDefinition.Series[seriesIndex].IsXAxis = false;
            scatterPlot = false;
            if (
              isXAxis &&
              state.trendDefinition.ChartTypeID === AtxScatterPlot
            ) {
              state.trendDefinition.ChartTypeID = AtxChartLine;
            }
          }
        });
        const isDirty = produce(vm.isDirty, (state) => {
          state[vm.selectedTrend.id] = true;
        });
        this.setDirty(isDirty);
        this.patchState((state) => ({
          isXY: scatterPlot,
          selectedTrend,
        }));
      }
    });
  }

  seriesBoldToggle(seriesIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      if (vm.selectedTrend.trendDefinition.Series[seriesIndex]) {
        const newState = produce(vm.selectedTrend, (state) => {
          if (vm.selectedTrend.trendDefinition.Series[seriesIndex].isBold) {
            state.trendDefinition.Series[seriesIndex].isBold = false;
          } else {
            state.trendDefinition.Series[seriesIndex].isBold = true;
          }
        });
        this.setSelectedTrend(newState);
      }
    });
  }

  activeToggle() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      let callApi = false;
      if (!vm.selectedTrend?.trendDefinition?.ShowSelected) {
        callApi = true;
      }
      if (callApi) {
        this.setLoadingTrends();
        const newState = produce(vm.selectedTrend, (state) => {
          if (vm.selectedTrend.trendDefinition?.Pins?.length > 0) {
            vm.selectedTrend.trendDefinition.Pins.forEach((pin, idx) => {
              state.trendDefinition.Pins[idx].Hidden = false;
            });
          }
          state.trendDefinition.ShowPins = true;
          state.trendDefinition.ShowSelected =
            !state.trendDefinition.ShowSelected;
        });
        this.getTrendData({
          trend: newState,
          trendID: vm.selectedTrendID,
          archive: '',
          startDate: vm.start,
          endDate: vm.end,
        });
      } else {
        const newState = produce(vm.selectedTrend, (state) => {
          state.trendDefinition.ShowSelected =
            !state.trendDefinition.ShowSelected;
        });
        this.setSelectedTrend(newState);
      }
    });
  }

  pinHideToggle(pinIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (state.trendDefinition.Pins[pinIndex]?.Hidden) {
          state.trendDefinition.Pins[pinIndex].Hidden = false;
          state.trendDefinition.ShowPins = true;
        } else {
          state.trendDefinition.Pins[pinIndex].Hidden = true;
          // are all hidden
          let allHidden = true;
          state.trendDefinition.Pins.forEach((pin) => {
            if (!pin.Hidden) {
              allHidden = false;
            }
          });
          if (allHidden) {
            state.trendDefinition.ShowPins = false;
          }
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.setSelectedTrend(newState);
    });
  }

  pinHideAllToggle() {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        let hidePins = false;
        if (state.trendDefinition.ShowPins) {
          hidePins = true;
        }
        state.trendDefinition.Pins.forEach((pin, idx) => {
          state.trendDefinition.Pins[idx].Hidden = hidePins;
        });
        state.trendDefinition.ShowPins =
          !vm.selectedTrend.trendDefinition.ShowPins;
      });
      this.setSelectedTrend(newState);
    });
  }

  updateGroupSeriesType(seriesType: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        state.groupedSeriesType = seriesType;
        processBandAxisBarsAndMeasurements(state);
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        isDirty,
      }));
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  updateSeriesState(seriesFilterState: SeriesFilterState[]) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        seriesFilterState.forEach((seriesFilter, seriesFilterIndex) => {
          if (seriesFilter.filterMin.valueChanged) {
            state.trendDefinition.Series = state.trendDefinition.Series.map(
              (series, i) => {
                let newSeries = { ...series };
                if (i === seriesFilterIndex) {
                  newSeries.FilterMin = seriesFilter.filterMin.currentValue;
                  newSeries.showFilter = false;
                  newSeries = finishFilterProcessing(newSeries);
                }
                return newSeries;
              }
            );
          }
          if (seriesFilter.filterMax.valueChanged) {
            state.trendDefinition.Series = state.trendDefinition.Series.map(
              (series, i) => {
                let newSeries = { ...series };
                if (i === seriesFilterIndex) {
                  newSeries.FilterMax = seriesFilter.filterMax.currentValue;
                  newSeries.showFilter = false;
                  newSeries = finishFilterProcessing(newSeries);
                }
                return newSeries;
              }
            );
          }
          if (seriesFilter.flatlineThreshold.valueChanged) {
            state.trendDefinition.Series = state.trendDefinition.Series.map(
              (series, i) => {
                let newSeries = { ...series };
                if (i === seriesFilterIndex) {
                  newSeries.FlatlineThreshold =
                    seriesFilter.flatlineThreshold.currentValue;
                  newSeries.showFilter = false;
                  newSeries = finishFilterProcessing(newSeries);
                }
                return newSeries;
              }
            );
          }
          if (seriesFilter.applyFilterToAll.valueChanged) {
            state.trendDefinition.Series = state.trendDefinition.Series.map(
              (series, i) => {
                let newSeries = { ...series };
                if (i === seriesFilterIndex) {
                  newSeries.ApplyToAll =
                    seriesFilter.applyFilterToAll.currentValue;
                  newSeries.showFilter = false;
                  newSeries = finishFilterProcessing(newSeries);
                }
                return newSeries;
              }
            );
          }
        });
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  updateGroupedSeriesState(seriesState: SeriesState[]) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        seriesState.forEach((series, seriesIndex) => {
          if (series.seriesType.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Type = series.seriesType.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.aggregationType1.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.AggregationType1 =
                    +series.aggregationType1.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.aggregationType2.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.AggregationType2 =
                    +series.aggregationType2.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.aggregationType3.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.AggregationType3 =
                    +series.aggregationType3.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.aggregationType4.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.AggregationType4 =
                    +series.aggregationType4.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.aggregationType5.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.AggregationType5 =
                    +series.aggregationType5.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.value1.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Value1 = series.value1.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.value2.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Value2 = series.value2.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.value3.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Value3 = series.value3.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.value4.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Value4 = series.value4.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.value5.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Value5 = series.value5.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.tagID1.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.TagID1 = series.tagID1.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.tagID2.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.TagID2 = series.tagID2.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.tagID3.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.TagID3 = series.tagID3.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.tagID4.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.TagID4 = series.tagID4.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.tagID5.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.TagID5 = series.tagID5.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.params1.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Params1 = series.params1.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.params2.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Params2 = series.params2.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.params3.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Params3 = series.params3.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.params4.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Params4 = series.params4.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.params5.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Params5 = series.params5.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.dataSource1.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  if (
                    series.dataSource1.currentValue ===
                    DataSourceValues.Constant
                  ) {
                    newMeasurement.Value1 = +series.value1.currentValue;
                  } else {
                    newMeasurement.Value1 = null;
                  }
                }
                return newMeasurement;
              }
            );
          }
          if (series.dataSource2.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  if (
                    series.dataSource2.currentValue ===
                    DataSourceValues.Constant
                  ) {
                    newMeasurement.Value2 = +series.value2.currentValue;
                  } else {
                    newMeasurement.Value2 = null;
                  }
                }
                return newMeasurement;
              }
            );
          }
          if (series.dataSource3.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  if (
                    series.dataSource3.currentValue ===
                    DataSourceValues.Constant
                  ) {
                    newMeasurement.Value3 = +series.value3.currentValue;
                  } else {
                    newMeasurement.Value3 = null;
                  }
                }
                return newMeasurement;
              }
            );
          }
          if (series.dataSource4.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  if (
                    series.dataSource4.currentValue ===
                    DataSourceValues.Constant
                  ) {
                    newMeasurement.Value4 = +series.value4.currentValue;
                  } else {
                    newMeasurement.Value4 = null;
                  }
                }
                return newMeasurement;
              }
            );
          }
          if (series.dataSource5.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  if (
                    series.dataSource5.currentValue ===
                    DataSourceValues.Constant
                  ) {
                    newMeasurement.Value5 = +series.value5.currentValue;
                  } else {
                    newMeasurement.Value5 = null;
                  }
                }
                return newMeasurement;
              }
            );
          }
          if (series.symbol.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Symbol = series.symbol.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.outlierType.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.Flags = series.outlierType.currentValue;
                }
                return newMeasurement;
              }
            );
          }
          if (series.groupName.valueChanged) {
            state.groupSeriesMeasurements = state.groupSeriesMeasurements.map(
              (measurement, i) => {
                const newMeasurement = { ...measurement };
                if (i === seriesIndex) {
                  newMeasurement.BarName = series.groupName.currentValue;
                }
                return newMeasurement;
              }
            );
          }
        });
        processBandAxisBarsAndMeasurements(state);
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  updateGroupSeriesSubType(subType: string) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        state.groupedSeriesSubType = subType;
        if (
          vm.selectedTrend.trendDefinition.BandAxisBars &&
          vm.selectedTrend.trendDefinition.BandAxisBars.length > 0
        ) {
          state.trendDefinition.BandAxisBars =
            state.trendDefinition.BandAxisBars.map((bar) => {
              const newBar = { ...bar };
              newBar.TimeDivision = subType;
              return newBar;
            });
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  pinTypeChange(pinTypeID: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.PinTypeID = pinTypeID;
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.setSelectedTrend(newState);
    });
  }

  editPin(pinIndex: number, editPin: IPDTrendPin) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (vm.selectedTrend.trendDefinition?.Pins?.length > pinIndex) {
          state.trendDefinition.Pins[pinIndex] = editPin;
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        isDirty,
      }));

      // we need to update pin information from server
      this.setLoadingTrends();
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  removePin(pinIndex: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newState = produce(vm.selectedTrend, (state) => {
        if (vm.selectedTrend.trendDefinition?.Pins?.length > 0) {
          state.trendDefinition.Pins.splice(pinIndex, 1);
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        isDirty,
      }));

      const newFormElements = produce(vm.formElements, (state) => {
        state.pins = newState.trendDefinition.Pins;
      });

      if (newState.trendDefinition.Pins.length === 0) {
        // If there are no pins left
        // we need to get active series from server
        this.setLoadingTrends();
        this.getTrendData({
          trend: newState,
          trendID: vm.selectedTrendID,
          archive: '',
          startDate: vm.start,
          endDate: vm.end,
        });
      } else {
        this.setFormElements(newFormElements);
        this.setSelectedTrend(newState);
      }
    });
  }

  addNewPin(
    newCriteriaPin: ICriteria,
    startDate: Date,
    endDate: Date,
    units: string
  ) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      let pDTrendPinID = Math.min(
        // eslint-disable-next-line no-unsafe-optional-chaining
        ...vm.selectedTrend.trendDefinition?.Pins?.map(
          (item) => item.PDTrendPinID
        )
      );
      if (pDTrendPinID < 0) {
        pDTrendPinID -= 1;
      } else {
        pDTrendPinID = -1;
      }
      const now = new Date();
      const newPin: IPDTrendPin = {
        PDTrendID: vm.selectedTrend.trendDefinition.PDTrendID,
        NameFormat: 'New Pin',
        Name: 'New Pin',
        CriteriaObjectId: newCriteriaPin.CoID,
        CreateDate: now,
        ChangeDate: now,
        CreatedBy: '',
        ChangedBy: '',
        TempPinName: '',
        EditName: false,
        Filters: [],
        PinColor: 0,
        PDTrendPinID: pDTrendPinID,
        EndTime: null,
        Hidden: null,
        SelectedCriteriaObject: null,
        span: null,
        SpanUnits: null,
        StartTime: null,
      };
      if (newCriteriaPin.CoID === -1) {
        newPin.CriteriaObjectId = null;
        newPin.StartTime = startDate;
        newPin.EndTime = endDate;
        newPin.SpanUnits = units;
      }
      let trendFilter: IPDTrendFilter;
      if (vm.selectedTrend.trendDefinition.Filter) {
        trendFilter = {
          FilterMin: vm.selectedTrend.trendDefinition.Filter.FilterMin,
          FilterMax: vm.selectedTrend.trendDefinition.Filter.FilterMax,
          FlatlineThreshold:
            vm.selectedTrend.trendDefinition.Filter.FlatlineThreshold,
          Exclude: vm.selectedTrend.trendDefinition.Filter.Exclude,
          ApplyToAll: vm.selectedTrend.trendDefinition.Filter.ApplyToAll,
          Color: null,
          SeriesID: null,
          Archive: vm.selectedTrend.trendDefinition.Filter.Archive,
        };
      } else {
        trendFilter = {
          FilterMin: null,
          FilterMax: null,
          FlatlineThreshold: 0,
          Exclude: '',
          ApplyToAll: false,
          Color: null,
          SeriesID: null,
          Archive: '',
        };
      }
      newPin.Filters.push(trendFilter);
      for (const series of vm.selectedTrend.trendDefinition.Series) {
        const seriesFilter = {
          FilterMin: series?.FilterMin,
          FilterMax: series?.FilterMax,
          FlatlineThreshold: series?.FlatlineThreshold,
          Exclude: series?.Exclude,
          ApplyToAll: series?.ApplyToAll,
          Color: null,
          SeriesID: series?.PDTagID,
          Archive: series?.Archive,
        };
        newPin.Filters.push(seriesFilter);
      }

      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.patchState((state) => ({
        isDirty,
      }));

      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.ShowSelected = false;
        state.trendDefinition.ShowPins = true;
        if (vm.selectedTrend.trendDefinition?.Pins?.length > 0) {
          newPin.NameFormat += ` ${
            vm.selectedTrend.trendDefinition.Pins.length + 1
          }`;
          newPin.Name += ` ${vm.selectedTrend.trendDefinition.Pins.length + 1}`;
          state.trendDefinition.Pins.push(newPin);
        } else {
          state.trendDefinition.Pins = [newPin];
        }
      });
      this.setLoadingTrends();
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  addSeriesToChart(assetVariableTypeTagMaps: IAssetVariableTypeTagMap[]) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      let isDirty = false;
      let convertVariablesToTags = false;
      let displayOrder = vm.selectedTrend?.trendDefinition?.Series?.length || 0;

      const newTrend = produce(vm.selectedTrend, (state) => {
        for (const assetVariableTypeTagMap of assetVariableTypeTagMaps) {
          const seriesExists: IPDTrendSeries = findSeries(
            assetVariableTypeTagMap.TagID,
            vm.selectedTrend.trendDefinition.Series
          );
          if (!seriesExists) {
            displayOrder++;
            const newSeries = createNewSeries(
              assetVariableTypeTagMap,
              displayOrder
            );

            const newAxis = addAxis(
              assetVariableTypeTagMap.Tag.EngUnits,
              true,
              state.trendDefinition.Axes
            );
            if (assetVariableTypeTagMap.Tag.IsAP) {
              newSeries.isAP = true;
            }
            newSeries.Axis = newAxis.Axis;
            state.trendDefinition.Series.push(newSeries);
            isDirty = true;
            if (newSeries.AlternateDisplayTexts.length == 0) {
              convertVariablesToTags = true;
            }
          }
        }
      });
      if (convertVariablesToTags) {
        this.convertVariablesToTags({
          trend: newTrend,
          archive: '',
          start: vm.start,
          end: vm.end,
        });
      } else if (isDirty) {
        this.setTrendDirty({
          trend: newTrend,
          trendID: newTrend.id,
          archive: '',
          startDate: vm.start,
          endDate: vm.end,
        });
      }
    });
  }

  addMeasurementToChart(tagID: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const existingColors = vm.selectedTrend.groupSeriesMeasurements
        .filter((m) => m.Color)
        .map((c) => c.Color)
        ?.slice(0, 5);
      const colors = new ColorService();
      const color = colors.checkAndGetNextColor(existingColors, 0);
      const newMeasurement = getDefaultMeasurement(0, tagID, color);
      const newState = produce(vm.selectedTrend, (state) => {
        state.groupSeriesMeasurements.push(newMeasurement);
        state.measurements?.Bands.forEach((band) => {
          band.Measurements.push(newMeasurement);
        });
        processBandAxisBarsAndMeasurements(state);
      });

      this.setTrendDirty({
        trend: newState,
        trendID: newState.id,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  addBandAxisMeasurementToChart(
    assetVariableTypeTagMaps: IAssetVariableTypeTagMap[]
  ) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      let displayOrder = vm.selectedTrend.groupSeriesMeasurements.length + 1;
      const newMeasurements: IPDTrendBandAxisMeasurement[] = [];
      const axes: IPDTrendAxis[] =
        [...vm.selectedTrend.trendDefinition.Axes] || [];

      const colors = new ColorService();
      const existingColors =
        vm.selectedTrend.trendDefinition.BandAxisMeasurements.filter(
          (m) => m.Color
        )
          .map((c) => c.Color)
          ?.slice(0, 5);

      for (const assetVariableTypeTagMap of assetVariableTypeTagMaps) {
        const newAxis = addAxis(
          assetVariableTypeTagMap.Tag.EngUnits,
          true,
          axes
        );

        let minID = 0;
        const color = colors.getColor(existingColors);
        existingColors.push(color);
        if (vm.selectedTrend?.groupSeriesMeasurements) {
          for (const t of vm.selectedTrend.groupSeriesMeasurements) {
            minID = Math.min(t?.TrendBandAxisMeasurementID, minID);
          }
        }
        minID--;
        displayOrder++;
        const newMeasurement = createNewGroupedSeries(
          newAxis,
          assetVariableTypeTagMap,
          color,
          minID,
          displayOrder
        );
        newMeasurements.push(newMeasurement);
      }

      const trend = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.Axes = axes;
        state.groupSeriesMeasurements = [
          ...state.groupSeriesMeasurements,
          ...newMeasurements,
        ];
        state.measurements?.Bands.forEach((band) => {
          band.Measurements = [...band.Measurements, ...newMeasurements];
        });
        processBandAxisBarsAndMeasurements(state);
      });

      this.setTrendDirty({
        trend: trend,
        trendID: trend.id,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  changeSummaryType(newSummaryType: ISummaryType) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();
      const newState = produce(vm.selectedTrend, (state) => {
        if (newSummaryType.SummaryTypeID === 0) {
          // Summary Type 0 means no aggregation selected.
          // so SummaryType has to be null.
          state.trendDefinition.SummaryType = null;
          state.trendDefinition.SummaryTypeID = newSummaryType.SummaryTypeID;
        } else {
          state.trendDefinition.SummaryType = newSummaryType;
          state.trendDefinition.SummaryTypeID = newSummaryType.SummaryTypeID;
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  chartTypeChange(chartTypeID: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();
      const newState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.ChartTypeID = chartTypeID;
        if (state.trendDefinition.BandAxis) {
          state.trendDefinition.BandAxis.Clockwise = null;
          state.trendDefinition.BandAxis.StartAngle = null;
          state.trendDefinition.BandAxis.EndAngle = null;
        }

        if (chartTypeID === AtxChartRadar) {
          state.trendDefinition.BandAxis.Clockwise = true;
          state.trendDefinition.BandAxis.StartAngle = 0;
          state.trendDefinition.BandAxis.EndAngle = 360;
        }

        if (!vm.isGroupedSeries) {
          if (chartTypeID === AtxChartTable) {
            if (
              isNil(vm.selectedTrend.trendDefinition.SummaryType) ||
              vm.selectedTrend.trendDefinition.SummaryTypeID === 0
            ) {
              state.trendDefinition.SummaryType = SummaryTypes[3];
              state.trendDefinition.SummaryTypeID =
                SummaryTypes[3].SummaryTypeID;
            }
          }
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  advancedUpdateChart(advanceOptions: IAdvancedOptions) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();
      const newState = produce(vm.selectedTrend, (state) => {
        if (advanceOptions.filter) {
          state.trendDefinition.Filter = finishFilterProcessing(
            advanceOptions.filter
          );
        }
        if (state.trendDefinition.DataRetrieval) {
          state.trendDefinition.DataRetrieval.Method =
            advanceOptions.dataRetrievalMethod;
          state.trendDefinition.DataRetrieval.MinInterval =
            advanceOptions.dataRetrievalMinInterval;
          state.trendDefinition.DataRetrieval.MinIntervalUnits =
            advanceOptions.dataRetrievalMinIntervalUnits;
          state.trendDefinition.DataRetrieval.Archive =
            advanceOptions.dataRetrievalArchive;
        }
      });
      const isDirty = produce(vm.isDirty, (state) => {
        state[vm.selectedTrend.id] = true;
      });
      this.setDirty(isDirty);
      this.getTrendData({
        trend: newState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  reflow() {
    reflowFast();
  }

  updateBounds(height: number, reflowScreen: boolean) {
    this.chartHeight(height - 120);
    if (reflowScreen) {
      reflowFast();
    }
  }

  setRadarChartOptions(clockwise: boolean, angle: number) {
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      this.setLoadingTrends();

      const newTrendState = produce(vm.selectedTrend, (state) => {
        state.trendDefinition.BandAxis.Clockwise = clockwise;
        state.trendDefinition.BandAxis.StartAngle = angle;
        state.trendDefinition.BandAxis.EndAngle = angle + 360;
      });

      this.setTrendDirty({
        trend: newTrendState,
        trendID: vm.selectedTrendID,
        archive: '',
        startDate: vm.start,
        endDate: vm.end,
      });
    });
  }

  private replaceTrend(
    trendID: string,
    oldTrends: IProcessedTrend[],
    serverTrends: IProcessedTrend[],
    state
  ) {
    if (trendID) {
      if (+trendID < 0) {
        // user created a new chart, save the chart with a different name
        // now that original 'new chart' needs to be deleted, since it is now
        // coming back from the server with an id
        const deleteTrendIndex = oldTrends.findIndex(
          (trend) => trend.id === trendID
        );
        if (deleteTrendIndex !== -1) {
          state.splice(deleteTrendIndex, 1);
        }
      } else {
        let refreshTrend = null;
        const trendIndex = serverTrends.findIndex(
          (trend) => trend.id === trendID
        );
        if (trendIndex !== -1) {
          refreshTrend = serverTrends[trendIndex];
        }
        if (refreshTrend) {
          let index = oldTrends.findIndex(
            (trend) => trend.id === refreshTrend.id
          );
          if (index === -1) {
            index = oldTrends.findIndex(
              (trend) => trend.label === refreshTrend.label
            );
          }

          if (index !== -1) {
            state[index] = refreshTrend;
          } else {
            state.push(refreshTrend);
          }
        }
      }
    }
    return state;
  }
  private updateDirtyList(
    trends: IProcessedTrend[],
    previousIsDirty: Record<string, boolean>,
    cleanedTrends: string[]
  ) {
    const isDirty: Record<string, boolean> = {};
    for (const key in previousIsDirty) {
      // keep new charts (id < 0) in correct dirty state
      if (+key < 0) {
        if (cleanedTrends.includes(key)) {
          isDirty[key] = false;
        } else {
          isDirty[key] = previousIsDirty[key];
        }
      }
    }
    trends.forEach((trend) => {
      if (cleanedTrends.includes(trend.id)) {
        isDirty[trend.id] = false;
      } else {
        isDirty[trend.id] = previousIsDirty[trend.id] ? true : false;
      }
    });
    return isDirty;
  }
  ngOnDestroy() {
    this.reset();
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
