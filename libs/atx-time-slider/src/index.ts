/*
 * Public API Surface of atx-time-slider
 */

export * from './lib/atx-time-slider.module';
export { ITimeSliderState } from './lib/model/time-slider-state';
export { ITimeSliderStateChange } from './lib/model/time-slider-state-change';
export {
  alterTimeSliderState,
  setJumpTo,
  getDefaultTimeSlider,
} from './lib/service/time-slider/time-slider.service';
