import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeSliderComponent } from './component/time-slider/time-slider.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarDialogRangeComponent } from './component/calendar-dialog/calendar-dialog-range/calendar-dialog-range.component';
import { SliderComponent } from './component/ng5-slider/slider.component';
import { SliderElementDirective } from './component/ng5-slider/slider-element.directive';
import { SliderHandleDirective } from './component/ng5-slider/slider-handle.directive';
import { TooltipWrapperComponent } from './component/ng5-slider/tooltip-wrapper.component';
import { SliderLabelDirective } from './component/ng5-slider/slider-label.directive';
import { TimeSliderDialogFacade } from './service/time-slider-dialog/time-slider-dialog.facade';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { SharedApiModule } from '@atonix/shared/api';

@NgModule({
  declarations: [
    TimeSliderComponent,
    CalendarDialogRangeComponent,
    SliderComponent,
    SliderElementDirective,
    SliderHandleDirective,
    SliderLabelDirective,
    TooltipWrapperComponent,
  ],
  imports: [
    CommonModule,
    SharedUtilsModule,
    AtxMaterialModule,
    FormsModule,
    SharedApiModule,
    ReactiveFormsModule,
  ],
  exports: [TimeSliderComponent, SliderComponent],
  providers: [TimeSliderDialogFacade],
})
export class TimeSliderModule {}
