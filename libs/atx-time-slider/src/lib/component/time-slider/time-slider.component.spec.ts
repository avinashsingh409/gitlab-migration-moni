/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TimeSliderComponent } from './time-slider.component';
import { CalendarDialogRangeComponent } from '../calendar-dialog/calendar-dialog-range/calendar-dialog-range.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ITimeSliderState } from '../../model/time-slider-state';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import moment from 'moment';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { getDefaultOptions } from '../ng5-slider/options';
import { TimeSliderDialogFacade } from '../../service/time-slider-dialog/time-slider-dialog.facade';
import { ICriteria } from '@atonix/atx-core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NavFacade } from '@atonix/atx-navigation';
import { of } from 'rxjs';

describe('TimeSliderComponent', () => {
  let component: TimeSliderComponent;
  let fixture: ComponentFixture<TimeSliderComponent>;

  const timeSliderStateSingle: ITimeSliderState = {
    dateIndicator: 'Single',
    showCalendarDialog: true,
    startDate: null,
    endDate: null,
    asOfDate: moment('2019-01-01').toDate(),
    isLive: true,
  };

  const timeSliderStateRange: ITimeSliderState = {
    dateIndicator: 'Range',
    showCalendarDialog: true,
    startDate: moment('2019-01-01').toDate(),
    endDate: moment('2020-01-01').toDate(),
    asOfDate: null,
    isLive: true,
  };

  beforeEach(() => {
    const navFacadeMock = createMockWithValues(NavFacade, {
      theme$: of('dark'),
    });

    TestBed.configureTestingModule({
      providers: [
        provideMock(TimeSliderDialogFacade),
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: NavFacade, useValue: navFacadeMock },
      ],
      imports: [
        AtxMaterialModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        FlexLayoutModule,
        NoopAnimationsModule,
      ],
      declarations: [TimeSliderComponent, CalendarDialogRangeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    component.options = getDefaultOptions({
      floor: moment('2015-01-01').toDate().getTime(),
      ceil: moment('2023-01-01').toDate().getTime(),
      translate: (value: number): string => {
        return moment(value).format('M/D/YY h:mm A');
      },
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit initialized event', () => {
    jest.spyOn(component.stateChange, 'emit');
    component.ngOnInit();
    expect(component.stateChange.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'Initialized',
        newValue: null,
      })
    );
  });

  it('should emit change regarding hiding the time slider', () => {
    jest.spyOn(component.stateChange, 'emit');
    component.hideTimeSlider();
    expect(component.stateChange.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'HideTimeSlider',
        newValue: null,
      })
    );
  });

  it('should emit run live', () => {
    component.timeSliderState = { ...timeSliderStateRange, isLive: true };
    jest.spyOn(component.stateChange, 'emit');
    component.tick();
    expect(component.stateChange.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'SelectDateRange',
        source: 'live',
      })
    );
  });

  it('should emit toggle live', () => {
    component.timeSliderState = timeSliderStateSingle;
    jest.spyOn(component.stateChange, 'emit');

    component.slideToggleLive();
    expect(component.stateChange.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'ToggleLive',
        newValue: undefined,
      })
    );
  });

  describe('should emit date value/s on mouseUp', () => {
    it('Range Slider', () => {
      jest.spyOn(component.stateChange, 'emit');
      component.timeSliderState = timeSliderStateRange;
      component.minValue = timeSliderStateRange.startDate;
      component.maxValue = timeSliderStateRange.endDate;

      component.mouseUp();
      expect(component.stateChange.emit).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'SelectDateRange',
          newStartDateValue: timeSliderStateRange.startDate,
          newEndDateValue: timeSliderStateRange.endDate,
          source: 'mouseUp',
        })
      );
    });
  });

  it('should emit indicator value on calendar toggle', () => {
    jest.spyOn(component.stateChange, 'emit');

    component.toggleCalendarIndicator();
    expect(component.stateChange.emit).toHaveBeenCalledWith(
      expect.objectContaining({
        event: 'ToggleCalendarIndicator',
        newValue: true,
      })
    );
  });

  describe('should step and emit date value/s', () => {
    it('Range Slider', () => {
      jest.spyOn(component.stateChange, 'emit');
      component.timeSliderState = timeSliderStateRange;
      component.minValue = timeSliderStateRange.startDate;
      component.maxValue = timeSliderStateRange.endDate;
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2024-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      component.step('right');
      expect(component.stateChange.emit).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'SelectDateRange',
          newStartDateValue: timeSliderStateRange.endDate,
          newEndDateValue: moment(
            timeSliderStateRange.endDate.getTime() +
              (timeSliderStateRange.endDate.getTime() -
                timeSliderStateRange.startDate.getTime())
          ).toDate(),
          source: 'step',
        })
      );

      component.step('left');
      expect(component.stateChange.emit).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'SelectDateRange',
          newStartDateValue: moment(
            timeSliderStateRange.startDate.getTime() -
              (timeSliderStateRange.endDate.getTime() -
                timeSliderStateRange.startDate.getTime())
          ).toDate(),
          newEndDateValue: timeSliderStateRange.startDate,
          source: 'step',
        })
      );

      component.minValue = moment('2020-01-01').toDate();
      component.maxValue = moment('2020-01-31').toDate();
      component.step('right');
      expect(component.stateChange.emit).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'SelectDateRange',
          newStartDateValue: timeSliderStateRange.endDate,
          newEndDateValue: moment(
            timeSliderStateRange.endDate.getTime() +
              (timeSliderStateRange.endDate.getTime() -
                timeSliderStateRange.startDate.getTime())
          ).toDate(),
          source: 'step',
        })
      );

      component.minValue = moment('2018-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.step('left');
      expect(component.stateChange.emit).toHaveBeenCalledWith(
        expect.objectContaining({
          event: 'SelectDateRange',
          newStartDateValue: moment(
            timeSliderStateRange.startDate.getTime() -
              (timeSliderStateRange.endDate.getTime() -
                timeSliderStateRange.startDate.getTime())
          ).toDate(),
          newEndDateValue: timeSliderStateRange.startDate,
          source: 'step',
        })
      );
    });
  });

  describe('should move range either past or future', () => {
    it('move range to past in single time slider', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        ticksArray: [
          1420041600000, 1451577600000, 1483200000000, 1514736000000,
          1546272000000, 1577808000000, 1609430400000, 1640966400000,
          1672502400000,
        ],
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      component.moveRange('past');
      expect(component.options.ticksArray.length).toEqual(9);
    });

    it('move range to future in range time slider', () => {
      component.timeSliderState = timeSliderStateRange;
      component.minValue = moment('2019-10-01').toDate();
      component.maxValue = moment('2019-12-31 23:59').toDate();
      component.options = getDefaultOptions({
        floor: moment('2019-07-01').toDate().getTime(),
        ceil: moment('2020-04-01').toDate().getTime(),
        ticksArray: [
          1561910400000, 1564588800000, 1567267200000, 1569859200000,
          1572537600000, 1575129600000, 1577808000000, 1580486400000,
          1582992000000, 1585670400000,
        ],
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      component.moveRange('future');
      expect(component.options.ticksArray.length).toEqual(10);
    });
  });

  describe('should zoom', () => {
    it('zoom in hour', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 31; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(10);
    });

    it('zoom in 8 hour', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 24; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(9);
    });

    it('zoom in day', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 20; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(10);
    });

    it('zoom in 3 day', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 16; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(10);
    });

    it('zoom in week', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 13; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(11);
    });

    it('zoom in month', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 8; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(10);
    });

    it('zoom in quarter', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 5; i++) {
        component.zoom('in');
      }

      expect(component.options.ticksArray.length).toEqual(8);
    });

    it('zoom out year', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      component.zoom('out');

      expect(component.options.ticksArray.length).toEqual(11);
    });

    it('zoom out decade', () => {
      component.timeSliderState = timeSliderStateSingle;
      component.minValue = moment('2019-01-01').toDate();
      component.maxValue = moment('2019-01-01').toDate();
      component.options = getDefaultOptions({
        floor: moment('2015-01-01').toDate().getTime(),
        ceil: moment('2023-01-01').toDate().getTime(),
        translate: (value: number): string => {
          return moment(value).format('M/D/YY h:mm A');
        },
      });

      for (let i = 0; i < 5; i++) {
        component.zoom('out');
      }

      expect(component.options.ticksArray.length).toEqual(3);
    });
  });
});
