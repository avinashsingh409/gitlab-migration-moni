/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import moment, { now } from 'moment';
import { Observable, Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { ITimeSliderState } from '../../model/time-slider-state';
import { ITimeSliderStateChange } from '../../model/time-slider-state-change';
import { CalendarDialogRangeComponent } from '../calendar-dialog/calendar-dialog-range/calendar-dialog-range.component';
import { CalendarService } from '../../service/calendar/calendar.service';
import { RangeData } from '../../model/range-data';
import { NavFacade } from '@atonix/atx-navigation';

import {
  initialized,
  hideTimeSlider,
  selectDateRange,
  toggleCalendar,
  toggleLive,
  RangeShift,
  Zoom,
} from '../../service/time-slider/time-slider.service';
import { Options } from '../ng5-slider/options';
import { ITimeData } from '@atonix/atx-core';
import { UIConfigFrameworkService } from '@atonix/shared/api';
import { LoggerService } from '@atonix/shared/utils';

@Component({
  selector: 'atx-time-slider',
  templateUrl: './time-slider.component.html',
  styleUrls: ['./time-slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeSliderComponent implements OnInit, OnChanges, OnDestroy {
  @Input() timeSliderState: ITimeSliderState;
  @Input() showJumpTo: boolean;
  @Output() stateChange = new EventEmitter<ITimeSliderStateChange>();

  // These will be used to calculate the number of divisions to show
  hourTicks = 1000 * 60 * 60;
  shiftTicks = this.hourTicks * 8;
  dayTicks = this.hourTicks * 24;
  weekTicks = this.dayTicks * 7;
  monthTicks = this.dayTicks * 30;
  quarterTicks = this.dayTicks * 90;
  yearTicks = this.dayTicks * 365;

  options: Options = null;
  minValue: Date = moment().toDate();
  maxValue: Date = moment().toDate();
  stepBy = 'day';

  theme$: Observable<string>;
  //controls when to show or hide the JumpTo Menu
  showJumpToMenu = false;
  //JumpToMenu starting Position
  dragPosition = { x: -46, y: -401 };
  //The range in miliseconds of the pending new timeslider range
  newRange = 0;
  //controls when to disable 'Apply'
  newRangeChanged = false;

  // This wil make the timer tick every 5 minutes.  It ticks regardless of whether
  // the live button is enabled.  But if live is not enabled the tick won't do anything.
  private liveEmitter$ = timer(1000 * 60 * 5, 1000 * 60 * 5);
  private unsubscribe$ = new Subject<void>();

  constructor(
    private calendarService: CalendarService,
    private uiConfigFrameworkService: UIConfigFrameworkService,
    private logger: LoggerService,
    private navFacade: NavFacade,
    public dialog: MatDialog
  ) {
    this.liveEmitter$.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      if (this.timeSliderState.isLive) {
        this.tick();
      }
    });
    this.theme$ = this.navFacade.theme$;
  }

  ngOnInit(): void {
    this.stateChange.emit(initialized());
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.timeSliderState) {
      this.minValue = this.timeSliderState.startDate;
      this.maxValue = this.timeSliderState.endDate;

      if (this.options === null) {
        this.getRangeAndDraw(this.minValue, this.maxValue);
      }

      if (
        this.timeSliderState.showCalendarDialog &&
        this.dialog.openDialogs.length === 0
      ) {
        this.openCalendarDialog();
      }

      if (
        changes?.timeSliderState?.currentValue?.isLive &&
        changes?.timeSliderState?.currentValue?.isLive !==
          changes?.timeSliderState?.previousValue?.isLive
      ) {
        setTimeout(() => {
          this.tick();
        }, 100);
      }

      this.updateNewRange({ Start: this.minValue, End: this.maxValue });
    }
  }

  tick() {
    const interval =
      moment(this.maxValue).toDate().getTime() -
      moment(this.minValue).toDate().getTime();
    const today = moment().toDate().getTime();
    const newStart = moment(today - interval).toDate();
    const newEnd = new Date();

    this.fixSliderRange(newStart, newEnd, 'live');
  }

  hideTimeSlider(): void {
    this.stateChange.emit(hideTimeSlider());
  }

  //bug? this is emiting events when a user
  //presses any key after changing the timeslider
  //need to emit events only when min/max value actually changes
  mouseUp(): void {
    this.logger.trackTaskCenterEvent('TimeControl:TimeSliderAdjustments');

    this.stateChange.emit(
      selectDateRange(
        moment(this.minValue).toDate(),
        moment(this.maxValue).toDate(),
        'mouseUp'
      )
    );

    if (this.timeSliderState.isLive) {
      this.stateChange.emit(toggleLive(false));
    }
  }

  toggleCalendarIndicator(): void {
    this.stateChange.emit(toggleCalendar(true));
  }

  openCalendarDialog(): void {
    this.calendarDialogRange();
  }

  private calendarDialogRange(): void {
    const unsubscribeDialog = new Subject<void>();

    const dialogConfig = new MatDialogConfig();

    dialogConfig.panelClass = 'custom-dialog';
    dialogConfig.data = {
      startDate: moment(this.minValue).toDate(),
      endDate: moment(this.maxValue).toDate(),
    };
    dialogConfig.position = {
      bottom: '75px',
      right: '10px',
    };

    const dialogRef = this.dialog.open(
      CalendarDialogRangeComponent,
      dialogConfig
    );

    dialogRef.componentInstance.updateValues
      .pipe(takeUntil(unsubscribeDialog))
      .subscribe((data: RangeData) => {
        if (
          data &&
          (data.startDate.getTime() !==
            moment(this.minValue).toDate().getTime() ||
            data.endDate.getTime() !== moment(this.maxValue).toDate().getTime())
        ) {
          this.fixSliderRange(data.startDate, data.endDate, 'popup');

          if (this.timeSliderState.isLive) {
            this.stateChange.emit(toggleLive(false));
          }
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(unsubscribeDialog))
      .subscribe((data: RangeData) => {
        this.stateChange.emit(toggleCalendar(false));
        if (
          data &&
          (data.startDate.getTime() !==
            moment(this.minValue).toDate().getTime() ||
            data.endDate.getTime() !== moment(this.maxValue).toDate().getTime())
        ) {
          this.fixSliderRange(data.startDate, data.endDate, 'popup');

          if (this.timeSliderState.isLive) {
            this.stateChange.emit(toggleLive(false));
          }
        }
        unsubscribeDialog.next();
      });
  }

  step(direction: string) {
    const newRangeValue: RangeData = this.calendarService.stepRange(
      direction,
      moment(this.minValue).toDate(),
      moment(this.maxValue).toDate()
    );
    this.fixSliderRange(newRangeValue.startDate, newRangeValue.endDate, 'step');
    if (this.timeSliderState.isLive) {
      this.stateChange.emit(toggleLive(false));
    }
  }

  private fixSliderRange(startDate: Date, endDate: Date, source?: string) {
    this.reDrawSlider(
      moment(
        startDate.getTime() - (endDate.getTime() - startDate.getTime())
      ).toDate(),
      moment(
        endDate.getTime() + (endDate.getTime() - startDate.getTime())
      ).toDate()
    );

    if (source !== 'external') {
      this.stateChange.emit(
        selectDateRange(
          moment(startDate).toDate(),
          moment(endDate).toDate(),
          source
        )
      );
    }
  }

  moveRange(direction: string): void {
    if (
      this.options &&
      this.options.ticksArray &&
      this.options.ticksArray.length >= 2
    ) {
      let divisionticks =
        this.options.ticksArray[1] - this.options.ticksArray[0];
      if (direction === 'past') {
        divisionticks = divisionticks * -1;
      }

      const min = moment(this.minValue).toDate();
      const max = moment(this.maxValue).toDate();
      const newRangeMin = moment(this.options.floor + divisionticks).toDate();
      const newRangeMax = moment(this.options.ceil + divisionticks).toDate();
      if (
        newRangeMin.getTime() <= min.getTime() &&
        newRangeMax.getTime() >= max.getTime()
      ) {
        this.reDrawSlider(newRangeMin, newRangeMax);
      } else {
        if (
          newRangeMin.getTime() > min.getTime() &&
          this.options.floor !== min.getTime()
        ) {
          this.reDrawSlider(
            min,
            moment(
              this.options.ceil + (min.getTime() - this.options.floor)
            ).toDate()
          );
        } else if (
          newRangeMax.getTime() < max.getTime() &&
          this.options.ceil !== max.getTime()
        ) {
          this.reDrawSlider(
            moment(
              this.options.floor - (this.options.ceil - max.getTime())
            ).toDate(),
            max
          );
        }
      }
      this.stateChange.emit(RangeShift());
    }
  }

  zoom(direction: string): void {
    let interval = this.options.ceil - this.options.floor;
    if (direction === 'in') {
      interval = interval * 0.75;
    } else {
      interval = interval * 1.25;
    }

    // This is to put the indicator at the center of the slider always when zooming.
    const newRangeMin = moment(
      moment(this.minValue).toDate().getTime() - interval / 2
    ).toDate();
    const newRangeMax = moment(
      moment(this.maxValue).toDate().getTime() + interval / 2
    ).toDate();

    this.reDrawSlider(newRangeMin, newRangeMax);
    this.stateChange.emit(Zoom());
  }

  onToggleJumpTo() {
    this.newRange = moment(this.maxValue).diff(moment(this.minValue));
    this.newRangeChanged = false;
    this.showJumpToMenu = !this.showJumpToMenu;
  }

  onCloseJumpToMenu() {
    this.showJumpToMenu = false;
  }

  onJumpToDays(amount: number) {
    const rangeData: ITimeData = {
      Start: moment(now()).subtract(amount, 'days').toDate(),
      End: moment(now()).toDate(),
    };
    this.jumpTo(rangeData);
    this.updateNewRange(rangeData);
  }

  onJumpToMonths(amount: number) {
    const rangeData: ITimeData = {
      Start: moment(now()).subtract(amount, 'months').toDate(),
      End: moment(now()).toDate(),
    };
    this.jumpTo(rangeData);
    this.updateNewRange(rangeData);
  }

  onJumpToNewRange() {
    const rangeData: ITimeData = {
      Start: moment(now() - this.newRange).toDate(),
      End: moment(now()).toDate(),
    };
    this.jumpTo(rangeData);
    this.newRangeChanged = false;
  }

  onChangeNewRange(multipler: number) {
    if (this.newRange * multipler > 4 * this.yearTicks) {
      return;
    }
    this.newRange = this.newRange * multipler;
    this.newRangeChanged = true;
  }

  private updateNewRange(rangeData: ITimeData) {
    this.newRange = moment(rangeData.End).diff(moment(rangeData.Start));
  }

  private jumpTo(rangeData: ITimeData) {
    const newMin = rangeData.Start;
    const newMax = rangeData.End;
    this.getRangeAndDraw(newMin, newMax);
    this.stateChange.emit(selectDateRange(newMin, newMax, 'jumpto'));
  }

  private getRangeAndDraw(min: Date, max: Date): void {
    let newRangeMin: Date = null;
    let newRangeMax: Date = null;
    const interval = max.getTime() - min.getTime();
    newRangeMin = moment(min.getTime() - interval).toDate();
    newRangeMax = moment(max.getTime() + interval).toDate();
    this.reDrawSlider(newRangeMin, newRangeMax);
  }

  private reDrawSlider(newMin: Date, newMax: Date): void {
    const newOptions: Options = Object.assign({}, this.options);
    const rangeTicks = newMax.getTime() - newMin.getTime();
    const divisions = [];
    if (rangeTicks < this.hourTicks * 10) {
      // hour divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth(),
        newMin.getDate(),
        newMin.getHours()
      );
      if (dateFrom.getTime() + this.hourTicks * 0.5 < newMin.getTime()) {
        dateFrom.setHours(dateFrom.getHours() + 1);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setHours(dateFrom.getHours() + 1);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        return moment(value).format('MM/DD hA');
      };
    } else if (rangeTicks < this.shiftTicks * 10) {
      // 8 hour divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth(),
        newMin.getDate(),
        newMin.getHours() - (newMin.getHours() % 8)
      );
      if (dateFrom.getTime() + this.hourTicks * 8 * 0.5 < newMin.getTime()) {
        dateFrom.setHours(dateFrom.getHours() + 8);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setHours(dateFrom.getHours() + 8);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        const date = moment(value).toDate();
        return moment(date).format('MM/DD hA');
      };
    } else if (rangeTicks < this.dayTicks * 10) {
      // day divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth(),
        newMin.getDate()
      );
      if (dateFrom.getTime() + this.dayTicks * 0.5 < newMin.getTime()) {
        dateFrom.setDate(dateFrom.getDate() + 1);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setDate(dateFrom.getDate() + 1);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        return moment(value).format('MM/DD');
      };
    } else if (rangeTicks < this.dayTicks * 35) {
      // 3 day divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth(),
        newMin.getDate()
      );
      if (dateFrom.getTime() + this.dayTicks * 0.5 < newMin.getTime()) {
        dateFrom.setDate(dateFrom.getDate() + 1);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setDate(dateFrom.getDate() + 3);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        const date = moment(value).toDate();
        return moment(date).format('MM/DD');
      };
    } else if (rangeTicks < this.weekTicks * 10) {
      // week divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth(),
        newMin.getDate() - newMin.getDay()
      );
      if (dateFrom.getTime() + this.weekTicks * 0.5 < newMin.getTime()) {
        dateFrom.setDate(dateFrom.getDate() + 7);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setDate(dateFrom.getDate() + 7);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        const date = moment(value).toDate();
        return moment(date).format('MM/DD/YYYY');
      };
    } else if (rangeTicks < this.monthTicks * 10) {
      // month divisions
      const dateFrom = new Date(newMin.getFullYear(), newMin.getMonth(), 1);
      if (dateFrom.getTime() + this.monthTicks * 0.5 < newMin.getTime()) {
        dateFrom.setMonth(dateFrom.getMonth() + 1);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setMonth(dateFrom.getMonth() + 1);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        return moment(value).format('MMM YYYY');
      };
    } else if (rangeTicks < this.quarterTicks * 10) {
      // quarter divisions
      const dateFrom = new Date(
        newMin.getFullYear(),
        newMin.getMonth() - (newMin.getMonth() % 3),
        1
      );
      if (dateFrom.getTime() + this.quarterTicks * 0.5 < newMin.getTime()) {
        dateFrom.setMonth(dateFrom.getMonth() + 3);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setMonth(dateFrom.getMonth() + 3);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        const date = moment(value).toDate();
        return (
          'Q' +
          (Math.floor(date.getMonth() / 3) + 1) +
          moment(date).format(' YYYY')
        );
      };
    } else if (rangeTicks < this.yearTicks * 20) {
      // Year divisions
      const dateFrom = new Date(newMin.getFullYear(), 0, 1);
      if (dateFrom.getTime() + this.yearTicks * 0.5 < newMin.getTime()) {
        dateFrom.setFullYear(dateFrom.getFullYear() + 1);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setFullYear(dateFrom.getFullYear() + 1);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        return moment(value).format('YYYY');
      };
    } else {
      // Decade divisions
      const dateFrom = new Date(
        newMin.getFullYear() - (newMin.getFullYear() % 10),
        0,
        1
      );
      if (dateFrom.getTime() + this.yearTicks * 10 * 0.5 < newMin.getTime()) {
        dateFrom.setFullYear(dateFrom.getFullYear() + 10);
      }
      while (dateFrom.getTime() <= newMax.getTime()) {
        divisions.push(dateFrom.getTime());
        dateFrom.setFullYear(dateFrom.getFullYear() + 10);
      }

      newOptions.ticksArray = divisions;
      newOptions.getLegend = (value: number): string => {
        return moment(value).format('YYYY');
      };
    }

    newOptions.floor = newMin.getTime();
    newOptions.ceil = newMax.getTime();
    newOptions.translate = (value: number): string => {
      return moment(value).format('MM/DD/YY h:mm A');
    };
    newOptions.draggableRange = this.timeSliderState.dateIndicator === 'Range';

    this.options = newOptions;
  }

  slideToggleLive() {
    this.stateChange.emit(toggleLive());
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }
}
