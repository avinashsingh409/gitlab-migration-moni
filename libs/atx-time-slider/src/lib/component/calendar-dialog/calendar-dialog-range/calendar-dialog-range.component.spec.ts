import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {
  MATERIAL_SANITY_CHECKS,
  MatNativeDateModule,
} from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarDialogRangeComponent } from './calendar-dialog-range.component';
import moment from 'moment';
import { TimeSliderDialogState } from '../../../service/time-slider-dialog/time-slider-dialog-state';
import {
  TimeSpanDirection,
  TimeSpanUnits,
} from '../../../model/time-span-enums';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { TimeSliderDialogFacade } from '../../../service/time-slider-dialog/time-slider-dialog.facade';
import { TimeSliderDialogQuery } from '../../../service/time-slider-dialog/time-slider-dialog.query';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';

describe('CalendarDialogRangeComponent', () => {
  const calendarDate = {
    startDate: moment().toDate(),
    endDate: moment().toDate(),
  };

  const mockDialogRef = {
    close: jest.fn(),
  };

  let component: CalendarDialogRangeComponent;
  let fixture: ComponentFixture<CalendarDialogRangeComponent>;
  const state: TimeSliderDialogState = {
    originalStartDate: moment('01-1-1970', 'MM-DD-YYYY').toDate(),
    originalEndDate: moment('01-1-9999', 'MM-DD-YYYY').toDate(),
    startTime: '',
    startDate: moment('01-1-1970', 'MM-DD-YYYY').toDate(),
    endTime: '',
    endDate: moment('01-1-9999', 'MM-DD-YYYY').toDate(),
    startDateAfterEndDate: false,
    startPickerStartAt: null,
    endPickerStartAt: null,
    invalidStartDateFormat: false,
    invalidEndDateFormat: false,
    startMin: null,
    startMax: null,
    endMin: null,
    endMax: null,
    timeSpanInterval: 1,
    timeSpanUnit: TimeSpanUnits.Days,
    timeSpanDate: null,
    timeSpanTime: '',
    invalidTimeSpanDateFormat: false,
    timeSpanDirection: TimeSpanDirection.Past,
  };

  let facadeMock: TimeSliderDialogFacade;
  let queryMock: TimeSliderDialogQuery;

  beforeEach(() => {
    queryMock = createMockWithValues(TimeSliderDialogQuery, {
      vm$: new BehaviorSubject<TimeSliderDialogState>(state),
    });
    facadeMock = createMockWithValues(TimeSliderDialogFacade, {
      query: queryMock,
      getStateSnapshot: jest.fn(() => state),
      buildStartDate: jest.fn(() => new FormControl()),
      buildStartTime: jest.fn(() => new FormControl()),
      buildEndDate: jest.fn(() => new FormControl()),
      buildEndTime: jest.fn(() => new FormControl()),
      buildTimeSpanDate: jest.fn(() => new FormControl()),
      buildTimeSpanTime: jest.fn(() => new FormControl()),
      buildTimeSpanDirection: jest.fn(() => new FormControl()),
      buildTimeSpanInterval: jest.fn(() => new FormControl()),
    });

    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        {
          provide: TimeSliderDialogFacade,
          useValue: facadeMock,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: calendarDate,
          },
        },
      ],
      imports: [
        FormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [CalendarDialogRangeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarDialogRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
