import {
  Inject,
  Component,
  EventEmitter,
  ViewChild,
  ElementRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import moment from 'moment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { UntypedFormControl } from '@angular/forms';
import { CalendarService } from '../../../service/calendar/calendar.service';
import { RangeData } from '../../../model/range-data';
import { Observable } from 'rxjs';
import { debounceTime, take } from 'rxjs/operators';
import { TimeSliderDialogState } from '../../../service/time-slider-dialog/time-slider-dialog-state';
import { TimeSliderDialogFacade } from '../../../service/time-slider-dialog/time-slider-dialog.facade';
import {
  TimeSpanDirection,
  TimeSpanUnitMapping,
  TimeSpanUnits,
} from '../../../model/time-span-enums';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { combineTime, LoggerService } from '@atonix/shared/utils';

@Component({
  selector: 'atx-calendar-dialog-range',
  templateUrl: './calendar-dialog-range.component.html',
  styleUrls: ['./calendar-dialog-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CalendarDialogRangeComponent {
  vm$: Observable<TimeSliderDialogState> = this.dialogFacade.query.vm$;

  @ViewChild('startDateInput') startDateInputControl: ElementRef;
  @ViewChild('endDateInput') endDateInputControl: ElementRef;
  @ViewChild('timeSpanInput') timeSpanInputControl: ElementRef;
  @ViewChild('tabGroup') tabGroup;

  public timeSpanUnitsStates = Object.keys(TimeSpanUnits).map(
    (itm) => TimeSpanUnits[itm]
  );
  public timeSpanUnitMapping = TimeSpanUnitMapping;
  public updateValues = new EventEmitter<RangeData>();

  startDate: UntypedFormControl;
  startTime: UntypedFormControl;

  endDate: UntypedFormControl;
  endTime: UntypedFormControl;

  timeSpanDate: UntypedFormControl;
  timespanStartMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
  timespanEndMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();

  timeSpanTime: UntypedFormControl;
  timeSpanInterval: UntypedFormControl;
  timeSpanDirection: UntypedFormControl;

  constructor(
    private calendarService: CalendarService,
    private matIconRegistry: MatIconRegistry, // To register icons that the navigation library will use
    private domSanitizer: DomSanitizer, // To register icons the navigation library will use
    private logger: LoggerService,
    public dialogRef: MatDialogRef<CalendarDialogRangeComponent>,
    public dialogFacade: TimeSliderDialogFacade,
    @Inject(MAT_DIALOG_DATA) public calendarData: RangeData
  ) {
    this.registerIcons();

    this.dialogFacade.setCalendarData(calendarData, true);
    const stateSnapshot = this.dialogFacade.getStateSnapshot();
    this.startDate = this.dialogFacade.buildStartDate();
    this.startTime = this.dialogFacade.buildStartTime();
    this.endDate = this.dialogFacade.buildEndDate();
    this.endTime = this.dialogFacade.buildEndTime();
    this.timeSpanDate = this.dialogFacade.buildTimeSpanDate();
    this.timeSpanTime = this.dialogFacade.buildTimeSpanTime();
    this.timeSpanInterval = this.dialogFacade.buildTimeSpanInterval();
    this.timeSpanDirection = this.dialogFacade.buildTimeSpanDirection();

    this.updateFormValues(stateSnapshot);

    this.timeSpanDirection.patchValue(stateSnapshot.timeSpanDirection, {
      emitEvent: false,
    });
  }

  toggleChange(change: MatButtonToggleChange) {
    const direction = change.value as TimeSpanDirection;
    this.dialogFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const timeSpanUpdate = TimeSliderDialogFacade.setTimeSpanDateAndTimes(
        vm,
        direction
      );
      this.timeSpanDate.patchValue(timeSpanUpdate.rangeDate);
      this.timeSpanTime.patchValue(
        moment(timeSpanUpdate.rangeDate).format('HH:mm')
      );
    });
  }
  updateFormValues(stateSnapshot: TimeSliderDialogState) {
    this.startDate.patchValue(stateSnapshot.startDate, { emitEvent: false });
    this.startTime.patchValue(stateSnapshot.startTime, { emitEvent: false });
    this.endDate.patchValue(stateSnapshot.endDate, { emitEvent: false });
    this.endTime.patchValue(stateSnapshot.endTime, { emitEvent: false });
    this.timeSpanDate.patchValue(stateSnapshot.timeSpanDate, {
      emitEvent: false,
    });
    this.timeSpanTime.patchValue(stateSnapshot.timeSpanTime, {
      emitEvent: false,
    });
    this.timeSpanInterval.patchValue(stateSnapshot.timeSpanInterval, {
      emitEvent: false,
    });
  }

  setFocus() {
    if (this.tabGroup.selectedIndex === 0) {
      setTimeout(() => {
        this.timeSpanInputControl.nativeElement.focus();
      }, 200);
    } else {
      setTimeout(() => {
        this.startDateInputControl.nativeElement.focus();
      }, 200);
    }
  }

  changeTimeUnits(timeUnits: TimeSpanUnits) {
    this.dialogFacade.changeTimeUnits(timeUnits);
  }

  onKeyUp(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.applyTimeSpanChanges(true);
    }
  }

  applyCustomDateChanges(close: boolean): void {
    if (
      this.startTime.invalid ||
      this.endTime.invalid ||
      this.endDate.errors ||
      this.startDate.errors
    ) {
      return;
    }

    this.logger.trackTaskCenterEvent('TimeControl:TimePickerAdjustments');

    this.dialogFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const start: Date = combineTime(vm.startDate, vm.startTime);
      const end: Date = combineTime(vm.endDate, vm.endTime);
      if (start.getTime() > end.getTime()) {
        return;
      }
      const newValues: RangeData = {
        startDate: start,
        endDate: end,
      };
      if (close) {
        this.dialogRef.close({
          startDate: newValues.startDate,
          endDate: newValues.endDate,
        });
      } else {
        this.updateDialogDatesAndTimes(newValues, true);
      }
    });
  }

  applyTimeSpanChanges(close: boolean): void {
    if (
      this.timeSpanDate.invalid ||
      this.timeSpanTime.invalid ||
      this.timeSpanInterval.invalid
    ) {
      return;
    }

    this.logger.trackTaskCenterEvent('TimeControl:TimePickerAdjustments');

    this.dialogFacade.query.vm$
      .pipe(debounceTime(100), take(1))
      .subscribe((vm) => {
        const referenceDate: Date = combineTime(
          vm.timeSpanDate,
          vm.timeSpanTime
        );
        const newValues: RangeData = this.calendarService.stepTimeSpan(
          vm.timeSpanInterval,
          referenceDate,
          vm.timeSpanDirection,
          vm.timeSpanUnit
        );
        if (close) {
          this.dialogRef.close({
            startDate: newValues.startDate,
            endDate: newValues.endDate,
          });
        } else {
          this.updateDialogDatesAndTimes(newValues, true);
        }
      });
  }

  step(direction: string): void {
    this.dialogFacade.query.vm$.pipe(take(1)).subscribe((vm) => {
      const newValues: RangeData = this.calendarService.stepWithUnits(
        direction,
        vm.originalStartDate,
        vm.originalEndDate,
        vm.timeSpanUnit,
        vm.timeSpanInterval
      );
      this.updateDialogDatesAndTimes(newValues, false);
    });
  }

  updateDialogDatesAndTimes(newValues: RangeData, updateInterval: boolean) {
    this.updateValues.emit({
      startDate: newValues.startDate,
      endDate: newValues.endDate,
    });
    this.dialogFacade.setCalendarData(newValues, updateInterval);
    this.dialogFacade.query.vm$.pipe(take(1)).subscribe((newSnapshot) => {
      this.updateFormValues(newSnapshot);
    });
  }

  resetRefDate() {
    this.timeSpanDate.patchValue(new Date());
    this.timeSpanTime.patchValue(moment(new Date()).format('HH:mm'));
  }

  private registerIcons() {
    this.matIconRegistry.addSvgIconLiteral(
      'past',
      this.domSanitizer.bypassSecurityTrustHtml(
        `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 36 8" style="enable-background:new 0 0 36 8;" xml:space="preserve">
        <path class="st0" d="M6,7.3V0.7c0-0.2-0.2-0.5-0.5-0.6C5.2,0,4.8,0,4.5,0.1L0.4,3.4C0.2,3.6,0,3.8,0,4s0.2,0.4,0.4,0.6l4.1,3.3
          c0.3,0.1,0.7,0.1,1,0C5.8,7.8,6,7.6,6,7.3z"/>
        <rect x="4" y="3" class="st0" width="26" height="2"/>
        <circle class="st0" cx="32" cy="4" r="4"/>
        </svg>`
      )
    );
    this.matIconRegistry.addSvgIconLiteral(
      'present',
      this.domSanitizer.bypassSecurityTrustHtml(
        `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 44 8" style="enable-background:new 0 0 44 8;" xml:space="preserve">
        <path class="st0" d="M6,7.3V0.7c0-0.2-0.2-0.5-0.5-0.6C5.2,0,4.8,0,4.5,0.1L0.4,3.4C0.2,3.6,0,3.8,0,4s0.2,0.4,0.4,0.6l4.1,3.3
          c0.3,0.1,0.7,0.1,1,0C5.8,7.8,6,7.6,6,7.3z"/>
        <rect x="4" y="3" class="st0" width="15" height="2"/>
        <path class="st0" d="M38,0.7v6.7c0,0.2,0.2,0.5,0.5,0.6c0.3,0.1,0.7,0.1,1,0l4.1-3.3C43.8,4.4,44,4.2,44,4s-0.2-0.4-0.4-0.6
          l-4.1-3.3c-0.3-0.1-0.7-0.1-1,0C38.2,0.2,38,0.4,38,0.7z"/>
        <rect x="25" y="3" class="st0" width="15" height="2"/>
        <circle class="st0" cx="22" cy="4" r="4"/>
        </svg>`
      )
    );
    this.matIconRegistry.addSvgIconLiteral(
      'future',
      this.domSanitizer.bypassSecurityTrustHtml(
        `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 36 8" style="enable-background:new 0 0 36 8;" xml:space="preserve">
        <path class="st0" d="M30,0.7v6.7c0,0.2,0.2,0.5,0.5,0.6c0.3,0.1,0.7,0.1,1,0l4.1-3.3C35.8,4.4,36,4.2,36,4s-0.2-0.4-0.4-0.6
          l-4.1-3.3c-0.3-0.1-0.7-0.1-1,0C30.2,0.2,30,0.4,30,0.7z"/>
        <rect x="6" y="3" class="st0" width="26" height="2"/>
        <circle class="st0" cx="4" cy="4" r="4"/>
        </svg>`
      )
    );
  }
}
