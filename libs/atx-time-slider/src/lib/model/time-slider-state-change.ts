import { ICriteria } from '@atonix/atx-core';

export interface ITimeSliderStateChange {
  event:
    | 'Initialized'
    | 'ToggleCalendarIndicator'
    | 'SelectDate'
    | 'SelectDateRange'
    | 'SetJumpTo'
    | 'HideTimeSlider'
    | 'ToggleLive'
    | 'RunLive'
    | 'Zoom'
    | 'RangeShift';
  newValue?: string | boolean | Date | ICriteria[];
  newStartDateValue?: Date;
  newEndDateValue?: Date;
  source?: string;
}
