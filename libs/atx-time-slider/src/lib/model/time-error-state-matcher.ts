import { ErrorStateMatcher } from '@angular/material/core';
import { UntypedFormControl, FormGroupDirective, NgForm } from '@angular/forms';

export class TimeErrorStateMatcher implements ErrorStateMatcher {
  constructor(public isInvalidDateFormat: boolean) {}

  isErrorState(
    control: UntypedFormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    return !!(
      control &&
      (control.invalid || this.isInvalidDateFormat) &&
      (control.dirty || control.touched)
    );
  }
}
