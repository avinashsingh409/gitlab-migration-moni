export enum TimeSpanUnits {
  Hours = 'Hours',
  Days = 'Days',
  Weeks = 'Weeks',
  Months = 'Months',
  Years = 'Years',
}
export const TimeSpanUnitMapping = {
  [TimeSpanUnits.Days]: 'Day',
  [TimeSpanUnits.Hours]: 'Hour',
  [TimeSpanUnits.Weeks]: 'Week',
  [TimeSpanUnits.Months]: 'Month',
  [TimeSpanUnits.Years]: 'Year',
};
export enum TimeSpanDirection {
  Past = 'Past',
  Present = 'Present',
  Future = 'Future',
}
