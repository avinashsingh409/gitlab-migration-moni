export interface CalendarDataSingle {
  asOfDate: Date;
  stepBy: string;
}
