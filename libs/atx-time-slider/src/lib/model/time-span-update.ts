export interface TimeSpanUpdate {
  rangeDate: Date;
  timeSpan: number;
}
