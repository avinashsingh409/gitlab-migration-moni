export interface ITimeSliderState {
  dateIndicator: 'Single' | 'Range';
  showCalendarDialog: boolean;
  startDate: Date;
  endDate: Date;
  asOfDate: Date;
  isLive: boolean;
}
