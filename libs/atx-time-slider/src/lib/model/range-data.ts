export interface RangeData {
  startDate: Date;
  endDate: Date;
}
