import { ICriteria } from '@atonix/atx-core';
import { ITimeSliderState } from '../../model/time-slider-state';
import { ITimeSliderStateChange } from '../../model/time-slider-state-change';

// Get the default object to populate the time slider.
export function getDefaultTimeSlider(params?: Partial<ITimeSliderState>) {
  const result: ITimeSliderState = {
    ...{
      dateIndicator: null,
      showCalendarDialog: false,
      startDate: null,
      endDate: null,
      asOfDate: null,
      isLive: false,
    },
    ...params,
  };

  return result;
}

// Helper function to change the state of the time slider based on events that can happen in the time slider.
export function alterTimeSliderState(
  oldState: ITimeSliderState,
  change: ITimeSliderStateChange
) {
  let newState: ITimeSliderState = { ...oldState };
  if (change.event === 'ToggleCalendarIndicator') {
    let newShowCalendarVal: boolean;
    if (change.newValue === undefined) {
      newShowCalendarVal = !oldState.showCalendarDialog;
    } else {
      newShowCalendarVal = change.newValue as boolean;
    }
    newState = { ...oldState, showCalendarDialog: newShowCalendarVal };
  } else if (change.event === 'SelectDate') {
    newState = {
      ...oldState,
      asOfDate: change.newValue ? (change.newValue as Date) : null,
    };
  } else if (change.event === 'SelectDateRange') {
    newState = {
      ...oldState,
      startDate: change.newStartDateValue
        ? (change.newStartDateValue as Date)
        : null,
      endDate: change.newEndDateValue ? (change.newEndDateValue as Date) : null,
    };
  } else if (change.event === 'ToggleLive') {
    let newIsLiveVal: boolean;
    if (change.newValue === undefined) {
      newIsLiveVal = !oldState.isLive;
    } else {
      newIsLiveVal = change.newValue as boolean;
    }
    newState = { ...oldState, isLive: newIsLiveVal };
  }

  return newState;
}

export function initialized() {
  const result: ITimeSliderStateChange = {
    event: 'Initialized',
    newValue: null,
  };
  return result;
}

export function toggleCalendar(newVal?: boolean) {
  const result: ITimeSliderStateChange = {
    event: 'ToggleCalendarIndicator',
    newValue: newVal,
  };
  return result;
}

export function selectDate(asOf: Date, source?: string) {
  const result: ITimeSliderStateChange = {
    event: 'SelectDate',
    newValue: asOf,
    source,
  };
  return result;
}

export function selectDateRange(start: Date, end: Date, source?: string) {
  const result: ITimeSliderStateChange = {
    event: 'SelectDateRange',
    newStartDateValue: start,
    newEndDateValue: end,
    source,
  };
  return result;
}

export function setJumpTo(jumpToList: ICriteria[]) {
  const result: ITimeSliderStateChange = {
    event: 'SetJumpTo',
    newValue: jumpToList,
  };
  return result;
}

// Creates an event message that will hide/close the time slider
export function hideTimeSlider() {
  const result: ITimeSliderStateChange = {
    event: 'HideTimeSlider',
    newValue: null,
  };
  return result;
}

export function toggleLive(newVal?: boolean) {
  const result: ITimeSliderStateChange = {
    event: 'ToggleLive',
    newValue: newVal,
  };
  return result;
}

export function runLive() {
  const result: ITimeSliderStateChange = {
    event: 'RunLive',
    newValue: null,
  };
  return result;
}

export function Zoom() {
  const result: ITimeSliderStateChange = {
    event: 'Zoom',
  };
  return result;
}

export function RangeShift() {
  const result: ITimeSliderStateChange = {
    event: 'RangeShift',
  };
  return result;
}
