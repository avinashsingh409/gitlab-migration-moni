import { TestBed } from '@angular/core/testing';
import { ITimeSliderState } from '../../model/time-slider-state';
import moment from 'moment';
import {
  getDefaultTimeSlider,
  alterTimeSliderState,
  initialized,
  toggleCalendar,
  selectDate,
  selectDateRange,
  setJumpTo,
  hideTimeSlider,
  runLive,
  toggleLive,
} from './time-slider.service';
import { ICriteria } from '@atonix/atx-core';

describe('TimeSliderService', () => {
  const timeSliderState: ITimeSliderState = {
    dateIndicator: 'Single',
    showCalendarDialog: false,
    startDate: null,
    endDate: null,
    asOfDate: moment('1/1/2019', 'M/D/YYYY').toDate(),
    isLive: false,
  };

  beforeEach(() => TestBed.configureTestingModule({}));

  it('should get default', () => {
    const state: ITimeSliderState = {
      dateIndicator: null,
      showCalendarDialog: false,
      startDate: null,
      endDate: null,
      asOfDate: null,
      isLive: false,
    };
    expect(getDefaultTimeSlider()).toEqual(state);
  });

  describe('should alter state', () => {
    it('ToggleCalendarIndicator', () => {
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'ToggleCalendarIndicator',
          newValue: true,
        })
      ).toEqual({
        ...timeSliderState,
        showCalendarDialog: true,
      });
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'ToggleCalendarIndicator',
          newValue: undefined,
        })
      ).toEqual({
        ...timeSliderState,
        showCalendarDialog: true,
      });
    });

    it('SelectDate', () => {
      const asOfDate = moment('1/1/2020', 'M/D/YYYY').toDate();
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'SelectDate',
          newValue: asOfDate,
        })
      ).toEqual({
        ...timeSliderState,
        asOfDate,
      });
    });

    it('SelectDateRange', () => {
      const startDate = moment('6/5/2020', 'M/D/YYYY').toDate();
      const endDate = moment('9/8/2020', 'M/D/YYYY').toDate();
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'SelectDateRange',
          newStartDateValue: startDate,
          newEndDateValue: endDate,
        })
      ).toEqual({
        ...timeSliderState,
        startDate,
        endDate,
      });
    });

    it('ToggleLive', () => {
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'ToggleLive',
          newValue: true,
        })
      ).toEqual({
        ...timeSliderState,
        isLive: true,
      });
      expect(
        alterTimeSliderState(timeSliderState, {
          event: 'ToggleLive',
          newValue: undefined,
        })
      ).toEqual({
        ...timeSliderState,
        isLive: true,
      });
    });
  });

  it('should return initialized event', () => {
    expect(initialized()).toEqual({
      event: 'Initialized',
      newValue: null,
    });
  });

  it('should return toggle calendar event', () => {
    expect(toggleCalendar(true)).toEqual({
      event: 'ToggleCalendarIndicator',
      newValue: true,
    });
  });

  it('should return select date event', () => {
    const asOfDate: Date = moment('4/3/2020', 'M/D/YYYY').toDate();
    expect(selectDate(asOfDate, 'something')).toEqual({
      event: 'SelectDate',
      newValue: asOfDate,
      source: 'something',
    });
  });

  it('should return select date range event', () => {
    const startDate: Date = moment('2/1/2020', 'M/D/YYYY').toDate();
    const endDate: Date = moment('3/2/2020', 'M/D/YYYY').toDate();
    expect(selectDateRange(startDate, endDate, 'something')).toEqual({
      event: 'SelectDateRange',
      newStartDateValue: startDate,
      newEndDateValue: endDate,
      source: 'something',
    });
  });

  it('should return set jump to event', () => {
    const jumpToList: ICriteria[] = [
      {
        CoID: 1,
        GlobalID: '1',
        CoTitle: 'Test',
        CoDescription: 'For Testing purposes',
        CreatedBy: 'Test',
        ChangedBy: 'Test',
        CreateDate: moment().toDate(),
        ChangeDate: moment().toDate(),
        CoDFID: 1,
        RangeFlag: false,
        IndicatorFlag: true,
      },
    ];
    expect(setJumpTo(jumpToList)).toEqual({
      event: 'SetJumpTo',
      newValue: jumpToList,
    });
  });

  it('should return hide time slider', () => {
    expect(hideTimeSlider()).toEqual({
      event: 'HideTimeSlider',
      newValue: null,
    });
  });

  it('should toggle live', () => {
    expect(toggleLive(true)).toEqual({
      event: 'ToggleLive',
      newValue: true,
    });
  });

  it('should run live', () => {
    expect(runLive()).toEqual({
      event: 'RunLive',
      newValue: null,
    });
  });
});
