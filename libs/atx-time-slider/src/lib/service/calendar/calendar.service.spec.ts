import { TestBed, inject } from '@angular/core/testing';
import { CalendarService } from './calendar.service';
import moment from 'moment';
import { TimeSpanDirection, TimeSpanUnits } from '../../model/time-span-enums';

describe('CalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(calendarService).toBeTruthy();
    }
  ));

  describe('should step and return date value of Single Slider', () => {
    it('step minute left', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'left',
            'min',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('6/4/2020 23:59', 'M/D/YYYY HH:mm').toDate());
      }
    ));

    it('step hour right', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'right',
            'hour',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('6/5/2020 01:00', 'M/D/YYYY HH:mm').toDate());
      }
    ));

    it('step day left', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'left',
            'day',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('6/4/2020', 'M/D/YYYY').toDate());
      }
    ));

    it('step week right', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'right',
            'week',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('6/12/2020', 'M/DD/YYYY').toDate());
      }
    ));

    it('step month left', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'left',
            'month',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('5/5/2020', 'M/D/YYYY').toDate());
      }
    ));

    it('step quarter right', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'right',
            'quarter',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('9/5/2020', 'M/D/YYYY').toDate());
      }
    ));

    it('step year left', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(
          calendarService.stepSingle(
            'left',
            'year',
            moment('6/5/2020', 'M/D/YYYY').toDate()
          )
        ).toEqual(moment('6/5/2019', 'M/D/YYYY').toDate());
      }
    ));
  });

  describe('should step and return date values of Range Slider', () => {
    const startDate = moment('9/8/2020', 'M/D/YYYY').toDate();
    const endDate = moment('9/8/2021', 'M/D/YYYY').toDate();

    it('step right', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(calendarService.stepRange('right', startDate, endDate)).toEqual({
          startDate: endDate,
          endDate: moment(
            endDate.getTime() + (endDate.getTime() - startDate.getTime())
          ).toDate(),
        });
      }
    ));

    it('step left', inject(
      [CalendarService],
      (calendarService: CalendarService) => {
        expect(calendarService.stepRange('left', startDate, endDate)).toEqual({
          startDate: moment(
            startDate.getTime() - (endDate.getTime() - startDate.getTime())
          ).toDate(),
          endDate: startDate,
        });
      }
    ));
  });

  it('should return time span in the future', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019', 'M/D/YYYY').toDate(),
          TimeSpanDirection.Future,
          TimeSpanUnits.Days
        )
      ).toEqual({
        startDate: moment('1/1/2019', 'M/D/YYYY').toDate(),
        endDate: moment('1/2/2019', 'M/D/YYYY').toDate(),
      });
    }
  ));

  it('should return time span split 12 hours in the past and 12 in the present', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
          TimeSpanDirection.Present,
          TimeSpanUnits.Days
        )
      ).toEqual({
        startDate: moment('1/1/2019 1:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/2/2019 1:00', 'M/D/YYYY HH:mm').toDate(),
      });
    }
  ));

  it('should return time span split 30 minutes in the past and 30 minutes in the future', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
          TimeSpanDirection.Present,
          TimeSpanUnits.Hours
        )
      ).toEqual({
        startDate: moment('1/1/2019 12:30', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:30', 'M/D/YYYY HH:mm').toDate(),
      });
    }
  ));

  it('should return time span one hour in the past', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
          TimeSpanDirection.Past,
          TimeSpanUnits.Hours
        )
      ).toEqual({
        startDate: moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      });
    }
  ));

  it('should return time span one week in the past', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
          TimeSpanDirection.Past,
          TimeSpanUnits.Weeks
        )
      ).toEqual({
        startDate: moment('12/25/2018 13:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      });
    }
  ));

  it('should return time span one week in the future', inject(
    [CalendarService],
    (calendarService: CalendarService) => {
      expect(
        calendarService.stepTimeSpan(
          1,
          moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
          TimeSpanDirection.Future,
          TimeSpanUnits.Weeks
        )
      ).toEqual({
        startDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/8/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      });
    }
  ));
});
