import { Injectable } from '@angular/core';
import { RangeData } from '../../model/range-data';
import { TimeSpanDirection, TimeSpanUnits } from '../../model/time-span-enums';
import moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class CalendarService {
  public stepSingle(direction: string, stepBy: string, asOfDate: Date): Date {
    let step = 1;
    if (direction === 'left') {
      step = -1;
    }

    switch (stepBy) {
      case 'min':
        asOfDate.setMinutes(asOfDate.getMinutes() + step);
        break;
      case 'hour':
        asOfDate.setHours(asOfDate.getHours() + step);
        break;
      case 'day':
        asOfDate.setDate(asOfDate.getDate() + step);
        break;
      case 'week':
        asOfDate.setDate(asOfDate.getDate() + step * 7);
        break;
      case 'month':
        asOfDate.setMonth(asOfDate.getMonth() + step);
        break;
      case 'quarter':
        asOfDate.setMonth(asOfDate.getMonth() + step * 3);
        break;
      case 'year':
        asOfDate.setFullYear(asOfDate.getFullYear() + step);
        break;
    }
    return asOfDate;
  }

  public stepRange(
    direction: string,
    startDate: Date,
    endDate: Date
  ): RangeData {
    const range: RangeData = {
      startDate,
      endDate,
    };

    if (direction === 'left') {
      range.startDate = moment(
        startDate.getTime() - (endDate.getTime() - startDate.getTime())
      ).toDate();
      range.endDate = moment(startDate).toDate();
    } else {
      range.startDate = moment(endDate).toDate();
      range.endDate = moment(
        endDate.getTime() + (endDate.getTime() - startDate.getTime())
      ).toDate();
    }
    return range;
  }

  public stepWithUnits(
    direction: string,
    startDate: Date,
    endDate: Date,
    timeUnit: TimeSpanUnits,
    interval: number
  ): RangeData {
    const step = direction === 'left' ? -1 : 1;
    const range: RangeData = {
      startDate,
      endDate,
    };
    switch (timeUnit) {
      case TimeSpanUnits.Days:
        range.startDate.setDate(startDate.getDate() + step * interval);
        range.endDate.setDate(endDate.getDate() + step * interval);
        break;
      case TimeSpanUnits.Weeks:
        range.startDate.setDate(startDate.getDate() + step * 7 * interval);
        range.endDate.setDate(endDate.getDate() + step * 7 * interval);
        break;
      case TimeSpanUnits.Hours:
        range.startDate.setHours(startDate.getHours() + step * interval);
        range.endDate.setHours(endDate.getHours() + step * interval);
        break;
      case TimeSpanUnits.Months:
        range.startDate.setMonth(startDate.getMonth() + step * interval);
        range.endDate.setMonth(endDate.getMonth() + step * interval);
        break;
      case TimeSpanUnits.Years:
        range.startDate.setFullYear(startDate.getFullYear() + step * interval);
        range.endDate.setFullYear(endDate.getFullYear() + step * interval);
        break;
      default:
        console.error('time unit not found');
    }
    return range;
  }

  public stepTimeSpan(
    timeSpanInterval: number,
    referenceDate: Date,
    timeSpanDirection: TimeSpanDirection,
    timeUnit: TimeSpanUnits
  ): RangeData {
    let startDate = new Date(referenceDate);
    let endDate = new Date(referenceDate);

    switch (timeUnit) {
      case TimeSpanUnits.Days:
        switch (timeSpanDirection) {
          case TimeSpanDirection.Future:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * 24 * timeSpanInterval)
            );
            break;
          case TimeSpanDirection.Past:
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * 24 * timeSpanInterval)
            );
            break;
          case TimeSpanDirection.Present:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * 24 * timeSpanInterval * 0.5)
            );
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * 24 * timeSpanInterval * 0.5)
            );
            break;
        }
        break;
      case TimeSpanUnits.Hours:
        switch (timeSpanDirection) {
          case TimeSpanDirection.Future:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * timeSpanInterval)
            );
            startDate = referenceDate;
            break;
          case TimeSpanDirection.Past:
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * timeSpanInterval)
            );
            endDate = referenceDate;
            break;
          case TimeSpanDirection.Present:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * timeSpanInterval * 0.5)
            );
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * timeSpanInterval * 0.5)
            );
            break;
        }
        break;
      case TimeSpanUnits.Weeks:
        switch (timeSpanDirection) {
          case TimeSpanDirection.Future:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * 24 * 7 * timeSpanInterval)
            );
            startDate = referenceDate;
            break;
          case TimeSpanDirection.Past:
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * 24 * 7 * timeSpanInterval)
            );
            endDate = referenceDate;
            break;
          case TimeSpanDirection.Present:
            endDate = new Date(
              referenceDate.getTime() +
                Math.round(1000 * 60 * 60 * 24 * 7 * timeSpanInterval * 0.5)
            );
            startDate = new Date(
              referenceDate.getTime() -
                Math.round(1000 * 60 * 60 * 24 * 7 * timeSpanInterval * 0.5)
            );
            break;
        }
        break;
      case TimeSpanUnits.Months:
        switch (timeSpanDirection) {
          case TimeSpanDirection.Future:
            endDate = new Date(
              referenceDate.setMonth(
                referenceDate.getMonth() + timeSpanInterval
              )
            );
            break;
          case TimeSpanDirection.Past:
            startDate = new Date(
              referenceDate.setMonth(
                referenceDate.getMonth() - timeSpanInterval
              )
            );
            break;
          case TimeSpanDirection.Present: {
            const hours = this.GetIntervalInHoursFromReferenceDate(
              timeSpanInterval,
              referenceDate
            );

            endDate = new Date(
              referenceDate.getTime() + Math.round(1000 * 60 * 60 * hours * 0.5)
            );

            startDate = new Date(
              referenceDate.getTime() - Math.round(1000 * 60 * 60 * hours * 0.5)
            );
            break;
          }
        }
        break;
      case TimeSpanUnits.Years:
        switch (timeSpanDirection) {
          case TimeSpanDirection.Future:
            endDate = new Date(
              referenceDate.setFullYear(
                referenceDate.getFullYear() + timeSpanInterval
              )
            );
            break;
          case TimeSpanDirection.Past:
            startDate = new Date(
              referenceDate.setFullYear(
                referenceDate.getFullYear() - timeSpanInterval
              )
            );
            break;
          case TimeSpanDirection.Present: {
            //Convert timespanInterval to months
            timeSpanInterval = timeSpanInterval * 12;
            const hours = this.GetIntervalInHoursFromReferenceDate(
              timeSpanInterval,
              referenceDate
            );
            endDate = new Date(
              referenceDate.getTime() + Math.round(1000 * 60 * 60 * hours * 0.5)
            );
            startDate = new Date(
              referenceDate.getTime() - Math.round(1000 * 60 * 60 * hours * 0.5)
            );
            break;
          }
        }
        break;
      default:
        console.error('time unit not found');
    }
    const range: RangeData = {
      startDate,
      endDate,
    };
    return range;
  }

  GetIntervalInHoursFromReferenceDate(
    timeSpanInterval: number,
    referenceDate: Date
  ) {
    let hours = 0;
    let tempDate = new Date(referenceDate);

    tempDate = new Date(
      tempDate.setMonth(referenceDate.getMonth() - timeSpanInterval)
    );

    hours = Math.floor(
      (referenceDate.getTime() - tempDate.getTime()) / 1000 / 60 / 60
    );

    return hours;
  }
}
