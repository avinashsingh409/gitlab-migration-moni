import { RangeData } from '../../model/range-data';
import { TimeSliderDialogFacade } from './time-slider-dialog.facade';
import moment from 'moment';
import { eachValueFrom } from 'rxjs-for-await';
import { take } from 'rxjs/operators';
import { TimeSliderDialogState } from './time-slider-dialog-state';
import { TimeSpanDirection } from '../../model/time-span-enums';
import { CalendarService } from '../calendar/calendar.service';

describe('TimeSliderDialogFacade', () => {
  it('should create dates and times and flag invalid data', async () => {
    const calendarService = new CalendarService();
    const timeSliderDialogFacade = new TimeSliderDialogFacade(calendarService);
    const range: RangeData = {
      startDate: moment('01-1-2220 11:45', 'MM-DD-YYYY HH:mm').toDate(),
      endDate: moment('01-1-1970 11:45', 'MM-DD-YYYY HH:mm').toDate(),
    };
    timeSliderDialogFacade.setCalendarData(range, true);
    let vmState: TimeSliderDialogState;
    for await (const value of eachValueFrom(
      timeSliderDialogFacade.query.vm$.pipe(take(1))
    )) {
      vmState = value;
    }
    expect(vmState.startTime).toEqual('11:45');
    expect(vmState.startDateAfterEndDate).toEqual(true);
  });

  it('should set the time span reference date to the past by default', async () => {
    const calendarService = new CalendarService();
    const timeSliderDialogFacade = new TimeSliderDialogFacade(calendarService);
    timeSliderDialogFacade.setCalendarData(
      {
        startDate: moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      },
      true
    );
    let vmState: TimeSliderDialogState;
    for await (const value of eachValueFrom(
      timeSliderDialogFacade.query.vm$.pipe(take(1))
    )) {
      vmState = value;
    }
    expect(vmState.timeSpanDirection).toEqual(TimeSpanDirection.Past);
    expect(vmState.timeSpanDate).toEqual(
      moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate()
    );
    expect(vmState.timeSpanTime).toEqual('13:00');
  });

  it('should update the time span reference date to the future', async () => {
    const calendarService = new CalendarService();
    const timeSliderDialogFacade = new TimeSliderDialogFacade(calendarService);
    timeSliderDialogFacade.setCalendarData(
      {
        startDate: moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      },
      true
    );

    let vmState: TimeSliderDialogState;
    for await (const value of eachValueFrom(
      timeSliderDialogFacade.query.vm$.pipe(take(1))
    )) {
      vmState = value;
    }

    const newDate = TimeSliderDialogFacade.setTimeSpanDateAndTimes(
      vmState,
      TimeSpanDirection.Future
    );

    expect(newDate.rangeDate).toEqual(
      moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate()
    );
  });

  it('should update the time span reference date to the present', async () => {
    const calendarService = new CalendarService();
    const timeSliderDialogFacade = new TimeSliderDialogFacade(calendarService);
    timeSliderDialogFacade.setCalendarData(
      {
        startDate: moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate(),
        endDate: moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate(),
      },
      true
    );
    let vmState: TimeSliderDialogState;
    for await (const value of eachValueFrom(
      timeSliderDialogFacade.query.vm$.pipe(take(1))
    )) {
      vmState = value;
    }
    const newDate = TimeSliderDialogFacade.setTimeSpanDateAndTimes(
      vmState,
      TimeSpanDirection.Present
    );
    const middleDate = new Date(
      (moment('1/1/2019 12:00', 'M/D/YYYY HH:mm').toDate().getTime() +
        moment('1/1/2019 13:00', 'M/D/YYYY HH:mm').toDate().getTime()) /
        2
    );
    expect(newDate.rangeDate).toEqual(middleDate);
  });
});
