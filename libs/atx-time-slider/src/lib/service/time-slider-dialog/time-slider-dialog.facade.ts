/* eslint-disable rxjs/no-nested-subscribe */
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { RangeData } from '../../model/range-data';
import moment from 'moment';
import { CalendarService } from '../calendar/calendar.service';
import { UntypedFormControl, Validators } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  map,
  take,
} from 'rxjs/operators';
import { TimeErrorStateMatcher } from '../../model/time-error-state-matcher';
import { TimeSpanDirection, TimeSpanUnits } from '../../model/time-span-enums';
import { TimeSliderDialogQuery } from './time-slider-dialog.query';
import { TimeSliderDialogState } from './time-slider-dialog-state';
import { TimeSpanUpdate } from '../../model/time-span-update';
import { combineTime } from '@atonix/shared/utils';

let _state: TimeSliderDialogState = {
  originalStartDate: null,
  originalEndDate: null,
  startTime: '',
  startDate: null,
  endTime: '',
  endDate: null,
  startDateAfterEndDate: false,
  startPickerStartAt: null,
  endPickerStartAt: null,
  invalidStartDateFormat: false,
  invalidEndDateFormat: false,
  startMin: null,
  startMax: null,
  endMin: null,
  endMax: null,
  timeSpanInterval: 1,
  timeSpanUnit: TimeSpanUnits.Days,
  timeSpanDate: null,
  timeSpanTime: '',
  invalidTimeSpanDateFormat: false,
  timeSpanDirection: TimeSpanDirection.Past,
};

@Injectable()
export class TimeSliderDialogFacade implements OnDestroy {
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<TimeSliderDialogState>(_state);
  public query = new TimeSliderDialogQuery(this.store);
  startDateMatcher: TimeErrorStateMatcher;
  endDateMatcher: TimeErrorStateMatcher;
  timeSpanDateMatcher: TimeErrorStateMatcher;

  static setTimeSpanDateAndTimes(
    state: TimeSliderDialogState,
    direction: TimeSpanDirection
  ): TimeSpanUpdate {
    let rangeDate: Date;
    const startDate = moment(state.originalStartDate);
    const endDate = moment(state.originalEndDate);
    const timeDiff = moment.duration(endDate.diff(startDate));
    let timeSpan = 0;

    switch (direction) {
      case TimeSpanDirection.Future:
        rangeDate = state.originalStartDate;
        break;
      case TimeSpanDirection.Past:
        rangeDate = state.originalEndDate;
        break;
      case TimeSpanDirection.Present:
        // eslint-disable-next-line no-case-declarations
        const middleDate = new Date(
          (state.originalStartDate.getTime() +
            state.originalEndDate.getTime()) /
            2
        );
        rangeDate = middleDate;
        break;
    }
    switch (state.timeSpanUnit) {
      case TimeSpanUnits.Days:
        timeSpan = Math.round(timeDiff.asDays());
        break;
      case TimeSpanUnits.Hours:
        timeSpan = Math.round(timeDiff.asHours());
        break;
      case TimeSpanUnits.Weeks:
        timeSpan = Math.round(timeDiff.asWeeks());
        break;
      case TimeSpanUnits.Months:
        timeSpan = Math.round(timeDiff.asMonths());
        break;
      case TimeSpanUnits.Years:
        timeSpan = Math.round(timeDiff.asYears());
        break;
    }
    return { rangeDate, timeSpan };
  }

  constructor(private calendarService: CalendarService) {}

  setCalendarData(calendarData: RangeData, updateInterval: boolean) {
    const startMin = moment('01-1-1970', 'MM-DD-YYYY').toDate();
    const startMax = calendarData.endDate;
    const endMin = calendarData.startDate;
    const endMax = moment('01-1-9999', 'MM-DD-YYYY').toDate();
    let startDateAfterEndDate = false;
    if (calendarData.startDate.getTime() >= calendarData.endDate.getTime()) {
      startDateAfterEndDate = true;
    }
    this.updateState({
      ..._state,
      originalStartDate: calendarData.startDate,
      originalEndDate: calendarData.endDate,
      startDate: calendarData.startDate,
      startTime: moment(calendarData.startDate).format('HH:mm'),
      endDate: calendarData.endDate,
      endTime: moment(calendarData.endDate).format('HH:mm'),
      startMin: startMin,
      startMax: startMax,
      endMin: endMin,
      endMax: endMax,
      startDateAfterEndDate,
    });
    this.query.vm$.pipe(take(1)).subscribe((vm) => {
      const timeSpanUpdate = TimeSliderDialogFacade.setTimeSpanDateAndTimes(
        vm,
        vm.timeSpanDirection
      );
      if (updateInterval) {
        if (
          timeSpanUpdate.timeSpan === 0 &&
          vm.timeSpanUnit === TimeSpanUnits.Days
        ) {
          const startDate = moment(vm.originalStartDate);
          const endDate = moment(vm.originalEndDate);
          const timeDiff = moment.duration(endDate.diff(startDate));
          timeSpanUpdate.timeSpan = Math.round(timeDiff.asHours());
          this.updateState({
            ..._state,
            timeSpanDate: timeSpanUpdate.rangeDate,
            timeSpanInterval: timeSpanUpdate.timeSpan,
            timeSpanUnit: TimeSpanUnits.Hours,
            timeSpanTime: moment(timeSpanUpdate.rangeDate).format('HH:mm'),
          });
        } else {
          this.updateState({
            ..._state,
            timeSpanDate: timeSpanUpdate.rangeDate,
            timeSpanInterval: timeSpanUpdate.timeSpan,
            timeSpanTime: moment(timeSpanUpdate.rangeDate).format('HH:mm'),
          });
        }
      } else {
        this.updateState({
          ..._state,
          timeSpanDate: timeSpanUpdate.rangeDate,
          timeSpanTime: moment(timeSpanUpdate.rangeDate).format('HH:mm'),
        });
      }
    });
  }

  checkStartAndEndTimes(
    startDate: Date,
    startTime: string,
    endDate: Date,
    endTime: string
  ) {
    const start: Date = combineTime(startDate, startTime);
    const end: Date = combineTime(endDate, endTime);
    if (start.getTime() >= end.getTime()) {
      this.updateState({ ..._state, startDateAfterEndDate: true });
    } else {
      this.updateState({ ..._state, startDateAfterEndDate: false });
    }
  }

  // Allows quick snapshot access to data for ngOnInit() purposes
  getStateSnapshot(): TimeSliderDialogState {
    return { ..._state };
  }

  buildTimeSpanDate(): UntypedFormControl {
    const timeSpanDate = new UntypedFormControl(null, [Validators.required]);
    timeSpanDate.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        let invalidTimeSpanDateFormat = false;
        if (!value) {
          invalidTimeSpanDateFormat = true;
          this.timeSpanDateMatcher = new TimeErrorStateMatcher(true);
        }
        if (
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid()
        ) {
          this.timeSpanDateMatcher = new TimeErrorStateMatcher(false);
        } else {
          invalidTimeSpanDateFormat = true;
          this.timeSpanDateMatcher = new TimeErrorStateMatcher(true);
        }
        this.updateState({
          ..._state,
          timeSpanDate: value,
          invalidTimeSpanDateFormat,
        });
      });
    return timeSpanDate;
  }

  buildTimeSpanDirection(): UntypedFormControl {
    const timeSpanDirection = new UntypedFormControl(null, []);
    timeSpanDirection.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({ ..._state, timeSpanDirection: value });
      });

    return timeSpanDirection;
  }

  buildTimeSpanTime(): UntypedFormControl {
    const timeSpanTime = new UntypedFormControl(null, [Validators.required]);
    timeSpanTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          timeSpanTime: value,
        });
      });
    return timeSpanTime;
  }

  buildStartDate(): UntypedFormControl {
    const startDate = new UntypedFormControl(null, [Validators.required]);
    startDate.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        let invalidStartDateFormat = false;
        if (!value) {
          invalidStartDateFormat = true;
          this.startDateMatcher = new TimeErrorStateMatcher(true);
        }
        if (
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid()
        ) {
          this.startDateMatcher = new TimeErrorStateMatcher(false);
          this.updateState({
            ..._state,
            endMin: value,
            startDate: value,
            startPickerStartAt: value,
            invalidStartDateFormat,
          });
        } else {
          this.updateState({
            ..._state,
            startDate: value,
            startPickerStartAt: value,
            invalidStartDateFormat,
          });
        }
        this.query.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return startDate;
  }
  buildStartTime(): UntypedFormControl {
    const startTime = new UntypedFormControl(null, [Validators.required]);
    startTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          startTime: value,
        });
        this.query.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return startTime;
  }

  buildEndDate(): UntypedFormControl {
    const endDate = new UntypedFormControl(null, [Validators.required]);
    endDate.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        let invalidEndDateFormat = false;
        if (!value) {
          invalidEndDateFormat = true;
          this.endDateMatcher = new TimeErrorStateMatcher(true);
        }
        if (
          moment(value, 'MM/DD/YYYY', true).isValid() ||
          moment(value as string, 'M/D/YYYY', true).isValid()
        ) {
          const startMax = value;
          this.endDateMatcher = new TimeErrorStateMatcher(false);
          this.updateState({
            ..._state,
            startMax: value,
            endPickerStartAt: value,
            endDate: value,
            invalidEndDateFormat,
          });
        } else {
          this.updateState({
            ..._state,
            endPickerStartAt: value,
            endDate: value,
            invalidEndDateFormat,
          });
        }
        this.query.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return endDate;
  }
  buildEndTime(): UntypedFormControl {
    const endTime = new UntypedFormControl(null, [Validators.required]);
    endTime.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.onDestroy)
      )
      .subscribe((value) => {
        this.updateState({
          ..._state,
          endTime: value,
        });
        this.query.vm$.pipe(take(1)).subscribe((vm) => {
          this.checkStartAndEndTimes(
            vm.startDate,
            vm.startTime,
            vm.endDate,
            vm.endTime
          );
        });
      });
    return endTime;
  }

  buildTimeSpanInterval(): UntypedFormControl {
    const timeSpanInterval = new UntypedFormControl(null, [
      Validators.pattern(/^[0-9]\d*$/),
      Validators.min(1),
      Validators.max(200000),
      Validators.required,
    ]);
    timeSpanInterval.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.onDestroy))
      .subscribe((value) => {
        this.updateState({
          ..._state,
          timeSpanInterval: +value,
        });
      });
    return timeSpanInterval;
  }

  changeTimeUnits(newUnits: TimeSpanUnits) {
    this.updateState({
      ..._state,
      timeSpanUnit: newUnits,
    });
  }

  private updateState(state: TimeSliderDialogState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
