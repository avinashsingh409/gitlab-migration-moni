import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { TimeSliderDialogState } from './time-slider-dialog-state';

export class TimeSliderDialogQuery {
  constructor(private store: BehaviorSubject<TimeSliderDialogState>) {}
  private state$ = this.store.asObservable();
  private originalStartDate$ = this.state$.pipe(
    map((state) => state.originalStartDate),
    distinctUntilChanged()
  );
  private originalEndDate$ = this.state$.pipe(
    map((state) => state.originalEndDate),
    distinctUntilChanged()
  );
  private startTime$ = this.state$.pipe(
    map((state) => state.startTime),
    distinctUntilChanged()
  );
  private startDate$ = this.state$.pipe(
    map((state) => state.startDate),
    distinctUntilChanged()
  );
  private endTime$ = this.state$.pipe(
    map((state) => state.endTime),
    distinctUntilChanged()
  );
  private endDate$ = this.state$.pipe(
    map((state) => state.endDate),
    distinctUntilChanged()
  );
  private startDateAfterEndDate$ = this.state$.pipe(
    map((state) => state.startDateAfterEndDate),
    distinctUntilChanged()
  );
  private startPickerStartAt$ = this.state$.pipe(
    map((state) => state.startPickerStartAt),
    distinctUntilChanged()
  );
  private endPickerStartAt$ = this.state$.pipe(
    map((state) => state.endPickerStartAt),
    distinctUntilChanged()
  );
  private invalidStartDateFormat$ = this.state$.pipe(
    map((state) => state.invalidStartDateFormat),
    distinctUntilChanged()
  );
  private invalidEndDateFormat$ = this.state$.pipe(
    map((state) => state.invalidEndDateFormat),
    distinctUntilChanged()
  );
  private startMin$ = this.state$.pipe(
    map((state) => state.startMin),
    distinctUntilChanged()
  );
  private startMax$ = this.state$.pipe(
    map((state) => state.startMax),
    distinctUntilChanged()
  );
  private endMin$ = this.state$.pipe(
    map((state) => state.endMin),
    distinctUntilChanged()
  );
  private endMax$ = this.state$.pipe(
    map((state) => state.endMax),
    distinctUntilChanged()
  );
  private timeSpanInterval$ = this.state$.pipe(
    map((state) => state.timeSpanInterval),
    distinctUntilChanged()
  );
  private timeSpanUnit$ = this.state$.pipe(
    map((state) => state.timeSpanUnit),
    distinctUntilChanged()
  );
  private timeSpanDate$ = this.state$.pipe(
    map((state) => state.timeSpanDate),
    distinctUntilChanged()
  );
  private timeSpanTime$ = this.state$.pipe(
    map((state) => state.timeSpanTime),
    distinctUntilChanged()
  );
  private invalidTimeSpanDateFormat$ = this.state$.pipe(
    map((state) => state.invalidTimeSpanDateFormat),
    distinctUntilChanged()
  );

  private timeSpanDirection$ = this.state$.pipe(
    map((state) => state.timeSpanDirection),
    distinctUntilChanged()
  );

  vm$: Observable<TimeSliderDialogState> = combineLatest([
    this.originalStartDate$,
    this.originalEndDate$,
    this.startTime$,
    this.startDate$,
    this.endTime$,
    this.endDate$,
    this.startDateAfterEndDate$,
    this.startPickerStartAt$,
    this.endPickerStartAt$,
    this.invalidStartDateFormat$,
    this.invalidEndDateFormat$,
    this.startMin$,
    this.startMax$,
    this.endMin$,
    this.endMax$,
    this.timeSpanInterval$,
    this.timeSpanUnit$,
    this.timeSpanDate$,
    this.timeSpanTime$,
    this.invalidTimeSpanDateFormat$,
    this.timeSpanDirection$,
  ]).pipe(
    map(
      ([
        originalStartDate,
        originalEndDate,
        startTime,
        startDate,
        endTime,
        endDate,
        startDateAfterEndDate,
        startPickerStartAt,
        endPickerStartAt,
        invalidStartDateFormat,
        invalidEndDateFormat,
        startMin,
        startMax,
        endMin,
        endMax,
        timeSpanInterval,
        timeSpanUnit,
        timeSpanDate,
        timeSpanTime,
        invalidTimeSpanDateFormat,
        timeSpanDirection,
      ]) => {
        return {
          originalStartDate,
          originalEndDate,
          startTime,
          startDate,
          endTime,
          endDate,
          startDateAfterEndDate,
          startPickerStartAt,
          endPickerStartAt,
          invalidStartDateFormat,
          invalidEndDateFormat,
          startMin,
          startMax,
          endMin,
          endMax,
          timeSpanInterval,
          timeSpanUnit,
          timeSpanDate,
          timeSpanTime,
          invalidTimeSpanDateFormat,
          timeSpanDirection,
        } as TimeSliderDialogState;
      }
    )
  );
}
