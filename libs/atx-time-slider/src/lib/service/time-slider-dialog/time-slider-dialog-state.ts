import { TimeSpanDirection, TimeSpanUnits } from '../../model/time-span-enums';

export interface TimeSliderDialogState {
  originalStartDate: Date;
  originalEndDate: Date;
  startTime: string;
  startDate: Date;
  endTime: string;
  endDate: Date;
  startDateAfterEndDate: boolean;
  startPickerStartAt: Date;
  endPickerStartAt: Date;
  invalidStartDateFormat: boolean;
  invalidEndDateFormat: boolean;
  startMin: Date;
  startMax: Date;
  endMin: Date;
  endMax: Date;
  timeSpanInterval: number;
  timeSpanUnit: TimeSpanUnits;
  timeSpanDate: Date;
  timeSpanTime: string;
  invalidTimeSpanDateFormat: boolean;
  timeSpanDirection: TimeSpanDirection;
}
