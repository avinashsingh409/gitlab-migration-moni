/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { IAtxAssetTree, IAtxAssetTreeNode } from '@atonix/atx-core';
import { catchError, map, switchMap, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AssetTreeDBService {
  constructor(private dbService: NgxIndexedDBService) {}

  public getNodesByTree(treeID: string) {
    return this.dbService.getAllByIndex(
      'assetTreeNodes',
      'TreeId',
      IDBKeyRange.only(treeID)
    );
  }

  public getAllTrees() {
    return this.dbService
      .getAll('assetTrees')
      .pipe(map((trees) => trees.sort()));
  }

  // This will add the asset trees on the indexed db
  public addAssetTreesOnIndxDB(trees: IAtxAssetTree[]): void {
    trees.map((tree) => {
      this.dbService
        .update('assetTrees', tree)
        .pipe(take(1))
        .subscribe(
          (_) => {},
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          (e) => {
            console.error(e);
          }
        );
    });
  }

  public addDefaultTreeNode(
    nodes: IAtxAssetTreeNode[],
    selectedTree: string
  ): void {
    if (nodes?.length > 0) {
      this.dbService
        .update('defaultTreeNode', {
          TreeId: selectedTree,
          UniqueKey: nodes[0].UniqueKey,
        })
        .pipe(take(1))
        .subscribe(
          (_) => {},
          // eslint-disable-next-line rxjs/no-implicit-any-catch
          (e) => {
            console.error(e);
          }
        );
    }
  }

  // This will add the asset tree nodes on the indexed db
  public addAssetTreeNodesOnIndxDB(treeNodes: IAtxAssetTreeNode[]): void {
    treeNodes.map((treeNode) => {
      if (treeNode.UniqueKey) {
        this.dbService
          .update('assetTreeNodes', {
            TreeId: treeNode.TreeId,
            UniqueKey: treeNode.UniqueKey,
            ReferencedBy: treeNode.ReferencedBy,
            ParentUniqueKey: treeNode.ParentUniqueKey,
            ParentNodeId: treeNode.ParentNodeId,
            NodeTypeId: treeNode.NodeTypeId,
            NodeId: treeNode.NodeId,
            NodeDesc: treeNode.NodeDesc,
            NodeAbbrev: treeNode.NodeAbbrev,
            HasChildren: treeNode.HasChildren,
            DisplayOrder: treeNode.DisplayOrder,
            AssetNodeBehaviorId: treeNode.AssetNodeBehaviorId,
            AssetId: treeNode.AssetId,
            AssetGuid: treeNode.AssetGuid,
            Asset: treeNode.Asset,
          })
          .pipe(take(1))
          .subscribe(
            (_) => {},
            // eslint-disable-next-line rxjs/no-implicit-any-catch
            (e) => {
              console.error(e);
            }
          );
      }
    });
  }

  public getNodeById(uniqueKey: string) {
    if (uniqueKey) {
      return this.dbService.getByKey('assetTreeNodes', uniqueKey);
    } else {
      return of(null);
    }
  }

  public getDefaultNode(treeID: string) {
    return this.dbService.getByKey('defaultTreeNode', treeID);
  }

  // This will get all the data of asset trees and asset tree nodes
  public reloadTree(
    treeID?: string,
    nodeID?: string,
    appContextID?: string
  ): Observable<
    { m_Item1: IAtxAssetTree[] | any; m_Item2: IAtxAssetTreeNode[] | any } | any
  > {
    return this.getNodeById(nodeID).pipe(
      switchMap((node) =>
        this.getAllTrees().pipe(
          map((allTrees) => {
            return { allTrees, node };
          })
        )
      ),
      switchMap((values) => {
        if (
          !values ||
          !values.allTrees ||
          values.allTrees.length === 0 ||
          (nodeID && !values.node)
        ) {
          return of(null);
        } else {
          return this.dbService
            .getAllByIndex(
              'assetTreeNodes',
              'TreeId',
              IDBKeyRange.only(
                (values.node as IAtxAssetTreeNode)?.TreeId ??
                  treeID ??
                  this.getDefaultTreeId(values.allTrees as IAtxAssetTree[])
              )
            )
            .pipe(
              // eslint-disable-next-line rxjs/no-implicit-any-catch
              catchError((error) => {
                console.error(error);
                return of(null);
              }),
              map((nodes) => {
                if (!nodes || nodes.length === 0) {
                  return of(null);
                } else {
                  return {
                    m_Item1: values.allTrees,
                    m_Item2: nodes,
                    currentTree:
                      (values.node as IAtxAssetTreeNode)?.TreeId ??
                      treeID ??
                      this.getDefaultTreeId(values.allTrees as IAtxAssetTree[]),
                  };
                }
              })
            );
        }
      })
    );
  }

  private getDefaultTreeId(trees: IAtxAssetTree[]) {
    let defaultID: string = null;
    for (const t of trees) {
      if (t.IsDefaultTree) {
        defaultID = t.TreeId;
        break;
      }
    }
    return defaultID;
  }

  // This will clear data on the Object Stores
  public clearIndxDBData() {
    return this.dbService
      .clear('assetTrees')
      .pipe(switchMap((_) => this.dbService.clear('defaultTreeNode')))
      .pipe(switchMap((_) => this.dbService.clear('assetTreeNodes')));
  }
}
