import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgxIndexedDBModule } from 'ngx-indexed-db';
import { AssetTreeDBService } from './asset-tree-db.service';
import { dbConfig } from '../model/database-config';
import { take } from 'rxjs/operators';
import { eachValueFrom } from 'rxjs-for-await';
import {
  getSelectedTree,
  IAtxAssetTree,
  IAtxAssetTreeNode,
} from '@atonix/atx-core';
require('fake-indexeddb/auto');
let assetTreeDbService: AssetTreeDBService;

const loadNodeFromKeyTrees: Partial<IAtxAssetTree[]> = [
  {
    TreeId: 'myTreeId',
    TreeName: 'Default Asset Tree',
    IsDefaultTree: true,
    IsUserOwned: false,
    CustomerId: 'customer id',
  },
];
const loadNodeFromKeyNodes: IAtxAssetTreeNode[] = [
  {
    NodeId: 'myNode',
    TreeId: 'myTreeId',
    NodeAbbrev: 'All Clients',
    NodeDesc: 'All Clients',
    NodeTypeId: 2,
    DisplayOrder: 10,
    AssetId: 1,
    AssetNodeBehaviorId: 1,
    ParentUniqueKey: null,
    UniqueKey: 'testKey',
    AssetGuid: 'testGuid',
    ReferencedBy: null,
    HasChildren: true,
  },
];

describe('AssetTreeDB Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, NgxIndexedDBModule.forRoot(dbConfig)],
      providers: [AssetTreeDBService],
    });
  });

  beforeEach(inject([AssetTreeDBService], (_assetTreeDbService) => {
    assetTreeDbService = _assetTreeDbService;
  }));

  it('should save asset trees', async () => {
    // Act
    // We add the Asset trees, followed by the top level nodes for each tree
    assetTreeDbService.addAssetTreesOnIndxDB(loadNodeFromKeyTrees);
    assetTreeDbService.addAssetTreeNodesOnIndxDB(loadNodeFromKeyNodes);
    assetTreeDbService.addDefaultTreeNode(
      loadNodeFromKeyNodes,
      getSelectedTree(loadNodeFromKeyNodes)
    );

    // Assert
    const trees = [];
    for await (const value of eachValueFrom(
      assetTreeDbService.getAllTrees().pipe(take(1))
    )) {
      trees.push(value);
    }
    expect(trees.length).toEqual(1);
    const treeId = trees[0][0]?.TreeId;
    // the tree Id we just added should be in the database
    expect(treeId).toEqual(loadNodeFromKeyTrees[0].TreeId);
    const treeNode = [];
    // now we query for the nodes on our tree
    for await (const value of eachValueFrom(
      assetTreeDbService.getNodesByTree(treeId).pipe(take(1))
    )) {
      treeNode.push(value);
    }
    expect(treeNode.length).toEqual(1);
    const myNode = treeNode[0][0]?.NodeAbbrev;
    expect(myNode).toEqual(loadNodeFromKeyNodes[0].NodeAbbrev);
    const defaultNode = [];
    for await (const value of eachValueFrom(
      assetTreeDbService.getDefaultNode(treeId).pipe(take(1))
    )) {
      defaultNode.push(value);
    }
    expect(defaultNode.length).toEqual(1);
    const defaultTreeId = defaultNode[0]?.TreeId;
    const uniqueKey = defaultNode[0]?.UniqueKey;
    // The default treeId and Node Id returned should be from the array above
    expect(defaultTreeId).toEqual(loadNodeFromKeyTrees[0].TreeId);
    expect(uniqueKey).toEqual(loadNodeFromKeyNodes[0].UniqueKey);
  });
});
