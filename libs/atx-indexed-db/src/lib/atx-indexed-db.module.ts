import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxIndexedDBModule } from 'ngx-indexed-db';
import { dbConfig } from './model/database-config';
import { AssetTreeDBService } from './service/asset-tree-db.service';
import { AtxCoreModule } from '@atonix/atx-core';

@NgModule({
  imports: [CommonModule, AtxCoreModule, NgxIndexedDBModule.forRoot(dbConfig)],
  providers: [AssetTreeDBService],
})
export class AtxIndexedDbModule {}
