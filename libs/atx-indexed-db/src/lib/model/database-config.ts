import { DBConfig } from 'ngx-indexed-db';

export const dbConfig: DBConfig = {
  name: 'AssetTreeDB',
  version: 1,
  objectStoresMeta: [
    {
      store: 'assetTrees',
      storeConfig: { keyPath: 'TreeId', autoIncrement: false },
      storeSchema: [],
    },
    {
      store: 'assetTreeNodes',
      storeConfig: { keyPath: 'UniqueKey', autoIncrement: false },
      storeSchema: [
        { name: 'TreeId', keypath: 'TreeId', options: { unique: false } },
      ],
    },
    {
      store: 'defaultTreeNode',
      storeConfig: { keyPath: 'TreeId', autoIncrement: false },
      storeSchema: [
        { name: 'TreeId', keypath: 'TreeId', options: { unique: true } },
      ],
    },
  ],
};
