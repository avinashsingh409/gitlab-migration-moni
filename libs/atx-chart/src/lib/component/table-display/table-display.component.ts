import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ITableData } from '../../models/bar-chart-data';
import {
  isBarTable,
  isBaseSummaryTable,
  getBaseSummaryTableDataSource,
} from '../../service/chart.service';
import { BarChartService } from '../../service/bar-chart.service';
import { IProcessedTrend } from '@atonix/atx-core';

@Component({
  selector: 'atx-table-display',
  templateUrl: './table-display.component.html',
  styleUrls: ['./table-display.component.scss'],
})
export class TableDisplayComponent implements OnChanges {
  @Input() inputTrend: Readonly<IProcessedTrend>;
  @Input() theme: string;
  // the indices hold the numbers in the range [0, 1, ... n] where
  // n is the number of columns in the table - 1.  And this is only used
  // if the user wants a table (instead of a chart, etc).
  indices: number[];

  tableData: ITableData;

  ngOnChanges(changes: SimpleChanges): void {
    if (isBarTable(changes.inputTrend.currentValue)) {
      const trend = changes.inputTrend.currentValue;
      const data = BarChartService.translatePDTrendToBarChartData(trend);
      const dataSource = BarChartService.getTableData(data);
      // This fills an array with indices for the columns. It is [0, 1 ... n]
      // We use it to iterate through the columns and the data.
      this.indices = Array.from(Array(dataSource.columns.length).keys());
      this.tableData = {
        title: trend.trendDefinition.Title,
        columns: dataSource.columns,
        // we do this mapping because the far left column label is blank: "",
        // and the mat-table breaks if you feed it a blank column id.
        // so we append some text to make the blank ones not blank.
        columnIds: dataSource.columns.map(
          (columnText, index) => `columnID: ${index}`
        ),
        data: dataSource.data,
        footers: dataSource.footer,
      };
    } else if (isBaseSummaryTable(changes.inputTrend.currentValue)) {
      const dataSource = getBaseSummaryTableDataSource(
        changes.inputTrend.currentValue,
        this.theme
      );
      // This fills an array with indices for the columns. It is [0, 1 ... n]
      // We use it to iterate through the columns and the data.
      this.indices = Array.from(Array(dataSource.columns.length).keys());
      this.tableData = {
        title: changes.inputTrend.currentValue.trendDefinition.Title,
        columns: dataSource.columns,
        // we do this mapping because the far left column label is blank: "",
        // and the mat-table breaks if you feed it a blank column id.
        // so we append some text to make the blank ones not blank.
        columnIds: dataSource.columns.map(
          (columnText) => 'columnID: "' + columnText + '"'
        ),
        data: dataSource.data,
      };
    }
  }
}
