import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { ITableData } from '../../models/bar-chart-data';
import {
  createHighchartsDefinition,
  isBarChart,
  isBaseSummaryChart,
  isBaseSummaryTable,
  isImage,
  isNd,
  populateBaseSummaryChart,
  isBarTable,
} from '../../service/chart.service';
import { HighchartsService } from '../../service/highcharts.service';
import { IBtnGrpStateChange } from '../../models/button-group-state';
import {
  copyLink,
  changeLabels,
  downloadChart,
  editChart,
  removeActiveYAxis,
  updateLimits,
  removeRectBounds,
  createInitialRectBounds,
  resetLimits,
  getDisplayedAxes,
  legendItemToggle,
} from '../../service/utilities';
import { ExportingOptions, Options } from 'highcharts';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UpdateLimitsDialogComponent } from '../update-limits-dialog/update-limits-dialog.component';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import { IProcessedTrend } from '@atonix/atx-core';
import { ImagesFrameworkService, ICorrelationData } from '@atonix/shared/api';
import { DarkUnicaTheme } from '@atonix/shared/utils';

// ****************************************************************
// this file is much more clear if you view it after doing a "fold all"
// ****************************************************************

@Component({
  selector: 'atx-chart-display',
  templateUrl: './chart-display.component.html',
  styleUrls: ['./chart-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartDisplayComponent implements OnChanges {
  @Input() theme: string;
  @Input() trend: Readonly<IProcessedTrend>;
  @Input() correlationTrend: ICorrelationData[];
  @Input() showToggle: boolean;
  @Input() showFilter: boolean;
  @Input() showNewChart: boolean;
  @Input() showEditChart: boolean;
  @Input() showCopyLink: boolean;
  @Input() showDownload: boolean;
  @Input() allowHiddenLabel: boolean;
  @Input() loading: boolean;
  @Input() isToggled: boolean;
  @Input() hideTitle: boolean;
  @Output() btnGrpStateChange = new EventEmitter<IBtnGrpStateChange>();

  trendDisplayType: '' | 'chart' | 'nd' | 'table' | 'image' | 'multi';
  // data for charts
  highcharts: any;
  chartConfiguration: Highcharts.Options;
  updateChart: boolean; // set this to true every time the chart is updated via chartConfiguration
  chartOneToOne = true; // this is apparently needed when modifying the chart after creation (which we are)

  // data for image
  imagePath: string;

  // data for table
  tableData: ITableData;
  // tableSummaryTypes: ISummaryType[]; // TODO: Load this using ngrx global store

  // data for multi-charts (i.e, multiple area trends)
  multiChartData: Highcharts.Options[];
  exportSettings: ExportingOptions;

  constructor(
    private highchartsService: HighchartsService,
    imagesFrameworkService: ImagesFrameworkService,
    private dialog: MatDialog
  ) {
    this.highcharts = highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.trend?.currentValue) {
      const myTrend = changes.trend.currentValue as Readonly<IProcessedTrend>;
      // The following block resets and clears a few things.
      {
        this.imagePath = null;
        this.chartConfiguration = null;
        this.updateChart = true;
        this.tableData = null;
        this.trendDisplayType = '';
      }
      if (isImage(myTrend)) {
        this.trendDisplayType = 'image';
        this.imagePath = myTrend.trendDefinition.Path;
        if (
          myTrend.trendDefinition.Math?.substr(0, 8).toLowerCase() ===
          'refresh='
        ) {
          this.imagePath += '?cacheBust=1';
          // There is supposed to be some code in here to upate the image on a certain interval.  I am not going to put it in now,
          // since this feature is one that is not ever used.  For future reference the
          // code is in BV.PowerPlantMD.Web.Sites\Common\Charts\hchart.ts
          // lines 115-138 in the configure callback.
        }
      } else if (isNd(myTrend)) {
        this.trendDisplayType = 'nd';
        // I am also delaying implementation of this, there are ND charts in the system, but they are not used often.
        // This will need to be filled in before this is complete though.
      } else if (isBarTable(myTrend)) {
        this.trendDisplayType = 'table';
      } else if (isBarChart(myTrend)) {
        // The bar-chart-display component will take care of this.
        this.trendDisplayType = 'multi';
      } else if (isBaseSummaryTable(myTrend)) {
        this.trendDisplayType = 'table';
      } else if (isBaseSummaryChart(myTrend)) {
        this.trendDisplayType = 'chart';
        this.setHighchart(populateBaseSummaryChart(myTrend, this.theme));
      } else {
        this.trendDisplayType = 'chart';
        this.updateChart = true;
        // The following code block sets this.chartConfiguration = createHighchartsDefinition(...)
        {
          const config = createHighchartsDefinition(
            myTrend.trendDefinition,
            this.theme,
            myTrend.measurements,
            myTrend.startDate,
            myTrend.endDate,
            myTrend.labelIndex,
            this.isToggled,
            this.allowHiddenLabel,
            this.exportSettings,
            this.correlationTrend
          );
          this.setHighchart(config);
        }
      }
    } else if (
      // eslint-disable-next-line no-dupe-else-if
      changes.trend?.currentValue &&
      changes.allowHiddenLabel?.currentValue
    ) {
      this.imagePath = null;
      this.chartConfiguration = null;
      this.updateChart = true;
      this.tableData = null;
      this.trendDisplayType = '';
      this.trendDisplayType = 'chart';
      this.updateChart = true;
      // the following creates a high charts definition and sets it = this.chartConfiguration
      {
        const config = createHighchartsDefinition(
          this.trend.trendDefinition,
          this.theme,
          this.trend.measurements,
          this.trend.startDate,
          this.trend.endDate,
          this.trend.labelIndex,
          false,
          this.allowHiddenLabel,
          this.exportSettings,
          this.correlationTrend
        );
        this.setHighchart(config);
      }
    }
  }

  setHighchart(config: Options) {
    if (this.hideTitle && config) {
      config.title = undefined;
    }
    if (this.theme === 'dark') {
      config = this.highcharts.merge(DarkUnicaTheme, config);
    }

    config.chart.events = {
      render: this.setOnRender(),
    };

    config.plotOptions.series.events.legendItemClick = (event) => {
      if (!this.correlationTrend) {
        this.btnGrpStateChange.emit(
          legendItemToggle(event.target.userOptions.legendIndex)
        );
      }
    };

    this.chartConfiguration = config;
  }

  public setOnRender() {
    return () => {
      setTimeout(() => {
        const yAxis: any = Array.from(
          document.querySelectorAll(`.chart-${this.trend.id} .highcharts-yaxis`)
        );
        yAxis.map((y, index) => {
          const rect = createInitialRectBounds(y);
          // This will add click event to the clickable area
          rect.addEventListener('click', (event) => {
            removeActiveYAxis();
            rect.setAttribute('class', 'activeYAxis rectBounds');
            rect.setAttribute(
              'style',
              'fill-opacity: 0; stroke-width: 1px; stroke: #6ea09d;'
            );

            const chartBackground: any = document.querySelector(
              `.chart-${this.trend.id}`
            );
            const chartBounding = chartBackground?.getBoundingClientRect();
            const displayedAxes = getDisplayedAxes(this.trend.trendDefinition);

            const dialogRef = this.dialog.open(UpdateLimitsDialogComponent, {
              width: '400px',
              position: {
                left: `${chartBounding?.x + 10}px`,
                top: `${chartBounding?.y + chartBounding?.height - 90}px`,
              },
              panelClass: 'custom-dialog-container',
              data: {
                AxisIndex: index,
                Min: displayedAxes[index]?.Min,
                Max: displayedAxes[index]?.Max,
              },
            });

            dialogRef.componentInstance.updateValues.subscribe(
              (data: IUpdateLimitsData) => {
                this.updateLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.componentInstance.resetValues.subscribe(
              (data: number) => {
                this.resetLimits(data);
                dialogRef.close();
              }
            );

            dialogRef.afterClosed().subscribe((result) => {
              removeActiveYAxis();
            });
          });

          y.parentElement.appendChild(rect);
        });
      }, 100);
    };
  }

  public changeLabels(event: Event) {
    event.stopImmediatePropagation();
    this.btnGrpStateChange.emit(changeLabels());
  }

  public copyLink() {
    this.btnGrpStateChange.emit(copyLink(window.location.href));
  }

  public editChart(event: Event) {
    event.stopImmediatePropagation();
    this.btnGrpStateChange.emit(editChart());
  }

  public downloadChart() {
    this.btnGrpStateChange.emit(downloadChart(this.chartConfiguration));
  }

  public updateLimits(event: IUpdateLimitsData) {
    this.btnGrpStateChange.emit(updateLimits(event));
  }

  public resetLimits(event: number) {
    this.btnGrpStateChange.emit(resetLimits(event));
  }
}
