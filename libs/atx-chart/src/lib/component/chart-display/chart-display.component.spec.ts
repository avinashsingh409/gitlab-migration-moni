import { HighchartsChartModule } from 'highcharts-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatTableModule } from '@angular/material/table';
import { ChartDisplayComponent } from './chart-display.component';
import { AuthService, JwtInterceptorService } from '@atonix/shared/state/auth';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('ChartDisplayComponent', () => {
  let component: ChartDisplayComponent;
  let fixture: ComponentFixture<ChartDisplayComponent>;

  let mockAuthService: AuthService;

  beforeEach(() => {
    mockAuthService = createMockWithValues(AuthService, {
      GetIdTokenOrWait: jest.fn(),
      GetIdToken: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        AtxMaterialModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
      ],
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChartDisplayComponent],
      imports: [HighchartsChartModule, HttpClientTestingModule, MatTableModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
