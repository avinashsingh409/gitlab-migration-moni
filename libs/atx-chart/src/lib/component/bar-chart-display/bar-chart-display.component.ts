import {
  Component,
  OnInit,
  Input,
  OnChanges,
  ElementRef,
  Output,
  EventEmitter,
  Inject,
} from '@angular/core';
import { BarChartService } from '../../service/bar-chart.service';
import { HighchartsService } from '../../service/highcharts.service';
import { ExportingOptions } from 'highcharts';

import {
  removeActiveYAxis,
  removeRectBounds,
  createInitialRectBounds,
  getDisplayedAxes,
} from '../../service/utilities';
import { MatDialog } from '@angular/material/dialog';
import { UpdateLimitsDialogComponent } from '../update-limits-dialog/update-limits-dialog.component';
import { IUpdateLimitsData } from '../../models/update-limits-data';
import { IProcessedTrend } from '@atonix/atx-core';
import { ImagesFrameworkService, IUserPreferences } from '@atonix/shared/api';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
@Component({
  selector: 'atx-bar-chart-display',
  templateUrl: './bar-chart-display.component.html',
  styleUrls: ['./bar-chart-display.component.scss'],
})
export class BarChartDisplayComponent implements OnChanges {
  @Input() trend: Readonly<IProcessedTrend>;
  @Input() theme: string;
  @Input() hideTitle: boolean;
  @Output() updateLimits = new EventEmitter<IUpdateLimitsData>();
  @Output() resetLimits = new EventEmitter<number>();
  highcharts: any;
  charts: Highcharts.Options[];
  exportSettings: ExportingOptions;

  constructor(
    private hostElement: ElementRef,
    highchartsService: HighchartsService,
    imagesFrameworkService: ImagesFrameworkService,
    private dialog: MatDialog,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.highcharts = highchartsService.highcharts();
    this.exportSettings = imagesFrameworkService.getExportSettings();
  }

  ngOnChanges() {
    const height = this.hostElement.nativeElement.clientHeight;
    let totalHeight = 0;
    let totalStars = 0;

    const data = BarChartService.translatePDTrendToBarChartData(this.trend);
    const multiChartData = BarChartService.getChartData(
      data,
      this.exportSettings,
      this.appConfig.baseSiteURL,
      this.theme
    );
    for (const d of multiChartData) {
      d.chart.events = {
        render: this.setOnRender(),
      };

      if (d.chart.height === null) {
        totalStars++;
      } else {
        totalHeight += d.chart.height as number;
      }
    }
    const starHeight = (height - totalHeight) / totalStars;
    if (starHeight > 0) {
      for (const d of multiChartData) {
        if (d.chart.height === null) {
          d.chart.height = starHeight;
        }
      }
    }

    if (this.hideTitle && multiChartData && multiChartData.length > 0) {
      multiChartData[0].title = undefined;
    }

    this.charts = multiChartData;
  }

  public setOnRender() {
    return () => {
      setTimeout(() => {
        const yAxis: any = Array.from(
          document.querySelectorAll(`.chart-${this.trend.id} .highcharts-yaxis`)
        );
        yAxis.map((y, index) => {
          const rect = createInitialRectBounds(y);
          // This will add click event to the clickable area
          rect.addEventListener('click', (event) => {
            removeActiveYAxis();
            rect.setAttribute('class', 'activeYAxis rectBounds');
            rect.setAttribute(
              'style',
              'fill-opacity: 0; stroke-width: 1px; stroke: #6ea09d;'
            );

            const chartBackground: any = document.querySelector(
              `.chart-${this.trend.id}`
            );
            const chartBounding = chartBackground?.getBoundingClientRect();
            const displayedAxes = getDisplayedAxes(this.trend.trendDefinition);

            const dialogRef = this.dialog.open(UpdateLimitsDialogComponent, {
              width: '400px',
              position: {
                left: `${chartBounding?.x + 10}px`,
                top: `${chartBounding?.y + chartBounding?.height - 90}px`,
              },
              panelClass: 'custom-dialog-container',
              data: {
                AxisIndex: index,
                Min: displayedAxes[index]?.Min,
                Max: displayedAxes[index]?.Max,
              },
            });

            dialogRef.componentInstance.updateValues.subscribe(
              (data: IUpdateLimitsData) => {
                this.updateLimits.emit(data);
                dialogRef.close();
              }
            );

            dialogRef.componentInstance.resetValues.subscribe(
              (data: number) => {
                this.resetLimits.emit(data);
                dialogRef.close();
              }
            );

            dialogRef.afterClosed().subscribe((result) => {
              removeActiveYAxis();
            });
          });

          y.parentElement.appendChild(rect);
        });
      }, 100);
    };
  }
}
