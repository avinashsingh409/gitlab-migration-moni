import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialogModule,
} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { UpdateLimitsDialogComponent } from './update-limits-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('UpdateLimitsDialogComponent', () => {
  const limitsData = {
    AxisIndex: 0,
    Min: null,
    Max: null,
  };

  const mockDialogRef = {
    close: jest.fn(),
  };

  let component: UpdateLimitsDialogComponent;
  let fixture: ComponentFixture<UpdateLimitsDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            data: limitsData,
          },
        },
      ],
      imports: [
        FormsModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      declarations: [UpdateLimitsDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLimitsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
