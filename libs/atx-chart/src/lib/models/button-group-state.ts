import { IUpdateLimitsData } from './update-limits-data';

export interface IBtnGrpStateChange {
  event:
    | 'ChangeLabels'
    | 'CopyLink'
    | 'DownloadChart'
    | 'EditChart'
    | 'UpdateLimits'
    | 'ResetLimits'
    | 'LegendItemToggle';
  newValue?: boolean | string | number | IUpdateLimitsData;
  newConfig?: Highcharts.Options;
}
