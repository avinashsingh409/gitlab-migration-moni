import { IPDTrendBandAxisMeasurementMarkerConfig } from '@atonix/atx-core';

// Base class that identifies the type of data to put on the chart
export interface IValueType {
  ValueTypeId: number;
  AxisId: number;
  Name: string;
  Type: string;
  Inverted: boolean;
}

// A range that will fill with a particular pattern or color from one point to another.
export interface IRangeValueType extends IValueType {
  FillColor: string;
  BorderColor: string;
  BorderThickness: number;
  TooltipHideMin: boolean;
}

// A symbol that will appear at one place on the bar.
export interface ISymbolValueType extends IValueType {
  Symbol: string;
  Offset: number;
  Color: string;
}

// A symbol for a line that will connect between the bars.
export interface ILineValueType extends IValueType {
  Color: string;
  LineColor: string;
  Thickness: number;
}

// A box and whiskers plot that will fill with a particular pattern or color
export interface IBoxAndWhiskersValueType extends IValueType {
  FillColor: string;
  BorderColor: string;
  BorderThickness: number;
  TooltipHideMin: boolean;
}

// An area that will fill with a particular pattern or color from one point to another.
export interface IAreaValueType extends IValueType {
  FillColor: string;
  BorderColor: string;
  BorderThickness: number;
}

export interface IPDTrendBandAxisBarMeasuredValue {
  MeasurementID: number;
  Value1: number;
  Value2: number;
  Value3: number;
  Value4: number;
  Value5: number;
  Points: number[];
  Markers: IPDTrendBandAxisMeasurementMarkerConfig[];
}

// This represents a bar on the chart.
// In a situation where each bar is an asset this will be the asset.
export interface IBar {
  BarId: number;
  DisplayOrder: number;
  Title: string;
  Data: IPDTrendBandAxisBarMeasuredValue[];
}

export interface IAxis {
  AxisId: number;
  Axis: number;
  Name: string;
  Min: number;
  Max: number;
  AreaId: number;
  Opposite: boolean;
  Hide: boolean;
  Units: string;
  TickInterval: number;
  MinorTickInterval: number;
  GridLine: boolean;
}

// The idea here is that each area is a horizontal division of the screen.
// If the area has a size that is null it will take up whatever space is left over.
export interface IArea {
  AreaId: number;
  Size: number;
  Invert: boolean;
}

export interface IBarChartData {
  Bars: IBar[];
  Axes: IAxis[];
  Areas: IArea[];
  ValueTypes: IValueType[];

  Title: string;
  XAxisLabel: string;
  XAxisOpposite: boolean;
  RenderAs: string;
  Summarize: string;
  ChartType: number;

  BarWidth: number;

  StartAngle: number;
  EndAngle: number;
  PlotClockwise: boolean;
}

export interface ITableData {
  title: string;
  data: any;
  columns?: any[];
  footers?: any[];
  // These are used in the mat-table if the user selects a table.
  // we need this instead of the "columns" property because we like to
  // have our far left column be blank, or "", which is a problem in the
  // mat-table because it doesn't like column ids being blank.  So we added
  // this property, and when we set it, we just append some text to the
  // beginning of the "columns" property.
  columnIds?: string[];
}
