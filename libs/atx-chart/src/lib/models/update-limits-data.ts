export interface IUpdateLimitsData {
  AxisIndex: number;
  Min: number;
  Max: number;
}
