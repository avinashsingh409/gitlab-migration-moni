import { IProcessedTrend } from '@atonix/atx-core';

export interface ITrendSelectorState {
  charts: IProcessedTrend[];
  selectedChart: number;
}
