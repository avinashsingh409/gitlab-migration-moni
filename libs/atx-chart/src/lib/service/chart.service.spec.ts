import { XAxisOptions, YAxisOptions } from 'highcharts';
import {
  boilerLossesLineChart,
  br1ScatterWithPins,
  grossGenTableLastValue,
  isBarOrColumnChart,
  isGroupedSeriesChart,
  isStandardTable,
  mockMeasurements,
  mockSolidFuelUnit,
  mockTrendBoiler,
} from '@atonix/atx-chart-v2';
import {
  getTrendTotalSeries,
  GroupedSeriesType,
  IProcessedTrend,
} from '@atonix/atx-core';
import { createHighchartsDefinition } from './chart.service';

describe('processTrendDefinition', () => {
  it('should process a line chart', () => {
    const result = {
      id: String(boilerLossesLineChart.PDTrendID),
      label: boilerLossesLineChart.Title,
      trendDefinition: boilerLossesLineChart,
      groupedSeriesSubType: '',
      groupedSeriesType: GroupedSeriesType.NONE,
      totalSeries: getTrendTotalSeries(boilerLossesLineChart),
      isDirty: false,
      labelIndex: null,
    } as IProcessedTrend;
    expect(result).toBeTruthy();
    expect(isGroupedSeriesChart(result)).toEqual(false);
    expect(isStandardTable(result)).toEqual(false);
    expect(isBarOrColumnChart(result)).toEqual(false);
  });

  it('should process a scatter with pins', () => {
    const result = {
      id: String(br1ScatterWithPins.PDTrendID),
      label: br1ScatterWithPins.Title,
      trendDefinition: br1ScatterWithPins,
      groupedSeriesSubType: '',
      groupedSeriesType: GroupedSeriesType.NONE,
      totalSeries: getTrendTotalSeries(br1ScatterWithPins),
      isDirty: false,
      labelIndex: null,
    } as IProcessedTrend;

    expect(result).toBeTruthy();
    expect(isGroupedSeriesChart(result)).toEqual(false);
    expect(isStandardTable(result)).toEqual(false);
    expect(isBarOrColumnChart(result)).toEqual(false);
  });

  it('should process a table', () => {
    const result = {
      id: String(grossGenTableLastValue.PDTrendID),
      label: grossGenTableLastValue.Title,
      trendDefinition: grossGenTableLastValue,
      groupedSeriesSubType: '',
      groupedSeriesType: GroupedSeriesType.NONE,
      totalSeries: getTrendTotalSeries(grossGenTableLastValue),
      isDirty: false,
      labelIndex: null,
    } as IProcessedTrend;
    expect(result).toBeTruthy();
    expect(isGroupedSeriesChart(result)).toEqual(false);
    expect(isStandardTable(result)).toEqual(true);
    expect(isBarOrColumnChart(result)).toEqual(false);
  });

  it('should generate a Highcharts object', () => {
    const result = createHighchartsDefinition(boilerLossesLineChart, 'dark');
    expect(result).toBeTruthy();
  });

  it('should create a highcharts definition correctly', () => {
    const trendDef = createHighchartsDefinition(mockTrendBoiler, 'dark');
    expect(trendDef).toBeTruthy();
    expect(trendDef.series.length).toEqual(20);
    expect(trendDef.series[0].id).toEqual('tag:217766:pin:11633:archive:');
    expect(trendDef.series[0].yAxis).toEqual('axis1:965979');
    expect((trendDef.xAxis as XAxisOptions).type).toEqual('datetime');
    expect((trendDef.yAxis as YAxisOptions[]).length).toEqual(1);
    expect((trendDef.yAxis as YAxisOptions[])[0].title.text).toEqual('Btu/kWh');
    expect(trendDef.chart.type).toEqual('table');
    expect(trendDef.chart.zoomType).toEqual('xy');
  });

  it('should create a highcharts definition with data correctly', () => {
    const startDate = new Date('2020-02-29T06:00:00.000Z');
    const endDate = new Date('2020-04-10T20:15:54.244Z');
    const labelIndex = 0;
    const isToggled = false;
    const allowHiddenLabels = true;

    const trendDef = createHighchartsDefinition(
      mockSolidFuelUnit,
      'dark',
      mockMeasurements,
      startDate,
      endDate,
      labelIndex,
      isToggled,
      allowHiddenLabels
    );
    expect(trendDef).toBeTruthy();
    expect(trendDef.series.length).toEqual(3);
    expect(trendDef.series[0].id).toEqual('tag:217794:pin::archive:');
    expect(trendDef.series[0].yAxis).toEqual('axis2:776291');
    expect((trendDef.xAxis as XAxisOptions).type).toEqual('datetime');
    expect((trendDef.xAxis as XAxisOptions).min).toEqual(1582956000000);
    expect((trendDef.xAxis as XAxisOptions).max).toEqual(1586549754244);
    expect((trendDef.yAxis as YAxisOptions[]).length).toEqual(2);
    expect((trendDef.yAxis as YAxisOptions[])[0].title.text).toEqual('MW');
    expect(trendDef.chart.type).toEqual('line');
    expect(trendDef.chart.zoomType).toEqual('xy');
  });
});
