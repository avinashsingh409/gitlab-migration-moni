import { XAxisOptions, YAxisOptions } from 'highcharts';
import {
  GroupedSeriesType,
  IAssetMeasurementsSet,
  IPDTrend,
  IPDTrendAxis,
  IPDTrendBandAxisMeasurement,
  IProcessedTrend,
  sigFig,
} from '@atonix/atx-core';
import { IDataFormatOption } from '../models/data-format-option';
import { IBtnGrpStateChange } from '../models/button-group-state';
import { BehaviorSubject } from 'rxjs';
import { IUpdateLimitsData } from '../models/update-limits-data';
import produce from 'immer';
import { findIndex } from 'lodash';

//copied from chartsv2 module because v1 tables still exist
export function toPrecisionCustom(num: number, precision: number): string {
  if (precision <= 0) {
    return '';
  }

  if (num == 0) {
    return '0';
  }

  let numString: string = num.toPrecision(precision);
  let index: number = numString.length - 1;
  let char: string = numString.charAt(index);

  while (char === '0' || char === '.') {
    numString = numString.substring(0, index); //removes the last character in the string
    index = numString.length - 1;
    char = numString.charAt(index); //gets the new last character in the string
  }

  return numString;
}

export function getNumberBySigFigs(input: number, digits: number): number {
  return Number(sigFig(input, digits));
}

export function getBandAxisMeasurementIndex(
  measurements: IPDTrendBandAxisMeasurement[],
  bandAxisMeasurement: IPDTrendBandAxisMeasurement
) {
  const idx = findIndex(
    measurements,
    (x) =>
      x.TagID1 === bandAxisMeasurement.TagID1 &&
      x.Color === bandAxisMeasurement.Color &&
      x.Type === bandAxisMeasurement.Type
  );

  return idx;
}

export function toolTipFormatter(
  val: number,
  formatOpt: IDataFormatOption
): string {
  let formattedString = String('');

  if (formatOpt.isRounded) {
    formattedString = `${Math.round(val).toLocaleString(formatOpt.locale, {
      style: formatOpt.format,
      currency: formatOpt.currency,
      minimumFractionDigits: formatOpt.decimalPlaces,
      maximumFractionDigits: 0,
    })}`;
  } else {
    formattedString = `${val.toLocaleString(formatOpt.locale, {
      style: formatOpt.format,
      currency: formatOpt.currency,
      minimumFractionDigits: formatOpt.decimalPlaces,
      maximumFractionDigits: formatOpt.decimalPlaces,
    })} `;
  }
  return formattedString;
}

// put any properties that needs to be cleared from the chart axes here.
export const defaultXAxis: XAxisOptions = {
  min: null,
  max: null,
  endOnTick: null,
  maxPadding: null,
  gridLineWidth: null,
  categories: null,
  showEmpty: null,
  tickLength: null,
};

export const defaultYAxis: YAxisOptions = {
  min: null,
  max: null,
  endOnTick: null,
  maxPadding: null,
  gridLineWidth: null,
  categories: null,
  showEmpty: null,
  tickLength: null,
};

export function getEmptyTrend(title?: string) {
  const result: IProcessedTrend = {
    id: '',
    groupedSeriesType: GroupedSeriesType.NONE,
    groupedSeriesSubType: '',
    label: title ?? '',
    trendDefinition: {
      PDTrendID: -1,
      TrendDesc: title ?? '',
      Title: title ?? '',
      LegendVisible: true,
      LegendDock: 0,
      LegendContentLayout: 0,
      IsCustom: true,
      CreatedBy: '',
      IsPublic: true,
      DisplayOrder: 0,
      ChartType: null,
      Series: [],
      Axes: [],
      IsStandardTrend: false,
      Filter: null,
      Pins: [],
      PinTypeID: 0,
      ShowPins: true,
      ShowSelected: true,
      SummaryType: null,
      Categories: [],
      XAxisGridlines: false,
    },
    totalSeries: 0,
  };
  return result;
}

export function getLoadingDataTrend(defaultValues?: Partial<IProcessedTrend>) {
  const result = getEmptyTrend('Loading ...');
  return { ...result, ...defaultValues };
}

export function getNoDataTrend(defaultValues?: Partial<IProcessedTrend>) {
  const result = getEmptyTrend('No Data Available');
  return { ...result, ...defaultValues };
}

export function setDataOnTrend(
  trend: IProcessedTrend,
  measurements: IAssetMeasurementsSet
) {
  const newTrend = produce(trend, (draftState) => {
    if (trend?.trendDefinition?.Pins) {
      // Not sure why we need to do this, but right now these Pins
      // need to come from the tagsDataFiltered api call
      draftState.trendDefinition.Pins = measurements.Pins;
    }

    draftState.measurements = measurements;
  });
  return newTrend;
}

export function copyLink(link: string) {
  const result: IBtnGrpStateChange = {
    event: 'CopyLink',
    newValue: link,
  };
  return result;
}

export function changeLabels() {
  const result: IBtnGrpStateChange = {
    event: 'ChangeLabels',
  };
  return result;
}

export function editChart() {
  const result: IBtnGrpStateChange = {
    event: 'EditChart',
  };
  return result;
}

export function downloadChart(config: Highcharts.Options) {
  const result: IBtnGrpStateChange = {
    event: 'DownloadChart',
    newConfig: config,
  };
  return result;
}

export function updateLimits(limitsData: IUpdateLimitsData) {
  const result: IBtnGrpStateChange = {
    event: 'UpdateLimits',
    newValue: limitsData,
  };
  return result;
}

export function resetLimits(axisIndex: number) {
  const result: IBtnGrpStateChange = {
    event: 'ResetLimits',
    newValue: axisIndex,
  };
  return result;
}

export function legendItemToggle(legendItemIndex: number) {
  const result: IBtnGrpStateChange = {
    event: 'LegendItemToggle',
    newValue: legendItemIndex,
  };
  return result;
}

export function copyToClipboard(link: string) {
  let result$: BehaviorSubject<boolean> = null;
  const selBox = document.createElement('textarea');

  selBox.value = link;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  const copy = document.execCommand('copy');
  if (copy) {
    result$ = new BehaviorSubject<boolean>(true);
  } else {
    result$ = new BehaviorSubject<boolean>(false);
  }
  document.body.removeChild(selBox);
  return result$;
}

export function createInitialRectBounds(yAxis) {
  // Standard code to create SVG element
  // https://developer.mozilla.org/en-US/docs/Web/API/Document/createElementNS
  const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
  // This will create the bounds of the clickable area
  if (yAxis) {
    const boundingRect = yAxis.getBBox();
    rect.setAttribute('width', boundingRect.width.toString());
    rect.setAttribute('height', (boundingRect.height + 20).toString());
    rect.setAttribute('x', boundingRect.x.toString());
    rect.setAttribute('y', (boundingRect.y - 10).toString());
    rect.setAttribute('fill', 'white');
    rect.setAttribute('style', 'fill-opacity: 0');
    rect.setAttribute('class', 'rectBounds');
  }

  return rect;
}

export function removeRectBounds() {
  const rectBounds = document.querySelectorAll('.rectBounds');
  rectBounds.forEach((rect) => {
    rect.parentElement.removeChild(rect);
  });
}

export function removeActiveYAxis() {
  const activeYAxis = document.querySelector('.activeYAxis');
  activeYAxis?.removeAttribute('class');
  activeYAxis?.setAttribute('class', 'rectBounds');
  activeYAxis?.setAttribute('style', 'fill-opacity: 0');
}

export function getDisplayedAxes(trendDefinition: IPDTrend) {
  const displayedAxes: IPDTrendAxis[] = [];
  const newAxes = [...trendDefinition.Axes];

  newAxes.map((x) => {
    if (
      trendDefinition.Series.filter((s) => s.Axis === x.Axis && !s.IsXAxis)
        .length > 0
    ) {
      displayedAxes.push(x);
    }
  });

  if (displayedAxes.length === 0) {
    newAxes.map((x) => {
      displayedAxes.push(x);
    });
  }

  return displayedAxes;
}

export function updateAxes(
  trendDefinition: IPDTrend,
  limitsData: IUpdateLimitsData
) {
  const displayedAxes = getDisplayedAxes(trendDefinition);
  const newAxes = [...trendDefinition.Axes];

  const idx = newAxes.findIndex(
    (x) => x.Axis === displayedAxes[limitsData.AxisIndex].Axis
  );
  if (idx >= 0) {
    newAxes.splice(idx, 1, {
      ...newAxes[idx],
      Min: limitsData.Min,
      Max: limitsData.Max,
    });
  }

  return newAxes;
}

export function resetAxes(trendDefinition: IPDTrend, axisIndex: number) {
  const displayedAxes = getDisplayedAxes(trendDefinition);
  const newAxes = [...trendDefinition.Axes];

  const idx = newAxes.findIndex(
    (x) => x.Axis === displayedAxes[axisIndex].Axis
  );
  if (idx >= 0) {
    newAxes.splice(idx, 1, {
      ...newAxes[idx],
      Min: newAxes[idx].OriginalMin,
      Max: newAxes[idx].OriginalMax,
    });
  }

  return newAxes;
}

export function updateTrendAxes(trend: IProcessedTrend) {
  const trendDefinition = { ...trend.trendDefinition };
  const Axes = [...trendDefinition.Axes].map((axis) => {
    const newAxis = { ...axis };
    newAxis.OriginalMin = newAxis.Min;
    newAxis.OriginalMax = newAxis.Max;
    return newAxis;
  });
  return {
    ...trend,
    trendDefinition: {
      ...trendDefinition,
      Axes,
    },
  };
}
