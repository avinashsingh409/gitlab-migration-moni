import * as Highcharts from 'highcharts';
import cloneDeep from 'lodash/cloneDeep';
import {
  IArea,
  IAreaValueType,
  IAxis,
  IBar,
  IBarChartData,
  IBoxAndWhiskersValueType,
  ILineValueType,
  IRangeValueType,
  ISymbolValueType,
  IValueType,
} from '../models/bar-chart-data';
import { getBandAxisMeasurementIndex, getNumberBySigFigs } from './utilities';
import { getDefaultHighchartsOptions } from './chart.service';
import {
  IPDTrend,
  IPDTrendArea,
  IPDTrendBandAxisBarMeasuredValue,
  IPDTrendBandAxisMeasurement,
  IPDTrendBandAxisMeasurementMarkerConfig,
  IProcessedTrend,
  isNil,
} from '@atonix/atx-core';
import { produce } from 'immer';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

// Range is called 'column' on the UI
export type VALUE_TYPES =
  | 'range'
  | 'symbol'
  | 'line'
  | 'boxandwhiskers'
  | 'area';
export type VALUE_TYPE_DEFAULTS =
  | Partial<IValueType>
  | Partial<IRangeValueType>
  | Partial<ISymbolValueType>
  | Partial<ILineValueType>
  | Partial<IBoxAndWhiskersValueType>
  | Partial<IAreaValueType>;

export function getValueType(val: IBarChartData, valueTypeId: number) {
  return val.ValueTypes.find((vt) => vt.ValueTypeId === valueTypeId);
}

export class BarChartService {
  public static digits = 4;

  public static createBarChartData(val: Partial<IBarChartData>) {
    return {
      ...{
        Bars: [],
        Axes: [],
        Areas: [],
        ValueTypes: [],
        Title: '',
        XAxisLabel: '',
        XAxisOpposite: false,
        RenderAs: '',
        Summarize: '',
        ChartType: 4,
        BarWidth: null,
        StartAngle: 0,
        EndAngle: 360,
        PlotClockwise: true,
      },
      ...val,
    } as IBarChartData;
  }

  public static createArea(val: Partial<IArea>) {
    return {
      ...{
        AreaId: 0,
        Size: null,
        Invert: false,
      },
      ...val,
    } as IArea;
  }

  public static addArea(
    val: IBarChartData,
    size?: number,
    id?: number,
    invert?: boolean
  ) {
    if (!id) {
      if (!val.Areas || val.Areas.length === 0) {
        id = 0;
      } else {
        id =
          // eslint-disable-next-line prefer-spread
          Math.max.apply(
            Math,
            val.Areas.map((a) => a.AreaId)
          ) + 1;
      }
    }

    val.Areas.push(
      BarChartService.createArea({ Size: size, AreaId: id, Invert: invert })
    );

    return id;
  }

  public static createAxis(val: Partial<IAxis>) {
    return {
      ...{
        AxisId: 0,
        Name: 'Axis',
        Min: 0,
        Max: 100,
        AreaId: 0,
        Opposite: false,
        Hide: false,
        Units: '',
        TickInterval: null,
        MinorTickInterval: null,
        GridLine: false,
      },
      ...val,
    } as IAxis;
  }

  public static addAxis(
    val: IBarChartData,
    id?: number,
    axisSettings?: Partial<IAxis>
  ) {
    if (!id) {
      if (!val.Axes || val.Axes.length === 0) {
        id = 0;
      } else {
        id =
          // eslint-disable-next-line prefer-spread
          Math.max.apply(
            Math,
            val.Axes.map((a) => a.AxisId)
          ) + 1;
      }
    }

    axisSettings = { ...axisSettings, AxisId: id };

    val.Axes.push(BarChartService.createAxis(axisSettings));
    return id;
  }

  public static getNextValueTypeId(val: IBarChartData, id: number = null) {
    if (!id) {
      if (!val.ValueTypes || val.ValueTypes.length === 0) {
        id = 0;
      } else {
        id =
          // eslint-disable-next-line prefer-spread
          Math.max.apply(
            Math,
            val.ValueTypes.map((a) => a.ValueTypeId)
          ) + 1;
      }
    }
    return id;
  }

  public static createValueType(
    valType: VALUE_TYPES,
    value?: VALUE_TYPE_DEFAULTS
  ) {
    let defaultVal: VALUE_TYPE_DEFAULTS;
    if (valType === 'range') {
      defaultVal = {
        ValueTypeId: 0,
        AxisId: 0,
        Name: '',
        Type: 'Range',
        Inverted: false,
        FillColor: 'white',
        BorderColor: 'black',
        BorderThickness: 1,
      } as IRangeValueType;
    } else if (valType === 'symbol') {
      defaultVal = {
        ValueTypeId: 0,
        AxisId: 0,
        Name: '',
        Type: 'Symbol',
        Inverted: false,
        Symbol: 'line',
        Offset: 0,
        Color: 'red',
      };
    } else if (valType === 'line') {
      defaultVal = {
        ValueTypeId: 0,
        AxisId: 0,
        Name: '',
        Type: 'Line',
        Inverted: false,
        Color: 'red',
        LineColor: 'black',
        Thickness: 1,
      };
    } else if (valType === 'boxandwhiskers') {
      defaultVal = {
        ValueTypeId: 0,
        AxisId: 0,
        Name: '',
        Type: 'BoxAndWhiskers',
        Inverted: false,
        FillColor: 'white',
        BorderColor: 'black',
        BorderThickness: 1,
        TooltipHideMin: false,
      };
    } else if (valType === 'area') {
      defaultVal = {
        ValueTypeId: 0,
        AxisId: 0,
        Name: '',
        Type: 'Area',
        Inverted: false,
        FillColor: 'white',
        BorderColor: 'black',
        BorderThickness: 1,
      };
    }

    return { ...defaultVal, ...value } as IValueType;
  }

  public static createBandAxisBarMeasuredValue(
    val: Partial<IPDTrendBandAxisBarMeasuredValue>
  ) {
    return {
      ...{
        MeasurementID: null,
        Value1: null,
        Value2: null,
        Value3: null,
        Value4: null,
        Value5: null,
        Points: null,
        Markers: [],
      },
      ...val,
    } as IPDTrendBandAxisBarMeasuredValue;
  }

  public static addNewValueTypeToAllBars(val: IBarChartData, vt: IValueType) {
    for (const b of val.Bars) {
      b.Data.push(
        BarChartService.createBandAxisBarMeasuredValue({
          MeasurementID: vt.ValueTypeId,
        })
      );
    }
  }

  public static addValueType(
    val: IBarChartData,
    valueType: VALUE_TYPES,
    initialValues?: VALUE_TYPE_DEFAULTS
  ) {
    const id: number = BarChartService.getNextValueTypeId(
      val,
      initialValues ? initialValues.ValueTypeId : null
    );
    const newValueType = BarChartService.createValueType(
      valueType,
      initialValues
    );
    val.ValueTypes.push(newValueType);
    BarChartService.addNewValueTypeToAllBars(val, newValueType);
    return id;
  }

  public static createBar(val: Partial<IBar>) {
    return {
      ...{
        BarId: 0,
        DisplayOrder: 0,
        Title: '',
        Data: [],
      },
      ...val,
    } as IBar;
  }

  public static addBar(val: IBarChartData, title: string, id: number = null) {
    if (!id) {
      if (!val.Bars || val.Bars.length === 0) {
        id = 0;
      } else {
        id =
          // eslint-disable-next-line prefer-spread
          Math.max.apply(
            Math,
            val.Bars.map((a) => a.BarId)
          ) + 1;
      }
    }

    const bar = BarChartService.createBar({ BarId: id, Title: title });

    if (!val.Bars || val.Bars.length === 0) {
      bar.DisplayOrder = 0;
    } else {
      bar.DisplayOrder =
        // eslint-disable-next-line prefer-spread
        Math.max.apply(
          Math,
          val.Bars.map((a) => a.DisplayOrder)
        ) + 1;
    }

    for (const vt of val.ValueTypes) {
      bar.Data.push(
        BarChartService.createBandAxisBarMeasuredValue({
          MeasurementID: vt.ValueTypeId,
        })
      );
    }

    val.Bars.push(bar);

    return id;
  }

  public static getBarData(
    bar: IBar,
    valueTypeID: number
  ): IPDTrendBandAxisBarMeasuredValue {
    const result: IPDTrendBandAxisBarMeasuredValue =
      bar.Data.find((d) => d.MeasurementID === valueTypeID) ||
      BarChartService.createBandAxisBarMeasuredValue({
        MeasurementID: valueTypeID,
      });
    return result;
  }

  public static getBar(val: IBarChartData, barId: string | number) {
    const result: IBar = val.Bars.find(
      (b) => String(b.BarId) === String(barId)
    );
    return result;
  }

  public static setValue(
    val: IBarChartData,
    barId: number,
    valueId: number,
    val1: number,
    val2: number,
    val3: number,
    val4: number,
    val5: number,
    overrides: IPDTrendBandAxisMeasurementMarkerConfig[],
    points: number[]
  ) {
    const bar = BarChartService.getBar(val, barId);
    const data = BarChartService.getBarData(bar, valueId);
    if (data) {
      data.Value1 = val1;
      data.Value2 = val2;
      data.Value3 = val3;
      data.Value4 = val4;
      data.Value5 = val5;
      data.Markers = overrides;
      data.Points = points;
    }
  }

  private static createHighchartsConfiguration(
    data: IBarChartData,
    exportSettings: Highcharts.ExportingOptions,
    areaId: number,
    height: number,
    baseSiteUrl: string,
    theme: string,
    animate: boolean = false,
    hideTitle: boolean = false
  ) {
    const columns = data.Bars.map((b) => b.Title);
    let series = [];
    const valueTypes = BarChartService.getValueTypesForArea(data, areaId);
    for (const m of valueTypes) {
      if (m.Type.toLowerCase() === 'range') {
        const typedM = m as IRangeValueType;
        const newSeries: Highcharts.SeriesColumnrangeOptions = {
          type: 'columnrange',
          name: typedM.Name,
        };

        if (m.AxisId !== null && m.AxisId !== undefined) {
          newSeries.yAxis = 'axis' + String(m.AxisId);
        }

        if (typedM.FillColor) {
          newSeries.color = typedM.FillColor;
        }
        const newSeriesData: [number, number][] = [];
        for (const b of data.Bars) {
          const v = BarChartService.getValue(data, b.BarId, typedM.ValueTypeId);
          if (v) {
            newSeriesData.push([v.Value2 || 0, v.Value1]);
          } else {
            newSeriesData.push([] as any);
          }
        }
        newSeries.tooltip = BarChartService.getTooltipRange(typedM);
        newSeries.data = newSeriesData;
        newSeries.animation = false;
        series.push(newSeries);
      } else if (m.Type.toLowerCase() === 'symbol') {
        const newSeries: Highcharts.SeriesScatterOptions = {
          type: 'scatter',
          name: m.Name,
          tooltip: BarChartService.getTooltipSymbol(),
        };
        if (m.AxisId !== null && m.AxisId !== undefined) {
          newSeries.yAxis = 'axis' + String(m.AxisId);
        }
        const symbol = (m as ISymbolValueType).Symbol;
        const color = (m as ISymbolValueType).Color;
        newSeries.marker = BarChartService.getMarker(
          symbol,
          color,
          baseSiteUrl
        );
        newSeries.color = color;
        const newSeriesData: any[] = [];
        for (const b of data.Bars) {
          const v = BarChartService.getValue(data, b.BarId, m.ValueTypeId);
          if (v) {
            if (v.Markers?.length > 0) {
              const vO = BarChartService.getValueWithMarkerOverrides(
                v.Value1,
                v.Markers
              );
              newSeriesData.push(vO);
            } else {
              newSeriesData.push(v.Value1);
            }
          } else {
            newSeriesData.push(null);
          }
        }
        newSeries.data = newSeriesData;
        newSeries.animation = false;
        series.push(newSeries);
      } else if (m.Type.toLowerCase() === 'line') {
        const typedM = m as ILineValueType;
        const newSeries: Highcharts.SeriesOptionsType = {
          type: 'line',
          name: m.Name,
          tooltip: BarChartService.getTooltipLine(),
          color: typedM.Color,
          marker: {
            fillColor: typedM.Color,
          },
        };
        if (m.AxisId !== null && m.AxisId !== undefined) {
          newSeries.yAxis = 'axis' + String(m.AxisId);
        }
        const newSeriesData: number[] = [];
        for (const b of data.Bars) {
          const v = BarChartService.getValue(data, b.BarId, m.ValueTypeId);
          if (v) {
            newSeriesData.push(v.Value1);
          } else {
            newSeriesData.push(null);
          }
        }
        newSeries.data = newSeriesData;
        newSeries.animation = false;
        series.push(newSeries);
      } else if (m.Type.toLowerCase() === 'boxandwhiskers') {
        const typedM = m as IBoxAndWhiskersValueType;

        const newSeries: Highcharts.SeriesOptionsType = {
          type: 'boxplot',
          name: m.Name,
          grouping: false,
        };

        const outlierSeries: Highcharts.SeriesOptionsType = {
          type: 'scatter',
          name: m.Name + 'outliers',
          marker: {
            fillColor: 'white',
            lineWidth: 1,
            symbol: 'circle',
          },
        };

        if (m.AxisId !== null && m.AxisId !== undefined) {
          newSeries.yAxis = 'axis' + String(m.AxisId);
          outlierSeries.yAxis = newSeries.yAxis;
        }

        if (typedM.FillColor) {
          newSeries.fillColor = typedM.FillColor;
          newSeries.color = '#000000';
          outlierSeries.marker.lineColor = typedM.FillColor;
          newSeries.lineWidth = 2.0;
          newSeries.medianWidth = 2.0;
          newSeries.stemWidth = 2.0;
          newSeries.whiskerWidth = 2.0;
        }

        const newSeriesData: {
          x: number;
          low: number;
          q1: number;
          median: number;
          q3: number;
          high: number;
          name: string;
        }[] = [];
        const outlierData: [number, number][] = [];
        let idx = 0;
        let hasOutlierData = false;
        for (const b of data.Bars) {
          const v = BarChartService.getValue(data, b.BarId, typedM.ValueTypeId);
          if (v) {
            newSeriesData.push({
              x: idx,
              low: v.Value5,
              q1: v.Value4,
              median: v.Value3,
              q3: v.Value2,
              high: v.Value1,
              name: columns[idx],
            });
          } else {
            (newSeriesData as any).push([]);
          }

          if (v && v.Points && v.Points.length > 0) {
            hasOutlierData = true;
            for (const outlierPoint of v.Points) {
              outlierData.push([idx, outlierPoint]);
            }
          }
          idx++;
        }
        newSeries.tooltip = BarChartService.getTooltipBoxAndWhiskers();
        newSeries.data = newSeriesData;
        newSeries.animation = false;
        series.push(newSeries);

        if (hasOutlierData) {
          outlierSeries.data = outlierData;
          outlierSeries.tooltip =
            BarChartService.getTooltipBoxAndWhiskersOutliers();
          outlierSeries.animation = false;
          series.push(outlierSeries);
        }
      }
      if (m.Type.toLowerCase() === 'area') {
        const typedM = m as IAreaValueType;
        const newSeries: Highcharts.SeriesOptionsType = {
          type: 'arearange',
          name: typedM.Name,
          marker: {
            enabled: false,
          },
          stickyTracking: false, // RMB Apr-2018: Reduces tooltip jitter where an area series is overlapped by a line series
        };

        if (m.AxisId !== null && m.AxisId !== undefined) {
          newSeries.yAxis = 'axis' + String(m.AxisId);
        }

        if (typedM.FillColor) {
          newSeries.color = typedM.FillColor;
        }
        const newSeriesData: [number, number][] = [];
        for (const b of data.Bars) {
          const v = BarChartService.getValue(data, b.BarId, typedM.ValueTypeId);
          if (v) {
            newSeriesData.push([v.Value2 || 0, v.Value1]);
          } else {
            (newSeriesData as any).push([]);
          }
        }
        newSeries.tooltip = BarChartService.getTooltipArea();
        newSeries.data = newSeriesData;
        newSeries.animation = false;
        series.push(newSeries);
      }
    }

    if (!series || series.length === 0) {
      // Create some default value
      series = [];
      series.push({
        type: 'line',
        name: '...',
        tooltip: {
          headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name}<br/>`,
          pointFormat: `<b>{point.y}</b>`,
        },
        color: 'white',
        marker: {
          fillColor: 'white',
        },
        data: data.Bars.map((b) => null),
      });
    }
    const axisColor = theme === 'dark' ? '#E0E0E3' : '#666666';

    const result: Highcharts.Options = getDefaultHighchartsOptions({
      chart: {
        type: 'columnrange',
        height,
        animation: animate,
      },
      xAxis: {
        categories: columns,
        visible: false,
        title: {
          style: {
            color: axisColor,
          },
        },
        labels: {
          style: {
            color: axisColor,
          },
        },
      },
      plotOptions: {
        columnrange: {
          grouping: false,
          shadow: false,
          borderWidth: 0,
        },
        series: {
          states: {
            inactive: {
              opacity: 1,
            },
          },
        },
      },
      legend: {
        enabled: false,
      },
      title: {
        text: null,
      },
      series,
    });
    if (data.Areas.length > 1) {
      if (areaId === data.Areas[0].AreaId) {
        if (!hideTitle) {
          result.title.text = data.Title;
        }

        if (data.XAxisOpposite) {
          (result.xAxis as Highcharts.AxisOptions).title = {
            text: data.XAxisLabel,
          };
          (result.xAxis as Highcharts.AxisOptions).opposite = true;
          (result.xAxis as Highcharts.AxisOptions).visible = true;
        }
      }

      if (areaId === data.Areas[data.Areas.length - 1].AreaId) {
        if (!data.XAxisOpposite) {
          (result.xAxis as Highcharts.AxisOptions).title = {
            text: data.XAxisLabel,
          };
          (result.xAxis as Highcharts.AxisOptions).opposite = false;
          (result.xAxis as Highcharts.AxisOptions).visible = true;
        }
      }
    } else {
      if (!hideTitle) {
        result.title.text = data.Title;
      }
      (result.xAxis as Highcharts.AxisOptions).title = {
        text: null,
      };
      (result.xAxis as Highcharts.AxisOptions).opposite = data.XAxisOpposite;
      (result.xAxis as Highcharts.AxisOptions).visible = true;
    }

    const axes = BarChartService.getAxesForArea(data, areaId);
    let alignTicks = true;
    result.yAxis = axes.map((a, index) => {
      const newAxis: Highcharts.AxisOptions = {
        opposite: a.Opposite,
        // I would use the ?? operator here, but its functionality
        // with zero is different from the behavior of this with zero,
        // and having min and max = 0 causes problems with the radar chart.
        // eslint-disable-next-line no-extra-boolean-cast
        min: !!a.Min ? a.Min : null,
        // eslint-disable-next-line no-extra-boolean-cast
        max: !!a.Max ? a.Max : null,
        gridLineWidth: 0,
        tickLength: 4,
        tickPosition: 'outside',
        tickWidth: 2,
        id: 'axis' + String(a.Axis),
        title: {
          text: a.Name,
          style: {
            color: axisColor,
          },
        },
        labels: {
          style: {
            color: axisColor,
          },
        },

        tickInterval: a.TickInterval === 0 ? undefined : a.TickInterval,
        minorTickInterval:
          a.MinorTickInterval === 0 ? undefined : a.MinorTickInterval,
      };

      if (a.GridLine) {
        newAxis.gridLineWidth = 1;
      }

      if (!!a.Max || !!a.Min) {
        newAxis.endOnTick = false;
        newAxis.maxPadding = 0;
        alignTicks = false;
      } else {
        newAxis.endOnTick = true;
        newAxis.maxPadding = null;
      }
      return newAxis;
    });

    result.chart.alignTicks = alignTicks;

    if (data.ChartType === 23) {
      result.chart.polar = true;
      result.pane = result.pane || {};
      result.pane.startAngle = data.StartAngle;
      result.pane.endAngle = data.EndAngle;

      // This is a fix to bug 14345.  For some reason if the start angle is exactly 15 one of the series is dropped.
      // This has to be a Highcharts bug, this fix may not be necessary after highcharts is updated next.
      if (result.pane.startAngle === 15) {
        result.pane.startAngle += 0.1;
        result.pane.endAngle += 0.1;
      }

      // Clockwise plotting is the default. If they want to plot counterclockwise,
      // that requires reversing the x-axis (or axes if there's more than one).
      if (data.PlotClockwise === false) {
        if (Array.isArray(result.xAxis)) {
          for (const a of result.xAxis as Highcharts.AxisOptions[]) {
            a.reversed = true;
          }
        } else {
          result.xAxis.reversed = true;
        }
      }
      for (const s of result.series) {
        if (s.type === 'column') {
          s.pointPlacement = 'between';
        }
      }
      // distribute axes ticks and values around circle; Highcharts does not put y axis titles with axis - they are fixed in the top middle
      const yAxesCount = (result.yAxis as Highcharts.YAxisOptions[]).length;
      (result.yAxis[0] as any).angle = -result.pane.startAngle;
      if (yAxesCount > 1) {
        if (yAxesCount === 2) {
          // angle not in Highcharts.AxisOptions type library yet
          (result.yAxis[1] as any).angle = 90 - result.pane.startAngle;
        } else {
          const deg = 360 / yAxesCount;
          for (let y = 1; y < yAxesCount; y++) {
            (result.yAxis[y] as any).angle =
              Math.round(y * deg) - result.pane.startAngle;
          }
        }
      }
    }
    // This is for the context menu at the top-right corner of the charts.
    // This menu has buttons for printing or downloading the chart graphic.
    result.exporting = exportSettings;

    return result;
  }

  public static createEmptyBarChartData() {
    return BarChartService.createBarChartData({});
  }

  public static translatePDTrendToBarChartData(
    processedTrend: Readonly<IProcessedTrend>
  ): IBarChartData {
    // trendDefinition is the configuration for the trend stored in the database.  It can be combined with
    // the resolved bars to yeild a full trend.
    const trendDefinition: IPDTrend = {
      ...processedTrend?.trendDefinition,
    };

    if (!processedTrend || !trendDefinition) {
      return this.createEmptyBarChartData();
    }

    trendDefinition.BandAxisBarsResolved =
      processedTrend?.measurements?.Bands ??
      trendDefinition?.BandAxisBarsResolved ??
      [];

    if (processedTrend.groupSeriesMeasurements?.length > 0) {
      trendDefinition.BandAxisBarsResolved = produce(
        trendDefinition.BandAxisBarsResolved,
        (state) => {
          state.forEach((bars) => {
            bars.Measurements.forEach((barMeasurement) => {
              const idx = getBandAxisMeasurementIndex(
                processedTrend.groupSeriesMeasurements,
                barMeasurement
              );
              if (idx !== -1) {
                barMeasurement.BorderThickness =
                  processedTrend.groupSeriesMeasurements[idx].BorderThickness;
                barMeasurement.Color =
                  processedTrend.groupSeriesMeasurements[idx].Color;
                barMeasurement.DisplayOrder =
                  processedTrend.groupSeriesMeasurements[idx].DisplayOrder;
                barMeasurement.Exclude =
                  processedTrend.groupSeriesMeasurements[idx].Exclude;
                barMeasurement.FillColor =
                  processedTrend.groupSeriesMeasurements[idx].FillColor;
                barMeasurement.Name =
                  processedTrend.groupSeriesMeasurements[idx].Name;
                barMeasurement.Axis =
                  processedTrend.groupSeriesMeasurements[idx].Axis;
              }
            });
            bars.Measurements.sort((a, b) => {
              return b.DisplayOrder - a.DisplayOrder;
            });
          });
        }
      );
    }

    // This is a data type that our bar chart renderer can use.  It puts all of the data necessary to render
    // a bar chart in one easy to use place.
    const data = BarChartService.createBarChartData({
      ChartType: +trendDefinition.ChartTypeID,
      Summarize: trendDefinition.BandAxis.Summarize,
      Title: trendDefinition.Title,
      XAxisLabel: trendDefinition.BandAxis.Label,
      XAxisOpposite: trendDefinition.BandAxis.Opposite,
      PlotClockwise: trendDefinition.BandAxis.Clockwise,
      StartAngle: trendDefinition.BandAxis.StartAngle,
      EndAngle: trendDefinition.BandAxis.EndAngle,
    });

    if (trendDefinition.Areas && trendDefinition.Areas.length > 0) {
      for (const area of trendDefinition.Areas) {
        BarChartService.addArea(
          data,
          area.Size,
          area.DisplayOrder,
          area.Invert
        );
      }
    } else {
      BarChartService.addArea(data, null, 0);
    }

    if (trendDefinition.Axes && trendDefinition.Axes.length > 0) {
      for (const axis of trendDefinition.Axes) {
        BarChartService.addAxis(data, axis.PDTrendAxisID, {
          Name: axis.Title,
          Min: +axis.Min,
          Max: +axis.Max,
          AreaId: BarChartService.getPDTrendArea(
            trendDefinition.Areas,
            axis.Axis
          ),
          Opposite: axis.Position !== 0,
          Units: '',
          TickInterval: +axis.Step,
          MinorTickInterval: +axis.MinorStep,
          GridLine: axis.GridLine,
          Axis: axis.Axis,
        });
      }
    } else {
      BarChartService.addAxis(data, undefined, {
        Name: '',
        Min: null,
        Max: null,
        AreaId: BarChartService.getPDTrendArea(trendDefinition.Areas, 1),
        Opposite: false,
        Units: '',
        TickInterval: 1,
        MinorTickInterval: 1,
        GridLine: false,
      });
    }

    // Grab the default width of the bars.
    data.BarWidth = trendDefinition.BandAxis.BarWidth ?? 25;

    let keys: string[] = [];
    const keysFirst: string[] = [];
    const keysLast: string[] = [];
    // This looks through all of the measurements and puts them in a dictionary and sorts them.
    // That way we can retrieve them easily.
    const measurements: {
      [key: string]: IPDTrendBandAxisMeasurement;
    } = {};
    if (
      trendDefinition.BandAxisBarsResolved &&
      trendDefinition.BandAxisBarsResolved.length > 0
    ) {
      for (const bar of trendDefinition.BandAxisBarsResolved) {
        for (const m of bar.Measurements) {
          if (!measurements[m.Name]) {
            measurements[m.Name] = m;
            if (m.DisplayOrder == null) {
              // this is necessary so blank group series are either drawn first or last
              keys.push(m.Name);
            } else if (m.DisplayOrder === 0) {
              keysFirst.push(m.Name);
            } else {
              keysLast.push(m.Name);
            }
          }
        }
      }
      keys = keysFirst.concat(keys);
      keys = keys.concat(keysLast);
    }

    // Put the measurements on the bar chart.
    for (const key of keys) {
      const vt = measurements[key];

      BarChartService.addValueType(data, vt.Type.toLowerCase() as VALUE_TYPES, {
        Name: vt.Name,
        AxisId: vt.Axis,
        Symbol: vt.Symbol,
        Offset: vt.Offset,
        FillColor: vt.FillColor,
        Color: vt.Color,
        BorderThickness: vt.BorderThickness,
        Inverted: vt.Invert,
        TooltipHideMin: vt.TooltipHideMin,
        ValueTypeId: vt.TrendBandAxisMeasurementID,
      });
    }

    // Render the bars.  The measurements should be on these bars.
    if (
      trendDefinition.BandAxisBarsResolved &&
      trendDefinition.BandAxisBarsResolved.length > 0
    ) {
      const sortedBars = [...trendDefinition.BandAxisBarsResolved].sort(
        (a, b) => {
          if (a.DisplayOrder === b.DisplayOrder) {
            if (a.Label > b.Label) {
              return 1;
            } else if (a.Label < b.Label) {
              return -1;
            }
            return 0;
          } else {
            return a.DisplayOrder - b.DisplayOrder;
          }
        }
      );
      for (const bar of sortedBars) {
        BarChartService.addBar(data, bar.Label, bar.PDTrendBandAxisBarID);

        if (bar.Measurements) {
          for (const m of bar.Measurements) {
            BarChartService.setValue(
              data,
              bar.PDTrendBandAxisBarID,
              m.TrendBandAxisMeasurementID,
              m.Result1,
              m.Result2,
              m.Result3,
              m.Result4,
              m.Result5,
              m.MarkerConfigs,
              m.Points
            );
          }
        }
      }
    } else {
      BarChartService.addBar(data, '-', -1);
    }

    return data;
  }

  public static getValueTypesForArea(val: IBarChartData, areaId?: number) {
    let result: IValueType[] = [];

    if (areaId === null || areaId === undefined) {
      result = val.ValueTypes;
    } else {
      const axisDict: boolean[] = [];
      for (const n of val.Axes) {
        if (n.AreaId === areaId) {
          axisDict[n.Axis] = true;
        }
      }

      for (const vt of val.ValueTypes) {
        if (axisDict[vt.AxisId]) {
          result.push(vt);
        }
      }
    }

    return result;
  }

  public static getBarIds(val: IBarChartData) {
    val.Bars.sort((a, b) => {
      if (a.DisplayOrder === b.DisplayOrder) {
        return a.Title.localeCompare(b.Title);
      } else {
        return a.DisplayOrder - b.DisplayOrder;
      }
    });
    return val.Bars.map((b) => String(b.BarId));
  }

  public static getBars(val: IBarChartData) {
    val.Bars.sort((a, b) => {
      return a.DisplayOrder - b.DisplayOrder;
    });
    return val.Bars;
  }

  public static getValue(val: IBarChartData, barId: number, valueId: number) {
    const bar = BarChartService.getBar(val, barId);
    let data = BarChartService.getBarData(bar, valueId);

    data = cloneDeep(data);
    if (getValueType(val, valueId).Inverted) {
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!data.Value1) {
        data.Value1 = -1 * data.Value1;
      }
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!data.Value2) {
        data.Value2 = -1 * data.Value2;
      }
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!data.Value3) {
        data.Value3 = -1 * data.Value3;
      }
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!data.Value4) {
        data.Value4 = -1 * data.Value4;
      }
      // eslint-disable-next-line no-extra-boolean-cast
      if (!!data.Value5) {
        data.Value5 = -1 * data.Value5;
      }
    }

    return data;
  }

  public static getAxesForArea(val: IBarChartData, areaId: number) {
    const result: IAxis[] = val.Axes.filter((a) => a.AreaId === areaId);
    return result;
  }

  public static getAxis(val: IBarChartData, axisId: number) {
    const result: IAxis = val.Axes.find((a) => a.AxisId === axisId);
    return result;
  }

  public static getAxisMin(data: IBarChartData, axisId: number) {
    let result: number = null;

    const axis = BarChartService.getAxis(data, axisId);
    if (axis) {
      if (axis.Min !== null && axis.Min !== undefined) {
        result = axis.Min;
      } else {
        // Here is the difficult part.
        // We need to look through all of the data that uses this axis and find the stuff with the lowest value;

        const valTypeDict: { [key: number]: boolean } = {};
        for (const valType of data.ValueTypes) {
          if (valType.AxisId === axisId) {
            valTypeDict[valType.ValueTypeId] = true;
          }
        }

        for (const bar of data.Bars) {
          for (const val of bar.Data) {
            if (valTypeDict[val.MeasurementID]) {
              if (result === null) {
                const values: number[] = [];
                if (val.Value1 !== null && val.Value1 !== undefined) {
                  values.push(val.Value1);
                }
                if (val.Value2 !== null && val.Value2 !== undefined) {
                  values.push(val.Value2);
                }
                if (val.Value3 !== null && val.Value3 !== undefined) {
                  values.push(val.Value3);
                }
                if (val.Value4 !== null && val.Value4 !== undefined) {
                  values.push(val.Value4);
                }
                if (val.Value5 !== null && val.Value5 !== undefined) {
                  values.push(val.Value5);
                }
                if (values.length > 0) {
                  result = Math.min(...values);
                }
              } else {
                if (val.Value1 !== null && val.Value1 !== undefined) {
                  result = Math.min(result, val.Value1);
                }
                if (val.Value2 !== null && val.Value2 !== undefined) {
                  result = Math.min(result, val.Value2);
                }
                if (val.Value3 !== null && val.Value3 !== undefined) {
                  result = Math.min(result, val.Value3);
                }
                if (val.Value4 !== null && val.Value4 !== undefined) {
                  result = Math.min(result, val.Value4);
                }
                if (val.Value5 !== null && val.Value5 !== undefined) {
                  result = Math.min(result, val.Value5);
                }
              }
            }
          }
        }
      }
    }

    return result || 0;
  }

  public static getAxisMax(data: IBarChartData, axisId: number) {
    let result: number = null;

    const axis = BarChartService.getAxis(data, axisId);
    if (axis) {
      if (axis.Max !== null && axis.Max !== undefined) {
        result = axis.Max;
      } else {
        // Here is the difficult part.
        // We need to look through all of the data that uses this axis and find the stuff with the lowest value;

        const valTypeDict: { [key: number]: boolean } = {};
        for (const valType of data.ValueTypes) {
          if (valType.AxisId === axisId) {
            valTypeDict[valType.ValueTypeId] = true;
          }
        }

        for (const bar of data.Bars) {
          for (const val of bar.Data) {
            if (valTypeDict[val.MeasurementID]) {
              if (result === null) {
                const values: number[] = [];
                if (val.Value1 !== null && val.Value1 !== undefined) {
                  values.push(val.Value1);
                }
                if (val.Value2 !== null && val.Value2 !== undefined) {
                  values.push(val.Value2);
                }
                if (val.Value3 !== null && val.Value3 !== undefined) {
                  values.push(val.Value3);
                }
                if (val.Value4 !== null && val.Value4 !== undefined) {
                  values.push(val.Value4);
                }
                if (val.Value5 !== null && val.Value5 !== undefined) {
                  values.push(val.Value5);
                }
                if (values.length > 0) {
                  result = Math.max(...values);
                }
              } else {
                if (val.Value1 !== null && val.Value1 !== undefined) {
                  result = Math.max(result, val.Value1);
                }
                if (val.Value2 !== null && val.Value2 !== undefined) {
                  result = Math.max(result, val.Value2);
                }
                if (val.Value3 !== null && val.Value3 !== undefined) {
                  result = Math.max(result, val.Value3);
                }
                if (val.Value4 !== null && val.Value4 !== undefined) {
                  result = Math.max(result, val.Value4);
                }
                if (val.Value5 !== null && val.Value5 !== undefined) {
                  result = Math.max(result, val.Value5);
                }
              }
            }
          }
        }
      }
    }

    return result || 100;
  }

  private static getPDTrendArea(areas: IPDTrendArea[], axis: number) {
    let result = 0;
    if (areas && areas.length > 0) {
      for (const area of areas) {
        if (area.Axes.indexOf(axis) >= 0) {
          result = area.DisplayOrder;
          break;
        }
      }
    }
    return result;
  }

  public static getBarChartArea(val: IBarChartData, areaId: number) {
    const result: IArea = val.Areas.find((a) => a.AreaId === areaId);
    return result;
  }

  public static getAreaForAxis(val: IBarChartData, axisId: number) {
    let area: IArea = null;
    const axis = BarChartService.getAxis(val, axisId);
    if (axis) {
      area = BarChartService.getBarChartArea(val, axis.AreaId);
    }
    return area || BarChartService.createArea({ Size: null, AreaId: 0 });
  }

  public static getAreaSize(
    val: IBarChartData,
    areaId: number,
    totalPixels: number
  ) {
    let result: number = totalPixels;
    const area = BarChartService.getBarChartArea(val, areaId);
    if (area) {
      if (area.Size !== null && area.Size !== undefined) {
        result = Math.min(area.Size, totalPixels);
      } else {
        let numStars = 0;
        let fixedPixels = 0;
        for (const a of val.Areas) {
          if (a.Size === null || a.Size === undefined) {
            numStars++;
          } else {
            // What wacko would set an area size to negative numbers!
            fixedPixels += Math.abs(a.Size);
          }
        }
        result = (totalPixels - fixedPixels) / numStars;
      }
    }
    return result;
  }

  public static getOffsetForAxis(
    val: IBarChartData,
    axisId: number,
    totalPixels: number
  ) {
    const numStars = val.Areas.filter((a) => isNil(a.Size)).length;

    const fixedPixels = val.Areas.map((a) => a.Size ?? 0).reduce(
      (previousValue, currentValue) => {
        return previousValue + currentValue;
      },
      0
    );
    const starSize = (totalPixels - fixedPixels) / numStars;

    const parts: IArea[] = [];
    for (const a of val.Areas) {
      parts.push(a);
    }
    parts.sort((a, b) => a.AreaId - b.AreaId);

    const myAreaId = BarChartService.getAxis(val, axisId).AreaId;
    let result = 0;
    for (const part of parts) {
      if (part.AreaId === myAreaId) {
        break;
      } else {
        if (part.Size !== null && part.Size !== undefined) {
          result += part.Size;
        } else {
          result += starSize;
        }
      }
    }
    return result;
  }

  public static hasLeftAxis(val: IBarChartData) {
    if (
      val.Axes.find((a) => {
        if (!a.Hide && !a.Opposite) {
          return true;
        }
        return false;
      })
    ) {
      return true;
    }
    return false;
  }

  public static hasRightAxis(val: IBarChartData) {
    if (
      val.Axes.find((a) => {
        if (!a.Hide && a.Opposite) {
          return true;
        }
        return false;
      })
    ) {
      return true;
    }
    return false;
  }

  public static getSvgPath(
    symbol: 'line' | 'circle' | 'arrow-down' | 'arrow-up',
    width: number
  ) {
    let result = '';
    if (symbol === 'line') {
      result = 'M' + (-1 * width) / 2 + ' 0 l' + String(width) + ' 0';
    } else if (symbol === 'circle') {
      result =
        'M' +
        (-1 * width) / 4 +
        ' 0 a' +
        width / 4 +
        ' ' +
        width / 4 +
        ' 0 1 0 ' +
        width / 2 +
        ' 0 a' +
        width / 4 +
        ' ' +
        width / 4 +
        ' 0 1 0 ' +
        -1 * (width / 2) +
        ' 0 Z';
    } else if (symbol === 'arrow-down') {
      result =
        'M' +
        (-1 * width) / 2 +
        ' 0 m 0 ' +
        -1 * width +
        ' h ' +
        width +
        ' l ' +
        (-1 * width) / 2 +
        ' ' +
        width +
        ' Z ';
      result += 'M' + (-1 * width) / 2 + ' 0 l' + String(width) + ' 0';
    } else if (symbol === 'arrow-up') {
      result =
        'M' +
        (-1 * width) / 2 +
        ' 0 m 0 ' +
        width +
        ' h ' +
        width +
        ' l ' +
        (-1 * width) / 2 +
        ' ' +
        -1 * width +
        ' Z ';
      result += 'M' + (-1 * width) / 2 + ' 0 l' + String(width) + ' 0';
    }
    return result;
  }

  public static getMarker(symbol: string, color: string, baseSiteUrl: string) {
    const result: any = {};

    if (symbol === 'pump') {
      result.symbol = `url(${baseSiteUrl}/MD/assets/chart-lib/icon-pump.png)`;
    } else if (symbol === 'level') {
      result.symbol = `url(${baseSiteUrl}/MD/assets/chart-lib/icon-water-level.png)`;
    } else if (symbol === 'line') {
      result.symbol = `url(${baseSiteUrl}/MD/assets/chart-lib/line-symbol.gif)`;
    } else if (
      symbol === 'circle' ||
      symbol === 'square' ||
      symbol === 'diamond' ||
      symbol === 'triangle' ||
      symbol === 'triangle-down'
    ) {
      result.symbol = symbol;
    }

    if (color) {
      result.fillColor = color;
    }
    return result;
  }

  public static getValueWithMarkerOverrides(
    value: number,
    overrides: IPDTrendBandAxisMeasurementMarkerConfig[]
  ) {
    const v = {
      y: value,
      marker: null,
    };
    const m: any = {};

    for (const override of overrides) {
      if (
        override.Min !== null &&
        override.Max !== null &&
        override.Min !== undefined &&
        override.Max !== undefined
      ) {
        if (
          ((override.MinInclusive && value >= override.Min) ||
            (!override.MinInclusive && value > override.Min)) &&
          ((override.MaxInclusive && value <= override.Max) ||
            (!override.MaxInclusive && value < override.Max))
        ) {
          m.Color = override.Color;
          m.FillColor = override.FillColor;
          m.BorderThickness = override.BorderThickness;
          m.Symbol = override.Symbol;
        }
      } else if (override.Min !== null && override.Min !== undefined) {
        if (
          (override.MinInclusive && value >= override.Min) ||
          (!override.MinInclusive && value > override.Min)
        ) {
          m.Color = override.Color;
          m.FillColor = override.FillColor;
          m.BorderThickness = override.BorderThickness;
          m.Symbol = override.Symbol;
        }
      } else if (override.Max !== null && override.Max !== undefined) {
        if (
          (override.MaxInclusive && value <= override.Max) ||
          (!override.MaxInclusive && value < override.Max)
        ) {
          m.Color = override.Color;
          m.FillColor = override.FillColor;
          m.BorderThickness = override.BorderThickness;
          m.Symbol = override.Symbol;
        }
      }
    }

    v.marker = m;

    return v;
  }

  public static getTooltipRange(config: IRangeValueType) {
    const d = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
    };

    if (config.TooltipHideMin) {
      result.pointFormatter = function () {
        return '<b>' + getNumberBySigFigs((this as any).high, 3) + '</b>';
      };
    } else {
      result.pointFormatter = function () {
        return (
          'High:<b>' +
          getNumberBySigFigs((this as any).high, 3) +
          '</b><br>Low:<b>' +
          getNumberBySigFigs((this as any).low, 3) +
          '</b>'
        );
      };
    }
    return result;
  }

  public static getTooltipBoxAndWhiskersOutliers() {
    const d: number = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
      pointFormatter() {
        return 'Value:<b>' + getNumberBySigFigs(this.y, d) + '</b>';
      },
    };

    return result;
  }

  private static getTooltipBoxAndWhiskers() {
    const d: number = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
      pointFormatter() {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const p: any = this;
        return `High:<b> ${getNumberBySigFigs(p.high, d)}</b><br />
        Upper Box:<b>${getNumberBySigFigs(p.q3, d)}</b><br />
        Median:<b>${getNumberBySigFigs(p.median, d)}</b><br />
        Lower Box:<b>${getNumberBySigFigs(p.q1, d)} </b><br />
        Low:<b>${getNumberBySigFigs(p.low, d)}</b>`;
      },
    };

    return result;
  }

  private static getTooltipArea() {
    const d = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
      pointFormatter() {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const p: any = this;
        return `High:<b>${getNumberBySigFigs(
          p.high,
          3
        )}</b><br>Low:<b>${getNumberBySigFigs(p.low, 3)}</b>`;
      },
    };

    return result;
  }

  private static getTooltipSymbol() {
    const d = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
      pointFormatter() {
        return `<b>${getNumberBySigFigs(this.y, d)}</b>`;
      },
    };

    return result;
  }

  private static getTooltipLine() {
    const d = BarChartService.digits;

    const result: Highcharts.TooltipOptions = {
      headerFormat: `<span style="color:{point.color}">\u25CF</span> {series.name} - {point.key}<br/>`,
      pointFormatter() {
        return `<b>${getNumberBySigFigs(this.y, d)}</b>`;
      },
    };

    return result;
  }

  private static createTableConfiguration(data: IBarChartData) {
    let result = `<div style='height:100%;overflow:auto;'><table class='table'>`;

    const valueTypes = BarChartService.getValueTypesForArea(data);

    // Row 1: Label
    result += '<thead><tr><th></th>';
    for (const b of valueTypes) {
      result += '<th>' + b.Name + '</th>';
    }
    result += '</tr></thead>';

    // Row 2: Units
    result += '<tbody><tr><td></td>';
    for (const b of valueTypes) {
      const ax = BarChartService.getAxis(data, b.AxisId);
      if (ax) {
        result += '<td>' + ax.Name + '</td>';
      } else {
        result += '<td></td>';
      }
    }
    result += '</tr>';

    // Start the bars
    const bars = BarChartService.getBars(data);
    for (const b of bars) {
      result += '<tr>';

      result += '<td>' + b.Title + '</td>';

      for (const v of valueTypes) {
        const val = BarChartService.getValue(data, b.BarId, v.ValueTypeId);
        if (!val || val.Value1 === null || val.Value1 === undefined) {
          result += '<td> - </td>';
        } else {
          result += `<td>${getNumberBySigFigs(
            val.Value1,
            BarChartService.digits
          )}</td>`;
        }
      }

      result += '</tr>';
    }

    result += '</tbody>';

    if (data.Summarize === 'average') {
      result += '<tfoot><tr><td>Summary</td>';

      for (const v of valueTypes) {
        let avg = 0;
        if (bars && bars.length > 0) {
          for (const b of bars) {
            avg += BarChartService.getValue(
              data,
              b.BarId,
              v.ValueTypeId
            ).Value1;
          }
          avg = avg / bars.length;
        }
        result += `<td>${getNumberBySigFigs(avg, BarChartService.digits)}</td>`;
      }

      result += '</tr></tfoot>';
    }

    result += '</table></div>';

    return result;
  }

  public static getChartType(data: IBarChartData) {
    return data.ChartType === 22 ? 'table' : 'chart';
  }

  public static getTableData(data: IBarChartData): {
    data: any[];
    columns: any[];
    footer: any[];
  } {
    const columns: string[] = [];
    const values: any[][] = [];
    const valueTypes = BarChartService.getValueTypesForArea(data);

    // Column Headers
    columns.push('');
    for (const b of valueTypes) {
      columns.push(b.Name);
    }

    // Units, if any
    const unitsRow: any[] = [];
    unitsRow.push(null);
    for (const b of valueTypes) {
      const ax = BarChartService.getAxis(data, b.AxisId);
      if (ax) {
        unitsRow.push(ax.Name);
      } else {
        unitsRow.push(null);
      }
    }
    if (unitsRow.filter((u) => u != null).length > 0) {
      values.push(unitsRow);
    }

    // Start the bars data
    const bars = BarChartService.getBars(data);
    for (const b of bars) {
      const row: any[] = [];

      row.push(b.Title);

      for (const v of valueTypes) {
        const val = BarChartService.getValue(data, b.BarId, v.ValueTypeId);
        if (!val || val.Value1 === null || val.Value1 === undefined) {
          row.push('-');
        } else {
          row.push(getNumberBySigFigs(val.Value1, BarChartService.digits));
        }
      }
      values.push(row);
    }

    // Summary, if any
    let footer: any[] = null;
    if (data.Summarize === 'average') {
      footer = [];
      for (const v of valueTypes) {
        let avg = 0;
        if (bars && bars.length > 0) {
          for (const b of bars) {
            avg += BarChartService.getValue(
              data,
              b.BarId,
              v.ValueTypeId
            ).Value1;
          }
          avg = avg / bars.length;
        }
        footer.push(getNumberBySigFigs(avg, BarChartService.digits));
      }
    }
    return { data: values, columns, footer };
  }

  public static getChartData(
    data: IBarChartData,
    exportSettings: Highcharts.ExportingOptions,
    baseSiteUrl: string,
    theme: string
  ): Highcharts.Options[] {
    const result: Highcharts.Options[] = [];
    for (const area of data.Areas) {
      const opts = BarChartService.createHighchartsConfiguration(
        data,
        exportSettings,
        area.AreaId,
        area.Size ?? null,
        baseSiteUrl,
        theme
        // BarChartService.getAreaSize(data, data.Areas[i].AreaId, ),
        // animate,
        // hideTitle
      );
      result.push(opts);
    }

    const yAxes: Highcharts.YAxisOptions[] = [];
    const seriesArray: any[] = [];
    const valueTypes = BarChartService.getValueTypesForArea(data);

    // Compute options
    const bars = BarChartService.getBars(data);
    let colCnt = 0;

    for (const v of valueTypes) {
      const ax = BarChartService.getAxis(data, v.AxisId);
      const columnId = 'col' + colCnt;
      yAxes.push({
        id: columnId,
        title: { text: v.Name + '(' + (ax ? ax.Name : '') + ')' },
      });

      const seriesData: any[] = [];
      for (const b of bars) {
        const x = b.Title;
        const y = BarChartService.getValue(data, b.BarId, v.ValueTypeId);
        seriesData.push([x, y]);
      }

      const series = {
        id: columnId,
        // color: set color here,
        data: seriesData,
        yAxis: columnId,
        name: v.Name,
        fillOpacity: 0.1,
      };
      seriesData.push(series);
      seriesArray.push({ yAxis: columnId, data: seriesData });
      colCnt++;
    }

    return result;
  }

  public static destroy(instanceElement: HTMLElement) {
    instanceElement.innerHTML = '';
  }
}
