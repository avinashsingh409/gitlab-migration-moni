import { TestBed } from '@angular/core/testing';
import { getNumberBySigFigs } from './utilities';

describe('Utilities', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('getNumberBySigFigs', () => {
    expect(getNumberBySigFigs(12.345678, 2)).toEqual(12);
    expect(getNumberBySigFigs(0, 2)).toEqual(0);
  });
});
