/* eslint-disable max-len */
import * as Highcharts from 'highcharts';
import moment from 'moment';
import merge from 'lodash/merge';
import isString from 'lodash/isString';
import isDate from 'lodash/isDate';
import cloneDeep from 'lodash/cloneDeep';
import every from 'lodash/every';
import {
  getTrendTotalSeries,
  IAssetMeasurements,
  IAssetMeasurementsSet,
  IPDRecordedPoint,
  IPDTrend,
  IPDTrendSeries,
  IProcessedTrend,
  isNil,
  setTimestampDictionary,
} from '@atonix/atx-core';
import {
  defaultXAxis,
  defaultYAxis,
  getNumberBySigFigs,
  toPrecisionCustom,
} from './utilities';
import {
  CorrelationTrendColorMapping,
  CorrelationTrendNameMapping,
  EForecastTrend,
  EROCTrend,
  IAssetModelChartingData,
  ICorrelationData,
  INDModelActionItemAnalysis,
  INDModelSummary,
  ITimeseriesDatapoint,
} from '@atonix/shared/api';

function getTrendLabel(trend: IPDTrend) {
  const result = trend.Title;
  return result;
}

export function isImage(trend: IProcessedTrend) {
  return (trend.trendDefinition.TrendSource || '').toLowerCase() === 'image';
}

export function isNd(trend: IProcessedTrend) {
  return (trend.trendDefinition.TrendSource || '').toLowerCase() === 'nd';
}

export function isBarChart(trend: IProcessedTrend) {
  return (
    !isNil(trend.trendDefinition.BandAxis) &&
    trend?.trendDefinition?.ChartTypeID !== 22
  );
}

export function isBarTable(trend: IProcessedTrend) {
  return (
    !isNil(trend.trendDefinition.BandAxis) &&
    trend?.trendDefinition?.ChartTypeID === 22
  );
}

export function isBaseSummaryTable(trend: IProcessedTrend) {
  // 22 = table - from the base chart types; not the grouped series
  const result =
    trend.trendDefinition.ChartTypeID === 22 &&
    !isNil(trend.trendDefinition.SummaryType) &&
    !isNil(trend.trendDefinition.SummaryType.SummaryDesc);
  return result;
}

export function isBaseSummaryChart(trend: IProcessedTrend) {
  // column (3) /bar (4) - from the base chart types; not the grouped series
  const result =
    (trend.trendDefinition.ChartTypeID === 3 ||
      trend.trendDefinition.ChartTypeID === 4) &&
    !isNil(trend.trendDefinition.SummaryType) &&
    !isNil(trend.trendDefinition.SummaryType.SummaryDesc);
  return result;
}

export function isTable(trend: IProcessedTrend) {
  let result = false;
  if (
    trend &&
    trend.trendDefinition &&
    trend.trendDefinition.ChartTypeID === 22
  ) {
    result = true;
  }
  return result;
}

export function isTimeSeriesChart(trend: IProcessedTrend) {
  return (
    isNil(trend.trendDefinition.BandAxis) &&
    (trend.trendDefinition.TrendSource || '').toLowerCase() !== 'image' &&
    (trend.trendDefinition.TrendSource || '').toLowerCase() !== 'nd' &&
    (trend.trendDefinition.TrendSource || '').toLowerCase() !== 'barchart' &&
    (trend.trendDefinition.TrendSource || '').toLowerCase() !== 'table' &&
    (trend.trendDefinition.ChartTypeID === null ||
      (trend.trendDefinition.ChartTypeID !== null &&
        trend.trendDefinition.ChartTypeID !== 22))
  );
}

function getPinNumber(id: string) {
  const pin = id.split(':')[3];
  if (pin === '' || isNil(pin)) {
    return null;
  } else {
    return +pin;
  }
}

function getTagNumber(id: string) {
  return id.split(':')[1];
}

function getArchive(id: string) {
  return id.split(':')[5];
}

function getName(id: string) {
  if (id) {
    if (id.lastIndexOf(':') >= 0) {
      // in case name includes :
      id = id.substr(id.lastIndexOf(':') + 1);
    }
    return id.replace(/</g, '_').replace(/>/g, '_');
  }
}

function findUnits(
  yAxis: string | number,
  axes: Highcharts.YAxisOptions | Highcharts.YAxisOptions[]
) {
  if (Array.isArray(axes)) {
    const ax = axes.find((n) => n.id === yAxis);
    return ax ? ax.title.text : '';
  } else {
    return axes.id === yAxis ? axes.title.text : '';
  }
}

function createId(tagNumber, pinNumber, archive) {
  if (isNil(pinNumber)) {
    return `tag:${tagNumber}:pin::archive:${archive}`;
  } else {
    return `tag:${tagNumber}:pin:${pinNumber}:archive:${archive}`;
  }
}

function getDate(val: any) {
  let result: Date = null;
  if (isDate(val)) {
    result = new Date(val.getTime());
  } else if (isString(val)) {
    result = new Date(val);

    if (isInvalidDate(result)) {
      // IE has a more limited set of date parsers than other platforms.  I am going to use Moment
      // to try to parse this date so that it doesn't use an invalid format
      // Unfortunately, moment falls back to using the browser date format to turn strings
      // into dates.  We can supply a format here to match what we use, but we would need
      // a standard format throughout the platform.  And we haven't defined that yet.
      // This format matches the toLocaleString format.
      const m = moment(val, 'M/D/YYYY H:m:s a', false);
      if (m.isValid()) {
        result = m.toDate();
      }
    }
  } else {
    result = new Date(val);
  }
  return result;
}

function isInvalidDate(val: any) {
  return val instanceof Date && isNaN(val as any);
}

function defaultChartTooltipFormatter(chartObject) {
  const d = moment(getDate(chartObject.x));
  let tooltipWrap: string;

  // Thursday, July 9th 2020  18:40:00
  let s = `<span style="font-size: 10px">
    ${d.format('dddd, MMMM Do YYYY h:mm:ss a')}
    </span><br/>`;

  let isHovered: boolean;

  if (chartObject.points) {
    for (const p of chartObject.points) {
      let units: string;
      units = '';
      if (p?.series?.userOptions?.custom?.units) {
        units = p.series.userOptions.custom.units;
      }
      // determine hovered series by checking the opacity value
      if (p.series.type !== 'arearange') {
        if (p.series.state === 'hover') {
          isHovered = true;
        } else {
          isHovered = false;
        }

        if (isHovered) {
          tooltipWrap = `<div class="tooltip" style="border: 1px solid ${p.color}; border-radius:3px; padding: 3px;">`;
          s += `<span style="color:${p.color}">●</span><b>${
            p.series.name
          }: ${getNumberBySigFigs(p.y, 4)}</b> ${units}<br/>`;
        } else {
          s += `<span style="color:${p.color}">●</span>${
            p.series.name
          }: ${getNumberBySigFigs(p.y, 4)}</b> ${units}<br/>`;
        }
      } else {
        s += `<span style="color:${p.color}">●</span>${p.series.name}: <b>
              ${getNumberBySigFigs(p.point.low, 4)}-
              ${getNumberBySigFigs(p.point.high, 4)}</b> ${units}<br/>`;
      }
    }
  } else if (chartObject.point) {
    if (chartObject.point.isAnnotation) {
      s = chartObject.point.text;
    }
  }
  return tooltipWrap + s + '</div>';
}

function varVsVarChartTooltipFormatter(chartObject) {
  let n = '';
  if (chartObject.series && chartObject.series.name) {
    n = chartObject.series.name;
  }
  let s =
    '<span style="color:' + chartObject.color + '">●</span> ' + n + ' <br/>';
  s +=
    '<span style="font-size: 10px">x: <b> ' +
    getNumberBySigFigs(chartObject.x, 4) +
    ' </b></span><br/>';
  s +=
    '<span style="font-size: 10px">y: <b> ' +
    getNumberBySigFigs(chartObject.y, 4) +
    ' </b></span><br/>';

  return s;
}

function correlationTrendTooltipFormatter(
  chartObject,
  correlationTrend: ICorrelationData[]
) {
  const d = moment(getDate(chartObject.x));
  let tooltipWrap: string;

  // Thursday, July 9th 2020  18:40:00
  let s = `<span style="font-size: 10px">
    ${d.format('dddd, MMMM Do YYYY h:mm:ss a')}
    </span><br/>`;

  let isHovered: boolean;

  if (chartObject.points) {
    for (const p of chartObject.points) {
      let units: string;
      units = '';
      if (p?.series?.userOptions?.custom?.units) {
        units = p.series.userOptions.custom.units;
      }

      // determine hovered series by checking the opacity value
      if (p.series.type !== 'arearange') {
        if (p.series.state === 'hover') {
          isHovered = true;
        } else {
          isHovered = false;
        }

        // Determine if regular series or ROC series base on series id
        if (!p?.series?.userOptions?.id?.includes('ROC-')) {
          if (isHovered) {
            tooltipWrap = `<div class="tooltip" style="border: 1px solid ${p.color}; border-radius:3px; padding: 3px;">`;
            s += `<span style="color:${p.color}">●</span><b>${
              p.series.name
            }: ${getNumberBySigFigs(p.y, 4)}</b> ${units}<br/>`;
          } else {
            s += `<span style="color:${p.color}">●</span>${
              p.series.name
            }: ${getNumberBySigFigs(p.y, 4)} ${units}<br/>`;
          }
        } else {
          const LastRegression = correlationTrend.find(
            (ct) => ct.Name === EROCTrend.LastRegression
          );
          if (LastRegression) {
            const d = moment(LastRegression.Info.split(': ')[1]);
            s = `<span style="font-size: 10px">Last Regression:
                ${d.format('dddd, MMMM Do YYYY h:mm:ss a')}
                </span><br/>`;
          }

          tooltipWrap = `<div class="tooltip" style="border: 1px solid ${p.color}; border-radius:3px; padding: 3px;">`;
          correlationTrend.forEach((ct) => {
            switch (ct.Name) {
              case EROCTrend.Regression:
              case EROCTrend.Expected:
              case EROCTrend.Upper:
              case EROCTrend.Lower:
                s += `<span style="color:${
                  CorrelationTrendColorMapping[ct.Name]
                }">● </span>${ct.Info.replace('per ', units + '/')}<br/>`;
            }
          });
        }
      } else {
        s += `<span style="color:${p.color}">●</span>${p.series.name}: <b>
              ${getNumberBySigFigs(p.point.low, 4)}-
              ${getNumberBySigFigs(p.point.high, 4)}</b> ${units}<br/>`;
      }
    }
  } else if (chartObject.point) {
    if (chartObject.point.isAnnotation) {
      s = chartObject.point.text;
    }
  }
  return tooltipWrap + s + '</div>';
}

function getBaseSummaryInfo(myTrend: IProcessedTrend, theme: string) {
  const highchartsOptions = createHighchartsDefinition(
    myTrend.trendDefinition,
    theme,
    myTrend.measurements,
    myTrend.startDate,
    myTrend.endDate,
    myTrend.labelIndex
  );

  // this holds the summary types, keyed like the other dictionaries below
  const summaryTypesDict = {};

  // This will hold the actual values to put in the table.
  // It is a dictionary with the id as the key from the series
  const valuesDict: { [key: string]: number } = {};

  let hasNonPin = false;

  // This is an array where each entry is the id of the series
  const seriesSettings: {
    seriesOrder: string;
    seriesAxis: string | number;
  }[] = [];

  // This is a dictionary used to make sure that we don't add a tag to the array twice.
  // It is also used to apply the series label to the table row
  const seriesDict = {};
  for (const s of highchartsOptions.series) {
    if (s.id) {
      if (getPinNumber(s.id) === null) {
        hasNonPin = true;
      }
      const tagId = getTagNumber(s.id);
      const archive = getArchive(s.id);
      const key = createId(tagId, null, archive);
      if (seriesDict[key] === undefined) {
        seriesSettings.push({ seriesOrder: key, seriesAxis: s.yAxis });
        seriesDict[key] = getName(s.name);
      }
      if (s && (s as any).data && (s as any).data[0]) {
        valuesDict[s.id] = getNumberBySigFigs((s as any).data[0].y, 4);
      } else {
        valuesDict[s.id] = null;
      }
      if (s && (s as any).summaryType && (s as any).summaryType.SummaryDesc) {
        summaryTypesDict[key] = (s as any).summaryType.SummaryDesc;
      } else if (
        myTrend.trendDefinition.SummaryType &&
        myTrend.trendDefinition.SummaryType.SummaryDesc
      ) {
        summaryTypesDict[key] = myTrend.trendDefinition.SummaryType.SummaryDesc;
      } else {
        summaryTypesDict[key] = '-';
      }
    }
  }

  return {
    hasNonPin,
    seriesSettings,
    seriesDict,
    summaryTypesDict,
    valuesDict,
    highchartsOptions,
  };
}

export function getBaseSummaryTableDataSource(
  myTrend: IProcessedTrend,
  theme: string
): { columns: string[]; data: any[] } {
  const baseSumInfo = getBaseSummaryInfo(myTrend, theme);

  const measurements = myTrend.measurements;

  const columns = ['Series'];
  columns.push('Aggregation');
  columns.push('Units');
  if (baseSumInfo.hasNonPin) {
    columns.push('Values');
  }

  // Array containing the ids of each pin
  const pinList = [];

  for (const pin of myTrend.trendDefinition.Pins) {
    if (!pin.Hidden) {
      columns.push(pin.Name.replace(/</g, '_').replace(/>/g, '_'));
      pinList.push(pin.PDTrendPinID);
    }
  }

  const measurementValuesByTagPinId: any[] = [];

  if (measurements) {
    measurements.Measurements.map((m) => {
      if (m.Data && m.Data.length > 0) {
        measurementValuesByTagPinId[
          createId(m.Tag.PDTagID, m.PinID, m.Archive)
        ] = m.Data[0].Value;
      }
    });
  }

  // We now have an ordered list of pins, an ordered list of tags and a way to get the series from the pins and tags
  const data = baseSumInfo.seriesSettings.map((setting) => {
    const tagId = getTagNumber(setting.seriesOrder);
    const archive = getArchive(setting.seriesOrder);
    const row = [baseSumInfo.seriesDict[setting.seriesOrder]];
    row.push(baseSumInfo.summaryTypesDict[setting.seriesOrder]);
    row.push(
      findUnits(setting.seriesAxis, baseSumInfo.highchartsOptions.yAxis)
    );
    if (baseSumInfo.hasNonPin) {
      const v = measurementValuesByTagPinId[setting.seriesOrder];
      if (v) {
        row.push(toPrecisionCustom(v, 4));
      } else {
        row.push(
          toPrecisionCustom(baseSumInfo.valuesDict[setting.seriesOrder], 4)
        );
      }
    }
    const pinData = pinList.map((pin) => {
      const id = createId(tagId, pin, archive);
      const v = measurementValuesByTagPinId[id];
      if (v) {
        return v;
      } else {
        return baseSumInfo.valuesDict[id];
      }
    });
    return [...row, ...pinData];
  });

  return { columns, data };
}

export function populateBaseSummaryChart(
  myTrend: IProcessedTrend,
  theme: string
) {
  const baseSumInfo = getBaseSummaryInfo(myTrend, theme);

  const newHighchartsConfiguration = {
    ...baseSumInfo.highchartsOptions,
  } as Highcharts.Options;

  // Array containing the ids of each pin
  const pinList = [];
  const pinNames = [];

  for (const pin of myTrend.trendDefinition.Pins) {
    if (!pin.Hidden) {
      pinNames.push(pin.Name.replace(/</g, '_').replace(/>/g, '_'));
      pinList.push(pin.PDTrendPinID);
    }
  }

  let noPins = false;
  if (!pinList || pinList.length === 0) {
    // No legend
    if (!newHighchartsConfiguration.legend) {
      newHighchartsConfiguration.legend = {};
    }
    newHighchartsConfiguration.legend.enabled = false;
    noPins = true;
  }

  const xAxis = newHighchartsConfiguration.xAxis as Highcharts.XAxisOptions;
  xAxis.type = 'category';
  xAxis.visible = true;
  xAxis.max = baseSumInfo.seriesSettings.length - 1;
  xAxis.min = 0;
  xAxis.categories = [];
  newHighchartsConfiguration.series = [];
  newHighchartsConfiguration.tooltip = {
    shared: true,
    useHTML: true,
    headerFormat: '<small>{point.key}</small><br/>',
    pointFormat:
      '<span style="color: {series.color}">●</span> {series.name}: <b>{point.y}</b><br/>',
    footerFormat: '',
  };
  // rebuild series data and categories for summary rendering
  for (const so of baseSumInfo.seriesSettings) {
    // categories are sets of same tag
    xAxis.categories.push(baseSumInfo.seriesDict[so.seriesOrder]);
  }

  // see below - because each series will possibly have different UOM
  // and we can't associate with multiple axes per series
  const yAxes = newHighchartsConfiguration.yAxis as Highcharts.YAxisOptions[];
  if (yAxes.length > 1) {
    // the product owner requested that only a
    // single Y axis be displayed, but have all UOM for various tags
    let title = yAxes[0].title.text;
    for (let a = 1; a < yAxes.length; a++) {
      title += ' | ' + yAxes[a].title.text;
      yAxes[a].visible = false;
    }
    yAxes[0].title.text = title;
  }
  if (baseSumInfo.hasNonPin) {
    const newSer = {
      name: noPins ? ' ' : 'Active', // if no pins, leave series names blank
      data: [],
    };
    for (let n = 0; n < baseSumInfo.seriesSettings.length; n++) {
      newSer.data.push({
        x: n,
        y: baseSumInfo.valuesDict[baseSumInfo.seriesSettings[n].seriesOrder],
      });
    }

    newHighchartsConfiguration.series.push(newSer as any);
  }
  for (let j = 0; j < pinList.length; j++) {
    const newSer = {
      // each series has pin name
      name: pinNames[j],
      data: [],
    };
    for (let i = 0; i < baseSumInfo.seriesSettings.length; i++) {
      const tagId = getTagNumber(baseSumInfo.seriesSettings[i].seriesOrder);
      const archive = getArchive(baseSumInfo.seriesSettings[i].seriesOrder);
      // and data values in array in category (tag names) order
      newSer.data.push({
        x: i,
        y: baseSumInfo.valuesDict[createId(tagId, pinList[j], archive)],
      });
    }
    newHighchartsConfiguration.series.push(newSer as any);
  }

  // TJC 2/21/18
  // each series is now named by Pin and has a point for each tag - this breaks the Highcharts assumption of the same UOM for a series
  // this is not ideal if the various tag UOM have order of magnitude different scales (0.35 vs 5,000)
  // can't find a Highcharts way around this other than to reverse categories and series (categories are pins and series are tags)
  // maybe each tag's points could be scaled for a relative proportion of the display and the tooltip displays the real value
  // each axis would be displayed and therefore line up with the tooltip/actual value

  newHighchartsConfiguration.chart = newHighchartsConfiguration.chart || {};
  return newHighchartsConfiguration;
}

function getDesignCurveKey(dc) {
  return 'dc:' + dc.PDTrendDesignCurveID;
}

function getRepeating(ticks: number, myData: any[], start: Date, stop: Date) {
  // Get the start of the hour the start time is in.
  let beginningOfTimeStuff = 0;
  if (myData.length > 0) {
    beginningOfTimeStuff =
      myData[0].x.getTime() - (myData[0].x.getTime() % ticks);
  }

  // get the start hour of the target interval
  const beginningOfTargetInterval = start.getTime() - (start.getTime() % ticks);

  // Translate each data point to be relative to the start of the hour.
  // Get rid of all data that is beyond the indicated interval.
  const data = myData
    .map((d) => {
      return { ...d, x: d.x.getTime() - beginningOfTimeStuff };
    })
    .filter((d) => d.x < ticks); // Gets all the data within the indicated interval

  // Create an array to hold the new repeating data.
  let newData = [];

  let intervalIdx = 0;
  while (beginningOfTargetInterval + intervalIdx * ticks < stop.getTime()) {
    newData = [
      ...newData,
      ...data.map((d) => {
        return {
          ...d,
          x: getDate(d.x + beginningOfTargetInterval + intervalIdx * ticks),
        };
      }),
    ];
    intervalIdx++;
  }

  return newData;
}

function getRepeatingMonth(myData, start, stop) {
  const targetIntervalYear = start.getFullYear();
  let targetIntervalMonth = start.getMonth();

  myData = cloneDeep(myData);

  const threshold = new Date(
    myData[0].x.getFullYear(),
    myData[0].x.getMonth() + 1,
    1
  );
  let indexOfFirstDataOverInterval = -1;
  for (let i = 0; i < myData.length; i++) {
    if (myData[i].x.getTime() > threshold.getTime()) {
      indexOfFirstDataOverInterval = i;
      break;
    }
  }
  if (indexOfFirstDataOverInterval >= 0) {
    myData = myData.slice(0, indexOfFirstDataOverInterval);
  }

  const newData = [];
  while (
    new Date(targetIntervalYear, targetIntervalMonth, 1, 0, 0, 0, 0).getTime() <
    stop.getTime()
  ) {
    const data = cloneDeep(myData);
    for (const d of data) {
      d.x.setFullYear(targetIntervalYear);
      d.x.setMonth(targetIntervalMonth);
      newData.push(d);
    }
    targetIntervalMonth++;
  }
  return newData;
}

function getRepeatingYear(myData, start, stop) {
  let targetIntervalYear = start.getFullYear();

  myData = cloneDeep(myData);

  const threshold = new Date(myData[0].x.getFullYear() + 1, 0, 1);
  let indexOfFirstDataOverInterval = -1;
  for (let i = 0; i < myData.length; i++) {
    if (myData[i].x.getTime() > threshold.getTime()) {
      indexOfFirstDataOverInterval = i;
      break;
    }
  }
  if (indexOfFirstDataOverInterval >= 0) {
    myData = myData.slice(0, indexOfFirstDataOverInterval);
  }

  const newData = [];
  while (
    new Date(targetIntervalYear, 0, 1, 0, 0, 0, 0).getTime() < stop.getTime()
  ) {
    const data = cloneDeep(myData);
    for (const d of data) {
      d.x.setFullYear(targetIntervalYear);
      newData.push(d);
    }
    targetIntervalYear++;
  }
  return newData;
}

function getDataFromDesignCurve(dc, isDCaDate, start?, stop?) {
  const values: { X: any; Y: any; Y1: any; Y2: any }[] = JSON.parse(dc.Values);
  let data = values.map((d) => {
    return {
      x: isDCaDate ? new Date(d.X) : Number(d.X),
      y: Number(d.Y),
      low: Number(d.Y1),
      high: Number(d.Y2),
    };
  });

  if (isDCaDate) {
    data.sort((a, b) => {
      return (a.x as Date).getTime() - (b.x as Date).getTime();
    });
  } else {
    data.sort((a, b) => {
      return (a.x as number) - (b.x as number);
    });
  }

  if (isDCaDate && dc.Repeat && start && stop) {
    if (dc.Repeat === 'hour') {
      data = getRepeating(1000 * 60 * 60, data, start, stop);
    } else if (dc.Repeat === 'day') {
      data = getRepeating(1000 * 60 * 60 * 24, data, start, stop);
    } else if (dc.Repeat === 'week') {
      data = getRepeating(1000 * 60 * 60 * 24 * 7, data, start, stop);
    } else if (dc.Repeat === 'month') {
      data = getRepeatingMonth(data, start, stop);
    } else if (dc.Repeat === 'year') {
      data = getRepeatingYear(data, start, stop);
    }
  }
  return data;
}

function transformTimeseriesDatasToCoordinates(data: ITimeseriesDatapoint[]) {
  return data.map((d) => {
    return transformTimeseriesDataToCoordinates({
      Timestamp: d.Timestamp,
      Value: d.Value,
    });
  });
}

function transformTimeseriesDataToCoordinates(data: ITimeseriesDatapoint) {
  return {
    x: moment(data.Timestamp).toDate(),
    y: Number(data.Value),
  };
}

function getMinTimeSeriesValue(correlationTrend: ICorrelationData[]) {
  let values: number[] = [];
  correlationTrend?.forEach((mcTrend) => {
    switch (mcTrend.Name) {
      case EForecastTrend.Forecast:
      case EForecastTrend.GreaterThanThreshold:
      case EForecastTrend.LessThanThreshold:
      case EROCTrend.Regression:
      case EROCTrend.Expected:
      case EROCTrend.Upper:
      case EROCTrend.Lower:
        if (mcTrend.Data?.length > 0) {
          const dataValues: number[] = mcTrend.Data.map((d) => d.Value);
          values = values.concat(dataValues);
        }
        break;
      case EForecastTrend.LastRegression:
      case EForecastTrend.Horizon:
      case EForecastTrend.Exceedance:
        if (mcTrend.Data?.length > 0) {
          values.push(mcTrend.Data[0].Value);
        }
        break;
    }
  });

  return Math.min(...values);
}

function getMaxTimeSeriesValue(correlationTrend: ICorrelationData[]) {
  let values: number[] = [];
  correlationTrend?.forEach((mcTrend) => {
    switch (mcTrend.Name) {
      case EForecastTrend.Forecast:
      case EForecastTrend.GreaterThanThreshold:
      case EForecastTrend.LessThanThreshold:
      case EROCTrend.Regression:
      case EROCTrend.Expected:
      case EROCTrend.Upper:
      case EROCTrend.Lower:
        if (mcTrend.Data?.length > 0) {
          const dataValues: number[] = mcTrend.Data.map((d) => d.Value);
          values = values.concat(dataValues);
        }
        break;
      case EForecastTrend.LastRegression:
      case EForecastTrend.Horizon:
      case EForecastTrend.Exceedance:
        if (mcTrend.Data?.length > 0) {
          values.push(mcTrend.Data[0].Value);
        }
        break;
    }
  });

  return Math.max(...values);
}

function getChartType(chartTypeID: number) {
  let result = null;
  if (!isNil(chartTypeID)) {
    switch (chartTypeID) {
      case 1: // Line
        result = 'line';
        break;
      case 2: // Area
        result = 'area';
        break;
      case 3: // Column
        result = 'column';
        break;
      case 4: // Bar
        result = 'bar';
        break;
      case 22:
        result = 'table';
        break;
    }
  }
  return result;
}

function getSeriesKey(series: IPDTrendSeries, pinID?: number) {
  return getSeriesKeyFromPdTagId(series.PDTagID, pinID, series.Archive);
}

function getSeriesKeyFromPdTagId(
  pdTagId: number,
  pinID?: number,
  archive?: string
) {
  if (!isNil(pinID)) {
    return 'tag:' + pdTagId + ':pin:' + pinID + ':archive:' + (archive || '');
  }
  return 'tag:' + pdTagId + ':pin::archive:' + (archive || '');
}

function seriesCompare(a, b) {
  if (a.legendIndex === b.legendIndex) {
    if (a.name > b.name) {
      return 1;
    } else if (b.name > a.name) {
      return -1;
    }
    return 0;
  } else {
    return a.legendIndex - b.legendIndex;
  }
}

// This is an internal method to figure out if there are series associated with an axis.
function countRelatedSeries(trendDef: IPDTrend, axisID: number) {
  return trendDef.Series.filter((s) => s.Axis === axisID && !s.IsXAxis).length;
}

// Helper functions for creating the x axis in the event of a no-gaps pinning setting.
function findMin(trendDef: IPDTrend) {
  let myMin: number = null;
  for (const pin of trendDef.Pins) {
    if (myMin === null || myMin > getDate(pin.StartTime).getTime()) {
      myMin = getDate(pin.StartTime).getTime();
    }
  }
  return myMin;
}

function findMax(trendDef: IPDTrend) {
  let myMax: number = null;
  for (const pin of trendDef.Pins) {
    if (myMax === null || myMax < getDate(pin.EndTime).getTime()) {
      myMax = getDate(pin.EndTime).getTime();
    }
  }
  return myMax;
}

function getPointsByTagPinArchive(
  measurements: IAssetMeasurementsSet,
  tagID: number,
  archive: string,
  pinID?: number
) {
  let result: IAssetMeasurements = null;
  if (
    measurements &&
    measurements.Measurements &&
    measurements.Measurements.length > 0
  ) {
    for (const item of measurements.Measurements) {
      if (!isNil(pinID) && pinID != null) {
        // && angular.isDefined(amSets[i].PinID) && (amSets[i].PinID !== null)) {
        if (
          tagID === item.Tag.PDTagID &&
          pinID === item.PinID &&
          archive === item.Archive
        ) {
          result = item;
        }
      } else {
        if (
          tagID === item.Tag.PDTagID &&
          archive === item.Archive &&
          item.PinID == null
        ) {
          result = item;
        }
      }
    }
    if (!result) {
      // try again ignoring archive
      for (const item of measurements.Measurements) {
        if (!isNil(pinID) && pinID != null) {
          // && angular.isDefined(amSets[i].PinID) && (amSets[i].PinID !== null)) {
          if (tagID === item.Tag.PDTagID && pinID === item.PinID) {
            result = item;
          }
        } else {
          if (tagID === item.Tag.PDTagID && item.PinID == null) {
            result = item;
          }
        }
      }
    }
  }
  return result;
}

function getBreaks(trendDef: IPDTrend) {
  let myBreaks = [
    {
      from: findMin(trendDef),
      to: findMax(trendDef),
    },
  ];
  for (const pin of trendDef.Pins) {
    const tempBreaks: { from: number; to: number }[] = [];

    for (const myBreak of myBreaks) {
      // If the break is invalid then skip it
      if (myBreak.from >= myBreak.to) {
        // do nothing to skip the break
      } else if (
        myBreak.from >= getDate(pin.StartTime).getTime() &&
        myBreak.to <= getDate(pin.EndTime).getTime()
      ) {
        // do nothing to skip the break
      } else if (
        getDate(pin.StartTime).getTime() > myBreak.from &&
        getDate(pin.EndTime).getTime() < myBreak.to
      ) {
        tempBreaks.push({
          from: myBreak.from,
          to: getDate(pin.StartTime).getTime(),
        });
        tempBreaks.push({
          from: getDate(pin.EndTime).getTime(),
          to: myBreak.to,
        });
      } else if (
        getDate(pin.StartTime).getTime() <= myBreak.from &&
        getDate(pin.EndTime).getTime() >= myBreak.from
      ) {
        tempBreaks.push({
          from: getDate(pin.EndTime).getTime(),
          to: myBreak.to,
        });
      } else if (
        getDate(pin.StartTime).getTime() <= myBreak.to &&
        getDate(pin.EndTime).getTime() >= myBreak.to
      ) {
        tempBreaks.push({
          from: myBreak.from,
          to: getDate(pin.StartTime).getTime(),
        });
      } else {
        tempBreaks.push(myBreak);
      }
    }

    myBreaks = tempBreaks;
  }
  return myBreaks;
}

function getAxis(trend: IPDTrend, axisID: number) {
  return trend.Axes.find((axis) => {
    return axis.Axis === axisID;
  });
}

export function getDefaultHighchartsOptions(
  options?: Partial<Highcharts.Options>
) {
  return merge(
    {
      chart: {
        backgroundColor: null,
        style: {
          fontFamily: 'Roboto, sans-serif',
        },
        marginRight: 40,
      },
      title: {
        style: {
          color: 'transparent',
          fontSize: '16px',
        },
      },
      subtitle: {
        style: {
          color: 'black',
        },
      },
      tooltip: {
        borderWidth: 0,
      },
      legend: {
        itemStyle: {
          fontWeight: 'bold',
          fontSize: '13px',
        },
      },
      xAxis: {
        labels: {
          style: {
            //color: '#6e6e70',
          },
        },
      },
      yAxis: {
        labels: {
          style: {
            //color: '#6e6e70',
          },
        },
      },
      plotOptions: {
        series: {
          shadow: true,
          turboThreshold: 0,
          animation: false,
        },
        candlestick: {
          lineColor: '#404048',
        },
        map: {
          shadow: false,
        },
      },

      // Highstock specific
      navigator: {
        xAxis: {
          gridLineColor: '#cfcfcf',
        },
      },
      rangeSelector: {
        buttonTheme: {
          fill: 'white',
          stroke: '#C0C0C8',
          'stroke-width': 1,
          states: {
            select: {
              fill: '#cfcfcf',
            },
          },
        },
      },
      scrollbar: {
        trackBorderColor: '#C0C0C8',
      },

      // General
      // background2: '#E0E0E8',
      credits: {
        enabled: false,
        style: {
          color: '#666',
        },
      },
      exporting: {
        allowHTML: true,
        enabled: false,
      },
      time: {
        useUTC: false,
      },
      lang: {
        thousandsSep: ',',
        numericSymbols: ['k', 'M', 'B', 'T', 'P', 'E'],
      },
    },
    options ?? {}
  ) as Highcharts.Options;
}

function getAlternateName(names: string[], index: number) {
  // First make sure the index is valid.  Can't have null, undefined or a negative number.
  index = Math.abs(index ?? 0);

  // The logic here is:
  // Choose a name from the list of names based on the index
  // If the index is one past the number of names in the list then return null so that we can turn off the legend.
  // If the index is too big then bring it down to a reasonable range using the modulus operator.

  let result: string = null;
  if (names && names.length > 0) {
    const i = index % (names.length + 1);
    if (i < names.length) {
      result = names[i];
    } else {
      result = null;
    }
  }
  return result;
}

// This will turn the definition of a trend that comes from the server into an object that can be interpreted by
// the highcharts directive.  If data has been added to the object from the server it will be displayed.  If not
// then data can be added with the setDataOnChartConfiguration method above.
export function createHighchartsDefinition(
  trend: IPDTrend,
  theme: string,
  measurements?: IAssetMeasurementsSet,
  startDate?: Date,
  endDate?: Date,
  labelIndex?: number,
  isToggled?: boolean,
  allowHiddenLabel?: boolean,
  exportSettings?: Highcharts.ExportingOptions,
  correlationTrend?: ICorrelationData[]
): Highcharts.Options {
  const colors = new ColorService();
  const startTime = startDate;
  const endTime = endDate;
  const colorServiceSymbols = [
    'circle',
    'square',
    'diamond',
    'triangle',
    'triangle-down',
  ];

  // Create a result that has the chart definition that can be passed to the chart object.
  const result = getDefaultHighchartsOptions({
    title: { text: trend.Title },
    series: [],
  });

  // This will send the pins along to the render function in the event that the user wants to see a table.
  // When pins come from the server they may be in strings instead of Dates.
  // This will turn the strings into dates at the beginning so we don't have to deal with it later.
  const tablePins = trend.Pins.map((p, k) => {
    return {
      ...p,
      StartTime: getDate(p.StartTime),
      EndTime: getDate(p.EndTime),
      PinColor: isNil(p.PinColor) ? k : p.PinColor,
    };
  });

  // This will be set to the series object if it is found.  It will remain null if this is not an xy chart.
  let isXY: IPDTrendSeries = null;

  let cntPin = 0;
  let subColor = 0;

  // The name lengths are used to format the legend area.
  let maxNameLength = 0;
  const sortedSeries = [...trend.Series].sort((a, b) => {
    return a.DisplayOrder - b.DisplayOrder;
  });

  let countNonEmptyLabels = 0;
  for (const series of sortedSeries) {
    // Grab the X Series if it exists.  This assumes that only one series has the IsXAxis flag set.
    // If more then one has it set it takes the last one.
    if (series.IsXAxis) {
      isXY = series;
    }

    const alternateNames = series.AlternateDisplayTexts || [];

    // A series could have multiple tags associated with it.  This looks through the tags in the
    // series.  In practice this will not be the case and any series will probably only have the one tag.
    for (const mapData of series.MapData) {
      // check trend label values if empty or not
      // return count trend labels that are not empty
      const seriesName = getAlternateName(alternateNames, labelIndex);
      if (seriesName) {
        countNonEmptyLabels++;
      }

      const mySeries: Highcharts.SeriesOptionsType = {
        legendIndex: series.DisplayOrder,
        id: getSeriesKey(series),
        data: [{}], // This will display the YAxis label even if there's no data on it
        marker: {
          symbol: colorServiceSymbols[0],
        },
        turboThreshold: 0,
        color: series.SeriesColor || colors.getNextColor(),
        stack: series.Stack || null,
        stacking: (series.StackType as Highcharts.OptionsStackingValue) || null,
        type: getChartType(series.ChartTypeID),
        name: series.DisplayText || '',
        visible: series.visible ?? true,
        custom: {
          units: '',
        },
      };

      // Name the series.
      if (series.DisplayText && trend.ChartTypeID !== 22) {
        mySeries.name = seriesName || series.DisplayText;
      }
      maxNameLength = Math.max(maxNameLength, mySeries.name?.length ?? 0);

      // Don't process the axis and pins for this tag if the series is the X Axis.
      if (!series.IsXAxis) {
        mySeries.yAxis = 'axis' + series.Axis + ':' + trend.PDTrendID;

        const ax = getAxis(trend, series.Axis);
        if (ax) {
          mySeries.custom.units = ax.Title;
        }

        if (tablePins.length > 0) {
          if (trend.PinTypeID === 1 && trend.ShowSelected) {
            // In this situation the currently selected time range needs to be shown possibly in addition to the pins.
            const activeSeries = {
              ...mySeries,
              id: getSeriesKey(series),
            };

            if (trend.ShowPins) {
              activeSeries.name = 'Active:' + activeSeries.name;
              activeSeries.color = colors.selectedColor();
            }

            if (every(result.series, (n) => n.name !== activeSeries.name)) {
              result.series.push(activeSeries);
            }
          }
          if (
            trend.PinTypeID === 2 ||
            trend.PinTypeID === 3 ||
            trend.ShowPins
          ) {
            // Here we need to take the series and add a new series for each pin in the system.
            for (const pin of trend.Pins) {
              if (!pin.Hidden) {
                const newPin = {
                  ...mySeries,
                  color: colors.getNextColor(),
                  PinColor: isNil(pin.PinColor) ? cntPin : pin.PinColor,
                };

                // newPin.color = colors.subColor(pin.PinColor, cntPin - 1);
                newPin.marker.symbol =
                  colorServiceSymbols[cntPin % colorServiceSymbols.length];

                const pinID = isNil(pin.PDTrendPinID)
                  ? cntPin
                  : pin.PDTrendPinID;

                newPin.id = getSeriesKey(series, pinID);
                newPin.name = pin.Name + ':' + newPin.name;

                if (!result.series.map((n) => n.id).includes(newPin.id)) {
                  result.series.push(newPin);
                }
              }
              cntPin++;
            }
            subColor++;
          }
        } else {
          result.series.push(mySeries);
        }
      }
    }
  }

  if (!result.legend) {
    result.legend = {};
  }
  // if all trend labels are empty, legend will not be shown in the chart
  // if at least one trend label value is not empty, legend will be shown in the chart
  if (isToggled && allowHiddenLabel) {
    if (countNonEmptyLabels > 0) {
      result.legend.enabled = true;
    } else {
      result.legend.enabled = false;
    }
  }
  // hover each for series
  let origYAxisColor: any;
  result.tooltip = {
    distance: 2,
    snap: 5,
  };
  result.plotOptions = {
    series: {
      stickyTracking: false,
      point: {
        events: {
          mouseOver() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const point = this;
            const series = point.series;
            origYAxisColor = series.yAxis.options.labels.style.color;
            // highlight hovered series and dim other series
            series.chart.series.forEach((seriesX) => {
              if (seriesX.visible && seriesX.data.length > 0) {
                if (seriesX.name === series.name) {
                  series.setState('hover');
                } else {
                  seriesX.setState('inactive');
                }
              }
            });
          },
          mouseOut() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const point = this;
            const series = point.series;
            // set series opacity back to default
            series.chart.series.forEach((seriesX) => {
              if (seriesX.visible && seriesX.data.length > 0) {
                seriesX.setState('');
              }
            });
            series.chart.tooltip.hide();
          },
        },
      },
      findNearestPointBy: 'xy',
      states: {
        inactive: {
          opacity: 0.6,
        },
      },
      events: {},
    },
  };

  // Sort the series based on the index and then the name.  The legend should be determined by the legendIndex
  // but there may be some aspects that do not respect that value.  Also, there could be multiple series with
  // the same legend index.  In that case we still want the order to be deterministic.
  result.series.sort(seriesCompare);

  // Set the legend index back to the order.  This is only necessary if there are multiple series with the same index.
  result.series.map((s, i) => {
    s.legendIndex = i;
  });

  // If one of the trends is set to X axis.
  if (isXY) {
    result.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'linear',
      showEmpty: true,
      title: {
        text: isXY.DisplayText,
      },
      min: null,
      max: null,
      breaks: null,
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
    };
    result.xAxis.endOnTick = true;

    const myXAxis = getAxis(trend, isXY.Axis);
    if (myXAxis) {
      if (!isNil(myXAxis.Min)) {
        result.xAxis.min = +myXAxis.Min;
      }
      if (!isNil(myXAxis.Max)) {
        result.xAxis.max = +myXAxis.Max;
      }
    }
  } else if (trend.ShowPins && trend.PinTypeID === 1 && !trend.ShowSelected) {
    // If the pin type is 1 and we are not showing the selected range then go from the minimum pin time to the maximum pin time
    result.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'datetime',
      showEmpty: true,
      min: findMin(trend),
      max: findMax(trend),
      title: {
        text: ' ',
      },
      breaks: null,
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
    };
  } else if (trend.ShowPins && trend.PinTypeID === 2) {
    // If the pin type is 2 then go from the minimum pin time to the maximum pin time but add in breaks
    result.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'datetime',
      showEmpty: true,
      min: findMin(trend),
      max: findMax(trend),
      breaks: getBreaks(trend),
      title: {
        text: ' ',
      },
    };
  } else if (trend.ShowPins && trend.PinTypeID === 3) {
    // If the pin type is 3 them we don't need to worry about the axis
    // because it will be switched to a number axis with a tick for each data point.
    // visible is not available until release 4.1.9 of highcharts.  We are currently on 4.1.5
    result.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'linear',
      showEmpty: true,
      title: {
        text: ' ',
      },
      visible: false,
      labels: {
        enabled: false,
      },
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
    };

    result.tooltip = result.tooltip || {};
    result.tooltip.useHTML = true;
    result.tooltip.headerFormat = `<table style='color:white'>`;

    result.tooltip.pointFormat = `<tr>
        <td>
          <div style="height:5px;width:5px;background-color:{series.color};padding-right:5px;"></div>
        </td>
        <td>{series.name}</td>
        <td style="padding-right:5px;padding-left:5px;text-align:right;">{point.y}</td>
        <td>{point.timestamp}</td>
      </tr>`;

    result.tooltip.footerFormat = '</table>';
  } else {
    // It arrives here if we are not showing pins or we are showing the currently selected time.
    result.xAxis = {
      ...defaultXAxis,
      id: 'xAxis:' + trend.PDTrendID,
      type: 'datetime',
      tickPosition: 'outside',
      tickLength: 8,
      tickWidth: 2,
      showEmpty: true,
      title: {
        text: ' ',
      },
    };

    if (startTime) {
      result.xAxis.min = startTime.getTime();
    }
    if (endTime) {
      result.xAxis.max = endTime.getTime();
    }
  }

  // Assigning the axis definitions to the chart.
  result.yAxis = [];
  for (const pdaxis of trend.Axes) {
    if (countRelatedSeries(trend, pdaxis.Axis) > 0) {
      // This is the axis that will get sent to Highcharts
      const axisColor = theme === 'dark' ? '#E0E0E3' : '#666666';

      const axis: Highcharts.YAxisOptions = {
        ...defaultYAxis,
        id: 'axis' + pdaxis.Axis + ':' + trend.PDTrendID,
        title: {
          text: pdaxis.Title,
          style: {
            color: axisColor,
          },
        },
        labels: {
          style: {
            color: axisColor,
          },
        },
        opposite: pdaxis.Position !== 0,
        showEmpty: false,
        tickPosition: 'outside',
        tickLength: 8,
        tickWidth: 2,
        gridLineWidth: 0,
      };

      if (pdaxis.GridLine) {
        axis.gridLineWidth = 1;
      }

      if (typeof pdaxis.Step === 'number') {
        axis.tickInterval = pdaxis.Step;
      }

      if (typeof pdaxis.MinorStep === 'number') {
        axis.minorTickInterval = pdaxis.MinorStep;
      }

      if (typeof pdaxis.Min === 'number') {
        axis.min = pdaxis.Min;
        axis.startOnTick = false;
        axis.minPadding = 0;
      } else {
        axis.startOnTick = false;
        axis.minPadding = 0;
        // For all of the series associated with this axis
        for (const s of trend.Series) {
          if (s.Axis === pdaxis.Axis) {
            for (const md of s.MapData) {
              // Find the data
              let min = getPointsByTagPinArchive(
                measurements,
                md.Map.TagID,
                s.Archive,
                md.PinID
              )?.proposedSeriesMin;

              if (correlationTrend) {
                min = Math.min(min, getMinTimeSeriesValue(correlationTrend));
              }

              // set the min to the lower of the existing min or the data min
              if (isNil(axis.min) || axis.min === undefined) {
                axis.min = min;
              } else {
                axis.min = Math.min(axis.min, min);
              }
            }
          }
        }
      }

      if (typeof pdaxis.Max === 'number') {
        axis.max = pdaxis.Max;
        axis.endOnTick = false;
        axis.maxPadding = 0;
      } else {
        axis.endOnTick = false;
        axis.maxPadding = 0;
        // For all of the series associated with this axis
        for (const s of trend.Series) {
          if (s.Axis === pdaxis.Axis) {
            for (const md of s.MapData) {
              // Find the data
              let max = getPointsByTagPinArchive(
                measurements,
                md.Map.TagID,
                s.Archive,
                md.PinID
              )?.proposedSeriesMax;

              if (correlationTrend) {
                max = Math.max(max, getMaxTimeSeriesValue(correlationTrend));
              }

              // set the max to the greater of the existing max or the data max
              if (isNil(axis.max) || axis.max === undefined) {
                axis.max = max;
              } else {
                axis.max = Math.max(axis.max, max);
              }
            }
          }
        }
      }
      result.yAxis.push(axis);
    }
  }

  result.tooltip = result.tooltip || {};
  // if tooltip.shared is set to true, hover specific series while other series are fade will not work.
  result.tooltip.shared = true;
  result.tooltip.valueDecimals = 0;
  result.tooltip.useHTML = true;
  result.tooltip.shadow = false;
  result.tooltip.outside = true;
  // result.xAxis.crosshair = true;

  if (isNil(result.tooltip.formatter)) {
    if (correlationTrend) {
      result.tooltip.formatter = function () {
        return correlationTrendTooltipFormatter(this, correlationTrend);
      };
      result.tooltip.backgroundColor = '';
      result.tooltip.borderWidth = 0;
    } else if (
      (result.xAxis as Highcharts.AxisOptions).title.text === ' ' &&
      (result.xAxis as Highcharts.AxisOptions).type === 'datetime'
    ) {
      result.tooltip.formatter = function () {
        return defaultChartTooltipFormatter(this);
      };
      result.tooltip.backgroundColor = '';
      result.tooltip.borderWidth = 0;
    } else {
      result.tooltip.formatter = function () {
        return varVsVarChartTooltipFormatter(this);
      };
      result.tooltip.backgroundColor = '';
      result.tooltip.borderWidth = 1;
    }
  }

  let fixedLabelWidth = 400;

  if (maxNameLength > 0 && maxNameLength * 20 < fixedLabelWidth) {
    fixedLabelWidth = maxNameLength * 20;
  }

  const totalSeries = getTrendTotalSeries(trend);

  if (totalSeries > 3) {
    result.legend = {
      itemStyle: {},
      itemWidth: fixedLabelWidth,
    };
    (result.legend.itemStyle as any).width = fixedLabelWidth - 20;
    if (isToggled && allowHiddenLabel) {
      if (countNonEmptyLabels > 0) {
        result.legend.enabled = true;
      } else {
        result.legend.enabled = false;
      }
    }
  } else {
    // result.legend = {};
  }

  result.chart = result.chart || {};
  result.chart.type =
    getChartType(+trend.ChartTypeID) || (isXY ? 'scatter' : 'line');

  result.chart.zoomType = 'xy';

  // Here we process the data so that if data is present it is drawn on the chart
  const dataToPlot: { [key: string]: IPDRecordedPoint[] } = {};
  for (const series of sortedSeries) {
    for (const data of series.MapData) {
      if (data.Data && data.Data.length > 0) {
        // There is data for at least one chart.
        dataToPlot[getSeriesKey(series, data.PinID)] = data.Data;
      }
    }
  }

  if (measurements && measurements.Measurements) {
    // add measurements to dataToPlot
    for (const am of measurements.Measurements) {
      dataToPlot[
        getSeriesKeyFromPdTagId(am.Tag.PDTagID, am.PinID, am.Archive)
      ] = am.Data;
    }
  }

  let timestampDictionary: { [key: number]: any } = null;
  if ((isXY?.MapData?.length ?? 0) > 0 && measurements?.Measurements) {
    // && isXY.MapData[0].Data && isXY.MapData[0].Data.length > 0
    // This is an XY chart so we need to process the data into appropriate series.
    // Here we need to process the data into a variable vs variable chart.
    // Find the xAxis Series
    // Need to make sure that value is not null or undefined.
    for (const mapData of isXY.MapData) {
      const xyTag = mapData.Map.TagID;
      if ((measurements?.Pins?.length ?? 0) > 0 && trend.ShowPins) {
        for (const p of measurements.Pins) {
          const xyData = getPointsByTagPinArchive(
            measurements,
            xyTag,
            isXY.Archive,
            p.PDTrendPinID
          );
          timestampDictionary = setTimestampDictionary(
            timestampDictionary,
            xyData
          );
        }
      }
      if (trend.ShowSelected) {
        const xyData = getPointsByTagPinArchive(
          measurements,
          xyTag,
          isXY.Archive,
          null
        );
        timestampDictionary = setTimestampDictionary(
          timestampDictionary,
          xyData
        );
      }
    }
  }

  let timeInMilliseconds = null;
  let timestampValue = null;
  for (const series of result.series) {
    let myData = dataToPlot[series.id];
    if (!myData) {
      if (series.id) {
        const archiveIndex = series.id.lastIndexOf(':') + 1;
        const archiveString = series.id.substring(0, archiveIndex);
        myData = dataToPlot[archiveString];
      }
    }
    if (myData && myData.length > 0) {
      const newSeries = [];
      if (timestampDictionary) {
        // Need to make sure that the timestamp dictionary and value is not null.
        for (const data of myData) {
          if (
            data.Status !== 255 &&
            data.Value !== null &&
            data.Value !== undefined
          ) {
            timeInMilliseconds = data.Time.getTime();
            timestampValue = timestampDictionary[timeInMilliseconds];
            if (timestampValue !== null && timestampValue !== undefined) {
              newSeries.push({
                x: timestampValue,
                y: data.Value,
                status: data.Status,
              });
            } else {
              timestampValue = null;
            }
          }
        }
      } else if (trend.ShowPins && trend.PinTypeID === 3) {
        // Here the chart is supposed to display pinned data in an overlay.  That means we need to take each series and
        // adjust the timestamp so that is a integer representing the ticks of the date minus the ticks of the first date.
        const startTick = myData
          .reduce(function (r, a) {
            return r.Time < a.Time ? r : a;
          })
          .Time.getTime();

        for (const data of myData) {
          if (data.Status !== 255) {
            newSeries.push({
              x: (data.Time.getTime() - startTick) / 10000,
              y: data.Value,
              status: data.Status,
              timestamp: moment(data.Time).format('M/D/YY h:mm A'),
            });
          }
        }
      } else {
        for (const data of myData) {
          newSeries.push({
            x: data.Time.getTime(),
            y: data.Status !== 255 ? data.Value : null,
            status: data.Status,
          });
        }
      }
      newSeries.sort((a, b) => {
        return a.x - b.x;
      });
      (series as any).data = newSeries;
    }
  }

  if (trend.DesignCurves && trend.DesignCurves.length > 0) {
    for (const dc of trend.DesignCurves) {
      const myData = getDataFromDesignCurve(dc, !isXY, startDate, endDate);

      let myType = 'spline';
      if (dc.Type === 'range') {
        myType = 'areasplinerange';
      }

      const newSeries = {
        type: myType,
        id: getDesignCurveKey(dc),
        color: dc.Color,
        data: myData,
        yAxis: 'axis' + String(dc.Axis) + ':' + trend.PDTrendID,
        name: dc.DisplayText,
        legendIndex: result.series.length + 1,
        fillOpacity: 0.1,
      };
      const hasAxis = result.yAxis.find((axis) => axis.id === newSeries.yAxis);
      if (hasAxis) {
        result.series.unshift(newSeries as any);
      }
    }
  }

  //Correlation Trend configuration
  if (trend.PDTrendID === -1 && correlationTrend) {
    const correlationTrendType =
      correlationTrend?.find((ct) => ct.Name === EROCTrend.Regression)?.Data
        ?.length > 0
        ? 'ROC'
        : 'Forecast';
    correlationTrend?.forEach((mcTrend) => {
      switch (mcTrend.Name) {
        case EForecastTrend.Forecast:
        case EForecastTrend.GreaterThanThreshold:
        case EForecastTrend.LessThanThreshold:
        case EROCTrend.Regression:
        case EROCTrend.Expected:
        case EROCTrend.Upper:
        case EROCTrend.Lower:
          if (mcTrend.Data?.length > 0) {
            result.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: 'line',
              legendIndex: result.series.length + 1,
              marker: {
                radius: 0,
              },
              dashStyle: mcTrend.Name === EROCTrend.Expected ? 'Dash' : 'Solid',
              lineWidth:
                mcTrend.Name === EROCTrend.Upper ||
                mcTrend.Name === EROCTrend.Lower
                  ? 1
                  : 2,
              color: CorrelationTrendColorMapping[mcTrend.Name],
              custom: {
                units: getAxis(trend, sortedSeries[0].Axis)?.Title ?? '',
              },
              data: transformTimeseriesDatasToCoordinates(mcTrend.Data),
            } as any);
          }
          break;
        case EForecastTrend.LastRegression:
        case EForecastTrend.Horizon:
          if (mcTrend.Data?.length > 0) {
            result.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: null,
              legendIndex: result.series.length + 1,
              marker: {
                symbol: 'diamond',
                radius: 8,
              },
              color: CorrelationTrendColorMapping[mcTrend.Name],
              lineWidth: 0,
              custom: {
                units: getAxis(trend, sortedSeries[0].Axis)?.Title ?? '',
              },
              data: [transformTimeseriesDataToCoordinates(mcTrend.Data[0])],
            } as any);
          }
          break;
        case EForecastTrend.Exceedance:
          if (mcTrend.Data?.length > 0) {
            result.series.push({
              id: `${correlationTrendType}-${mcTrend.Name}`,
              name: CorrelationTrendNameMapping[mcTrend.Name],
              type: null,
              legendIndex: result.series.length + 1,
              marker: {
                symbol: 'url(./assets/emblem-alerts.svg)',
                width: 15,
                height: 15,
              },
              lineWidth: 0,
              custom: {
                units: getAxis(trend, sortedSeries[0].Axis)?.Title ?? '',
              },
              data: [transformTimeseriesDataToCoordinates(mcTrend.Data[0])],
            } as any);
          }
          break;
      }
    });
  }

  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  result.exporting = exportSettings || result.exporting;

  // Manipulate some of the styling to get more data into view.
  result.credits = {
    enabled: false,
  };

  result.chart = { ...result.chart, spacingBottom: 0 };

  return result;
}

// Model Trend and Model Action Charts Configurations
export function getChartConfigForEmptyResult(start?: Date, end?: Date) {
  const chartConfig = getChartConfigForResultWithData(
    [],
    [],
    [],
    '',
    3600,
    [],
    [],
    start,
    end,
    null,
    null
  );
  chartConfig.title.text = 'No data found.';
  return chartConfig;
}

export function turnEmptyModelTrendToHighcharts(
  title: string,
  exportSettings?: Highcharts.ExportingOptions
) {
  const result = getChartConfigForEmptyResult();
  result.title.text = title;
  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  result.exporting = exportSettings || result.exporting;
  return result;
}

export function turnModelTrendToHighcharts(
  title: string,
  modelTrend: IAssetModelChartingData,
  exportSettings?: Highcharts.ExportingOptions
) {
  let result = getChartConfigForEmptyResult(modelTrend?.start, modelTrend?.end);

  const measurements = modelTrend;
  const ranges: {
    // name: string;
    x: number;
    low: number;
    high: number;
  }[] = [];
  const actuals = [];
  const expecteds = [];
  const bands = [];
  const annotations = [];

  let startingTick = 0;
  let endingTick = 0;
  let tickInterval = 3600; // 1 hour by default
  let minValueEncountered = Number.MAX_VALUE;
  let maxValueEncountered = Number.MIN_VALUE;

  if ((measurements?.SeriesList?.length ?? 0) > 0) {
    const series = measurements.SeriesList;
    startingTick = new Date(series[0].TimeStamp).getTime();
    endingTick = new Date(series[series.length - 1].TimeStamp).getTime();
    for (let i = 0; i < series.length; i++) {
      const element = series[i];
      const date = new Date(element.TimeStamp).getTime();
      const dataPoints = [
        element.Min,
        element.Max,
        element.Actual,
        element.Expected,
      ];
      const minValueForThisElement = Math.min(...dataPoints);
      if (minValueForThisElement < minValueEncountered) {
        minValueEncountered = minValueForThisElement;
      }

      const maxValueForThisElement = Math.max(...dataPoints);
      if (maxValueForThisElement > maxValueEncountered) {
        maxValueEncountered = maxValueForThisElement;
      }

      // check if surrounding points are null.
      let surroundedByNulls = element.EvaluationStatus === 0; // only true if current point is valid
      if (surroundedByNulls && i > 0) {
        surroundedByNulls = series[i - 1].EvaluationStatus !== 0; // and if previous point is null
      }
      if (surroundedByNulls && i < series.length - 1) {
        surroundedByNulls = series[i + 1].EvaluationStatus !== 0; // and if next point is null
      }

      ranges.push({
        // name: String(date),
        x: date,
        low: element.EvaluationStatus === 0 ? element.Min : null,
        high: element.EvaluationStatus === 0 ? element.Max : null,
      });

      if (surroundedByNulls) {
        actuals.push({
          // name: date,
          x: date,
          y: element.Actual,
          marker: {
            enabled: true,
            radius: 2,
            states: {
              hover: {
                radius: 6,
              },
            },
          },
        });
        expecteds.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Expected : null,
          marker: {
            enabled: true,
            radius: 2,
            states: {
              hover: {
                radius: 6,
              },
            },
          },
        });
      } else {
        actuals.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Actual : null,
        });
        expecteds.push({
          // name: date,
          x: date,
          y: element.EvaluationStatus === 0 ? element.Expected : null,
        });
      }
    }

    if (endingTick > 0) {
      tickInterval = (endingTick - startingTick) / 60;
    }

    for (const outOfBoundDate of measurements.OutOfBoundDates) {
      const dateFrom = new Date(outOfBoundDate.From).getTime();
      const dateTo = new Date(outOfBoundDate.To).getTime();
      bands.push({
        color: 'rgba(255,217,142,0.80)',
        from: dateFrom,
        to: dateTo,
        zIndex: -1,
      });
    }

    for (const alertEvent of measurements.Events) {
      if (alertEvent.TimeStamp) {
        const alertDate = new Date(alertEvent.TimeStamp).getTime();
        annotations.push({
          x: alertDate,
          y:
            minValueEncountered -
            (maxValueEncountered - minValueEncountered) * 0.17,
          name: alertEvent.Text,
          marker: {
            radius: 7,
          },
          text: alertEvent.Text,
          eventType: alertEvent.EventType,
        });
      }
    }
    // configure chart and then get the minimum y value so that we can plot events there
    result = getChartConfigForResultWithData(
      ranges,
      actuals,
      expecteds,
      modelTrend?.Model?.DependentVariable?.Tag?.EngUnits,
      tickInterval,
      bands,
      annotations,
      modelTrend.start,
      modelTrend.end,
      modelTrend.Min,
      modelTrend.Max
    );
  }

  if (result && title) {
    result.title = {
      text: title,
    };
  }

  // This is for the context menu at the top-right corner of the charts.
  // This menu has buttons for printing or downloading the chart graphic.
  result.exporting = exportSettings || result.exporting;

  return result;
}

export function getChartConfigForResultWithData(
  ranges,
  actuals,
  expecteds,
  yAxisUnit,
  tickInterval,
  bands,
  flags,
  start: Date,
  end: Date,
  min: number,
  max: number
): Highcharts.Options {
  let startTicks: number = null;
  let endTicks: number = null;
  if (start) {
    startTicks = start.getTime();
  }
  if (end) {
    endTicks = end.getTime();
  }

  const chartConfig: Highcharts.Options = getDefaultHighchartsOptions({
    chart: {
      marginRight: 40,
      zoomType: 'xy',
    },
    plotOptions: {
      series: {
        turboThreshold: 0,
      },
    },
    credits: {
      enabled: false,
    },
    title: {
      text: 'Testing',
    },
    xAxis: {
      type: 'datetime',
      plotBands: bands,
      min: startTicks,
      max: endTicks,
    },
    tooltip: {
      enabled: true,
      shared: true,
      valueDecimals: 0,
      formatter: null,
      xDateFormat: 'dddd, MMMM Do YYYY h:mm:ss a',
    },
    yAxis: [
      {
        title: {
          text: yAxisUnit,
        },
        labels: {},
        min,
        max,
        startOnTick: true,
        endOnTick: true,
      },
    ],

    series: [
      {
        name: 'Actual',
        data: actuals,
        zIndex: 2,
        color: 'blue',
        lineWidth: 2,
        type: null,
      },
      {
        name: 'Expected',
        data: expecteds,
        zIndex: 3,
        color: 'red',
        lineWidth: 2,
        type: null,
      },
      {
        name: 'Range',
        data: ranges,
        type: 'arearange',
        lineWidth: 0,
        linkedTo: ':previous',
        fillColor: 'rgba(189, 228, 228, 0.4)',
        marker: {
          enabled: false,
          states: {
            hover: {
              enabled: false,
            },
          },
        },
        zIndex: 1,
      },
      {
        name: 'Event',
        data: flags,
        type: 'scatter',
        color: 'red',
        tooltip: {
          // eslint-disable-next-line object-shorthand
          pointFormatter: function () {
            return String(this.x);
          },
        },
      },
    ],
  });

  //For custom yAxis min/max values we want the startOnTick & endOnTick props = false
  //so odd min/max values won't cause the yAxis to be extended because it needs
  //to start/end on a tick
  if (
    typeof chartConfig.yAxis[0].min === 'number' &&
    typeof chartConfig.yAxis[0].max === 'number'
  ) {
    chartConfig.yAxis[0] = {
      ...chartConfig.yAxis[0],
      startOnTick: false,
      endOnTick: false,
      minPadding: 0,
    };
  }

  chartConfig.tooltip.formatter = function () {
    return defaultChartTooltipFormatter(this);
  };

  return chartConfig;
}

export function getTitleFromModel(model: INDModelSummary) {
  let result = model?.ModelName ?? '';
  if (model.TagName) {
    result = result + ' (' + model.TagName + ')';
  }
  return result;
}

export function turnModelActionToHighCharts(
  data: INDModelActionItemAnalysis[],
  model: INDModelSummary
) {
  let result = getChartConfigForEmptyResult();
  const action = data;
  const watch: number[][] = [];
  const clear: number[][] = [];
  const diagnose: number[][] = [];
  const maintenance: number[][] = [];
  const current: number[][] = [];
  const bounds: number[][] = [];
  const currentUpper = getNumberBySigFigs(model.UpperLimit, 4);
  const currentLower = getNumberBySigFigs(model.LowerLimit, 4);
  const currentExpected = getNumberBySigFigs(model.Expected, 4);
  const currentActual = getNumberBySigFigs(model.Actual, 4);
  const engUnits = model.EngUnits;

  if ((action?.length ?? 0) > 0) {
    for (const item of action) {
      const actual = getNumberBySigFigs(item.Actual, 4);
      const expected = getNumberBySigFigs(item.Expected, 4);
      switch (+item.ModelActionItemTypeID) {
        case 1: // diagnose
          diagnose.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
        case 3: // watch set
          watch.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
        case 6: // maintenance
          maintenance.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
        case 11: // ignore set
          watch.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
        case 12: // clear alert
          clear.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
        case 16: // quick watch
          watch.push([getNumberBySigFigs(actual - expected, 4), actual]);
          break;
      }
    }

    current.push([
      getNumberBySigFigs(currentActual - currentExpected, 4),
      currentActual,
    ]);
    bounds.push(
      [getNumberBySigFigs(currentLower - currentExpected, 4), currentUpper],
      [getNumberBySigFigs(currentUpper - currentExpected, 4), currentUpper],
      [getNumberBySigFigs(currentUpper - currentExpected, 4), currentLower],
      [getNumberBySigFigs(currentLower - currentExpected, 4), currentLower],
      [getNumberBySigFigs(currentLower - currentExpected, 4), currentUpper]
    );
    result = getChartConfigForData(
      diagnose,
      clear,
      watch,
      maintenance,
      current,
      bounds,
      engUnits
    );
  }
  return result;
}

function sortData(data: Array<Array<number>>) {
  if (data && data.length > 0) {
    data.sort((a, b) => {
      return (a[0] ?? 0) - (b[0] ?? 0);
    });
  }
}

export function getChartConfigForData(
  diagnose: Array<Array<number>>,
  clear: Array<Array<number>>,
  watch: Array<Array<number>>,
  maintenance: Array<Array<number>>,
  current: Array<Array<number>>,
  bounds: Array<Array<number>>,
  engUnits: string
): Highcharts.Options {
  sortData(diagnose);
  sortData(clear);
  sortData(watch);
  sortData(maintenance);
  sortData(current);
  // The bounds is intended to draw a box.  In order to get it to show in the
  // legend it has to be in a series.  Highcharts complains about unsorted data
  // but it will display the box correctly if the data is not sorted.
  // sortData(bounds);

  const chartConfig: Highcharts.Options = getDefaultHighchartsOptions({
    chart: {
      type: 'scatter',
      zoomType: 'xy',
      marginRight: 40,
    },
    credits: {
      enabled: false,
    },
    title: {
      text: '',
    },
    xAxis: {
      title: {
        text: 'Actual - Expected, ' + engUnits,
        style: {
          fontWeight: 'bold',
        },
      },
      startOnTick: true,
      endOnTick: true,
      showLastLabel: true,
    },
    yAxis: {
      title: {
        text: 'Actual, ' + engUnits,
        style: {
          fontWeight: 'bold',
        },
      },
    },
    legend: {
      layout: 'horizontal',
      align: 'center',
      verticalAlign: 'top',
      x: 0,
      y: 0,
      borderWidth: 0,
    },
    plotOptions: {
      scatter: {
        marker: {
          radius: 5,
          states: {
            hover: {
              enabled: true,
              lineColor: 'rgb(100,100,100)',
            },
          },
        },
        tooltip: {
          headerFormat: '<b>{series.name}</b><br>',
          pointFormat: 'Actual - Expected: {point.x}, Actual Value: {point.y}',
        },
      },
      series: {
        stickyTracking: false,
        allowPointSelect: true,
      },
    },
    series: [
      {
        name: 'Watch',
        marker: {
          symbol: 'url(./assets/emblem-watch.svg)',
          width: 8,
          height: 8,
        },
        data: watch,
        type: null,
      },
      {
        name: 'Clear',
        marker: {
          symbol: 'url(./assets/emblem-clear.svg)',
          width: 8,
          height: 8,
        },
        data: clear,
        type: null,
      },
      {
        name: 'Maintenance',
        marker: {
          symbol: 'url(./assets/emblem-maintainence.svg)',
          width: 12,
          height: 12,
        },
        data: maintenance,
        type: null,
      },
      {
        name: 'Diagnose',
        marker: {
          symbol: 'url(./assets/emblem-diagnose.svg)',
          width: 12,
          height: 12,
        },
        data: diagnose,
        type: null,
      },
      {
        name: 'Current Status',
        marker: {
          symbol: 'url(./assets/emblem-status.svg)',
          width: 24,
          height: 24,
        },
        data: current,
        type: null,
      },
      {
        name: 'Model Bounds',
        type: 'line',
        pointPlacement: 'on',
        color: 'rgba(255,196,60,.9)',
        data: bounds as any,
      },
    ],
  });
  return chartConfig;
}

export function getChartConfigForEmptyResultModelAction() {
  const chartConfig = getChartConfigForData([], [], [], [], [], [], null);
  chartConfig.title.text = 'No data found.';
  chartConfig.title.y = 70;
  return chartConfig;
}

class ColorService {
  // Internal method to choose colors in sequence.  If the color is set in the configuration this is not used.
  private nextColorIndex = 0;
  private nextSelectedColorIndex = 0;
  private seriesColor = [
    '#6ea09d',
    '#79c250',
    '#15afea',
    '#bde4e4',
    '#e16b55',
    '#e2bd68',
    '#437638',
    '#ba3f26',
    '#15afea',
    '#bde4e4',
  ];

  public getNextColor() {
    if (Highcharts && Highcharts.getOptions) {
      const options = Highcharts.getOptions();

      if (this.nextColorIndex >= this.seriesColor.length) {
        this.nextColorIndex = 0;
      }
      return this.seriesColor[this.nextColorIndex++];
    }
    return '#000000';
  }

  public selectedColor(idx?: number) {
    if (isNil(idx)) {
      idx = this.nextSelectedColorIndex;
      this.nextSelectedColorIndex++;
    }
    const selectedColors = [
      '#929497',
      '#404041',
      '#d0d2d3',
      '#6d6e70',
      '#bbbdbf',
    ];
    return selectedColors[Math.abs(idx) % selectedColors.length];
  }
}
