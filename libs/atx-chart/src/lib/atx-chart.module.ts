import { HighchartsChartModule } from 'highcharts-angular';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartDisplayComponent } from './component/chart-display/chart-display.component';
import { ChartSelectorComponent } from './component/chart-selector/chart-selector.component';
import { DonutChartComponent } from './component/donut-chart/donut-chart.component';
import { BarChartDisplayComponent } from './component/bar-chart-display/bar-chart-display.component';
import { TableDisplayComponent } from './component/table-display/table-display.component';
import { UpdateLimitsDialogComponent } from './component/update-limits-dialog/update-limits-dialog.component';
import { CommonModule } from '@angular/common';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedApiModule } from '@atonix/shared/api';
import { ModelTrendChartDisplayComponent } from './component/model-trend-chart-display/model-trend-chart-display.component';

@NgModule({
  declarations: [
    ChartDisplayComponent,
    ModelTrendChartDisplayComponent,
    DonutChartComponent,
    ChartSelectorComponent,
    TableDisplayComponent,
    BarChartDisplayComponent,
    UpdateLimitsDialogComponent,
  ],
  imports: [
    CommonModule,
    AtxMaterialModule,
    SharedApiModule,
    FormsModule,
    ReactiveFormsModule,
    HighchartsChartModule,
  ],
  exports: [
    DonutChartComponent,
    ChartSelectorComponent,
    ChartDisplayComponent,
    ModelTrendChartDisplayComponent,
  ],
})
export class ChartModule {}
