import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssetTreeComponent } from './component/asset-tree/asset-tree.component';
import { AssetTreeRoutingComponents } from './atx-asset-tree-routing.module';
import { HorizontalLayoutComponent } from './component/horizontal-layout/horizontal-layout.component';
import { ModelService } from './service/model.service';
import { AtxIndexedDbModule } from '@atonix/atx-indexed-db';
import { AtxMaterialModule } from '@atonix/atx-material';
import { A11yModule } from '@angular/cdk/a11y';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { TreeWrapperComponent } from './component/tree-config/tree-wrapper/tree-wrapper.component';
import { TreeConfigFacade } from './service/tree-config/tree-config.facade';
import { NodeTreeComponent } from './component/tree-config/node-tree/node-tree.component';
import { AtxCustomerTreeModule } from '@atx/atx-customer-tree';

@NgModule({
  declarations: [
    AssetTreeComponent,
    AssetTreeRoutingComponents,
    HorizontalLayoutComponent,
    TreeWrapperComponent,
    NodeTreeComponent,
  ],
  imports: [
    CommonModule,
    AtxIndexedDbModule,
    AtxMaterialModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    A11yModule,
    DragDropModule,
    ScrollingModule,
    AtxCustomerTreeModule,
  ],
  exports: [AssetTreeComponent, HorizontalLayoutComponent],
})
export class AssetTreeModule {
  static forRoot(): ModuleWithProviders<AssetTreeModule> {
    return {
      ngModule: AssetTreeModule,
      providers: [ModelService, TreeConfigFacade],
    };
  }
}
