import { ITree } from './tree';
import { ITreeNode } from './tree-node';
import { EntityState, Dictionary } from '@ngrx/entity';
import { IAutoCompleteItem } from '@atonix/atx-core';

// The entirety of the data the asset tree needs to display itself.
// This doesn't include all of the data used to track non-displayed nodes.
export interface ITreeConfiguration {
  // If true the ad-hoc tree dropdown will be displayed.
  showTreeSelector: boolean;

  // The list of trees to display in the ad-hoc tree dropdown.
  trees: ITree[];

  // The ID of the tree that is currently selected
  selectedTree: string;

  // The value displayed in the autocomplete box.
  autoCompleteValue: string;

  // True if the results of an autocomplete webservice call are pending.
  autoCompletePending: boolean;

  // The list of assets in the dropdown of the autocomplete box.,
  autoCompleteAssets: IAutoCompleteItem[];

  // The nodes to be displayed.  This is a flat list derived from the full list of assets in memory.
  nodes: ITreeNode[];

  // True if the pin icon displays the pin.  Used to indicate whether the layout should put the
  // asset tree over or next to other data on the screen.
  pin: boolean;

  // True if assets are being loaded.  Used to apply styling.
  loadingAssets: boolean;

  // True if an entire tree is being loaded.  Used to apply styling.
  loadingTree: boolean;

  // If true the drag/drop is enabled on this control.
  canDrag?: boolean;

  // List of targets for dropping.  Only these targets will accept assets from the tree.
  dropTargets?: string[];

  // Permissions
  canView: boolean;
  canAdd: boolean;
  canEdit: boolean;
  canDelete: boolean;

  // If true, the path of non-selected nodes will be collapsed.
  collapseOthers: boolean;

  // If true the configure button will be hidden
  hideConfigureButton: boolean;
}

// The definition of an objec that will hold the asset tree nodes.
// The displayed nodes will be derived from this store.
// This object is intended to be immutable.  It should not be changed, only
// copied into a new object with some parameters changed.
export interface ITreeNodes extends EntityState<ITreeNode> {
  // This is the list of the root assets of the tree
  rootAssets: string[];

  // This dictionary allows you to easily look up the children of an asset.
  childrenDict: Dictionary<string[]>;

  // This is the ID of the tree.  Many applications will want to only keep the selected
  // tree in memory, so the tree ID may not be necessary.  Others may want to keep all
  // of the trees.  In that case the tree ID will allow them to be kept in a dictionary.
  treeID: string;

  // This is the App Context used when the tree was created.  It is included here so that
  // if we want to see if we already have the requested tree loaded we can.
  appContext?: string;
}

// Most apps will need to keep the configuration (what controls the display) and the nodes (what maintains the store of data) together.
// This interface keeps them together.
export interface ITreeState {
  treeConfiguration: ITreeConfiguration;
  treeNodes: ITreeNodes;
  hasDefaultSelectedAsset: boolean;
}
