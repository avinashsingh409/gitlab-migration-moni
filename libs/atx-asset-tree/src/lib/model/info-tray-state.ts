import { SafeStyle } from '@angular/platform-browser';
import { IAssetAndAttributes, ITempAttributeType } from '@atonix/atx-core';

export interface IInfoTrayState {
  showInfoTray: boolean;
  asset: IAssetData;
  assetIDs: string[];
  attributes: ITempAttributeType[];
  selectedTab: string;
}

export interface IAssetData {
  selectedAssetIDs: string[];
  assetID: string;
  assetAttributes: IAssetAndAttributes;
  hasImages: boolean;
  imagesPath: IImageData[];
  assetName: string;
}

export interface IImageData {
  imageBlobUrl: SafeStyle;
  imageCaption: string;
  displayOrder: number;
}
