// The object that is actually displayed in the tree.  This is just enough information for the
// tree to be able to display the node correctly.
export interface ITreeNode {
  uniqueKey: string;
  parentUniqueKey: string;
  nodeAbbrev: string;
  displayOrder: number;
  level: number;
  selected: boolean;
  retrieved: boolean;
  symbol: 'collapsed' | 'expanded' | 'loading' | 'nothing';
  data?: any;
}
