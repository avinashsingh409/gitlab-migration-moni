// Populates the asset tree dropdown.
export interface ITree {
  name: string;
  id: string;
  isUserOwned?: boolean;
  isDefaultTree?: boolean;
  customerId?: string;
}
