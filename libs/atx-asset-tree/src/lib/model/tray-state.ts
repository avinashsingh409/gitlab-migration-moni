import { MatDrawerMode } from '@angular/material/sidenav';

export type TrayState = MatDrawerMode;
