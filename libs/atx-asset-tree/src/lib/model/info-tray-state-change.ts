import { IAssetData } from '../model/info-tray-state';

export interface IInfoTrayStateChange {
  event: 'ToggleInfoTray' | 'GetAsset' | 'AssetRetrieved';
  newValue?: string | string[] | boolean | IAssetData;
}
