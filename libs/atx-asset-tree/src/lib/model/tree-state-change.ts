import { ITree } from './tree';
import { ITreeNode } from './tree-node';
import {
  IAtxAssetTreeNode,
  IAtxTreeRetrieval,
  IAutoCompleteResult,
  ITreePermissions,
} from '@atonix/atx-core';

// Used with the SelectAsset event.  Will indicate what assets were selected.
export interface ISelectAsset {
  ids: string[] | string;
  multiSelect: boolean;
}

// Used with the NodesRetrieved event.  Sends the list of nodes retrieved from
// the server back to the state management routine.
export interface IExpandNode {
  id: string;
  nodes: ITreeNode[];
}

// The change event.  A stream of these emitted from the asset tree component should control
// the behavior of the asset tree.
export interface ITreeStateChange {
  event:
    | 'GetPermissions' // null
    | 'PermissionsRetrieved' // IAssetTreePermissions
    | 'LoadTree' // string
    | 'TreeRetrieved' // IAtxTreeRetrieval
    | 'SelectAsset' // ISelectAsset
    | 'IconClicked' // string
    | 'NodesRetrieved' // IExpandNode
    | 'AutoCompleteChanged' // string
    | 'AutoCompleteChangedRetrieved' // IAutoCompleteResult
    | 'PinToggle' // boolean?
    | 'EditAssetTree' // null
    | 'ReloadAssetTree';
  newValue?:
    | string
    | string[]
    | boolean
    | ITree
    | ITreeNode
    | ITreePermissions
    | IAtxTreeRetrieval
    | IExpandNode
    | IAutoCompleteResult
    | ISelectAsset
    | IAtxAssetTreeNode;
}
