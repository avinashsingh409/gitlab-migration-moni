import { Route, Routes } from '@angular/router';
import { TreeConfigComponent } from './component/tree-config/modal/tree-config.component';

// If app wants a specific route
export const TreeConfigRoute: Route = {
  path: 'edit',
  component: TreeConfigComponent,
};

// If app wants all the routes specified in this module.
export const AssetTreeRoutes: Routes = [TreeConfigRoute];

export const AssetTreeRoutingComponents = [TreeConfigComponent];
