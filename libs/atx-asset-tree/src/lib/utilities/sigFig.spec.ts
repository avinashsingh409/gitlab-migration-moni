import { sigFig } from './sigFig';

describe('sigFig', () => {
  it('should work', () => {
    expect(sigFig(null, 4)).toEqual('');
    expect(sigFig(0, 4)).toEqual('0');
  });

  it('should default to 4 sig figs when fed a null v1', () => {
    expect(sigFig(1, null)).toEqual('1');
  });

  it('should default to 4 sig figs when fed a null v2', () => {
    expect(sigFig(1.253, null)).toEqual('1.253');
  });

  it('should default to 4 sig figs when fed a null v3', () => {
    expect(sigFig(1.25386, null)).toEqual('1.254');
  });

  it('should round up', () => {
    expect(sigFig(1.2535, null)).toEqual('1.254');
  });

  it('should round down', () => {
    expect(sigFig(1.2534, null)).toEqual('1.253');
  });
});
