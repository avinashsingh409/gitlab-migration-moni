import {
  IAssetAttribute,
  IAttributeType,
  IAttributeTypeWithOptions,
} from '@atonix/atx-core';
import { AttributeValue } from './attributeValue';
import { GetAttributeDisplayValue } from './getAttributeDisplayValue';

describe('getAttributeDisplayValue', () => {
  const testAttributeType: IAttributeType = {
    DisplayFormat: 'int',
  } as IAttributeType;
  const testAssetAttribute: IAssetAttribute = {
    Attribute_int: 1,
    AttributeType: testAttributeType,
    DisplayOrder: 2,
  } as IAssetAttribute;
  const testAttributeTypeWithOpts: IAttributeTypeWithOptions = {
    AttributeType: testAttributeType,
  } as IAttributeTypeWithOptions;

  it('should work on a test attribute', () => {
    expect(
      GetAttributeDisplayValue(testAssetAttribute, testAttributeTypeWithOpts)
    ).toEqual({
      value: AttributeValue(testAssetAttribute),
      name: testAttributeTypeWithOpts.AttributeType.AttributeTypeDesc,
      typeString: testAttributeTypeWithOpts.AttributeType.DisplayFormat,
      displayOrder: testAssetAttribute.DisplayOrder,
      isURL: false,
    });
  });
});
