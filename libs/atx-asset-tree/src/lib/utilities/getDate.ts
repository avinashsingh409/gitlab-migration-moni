import { isInvalidDate } from './isInvalidDate';
import isDate from 'lodash/isDate';
import isString from 'lodash/isString';
import moment from 'moment';

export function getDate(val: any): Date {
  if (isDate(val)) {
    return new Date(val.getTime());
  } else if (isString(val)) {
    let result = new Date(val);
    if (isInvalidDate(result)) {
      /**
       * IE has a more limited set of date parsers than other platforms.  I am going to use Moment
       * to try to parse this date so that it doesn't use an invalid format
       * Unfortunately, moment falls back to using the browser date format to turn strings
       * into dates.  We can supply a format here to match what we use, but we would need
       * a standard format throughout the platform.  And we haven't defined that yet.
       * This format matches the toLocaleString format.
       */
      const m = moment(val, 'M/D/YYYY H:m:s a', false);
      if (m.isValid()) {
        result = m.toDate();
      }
    }

    return result;
  } else {
    return new Date(val);
  }
}
