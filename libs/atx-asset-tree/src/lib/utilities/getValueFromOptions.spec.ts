import { IAssetAttribute, IAttributeTypeOption } from '@atonix/atx-core';
import { getValueFromOptions } from './getValueFromOptions';

describe('getValueFromOptions', () => {
  it('should return a good value if the attribute option type IDs match', () => {
    const testAtt: IAssetAttribute = {
      AttributeOptionTypeID: 1,
    } as IAssetAttribute;
    const testOptions: IAttributeTypeOption[] = [
      {
        attributeOptionTypeID: 2,
        attributeOptionTypeDesc: 'incorrectAnswer',
      } as IAttributeTypeOption,
      {
        attributeOptionTypeID: 1,
        attributeOptionTypeDesc: 'correctAnswer',
      } as IAttributeTypeOption,
    ];

    expect(getValueFromOptions(testAtt, testOptions)).toBe('correctAnswer');
  });

  it('should return a blank string if the attribute option type IDs do not match', () => {
    const testAtt: IAssetAttribute = {
      AttributeOptionTypeID: 1,
    } as IAssetAttribute;
    const testOptions: IAttributeTypeOption[] = [
      {
        attributeOptionTypeID: 2,
        attributeOptionTypeDesc: 'incorrectAnswer',
      } as IAttributeTypeOption,
      {
        attributeOptionTypeID: 3,
        attributeOptionTypeDesc: 'another incorrectAnswer',
      } as IAttributeTypeOption,
    ];

    expect(getValueFromOptions(testAtt, testOptions)).toBe('');
  });

  it('should return a blank string if the attribute option type IDs do not match', () => {
    const testAtt: IAssetAttribute = {
      AttributeOptionTypeID: 1,
    } as IAssetAttribute;
    expect(getValueFromOptions(testAtt)).toBe('1');
  });
});
