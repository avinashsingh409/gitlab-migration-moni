import { addEngUnits } from './addEngUnits';
import { getDate } from './getDate';
import isDate from 'lodash/isDate';
import isNil from 'lodash/isNil';
import { isFloatDecimals } from './isFloatDecimals';
import { getValueFromOptions } from './getValueFromOptions';
import { sigFig } from './sigFig';
import { IAssetAttribute, IAttributeTypeOption } from '@atonix/atx-core';

// This is pulled into its own method because we want it to be used throughout the application anywhere there is an attribute.
// We don't want to have to define the functionality everywhere.
// It should be noted that there is a similar function in the back end.
// This doesn't work exactly the same because this is adding in the engineering units as necessary.  The back end one
// is mostly concerned with formatting to get stuff into and out of the database.
export function AttributeValue(
  attribute: IAssetAttribute,
  options?: IAttributeTypeOption[],
  excludeUnits?: boolean
) {
  options = options || attribute.Options;
  excludeUnits = excludeUnits || false;

  let result: string = attribute.Attribute_string;
  if (attribute.AttributeType) {
    const myDispFormat = (
      attribute.AttributeType.DisplayFormat || ''
    ).toLowerCase();

    const decFormat = isFloatDecimals(myDispFormat);
    if (myDispFormat === 'radio') {
      result = getValueFromOptions(attribute, options);
    } else if (myDispFormat === 'boolean') {
      if (isNil(attribute.Attribute_int)) {
        result = null;
      } else {
        result = attribute.Attribute_int === 1 ? 'true' : 'false';
      }
    } else if (myDispFormat === 'int') {
      if (isNil(attribute.Attribute_int)) {
        result = null;
      } else {
        result = String(attribute.Attribute_int);
        result = addEngUnits(result, attribute, excludeUnits);
      }
    } else if (myDispFormat === 'float') {
      if (isNil(attribute.Attribute_float)) {
        result = null;
      } else {
        result = sigFig(attribute.Attribute_float, 4);
        result = addEngUnits(result, attribute, excludeUnits);
      }
    } else if (myDispFormat === 'date') {
      // We need to turn the date field into a valid date if it is not already one.
      // Then we need to format the date in the local time zone and culture.
      if (isNil(attribute.Attribute_date)) {
        result = null;
      } else {
        const d = getDate(attribute.Attribute_date);
        if (isDate(d)) {
          result = d.toLocaleString();
        }
      }
    } else if (decFormat) {
      if (isNil(attribute.Attribute_float)) {
        result = null;
      } else {
        if (myDispFormat.charAt(0) === 'f') {
          result = String(attribute.Attribute_float.toFixed(decFormat));
        } else if (myDispFormat.charAt(0) === 'p') {
          result = String(attribute.Attribute_float.toFixed(decFormat)) + '% ';
        }
        result = addEngUnits(result, attribute, excludeUnits);
      }
    }
  }
  return result;
}
