import { IAssetAttribute } from '@atonix/atx-core';

export function addEngUnits(
  val: string,
  attribute: IAssetAttribute,
  excludeUnits?: boolean
): string {
  if (!excludeUnits && attribute.AttributeType) {
    return val + ' ' + (attribute.AttributeType.EngrUnits || '');
  }
  return val;
}
