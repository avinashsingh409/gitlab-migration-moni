import { AttributeValue } from './attributeValue';

import { getValueFromOptions } from './getValueFromOptions';
import { addEngUnits } from './addEngUnits';
import { sigFig } from './sigFig';
import { getDate } from './getDate';
import { isFloatDecimals } from './isFloatDecimals';
import {
  IAssetAttribute,
  IAttributeType,
  IAttributeTypeOption,
} from '@atonix/atx-core';

describe('AttributeValue', () => {
  it('should return just the attribute_string if attributeType is empty', () => {
    const testAttribute: IAssetAttribute = {
      Attribute_string: 'correctAnswer',
    } as IAssetAttribute;
    expect(AttributeValue(testAttribute)).toEqual('correctAnswer');
  });

  it('should return the getValueFromOptions if the display format is "radio".', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'radio',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_string: 'Unused?',
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    const testOptions: IAttributeTypeOption[] = [
      {
        attributeOptionTypeID: 2,
        attributeOptionTypeDesc: 'incorrectAnswer',
      } as IAttributeTypeOption,
      {
        attributeOptionTypeID: 1,
        attributeOptionTypeDesc: 'correctAnswer',
      } as IAttributeTypeOption,
    ];
    expect(AttributeValue(testAssetAttribute, testOptions)).toEqual(
      getValueFromOptions(testAssetAttribute, testOptions)
    );
  });

  it('should work if the display format is "boolean".', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'boolean',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_int: 1,
      Attribute_string: 'Unused?',
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(AttributeValue(testAssetAttribute)).toEqual('true');
  });

  it('should work if the display format is "int".', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'int',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_int: 1,
      Attribute_string: 'Unused?',
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(AttributeValue(testAssetAttribute)).toEqual(
      addEngUnits('1', testAssetAttribute, false)
    );
  });

  it('should work if the display format is "float".', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'float',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_float: 1.2345,
      Attribute_string: 'Unused?',
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(AttributeValue(testAssetAttribute)).toEqual(
      addEngUnits(
        sigFig(testAssetAttribute.Attribute_float, 4),
        testAssetAttribute,
        false
      )
    );
  });

  it('should work if the display format is "date".', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'date',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_date: new Date('december 12 1990'),
      Attribute_string: 'Unused?',
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(AttributeValue(testAssetAttribute)).toEqual(
      getDate(new Date('december 12 1990')).toLocaleString()
    );
  });

  it('should work if the display format is arbitrary text.', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'arbitrary text',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_string: 'ThisDefault',
      AttributeType: testAttributeType,
    } as IAssetAttribute;

    expect(AttributeValue(testAssetAttribute)).toEqual('ThisDefault');
  });

  it('should work if the display format is blank text.', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: '',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_string: 'ThisDefault',
      AttributeType: testAttributeType,
      Attribute_float: 1.254,
    } as IAssetAttribute;

    expect(AttributeValue(testAssetAttribute)).toEqual('ThisDefault');
  });

  it('should work if the display format is text like "f3"', () => {
    const testAttributeType: IAttributeType = {
      DisplayFormat: 'f3',
    } as IAttributeType;
    const testAssetAttribute: IAssetAttribute = {
      Attribute_string: 'ThisDefault',
      AttributeType: testAttributeType,
      Attribute_float: 12.3524,
    } as IAssetAttribute;

    expect(AttributeValue(testAssetAttribute)).toEqual(
      addEngUnits(
        String(
          testAssetAttribute.Attribute_float.toFixed(
            isFloatDecimals(testAssetAttribute.AttributeType.DisplayFormat)
          )
        ),
        testAssetAttribute,
        false
      )
    );
  });
});
