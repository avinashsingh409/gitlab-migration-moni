export function sigFig(input: number, digits: number): string {
  if (input === null || isNaN(input)) {
    return '';
  } else if (input === 0) {
    return '0';
  } else {
    if (!digits) {
      digits = 4;
    }
    const roundValue = Math.pow(
      10,
      Math.ceil(Math.log(Math.abs(input)) / Math.LN10) - digits
    );
    // roundValue now holds the power of 10 to which we should round numToFormat to preserve the specified number of significant figures.
    return String(
      Math.round(1000000 * (Math.round(input / roundValue) * roundValue)) /
        1000000
    );
  }
}
