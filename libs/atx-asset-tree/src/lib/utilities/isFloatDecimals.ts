export function isFloatDecimals(format: string) {
  if (format) {
    format = format.toLowerCase();
  }
  if (
    format &&
    format.length >= 1 &&
    format !== 'radio' &&
    format !== 'int' &&
    format !== 'float' &&
    (format.charAt(0).toLowerCase() === 'f' || format.charAt(0) === 'p')
  ) {
    const dec = parseInt(format.substring(1), 10);
    if (isNaN(dec)) {
      return 2; // default to 2 decimals if not defined, consistent with other apps
    } else {
      return dec; // returned the assigned decimals
    }
  } else {
    return null;
  }
}
