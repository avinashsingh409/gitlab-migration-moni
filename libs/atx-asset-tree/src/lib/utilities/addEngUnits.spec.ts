import { IAttributeType, IAssetAttribute } from '@atonix/atx-core';
import { addEngUnits } from './addEngUnits';

describe('addEngUnits', () => {
  it('should add the units on a normal input', () => {
    const testAttributeType: IAttributeType = {
      EngrUnits: 'Newtons',
    } as IAttributeType;
    const testAttribute: IAssetAttribute = {
      AttributeType: testAttributeType,
    } as IAssetAttribute;
    expect(addEngUnits('22', testAttribute)).toEqual('22 Newtons');
  });
});
