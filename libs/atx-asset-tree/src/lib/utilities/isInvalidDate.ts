export function isInvalidDate(val: any): boolean {
  return val instanceof Date && isNaN(val as any);
}
