import { IAssetAttribute, IAttributeTypeOption } from '@atonix/atx-core';
import find from 'lodash/find';

export function getValueFromOptions(
  att: IAssetAttribute,
  options?: IAttributeTypeOption[]
) {
  if (options) {
    const op = find(
      options,
      (n) => n.attributeOptionTypeID === att.AttributeOptionTypeID
    );
    const result = op ? op.attributeOptionTypeDesc : '';
    return result;
  }
  return String(att.AttributeOptionTypeID);
}
