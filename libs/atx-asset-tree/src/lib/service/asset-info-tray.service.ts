import { IInfoTrayStateChange } from './../model/info-tray-state-change';
import { IInfoTrayState, IAssetData } from './../model/info-tray-state';

export function createDefaultInfoTray(params?: Partial<IInfoTrayState>) {
  const result: IInfoTrayState = {
    ...{
      showInfoTray: false,
      selectedTab: 'info',
      attributes: [],
      asset: null,
      assetIDs: [],
    },
    ...params,
  };

  return result;
}

export function alterAssetInfoTrayState(
  oldState: IInfoTrayState,
  change: IInfoTrayStateChange
) {
  let newState: IInfoTrayState;

  if (change.event === 'ToggleInfoTray') {
    let newToggleValue = change.newValue as boolean;
    if (newToggleValue === undefined) {
      newToggleValue = !oldState.showInfoTray;
    }

    newState = { ...oldState, showInfoTray: newToggleValue };
  } else if (change.event === 'AssetRetrieved') {
    newState = { ...oldState, asset: change.newValue as IAssetData };
  }

  return newState;
}

export function setInfoTrayToggle(newVal?: boolean) {
  const result: IInfoTrayStateChange = {
    event: 'ToggleInfoTray',
    newValue: newVal,
  };
  return result;
}

export function assetRetrieved(newVal?: IAssetData) {
  const result: IInfoTrayStateChange = {
    event: 'AssetRetrieved',
    newValue: newVal,
  };
  return result;
}

export function getAsset(newVal?: string[]) {
  const result: IInfoTrayStateChange = {
    event: 'GetAsset',
    newValue: newVal,
  };
  return result;
}
