import isUndefined from 'lodash/isUndefined';
import isNil from 'lodash/isNil';

import {
  ITreeConfiguration,
  ITreeNodes,
  ITreeState,
} from '../model/tree-state';
import {
  ITreeStateChange,
  ISelectAsset,
  IExpandNode,
} from '../model/tree-state-change';
import { ITreeNode } from '../model/tree-node';

import {
  createTreeBuilder,
  createTreeBuilderFromModel,
  selectTreeNodes,
  nodeExists,
  expandAndSelectNodes,
  addInteriorNodes,
  setRetrieved,
  expandNode,
  isAnyNodeLoading,
} from './tree-builder.service';
import {
  IAtxAssetTreeNode,
  IAtxTreeRetrieval,
  IAutoCompleteItem,
  IAutoCompleteResult,
  ITreePermissions,
} from '@atonix/atx-core';

// Get the default object to populate the asset tree.  A typical application will have to keep
// a lot more information to drive the tree than what is present in this object.
// This should be just what the tree itself needs to render.
export function getDefaultTree(params?: Partial<ITreeConfiguration>) {
  const result: ITreeConfiguration = {
    ...{
      showTreeSelector: true,
      trees: [],
      selectedTree: null,
      autoCompleteValue: null,
      autoCompletePending: false,
      autoCompleteAssets: [],
      nodes: [],
      pin: false,
      loadingAssets: false,
      loadingTree: false,
      canView: false,
      canAdd: false,
      canEdit: false,
      canDelete: false,
      collapseOthers: true,
      hideConfigureButton: false,
    },
    ...params,
  };

  return result;
}

// Helper function to change the state of the tree based on events that can happen in the tree
// This is just a set of helpers to provide a default behavior.
// Many things will be more dependent on the application than this method.
export function alterAssetTreeState(
  oldState: ITreeState,
  change: ITreeStateChange
) {
  const newState: ITreeState = {
    treeConfiguration: null,
    treeNodes: null,
    hasDefaultSelectedAsset: oldState.hasDefaultSelectedAsset,
  };

  if (change.event === 'GetPermissions') {
    // This is the event when the permissions are requested.  It should set the permissions to
    // some default state.
    newState.treeNodes = { ...oldState.treeNodes };
    newState.treeConfiguration = setPermissions(
      oldState.treeConfiguration,
      null
    );
  } else if (change.event === 'PermissionsRetrieved') {
    // This sets the users permissions.  It will indicate whether the buttons to let the user edit
    // the tree are visible and clickable.
    newState.treeNodes = { ...oldState.treeNodes };
    newState.treeConfiguration = setPermissions(
      oldState.treeConfiguration,
      change.newValue as ITreePermissions
    );
  } else if (change.event === 'LoadTree') {
    // We are loading a tree, need to set the loading flag to true
    // This will also be called when the component is initialized
    newState.treeNodes = createTreeBuilder();
    newState.treeConfiguration = setTreeLoading(
      oldState.treeConfiguration,
      true
    );
    newState.treeConfiguration = {
      ...newState.treeConfiguration,
      selectedTree: change.newValue as string,
    };
  } else if (change.event === 'ReloadAssetTree') {
    newState.treeConfiguration = setTreeLoading(
      oldState.treeConfiguration,
      true
    );
    newState.treeNodes = { ...oldState.treeNodes };
  } else if (change.event === 'SelectAsset') {
    // This indicates the user selected a node.  This will either be a node that already exists,
    // in which case we want to expand down to that node and select it.  Or it will be a node that doesn't
    // exist, in which case we want to treat this the same way as a LoadTree event because it will
    // load the entire tree down to the selected node.
    const p = change.newValue as ISelectAsset;
    if (nodeExists(p.ids, oldState.treeNodes)) {
      newState.treeNodes = expandAndSelectNodes(
        p.ids,
        oldState.treeNodes,
        p.multiSelect,
        oldState.treeConfiguration.collapseOthers
      );
      newState.treeConfiguration = setNodes(
        oldState.treeConfiguration,
        newState.treeNodes
      );
    } else {
      newState.treeConfiguration = setTreeLoading(
        oldState.treeConfiguration,
        true
      );
      newState.treeNodes = { ...oldState.treeNodes };
    }
  } else if (change.event === 'TreeRetrieved') {
    // This sets the trees available for the user to select.  It also selects a single
    // tree to be the selected tree.  The data will contain an initial list of nodes for the
    // selected tree.  Those might be the top level or many more than the top level.  They should
    // be loaded and displayed as well.
    let r = change.newValue as IAtxTreeRetrieval;
    if (
      oldState.hasDefaultSelectedAsset &&
      !r.selectedNode &&
      r.nodes.length > 0
    ) {
      r = { ...r, selectedNode: r.nodes[0].UniqueKey };
    }
    newState.treeNodes = createTreeBuilderFromModel(r);
    newState.treeConfiguration = setTrees(oldState.treeConfiguration, r);
    newState.treeConfiguration = setTreeLoading(
      newState.treeConfiguration,
      false
    );
    newState.treeConfiguration = setNodes(
      newState.treeConfiguration,
      newState.treeNodes
    );
  } else if (change.event === 'IconClicked') {
    // The user clicked the icon next to a node, so it needs to expand if it can expand.
    newState.treeNodes = expandNode(
      (change.newValue as IAtxAssetTreeNode).UniqueKey,
      oldState.treeNodes
    );
    newState.treeConfiguration = setNodes(
      oldState.treeConfiguration,
      newState.treeNodes
    );
    newState.treeConfiguration = setAssetsLoading(
      newState.treeConfiguration,
      isAnyNodeLoading(newState.treeNodes)
    );
  } else if (change.event === 'NodesRetrieved') {
    // New nodes are available, so the nodes need to be added into the node pool and
    // the tree needs to be expanded.
    const expandAsset = change.newValue as IExpandNode;
    newState.treeNodes = addInteriorNodes(
      expandAsset.nodes,
      oldState.treeNodes
    );
    newState.treeNodes = setRetrieved(expandAsset.id, newState.treeNodes);
    newState.treeNodes = expandNode(
      expandAsset.id,
      newState.treeNodes,
      'expanded'
    );
    newState.treeConfiguration = setNodes(
      oldState.treeConfiguration,
      newState.treeNodes
    );
    newState.treeConfiguration = setAssetsLoading(
      newState.treeConfiguration,
      isAnyNodeLoading(newState.treeNodes)
    );
  } else if (change.event === 'AutoCompleteChanged') {
    // This happens when the auto complete value is changed.  It simply updates the
    // string filter value.  If the new value is null it will set pending to off.
    // Otherwise it expects the AutoCompleteChangedComplete event to turn off the
    // pending flag.
    newState.treeConfiguration = {
      ...oldState.treeConfiguration,
      autoCompleteAssets: findAssetsByName(
        oldState,
        change.newValue as string,
        5
      ),
      autoCompletePending: !isNil(change.newValue),
      autoCompleteValue: change.newValue as string,
    };
    newState.treeNodes = { ...oldState.treeNodes };
  } else if (change.event === 'AutoCompleteChangedRetrieved') {
    // The autocomplete nodes have been retrieved from the server and they need to be added
    // into the configuration.
    const autoCompleteResult = change.newValue as IAutoCompleteResult;
    let autoCompletePendingSearchComplete =
      oldState.treeConfiguration.autoCompletePending;
    let autoCompleteAssetsSearchComplete =
      oldState.treeConfiguration.autoCompleteAssets;
    if (
      autoCompleteResult.search ===
        oldState.treeConfiguration.autoCompleteValue &&
      autoCompleteResult.treeID === oldState.treeConfiguration.selectedTree
    ) {
      autoCompletePendingSearchComplete = false;
      autoCompleteAssetsSearchComplete = autoCompleteResult.items;
    }
    newState.treeConfiguration = {
      ...oldState.treeConfiguration,
      autoCompletePending: autoCompletePendingSearchComplete,
      autoCompleteAssets: [...autoCompleteAssetsSearchComplete],
    };
    newState.treeNodes = { ...oldState.treeNodes };
  } else if (change.event === 'PinToggle') {
    // Set the pinned state of the asset tree.  This will affect the visual of the pin
    // on the asset tree to determine if it shows a pin unpinned or a pin pinned.
    let newPinVal: boolean;
    if (isUndefined(change.newValue)) {
      newPinVal = !oldState.treeConfiguration.pin;
    } else {
      newPinVal = change.newValue as boolean;
    }
    newState.treeConfiguration = {
      ...oldState.treeConfiguration,
      pin: newPinVal,
    };
    newState.treeNodes = { ...oldState.treeNodes };
  } else {
    // Base case that does nothing in the event that this method doesn't know what to do.
    newState.treeConfiguration = { ...oldState.treeConfiguration };
    newState.treeNodes = { ...oldState.treeNodes };
  }
  return newState;
}

export function setPermissions(
  state: ITreeConfiguration,
  permissions: ITreePermissions
): ITreeConfiguration {
  let newState: ITreeConfiguration;
  if (permissions) {
    newState = {
      ...state,
      canAdd: permissions.canAdd,
      canDelete: permissions.canDelete,
      canEdit: permissions.canEdit,
      canView: permissions.canView,
    };
  } else {
    newState = {
      ...state,
      canAdd: false,
      canDelete: false,
      canEdit: false,
      canView: true,
    };
  }

  return newState;
}

export function setTreeLoading(
  state: ITreeConfiguration,
  loading: boolean
): ITreeConfiguration {
  return { ...state, loadingTree: loading, nodes: [] };
}

export function setAssetsLoading(
  state: ITreeConfiguration,
  loading: boolean
): ITreeConfiguration {
  return { ...state, loadingAssets: loading };
}

export function setTrees(state: ITreeConfiguration, trees: IAtxTreeRetrieval) {
  const newState = {
    ...state,
    selectedTree: trees.tree,
    trees: trees.assetTrees.map((t) => {
      return {
        name: t.TreeName,
        id: t.TreeId,
        isUserOwned: t.IsUserOwned,
        isDefaultTree: t.IsDefaultTree,
        customerId: t.CustomerId,
      };
    }),
  };
  return newState;
}

export function setNodes(
  state: ITreeConfiguration,
  assets: ITreeNodes
): ITreeConfiguration {
  return { ...state, nodes: selectTreeNodes(assets) };
}

export function getPermissions() {
  const result: ITreeStateChange = {
    event: 'GetPermissions',
  };
  return result;
}

// Create a message to indicate the asset tree permissions have been retrieved.
export function permissionsChanged(value: ITreePermissions) {
  const result: ITreeStateChange = {
    event: 'PermissionsRetrieved',
    newValue: value,
  };
  return result;
}

// Create a tree selected event message
export function selectTree(id: string) {
  const result: ITreeStateChange = {
    event: 'LoadTree',
    newValue: id,
  };
  return result;
}

export function treeRetrieved(newValue: IAtxTreeRetrieval) {
  const result: ITreeStateChange = {
    event: 'TreeRetrieved',
    newValue,
  };
  return result;
}

export function selectAsset(ids: string[] | string, multiSelect?: boolean) {
  const result: ITreeStateChange = {
    event: 'SelectAsset',
    newValue: { ids, multiSelect: multiSelect || false } as ISelectAsset,
  };
  return result;
}

// Create a message to indicate an asset tree node has been expanded/collapsed
export function clickIcon(value: ITreeNode) {
  const result: ITreeStateChange = {
    event: 'IconClicked',
    newValue: value ? value.data : null,
  };
  return result;
}

export function nodesRetrieved(id: string, nodes: ITreeNode[]) {
  const result: ITreeStateChange = {
    event: 'NodesRetrieved',
    newValue: { id, nodes },
  };
  return result;
}

export function toggleReload() {
  const result: ITreeStateChange = {
    event: 'ReloadAssetTree',
  };
  return result;
}

// Create a message indicating whether the pin has been toggled.
export function togglePin(newVal?: boolean) {
  const result: ITreeStateChange = {
    event: 'PinToggle',
    newValue: newVal,
  };
  return result;
}

export function toggleEdit(newVal?: boolean) {
  const result: ITreeStateChange = {
    event: 'EditAssetTree',
    newValue: newVal,
  };
  return result;
}

export function autocompleteChanged(value?: string) {
  const result: ITreeStateChange = {
    event: 'AutoCompleteChanged',
    newValue: value,
  };
  return result;
}

export function autocompleteChangedRetrieved(
  newValues: IAutoCompleteItem[],
  search: string,
  treeID: string
) {
  const newValue: IAutoCompleteResult = {
    items: newValues,
    search,
    treeID,
  };
  const result: ITreeStateChange = {
    event: 'AutoCompleteChangedRetrieved',
    newValue,
  };
  return result;
}

export function findAssetsByName(
  state: ITreeState,
  searchString: string,
  maxItems: number
) {
  const result: IAutoCompleteItem[] = [];
  if (searchString) {
    const searchStringLower = searchString.toLowerCase();

    for (const id of state.treeNodes.ids) {
      const node = state.treeNodes.entities[id];
      if (node.nodeAbbrev.toLowerCase().includes(searchStringLower)) {
        result.push({
          UniqueID: node.uniqueKey,
          Display: node.nodeAbbrev,
        });
      }
      if (result.length >= maxItems) {
        break;
      }
    }
  }

  return result;
}

export function getAssetByAssetId(state: ITreeState, assetId: number) {
  let data = null;
  if (assetId) {
    for (const id of state.treeNodes.ids) {
      const node = state.treeNodes.entities[id];
      if (node?.data?.AssetId === assetId) {
        data = node.data;
        return data;
      }
    }
  }

  return data;
}

export function treeRetrievedStateChange(payload: ITreeStateChange) {
  const payloadValue = { ...payload };
  if (
    payloadValue &&
    payloadValue.newValue &&
    payloadValue.event === 'TreeRetrieved'
  ) {
    const newValue = { ...(payloadValue.newValue as IAtxTreeRetrieval) };
    if (newValue?.selectedNode) {
      const assetGuid = newValue.selectedNode;
      const newSelectedNode = newValue.nodes.find(
        (node) => node.AssetGuid?.toLowerCase() === assetGuid.toLowerCase()
      );
      if (newSelectedNode) {
        newValue.selectedNode = newSelectedNode.UniqueKey;
        payloadValue.newValue = newValue;
      }
    }
  }
  return payloadValue;
}
