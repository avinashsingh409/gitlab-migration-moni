import isNil from 'lodash/isNil';
import uniq from 'lodash/uniq';
import isUndefined from 'lodash/isUndefined';
import some from 'lodash/some';

import { EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import {
  Comparer,
  Dictionary,
  Update,
  UpdateStr,
} from '@ngrx/entity/src/models';

import { ITreeNode } from '../model/tree-node';
import { ITreeNodes } from '../model/tree-state';
import { IAtxAssetTreeNode, IAtxTreeRetrieval } from '@atonix/atx-core';

// The function that will be used to sort nodes.
export const compareAssetTreeNodes: Comparer<ITreeNode> = (a, b) => {
  if (a.displayOrder === b.displayOrder) {
    return a.nodeAbbrev.localeCompare(b.nodeAbbrev, undefined, {
      usage: 'sort',
      sensitivity: 'accent',
    });
  } else {
    // when the answer is positive, object b takes precedence.
    // Returning negative will do the opposite.
    return a.displayOrder - b.displayOrder;
  }
};

// Translates a node object from the server into a node object for the store.
// The server may return assets or adhoc nodes with no asset.  In the future
// the server may return other types that need to be represented in the tree.
// This method will allow us to turn all of those things into asset tree
// nodes in one method.
export function createNode(fromServer: IAtxAssetTreeNode) {
  return {
    uniqueKey: fromServer.UniqueKey,
    parentUniqueKey: fromServer.ParentUniqueKey,
    nodeAbbrev: fromServer.NodeAbbrev,
    displayOrder: fromServer.DisplayOrder,
    level: 0,
    retrieved: !fromServer.HasChildren,
    selected: false,
    symbol: fromServer.HasChildren ? 'collapsed' : 'nothing',
    data: fromServer,
  } as ITreeNode;
}

// This is the set of helper functions that will allow easy manipulation of the master list of assets.
// This should be used to create a new copy of the state with some parameters changed.
export const assetTreeAdapter: EntityAdapter<ITreeNode> =
  createEntityAdapter<ITreeNode>({
    sortComparer: compareAssetTreeNodes,
    selectId: (a) => a.uniqueKey,
  });

// This will take the data from the return of the call to the web service and turn it into a
// tree object.  It is intended to be used in a pipe/map on the return of the
// web service method.
export function createTreeBuilderFromModel(modelFromServer: IAtxTreeRetrieval) {
  const allNodesForNewTree = modelFromServer.nodes.map((n) => createNode(n));
  let nodes = createTreeBuilder(modelFromServer.tree);
  nodes = addNodes(allNodesForNewTree, nodes);
  nodes = expandAndSelectNodes(modelFromServer.selectedNode, nodes);
  nodes = setRetrievedNodes(nodes);
  return nodes;
}

// Initialize a new tree builder.  If the root nodes are present they will
// be added to the list of assets and to the list of root assets.
export function createTreeBuilder(treeID?: string, appContext?: string) {
  const result: ITreeNodes = assetTreeAdapter.getInitialState({
    rootAssets: new Array<string>(),
    childrenDict: {} as Dictionary<string[]>,
    treeID: treeID || null,
    appContext: appContext || null,
  });
  return result;
}

export function addNodes(allNodes: ITreeNode[] | ITreeNode, state: ITreeNodes) {
  // Make sure we are dealing with an array and not a single entity
  let nodesArray = Array.isArray(allNodes)
    ? (allNodes as ITreeNode[])
    : [allNodes as ITreeNode];

  // Filter out any nodes that already exist
  nodesArray = nodesArray.filter((node) => !state.entities[node.uniqueKey]);

  // Figure out which nodes are root and which are interior, and add them appropriately
  const rootAndInteriorNodes = getRootAndInteriorNodes(nodesArray, state);
  state = addRootNodes(rootAndInteriorNodes.rootNodes, state);
  state = addInteriorNodes(rootAndInteriorNodes.interiorNodes, state);

  return state;
}

// Internal function to figure out which nodes are interior and which are root.
// The root nodes need to be added differently, since they don't need to be added to their parents
// array.
function getRootAndInteriorNodes(allNodes: ITreeNode[], state: ITreeNodes) {
  const nodesDict: { [key: string]: ITreeNode } = {};
  for (const n of allNodes) {
    nodesDict[n.uniqueKey] = n;
  }
  for (const k of state.ids) {
    if (!nodesDict[k]) {
      nodesDict[k] = state.entities[k];
    }
  }

  const result: { rootNodes: ITreeNode[]; interiorNodes: ITreeNode[] } = {
    rootNodes: [],
    interiorNodes: [],
  };
  for (const n of allNodes) {
    if (nodesDict[n.parentUniqueKey]) {
      result.interiorNodes.push(n);
    } else {
      result.rootNodes.push(n);
    }
  }

  return result;
}

// Add new root nodes to the tree.  This will add nodes after the tree is initialized.
export function addRootNodes(rootNodes: ITreeNode[], state: ITreeNodes) {
  // Grab a list of all root nodes with these added
  const allRootAssets = [
    ...rootNodes,
    ...state.rootAssets.map((n) => state.entities[n]),
  ];

  // Sort them so they are easier to retrieve later
  allRootAssets.sort(compareAssetTreeNodes);

  // Merge the new data into the existing state
  return {
    ...assetTreeAdapter.addMany(rootNodes, state),
    rootAssets: [...allRootAssets.map((n) => n.uniqueKey)],
  };
}

// Add nodes to the tree
export function addInteriorNodes(
  newNodes: ITreeNode[] | ITreeNode,
  state: ITreeNodes
) {
  const nodesArray = Array.isArray(newNodes)
    ? (newNodes as ITreeNode[])
    : [newNodes as ITreeNode];
  let result = assetTreeAdapter.addMany(nodesArray, state);
  result = addToChildDictionary(nodesArray, result);
  return result;
}

// Used to add new assets into the asset state.  This will
// update the parents dictionary and sort everything appropriately.
// This should not be called from the outside.  It should only be called from the addNodes methods.
function addToChildDictionary(newNode: ITreeNode[], state: ITreeNodes) {
  // Grab the children dictionary
  const result = { ...state.childrenDict };

  // Grab the list of parents in the nodes to add.
  const parents = uniq(newNode.map((n) => n.parentUniqueKey));

  // Make sure each parent has a list of assets to add children to.
  for (const pToInit of parents) {
    if (isUndefined(result[pToInit])) {
      result[pToInit] = [];
    }
  }

  // Add each asset to its parent's dictionary
  for (const n of newNode) {
    result[n.parentUniqueKey].push(n.uniqueKey);
  }

  // Make sure each list is sorted correctly.  This will speed up the process of
  // deriving the list of assets to display.
  for (const pToSort of parents) {
    result[pToSort] = uniq(result[pToSort].map((n) => state.entities[n]))
      .sort(compareAssetTreeNodes)
      .map((n) => n.uniqueKey);
  }

  return { ...state, childrenDict: result };
}

// Update the contents of a single node.  The Update<IAssetTreeNode> object in the
// update parameter requires an id field, which is th eunique key of the asset,
// and it requires a list of changes, which is a partial object with the things that need
// to be changed on that asset set.  Anything not in the updates parameter will not be changed.
export function updateNode(update: Update<ITreeNode>, state: ITreeNodes) {
  return assetTreeAdapter.updateOne(update, state);
}

export function setOrToggleSelectedNodes(
  ids: string[] | string,
  multiSelect: boolean,
  state: ITreeNodes
) {
  if (multiSelect) {
    // If multiselect is set we have to assume the nodes already exist in the tree
    state = toggleSelectedNodes(ids, state);
  } else {
    state = setSelectedNodes(ids, state);
  }
  return state;
}

// This sets selected nodes to unselected and unselected nodes to selected.
// This will be the typical behavior if the user holds the control key when
// selecting nodes.
function toggleSelectedNodes(ids: string[] | string, state: ITreeNodes) {
  // Short circuit if the ids is null
  if (isNil(state) || isNil(ids) || ids.length <= 0) {
    return state;
  }

  let assetKeys: string[];

  if (Array.isArray(ids)) {
    assetKeys = ids as string[];
  } else {
    assetKeys = [ids as string];
  }

  const updates: Update<ITreeNode>[] = [];
  for (const assetKey of assetKeys) {
    updates.push({
      id: assetKey,
      changes: {
        selected: !state.entities[assetKey].selected,
      },
    });
  }

  return assetTreeAdapter.updateMany(updates, state);
}

// Set the nodes with the specified IDs to selected
function setSelectedNodes(ids: string[] | string, state: ITreeNodes) {
  const keys: { [key: string]: boolean } = {};

  if (!Array.isArray(ids)) {
    keys[ids as string] = true;
  } else {
    for (const id of ids) {
      keys[id] = true;
    }
  }

  const updates: Update<ITreeNode>[] = [];
  for (const uniqueKey of state.ids as string[]) {
    updates.push({
      id: uniqueKey,
      changes: {
        selected: keys[uniqueKey] ? true : false,
      },
    });
  }

  return assetTreeAdapter.updateMany(updates, state);
}

// Retrieves all selected nodes.  If no nodes are selected
// will return an empty array.
export function getSelectedNodes(state: ITreeNodes) {
  const result: ITreeNode[] = [];
  if (state && state.ids) {
    for (const uniqueKey of state.ids as string[]) {
      if (state.entities[uniqueKey].selected) {
        result.push({ ...state.entities[uniqueKey] });
      }
    }
  }

  return result;
}

// Retrieves a single node that is marked as selected.
// This will not retrieve multiple selected nodes, only one.
export function getSelectedNode(state: ITreeNodes) {
  const result = (state.ids as string[]).find((n) => {
    return state.entities[n].selected;
  });
  if (result) {
    return { ...state.entities[result] };
  }
  return null;
}

// This will change a node from collapsed to expanded in the state.  If a new state value is passed in
// it will also set the specific expanded value of the asset.
// If you try to expand a node that does not have an entry in the children dictionary it will be changed to the loading state.
export function expandNode(
  nodeID: string,
  state: ITreeNodes,
  newState?: 'loading' | 'collapsed' | 'expanded'
) {
  if (state.entities[nodeID]) {
    const node = state.entities[nodeID];
    let newSymbol = node.symbol;

    if (!isUndefined(newState)) {
      newSymbol = newState;
    } else if (!isRetrieved(nodeID, state)) {
      newSymbol = 'loading';
    } else if (node.symbol === 'collapsed') {
      newSymbol = 'expanded';
    } else if (node.symbol === 'expanded') {
      newSymbol = 'collapsed';
    }
    // There is an unlisted choice where the item is symbol of nothing and it is retrived.
    // In that situation nothing should change.

    const update: Update<ITreeNode> = {
      id: nodeID,
      changes: { symbol: newSymbol },
    };
    return updateNode(update, state);
  }
}

// Helper function to determine if an asset has its children retrieved from the server yet.
export function isRetrieved(nodeID: string, state: ITreeNodes) {
  let result = false;
  if (state.entities[nodeID]) {
    result = state.entities[nodeID].retrieved;
  }
  return result;
}

export function setRetrieved(nodeID: string, state: ITreeNodes) {
  return updateNode({ id: nodeID, changes: { retrieved: true } }, state);
}

// Get the selectors defined for this adapter.
export function getSelectors() {
  return assetTreeAdapter.getSelectors();
}

// Return the list of nodes under the specific parent ID.
export function getChildren(state: ITreeNodes, parent: string) {
  let result: ITreeNode[] = [];
  if (state.childrenDict[parent]) {
    result = state.childrenDict[parent].map((n) => state.entities[n]);
  }
  return result;
}

export function isAnyNodeLoading(state: ITreeNodes) {
  return (
    state &&
    state.ids &&
    some(state.ids as string[], (id: string) => {
      return state.entities[id].symbol === 'loading';
    })
  );
}

// Helper to derive the list of nodes to display from the state with
// all asset tree nodes.
export function selectTreeNodes(state: ITreeNodes): ITreeNode[] {
  const result: ITreeNode[] = [];
  if (state) {
    const stack = state.rootAssets
      .map((n) => {
        return { ...state.entities[n], level: 0 };
      })
      .reverse();

    let parentNode: ITreeNode = null;
    let childrenIDs: Array<string> = null;
    while (stack.length > 0) {
      parentNode = stack.pop();
      result.push(parentNode);
      if (parentNode.symbol === 'expanded') {
        childrenIDs = (state.childrenDict[parentNode.uniqueKey] || [])
          .slice()
          .reverse();
        for (const c of childrenIDs) {
          stack.push({ ...state.entities[c], level: parentNode.level + 1 });
        }
      }
    }
  }
  return result;
}

// This should let us know if a node exists.  If some node doesn't exist this will return false.
export function nodeExists(nodeID: string[] | string, state: ITreeNodes) {
  let result = false;
  if (nodeID && state && Object.keys(state).length !== 0) {
    if (Array.isArray(nodeID)) {
      result = !(nodeID as string[]).some((n) =>
        state.entities[n] ? false : true
      );
    } else {
      result = state.entities[nodeID as string] ? true : false;
    }
  }
  return result;
}

// This will put the tree in a state where the parents down to a specific node are expanded
// and nothing else is expanded.  The decendants of the selected node and the nodes that are not
// in the path should not be expanded.  If a node does not have any children it should be considered nothing
// rather than expanded or collapsed, so we can't just set everything to collapsed.
// If the selected node is null, or it doesn't appear in the tree we should collapse everything that can be collapsed.
export function expandToNode(
  nodeID: string[] | string,
  state: ITreeNodes,
  collapseOthers?: boolean
) {
  const objectsToExpand: { [key: string]: boolean } = {};

  if (nodeExists(nodeID, state)) {
    const nodeIDs = Array.isArray(nodeID) ? nodeID : [nodeID];

    // This will grab a dictionary of all the nodes that are ancestors of the selected node.
    // If there is no selected node then there are no ancestors.
    let myParents: string[] = nodeIDs.map(
      (n) => state.entities[n].parentUniqueKey
    );
    while (myParents && myParents.length > 0) {
      for (const p of myParents) {
        objectsToExpand[p] = true;
      }

      myParents = myParents
        .map((n) =>
          state.entities[n] ? state.entities[n].parentUniqueKey : null
        )
        .filter((n) => n);
    }
  }

  const updates: Update<ITreeNode>[] = [];
  for (const uniqueKey of state.ids as string[]) {
    // If the node is in the path to the selected node then expand it.
    if (objectsToExpand[uniqueKey]) {
      updates.push({
        id: uniqueKey,
        changes: {
          retrieved: true,
          symbol: 'expanded',
        },
      });
    } else if (
      state.entities[uniqueKey].symbol === 'expanded' &&
      collapseOthers
    ) {
      // If the node is not in the path to the selected node and it is expanded then collapse it.
      // We can't just set everything to collapsed because nodes that have no decendants should not be changed.
      updates.push({
        id: uniqueKey,
        changes: {
          symbol: 'collapsed',
        },
      });
    }
  }

  return assetTreeAdapter.updateMany(updates, state);
}

// Basically calls the expand and select together.  This will only work with a single selected node
// as expanding to a bunch of nodes doesn't make sense.  It simply calls expand and select.
export function expandAndSelectNodes(
  nodeID: string[] | string,
  assets: ITreeNodes,
  multiselect?: boolean,
  collapseOthers?: boolean
) {
  if (nodeID) {
    assets = expandToNode(nodeID, assets, collapseOthers);
    assets = setOrToggleSelectedNodes(nodeID, multiselect, assets);
  }
  return assets;
}

export function setRetrievedNodes(state: ITreeNodes) {
  const updates: UpdateStr<ITreeNode>[] = [];
  const ids = new Set(
    (state.ids as string[]).map((id) => state.entities[id].parentUniqueKey)
  );
  for (const id of ids) {
    if (id) {
      updates.push({ id, changes: { retrieved: true } });
    }
  }
  return assetTreeAdapter.updateMany(updates, state);
}

// Helper function that will grab a single string from what could be one or more strings.
export function getIDFromArrayOfIDs(ids: string | string[]) {
  let result: string = null;
  if (Array.isArray(ids)) {
    if (ids.length > 0) {
      result = ids[0];
    }
  } else {
    result = ids as string;
  }
  return result;
}

// Grabs a single string from the set of selected assets.  If there is more than one
// asset selected this will return the first one.
export function getIDFromSelectedAssets(
  selectedAssets: ITreeNode | ITreeNode[]
) {
  let id: string = null;
  if (selectedAssets) {
    if (Array.isArray(selectedAssets)) {
      if (selectedAssets.length > 0) {
        id = selectedAssets[0].uniqueKey;
      }
    } else {
      id = selectedAssets.uniqueKey;
    }
  }

  return id;
}

export function createNewNode(newData: Partial<ITreeNode>) {
  return {
    ...{
      uniqueKey: null,
      parentUniqueKey: null,
      nodeAbbrev: null,
      level: 0,
      selected: true,
      retrieved: false,
      symbol: null,
      displayOrder: 0,
    },
    ...newData,
  } as ITreeNode;
}
