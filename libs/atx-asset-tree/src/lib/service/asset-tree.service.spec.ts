import { ITreeState, ITreeConfiguration } from '../model/tree-state';
import { ITreeNode } from '../model/tree-node';
import { ITreeStateChange, ISelectAsset } from '../model/tree-state-change';
import {
  createNewNode,
  createTreeBuilder,
  createTreeBuilderFromModel,
  getSelectedNodes,
  addRootNodes,
  addNodes,
  setRetrieved,
} from './tree-builder.service';
import {
  getDefaultTree,
  alterAssetTreeState,
  togglePin,
  selectTree,
  permissionsChanged,
  clickIcon,
  selectAsset,
  getPermissions,
  treeRetrieved,
  nodesRetrieved,
  autocompleteChanged,
  autocompleteChangedRetrieved,
} from './asset-tree.service';
import { IAtxTreeRetrieval, ITreePermissions } from '@atonix/atx-core';

describe('AssetTreeService', () => {
  it('should get default of asset tree state', () => {
    const expectedOutput: ITreeConfiguration = {
      showTreeSelector: true,
      trees: [],
      selectedTree: null,
      autoCompleteValue: null,
      autoCompletePending: false,
      autoCompleteAssets: [],
      nodes: [],
      pin: false,
      loadingAssets: false,
      loadingTree: false,
      canView: false,
      canAdd: false,
      canEdit: false,
      canDelete: false,
      collapseOthers: true,
      hideConfigureButton: false,
    };

    expect(getDefaultTree()).toEqual(expectedOutput);
  });

  it('should apply state changes to alter the state', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      showTreeSelector: true,
      trees: [
        {
          name: 'Default Tree',
          id: 'default',
        },
      ],
      selectedTree: null,
      autoCompleteValue: null,
      autoCompletePending: false,
      autoCompleteAssets: [],
      nodes: [],
      pin: true,
      loadingAssets: true,
      loadingTree: true,
      canView: true,
      canAdd: true,
      canEdit: true,
      canDelete: true,
      collapseOthers: true,
      hideConfigureButton: false,
    });
    const nodes = createTreeBuilder();

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const permissions: ITreePermissions = {
      canView: false,
      canAdd: true,
      canEdit: false,
      canDelete: true,
    };

    const node: ITreeNode[] = [
      createNewNode({
        uniqueKey: 'a',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 1',
        level: 1,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
    ];

    // pin
    expect(alterAssetTreeState(state, togglePin())).toEqual({
      ...state,
      treeConfiguration: { ...state.treeConfiguration, pin: false },
    });

    // selected tree
    expect(alterAssetTreeState(state, selectTree('default'))).toEqual({
      ...state,
      treeConfiguration: {
        ...state.treeConfiguration,
        selectedTree: 'default',
      },
    });

    // permissions
    expect(alterAssetTreeState(state, permissionsChanged(permissions))).toEqual(
      {
        ...state,
        treeConfiguration: {
          ...state.treeConfiguration,
          canView: false,
          canAdd: true,
          canEdit: false,
          canDelete: true,
        },
      }
    );
  });

  it('should apply default permissions', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      canAdd: true,
      canEdit: true,
      canDelete: true,
      canView: false,
    });
    const nodes = createTreeBuilder();

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const newState = alterAssetTreeState(state, getPermissions());
    expect(newState.treeConfiguration.canAdd).toEqual(false);
    expect(newState.treeConfiguration.canDelete).toEqual(false);
    expect(newState.treeConfiguration.canEdit).toEqual(false);
    expect(newState.treeConfiguration.canView).toEqual(true);
  });

  it('should return initialized asset tree state change', () => {
    const expectedOutput: ITreeStateChange = {
      event: 'SelectAsset',
      newValue: { ids: null, multiSelect: false } as ISelectAsset,
    };
    expect(selectAsset(null)).toEqual(expectedOutput);
  });

  it('should set the trees when they are returned from the server', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree();
    const nodes = createTreeBuilder();

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const retrieval: IAtxTreeRetrieval = {
      assetTrees: [
        {
          TreeId: 'treea',
          TreeName: 'treea',
          IsDefaultTree: true,
          IsUserOwned: false,
          CustomerId: '',
        },
      ],
      nodes: [
        {
          NodeId: 'nodea',
          TreeId: 'treea',
          NodeAbbrev: 'a',
          NodeDesc: 'a',
          NodeTypeId: 1,
          DisplayOrder: 1,
          AssetId: 1,
          AssetNodeBehaviorId: 1,
          ParentUniqueKey: null,
          UniqueKey: 'nodea',
          AssetGuid: 'asset',
          ReferencedBy: null,
          HasChildren: false,
        },
      ],
      tree: 'treea',
      selectedNode: 'nodea',
    };
    const newState = alterAssetTreeState(state, treeRetrieved(retrieval));
    expect(newState.treeConfiguration.loadingTree).toBe(false);
    expect(newState.treeConfiguration.trees.length).toEqual(1);
    expect(newState.treeConfiguration.nodes.length).toEqual(1);
    const selectedNodes = getSelectedNodes(newState.treeNodes);
    expect(selectedNodes.length).toEqual(1);
  });

  it('should select an asset that doesnt exist', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree();
    const nodes = createTreeBuilder();

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const newState = alterAssetTreeState(state, selectAsset('id1'));
    expect(newState.treeConfiguration.loadingTree).toBe(true);
  });

  it('should select an asset that exists', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree();
    const nodes = createTreeBuilderFromModel({
      assetTrees: [
        {
          TreeId: 'treea',
          TreeName: 'treea',
          IsDefaultTree: true,
          IsUserOwned: false,
          CustomerId: '',
        },
      ],
      nodes: [
        {
          NodeId: 'nodea',
          TreeId: 'treea',
          NodeAbbrev: 'a',
          NodeDesc: 'a',
          NodeTypeId: 1,
          DisplayOrder: 1,
          AssetId: 1,
          AssetNodeBehaviorId: 1,
          ParentUniqueKey: null,
          UniqueKey: 'nodea',
          AssetGuid: 'asset',
          ReferencedBy: null,
          HasChildren: false,
        },
      ],
      tree: 'treea',
      selectedNode: 'nodea',
    });

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const newState = alterAssetTreeState(state, selectAsset('nodea'));
    expect(newState.treeConfiguration.loadingTree).toBe(false);
    expect(newState.treeConfiguration.nodes.length).toEqual(1);
    const selectedNodes = getSelectedNodes(newState.treeNodes);
    expect(selectedNodes.length).toEqual(1);
    expect(selectedNodes[0].uniqueKey).toEqual('nodea');
  });

  it('should return selected tree asset tree state change', () => {
    const expectedOutput: ITreeStateChange = {
      event: 'LoadTree',
      newValue: 'testTree',
    };
    expect(selectTree('testTree')).toEqual(expectedOutput);
  });

  it('should return toggle pin asset tree state change', () => {
    const expectedOutput: ITreeStateChange = {
      event: 'PinToggle',
      newValue: false,
    };
    expect(togglePin(false)).toEqual(expectedOutput);
  });

  it('should return click icon asset tree state change', () => {
    const node: ITreeNode = createNewNode({
      uniqueKey: 'b',
      parentUniqueKey: null,
      nodeAbbrev: 'Asset B',
      level: 1,
      selected: true,
      symbol: 'expanded',
      displayOrder: 1,
      data: {
        NodeId: 'nodeb',
        TreeId: 'treea',
        NodeAbbrev: 'Asset B',
        NodeDesc: 'Asset B',
        NodeTypeId: 1,
        DisplayOrder: 1,
        AssetId: 1,
        AssetNodeBehaviorId: 1,
        ParentUniqueKey: null,
        UniqueKey: 'b',
        AssetGuid: 'asset',
        ReferencedBy: null,
        HasChildren: false,
      },
    });

    const expectedOutput: ITreeStateChange = {
      event: 'IconClicked',
      newValue: node.data,
    };
    expect(clickIcon(node)).toEqual(expectedOutput);
  });

  it('should return change permission asset tree state change', () => {
    const permissions: ITreePermissions = {
      canView: true,
      canAdd: true,
      canEdit: false,
      canDelete: false,
    };

    const expectedOutput: ITreeStateChange = {
      event: 'PermissionsRetrieved',
      newValue: permissions,
    };
    expect(permissionsChanged(permissions)).toEqual(expectedOutput);
  });

  it('should alter state when the icon is clicked on a node with available children', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
      data: {
        NodeId: 'a',
        TreeId: 'a123',
        NodeAbbrev: 'a',
        NodeDesc: 'a',
        ParentNodeId: null,
        NodeTypeId: 1,
        DisplayOrder: 1,
        AssetId: 123456,
        AssetNodeBehaviorId: 1234,
        Asset: null,
        ParentUniqueKey: null,
        UniqueKey: 'nodea',
        AssetGuid: 'qwertyuiop123456789',
        ReferencedBy: null,
        HasChildren: true,
      },
    });
    const nodeb = createNewNode({
      uniqueKey: 'nodeb',
      displayOrder: 2,
      nodeAbbrev: 'b',
      symbol: 'collapsed',
    });
    const nodec = createNewNode({
      uniqueKey: 'nodec',
      displayOrder: 3,
      nodeAbbrev: 'c',
      symbol: 'collapsed',
    });
    const node1 = createNewNode({
      parentUniqueKey: 'nodea',
      uniqueKey: 'node1',
      displayOrder: 1,
      nodeAbbrev: '1',
      symbol: 'nothing',
    });
    nodes = addNodes([nodea, nodeb, nodec, node1], nodes);
    nodes = setRetrieved('nodea', nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    expect(state.treeNodes.ids.length).toBe(4);

    state = alterAssetTreeState(state, clickIcon(nodea));
    expect(state.treeConfiguration.nodes[0].symbol).toEqual('expanded');
    expect(state.treeConfiguration.nodes[1].symbol).toEqual('nothing');
    expect(state.treeConfiguration.nodes[2].symbol).toEqual('collapsed');
    expect(state.treeConfiguration.nodes[3].symbol).toEqual('collapsed');
    expect(state.treeConfiguration.nodes.length).toEqual(4);
    expect(state.treeConfiguration.loadingAssets).toBe(false);
  });

  it('should alter state when the icon is clicked on a node with no available children', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
      data: {
        NodeId: 'a',
        TreeId: 'a123',
        NodeAbbrev: 'a',
        NodeDesc: 'a',
        ParentNodeId: null,
        NodeTypeId: 1,
        DisplayOrder: 1,
        AssetId: 123456,
        AssetNodeBehaviorId: 1234,
        Asset: null,
        ParentUniqueKey: null,
        UniqueKey: 'nodea',
        AssetGuid: 'qwertyuiop123456789',
        ReferencedBy: null,
        HasChildren: true,
      },
    });
    const nodeb = createNewNode({
      uniqueKey: 'nodeb',
      displayOrder: 2,
      nodeAbbrev: 'b',
      symbol: 'collapsed',
    });
    const nodec = createNewNode({
      uniqueKey: 'nodec',
      displayOrder: 3,
      nodeAbbrev: 'c',
      symbol: 'collapsed',
    });
    const node1 = createNewNode({
      parentUniqueKey: 'nodea',
      uniqueKey: 'node1',
      displayOrder: 1,
      nodeAbbrev: '1',
      symbol: 'nothing',
    });
    nodes = addNodes([nodea, nodeb, nodec, node1], nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    expect(state.treeNodes.ids.length).toBe(4);

    state = alterAssetTreeState(state, clickIcon(nodea));
    expect(state.treeConfiguration.nodes[0].symbol).toEqual('loading');
    expect(state.treeConfiguration.nodes[1].symbol).toEqual('collapsed');
    expect(state.treeConfiguration.nodes[2].symbol).toEqual('collapsed');
    expect(state.treeConfiguration.nodes.length).toEqual(3);
    expect(state.treeConfiguration.loadingAssets).toBe(true);
  });

  it('should add new nodes when retrieved', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
    });
    const nodeb = createNewNode({
      uniqueKey: 'nodeb',
      displayOrder: 2,
      nodeAbbrev: 'b',
      symbol: 'collapsed',
    });
    const nodec = createNewNode({
      uniqueKey: 'nodec',
      displayOrder: 3,
      nodeAbbrev: 'c',
      symbol: 'collapsed',
    });
    const node1 = createNewNode({
      parentUniqueKey: 'nodea',
      uniqueKey: 'node1',
      displayOrder: 1,
      nodeAbbrev: '1',
      symbol: 'nothing',
    });
    nodes = addNodes([nodea, nodeb, nodec, node1], nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    state = alterAssetTreeState(
      state,
      nodesRetrieved('nodeb', [
        createNewNode({
          uniqueKey: 'node2',
          parentUniqueKey: 'nodeb',
          nodeAbbrev: 'node2',
        }),
        createNewNode({
          uniqueKey: 'node3',
          parentUniqueKey: 'nodeb',
          nodeAbbrev: 'node3',
        }),
      ])
    );

    expect(state.treeNodes.ids.length).toEqual(6);
    expect(state.treeNodes.entities.nodeb.retrieved).toBe(true);
  });

  it('changes the autocomplete', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
    });

    nodes = addNodes([nodea], nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    state = alterAssetTreeState(state, autocompleteChanged('newValue'));

    expect(state.treeConfiguration.autoCompletePending).toBe(true);
    expect(state.treeConfiguration.autoCompleteValue).toEqual('newValue');
    expect(state.treeConfiguration.autoCompleteAssets).toEqual([]);

    state = alterAssetTreeState(state, autocompleteChanged(null));

    expect(state.treeConfiguration.autoCompletePending).toBe(false);
    expect(state.treeConfiguration.autoCompleteValue).toEqual(null);
    expect(state.treeConfiguration.autoCompleteAssets).toEqual([]);
  });

  it('finished the autocomplete call', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
    });

    nodes = addNodes([nodea], nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    state = alterAssetTreeState(state, autocompleteChanged('newValue'));
    state = alterAssetTreeState(
      state,
      autocompleteChangedRetrieved(
        [
          {
            UniqueID: 'aaa',
            Display: 'display1',
          },
          {
            UniqueID: 'bbb',
            Display: 'display2',
          },
          {
            UniqueID: 'ccc',
            Display: 'display3',
          },
        ],
        'newValue',
        'treeb'
      )
    );

    expect(state.treeConfiguration.autoCompletePending).toBe(true);
    expect(state.treeConfiguration.autoCompleteValue).toEqual('newValue');
    expect(state.treeConfiguration.autoCompleteAssets).toEqual([]);

    state = alterAssetTreeState(
      state,
      autocompleteChangedRetrieved(
        [
          {
            UniqueID: 'aaa',
            Display: 'display1',
          },
          {
            UniqueID: 'bbb',
            Display: 'display2',
          },
          {
            UniqueID: 'ccc',
            Display: 'display3',
          },
        ],
        'newValue2',
        'treea'
      )
    );

    expect(state.treeConfiguration.autoCompletePending).toBe(true);
    expect(state.treeConfiguration.autoCompleteValue).toEqual('newValue');
    expect(state.treeConfiguration.autoCompleteAssets).toEqual([]);

    state = alterAssetTreeState(
      state,
      autocompleteChangedRetrieved(
        [
          {
            UniqueID: 'aaa',
            Display: 'display1',
          },
          {
            UniqueID: 'bbb',
            Display: 'display2',
          },
          {
            UniqueID: 'ccc',
            Display: 'display3',
          },
        ],
        'newValue',
        'treea'
      )
    );

    expect(state.treeConfiguration.autoCompletePending).toBe(false);
    expect(state.treeConfiguration.autoCompleteValue).toEqual('newValue');
    expect(state.treeConfiguration.autoCompleteAssets.length).toBe(3);
  });

  it('toggle pins', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
    });

    nodes = addNodes([nodea], nodes);

    let state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    expect(state.treeConfiguration.pin).toBe(false);
    state = alterAssetTreeState(state, togglePin(true));
    expect(state.treeConfiguration.pin).toBe(true);
    state = alterAssetTreeState(state, togglePin(false));
    expect(state.treeConfiguration.pin).toBe(false);
  });

  it('alter state default', () => {
    const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
      selectedTree: 'treea',
      trees: [
        {
          id: 'treea',
          name: 'treea',
        },
      ],
    });
    let nodes = createTreeBuilder();
    const nodea = createNewNode({
      uniqueKey: 'nodea',
      displayOrder: 1,
      nodeAbbrev: 'a',
      symbol: 'collapsed',
    });

    nodes = addNodes([nodea], nodes);

    const state: ITreeState = {
      treeConfiguration: assetTreeConfiguration,
      treeNodes: nodes,
      hasDefaultSelectedAsset: true,
    };

    const newState = alterAssetTreeState(state, { event: 'somethjin' } as any);
    expect(newState).toEqual(state);
  });
});
