import { TestBed } from '@angular/core/testing';
import { ITreeNodes } from '../model/tree-state';
import * as TreeBuilderService from './tree-builder.service';
import { ITreeNode } from '../model/tree-node';
import { Update } from '@ngrx/entity';
import { IAtxAssetTreeNode } from '@atonix/atx-core';

const pristineA: ITreeNode = {
  uniqueKey: 'a',
  parentUniqueKey: null,
  nodeAbbrev: 'Asset 1',
  level: 1,
  selected: true,
  retrieved: false,
  symbol: 'nothing',
  displayOrder: 1,
};

const pristineB: ITreeNode = {
  uniqueKey: 'b',
  parentUniqueKey: null,
  nodeAbbrev: 'Asset 2',
  level: 2,
  selected: false,
  retrieved: true,
  symbol: 'expanded',
  displayOrder: 2,
};

const treeBuilderState: ITreeNodes = {
  ids: ['a', 'b'],
  entities: {
    a: TreeBuilderService.createNewNode(pristineA),
    b: TreeBuilderService.createNewNode(pristineB),
  },
  rootAssets: ['a', 'b'],
  childrenDict: {},
  appContext: null,
  treeID: null,
};

describe('TreeBuilderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should sort same display order', () => {
    const nodes: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'node2',
        displayOrder: 4,
        nodeAbbrev: 'a',
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'node1',
        displayOrder: 3,
        nodeAbbrev: 'b',
      }),
    ];
    expect(
      nodes
        .sort(TreeBuilderService.compareAssetTreeNodes)
        .map((n) => n.uniqueKey)
    ).toEqual(['node1', 'node2']);
  });

  describe('Sort nodes', () => {
    it('should sort by display order then node abbrev case insensitive', () => {
      const nodes: ITreeNode[] = [
        TreeBuilderService.createNewNode({
          uniqueKey: 'node4',
          displayOrder: 4,
          nodeAbbrev: 'd',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node3',
          displayOrder: 4,
          nodeAbbrev: 'C',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node2',
          displayOrder: 4,
          nodeAbbrev: 'b',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node1',
          displayOrder: 4,
          nodeAbbrev: 'A',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node8',
          displayOrder: 13,
          nodeAbbrev: 'p',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node6',
          displayOrder: 11,
          nodeAbbrev: 'o',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node7',
          displayOrder: 12,
          nodeAbbrev: 'n',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node5',
          displayOrder: 10,
          nodeAbbrev: 'm',
        }),
      ];

      expect(
        nodes
          .sort(TreeBuilderService.compareAssetTreeNodes)
          .map((n) => n.uniqueKey)
      ).toEqual([
        'node1',
        'node2',
        'node3',
        'node4',
        'node5',
        'node6',
        'node7',
        'node8',
      ]);
    });

    it('should sort different display order', () => {
      const nodes: ITreeNode[] = [
        TreeBuilderService.createNewNode({
          uniqueKey: 'node2',
          displayOrder: 2,
          nodeAbbrev: 'A',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node1',
          displayOrder: 1,
          nodeAbbrev: 'B',
        }),
      ];

      expect(
        nodes
          .sort(TreeBuilderService.compareAssetTreeNodes)
          .map((n) => n.uniqueKey)
      ).toEqual(['node1', 'node2']);
    });

    it('should sort both', () => {
      const nodes: ITreeNode[] = [
        TreeBuilderService.createNewNode({
          uniqueKey: 'node2',
          displayOrder: 1,
          nodeAbbrev: 'Asset 2',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node3',
          displayOrder: 2,
          nodeAbbrev: 'Asset 3',
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'node1',
          displayOrder: 1,
          nodeAbbrev: 'Asset 1',
        }),
      ];

      expect(
        nodes
          .sort(TreeBuilderService.compareAssetTreeNodes)
          .map((n) => n.uniqueKey)
      ).toEqual(['node1', 'node2', 'node3']);
    });
  });

  it('should create Asset Tree Adapter', () => {
    expect(TreeBuilderService.assetTreeAdapter).toBeTruthy();
  });

  describe('Tree builder', () => {
    it('should build initial tree', () => {
      expect(TreeBuilderService.createTreeBuilder()).toEqual({
        ids: [],
        entities: {},
        rootAssets: [],
        childrenDict: {},
        treeID: null,
        appContext: null,
      });
    });

    it('should build initial tree with parameters', () => {
      const newTree = TreeBuilderService.createTreeBuilder(
        'tree1',
        'appContext1'
      );
      expect(newTree.treeID).toEqual('tree1');
      expect(newTree.appContext).toEqual('appContext1');
    });

    it('should build the tree base on the nodes passed', () => {
      const nodes: ITreeNode[] = [
        TreeBuilderService.createNewNode({
          uniqueKey: 'b',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset B',
          symbol: 'collapsed',
          displayOrder: 1,
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset A',
          symbol: 'collapsed',
          displayOrder: 1,
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'c',
          parentUniqueKey: 'a',
          nodeAbbrev: 'Asset C',
          symbol: 'collapsed',
          displayOrder: 1,
        }),
        TreeBuilderService.createNewNode({
          uniqueKey: 'd',
          parentUniqueKey: 'c',
          nodeAbbrev: 'Asset D',
          symbol: 'collapsed',
          displayOrder: 1,
        }),
      ];

      let newTree = TreeBuilderService.createTreeBuilder();
      newTree = TreeBuilderService.addNodes(nodes, newTree);

      expect(newTree.ids.sort()).toEqual(['a', 'b', 'c', 'd']);
      expect(newTree.childrenDict.a.length).toEqual(1);
      expect(newTree.childrenDict.c.length).toEqual(1);
      expect(newTree.childrenDict.b).toBeFalsy();
      expect(newTree.rootAssets.sort()).toEqual(['a', 'b']);
    });
  });

  it('should select node', () => {
    const atxAssetTreeNode: IAtxAssetTreeNode = {
      NodeId: 'a',
      TreeId: 'default',
      NodeAbbrev: 'Asset 1',
      NodeDesc: 'Asset for testing only',
      ParentNodeId: null,
      NodeTypeId: 456,
      DisplayOrder: 1,
      AssetId: 123,
      AssetNodeBehaviorId: null,
      Asset: null,
      ParentUniqueKey: null,
      UniqueKey: 'a',
      AssetGuid: 'testingonly',
      ReferencedBy: 'Unit Tests',
      HasChildren: true,
    };

    const expectedOutput: ITreeNode = {
      uniqueKey: atxAssetTreeNode.UniqueKey,
      parentUniqueKey: atxAssetTreeNode.ParentUniqueKey,
      nodeAbbrev: atxAssetTreeNode.NodeAbbrev,
      displayOrder: atxAssetTreeNode.DisplayOrder,
      level: 0,
      selected: false,
      retrieved: false,
      symbol: 'collapsed',
      data: atxAssetTreeNode,
    };

    expect(TreeBuilderService.createNode(atxAssetTreeNode)).toEqual(
      expectedOutput
    );
  });

  it('should add root nodes', () => {
    const nodes: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'd',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 4',
        level: 1,
        selected: true,
        retrieved: false,
        symbol: 'nothing',
        displayOrder: 5,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'c',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 3',
        level: 1,
        selected: true,
        retrieved: false,
        symbol: 'nothing',
        displayOrder: 4,
      }),
    ];

    const expectedOutput: ITreeNodes = {
      ids: ['a', 'b', 'c', 'd'],
      entities: {
        a: TreeBuilderService.createNewNode(pristineA),
        b: TreeBuilderService.createNewNode(pristineB),
        c: {
          uniqueKey: 'c',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 3',
          level: 1,
          selected: true,
          retrieved: false,
          symbol: 'nothing',
          displayOrder: 4,
        },
        d: {
          uniqueKey: 'd',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 4',
          level: 1,
          selected: true,
          retrieved: false,
          symbol: 'nothing',
          displayOrder: 5,
        },
      },
      rootAssets: ['a', 'b', 'c', 'd'],
      childrenDict: {},
      treeID: null,
      appContext: null,
    };

    expect(TreeBuilderService.addRootNodes(nodes, treeBuilderState)).toEqual(
      expectedOutput
    );
  });

  it('should add a node', () => {
    const node: ITreeNode = {
      uniqueKey: 'e',
      parentUniqueKey: 'a',
      nodeAbbrev: 'Asset 5',
      level: 2,
      selected: true,
      retrieved: false,
      symbol: 'nothing',
      displayOrder: 1,
    };

    const expectedOutput: ITreeNodes = {
      ids: ['a', 'e', 'b'],
      entities: {
        a: TreeBuilderService.createNewNode(pristineA),
        b: TreeBuilderService.createNewNode(pristineB),
        e: {
          uniqueKey: 'e',
          parentUniqueKey: 'a',
          nodeAbbrev: 'Asset 5',
          level: 2,
          selected: true,
          retrieved: false,
          symbol: 'nothing',
          displayOrder: 1,
        },
      },
      rootAssets: ['a', 'b'],
      childrenDict: {
        a: ['e'],
      },
      treeID: null,
      appContext: null,
    };

    expect(TreeBuilderService.addNodes(node, treeBuilderState)).toEqual(
      expectedOutput
    );
  });

  it('should add more than one node', () => {
    const nodes: ITreeNode[] = [
      {
        uniqueKey: 'e',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 5',
        level: 2,
        selected: true,
        retrieved: true,
        symbol: 'nothing',
        displayOrder: 1,
      },
      {
        uniqueKey: 'f',
        parentUniqueKey: 'b',
        nodeAbbrev: 'Asset 6',
        level: 2,
        selected: true,
        retrieved: true,
        symbol: 'nothing',
        displayOrder: 1,
      },
    ];

    const expectedOutput: ITreeNodes = {
      ids: ['a', 'b', 'e', 'f'],
      entities: {
        a: {
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 1',
          level: 1,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
          displayOrder: 1,
        },
        b: {
          uniqueKey: 'b',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 2',
          level: 1,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
          displayOrder: 1,
        },
        e: {
          uniqueKey: 'e',
          parentUniqueKey: 'a',
          nodeAbbrev: 'Asset 5',
          level: 2,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
          displayOrder: 1,
        },
        f: {
          uniqueKey: 'f',
          parentUniqueKey: 'b',
          nodeAbbrev: 'Asset 6',
          level: 2,
          selected: true,
          retrieved: true,
          symbol: 'nothing',
          displayOrder: 1,
        },
      },
      rootAssets: ['a', 'b'],
      childrenDict: {
        a: ['e'],
        b: ['f'],
      },
      treeID: null,
      appContext: null,
    };

    const newState = TreeBuilderService.addNodes(nodes, treeBuilderState);
    expect(newState.entities.a).toBeTruthy();
    expect(newState.entities.b).toBeTruthy();
    expect(newState.entities.e).toBeTruthy();
    expect(newState.entities.f).toBeTruthy();
    expect(newState.ids.length).toEqual(4);
  });

  it('should update a node', () => {
    const update: Update<ITreeNode> = {
      id: 'a',
      changes: {
        nodeAbbrev: 'Asset A',
        selected: false,
      },
    };

    const newState = TreeBuilderService.updateNode(update, treeBuilderState);
    expect(newState.entities.a.nodeAbbrev).toEqual('Asset A');
    expect(newState.entities.a.selected).toEqual(false);
  });

  it('should expand a node', () => {
    const newState = TreeBuilderService.expandNode(
      'a',
      treeBuilderState,
      'expanded'
    );
    expect(newState.entities.a.symbol).toEqual('expanded');
  });

  it('should determine that a node have already retrieved its children', () => {
    let state: ITreeNodes = {
      ids: ['a'],
      entities: {
        a: TreeBuilderService.createNewNode({
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 1',
          level: 1,
          selected: true,
          retrieved: false,
          symbol: 'nothing',
          displayOrder: 1,
        }),
      },
      rootAssets: ['a'],
      childrenDict: {},
      treeID: null,
      appContext: null,
    };

    const children: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'e',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 5',
        level: 2,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
    ];

    expect(TreeBuilderService.isRetrieved('a', state)).toBeFalsy();
    state = TreeBuilderService.addNodes(children, state);
    state = TreeBuilderService.setRetrieved('a', state);
    expect(TreeBuilderService.isRetrieved('a', state)).toBeTruthy();
  });

  it('should get selectors of Asset Tree Adapter', () => {
    expect(TreeBuilderService.getSelectors()).toBeTruthy();
  });

  it('should return children of a node', () => {
    let state: ITreeNodes = {
      ids: ['a'],
      entities: {
        a: TreeBuilderService.createNewNode({
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 1',
          level: 1,
          selected: true,
          symbol: 'nothing',
          displayOrder: 1,
        }),
      },
      rootAssets: ['a'],
      childrenDict: {},
      appContext: null,
      treeID: null,
    };

    const children: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'b',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 2',
        level: 1,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'e',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 5',
        level: 2,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
    ];

    const expectedOutput: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'b',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 2',
        level: 1,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'e',
        parentUniqueKey: 'a',
        nodeAbbrev: 'Asset 5',
        level: 2,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
    ];

    expect(TreeBuilderService.getChildren(state, 'a')).toEqual([]);
    state = TreeBuilderService.addNodes(children, state);
    expect(TreeBuilderService.getChildren(state, 'a')).toEqual(expectedOutput);
  });

  it('should check if a node is loading', () => {
    let state: ITreeNodes = {
      ids: ['a', 'b', 'c'],
      entities: {
        a: TreeBuilderService.createNewNode({
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 1',
          level: 1,
          selected: true,
          symbol: 'nothing',
          displayOrder: 1,
        }),
        b: TreeBuilderService.createNewNode({
          uniqueKey: 'b',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 2',
          level: 1,
          selected: true,
          symbol: 'nothing',
          displayOrder: 1,
        }),
        c: TreeBuilderService.createNewNode({
          uniqueKey: 'c',
          parentUniqueKey: 'a',
          nodeAbbrev: 'Asset 3',
          level: 2,
          selected: true,
          symbol: 'nothing',
          displayOrder: 1,
        }),
      },
      rootAssets: ['a', 'b'],
      childrenDict: {
        a: ['c'],
      },
      appContext: null,
      treeID: null,
    };

    expect(TreeBuilderService.isAnyNodeLoading(state)).toBeFalsy();
    state = TreeBuilderService.expandNode('c', state, 'loading');
    expect(TreeBuilderService.isAnyNodeLoading(state)).toBeTruthy();
  });

  it('should select tree nodes', () => {
    let state: ITreeNodes = {
      ids: ['a', 'b'],
      entities: {
        a: TreeBuilderService.createNewNode({
          uniqueKey: 'a',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 1',
          level: 1,
          selected: true,
          symbol: 'nothing',
          displayOrder: 1,
        }),
        b: TreeBuilderService.createNewNode({
          uniqueKey: 'b',
          parentUniqueKey: null,
          nodeAbbrev: 'Asset 2',
          level: 1,
          selected: true,
          symbol: 'expanded',
          displayOrder: 1,
        }),
      },
      rootAssets: ['a', 'b'],
      childrenDict: {},
      treeID: null,
      appContext: null,
    };

    const children: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'd',
        parentUniqueKey: 'c',
        nodeAbbrev: 'Asset 4',
        level: 1,
        selected: true,
        symbol: 'nothing',
        displayOrder: 2,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'c',
        parentUniqueKey: 'b',
        nodeAbbrev: 'Asset 3',
        level: 1,
        selected: true,
        symbol: 'expanded',
        displayOrder: 1,
      }),
    ];

    const expectedOutput1: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'a',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 1',
        level: 0,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'b',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 2',
        level: 0,
        selected: true,
        symbol: 'expanded',
        displayOrder: 1,
      }),
    ];

    const expectedOutput2: ITreeNode[] = [
      TreeBuilderService.createNewNode({
        uniqueKey: 'a',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 1',
        level: 0,
        selected: true,
        symbol: 'nothing',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'b',
        parentUniqueKey: null,
        nodeAbbrev: 'Asset 2',
        level: 0,
        selected: true,
        symbol: 'expanded',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'c',
        parentUniqueKey: 'b',
        nodeAbbrev: 'Asset 3',
        level: 1,
        selected: true,
        symbol: 'expanded',
        displayOrder: 1,
      }),
      TreeBuilderService.createNewNode({
        uniqueKey: 'd',
        parentUniqueKey: 'c',
        nodeAbbrev: 'Asset 4',
        level: 2,
        selected: true,
        symbol: 'nothing',
        displayOrder: 2,
      }),
    ];

    expect(TreeBuilderService.selectTreeNodes(state)).toEqual(expectedOutput1);
    state = TreeBuilderService.addNodes(children, state);
    expect(TreeBuilderService.selectTreeNodes(state)).toEqual(expectedOutput2);
  });
});
