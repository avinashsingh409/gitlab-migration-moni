import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { TreeConfigState } from './tree-config.models';
import { IAtxAssetTree } from '@atonix/atx-core';
import { getSelectedNodes } from '../tree-builder.service';
import { combineLatest, map } from 'rxjs';

@Injectable()
export class TreeConfigQuery {
  constructor(private store: ComponentStore<TreeConfigState>) {}

  readonly treeWrapperTreeState$ = this.store.select(
    (state) => state.treeWrapperTreeState,
    { debounce: true }
  );

  readonly assetTreeConfigurationSelectedNodes$ = this.store.select((state) =>
    getSelectedNodes(state.treeWrapperTreeState.treeNodes)
  );

  readonly showCustomerTrees$ = this.store.select(
    (state) => state.showCustomerTrees,
    { debounce: false }
  );

  readonly assetTrees$ = this.store.select((state) => state.assetTrees, {
    debounce: true,
  });

  readonly selectedTree$ = this.store.select((state) => state.selectedTree, {
    debounce: true,
  });

  readonly selectedTreeNodes$ = this.store.select(
    (state) => state.selectedTreeNodes,
    { debounce: true }
  );

  readonly selectedTreeNodesStore$ = this.store.select(
    (state) => state.selectedTreeNodesStore,
    { debounce: true }
  );

  readonly selectedTreeNodesByTree$ = this.store.select(
    (state) =>
      state.selectedTreeNodes.filter(
        (x) => x.TreeId === state.selectedTree.TreeId
      ) ?? [],
    { debounce: true }
  );

  readonly selectedTreeNodesStoreByTree$ = this.store.select(
    (state) =>
      state.selectedTreeNodesStore.filter(
        (x) => x.TreeId === state.selectedTree.TreeId
      ) ?? [],
    { debounce: true }
  );

  readonly selectedTreeNode$ = this.store.select(
    (state) => state.selectedTreeNode,
    { debounce: true }
  );

  readonly customers$ = this.store.select((state) => state.customers, {
    debounce: true,
  });

  readonly selectedCustomer$ = this.store.select(
    (state) => state.selectedCustomer,
    {
      debounce: false,
    }
  );

  readonly customerId$ = this.store.select(
    this.selectedCustomer$,
    (state) => state.Id,
    {
      debounce: true,
    }
  );

  readonly showCustomerHierarchyToggle$ = this.store.select(
    (state) => state.showCustomerHierarchyToggle,
    {
      debounce: true,
    }
  );

  readonly errorMessage$ = this.store.select((state) => state.errorMessage, {
    debounce: true,
  });

  readonly selectedCustomerTopLevelAssets$ = this.store.select(
    (state) => state.selectedCustomerTopLevelAssets,
    {
      debounce: true,
    }
  );

  readonly selectedTopLevelAsset$ = this.store.select(
    (state) => state.selectedTopLevelAsset,
    {
      debounce: true,
    }
  );

  readonly currentUserCustomerId$ = this.store.select(
    (state) => state.currentUserCustomerId,
    {
      debounce: true,
    }
  );

  readonly isEditing$ = this.store.select((state) => state.isEditing, {
    debounce: true,
  });

  readonly combinedStateForCustomerTree$ = this.store.select(
    this.assetTrees$,
    this.selectedCustomer$,
    this.showCustomerTrees$,
    (trees, selectedCustomer, showCustomerTree) => ({
      trees,
      selectedCustomer,
      showCustomerTree,
    })
  );

  readonly userAssetTrees$ = this.store.select(
    this.combinedStateForCustomerTree$,
    (state) => {
      let userTrees: IAtxAssetTree[] = state.trees.filter(
        (t) => t.IsUserOwned === true
      );
      if (state.showCustomerTree) {
        userTrees = state.trees.filter(
          (t) =>
            t.IsUserOwned === false &&
            t.CustomerId === state?.selectedCustomer?.Id
        );
      }

      return userTrees;
    },
    {
      debounce: true,
    }
  );

  readonly vm$ = combineLatest([
    this.treeWrapperTreeState$,
    this.assetTrees$,
    this.selectedTree$,
    this.selectedTreeNodes$,
    this.selectedTreeNodesStore$,
    this.selectedTreeNode$,
    this.showCustomerTrees$,
    this.customers$,
    this.selectedCustomer$,
    this.selectedCustomerTopLevelAssets$,
    this.selectedTopLevelAsset$,
    this.showCustomerHierarchyToggle$,
    this.errorMessage$,
    this.currentUserCustomerId$,
    this.isEditing$,
  ]).pipe(
    map(
      ([
        treeWrapperTreeState,
        assetTrees,
        selectedTree,
        selectedTreeNodes,
        selectedTreeNodesStore,
        selectedTreeNode,
        showCustomerTrees,
        customers,
        selectedCustomer,
        selectedCustomerTopLevelAssets,
        selectedTopLevelAsset,
        showCustomerHierarchyToggle,
        errorMessage,
        currentUserCustomerId,
        isEditing,
      ]) =>
        ({
          treeWrapperTreeState,
          assetTrees,
          selectedTree,
          selectedTreeNodes,
          selectedTreeNodesStore,
          selectedTreeNode,
          showCustomerTrees,
          customers,
          selectedCustomer,
          selectedCustomerTopLevelAssets,
          selectedTopLevelAsset,
          showCustomerHierarchyToggle,
          errorMessage,
          currentUserCustomerId,
          isEditing,
        } as TreeConfigState)
    )
  );
}
