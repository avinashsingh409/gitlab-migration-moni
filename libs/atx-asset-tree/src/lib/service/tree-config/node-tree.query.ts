import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, take, map } from 'rxjs/operators';
import { NodeTreeNode } from './tree-config.models';
import { NodeTreeState } from './node-tree.facade';

export class NodeTreeQuery {
  private state$ = this.store.asObservable();

  constructor(public store: BehaviorSubject<NodeTreeState>) {}

  readonly nodes$ = this.state$.pipe(
    map((state) => state.nodes),
    distinctUntilChanged()
  );

  readonly nodesStore$ = this.state$.pipe(
    map((state) => state.nodesStore),
    distinctUntilChanged()
  );

  getNodes = (): NodeTreeNode[] => {
    let nodes: NodeTreeNode[] = [];
    this.nodes$.pipe(take(1)).subscribe((queryNodes: NodeTreeNode[]) => {
      nodes = queryNodes;
    });
    return nodes;
  };

  getNodesStore = (): NodeTreeNode[] => {
    let allNodes: NodeTreeNode[] = [];
    this.nodesStore$.pipe(take(1)).subscribe((queryNodes: NodeTreeNode[]) => {
      allNodes = queryNodes;
    });
    return allNodes;
  };
}
