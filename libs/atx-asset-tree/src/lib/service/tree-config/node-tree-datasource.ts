import {
  DataSource,
  CollectionViewer,
  SelectionChange,
} from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable, OnDestroy } from '@angular/core';
import { NodeTreeNode } from './tree-config.models';
import { Subject, Observable, merge } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { NodeTreeFacade } from './node-tree.facade';

@Injectable({
  providedIn: 'root',
})
export class NodeTreeDataSource implements DataSource<NodeTreeNode>, OnDestroy {
  public data: NodeTreeNode[] = [];
  public unsubscribe$ = new Subject<void>();

  constructor(
    private treeControl: FlatTreeControl<NodeTreeNode>,
    private nodeTreeFacade: NodeTreeFacade
  ) {
    nodeTreeFacade.query.nodes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((nodes: NodeTreeNode[]) => {
        this.data = nodes;
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<NodeTreeNode[]> {
    this.treeControl.expansionModel.changed.subscribe(
      (change: SelectionChange<NodeTreeNode>) => {
        if (
          (change as SelectionChange<NodeTreeNode>).added ||
          (change as SelectionChange<NodeTreeNode>).removed
        ) {
          this.handleTreeControl(change as SelectionChange<NodeTreeNode>);
        }
      }
    );

    return merge(
      collectionViewer.viewChange,
      this.nodeTreeFacade.query.nodes$
    ).pipe(map((_) => this.data));
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  disconnect(_collectionViewer: CollectionViewer): void {}

  handleTreeControl(change: SelectionChange<NodeTreeNode>): void {
    if (change.added) {
      change.added.forEach((node: NodeTreeNode) => {
        this.nodeTreeFacade.toggleNode(node, true);
      });
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach((node: NodeTreeNode) =>
          this.nodeTreeFacade.toggleNode(node, false)
        );
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
