import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { cloneDeep } from 'lodash';
import { NodeTreeQuery } from './node-tree.query';
import { NodeTreeNode } from './tree-config.models';

export interface NodeTreeState {
  nodes: NodeTreeNode[];
  nodesStore: NodeTreeNode[];
}

const _initialState: NodeTreeState = {
  nodes: [],
  nodesStore: [],
};

let _state: NodeTreeState = _initialState;

@Injectable({
  providedIn: 'root',
})
export class NodeTreeFacade implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private store = new BehaviorSubject<NodeTreeState>(_state);
  public query = new NodeTreeQuery(this.store);

  setNodes(nodes: NodeTreeNode[]): void {
    this.updateState({
      ..._state,
      nodes,
    });
  }

  setNodesStore(nodes: NodeTreeNode[]): void {
    this.updateState({
      ..._state,
      nodesStore: nodes,
    });
  }

  toggleNode(node: NodeTreeNode, expand: boolean): void {
    const nodes: NodeTreeNode[] = [...this.query.getNodes()];
    const index: number = nodes.indexOf(node);
    if (index < 0) {
      return;
    }

    const allNodes: NodeTreeNode[] = [...this.query.getNodesStore()];
    if (!node.IsExpanded) {
      node.IsLoading = true;
      if (node.Children == null || node.Children.length === 0) {
        const children: NodeTreeNode[] = allNodes
          .filter((n: NodeTreeNode) => n.ParentNodeId === node.NodeId)
          .map((n: NodeTreeNode) => {
            n.Level = node.Level + 1;
            return n;
          });
        node.Children = children;
        nodes.splice(index + 1, 0, ...node.Children);
        node.IsExpanded = true;
        node.IsLoading = false;
        this.updateState({
          ..._state,
          nodes,
        });
      } else {
        node.Children = cloneDeep(node.Children);
        nodes.splice(index + 1, 0, ...node.Children);
        node.IsExpanded = true;
        node.IsLoading = false;
        this.updateState({
          ..._state,
          nodes,
        });
      }
    } else {
      let count = 0;
      for (
        let i = index + 1;
        i < nodes.length && nodes[i].Level > node.Level;
        i++, count++
      );
      for (let i = index + 1; i < index + count; i++) {
        nodes[i].IsExpanded = false;
        allNodes[
          allNodes.findIndex((a) => a.NodeId === nodes[i].NodeId)
        ].IsExpanded = false;
      }
      nodes.splice(index + 1, count);
      node.IsExpanded = false;
      this.updateState({
        ..._state,
        nodes,
        nodesStore: allNodes,
      });
    }
  }

  private updateState(newState: NodeTreeState) {
    this.store.next((_state = newState));
  }

  private reset() {
    this.updateState(_initialState);
  }

  ngOnDestroy() {
    this.reset();
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
