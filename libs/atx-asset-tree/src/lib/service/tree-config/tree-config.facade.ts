import { Injectable, OnDestroy } from '@angular/core';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import {
  Subject,
  Observable,
  switchMap,
  withLatestFrom,
  tap,
  of,
  EMPTY,
  filter,
} from 'rxjs';
import { ITreeStateChange } from '../../model/tree-state-change';
import { TreeConfigState, NodeTreeNode } from './tree-config.models';
import { TreeConfigQuery } from './tree-config.query';
import { TreeConfigCommands } from './tree-config.commands';
import { ModelService } from '../model.service';
import { getDefaultTree } from '../asset-tree.service';
import { createTreeBuilder } from '../tree-builder.service';
import {
  AccountFrameworkService,
  AssetsCoreService,
  CustomerNode,
  TopLevelAssetNode,
  UserAdminCoreService,
} from '@atonix/shared/api';
import { IAtxAssetTree, IAtxAssetTreeNode, isNil } from '@atonix/atx-core';
import { ITreeNode } from '../../model/tree-node';
import { HttpErrorResponse } from '@angular/common/http';

export const _initialState: TreeConfigState = {
  treeWrapperTreeState: {
    treeConfiguration: getDefaultTree({
      collapseOthers: false,
      showTreeSelector: true,
    }),
    treeNodes: createTreeBuilder(),
    hasDefaultSelectedAsset: false,
  },
  assetTrees: [],
  selectedTree: null,
  selectedTreeNodes: [],
  selectedTreeNodesStore: [],
  selectedTreeNode: null,
  showCustomerTrees: false,
  customers: [],
  selectedCustomer: null,
  showCustomerHierarchyToggle: false,
  errorMessage: null,
  selectedCustomerTopLevelAssets: [],
  selectedTopLevelAsset: null,
  currentUserCustomerId: null,
  isEditing: false,
};

@Injectable()
export class TreeConfigFacade
  extends ComponentStore<TreeConfigState>
  implements OnDestroy
{
  public query = new TreeConfigQuery(this);
  public command = new TreeConfigCommands(this);
  unsubscribe$ = new Subject<void>();

  constructor(
    private assetTreeModelService: ModelService,
    private assetsCoreService: AssetsCoreService,
    private userAdminCoreService: UserAdminCoreService,
    private accountFrameworkService: AccountFrameworkService
  ) {
    super(_initialState);
  }

  readonly changeTreeWrapperState = this.effect(
    (treeChange$: Observable<ITreeStateChange>) => {
      return treeChange$.pipe(
        withLatestFrom(this.query.treeWrapperTreeState$),
        tap(([treeChange, treeState]) =>
          this.command.setTreeWrapperState(treeChange)
        ),
        switchMap(([treeChange, treeState]) =>
          this.assetTreeModelService
            .getAssetTreeData(
              treeChange,
              treeState.treeConfiguration,
              treeState.treeNodes
            )
            .pipe(
              tapResponse(
                (response) => this.changeTreeWrapperState(response),
                (err: HttpErrorResponse) => {
                  console.log(err);
                  const errorResults = err.error?.Results ?? [];
                  if (errorResults.length > 0) {
                    this.command.setErrorMessage(errorResults[0]?.Message);
                  }

                  return EMPTY;
                }
              )
            )
        )
      );
    }
  );

  readonly getCurrentUserCustomerId = this.effect<void>(($) =>
    $.pipe(
      switchMap((_) =>
        this.accountFrameworkService.user().pipe(
          filter((u) => u !== null || u !== undefined),
          tapResponse(
            (response) => {
              this.command.setCurrentUseCustomerId(response.customerId);
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              const errorResults = err.error?.Results ?? [];
              if (errorResults.length > 0) {
                this.command.setErrorMessage(errorResults[0]?.Message);
              }

              return EMPTY;
            }
          )
        )
      )
    )
  );

  readonly getCustomer = this.effect<void>(($) =>
    $.pipe(
      switchMap((_) =>
        this.userAdminCoreService.getCustomers().pipe(
          tapResponse(
            (response) => {
              if (response && response.length > 0) {
                const customer: CustomerNode = response[0];
                this.command.setCustomers(response);
                this.command.setSelectedCustomer(customer);
              }
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              const errorResults = err.error?.Results ?? [];
              if (errorResults.length > 0) {
                this.command.setErrorMessage(errorResults[0]?.Message);
              }

              return EMPTY;
            }
          )
        )
      )
    )
  );

  readonly getCustomerTopLevelAssets = this.effect(
    (customerId$: Observable<string>) =>
      customerId$.pipe(
        switchMap((customerId) =>
          this.userAdminCoreService.getCustomerDetails(customerId).pipe(
            tapResponse(
              (response) => {
                if (response) {
                  const topLevelAssetNodes = response.TopLevelAssets.map(
                    (a) => {
                      return {
                        ...a,
                        IsExpanded: false,
                        IsLoading: false,
                        IsSelected: false,
                        Level: 0,
                        Children: [],
                      } as TopLevelAssetNode;
                    }
                  );
                  this.command.setCustomerTopLevelAssets(topLevelAssetNodes);
                  this.command.resetTreeSelection();
                }
              },
              (err: string) => {
                console.log(err);
                return EMPTY;
              }
            )
          )
        )
      )
  );

  readonly getTrees = this.effect((params$: Observable<void>) =>
    params$.pipe(
      switchMap((params) =>
        this.assetsCoreService.getTreesForTreeConfig().pipe(
          tapResponse(
            (response) => {
              this.command.setTrees(response);
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              const errorResults = err.error?.Results ?? [];
              if (errorResults.length > 0) {
                this.command.setErrorMessage(errorResults[0]?.Message);
              }

              return EMPTY;
            }
          )
        )
      )
    )
  );

  readonly selectTree = this.effect((tree$: Observable<IAtxAssetTree>) => {
    return tree$.pipe(
      tap((tree: IAtxAssetTree) => {
        this.command.setTree(tree);
        this.command.setSelectedNode(null);
      }),
      tap((tree: IAtxAssetTree) => this.getNodes({ treeId: tree.TreeId }))
    );
  });

  readonly getNodes = this.effect(
    (
      getNodesParam$: Observable<{
        treeId: string;
        newNode?: NodeTreeNode;
        selectNewNode?: boolean;
      }>
    ) =>
      getNodesParam$.pipe(
        switchMap((getNodesParam) =>
          this.assetsCoreService
            .getNodesForTreeConfig(getNodesParam.treeId)
            .pipe(
              tapResponse(
                (response) => {
                  const isRealNode = getNodesParam?.newNode?.NodeTypeId === 2;

                  const allNodes: NodeTreeNode[] = response.map(
                    (n: IAtxAssetTreeNode) => {
                      const node: NodeTreeNode = n as NodeTreeNode;
                      node.IsLoading = false;
                      node.IsExpanded = true;
                      node.Level = 0;
                      node.HasChildren = n.HasChildren;

                      if (!getNodesParam.selectNewNode) {
                        const isParentNodeToNewNode =
                          node.NodeId === getNodesParam?.newNode?.ParentNodeId;

                        if (isRealNode && isParentNodeToNewNode) {
                          //if new added node is a real node and current node is parent of the newly added node, select it
                          node.IsSelected = true;
                        }

                        if (
                          node.NodeId === getNodesParam?.newNode?.NodeId &&
                          !isRealNode
                        ) {
                          //if new added node is a container node, select it
                          node.IsSelected = true;
                        }

                        if (node.IsSelected && !getNodesParam.selectNewNode) {
                          this.command.setSelectedNode(node);
                        }
                      } else {
                        if (node.NodeId === getNodesParam.newNode.NodeId) {
                          node.IsSelected = true;
                        }
                      }
                      return node;
                    }
                  );

                  if (getNodesParam.selectNewNode) {
                    this.command.setSelectedNode(getNodesParam.newNode);
                  }

                  const sortedNodes = this.sortNodes(allNodes).sort((a, b) => {
                    if (a.ParentNodeId === b.ParentNodeId) {
                      return a.DisplayOrder - b.DisplayOrder;
                    }
                  });
                  this.command.setNodes(sortedNodes);
                  this.command.setNodesStore(sortedNodes);
                },
                (err: HttpErrorResponse) => {
                  console.log(err);
                  const errorResults = err.error?.Results ?? [];
                  if (errorResults.length > 0) {
                    this.command.setErrorMessage(errorResults[0]?.Message);
                  }

                  return EMPTY;
                }
              )
            )
        )
      )
  );

  readonly upsertTree = this.effect((upsertTree$: Observable<IAtxAssetTree>) =>
    upsertTree$.pipe(
      withLatestFrom(this.query.userAssetTrees$),
      switchMap(([upsertTree, treeSummaries]) =>
        this.assetsCoreService.upsertCustomTree(upsertTree).pipe(
          tapResponse(
            (response) => {
              let index = treeSummaries.findIndex(
                (t: IAtxAssetTree) => t.TreeId === upsertTree.TreeId
              );
              if (!isNil(upsertTree.TreeId)) {
                treeSummaries.splice(index, 1, upsertTree);
              } else {
                treeSummaries.push(response);
                index = treeSummaries.length - 1;
              }
              this.command.setTrees(treeSummaries);
              this.command.setTree(treeSummaries[index]);
              this.command.setErrorMessage(null);
              this.command.setSelectedNode(null);
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              const errorResults = err.error?.Results ?? [];
              if (errorResults.length > 0) {
                this.command.setErrorMessage(errorResults[0]?.Message);
              }

              return EMPTY;
            }
          )
        )
      )
    )
  );

  readonly deleteTree = this.effect((tree$: Observable<IAtxAssetTree | null>) =>
    tree$.pipe(
      withLatestFrom(this.query.selectedTree$, this.query.userAssetTrees$),
      switchMap(([tree, selectedTree, treeSummaries]) =>
        this.assetsCoreService.deleteCustomTree(selectedTree.TreeId).pipe(
          tapResponse(
            (response) => {
              const index = treeSummaries.findIndex(
                (t: IAtxAssetTree) => t.TreeId === selectedTree.TreeId
              );
              treeSummaries.splice(index, 1);
              this.command.setTrees(treeSummaries);
              this.command.setTree(null);
              this.command.setSelectedNode(null);
              this.command.setNodes([]);
              this.command.setNodesStore([]);
            },
            (err: HttpErrorResponse) => {
              console.log(err);
              const errorResults = err.error?.Results ?? [];
              if (errorResults.length > 0) {
                this.command.setErrorMessage(errorResults[0]?.Message);
              }

              return EMPTY;
            }
          )
        )
      )
    )
  );

  readonly clickNode = this.effect(
    (clickEvent$: Observable<{ clickedNode: NodeTreeNode; index: number }>) =>
      clickEvent$.pipe(
        withLatestFrom(this.query.selectedTreeNodes$),
        tap(([clickEvent, selectedTreeNodes]) => {
          const node: NodeTreeNode = clickEvent.clickedNode;
          const index: number = clickEvent.index;
          const newNodes: NodeTreeNode[] = [...selectedTreeNodes];
          const currentSelectedNodeIdx =
            selectedTreeNodes.findIndex((x) => x.IsSelected) ?? -1;
          const selectedNodeIdx =
            newNodes.findIndex((x) => x.NodeId === node.NodeId) ?? -1;

          if (currentSelectedNodeIdx >= 0) {
            newNodes[currentSelectedNodeIdx].IsSelected = false;
          }

          if (node.NodeTypeId === 1 && !node.IsExpanded && !node.IsSelected) {
            newNodes[selectedNodeIdx].IsExpanded = true;
            node.IsExpanded = true;
            this.toggleNode({ node });
          }

          newNodes[selectedNodeIdx].IsSelected = !node.IsSelected;
          this.command.setNodes(newNodes);
          this.command.setNodesStore(newNodes);
          if (node.IsSelected) {
            this.command.setSelectedNode(null);
          } else {
            node.IsSelected = true;
            this.command.setSelectedNode(node);
          }
        })
      )
  );

  readonly toggleNode = this.effect(
    (
      toggleParams$: Observable<{
        node: NodeTreeNode;
      }>
    ) =>
      toggleParams$.pipe(
        withLatestFrom(this.query.selectedTreeNodes$),
        tap(([toggleParams, selectedTreeNodes]) => {
          let newNodes: NodeTreeNode[] = [...selectedTreeNodes];
          const node = toggleParams.node;
          const selectedNodeIdx =
            newNodes.findIndex((x) => x.NodeId === node.NodeId) ?? -1;

          const childNodes = this.getChildNodes(
            node.NodeId,
            selectedTreeNodes,
            []
          ).map((n) => n.NodeId);
          if (node.IsExpanded) {
            newNodes = selectedTreeNodes.map((n) => {
              if (childNodes.some((x) => x === n.NodeId)) {
                n.IsExpanded = true;
                n.IsHidden = false;
              }
              return n;
            });
          } else {
            newNodes = selectedTreeNodes.map((n) => {
              if (childNodes.some((x) => x === n.NodeId)) {
                n.IsHidden = true;
              }
              return n;
            });
          }

          newNodes[selectedNodeIdx].IsExpanded = node.IsExpanded;
          this.command.setNodes(newNodes);
          this.command.setNodesStore(newNodes);
        })
      )
  );

  readonly addCustomerAssetNode = this.effect(
    (customerAssetNode$: Observable<{ assetId: string; assetName: string }>) =>
      customerAssetNode$.pipe(
        withLatestFrom(
          this.query.selectedTree$,
          this.query.selectedTreeNodes$,
          this.query.selectedTreeNode$
        ),
        tap(() => this.command.setErrorMessage(null)),
        switchMap(
          ([
            customerAssetNode,
            selectedTree,
            selectedTreeNodes,
            selectedNode,
          ]) => {
            if (!selectedTree) {
              return EMPTY;
            }

            const newAssetNode = this.createNode(
              customerAssetNode.assetName,
              customerAssetNode.assetId,
              selectedTree.TreeId,
              2,
              3
            );

            if (selectedNode) {
              if (selectedNode.NodeTypeId === 2) {
                return EMPTY;
              }

              const selectedNodeChildren = selectedTreeNodes.filter(
                (n) => n.ParentNodeId === selectedNode.NodeId
              );
              newAssetNode.DisplayOrder =
                this.getNodeDisplayOrder(selectedNodeChildren);
              newAssetNode.ParentNodeId = selectedNode.NodeId;
              newAssetNode.Level = selectedNode.Level + 1;
            } else {
              const assetNodeExistInRoot =
                selectedTreeNodes?.some(
                  (x) =>
                    x.Level === 0 &&
                    x.AssetGuid === customerAssetNode.assetId &&
                    x.TreeId === selectedTree.TreeId
                ) ?? false;
              if (assetNodeExistInRoot) {
                return EMPTY;
              }

              const topLevelNodes = selectedTreeNodes.filter((n) =>
                isNil(n.ParentNodeId)
              );
              newAssetNode.DisplayOrder =
                this.getNodeDisplayOrder(topLevelNodes);
              newAssetNode.Level = 0;
            }

            return this.assetsCoreService
              .upsertCustomTreeNode(newAssetNode)
              .pipe(
                tapResponse(
                  (response) => {
                    const newNode = response as NodeTreeNode;
                    return this.getNodes({
                      treeId: selectedTree.TreeId,
                      newNode,
                    });
                  },
                  (err: HttpErrorResponse) => {
                    console.log(err);
                    const errorResults = err.error?.Results ?? [];
                    if (errorResults.length > 0) {
                      this.command.setErrorMessage(errorResults[0]?.Message);
                    }

                    return EMPTY;
                  }
                )
              );
          }
        )
      )
  );

  readonly addAssetNode = this.effect((assetNode$: Observable<ITreeNode>) =>
    assetNode$.pipe(
      withLatestFrom(
        this.query.selectedTree$,
        this.query.selectedTreeNodes$,
        this.query.selectedTreeNode$
      ),
      tap(() => this.command.setErrorMessage(null)),
      switchMap(
        ([assetNode, selectedTree, selectedTreeNodes, selectedNode]) => {
          if (!selectedTree) {
            return EMPTY;
          }

          const newAssetNode = this.createNode(
            assetNode.data.Asset.AssetAbbrev,
            assetNode.data.AssetGuid,
            selectedTree.TreeId,
            2,
            3
          );

          if (selectedNode) {
            if (selectedNode.NodeTypeId === 2) {
              return EMPTY;
            }

            const selectedNodeChildren = selectedTreeNodes.filter(
              (n) => n.ParentNodeId === selectedNode.NodeId
            );
            newAssetNode.DisplayOrder =
              this.getNodeDisplayOrder(selectedNodeChildren);
            newAssetNode.ParentNodeId = selectedNode.NodeId;
            newAssetNode.Level = selectedNode.Level + 1;
          } else {
            const assetNodeExistInRoot =
              selectedTreeNodes?.some(
                (x) =>
                  x.Level === 0 &&
                  x.AssetGuid === assetNode.data?.AssetGuid &&
                  x.TreeId === selectedTree.TreeId
              ) ?? false;
            if (assetNodeExistInRoot) {
              return EMPTY;
            }

            const topLevelNodes = selectedTreeNodes.filter((n) =>
              isNil(n.ParentNodeId)
            );
            newAssetNode.DisplayOrder = this.getNodeDisplayOrder(topLevelNodes);
            newAssetNode.Level = 0;
          }

          return this.assetsCoreService.upsertCustomTreeNode(newAssetNode).pipe(
            tapResponse(
              (response) => {
                const newNode = response as NodeTreeNode;
                return this.getNodes({ treeId: selectedTree.TreeId, newNode });
              },
              (err: HttpErrorResponse) => {
                console.log(err);
                const errorResults = err.error?.Results ?? [];
                if (errorResults.length > 0) {
                  this.command.setErrorMessage(errorResults[0]?.Message);
                }

                return EMPTY;
              }
            )
          );
        }
      )
    )
  );

  readonly addContainerNode = this.effect<void>(($) =>
    $.pipe(
      withLatestFrom(
        this.query.selectedTree$,
        this.query.selectedTreeNode$,
        this.query.selectedTreeNodes$
      ),
      tap(() => this.command.setErrorMessage(null)),
      switchMap(([_, selectedTree, selectedNode, selectedTreeNodes]) => {
        if (!selectedTree) {
          return EMPTY;
        }

        const newContainerNode = this.createNode(
          'New Container Node',
          null,
          selectedTree.TreeId,
          1,
          1
        );

        if (selectedNode) {
          newContainerNode.ParentNodeId = selectedNode.NodeId;
          newContainerNode.Level = selectedNode.Level + 1;

          const selectedNodeChildren = selectedTreeNodes.filter(
            (n) => n.ParentNodeId === selectedNode.NodeId
          );
          newContainerNode.DisplayOrder =
            this.getNodeDisplayOrder(selectedNodeChildren);
          if (selectedNode.NodeTypeId === 2) {
            return EMPTY;
          }
        } else {
          const topLevelNodes = selectedTreeNodes.filter((n) =>
            isNil(n.ParentNodeId)
          );
          newContainerNode.DisplayOrder =
            this.getNodeDisplayOrder(topLevelNodes);
        }

        return this.assetsCoreService
          .upsertCustomTreeNode(newContainerNode)
          .pipe(
            tapResponse(
              (response) => {
                const newNode = response as NodeTreeNode;
                return this.getNodes({
                  treeId: selectedTree.TreeId,
                  newNode,
                });
              },
              (err: HttpErrorResponse) => {
                console.log(err);
                const errorResults = err.error?.Results ?? [];
                if (errorResults.length > 0) {
                  this.command.setErrorMessage(errorResults[0]?.Message);
                }

                return EMPTY;
              }
            )
          );
      })
    )
  );

  readonly deleteNode = this.effect<void>(($) =>
    $.pipe(
      withLatestFrom(this.query.selectedTreeNode$),
      tap(() => this.command.setErrorMessage(null)),
      switchMap(([_, selectedNode]) => {
        return this.assetsCoreService
          .deleteCustomTreeNode((selectedNode as NodeTreeNode).NodeId)
          .pipe(
            tapResponse(
              (response) => {
                if (response) {
                  this.command.setSelectedNode(null);
                  this.getNodes({ treeId: selectedNode.TreeId });
                }
              },
              (err: HttpErrorResponse) => {
                console.log(err);
                const errorResults = err.error?.Results ?? [];
                if (errorResults.length > 0) {
                  this.command.setErrorMessage(errorResults[0]?.Message);
                }

                return EMPTY;
              }
            )
          );
      })
    )
  );

  readonly updateNodeName = this.effect((updatedName$: Observable<string>) =>
    updatedName$.pipe(
      withLatestFrom(
        this.query.selectedTreeNodes$,
        this.query.selectedTreeNodesStore$,
        this.query.selectedTreeNode$
      ),
      tap(() => this.command.setErrorMessage(null)),
      switchMap(
        ([
          updatedName,
          selectedTreeNodes,
          selectedTreeNodesStore,
          selectedNode,
        ]) => {
          if (selectedNode.NodeAbbrev === updatedName) {
            return of(null);
          }
          //if (selectedNode.NodeTypeId === 2 && updatedName === '') {
          // might be able to get AssetAbbrev from api AssetPath
          // but it isn't currently on NodeTreeNode
          //}
          selectedNode.NodeAbbrev = updatedName;
          const nodesIndex = selectedTreeNodes.findIndex(
            (n: NodeTreeNode) => n.NodeId === selectedNode.NodeId
          );
          selectedTreeNodes[nodesIndex].NodeAbbrev = updatedName;
          const nodesStoreIndex = selectedTreeNodesStore.findIndex(
            (n: NodeTreeNode) => n.NodeId === selectedNode.NodeId
          );
          selectedTreeNodesStore[nodesStoreIndex].NodeAbbrev = updatedName;

          return this.assetsCoreService.upsertCustomTreeNode(selectedNode).pipe(
            tapResponse(
              (response) => {
                this.getNodes({
                  treeId: selectedNode.TreeId,
                  newNode: selectedNode,
                  selectNewNode: true,
                });
              },
              (err: HttpErrorResponse) => {
                console.log(err);
                const errorResults = err.error?.Results ?? [];
                if (errorResults.length > 0) {
                  this.command.setErrorMessage(errorResults[0]?.Message);
                }

                return EMPTY;
              }
            )
          );
        }
      )
    )
  );

  readonly updateNodeBehavior = this.effect(
    (updatedNodeBehavior$: Observable<number>) =>
      updatedNodeBehavior$.pipe(
        withLatestFrom(
          this.query.selectedTreeNodes$,
          this.query.selectedTreeNodesStore$,
          this.query.selectedTreeNode$
        ),
        tap(() => this.command.setErrorMessage(null)),
        switchMap(
          ([
            updatedNodeBehaviorId,
            selectedTreeNodes,
            selectedTreeNodesStore,
            selectedNode,
          ]) => {
            if (
              selectedNode.AssetNodeBehaviorId === Number(updatedNodeBehaviorId)
            ) {
              return of(null);
            }
            selectedNode.AssetNodeBehaviorId = updatedNodeBehaviorId;
            const nodesIndex = selectedTreeNodes.findIndex(
              (n: NodeTreeNode) => n.NodeId === selectedNode.NodeId
            );
            selectedTreeNodes[nodesIndex].AssetNodeBehaviorId =
              updatedNodeBehaviorId;
            const nodesStoreIndex = selectedTreeNodesStore.findIndex(
              (n: NodeTreeNode) => n.NodeId === selectedNode.NodeId
            );
            selectedTreeNodesStore[nodesStoreIndex].AssetNodeBehaviorId =
              updatedNodeBehaviorId;

            return this.assetsCoreService
              .upsertCustomTreeNode(selectedNode)
              .pipe(
                tapResponse(
                  (response) => {
                    this.getNodes({
                      treeId: selectedNode.TreeId,
                      newNode: selectedNode,
                      selectNewNode: true,
                    });
                  },
                  (err: HttpErrorResponse) => {
                    console.log(err);
                    const errorResults = err.error?.Results ?? [];
                    if (errorResults.length > 0) {
                      this.command.setErrorMessage(errorResults[0]?.Message);
                    }

                    return EMPTY;
                  }
                )
              );
          }
        )
      )
  );

  private sortNodes(nodes: NodeTreeNode[]): NodeTreeNode[] {
    const nodeDict: { [id: string]: NodeTreeNode[] } = {};
    nodeDict['toplevelnodes'] = [];

    nodes.forEach((node: NodeTreeNode) => {
      if (isNil(node.ParentNodeId)) {
        nodeDict['toplevelnodes'].push(node);
      } else {
        if (isNil(nodeDict[node.ParentNodeId])) {
          nodeDict[node.ParentNodeId] = [];
        }
        nodeDict[node.ParentNodeId].push(node);
      }
    });

    return this.hierarchySort(nodeDict, 'toplevelnodes', [], -1);
  }

  private hierarchySort(
    nodeDictionary: { [id: string]: NodeTreeNode[] },
    parentId: string,
    sortedNodes: NodeTreeNode[],
    parentLevel: number
  ): NodeTreeNode[] {
    if (isNil(nodeDictionary[parentId])) return;
    const childrenNodes: NodeTreeNode[] = nodeDictionary[parentId];
    childrenNodes.forEach((child: NodeTreeNode) => {
      child.Level = parentLevel + 1;
      sortedNodes.push(child);
      this.hierarchySort(
        nodeDictionary,
        child.NodeId,
        sortedNodes,
        parentLevel + 1
      );
    });
    return sortedNodes;
  }

  private createNode(
    nodeAbbrev: string,
    assetGuid: string,
    treeId: string,
    nodeTypeId: number,
    nodeBehaviorId: number
  ): NodeTreeNode {
    return {
      IsLoading: false,
      IsExpanded: false,
      Level: 0,
      Children: null,
      IsSelected: true,
      NodeId: null,
      ParentNodeId: null,
      TreeId: treeId,
      NodeAbbrev: nodeAbbrev,
      NodeDesc: nodeAbbrev,
      NodeTypeId: nodeTypeId,
      DisplayOrder: 0,
      AssetNodeBehaviorId: nodeBehaviorId,
      ParentUniqueKey: null,
      UniqueKey: null,
      AssetGuid: assetGuid,
      ReferencedBy: null,
      HasChildren: false,
    };
  }

  private getNodeDisplayOrder(nodes: NodeTreeNode[]): number {
    if (nodes && nodes.length > 0) {
      return Math.max(...nodes.map((n) => n.DisplayOrder)) + 1;
    }
    return 0;
  }

  private getChildNodes(
    parentNode: string,
    nodes: NodeTreeNode[],
    childNodes: NodeTreeNode[]
  ) {
    const currentChildNodes =
      nodes.filter((x) => x.ParentNodeId === parentNode) ?? [];
    for (const index of currentChildNodes) {
      childNodes.push(index);
    }

    if (currentChildNodes.length > 0) {
      this.getChildNodes(currentChildNodes[0].NodeId, nodes, childNodes);
    }

    return childNodes;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
