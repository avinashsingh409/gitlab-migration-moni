import { CustomerNode, TopLevelAssetNode } from '@atonix/shared/api';
import { ITreeState } from '../../model/tree-state';
import { IAtxAssetTree, IAtxAssetTreeNode } from '@atonix/atx-core';

export interface TreeConfigState {
  treeWrapperTreeState: ITreeState | null;
  assetTrees: IAtxAssetTree[];
  selectedTree: IAtxAssetTree | null;
  selectedTreeNodes: NodeTreeNode[];
  selectedTreeNodesStore: NodeTreeNode[];
  selectedTreeNode: NodeTreeNode | null;
  showCustomerTrees: boolean;
  customers: CustomerNode[];
  selectedCustomer: CustomerNode;
  selectedCustomerTopLevelAssets: TopLevelAssetNode[];
  currentUserCustomerId: string;

  selectedTopLevelAsset: TopLevelAssetNode;
  showCustomerHierarchyToggle: boolean;
  errorMessage: string | null;
  isEditing: boolean;
}

export interface NodeTreeNode extends IAtxAssetTreeNode {
  IsLoading: boolean;
  IsExpanded: boolean;
  Level: number;
  Children?: NodeTreeNode[] | null;
  IsSelected: boolean;
  IsHidden?: boolean;
}
