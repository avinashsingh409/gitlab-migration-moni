import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { TreeConfigState } from './tree-config.models';
import { ITreeStateChange } from '../../model/tree-state-change';
import {
  treeRetrievedStateChange,
  alterAssetTreeState,
} from '../asset-tree.service';
import { IAtxAssetTree } from '@atonix/atx-core';
import { NodeTreeNode } from './tree-config.models';
import { CustomerNode, TopLevelAssetNode } from '@atonix/shared/api';
import { _initialState } from './tree-config.facade';

@Injectable()
export class TreeConfigCommands {
  constructor(private componentStore: ComponentStore<TreeConfigState>) {}

  readonly setTreeWrapperState = this.componentStore.updater(
    (state: TreeConfigState, change: ITreeStateChange): TreeConfigState => {
      const newChange = treeRetrievedStateChange(change);
      return {
        ...state,
        treeWrapperTreeState: alterAssetTreeState(
          state.treeWrapperTreeState,
          newChange
        ),
      };
    }
  );

  readonly changeShowHideCustomerTree = this.componentStore.updater(
    (state: TreeConfigState): TreeConfigState => {
      return {
        ...state,
        showCustomerTrees: !state.showCustomerTrees,
      };
    }
  );

  readonly setShowCustomerTree = this.componentStore.updater(
    (state: TreeConfigState, showCustomerTree: boolean): TreeConfigState => {
      return {
        ...state,
        showCustomerTrees: showCustomerTree,
      };
    }
  );

  readonly setTrees = this.componentStore.updater(
    (state: TreeConfigState, trees: IAtxAssetTree[]): TreeConfigState => {
      return {
        ...state,
        assetTrees: trees,
      };
    }
  );

  readonly setTree = this.componentStore.updater(
    (state: TreeConfigState, tree: IAtxAssetTree): TreeConfigState => {
      const newSelectedTreeNodes = state.selectedTreeNodes.map((n) => ({
        ...n,
        IsSelected: false,
      }));

      const newSelectedTreeNodesStore = state.selectedTreeNodes.map((n) => ({
        ...n,
        IsSelected: false,
      }));
      return {
        ...state,
        selectedTreeNodes: newSelectedTreeNodes,
        selectedTreeNodesStore: newSelectedTreeNodesStore,
        selectedTree: tree,
      };
    }
  );

  readonly setNodes = this.componentStore.updater(
    (state: TreeConfigState, nodes: NodeTreeNode[]): TreeConfigState => {
      return {
        ...state,
        selectedTreeNodes: nodes,
      };
    }
  );

  readonly setNodesStore = this.componentStore.updater(
    (state: TreeConfigState, nodes: NodeTreeNode[]): TreeConfigState => {
      return {
        ...state,
        selectedTreeNodesStore: nodes,
      };
    }
  );

  readonly setSelectedNode = this.componentStore.updater(
    (state: TreeConfigState, selectedNode: NodeTreeNode): TreeConfigState => {
      return {
        ...state,
        selectedTreeNode: selectedNode,
      };
    }
  );

  readonly setCustomers = this.componentStore.updater(
    (state: TreeConfigState, customers: CustomerNode[]): TreeConfigState => {
      return {
        ...state,
        customers,
      };
    }
  );

  readonly setSelectedCustomer = this.componentStore.updater(
    (
      state: TreeConfigState,
      selectedCustomer: CustomerNode
    ): TreeConfigState => {
      return {
        ...state,
        selectedCustomer,
      };
    }
  );

  readonly setAllowManageCustomerHierarchy = this.componentStore.updater(
    (state: TreeConfigState, allow: boolean): TreeConfigState => {
      return {
        ...state,
        showCustomerHierarchyToggle: allow,
      };
    }
  );

  readonly setErrorMessage = this.componentStore.updater(
    (state: TreeConfigState, message: string): TreeConfigState => {
      return {
        ...state,
        errorMessage: message,
      };
    }
  );

  readonly setCustomerTopLevelAssets = this.componentStore.updater(
    (
      state: TreeConfigState,
      topLevelAssets: TopLevelAssetNode[]
    ): TreeConfigState => {
      return {
        ...state,
        selectedCustomerTopLevelAssets: topLevelAssets,
      };
    }
  );

  readonly setTopLevelAsset = this.componentStore.updater(
    (
      state: TreeConfigState,
      topLevelAsset: TopLevelAssetNode
    ): TreeConfigState => {
      return {
        ...state,
        selectedTopLevelAsset: topLevelAsset,
      };
    }
  );

  readonly setCurrentUseCustomerId = this.componentStore.updater(
    (state: TreeConfigState, userCustomerId: string): TreeConfigState => {
      return {
        ...state,
        currentUserCustomerId: userCustomerId,
      };
    }
  );

  readonly setIsEditing = this.componentStore.updater(
    (state: TreeConfigState, isEditing: boolean): TreeConfigState => {
      return {
        ...state,
        isEditing,
      };
    }
  );

  readonly resetTreeSelection = this.componentStore.updater(
    (state: TreeConfigState): TreeConfigState => {
      const newSelectedTreeNodes = state.selectedTreeNodes.map((n) => ({
        ...n,
        IsSelected: false,
      }));

      const newSelectedTreeNodesStore = state.selectedTreeNodes.map((n) => ({
        ...n,
        IsSelected: false,
      }));
      return {
        ...state,
        selectedTreeNodes: [],
        selectedTreeNodesStore: [],
        selectedTree: null,
        selectedTreeNode: null,
      };
    }
  );

  readonly resetState = this.componentStore.updater(
    (state: TreeConfigState): TreeConfigState => {
      return _initialState;
    }
  );
}
