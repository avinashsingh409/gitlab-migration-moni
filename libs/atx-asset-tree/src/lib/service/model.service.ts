import { IInfoTrayStateChange } from './../model/info-tray-state-change';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import { Observable, of, EMPTY, Subject, BehaviorSubject } from 'rxjs';
import { ITreeStateChange, ISelectAsset } from '../model/tree-state-change';
import {
  take,
  map,
  switchMap,
  catchError,
  distinctUntilChanged,
} from 'rxjs/operators';

import {
  permissionsChanged,
  autocompleteChangedRetrieved,
  nodesRetrieved,
} from './asset-tree.service';
import {
  nodeExists,
  getIDFromArrayOfIDs,
  isRetrieved,
  createNode,
  getIDFromSelectedAssets,
  getSelectedNodes,
} from './tree-builder.service';
import { ITreeConfiguration, ITreeNodes } from '../model/tree-state';
import { IAssetData } from '../model/info-tray-state';
import { assetRetrieved } from '../service/asset-info-tray.service';
import {
  getSelectedTree,
  IAtxAssetTree,
  IAtxAssetTreeNode,
  IAtxTreeRetrieval,
  IAutoCompleteItem,
  IAWSFileUrl,
} from '@atonix/atx-core';
import { AssetTreeDBService } from '@atonix/atx-indexed-db';

import {
  AssetFrameworkService,
  AuthorizationFrameworkService,
  UIConfigFrameworkService,
  AssetsCoreService,
} from '@atonix/shared/api';
export interface AssetTreeState {
  node: IAtxAssetTreeNode | null;
  selectedNode: string;
  selectedTree: string;
}

let _state: AssetTreeState = {
  node: null,
  selectedNode: '',
  selectedTree: '',
};

@Injectable({
  providedIn: 'root',
})
export class ModelService implements OnDestroy {
  constructor(
    private sanitizer: DomSanitizer,
    private indexedDB: AssetTreeDBService,
    private uiConfigFrameworkService: UIConfigFrameworkService,
    private assetFrameworkService: AssetFrameworkService,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private assetsCoreService: AssetsCoreService
  ) {}
  private onDestroy = new Subject<void>();
  private store = new BehaviorSubject<AssetTreeState>(_state);
  private state$ = this.store.asObservable();

  node$ = this.state$.pipe(
    map((state) => state.node),
    distinctUntilChanged()
  );
  defaultNode$ = this.state$.pipe(
    map((state) => state.selectedNode),
    distinctUntilChanged()
  );

  // Helper method to get the initial values to populate the asset tree from the server.
  public reloadTree(
    treeID?: string,
    nodeID?: string,
    appContextID?: string
  ): Observable<ITreeStateChange> {
    return this.indexedDB.reloadTree(treeID, nodeID, appContextID).pipe(
      catchError((val: unknown) => {
        console.error(val);
        return of(null);
      }),
      switchMap(
        (values: {
          m_Item1: IAtxAssetTree[];
          m_Item2: IAtxAssetTreeNode[];
          currentTree: string;
        }) => {
          if (values?.m_Item2) {
            if (!nodeID && values?.currentTree) {
              return this.indexedDB.getDefaultNode(values.currentTree).pipe(
                switchMap((node: { UniqueKey: string; TreeId: string }) => {
                  const nodeResult: ITreeStateChange = {
                    event: 'TreeRetrieved',
                    newValue: {
                      assetTrees: values.m_Item1,
                      nodes: values.m_Item2,
                      tree: values.currentTree,
                      selectedNode: node.UniqueKey,
                    } as IAtxTreeRetrieval,
                  };
                  return of(nodeResult);
                })
              );
            }
            const result: ITreeStateChange = {
              event: 'TreeRetrieved',
              newValue: {
                assetTrees: values.m_Item1,
                nodes: values.m_Item2,
                tree: getSelectedTree(values.m_Item2),
                selectedNode: nodeID,
              } as IAtxTreeRetrieval,
            };
            return of(result);
          } else {
            return this.loadTree(treeID, nodeID, appContextID);
          }
        }
      )
    );
  }

  public loadTree(
    treeID?: string,
    nodeID?: string,
    appContextID?: string
  ): Observable<ITreeStateChange> {
    return this.assetsCoreService
      .loadNodeFromKey(treeID, nodeID, appContextID)
      .pipe(
        take(1),
        map((n) => {
          this.indexedDB.addAssetTreesOnIndxDB(n.m_Item1);
          this.indexedDB.addAssetTreeNodesOnIndxDB(n.m_Item2);
          this.indexedDB.addDefaultTreeNode(
            n.m_Item2,
            getSelectedTree(n.m_Item2)
          );
          if (nodeID) {
            if (n.m_Item2?.length > 0 && !isNaN(Number(nodeID))) {
              const node = n.m_Item2.find(
                (x) => x.AssetId.toString() === nodeID
              );
              nodeID = node?.UniqueKey || nodeID;
            } else if (n.m_Item3) {
              // Asset node was identified by UniqueKey and required translation by API
              nodeID = n.m_Item3;
            }
          }
          return {
            event: 'TreeRetrieved',
            newValue: {
              assetTrees: n.m_Item1,
              nodes: n.m_Item2,
              tree: getSelectedTree(n.m_Item2),
              selectedNode: nodeID,
            } as IAtxTreeRetrieval,
          };
        })
      );
  }

  // Helper method to get the children of a node from the server.
  // We are using the "GetChildren" rather than the "Children" api call here to have the
  // same behavior as the Asset Tree of the legacy app.
  // The "Children" api call uses the uniqueKey that will cause a misconception while
  // getting the right node object due to the uniqueKey not determining if it is a top asset or not.
  public getChildren(node: IAtxAssetTreeNode) {
    return this.assetsCoreService.getChildren(node).pipe(
      take(1),
      map((n) => {
        // this.indexedDB.addAssetTreeNodesOnIndxDB(n);
        n.map((x) => (x.ParentUniqueKey = node.UniqueKey));
        this.indexedDB.addAssetTreeNodesOnIndxDB(n);
        return n;
      })
    );
  }

  public autoComplete(search: string, tree: string, count?: number) {
    if (search) {
      return this.assetFrameworkService.autoCompleteSearch(search, tree, count);
    } else {
      return of([] as IAutoCompleteItem[]);
    }
  }

  public getAssetInfo(assetIDs: string[]): Observable<IAssetData> {
    let result: Observable<IAssetData> = EMPTY;
    // initialized assetData
    const assetData: IAssetData = {
      selectedAssetIDs: assetIDs,
      assetID: null,
      assetAttributes: null,
      hasImages: false,
      assetName: '',
      imagesPath: [],
    };

    if (assetIDs) {
      // process single asset being selected (testing purposes)
      assetData.assetID = assetIDs[0];
      result = new Observable<IAssetData>((res) => {
        return this.assetFrameworkService
          .getAssetAndAttributes(assetIDs.toString())
          .subscribe((attr) => {
            if (attr) {
              assetData.assetAttributes = attr;
              assetData.assetName = assetData.assetAttributes.Asset.AssetDesc;

              res.next(assetData);
            }
          });
      });
    } else {
      // return assetData with null items
      result = new Observable<IAssetData>((res) => {
        res.next(assetData);
      });
    }
    return result;
  }

  public getAssetData(
    change: IInfoTrayStateChange
  ): Observable<IInfoTrayStateChange> {
    let result: Observable<IInfoTrayStateChange> = EMPTY;
    if (change.event === 'GetAsset') {
      const assetIDs = change.newValue as string[];
      result = this.getAssetInfo(assetIDs).pipe(map((n) => assetRetrieved(n)));
    }

    return result;
  }

  public getAssetTreeData(
    change: ITreeStateChange,
    configuration: ITreeConfiguration,
    nodes: ITreeNodes,
    appContext?: string
  ): Observable<ITreeStateChange> {
    let result: Observable<ITreeStateChange> = EMPTY;
    if (change.event === 'TreeRetrieved') {
      const newEvent = change?.newValue as IAtxTreeRetrieval;
      if (newEvent) {
        let selectedNode = null;
        newEvent.nodes.forEach((node: IAtxAssetTreeNode) => {
          if (node.UniqueKey === newEvent.selectedNode) {
            selectedNode = node;
          }
        });
        this.updateState({
          ..._state,
          node: selectedNode,
          selectedNode: newEvent.selectedNode,
          selectedTree: newEvent.tree,
        });
      }
    } else if (change.event === 'LoadTree') {
      result = this.reloadTree(change.newValue as string, null, appContext);
    } else if (change.event === 'GetPermissions') {
      result = this.authorizationFrameworkService
        .getPermissions()
        .pipe(map((n) => permissionsChanged(n)));
    } else if (change.event === 'ReloadAssetTree') {
      result = this.indexedDB.clearIndxDBData().pipe(
        switchMap((retVal) => {
          return this.reloadTree(
            null,
            getIDFromSelectedAssets(getSelectedNodes(nodes)),
            appContext
          );
        })
      );
    } else if (change.event === 'SelectAsset') {
      // switchmap
      const selectAsset = change.newValue as ISelectAsset;
      if (!nodeExists(selectAsset.ids, nodes)) {
        // you can hit the 'SelectAsset' event with
        // selectAsset.ids = ''
        // this will reload the tree
        // and select the default first asset
        result = this.reloadTree(
          null,
          getIDFromArrayOfIDs(selectAsset.ids),
          appContext
        );
        return result;
      }
    } else if (change.event === 'IconClicked') {
      if (
        !isRetrieved((change.newValue as IAtxAssetTreeNode).UniqueKey, nodes)
      ) {
        result = this.getChildren(change.newValue as IAtxAssetTreeNode).pipe(
          map((n) =>
            nodesRetrieved(
              (change.newValue as IAtxAssetTreeNode).UniqueKey,
              n.map((m) => createNode(m))
            )
          )
        );
      }
    } else if (change.event === 'AutoCompleteChanged') {
      // Switchmap
      result = this.autoComplete(
        change.newValue as string,
        configuration.selectedTree
      ).pipe(
        map((n) =>
          autocompleteChangedRetrieved(
            n,
            change.newValue as string,
            configuration.selectedTree
          )
        )
      );
    }

    return result;
  }

  public convertToBlob(data: IAWSFileUrl) {
    return new Observable<SafeStyle>((res) => {
      const oReq = new XMLHttpRequest();
      oReq.open('GET', data.Url, true);
      let blobUrl: string;

      oReq.responseType = 'blob';

      oReq.onload = (oEvent: any) => {
        const blob = new Blob([oReq.response], {
          type: oReq.getResponseHeader('Content-Type') || 'image/png',
        });
        blobUrl = window.URL.createObjectURL(blob);
        const sanitizeUrl: SafeStyle = this.sanitizer.bypassSecurityTrustStyle(
          `url(${blobUrl})`
        );
        // return sanitize url even though there is an error for images which are not present in the s3
        // same with the old one which ignore the console error to just display other images
        res.next(sanitizeUrl);
      };

      oReq.send(null);
    });
  }

  private updateState(state: AssetTreeState) {
    this.store.next((_state = state));
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
}
