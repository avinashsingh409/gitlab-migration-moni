/* eslint-disable @typescript-eslint/no-empty-function */
import { TestBed, inject } from '@angular/core/testing';
import { ModelService } from './model.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ITreeConfiguration } from '../model/tree-state';
import { ITreeStateChange } from '../model/tree-state-change';
import { getDefaultTree, autocompleteChanged } from './asset-tree.service';
import {
  createTreeBuilder,
  createNewNode,
  addNodes,
} from './tree-builder.service';
import { of } from 'rxjs';
import {
  IAtxAssetTreeNode,
  IAutoCompleteItem,
  IAutoCompleteResult,
  ITreePermissions,
} from '@atonix/atx-core';
import { AssetTreeDBService } from '@atonix/atx-indexed-db';
import { APP_CONFIG } from '@atonix/app-config';

class FakeAppConfig {
  baseServicesUrl: 'https://dev.atonix.com/Services/';
}
class FakeAssetTreeDBService {
  addAssetTreesOnIndxDB() {
    return of(null);
  }
  addAssetTreeNodesOnIndxDB() {
    return of(null);
  }

  addDefaultTreeNode() {
    return of(null);
  }
}

describe('ModelService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: AssetTreeDBService, useClass: FakeAssetTreeDBService },
        { provide: APP_CONFIG, useValue: FakeAppConfig },
      ],
    }).compileComponents();
  });

  it('should be created', () => {
    const service: ModelService = TestBed.inject(ModelService);
    expect(service).toBeTruthy();
  });

  it('should get autocomplete values', inject(
    [ModelService, HttpTestingController],
    (service: ModelService, httpTest: HttpTestingController) => {
      let completed = false;
      let result: IAutoCompleteItem[] = null;
      service.autoComplete('aaa', 'bbb', 5).subscribe(
        (data) => {
          result = data;
        },
        () => {},
        () => {
          completed = true;
        }
      );

      const values = [
        {
          UniqueID: 'item1',
          Display: 'item1',
        },
        {
          UniqueID: 'item2',
          Display: 'item2',
        },
        {
          UniqueID: 'item3',
          Display: 'item3',
        },
      ];

      const res = {
        Success: true,
        StatusCode: 200,
        Type: 'TreeSearchRes',
        Count: 5,
        Results: [
          {
            UniqueID: 'item1',
            Display: 'item1',
          },
          {
            UniqueID: 'item2',
            Display: 'item2',
          },
          {
            UniqueID: 'item3',
            Display: 'item3',
          },
        ],
      };

      httpTest
        .expectOne({
          url: `undefined/api/v1/assets/trees/bbb/search?search=aaa&count=5`,
        })
        .flush(res);

      httpTest.verify();

      expect(result).toEqual(values);
      expect(completed).toBe(true);
    }
  ));

  it('should shortcircuit autocomplete', inject(
    [ModelService, HttpTestingController],
    (service: ModelService, httpTest: HttpTestingController) => {
      let completed = false;
      let result: IAutoCompleteItem[] = null;
      service.autoComplete('', 'bbb', 5).subscribe(
        (data) => {
          result = data;
        },
        () => {},
        () => {
          completed = true;
        }
      );

      httpTest.expectNone(() => {
        return true;
      });

      httpTest.verify();

      expect(result).toEqual([]);
      expect(completed).toBe(true);
    }
  ));

  it('should call http for first values of Asset Tree', inject(
    [ModelService, HttpTestingController],
    (service: ModelService, httpTest: HttpTestingController) => {
      let result: ITreeStateChange;
      service
        .loadTree('treeIDC', 'nodeA', 'appContextIDB')
        .subscribe((data: any) => {
          result = data;
        });

      httpTest
        .expectOne({
          url: 'undefined/api/v1/assets/trees/load?assetId=nodeA&treeId=treeIDC',
        })
        .flush({
          m_Item1: [
            {
              TreeId: 'first',
              TreeName: 'First Tree',
              IsDefaultTree: true,
              IsUserOwned: false,
            },
          ],
          m_Item2: [
            {
              NodeId: 'n1',
              TreeId: 'first',
              NodeAbbrev: 'na1',
              NodeDesc: 'nd1',
              ParentNodeId: null,
              NodeTypeId: null,
              DisplayOrder: null,
              AssetId: null,
              AssetNodeBehaviorId: null,
              Asset: null,
              ParentUniqueKey: null,
              UniqueKey: null,
              AssetGuid: null,
              ReferencedBy: null,
              HasChildren: null,
            },
          ],
        });

      httpTest.verify();
    }
  ));

  it('should call http service to get the children of a node', inject(
    [ModelService, HttpTestingController],
    (service: ModelService, httpTest: HttpTestingController) => {
      const assetNode: IAtxAssetTreeNode = {
        NodeId: 'a',
        TreeId: 'a123',
        NodeAbbrev: 'First Node',
        NodeDesc: 'First Node in Test',
        ParentNodeId: null,
        NodeTypeId: 1,
        DisplayOrder: 1,
        AssetId: 123456,
        AssetNodeBehaviorId: 1234,
        Asset: null,
        ParentUniqueKey: null,
        UniqueKey: 'a',
        AssetGuid: 'qwertyuiop123456789',
        ReferencedBy: null,
        HasChildren: true,
      };

      const atxAssetNodes: IAtxAssetTreeNode[] = [
        {
          NodeId: 'b',
          TreeId: 'a1234',
          NodeAbbrev: 'Child Node',
          NodeDesc: 'Child Node in Test',
          ParentNodeId: null,
          NodeTypeId: 1,
          DisplayOrder: 1,
          AssetId: 12345,
          AssetNodeBehaviorId: 123,
          Asset: null,
          ParentUniqueKey: null,
          UniqueKey: 'a',
          AssetGuid: 'qwertyuiop123456789',
          ReferencedBy: null,
          HasChildren: false,
        },
      ];

      let result;
      service.getChildren(assetNode).subscribe((data: any) => {
        result = data;
      });

      httpTest
        .expectOne({
          url: 'undefined/api/v1/assets/nodes/children',
        })
        .flush(atxAssetNodes);

      httpTest.verify();
    }
  ));

  it('should get asset tree data', inject(
    [ModelService, HttpTestingController],
    (service: ModelService, httpTest: HttpTestingController) => {
      const assetTreeConfiguration: ITreeConfiguration = getDefaultTree({
        selectedTree: 'treea',
        trees: [
          {
            id: 'treea',
            name: 'treea',
          },
        ],
      });
      let nodes = createTreeBuilder();
      const nodea = createNewNode({
        uniqueKey: 'nodea',
        displayOrder: 1,
        nodeAbbrev: 'a',
        symbol: 'collapsed',
      });

      nodes = addNodes([nodea], nodes);

      let completed = false;
      let result: ITreeStateChange = null;

      service
        .getAssetTreeData(
          autocompleteChanged(''),
          assetTreeConfiguration,
          nodes,
          'ac'
        )
        .subscribe(
          (data) => {
            result = data;
          },
          () => {},
          () => {
            completed = true;
          }
        );

      httpTest.expectNone(() => {
        return true;
      });

      httpTest.verify();

      expect(result.event).toEqual('AutoCompleteChangedRetrieved');
      expect(result.newValue as IAutoCompleteResult).toEqual({
        items: [],
        search: '',
        treeID: 'treea',
      });
      expect(completed).toBe(true);
    }
  ));
});
