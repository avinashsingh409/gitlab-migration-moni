import { TestBed } from '@angular/core/testing';
import { IInfoTrayStateChange } from './../model/info-tray-state-change';
import { IInfoTrayState } from './../model/info-tray-state';
import {
  createDefaultInfoTray,
  setInfoTrayToggle,
  alterAssetInfoTrayState,
  getAsset,
  assetRetrieved,
} from './asset-info-tray.service';

describe('AssetInfoTrayService', () => {
  it('should get default of asset info tray state', () => {
    const expectedOutput: IInfoTrayState = {
      showInfoTray: false,
      selectedTab: 'info',
      attributes: [],
      asset: null,
      assetIDs: [],
    };

    expect(createDefaultInfoTray()).toEqual(expectedOutput);
  });

  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    expect(createDefaultInfoTray).toBeTruthy();
    expect(setInfoTrayToggle).toBeTruthy();
    expect(getAsset).toBeTruthy();
    expect(assetRetrieved).toBeTruthy();
    expect(alterAssetInfoTrayState).toBeTruthy();
  });

  it('should return set the toggle value of info tray state change', () => {
    const expectedOutput: IInfoTrayStateChange = {
      event: 'ToggleInfoTray',
      newValue: true,
    };
    expect(setInfoTrayToggle(true)).toEqual(expectedOutput);
  });

  it('should return set of items of info tray state change', () => {
    const expectedOutput: IInfoTrayStateChange = {
      event: 'GetAsset',
      newValue: null,
    };
    expect(getAsset(null)).toEqual(expectedOutput);
  });

  it('should return set the toggle value of info tray state change', () => {
    const expectedOutput: IInfoTrayStateChange = {
      event: 'AssetRetrieved',
      newValue: null,
    };
    expect(assetRetrieved(null)).toEqual(expectedOutput);
  });
});
