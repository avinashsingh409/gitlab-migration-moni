import {
  Component,
  AfterViewInit,
  OnDestroy,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { Observable, Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  takeUntil,
  take,
  filter,
  withLatestFrom,
} from 'rxjs/operators';
import {
  UntypedFormGroup,
  UntypedFormControl,
  FormControl,
  Validators,
} from '@angular/forms';
import { ITreeStateChange } from '../../../model/tree-state-change';
import { IAtxAssetTree, isNil, isNilOrEmptyString } from '@atonix/atx-core';
import { TreeConfigFacade } from '../../../service/tree-config/tree-config.facade';
import { selectAsset } from '../../../service/asset-tree.service';
import {
  NodeTreeNode,
  TreeConfigState,
} from '../../../service/tree-config/tree-config.models';
import { ITreeNode } from '../../../model/tree-node';
import { CustomerNode } from '@atonix/shared/api';
import { AuthFacade } from '@atonix/shared/state/auth';
import { ConfirmationDialogComponent } from '@atonix/shared/utils';

@Component({
  selector: 'atx-tree-config',
  templateUrl: './tree-config.component.html',
  styleUrls: ['./tree-config.component.scss'],
})
export class TreeConfigComponent implements AfterViewInit, OnDestroy {
  public vm$: Observable<TreeConfigState>;
  public unsubscribe$ = new Subject<void>();
  public myNodes: NodeTreeNode[] = [];
  public nodeForm: UntypedFormGroup;
  public editingTreeName = false;
  public creatingNewTree = false;
  public treeName = '';
  public helpToolTip =
    'Create or select your custom hierachies to organize the hierarchy for different use cases. Contact your administrator to request custom hierarchies shared across users.';

  public treeNameFormControl = new FormControl('', [Validators.required]);
  @ViewChild('nodeNameInput') public nodeNameInput: ElementRef;

  constructor(
    private dialogRef: MatDialogRef<TreeConfigComponent>,
    public treeConfigFacade: TreeConfigFacade,
    private authFacade: AuthFacade,
    private dialog: MatDialog
  ) {
    this.treeConfigFacade.getCurrentUserCustomerId();

    this.vm$ = this.treeConfigFacade.query.vm$;
    const nodeNameControl = new UntypedFormControl(null);
    nodeNameControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((value: string) => {
        if (!isNilOrEmptyString(value)) {
          this.treeConfigFacade.query.selectedTreeNode$
            .pipe(take(1))
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe((selectedNode) => {
              let isEditing = false;
              if (value === selectedNode.NodeAbbrev) {
                this.treeConfigFacade.command.setIsEditing(false);
                this.nodeForm.get('nodeName').markAsPristine();
              } else {
                this.treeConfigFacade.command.setIsEditing(true);
                isEditing = true;
              }

              if (selectedNode.NodeTypeId === 2 && !isEditing) {
                this.nodeForm.get('assetNodeBehaviorId').enable();
              } else {
                this.nodeForm.get('assetNodeBehaviorId').disable();
              }
            });
        }
      });
    const nodeBehaviorIdControl = new UntypedFormControl({ disabled: true });
    nodeBehaviorIdControl.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((value: number) => {
        if (!isNilOrEmptyString(value)) {
          this.treeConfigFacade.updateNodeBehavior(value);
        }
      });
    this.nodeForm = new UntypedFormGroup({
      nodeName: nodeNameControl,
      assetNodeBehaviorId: nodeBehaviorIdControl,
    });

    this.authFacade.userAdminUtilityAccess$
      .pipe(take(1))
      .subscribe((rights) => {
        if (rights?.CanView) {
          this.treeConfigFacade.command.setAllowManageCustomerHierarchy(true);
        }
      });
  }

  ngAfterViewInit(): void {
    const treeWrapperStateChange: ITreeStateChange = selectAsset('');
    this.treeConfigFacade.changeTreeWrapperState(treeWrapperStateChange);
    this.treeConfigFacade.getTrees();
    this.treeConfigFacade.query.selectedTreeNode$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((selectedNode: NodeTreeNode) => {
        if (selectedNode) {
          this.nodeForm.patchValue({
            nodeName: selectedNode.NodeAbbrev,
            assetNodeBehaviorId: selectedNode.AssetNodeBehaviorId
              ? selectedNode.AssetNodeBehaviorId.toString()
              : '',
          });

          if (selectedNode.NodeTypeId === 2) {
            this.nodeForm.get('assetNodeBehaviorId').enable();
          } else {
            this.nodeForm.get('assetNodeBehaviorId').disable();
          }
        } else {
          this.nodeForm.patchValue({
            nodeName: '',
            assetNodeBehaviorId: '',
          });
        }
      });

    this.treeConfigFacade.query.selectedTree$
      .pipe(
        filter((x) => !isNil(x)),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((val) => {
        this.treeNameFormControl.patchValue(val.TreeName);
      });

    this.treeNameFormControl.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (!isNilOrEmptyString(val)) {
          // this.nodeForm.get('assetNodeBehaviorId').disable();
          this.treeConfigFacade.query.assetTrees$
            .pipe(
              withLatestFrom(this.treeConfigFacade.query.selectedTree$),
              filter(([ts, t]) => !isNil(t) && !isNil(ts)),
              take(1)
            )
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe(([trees, tree]) => {
              const treeExist = trees.some(
                (x) => x.TreeName.toLowerCase() === val.toLowerCase()
              );
              if (treeExist) {
                this.treeNameFormControl.setErrors({ duplicateName: true });
              } else {
                this.treeNameFormControl.setErrors(null);
              }

              if (tree?.TreeName === val) {
                this.treeConfigFacade.command.setIsEditing(false);
                this.treeNameFormControl.markAsPristine();
              } else {
                this.treeConfigFacade.command.setIsEditing(true);
              }
            });
        }
      });

    this.treeConfigFacade.query.selectedCustomer$
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe((customer) => {
        if (customer) {
          this.treeConfigFacade.getCustomerTopLevelAssets(customer.Id);
          this.treeConfigFacade.command.setIsEditing(false);
          this.editingTreeName = false;
        }
      });

    this.treeConfigFacade.query.showCustomerTrees$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((showCustomerTree) => {
        if (showCustomerTree) {
          this.treeConfigFacade.command.setCustomers([]);
          this.treeConfigFacade.getCustomer();
        }

        this.treeConfigFacade.command.resetTreeSelection();
        this.treeConfigFacade.command.setIsEditing(false);
      });
  }

  ngOnDestroy(): void {
    this.treeConfigFacade.command.resetState();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  assetTreeStateChange(change: ITreeStateChange): void {
    this.treeConfigFacade.changeTreeWrapperState(change);
  }

  configureCustomerTreesChange(): void {
    this.treeConfigFacade.command.changeShowHideCustomerTree();
  }

  selectedTreeChange(changeEvent: MatSelectChange): void {
    // this.treeNameFormControl.patchValue(changeEvent.value.TreeName);
    this.treeConfigFacade.selectTree(changeEvent.value);
    this.treeConfigFacade.command.setIsEditing(false);
  }

  createTree(): void {
    this.creatingNewTree = true;
    this.treeNameFormControl.patchValue(null);

    this.treeConfigFacade.command.resetTreeSelection();
  }

  deleteTree(): void {
    this.treeConfigFacade.deleteTree(null);
    this.treeNameFormControl.patchValue(null);
  }

  editTreeName(): void {
    this.editingTreeName = true;
    // this.treeConfigFacade.query.selectedTree$
    //   .pipe(take(1))
    //   .subscribe((val) => this.treeNameFormControl.patchValue(val.TreeName));

    this.treeConfigFacade.command.setIsEditing(true);
    this.nodeForm.get('assetNodeBehaviorId').disable();
  }

  cancelTreeNameEdit(): void {
    if (this.creatingNewTree) {
      this.treeNameFormControl.patchValue(null);
    }

    this.creatingNewTree = false;
    this.editingTreeName = false;
    this.treeConfigFacade.command.setIsEditing(false);
    this.nodeForm.get('assetNodeBehaviorId').enable();
  }

  saveTreeName(): void {
    if (this.creatingNewTree) {
      this.treeConfigFacade.query.showCustomerTrees$
        .pipe(take(1))
        .subscribe((val) => {
          if (val) {
            this.treeConfigFacade.query.selectedCustomer$
              .pipe(
                filter((c) => c !== null || c !== undefined),
                take(1)
              )
              // eslint-disable-next-line rxjs/no-nested-subscribe
              .subscribe((selectedCustomer) => {
                const newTree: IAtxAssetTree = {
                  TreeName: this.treeNameFormControl.value,
                  IsDefaultTree: false,
                  IsUserOwned: false,
                  CustomerId: selectedCustomer.Id,
                };
                this.treeConfigFacade.upsertTree(newTree);
                this.creatingNewTree = false;
              });
          } else {
            this.treeConfigFacade.query.currentUserCustomerId$
              .pipe(
                filter((u) => u !== null || u !== undefined),
                take(1)
              )
              // eslint-disable-next-line rxjs/no-nested-subscribe
              .subscribe((currentUserCustomerId) => {
                const newTree: IAtxAssetTree = {
                  TreeName: this.treeNameFormControl.value,
                  IsDefaultTree: false,
                  IsUserOwned: true,
                  CustomerId: currentUserCustomerId,
                };
                this.treeConfigFacade.upsertTree(newTree);
                this.creatingNewTree = false;
              });
          }
        });
    }
    if (this.editingTreeName) {
      this.treeConfigFacade.query.selectedTree$
        .pipe(take(1))
        .subscribe((selectedTree: IAtxAssetTree) => {
          if (selectedTree.TreeName !== this.treeNameFormControl.value) {
            selectedTree.TreeName = this.treeNameFormControl.value;
            this.treeConfigFacade.upsertTree(selectedTree);
          }
          this.editingTreeName = false;
        });
    }

    this.treeConfigFacade.command.setIsEditing(false);
  }

  addAssetNode(): void {
    this.treeConfigFacade.query.showCustomerTrees$
      .pipe(take(1))
      .subscribe((val) => {
        if (val) {
          this.treeConfigFacade.query.selectedTopLevelAsset$
            .pipe(take(1))
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe((asset) => {
              this.treeConfigFacade.addCustomerAssetNode({
                assetId: asset.AssetId,
                assetName: asset.AssetName,
              });
            });
        } else {
          this.treeConfigFacade.query.assetTreeConfigurationSelectedNodes$
            .pipe(take(1))
            // eslint-disable-next-line rxjs/no-nested-subscribe
            .subscribe((selectedNodes: ITreeNode[]) => {
              if (selectedNodes && selectedNodes.length == 1)
                this.treeConfigFacade.addAssetNode(selectedNodes[0]);
            });
        }
      });
  }

  addContainerNode(): void {
    this.treeConfigFacade.query.selectedTreeNode$
      .pipe(take(1))
      .subscribe((node) => {
        if (isNil(node) || node.NodeTypeId === 1) {
          this.treeConfigFacade.addContainerNode();

          setTimeout(() => {
            this.nodeNameInput?.nativeElement?.focus();
            this.nodeNameInput?.nativeElement?.select();
          }, 2000);
        }
      });
  }

  public onNodeClick(clickEvent): void {
    this.treeConfigFacade.query.isEditing$.pipe(take(1)).subscribe((val) => {
      if (val === false) {
        this.treeConfigFacade.clickNode(clickEvent);
      }
    });
  }

  public onToggleNode(node) {
    this.treeConfigFacade.toggleNode({ node });
  }

  public onTopLevelAssetNodeClick(clickEvent): void {
    this.treeConfigFacade.command.setTopLevelAsset(clickEvent);
  }

  deleteNode(): void {
    this.treeConfigFacade.deleteNode();
  }

  public onCustomerNodeClick(node: CustomerNode): void {
    this.treeConfigFacade.command.setSelectedCustomer(node);
  }
  saveNodeName(): void {
    const nodeName = this.nodeForm.get('nodeName').value;
    this.treeConfigFacade.updateNodeName(nodeName);

    this.nodeForm.get('nodeName').markAsPristine();
    this.treeConfigFacade.command.setIsEditing(false);
    this.nodeForm.get('assetNodeBehaviorId').enable();
  }

  cancelNodeNameEdit() {
    this.treeConfigFacade.command.setIsEditing(false);
    this.nodeForm.get('assetNodeBehaviorId').enable();
    this.treeConfigFacade.query.selectedTreeNode$
      .pipe(take(1))
      .subscribe((selectedNode) => {
        this.nodeForm.patchValue({
          nodeName: selectedNode.NodeAbbrev,
          assetNodeBehaviorId: selectedNode.AssetNodeBehaviorId
            ? selectedNode.AssetNodeBehaviorId.toString()
            : '',
        });
      });
  }

  onClose() {
    this.dialogRef.close();
  }

  selectTextOnFocus(element: HTMLInputElement) {
    element?.select();
  }

  onTreeNameEditBlur(element: HTMLInputElement, event: FocusEvent) {
    const treeSaveBtn = event?.relatedTarget as any;
    const treeCancelBtn = event?.relatedTarget as any;
    if (treeSaveBtn?.id === 'treeSaveBtn') {
      event.preventDefault();
      return;
    }

    if (treeCancelBtn?.id === 'treeCancelBtn') {
      event.preventDefault();
      return;
    }

    if (!this.treeNameFormControl.pristine) {
      this.dialog
        .open(ConfirmationDialogComponent, {
          disableClose: true,
          width: '350px',
          data: {
            isYesNo: true,
            title: 'Canceling Tree Name Edit?',
            message: 'Click Yes to cancel edit, or Cancel to continue editing.',
          },
        })
        .afterClosed()
        .subscribe((r) => {
          if (r) {
            this.cancelTreeNameEdit();
          } else {
            element?.select();
            element?.focus();
          }
        });
    }
  }

  onNodeNameEditBlur(element: HTMLInputElement, event: FocusEvent) {
    const nodeNameBtn = event?.relatedTarget as any;
    const nodeNameCancelBtn = event?.relatedTarget as any;

    if (nodeNameBtn?.id === 'nodeNameBtn') {
      event.preventDefault();
      return;
    }

    if (nodeNameCancelBtn?.id === 'nodeNameCancelBtn') {
      event.preventDefault();
      return;
    }

    if (!this.nodeForm.get('nodeName').pristine) {
      this.dialog
        .open(ConfirmationDialogComponent, {
          disableClose: true,
          width: '350px',
          data: {
            isYesNo: true,
            title: 'Canceling Node Name Edit?',
            message: 'Click Yes to cancel edit, or Cancel to continue editing.',
          },
        })
        .afterClosed()
        .subscribe((r) => {
          if (r) {
            this.cancelNodeNameEdit();
          } else {
            element?.select();
            element?.focus();
          }
        });
    }
  }
}
