import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TreeConfigComponent } from './tree-config.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TreeConfigFacade } from '../../../service/tree-config/tree-config.facade';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { HttpClient } from '@microsoft/signalr';
import { ModelService } from '../../../service/model.service';
import { AssetsCoreService, UserAdminCoreService } from '@atonix/shared/api';
import { AssetTreeDBService } from '@atonix/atx-indexed-db';
import { APP_CONFIG } from '@atonix/app-config';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  FakeAppConfig,
  FakeModelService,
  FakeAssetTreeDBService,
} from '@atonix/shared/test';
import { AuthFacade } from '@atonix/shared/state/auth';
import { BehaviorSubject } from 'rxjs';
import { ISecurityRights } from '@atonix/atx-core';

describe('TreeConfigComponent', () => {
  let component: TreeConfigComponent;
  let fixture: ComponentFixture<TreeConfigComponent>;
  let mockTreeConfigFacade: TreeConfigFacade;
  let assetsCoreServiceMock: AssetsCoreService;
  let userAdminCoreServiceMock: UserAdminCoreService;
  let mockAuthFacade: AuthFacade;

  beforeEach(waitForAsync(() => {
    assetsCoreServiceMock = createMock(AssetsCoreService);
    userAdminCoreServiceMock = createMock(UserAdminCoreService);
    mockAuthFacade = createMockWithValues(AuthFacade, {
      userAdminUtilityAccess$: new BehaviorSubject<ISecurityRights>({
        CanAdd: true,
        CanAdmin: true,
        CanDelete: true,
        CanEdit: true,
        CanView: true,
      }),
    });
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [TreeConfigComponent],
      providers: [
        TreeConfigFacade,
        HttpClient,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatDialogRef, useValue: {} },
        { provide: MatDialog, useValue: {} },
        { provide: ModelService, useClass: FakeModelService },
        { provide: AssetTreeDBService, useClass: FakeAssetTreeDBService },
        { provide: APP_CONFIG, useValue: FakeAppConfig },
        { provide: AssetsCoreService, userValue: assetsCoreServiceMock },
        { provide: UserAdminCoreService, userValue: userAdminCoreServiceMock },
        { provide: AuthFacade, useValue: mockAuthFacade },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeConfigComponent);
    component = fixture.componentInstance;
    mockTreeConfigFacade = TestBed.inject(TreeConfigFacade);
    fixture.detectChanges();
  });

  it('should create', () => {
    mockAuthFacade.getAllUIAccess();
    expect(component).toBeTruthy();
  });
});
