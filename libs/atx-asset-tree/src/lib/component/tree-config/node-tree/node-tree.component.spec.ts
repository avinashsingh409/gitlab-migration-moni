import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NodeTreeComponent } from './node-tree.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NodeTreeFacade } from '../../../service/tree-config/node-tree.facade';

describe('NodeTreeComponent', () => {
  let component: NodeTreeComponent;
  let fixture: ComponentFixture<NodeTreeComponent>;
  let nodeTreeFacadeMock: NodeTreeFacade;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [NodeTreeComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatDialogRef, useValue: {} },
        { provide: NodeTreeFacade, useValue: nodeTreeFacadeMock },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
