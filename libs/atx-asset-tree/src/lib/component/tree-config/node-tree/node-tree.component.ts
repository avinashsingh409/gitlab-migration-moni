import { FlatTreeControl } from '@angular/cdk/tree';
import {
  Component,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Subject } from 'rxjs';
import { isNil } from '@atonix/atx-core';
import { cloneDeep } from 'lodash';
import { NodeTreeDataSource } from '../../../service/tree-config/node-tree-datasource';
import { NodeTreeNode } from '../../../service/tree-config/tree-config.models';
import { NodeTreeFacade } from '../../../service/tree-config/node-tree.facade';

@Component({
  selector: 'atx-tree-config-node-tree',
  templateUrl: './node-tree.component.html',
  styleUrls: ['./node-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeTreeComponent implements OnChanges, OnDestroy {
  @Input() nodes: NodeTreeNode[];
  @Input() nodesStore: NodeTreeNode[];
  @Input() selectedNode: NodeTreeNode;
  @Output() nodeClicked = new EventEmitter<{
    clickedNode: NodeTreeNode;
    index: number;
  }>();
  @Output() toggleNode = new EventEmitter<NodeTreeNode>();
  public treeControl: FlatTreeControl<NodeTreeNode>;
  public dataSource: NodeTreeDataSource;
  public displayedColumns: string[] = ['NodeAbbrev'];
  public onDestroy$ = new Subject<void>();

  constructor(private facade: NodeTreeFacade) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!isNil(changes)) {
      if (
        !isNil(changes.nodes?.currentValue) ||
        changes.nodes?.currentValue?.length > 0
      ) {
        if (changes.nodes.currentValue !== changes.nodes.previousValue) {
          const nodeStores = changes.nodesStore.currentValue as NodeTreeNode[];
          const nodes = changes.nodes.currentValue as NodeTreeNode[];

          this.facade.setNodesStore(cloneDeep(nodeStores));
          this.facade.setNodes(cloneDeep(nodes));

          this.treeControl = new FlatTreeControl<NodeTreeNode>(
            (node: NodeTreeNode) => node.Level,
            (node: NodeTreeNode) => !isNil(node.Children)
          );
          this.dataSource = new NodeTreeDataSource(
            this.treeControl,
            this.facade
          );
        }
      }
    }
  }

  selectNode(
    event: PointerEvent | MouseEvent,
    node: NodeTreeNode,
    i: number
  ): void {
    event.stopPropagation();

    this.nodeClicked.emit({ clickedNode: node, index: i });
  }

  onExpandNode(event: PointerEvent | MouseEvent, node: NodeTreeNode): void {
    event.stopPropagation();
    this.treeControl.toggle(node);
    this.toggleNode.emit(node);
  }

  isNodeExpanded(node: NodeTreeNode): boolean {
    if (node.IsExpanded) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
