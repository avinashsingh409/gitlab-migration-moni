import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TreeWrapperComponent } from './tree-wrapper.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('TreeWrapperComponent', () => {
  let component: TreeWrapperComponent;
  let fixture: ComponentFixture<TreeWrapperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [TreeWrapperComponent],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
