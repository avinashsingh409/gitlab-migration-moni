import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { ITreeConfiguration } from '../../../model/tree-state';
import { ITreeStateChange } from '../../../model/tree-state-change';
import { Subject } from 'rxjs';

@Component({
  selector: 'atx-tree-wrapper',
  templateUrl: './tree-wrapper.component.html',
  styleUrls: ['./tree-wrapper.component.scss'],
})
export class TreeWrapperComponent implements OnDestroy {
  @Input() assetTreeConfiguration: ITreeConfiguration;
  @Output() assetTreeStateChange = new EventEmitter<ITreeStateChange>();
  public unsubscribe$ = new Subject<void>();

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.assetTreeStateChange.emit(change);
  }
}
