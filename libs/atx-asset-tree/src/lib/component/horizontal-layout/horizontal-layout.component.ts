import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from '@angular/core';
import { CdkDrag } from '@angular/cdk/drag-drop';
import { MatDrawerContainer } from '@angular/material/sidenav';
import { TrayState } from '../../model/tray-state';

@Component({
  selector: 'atx-horizontal-layout',
  templateUrl: './horizontal-layout.component.html',
  styleUrls: ['./horizontal-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HorizontalLayoutComponent implements OnInit, OnChanges {
  @Input() showTimeSlider: boolean;

  @Input() leftTraySize: number;
  @Input() leftTrayMode: TrayState;
  @Input() leftTrayOpen: boolean;
  @Input() leftTraySizeMin: number;

  @Input() rightTrayMode: TrayState;
  @Input() rightTraySize: number;
  @Input() rightTrayOpen: boolean;

  @Output() sizeChange = new EventEmitter<number>();
  @Output() mainNavclicked = new EventEmitter();

  // This is going to grab the MatDrawerContainer so we can reflow the contents once the
  // size of the drawer changes.
  @ViewChild(MatDrawerContainer)
  horizontalLayout: MatDrawerContainer;

  // This is kept locally so that we can manipulate it during drag.  When this value changes in the
  // configuration the local copy is updated.  We don't want to have a 2-way binding.
  public _leftTraySize = 250;
  public _rightTraySize = 300;
  // This is a local variable that gets set when the drag starts.  It allows us to calculate the position
  // of the slider based on the total travel of the drag.
  private dragStartedTraySize: number;

  ngOnInit() {
    // Initial update of the internal tray size.
    this._leftTraySize = this.leftTraySize ?? 250;
    this._rightTraySize = this.rightTraySize ?? 300;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.leftTraySize) {
      // Update the internal tray size when the user changes the value in the configuration.
      // The other configuration values are directly bound in the HTML.
      this._leftTraySize = changes.leftTraySize.currentValue ?? 250;
    }
    if (changes.rightTraySize) {
      this._rightTraySize = changes.rightTraySize.currentValue ?? 300;
    }
  }

  contentClicked() {
    // if open asset tree navigator && asset tree unpinned
    if (this.leftTrayOpen && this.leftTrayMode === 'over') {
      this.mainNavclicked.emit();
    }
  }

  // Grab the initial position on drag start.  Then we can calculate the current position based on the
  // total drag travel.
  public dragStarted() {
    this.dragStartedTraySize = this._leftTraySize;
  }

  public dragMoved(event) {
    // Update the tray size.  This is an internal value and not reported back to the parent until the user drops the drag.
    // If we have a minimum size set it is respected. Otherwise the minimum size is 0.
    this._leftTraySize = Math.max(
      this.leftTraySizeMin ?? 0,
      this.dragStartedTraySize + event.distance.x
    );

    // The drag directive calcualtes an offset based on the drag position.  If we allow that to be applied
    // the drag will be twice what it should be.  So we strip that out and just keep the bar set to the width
    // of the panel.
    (event.source as CdkDrag).reset();
  }

  // When the drag is released we want to re-flow the contents of the drawer
  // We also want to report the new size to the parent.  If the parent doesn't update the size in the
  // configuration the component should still work, but they probably should update the size.
  public dragReleased(event) {
    (event.source as CdkDrag).reset();
    if (this.horizontalLayout) {
      this.horizontalLayout.updateContentMargins();
    }
    this.sizeChange.emit(this._leftTraySize);
  }
}
