import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HorizontalLayoutComponent } from './horizontal-layout.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import {
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
describe('HorizontalLayoutComponent', () => {
  let component: HorizontalLayoutComponent;
  let fixture: ComponentFixture<HorizontalLayoutComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [HorizontalLayoutComponent],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatchMedia, useClass: MockMatchMedia },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorizontalLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should drag stuff', () => {
    expect(component._leftTraySize).toEqual(250);
    jest.spyOn(component, 'dragMoved');
    jest.spyOn(component, 'dragReleased');
    const reset1 = jest.fn();
    const reset2 = jest.fn();
    component.dragStarted();
    component.dragMoved({
      distance: { x: 10 },
      source: { reset: reset1 },
    });
    component.dragReleased({
      source: { reset: reset2 },
    });
    expect(reset1).toHaveBeenCalled();
    expect(reset2).toHaveBeenCalled();
    expect(component._leftTraySize).toEqual(260);
  });

  it('should init', () => {
    expect(component._leftTraySize).toEqual(250);
    component.leftTraySize = 2;
    component.ngOnInit();
    expect(component._leftTraySize).toEqual(2);
  });

  it('should init info tray size', () => {
    expect(component._rightTraySize).toEqual(300);
    component.rightTraySize = 2;
    component.ngOnInit();
    expect(component._rightTraySize).toEqual(2);
  });

  it('should change', () => {
    expect(component._leftTraySize).toEqual(250);
    component.ngOnChanges({
      leftTraySize: {
        previousValue: null,
        firstChange: true,
        isFirstChange: () => true,
        currentValue: 2,
      },
    });
    expect(component._leftTraySize).toEqual(2);
  });

  it('should drag stuff to the max', () => {
    component.leftTraySize = 250;
    component.leftTraySizeMin = 100;
    component.ngOnInit();

    expect(component._leftTraySize).toEqual(250);
    jest.spyOn(component, 'dragMoved');
    jest.spyOn(component, 'dragReleased');
    const reset1 = jest.fn();
    const reset2 = jest.fn();
    component.dragStarted();
    component.dragMoved({
      distance: { x: -1000 },
      source: { reset: reset1 },
    });
    component.dragReleased({
      source: { reset: reset2 },
    });
    expect(reset1).toHaveBeenCalled();
    expect(reset2).toHaveBeenCalled();
    expect(component._leftTraySize).toEqual(100);
  });

  it('should drag stuff but not too much', () => {
    component.leftTraySize = 250;
    component.leftTraySizeMin = null;
    component.ngOnInit();
    expect(component._leftTraySize).toEqual(250);
    jest.spyOn(component, 'dragMoved');
    jest.spyOn(component, 'dragReleased');
    const reset1 = jest.fn();
    const reset2 = jest.fn();
    component.dragStarted();
    component.dragMoved({
      distance: { x: -1000 },
      source: { reset: reset1 },
    });
    component.dragReleased({
      source: { reset: reset2 },
    });
    expect(reset1).toHaveBeenCalled();
    expect(reset2).toHaveBeenCalled();
    expect(component._leftTraySize).toEqual(0);
  });
});
