import {
  ComponentFixture,
  TestBed,
  tick,
  fakeAsync,
  waitForAsync,
} from '@angular/core/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { MATERIAL_SANITY_CHECKS, MatOption } from '@angular/material/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AssetTreeComponent } from './asset-tree.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ITreeNode } from '../../model/tree-node';
import { createNewNode } from '../../service/tree-builder.service';
import { IAutoCompleteItem } from '@atonix/atx-core';
import {
  ɵMatchMedia as MatchMedia,
  ɵMockMatchMedia as MockMatchMedia,
} from '@angular/flex-layout';
describe('AssetTreeComponent', () => {
  let component: AssetTreeComponent;
  let fixture: ComponentFixture<AssetTreeComponent>;

  const node: ITreeNode = createNewNode({
    uniqueKey: 'a',
    parentUniqueKey: null,
    nodeAbbrev: 'Testing',
    level: 1,
    selected: true,
    symbol: 'expanded',
    displayOrder: 0,
    data: {
      NodeId: 'a',
      TreeId: 'a123',
      NodeAbbrev: 'Testing',
      NodeDesc: 'Testing',
      ParentNodeId: null,
      NodeTypeId: 1,
      DisplayOrder: 0,
      AssetId: 123456,
      AssetNodeBehaviorId: 1234,
      Asset: null,
      ParentUniqueKey: null,
      UniqueKey: 'a',
      AssetGuid: 'test',
      ReferencedBy: null,
      HasChildren: true,
    },
  });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        FontAwesomeModule,
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,

        NoopAnimationsModule,
        DragDropModule,

        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatchMedia, useClass: MockMatchMedia },
      ],
      declarations: [AssetTreeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should emit state change when tree selection', () => {
    jest.spyOn(component.stateChange, 'emit');
    component.treeSelectionChanged({
      name: 'Default Tree',
      value: 'default',
    });
    expect(component.stateChange.emit).toHaveBeenCalledWith({
      event: 'LoadTree',
      newValue: 'default',
    });
  });

  it('should emit state change when icon is click', () => {
    jest.spyOn(component.stateChange, 'emit');
    component.iconClicked(node);
    expect(component.stateChange.emit).toHaveBeenCalledWith({
      event: 'IconClicked',
      newValue: node.data,
    });
  });

  it('should emit state change when pin is toggled', () => {
    jest.spyOn(component.stateChange, 'emit');
    component.pin();
    expect(component.stateChange.emit).toHaveBeenCalledWith({
      event: 'PinToggle',
      newValue: undefined,
    });
  });

  it('should return unique key', () => {
    const result = component.track(null, node);
    expect(result).toBe('a');
  });

  it('should do autocomplete stuff', fakeAsync(() => {
    jest.spyOn(component.stateChange, 'emit');
    component.searchLostFocus();
    tick(500);
    expect(component.stateChange.emit).toHaveBeenCalledTimes(1);
  }));

  it('should do autocomplete stuff', fakeAsync(() => {
    jest.spyOn(component.stateChange, 'emit');
    component.autoComplete.patchValue('SOMETHING');
    component.searchChange();
    tick(1000);
    expect(component.stateChange.emit).toHaveBeenCalledTimes(1);
  }));

  it('should select an autocomplete value', () => {
    jest.spyOn(component.stateChange, 'emit');

    component.autoCompleteSelection({
      source: {} as MatAutocomplete,
      option: {} as MatOption,
    });

    expect(component.stateChange.emit).toHaveBeenCalledTimes(2);
  });

  it('should select a node', () => {
    jest.spyOn(component.stateChange, 'emit');

    component.select(
      {
        ctrlKey: true,
        altKey: false,
      } as MouseEvent,
      {
        uniqueKey: '1234',
      } as ITreeNode
    );

    expect(component.stateChange.emit).toHaveBeenCalledWith({
      event: 'SelectAsset',
      newValue: { ids: '1234', multiSelect: true },
    });
  });

  it('should track', () => {
    expect(component.track(1, { uniqueKey: '1234' } as ITreeNode)).toEqual(
      '1234'
    );
    expect(component.track(1, null)).toEqual(null);
  });

  it('should track auto complete stuff', () => {
    expect(
      component.acTrack(1, { UniqueID: '1234' } as IAutoCompleteItem)
    ).toEqual('1234');
    expect(component.acTrack(1, null)).toEqual(null);
  });

  it('should respect changes unrelated to auto complete', () => {
    component.autoComplete.patchValue('1234');

    component.ngOnChanges({});

    expect(component.autoComplete.value).toEqual('1234');
  });

  it('should respect changes related to auto complete', () => {
    component.autoComplete.patchValue('1234');

    component.ngOnChanges({
      treeState: {
        previousValue: null,
        isFirstChange: () => false,
        firstChange: false,
        currentValue: { autoCompleteValue: 'something' },
      },
    });

    expect(component.autoComplete.value).toEqual('something');
  });
});
