import {
  Component,
  Input,
  Output,
  OnChanges,
  SimpleChange,
  EventEmitter,
  OnDestroy,
  ChangeDetectionStrategy,
  ElementRef,
  AfterViewChecked,
} from '@angular/core';
import { faCaretRight } from '@fortawesome/free-solid-svg-icons/faCaretRight';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons/faCaretDown';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';
import { faDotCircle } from '@fortawesome/free-solid-svg-icons/faDotCircle';
import { faThumbtack } from '@fortawesome/free-solid-svg-icons/faThumbtack';
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus';
import { faRedoAlt } from '@fortawesome/free-solid-svg-icons/faRedoAlt';
import { ITreeConfiguration } from '../../model/tree-state';
import { ITreeStateChange } from '../../model/tree-state-change';
import { ITreeNode } from '../../model/tree-node';
import {
  autocompleteChanged,
  selectTree,
  selectAsset,
  clickIcon,
  togglePin,
  toggleEdit,
  toggleReload,
} from '../../service/asset-tree.service';
import { UntypedFormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil, delay } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { IAutoCompleteItem } from '@atonix/atx-core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TreeConfigComponent } from '../tree-config/modal/tree-config.component';

@Component({
  selector: 'atx-asset-tree',
  templateUrl: './asset-tree.component.html',
  styleUrls: ['./asset-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssetTreeComponent
  implements OnChanges, OnDestroy, AfterViewChecked
{
  // The treeState should contain all of the information necessary for the tree to display
  // It is not necessarily all information in the system, just what is necessary for the tree to display
  @Input() treeState: ITreeConfiguration;

  // This option will make the selected asset node on top of the scrollable view
  @Input() needScroll = false;

  // This option will drive enabling or disabling the mat-select for Trees
  @Input() disableAdHocDropdown = false;

  // A stream of change events initiated from the asset tree.  The container must process these into something useful.
  @Output() stateChange = new EventEmitter<ITreeStateChange>();

  // This is the window for user input.  The value won't be actually sent to the
  // server until this time has elapsed.  That way we don't make a call on each user keystroke.
  private autoCompleteInputDebounceTime = 1000;

  // This is how long we wait before processing the lost focus event.
  // 500 is visible, something like 100 would probably be more appropriate.
  private lostFocusDelay = 500;

  // The form control object is what backs the html input control.  Because they are bound we only
  // have to update this autoComplete object and it will be reflected on the UI input box.
  public autoComplete: UntypedFormControl;

  // This observable exists so we can do a debounce on the input from the user.
  public autoCompleteSubject$: Subject<string>;

  // Font Awesome objects.  These create the fontawesome characters on the screen.
  faCaretRight: IconDefinition = faCaretRight;
  faCaretDown: IconDefinition = faCaretDown;
  faSpinner: IconDefinition = faSpinner;
  faDotCircle: IconDefinition = faDotCircle;
  faThumbtack: IconDefinition = faThumbtack;
  faMinus: IconDefinition = faMinus;
  faRedoAlt: IconDefinition = faRedoAlt;
  spinReload = false;

  // Kills subscriptions when the component is destroyed.
  private unsubscribe$ = new Subject<void>();

  // This is used to delay the blur event.  We have a problem where the blur happens before the
  // select, so the select doesn't actually do anything.  This will delay the blur so the select happens
  // first.  Then the blur clears the input.
  private searchLostFocus$ = new Subject<void>();

  constructor(private el: ElementRef, private dialog: MatDialog) {
    this.autoCompleteSubject$ = new Subject<string>();
    this.autoComplete = new UntypedFormControl();

    // Will make sure the user input is chunked so they don't send every keystroke to the server.
    this.autoCompleteSubject$
      .pipe(
        debounceTime(this.autoCompleteInputDebounceTime),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((autoCompleteValue) => {
        this.stateChange.emit(autocompleteChanged(autoCompleteValue));
      });

    // Delays the blur on the input box so the select can happen before the input is cleared.
    this.searchLostFocus$
      .pipe(delay(this.lostFocusDelay), takeUntil(this.unsubscribe$))
      .subscribe((n) => {
        this.stateChange.emit(autocompleteChanged());
      });
  }

  setEventData(event: any, data: ITreeNode) {
    event.dataTransfer.setData('nodeData', JSON.stringify(data));
  }

  // Kill the subscriptions
  ngOnDestroy() {
    this.unsubscribe$.next();
  }

  // If the autocomplete value changes we want to update the input box to reflect that.
  // Most of the time the input will change within the component so this will be setting
  // the value to what it is already set to.  But if the value is cleared or for some reason
  // set to something different this will update the input box.
  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (changes && changes.treeState) {
      if (
        this.autoComplete.value !==
        changes.treeState.currentValue.autoCompleteValue
      ) {
        this.autoComplete.setValue(
          changes.treeState.currentValue.autoCompleteValue
        );
      }
    }
  }

  ngAfterViewChecked() {
    if (this.needScroll) {
      const selectedNode: HTMLElement =
        this.el.nativeElement.querySelector('.selectedAsset');
      if (selectedNode) {
        selectedNode.scrollIntoView({ block: 'center' });
        this.needScroll = false;
      }
    }
  }

  // User has changed the selected tree, so emit the event.
  treeSelectionChanged($event) {
    this.stateChange.emit(selectTree($event.value));
  }

  // The input box has lost focus.  So we just want to put that event in the observable so
  // it can be delayed while we take care of the potential selecting of a new asset.
  searchLostFocus() {
    this.searchLostFocus$.next();
  }

  // This will fire on each keypress.  We put it into an observable with a debounce so it won't
  // be too chatty.
  searchChange() {
    this.autoCompleteSubject$.next(this.autoComplete.value);
  }

  // The user has chosen a new asset.  We want to emit an event for selecting the asset and an
  // event for clearing the autocomplete text.
  autoCompleteSelection(event: MatAutocompleteSelectedEvent) {
    this.stateChange.emit(autocompleteChanged(null));
    this.stateChange.emit(selectAsset(event.option.value, false));
  }

  // The user has clicked the expand/contract on a specific asset node.
  iconClicked(node: ITreeNode) {
    this.stateChange.emit(clickIcon(node));
  }

  // The user has selected a node.
  select($event: MouseEvent, node: ITreeNode) {
    this.stateChange.emit(
      selectAsset(node.uniqueKey, $event.ctrlKey || $event.altKey)
    );
  }

  reload() {
    this.spinReload = true;

    setTimeout(() => {
      this.spinReload = false;
    }, 100);

    this.stateChange.emit(toggleReload());
  }

  // The user has clicked the pin button.  The navigation component actually takes care of the pinning
  // behavior, so the calling application will need to wire all of that together.
  pin() {
    this.stateChange.emit(togglePin());
  }

  configure() {
    const dialogConfig = new MatDialogConfig();
    this.dialog.open(TreeConfigComponent, dialogConfig);
  }

  // This is a function that is used to make the display of nodes more efficient.  Angular can reuse
  // html elements if it has a way to track the data.  This allows Angular to know what data is the same
  // and what is different.
  track(index: number, item: ITreeNode) {
    if (item) {
      return item.uniqueKey;
    } else {
      return null;
    }
  }

  // This allows the quick search drop down to track its elements.
  acTrack(index: number, item: IAutoCompleteItem) {
    if (item) {
      return item.UniqueID;
    } else {
      return null;
    }
  }
}
