# atx-asset-tree

## Running unit tests

Run `nx test atx-asset-tree` to execute the unit tests.

## Usage - Configuration

### Asset Tree

To include the asset treecomponent in an application include the following html tag.

```html
<atx-asset-tree
  [treeState]="treeState"
  (stateChange)="onAssetTreeStateChange($event)"
>
</atx-asset-tree>
```

The asset tree accepts a configuration object in the following format:

```ts
interface ITree {
  name: string;
  id: string;
}

interface ITreeNode {
  uniqueKey: string;
  parentUniqueKey: string;
  nodeAbbrev: string;
  displayOrder: number;
  level: number;
  selected: boolean;
  retrieved: boolean;
  symbol: 'collapsed' | 'expanded' | 'loading' | 'nothing';
  data?: any;
}

interface ITreeConfiguration {
  // If true the ad-hoc tree dropdown will be displayed.
  showTreeSelector: boolean;

  // The list of trees to display in the ad-hoc tree dropdown.
  trees: ITree[];

  // The ID of the tree that is currently selected
  selectedTree: string;

  // The value displayed in the autocomplete box.
  autoCompleteValue: string;

  // True if the results of an autocomplete webservice call are pending.
  autoCompletePending: boolean;

  // The list of assets in the dropdown of the autocomplete box.,
  autoCompleteAssets: IAutoCompleteItem[];

  // The nodes to be displayed.  This is a flat list derived from the full list of assets in memory.
  nodes: ITreeNode[];

  // True if the pin icon displays the pin.  Used to indicate whether the layout should put the
  // asset tree over or next to other data on the screen.
  pin: boolean;

  // True if assets are being loaded.  Used to apply styling.
  loadingAssets: boolean;

  // True if an entire tree is being loaded.  Used to apply styling.
  loadingTree: boolean;

  // If true the drag/drop is enabled on this control.
  canDrag?: boolean;

  // List of targets for dropping.  Only these targets will accept assets from the tree.
  dropTargets?: string[];

  // Permissions
  canView: boolean;
  canAdd: boolean;
  canEdit: boolean;
  canDelete: boolean;

  // If true, the path of non-selected nodes will be collapsed.
  collapseOthers: boolean;

  // IF false the asset tree can be closed

  // If true the configure button will be hidden
  hideConfigureButton: boolean;
}
```

Many applications will keep a list of nodes in the state beyond what is currently displayed. The following data structure exists to assist in that bookkeeping task.

```ts
export interface ITreeNodes extends EntityState<ITreeNode> {
  // This is the list of the root assets of the tree
  rootAssets: string[];

  // This dictionary allows you to easily look up the children of an asset.
  childrenDict: Dictionary<string[]>;

  // This is the ID of the tree.  Many applications will want to only keep the selected
  // tree in memory, so the tree ID may not be necessary.  Others may want to keep all
  // of the trees.  In that case the tree ID will allow them to be kept in a dictionary.
  treeID: string;

  // This is the App Context used when the tree was created.  It is included here so that
  // if we want to see if we already have the requested tree loaded we can.
  appContext?: string;
}

export interface ITreeState {
  treeConfiguration: ITreeConfiguration;
  treeNodes: ITreeNodes;
  hasDefaultSelectedAsset: boolean;
}
```

The nodes array of the ITreeConfiguration interface represents the assets that will be displayed. Maintaining the asset tree in memory and deriving the displayed list from the total set of assets must be done outside of this component.

Some methods have been included to aid in the generation of the nodes list.  
The TreeBuilder object contains functionality based on the EntityState object to aid in maintaining the asset store.

- assetTreeAdapter - An object based on the EntityAdapter object that provides some methods for manipulating the TreeBuilder object.
- createTreeBuilder - Method that returns a new TreeBuilder object
- selectNode - Turns a IAtxAssetTreeNode object that is returned from the server into an object usable by the asset tree.
- addRootNodes - Inserts root nodes into the asset tree
- addNode - Inserts a single child node into the asset tree.
- addNodes - Inserts multiple nodes into the asset tree.
- updateNode - Updates the contents of a node with new values. You must supply an Update<IAssetTreeNode> object to this method and any non-undefined properties will be set.
- expandNode - Expands or collapses a node. If the node children have not been retrieved this will set the node to loading.
- isRetrieved - Indicates whether a node has children that need to be loaded.
- getSelectors - Retrieves the selectors for the assetTreeAdapter.
- getChildren - Returns the children of a specified node
- isAnyNodeLoading - Returns true if any node is in the loading state.
- selectTreeNodes - Derives a nodes array from the node store.

Other methods are provided for helping to maintain the configuration of the object.

- getDefaultAssetTree - Returns a default configuration for the component
- alterAssetTreeState - Provides default methods for changing the configuration from standard events.
- selectTree, nodesChanged, togglePin, clickIcon, permissionsChanged, loadingAssetChanged, loadingTreeChanged - All generate IAssetTreeStateChange objects indicating the specified change.

The Model service contains functions that interact with the server

- getPermissions - Retrieves the permissions of the tree from the server.
- getInitialValues - Retrieves the initial configuration for the tree from the server.
- getChildren - Retrieves the children of a node from the server.
- getSelectedTree - Retrieves a specific set of root nodes from the server.

### Horizontal Layout

The horizontal layout component exists to provide the ability to place the asset tree and asset info tray on the left and right of the content of an application.

The following code provides a minimal configuration of the horizontal layout.

```ts
<atx-horizontal-layout
  [layoutState]="horizontalLayoutState"
  (sizeChange)="assetTreeSizeChanged($event)"
  [showTimeSlider]="showTimeSlider"
>

  <atx-asset-tree
    class="side-nav"
    [treeState]="treeConfiguration$ | async"
    (stateChange)="onTreeStateChange($event)"
  ></atx-asset-tree>

  <atx-asset-info-tray
    class="right-side-nav"
    [infoTrayState]="infoTrayState$ | async"
    (infoTrayStateChange)="onAssetInfoTrayStateChange($event)"
  ></atx-asset-info-tray>

  <div class="main-nav">
  Content!
  </div>

</atx-horizontal-layout>
```

The component uses the classes 'side-nav', 'right-side-nav' and 'main-nav' to identify the pieces of content to apply to the three regions it maintains. The side-nav and right-side-nav components are optional as is the showTimeSlider parameter.

- showTimeSlider is a boolean indicating whether the horizontal layout should apply a style which adds an 80 px gap at the bottom of the layout to accomodate the time slider for applications where the time slider appears.

The horizontal layout state is of the following definition.

```ts
export interface IHorizontalLayoutConfig {
  // Size of the left tray (typically the asset tree)
  leftTraySize: number;

  // Whether the left tray is open or closed
  isOpen: boolean;

  // How the contents interact with the trays.
  mode: 'over' | 'push' | 'side';

  // The left panel is resizable.  This represents a minimum
  minLeftPanelSize: number;

  // Whether the right tray is open or closed
  isOpenInfoTray: boolean;

  // The default size of the right tray
  infoTraySize: number;
}
```

### Asset Tray

The asset tray is a component intended to display information about a single asset or a group of assets. It is intended to be opened or closed as needed by the user.

The info tray state is defined as indicated below

```ts
export interface IInfoTrayState {
  showInfoTray: boolean;
  asset: IAssetData;
  assetIDs: string[];
  attributes: ITempAttributeType[];
  selectedTab: string;
}

export interface IAssetData {
  selectedAssetIDs: string[];
  assetID: string;
  assetAttributes: IAssetAndAttributes;
  hasImages: boolean;
  imagesPath: IImageData[];
  assetName: string;
}

export interface IImageData {
  imageBlobUrl: SafeStyle;
  imageCaption: string;
  displayOrder: number;
}
```
