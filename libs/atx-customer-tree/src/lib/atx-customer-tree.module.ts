import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerTreeComponent } from './component/customer-tree/customer-tree.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { CustomerTreeFacade } from './service/customer-tree/customer-tree.service.facade';
import { CustomerTreeService } from './service/customer-tree/customer-tree.service';
import { TopLevelAssetTreeComponent } from './component/top-level-asset-tree/top-level-asset-tree.component';
import { TopLevelAssetTreeDataSource } from './service/top-level-assets/top-level-asset-tree.service.datasource';
import { TopLevelAssetTreeFacade } from './service/top-level-assets/top-level-asset-tree.service.facade';

@NgModule({
  imports: [CommonModule, AtxMaterialModule],
  declarations: [CustomerTreeComponent, TopLevelAssetTreeComponent],
  providers: [
    CustomerTreeFacade,
    CustomerTreeService,
    TopLevelAssetTreeDataSource,
    TopLevelAssetTreeFacade,
  ],
  exports: [CustomerTreeComponent, TopLevelAssetTreeComponent],
})
export class AtxCustomerTreeModule {}
