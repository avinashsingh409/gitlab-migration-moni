import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ControlContainer } from '@angular/forms';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { AtxMaterialModule } from '@atonix/atx-material';
import { ToastService } from '@atonix/shared/utils';
import { createMockWithValues } from '@testing-library/angular/jest-utils';

import { TopLevelAssetTreeComponent } from './top-level-asset-tree.component';

describe('TopLevelAssetTreeComponent', () => {
  let component: TopLevelAssetTreeComponent;
  let fixture: ComponentFixture<TopLevelAssetTreeComponent>;
  let mockToastService: ToastService;

  beforeEach(waitForAsync(() => {
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });
    TestBed.configureTestingModule({
      imports: [
        AtxMaterialModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
      ],
      declarations: [TopLevelAssetTreeComponent],
      providers: [
        ControlContainer,
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        { provide: ToastService, useValue: mockToastService },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopLevelAssetTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
