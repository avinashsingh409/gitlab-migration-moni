import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerTreeComponent } from './customer-tree.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG } from '@atonix/app-config';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AtxMaterialModule } from '@atonix/atx-material';
import { FakeAppConfig } from '@atonix/shared/test';

describe('CustomerTreeComponent', () => {
  let component: CustomerTreeComponent;
  let fixture: ComponentFixture<CustomerTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AtxMaterialModule, HttpClientTestingModule],
      declarations: [CustomerTreeComponent],
      providers: [{ provide: APP_CONFIG, useValue: FakeAppConfig }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(CustomerTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
