import {
  CollectionViewer,
  DataSource,
  SelectionChange,
} from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable, OnDestroy } from '@angular/core';
import { TopLevelAssetNode } from '@atonix/shared/api';
import { Observable, merge, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { TopLevelAssetTreeFacade } from './top-level-asset-tree.service.facade';

@Injectable({
  providedIn: 'root',
})
export class TopLevelAssetTreeDataSource
  implements DataSource<TopLevelAssetNode>, OnDestroy
{
  public data: TopLevelAssetNode[] = [];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private treeControl: FlatTreeControl<TopLevelAssetNode>,
    private facade: TopLevelAssetTreeFacade
  ) {
    facade.query.nodes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((nodes: TopLevelAssetNode[]) => {
        this.data = nodes;
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<TopLevelAssetNode[]> {
    this.treeControl.expansionModel.changed.subscribe(
      (change: SelectionChange<TopLevelAssetNode>) => {
        if (
          (change as SelectionChange<TopLevelAssetNode>).added ||
          (change as SelectionChange<TopLevelAssetNode>).removed
        ) {
          this.handleTreeControl(change as SelectionChange<TopLevelAssetNode>);
        }
      }
    );

    return merge(collectionViewer.viewChange, this.facade.query.nodes$).pipe(
      map((_) => this.data)
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  disconnect(_collectionViewer: CollectionViewer): void {}

  handleTreeControl(change: SelectionChange<TopLevelAssetNode>): void {
    if (change.added) {
      change.added.forEach((node: TopLevelAssetNode) => {
        this.facade.toggleNode(node, true);
      });
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach((node: TopLevelAssetNode) =>
          this.facade.toggleNode(node, false)
        );
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
