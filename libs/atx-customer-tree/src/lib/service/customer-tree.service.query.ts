import { CustomerNode } from '@atonix/shared/api';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { CustomerTreeState } from './customer-tree.service.facade';

export class CustomerTreeQuery {
  constructor(private store: BehaviorSubject<CustomerTreeState>) {}
  private state$ = this.store.asObservable();

  readonly nodes$ = this.state$.pipe(
    map((state) => state.nodes),
    distinctUntilChanged()
  );

  readonly vm$: Observable<CustomerTreeState> = combineLatest([
    this.nodes$,
  ]).pipe(
    map(([nodes]) => {
      return {
        nodes,
      } as CustomerTreeState;
    })
  );

  getNodes = (): CustomerNode[] => {
    let nodes: CustomerNode[] = [];
    this.nodes$.pipe(take(1)).subscribe((queryNodes: CustomerNode[]) => {
      nodes = queryNodes;
    });
    return nodes;
  };
}
