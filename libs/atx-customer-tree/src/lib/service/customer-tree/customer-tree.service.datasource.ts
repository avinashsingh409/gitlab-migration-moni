import {
  CollectionViewer,
  DataSource,
  SelectionChange,
} from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable, OnDestroy } from '@angular/core';
import { CustomerNode } from '@atonix/shared/api';
import { Observable, merge, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { CustomerTreeFacade } from './customer-tree.service.facade';

@Injectable({
  providedIn: 'root',
})
export class CustomerTreeDataSource
  implements DataSource<CustomerNode>, OnDestroy
{
  public data: CustomerNode[] = [];
  private unsubscribe$ = new Subject<void>();

  constructor(
    private treeControl: FlatTreeControl<CustomerNode>,
    private facade: CustomerTreeFacade
  ) {
    facade.query.nodes$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((nodes: CustomerNode[]) => {
        this.data = nodes;

        this.data.map((node: CustomerNode) => {
          if (node.IsExpanded && !this.treeControl.isExpanded(node)) {
            this.treeControl.expand(node);
          }
        });
      });
  }

  connect(collectionViewer: CollectionViewer): Observable<CustomerNode[]> {
    this.treeControl.expansionModel.changed.subscribe(
      (change: SelectionChange<CustomerNode>) => {
        if (
          (change as SelectionChange<CustomerNode>).added ||
          (change as SelectionChange<CustomerNode>).removed
        ) {
          this.handleTreeControl(change as SelectionChange<CustomerNode>);
        }
      }
    );

    return merge(collectionViewer.viewChange, this.facade.query.nodes$).pipe(
      map((_) => this.data)
    );
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  disconnect(_collectionViewer: CollectionViewer): void {}

  handleTreeControl(change: SelectionChange<CustomerNode>): void {
    if (change.added) {
      change.added.forEach((node: CustomerNode) => {
        if (!node.IsExpanded) {
          this.facade.toggleNode(node, true);
        }
      });
    }
    if (change.removed) {
      change.removed
        .slice()
        .reverse()
        .forEach((node: CustomerNode) => {
          if (node.IsExpanded) {
            this.facade.toggleNode(node, false);
          }
        });
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
