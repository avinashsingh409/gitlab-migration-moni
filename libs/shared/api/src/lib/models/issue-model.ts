import { AssetIssue, IssueType } from './asset-issue';

export interface IssueCauseType {
  IssueCauseTypeID: number;
  IssueTypeID: number;
  IssueCauseDesc: string;
  IssueCauseAbbrev: string;
  IssueCauseTypeVariableTypeMaps: any[];
  IsBVApproved: boolean;
  CreateDate?: any;
  ChangeDate?: any;
  CreatedByUserID: number;
  ChangedByUserID: number;
  displayOrder?: number;
}

export interface AssetIssueWithOptions {
  Issue: AssetIssue;
  ActivityStatuses: ActivityStatus[];
  ResolutionStatuses: ResolutionStatus[];
  Impacts: Impact[];
  IssueTypes: IssueType[];
  ActivityStatusTransitions: any[];
  ResolutionStatusTransitions: ResolutionStatusTransition[];
  Priorities: string[];
}

export interface ActivityStatus {
  AssetIssueActivityStatusTypeID: number;
  AssetIssueActivityStatusTypeDesc: string;
  IsOpen: boolean;
  IsDefault: boolean;
}

export interface ResolutionStatus {
  AssetIssueResolutionStatusTypeID: number;
  AssetIssueResolutionStatusTypeDesc: string;
  DisplayOrder: number;
}

export interface Impact {
  AssetIssueImpactTypeID: number;
  AssetIssueImpactCategoryTypeID: number;
  AssetIssueImpactTypeDesc: string;
  AssetIssueImpactCategoryTypeDesc: string;
  AssetIssueImpactCategoryTypeCalcDesc: string;
  AssetIssueImpactCategoryAndTypeDesc: string;
  Units: string;
  DisplayOrder: number;
}

export interface ResolutionStatusTransition {
  From: From;
  To: To;
}

export interface From {
  AssetIssueResolutionStatusTypeID: number;
  AssetIssueResolutionStatusTypeDesc: string;
  DisplayOrder: number;
}

export interface To {
  AssetIssueResolutionStatusTypeID: number;
  AssetIssueResolutionStatusTypeDesc: string;
  DisplayOrder: number;
}

export interface IssueClassType {
  AssetIssueClassTypeID: number;
  Description: string;
  Abbrev: string;
}

export interface IssueClassesAndCategories {
  AssetIssueCategoryTypeID: number;
  CategoryAbbrev: string;
  CategoryDesc: string;
  IssueClassTypeID: number;
  IssueClassType: IssueClassType;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
}

export interface AssetIssueCategoryType {
  AssetIssueCategoryTypeID: number;
  CategoryAbbrev: string;
  CategoryDesc: string;
  IssueClassTypeID: number;
  IssueClassType: IssueClassType;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
}

export interface ImpactMap {
  AssetIssueAssetIssueImpactTypeMapID: number;
  AssetIssueID: number;
  AssetIssueImpactTypeID: number;
  Impact?: number;
  Impact_Cost?: number;
  InputString: string;
  CategoryID: number;
  CategoryDesc: string;
  CategoryCalcDesc: string;
  Units: string;
  ImpactTypeDesc: string;
  DisplayOrder: number;
  CalculatorAvailable: boolean;
  Impact_Cost_Monthly?: number;
  ImpactScenarioID?: number;
  ImpactScenarioPercentLikelihood?: number;
}
