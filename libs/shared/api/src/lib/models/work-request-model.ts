export interface IWorkRequest {
  workRequestId?: string;
  workReferenceId?: string;
  createDate?: string;
  title: string;
  description: string;
  assetIssueId: string;
  location?: string;
  equipment?: string;
  status?: string;
  link?: string;
  priority?: string;
  lastUpdatedAt?: Date;
  lastQueriedAt?: Date;
}

export interface IWorkRequestLoading {
  loading?: boolean;
  error?: Error;
  swrLoading?: boolean;
  swrError?: Error;
  polling?: boolean;
  pollingError?: Error;
}
export interface IExternalAssetsLoading {
  loading?: boolean;
  error?: Error;
}

export interface ISelectedExternalAssetLoading {
  loading?: boolean;
  error?: Error;
}
