export interface INotificationItem {
  NotificationID: number;
  NotificationGroupID: string;
  NotificationTypeID: number;
  Message: string;
  Options: string;
  Result: string;
  Viewed: boolean;
  CreatedDate: Date;
  CreatedBy: string;
  ChangedDate: Date;
  ChangedBy: string;
}
