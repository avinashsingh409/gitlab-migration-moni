export interface ISendIssueResult {
  ErrorMessage: string;
  SuccessMessage: string;
}
