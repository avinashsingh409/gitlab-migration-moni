import { IAsset, IAssetVariableTypeTagMap, IWidget } from '@atonix/atx-core';

export interface IReport {
  ReportID: number;
  ReportDesc: string;
  ReportAbbrev: string;
  DisplayOrder: number;
  AssetID: number;
  Asset: IAsset;
  Title: string;
  Subtitle: string;
  CreatedBy: string;
  ChangedBy: string;
  CreateDate: string | Date;
  ChangeDate: string | Date;
  Sections: IReportSection[];
  NodeID: string;
  full?: boolean;
  loading?: boolean;
  Kind?: string;
}

export interface IReportSection {
  ReportSectionID: number;
  ReportID: number;
  Title: string;
  Subtitle: string;
  Height: string;
  DisplayOrder: number;
  Color: string;
  Widgets: IWidget[];
  backgroundColor?: string;
  sectionHeight: string;
  Kind?: string;
}

export interface IKpiAggregatedValue {
  Min?: number;
  Max?: number;
  First?: number;
  Last?: number;
  Average?: number;
  Sum?: number;
  Delta?: number;
  Median?: number;
  Mode?: number;
  FirstStdAbove?: number;
  SecondStdAbove?: number;
  FirstStdBelow?: number;
  SecondStdBelow?: number;
  Percentile?: number;
}

export interface IAssetVariableTypeTagMapToAggregate {
  AssetVariableTypeTagMap: IAssetVariableTypeTagMap;
  AggregatedValue: IKpiAggregatedValue;
}

export interface IKpiData {
  AssetId: number;
  Values: IAssetVariableTypeTagMapToAggregate[];
  Min?: number;
  Max?: number;
  DisplayText: string;
  Units: string;
  DisplayOrder?: number;
  Map: IAssetVariableTypeTagMap;
}
