export interface ICorrelationData {
  Name: EForecastTrend | EROCTrend;
  Info: string;
  Data: ITimeseriesDatapoint[];
}

export enum EForecastTrend {
  Forecast = 'FC_Forecast',
  GreaterThanThreshold = 'FC_GreaterThanThreshold',
  LessThanThreshold = 'FC_LessThanThreshold',
  LastRegression = 'FC_LastRegression',
  Horizon = 'FC_Horizon',
  Exceedance = 'FC_Exceedance',
}

export enum EROCTrend {
  Regression = 'RC_Regression',
  Expected = 'RC_Expected',
  Upper = 'RC_Upper',
  Lower = 'RC_Lower',
  LastRegression = 'RC_LastRegression',
}

export interface ITimeseriesDatapoint {
  Timestamp: string;
  Value: number;
}

export const CorrelationTrendNameMapping: Record<
  EForecastTrend | EROCTrend,
  string
> = {
  [EForecastTrend.Forecast]: 'Forecast',
  [EForecastTrend.GreaterThanThreshold]: 'Greater Than Threshold',
  [EForecastTrend.LessThanThreshold]: 'Less Than Threshold',
  [EForecastTrend.LastRegression]: 'Last Prediction',
  [EForecastTrend.Horizon]: 'Earliest Acceptable Intercept',
  [EForecastTrend.Exceedance]: 'Forecasted Intercept',
  [EROCTrend.Regression]: 'Actual Rate of Change',
  [EROCTrend.Expected]: 'Expected Rate of Change',
  [EROCTrend.Upper]: 'ROC Upper',
  [EROCTrend.Lower]: 'ROC Lower',
  [EROCTrend.LastRegression]: 'ROC Last Regression',
};

export const CorrelationTrendColorMapping: Record<
  EForecastTrend | EROCTrend,
  string
> = {
  [EForecastTrend.Forecast]: 'blue',
  [EForecastTrend.GreaterThanThreshold]: 'red',
  [EForecastTrend.LessThanThreshold]: 'red',
  [EForecastTrend.LastRegression]: '#00FF00',
  [EForecastTrend.Horizon]: '#ff0000',
  [EForecastTrend.Exceedance]: '',
  [EROCTrend.Regression]: '#0000FF',
  [EROCTrend.Expected]: '#ff0000',
  [EROCTrend.Upper]: '#FFA500',
  [EROCTrend.Lower]: '#FFA500',
  [EROCTrend.LastRegression]: '',
};
