import {
  ICategory,
  IAssetClassTypeVariableTypeMap,
  IAssetVariableTypeTagMap,
  INDDataPoint,
  IDataPoint,
  IAssetMeasurementsSet,
} from '@atonix/atx-core';

export interface IAssetModelAlertMetric {
  Expected: number;
  Min: number;
  Max: number;
  Actual: number;
  EvaluationStatus: number;
  TimeStamp: Date;
}

export interface IDateRange {
  From: Date;
  To: Date;
}

export interface ModelConfiguration {
  modelID: number;
  modelName: string;
  modelDesc: string;
}

export interface IAssetModelChartingData {
  SeriesList: IAssetModelAlertMetric[];
  OutOfBoundDates: IDateRange[];
  AboveExpectedDates: IDateRange[];
  Events: any[];
  Model: INDModel;
  start?: Date;
  end?: Date;
  Min?: number;
  Max?: number;
  // for front end purposes only
  OriginalMin?: number;
  OriginalMax?: number;
}

export interface INDModelType {
  ModelTypeID: number;
  ModelTypeAbbrev: string;
  ModelTypeDesc: string;
  ModelTypeEdictID: number;
  ModelTypeEdict: INDModelTypeEdict;
  DisplayOrder: number;
}

export interface INDModelTypeEdict {
  ModelTypeEdictID: number;
  InputsAvailable: boolean;
  AltAreaAvailable: boolean;
  AltFrequencyAvailable: boolean;
  AltOscillationAvailable: boolean;
  AltHighHighAvailable: boolean;
  AltLowLowAvailable: boolean;
  AltFrozenDataAvailable: boolean;
  AnoMAEAvailable: boolean;
  AnoBiasAvailable: boolean;
  AnoFixedLimitAvailable: boolean;
  DataAvailable: boolean;
  MustHaveAnoMAEOrAnoBias: boolean;
  MustHaveAnoFixedLimit: boolean;
  MustHaveAltFrozenData: boolean;
  PredictiveMethodTypeID?: number;
  PredictiveMethodType: INDPredictiveMethodType;
}

export interface INDPredictiveMethodType {
  PredictiveMethodTypeID: number;
  PredictiveMethodAbbrev: string;
  PredictiveMethodDesc: string;
}

export interface INDModel {
  ModelID: number;
  NDProjectPath: string;
  NDProjectName: string;
  NDResultsPath: string;
  ModelExtID: string;
  ModelName: string;
  ModelDesc: string;
  ModelTemplateID?: number;
  ModelTemplate: INDModelTemplate;
  AssetVariableTypeTagMapID: number;
  DependentVariable: IAssetVariableTypeTagMap;
  ModelTypeID: number;
  ModelType: INDModelType;
  OpModeTypeMaps: Array<INDModelOpModeTypeMap>;
  OpModeParamsID: number;
  OpModeParams: INDOpModeParams;
  DefaultOpModeParams: INDOpModeParams;
  ModelInputSeries: Array<IModelInputVariableSeries>;
  PredictiveMethodMaps: Array<INDModelPredictiveMethodMap>;
  Temporal: boolean;
  Built: boolean;
  BuiltSinceTime?: Date;
  Active: boolean;
  ActiveSinceTime?: Date;
  PreserveAlertStatus?: boolean;
  AlertStatusTypeID?: number;
  IsPublic: boolean;
  DisplayOrder?: number;
  CreatedByUserID: number;
  CreateDate: Date;
  ChangedByUserID: number;
  ChangeDate: Date;
  ModelRequest: IModelRequest;
  DisplayModelPath: string;

  IsStandard: boolean;
  LastErrorMessage: string;
  AutoRetrain: boolean;

  NDProjectId?: string;
  Locked: boolean;
  LockReasonID?: number;
  LockedSinceTime?: Date;

  ComputedCriticality: INDModelCriticality;
  DefaultCriticality: INDModelCriticality;
  Categories: ICategory[];
}

export interface INDModelPredictiveMethodMap {
  ModelPredictiveMethodMapID: number;
  ModelID: number;
  PredictiveMethodTypeID: number;
  PredictiveMethodType: INDPredictiveMethodType;
  Score: number;
  Active: boolean;
  CommonTracesID: string;
  CommonTracesFastID: string;
  PredictiveMethodOutput: IPredictiveMethod;
}

export interface IPredictiveMethod {
  PredictiveMethodName: string;
  Score: number;
  IncludedInputs: Array<INDDataPoint>;
  OutputDetails: IOutputDetails;
  IsSelected: boolean;
}

export interface IOutputDetails {
  RValue: number;
  RMSE: number;
  MAE: number;
  AssetMeasurementsSet: IAssetMeasurementsSet;
}

export interface ITagRetrievalError {
  ErrorType: number; // 0:No feasible archive found;  1:Exceeding maximum allowable data points
  TagNames: string[];
}

export interface IAnnotation {
  AnnotationId: number;
  AssetId: number;
  TagId: number;
  AnnotationCategoryId: number;
  StartDate: Date;
  EndDate: Date;
  AnnotationProperties: IAnnotationProperty[];
}

export interface IAnnotationProperty {
  AnnotationId: number;
  AnnotationTypePropertyId: number;
  ValueAsText: string;
}

export interface IAssetMeasurements {
  Tag: any;
  Data: IDataPoint[];
  SensorID: number;
  proposedSeriesMin?: number;
  proposedSeriesMax?: number;
  actualSeriesMin?: number;
  actualSeriesMax?: number;
  Statistics: any;
  Archive: string;
  PinID?: any;
  displayText?: string;
  color?: string;
  units?: string;
}

export interface IModelRequest {
  Build?: IBuildAction;
  Save?: IAction;
  Activate?: IAction;
  Retrain?: IAction;
  RequestTime?: Date;
  PostStatus?: string;
  GetStatusPayloadSchema?: string;
  PostFulfillment?: string;
  GetFulfillmentPayloadSchema?: string;
  ModelRequesterID?: number;
}

export interface IAction {
  Requested: boolean;
  Fulfilled?: boolean;
  FulfilledStatus?: string;
  FulfillmentStartTime?: Date;
  FulfillmentEndTime?: Date;
}

export interface IBuildAction extends IAction {
  TrainingPeriodStartTime?: Date;
  TrainingPeriodEndTime?: Date;
  PredictiveMethodOutputs?: Array<IPredictiveMethod>;
  SelectedPredictiveMethod?: IPredictiveMethod;
}

export interface IModelInputVariableSeries {
  Series: number;
  DataPoints: Array<INDDataPoint>;
  PreprocessingAction: string;
  RecInputVariableMapID: number;

  Drops: Array<IAssetVariableTypeTagMap>;
  ShowOver: boolean;
}

export interface INDOpModeParams {
  OpModeParamsID: number;
  AreaFastResponseValue?: number;
  AreaFastResponseTime?: number;
  AreaFastResponseTimeTemporalTypeID?: number;
  AreaSlowResponseValue?: number;
  AreaSlowResponseTime?: number;
  AreaSlowResponseTimeTemporalTypeID?: number;
  FrequencyTrigger?: number;
  FrequencyDuration?: number;
  FrequencyDurationTemporalTypeID?: number;
  OscillationTrigger?: number;
  OscillationDuration?: number;
  OscillationDurationTemporalTypeID?: number;
  HighHighTrigger?: number;
  HighHighUseMAE?: boolean;
  LowLowTrigger?: number;
  LowLowUseMAE?: boolean;
  FrozenDataDuration?: number;
  FrozenDataDurationTemporalTypeID?: number;
  ConcerningBoundaryMultiplier?: number;
  LessConcerningBoundaryMultiplier?: number;
  MAEUpper?: number;
  MAELower?: number;
  BiasUpper?: number;
  BiasLower?: number;
  FixedLimitUpper?: number;
  FixedLimitLower?: number;
  TrnSampleRate?: number;
  TrnSampleRateTemporalTypeID?: number;
  TrnAverageData?: boolean;
  TrnDuration?: number;
  TrnDurationTemporalTypeID?: number;
  TrnLag?: number;
  TrnLagTemporalTypeID?: number;
  TrnMinDataPoints?: number;
  AltMinFrequency?: number;
  AltMinFrequencyTemporalTypeID?: number;
  AltMaxFrequency?: number;
  AltMaxFrequencyTemporalTypeID?: number;
  CreatedByUserID: number;
  CreateDate: Date;
  ChangedByUserID: number;
  ChangeDate: Date;
}

export interface INDModelOpModeTypeMap {
  ModelOpModeTypeMapID: number;
  ModelID: number;
  OpModeTypeID: number;
  OpModeType: INDOpModeType;
}

export interface INDOpModeType {
  OpModeTypeID: number;
  OpModeTypeAbbrev: string;
  OpModeTypeDesc: string;
  Priority: number;
  DisplayOrder: number;
}

export interface INDModelCriticality {
  ModelID: number;
  UseDefault: boolean;
  UpperCriticality?: number;
  LowerCriticality?: number;
  CreatedByUsername: string;
  CreateDate: Date;
  ChangedByUsername: string;
  ChangeDate: Date;
  HasMultipleUpperCriticality: boolean;
  HasMultipleLowerCriticality: boolean;
}

export interface INDModelTemplate {
  ModelTemplateID: number;
  AssetClassTypeVariableTypeID: number;
  DependentVariable: IAssetClassTypeVariableTypeMap;
  ParentAssetClassTypeID: number;
  ModelTypeID: number;
  ModelType: INDModelType;
  Temporal: boolean;
  ConcerningBoundTypeID: number;
  ConcerningBoundType: INDConcerningBoundType;
  NDModelTemplateOpModeMaps: Array<INDModelTemplateOpModeMap>;
  CreatedByUserID: number;
  CreateDate: Date;
  ChangedByUserID: number;
  ChangeDate: number;
  UpperCriticality?: number;
  LowerCriticality?: number;
}

export interface INDConcerningBoundType {
  ConcerningBoundTypeID: number;
  ConcerningBoundTypeAbbrev: string;
  ConcerningBoundTypeDesc: string;
  DisplayOrder: number;
}

export interface INDModelTemplateOpModeMap {
  ModelTemplateOpModeMapID: number;
  ModelTemplateID: number;
  OpModeTypeID;
  number;
}

export interface INDModelActionItemAnalysis {
  Upper: number;
  Lower: number;
  Actual: number;
  Expected: number;
  ModelActionItemTypeID: string;
  ChangeDate: Date;
}

export interface INDModelActionItemAnalysisResult {
  Results: INDModelActionItemAnalysis[];
}
