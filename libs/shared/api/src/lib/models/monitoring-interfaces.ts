import { IAsset, IAssetVariableTypeTagMap, ICategory } from '@atonix/atx-core';
import { INDModel, INDOpModeType } from './charting-data';

export interface INDModelsListResult {
  Models: Array<INDModelSummary>;
  NumModels: number;
}

export interface INDModelSummary {
  RowNumber: number;
  ModelID: number;
  ModelExtID: string;
  Active: boolean;
  Built: boolean;
  TagName: string;
  TagDesc: string;
  ModelName: string;
  ModelTypeAbbrev: string;
  Score?: number;
  EngUnits: string;
  AssetID: number;
  AssetDesc: string;
  AssetGUID: string;
  AssetAbbrev: string;
  UnitID: number;
  UnitAbbrev: string;
  PrioritizedOpMode: string;
  MAELower?: number;
  MAEUpper?: number;
  ActionDiagnose: boolean;
  ActionWatch: boolean;
  Alert: boolean;
  ActionModelMaintenance: boolean;
  HasOpenIssue: boolean;
  UpperLimit?: number;
  LowerLimit?: number;
  AlertStatusType: string;
  AlertPriority: string;
  PercentOutOfBounds?: number;
  Actual?: number;
  Expected?: number;
  AddedToAlerts?: Date;
  ModelActionItemPriorityTypeID: number;
  NotePriority: string;
  LastNote: string;
  LastNoteDate?: Date;
  TagGroup: string;
  OutOfBounds: boolean;
  Ignore: boolean;
  StatusInfo: string;
  CurrentBuildStatus: string;
  BuildStatus: string;
  LiveStatus: string;
  TagNDId?: string;
  IsStandard: boolean;
  AlertTypes: INDAlertStatusType[];
  AssetCriticality: number;
  ModelCriticality: number;
  Categories: ICategory[];
  PercentExpected: number;
  Criticality: number;
  DiagnosticDrilldownURL: string;
  ModelConfigURL: string;
  ModelDesc: string;
}

export interface INDModelSummaryGroup {
  ModelID?: number;
  ModelExtID?: string;
  Active?: any;
  Built?: any;
  TagName?: string;
  TagDesc?: string;
  ModelName?: string;
  ModelTypeID?: number;
  ModelTypeAbbrev?: string;
  Score?: number;
  EngUnits?: string;
  AssetID?: number;
  AssetGUID?: string;
  AssetAbbrev?: string;
  UnitID?: number;
  UnitAbbrev?: string;
  PrioritizedOpMode?: string;
  MAELower?: number;
  MAEUppe?: number;
  ActionDiagnose?: any;
  ActionWatch?: any;
  ActionWatchOverride?: any;
  ActionModelMaintenance?: any;
  HasOpenIssue?: any;
  AlertStatusType?: string;
  AlertTypes?: any[];
  AlertPriority?: string;
  AlertPrioritySeverity?: number;
  PercentOutOfBounds?: number;
  Actual?: number;
  Expected?: number;
  AddedToAlerts?: Date;
  Alert?: any;
  Alerts?: number;
  UpperLimit?: number;
  UpperSeverity?: number;
  LowerLimit?: number;
  LowerSeverity?: number;
  HiHiAlert?: any;
  HiHiAlertSinceTime?: Date;
  LowLowAlert?: any;
  LowLowAlertSinceTime?: Date;
  AreaFastResponseAlert?: any;
  AreaFastResponseAlertSinceTime?: Date;
  AreaSlowResponseAlert?: any;
  AreaSlowResponseAlertSinceTime?: Date;
  FrequencyAlert?: any;
  FrequencyAlertSinceTime?: Date;
  OscillationAlert?: any;
  OscillationAlertSinceTime?: Date;
  FrozenDataAlert?: any;
  FrozenDataAlertSinceTime?: Date;
  WatchOverrideAlert?: any;
  WatchOverrideAlertSinceTime?: Date;
  IgnoreOverrideAlert?: any;
  IgnoreOverrideAlertSinceTime?: Date;
  ModelActionItemPriorityTypeID?: number;
  NotePriority?: string;
  LastNote?: string;
  LastNoteDate?: Date;
  TagGroup?: string;
  OutOfBounds?: any;
  Ignore?: any;
  StatusInfo?: string;
  CurrentBuildStatus?: string;
  BuildStatus?: string;
  ReferenceModelId?: string;
  TagNDId?: string;
  IsStandard?: any;
  AssetCriticality?: number;
  ModelCriticality?: number;
  Categories?: any[];
  PercentExpected?: number;
  ModelConfigURL?: string;
  DiagnosticDrilldownURL?: string;
  Count?: number;
  Criticality?: number;
  ModelDesc?: string;
}

export interface INDModelsFilter {
  startAssetID?: number;
  assetVariableTypeTagMapID?: number;
  active?: string;
  built?: string;
  hasCalcPoint?: string;
  tagName?: string;
  assetCriticalityID?: Array<number>;
  modelCriticalityID?: Array<number>;
  tagDesc?: string;
  modelName?: string;
  engUnits?: string;
  modelTypeID: Array<number>;
  modelOpModeTypeID: Array<number>;
  isDiagnose?: string;
  isWatch?: string;
  isMaintenance?: string;
  isOpenIssue?: string;
  priority?: number;
  isAlert?: string;
  isIgnore?: string;
  currentBuildStatusTypeID: Array<number>;

  evaluationErrorMessage?: string;
  outOfBounds?: string;
  alertPriority?: number;
  alertType?: string;
  addedToAlertsStart?: Date;
  addedToAlertsEnd?: Date;
  addedToAlertsStartIndicatorPopup?: any;
  addedToAlertsEndIndicatorPopup?: any;
  Criticality: number;
}

export interface INDAlertStatusType {
  AlertStatusTypeID: number;
  AlertStatusTypeAbbrev: string;
  AlertStatusTypeDesc: string;
  AlertStatusPriorityTypeID: number;
}

export interface IAlertModelsListResult {
  Models: Array<IAlertsModelSummary>;
  NumModels: number;
}

// This interface has been reconciled to only include properties populated by the API
// 9-Mar-2021 RMB
export interface IAlertsModelSummary {
  ActionDiagnose: boolean;
  ActionModelMaintenance: boolean;
  ActionWatch: boolean;
  Actual?: number;
  AddedToAlerts?: Date;
  Alert: boolean;
  AlertPriority: string;
  AlertTypesActive: string;
  AssetAbbrev: string;
  AssetClassType: string;
  AssetClassTypeID: number;
  AssetCriticality: number;
  AssetDesc: string;
  AssetGUID: string;
  AssetID: number;
  EngUnits: string;
  Expected?: number;
  Ignore: boolean;
  LastNote: string;
  LastNoteDate?: Date;
  LowerLimit?: number;
  ModelCriticality: number;
  ModelExtID: string;
  ModelID: number;
  ModelName: string;
  ModelTypeAbbrev: string;
  NotePriority: string;
  OpenIssues: number;
  OutOfBounds: boolean;
  PercentExpected: number;
  PercentOutOfBounds?: number;
  PrioritizedOpMode: string;
  Score?: number;
  StatusInfo: string;
  TagGroup: string;
  TagName: string;
  UnitAbbrev: string;
  UpperLimit?: number;
  VariableType: string;
  VariableTypeID: number;
  Criticality: number;
}

// This interface has been reconciled to only include properties populated by the API
// 9-Mar-2021 RMB
export interface IAlertsModelSummaryGroup {
  ActionDiagnose: any;
  ActionModelMaintenance: any;
  ActionWatch: any;
  Actual?: number;
  AddedToAlerts?: Date;
  Alert: any;
  AlertPriority: string;
  AssetClassType: string;
  AssetClassTypeID: any;
  AssetCriticality?: number;
  AssetDesc: string;
  AssetGUID: string;
  Count: number;
  EngUnits: string;
  Expected?: number;
  Ignore: any;
  LastNote: string;
  LastNoteDate?: Date;
  LowerLimit?: number;
  ModelCriticality?: number;
  ModelName: string;
  ModelTypeAbbrev: string;
  NotePriority: string;
  OutOfBounds: any;
  PercentExpected?: number;
  PercentOutOfBounds?: number;
  PrioritizedOpMode: string;
  Score?: number;
  StatusInfo: string;
  TagGroup: string;
  TagName: string;
  UnitAbbrev: string;
  UpperLimit?: number;
  VariableType: string;
  VariableTypeID: any;
  Criticality?: number;
}

export interface IAlertStatusType {
  AlertStatusTypeID: number;
  AlertStatusTypeAbbrev: string;
  AlertStatusTypeDesc: string;
  AlertStatusPriorityTypeID: number;
}

export interface INDModelWithMessage {
  Model: INDModel;
  Message: string;
}

export interface INDQuickDeployModelCreationResults {
  GoodModels: Array<INDModel>;
  OverrideModels: Array<INDModelWithMessage>;
  BadModels: Array<INDModelWithMessage>;
  QDID: string;
  QDAssetGUID: string;
  QDAstVarTypeTagMapID?: number;
  OpModeTypeID?: number;
  QDStartTime: string;
}

export interface INDAlerNotification {
  NDAlertNotificationID: string;
  AlertStatusTypeID: number;
  ModelID: number;
  NotificationFrequency: number;
  NotificationMode: boolean;
  NotificationSummary: string;
  NotificationTitle: string;
}
