export interface IUser {
  firstName: string;
  lastName: string;
  email: string;
  customerId?: string;
}

export interface IUserPreferences {
  Theme: string;
  Logo: string;
  Title: string;
}

export interface IUserAccount {
  UserName: string;
  Email: string;
  Name: string;
  AccountStatus: string;
  MFAStatus: string;
  EmailVerified: boolean;
  Phone: string;
  PhoneVerified: boolean;

  // This will be assigned by the API /Account/IsUserFederated
  IsUserFederated?: boolean;
}
