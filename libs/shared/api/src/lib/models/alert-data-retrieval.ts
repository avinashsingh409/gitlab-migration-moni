import { INDModelSummary, INDModelSummaryGroup } from './monitoring-interfaces';

export interface IAlertDataRetrieval {
  Results: { Summary: INDModelSummary[] | INDModelSummaryGroup[] };
  NumModelSummary: number;
  NumRowSummary: number;
}

export interface INDModelActionItem {
  AssetID: number;
  ChangeDate: Date;
  ChangedByUserID: number;
  CreateDate: Date;
  CreatedByUserID: number;
  EngUnits: string;
  Executor: string;
  Favorite: boolean;
  ModelActionItemArchiveID?: number;
  ModelActionItemPriorityType: any;
  ModelActionItemPriorityTypeID?: number;
  ModelActionItemType: any;
  ModelActionItemTypeAbbrev?: string;
  ModelActionItemTypeID: number;
  ModelConditionNoteID: number;
  ModelID: number;
  NoteText: string;
  WatchCriteriaType: any;
  WatchCriteriaTypeID?: number;
  WatchDuration?: number;
  WatchDurationTemporalType: any;
  WatchDurationTemporalTypeID?: number;
  WatchEmailIfAlert: boolean;
  WatchLimit?: number;
  WatchLimitMeetsTemporalType?: number;
  WatchLimitMeetsTemporalTypeID?: number;
  WatchRelativeToExpected: boolean;
  WatchRelativeToExpected2: boolean;
  WatchCriteriaTypeID2?: number;
  WatchLimit2?: number;
}

export interface IActionItem {
  NoteText?: string;
  ModelID?: number;
  ModelActionItemPriorityTypeId?: number;
  ModelActionItemTypeId?: number;
  WatchDuration?: number;
  WatchDurationTemporalTypeId?: number;
  WatchCriteriaTypeId?: number;
  WatchLimit?: number;
  WatchRelativeToExpected?: boolean;
  WatchEmailIfAlert?: boolean;
  WatchLimitMeetsTemporalTypeId?: number;
  Favorite?: boolean;
  WatchRelativeToExpected2?: boolean;
  WatchCriteriaTypeId2?: number;
  WatchLimit2?: number;
}

export interface IAlertTimelineRequest {
  page: number;
  itemsPerPage: number;
  sort: string;
  sortOrder: boolean;
  FilterParameters: {
    startAssetID: number;
    modelID: number;
    issueCreated: Date;
    issueClosed: Date;
  };
  getTotalItems: boolean;
}

export interface IAlertTimelineResult {
  Items: INDModelActionItem[];
  TotalItems: number;
}

export interface IModelHistoryFilters {
  historyTypes: string[] | null;
  start: Date | null;
  end: Date | null;
  user: string | null;
  note: string | null;
  favorite: boolean | null;
}
