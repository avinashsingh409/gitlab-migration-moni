export interface IModelConfigSummary {
  ModelID: number;
  ModelExtID: string;
  Active: boolean;
  Built: boolean;
  ModelName: string;
  ModelTypeID?: number;
  ModelTypeAbbrev: string;
  TagName: string;
  TagDesc: string;
  TagGroup: string;
  Score?: number;
  EngUnits: string;
  PrioritizedOpMode: string;
  AssetDesc: string;
  AssetAbbrev: string;
  AssetID?: number;
  AssetGUID: string;
  UnitAbbrev: string;
  UnitID?: number;
  MAELower?: number;
  MAEUpper?: number;
  LastBuildStatus: string;
  LastBuildDate: Date;
  LastUpdateDate: Date;
  IsStandard: boolean;
  AssetCriticality?: number;
  ModelCriticality?: number;
  ActionModelMaintenance?: boolean;
  MaintenanceAge?: number;
  ThirtyDayCountOfActions?: number;
  LowerBoundTolerance?: string;
  UpperBoundTolerance?: string;
  InterQuartileRange?: string;
  LowerCriticality?: number;
  UpperCriticality?: number;
  VariableTypeDesc?: string;
  AssetTypeDesc?: string;
}

export interface IModelConfigTagListSummary {
  TagID: number;
  TagGlobalID: string;
  TagName: string;
  TagDesc: string;
  TagGroup: string;
  TagMapID: number;
  VariableID: number;
  VariableDesc: string;
  AssetDesc: string;
  AssetAbbrev: string;
  AssetID?: number;
  AssetGUID: string;
  AssetCriticality?: number;
  EngUnits: string;
  ModelsList: IModelConfigTagModels[];
  AssetTypeDesc: string;
  UnitAbbrev: string;
  AssetPath: string;
}

export interface IModelConfigTagModels {
  ModelID: number;
  ModelExtID: string;
  ModelName: string;
  EngUnits: string;
  ModelTypeAbbrev: string;
  ModelTypeID?: number;
  ActionModelMaintenance?: boolean;
  IsStandard: boolean;
  Active: boolean;
  Built: boolean;
  LastBuildStatus: string;
  LastBuildDate?: Date;
  LastUpdateDate?: Date;
  Score?: number;
  OpModeTypeID?: number;
  AssetGuid?: string;
  OpModeTypes: string;
}

export interface IModelConfigDataRetrieval {
  Success: boolean;
  StatusCode: number;
  Type: any;
  Count: number;
  Results: any[];
}

export enum ETagType {
  ModelInputTags = 0,
  DependentTags = 1,
}
