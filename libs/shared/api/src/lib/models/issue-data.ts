export interface IIssueSummary {
  AssetIssueGUID: string;
  AssetIssueID: number;
  CategoryDesc: string;
  CategoryID: number;
  IssueTitle: string;
  IssueSummary: string;
  ShortSummary: string;
  Description: string;
  Client: string;
  StationGroup: string;
  Station: string;
  StationAssetAbbrev: string;
  Unit: string;
  UnitAssetAbbrev: string;
  System: string;
  SystemAssetClassTypeID: number;
  Asset: string;
  AssetClassTypeID: number;
  AssetID: number;
  ImpactsString: string;
  ImpactTotal: number;
  Confidence_Pct: number;
  IssueTypeDesc: string;
  IssueTypeID: number;
  IssueClassTypeID: number;
  IssueClassTypeDescription: string;
  Priority: string;
  CreatedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  ChangedBy: string;
  ActivityStatus: string;
  ResolutionStatus: string;
  CloseDate: Date;
  IsSubscribed: boolean;
  GlobalID: string;
  OpenDuration: number;
  OpenDate: Date;
  AssignedTo: string;
  Scorecard: boolean;
  ResolveBy: Date;
  AssetClassTypeAbbrev: string;
  AssetClassTypeDesc: string;
  AssetTypeAbbrev: string;
  AssetTypeDesc: string;
  CreatedByUserID: number;
  CanEdit: boolean;
  CanDelete: boolean;
  CanEditIssueStatus: boolean;
}

export interface IIssueSummaryGroup {
  AssetIssueGUID: string;
  AssetIssueID: number;
  CategoryDesc: string;
  CategoryID: number;
  IssueTitle: string;
  IssueSummary: string;
  ShortSummary: string;
  Description: string;
  Client: string;
  StationGroup: string;
  Station: string;
  StationAssetAbbrev: string;
  Unit: string;
  UnitAssetAbbrev: string;
  System: string;
  SystemAssetClassTypeID: number;
  Asset: string;
  AssetClassTypeID: number;
  AssetID: number;
  ImpactsString: string;
  ImpactTotal: number;
  Confidence_Pct: number;
  IssueTypeDesc: string;
  IssueTypeID: number;
  IssueClassTypeID: number;
  IssueClassTypeDescription: string;
  Priority: string;
  CreatedBy: string;
  CreateDate: Date;
  ChangeDate: Date;
  ChangedBy: string;
  ActivityStatus: string;
  ResolutionStatus: string;
  CloseDate: Date;
  IsSubscribed: boolean;
  GlobalID: string;
  OpenDuration: number;
  OpenDate: Date;
  AssignedTo: string;
  Scorecard: boolean;
  ResolveBy: Date;
  AssetClassTypeAbbrev: string;
  AssetClassTypeDesc: string;
  AssetTypeAbbrev: string;
  AssetTypeDesc: string;
  CreatedByUserID: number;
  CanEdit: boolean;
  CanDelete: boolean;
  CanEditIssueStatus: boolean;
  Count: number;
}

export interface IIssueDataRetrieval {
  Success: boolean;
  StatusCode: number;
  Type: any;
  Count: number;
  Results: {
    Issues: IIssueSummaryGroup[] | IIssueSummary[];
    NumIssues: number;
  }[];
}

export interface IGridExportRetrieval {
  Success: boolean;
  StatusCode: number;
  Type: any; // I think this could maybe be a string instead of "any"
  Count: number;
  Results: string[];
}
