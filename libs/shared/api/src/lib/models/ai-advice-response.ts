export interface AIAdviceResponse {
  id: string;
  object: string;
  created: Date;
  model: string;
  prompt_filter_results: [promptFilterResult];
  choices: [adviceResult];
  usage: {
    prompt_tokens: number;
    completion_tokens: number;
    total_tokens: number;
  };
}

export interface adviceResult {
  index: number;
  finish_reason: string;
  message: { role: string; content: string };
  content_filter_results: contentFilterResults;
}

export interface promptFilterResult {
  prompt_index: number;
  content_filter_results: contentFilterResults;
}

export interface contentFilterResults {
  hate: contentFilter;
  self_harm: contentFilter;
  sexual: contentFilter;
  violence: contentFilter;
}

export interface contentFilter {
  filtered: boolean;
  severity: string;
}
