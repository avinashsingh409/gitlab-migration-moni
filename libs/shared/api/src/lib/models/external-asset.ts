export interface ExternalAsset {
  ExternalSystemAssetID: number;
  ExternalAssetID: string;
  ExternalAssetName: string;
  ExternalAssetType: string;
  ExternalAssetMeta: string;
  Hierarchy: string[];
}
