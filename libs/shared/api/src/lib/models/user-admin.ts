import { InjectionToken } from '@angular/core';

export interface User {
  Id: string;
  FirstName: string;
  LastName: string;
  Email: string;
  Active: boolean;
  CustomerId: string;
  CustomerName: string;
  IsGroupSub: boolean;
}

export interface UserDetails extends User {
  Roles: Role[];
  RoleIds: number[];
  Groups: Group[];
  GroupIds: string[];
  ServiceAccount: boolean;
  FederatedAccount: boolean;
}

export interface Resource {
  Id: number;
  Name: string;
}

export interface ResourcePermission {
  ResourceId: number;
  ResourceName: string;
  AccessTypeId: number;
  AccessType: string;
  Allow: boolean;
}

export interface AssetPermission {
  AssetId: string;
  AssetName: string;
  Allow: boolean;
}

export interface AssetPermissionDetails extends AssetPermission {
  ParentAssetId?: string;
  ParentAssetName: string;
  HasChildren: boolean;
  ExplicitAllow: boolean;
  ExplicitDeny: boolean;
  ImplicitAllow: boolean;
  ImplicitDeny: boolean;
  AscendAllow: boolean;
  AscendDeny: boolean;
}

export interface Role {
  Id: number;
  Name: string;
  ParentId: number;
  CustomerId: string;
  RoleType: string;
  Ownership: string;
}

export interface RoleDetails extends Role {
  ResourcePermissions: ResourcePermission[];
  AssetPermissions: AssetPermission[];
  Users: User[];
  UserIds: string[];
  Shares: Customer[];
  SharedCustomerIds: string[];
}

export const ROLE_DETAILS = new InjectionToken<RoleDetails>('ROLE_DETAILS');

export interface AssetNode {
  Asset: string;
  AssetId: string;
  Allow: boolean;
  Deny: boolean;
  ImplicitAllow: boolean;
  ImplicitDeny: boolean;
  AscendAllow: boolean;
  AscendDeny: boolean;
  Children?: AssetNode[];
  IsLoading: boolean;
  Level: number;
  IsExpanded: boolean;
}

export interface Group {
  Id: string;
  Name: string;
  CustomerName: string;
  Active: boolean;
}

export interface GroupDetails extends Group {
  Users: User[];
  UserIds: string[];
  Roles: Role[];
  RoleIds: number[];
  CustomerId: string;
}

export class TopLevelAsset {
  AssetId: string;
  AssetName: string;
  HasChildren: boolean;
}

export class Customer {
  Id: string;
  Name: string;
  HasChildren: boolean;
  ParentCustomerId?: string;
}

export class CustomerNode extends Customer {
  Children?: CustomerNode[];
  IsLoading: boolean;
  Level: number;
  IsSelected: boolean;
  IsExpanded: boolean;
}

export class CustomerDetails extends Customer {
  EmailDomain: string[];
  TopLevelAssetIds: string[];
  TopLevelAssets: TopLevelAsset[];
}

export class TopLevelAssetNode extends TopLevelAsset {
  Children?: TopLevelAssetNode[];
  IsLoading: boolean;
  Level: number;
  IsSelected: boolean;
  IsExpanded: boolean;
}

export const CUSTOMER_ID = new InjectionToken<string>('CUSTOMER_ID');
