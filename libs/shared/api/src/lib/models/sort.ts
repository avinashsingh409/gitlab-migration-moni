import { SortModelItem } from '@ag-grid-enterprise/all-modules';

export function sortModelToStrings(value: SortModelItem[]) {
  return (value ?? []).map((n) => {
    return `${n.colId}:${n.sort}`;
  });
}
