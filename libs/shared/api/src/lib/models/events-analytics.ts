import { IEvent } from '@atonix/atx-core';

export interface AnalyticsEvent {
  GlobalID: string;
  AssetID: number;
  EventTypeID: number;
  StartTime: string | Date;
  EndTime: string | Date;
  AssetPath?: any;
  Impact: number;
  ModifiedByEventID?: any;
  CreatedBy?: any;
  ChangedBy?: any;
  CreateDate: string | Date;
  ChangeDate: string | Date;
  IsDeleted: boolean;
  EventID: number;
  Description: string;
}

export interface PeriodOpStateEventAnalytic {
  BackingEvent: AnalyticsEvent;
  // TODO: currently empty
  PeriodOpStateEventAnalytics?: any;
  StartTime: string | Date;
  EndTime: string | Date;
  NetMaxCapacity: number;
  Impact: number;
  AnalyticsPeriod: number;
  EventCount: number;
  PlannedOutageTime: number;
  ForcedOutageTime: number;
  PlannedDerateTime: number;
  ForcedDerateTime: number;
  EquivalentPlannedDerateTime: number;
  EquivalentForcedDerateTime: number;
  AvailableTime: number;
  EquivalentAvailabilityFactor: number;
  CalcedEquivalentAvailabilityFactor: number;
  EquivalentForcedOutageRate: number;
  ScheduledOutageFactor: number;
}

export interface OpStateAnalytic {
  BackingEvent?: any;
  PeriodOpStateEventAnalytics: PeriodOpStateEventAnalytic[];
  StartTime: string | Date;
  EndTime: string | Date;
  NetMaxCapacity: number;
  Impact: number;
  AnalyticsPeriod: number;
  EventCount: number;
  PlannedOutageTime: number;
  ForcedOutageTime: number;
  PlannedDerateTime: number;
  ForcedDerateTime: number;
  EquivalentPlannedDerateTime: number;
  EquivalentForcedDerateTime: number;
  AvailableTime: number;
  EquivalentAvailabilityFactor: number;
  CalcedEquivalentAvailabilityFactor: number;
  EquivalentForcedOutageRate: number;
  ScheduledOutageFactor: number;
}

export interface Children {
  [child: string]: AnalyticsResult;
}
export interface OpStateEvents {
  [event: string]: AnalyticsEvent;
}
export interface OpStateUDEvents {
  [event: string]: AnalyticsEvent;
}
// TODO: Empty Currently
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DescendantOpStateEvents {}
export interface AnalyticsResult {
  AssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  CF: number;
  EAF: number;
  EFOR: number;
  SOF: number;
  EventCount: number;
  ParentID: number;
  Level: string;
  StartTime: string | Date;
  EndTime: string | Date;
  AvgGen: number;
  TotalGen: number;
  MaxCapacity: number;
}

export interface AnalyticsResponse {
  Success: boolean;
  StatusCode: number;
  Type: string;
  Count: number;
  Results: AnalyticsResult[];
}

export interface IChartData {
  CF: number[][];
  EAF: number[][];
  EFOR: number[][];
  SOF: number[][];
}

export interface IChildData {
  AssetID: number;
  AssetAbbrev: string;
  AssetDesc: string;
  MW: number;
  CF: number;
  EAF: number;
  EFOR: number;
  SOF: number;
  MWMaxCapacity: number;
  EventCount: number;
  ParentID: number;
  Level: string;
  StartTime: string | Date;
  EndTime: string | Date;
  ShowDetails: boolean;
  ChartDetails: IChartData;
  Interval: string;
  IntervalDetails: AnalyticsResult[];
  EventDetails: IEvent[];
}

export interface IDashboardData {
  id: number; // Entity state looks for this
  ParentID: number;
  AssetDesc: string;
  MW: number;
  CF: number;
  EAF: number;
  EFOR: number;
  SOF: number;
  MWMaxCapacity: number;
  EventCount: number;
  Level: string;
  Path: IAssetData[];
  Siblings: IAssetData[];
  Children: IChildData[];
  Data: AnalyticsResult;
  ShowDetails: boolean;
  ChartDetails: IChartData;
  Interval: string;
  IntervalDetails: AnalyticsResult[];
  EventDetails: IEvent[];
}

export interface IAssetData {
  AssetID: number;
  AssetDesc: string;
}
