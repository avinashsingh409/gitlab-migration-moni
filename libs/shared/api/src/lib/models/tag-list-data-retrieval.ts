import { IAssetVariableTypeTagMap } from '@atonix/atx-core';

export interface ITagListDataRetrieval {
  Results: { TagMaps: IAssetVariableTypeTagMap[]; NumTagMaps: number }[];
}
