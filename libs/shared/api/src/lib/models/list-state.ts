import { SortModelItem } from '@ag-grid-enterprise/all-modules';
import { IGroup } from './group';

export interface IListState {
  columns: any;
  sort: SortModelItem[];
  groups: IGroup[];
  filter: any;
}

export interface IListStateV2 {
  version: number;
  columns: any;
  sort: SortModelItem[];
  groups: IGroup[];
  filter: any;
  selectedListId: number | null;
}
