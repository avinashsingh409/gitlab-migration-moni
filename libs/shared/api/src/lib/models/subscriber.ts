import { IDiscussionAssetMapType } from '@atonix/atx-core';

export interface ISubscriber {
  SecurityUserID: number;
  UserName: string;
  FirstName: string;
  LastName: string;
  FriendlyName: string;
  EmailAddress: string;
  IsBV: boolean;
  IsGroup: boolean;
  IsSubscribed: boolean;
  groups: string[];
  isChecked?: boolean;
  SubscriptionTypes: IDiscussionAssetMapType[];
  ServiceAccount: boolean;

  // Front end use
  groupMembers?: any[];
}
