export interface IUpdateIssueSubscribersModel {
  AssetIssueId: number;
  SubscriberEmailIds: Array<string>;
  UnSubscriberEmailIds: Array<string>;
}

export interface IIssueSubscriber {
  SecurityUserId: number;
  Email: string;
  FirstName: string;
  LastName: string;
  FriendlyName: string;
  IsSubscribed: boolean;
  UserGroups: ISubscriberGroup[];
}

export interface ISubscriberGroup {
  SecurityGroupId: number;
  Name: string;
  Username: string;
}

export interface IIssueSubscribersRetrieval {
  Success: boolean;
  StatusCode: number;
  Type: any;
  Count: number;
  Results: any[];
}
