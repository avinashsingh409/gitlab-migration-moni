export class AccessTypeBit {
  static CanView = 1;
  static CanEdit = 2;
  static CanAdd = 4;
  static CanDelete = 8;
  static CanAdmin = 16;
}
