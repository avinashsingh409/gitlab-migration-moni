import {
  ITextFilterParams,
  TextFilterModel,
  ProvidedFilterModel,
  NumberFilterModel,
  DateFilterModel,
  ISimpleFilterModel,
} from '@ag-grid-enterprise/all-modules';

export interface ICombinedSimpleModel<M extends ISimpleFilterModel>
  extends ProvidedFilterModel {
  operator: string;
  condition1: M;
  condition2: M;
}

export interface ISetFilterModel extends ISimpleFilterModel {
  values: string[];
}

export interface IFilterModels {
  AssetID: ProvidedFilterModel;
  AssetIssueID: ProvidedFilterModel;
  CreateDate: ProvidedFilterModel;
  ChangeDate: ProvidedFilterModel;
  ChangedBy: ProvidedFilterModel;
  ActivityStatus: ProvidedFilterModel;
  ResolutionStatus: ProvidedFilterModel;
  CloseDate: ProvidedFilterModel;
  IsSubscribed: ProvidedFilterModel;
  GlobalID: ProvidedFilterModel;
  OpenDuration: ProvidedFilterModel;
  CreatedBy: ProvidedFilterModel;
  OpenDate: ProvidedFilterModel;
  Scorecard: ProvidedFilterModel;
  ResolveBy: ProvidedFilterModel;
  AssetClassTypeAbbrev: ProvidedFilterModel;
  AssetClassTypeDesc: ProvidedFilterModel;
  AssetTypeAbbrev: ProvidedFilterModel;
  AssetTypeDesc: ProvidedFilterModel;
  CreatedByUserID: ProvidedFilterModel;
  CanEdit: ProvidedFilterModel;
  CanDelete: ProvidedFilterModel;
  AssignedTo: ProvidedFilterModel;
  CanEditIssueStatus: ProvidedFilterModel;
  Priority: ProvidedFilterModel;
  IssueClassTypeID: ProvidedFilterModel;
  CategoryDesc: ProvidedFilterModel;
  CategoryID: ProvidedFilterModel;
  Title: ProvidedFilterModel;
  IssueSummary: ProvidedFilterModel;
  ShortSummary: ProvidedFilterModel;
  Description: ProvidedFilterModel;
  Client: ProvidedFilterModel;
  StationGroup: ProvidedFilterModel;
  Station: ProvidedFilterModel;
  IssueClassTypeDesc: ProvidedFilterModel;
  StationAssetAbbrev: ProvidedFilterModel;
  UnitAssetAbbrev: ProvidedFilterModel;
  System: ProvidedFilterModel;
  SystemAssetClassTypeID: ProvidedFilterModel;
  Asset: ProvidedFilterModel;
  AssetClassTypeID: ProvidedFilterModel;
  ImpactTotal: ProvidedFilterModel;
  Confidence_Pct: ProvidedFilterModel;
  IssueTypeDesc: ProvidedFilterModel;
  IssueTypeID: ProvidedFilterModel;
  Unit: ProvidedFilterModel;
  OpenIssuesAssetID: ProvidedFilterModel;
}

export function filterModelToString(value: ProvidedFilterModel, name: string) {
  if (value) {
    if ((value as any).condition1) {
      const combinedFilterType =
        value as ICombinedSimpleModel<ISimpleFilterModel>;
      return `${name},${value.filterType},${combinedFilterModelToString(
        combinedFilterType
      )}`;
    } else if (value.filterType === 'text') {
      const textFilterType = value as TextFilterModel;
      return `${name},${value.filterType},${textFilterModelToString(
        textFilterType
      )}`;
    } else if (value.filterType === 'number') {
      const numFilterType = value as NumberFilterModel;
      return `${name},${value.filterType},${numberFilterModelToString(
        numFilterType
      )}`;
    } else if (value.filterType === 'date') {
      const dateFilterType = value as DateFilterModel;
      return `${name},${value.filterType},${dateFilterModelToString(
        dateFilterType
      )}`;
    } else if (value.filterType === 'set') {
      const setFilterType = value as ISetFilterModel;
      return `${name},${value.filterType},${setFilterModelToString(
        setFilterType
      )}`;
    }
    return `${name},${value.filterType}`;
  }
  return '';
}

export function numberFilterModelToString(value: NumberFilterModel) {
  let f1 = '';
  let f2 = '';
  if (
    value.filter !== null &&
    value.filter !== undefined &&
    !isNaN(value.filter)
  ) {
    f1 = String(value.filter);
  }
  if (
    value.filterTo !== null &&
    value.filterTo !== undefined &&
    !isNaN(value.filterTo)
  ) {
    f2 = String(value.filterTo);
  }
  return `${value.type},${f1},${f2}`;
}

export function textFilterModelToString(value: TextFilterModel) {
  return `${value.type},${value.filter}`;
}
export function setFilterModelToString(value: ISetFilterModel) {
  return [...(value.values ?? [])].sort().join(',');
}

export function dateFilterModelToString(value: DateFilterModel) {
  let df = '';
  let dt = '';
  if (value.dateFrom && value.dateFrom !== 'null null') {
    df = String(value.dateFrom);
  }
  if (value.dateTo && value.dateTo !== 'null null') {
    dt = String(value.dateTo);
  }
  return `${value.type},${df},${dt}`;
}

export function combinedFilterModelToString(
  value: ICombinedSimpleModel<ISimpleFilterModel>
) {
  return `${value.operator},${value.condition1.type},${filterModelToString(
    value.condition1,
    ''
  )},${value.condition2.type},${filterModelToString(value.condition2, '')}`;
}

export function filterModelsToStrings(value: IFilterModels) {
  const fields = [
    'AssetID',
    'AssetIssueID',
    'CreateDate',
    'ChangeDate',
    'ChangedBy',
    'ActivityStatus',
    'ResolutionStatus',
    'CloseDate',
    'IsSubscribed',
    'GlobalID',
    'OpenDuration',
    'CreatedBy',
    'OpenDate',
    'Scorecard',
    'ResolveBy',
    'AssetClassTypeAbbrev',
    'AssetClassTypeDesc',
    'AssetTypeAbbrev',
    'AssetTypeDesc',
    'CreatedByUserID',
    'CanEdit',
    'CanDelete',
    'AssignedTo',
    'CanEditIssueStatus',
    'Priority',
    'IssueClassTypeID',
    'CategoryDesc',
    'CategoryID',
    'Title',
    'IssueSummary',
    'ShortSummary',
    'Description',
    'Client',
    'StationGroup',
    'Station',
    'IssueClassTypeDesc',
    'StationAssetAbbrev',
    'UnitAssetAbbrev',
    'System',
    'SystemAssetClassTypeID',
    'Asset',
    'AssetClassTypeID',
    'ImpactTotal',
    'Confidence_Pct',
    'IssueTypeDesc',
    'IssueTypeID',
    'Unit',
    'OpenIssuesAssetID',
  ];

  const result: string[] = fields.map((field) =>
    filterModelToString(value[field], field)
  );
  return result;
}
