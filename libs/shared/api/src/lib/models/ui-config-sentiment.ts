export interface ISentiment {
  SentimentID?: number;
  User?: string;
  CreatedOn?: Date;
  Url?: string;
  Disposition: Satisfieds;
  Content: string;
}

export type Satisfieds =
  | ''
  | 'very-satisfied'
  | 'satisfied'
  | 'dissatisfied'
  | 'very-dissatisfied';
