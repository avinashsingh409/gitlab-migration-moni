export interface IModelConfigModelTrend {
  expectedValues: IHighChartsCategoryData[];
  actualValues: IHighChartsCategoryData[];
  timeStamps: string[];
  breaks: IHighchartsBreaks[];
  expectedRange: any[][];
  startDate: Date;
  endDate: Date;
  tagUnits: string;
  tagName: string;
  yAxisMin: number;
  yAxisMax: number;
}

export interface IHighChartsSimpleData {
  x: number;
  y: number;
}

export interface IHighChartsCategoryData {
  name: string;
  y: number;
}

export interface IHighChartsAreaData {
  x: number;
  upperData: number;
  lowerData: number;
}

export interface IHighchartsBreaks {
  from: number;
  to: number;
}

export interface IRawModelTrend {
  PredictiveMethodName: string;
  Score: number;
  IsSelected: boolean;
  IncludedInputs?: any;
  OutputDetails: IRawOutputDetails;
}

export interface IRawOutputDetails {
  RValue: number;
  RMSE: number;
  MAE: number;
  AssetMeasurementsSet: IRawAssetMeasurementsSet;
}

export interface IRawAssetMeasurementsSet {
  Errors?: any;
  Measurements: IRawMeasurement[];
  Annotations: any[];
  Bands?: any;
  Pins: any[];
}

export interface IRawMeasurement {
  Tag: ITag;
  Archive?: any;
  Data: IHighchartsData[];
  SensorID: number;
  PinID?: any;
  proposedSeriesMin?: any;
  proposedSeriesMax?: any;
  actualSeriesMin?: any;
  actualSeriesMax?: any;
  Statistics?: any;
}

export interface ITag {
  PDTagID: number;
  PDServerID: number;
  TagName: string;
  TagDesc: string;
  EngUnits: string;
  Qualifier: string;
  Mapping: string;
  ExistsOnServer: boolean;
  ChangeDate: Date;
  ExternalID: string;
  GlobalID: string;
  NDCreated: boolean;
  NDPath: string;
  NDName: string;
  NDId: string;
}

export interface IHighchartsData {
  Status: number;
  Value: number;
  Timestamp: Date;
}
