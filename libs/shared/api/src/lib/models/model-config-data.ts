export interface LegacyProperties {
  modelID: number;
  modelTemplateID?: number;
  opModeParamsID: number;
}

export interface IRawModelConfigData {
  legacy: LegacyProperties;
  active: boolean;
  alerts: IAlert[];
  anomalies: IAnomaly[];
  buildProperties: IModelBuildProperties;
  dependent: IModelDependent;
  independents: IModelIndependent[];
  locked: boolean;
  modelExtID: string;
  name: string;
  path: string;
  properties: IModelProperties;
  standardModel: boolean;
  training: IModelTraining;
}

export interface IModelConfigData {
  active: boolean;
  averageAnomalyAlert: IAlert;
  anomalyAreaAlert: IAlert;
  anomalyFrequencyAlert: IAlert;
  anomalyOscillationAlert: IAlert;
  highHighAlert: IAlert;
  lowLowAlert: IAlert;
  frozenDataCheckAlert: IAlert;
  relativeBoundsAnomaly: IAnomaly;
  upperFixedLimitAnomaly: IAnomaly;
  lowerFixedLimitAnomaly: IAnomaly;
  criticalityAnomaly: IAnomaly;
  buildProperties: IModelBuildProperties;
  dependent: IModelDependent;
  independents: IModelIndependent[];
  locked: boolean;
  modelExtID: string;
  legacy: LegacyProperties;
  name: string;
  path: string;
  properties: IModelProperties;
  standardModel: boolean;
  training: IModelTraining;

  isDirty: boolean;
  // Generated from Training Data
  savedTrainingRange: string;
  projectedTrainingRange: string;

  // Forecast temp values
  relativeEarliestInterceptDate: string;
  fixedEarliestInterceptDate: string;
  fixedEarliestInterceptTime: string;
  fixedEarliestInterceptChanged: boolean;

  // Multi-model Live Status
  liveStatus: string;
}

export interface IForecastModelProperties {
  historicalPeriodUnitOfTime?: EUnitOfTime | null;
  historicalPeriod?: number;
  thresholdOperator?: string;
  threshold?: number;
  horizonType?: string;
  horizonDate?: string | null;
  horizonPeriodUnitOfTime?: EUnitOfTime | null;
  horizonPeriod?: number;
}

export interface IRateOfChangeProperties {
  evaluationPeriod?: number;
  evaluationPeriodUnitOfTime?: EUnitOfTime | null;
  expressionPeriodUnitOfTime?: EUnitOfTime | null;
}

export interface IModelTypeProperties {
  type?: EModelTypes;
  properties: IForecastModelProperties | null;
}

export interface IModelProperties {
  modelType: IModelTypeProperties;
  opModeTypes?: EOpModeTypes[];
  predictiveTypeSelected?: EPredictiveTypes;
  predictiveTypes?: IModelPredictiveTypes[];
}

export interface IModelPredictiveTypes {
  type?: EPredictiveTypes;
  properties?: IModelPredictiveProperties;
}

export interface IModelPredictiveProperties {
  interquartileRange?: number;
  meanAbsoluteError?: number;
  rootMeanSquareError?: number;
  rPearsonsCorrelationCoefficient?: number;
  score?: number;
}

// Start of Alert
export interface IAlert {
  type: EAlertTypes;
  enabled: boolean | 'Yes' | 'No';
  notificationEnabled: boolean;
  properties:
    | IAverageAnomalyAlertProperties
    | IAnomalyAreaAlertProperties
    | IAnomalyFrequencyAlertProperties
    | IAnomalyOscillationAlertProperties
    | IHighHighAlertProperties
    | ILowLowAlertProperties
    | IFrozenDataAlertProperties;
}

export enum EAlertTypes {
  AverageAnomaly = 'average_anomaly',
  AnomalyFrequency = 'anomaly_frequency',
  AnomalyOscillation = 'anomaly_oscillation',
  HighHigh = 'high_high',
  LowLow = 'low_low',
  FrozenDataCheck = 'frozen_data_check',
  AnomalyArea = 'anomaly_area',
}

export enum AlertType {
  AnomalyFrequency = 8,
  AnomalyOscillation = 9,
  HighHigh = 1,
  LowLow = 2,
  FrozenDataCheck = 7,
  AnomalyArea = 3, //3 or 6 Fast/Slow Response
}

export interface IAverageAnomalyAlertProperties {
  evaluationPeriodUnitOfTime?: EUnitOfTime;
  averageEvalPeriod?: number;
  averagingWindowUnitOfTime?: EUnitOfTime;
  averagingWindow?: number;
  useMeanAbsoluteError?: boolean | 'Yes' | 'No';
  threshold?: number;
}

export interface IAnomalyAreaAlertProperties {
  areaFastResponse?: number;
  areaFastResponsePeriod?: number;
  areaFastResponsePeriodUnitOfTime?: string;
  areaSlowResponse?: number;
  areaSlowResponsePeriod?: number;
  areaSlowResponsePeriodUnitOfTime?: string;
}

export interface IAnomalyFrequencyAlertProperties {
  evaluationPeriodUnitOfTime?: EUnitOfTime;
  evaluationPeriod?: number;
  threshold?: number;
}

export interface IAnomalyOscillationAlertProperties {
  evaluationPeriodUnitOfTime?: EUnitOfTime;
  evaluationPeriod?: number;
  threshold?: number;
}

export interface IHighHighAlertProperties {
  useMeanAbsoluteError?: boolean | 'Yes' | 'No';
  threshold?: number;
}

export interface ILowLowAlertProperties {
  useMeanAbsoluteError?: boolean | 'Yes' | 'No';
  threshold?: number;
}

export interface IFrozenDataAlertProperties {
  evaluationPeriodUnitOfTime?: EUnitOfTime;
  evaluationPeriod?: number;
}
// End of Alert

// Start of Anomaly
export interface IAnomaly {
  type: EAnomalyTypes;
  enabled: boolean | 'Yes' | 'No';
  properties:
    | IRelativeBoundsAnomalyProperties
    | IUpperFixedLimitAnomalyProperties
    | ILowerFixedLimitAnomalyProperties
    | ICriticalityAnomalyProperties;
}

export enum EAnomalyTypes {
  RelativeBounds = 'relative_bounds',
  UpperFixedLimit = 'upper_fixed_limit',
  LowerFixedLimit = 'lower_fixed_limit',
  Criticality = 'criticality',
}

export interface IRelativeBoundsAnomalyProperties {
  upperMultiplier?: number | null;
  upperBiasEnabled?: boolean;
  upperBias?: number | null;
  lowerMultiplier?: number | null;
  lowerBiasEnabled?: boolean;
  lowerBias?: number | null;
}

export interface IUpperFixedLimitAnomalyProperties {
  upperFixedLimit?: number;
}

export interface ILowerFixedLimitAnomalyProperties {
  lowerFixedLimit?: number;
}

export interface ICriticalityAnomalyProperties {
  upperBound?: string;
  upperBoundDefault?: string;
  lowerBound?: string;
  lowerBoundDefault?: string;
  useDefault?: boolean;
}
// End of Anomalies

export interface IModelDependent {
  tagGuid: string;
  tagName?: string;
  tagDesc?: string;
  modeledTagString?: string;
  assetVariableTagMapID: number;
  assetGuid: string;
  tagUnits?: string;
  assetPath?: string;
  processDataServerGuid: string;
}

export interface IModelIndependent {
  tagGuid: string;
  tagName?: string;
  tagDesc?: string;
  forcedInclusion: boolean;
  assetVariableTagMapID: number;
  inUse: boolean;
}

export interface IModelTraining {
  rangeType?: string;
  range?: IModelTrainingRange[];
  durationUnitOfTime?: EUnitOfTime;
  duration?: number;
  lagUnitOfTime?: EUnitOfTime;
  lag?: number;
  sampleRateUnitOfTime?: EUnitOfTime;
  sampleRate?: number;
  minDataPoints?: number;
  // Smoothing Factor applies to Moving Average model type and is required. Do we really need a boolean here?
  //public bool SmoothingFactor { get; set; }
  smoothingFactorActual?: number;
  smoothingFactorExpected?: number;
  autoRetrain?: boolean;

  // V3 values that are readonly right now.
  smoothingFactorActualUnits: EMovingAverageUnitOfTime;
  smoothingFactorExpectedUnits: EMovingAverageUnitOfTime;
}

export interface IModelTrainingRange {
  startDate: Date;
  endDate: Date;
}

export interface IModelBuildProperties {
  status?: IModelBuildStatus;
}

export interface IModelBuildStatus {
  built: boolean;
  lastSaved: Date;
  builtSinceTime?: Date;
  buildState?: string;
  changedBy?: string;
}

export enum EUnitOfTime {
  Seconds = 'Seconds',
  Minutes = 'Minutes',
  Hours = 'Hours',
  Days = 'Days',
  Weeks = 'Weeks',
  Months = 'Months',
  Years = 'Years',
}

export enum EModelTypes {
  AdvancedPatternRecognition = 'advanced_pattern_recognition',
  RollingAverage = 'rolling_average',
  FixedLimit = 'fixed_limit',
  FrozenData = 'frozen_data',
  MovingAverage = 'moving_average',
  Forecast = 'forecast',
  ROC = 'rate_of_change',
}

export const ModelTypeLabelMapping: Record<EModelTypes, string> = {
  [EModelTypes.AdvancedPatternRecognition]: 'APR',
  [EModelTypes.RollingAverage]: 'Rolling Average',
  [EModelTypes.FixedLimit]: 'Fixed Limit',
  [EModelTypes.FrozenData]: 'Frozen Data Check',
  [EModelTypes.MovingAverage]: 'Moving Average',
  [EModelTypes.Forecast]: 'Forecast',
  [EModelTypes.ROC]: 'Rate of Change',
};

export const ModelTypeAbbrevMapping: Record<EModelTypes, string> = {
  [EModelTypes.AdvancedPatternRecognition]: 'APR',
  [EModelTypes.RollingAverage]: 'RA',
  [EModelTypes.FixedLimit]: 'FL',
  [EModelTypes.FrozenData]: 'FD',
  [EModelTypes.MovingAverage]: 'MA',
  [EModelTypes.Forecast]: 'FCST',
  [EModelTypes.ROC]: 'ROC',
};

export enum EOpModeTypes {
  SteadyState = 'steady_state',
  Startup = 'startup',
  Transient = 'transient',
  OutOfService = 'out_of_service',
}

export const OpModeTypeLabelMapping: Record<EOpModeTypes, string> = {
  [EOpModeTypes.SteadyState]: 'Steady State',
  [EOpModeTypes.Startup]: 'Startup',
  [EOpModeTypes.Transient]: 'Transient',
  [EOpModeTypes.OutOfService]: 'Out of Service',
};

export enum EPredictiveTypes {
  None = 'none',
  RollingAverage = 'rolling_average',
  FixedLimit = 'fixed_limit',
  LogReg = 'logreg',
  Gff = 'gff',
  FrozenDataCheck = 'frozen_data_check',
  LinReg = 'linreg',
  Linear = 'linear',
  ExpAvg = 'expavg',
}

export enum EMovingAverageUnitOfTime {
  Minutes = 'Minutes',
}

export const PredictiveMethodTypeLabel: Record<EPredictiveTypes, string> = {
  [EPredictiveTypes.None]: '',
  [EPredictiveTypes.RollingAverage]: 'Rolling Average',
  [EPredictiveTypes.FixedLimit]: 'Fixed Limit',
  [EPredictiveTypes.LogReg]: 'Logistic Regression',
  [EPredictiveTypes.Gff]: 'Neural Net',
  [EPredictiveTypes.FrozenDataCheck]: 'Frozen Data Check',
  [EPredictiveTypes.LinReg]: 'Linear Regression',
  [EPredictiveTypes.Linear]: 'Linear',
  [EPredictiveTypes.ExpAvg]: 'Exponential Average',
};

export const PredictiveMethodTypeNameMapping: Record<EPredictiveTypes, string> =
  {
    [EPredictiveTypes.None]: '',
    [EPredictiveTypes.RollingAverage]: 'Rolling Average',
    [EPredictiveTypes.FixedLimit]: 'Fixed Limit',
    [EPredictiveTypes.LogReg]: 'LogReg',
    [EPredictiveTypes.Gff]: 'GFF',
    [EPredictiveTypes.FrozenDataCheck]: 'Frozen Data Check',
    [EPredictiveTypes.LinReg]: 'LinReg',
    [EPredictiveTypes.ExpAvg]: 'ExpAvg',
    [EPredictiveTypes.Linear]: 'Linear',
  };

export enum OPModeTypes {
  Startup = 1,
  Transient = 2,
  OOS = 3,
  SteadyState = 4,
  ExclusionPeriod = 5,
}

export enum ModelTypes {
  AdvancedPatternRecognition = 1,
  RollingAverage = 2,
  FixedLimit = 3,
  FrozenData = 4,
  MovingAverage = 6,
  Forecast = 7,
  ROC = 8,
}
export const OpModeTypeMapping: Record<EOpModeTypes, number> = {
  [EOpModeTypes.SteadyState]: OPModeTypes.SteadyState,
  [EOpModeTypes.Startup]: OPModeTypes.Startup,
  [EOpModeTypes.Transient]: OPModeTypes.Transient,
  [EOpModeTypes.OutOfService]: OPModeTypes.OOS,
};

export const ModelTypeMapping: Record<EModelTypes, number> = {
  [EModelTypes.AdvancedPatternRecognition]:
    ModelTypes.AdvancedPatternRecognition,
  [EModelTypes.RollingAverage]: ModelTypes.RollingAverage,
  [EModelTypes.FixedLimit]: ModelTypes.FixedLimit,
  [EModelTypes.FrozenData]: ModelTypes.FrozenData,
  [EModelTypes.MovingAverage]: ModelTypes.MovingAverage,
  [EModelTypes.Forecast]: ModelTypes.Forecast,
  [EModelTypes.ROC]: ModelTypes.ROC,
};

export const ModelSummaryDependentMapping: Record<string, string> = {
  'ComputedCriticality.DefaultUpperBound': 'ComputedCriticality.UpperBound',
  'ComputedCriticality.DefaultLowerBound': 'ComputedCriticality.LowerBound',
};
