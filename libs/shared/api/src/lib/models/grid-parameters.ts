import { ColumnVO, SortModelItem } from '@ag-grid-enterprise/all-modules';

export interface IGridParameters {
  startRow: number;
  endRow: number;
  rowGroupCols: ColumnVO[];
  valueCols: ColumnVO[];
  pivotCols: ColumnVO[];
  displayCols: ColumnVO[];
  pivotMode: boolean;
  filterModel: any;
  sortModel: SortModelItem[];
  groupKeys: string[];
}
