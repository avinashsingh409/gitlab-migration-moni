import { IListState, IListStateV2 } from './list-state';

export interface ISavedModelList {
  id: number;
  name: string;
  state: IListState;
  checked: boolean;
}

export interface ISavedIssueList {
  id: number;
  name: string;
  state: IListState;
  checked: boolean;
}

export interface ISavedList {
  id: number;
  name: string;
  state: IListState;
  checked: boolean;
}

export interface ISavedModelConfigList {
  id: number;
  name: string;
  state: IListStateV2;
  checked: boolean;
}
