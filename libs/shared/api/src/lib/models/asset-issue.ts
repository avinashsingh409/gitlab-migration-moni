import { ITaggingKeyword } from '@atonix/atx-core';
import {
  AssetIssueCategoryType,
  ImpactMap,
  IssueCauseType,
  IssueClassType,
} from './issue-model';

export interface IssueType {
  IssueTypeID: number;
  IssueTypeDesc: string;
  IssueTypeAbbrev: string;
  TrendDirection: number;
  IsBVApproved: boolean;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedByUserID: number;
  ChangedByUserID: number;
  IssueTypeCausesTypes: IssueCauseType[];
  AssetIssueClassType: IssueClassType;
}

export interface AssetIssue {
  AssetIssueID: number;
  AssetID: number;
  IssueType: IssueType;
  IssueTypeID: number;
  AssetIssueCategoryType: AssetIssueCategoryType;
  AssetIssueCategoryTypeID: number;
  ActivityStatus: string;
  ActivityStatusID: number;
  ResolutionStatus: string;
  ResolutionStatusID: number;
  IssueCauseTypeID?: number;
  Priority: string;
  Action: string;
  CreateDate: Date;
  ChangeDate: Date;
  CreatedBy: string;
  ChangedBy: string;
  Confidence_Pct: number;
  Risk: number;
  ModelConditionID: number;
  IssueTitle: string;
  IssueSummary: string;
  CloseDate: Date;
  OpenDate: Date;
  OpenDuration: number;
  LastEmailSent: Date;
  CreatedByUserID: number;
  AssetAndParents: any;
  CategoryDesc: string;
  IssueTypeDesc: string;
  IssueCauseType: IssueCauseType;
  ImpactMaps: ImpactMap[];
  ImpactsString: string;
  BlogID: string;
  RelatedIssues: number[];
  ActionPlans: any[];
  CausalFactors: any[];
  IssueCauseTypes: IssueCauseType[];
  GlobalID: string;
  CanEdit: boolean;
  CanDelete: boolean;
  CanEditIssueStatus: boolean;
  IssueShortSummary: string;
  Scorecard: boolean;
  AssignedTo: string;
  NERCCIP?: boolean;
  Keywords: ITaggingKeyword[];
  ResolveBy?: Date;
}
