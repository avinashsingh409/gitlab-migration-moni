export interface IListConfig {
  ListConfigID: number;
  Owner: string;
  ListType: 'issues' | 'alerts' | 'models';
  Name: string;
  Content: string;
}
