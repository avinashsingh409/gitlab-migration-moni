import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertsCoreService } from './services/netcore/alerts-core.service';
import { AlertsFrameworkService } from './services/netframework/alerts-framework.service';
import { IssuesCoreService } from './services/netcore/issues-core.service';
import { IssuesFrameworkService } from './services/netframework/issues-framework.service';
import { UIConfigFrameworkService } from './services/netframework/uiconfig-framework.service';
import { ProcessDataFrameworkService } from './services/netframework/processdata-framework.service';
import { ImagesFrameworkService } from './services/netframework/images-framework.service';
import { AuthorizationFrameworkService } from './services/netframework/authorization-framework.service';
import { JobsFrameworkService } from './services/netframework/jobs-framework.service';
import { AccountFrameworkService } from './services/netframework/account-framework.service';
import { AssetsCoreService } from './services/netcore/assets-core.service';
import { DiscussionsFrameworkService } from './services/netframework/discussions-framework.service';
import { BusinessIntelligenceFrameworkService } from './services/netframework/business-intelligence-framework.service';
import { UserAdminCoreService } from './services/netcore/user-admin-core.service';
import { ModelConfigCoreService } from './services/netcore/model-config-core.service';
import { PrivateApiAlertsCoreService } from './services/netcore/private-api-alerts-core.service';
import { AIAdviceService } from './services/netframework/ai-advice.service';

@NgModule({
  imports: [CommonModule],
})
export class SharedApiModule {
  static forRoot(): ModuleWithProviders<SharedApiModule> {
    return {
      ngModule: SharedApiModule,
      providers: [
        AlertsCoreService,
        AlertsFrameworkService,
        AccountFrameworkService,
        AssetsCoreService,
        BusinessIntelligenceFrameworkService,
        DiscussionsFrameworkService,
        IssuesCoreService,
        IssuesFrameworkService,
        JobsFrameworkService,
        UIConfigFrameworkService,
        ProcessDataFrameworkService,
        PrivateApiAlertsCoreService,
        ImagesFrameworkService,
        AuthorizationFrameworkService,
        UserAdminCoreService,
        ModelConfigCoreService,
        AIAdviceService,
      ],
    };
  }
}
