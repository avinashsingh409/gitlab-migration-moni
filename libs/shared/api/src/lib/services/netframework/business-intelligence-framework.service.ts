import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IAsset,
  IWidgetChart,
  IWidgetGauge,
  IWidgetKPI,
  IWidgetTable,
  IWidgetText,
  isNil,
} from '@atonix/atx-core';
import { of } from 'rxjs';
import { IReport, IReportSection } from '../../models/bi-models';
@Injectable({
  providedIn: 'root',
})
export class BusinessIntelligenceFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  createReport(asset: IAsset) {
    const newSection: IReportSection = {
      ReportSectionID: -1,
      ReportID: -1,
      Title: 'New Section',
      Subtitle: '',
      Height: 'Small',
      DisplayOrder: 1,
      Widgets: [],
      backgroundColor: 'transparent',
      Color: 'transparent',
      sectionHeight: '300px',
      Kind: 'Section',
    };

    const newReport: IReport = {
      ReportID: -1,
      ReportDesc: '',
      ReportAbbrev: 'New View',
      DisplayOrder: 0,
      AssetID: asset.AssetID,
      Asset: asset,
      Title: 'New View',
      Subtitle: '',
      CreatedBy: '',
      ChangedBy: '',
      CreateDate: '',
      ChangeDate: '',
      Sections: [newSection],
      NodeID: '',
      Kind: 'View',
    };

    return this.$http.post<IReport>(
      `${this.appConfig.baseServicesURL}/api/BI/Report`,
      {
        Report: newReport,
        WidgetChart: [],
        WidgetTable: [],
        WidgetFixedChart: [],
        WidgetText: [],
        WidgetGauge: [],
        WidgetKPI: [],
      }
    );
  }

  getReport(reportID: number) {
    return this.$http.get<IReport>(
      `${this.appConfig.baseServicesURL}/api/BI/Report`,
      {
        params: { reportID: String(reportID), getFull: 'true' },
      }
    );
  }

  createNewTextboxWidget() {
    return this.$http.get<IWidgetText>(
      `${this.appConfig.baseServicesURL}/api/BI/CreateTextWidget`
    );
  }
  createNewGaugeWidget() {
    return this.$http.get<IWidgetGauge>(
      `${this.appConfig.baseServicesURL}/api/BI/CreateGaugeWidget`
    );
  }
  createNewKPIWidget() {
    return this.$http.get<IWidgetKPI>(
      `${this.appConfig.baseServicesURL}/api/BI/CreateKPIWidget`
    );
  }
  createNewChartWidget() {
    return this.$http.get<IWidgetChart>(
      `${this.appConfig.baseServicesURL}/api/BI/CreateChartWidget`
    );
  }
  createNewTableWidget() {
    return this.$http.get<IWidgetTable>(
      `${this.appConfig.baseServicesURL}/api/BI/CreateTableWidget`
    );
  }

  saveReport(report: IReport) {
    //to support the API's request format
    const charts = [];
    const tables = [];
    const fixedCharts = [];
    const texts = [];
    const gauges = [];
    const kpis = [];
    report.Sections.forEach((section) => {
      section.Widgets.forEach((widget) => {
        switch (widget.Kind) {
          case 'Chart':
            charts.push(widget);
            break;
          case 'Table':
            tables.push(widget);
            break;
          case 'FixedChart':
            fixedCharts.push(widget);
            break;
          case 'Text':
            texts.push(widget);
            break;
          case 'Gauge':
            gauges.push(widget);
            break;
          case 'KPI':
            kpis.push(widget);
            break;
        }
      });
    });

    return this.$http.post<IReport>(
      `${this.appConfig.baseServicesURL}/api/BI/Report`,
      {
        Report: report,
        WidgetChart: charts,
        WidgetTable: tables,
        WidgetFixedChart: fixedCharts,
        WidgetText: texts,
        WidgetGauge: gauges,
        WidgetKPI: kpis,
      }
    );
  }

  deleteReport(reportId: number) {
    return this.$http.delete(
      `${this.appConfig.baseServicesURL}/api/BI/Report?reportID=${reportId}`
    );
  }

  saveTextbox(content: string, widgetId: string) {
    return this.$http.post<boolean>(
      `${this.appConfig.baseServicesURL}/api/BI/SaveTextWidgetContent`,
      {
        Content: content,
        TextWidgetID: widgetId,
      }
    );
  }

  getViews(asset: string, node: string) {
    if (isNil(asset) && isNil(node)) {
      return of([]);
    }

    const reportRetrievalCriteria = {
      AssetId: asset,
      NodeId: node,
      IncludeDescendants: false,
    };

    return this.$http.post<IReport[]>(
      `${this.appConfig.baseServicesURL}/api/BI/GetReportsForNode`,
      reportRetrievalCriteria
    );
  }
}
