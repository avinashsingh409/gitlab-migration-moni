import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import {
  IAlertTimelineResult,
  INDModelActionItem,
  IActionItem,
} from '../../models/alert-data-retrieval';
import { IAssetModelChartingData, INDModel } from '../../models/charting-data';
import {
  INDAlerNotification,
  INDModelsListResult,
  INDModelSummary,
  INDQuickDeployModelCreationResults,
} from '../../models/monitoring-interfaces';
import { Observable, of, forkJoin } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { INDModelPDTrendMap, isNil } from '@atonix/atx-core';
import {
  EPredictiveTypes,
  PredictiveMethodTypeNameMapping,
} from '../../models/model-config-data';
import {
  IHighChartsCategoryData,
  IModelConfigModelTrend,
  IRawModelTrend,
} from '../../models/model-config-trend';
import moment from 'moment';
import { ISubscriber } from '../../models/subscriber';

@Injectable({
  providedIn: 'root',
})
export class AlertsFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getModelSummary(id: string | string[]): Observable<INDModelSummary[]> {
    if (isNil(id)) {
      return of([] as INDModelSummary[]);
    } else {
      let idArray: string[];
      if (!Array.isArray(id)) {
        idArray = [id];
      } else {
        idArray = id;
      }

      return forkJoin(
        idArray.map((singleID) => {
          if (isNil(singleID)) {
            return of(null);
          } else {
            return this.$http.get<INDModelSummary>(
              `${this.appConfig.baseServicesURL}/api/Monitoring/ModelSummary`,
              {
                params: { id: String(singleID) },
              }
            );
          }
        })
      );
    }
  }

  public prioritizedOpModeDefinitionsForTagMap(
    astVarTypeTagMapID: number
  ): Observable<any> {
    const params: { astVarTypeTagMapID: number } = {
      astVarTypeTagMapID,
    };
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/PrioritizedOpModeDefinitionsForTagMap`,
      { params }
    );
  }

  public modelValidate(model: any): Observable<any> {
    return this.$http.post<any>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/Model`,
      model
    );
  }

  public modeleExecuteSaveActions(modelValidationObject: any): Observable<any> {
    return this.$http.post<any>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/ExecuteModelSaveActions`,
      modelValidationObject
    );
  }

  public setWatch(
    hours: string,
    model: INDModelSummary[]
  ): Observable<INDModelSummary[]> {
    const params = { hours };
    return this.$http
      .post<INDModelSummary[]>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/WatchNDModel`,
        model,
        {
          params,
        }
      )
      .pipe(take(1));
  }

  public setActionItemModelMaintenanceWithGuid(
    affectedModelGuids: string[],
    parameters: IActionItem,
    actionItemType: string
  ): Observable<INDModelSummary[]> {
    const modelActionItemTypeId = this.getActionItemTypeId(actionItemType);

    const params = [];

    for (const modelGuid of affectedModelGuids) {
      const actionItem = {
        NoteText: parameters.NoteText,
        ModelExtId: modelGuid,
        ModelActionItemPriorityTypeId: 0,
        ModelActionItemTypeId: modelActionItemTypeId,
      };
      params.push(actionItem);
    }

    return this.$http
      .post<INDModelSummary[]>(
        `${this.appConfig.baseSiteURL}/api/modelconfig/actionitems/modelmaintenance`,
        params
      )
      .pipe(take(1));
  }

  public actionItem(
    model: INDModelSummary[],
    parameters: IActionItem,
    actionItemType: string
  ): Observable<INDModelSummary[]> {
    const modelActionItemTypeId = this.getActionItemTypeId(actionItemType);

    const arr = [];

    if (model.length === 1) {
      const params = {
        NoteText: parameters.NoteText,
        ModelID: model[0].ModelID,
        ModelActionItemPriorityTypeId: 0,
        ModelActionItemTypeId: modelActionItemTypeId,
        WatchDuration: parameters.WatchDuration,
        WatchDurationTemporalTypeId: parameters.WatchDurationTemporalTypeId,
        WatchCriteriaTypeId: parameters.WatchCriteriaTypeId,
        WatchLimit: parameters.WatchLimit,
        WatchRelativeToExpected: parameters.WatchRelativeToExpected,
        WatchEmailIfAlert: parameters.WatchEmailIfAlert,
        WatchLimitMeetsTemporalTypeId: parameters.WatchLimitMeetsTemporalTypeId,
        Favorite: parameters.Favorite,
        WatchRelativeToExpected2: parameters.WatchRelativeToExpected2,
        WatchCriteriaTypeId2: parameters.WatchCriteriaTypeId2,
        WatchLimit2: parameters.WatchLimit2,
      };
      return this.$http
        .post<INDModelSummary[]>(
          `${this.appConfig.baseServicesURL}/api/Monitoring/ActionItem`,
          params
        )
        .pipe(take(1));
    } else {
      const params = [];

      for (const item of model) {
        const att = {
          NoteText: parameters.NoteText,
          ModelID: item.ModelID,
          ModelActionItemPriorityTypeId: 0,
          ModelActionItemTypeId: modelActionItemTypeId,
          WatchDuration: parameters.WatchDuration,
          WatchDurationTemporalTypeId: parameters.WatchDurationTemporalTypeId,
          WatchCriteriaTypeId: parameters.WatchCriteriaTypeId,
          WatchLimit: parameters.WatchLimit,
          WatchRelativeToExpected: parameters.WatchRelativeToExpected,
          WatchEmailIfAlert: parameters.WatchEmailIfAlert,
          WatchLimitMeetsTemporalTypeId:
            parameters.WatchLimitMeetsTemporalTypeId,
          Favorite: parameters.Favorite,
          WatchRelativeToExpected2: parameters.WatchRelativeToExpected2,
          WatchCriteriaTypeId2: parameters.WatchCriteriaTypeId2,
          WatchLimit2: parameters.WatchLimit2,
        };
        params.push(att);
      }

      return this.$http
        .post<INDModelSummary[]>(
          `${this.appConfig.baseServicesURL}/api/Monitoring/ActionItems`,
          params
        )
        .pipe(take(1));
    }
  }

  private getActionItemTypeId(actionItemType: string): number {
    let modelActionItemTypeId = -1;
    switch (actionItemType) {
      case 'savediagnose':
        modelActionItemTypeId = 1;
        break;
      case 'cleardiagnose':
        modelActionItemTypeId = 2;
        break;
      case 'savewatch':
        modelActionItemTypeId = 3;
        break;
      case 'clearwatch':
        modelActionItemTypeId = 4;
        break;
      case 'savemaintenance':
        modelActionItemTypeId = 6;
        break;
      case 'clearmaintenance':
        modelActionItemTypeId = 7;
        break;
      case 'savenote':
        modelActionItemTypeId = 10;
        break;
    }
    return modelActionItemTypeId;
  }

  public clearAlert(model: INDModelSummary[]) {
    const hours = '0';
    const params = { hours };
    return this.$http
      .post<boolean>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/IgnoreNDModel`,
        model,
        { params }
      )
      .pipe(take(1));
  }

  public savePDTrendsMappedToNDModel(
    modelExtID: string,
    maps: INDModelPDTrendMap[]
  ): Observable<INDModelPDTrendMap[]> {
    const params = {
      modelExtID,
    };
    return this.$http
      .post<INDModelPDTrendMap[]>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/NDModelPDTrendMap`,
        maps,
        { params }
      )
      .pipe(take(1));
  }

  public getPDModelTrendForAlertCharting(
    modelId: string | null,
    startDate?: Date,
    endDate?: Date
  ): Observable<IAssetModelChartingData> {
    const params: { modelId: string; start?: string; end?: string } = {
      modelId,
    };

    if (startDate) {
      params.start = startDate.toISOString();
    }
    if (endDate) {
      params.end = endDate.toISOString();
    }
    return this.$http
      .get<IAssetModelChartingData>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/ModelTrendForAlertCharting`,
        { params }
      )
      .pipe(take(1));
  }

  public getModelTrend(
    modelExtId: string,
    predictiveMethodType: EPredictiveTypes,
    startDate?: Date,
    endDate?: Date
  ): Observable<IModelConfigModelTrend> {
    const params: {
      modelExtId: string;
      startTime?: string;
      endTime?: string;
      predictiveMethodName?: string;
    } = {
      modelExtId,
    };

    if (startDate) {
      params.startTime = startDate.toISOString();
    }
    if (endDate) {
      params.endTime = endDate.toISOString();
    }
    if (predictiveMethodType) {
      params.predictiveMethodName =
        PredictiveMethodTypeNameMapping[predictiveMethodType];
    }

    return this.$http
      .get<IRawModelTrend>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/ModelTrend`,
        { params }
      )
      .pipe(
        map((data) => {
          if (
            isNil(data) ||
            isNil(data?.OutputDetails) ||
            isNil(data?.OutputDetails?.AssetMeasurementsSet) ||
            isNil(data?.OutputDetails?.AssetMeasurementsSet?.Measurements)
          ) {
            const modelTrend: IModelConfigModelTrend = {
              expectedValues: [],
              actualValues: [],
              expectedRange: [],
              breaks: [],
              timeStamps: [],
              startDate,
              endDate,
              tagName: '',
              tagUnits: 'n/a',
              yAxisMax: 10,
              yAxisMin: 0,
            };
            return modelTrend;
          }

          const modelTrendData =
            data?.OutputDetails?.AssetMeasurementsSet?.Measurements;

          const timeStamps = [];
          const upperData = [];
          const lowerData = [];
          const expectedValues: IHighChartsCategoryData[] = [];
          const actualValues: IHighChartsCategoryData[] = [];
          const expectedRange: IHighChartsCategoryData[][] = [];
          const modelTrend: IModelConfigModelTrend = {
            expectedValues,
            actualValues,
            breaks: [],
            expectedRange,
            startDate,
            endDate,
            timeStamps,
            tagName: '',
            tagUnits: 'n/a',
            yAxisMax: Number.MIN_VALUE,
            yAxisMin: Number.MAX_VALUE,
          };

          modelTrend.tagUnits = modelTrendData[0].Tag?.EngUnits;
          modelTrend.tagName = modelTrendData[0].Tag?.TagName;

          modelTrendData.forEach((measurement, measurementIndex) => {
            if (measurementIndex === 0) {
              measurement.Data.forEach((value) => {
                const formattedDate = moment(value.Timestamp).format(
                  'MM/DD/YY HH:mm'
                );
                timeStamps.push(formattedDate);
              });
            }
            if (
              measurement?.Tag?.TagDesc.toLowerCase().indexOf('expected') >= 0
            ) {
              measurement.Data.forEach((value) => {
                const formattedDate = moment(value.Timestamp).format(
                  'MM/DD/YY HH:mm'
                );
                expectedValues.push({
                  name: formattedDate,
                  y: value.Value,
                });
                modelTrend.yAxisMax = Math.max(
                  modelTrend.yAxisMax,
                  value.Value
                );
                modelTrend.yAxisMin = Math.min(
                  modelTrend.yAxisMin,
                  value.Value
                );
              });
            } else if (
              measurement?.Tag?.TagDesc.toLowerCase().indexOf('upper limit') >=
              0
            ) {
              measurement.Data.forEach((value) => {
                upperData.push(value.Value);
                modelTrend.yAxisMax = Math.max(
                  modelTrend.yAxisMax,
                  value.Value
                );
                modelTrend.yAxisMin = Math.min(
                  modelTrend.yAxisMin,
                  value.Value
                );
              });
            } else if (
              measurement?.Tag?.TagDesc.toLowerCase().indexOf('lower limit') >=
              0
            ) {
              measurement.Data.forEach((value) => {
                lowerData.push(value.Value);
                modelTrend.yAxisMax = Math.max(
                  modelTrend.yAxisMax,
                  value.Value
                );
                modelTrend.yAxisMin = Math.min(
                  modelTrend.yAxisMin,
                  value.Value
                );
              });
            } else {
              measurement.Data.forEach((value) => {
                const formattedDate = moment(value.Timestamp).format(
                  'MM/DD/YY HH:mm'
                );
                actualValues.push({
                  name: formattedDate,
                  y: value.Value,
                });
                modelTrend.yAxisMax = Math.max(
                  modelTrend.yAxisMax,
                  value.Value
                );
                modelTrend.yAxisMin = Math.min(
                  modelTrend.yAxisMin,
                  value.Value
                );
              });
            }
          });

          if (
            timeStamps.length > 0 &&
            upperData.length > 0 &&
            lowerData.length > 0 &&
            timeStamps.length == upperData.length &&
            upperData.length == lowerData.length
          ) {
            timeStamps.forEach((value, i) => {
              if (upperData[i] > lowerData[i]) {
                expectedRange.push([value, upperData[i], lowerData[i]]);
              } else {
                expectedRange.push([value, lowerData[i], upperData[i]]);
              }
            });

            // const sortedData = [...gaps];
            // sortedData.sort((n1, n2) => n1 - n2);
            // const q1 = this.getPercentile(sortedData, 25);
            // const q3 = this.getPercentile(sortedData, 90);
            // const iqr = q3 - q1;
            // let upperFence = q3 + iqr * 3;
            // if (upperFence < 4200000) {
            //   upperFence = 4200000;
            // }

            // const newBreaks = [];
            // gaps.forEach((value, i) => {
            //   if (value > upperFence) {
            //     newBreaks.push({
            //       from: timeStamps[i - 1] + 120000,
            //       to: timeStamps[i] - 120000,
            //     });
            //   }
            // });
            // modelTrend.breaks = newBreaks;
          }
          return modelTrend;
        })
      );
  }

  // private getPercentile(data, percentile) {
  //   const index = (percentile / 100) * data.length;
  //   let result = 0;
  //   if (Math.floor(index) == index) {
  //     result = (data[index - 1] + data[index]) / 2;
  //   } else {
  //     result = data[Math.floor(index)];
  //   }
  //   return result;
  // }

  public getPDModelTrendForAlertChartingByModelExtId(
    ModelExtId: string | null,
    startDate?: Date,
    endDate?: Date
  ): Observable<IAssetModelChartingData> {
    const params: { ModelExtId: string; start?: string; end?: string } = {
      ModelExtId,
    };

    if (startDate) {
      params.start = startDate.toISOString();
    }
    if (endDate) {
      params.end = endDate.toISOString();
    }
    return this.$http
      .get<IAssetModelChartingData>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/ModelTrendForAlertChartingByModelExtId`,
        { params }
      )
      .pipe(take(1));
  }

  public getModelByGuid(modelExtId: string) {
    const params = { modelExtId };
    return this.$http
      .get<INDModel>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/ModelByGUID`,
        { params }
      )
      .pipe(take(1));
  }

  getAlertTimeLine(
    modelId: number,
    page?: number,
    itemsPerPage?: number,
    sort?: string,
    sortOrder?: boolean,
    favorite?: boolean,
    actionItemTypeID?: number[],
    start?: Date,
    end?: Date,
    executor?: string,
    noteText?: string,
    issueCreated?: boolean,
    issueClosed?: boolean
  ) {
    const FilterParameters: any = {};
    if (!isNil(modelId)) {
      FilterParameters.modelID = Number(modelId);
    }
    if (!isNil(actionItemTypeID)) {
      FilterParameters.actionItemTypeID = actionItemTypeID;
    }
    if (!isNil(start)) {
      FilterParameters.timestampStart = start.toISOString();
    }
    if (!isNil(end)) {
      FilterParameters.timestampEnd = end.toISOString();
    }
    if (!isNil(executor)) {
      FilterParameters.executor = executor;
    }
    if (!isNil(noteText)) {
      FilterParameters.noteText = noteText;
    }
    if (!isNil(issueCreated)) {
      FilterParameters.issueCreated = issueCreated;
    }
    if (!isNil(issueClosed)) {
      FilterParameters.issueClosed = issueClosed;
    }
    if (!isNil(favorite)) {
      FilterParameters.favorite = favorite;
    }

    return this.$http
      .post<IAlertTimelineResult>(
        `${this.appConfig.baseServicesURL}/api/Monitoring/AlertTimeline`,
        {
          page: page ?? 0,
          itemsPerPage: itemsPerPage ?? 30,
          sortOrder: sortOrder ?? true,
          sort: sort ?? null,
          FilterParameters,
          getTotalItems: true,
        }
      )
      .pipe(take(1));
  }

  public updateActionItem(actionItem: INDModelActionItem) {
    const value = actionItem;
    return this.$http.post<{
      ActionItem: INDModelActionItem;
      IsMostRecent: boolean;
    }>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/UpdateActionItemArchiveNote`,
      value
    );
  }

  public getModelList(asset: number): Observable<INDModelsListResult> {
    // This is just for displaying data on the view.
    // New API will be use in the feature
    const filter = {
      startAssetID: asset,
      modelTypeID: [1, 2, 3, 4, 6],
      modelOpModeTypeID: [],
      currentBuildStatusTypeID: [],
      assetCriticalityID: [],
      modelCriticalityID: [],
      addedToAlertsStart: null,
      addedToAlertsEnd: null,
      addedToAlertsStartIndicatorPopup: {
        dateFormat: 'MM/dd/yyyy',
        maxDate: null,
        startOpen: false,
      },
      addedToAlertsEndIndicatorPopup: {
        dateFormat: 'MM/dd/yyyy',
        maxDate: null,
        startOpen: false,
      },
    };

    const params = {
      page: 0,
      itemsPerPage: 100,
      sort: 'TagGroup',
      sortOrder: false,
    };

    return this.$http.post<INDModelsListResult>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/ModelsFiltered`,
      filter,
      { params }
    );
  }

  public getAssetModelsForQuickDeploy(
    assetID: number,
    opModeTypeID: number
  ): Observable<INDQuickDeployModelCreationResults> {
    return this.$http.get<INDQuickDeployModelCreationResults>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/AssetModelsForQuickDeploy`,
      {
        params: {
          assetID,
          opModeTypeID,
        },
      }
    );
  }

  public reconcileQuickDeployModels(
    qDID: string,
    qDStartTime: string,
    models?: Array<INDModel>,
    deleteModelIds?: Array<string>,
    qDAssetGuid?: string,
    qDAstVarTypeTagMapID?: number,
    qDOpModeTypeID?: number
  ): Observable<boolean> {
    const params = {
      deleteModelIds,
      qDID,
      qDStartTime,
      qDAssetGuid,
      qDAstVarTypeTagMapID,
      qDOpModeTypeID,
    };
    return this.$http.post<boolean>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/ReconcileQuickDeployModels`,
      models,
      { params }
    );
  }

  public getModelsForTag(tagMapId: number): Observable<INDModel> {
    return this.$http.get<INDModel>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/ModelsForTag`,
      {
        params: {
          tagMapId,
        },
      }
    );
  }

  public deleteModelsByModelId(modelId: number): Observable<any> {
    return this.$http
      .delete<any>(`${this.appConfig.baseServicesURL}/api/Monitoring/Model`, {
        params: { modelId },
      })
      .pipe(
        map((isDeleted) => {
          return { modelId, isDeleted };
        })
      );
  }

  public getAlertNotification(
    alertStatusTypeId: number,
    modelId: number
  ): Observable<INDAlerNotification> {
    return this.$http.get<INDAlerNotification>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/GetAlertNotification?alertStatusTypeId=${alertStatusTypeId}&modelId=${modelId}`
    );
  }

  public getAlertNotificationParameters(
    alertStatusTypeId: number,
    modelId: number
  ): Observable<ISubscriber[]> {
    return this.$http.get<ISubscriber[]>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/AlertNotificationParameters?alertStatusTypeId=${alertStatusTypeId}&modelId=${modelId}`
    );
  }

  public getAlertNotificationParametersGroupMembers(
    userName: string
  ): Observable<ISubscriber[]> {
    return this.$http.get<ISubscriber[]>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/GroupMembers?id=${userName}`
    );
  }

  public saveAlertNotification(
    alertNotification: INDAlerNotification
  ): Observable<INDAlerNotification> {
    return this.$http.post<INDAlerNotification>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/SaveAlertNotification`,
      alertNotification
    );
  }

  public alertNotificationSubscribeUsers(
    alertStatusTypeId: number,
    modelId: number,
    subscribers: string[]
  ): Observable<{ ErrorMessage: string; SuccessMessage: string }> {
    const params = {
      AlertStatusTypeID: alertStatusTypeId,
      ModelID: modelId,
      Subscribers: subscribers,
    };
    return this.$http.post<{ ErrorMessage: string; SuccessMessage: string }>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/Subscribe`,
      params
    );
  }

  public alertNotificationUnsubscribeUsers(
    alertStatusTypeId: number,
    modelId: number,
    subscribers: string[]
  ): Observable<{ ErrorMessage: string; SuccessMessage: string }> {
    const params = {
      AlertStatusTypeID: alertStatusTypeId,
      ModelID: modelId,
      Subscribers: subscribers,
    };
    return this.$http.post<{ ErrorMessage: string; SuccessMessage: string }>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/Unsubscribe`,
      params
    );
  }

  public deleteAlertNotification(
    modelId: number,
    alertNotificationId: string
  ): Observable<boolean> {
    return this.$http.delete<boolean>(
      `${this.appConfig.baseServicesURL}/api/Monitoring/DeleteAlertNotification?ModelID=${modelId}&NDAlertNotificationID=${alertNotificationId}`
    );
  }
}
