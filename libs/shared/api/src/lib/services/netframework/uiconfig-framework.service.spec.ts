import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { UIConfigFrameworkService } from './uiconfig-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('UiconfigFrameworkService', () => {
  let service: UIConfigFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(UIConfigFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
