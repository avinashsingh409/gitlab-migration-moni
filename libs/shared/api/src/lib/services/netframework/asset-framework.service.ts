import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  IAsset,
  IAssetAndAttributes,
  IAssetClassType,
  IAssetInfo,
  IAtxHttpResponse,
  IAutoCompleteItem,
  ITaggingKeyword,
} from '@atonix/atx-core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { map, take } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class AssetFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAssetAndAncestors(assetID: number) {
    const params = { assetId: assetID.toString() };
    return this.$http.get<IAssetInfo>(
      `${this.appConfig.baseServicesURL}/api/Assets/Asset`,
      { params }
    );
  }

  getAssetClassTypesForAsset(
    assetId: string,
    parentAssetClassTypeID: string,
    childAssetClassTypeID: string[]
  ) {
    const params = { assetId, parentAssetClassTypeID, childAssetClassTypeID };
    return this.$http.get<IAssetClassType[]>(
      `${this.appConfig.baseServicesURL}/api/Assets/AssetClassTypesForAsset`,
      {
        params,
      }
    );
  }

  getAssetAndAttributes(assetID?: string, full: string = 'true') {
    const params = {
      assetID,
      full,
    };
    return this.$http.get<IAssetAndAttributes>(
      `${this.appConfig.baseServicesURL}/api/Assets/AssetAndAttributes`,
      { params }
    );
  }

  getAttachmentPopupLink(attachmentId: string, contentId: string) {
    const params = {
      attachmentId,
      contentId,
    };

    return this.$http.get<string>(
      `${this.appConfig.baseServicesURL}/api/Assets/AttachmentPopupLink`,
      { params }
    );
  }

  getAttachmentPopupLinkFromIDs(attachmentId: string, contentId: string) {
    const params = {
      attachmentId,
      contentId,
    };

    return this.$http
      .get<string>(
        `${this.appConfig.baseServicesURL}/api/Assets/AttachmentPopupLink`,
        { params }
      )
      .pipe(
        take(1),
        map((link) => {
          return decodeURIComponent(link);
        })
      );
  }

  addCurrentUserToAssetBlog(assetId: string, discussionAssetType: string) {
    const params = { assetId, discussionAssetType };
    return this.$http.get<any>(
      `${this.appConfig.baseMicroServicesURL}/api/Assets/AddCurrentUserToAssetBlog`,
      { params }
    );
  }

  searchTaggingKeywords(
    assetID: string,
    searchExpression: string,
    limitResultsTo: string = '20'
  ) {
    const searchObject = {
      Tag: searchExpression,
      AssetIDs: [assetID],
    };

    const params = { limitResultsTo };
    return this.$http.post<ITaggingKeyword[]>(
      `${this.appConfig.baseServicesURL}/api/Assets/SearchTaggingKeywords`,
      searchObject,
      { params }
    );
  }

  isReservedKeyword(assetID: string, tag: string) {
    const modelObject = {
      Tag: tag,
      AssetIDs: [assetID],
    };

    return this.$http.post<boolean>(
      `${this.appConfig.baseServicesURL}/api/Assets/IsReservedKeyword`,
      modelObject
    );
  }

  addTaggingKeyword(assetID: string, tag: string) {
    const modelObject = {
      Tag: tag,
      AssetIDs: [assetID],
    };

    return this.$http.post<ITaggingKeyword[]>(
      `${this.appConfig.baseServicesURL}/api/Assets/AddTaggingKeyword`,
      modelObject
    );
  }

  getAssetFromGuid(assetGuid: string) {
    const params = { assetGuid: assetGuid };
    return this.$http.get<IAsset>(
      `${this.appConfig.baseServicesURL}/api/Assets/AssetFromGuid`,
      { params }
    );
  }

  //Deep Search for an asset in the selected Asset Tree
  autoCompleteSearch(search: string, tree: string, count: number = 5) {
    const params = {
      search,
      count: String(count),
    };
    return this.$http
      .get<IAtxHttpResponse<IAutoCompleteItem>>(
        `${this.appConfig.baseSiteURL}/api/v1/assets/trees/${tree}/search`,
        {
          params,
        }
      )
      .pipe(map((res) => res.Results));
  }
}
