import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AlertsFrameworkService } from './alerts-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('AlertsFrameworkService', () => {
  let service: AlertsFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AlertsFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
