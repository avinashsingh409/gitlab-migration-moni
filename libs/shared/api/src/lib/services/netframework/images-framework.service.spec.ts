import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ImagesFrameworkService } from './images-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('ImagesFrameworkService', () => {
  let service: ImagesFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(ImagesFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
