import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { take, map } from 'rxjs/operators';
import { IListConfig } from '../../models/list-config';
import {
  ISavedList,
  ISavedModelList,
  ISavedIssueList,
  ISavedModelConfigList,
} from '../../models/saved-list';
import { Observable, of, forkJoin } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IAtxAssetTree,
  IAtxAssetTreeNode,
  IAutoCompleteItem,
  ICriteria,
  isNil,
  ITimeData,
} from '@atonix/atx-core';
import { ISentiment } from '../../models/ui-config-sentiment';
import { IListStateV2 } from '../../models/list-state';

@Injectable({
  providedIn: 'root',
})
export class UIConfigFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getModelLists(): Observable<ISavedModelList[]> {
    return this.getLists('alerts');
  }

  getIssueLists(): Observable<ISavedIssueList[]> {
    return this.getLists('issues');
  }

  public getModelConfigLists(
    listType: 'models'
  ): Observable<ISavedModelConfigList[]> {
    return this.$http
      .get<IListConfig[]>(
        `${this.appConfig.baseServicesURL}/api/UIConfig/ListConfig`,
        {
          params: { listType: listType },
        }
      )
      .pipe(
        map((n) => {
          return n.map((list) => {
            return {
              id: list.ListConfigID,
              name: list.Name,
              state: JSON.parse(list.Content),
              checked: false,
            };
          });
        })
      );
  }

  private getLists(
    listType: 'alerts' | 'issues' | 'models'
  ): Observable<ISavedList[]> {
    return this.$http
      .get<IListConfig[]>(
        `${this.appConfig.baseServicesURL}/api/UIConfig/ListConfig`,
        {
          params: { listType: listType },
        }
      )
      .pipe(
        map((n) => {
          return n.map((list) => {
            return {
              id: list.ListConfigID,
              name: list.Name,
              state: JSON.parse(list.Content),
              checked: false,
            };
          });
        })
      );
  }

  public saveSentiment(sentiment: ISentiment) {
    const newSentiment = { ...sentiment, Url: window?.location?.href };
    return this.$http.post<boolean>(
      `${this.appConfig.baseServicesURL}/api/UIConfig/Sentiment`,
      newSentiment
    );
  }

  saveModelList(list: ISavedModelList): Observable<ISavedList> {
    return this.saveList(list, 'alerts');
  }

  saveIssueList(list: ISavedModelList): Observable<ISavedList> {
    return this.saveList(list, 'issues');
  }

  public saveModelsList(name: string, list: IListStateV2) {
    const listConfig: IListConfig = {
      ListConfigID: -1,
      ListType: 'models',
      Owner: '',
      Name: name,
      Content: JSON.stringify(list),
    };

    return this.$http
      .post<IListConfig>(
        `${this.appConfig.baseServicesURL}/api/UIConfig/ListConfig`,
        listConfig
      )
      .pipe(
        map((savedList) => {
          return {
            id: savedList.ListConfigID,
            name: savedList.Name,
            state: JSON.parse(savedList.Content),
            checked: true,
          };
        })
      );
  }

  private saveList(
    list: ISavedList,
    listType: 'alerts' | 'issues'
  ): Observable<ISavedList> {
    const listConfig: IListConfig = {
      ListConfigID: list.id,
      ListType: listType,
      Owner: '',
      Name: list.name,
      Content: JSON.stringify(list.state),
    };

    return this.$http
      .post<IListConfig>(
        `${this.appConfig.baseServicesURL}/api/UIConfig/ListConfig`,
        listConfig
      )
      .pipe(
        map((savedList) => {
          return {
            id: savedList.ListConfigID,
            name: savedList.Name,
            state: JSON.parse(savedList.Content),
            checked: true,
          };
        })
      );
  }

  deleteList(id: number) {
    return this.$http.delete<boolean>(
      `${this.appConfig.baseServicesURL}/api/UIConfig/ListConfig`,
      {
        params: { id: String(id) },
      }
    );
  }

  getTimeRangeDateRangeFromCriteria(
    criteriaFilterid: string,
    startDate: string,
    endDate: string
  ) {
    const params = {
      criteriaFilterid,
      startDate,
      endDate,
    };

    return this.$http.get<ITimeData>(
      `${this.appConfig.baseServicesURL}/api/UIConfig/TimeSelectorGetDateRangeFromCriteria`,
      {
        params,
      }
    );
  }

  // Deprecate.
  // This was for the Time slider List categories shown in Legacy
  // We are not using currently.
  // getCriteriaObjectListCategory(
  //   categoryID: string,
  //   appContextID: string = null,
  //   tabID: string = null
  // ) {
  //   const params = {
  //     categoryID,
  //     appContextID,
  //     tabID,
  //   };

  //   return this.$http.get<ICriteria[]>(
  //     `${this.appConfig.baseServicesURL}/api/UIConfig/TimeSelectorCategoryCriteria`,
  //     {
  //       params,
  //     }
  //   );
  // }
}
