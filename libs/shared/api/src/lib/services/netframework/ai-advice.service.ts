import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { Observable, of } from 'rxjs';
import { AIAdviceResponse } from '../../models/ai-advice-response';

@Injectable({
  providedIn: 'root',
})
export class AIAdviceService {
  constructor(
    private http$: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  public loadAIAdvice(promt: string): Observable<AIAdviceResponse> {
    let headers = new HttpHeaders();
    headers = headers.set('api-key', '56a8f92e1ab846dd9105e955ea3e973a');

    const body = {
      messages: [
        {
          role: 'user',
          content: '',
        },
      ],
      max_tokens: 2000,
      temperature: 0,
      frequency_penalty: 0,
      presence_penalty: 0,
      top_p: 0.5,
      stop: null,
    };

    body.messages[0].content = promt;

    return this.http$.post<AIAdviceResponse>(
      'https://pgdev-openai.openai.azure.com/openai/deployments/gpt-35-turbo/chat/completions?api-version=2023-07-01-preview',
      body,
      { headers }
    );
  }
}
