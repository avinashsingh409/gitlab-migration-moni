import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { BusinessIntelligenceFrameworkService } from './business-intelligence-framework.service';

describe('BusinessIntelligenceFrameworkService', () => {
  let service: BusinessIntelligenceFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(BusinessIntelligenceFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
