import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { Observable, of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';

import { IUserAccount, IUser, IUserPreferences } from '../../models/accounts';
@Injectable({
  providedIn: 'root',
})
export class AccountFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  // Get the user preferences from the web services.
  public userPreferences(): Observable<IUserPreferences> {
    return this.$http.get<IUserPreferences>(
      `${this.appConfig.baseServicesURL}/api/Account/UserPreferences`
    );
  }

  // Set the user preferences through the web services.
  public setUserPreferences(userPreferences: any) {
    return this.$http.post<IUserPreferences>(
      `${this.appConfig.baseServicesURL}/api/Account/SetUserPreferences`,
      userPreferences
    );
  }

  // See if user is valid and can be created in Cognito
  public initializeUser(email: string): Observable<any> {
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Account/InitializeUser`,
      {
        headers: new HttpHeaders({
          Anonymous: 'true',
        }),
        params: { email: email },
      }
    );
  }

  public user() {
    return this.$http
      .get<any>(`${this.appConfig.baseServicesURL}/api/Account/UserInfo`)
      .pipe(
        take(1),
        map((n) => {
          return {
            firstName: n?.FirstName,
            lastName: n?.LastName,
            email: n?.Email,
            customerId: n?.CustomerId,
          } as IUser;
        })
      );
  }

  public userAccount(token: string) {
    return this.$http
      .get<IUserAccount>(
        `${this.appConfig.baseServicesURL}/api/Account/UserAccount`,
        {
          params: { token },
        }
      )
      .pipe(take(1));
  }

  public isUserFederated(username: string) {
    return this.$http
      .get<boolean>(
        `${this.appConfig.baseServicesURL}/api/Account/IsUserFederated`,
        {
          params: { username },
        }
      )
      .pipe(take(1));
  }

  // This method will tell the server that the user has been authenticated.
  // It will give the back end a chance to write an entry to the AccessControl.tAuthenticationHistory
  // table indicating that the user was authenticated and when.
  userAuthenticated() {
    return this.$http.get<{ GlobalId: string; CustomerID: string }>(
      `${this.appConfig.baseServicesURL}/api/Account/UserAuthenticated`
    );
  }
}
