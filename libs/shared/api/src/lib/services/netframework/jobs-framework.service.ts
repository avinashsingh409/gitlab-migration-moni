import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { INotificationItem } from '../../models/jobs-items';
@Injectable({
  providedIn: 'root',
})
export class JobsFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  notificationViewed(groupID: string[]) {
    return this.$http
      .post<boolean>(
        `${this.appConfig.baseServicesURL}/api/Jobs/NotificationViewed`,
        groupID
      )
      .pipe(take(1));
  }

  getNotifications(
    pageSize?: number,
    offset?: number
  ): Observable<INotificationItem[][]> {
    pageSize = pageSize ?? 5;
    offset = offset ?? 0;
    return this.$http
      .get<INotificationItem[]>(
        `${this.appConfig.baseServicesURL}/api/Jobs/Notifications`,
        {
          params: { pageSize: pageSize.toString(), offset: offset.toString() },
        }
      )
      .pipe(map((n) => this.fixNotificationItems(n)));
  }

  private fixNotificationItems(
    notifications: INotificationItem[]
  ): INotificationItem[][] {
    // Make sure the dates are really dates.  Sometimes they can be strings which will make the sorting not work.
    notifications = notifications.map((notification: INotificationItem) => {
      return {
        ...notification,
        CreatedDate: new Date(notification.CreatedDate),
        ChangedDate: new Date(notification.ChangedDate),
      };
    });

    // Sort the notifications ascending
    notifications = notifications.sort((notification1, notification2) => {
      return (
        notification1.CreatedDate.getTime() -
        notification2.CreatedDate.getTime()
      );
    });

    const newNotifications: INotificationItem[][] = [];
    const groupDict: { [key: string]: INotificationItem[] } = {};

    for (const n of notifications) {
      if (groupDict[n.NotificationGroupID]) {
        groupDict[n.NotificationGroupID].unshift(n);
      } else {
        const g: INotificationItem[] = [];
        g.unshift(n);
        groupDict[n.NotificationGroupID] = g;
        newNotifications.unshift(g);
      }
    }

    return newNotifications;
  }
}
