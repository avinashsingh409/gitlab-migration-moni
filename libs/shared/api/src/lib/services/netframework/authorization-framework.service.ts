import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  AdvancedPreview,
  IAssetInfo,
  ISecurityRights,
  ITreePermissions,
  IResourceAccessType,
} from '@atonix/atx-core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { catchError, map, take, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { AccessTypeBit } from '../../models/access-type-bit';

@Injectable({
  providedIn: 'root',
})
export class AuthorizationFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  private tagEditorRights: ISecurityRights;

  getAssetAndAncestors(assetID: number) {
    const params = { assetId: assetID.toString() };
    return this.$http.get<IAssetInfo>(
      `${this.appConfig.baseServicesURL}/api/Assets/Asset`,
      { params }
    );
  }

  // Helper method to retrieve the asset tree permissions from the server.
  getPermissions() {
    return this.$http
      .get<{
        AssetRights: {
          CanView: boolean;
          CanAdd: boolean;
          CanEdit: boolean;
          CanDelete: boolean;
        };
      }>(
        `${this.appConfig.baseServicesURL}/api/Authorization/AssetAccessRights`
      )
      .pipe(
        take(1),
        map((n) => {
          return {
            canView: n?.AssetRights?.CanView,
            canAdd: n?.AssetRights?.CanAdd,
            canEdit: n?.AssetRights?.CanEdit,
            canDelete: n?.AssetRights?.CanDelete,
          } as ITreePermissions;
        })
      );
  }

  loginWithCode(code: string) {
    return this.$http.get<string>(
      `${this.appConfig.baseServicesURL}/api/Authorization/Token`,
      {
        params: {
          code,
        },
        headers: new HttpHeaders({
          Anonymous: '',
        }),
      }
    );
  }

  getAePermissions() {
    return this.$http
      .get<boolean>(
        `${this.appConfig.baseServicesURL}/api/Authorization/HasAccessToNewAssetExplorer`
      )
      .pipe(
        catchError(() => {
          return of(false);
        }),
        map((canView) => {
          return canView;
        })
      );
  }

  getEventsDashboardPermissions() {
    return this.$http
      .get<ISecurityRights>(
        `${this.appConfig.baseServicesURL}/api/Authorization/EventsDashboardApp`
      )
      .pipe(
        catchError(() => {
          const rights: ISecurityRights = {
            CanAdd: false,
            CanDelete: false,
            CanEdit: false,
            CanView: false,
            CanAdmin: false,
          };
          return of(rights);
        }),
        map((rights) => {
          return rights.CanView;
        })
      );
  }

  identityProvider(username: string) {
    let params: { [param: string]: string | string[] };
    const redirectUri = this.appConfig.authCallback;
    // eslint-disable-next-line prefer-const
    params = { username: username, redirectUri };
    return this.$http
      .get<any>(
        `${this.appConfig.baseServicesURL}/api/Authorization/IdentityProvider`,
        {
          headers: new HttpHeaders({
            Anonymous: '',
            'Content-Type': 'application/json',
          }),
          params,
        }
      )
      .pipe(
        take(1),
        map((n) => {
          if (!n) {
            return '';
          }
          return n;
        })
      );
  }

  cognitoSettings(code?: string) {
    let params: { [param: string]: string | string[] };
    if (code) {
      params = { code };
    }
    return this.$http.get<{
      PoolID: string;
      ClientID: string;
      HostedUI: string;
      Token: string;
      AuthFlowType: string;
    }>(`${this.appConfig.baseServicesURL}/api/Authorization/CognitoSettings`, {
      headers: new HttpHeaders({
        Anonymous: '',
        'Content-Type': 'application/json',
      }),
      params,
    });
  }

  logoutUser() {
    const logoutUri = this.appConfig.authCallback;
    return this.$http
      .get<any>(
        `${this.appConfig.baseServicesURL}/api/Authorization/IdentityProviderLogout`,
        {
          headers: new HttpHeaders({
            Anonymous: '',
            'Content-Type': 'application/json',
          }),
          params: {
            logoutUri,
          },
        }
      )
      .pipe(
        take(1),
        map((n) => {
          if (!n) {
            return '';
          }
          return n;
        })
      );
  }

  getTagEditorRights() {
    if (this.tagEditorRights) {
      return of(this.tagEditorRights);
    }
    return this.$http
      .get<ISecurityRights>(
        `${this.appConfig.baseServicesURL}/api/Authorization/TagEditor`
      )
      .pipe(tap((n) => (this.tagEditorRights = n)));
  }

  getAllUIAccess() {
    return this.$http
      .get<IResourceAccessType[]>(
        `${this.appConfig.baseServicesURL}/api/Authorization/UI`
      )
      .pipe(take(1));
  }
}
