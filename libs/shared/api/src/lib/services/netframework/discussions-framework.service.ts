import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import moment from 'moment';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IDiscussion,
  IDiscussionAssetMapType,
  IDiscussionAttachment,
  IDiscussionEntry,
  IQuickReplyEntry,
  toDateTimeOffset,
} from '@atonix/atx-core';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class DiscussionsFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getDiscussionForAsset(
    asset: number,
    createIfNecessary: boolean,
    showAutogen: boolean = false,
    discussionAssetType: number
  ) {
    const params = {
      asset: String(asset),
      discussionAssetType: String(discussionAssetType),
      createIfNecessary: String(createIfNecessary),
      saveTime: toDateTimeOffset(moment().toDate()),
      showAutogen: String(showAutogen),
    };

    return this.$http.get<IDiscussion[]>(
      `${this.appConfig.baseServicesURL}/api/Discussions/DiscussionForAsset`,
      { params }
    );
  }

  getDiscussionForAssetIssue(
    assetIssueID: number,
    assetIssueGuid: string,
    createIfNecessary: boolean,
    showAutogen: boolean = false
  ): Observable<IDiscussion> {
    const params = {
      assetIssueID: String(assetIssueID),
      assetIssueGuid,
      createIfNecessary: String(createIfNecessary),
      saveTime: toDateTimeOffset(moment().toDate()),
      showAutogen: String(showAutogen),
    };

    return this.$http
      .get<IDiscussion>(
        `${this.appConfig.baseServicesURL}/api/Discussions/DiscussionForAssetIssue`,
        {
          params,
        }
      )
      .pipe(take(1));
  }

  getDiscussionAssetType(
    assetID: string
  ): Observable<IDiscussionAssetMapType[]> {
    const params = { assetID };
    return this.$http
      .get<IDiscussionAssetMapType[]>(
        `${this.appConfig.baseServicesURL}/api/Discussions/DiscussionAssetType`,
        { params }
      )
      .pipe(take(1));
  }

  createDiscussionEntry(
    discussionID: string,
    title: string,
    subtitle: string
  ): Observable<IDiscussionEntry> {
    const params = { discussionID, title, subtitle };
    return this.$http
      .get<IDiscussionEntry>(
        `${this.appConfig.baseServicesURL}/api/Discussions/CreateDiscussionEntry`,
        {
          params,
        }
      )
      .pipe(
        take(1),
        map((discussionEntry: IDiscussionEntry) => {
          return {
            ...discussionEntry,
            Title: 'New Discussion Post',
            Content: null,
            collapsed: true,
            statusMessage: null,
            saving: false,
            dirty: false,
            attachmentsToDelete: [],
            newAttachments: [],
            newKeywords: [],
          };
        })
      );
  }

  deleteDiscussionEntry(id: string): Observable<boolean> {
    const params = {
      id,
      saveTime: toDateTimeOffset(moment().toDate()),
    };

    return this.$http
      .delete<boolean>(
        `${this.appConfig.baseServicesURL}/api/Discussions/DiscussionEntry`,
        { params }
      )
      .pipe(take(1));
  }

  createDiscussionAttachment(
    discussionID: string,
    entryID: string,
    title: string,
    type: string,
    displayOrder
  ): Observable<IDiscussionAttachment> {
    const params = {
      discussionID,
      entryID,
      title: encodeURIComponent(title),
      type,
      displayOrder,
    };

    return this.$http
      .get<IDiscussionAttachment>(
        `${this.appConfig.baseServicesURL}/api/Discussions/CreateDiscussionAttachment`,
        { params }
      )
      .pipe(take(1));
  }

  notifyDiscussionSubscribers(entry: IDiscussionEntry): Observable<void> {
    return this.$http
      .post<void>(
        `${this.appConfig.baseServicesURL}/api/Discussions/NotifyDiscussionSubscribers`,
        entry
      )
      .pipe(take(1));
  }

  saveDiscussionEntryWithAttachments(
    entry: IDiscussionEntry,
    attachmentsToDelete: string[],
    sendEmails: boolean
  ): Observable<IDiscussionEntry> {
    const params = {
      saveTime: toDateTimeOffset(moment().toDate()),
      attachmentsToDelete,
      sendEmails: String(sendEmails),
    };

    return this.$http
      .post<IDiscussionEntry>(
        `${this.appConfig.baseServicesURL}/api/Discussions/DiscussionEntryWithAttachments`,
        entry,
        { params }
      )
      .pipe(take(1));
  }

  saveQuickReply(quickReplyEntry: IQuickReplyEntry): Observable<any> {
    return this.$http.post<any>(
      `${this.appConfig.baseServicesURL}/api/Discussions/QuickReply`,
      quickReplyEntry
    );
  }
}
