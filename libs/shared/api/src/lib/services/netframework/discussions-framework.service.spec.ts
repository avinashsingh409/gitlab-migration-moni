import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { DiscussionsFrameworkService } from './discussions-framework.service';

describe('DiscussionsFrameworkService', () => {
  let service: DiscussionsFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(DiscussionsFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
