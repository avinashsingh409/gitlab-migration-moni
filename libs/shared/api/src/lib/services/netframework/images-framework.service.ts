import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { take, map, tap } from 'rxjs/operators';
import {
  IAWSFileUrl,
  IFileInfo,
  IImageDetails,
  IProcessedTrend,
} from '@atonix/atx-core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { ExportingOptions } from 'highcharts';
import moment from 'moment';
import { INotificationItem } from '../../models/jobs-items';
import { Observable, of } from 'rxjs';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
@Injectable({
  providedIn: 'root',
})
export class ImagesFrameworkService {
  constructor(
    private $http: HttpClient,
    private sanitizer: DomSanitizer,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  downloadCSV(id: string) {
    const params = {
      id,
    };
    return this.$http.get<string>(
      `${this.appConfig.baseServicesURL}/api/Images/TempCSV`,
      { params }
    );
  }

  saveTempCSV(
    isGroupedSeries: boolean,
    trend: IProcessedTrend,
    config?: Highcharts.Options
  ) {
    const data = this.getContentAsString(isGroupedSeries, trend, config);
    const filename = 'chart.csv';
    const mimetype = 'application/octet-stream';
    return this.$http.post<string>(
      `${this.appConfig.baseServicesURL}/api/Images/TempCSV`,
      {
        FileName: filename,
        MimeType: mimetype,
        Content: data,
      }
    );
  }

  public notificationUrl(groupID: string) {
    return this.$http
      .get<string>(
        `${this.appConfig.baseServicesURL}/api/Images/DownloadNotification`,
        {
          params: {
            group: groupID,
          },
        }
      )
      .pipe(take(1));
  }

  public downloadNotificationFile(notifications: INotificationItem[]) {
    const notification = notifications.find((n) => n.NotificationTypeID === 3);
    if (notification === null) {
      console.log('Nothing to download');
      return of('');
    } else {
      this.notificationUrl(notification.NotificationGroupID).pipe(
        tap((url) => {
          const filename = decodeURIComponent(url);
          const link = document.createElement('a');
          document.body.appendChild(link);
          link.style.display = 'none';
          link.href = filename;
          link.download = filename;
          link.click();
          setTimeout(() => {
            document.body.removeChild(link);
            URL.revokeObjectURL(filename);
          }, 100);
        })
      );
    }
  }

  private getContentAsString(
    isGroupedSeries: boolean,
    trend: IProcessedTrend,
    config?: Highcharts.Options
  ) {
    let c = null;
    if (isGroupedSeries) {
      c = 'Atonix Chart Definition\r\n ';
      c += trend.trendDefinition.Title + '\r\n';
      for (const bar of trend.measurements.Bands) {
        c += bar.Label + '\r\n';
        for (const measurement of bar.Measurements) {
          c += measurement.Name + ',';
          for (let i = 1; i <= 5; i++) {
            if (measurement['Result' + i]) {
              c += measurement['Result' + i] + ',';
            }
          }
          c += '\r\n';
        }
      }
      return c;
    } else {
      const configuration = config;

      const axes: { [key: number]: string } = {};

      for (const a of configuration.yAxis as Highcharts.AxisOptions[]) {
        axes[a.id] = a.title.text.replace(/,/g, '');
      }

      const xAxisName = (
        configuration.xAxis as Highcharts.AxisOptions
      ).title.text.replace(/,/g, '');

      const isXAxisDateTime =
        (configuration.xAxis as Highcharts.AxisOptions).type === 'datetime';

      const timeStamp = new Date(Date.now()).toString();
      const timeZone = timeStamp.slice(timeStamp.search(/GMT.*/));

      c = 'Atonix Chart Definition\r\n ';
      c += `Created At: ${timeStamp}\r\n `;
      c += `Time Zone: ${timeZone}\r\n`;
      let seriesTitleRow = '';
      let columnHeaderRow = '';
      let rowCount = 0;

      for (const element of configuration.series) {
        seriesTitleRow += element.name.replace(/,/g, '') + ',,';
        columnHeaderRow += xAxisName + ',' + axes[element.yAxis] + ',';
        rowCount = (element as any).data.length;
      }

      c += seriesTitleRow + '\r\n' + columnHeaderRow + '\r\n';

      for (let row = 0; row < rowCount; row++) {
        for (const i of configuration.series) {
          if (row < (i as any).data.length) {
            if (isXAxisDateTime) {
              let d = null;
              d = ((i as any).data[row] as any).x;
              c += moment(d).utc(true).format('MM/DD/YYYY h:mm:ss a');
            } else {
              c += ((i as any).data[row] as any).x;
            }
            c += ',' + ((i as any).data[row] as any).y + ',';
          } else {
            c += ',,';
          }
        }
        c += '\r\n';
      }

      return c;
    }
  }

  getFileInfo(uniqueID: string) {
    const params = { uniqueID };
    return this.$http
      .get<IFileInfo>(`${this.appConfig.baseServicesURL}/api/Images/FileInfo`, {
        params,
      })
      .pipe(
        take(1),
        map((file: IFileInfo) => {
          file.Url = decodeURIComponent(file.Url);
          return file;
        })
      );
  }

  getFileUrl(
    id: string | null,
    fileName: string,
    upload: boolean,
    fileType?: string,
    width?: number | null,
    bucketType?: string
  ): Observable<IAWSFileUrl> {
    const params = {
      id,
      fileName: encodeURIComponent(fileName),
      upload: String(upload),
      fileType,
      width: String(width),
      bucketType,
    };

    return this.$http
      .get<IAWSFileUrl>(
        `${this.appConfig.baseServicesURL}/api/Images/FileURL`,
        { params }
      )
      .pipe(
        take(1),
        map((awsFile: IAWSFileUrl) => {
          return {
            ...awsFile,
            Url: decodeURIComponent(awsFile.Url),
          };
        })
      );
  }

  getQuickReplyFileURL(
    contentID: string,
    fileName: string,
    fileType?: string
  ): Observable<IAWSFileUrl> {
    const params = {
      id: contentID,
      fileName: fileName,
      fileType: fileType,
    };

    return this.$http
      .get<IAWSFileUrl>(
        `${this.appConfig.baseServicesURL}/api/Images/QuickReplyFileURL`,
        { params }
      )
      .pipe(
        take(1),
        map((awsFile: IAWSFileUrl) => {
          return {
            ...awsFile,
            Url: decodeURIComponent(awsFile.Url),
          };
        })
      );
  }

  uploadFile(
    uploadUrl: string,
    file: any,
    includeFilenameHeader: boolean = true
  ) {
    return new Observable<boolean>((res) => {
      const xhr = new XMLHttpRequest();

      xhr.open('PUT', uploadUrl, true);
      (xhr as any).msCaching = 'disabled';
      if (includeFilenameHeader) {
        xhr.setRequestHeader(
          'x-amz-meta-filename',
          encodeURIComponent(file.name)
        );
      }
      xhr.setRequestHeader(
        'Content-Type',
        file.type || 'application/octet-stream'
      );

      xhr.onload = (event: any) => {
        if (xhr.status === 200) {
          res.next(true);
          res.complete();
        } else {
          res.error({ success: false });
          // eslint-disable-next-line rxjs/no-redundant-notify
          res.complete();
        }
      };
      xhr.onerror = (err: any) => {
        res.error({ success: false, error: err.message });
        // eslint-disable-next-line rxjs/no-redundant-notify
        res.complete();
      };

      xhr.send(file);
    });
  }

  downloadFile(fileURL: string, fileName: string) {
    return new Observable((observer) => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', fileURL, true);
      xhr.responseType = 'blob';
      xhr.onload = function () {
        if (xhr.readyState === 4) {
          observer.next(xhr.response);
          observer.complete();
        }
      };
      xhr.send();
    }).subscribe((blob: any) => {
      const downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(blob);
      downloadLink.download = fileName;
      downloadLink.click();
    });
  }

  convertToBlob(data: IAWSFileUrl) {
    return new Observable<SafeStyle>((res) => {
      const oReq = new XMLHttpRequest();
      oReq.open('GET', data.Url, true);
      let blobUrl: string;

      oReq.responseType = 'blob';

      oReq.onload = (oEvent: any) => {
        if (oReq.status === 200) {
          const blob = new Blob([oReq.response], {
            type: oReq.getResponseHeader('Content-Type') || 'image/png',
          });
          blobUrl = window.URL.createObjectURL(blob);
          const sanitizeUrl: SafeStyle =
            this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          // return sanitize url even though there is an error for images which are not present in the s3
          // same with the old one which ignore the console error to just display other images
          res.next(sanitizeUrl);
          res.complete();
        } else {
          res.next('');
          res.complete();
        }
      };

      oReq.send(null);
    });
  }

  convertToImageDetails(data: IAWSFileUrl) {
    return new Observable<IImageDetails>((res) => {
      const oReq = new XMLHttpRequest();
      oReq.open('GET', data.Url, true);
      let blobUrl: string;

      oReq.responseType = 'blob';

      oReq.onload = (oEvent: any) => {
        if (oReq.status === 200) {
          const blob = new Blob([oReq.response], {
            type: oReq.getResponseHeader('Content-Type') || 'image/png',
          });
          blobUrl = window.URL.createObjectURL(blob);
          const sanitizeUrl: SafeStyle =
            this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          // return sanitize url even though there is an error for images which are not present in the s3
          // same with the old one which ignore the console error to just display other images
          res.next({
            BlobUrl: sanitizeUrl,
            ContentID: data.ContentID,
            Url: data.Url,
          });
          res.complete();
        } else {
          res.next({
            BlobUrl: '',
            ContentID: data.ContentID,
            Url: data.Url,
          });
          res.complete();
        }
      };

      oReq.send(null);
    });
  }

  saveFileInfo(
    fileName: string,
    assetID: number,
    contentID: string,
    quickReply: boolean
  ): Observable<IAWSFileUrl> {
    const params = {
      fileName: encodeURIComponent(fileName),
      assetID: String(assetID),
      contentID,
      quickReply: String(quickReply),
    };

    return this.$http
      .post<IAWSFileUrl>(
        `${this.appConfig.baseServicesURL}/api/Images/SaveFileInfo`,
        null,
        { params }
      )
      .pipe(
        take(1),
        map((saveAWSFile: IAWSFileUrl) => {
          return {
            ...saveAWSFile,
            Url: decodeURIComponent(saveAWSFile.Url),
          };
        })
      );
  }

  getExportSettings() {
    const menuItems = [];
    menuItems.push('viewFullscreen');
    menuItems.push('printChart');
    menuItems.push('separator');
    menuItems.push('downloadPNG');
    menuItems.push('downloadJPEG');
    menuItems.push('downloadSVG');

    const exportSettings: ExportingOptions = {
      enabled: true,
      url: `${this.appConfig.baseServicesURL}/api/Images/HighchartsExport`,
      width: 800,
      buttons: {
        contextButton: {
          menuItems,
        },
      },
    };
    return exportSettings;
  }
}
