import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AssetFrameworkService } from './asset-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('AssetFrameworkService', () => {
  let service: AssetFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AssetFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
