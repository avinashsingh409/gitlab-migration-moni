import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, map, switchMap } from 'rxjs/operators';
import { isArray } from 'lodash';

import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  CriteriaObjectTypes,
  getTrendTotalSeries,
  GroupedSeriesType,
  IArchiveExt,
  IAssetMeasurementsSet,
  ICriteria,
  INDModelPDTrendMap,
  IPDServer,
  IPDTag,
  IPDTrend,
  IPDTrendSeries,
  IProcessedTrend,
  ISummaryType,
  IWidgetChart,
  IWidgetTable,
  LegacyDataFilter,
  TimeFilter,
} from '@atonix/atx-core';
import { Observable, of, throwError } from 'rxjs';
import { IKpiData } from '../../models/bi-models';

@Injectable({
  providedIn: 'root',
})
export class ProcessDataFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getCriteriaWithDataTimeFilters(assetGuid: string) {
    const params: {
      assetGuid: string;
    } = {
      assetGuid,
    };
    return this.$http.get<ICriteria[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/GetCriteriaWithDataTimeFilters`,
      {
        params,
      }
    );
  }

  getCriteriaObjectTypes() {
    return this.$http.get<CriteriaObjectTypes>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/GetCriteriaObjectTypes`
    );
  }

  getPDServers(asset: string) {
    return this.$http.get<IPDServer[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/PDServers`,
      {
        params: { asset },
      }
    );
  }

  convertVariablesToTags(series: IPDTrendSeries[]) {
    return this.$http.post<IPDTrendSeries[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/VariablesToTags`,
      series
    );
  }

  getCriteriaWithDataTimeFiltersAndParams(uniqueKey: string) {
    return this.$http.get<TimeFilter[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/GetCriteriaWithDataTimeFiltersAndParams?uniqueKey=${uniqueKey}`
    );
  }

  getSummaryTypes() {
    return this.$http.get<ISummaryType[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/SummaryTypes`
    );
  }

  saveParams(timeFilter: TimeFilter) {
    return this.$http
      .post<TimeFilter>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/SaveDataFilterCriteriaObject`,
        timeFilter
      )
      .pipe(
        catchError((error: unknown) => {
          console.error(JSON.stringify(error));
          return throwError('an error occurred');
        })
      );
  }

  deleteTimeFilter(timeFilter: TimeFilter) {
    return this.$http
      .post<boolean>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/deletedatafiltercriteriaobj`,
        timeFilter
      )
      .pipe(
        catchError((error: unknown) => {
          console.error(JSON.stringify(error));
          return throwError('an error occurred');
        })
      );
  }

  getTrendsPathLinkedToCriteriaObject(
    assetGlobalID: string,
    dfCriteriaObjID: number
  ) {
    const params = {
      assetGlobalID: assetGlobalID,
      criteriaObjID: dfCriteriaObjID,
    };
    return this.$http.get<string[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/gettrendspathforcriteriaobj`,
      { params: params }
    );
  }

  savePDTrendForAsset(
    assetID: number,
    endDate: Date,
    startDate: Date,
    trend: any // TODO: IPDTrend requires a refactor between charts/alarms
  ) {
    const startString = startDate.toISOString();
    const endString = endDate.toISOString();
    return this.$http.post<number>(
      // eslint-disable-next-line max-len
      `${this.appConfig.baseServicesURL}/api/ProcessData/SavePDTrendForAsset?assetID=${assetID}&endDate=${endString}&startDate=${startString}`,
      trend
    );
  }

  testEndpoint(legacyFilter: LegacyDataFilter) {
    const aDayAgo = this.getADayAgo().toISOString();
    const today = this.getToday().toISOString();
    return this.$http
      .post<any>(
        // eslint-disable-next-line max-len
        `${this.appConfig.baseServicesURL}/api/ProcessData/ResolveCriteriaObject?endDate=${today}&startDate=${aDayAgo}`,
        legacyFilter
      )
      .pipe(
        catchError((error: unknown) => {
          console.error(JSON.stringify(error));
          return throwError('an error occurred');
        })
      );
  }

  public getViewExplorerTrend(
    widget: IWidgetChart | IWidgetTable,
    assetID: number,
    startDate: Date,
    endDate: Date
  ) {
    const params = {
      trendID: String(widget.PDTrendID),
      assetID: String(assetID),
      start: startDate?.toISOString(),
      end: endDate?.toISOString(),
    };
    return this.$http
      .get<IPDTrend[]>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/PDTrends`,
        { params }
      )
      .pipe(
        map((trends) => {
          const result: {
            widget: IWidgetChart | IWidgetTable;
            trend: IProcessedTrend;
          } = { widget, trend: null };

          if (trends && trends.length > 0) {
            const trendDef: IProcessedTrend = {
              id: String(trends[0].PDTrendID),
              label: trends[0].Title,
              trendDefinition: trends[0],
              groupedSeriesType: GroupedSeriesType.NONE,
              groupedSeriesSubType: '',
              totalSeries: getTrendTotalSeries(trends[0]),
              labelIndex: null,
            };

            result.trend = {
              ...trendDef,
              startDate,
              endDate,
              assetID: String(assetID),
            } as IProcessedTrend;
            result.trend.trendDefinition.BandAxisBarsResolved = null;
          }
          return result;
        })
      );
  }

  getTrends(assetID: string): Observable<IProcessedTrend[]> {
    const params: {
      assetID: string;
      preventOverride: string;
    } = {
      assetID,
      preventOverride: 'false',
    };

    return this.$http
      .get<IPDTrend[]>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/PDTrends`,
        { params }
      )
      .pipe(
        map((trends) => {
          // Used sessionStorage rather than localStorage here to
          // respect opening multiple Model Context Trend consecutively in a short period of time.
          if (window.sessionStorage.getItem('ModelContextTrend')) {
            const modelContext: IPDTrend = JSON.parse(
              window.sessionStorage.getItem('ModelContextTrend')
            );
            modelContext.Title = 'New Trend 1';
            modelContext.TrendDesc = 'New Trend 1';
            modelContext.DisplayOrder = 10;
            trends.push(modelContext);
            window.sessionStorage.removeItem('ModelContextTrend');
          }

          return trends.map((m) => {
            const result = {
              id: String(m.PDTrendID),
              label: m.Title,
              trendDefinition: m,
              totalSeries: getTrendTotalSeries(m),
              labelIndex: null,
            } as IProcessedTrend;
            return {
              ...result,
              assetID,
            };
          });
        })
      );
  }

  deleteTrend(trendID: number) {
    return this.$http.delete<boolean>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/DeleteTrend?trendID=${trendID}`
    );
  }

  getPDNDModelTrends(
    assetID: string | null,
    modelExtID: string | null,
    getMappedTrends?: boolean,
    startDate?: Date,
    endDate?: Date
  ) {
    const params: {
      assetID: string;
      modelExtID: string;
      getMappedTrends?: string;
      start?: string;
      stop?: string;
    } = {
      assetID,
      modelExtID,
    };

    if (startDate) {
      params.start = startDate.toISOString();
    }
    if (endDate) {
      params.stop = endDate.toISOString();
    }
    if (getMappedTrends === false) {
      params.getMappedTrends = String(getMappedTrends);
    }
    return this.$http
      .get<INDModelPDTrendMap[]>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/PDNDModelTrends`,
        {
          params,
        }
      )
      .pipe(
        map((trends) => {
          const sortedTrend = [...trends];
          return sortedTrend.sort((a, b) => {
            if (a.DisplayOrder < b.DisplayOrder) {
              return -1;
            } else if (a.DisplayOrder > b.DisplayOrder) {
              return 1;
            } else {
              return 0;
            }
          });
        })
      );
  }

  getPDNDModelTrendsWithAssetGuid(
    assetGuid: string | null,
    modelExtID: string | null,
    getMappedTrends?: boolean,
    startDate?: Date,
    endDate?: Date
  ) {
    const params: {
      assetGuid: string;
      modelExtID: string;
      getMappedTrends?: string;
      start?: string;
      stop?: string;
    } = {
      assetGuid,
      modelExtID,
    };

    if (startDate) {
      params.start = startDate.toISOString();
    }
    if (endDate) {
      params.stop = endDate.toISOString();
    }
    if (getMappedTrends === false) {
      params.getMappedTrends = String(getMappedTrends);
    }
    return this.$http
      .get<INDModelPDTrendMap[]>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/PDNDModelTrendsWithAssetGuid`,
        {
          params,
        }
      )
      .pipe(
        map((trends) => {
          const sortedTrend = [...trends];
          return sortedTrend.sort((a, b) => {
            if (a.DisplayOrder < b.DisplayOrder) {
              return -1;
            } else if (a.DisplayOrder > b.DisplayOrder) {
              return 1;
            } else {
              return 0;
            }
          });
        })
      );
  }

  getTagsDataFiltered(
    trendDefinition: IPDTrend,
    startDate?: Date,
    endDate?: Date,
    numPoints?: number,
    archive?: string,
    assetID?: string | null
  ): Observable<IAssetMeasurementsSet> {
    const trendToSend = {
      ...trendDefinition,
      ShowPins: true,
      BandAxisBarsResolved: null,
      Areas: trendDefinition.Areas ? trendDefinition.Areas : [],
      CreatedBy: null,
    };
    const params: {
      startDate?: string;
      endDate?: string;
      numPoints?: string;
      archive?: string;
      asset?: string;
    } = {};
    if (assetID) {
      params.asset = assetID;
    }
    if (endDate) {
      params.endDate = endDate.toISOString();
    }
    params.numPoints = numPoints ? numPoints.toString() : '0';
    if (archive) {
      params.archive = archive;
    }
    if (startDate) {
      params.startDate = startDate.toISOString();
    }

    return this.$http
      .post<IAssetMeasurementsSet>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/TagsDataFiltered`,
        trendToSend,
        { params }
      )
      .pipe(
        map((measurementSet) => {
          // We convert TimeStamp strings to Time Dates once
          // instead of converting string to Dates everytime we need them
          measurementSet?.Measurements?.forEach((measurement) => {
            measurement.Data.forEach((dataPoint) => {
              dataPoint.Time = new Date((dataPoint as any).Timestamp);
            });
          });
          return measurementSet;
        })
      );
  }

  getArchivesExtFromTagId(tagID: number | number[]) {
    let tagsArray: number[] = [];
    if (isArray(tagID)) {
      tagsArray = tagID;
    } else {
      tagsArray.push(tagID);
    }

    const params = { IDs: tagsArray };

    return this.$http.post<IArchiveExt[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/ArchivesExtFromTagId`,
      params
    );
  }

  getTagsForServer(server: number) {
    return this.$http.get<IPDTag[]>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/PDTagsForServer`,
      {
        params: { server: String(server) },
      }
    );
  }

  saveTags(server: number, tags: IPDTag[]) {
    return this.$http
      .post<{ Server: number; Tags: IPDTag[]; Message: string }>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/PDTagsForServer`,
        {
          Server: server,
          Tags: tags,
        }
      )
      .pipe(
        map((result) => {
          return result.Message || result.Tags;
        })
      );
  }

  validateTags(server: number, tags: IPDTag[]) {
    return this.$http.post<string>(
      `${this.appConfig.baseServicesURL}/api/ProcessData/ValidatePDTagsForServer`,
      { Server: server, Tags: tags }
    );
  }

  public getGaugeData(
    sensorID: number,
    startTime: Date,
    endTime: Date,
    operation: string | number,
    scenario?: string,
    assetID?: number,
    variableTypeID?: number,
    valueTypeID?: number,
    tagID?: number,
    scale?: number
  ) {
    let operationInt: number;
    if (typeof operation === 'number') {
      operationInt = operation;
    } else {
      switch (operation) {
        case 'min':
          operationInt = 1;
          break;
        case 'max':
          operationInt = 2;
          break;
        case 'ave':
          operationInt = 3;
          break;
        case 'first':
          operationInt = 4;
          break;
        case 'last':
          operationInt = 5;
          break;
        case 'sum':
          operationInt = 6;
          break;
        case 'firsthonorbadstatus':
          operationInt = 11;
          break;
        case 'lasthonorbadstatus':
          operationInt = 12;
          break;
        case 'delta':
          operationInt = 13;
          break;
        default:
          operationInt = 3;
          break;
      }
    }

    const gaugeDataRequest = {
      SensorID: sensorID,
      StartTime: startTime.toISOString(),
      EndTime: endTime.toISOString(),
      Operation: operationInt,
      Archive: scenario,
      AssetID: assetID,
      VariableTypeID: variableTypeID,
      ValueTypeID: valueTypeID,
      TagID: tagID,
    };

    return this.$http
      .post<IKpiData>(
        `${this.appConfig.baseServicesURL}/api/ProcessData/GaugeData`,
        gaugeDataRequest
      )
      .pipe(
        map((data) => {
          let value = 0;
          let units: string = null;
          let min = 0;
          let max = 100;
          let tagName = '';
          let tagDesc = '';
          let tagAssetId = 0;
          if (data) {
            value = this.getValueFromAggregate(data, operationInt, scale);
            units = data.Units;
            min = data.Min || min;
            max = data.Max || max;
            tagName = data.Map.Tag.TagName;
            tagDesc = data.Map.Tag.TagDesc;
            tagAssetId = data.Map.AssetID;
          }
          return { value, units, min, max, tagName, tagDesc, tagAssetId };
        })
      );
  }

  private getValueFromAggregate(
    data: IKpiData,
    aggregate: number,
    scale?: number
  ) {
    let result = 0;

    // Short circuit if there is no data.
    if (!data || !data.Values || data.Values.length < 1) {
      return result;
    }

    const agVal = data.Values[0]?.AggregatedValue;
    if (agVal) {
      switch (aggregate) {
        case 1:
          result = data.Values[0].AggregatedValue.Min;
          break;
        case 2:
          result = data.Values[0].AggregatedValue.Max;
          break;
        case 3:
          result = data.Values[0].AggregatedValue.Average;
          break;
        case 4:
        case 11:
          result = data.Values[0].AggregatedValue.First;
          break;
        case 5:
        case 12:
          result = data.Values[0].AggregatedValue.Last;
          break;
        case 6:
          result = data.Values[0].AggregatedValue.Sum;
          break;

        case 9:
          result = data.Values[0].AggregatedValue.Median;
          break;

        case 10:
          result = data.Values[0].AggregatedValue.Mode;
          break;

        case 13:
          result = data.Values[0].AggregatedValue.Delta;
          break;

        case 14:
          result = data.Values[0].AggregatedValue.FirstStdAbove;
          break;
        case 15:
          result = data.Values[0].AggregatedValue.SecondStdAbove;
          break;
        case 16:
          result = data.Values[0].AggregatedValue.FirstStdBelow;
          break;
        case 17:
          result = data.Values[0].AggregatedValue.SecondStdBelow;
          break;
        case 18:
          result = data.Values[0].AggregatedValue.Percentile;
          break;
        default:
          result = null;
          break;
      }
    }

    if (result !== null) {
      result = result * (scale ?? 1);
    }

    return result;
  }

  private getToday() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
  }

  private getADayAgo() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1, 0, 0, 0, 0);
  }
}
