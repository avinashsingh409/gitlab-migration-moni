import { inject, TestBed } from '@angular/core/testing';
import { AuthorizationFrameworkService } from './authorization-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ITreePermissions } from '@atonix/atx-core';
describe('AuthorizationFrameworkService', () => {
  let service: AuthorizationFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AuthorizationFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get permissions', inject(
    [AuthorizationFrameworkService, HttpTestingController],
    (
      service: AuthorizationFrameworkService,
      httpTest: HttpTestingController
    ) => {
      let result;
      const expectedOutput: ITreePermissions = {
        canView: true,
        canAdd: true,
        canEdit: true,
        canDelete: true,
      };

      service.getPermissions().subscribe((data: any) => {
        result = data;
      });

      httpTest
        .expectOne({
          url: 'undefined/api/Authorization/AssetAccessRights',
        })
        .flush({
          AssetRights: {
            CanView: true,
            CanAdd: true,
            CanEdit: true,
            CanDelete: true,
          },
        });

      httpTest.verify();
      expect(result).toEqual(expectedOutput);
    }
  ));
});
