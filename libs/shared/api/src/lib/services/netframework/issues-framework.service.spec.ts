import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { IssuesFrameworkService } from './issues-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('IssuesFrameworkService', () => {
  let service: IssuesFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(IssuesFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
