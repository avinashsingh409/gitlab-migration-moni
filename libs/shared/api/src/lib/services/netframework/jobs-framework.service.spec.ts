import { HttpClientTestingModule } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { JobsFrameworkService } from './jobs-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { INotificationItem } from '../../models/jobs-items';

describe('JobsFrameworkService', () => {
  let service: JobsFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(JobsFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should correctly sort the notification items', inject(
    [JobsFrameworkService],
    (service: JobsFrameworkService) => {
      const items: INotificationItem[] = [
        {
          NotificationID: 1,
          NotificationGroupID: '10',
          NotificationTypeID: 1,
          Message: 'message 1',
          Options: 'options 1',
          Result: 'result 1',
          Viewed: false,
          CreatedDate: new Date(2020, 12, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 1',
          ChangedDate: new Date(2020, 12, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 1',
        },
        {
          NotificationID: 2,
          NotificationGroupID: '11',
          NotificationTypeID: 1,
          Message: 'message 2',
          Options: 'options 2',
          Result: 'result 2',
          Viewed: false,
          CreatedDate: new Date(2020, 11, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 2',
          ChangedDate: new Date(2020, 11, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 2',
        },
        {
          NotificationID: 3,
          NotificationGroupID: '12',
          NotificationTypeID: 1,
          Message: 'message 3',
          Options: 'options 3',
          Result: 'result 3',
          Viewed: false,
          CreatedDate: new Date(2020, 10, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 3',
          ChangedDate: new Date(2020, 10, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 3',
        },
        {
          NotificationID: 4,
          NotificationGroupID: '10',
          NotificationTypeID: 1,
          Message: 'message 4',
          Options: 'options 4',
          Result: 'result 4',
          Viewed: false,
          CreatedDate: new Date(2020, 9, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 4',
          ChangedDate: new Date(2020, 9, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 4',
        },
        {
          NotificationID: 5,
          NotificationGroupID: '11',
          NotificationTypeID: 1,
          Message: 'message 5',
          Options: 'options 5',
          Result: 'result 5',
          Viewed: false,
          CreatedDate: new Date(2020, 8, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 5',
          ChangedDate: new Date(2020, 8, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 5',
        },
        {
          NotificationID: 6,
          NotificationGroupID: '12',
          NotificationTypeID: 1,
          Message: 'message 6',
          Options: 'options 6',
          Result: 'result 6',
          Viewed: false,
          CreatedDate: new Date(2020, 7, 0, 0, 0, 0, 0),
          CreatedBy: 'createdby 6',
          ChangedDate: new Date(2020, 7, 0, 0, 0, 0, 0),
          ChangedBy: 'changedby 6',
        },
      ];

      const sortedItems = (service as any).fixNotificationItems(items);
      expect(sortedItems.length).toEqual(3);
      expect(sortedItems[0].length).toEqual(2);
      expect(sortedItems[0][0].Message).toEqual('message 1');
      expect(sortedItems[2][1].Message).toEqual('message 6');
    }
  ));
});
