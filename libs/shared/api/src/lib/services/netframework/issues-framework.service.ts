import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { take, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AssetIssue } from '../../models/asset-issue';
import { AssetIssueWithOptions } from '../../models/issue-model';
import {
  IssueCauseType,
  IssueClassesAndCategories,
} from '../../models/issue-model';
import { ISubscriber } from '../../models/subscriber';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  AssetIssueImpactScenario,
  BlankScenario,
  CalculationFactors,
  ImpactCalculatorSave,
} from '@atonix/atx-core';
import { ISendIssueResult } from '../../models/send-issue';

@Injectable({
  providedIn: 'root',
})
export class IssuesFrameworkService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}
  getOptions(asset: string | number, field: string) {
    return this.$http
      .get<string[]>(
        `${this.appConfig.baseServicesURL}/api/Issues/FilterValues`,
        {
          params: {
            asset: String(asset),
            field: String(field),
          },
        }
      )
      .pipe(take(1));
  }

  saveAssetIssue(assetIssue: AssetIssue) {
    const params = {
      AssetIssue: assetIssue,
    };
    return this.$http.post<AssetIssue>(
      `${this.appConfig.baseServicesURL}/api/Issues/SaveAssetIssue`,
      params
    );
  }

  deleteAssetIssue(assetIssueID: number): Observable<boolean> {
    const params = {
      assetIssueID: assetIssueID.toString(),
    };
    return this.$http.get<boolean>(
      `${this.appConfig.baseServicesURL}/api/Issues/DeleteAssetIssue`,
      {
        params,
      }
    );
  }

  subscribeUser(assetIssueId: number): Observable<any> {
    const params = { IssueID: assetIssueId.toString() };
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Issues/SubscribeUsers`,
      {
        params,
      }
    );
  }

  unSubscribeUser(assetIssueId: string, user: string[]): Observable<any> {
    return this.$http.post<any>(
      `${this.appConfig.baseServicesURL}/api/Issues/Unsubscribe`,
      {
        IssueID: assetIssueId,
        Subscribers: user,
      }
    );
  }

  saveIssueSummary(id: string, summary: string) {
    const params = {
      Id: id,
      Summary: summary,
    };

    return this.$http.post<AssetIssue>(
      `${this.appConfig.baseServicesURL}/api/Issues/SaveIssueSummary`,
      params
    );
  }

  validateIssueType(candidateIssueTypeDesc: string) {
    const params = { candidateIssueTypeDesc };
    return this.$http.get<boolean>(
      `${this.appConfig.baseServicesURL}/api/Issues/ValidateIssueType`,
      {
        params,
      }
    );
  }

  validateIssueCauseType(candidateIssueCauseDesc: string) {
    const params = { candidateIssueCauseDesc };
    return this.$http.get<boolean>(
      `${this.appConfig.baseServicesURL}/api/Issues/ValidateIssueCauseType`,
      {
        params,
      }
    );
  }

  getIssueTypeCauses(issueTypeID: number) {
    const params = {
      IssueTypeID: issueTypeID.toString(),
    };
    return this.$http.get<IssueCauseType[]>(
      `${this.appConfig.baseServicesURL}/api/Issues/IssueTypeIssueCauses`,
      { params }
    );
  }

  setScorecard(assetIssueID: number, include: boolean) {
    // Must call setScorecard from IssuesRetrieverService to manage caching for Issues Management task center
    return this.$http.get(
      `${this.appConfig.baseServicesURL}/api/Issues/ScorecardInclude`,
      {
        params: {
          assetIssueID: String(assetIssueID),
          include: String(include),
        },
      }
    );
  }

  getImpactScenariosByAssetIssueID(assetIssueID: number) {
    const params = {
      AssetIssueID: assetIssueID.toString(),
    };
    return this.$http.get<AssetIssueImpactScenario[]>(
      `${this.appConfig.baseServicesURL}/api/Issues/GetAssetIssueImpactScenariosByAssetIssueID`,
      {
        params,
      }
    );
  }

  getBlankScenario(assetId: number): Observable<BlankScenario> {
    const params = {
      assetId: assetId.toString(),
    };
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Issues/CreateBlankAssetIssueImpactScenario`,
      {
        params,
      }
    );
  }

  saveImpactCalculator(calculator: ImpactCalculatorSave): Observable<any> {
    return this.$http.post<any>(
      `${this.appConfig.baseServicesURL}/api/v2/Issues/AssetIssueImpactCalculator`,
      calculator
    );
  }

  newAssetIssue(asset: string, issueCategoryType: number) {
    const params = {
      asset: asset,
      issueCategoryType: issueCategoryType.toString(),
    };
    return this.$http.get<AssetIssueWithOptions>(
      `${this.appConfig.baseServicesURL}/api/Issues/NewIssue`,
      {
        params,
      }
    );
  }

  getAssetIssueWithOptions(issueId: string): Observable<AssetIssueWithOptions> {
    const params = { Id: issueId };
    return this.$http.get<AssetIssueWithOptions>(
      `${this.appConfig.baseServicesURL}/api/Issues/AssetIssueWithOptions`,
      {
        params,
      }
    );
  }

  getIssueIdsFromAlias(aliasID: string): Observable<any> {
    const params = { alias: aliasID };
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Issues/AliasToID`,
      {
        params,
      }
    );
  }

  issueCategoryTypes(guid: string) {
    return this.$http.get<IssueClassesAndCategories[]>(
      `${this.appConfig.baseServicesURL}/api/Issues/IssueCategoryTypes?asset=${guid}`
    );
  }

  getOpenIssuesCount(assetId: string) {
    const params = { assetId };
    return this.$http.get<number>(
      `${this.appConfig.baseServicesURL}/api/Issues/GetOpenIssuesCount`,
      { params }
    );
  }

  updateImpactCosts(assetIssueID: string) {
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/v2/Issues/AssetIssueImpactTypeMapTotals?assetIssueID=${assetIssueID}`
    );
  }

  getAssetSubscription(guid: string): Observable<ISubscriber[]> {
    return this.$http.get<ISubscriber[]>(
      `${this.appConfig.baseServicesURL}/api/Issues/SendIssueParameters?Id=${guid}`
    );
  }

  getAssetGroupMembersSubscription(
    groupUserName: string,
    assetGuid: string
  ): Observable<ISubscriber[]> {
    const params = { Id: groupUserName, assetGuid: assetGuid };
    return this.$http.get<ISubscriber[]>(
      `${this.appConfig.baseServicesURL}/api/Issues/GroupMembers`,
      { params: params }
    );
  }

  sendIssue(
    issueID: string,
    message: string,
    recipients: string[],
    issueLabel: string,
    subscribeUsers: boolean
  ): Observable<ISendIssueResult> {
    const params = {
      IssueID: issueID,
      Message: message,
      Recipients: recipients,
      IssueLabel: issueLabel,
      SubscribeUser: subscribeUsers,
    };
    return this.$http.post<ISendIssueResult>(
      `${this.appConfig.baseServicesURL}/api/Issues/SendIssue`,
      params
    );
  }

  getImpactCalculationFactorsByImpactCategoryType(
    assetIssueImpactCategoryID: number
  ): Observable<CalculationFactors[]> {
    const params = {
      AssetIssueImpactCategoryID: assetIssueImpactCategoryID.toString(),
    };
    return this.$http.get<any>(
      `${this.appConfig.baseServicesURL}/api/Issues/GetImpactCalculationFactorsByImpactCategoryType`,
      {
        params,
      }
    );
  }
}
