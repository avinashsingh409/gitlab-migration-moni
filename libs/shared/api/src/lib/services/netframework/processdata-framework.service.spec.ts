import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ProcessDataFrameworkService } from './processdata-framework.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('ProcessdataFrameworkService', () => {
  let service: ProcessDataFrameworkService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(ProcessDataFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
