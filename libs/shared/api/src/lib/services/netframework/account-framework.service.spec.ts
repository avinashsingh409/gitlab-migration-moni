import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { Observable } from 'rxjs';
import { Store, Action } from '@ngrx/store';
import { AccountFrameworkService } from './account-framework.service';

describe('AccountFrameworkService', () => {
  let service: AccountFrameworkService;
  let actions$: Observable<Action>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AccountFrameworkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
