import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IModelConfigDataRetrieval,
  IModelConfigSummary,
  IModelConfigTagListSummary,
  IModelConfigTagModels,
} from '../../models/model-config';
import { IAtxHttpResponse } from '@atonix/atx-core';
import { mapKeys, startCase, tap } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class ModelConfigCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getModelList(uniqueKey: string): Observable<IModelConfigSummary[]> {
    const params = {
      uniqueKey,
    };
    return this.$http
      .get<IModelConfigDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/modelconfig/modelconfiglist`,
        { params: params }
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results[0]?.map((m: IModelConfigSummary) => {
            /*
            Model Types
            1 - APR
            2 - Rolling Average
            3 - Fixed Limit
            4 - Frozen Data
            5 - External
            6 - Moving Average
             */
            const model = { ...m };
            if (
              m?.ModelTypeID === 3 ||
              m?.ModelTypeID === 4 ||
              m?.ModelTypeID === 5
            ) {
              model.UpperBoundTolerance = null;
              model.LowerBoundTolerance = null;
            }

            if (m?.ModelTypeID === 6 || m?.ModelTypeID === 2) {
              if (!model.InterQuartileRange) {
                model.UpperBoundTolerance = '?';
                model.LowerBoundTolerance = '?';
              }
            }

            return model;
          });
        })
      );
  }

  getDependentTagList(
    uniqueKey: string
  ): Observable<IModelConfigTagListSummary[]> {
    const params = {
      uniqueKey,
    };
    return this.$http
      .get<IModelConfigDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/modelconfig/modelconfigtagslist`,
        { params: params }
      )
      .pipe(
        map((n) => {
          const results: IModelConfigTagListSummary[] = [];

          n.Results[0]?.map((r: IModelConfigTagListSummary) => {
            const newResult: IModelConfigTagListSummary = {
              TagID: r.TagID,
              TagGlobalID: r.TagGlobalID,
              TagName: r.TagName,
              TagDesc: r.TagDesc,
              TagGroup: r.TagGroup,
              TagMapID: r.TagMapID,
              VariableID: r.VariableID,
              VariableDesc: r.VariableDesc,
              AssetDesc: r.AssetDesc,
              AssetAbbrev: r.AssetAbbrev,
              AssetID: r.AssetID,
              AssetGUID: r.AssetGUID,
              AssetCriticality: r.AssetCriticality,
              EngUnits: r.EngUnits,
              ModelsList: null,
              AssetTypeDesc: r.AssetTypeDesc,
              UnitAbbrev: r.UnitAbbrev,
              AssetPath: r.AssetPath,
            };

            if (r.ModelsList?.length > 0) {
              const modelList: IModelConfigTagModels[] = [];
              r.ModelsList.forEach((model: IModelConfigTagModels) => {
                const newModel: IModelConfigTagModels = {
                  ModelID: model.ModelID,
                  ModelExtID: model.ModelExtID,
                  ModelName: model.ModelName,
                  EngUnits: model.EngUnits || '',
                  ModelTypeAbbrev: model.ModelTypeAbbrev,
                  ModelTypeID: model.ModelTypeID,
                  ActionModelMaintenance: model.ActionModelMaintenance || false,
                  IsStandard: model.IsStandard,
                  Active: model.Active,
                  Built: model.Built,
                  LastBuildStatus: model.LastBuildStatus,
                  LastBuildDate: model.LastBuildDate,
                  LastUpdateDate: model.LastUpdateDate,
                  Score: model.Score,
                  AssetGuid: newResult.AssetGUID,
                  OpModeTypeID: model.OpModeTypeID,
                  OpModeTypes: model.OpModeTypes,
                };
                modelList.push(newModel);
              });
              newResult.ModelsList = modelList;
            }
            results.push(newResult);
          });
          return results;
        })
      );
  }

  getInputTagsWithModels(
    uniqueKey: string
  ): Observable<IModelConfigTagListSummary[]> {
    return this.$http
      .post<IModelConfigTagListSummary[]>(
        `${this.appConfig.baseMicroServicesURL}/api/models/inputtagswithmodels`,
        {
          UserIdentityName: '',
          UniqueKey: uniqueKey,
        }
      )
      .pipe(
        map((result) => {
          const inputTags: IModelConfigTagListSummary[] = [];
          result.map((inputTag: IModelConfigTagListSummary) => {
            const xInputTag = mapKeys(inputTag, (v, k) =>
              startCase(k).replace(/ /g, '')
            );
            const mappedInputTag = xInputTag as any;
            const newInputTag: IModelConfigTagListSummary = {
              TagID: mappedInputTag?.TagID,
              TagGlobalID: mappedInputTag?.TagGlobalID,
              TagName: mappedInputTag?.TagName,
              TagDesc: mappedInputTag?.TagDesc,
              TagGroup: mappedInputTag?.TagGroup,
              TagMapID: mappedInputTag?.TagMapID,
              VariableID: mappedInputTag?.VariableID,
              VariableDesc: mappedInputTag?.VariableDesc,
              AssetDesc: mappedInputTag?.AssetDesc,
              AssetAbbrev: mappedInputTag?.AssetAbbrev,
              AssetID: mappedInputTag?.AssetID,
              AssetGUID: mappedInputTag?.AssetGUID,
              AssetCriticality: mappedInputTag?.AssetCriticality,
              EngUnits: mappedInputTag?.EngUnits,
              ModelsList: null,
              AssetTypeDesc: mappedInputTag?.AssetTypeDesc,
              UnitAbbrev: mappedInputTag?.UnitAbbrev,
              AssetPath: mappedInputTag?.AssetPath,
            };

            if (mappedInputTag.ModelsList?.length > 0) {
              const modelList: IModelConfigTagModels[] = [];
              mappedInputTag.ModelsList.forEach(
                (modelx: IModelConfigTagModels) => {
                  const model = mapKeys(modelx, (v, k) =>
                    startCase(k).replace(/ /g, '')
                  ) as any;
                  const newModel: IModelConfigTagModels = {
                    ModelID: model?.ModelID,
                    ModelExtID: model?.ModelExtID,
                    ModelName: model?.ModelName,
                    EngUnits: model?.EngUnits || '',
                    ModelTypeAbbrev: model?.ModelTypeAbbrev,
                    ModelTypeID: model?.ModelTypeID,
                    ActionModelMaintenance:
                      model?.ActionModelMaintenance || false,
                    IsStandard: model?.IsStandard,
                    Active: model?.Active,
                    Built: model?.Built,
                    LastBuildStatus: model?.LastBuildStatus,
                    LastBuildDate: model?.LastBuildDate,
                    LastUpdateDate: model?.LastUpdateDate,
                    Score: model?.Score,
                    AssetGuid: newInputTag?.AssetGUID,
                    OpModeTypeID: model?.OpModeTypeID,
                    OpModeTypes: model?.OpModeTypes,
                  };
                  modelList.push(newModel);
                }
              );
              newInputTag.ModelsList = modelList;
            }

            inputTags.push(newInputTag);
          });
          return inputTags;
        })
      );
  }

  multiModelJobProcessing(pdServerID: number, filename: string) {
    const params = {
      PDServerID: pdServerID,
      Filename: filename,
    };

    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/modelconfig/multimodel`,
        params
      )
      .pipe(take(1));
  }

  downloadModelTemplate() {
    return this.$http
      .get<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/modelconfig/downloadmodeltemplate`
      )
      .pipe(take(1));
  }

  patchModelSummary(modelDesc: string, modelGuids: string[]): Observable<any> {
    //slow when a large amount of models are selected (we have to send a PATCH req for each model)
    modelGuids.forEach((modelGuid, index) => {
      if (index === 0) return;
      this.$http
        .patch(`${this.appConfig.baseSiteURL}/api/modelconfig/${modelGuid}`, {
          ModelDescription: modelDesc,
        })
        .subscribe();
    });

    return this.$http.patch(
      `${this.appConfig.baseSiteURL}/api/modelconfig/${modelGuids[0]}`,
      {
        ModelDescription: modelDesc,
      }
    );
  }
}
