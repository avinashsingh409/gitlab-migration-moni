import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IssuesCoreService } from './issues-core.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('IssuesCoreService', () => {
  let service: IssuesCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(IssuesCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
