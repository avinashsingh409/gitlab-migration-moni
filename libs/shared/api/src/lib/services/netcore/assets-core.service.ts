/* eslint-disable rxjs/no-implicit-any-catch */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, catchError, take } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AnalyticsResponse } from '../../models/events-analytics';
import {
  IAssetClassTypeResult,
  IAssetCreate,
  IAssetCreateUpdateResult,
  IAssetCreateValidateResult,
  IAssetResult,
  IAssetUpdate,
  IAtxHttpResponse,
  IEvent,
  IEventChange,
  IEventType,
  IUserEventWithDescription,
  IAtxAssetTree,
  IAtxAssetTreeNode,
  isNil,
} from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class AssetsCoreService {
  private eventTypesCache: IEventType[];

  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  load(start: string, end: string) {
    return this.$http
      .post<AnalyticsResponse>(
        `${this.appConfig.baseSiteURL}/api/v1/assets/B209CD00-B8C6-41DB-88AE-957CA58972D6/analytics?type=reliability&maxLevels=100`,
        { start, end }
      )
      .pipe(
        map((n) => n.Results),
        catchError((err) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );
  }

  getProcessData(start: string, end: string) {
    return this.$http
      .post<AnalyticsResponse>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/B209CD00-B8C6-41DB-88AE-957CA58972D6/analytics?type=capacity-factor&maxLevels=100`,
        { start, end }
      )
      .pipe(
        map((n) => n.Results),
        catchError((err) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );
  }

  getProcessDataIntervals(
    assetID: string,
    interval: string,
    start: string,
    end: string
  ) {
    return this.$http
      .post<AnalyticsResponse>(
        // eslint-disable-next-line max-len
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetID}/analytics?type=capacity-factor-intervals&maxLevels=100&interval=${interval}`,
        { start, end }
      )
      .pipe(
        map((n) => n.Results),
        catchError((err) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );
  }

  getReliabilityIntervals(
    assetID: string,
    interval: string,
    start: string,
    end: string
  ) {
    return this.$http
      .post<AnalyticsResponse>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetID}/analytics?type=reliability-intervals&maxLevels=100&interval=${interval}`,
        { start, end }
      )
      .pipe(
        map((n) => n.Results),
        catchError((err) => {
          if (err?.error?.Results?.length > 0) {
            if (err?.error?.Results[0]?.Message) {
              return throwError(err.error.Results[0].Message);
            }
          }
          return throwError('an error occurred');
        })
      );
  }

  getEventsAssetExplorer(
    asset: string,
    includeDescendants: boolean,
    full: boolean = false,
    skipEvents: number = 0,
    takeEvents: number = 1000
  ): Observable<IEvent[]> {
    if (asset) {
      return this.$http
        .get<IAtxHttpResponse<IEvent>>(
          `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events`,
          {
            params: {
              full: String(full),
              skip: String(skipEvents),
              take: String(takeEvents),
              includeDescendants: includeDescendants ? 'true' : 'false',
              includeDeleted: 'true',
            },
          }
        )
        .pipe(
          map((n) => {
            return n?.Results;
          })
        );
    } else {
      console.log('No Asset Selected');
      return of([]);
    }
  }

  getEvents(
    asset: string,
    includeDescendants: boolean,
    start?: string,
    end?: string,
    full: boolean = false,
    skipEvents: number = 0,
    takeEvents: number = 1000
  ): Observable<IEvent[]> {
    if (asset) {
      return this.$http
        .get<IAtxHttpResponse<IEvent>>(
          `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events`,
          {
            params: {
              full: String(full),
              skip: String(skipEvents),
              take: String(takeEvents),
              includeDescendants: includeDescendants ? 'true' : 'false',
              includeDeleted: 'false',
              start,
              end,
            },
          }
        )
        .pipe(
          map((n) => {
            return n?.Results;
          })
        );
    } else {
      console.log('No Asset Selected');
      return of([]);
    }
  }

  getEventTypes() {
    if (this.eventTypesCache) {
      return of(this.eventTypesCache);
    }
    return this.$http
      .get<IAtxHttpResponse<IEventType>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/event-types`
      )
      .pipe(
        map((n) => {
          this.eventTypesCache = n.Results;
          return this.eventTypesCache;
        })
      );
  }

  getEventHistory(
    asset: string,
    eventID: string,
    includeDeleted: boolean = true
  ): Observable<IEvent[]> {
    return this.$http
      .get<IAtxHttpResponse<IEvent>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events/${eventID}/history`,
        {
          params: {
            includeDeleted: String(includeDeleted),
          },
        }
      )
      .pipe(
        map((n) => {
          return n?.Results;
        })
      );
  }

  validateEvent(asset: string, event: IUserEventWithDescription) {
    if (event.EventID !== -1) {
      return this.$http
        .put<IAtxHttpResponse<IEventChange>>(
          `${
            this.appConfig.baseMicroServicesURL
          }/api/v1/assets/${asset}/events/${event.EventID.toString()}`,
          event,
          {
            params: {
              validate: 'true',
            },
          }
        )
        .pipe(
          map((n) => {
            return {
              changedEventCount: n.Count,
              validationMessage:
                n.Count > 0
                  ? `This will modify ${n.Count} ${
                      n.Count > 1 ? 'events' : 'event'
                    }`
                  : `Event is valid.`,
              changedEvents: n.Results,
            };
          })
        );
    } else {
      return this.$http
        .post<IAtxHttpResponse<IEventChange>>(
          `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events`,
          event,
          {
            params: {
              validate: 'true',
            },
          }
        )
        .pipe(
          map((n) => {
            return {
              changedEventCount: n.Count,
              validationMessage:
                n.Count > 0
                  ? `This will modify ${n.Count} ${
                      n.Count > 1 ? 'events' : 'event'
                    }`
                  : `Event is valid.`,
              changedEvents: n.Results,
            };
          })
        );
    }
  }

  updateEvent(asset: string, event: IUserEventWithDescription) {
    return this.$http
      .put<IAtxHttpResponse<IEvent>>(
        `${
          this.appConfig.baseMicroServicesURL
        }/api/v1/assets/${asset}/events/${event.EventID.toString()}`,
        event
      )
      .pipe(map((n) => n.Results));
  }

  createEvent(asset: string, event: IUserEventWithDescription) {
    return this.$http
      .post<IAtxHttpResponse<IEvent>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events`,
        event,
        {
          params: {
            validate: 'false',
          },
        }
      )
      .pipe(map((n) => n.Results));
  }

  deleteEvent(asset: string, eventID: number) {
    return this.$http
      .delete<IAtxHttpResponse<IEvent>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${asset}/events/${eventID}`
      )
      .pipe(map((n) => n?.Results));
  }

  getAssets(
    assetId: string,
    includeDescendants?: boolean
  ): Observable<IAssetResult[]> {
    const params: {
      assetId: string;
      includeDescendants?: boolean;
    } = {
      assetId,
    };
    if (includeDescendants) {
      params.includeDescendants = includeDescendants;
    }
    return this.$http
      .get<IAtxHttpResponse<IAssetResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAssetResult>) => {
          return r.Results;
        })
      );
  }

  getAssetClassTypes(
    parentAssetClassTypeKey: string,
    searchString?: string
  ): Observable<IAssetClassTypeResult[]> {
    const params: {
      parentAssetClassTypeKey: string;
      searchString?: string;
    } = {
      parentAssetClassTypeKey,
    };

    if (searchString) {
      params.searchString = searchString;
    }

    return this.$http
      .get<IAtxHttpResponse<IAssetClassTypeResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assetclasstypes`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAssetClassTypeResult>) => {
          return r.Results;
        })
      );
  }

  createAssets(assetId: string, assetsToCreate: IAssetCreate[]) {
    return this.$http
      .post<IAtxHttpResponse<IAssetCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}`,
        assetsToCreate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAssetCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  updateAssets(
    assetId: string,
    assetsToUpdate: IAssetUpdate[]
  ): Observable<IAssetCreateUpdateResult[]> {
    return this.$http
      .put<IAtxHttpResponse<IAssetCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}`,
        assetsToUpdate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAssetCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  deleteAsset(assetIds: string[]): Observable<string> {
    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/delete`,
        assetIds
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<string>) => {
          return r.Results[0];
        })
      );
  }

  validateCreateAssets(
    assetId: string,
    assets: IAssetCreate[]
  ): Observable<IAssetCreateValidateResult[]> {
    return this.$http
      .post<IAtxHttpResponse<IAssetCreateValidateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/validateCreate`,
        assets
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAssetCreateValidateResult>) => {
          return r.Results;
        })
      );
  }

  getAttributes(
    assetId: string,
    includeDescendants?: boolean
  ): Observable<any[]> {
    const params: {
      assetId: string;
      includeDescendants?: boolean;
    } = {
      assetId,
    };
    if (includeDescendants) {
      params.includeDescendants = includeDescendants;
    }

    return this.$http
      .get<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/attributes`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<any>) => {
          return r.Results;
        })
      );
  }

  upsertAttributes(
    assetId: string,
    attributesToUpdate: any[]
  ): Observable<any[]> {
    return this.$http
      .put<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/attributes`,
        attributesToUpdate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<any>) => {
          return r.Results;
        })
      );
  }

  deleteAttributes(
    assetId: string,
    attributePks: number[]
  ): Observable<string> {
    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/attributes/delete`,
        attributePks
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<string>) => {
          return r.Results[0];
        })
      );
  }

  validateAttributes(
    assetId: string,
    attributesToValidate: any[]
  ): Observable<any[]> {
    return this.$http
      .post<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/attributes/validate`,
        attributesToValidate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<any>) => {
          return r.Results;
        })
      );
  }

  getAttachments(assetId: string): Observable<any[]> {
    const params: {
      assetId: string;
    } = {
      assetId,
    };
    return this.$http
      .get<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/attachments`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<any>) => {
          return r.Results;
        })
      );
  }

  upsertAttachment(assetId: string, attachmentToUpdate: any): Observable<any> {
    return this.$http
      .put<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/attachments`,
        attachmentToUpdate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<any>) => {
          return r.Results[0];
        })
      );
  }

  deleteAttachments(
    assetId: string,
    attachmentIds: string[]
  ): Observable<string> {
    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetId}/attachments/delete`,
        attachmentIds
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<string>) => {
          return r.Results[0];
        })
      );
  }

  loadNodeFromKey(
    treeID?: string,
    nodeID?: string,
    appContextID?: string
  ): Observable<{
    m_Item1: IAtxAssetTree[];
    m_Item2: IAtxAssetTreeNode[];
    m_Item3: string;
  }> {
    const params: any = {};
    if (!isNil(nodeID)) {
      params.assetId = nodeID;
    }
    if (!isNil(treeID)) {
      params.treeId = treeID;
    }
    return this.$http.get<{
      m_Item1: IAtxAssetTree[];
      m_Item2: IAtxAssetTreeNode[];
      m_Item3: string;
    }>(`${this.appConfig.baseMicroServicesURL}/api/v1/assets/trees/load`, {
      params,
      headers: new HttpHeaders({
        'Content-Type': 'application/json; chartset=utf-8',
      }),
    });
  }

  getChildren(node: IAtxAssetTreeNode): Observable<IAtxAssetTreeNode[]> {
    return this.$http.post<IAtxAssetTreeNode[]>(
      `${this.appConfig.baseMicroServicesURL}/api/v1/assets/nodes/children`,
      node
    );
  }

  getTreesForTreeConfig(): Observable<IAtxAssetTree[]> {
    return this.$http
      .get<IAtxHttpResponse<IAtxAssetTree>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/trees`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAtxAssetTree>) => {
          return r.Results;
        })
      );
  }

  getNodesForTreeConfig(treeId: string): Observable<IAtxAssetTreeNode[]> {
    const params: {
      treeId: string;
    } = {
      treeId: treeId,
    };
    return this.$http
      .get<IAtxHttpResponse<IAtxAssetTreeNode>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/nodes`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAtxAssetTreeNode>) => {
          return r.Results;
        })
      );
  }

  upsertCustomTree(tree: any): Observable<IAtxAssetTree> {
    return this.$http
      .put<IAtxHttpResponse<IAtxAssetTree>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/trees`,
        tree
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAtxAssetTree>) => {
          return r.Results[0];
        })
      );
  }

  deleteCustomTree(treeId: string): Observable<string> {
    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/trees/${treeId}/delete`,
        null
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<string>) => {
          return r.Results[0];
        })
      );
  }

  upsertCustomTreeNode(upsertNode: any): Observable<IAtxAssetTreeNode> {
    return this.$http
      .put<IAtxHttpResponse<IAtxAssetTreeNode>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/nodes`,
        upsertNode
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IAtxAssetTreeNode>) => {
          return r.Results[0];
        })
      );
  }

  deleteCustomTreeNode(nodeId: string): Observable<boolean> {
    return this.$http
      .delete<IAtxHttpResponse<boolean>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/assets/nodes/${nodeId}`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<boolean>) => {
          return r.Results[0];
        })
      );
  }
}
