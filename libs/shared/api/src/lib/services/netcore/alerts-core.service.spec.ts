import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AlertsCoreService } from './alerts-core.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('AlertsCoreService', () => {
  let service: AlertsCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AlertsCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
