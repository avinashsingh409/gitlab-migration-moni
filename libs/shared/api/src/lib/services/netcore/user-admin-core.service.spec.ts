import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserAdminCoreService } from './user-admin-core.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('UserAdminCoreService', () => {
  let service: UserAdminCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(UserAdminCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
