import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataExplorerCoreService } from './data-explorer-core.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('DataExplorerCoreService', () => {
  let service: DataExplorerCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(DataExplorerCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
