import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ProcessDataCoreService } from './process-data-core.service';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('ProcessDataCoreService', () => {
  let service: ProcessDataCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(ProcessDataCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
