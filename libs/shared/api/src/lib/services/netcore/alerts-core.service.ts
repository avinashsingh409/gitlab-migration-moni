import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { take, map } from 'rxjs/operators';
import {
  IAlertsModelSummaryGroup,
  IAlertsModelSummary,
  INDModelSummaryGroup,
  INDModelSummary,
} from '../../models/monitoring-interfaces';
import {
  INDModelActionItemAnalysis,
  INDModelActionItemAnalysisResult,
} from '../../models/charting-data';
import { IGridParameters } from '../../models/grid-parameters';
import { IGridExportRetrieval } from '../../models/issue-data';
import { IAlertDataRetrieval } from '../../models/alert-data-retrieval';
import { Observable, of } from 'rxjs';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  EAlertTypes,
  EAnomalyTypes,
  EModelTypes,
  EMovingAverageUnitOfTime,
  EUnitOfTime,
  IAverageAnomalyAlertProperties,
  IForecastModelProperties,
  IModelConfigData,
  IRawModelConfigData,
} from '../../models/model-config-data';
import moment, { unitOfTime } from 'moment';
import {
  IAtxHttpResponse,
  isNil,
  isNilOrEmptyString,
  transformValueToMilliseconds,
} from '@atonix/atx-core';
import { ICorrelationData } from '../../models/correlation-data';

@Injectable({
  providedIn: 'root',
})
export class AlertsCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAlerts(params: IServerSideGetRowsRequest): Observable<{
    NumModelSummary: number;
    NumRowSummary: number;
    Summary: INDModelSummary[] | INDModelSummaryGroup[];
  }> {
    if (!params?.filterModel?.AssetID) {
      return of({
        Summary: [],
        NumModelSummary: 0,
        NumRowSummary: 0,
      });
    }

    if (
      params.rowGroupCols &&
      params.rowGroupCols.length > 0 &&
      (!params.groupKeys ||
        params.groupKeys.length < params.rowGroupCols.length)
    ) {
      return this.$http
        .post<IAlertDataRetrieval>(
          `${this.appConfig.baseMicroServicesURL}/api/alerts/SummaryGroups`,
          params
        )
        .pipe(
          take(1),
          map((n) => {
            // New interface for Summary grouped items
            (n.Results[0].Summary as IAlertsModelSummaryGroup[]).map(
              (issue) => {
                issue.AddedToAlerts = issue.AddedToAlerts
                  ? new Date(issue.AddedToAlerts)
                  : null;
                issue.LastNoteDate = issue.LastNoteDate
                  ? new Date(issue.LastNoteDate)
                  : null;
                // TODO: Remove this type assertion when .NET Framework APIs have been udpated to use the IModelSummary C# interface.
                // Then, replace INDModelSummaryGroup with IAlertsModelSummaryGroup throughout atx-apps.
                issue as unknown as INDModelSummaryGroup;
              }
            );
            return n.Results[0];
          })
        );
    } else {
      return this.$http
        .post<IAlertDataRetrieval>(
          `${this.appConfig.baseMicroServicesURL}/api/alerts/Summary`,
          params
        )
        .pipe(
          take(1),
          map((n) => {
            // New interface for Summary items
            (n.Results[0].Summary as IAlertsModelSummary[]).map((issue) => {
              issue.AddedToAlerts = issue.AddedToAlerts
                ? new Date(issue.AddedToAlerts)
                : null;
              issue.LastNoteDate = issue.LastNoteDate
                ? new Date(issue.LastNoteDate)
                : null;
              // TODO: Remove this type assertion when .NET Framework APIs have been udpated to use the IModelSummary C# interface.
              // Then, replace INDModelSummary with IAlertsModelSummary throughout atx-apps.
              issue as unknown as INDModelSummary;
            });
            return n.Results[0];
          })
        );
    }
  }

  downloadModelsService(
    gridParams: IGridParameters
  ): Observable<IGridExportRetrieval> {
    return this.$http
      .post<IGridExportRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/alerts/SummaryExport`,
        JSON.stringify(gridParams),
        { headers: { 'Content-Type': 'application/json' } }
      )
      .pipe(take(1));
  }

  public getModelActionForAlertCharting(
    modelID: string
  ): Observable<INDModelActionItemAnalysis[]> {
    const params: { modelID: string } = {
      modelID,
    };
    return this.$http
      .get<INDModelActionItemAnalysisResult>(
        `${this.appConfig.baseMicroServicesURL}/api/alerts/ActionItemsAnalysis`,
        { params }
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results;
        })
      );
  }

  getAlertsForIssues(params: IServerSideGetRowsRequest) {
    return this.$http
      .post<IAlertDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/alerts/Summary`,
        params
      )
      .pipe(
        take(1),
        map((n) => {
          (n.Results[0].Summary as IAlertsModelSummary[]).map((issue) => {
            issue.AddedToAlerts = issue.AddedToAlerts
              ? new Date(issue.AddedToAlerts)
              : null;
            issue.LastNoteDate = issue.LastNoteDate
              ? new Date(issue.LastNoteDate)
              : null;
          });
          return n.Results[0];
        })
      );
  }

  getAlertsCountByAsset(uniqueKey: string) {
    return this.$http
      .get<any>(
        `${this.appConfig.baseMicroServicesURL}/api/alerts/alertscountbyasset?uniqueKey=${uniqueKey}`
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results[0];
        })
      );
  }

  getModelTypeVersion(modelExtID: string) {
    return this.$http
      .get<any>(
        `${this.appConfig.baseMicroServicesURL}/api/alertengine/mlmodellatesttype?modelId=${modelExtID}`
      )
      .pipe(
        take(1),
        map((n) => {
          if (
            isNil(n?.Results) ||
            n?.Results?.length < 1 ||
            isNil(n?.Results[0]?.Updates)
          ) {
            return '';
          }
          return n?.Results[0]?.Updates.join(' ');
        })
      );
  }

  getModelTransform(modelObject: IRawModelConfigData) {
    return this.$http.post<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/transform`,
      {
        model: modelObject,
      }
    );
  }

  getModelConfig(modelExtIds: string[]) {
    return this.$http
      .post<IRawModelConfigData[]>(
        `${this.appConfig.baseMicroServicesURL}/api/models/config`,
        {
          ModelExtIDs: modelExtIds,
        }
      )
      .pipe(
        take(1),
        map((rawModels) => {
          return rawModels.map((rm) => {
            return this.createModelConfigData(rm);
          });
        })
      );
  }

  getCorrelationTrend(modelExtId: string, startDate: Date, endDate: Date) {
    return this.$http
      .get<IAtxHttpResponse<ICorrelationData>>(
        `${this.appConfig.baseMicroServicesURL}/api/alertengine/correlationtrend`,
        {
          params: {
            modelId: modelExtId,
            start: startDate.toISOString(),
            end: endDate.toISOString(),
          },
        }
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results;
        })
      );
  }

  getModelTemplates(tagMapId: number, opModeTypeId: number) {
    return this.$http
      .post<IRawModelConfigData[]>(
        `${this.appConfig.baseMicroServicesURL}/api/models/configbytemplate`,
        {
          UserIdentityName: '',
          AssetVariableTagMapID: tagMapId,
          OpModeTypeID: opModeTypeId,
        }
      )
      .pipe(
        map((rawModels) => {
          return rawModels.map((rm) => {
            return this.createModelConfigData(rm);
          });
        })
      );
  }

  getBlankModel(
    modelName: string,
    tagMapId: number,
    opModeTypeId: number,
    modelTypeId
  ) {
    return this.$http
      .post<IRawModelConfigData>(
        `${this.appConfig.baseMicroServicesURL}/api/models/blankconfig`,
        {
          UserIdentityName: '',
          ModelName: modelName,
          AssetVariableTagMapID: tagMapId,
          OpModeTypeID: opModeTypeId,
          ModelTypeID: modelTypeId,
        }
      )
      .pipe(map((rm) => this.createModelConfigData(rm)));
  }

  getRecommendedInputs(tagMapID: number, modelType: string) {
    return this.$http
      .post<number[]>(
        `${this.appConfig.baseMicroServicesURL}/api/models/recommendedinputs`,
        {
          AssetVariableTagMapID: tagMapID,
          ModelType: modelType,
        }
      )
      .pipe(
        take(1),
        map((recommended) => {
          return recommended;
        })
      );
  }

  private createModelConfigData(rm: IRawModelConfigData) {
    if (rm.dependent?.tagDesc && rm.dependent?.tagName) {
      rm.dependent.modeledTagString = `${rm.dependent.tagDesc} (${rm.dependent.tagName})`;
    }
    const modelConfigData: IModelConfigData = {
      active: rm.active,
      buildProperties: rm.buildProperties,
      dependent: rm.dependent,
      independents: rm.independents,
      locked: rm.locked,
      modelExtID: rm.modelExtID,
      legacy: rm.legacy,
      name: rm.name,
      path: rm.path,
      properties: rm.properties,
      training: rm.training,
      savedTrainingRange: null,
      anomalyAreaAlert: null,
      averageAnomalyAlert: null,
      anomalyFrequencyAlert: null,
      anomalyOscillationAlert: null,
      highHighAlert: null,
      lowLowAlert: null,
      frozenDataCheckAlert: null,
      relativeBoundsAnomaly: null,
      upperFixedLimitAnomaly: null,
      lowerFixedLimitAnomaly: null,
      criticalityAnomaly: null,
      isDirty: false,
      relativeEarliestInterceptDate: '',
      projectedTrainingRange: '',
      fixedEarliestInterceptDate: '',
      fixedEarliestInterceptTime: '',
      fixedEarliestInterceptChanged: false,
      standardModel: rm.standardModel,
      liveStatus: '',
    };

    rm.alerts.map((alert) => {
      switch (alert.type) {
        case EAlertTypes.AnomalyArea:
          modelConfigData.anomalyAreaAlert = alert;
          break;
        case EAlertTypes.AverageAnomaly:
          alert.enabled = alert.enabled ? 'Yes' : 'No';
          (
            alert.properties as IAverageAnomalyAlertProperties
          ).useMeanAbsoluteError = this.setMeanAbsoluteError(
            (alert.properties as IAverageAnomalyAlertProperties)
              .useMeanAbsoluteError
          );
          modelConfigData.averageAnomalyAlert = alert;
          break;
        case EAlertTypes.AnomalyFrequency:
          modelConfigData.anomalyFrequencyAlert = alert;
          break;
        case EAlertTypes.AnomalyOscillation:
          modelConfigData.anomalyOscillationAlert = alert;
          break;
        case EAlertTypes.HighHigh:
          modelConfigData.highHighAlert = alert;
          break;
        case EAlertTypes.LowLow:
          modelConfigData.lowLowAlert = alert;
          break;
        case EAlertTypes.FrozenDataCheck:
          modelConfigData.frozenDataCheckAlert = alert;
          break;
      }
    });

    rm.anomalies.map((anomaly) => {
      switch (anomaly.type) {
        case EAnomalyTypes.RelativeBounds:
          modelConfigData.relativeBoundsAnomaly = anomaly;
          break;
        case EAnomalyTypes.UpperFixedLimit:
          modelConfigData.upperFixedLimitAnomaly = anomaly;
          break;
        case EAnomalyTypes.LowerFixedLimit:
          modelConfigData.lowerFixedLimitAnomaly = anomaly;
          break;
        case EAnomalyTypes.Criticality:
          anomaly.enabled = anomaly.enabled ? 'Yes' : 'No';
          modelConfigData.criticalityAnomaly = anomaly;
          break;
      }
    });

    // Saved Training Range
    if (rm.buildProperties.status?.builtSinceTime && rm.training) {
      const trainingEndDate = rm.training.lag
        ? moment(rm.buildProperties.status?.builtSinceTime).toDate().getTime() -
          transformValueToMilliseconds(
            rm.training.lag,
            rm.training.lagUnitOfTime
          )
        : moment(rm.buildProperties.status?.builtSinceTime).toDate().getTime();
      const trainingStartDate = rm.training.duration
        ? trainingEndDate -
          transformValueToMilliseconds(
            rm.training.duration,
            rm.training.durationUnitOfTime
          )
        : trainingEndDate;
      modelConfigData.savedTrainingRange = `${moment(trainingStartDate).format(
        'MM/DD/YYYY'
      )} - ${moment(trainingEndDate).format('MM/DD/YYYY')}`;
    }

    modelConfigData.relativeEarliestInterceptDate = '';
    if (modelConfigData.properties.modelType.type === EModelTypes.Forecast) {
      const props = modelConfigData.properties.modelType
        .properties as IForecastModelProperties;

      modelConfigData.fixedEarliestInterceptDate = moment(
        props.horizonDate
      ).format('MM/DD/YYYY');
      modelConfigData.fixedEarliestInterceptTime = moment(
        props.horizonDate
      ).format('HH:mm');

      if (isNilOrEmptyString(props.horizonPeriod)) {
        modelConfigData.relativeEarliestInterceptDate = '';
      } else {
        modelConfigData.relativeEarliestInterceptDate = moment(
          new Date(),
          'MM/DD/YYYY HH:mm'
        )
          .add(
            props.horizonPeriod,
            props.horizonPeriodUnitOfTime.toLowerCase() as unitOfTime.DurationConstructor
          )
          .format('MM/DD/YYYY');
      }
    }

    if (
      rm?.training &&
      !isNil(rm.training?.duration) &&
      rm.training.durationUnitOfTime
    ) {
      const trainingEndDate =
        moment(moment()).toDate().getTime() -
        transformValueToMilliseconds(
          rm.training.lag || 0,
          rm.training.lagUnitOfTime || EUnitOfTime.Seconds
        );
      const trainingStartDate = rm.training.duration
        ? trainingEndDate -
          transformValueToMilliseconds(
            rm.training.duration || 0,
            rm.training.durationUnitOfTime || EUnitOfTime.Seconds
          )
        : trainingEndDate;
      modelConfigData.projectedTrainingRange = `${moment(
        trainingStartDate
      ).format('MM/DD/YYYY')} - ${moment(trainingEndDate).format(
        'MM/DD/YYYY'
      )}`;
    }

    rm.training.smoothingFactorActualUnits = EMovingAverageUnitOfTime.Minutes;
    rm.training.smoothingFactorExpectedUnits = EMovingAverageUnitOfTime.Minutes;
    return modelConfigData;
  }

  private setMeanAbsoluteError(
    value?: boolean | 'Yes' | 'No'
  ): null | 'Yes' | 'No' {
    if (isNil(value)) {
      return null;
    }
    return value === true ? 'Yes' : 'No';
  }
}
