import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import { take, map } from 'rxjs/operators';
import { IGridParameters } from '../../models/grid-parameters';
import {
  IIssueSummary,
  IIssueSummaryGroup,
  IGridExportRetrieval,
  IIssueDataRetrieval,
} from '../../models/issue-data';
import {
  IAssetIssueDonutDataRetrieval,
  IAtxHttpResponse,
  isInvalidDate,
  isValidDate,
} from '@atonix/atx-core';
import { Observable } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IUpdateIssueSubscribersModel,
  IIssueSubscriber,
  IIssueSubscribersRetrieval,
} from '../../models/update-subscribers-model';
import { ExternalAsset } from '../../models/external-asset';
import { IWorkRequest } from '../../models/work-request-model';

export interface IWorkRequestCreate {
  title: string;
  description: string;
  assetIssueId: string;
  externalSystemAssetId: number;
  priority: string;
}

@Injectable({
  providedIn: 'root',
})
export class IssuesCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAssociatedExternalAsset(assetId: string) {
    return this.$http.get<IAtxHttpResponse<ExternalAsset>>(
      `${this.appConfig.baseSiteURL}/api/v1/assets/${assetId}/externalasset`
    );
  }

  getExternalAssets(assetGuid: string) {
    return this.$http.get<IAtxHttpResponse<ExternalAsset>>(
      `${this.appConfig.baseMicroServicesURL}/api/v1/assets/${assetGuid}/externalassets`
    );
  }

  createWorkRequest(params: IWorkRequestCreate) {
    return this.$http
      .post<IAtxHttpResponse<any>>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/${params.assetIssueId}/workrequests`,
        {
          Title: params.title,
          Description: params.description,
          ExternalSystemAssetId: params.externalSystemAssetId,
          Priority: params.priority,
        }
      )
      .pipe(
        take(1),
        map((res) => this.extractWorkRequest(res))
      );
  }

  getWorkRequest(assetIssueId: string, refresh?: boolean) {
    let params = {};
    if (refresh) params = { refresh };
    return this.$http
      .get<IAtxHttpResponse<IWorkRequest>>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/${assetIssueId}/workrequests`,
        { params }
      )
      .pipe(
        take(1),
        map((res) => this.extractWorkRequest(res))
      );
  }

  private extractWorkRequest(res: IAtxHttpResponse<IWorkRequest>) {
    let workRequest;
    if (res.Results.length > 0) {
      workRequest = res.Results[0];
    }

    return {
      ...res,
      workRequest: {
        title: workRequest.Title,
        description: workRequest.Description,
        assetIssueId: workRequest.AssetIssueID,
        createDate: workRequest.CreateDate,
        workReferenceId: workRequest.WorkReferenceID,
        workRequestId: workRequest.WorkRequestID,
        location: workRequest.Location,
        equipment: workRequest.Equipment,
        priority: workRequest.Priority,
        lastQueriedAt: isValidDate(workRequest.LastQueriedAt)
          ? new Date(workRequest.LastQueriedAt)
          : null,
        lastUpdatedAt: isValidDate(workRequest.LastUpdatedAt)
          ? new Date(workRequest.LastUpdatedAt)
          : null,
        status: workRequest.Status,
      } as IWorkRequest,
    };
  }

  getIssues(params: IServerSideGetRowsRequest) {
    if (
      params.rowGroupCols &&
      params.rowGroupCols.length > 0 &&
      (!params.groupKeys ||
        params.groupKeys.length < params.rowGroupCols.length)
    ) {
      return this.$http
        .post<IIssueDataRetrieval>(
          `${this.appConfig.baseMicroServicesURL}/api/Issues/IssuesGridGroups`,
          params
        )
        .pipe(
          take(1),
          map((n) => {
            (n.Results[0].Issues as IIssueSummaryGroup[]).map((issue) => {
              issue.OpenDate = issue.OpenDate ? new Date(issue.OpenDate) : null;
              issue.CloseDate = issue.CloseDate
                ? new Date(issue.CloseDate)
                : null;
              issue.ChangeDate = issue.ChangeDate
                ? new Date(issue.ChangeDate)
                : null;
              issue.CreateDate = issue.CreateDate
                ? new Date(issue.CreateDate)
                : null;
              issue.AssetIssueGUID = issue.AssetIssueID.toString();
              issue.AssetIssueID = undefined;
            });

            return n;
          })
        );
    } else {
      return this.$http
        .post<IIssueDataRetrieval>(
          `${this.appConfig.baseMicroServicesURL}/api/Issues/IssuesGrid`,
          params
        )
        .pipe(
          take(1),
          map((n: IIssueDataRetrieval) => {
            (n.Results[0].Issues as IIssueSummary[]).map((issue) => {
              issue.OpenDate = issue.OpenDate ? new Date(issue.OpenDate) : null;
              issue.CloseDate = issue.CloseDate
                ? new Date(issue.CloseDate)
                : null;
              issue.ChangeDate = issue.ChangeDate
                ? new Date(issue.ChangeDate)
                : null;
              issue.CreateDate = issue.CreateDate
                ? new Date(issue.CreateDate)
                : null;
              issue.AssetIssueGUID = issue.AssetIssueID.toString();
            });

            return n;
          })
        );
    }
  }

  getIssuesForAlerts(params: IServerSideGetRowsRequest) {
    return this.$http
      .post<IIssueDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/IssuesGrid`,
        params
      )
      .pipe(
        take(1),
        map((n: IIssueDataRetrieval) => {
          (n.Results[0].Issues as IIssueSummary[]).map((issue) => {
            issue.CreateDate = issue.CreateDate
              ? new Date(issue.CreateDate)
              : null;
          });
          return n;
        })
      );
  }

  // The "pivotCols" and "pivotMode" fields aren't used.
  ListViewServiceDownload(
    gridParams: IGridParameters
  ): Observable<IGridExportRetrieval> {
    return this.$http
      .post<IGridExportRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/IssuesGridExport`,
        JSON.stringify(gridParams),
        { headers: { 'Content-Type': 'application/json' } }
      )
      .pipe(take(1));
  }

  getDonutData(params: IServerSideGetRowsRequest) {
    return this.$http
      .post<IAssetIssueDonutDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/IssuesDonutData`,
        params
      )
      .pipe(take(1));
  }

  updateIssueSubscribers(
    issueSubscribersModel: IUpdateIssueSubscribersModel
  ): Observable<IIssueSubscribersRetrieval> {
    return this.$http.post<IIssueSubscribersRetrieval>(
      `${this.appConfig.baseMicroServicesURL}/api/Issues/manageissuefollowers`,
      issueSubscribersModel
    );
  }

  getIssueFollowers(assetIssueId: number): Observable<IIssueSubscriber[]> {
    const params = {
      assetIssueId: assetIssueId.toString(),
    };
    return this.$http
      .get<IIssueSubscribersRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/Issues/getissuefollowers`,
        { params: params }
      )
      .pipe(
        map((response) => {
          const issueSubscribers = response.Results[0] as IIssueSubscriber[];
          if (issueSubscribers) {
            issueSubscribers.forEach((sub) => {
              sub.FriendlyName = `${sub.FirstName} ${sub.LastName}`;
              sub.IsSubscribed = true;
            });
          }
          return issueSubscribers;
        })
      );
  }
}
