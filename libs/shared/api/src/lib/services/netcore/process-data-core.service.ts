import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  IAtxHttpResponse,
  ITagMapBare,
  IPDTagMap,
  isNil,
  ITagMapCreateUpdateResult,
  ITagMapAuditResult,
  IServerResult,
  ITagResult,
  ITagCreateUpdateResult,
} from '@atonix/atx-core';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class ProcessDataCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getTagMaps(
    assetId: string,
    serverId: string,
    includeDescendants: boolean = false,
    includeInactive: boolean = true,
    skip: number = 0,
    toTake: number = 10000
  ): Observable<IPDTagMap[]> {
    const params: {
      assetId: string;
      serverId: string;
      includeDescendants?: boolean;
      includeInactive?: boolean;
      skip?: number;
      take?: number;
    } = {
      assetId,
      serverId,
    };
    if (!isNil(includeDescendants)) {
      params.includeDescendants = includeDescendants;
    }
    if (!isNil(includeInactive)) {
      params.includeInactive = includeInactive;
    }
    if (!isNil(skip)) {
      params.skip = skip;
    }
    if (!isNil(toTake)) {
      params.take = toTake;
    }
    return this.$http
      .get<IAtxHttpResponse<IPDTagMap>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IPDTagMap>) => {
          return r.Results;
        })
      );
  }

  saveTagMaps(
    tagMapsToCreate: ITagMapBare[]
  ): Observable<ITagMapCreateUpdateResult[]> {
    return this.$http
      .post<IAtxHttpResponse<ITagMapCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps`,
        tagMapsToCreate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagMapCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  updateTagMaps(
    tagMapsToUpdate: ITagMapBare[]
  ): Observable<ITagMapCreateUpdateResult[]> {
    return this.$http
      .put<IAtxHttpResponse<ITagMapCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps`,
        tagMapsToUpdate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagMapCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  auditTagMaps(tagMapIds: number[]): Observable<ITagMapAuditResult[]> {
    return this.$http
      .post<IAtxHttpResponse<ITagMapAuditResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps/audit`,
        tagMapIds
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagMapAuditResult>) => {
          return r.Results;
        })
      );
  }

  deleteTagMaps(tagMapIds: number[]): Observable<boolean> {
    return this.$http
      .post<IAtxHttpResponse<boolean>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps/delete`,
        tagMapIds
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<boolean>) => {
          return r.Results[0];
        })
      );
  }

  getServers(assetId: string): Observable<IServerResult[]> {
    const params: {
      assetId: string;
    } = {
      assetId,
    };
    return this.$http
      .get<IAtxHttpResponse<IServerResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/servers`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<IServerResult>) => {
          return r.Results;
        })
      );
  }

  getTagsForServer(serverId: string): Observable<ITagResult[]> {
    return this.$http
      .get<IAtxHttpResponse<ITagResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/servers/${serverId}/tags`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagResult>) => {
          return r.Results;
        })
      );
  }

  validateCreateTagMaps(
    tagMaps: ITagMapBare[]
  ): Observable<ITagMapCreateUpdateResult[]> {
    return this.$http
      .post<IAtxHttpResponse<ITagMapCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/tagmaps/validateCreate`,
        tagMaps
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagMapCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  createTags(
    serverId: string,
    tagsToCreate: ITagResult[]
  ): Observable<ITagCreateUpdateResult[]> {
    return this.$http
      .post<IAtxHttpResponse<ITagCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/servers/${serverId}/tags`,
        tagsToCreate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }

  updateTags(
    serverId: string,
    tagsToUpdate: ITagResult[]
  ): Observable<ITagCreateUpdateResult[]> {
    return this.$http
      .put<IAtxHttpResponse<ITagCreateUpdateResult>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/processdata/servers/${serverId}/tags`,
        tagsToUpdate
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<ITagCreateUpdateResult>) => {
          return r.Results;
        })
      );
  }
}
