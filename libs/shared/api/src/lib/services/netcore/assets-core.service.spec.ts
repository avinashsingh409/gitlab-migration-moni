import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AssetsCoreService } from './assets-core.service';

describe('AssetsCoreService', () => {
  let service: AssetsCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: APP_CONFIG, useValue: AppConfig }],
    });
    service = TestBed.inject(AssetsCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
