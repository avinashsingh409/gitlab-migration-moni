import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { take, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IServerSideGetRowsRequest } from '@ag-grid-enterprise/all-modules';
import { ITagListDataRetrieval } from '../../models/tag-list-data-retrieval';
import {
  IAssetVariableTypeTagMap,
  IAtxHttpResponse,
  IPDTag,
} from '@atonix/atx-core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
@Injectable({
  providedIn: 'root',
})
export class DataExplorerCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getTagList(
    params: IServerSideGetRowsRequest
  ): Observable<{ TagMaps: IAssetVariableTypeTagMap[]; NumTagMaps: number }> {
    return this.$http
      .post<ITagListDataRetrieval>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/gridprocessdata/TagGrid`,
        params
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results[0];
        })
      );
  }

  getTags(params: number[]): Observable<IPDTag[]> {
    return this.$http
      .post<IAtxHttpResponse<IPDTag[]>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/gridprocessdata/Tags`,
        params
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results[0];
        })
      );
  }

  assetParamsForTagGrid(dependentTagIDs: number[]): Observable<any> {
    return this.$http
      .post<IAtxHttpResponse<IPDTag[]>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/gridprocessdata/AssetParamsForTagGrid`,
        dependentTagIDs
      )
      .pipe(
        take(1),
        map((n) => {
          return n.Results[0];
        })
      );
  }
}
