import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Injectable({
  providedIn: 'root',
})
export class PrivateApiAlertsCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getOpMode(opModeDefinitionID: number) {
    const params: { opModeDefinitionID: number } = {
      opModeDefinitionID,
    };
    return this.$http.get<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/opmode`,
      { params }
    );
  }

  saveOpMode(opModeDefinition: any) {
    return this.$http.post<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/opmode`,
      opModeDefinition
    );
  }

  validateOpMode(opModeDefinition: any) {
    return this.$http.post<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/opmode/validate`,
      opModeDefinition
    );
  }

  getOpModeSummaries(uniqueKey: string) {
    const params: { uniqueKey: string } = {
      uniqueKey,
    };
    return this.$http.get<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/opmode/summaries`,
      { params }
    );
  }

  deleteOpMode(opModeDefinitionID: number) {
    return this.$http.delete<any>(
      `${this.appConfig.baseMicroServicesURL}/api/models/opmode?opModeDefinitionID=${opModeDefinitionID}`
    );
  }
}
