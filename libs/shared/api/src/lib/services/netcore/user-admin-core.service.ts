import { HttpClient, HttpResponse } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import {
  Role,
  RoleDetails,
  User,
  Resource,
  AssetPermissionDetails,
  UserDetails,
  Group,
  CustomerNode,
  Customer,
  CustomerDetails,
  TopLevelAsset,
  TopLevelAssetNode,
  GroupDetails,
} from '../../models/user-admin';
import { AssetNode } from '../../models/user-admin';
import { IAtxHttpResponse, isNil } from '@atonix/atx-core';

@Injectable({
  providedIn: 'root',
})
export class UserAdminCoreService {
  constructor(
    private $http: HttpClient,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAssets(
    roleId: number,
    customerId: string,
    assetId?: string
  ): Observable<AssetNode[]> {
    const params: { customerId: string; assetId?: string } = {
      customerId,
    };
    if (assetId) {
      params.assetId = assetId;
    }
    return this.$http
      .get<IAtxHttpResponse<AssetPermissionDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles/${roleId}/assetpermission`,
        { params }
      )
      .pipe(
        take(1),
        map((a: IAtxHttpResponse<AssetPermissionDetails>) =>
          a.Results.map((details: AssetPermissionDetails) => ({
            Asset: details.AssetName,
            AssetId: details.AssetId,
            Allow: details.ExplicitAllow,
            Deny: details.ExplicitDeny,
            ImplicitAllow: details.ImplicitAllow,
            ImplicitDeny: details.ImplicitDeny,
            AscendAllow: details.AscendAllow,
            AscendDeny: details.AscendDeny,
            Children: details.HasChildren ? [] : null,
            IsLoading: false,
            Level: 0,
            IsExpanded: false,
          }))
        )
      );
  }

  getCustomers(requestedCustomerId?: string): Observable<CustomerNode[]> {
    const params: {
      requestedCustomerId?: string;
    } = {};
    if (requestedCustomerId) {
      params.requestedCustomerId = requestedCustomerId;
    }
    return this.$http
      .get<IAtxHttpResponse<Customer>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers`,
        { params }
      )
      .pipe(
        take(1),
        map((a: IAtxHttpResponse<Customer>) =>
          a.Results.map((customer: Customer) => ({
            Id: customer.Id,
            Name: customer.Name,
            HasChildren: customer.HasChildren,
            ParentCustomerId: customer.ParentCustomerId,
            Level: 0,
            IsLoading: false,
            IsSelected: false,
            IsExpanded: false,
            Children: customer.HasChildren ? [] : null,
          }))
        )
      );
  }

  getRoleCustomers(
    requestedCustomerId: string,
    inclBranch?: boolean
  ): Observable<Customer[]> {
    const params: {
      requestedCustomerId: string;
      inclBranch?: boolean;
    } = { requestedCustomerId };
    if (inclBranch) {
      params.inclBranch = inclBranch;
    }
    return this.$http
      .get<IAtxHttpResponse<Customer>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers`,
        { params }
      )
      .pipe(
        take(1),
        map((a: IAtxHttpResponse<Customer>) => {
          return a.Results;
        })
      );
  }

  createCustomer(
    customerName: string,
    parentCustomerId: string
  ): Observable<CustomerNode> {
    const newCustomer: CustomerDetails = {
      Id: '',
      Name: customerName,
      ParentCustomerId: parentCustomerId,
      EmailDomain: [],
      TopLevelAssetIds: [],
      TopLevelAssets: [],
      HasChildren: false,
    };

    return this.$http
      .post<IAtxHttpResponse<CustomerDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers`,
        newCustomer
      )
      .pipe(
        take(1),
        map((a: IAtxHttpResponse<CustomerDetails>) => {
          const customer: CustomerDetails = a.Results[0];
          return {
            Id: customer.Id,
            Name: customer.Name,
            HasChildren: customer.HasChildren,
            ParentCustomerId: customer.ParentCustomerId,
            Level: 0,
            IsLoading: false,
            IsSelected: false,
            IsExpanded: false,
            Children: customer.HasChildren ? [] : null,
          };
        })
      );
  }

  getCustomerDetails(customerId: string): Observable<CustomerDetails> {
    return this.$http
      .get<IAtxHttpResponse<CustomerDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers/${customerId}`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<CustomerDetails>) => {
          const customer: CustomerDetails = r.Results[0];
          customer.Id = customerId;
          return customer;
        })
      );
  }

  editCustomer(customer: CustomerDetails): Observable<CustomerDetails> {
    return this.$http
      .put<IAtxHttpResponse<CustomerDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers/${customer.Id}`,
        customer
      )
      .pipe(
        take(1),
        map((r) => {
          const editedCustomer: CustomerDetails = r.Results[0];
          editedCustomer.Id = customer.Id;
          return editedCustomer;
        })
      );
  }

  getCustomerEditPermission(customerId: string): Observable<boolean> {
    return this.$http
      .get<IAtxHttpResponse<boolean>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers/${customerId}/customereditpermission`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<boolean>) => {
          return r.Results[0];
        })
      );
  }

  getTopLevelAssets(assetId?: string): Observable<TopLevelAssetNode[]> {
    const params: { assetId?: string } = {};
    if (assetId) {
      params.assetId = assetId;
    }
    return this.$http
      .get<IAtxHttpResponse<TopLevelAsset>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/customers/toplevelassets`,
        { params }
      )
      .pipe(
        take(1),
        map((n: IAtxHttpResponse<TopLevelAsset>) => {
          return n.Results.map(
            (tla: TopLevelAsset) =>
              ({
                ...tla,
                Children: tla.HasChildren ? [] : null,
                IsLoading: false,
                IsSelected: false,
                IsExpanded: false,
                Level: 0,
              } as TopLevelAssetNode)
          );
        })
      );
  }

  getRoles(customerId: string): Observable<Role[]> {
    const params: { customerId: string } = {
      customerId,
    };
    return this.$http
      .get<IAtxHttpResponse<Role>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles`,
        { params }
      )
      .pipe(
        take(1),
        map((n: IAtxHttpResponse<Role>) => {
          return n.Results;
        })
      );
  }

  getRole(customerId: string, roleId: number): Observable<Role> {
    const params: { customerId: string } = {
      customerId,
    };
    return this.$http
      .get<IAtxHttpResponse<Role>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles/${roleId}`,
        { params }
      )
      .pipe(
        take(1),
        map((n: IAtxHttpResponse<Role>) => {
          return n.Results[0];
        })
      );
  }

  getUsers(
    customerId: string,
    includeDisabled?: boolean,
    includeGroups?: boolean,
    includeServiceAccounts?: boolean,
    includeAncestorUsers?: boolean
  ): Observable<User[]> {
    const params: {
      customerId: string;
      includeDisabled?: boolean;
      includeGroups?: boolean;
      includeServiceAccounts?: boolean;
      includeAncestorUsers?: boolean;
    } = {
      customerId,
    };
    if (!isNil(includeDisabled)) {
      params.includeDisabled = includeDisabled;
    }
    if (!isNil(includeGroups)) {
      params.includeGroups = includeGroups;
    }
    if (!isNil(includeServiceAccounts)) {
      params.includeServiceAccounts = includeServiceAccounts;
    }
    if (!isNil(includeAncestorUsers)) {
      params.includeAncestorUsers = includeAncestorUsers;
    }
    return this.$http
      .get<IAtxHttpResponse<User>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/users`,
        { params }
      )
      .pipe(
        take(1),
        map((u: IAtxHttpResponse<User>) => {
          return u.Results;
        })
      );
  }

  getResources(): Observable<Resource[]> {
    return this.$http
      .get<IAtxHttpResponse<Resource>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/resources`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<Resource>) => {
          return r.Results;
        })
      );
  }

  getGroups(
    customerId: string,
    includeDescendants: boolean
  ): Observable<Group[]> {
    const params: { customerId: string; includeDescendants: boolean } = {
      customerId,
      includeDescendants,
    };
    return this.$http
      .get<IAtxHttpResponse<Group>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/groups`,
        { params }
      )
      .pipe(
        take(1),
        map((u: IAtxHttpResponse<Group>) => {
          return u.Results;
        })
      );
  }

  createGroup(groupName: string, customerId: string): Observable<GroupDetails> {
    const newGroup: GroupDetails = {
      Id: '',
      Name: groupName,
      CustomerName: '',
      Active: true,
      Users: [],
      UserIds: [],
      Roles: [],
      RoleIds: [],
      CustomerId: customerId,
    };
    return this.$http
      .post<IAtxHttpResponse<GroupDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/groups`,
        newGroup
      )
      .pipe(
        take(1),
        map((r) => {
          return r.Results[0];
        })
      );
  }

  getGroupDetails(groupId: string): Observable<GroupDetails> {
    return this.$http
      .get<IAtxHttpResponse<GroupDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/groups/${groupId}`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<GroupDetails>) => {
          const group: GroupDetails = r.Results[0];
          group.Id = groupId;
          return group;
        })
      );
  }

  editGroup(group: GroupDetails): Observable<GroupDetails> {
    return this.$http
      .put<IAtxHttpResponse<GroupDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/groups/${group.Id}`,
        group
      )
      .pipe(
        take(1),
        map((r) => {
          const editedGroup: GroupDetails = r.Results[0];
          editedGroup.Id = group.Id;
          return editedGroup;
        })
      );
  }

  getRoleDetails(roleId: number, customerId: string): Observable<RoleDetails> {
    const params: { customerId: string } = {
      customerId,
    };
    return this.$http
      .get<IAtxHttpResponse<RoleDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles/${roleId}`,
        { params }
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<RoleDetails>) => {
          const role: RoleDetails = r.Results[0];
          role.Id = roleId;
          return role;
        })
      );
  }

  createRole(
    roleName: string,
    parentRoleId: number,
    customerId: string
  ): Observable<RoleDetails> {
    const newRole: RoleDetails = {
      Id: 0,
      Name: roleName,
      ParentId: parentRoleId,
      ResourcePermissions: [],
      AssetPermissions: [],
      Users: [],
      UserIds: [],
      CustomerId: customerId,
      RoleType: '',
      Ownership: '',
      Shares: [],
      SharedCustomerIds: [],
    };
    return this.$http
      .post<IAtxHttpResponse<RoleDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles`,
        newRole
      )
      .pipe(
        take(1),
        map((r) => {
          return r.Results[0];
        })
      );
  }

  editRole(role: RoleDetails, customerId: string): Observable<RoleDetails> {
    const params: { customerId: string } = {
      customerId,
    };
    return this.$http
      .put<IAtxHttpResponse<RoleDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/roles/${role.Id}`,
        role,
        { params }
      )
      .pipe(
        take(1),
        map((r) => {
          const editedRole: RoleDetails = r.Results[0];
          editedRole.Id = role.Id;
          return editedRole;
        })
      );
  }

  getAssetChildren(
    assetId: string,
    customerId: string,
    roleId: number
  ): Observable<AssetNode[]> {
    return this.getAssets(roleId, customerId, assetId);
  }

  getUserDetails(userId: string): Observable<UserDetails> {
    return this.$http
      .get<IAtxHttpResponse<UserDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/users/${userId}`
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<UserDetails>) => {
          const user: UserDetails = r.Results[0];
          user.Id = userId;
          return user;
        })
      );
  }

  createUser(
    firstName: string,
    lastName: string,
    email: string,
    serviceAccount: boolean,
    active: boolean,
    customerId: string
  ): Observable<UserDetails> {
    const newUser: UserDetails = {
      Id: '',
      FirstName: firstName,
      LastName: lastName,
      Email: email,
      Active: active,
      RoleIds: [],
      Roles: [],
      CustomerId: customerId,
      CustomerName: '',
      FederatedAccount: false,
      ServiceAccount: serviceAccount,
      Groups: [],
      GroupIds: [],
      IsGroupSub: false,
    };
    return this.$http
      .post<IAtxHttpResponse<UserDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/users`,
        newUser
      )
      .pipe(
        take(1),
        map((r) => {
          return r.Results[0];
        })
      );
  }

  editUser(user: UserDetails): Observable<UserDetails> {
    return this.$http
      .put<IAtxHttpResponse<UserDetails>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/users/${user.Id}`,
        user
      )
      .pipe(
        take(1),
        map((r) => {
          const editedUser: UserDetails = r.Results[0];
          editedUser.Id = user.Id;
          return editedUser;
        })
      );
  }

  initiatePasswordReset(userId: string): Observable<string> {
    return this.$http
      .post<IAtxHttpResponse<string>>(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/users/${userId}/resetpassword`,
        null
      )
      .pipe(
        take(1),
        map((r: IAtxHttpResponse<string>) => {
          return r.Results[0];
        })
      );
  }

  downloadUserList(customerId: string): Observable<HttpResponse<any>> {
    return this.$http
      .get(
        `${this.appConfig.baseMicroServicesURL}/api/v1/accesscontrol/downloaduserlist/${customerId}`,
        { observe: 'response', responseType: 'blob' }
      )
      .pipe(
        take(1),
        map((r: HttpResponse<any>) => r)
      );
  }
}
