import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { map, switchMap } from 'rxjs/operators';
import { SaveChangesModalComponent } from '../../component/save-changes-modal/save-changes-modal.component';

export interface ComponentCanDeactivate {
  canDeactivate: () => Observable<boolean>;
}

@Injectable()
export class PendingChangesGuard
  implements CanDeactivate<ComponentCanDeactivate>
{
  constructor(private dialog: MatDialog) {}

  canDeactivate(
    component: ComponentCanDeactivate
  ): boolean | Observable<boolean> {
    if (component.canDeactivate()) {
      return component.canDeactivate().pipe(
        switchMap((deactivate) => {
          if (deactivate) {
            return of(true);
          }
          const dialogConfig = new MatDialogConfig();
          const dialogRef = this.dialog.open(
            SaveChangesModalComponent,
            dialogConfig
          );
          return dialogRef.afterClosed().pipe(map((result) => result === true));
        })
      );
    }
    return true;
  }
}
