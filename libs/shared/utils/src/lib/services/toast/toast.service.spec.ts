import { TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

import { ToastService } from './toast.service';
import { Subject } from 'rxjs';
import { createMock } from '@testing-library/angular/jest-utils';

describe('ToastService', () => {
  let service: ToastService;
  let snackBar: MatSnackBar;
  beforeEach(() => {
    snackBar = createMock(MatSnackBar);
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MatSnackBar,
          useValue: snackBar,
        },
      ],
    });
    service = TestBed.inject(ToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open snackbar', () => {
    let result: boolean;

    const dismissed = new Subject<void>();
    const snackbarRef = {
      afterDismissed: () => {
        return dismissed;
      },
    };
    snackBar.openFromComponent = jest.fn().mockReturnValue(snackbarRef as any);

    service.isDismissed().subscribe((n) => {
      result = n;
    });

    service.openSnackBar('Some Message', 'warning', 100);

    dismissed.next();

    expect(snackBar.openFromComponent).toHaveBeenCalled();
    expect(result).toEqual(true);
  });
});
