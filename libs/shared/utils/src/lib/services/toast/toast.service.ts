import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent } from '../../component/toast/snackbar.component';
import { Subject, Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  subject = new Subject<boolean>();

  constructor(public snackBar: MatSnackBar) {}
  public openSnackBar(
    toastMessage: string,
    toastType: string,
    toastDuration = 10000,
    toastArray?: string[]
  ) {
    const snackBarRef = this.snackBar.openFromComponent(SnackbarComponent, {
      data: { message: toastMessage, type: toastType },
      verticalPosition: 'bottom',
      horizontalPosition: 'right',
      duration: toastDuration,
      panelClass: toastArray ? toastArray : [toastType],
    });

    snackBarRef
      .afterDismissed()
      .pipe(take(1))
      .subscribe(() => {
        this.subject.next(true);
      });
  }

  public isDismissed(): Observable<boolean> {
    return this.subject.asObservable();
  }

  public dismiss() {
    if (this.snackBar && this.snackBar._openedSnackBarRef) {
      this.snackBar._openedSnackBarRef.dismiss();
    }
  }
}
