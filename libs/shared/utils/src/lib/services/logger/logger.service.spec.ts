import { TestBed } from '@angular/core/testing';
import { LoggerService, ILoggerServiceWindow } from './logger.service';
import { createMock } from '@testing-library/angular/jest-utils';

describe('LoggerService', () => {
  let myService: LoggerService;

  beforeEach(() => TestBed.configureTestingModule({}));
  beforeEach(() => {
    myService = TestBed.inject(LoggerService);
  });

  it('should be created', () => {
    expect(myService).toBeTruthy();
  });

  it('should clear the user', () => {
    const w: ILoggerServiceWindow = {
      localStorage: createMock(Storage),
    };

    myService.setWindow(w);
    myService.clearUser();
    expect(w.localStorage.removeItem).toHaveBeenCalledWith(
      'AppInsightsUserInfo'
    );
  });

  it('should get the user if null', () => {
    const u = myService.getUser();
    expect(u).toBeTruthy();
    expect(u.userId).toEqual('');
    expect(u.customerId).toEqual('');
  });
});
