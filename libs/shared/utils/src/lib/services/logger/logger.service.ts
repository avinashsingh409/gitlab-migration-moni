import { Injectable } from '@angular/core';
import NewRelicBrowser from 'new-relic-browser';

export enum LogEventTypes {
  Login = 'login',
  AssetClick = 'AssetClick',
  ListLoad = 'ListLoad',
  TabClicked = 'TabClicked',
  ChartInteraction = 'ChartInteraction',
  GridInteraction = 'GridInteraction',
  GridFilter = 'GridFilter',
  SavedFilter = 'SavedFilter',
  Feature = 'Feature',
}

export interface ILoggerServiceWindow {
  localStorage: Storage;
  newrelic?: typeof NewRelicBrowser;
}

@Injectable({
  providedIn: 'root',
})
export class LoggerService {
  // The key in local storage to look for the user information.
  private userKey = 'AppInsightsUserInfo';
  private $window: ILoggerServiceWindow;

  constructor() {
    this.setWindow(window as any);
    const user = this.getUser();
    if (user) {
      this.logUser(user.userId, user.customerId, 'new-page-launch');
    }
  }

  public setWindow(newWindowObject?: ILoggerServiceWindow) {
    this.$window = newWindowObject;
  }

  // Grab the user ID and account ID out of local storage.  This should ensure
  // that something in the correct form is returned even if the user is not present.
  public getUser() {
    let result: { userId: string; customerId: string } = null;
    if (
      this.$window &&
      this.$window.localStorage &&
      this.$window.localStorage[this.userKey]
    ) {
      result = JSON.parse(this.$window.localStorage.getItem(this.userKey));
    }
    if (!result || !result.userId || !result.customerId) {
      result = {
        userId: '',
        customerId: '',
      };
    }
    return result;
  }

  // Save the user information in local storage.  This should be called when the
  // user signs into the tool.  To delete the user use the clear user command.
  public setUser(userId: string, customerId: string) {
    userId = (userId || '').replace(/[,;=| ]+/g, '_');
    customerId = (customerId || '').replace(/[,;=| ]+/g, '_');
    if (this.$window && this.$window.localStorage) {
      this.$window.localStorage.setItem(
        this.userKey,
        JSON.stringify({ userId, customerId })
      );
    }
    this.logUser(userId, customerId, 'login');
  }

  // Delete the user out of local storage.
  public clearUser() {
    if (this.$window && this.$window.localStorage) {
      this.$window.localStorage.removeItem(this.userKey);
    }
  }

  public tabClicked(tabName: string, app: string) {
    const properties = { tabName, app };
    this.trackEvent(LogEventTypes.TabClicked, properties);
  }

  public chartInteraction(whichChart: string, app: string) {
    const properties = { whichChart: whichChart, app: app };
    this.trackEvent(LogEventTypes.TabClicked, properties);
  }

  public gridAction(
    gridAction: 'draggroup' | 'menugroup' | 'sort' | 'filter',
    field: string,
    app: string
  ) {
    const properties = { gridAction: gridAction, field: field, app: app };
    this.trackEvent(LogEventTypes.GridInteraction, properties);
  }

  public gridFilter(filterSource: 'inline' | 'column' | 'flyout', app: string) {
    this.trackEvent(LogEventTypes.GridFilter, {
      filterSource: filterSource,
      app: app,
      action: 'track',
    });
  }

  public savedFilter(
    name: string,
    app: string,
    action: 'load' | 'save' | 'delete'
  ) {
    this.trackEvent(LogEventTypes.SavedFilter, {
      name: name,
      app: app,
      action: action,
    });
  }

  public feature(name: string, app: string) {
    this.trackEvent(LogEventTypes.Feature, {
      name: name,
      app: app,
      action: 'track',
    });
  }

  public trackEvent(
    event: LogEventTypes,
    properties?: { [name: string]: string }
  ) {
    const newrelic = this.$window?.newrelic;
    if (!newrelic) {
      return;
    }
    newrelic.addPageAction(event, properties);
  }

  public trackTaskCenterEvent(event: string, properties?: string) {
    const newrelic = this.$window?.newrelic;
    if (!newrelic) {
      return;
    }
    newrelic.setCustomAttribute('event', event);
    newrelic.setCustomAttribute('properties', properties);
    newrelic.addPageAction(event, {});
  }

  public trackAppInsightsEvent(
    event: string,
    properties?: { [name: string]: string }
  ) {
    const newrelic = this.$window?.newrelic;
    if (!newrelic) {
      return;
    }
    newrelic.addPageAction(event, properties);
  }

  private logUser(userId: string, customerId: string, loginMethod: string) {
    const newrelic = this.$window?.newrelic;
    if (!newrelic) {
      return;
    }
    newrelic.setCustomAttribute('userId', userId);
    newrelic.setCustomAttribute('customerId', customerId);
    newrelic.addPageAction(loginMethod, {
      userId: userId,
    });
  }
}
