import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageViewerDialogComponent } from './component/image-viewer-dialog/image-viewer-dialog.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SnackbarComponent } from './component/toast/snackbar.component';
import { DragCellRendererComponent } from './component/drag-cell-renderer/drag-cell-renderer.component';
import { ToastService } from './services/toast/toast.service';
import { WindowService } from './services/window/window.service';
import { LoggerService } from './services/logger/logger.service';
import { SaveChangesModalComponent } from './component/save-changes-modal/save-changes-modal.component';
import { PendingChangesGuard } from './services/component-guard/pending-changes-guard';
import { ConfirmationDialogComponent } from './component/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    ImageViewerDialogComponent,
    SnackbarComponent,
    DragCellRendererComponent,
    SaveChangesModalComponent,
    ConfirmationDialogComponent,
  ],
  imports: [CommonModule, AtxMaterialModule],
})
export class SharedUtilsModule {
  static forRoot(): ModuleWithProviders<SharedUtilsModule> {
    return {
      ngModule: SharedUtilsModule,
      providers: [
        ToastService,
        WindowService,
        LoggerService,
        PendingChangesGuard,
      ],
    };
  }
}
