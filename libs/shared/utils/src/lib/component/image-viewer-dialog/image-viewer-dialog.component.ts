import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'atx-image-viewer',
  templateUrl: './image-viewer-dialog.component.html',
  styleUrls: ['./image-viewer-dialog.component.scss'],
})
export class ImageViewerDialogComponent {
  public imageSources = [];
  public selectedIndex = 0;

  public minDotIndex: number;
  public maxDotIndex: number;
  public numDotsToShow = 5; // default value for dots to show
  public translateOffset = 0;
  @ViewChild('imgZm') zoomView: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<ImageViewerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.imageSources = data.imageSources;
    this.selectedIndex = data.selectedIndex;

    this.initDots();
    this.dotsConfig();
  }

  showNext() {
    if (this.selectedIndex + 1 === this.imageSources.length) {
      this.selectedIndex = 0;
    } else {
      this.selectedIndex++;
    }

    this.selectedIndex = this.selectedIndex % this.imageSources.length;
    this.dotsConfig();
  }

  showPrev() {
    if (this.selectedIndex === 0) {
      this.selectedIndex = this.imageSources.length - 1;
    } else {
      this.selectedIndex--;
    }

    this.selectedIndex = this.selectedIndex % this.imageSources.length;
    this.dotsConfig();
  }

  private initDots() {
    if (this.imageSources) {
      this.translateOffset = 0;
      this.minDotIndex = 0;
      this.maxDotIndex = 0;
      if (this.imageSources.length > this.numDotsToShow) {
        this.maxDotIndex = 3;
      } else if (this.imageSources.length === this.numDotsToShow) {
        this.maxDotIndex = this.numDotsToShow - 1;
      } else {
        this.maxDotIndex = this.imageSources.length - 1;
      }
    }
  }

  private dotsConfig() {
    if (this.selectedIndex > this.maxDotIndex) {
      if (this.selectedIndex >= this.imageSources.length - 2) {
        this.maxDotIndex = this.imageSources.length - 1;
        this.minDotIndex = this.maxDotIndex - 3;
        this.translateOffset -= 20;
      } else {
        this.maxDotIndex = this.selectedIndex;
        this.minDotIndex = this.maxDotIndex - 2;
        this.translateOffset -= 20;
      }
    } else if (this.selectedIndex < this.minDotIndex) {
      if (this.selectedIndex <= 1) {
        this.minDotIndex = 0;
        this.maxDotIndex = 3;
        this.translateOffset = 0;
      } else {
        this.minDotIndex = this.selectedIndex;
        this.maxDotIndex = this.minDotIndex + 2;
        this.translateOffset += 20;
      }
    }
  }

  zoom(direction: string): void {
    if (direction === 'in') {
      this.zoomView.nativeElement.style.height =
        this.zoomView.nativeElement.clientHeight + 60 + 'px';
    } else {
      this.zoomView.nativeElement.style.height =
        this.zoomView.nativeElement.clientHeight - 60 + 'px';
    }
  }
}
