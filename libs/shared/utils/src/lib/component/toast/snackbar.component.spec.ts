import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';
import { SnackbarComponent } from './snackbar.component';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('SnackbarComponent', () => {
  let component: SnackbarComponent;
  let fixture: ComponentFixture<SnackbarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: MAT_SNACK_BAR_DATA, useValue: {} },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: MatSnackBarRef, useValue: {} },
      ],
      declarations: [SnackbarComponent],
    });

    fixture = TestBed.createComponent(SnackbarComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
