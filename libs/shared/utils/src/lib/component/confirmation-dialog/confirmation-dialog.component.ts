/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface IConfirmationDialogData {
  title: string;
  message: string;
  isYesNo: boolean;
  isOk: boolean;
}

@Component({
  selector: 'atx-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: IConfirmationDialogData) {}
}
