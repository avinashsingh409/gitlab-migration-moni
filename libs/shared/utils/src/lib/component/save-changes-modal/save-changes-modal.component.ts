/* eslint-disable @typescript-eslint/no-empty-function */
import { Component } from '@angular/core';

@Component({
  selector: 'atx-save-changes-modal',
  templateUrl: './save-changes-modal.component.html',
  styleUrls: ['./save-changes-modal.component.scss'],
})
export class SaveChangesModalComponent {
  constructor() {}
}
