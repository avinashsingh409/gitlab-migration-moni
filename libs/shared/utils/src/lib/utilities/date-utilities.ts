import { isNil } from '@atonix/atx-core';
import moment from 'moment';

export function combineTime(myDate: Date, time: string): Date {
  const newTime = moment(time, 'HH:mm:ss.SSSS');
  if (newTime.isValid) {
    const newVal = moment(myDate)
      .hour(newTime.hour())
      .minute(newTime.minute())
      .second(newTime.second())
      .millisecond(newTime.millisecond());
    return newVal.toDate();
  }
  return null;
}

export function checkStartAndEndTimes(
  startDate: Date,
  startTime: string,
  endDate: Date,
  endTime: string
) {
  const start: Date = combineTime(startDate, startTime);
  const end: Date = combineTime(endDate, endTime);
  if (start >= end) {
    return true;
  } else {
    return false;
  }
}

export function getADayAgo() {
  const d = new Date();
  return new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1, 0, 0, 0, 0);
}

export function getAMonthAgo() {
  const d = new Date();
  return new Date(d.getFullYear(), d.getMonth() - 1, d.getDate(), 0, 0, 0, 0);
}

export function getAMonthAgoLive() {
  const d = new Date();
  d.setMonth(d.getMonth() - 1);
  return d;
}

export function getToday() {
  const d = new Date();
  return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
}

export function dateFormatter(params) {
  return !isNil(params.value)
    ? moment(params.value).format('MM/DD/YYYY hh:mm A')
    : '';
}
