import { Observable, combineLatest } from 'rxjs';
import { skip } from 'rxjs/operators';

export function waitFor<T>(signal$: Observable<any>) {
  return (source$: Observable<T>) =>
    new Observable<T>((observer) => {
      // combineLatest emits the first value only when
      // both source and signal emitted at least once
      combineLatest([source$, signal$.pipe(skip(1))]).subscribe(([v]) =>
        observer.next(v)
      );
    });
}
