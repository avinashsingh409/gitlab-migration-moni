import { IEvent, IEventType } from '@atonix/atx-core';

export function applyEventTypesToEvents(
  eventTypes: IEventType[],
  events: IEvent[]
): IEvent[] {
  let result = events;
  if (eventTypes && eventTypes.length > 0 && events && events.length > 0) {
    const eventTypeDict: { [key: number]: string } = {};
    for (const t of eventTypes) {
      eventTypeDict[t.EventTypeID] = t.Description;
    }
    result = events.map((n) => {
      return { ...n, eventType: eventTypeDict[n.EventTypeID] ?? '-' };
    });
  }
  return result;
}
