import { IResourceAccessType, ISecurityRights } from '@atonix/atx-core';
import { AccessTypeBit } from '@atonix/shared/api';

export function processResourceAccessType(
  name: string,
  resourceAccessTypes: IResourceAccessType[]
) {
  const resourceAccess = resourceAccessTypes.find(
    (resource) => resource.Name === name
  );
  const value = resourceAccess?.Value ?? 0;
  const access: ISecurityRights = {
    CanView: (value & AccessTypeBit.CanView) === AccessTypeBit.CanView,
    CanEdit: (value & AccessTypeBit.CanEdit) === AccessTypeBit.CanEdit,
    CanAdd: (value & AccessTypeBit.CanAdd) === AccessTypeBit.CanAdd,
    CanDelete: (value & AccessTypeBit.CanDelete) === AccessTypeBit.CanDelete,
    CanAdmin: (value & AccessTypeBit.CanAdmin) === AccessTypeBit.CanAdmin,
  };

  return access;
}
