import moment from 'moment';

export function showModalAnnouncement() {
  if (window.localStorage.getItem('ATX_ANNOUNCEMENT_DATE')) {
    const announcementStorage = window.localStorage.getItem(
      'ATX_ANNOUNCEMENT_DATE'
    );
    const announcementDate = JSON.parse(announcementStorage);
    if (
      moment().toDate().getTime() -
        moment(announcementDate).toDate().getTime() >=
      60 * 60 * 24 * 1000
    ) {
      window.localStorage.setItem(
        'ATX_ANNOUNCEMENT_DATE',
        JSON.stringify(moment().toDate())
      );
      return true;
    } else {
      return false;
    }
  } else {
    window.localStorage.setItem(
      'ATX_ANNOUNCEMENT_DATE',
      JSON.stringify(moment().toDate())
    );
    return true;
  }
}
