/* eslint-disable rxjs/no-implicit-any-catch */
//Custom rxjs operator to avoid effect dying

import { Observable, catchError, startWith } from 'rxjs';

//used in place of catchError
export const catchSwitchMapError =
  (errorAction: (error: any) => any) =>
  <T>(source: Observable<T>) =>
    source.pipe(
      catchError((error, innerSource) =>
        innerSource.pipe(startWith(errorAction(error)))
      )
    );
