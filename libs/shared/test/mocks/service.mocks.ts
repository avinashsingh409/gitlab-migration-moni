import { of } from 'rxjs';

export class FakeAppConfig {
  baseServicesUrl: 'https://dev.atonix.com/Services/';
}

export class FakeAssetTreeDBService {
  addAssetTreesOnIndxDB() {
    return of(null);
  }
  addAssetTreeNodesOnIndxDB() {
    return of(null);
  }

  addDefaultTreeNode() {
    return of(null);
  }
}

export class FakeModelService {
  reloadTree() {
    return of(null);
  }

  loadTree() {
    return of(null);
  }
}
