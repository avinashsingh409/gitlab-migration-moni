import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedUiLibraryComponent } from './component/shared-ui-library/shared-ui-library.component';
import { FormFieldsComponent } from './component/form-fields/form-fields.component';
import { AtxMaterialModule } from '@atonix/atx-material';
import { DropdownComponent } from './component/form-fields/dropdown/dropdown.component';
import { InputComponent } from './component/form-fields/input/input.component';
import { DatePickerComponent } from './component/form-fields/date-picker/date-picker.component';
import { AssetTreeDropdownComponent } from './component/asset-tree-dropdown/asset-tree-dropdown.component';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { NotificationBannerComponent } from './component/notification-banner/notification-banner.component';
import { NotificationDialogComponent } from './component/notification-dialog/notification-dialog.component';
import { NumericCellEditorComponent } from './component/ag-grid-editors/numeric-cell-editor/numeric-cell-editor';
import { DateCellEditorComponent } from './component/ag-grid-editors/date-cell-editor/date-cell-editor';
import { PositiveIntCellEditorComponent } from './component/ag-grid-editors/positive-int-cell-editor/positive-int-cell-editor';
import { NullableRealNumberNumericCellEditorComponent } from './component/ag-grid-editors/nullable-real-number-numeric-cell-editor/nullable-real-number-numeric-cell-editor';
import { NullableNumericCellEditorComponent } from './component/ag-grid-editors/nullable-numeric-cell-editor/nullable-numeric-cell-editor';
import { NullableIntCellEditorComponent } from './component/ag-grid-editors/nullable-int-cell-editor/nullable-int-cell-editor';
import { RealNumberNumericCellEditorComponent } from './component/ag-grid-editors/real-number-numeric-cell-editor/real-number-numeric-cell-editor';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [AtxMaterialModule, CommonModule, AssetTreeModule, FormsModule],

  declarations: [
    SharedUiLibraryComponent,
    FormFieldsComponent,
    DropdownComponent,
    InputComponent,
    DatePickerComponent,
    AssetTreeDropdownComponent,
    NotificationBannerComponent,
    NotificationDialogComponent,
    NumericCellEditorComponent,
    DateCellEditorComponent,
    PositiveIntCellEditorComponent,
    NullableRealNumberNumericCellEditorComponent,
    NullableNumericCellEditorComponent,
    NullableIntCellEditorComponent,
    RealNumberNumericCellEditorComponent,
  ],
  exports: [
    SharedUiLibraryComponent,
    FormFieldsComponent,
    AssetTreeDropdownComponent,
    NotificationBannerComponent,
    NotificationDialogComponent,
  ],
})
export class SharedUiModule {}
