import { ICellEditorAngularComp } from '@ag-grid-community/angular';
import { ICellEditorParams } from '@ag-grid-community/core';
import {
  AfterViewInit,
  Component,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import {
  KEY_BACKSPACE,
  KEY_DELETE,
  KEY_ENTER,
  KEY_F2,
  KEY_TAB,
  MAX_INT_VALUE,
} from '@atonix/atx-core';

@Component({
  selector: 'atx-nullable-real-number-numeric-cell-editor',
  template: `<input
    #input
    class="numeric-input"
    (keydown)="onKeyDown($event)"
    [ngModel]="$any(value)"
    (ngModelChange)="onValueChange($event)"
  />`,
  styleUrls: ['./nullable-real-number-numeric-cell-editor.scss'],
})
export class NullableRealNumberNumericCellEditorComponent
  implements ICellEditorAngularComp, AfterViewInit
{
  public params: any;
  public value!: number | null;
  public max!: number;
  public highlightAllOnFocus = true;
  private cancelBeforeStart = false;
  @ViewChild('input', { read: ViewContainerRef })
  public input!: ViewContainerRef;
  agInit(params: ICellEditorParams): void {
    this.params = params;

    this.max = MAX_INT_VALUE;
    this.setInitialState(params);
    // only start edit if key pressed is a number, not a letter
    this.cancelBeforeStart = !!(
      params.charPress && '1234567890'.indexOf(params.charPress) < 0
    );
  }
  setInitialState(params: ICellEditorParams) {
    let startValue;
    let highlightAllOnFocus = true;
    if (params.eventKey === KEY_BACKSPACE || params.eventKey === KEY_DELETE) {
      // if backspace or delete pressed, we clear the cell
      startValue = '';
    } else if (params.charPress) {
      // if a letter was pressed, we start with the letter
      startValue = params.charPress;
      highlightAllOnFocus = false;
    } else {
      // otherwise we start with the current value
      startValue = params.value;
      if (params.eventKey === KEY_F2) {
        highlightAllOnFocus = false;
      }
    }
    this.value = startValue;
    this.highlightAllOnFocus = highlightAllOnFocus;
  }
  isCancelBeforeStart(): boolean {
    return this.cancelBeforeStart;
  }
  getValue(): any {
    if (this.value) {
      if (isNaN(+this.value)) {
        return null;
      }
      return +this.value;
    }
    return null;
  }

  onValueChange(event: any) {
    this.value = event;
  }

  onKeyDown(event: any): void {
    if (this.isLeftOrRight(event) || this.deleteOrBackspace(event)) {
      event.stopPropagation();
      return;
    }
    if (
      !this.finishedEditingPressed(event) &&
      !this.isKeyPressedNumeric(event)
    ) {
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
  }
  ngAfterViewInit() {
    window.setTimeout(() => {
      this.input.element.nativeElement.focus();
      if (this.highlightAllOnFocus) {
        this.input.element.nativeElement.select();
        this.highlightAllOnFocus = false;
      } else {
        // when we started editing, we want the caret at the end, not the start.
        // this comes into play in two scenarios:
        //   a) when user hits F2
        //   b) when user hits a printable character
        const length = this.input.element.nativeElement.value
          ? this.input.element.nativeElement.value.length
          : 0;
        if (length > 0) {
          this.input.element.nativeElement.setSelectionRange(length, length);
        }
      }
      this.input.element.nativeElement.focus();
    });
  }
  private isCharNumeric(charStr: string): boolean {
    return !!/\d/.test(charStr);
  }
  private isKeyPressedNumeric(event: any): boolean {
    if (event.key === '-') {
      return true;
    }
    if (event.key === '.') {
      return true;
    }
    const charStr = event.key;
    return this.isCharNumeric(charStr);
  }
  private deleteOrBackspace(event: any) {
    return [KEY_DELETE, KEY_BACKSPACE].indexOf(event.key) > -1;
  }
  private isLeftOrRight(event: any) {
    return ['ArrowLeft', 'ArrowRight'].indexOf(event.key) > -1;
  }
  private finishedEditingPressed(event: any) {
    const key = event.key;
    return key === KEY_ENTER || key === KEY_TAB;
  }
}
