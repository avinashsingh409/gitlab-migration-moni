import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'atx-notification-dialog',
  templateUrl: './notification-dialog.component.html',
  styleUrls: ['./notification-dialog.component.scss'],
})
export class NotificationDialogComponent {
  constructor(public dialogRef: MatDialogRef<NotificationDialogComponent>) {
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe((data) => {
      console.log('data returned from mat-dialog-close is ', data);
    });
  }

  redirectToAtonix() {
    const newOrigin = location.origin
      .replace(/apps.asset360.com/gi, 'oi.atonix.com') // Prod stack
      .replace(/.asset360.com/gi, '.atonix.com'); // other stacks
    const pathName = `${newOrigin}${window.location.pathname}${window.location.search}${window.location.hash}`;
    window.open(pathName, '_self').focus();
  }
}
