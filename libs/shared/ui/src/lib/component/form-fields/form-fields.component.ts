import { Component } from '@angular/core';

@Component({
  selector: 'atx-form-fields',
  templateUrl: './form-fields.component.html',
  styleUrls: ['./form-fields.component.scss'],
})
export class FormFieldsComponent {}
