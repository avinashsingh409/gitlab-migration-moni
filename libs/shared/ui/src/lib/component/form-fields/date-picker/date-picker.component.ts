import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'atx-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DatePickerComponent {}
