import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'atx-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InputComponent {}
