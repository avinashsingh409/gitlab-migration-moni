import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { ITreeConfiguration, ITreeStateChange } from '@atonix/atx-asset-tree';
import { Subject } from 'rxjs';

@Component({
  selector: 'atx-asset-tree-dropdown',
  templateUrl: './asset-tree-dropdown.component.html',
  styleUrls: ['./asset-tree-dropdown.component.scss'],
})
export class AssetTreeDropdownComponent implements OnDestroy {
  @Input() assetTreeConfiguration: ITreeConfiguration;
  @Output() assetTreeStateChange = new EventEmitter<ITreeStateChange>();
  @Output() closeDropdown = new EventEmitter();
  public unsubscribe$ = new Subject<void>();
  pristine = true;

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }

  @HostListener('window:click', ['$event'])
  close(event) {
    // This will neglect the event used to open the dropdown
    if (!this.pristine) {
      // This will close the dropdown when clicking outside of it
      this.closeDropdown.emit();
    }
    this.pristine = false;
  }

  public onAssetTreeStateChange(change: ITreeStateChange) {
    this.assetTreeStateChange.emit(change);

    if (change.event === 'SelectAsset') {
      this.closeDropdown.emit();
    }
  }
}
