import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AssetTreeDropdownComponent } from '../asset-tree-dropdown/asset-tree-dropdown.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AssetTreeDialogComponent', () => {
  let component: AssetTreeDropdownComponent;
  let fixture: ComponentFixture<AssetTreeDropdownComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      declarations: [AssetTreeDropdownComponent],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTreeDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
