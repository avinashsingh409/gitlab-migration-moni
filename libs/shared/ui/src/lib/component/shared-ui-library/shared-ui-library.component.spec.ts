import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import { DatePickerComponent } from '../form-fields/date-picker/date-picker.component';
import { DropdownComponent } from '../form-fields/dropdown/dropdown.component';
import { FormFieldsComponent } from '../form-fields/form-fields.component';
import { InputComponent } from '../form-fields/input/input.component';

import { SharedUiLibraryComponent } from './shared-ui-library.component';

describe('SharedUiLibraryComponent', () => {
  let component: SharedUiLibraryComponent;
  let fixture: ComponentFixture<SharedUiLibraryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AtxMaterialModule, NoopAnimationsModule],
      providers: [{ provide: MATERIAL_SANITY_CHECKS, useValue: false }],
      declarations: [
        DropdownComponent,
        FormFieldsComponent,
        InputComponent,
        DatePickerComponent,
        SharedUiLibraryComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedUiLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
