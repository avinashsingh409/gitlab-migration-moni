import { Component } from '@angular/core';

@Component({
  selector: 'atx-shared-ui-library',
  templateUrl: './shared-ui-library.component.html',
  styleUrls: ['./shared-ui-library.component.scss'],
})
export class SharedUiLibraryComponent {}
