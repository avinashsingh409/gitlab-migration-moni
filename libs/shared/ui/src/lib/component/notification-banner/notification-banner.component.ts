import { Component, ViewEncapsulation } from '@angular/core';
import { MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'atx-notification-banner',
  templateUrl: 'notification-banner.component.html',
  styleUrls: ['notification-banner.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NotificationBannerComponent {
  constructor(
    public snackBarRef: MatSnackBarRef<NotificationBannerComponent>
  ) {}

  redirectToAtonix() {
    const newOrigin = location.origin
      .replace(/apps.asset360.com/gi, 'oi.atonix.com') // Prod stack
      .replace(/.asset360.com/gi, '.atonix.com'); // other stacks
    const pathName = `${newOrigin}${window.location.pathname}${window.location.search}${window.location.hash}`;
    window.open(pathName, '_self').focus();
  }
}
