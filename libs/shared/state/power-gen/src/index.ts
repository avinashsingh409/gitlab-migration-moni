export * from './lib/shared-state-power-gen.module';

import * as PowerGenActions from './lib/store/power-gen.actions';
import * as PowerGenFeature from './lib/store/power-gen.reducer';
export { PowerGenActions, PowerGenFeature };
