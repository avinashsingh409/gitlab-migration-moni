import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { PowerGenEffects } from './store/power-gen.effects';
import { StoreModule } from '@ngrx/store';
import { powerGenFeatureKey, powerGenReducer } from './store/power-gen.reducer';
import { SharedApiModule } from '@atonix/shared/api';

@NgModule({
  imports: [
    CommonModule,
    SharedApiModule,
    EffectsModule.forFeature([PowerGenEffects]),
    StoreModule.forFeature(powerGenFeatureKey, powerGenReducer),
  ],
})
export class SharedStatePowerGenModule {}
