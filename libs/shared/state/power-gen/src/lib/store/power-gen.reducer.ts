/* eslint-disable ngrx/on-function-explicit-return-type */
import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import {
  EntityAdapter,
  EntityState,
  createEntityAdapter,
  Update,
} from '@ngrx/entity';
import * as PowerGenActions from './power-gen.actions';
import {
  alterTimeSliderState,
  getDefaultTimeSlider,
  ITimeSliderState,
} from '@atonix/atx-time-slider';

import { getDate, IEventType } from '@atonix/atx-core';
import { applyEventTypesToEvents } from '@atonix/shared/utils';
import moment from 'moment';
import {
  AnalyticsResult,
  IAssetData,
  IChildData,
  IDashboardData,
} from '@atonix/shared/api';

export const powerGenFeatureKey = 'powerGen';
export const powerGenAdapter: EntityAdapter<IDashboardData> =
  createEntityAdapter<IDashboardData>();
export const { selectAll } = powerGenAdapter.getSelectors();

export interface PowerGenState extends EntityState<IDashboardData> {
  SelectedAssetID: number;
  TimeSliderState: ITimeSliderState;
  Error: string;
  Loading: boolean;
  EventTypes: IEventType[];
}

export const initialState: PowerGenState = {
  ids: [],
  entities: {},
  SelectedAssetID: 894422, // ContourGlobal's AssetID
  TimeSliderState: null,
  Error: '',
  Loading: true,
  EventTypes: [],
};

export function getInitialState(defaultValue?: Partial<PowerGenState>) {
  return powerGenAdapter.getInitialState({
    ...initialState,
    ...defaultValue,
  });
}

export const powerGenReducer = createReducer(
  getInitialState(),

  on(PowerGenActions.initializeTimeSlider, (state, action) => {
    return {
      ...state,
      TimeSliderState: getDefaultTimeSlider({
        dateIndicator: 'Range',
        startDate: action.start,
        endDate: action.end,
      }),
    };
  }),
  on(PowerGenActions.timeSliderStateChange, (state, action) => {
    return {
      ...state,
      TimeSliderState: alterTimeSliderState(state.TimeSliderState, action),
    };
  }),
  on(PowerGenActions.updateAnalytics, (state, action) => {
    return {
      ...state,
      Loading: true,
      Error: '',
    };
  }),
  on(PowerGenActions.analyticsLoadedError, (state, action) => {
    return {
      ...state,
      Error: action.error,
      Loading: false,
    };
  }),
  on(
    PowerGenActions.analyticsLoaded,
    (state, { reliabilityResults, processDataResults }) => {
      state = powerGenAdapter.addMany(
        reliabilityResults.map((res) => {
          return processDashboardData(
            reliabilityResults,
            processDataResults,
            res
          );
        }),
        state
      );
      return {
        ...state,
        Error: '',
        Loading: false,
      };
    }
  ),
  on(
    PowerGenActions.updateAnalyticsSuccess,
    (state, { reliabilityResults, processDataResults }) => {
      state = powerGenAdapter.updateMany(
        reliabilityResults.map((res) => {
          const updatedAnalytics = {
            changes: {
              EAF: res?.EAF,
              EFOR: res?.EFOR,
              SOF: res?.SOF,
              MW:
                processDataResults.find((d) => d.AssetID === res?.AssetID)
                  ?.AvgGen || 0,
              CF:
                processDataResults.find((d) => d.AssetID === res?.AssetID)
                  ?.CF || 0,
              MWMaxCapacity:
                processDataResults.find((d) => d.AssetID === res?.AssetID)
                  ?.MaxCapacity || 100,
              EventCount: res?.EventCount,
              Children: updateChildren(
                state.entities[res.AssetID].Children,
                reliabilityResults.filter((d) => d.ParentID === res?.AssetID),
                processDataResults.filter((d) => d.ParentID === res?.AssetID)
              ),
              Data: res,
            },
            id: res.AssetID,
          } as Update<IDashboardData>;
          return updatedAnalytics;
        }),
        state
      );
      return {
        ...state,
        Error: '',
        Loading: false,
      };
    }
  ),
  on(PowerGenActions.selectAsset, (state, { assetID }) => {
    const children = state.entities[assetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      newChild.ShowDetails = false;
      newChild.ChartDetails = null;
      newChild.Interval = 'daily';
      newChild.IntervalDetails = [];
      newChild.EventDetails = [];
      return newChild;
    });

    const updatedDashboardData = {
      changes: {
        ShowDetails: state.entities[assetID].Level === 'PowerGenerating',
        ChartDetails: null,
        Interval: 'daily',
        IntervalDetails: [],
        EventDetails: [],
        Children: newChildren,
      },
      id: assetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
      SelectedAssetID: assetID,
    };
  }),
  on(PowerGenActions.showDetails, (state, action) => {
    let updatedDashboardData;
    if (action.parentID === null) {
      updatedDashboardData = {
        changes: {
          ShowDetails: action.showDetailsValue,
          ChartDetails: null,
          Interval: 'daily',
          IntervalDetails: [],
          EventDetails: [],
        },
        id: action.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[action.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === action.assetID) {
          newChild.ShowDetails = action.showDetailsValue;
          newChild.ChartDetails = null;
          newChild.Interval = 'daily';
          newChild.IntervalDetails = [];
          newChild.EventDetails = [];
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: { Children: newChildren },
        id: action.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getChartData, (state, action) => {
    let updatedDashboardData;
    if (action.parentID === null) {
      updatedDashboardData = {
        changes: { ChartDetails: null },
        id: action.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[action.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === action.assetID) {
          newChild.ChartDetails = null;
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: { Children: newChildren },
        id: action.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getChartDataSuccess, (state, results) => {
    let updatedDashboardData;
    const processedChartData = processChartData(
      results.reliabilityIntervals,
      results.processDataIntervals
    );

    if (results.parentID === null) {
      updatedDashboardData = {
        changes: {
          ChartDetails: {
            EFOR: processedChartData.EFOR,
            SOF: processedChartData.SOF,
            EAF: processedChartData.EAF,
            CF: processedChartData.CF,
          },
        },
        id: results.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[results.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = {
          ...child,
        };
        if (newChild.AssetID === results.assetID) {
          newChild.ChartDetails = {
            EFOR: processedChartData.EFOR,
            SOF: processedChartData.SOF,
            EAF: processedChartData.EAF,
            CF: processedChartData.CF,
          };
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: {
          Children: newChildren,
        },
        id: results.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getChartDataError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getAllChartData, (state, action) => {
    let updatedDashboardData;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      updatedDashboardData = {
        changes: {
          ChartDetails: null,
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        newChild.ChartDetails = null;
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllChartDataSuccess, (state, results) => {
    let updatedDashboardData;
    let count = 0;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      const processedChartData = processChartData(
        results.reliabilityIntervals[count],
        results.processDataIntervals[count++]
      );

      updatedDashboardData = {
        changes: {
          ChartDetails: {
            EFOR: processedChartData.EFOR,
            SOF: processedChartData.SOF,
            EAF: processedChartData.EAF,
            CF: processedChartData.CF,
          },
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        const processedChartData = processChartData(
          results.reliabilityIntervals[count],
          results.processDataIntervals[count++]
        );

        newChild.ChartDetails = {
          EFOR: processedChartData.EFOR,
          SOF: processedChartData.SOF,
          EAF: processedChartData.EAF,
          CF: processedChartData.CF,
        };
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllChartDataError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getIntervals, (state, action) => {
    let updatedDashboardData;
    if (action.parentID === null) {
      updatedDashboardData = {
        changes: { Interval: action.interval, IntervalDetails: [] },
        id: action.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[action.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === action.assetID) {
          newChild.Interval = action.interval;
          newChild.IntervalDetails = [];
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: { Children: newChildren },
        id: action.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getIntervalsSuccess, (state, results) => {
    let updatedDashboardData;
    if (results.parentID === null) {
      updatedDashboardData = {
        changes: {
          IntervalDetails: processIntervals(
            results.reliabilityIntervals,
            results.processDataIntervals
          ),
        },
        id: results.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[results.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === results.assetID) {
          newChild.IntervalDetails = processIntervals(
            results.reliabilityIntervals,
            results.processDataIntervals
          );
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: {
          Children: newChildren,
        },
        id: results.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getIntervalsError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getAllIntervals, (state, action) => {
    let updatedDashboardData;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      updatedDashboardData = {
        changes: {
          IntervalDetails: [],
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        newChild.IntervalDetails = [];
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllIntervalsSuccess, (state, results) => {
    let updatedDashboardData;
    let count = 0;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      updatedDashboardData = {
        changes: {
          IntervalDetails: processIntervals(
            results.reliabilityIntervals[count],
            results.processDataIntervals[count++]
          ),
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        newChild.IntervalDetails = processIntervals(
          results.reliabilityIntervals[count],
          results.processDataIntervals[count++]
        );
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllIntervalsError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getEvents, (state, action) => {
    let updatedDashboardData;
    if (action.parentID === null) {
      updatedDashboardData = {
        changes: { EventDetails: [] },
        id: action.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[action.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === action.assetID) {
          newChild.EventDetails = [];
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: { Children: newChildren },
        id: action.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getEventsSuccess, (state, results) => {
    let updatedDashboardData;
    let events = results.events || [];
    events = [...events].sort((a, b) => {
      return (
        moment(b.StartTime).toDate().getTime() -
        moment(a.StartTime).toDate().getTime()
      );
    });

    if (results.parentID === null) {
      updatedDashboardData = {
        changes: {
          EventDetails: applyEventTypesToEvents(state.EventTypes, events),
        },
        id: results.assetID,
      } as Update<IDashboardData>;
    } else {
      const children = state.entities[results.parentID]?.Children || [];
      const newChildren = children.map((child) => {
        const newChild = { ...child };
        if (newChild.AssetID === results.assetID) {
          newChild.EventDetails = applyEventTypesToEvents(
            state.EventTypes,
            events
          );
        }
        return newChild;
      });

      updatedDashboardData = {
        changes: {
          Children: newChildren,
        },
        id: results.parentID,
      } as Update<IDashboardData>;
    }

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getEventsError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getEventTypesSuccess, (state, result) => {
    return {
      ...state,
      EventTypes: result.eventTypes,
    };
  }),
  on(PowerGenActions.getEventTypesError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.getAllEvents, (state, action) => {
    let updatedDashboardData;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      updatedDashboardData = {
        changes: {
          EventDetails: [],
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        newChild.EventDetails = [];
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllEventsSuccess, (state, results) => {
    let updatedDashboardData;
    let count = 0;

    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      let events = results.events[count++] || [];
      events = [...events].sort((a, b) => {
        return (
          moment(b.StartTime).toDate().getTime() -
          moment(a.StartTime).toDate().getTime()
        );
      });

      updatedDashboardData = {
        changes: {
          EventDetails: applyEventTypesToEvents(state.EventTypes, events),
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      let events = results.events[count++] || [];
      events = [...events].sort((a, b) => {
        return (
          moment(b.StartTime).toDate().getTime() -
          moment(a.StartTime).toDate().getTime()
        );
      });

      if (newChild.ShowDetails) {
        newChild.EventDetails = applyEventTypesToEvents(
          state.EventTypes,
          events
        );
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  }),
  on(PowerGenActions.getAllEventsError, (state, action) => {
    return {
      ...state,
      Error: action.error,
    };
  }),
  on(PowerGenActions.resetAllIntervalsAndEvents, (state, action) => {
    let updatedDashboardData;
    if (state.entities[state.SelectedAssetID]?.ShowDetails) {
      updatedDashboardData = {
        changes: {
          ShowDetails:
            state.entities[state.SelectedAssetID].Level === 'PowerGenerating',
          ChartDetails: null,
          Interval: 'daily',
          IntervalDetails: [],
          EventDetails: [],
        },
        id: state.SelectedAssetID,
      } as Update<IDashboardData>;

      state = powerGenAdapter.updateOne(updatedDashboardData, state);
    }

    const children = state.entities[state.SelectedAssetID]?.Children || [];
    const newChildren = children.map((child) => {
      const newChild = { ...child };
      if (newChild.ShowDetails) {
        newChild.ShowDetails = false;
        newChild.ChartDetails = null;
        newChild.Interval = 'daily';
        newChild.IntervalDetails = [];
        newChild.EventDetails = [];
      }
      return newChild;
    });

    updatedDashboardData = {
      changes: {
        Children: newChildren,
      },
      id: state.SelectedAssetID,
    } as Update<IDashboardData>;

    state = powerGenAdapter.updateOne(updatedDashboardData, state);
    return {
      ...state,
    };
  })
);

export function processDashboardData(
  allData: AnalyticsResult[],
  processData: AnalyticsResult[],
  data: AnalyticsResult
): IDashboardData {
  return {
    id: data?.AssetID, // EntityState needs this
    ParentID: data?.ParentID,
    AssetDesc: data?.AssetDesc,
    EAF: data?.EAF,
    EFOR: data?.EFOR,
    SOF: data?.SOF,
    MW: processData.find((d) => d.AssetID === data?.AssetID)?.AvgGen || 0,
    CF: processData.find((d) => d.AssetID === data?.AssetID)?.CF || 0,
    MWMaxCapacity:
      processData.find((d) => d.AssetID === data?.AssetID)?.MaxCapacity || 100,
    EventCount: data?.EventCount,
    Level: data?.Level,
    Path: getPath(allData, data),
    Siblings: getSiblings(allData.filter((d) => d.ParentID === data?.ParentID)),
    Children: processChildren(
      allData.filter((d) => d.ParentID === data?.AssetID),
      processData.filter((d) => d.ParentID === data?.AssetID)
    ),
    Data: data || null,
    ShowDetails: data?.Level === 'PowerGenerating',
    ChartDetails: null,
    Interval: 'daily',
    IntervalDetails: [],
    EventDetails: [],
  };
}

export function processChildren(
  children: AnalyticsResult[],
  processData: AnalyticsResult[]
): IChildData[] {
  const dict: { [key: number]: AnalyticsResult } = {};
  processData.forEach((element) => {
    dict[element.AssetID] = element;
  });

  const result = children.map((child) => {
    return {
      ...child,
      MW: dict[child.AssetID]?.AvgGen || 0,
      CF: dict[child.AssetID]?.CF || 0,
      MWMaxCapacity: dict[child.AssetID]?.MaxCapacity || 100,
      ShowDetails: false,
      ChartDetails: null,
      Interval: 'daily',
      IntervalDetails: [],
      EventDetails: [],
    };
  });

  return result || [];
}

export function updateChildren(
  oldChildren: IChildData[],
  newChildren: AnalyticsResult[],
  processData: AnalyticsResult[]
): IChildData[] {
  const dict: { [key: number]: AnalyticsResult } = {};
  processData.forEach((element) => {
    dict[element.AssetID] = element;
  });

  const result = newChildren.map((child) => {
    const oldChild =
      oldChildren?.find((c) => c.AssetID === child.AssetID) || null;
    return {
      ...child,
      MW: dict[child.AssetID]?.AvgGen || 0,
      CF: dict[child.AssetID]?.CF || 0,
      MWMaxCapacity: dict[child.AssetID]?.MaxCapacity || 100,
      ShowDetails: oldChild ? oldChild.ShowDetails : false,
      ChartDetails: oldChild ? oldChild.ChartDetails : null,
      Interval: oldChild ? oldChild.Interval : 'daily',
      IntervalDetails: oldChild ? oldChild.IntervalDetails : [],
      EventDetails: oldChild ? oldChild.EventDetails : [],
    };
  });

  return result || [];
}

export function getSiblings(filteredData: AnalyticsResult[]): IAssetData[] {
  const siblings = filteredData.map((d) => {
    return {
      AssetID: d.AssetID,
      AssetDesc: d.AssetDesc,
    };
  });

  return siblings || [];
}

export function getPath(
  allData: AnalyticsResult[],
  data: AnalyticsResult
): IAssetData[] {
  const path = [];
  let filteredData = data;

  while (filteredData) {
    path.unshift({
      AssetID: filteredData.AssetID,
      AssetDesc: filteredData.AssetDesc,
    });

    filteredData = allData.find((d) => d.AssetID === filteredData.ParentID);
  }

  return path;
}

export function processIntervals(
  reliabilityIntervals: AnalyticsResult[],
  processDataIntervals: AnalyticsResult[]
): AnalyticsResult[] {
  const result = reliabilityIntervals.map((rel, index) => {
    return {
      ...rel,
      MW: processDataIntervals[index]?.TotalGen || 0,
      CF: processDataIntervals[index]?.CF || 0,
    };
  });

  return result || [];
}

export function processChartData(
  reliabilityIntervals: AnalyticsResult[],
  processDataIntervals: AnalyticsResult[]
) {
  const EFOR: any = [];
  const SOF: any = [];
  const EAF: any = [];
  const CF: any = [];
  let currentEFOR = null;
  let currentSOF = null;
  let currentEAF = null;
  let currentCF = null;

  reliabilityIntervals.forEach((data, index) => {
    const start = getDate(data.StartTime).getTime();
    const end = getDate(data.EndTime).getTime();

    if (index < reliabilityIntervals.length - 1) {
      if (data.EFOR !== currentEFOR) {
        EFOR.push([start, data.EFOR]);
        currentEFOR = data.EFOR;
      } else {
        EFOR.push(getAreaConfig(start, data.EFOR, false));
      }

      if (data.SOF !== currentSOF) {
        SOF.push([start, data.SOF]);
        currentSOF = data.SOF;
      } else {
        SOF.push(getAreaConfig(start, data.SOF, false));
      }

      if (data.EAF !== currentEAF) {
        EAF.push([start, data.EAF]);
        currentEAF = data.EAF;
      } else {
        EAF.push(getAreaConfig(start, data.EAF, false));
      }
    } else {
      if (data.EFOR !== currentEFOR) {
        EFOR.push([start, data.EFOR]);
        EFOR.push(getAreaConfig(end, data.EFOR, false));
      } else {
        EFOR.push(getAreaConfig(start, data.EFOR, false));
        EFOR.push(getAreaConfig(end, data.EFOR, false));
      }

      if (data.SOF !== currentSOF) {
        SOF.push([start, data.SOF]);
        SOF.push(getAreaConfig(end, data.SOF, false));
      } else {
        SOF.push(getAreaConfig(start, data.SOF, false));
        SOF.push(getAreaConfig(end, data.SOF, false));
      }

      if (data.EAF !== currentEAF) {
        EAF.push([start, data.EAF]);
        EAF.push(getAreaConfig(end, data.EAF, false));
      } else {
        EAF.push(getAreaConfig(start, data.EAF, false));
        EAF.push(getAreaConfig(end, data.EAF, false));
      }
    }
  });

  processDataIntervals.forEach((data, index) => {
    const start = getDate(data.StartTime).getTime();
    const end = getDate(data.EndTime).getTime();

    if (index < processDataIntervals.length - 1) {
      if (data.CF !== currentCF) {
        CF.push([start, data.CF]);
        currentCF = data.CF;
      }
    } else {
      if (data.CF !== currentCF) {
        CF.push([start, data.CF]);
      }
      if (data.CF !== 0) {
        CF.push(getAreaConfig(end, data.CF, false));
      }
    }
  });

  return { EFOR, SOF, EAF, CF };
}

export function getAreaConfig(x: number, y: number, enabled: boolean) {
  const config = {
    x: x,
    y: y,
    marker: {
      enabled: enabled,
      states: {
        hover: {
          enabled: enabled,
        },
      },
    },
  };

  return config;
}

export const selectPowerGenState =
  createFeatureSelector<PowerGenState>(powerGenFeatureKey);

export const getAllDashboardData = createSelector(
  selectPowerGenState,
  selectAll
);

export const getSelectedDashboardData = createSelector(
  selectPowerGenState,
  (state) => {
    return state?.entities[state.SelectedAssetID] || null;
  }
);

export const getSelectedDashboardDataTitle = createSelector(
  selectPowerGenState,
  (state) => {
    return state?.entities[state.SelectedAssetID]?.AssetDesc || null;
  }
);

export const selectTimeSliderState = createSelector(
  selectPowerGenState,
  (state) => state.TimeSliderState
);

export const selectTimeSelection = createSelector(
  selectTimeSliderState,
  (state) => {
    return { startDate: state?.startDate, endDate: state?.endDate };
  }
);

export const selectError = createSelector(
  selectPowerGenState,
  (state) => state.Error
);

export const selectLoading = createSelector(
  selectPowerGenState,
  (state) => state.Loading
);
