/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import {
  map,
  withLatestFrom,
  catchError,
  filter,
  switchMap,
  tap,
} from 'rxjs/operators';
import { RouteSelectors } from '@atonix/shared/state/router';
import * as PowerGenActions from './power-gen.actions';
import { forkJoin, of } from 'rxjs';
import {
  getSelectedDashboardData,
  selectTimeSliderState,
} from './power-gen.reducer';
import {
  NavActions,
  updateRouteTimeSliderWithoutReloading,
} from '@atonix/atx-navigation';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsCoreService } from '@atonix/shared/api';

@Injectable()
export class PowerGenEffects implements OnInitEffects {
  constructor(
    private assetsCoreService: AssetsCoreService,
    private actions$: Actions,
    private store: Store<any>,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  load$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.enter),
      withLatestFrom(this.store.select(RouteSelectors.selectRouterState)),
      map(([action, routerState]) => {
        let start = this.getADayAgo();
        let end = this.getToday();
        if (routerState?.queryParams) {
          if (routerState.queryParams?.start) {
            start = new Date(parseFloat(routerState.queryParams?.start));
          }
          if (routerState.queryParams?.end) {
            end = new Date(parseFloat(routerState.queryParams?.end));
          }
        }

        return { start, end };
      }),
      switchMap((vals) =>
        forkJoin([
          this.assetsCoreService.load(
            vals.start.toISOString(),
            vals.end.toISOString()
          ),
          this.assetsCoreService.getProcessData(
            vals.start.toISOString(),
            vals.end.toISOString()
          ),
        ])
      ),
      switchMap(([reliabilityResults, processDataResults]) => {
        return [
          PowerGenActions.analyticsLoaded({
            reliabilityResults,
            processDataResults,
          }),
        ];
      }),
      catchError((error) => {
        return of(PowerGenActions.analyticsLoadedError({ error: 'test' }));
      })
    )
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.updateAnalytics),
      switchMap((action) =>
        forkJoin([
          this.assetsCoreService.load(
            action.start.toISOString(),
            action.end.toISOString()
          ),
          this.assetsCoreService.getProcessData(
            action.start.toISOString(),
            action.end.toISOString()
          ),
        ])
      ),
      switchMap(([reliabilityResults, processDataResults]) => {
        return [
          PowerGenActions.updateAnalyticsSuccess({
            reliabilityResults,
            processDataResults,
          }),
        ];
      }),
      catchError((error) => {
        return of(PowerGenActions.analyticsLoadedError({ error: 'test' }));
      })
    )
  );

  getChartData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getChartData),
      withLatestFrom(this.store.select(selectTimeSliderState)),
      switchMap(([action, time]) =>
        forkJoin([
          this.assetsCoreService.getReliabilityIntervals(
            action.assetID.toString(),
            'unique',
            time.startDate.toISOString(),
            time.endDate.toISOString()
          ),
          this.assetsCoreService.getProcessDataIntervals(
            action.assetID.toString(),
            'hourly',
            time.startDate.toISOString(),
            time.endDate.toISOString()
          ),
          of(action),
        ])
      ),
      switchMap(([reliabilityIntervals, processDataIntervals, action]) => {
        return [
          PowerGenActions.getChartDataSuccess({
            assetID: action.assetID,
            parentID: action.parentID,
            reliabilityIntervals,
            processDataIntervals,
          }),
        ];
      }),
      catchError((error) => {
        return of(PowerGenActions.getChartDataError({ error: 'test' }));
      })
    )
  );

  getAllChartData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getAllChartData),
      withLatestFrom(
        this.store.select(selectTimeSliderState),
        this.store.select(getSelectedDashboardData)
      ),
      map(([action, time, dashboardData]) => {
        const assetIDs = [];
        if (dashboardData?.ShowDetails) {
          assetIDs.push(dashboardData.id);
        }

        const children = dashboardData.Children || [];
        children.map((child) => {
          if (child.ShowDetails) {
            assetIDs.push(child.AssetID);
          }
        });
        return { assetIDs, time };
      }),
      filter((vals) => vals.assetIDs && vals.assetIDs?.length > 0),
      switchMap((vals) =>
        forkJoin([
          forkJoin(
            vals.assetIDs.map((asset) =>
              this.assetsCoreService.getReliabilityIntervals(
                asset.toString(),
                'unique',
                vals.time.startDate.toISOString(),
                vals.time.endDate.toISOString()
              )
            )
          ),
          forkJoin(
            vals.assetIDs.map((asset) =>
              this.assetsCoreService.getProcessDataIntervals(
                asset.toString(),
                'hourly',
                vals.time.startDate.toISOString(),
                vals.time.endDate.toISOString()
              )
            )
          ),
        ])
      ),
      switchMap(([reliabilityIntervals, processDataIntervals]) => {
        return [
          PowerGenActions.getAllChartDataSuccess({
            reliabilityIntervals,
            processDataIntervals,
          }),
        ];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(PowerGenActions.getAllChartDataError(error));
      })
    )
  );

  getReliabilityIntervals$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getIntervals),
      withLatestFrom(this.store.select(selectTimeSliderState)),
      switchMap(([action, time]) =>
        forkJoin([
          this.assetsCoreService.getReliabilityIntervals(
            action.assetID.toString(),
            action.interval,
            time.startDate.toISOString(),
            time.endDate.toISOString()
          ),
          this.assetsCoreService.getProcessDataIntervals(
            action.assetID.toString(),
            action.interval,
            time.startDate.toISOString(),
            time.endDate.toISOString()
          ),
          of(action),
        ])
      ),
      switchMap(([reliabilityIntervals, processDataIntervals, action]) => {
        return [
          PowerGenActions.getIntervalsSuccess({
            assetID: action.assetID,
            parentID: action.parentID,
            reliabilityIntervals,
            processDataIntervals,
          }),
        ];
      }),
      catchError((error) => {
        return of(PowerGenActions.getIntervalsError({ error: 'test' }));
      })
    )
  );

  getAllIntervals$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getAllIntervals),
      withLatestFrom(
        this.store.select(selectTimeSliderState),
        this.store.select(getSelectedDashboardData)
      ),
      map(([action, time, dashboardData]) => {
        const data = [];
        if (dashboardData?.ShowDetails) {
          data.push({
            assetID: dashboardData.id,
            interval: dashboardData.Interval,
          });
        }

        const children = dashboardData.Children || [];
        children.map((child) => {
          if (child.ShowDetails) {
            data.push({
              assetID: child.AssetID,
              interval: child.Interval,
            });
          }
        });
        return { data, time };
      }),
      filter((vals) => vals.data && vals.data?.length > 0),
      switchMap((vals) =>
        forkJoin([
          forkJoin(
            vals.data.map((d) =>
              this.assetsCoreService.getReliabilityIntervals(
                d.assetID.toString(),
                d.interval,
                vals.time.startDate.toISOString(),
                vals.time.endDate.toISOString()
              )
            )
          ),
          forkJoin(
            vals.data.map((d) =>
              this.assetsCoreService.getProcessDataIntervals(
                d.assetID.toString(),
                d.interval,
                vals.time.startDate.toISOString(),
                vals.time.endDate.toISOString()
              )
            )
          ),
        ])
      ),
      switchMap(([reliabilityIntervals, processDataIntervals]) => {
        return [
          PowerGenActions.getAllIntervalsSuccess({
            reliabilityIntervals,
            processDataIntervals,
          }),
        ];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(PowerGenActions.getAllIntervalsError(error));
      })
    )
  );

  getEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getEvents),
      withLatestFrom(this.store.select(selectTimeSliderState)),
      map(([action, time]) => {
        return {
          assetID: action.assetID?.toString() || null,
          parentID: action.parentID,
          start: time.startDate.toISOString(),
          end: time.endDate.toISOString(),
        };
      }),
      filter((vals) => {
        return vals.assetID ? true : false;
      }),
      switchMap((vals) => {
        return this.assetsCoreService
          .getEvents(vals.assetID, true, vals.start, vals.end)
          .pipe(
            map(
              (events) =>
                PowerGenActions.getEventsSuccess({
                  assetID: +vals.assetID,
                  parentID: vals.parentID,
                  events,
                }),
              catchError((error) =>
                of(PowerGenActions.getEventsError({ error }))
              )
            )
          );
      })
    )
  );

  getAllEvents$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getAllEvents),
      withLatestFrom(
        this.store.select(selectTimeSliderState),
        this.store.select(getSelectedDashboardData)
      ),
      map(([action, time, dashboardData]) => {
        const data = [];
        if (dashboardData?.ShowDetails) {
          data.push({ assetID: dashboardData.id });
        }

        const children = dashboardData.Children || [];
        children.map((child) => {
          if (child.ShowDetails) {
            data.push({ assetID: child.AssetID });
          }
        });
        return { data, time };
      }),
      filter((vals) => vals.data && vals.data?.length > 0),
      switchMap((vals) =>
        forkJoin(
          vals.data.map((d) =>
            this.assetsCoreService.getEvents(
              d.assetID,
              true,
              vals.time.startDate.toISOString(),
              vals.time.endDate.toISOString()
            )
          )
        )
      ),
      switchMap((events) => {
        return [PowerGenActions.getAllEventsSuccess({ events })];
      }),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(PowerGenActions.getAllEventsError(error));
      })
    )
  );

  getEventTypes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.getEventTypes),
      switchMap(() =>
        this.assetsCoreService.getEventTypes().pipe(
          map(
            (eventTypes) =>
              PowerGenActions.getEventTypesSuccess({ eventTypes }),
            catchError((error) => of(PowerGenActions.getEventTypesError(error)))
          )
        )
      )
    )
  );

  updateRoute$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PowerGenActions.updateRoute),
      withLatestFrom(this.store.select(selectTimeSliderState)),
      switchMap(([action, time]) => {
        const start = time?.startDate?.getTime().toString();
        const end = time?.endDate?.getTime().toString();

        updateRouteTimeSliderWithoutReloading(
          start,
          end,
          this.router,
          this.activatedRoute
        );

        return [
          NavActions.updateNavigationItems({
            urlParams: { start, end },
          }),
        ];
      })
    )
  );

  reflow$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NavActions.toggleNavigation, NavActions.clickLayoutButton),
        tap(() => {
          setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
          }, 400);
        })
      ),
    { dispatch: false }
  );

  ngrxOnInitEffects(): Action {
    return PowerGenActions.noop();
  }

  getADayAgo() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1, 0, 0, 0, 0);
  }
  getToday() {
    const d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
  }
}
