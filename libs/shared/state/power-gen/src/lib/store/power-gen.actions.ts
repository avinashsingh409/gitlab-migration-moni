/* eslint-disable ngrx/prefer-inline-action-props */
import { createAction, props } from '@ngrx/store';
import { ITimeSliderStateChange } from '@atonix/atx-time-slider';
import { IEvent, IEventType } from '@atonix/atx-core';
import { AnalyticsResult } from '@atonix/shared/api';

export const enter = createAction('[PowerGen Page] Enter');
export const noop = createAction('[PowerGen Page] Noop');

export const analyticsLoaded = createAction(
  '[PowerGen] Analytics Loaded Success',
  props<{
    reliabilityResults: AnalyticsResult[];
    processDataResults: AnalyticsResult[];
  }>()
);

export const analyticsLoadedError = createAction(
  '[PowerGen] Analytics Loaded Error',
  props<{ error: string }>()
);

export const updateAnalytics = createAction(
  '[PowerGen] Update Analytics',
  props<{ start: Date; end: Date }>()
);

export const updateAnalyticsSuccess = createAction(
  '[PowerGen] Update Analytics Success',
  props<{
    reliabilityResults: AnalyticsResult[];
    processDataResults: AnalyticsResult[];
  }>()
);

export const selectAsset = createAction(
  '[PowerGen] Select Asset',
  props<{ assetID: number }>()
);

export const initializeTimeSlider = createAction(
  '[PowerGen] Initialize Time Slider',
  props<{ start: Date; end: Date }>()
);
export const timeSliderStateChange = createAction(
  '[PowerGen] Time Slider State Change',
  props<ITimeSliderStateChange>()
);

export const showDetails = createAction(
  '[PowerGen] Show Details',
  props<{ assetID: number; parentID: number; showDetailsValue: boolean }>()
);

export const getChartData = createAction(
  '[PowerGen] Get Chart Data',
  props<{ assetID: number; parentID: number }>()
);

export const getChartDataSuccess = createAction(
  '[PowerGen] Get Chart Data Success',
  props<{
    assetID: number;
    parentID: number;
    reliabilityIntervals: AnalyticsResult[];
    processDataIntervals: AnalyticsResult[];
  }>()
);

export const getChartDataError = createAction(
  '[PowerGen] Get Intervals Error',
  props<{ error: string }>()
);

export const getAllChartData = createAction('[PowerGen] Get All Chart Data');

export const getAllChartDataSuccess = createAction(
  '[PowerGen] Get All Chart Data Success',
  props<{
    reliabilityIntervals: AnalyticsResult[][];
    processDataIntervals: AnalyticsResult[][];
  }>()
);

export const getAllChartDataError = createAction(
  '[PowerGen] Get All Intervals Error',
  props<{ error: string }>()
);

export const getIntervals = createAction(
  '[PowerGen] Get Intervals',
  props<{ assetID: number; parentID: number; interval: string }>()
);

export const getIntervalsSuccess = createAction(
  '[PowerGen] Get Intervals Success',
  props<{
    assetID: number;
    parentID: number;
    reliabilityIntervals: AnalyticsResult[];
    processDataIntervals: AnalyticsResult[];
  }>()
);

export const getIntervalsError = createAction(
  '[PowerGen] Get Intervals Error',
  props<{ error: string }>()
);

export const getAllIntervals = createAction('[PowerGen] Get All Intervals');

export const getAllIntervalsSuccess = createAction(
  '[PowerGen] Get All Intervals Success',
  props<{
    reliabilityIntervals: AnalyticsResult[][];
    processDataIntervals: AnalyticsResult[][];
  }>()
);

export const getAllIntervalsError = createAction(
  '[PowerGen] Get All Intervals Error',
  props<{ error: string }>()
);

export const getEvents = createAction(
  '[PowerGen] Get Events',
  props<{ assetID: number; parentID: number }>()
);

export const getEventsSuccess = createAction(
  '[PowerGen] Get Events Success',
  props<{ assetID: number; parentID: number; events: IEvent[] }>()
);

export const getEventsError = createAction(
  '[PowerGen] Get Events Error',
  props<{ error: string }>()
);

export const getAllEvents = createAction('[PowerGen] Get All Events');

export const getAllEventsSuccess = createAction(
  '[PowerGen] Get All Events Success',
  props<{ events: IEvent[][] }>()
);

export const getAllEventsError = createAction(
  '[PowerGen] Get All Events Error',
  props<{ error: string }>()
);

export const getEventTypes = createAction('[PowerGen] Get Event Types');

export const getEventTypesSuccess = createAction(
  '[PowerGen] Get Event Types Success',
  props<{ eventTypes: IEventType[] }>()
);

export const getEventTypesError = createAction(
  '[PowerGen] Get Event Types Error',
  props<{ error: string }>()
);

export const updateRoute = createAction('[PowerGen] Update Route');

export const resetAllIntervalsAndEvents = createAction(
  '[PowerGen] reset All Intervals and Events'
);
