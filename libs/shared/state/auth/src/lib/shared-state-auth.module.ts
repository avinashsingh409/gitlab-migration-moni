import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { JwtInterceptorService } from './services/jwt-interceptor.service';
import { AuthFacade } from './store/auth.facade';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/auth.effects';
import { StoreModule } from '@ngrx/store';
import { authReducer } from './store/auth.reducers';
import { SharedApiModule } from '@atonix/shared/api';
import { TokenGuard } from './services/guards/token.guard';

@NgModule({
  imports: [
    CommonModule,
    SharedUtilsModule,
    SharedApiModule,
    EffectsModule.forFeature([AuthEffects]),
    StoreModule.forFeature('auth', authReducer),
  ],
})
export class SharedStateAuthModule {
  static forRoot(): ModuleWithProviders<SharedStateAuthModule> {
    return {
      ngModule: SharedStateAuthModule,
      providers: [
        AuthService,
        AuthFacade,
        TokenGuard,
        {
          provide: HTTP_INTERCEPTORS,
          useExisting: JwtInterceptorService,
          multi: true,
        },
      ],
    };
  }
}
