import { AtonixAuthenticationConstants } from './authentication-constants';

export interface IAuthenticationResponse {
  UserConfirmationNecessary?: boolean;
  Error?: any;
  Status: AtonixAuthenticationConstants;
  ChallengeName?: string;
  ChallengeParameters?: string;
  UserAttributes?: any;
  RequiredAttributes?: any;
  EmailConfirmed?: boolean;
  CodeDeliveryDetails?: {
    AttributeName: string;
    DeliveryMedium: string;
    Destination: string;
  };
}
