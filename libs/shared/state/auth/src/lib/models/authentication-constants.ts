export enum AtonixAuthenticationConstants {
  LoginSuccessful,
  MFARequired,
  TotpRequired,
  PasswordChangeRequired,
  LoginFailed,
  ChangeSuccessful,
  VerificationCodeRequired,
  InitialPasswordRequired,
  NotImplemented,
  CustomChallenge,
  MfaSetup,
  SelectMfaType,
}
