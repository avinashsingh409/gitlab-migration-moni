import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import { AuthFacade } from '../../store/auth.facade';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(private authFacade: AuthFacade) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    const code = state.root.queryParams?.code || '';
    const stateKey = state.root.queryParams?.state || '';
    return this.getLoggedInState().pipe(
      switchMap((isLoggedIn) => {
        if (isLoggedIn) {
          return of(true);
        }
        this.authFacade.authInit(code, stateKey);
        return of(true);
      }),
      catchError(() => {
        return of(false);
      })
    );
  }

  private getLoggedInState(): Observable<boolean> {
    return this.authFacade.isLoggedIn$.pipe(take(1));
  }
}
