import { TestBed, inject } from '@angular/core/testing';
import { JwtInterceptorService } from './jwt-interceptor.service';
import {
  HttpClient,
  HTTP_INTERCEPTORS,
  HttpRequest,
  HttpHeaders,
  HttpErrorResponse,
  HttpClientModule,
} from '@angular/common/http';
import { createMock } from '@testing-library/angular/jest-utils';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { AuthService } from './auth.service';
import { of, throwError } from 'rxjs';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

describe('JwtInterceptorService', () => {
  let httpMock: HttpTestingController;
  let authService: AuthService;
  let httpClient: HttpClient;
  beforeEach(() => {
    authService = createMock(AuthService);
    authService.GetIdTokenOrWait = jest
      .fn()
      .mockReturnValue(of('testToken123'));
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [
        { provide: AuthService, useValue: authService },
        { provide: APP_CONFIG, useValue: AppConfig },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptorService,
          multi: true,
        },
      ],
    });
    authService = TestBed.inject<AuthService>(AuthService);
    httpMock = TestBed.inject<HttpTestingController>(HttpTestingController);
    httpClient = TestBed.inject<HttpClient>(HttpClient);
  });

  it('should be created', inject(
    [JwtInterceptorService],
    (service: JwtInterceptorService) => {
      expect(service).toBeTruthy();
    }
  ));

  describe('making http calls to trigger intercept', () => {
    it('should add Authorization header', () => {
      let result = null;
      httpClient.get('/api').subscribe((data) => {
        result = data;
      });
      httpMock
        .expectOne(
          (r) =>
            r.headers.has('Authorization') &&
            r.headers.get('Authorization') === 'Bearer testToken123'
        )
        .flush({ response: true });
      httpMock.verify();
      expect(result).toEqual({ response: true });
    });

    it('should alter the url', () => {
      let result = null;
      httpClient
        .get('https://dev.atonix.com/Services/api')
        .subscribe((data) => {
          result = data;
        });

      httpMock
        .expectOne((r) => r.url === 'https://dev.atonix.com/Services/api')
        .flush({ response: true });

      httpMock.verify();
      expect(result).toEqual({ response: true });
    });

    it('should not add Authorization to anonymous traffic', () => {
      let result = null;
      httpClient
        .get('https://dev.atonix.com/Services/api', {
          headers: new HttpHeaders({
            Anonymous: '',
          }),
        })
        .subscribe((data) => {
          result = data;
        });
      httpMock
        .expectOne((r) => !r.headers.has('Authorization'))
        .flush({ response: true });
      expect(result).toEqual({ response: true });
    });

    it('should handle unspecified errors', inject(
      [JwtInterceptorService],
      (service: JwtInterceptorService) => {
        const request = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice'
        );
        const handler = { handle: jest.fn(() => throwError({ id: 1 })) };
        let myVal: any = null;
        let myErr: any = null;
        service.intercept(request, handler).subscribe(
          (val) => {
            myVal = val;
          },
          (err: unknown) => {
            myErr = err;
          }
        );
        expect(authService.GetIdTokenOrWait).toHaveBeenCalled();
        expect(handler.handle).toHaveBeenCalled();
        expect(myVal).toBeFalsy();
        expect(myErr).toBeTruthy();
        expect(myErr.id).toEqual(1);
      }
    ));

    it('should handle normal 400 errors', inject(
      [JwtInterceptorService],
      (service: JwtInterceptorService) => {
        const request = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice'
        );
        const handler = {
          handle: jest.fn(() =>
            throwError(
              new HttpErrorResponse({
                error: 'myErr',
                status: 400,
                statusText: 'statText',
              })
            )
          ),
        };
        let myVal: any = null;
        let myErr: any = null;
        service.intercept(request, handler).subscribe(
          (val) => {
            myVal = val;
          },
          (err: unknown) => {
            myErr = err;
          }
        );
        expect(authService.GetIdTokenOrWait).toHaveBeenCalled();
        expect(handler.handle).toHaveBeenCalled();
        expect(myVal).toBeFalsy();
        expect(myErr).toBeTruthy();
        expect(myErr.error).toEqual('myErr');
      }
    ));

    it('should handle token refresh errors', inject(
      [JwtInterceptorService],
      (service: JwtInterceptorService) => {
        const request1 = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice1'
        );
        const request2 = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice2'
        );
        const errorError = new ErrorEvent('error');
        const error = new HttpErrorResponse({ error: errorError });
        const handler = {
          handle: jest.fn((value: HttpRequest<any>) => {
            if (value.headers.get('Authorization') !== 'Bearer testToken123') {
              return throwError(error);
            } else {
              return of({
                id: 1,
                url: value.url,
                body: '',
                type: null,
                clone: '',
                status: '',
                statusText: '',
                ok: '',
                headers: '',
              });
            }
          }),
        };

        let myVal1: any = null;
        let myErr1: any = null;
        service.intercept(request1, handler).subscribe(
          (val) => {
            myVal1 = val;
          },
          (err: unknown) => {
            myErr1 = err;
          }
        );

        expect(authService.GetIdTokenOrWait).toHaveBeenCalled();
        expect(myVal1.url).toEqual(
          'https://dev.atonix.com/Services/someservice1'
        );
        expect(handler.handle).toHaveBeenCalled();
        expect(myVal1).toBeTruthy();
        expect(myErr1).toBeNull();
        authService.GetIdTokenOrWait = jest.fn().mockReturnValue(of(''));

        let myVal2: any = null;
        let myErr2: any = null;
        service.intercept(request2, handler).subscribe(
          (val) => {
            myVal2 = val;
          },
          (err: unknown) => {
            myErr2 = err;
          }
        );
        expect(myVal2).toBeNull();
        expect(myErr2).toBeTruthy();
      }
    ));

    it('should handle error refresh', inject(
      [JwtInterceptorService],
      (service: JwtInterceptorService) => {
        const request1 = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice1'
        );
        const request2 = new HttpRequest<any>(
          'GET',
          'https://dev.atonix.com/Services/someservice2'
        );
        const errorError = new ErrorEvent('error');
        const error = new HttpErrorResponse({ error: errorError });
        const handler = {
          handle: jest.fn((value: HttpRequest<any>) => {
            if (value.headers.get('Authorization') !== 'Bearer testToken123') {
              return throwError(error);
            } else {
              return of({
                id: 1,
                url: value.url,
                body: '',
                type: null,
                clone: '',
                status: '',
                statusText: '',
                ok: '',
                headers: '',
              });
            }
          }),
        };

        let myVal1: any = null;
        let myErr1: any = null;
        service.intercept(request1, handler).subscribe(
          (val) => {
            myVal1 = val;
          },
          (err: unknown) => {
            myErr1 = err;
          }
        );
        expect(authService.GetIdTokenOrWait).toHaveBeenCalled();
        expect(handler.handle).toHaveBeenCalled();
        expect(myVal1).toBeTruthy();
        expect(myErr1).toBeNull();
        authService.GetIdTokenOrWait = jest.fn().mockReturnValue(of(''));

        let myVal2: any = null;
        let myErr2: any = null;
        service.intercept(request2, handler).subscribe(
          (val) => {
            myVal2 = val;
          },
          (err: unknown) => {
            myErr2 = err;
          }
        );

        expect(myVal2).toBeNull();
        expect(myErr2).toBeTruthy();
      }
    ));
  });
});
