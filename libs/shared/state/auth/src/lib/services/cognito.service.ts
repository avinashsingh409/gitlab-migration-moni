import { Injectable } from '@angular/core';
import * as cognito from 'amazon-cognito-identity-js';

@Injectable({
  providedIn: 'root',
})
export class CognitoService {
  createUserPool(poolData: cognito.ICognitoUserPoolData) {
    return new cognito.CognitoUserPool(poolData);
  }

  createCognitoUser(userData: cognito.ICognitoUserData) {
    return new cognito.CognitoUser(userData);
  }

  createAuthenticationDetails(
    authenticationData: cognito.IAuthenticationDetailsData
  ) {
    return new cognito.AuthenticationDetails(authenticationData);
  }

  createUserAttribute(data: cognito.ICognitoUserAttributeData) {
    return new cognito.CognitoUserAttribute(data);
  }

  getSessionStorage() {
    return window.sessionStorage;
  }

  setUserPoolID(userPoolID) {
    window.localStorage.setItem('Cognito_UserPoolID', userPoolID);
  }

  getUserPoolID() {
    return window.localStorage.getItem('Cognito_UserPoolID');
  }

  removeUserPoolID() {
    window.localStorage.removeItem('Cognito_UserPoolID');
  }

  setClientID(clientID: string) {
    window.localStorage.setItem('Cognito_ClientID', clientID);
  }

  getClientID() {
    return window.localStorage.getItem('Cognito_ClientID');
  }

  removeClientID() {
    window.localStorage.removeItem('Cognito_ClientID');
  }

  setHostedUI(hostedUI: string) {
    window.localStorage.setItem('Cognito_HostedUI', hostedUI);
  }

  getHostedUI() {
    return window.localStorage.getItem('Cognito_HostedUI');
  }

  removeHostedUI() {
    window.localStorage.removeItem('Cognito_HostedUI');
  }

  setAuthFlowType(authFlowType: string) {
    window.localStorage.setItem('Cognito_AuthFlowType', authFlowType);
  }

  getAuthFlowType() {
    return window.localStorage.getItem('Cognito_AuthFlowType');
  }

  removeAuthFlowType() {
    window.localStorage.removeItem('Cognito_AuthFlowType');
  }

  setUsername(username: string) {
    window.localStorage.setItem('Cognito_Username', username);
  }

  getUsername() {
    return window.localStorage.getItem('Cognito_Username');
  }

  removeUsername() {
    window.localStorage.removeItem('Cognito_Username');
  }

  setState(state: string) {
    window.sessionStorage.setItem('Cognito_State', state);
  }

  getState() {
    return window.sessionStorage.getItem('Cognito_State');
  }

  removeState() {
    window.sessionStorage.removeItem('Cognito_State');
  }

  logError(error) {
    console.log(error);
  }
}
