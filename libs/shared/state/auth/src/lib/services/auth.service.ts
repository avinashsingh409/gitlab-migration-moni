/* eslint-disable max-len */
import { Injectable, Inject } from '@angular/core';
import { Observable, Subject, of, throwError } from 'rxjs';
import jwtDecode from 'jwt-decode';
import * as cognito from 'amazon-cognito-identity-js';
import { catchError, switchMap, take, map, tap } from 'rxjs/operators';
import { CognitoService } from './cognito.service';
import { IAuthenticationResponse } from '../models/authentication-response';
import { AtonixAuthenticationConstants } from '../models/authentication-constants';
import { LoggerService } from '@atonix/shared/utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  AccountFrameworkService,
  AuthorizationFrameworkService,
} from '@atonix/shared/api';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userpool: cognito.CognitoUserPool;
  private user: cognito.CognitoUser;
  private session: cognito.CognitoUserSession;
  private attributes: cognito.CognitoUserAttribute[];
  public onLoggedOut: Subject<boolean> = new Subject<boolean>();
  public onRefreshToken$: Subject<boolean> = new Subject<boolean>();

  // This subject will emit something whenever the user logs in.
  // It will be used to release the waiting requests.  That way we don't
  // get a bunch of failed server requests while we are waiting for the user
  // to log in.
  private loggedIn$: Subject<boolean>;

  constructor(
    private cognitoService: CognitoService,
    private authorizationFrameworkService: AuthorizationFrameworkService,
    private accountFrameworkService: AccountFrameworkService,
    private logger: LoggerService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
    this.loggedIn$ = new Subject<boolean>();

    if (this.UserPoolID() !== null && this.ClientID() !== null) {
      this.Initialize();
    }
  }

  public Initialize(): Observable<boolean> {
    return this.populateUser(false).pipe(
      switchMap((success) => {
        if (success) {
          return of(true);
        } else {
          return this.populateUser(true);
        }
      })
    );
  }

  public SendTOTPCode(code: string) {
    if (this.user && this.user.sendMFACode) {
      return new Observable<IAuthenticationResponse>((subscribe) => {
        this.user.sendMFACode(
          code,
          {
            onSuccess: (session) => {
              this.session = session;
              subscribe.next({
                Status: AtonixAuthenticationConstants.LoginSuccessful,
              });
              this.loginCompleted();
              subscribe.complete();
            },
            onFailure: (err) => {
              subscribe.next({
                Status: AtonixAuthenticationConstants.LoginFailed,
                Error: err,
              });
              subscribe.complete();
            },
          },
          'SOFTWARE_TOKEN_MFA'
        );
      });
    } else {
      return of({ Error: 'No Auth Flow Present' } as IAuthenticationResponse);
    }
  }

  public SendMFACode(code: string) {
    if (this.user && this.user.sendMFACode) {
      return new Observable<IAuthenticationResponse>((subscribe) => {
        this.user.sendMFACode(code, {
          onSuccess: (session) => {
            this.session = session;
            subscribe.next({
              Status: AtonixAuthenticationConstants.LoginSuccessful,
            });
            this.loginCompleted();
            subscribe.complete();
          },
          onFailure: (err) => {
            subscribe.next({
              Status: AtonixAuthenticationConstants.LoginFailed,
              Error: err,
            });
            subscribe.complete();
          },
        });
      });
    } else {
      return of({ Error: 'No Auth Flow Present' } as IAuthenticationResponse);
    }
  }

  public InitialPassword(password: string, requiredAttributes: any) {
    if (this.user && this.user.completeNewPasswordChallenge) {
      return new Observable<IAuthenticationResponse>((subscribe) => {
        this.user.completeNewPasswordChallenge(
          password,
          requiredAttributes || {},
          {
            onSuccess: (session) => {
              this.session = session;
              subscribe.next({
                Status: AtonixAuthenticationConstants.LoginSuccessful,
              });
              this.loginCompleted();
              subscribe.complete();
            },
            onFailure: (err) => {
              subscribe.next({
                Status: AtonixAuthenticationConstants.LoginFailed,
                Error: err,
              });
              subscribe.complete();
            },
            mfaRequired: (challengeName: any, challengeParameters: any) => {
              subscribe.next({
                Status: AtonixAuthenticationConstants.MFARequired,
                ChallengeName: challengeName,
                ChallengeParameters: challengeParameters,
              });
              subscribe.complete();
            },
            customChallenge: (challengeParameters: any) => {
              subscribe.next({
                Status: AtonixAuthenticationConstants.CustomChallenge,
                ChallengeParameters: challengeParameters,
              });
              subscribe.complete();
            },
            mfaSetup: (challengeName: any, challengeParameters: any) => {
              subscribe.next({
                Status: AtonixAuthenticationConstants.MfaSetup,
                ChallengeName: challengeName,
                ChallengeParameters: challengeParameters,
              });
              subscribe.complete();
            },
          }
        );
      });
    } else {
      return of({ Error: 'No Auth Flow Present' } as IAuthenticationResponse);
    }
  }

  public Login(username: string, password: string, rememberMe: boolean) {
    const authenticationData: cognito.IAuthenticationDetailsData = {
      Username: username,
      Password: password,
    };

    const authenticationDetails = new cognito.AuthenticationDetails(
      authenticationData
    );

    this.user = this.CreateUser(username, rememberMe);
    // 'USER_SRP_AUTH' standard, 'USER_PASSWORD_AUTH' for migration
    this.user.setAuthenticationFlowType(this.AuthFlowType());

    return new Observable<IAuthenticationResponse>((subscribe) => {
      this.user.authenticateUser(authenticationDetails, {
        onSuccess: (
          session: cognito.CognitoUserSession,
          userConfirmationNecessary?: boolean
        ) => {
          this.session = session;
          this.loginCompleted();

          this.user.getUserAttributes((error, attributes) => {
            let emailVerified = true;
            if (attributes) {
              emailVerified = false;
              for (const a of attributes) {
                if (a.getName() === 'email_verified') {
                  emailVerified = a.getValue() === 'true';
                }
              }
            }

            subscribe.next({
              Status: AtonixAuthenticationConstants.LoginSuccessful,
              EmailConfirmed: emailVerified,
            });
            subscribe.complete();
          });
        },
        onFailure: (err: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.LoginFailed,
            Error: err,
          });
          subscribe.complete();
        },
        newPasswordRequired: (userAttributes: any, requiredAttributes: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.PasswordChangeRequired,
            UserAttributes: userAttributes,
            RequiredAttributes: requiredAttributes,
          });
          subscribe.complete();
        },
        mfaRequired: (challengeName: any, challengeParameters: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.MFARequired,
            ChallengeName: challengeName,
            ChallengeParameters: challengeParameters,
          });
          subscribe.complete();
        },
        totpRequired: (challengeName: any, challengeParameters: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.TotpRequired,
            ChallengeName: challengeName,
            ChallengeParameters: challengeParameters,
          });
          subscribe.complete();
        },
        customChallenge: (challengeParameters: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.CustomChallenge,
            ChallengeParameters: challengeParameters,
          });
          subscribe.complete();
        },
        mfaSetup: (challengeName: any, challengeParameters: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.MfaSetup,
            ChallengeName: challengeName,
            ChallengeParameters: challengeParameters,
          });
          subscribe.complete();
        },
        selectMFAType: (challengeName: any, challengeParameters: any) => {
          subscribe.next({
            Status: AtonixAuthenticationConstants.SelectMfaType,
            ChallengeName: challengeName,
            ChallengeParameters: challengeParameters,
          });
          subscribe.complete();
        },
      });
    });
  }

  private getPoolData(): cognito.ICognitoUserPoolData {
    return {
      UserPoolId: this.UserPoolID(),
      ClientId: this.ClientID(),
    };
  }

  public populateUser(useSession: boolean = false): Observable<boolean> {
    const poolData = this.getPoolData();

    // If the user has unchecked remember me this should use session storage instead of local storage.
    if (useSession) {
      poolData.Storage = this.cognitoService.getSessionStorage();
    }

    this.userpool = this.cognitoService.createUserPool(poolData);
    this.user = this.userpool.getCurrentUser();

    if (this.user && this.user.getSession) {
      return new Observable<boolean>((subscribe) => {
        this.user.getSession((err, s) => {
          if (err) {
            subscribe.next(false);
          } else {
            this.session = s;
            this.attributes = null;
            subscribe.next(true);
          }
          subscribe.complete();
        });
      });
    } else {
      return of(false);
    }
  }

  public UserPoolID(newVal?: string) {
    if (newVal !== undefined) {
      this.cognitoService.setUserPoolID(newVal);
    }
    return this.cognitoService.getUserPoolID();
  }

  public ClientID(newVal?: string) {
    if (newVal !== undefined) {
      this.cognitoService.setClientID(newVal);
    }
    return this.cognitoService.getClientID();
  }

  public HostedUI(newVal?: string) {
    if (newVal !== undefined) {
      this.cognitoService.setHostedUI(newVal);
    }
    return this.cognitoService.getHostedUI();
  }

  public AuthFlowType(newVal?: string) {
    if (newVal !== undefined) {
      this.cognitoService.setAuthFlowType(newVal);
    }
    return this.cognitoService.getAuthFlowType();
  }

  public SaveCallbackUrlInStorage() {
    if (window && window.localStorage) {
      const id = String(Math.floor(Math.random() * 10000));
      window.localStorage.setItem(
        'REDIRECTSTATE:' + id,
        window?.location?.href || ''
      );
      return id;
    }
    return 0;
  }

  public GetCallbackUrlFromStorage(state: number) {
    let result: string = null;
    if (state) {
      result = window?.localStorage?.getItem('REDIRECTSTATE:' + state);
    }
    return result;
  }

  public CreateHostedUIURL() {
    const statevalue = this.SaveCallbackUrlInStorage();
    const clientID = this.ClientID();
    const hostedUI = this.HostedUI();

    let url = '';
    if (clientID && hostedUI) {
      const redirectURL = this.appConfig.authCallback;
      url =
        hostedUI +
        '/login?response_type=code&client_id=' +
        clientID +
        '&redirect_uri=' +
        redirectURL +
        '&state=' +
        statevalue;
    }

    return url;
  }

  private CreateUser(email: string, rememberMe: boolean = true) {
    const userData: cognito.ICognitoUserData = {
      Username: email,
      Pool: this.userpool,
    };

    if (!rememberMe) {
      userData.Storage = this.cognitoService.getSessionStorage();
    }

    return this.cognitoService.createCognitoUser(userData);
  }

  private GetAttribute(attributeName): Observable<string> {
    let value = '';
    return new Observable<string>((res) => {
      if (this.attributes) {
        for (const attribute of this.attributes) {
          if (attribute.getName() === attributeName) {
            value = attribute.getValue();
            break;
          }
        }
        res.next(value);
        res.complete();
      } else {
        this.user.getUserAttributes((err, result) => {
          if (err) {
            res.error(err);
          } else {
            this.attributes = result;
            for (const r of result) {
              if (r.getName() === attributeName) {
                value = r.getValue();
                break;
              }
            }
            res.next(value);
          }
          res.complete();
        });
      }
    });
  }

  private loginCompleted() {
    this.loggedIn$.next(true);
  }

  public GetSessionWithToken(tokenString?: string) {
    const tokens: any = JSON.parse(tokenString);
    if (tokens) {
      if (tokens.error) {
        const errorMessage = `Federated Provider Error: ${tokens.error}`;
        return throwError({ valid: false, message: errorMessage });
      }
      if (tokens && tokens.refresh_token && tokens.id_token) {
        const token = jwtDecode(tokens.id_token);
        // console.log(token); // -- jwt response
        if (
          this.cognitoService.getUsername() !== token['email'] &&
          this.cognitoService.getUsername() !== token['cognito:username']
        ) {
          const errorMessage = `Another user on your device is authenticated.`;
          this.authorizationFrameworkService
            .logoutUser()
            .pipe(take(1))
            .subscribe((val) => {
              setTimeout(() => {
                if (val !== '') {
                  window.location.href = val;
                }
              }, 1000);
            });
          return throwError({ valid: false, message: errorMessage });
        }
        return new Observable<boolean>((res) => {
          const poolData = this.getPoolData();
          const userpool = new cognito.CognitoUserPool(poolData);

          const data: cognito.ICognitoUserData = {
            Username: token['cognito:username'],
            Pool: userpool,
          };

          // this is not authenticated user.
          // force sign out based on domain:
          // https://asset360dev2.auth.us-east-1.amazoncognito.com/logout?client_id=77dpshfpmakmnvdhhld17ne9s1&logout_uri=http://localhost:4200/
          const user = new cognito.CognitoUser(data);

          const rt = new cognito.CognitoRefreshToken({
            RefreshToken: tokens.refresh_token,
          });
          this.userpool = userpool;
          this.user = user;

          user.refreshSession(rt, (err, session) => {
            if (err) {
              console.log(`error: ${JSON.stringify(err)}`);
              res.next(false);
            } else {
              this.userpool = userpool;
              this.user = user;
              this.session = session;
              //   console.log(`session: ${JSON.stringify(session)}`);
              res.next(true);
            }
            res.complete();
          });
        });
      } else {
        return of(false);
      }
    } else {
      return of(false);
    }
  }

  public LogOut(): Observable<boolean> {
    const loggedIn = this.IsLoggedIn();
    if (this.user) {
      this.user.signOut();
      this.user = null;
    }
    this.session = null;
    // Removing this after logout
    this.cognitoService.removeUserPoolID();
    this.cognitoService.removeClientID();
    this.cognitoService.removeUsername();
    this.cognitoService.removeState();
    this.onLoggedOut.next(loggedIn);
    if (window && window.localStorage) {
      window.localStorage.clear();
    }
    if (window && window.sessionStorage) {
      window.sessionStorage.clear();
    }
    this.authorizationFrameworkService
      .logoutUser()
      .pipe(take(1))
      .subscribe((val) => {
        if (val !== '') {
          window.location.href = val;
        }
      });

    return of(true);
  }

  public IsLoggedIn() {
    let result = false;
    if (this.user) {
      result = true;
    }
    return result;
  }

  public FriendlyName() {
    return this.GetAttribute('name').pipe(
      catchError((err: unknown) => {
        this.cognitoService.logError(
          'Error getting name:' + JSON.stringify(err)
        );
        return of('');
      })
    );
  }

  public Email() {
    return this.GetAttribute('email').pipe(
      catchError((err: unknown) => {
        this.cognitoService.logError(
          'Error getting email:' + JSON.stringify(err)
        );
        return of('');
      })
    );
  }

  public PhoneNumber() {
    return this.GetAttribute('phone_number').pipe(
      catchError((err: unknown) => {
        this.cognitoService.logError(
          'Error getting phone_number:' + JSON.stringify(err)
        );
        return of('');
      })
    );
  }

  public PhoneNumberVerified() {
    return this.GetAttribute('phone_number_verified').pipe(
      map((data: string) => {
        const pnv: boolean = data === 'true';
        return pnv;
      }),
      catchError((err: unknown) => {
        this.cognitoService.logError(
          'Error getting phone number verified:' + JSON.stringify(err)
        );
        return of(null);
      })
    );
  }

  public GetIdToken(force: boolean = false): Observable<string> {
    return new Observable<string>((observer) => {
      this.attributes = null;
      if (this.session && !force) {
        if (this.session.isValid()) {
          const t = this.session.getIdToken().getJwtToken();
          observer.next(t);
          observer.complete();
        } else {
          const refreshToken = this.session.getRefreshToken();
          this.user.refreshSession(refreshToken, (err, session) => {
            if (err) {
              if (err.code === 'NotAuthorizedException') {
                // If the result of the refresh call is not authorized then the user needs to be logged out.
                this.LogOut();
              }
              observer.error('Could not refresh user.');
              // eslint-disable-next-line rxjs/no-redundant-notify
              observer.complete();
            } else {
              this.session = session;
              const t = this.session.getIdToken().getJwtToken();
              observer.next(t);
              observer.complete();
              this.onRefreshToken$.next(true);
            }
          });
        }
      } else if (this.user) {
        this.user.getSession((err, session) => {
          if (err) {
            if (err.code === 'UserNotFoundException') {
              // If the result of the refresh call is not authorized then the user needs to be logged out.
              this.LogOut();
            }
            observer.error('Could not get user.');
            // eslint-disable-next-line rxjs/no-redundant-notify
            observer.complete();
          } else {
            this.session = session;
            observer.next(this.session.getIdToken().getJwtToken());
            observer.complete();
            this.onRefreshToken$.next(true);
          }
        });
      } else {
        throwError('no user');
      }
    });
  }

  public GetAccessToken(force: boolean = false): Observable<string> {
    return new Observable<string>((observer) => {
      this.attributes = null;
      if (this.session && !force) {
        if (this.session.isValid()) {
          const t = this.session.getAccessToken().getJwtToken();
          observer.next(t);
          observer.complete();
        } else {
          const refreshToken = this.session.getRefreshToken();
          this.user.refreshSession(refreshToken, (err, session) => {
            if (err) {
              if (err.code === 'NotAuthorizedException') {
                // If the result of the refresh call is not authorized then the user needs to be logged out.
                this.LogOut();
              }
              observer.error('Could not refresh user.');
              // eslint-disable-next-line rxjs/no-redundant-notify
              observer.complete();
            } else {
              this.session = session;
              const t = this.session.getAccessToken().getJwtToken();
              observer.next(t);
              observer.complete();
              this.onRefreshToken$.next(true);
            }
          });
        }
      } else if (this.user) {
        this.user.getSession((err, session) => {
          if (err) {
            if (err.code === 'UserNotFoundException') {
              // If the result of the refresh call is not authorized then the user needs to be logged out.
              this.LogOut();
            }
            observer.error('Could not get user.');
            // eslint-disable-next-line rxjs/no-redundant-notify
            observer.complete();
          } else {
            this.session = session;
            observer.next(this.session.getAccessToken().getJwtToken());
            observer.complete();
            this.onRefreshToken$.next(true);
          }
        });
      } else {
        throwError('no user');
      }
    });
  }

  public GetIdTokenOrWait() {
    return this.GetIdToken().pipe(
      catchError(() => {
        return this.loggedIn$.pipe(switchMap(() => this.GetIdToken()));
      })
    );
  }

  getHelpUrl() {
    return this.GetIdToken(false).pipe(
      take(1),
      map((token) => {
        return (
          this.appConfig.baseServicesURL +
          '/api/Account/Training?access_token=' +
          token
        );
      })
    );
  }

  invalidateTokenForTesting() {
    this.session = new cognito.CognitoUserSession({
      IdToken: new cognito.CognitoIdToken({ IdToken: 'InvalidIdToken' }),
      AccessToken: new cognito.CognitoAccessToken({
        AccessToken: 'InvalidAccessToken',
      }),
      RefreshToken: new cognito.CognitoRefreshToken({
        RefreshToken: 'InvalidRefreshToken',
      }),
    });
  }

  ConfirmPassword(email: string, password: string, code: string) {
    this.user = this.CreateUser(email);

    return new Observable<{ success: boolean; error: any }>((subscribe) => {
      this.user.confirmPassword(code, password, {
        onSuccess: () => {
          subscribe.next({ success: true, error: null });
          subscribe.complete();
        },
        onFailure: (err: Error) => {
          subscribe.next({ success: false, error: err });
          subscribe.complete();
        },
      });
    });
  }

  ResendConfirmationCode(email: string) {
    this.user = this.CreateUser(email);
    return new Observable<{ success: boolean; code?: string; error?: any }>(
      (subscribe) => {
        this.user.resendConfirmationCode((err, result) => {
          if (err) {
            subscribe.next({ success: false, error: err.message });
            subscribe.complete();
          } else {
            subscribe.next({ success: true });
            subscribe.complete();
          }
        });
      }
    );
  }

  VerifyEmail() {
    return new Observable<{ success: boolean; message: string }>(
      (subscribe) => {
        this.user.getAttributeVerificationCode('email', {
          onSuccess: () => {
            subscribe.next({ success: true, message: null });
            subscribe.complete();
          },
          onFailure: (err: Error) => {
            subscribe.next({ success: false, message: err.message });
            subscribe.complete();
          },
          inputVerificationCode: (data: string) => {
            subscribe.next({ success: true, message: data });
            subscribe.complete();
          },
        });
      }
    );
  }

  SubmitVerifyEmailCode(code: string) {
    return new Observable<boolean>((subscribe) => {
      this.user.verifyAttribute('email', code, {
        onSuccess: (success: string) => {
          subscribe.next(true);
          subscribe.complete();
        },
        onFailure: (error: any) => {
          subscribe.next(false);
          subscribe.complete();
        },
      });
    });
  }

  ForgotPassword(email: string) {
    this.user = this.CreateUser(email);

    return new Observable<{
      success: boolean;
      errorMessage?: string;
      errorCode?: string;
      attributeName?: string;
      deliveryMedium?: string;
      destination?: string;
    }>((subscribe) => {
      this.user.forgotPassword({
        onSuccess: (data: any) => {
          subscribe.next({ success: true, errorMessage: data });
          subscribe.complete();
        },
        onFailure: (err: any) => {
          subscribe.next({
            success: false,
            errorMessage: err.message,
            errorCode: err.code,
          });
          subscribe.complete();
        },
        inputVerificationCode: (data: {
          CodeDeliveryDetails: {
            AttributeName: string;
            DeliveryMedium: string;
            Destination: string;
          };
        }) => {
          subscribe.next({
            success: true,
            attributeName: data?.CodeDeliveryDetails?.AttributeName,
            deliveryMedium: data?.CodeDeliveryDetails?.DeliveryMedium,
            destination: data?.CodeDeliveryDetails?.Destination,
          });
          subscribe.complete();
        },
      });
    });
  }

  changePassword(oldPassword: string, newPassword: string) {
    return new Observable<any>((subscribe) => {
      this.user.changePassword(oldPassword, newPassword, (err, result) => {
        if (err) {
          subscribe.error(err);
          // eslint-disable-next-line rxjs/no-redundant-notify
          subscribe.complete();
        } else {
          subscribe.next();
          subscribe.complete();
        }
      });
    });
  }

  configureMFAWithSMS() {
    const totpMfaSettings = null;
    const smsMfaSettings = {
      PreferredMfa: true,
      Enabled: true,
    };

    return new Observable<any>((subscribe) => {
      this.user.enableMFA((err, result) => {
        if (err) {
          subscribe.error(err);
          // eslint-disable-next-line rxjs/no-redundant-notify
          subscribe.complete();
        } else {
          (<any>this.user).setUserMfaPreference(
            smsMfaSettings,
            totpMfaSettings,
            (err2, result) => {
              if (err2) {
                subscribe.error(err2);
                // eslint-disable-next-line rxjs/no-redundant-notify
                subscribe.complete();
              } else {
                subscribe.next();
                subscribe.complete();
              }
            }
          );
        }
      });
    });
  }

  disableMFA() {
    const smsMfaSettings = {
      Enabled: false,
      PreferredMfa: false,
    };
    const totpMfaSettings = {
      Enabled: false,
      PreferredMfa: false,
    };

    return new Observable<any>((subscribe) => {
      (<any>this.user).setUserMfaPreference(
        smsMfaSettings,
        totpMfaSettings,
        (err: { code: string; message: string; name: string }, result) => {
          if (err) {
            if (err.name == 'InvalidParameterException') {
              (<any>this.user).setUserMfaPreference(
                smsMfaSettings,
                null,
                (
                  err2: { code: string; message: string; name: string },
                  result
                ) => {
                  if (err2) {
                    subscribe.error(err2);
                    // eslint-disable-next-line rxjs/no-redundant-notify
                    subscribe.complete();
                  } else {
                    subscribe.next();
                    subscribe.complete();
                  }
                }
              );
            }
          } else {
            subscribe.next();
            subscribe.complete();
          }
        }
      );
    });
  }

  // Get Cognito Settings
  public getCognito(code?: string) {
    return this.authorizationFrameworkService.cognitoSettings(code).pipe(
      map((n) => {
        this.UserPoolID(n.PoolID);
        this.ClientID(n.ClientID);
        this.HostedUI(n.HostedUI);
        this.AuthFlowType(n.AuthFlowType);
        return n;
      }),
      switchMap((n) => this.GetSessionWithToken(n.Token)),
      switchMap(() => this.Initialize())
    );
  }

  // This method will tell the server that the user has been authenticated.
  // It will give the back end a chance to write an entry to the AccessControl.tAuthenticationHistory
  // table indicating that the user was authenticated and when.
  public userAuthenticated() {
    return this.accountFrameworkService.userAuthenticated().pipe(
      take(1),
      tap((n) => {
        this.logger.setUser(n.GlobalId, n.CustomerID);
      })
    );
  }

  public setPhoneNo(phoneNo: string) {
    const userAttribute = new cognito.CognitoUserAttribute({
      Name: 'phone_number',
      Value: phoneNo,
    });
    const attributeList = [];
    attributeList.push(userAttribute);

    return new Observable<{ success: boolean; error?: string }>((obs) => {
      this.user.updateAttributes(attributeList, (err, res) => {
        if (res.toLocaleLowerCase() === 'success') {
          obs.next({ success: true });
          obs.complete();
        } else {
          obs.error({ success: false, error: err.message });
          // eslint-disable-next-line rxjs/no-redundant-notify
          obs.complete();
        }
      });
    });
  }

  public removePhoneNo() {
    const userAttribute = new cognito.CognitoUserAttribute({
      Name: 'phone_number',
      Value: null,
    });
    const attributeList = [];
    attributeList.push(userAttribute);

    return new Observable<{ success: boolean; error?: string }>((obs) => {
      this.user.updateAttributes(attributeList, (err, res) => {
        if (res.toLocaleLowerCase() === 'success') {
          obs.next({ success: true });
          obs.complete();
        } else {
          obs.error({ success: false, error: err.message });
          // eslint-disable-next-line rxjs/no-redundant-notify
          obs.complete();
        }
      });
    });
  }

  public sendVerificationCode() {
    return new Observable<{ success: boolean; error?: string }>((obs) => {
      this.user.getAttributeVerificationCode('phone_number', {
        onSuccess: () => {
          obs.next({ success: true });
          obs.complete();
        },
        onFailure: (err) => {
          obs.error({ success: false, error: err.message });
          // eslint-disable-next-line rxjs/no-redundant-notify
          obs.complete();
        },
      });
    });
  }

  public verifyPhone(code: string) {
    return new Observable<{ success: boolean; error?: string }>((obs) => {
      this.user.verifyAttribute('phone_number', code, {
        onSuccess: () => {
          obs.next({ success: true });
          obs.complete();
        },
        onFailure: (err: Error) => {
          obs.error({ success: false, error: err.message });
          // eslint-disable-next-line rxjs/no-redundant-notify
          obs.complete();
        },
      });
    });
  }
}
