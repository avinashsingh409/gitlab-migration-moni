import { Injectable, Inject } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import {
  Observable,
  throwError as observableThrowError,
  Subject,
  BehaviorSubject,
} from 'rxjs';
import {
  catchError,
  map,
  take,
  switchMap,
  distinctUntilChanged,
} from 'rxjs/operators';
import { AuthService } from './auth.service';

export interface TokenState {
  token: string;
}

const _initialState: TokenState = {
  token: null,
};

let _state: TokenState = _initialState;
@Injectable({
  providedIn: 'root',
})
export class JwtInterceptorService {
  private store = new BehaviorSubject<TokenState>(_state);
  private state$ = this.store.asObservable();

  constructor(private authService: AuthService) {
    this.authService
      .GetIdTokenOrWait()
      .pipe(take(1))
      .subscribe((token) => {
        this.updateState({ ..._state, token });
      });
  }

  token$: Observable<string> = this.state$.pipe(
    map((state) => {
      return state.token || '';
    }),
    distinctUntilChanged()
  );

  private updateState(newState: TokenState) {
    this.store.next((_state = newState));
  }

  addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
    return req.clone({
      url: req.url,
      setHeaders: { Authorization: 'Bearer ' + token },
    });
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      req.headers.has('Anonymous') ||
      req.url.toLowerCase().startsWith('./assets/')
    ) {
      return next.handle(req);
    } else {
      return this.authService
        .GetIdTokenOrWait()
        .pipe(switchMap((n) => next.handle(this.addToken(req, n))))
        .pipe(
          catchError((error: unknown) => {
            if (error instanceof HttpErrorResponse) {
              switch ((error as HttpErrorResponse).status) {
                case 400:
                  return this.handle400Error(error);
                case 401:
                  // This means the token needs to be refreshed
                  return this.handle401Error(req, next);
                default:
                  return observableThrowError(error);
              }
            } else {
              return observableThrowError(error);
            }
          })
        );
    }
  }

  handle400Error(error: HttpErrorResponse) {
    if (
      error &&
      error.status === 400 &&
      error.error &&
      error.error.error === 'invalid_grant'
    ) {
      // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
      return this.logoutUser();
    }

    return observableThrowError(error);
  }

  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    this.authService.GetIdToken(true).subscribe(
      (token) => {
        this.updateState({ ..._state, token });
        return next.handle(this.addToken(req, token));
      },
      (error: unknown) => {
        this.updateState({ ..._state, token: null });
        // If there is an exception calling 'refreshToken', bad news so logout.
        this.logoutUser();
      }
    );
    return this.token$.pipe(
      switchMap((token) => {
        if (token) {
          return next.handle(this.addToken(req, token));
        } else {
          return observableThrowError('Invalid Token');
        }
      })
    );
  }

  logoutUser() {
    this.authService.LogOut();
    return observableThrowError('Http Error');
  }
}
