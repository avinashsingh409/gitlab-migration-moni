import { TestBed, inject } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { CognitoService } from './cognito.service';
import { render, screen } from '@testing-library/angular';
import {
  ICognitoUserPoolData,
  CognitoUserPool,
  CognitoUser,
  CognitoUserAttribute,
} from 'amazon-cognito-identity-js';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import {
  createMock,
  createMockWithValues,
  provideMock,
} from '@testing-library/angular/jest-utils';
import { eachValueFrom } from 'rxjs-for-await';

describe('AuthService', () => {
  describe('SimpleTests', () => {
    let service: AuthService;
    let cognitoServiceMock: CognitoService;
    let cognitoUserMock: CognitoUser;

    beforeEach(() => {
      cognitoUserMock = createMock(CognitoUser);
      cognitoServiceMock = createMockWithValues(CognitoService, {
        createUserPool: () => createMock(CognitoUserPool),
        getUserPoolID: () => 'testUserPool',
      });
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          AuthService,
          provideMock(CognitoService),
          provideMock(CognitoUserPool),
          { provide: CognitoUser, useValue: cognitoUserMock },
          {
            provide: CognitoService,
            useValue: cognitoServiceMock,
          },
          { provide: APP_CONFIG, useValue: AppConfig },
        ],
      });

      cognitoUserMock = TestBed.inject(CognitoUser);
      cognitoServiceMock = TestBed.inject(CognitoService);
      service = TestBed.inject(AuthService);
    });

    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('should get the user pool', () => {
      let val: string = null;
      cognitoServiceMock.getUserPoolID = jest.fn(() => val);
      let result = service.UserPoolID();
      expect(result).toEqual(val);

      val = 'something';
      cognitoServiceMock.getUserPoolID = jest.fn(() => val);
      result = service.UserPoolID();
      expect(result).toEqual(val);

      val = 'NewVal';
      cognitoServiceMock.getUserPoolID = jest.fn(() => val);
      result = service.UserPoolID();
      expect(result).toEqual(val);
    });

    it('should get the ClientID', () => {
      let val: string = null;
      cognitoServiceMock.getClientID = jest.fn(() => val);
      let result = service.ClientID();
      expect(result).toEqual(val);

      val = 'somthing';
      cognitoServiceMock.getClientID = jest.fn(() => val);
      result = service.ClientID();
      expect(result).toEqual(val);

      val = 'NewValue';
      cognitoServiceMock.getClientID = jest.fn(() => val);
      result = service.ClientID(val);
      expect(cognitoServiceMock.setClientID).toHaveBeenCalledWith(val);
      expect(result).toEqual(val);

      result = service.ClientID();
      expect(result).toEqual(val);
    });

    it('should populate the user with session storage', () => {
      const mockStorage = {};
      cognitoServiceMock.getSessionStorage = jest.fn(
        () => mockStorage as Storage
      );
      service.populateUser(true);

      expect(cognitoServiceMock.getSessionStorage).toHaveBeenCalled();
      expect(service.IsLoggedIn()).toEqual(false);
    });

    it('should log out a user', () => {
      service.LogOut();
      expect(service.IsLoggedIn()).toEqual(false);
    });
  });

  describe('HarderTests', () => {
    let service: AuthService;
    let cognitoServiceMock: CognitoService;
    let cognitoUserPoolMock: CognitoUserPool;
    let cognitoUserMock: CognitoUser;

    beforeEach(() => {
      cognitoUserMock = createMockWithValues(CognitoUser, {
        getUserAttributes: jest.fn((callback) => {
          const nameAttribute = new CognitoUserAttribute({
            Name: 'name',
            Value: 'josh',
          });
          const emailAttribute = new CognitoUserAttribute({
            Name: 'email',
            Value: 'josh@josh.josh',
          });
          const verifiedAttribute = new CognitoUserAttribute({
            Name: 'phone_number_verified',
            Value: 'true',
          });
          callback(null, [nameAttribute, emailAttribute, verifiedAttribute]);
        }),
      });

      cognitoUserPoolMock = createMockWithValues(CognitoUserPool, {
        getCurrentUser: () => cognitoUserMock,
      });

      cognitoServiceMock = createMockWithValues(CognitoService, {
        createUserPool: () => cognitoUserPoolMock,
        getUserPoolID: () => 'testUserPool',
      });
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          AuthService,
          { provide: CognitoUserPool, useValue: cognitoUserPoolMock },
          { provide: CognitoUser, useValue: cognitoUserMock },
          {
            provide: CognitoService,
            useValue: cognitoServiceMock,
          },
          { provide: APP_CONFIG, useValue: AppConfig },
        ],
      });
      cognitoUserPoolMock = TestBed.inject(CognitoUserPool);
      cognitoUserMock = TestBed.inject(CognitoUser);
      cognitoServiceMock = TestBed.inject(CognitoService);
      service = TestBed.inject(AuthService);
    });

    it('should get friendly name and email', async () => {
      let retVal = [];
      for await (const value of eachValueFrom(service.FriendlyName())) {
        retVal.push(value);
      }
      expect(retVal).toEqual(['josh']);
      retVal = [];
      for await (const value of eachValueFrom(service.Email())) {
        retVal.push(value);
      }
      expect(retVal).toEqual(['josh@josh.josh']);
    });

    it('should get phone number verified', async () => {
      const retVal = [];
      for await (const value of eachValueFrom(service.PhoneNumberVerified())) {
        retVal.push(value);
      }
      expect(retVal).toEqual([true]);
    });

    it('should log in the user with session storage', () => {
      const mockStorage = {};
      cognitoServiceMock.getSessionStorage = jest.fn(
        () => mockStorage as Storage
      );
      service.populateUser(true);
      expect(cognitoServiceMock.getSessionStorage).toHaveBeenCalled();
      expect(service.IsLoggedIn()).toEqual(true);
    });

    it('should get Friendly Name error', (done) => {
      cognitoUserMock.getUserAttributes = jest.fn((callback) => {
        const attribute = new CognitoUserAttribute({
          Name: 'name',
          Value: 'josh',
        });
        callback(
          { name: 'something', message: 'Something is wrong' } as Error,
          [attribute]
        );
      });
      service.FriendlyName().subscribe((value) => {
        expect(value).toEqual('');
        done();
      });
    });

    it('should get phone number error', (done) => {
      cognitoUserMock.getUserAttributes = jest.fn((callback) => {
        const attribute = new CognitoUserAttribute({
          Name: 'phone_number',
          Value: 'josh',
        });
        callback(
          { name: 'something', message: 'Something is wrong' } as Error,
          [attribute]
        );
      });

      service.PhoneNumber().subscribe((value) => {
        expect(value).toEqual('');
        done();
      });
    });

    it('should get phone number verified error', (done) => {
      cognitoUserMock.getUserAttributes = jest.fn((callback) => {
        const attribute = new CognitoUserAttribute({
          Name: 'phone_number_verified',
          Value: 'true',
        });
        callback(
          { name: 'something', message: 'Something is wrong' } as Error,
          [attribute]
        );
      });

      service.PhoneNumberVerified().subscribe((value) => {
        expect(value).toBeNull();
        done();
      });
    });
  });
});
