/* eslint-disable ngrx/good-action-hygiene */
import { IResourceAccessType } from '@atonix/atx-core';
import { createAction, props } from '@ngrx/store';
import { IAuthenticationResponse } from '../models/authentication-response';

export const authInitialize = createAction(
  '[Auth] Initialize',
  props<{ code: string; stateKey: string }>()
);

export const authenticatedSuccess = createAction(
  '[Auth] Authenticated Success'
);
export const authFailure = createAction(
  '[Auth] Auth Failure',
  props<{ message: string; code?: string; stateKey?: string }>()
);

export const totpCode = createAction(
  '[Auth] Totp Code',
  props<{ code: string }>()
);
export const totpCodeSuccess = createAction(
  '[Auth] Totp Code Success',
  props<{ response: IAuthenticationResponse }>()
);
export const totpCodeFailure = createAction(
  '[Auth] Totp Code Failure',
  props<{ error }>()
);

export const mfaCode = createAction(
  '[Auth] Mfa Code',
  props<{ code: string }>()
);
export const mfaCodeSuccess = createAction(
  '[Auth] Mfa Code Success',
  props<{ response: IAuthenticationResponse }>()
);
export const mfaCodeFailure = createAction(
  '[Auth] Mfa Code Failure',
  props<{ error }>()
);

export const initialPassword = createAction(
  '[Auth] Initial Password',
  props<{ password: string; requiredAttributes: any }>()
);
export const initialPasswordSuccess = createAction(
  '[Auth] Initial Password Success',
  props<{ response: IAuthenticationResponse }>()
);
export const initialPasswordFailure = createAction(
  '[Auth] Initial Password Failure',
  props<{ error }>()
);

export const passwordCode = createAction(
  '[Auth] Password Code',
  props<{ email: string; password: string; code: string }>()
);
export const passwordCodeSuccess = createAction(
  '[Auth] Password Code Success',
  props<{ success: boolean; error: any }>()
);
export const passwordCodeFailure = createAction(
  '[Auth] Password Code Failure',
  props<{ error }>()
);

export const forgotPasswordInit = createAction('[Auth] Forgot Password Init');
export const forgotPassword = createAction(
  '[Auth] Forgot Password',
  props<{ email: string }>()
);
export const forgotPasswordSuccess = createAction(
  '[Auth] Forgot Password Success',
  props<{
    success: boolean;
    errorMessage: string;
    email: string;
    attributeName: string;
    deliveryMedium: string;
    destination: string;
  }>()
);
export const forgotPasswordFailure = createAction(
  '[Auth] Forgot Password Failure',
  props<{ error }>()
);

export const cognitoUserNotFound = createAction(
  '[Auth] Cognito User Not Found',
  props<{ userEmail: string }>()
);

export const cognitoUserNotFoundSuccess = createAction(
  '[Auth] Cognito User Not Found Success',
  props<{ userEmail: string; apiResult: string }>()
);
export const cognitoUserNotFoundFailure = createAction(
  '[Auth] Cognito User Not Found Failure'
);

export const resetLogin = createAction('[Navigation Reset Login');

export const iHaveACode = createAction('[Auth] I Have A Code');

export const dismissVerifyEmailDialog = createAction(
  '[Auth] Dismiss Verify Email Dialog'
);
export const sendVerifyEmailMessage = createAction(
  '[Auth] Send Verify Email Message'
);
export const sendVerifyEmailMessageSuccess = createAction(
  '[Auth] Send Verify Email Message Success',
  props<{ success: boolean; message: string }>()
);
export const sendVerifyEmailMessageFailure = createAction(
  '[Auth] Send Verify Email Message Failure',
  props<{ error }>()
);
export const submitVerifyEmailCode = createAction(
  '[Auth] Submit Verify Email Code',
  props<{ code: string }>()
);
export const submitVerifyEmailCodeSuccess = createAction(
  '[Auth] Submit Verify Email Code Success'
);
export const submitVerifyEmailCodeFailure = createAction(
  '[Auth] Submit Verify Email Code Failure',
  props<{ error }>()
);

export const login = createAction(
  '[Auth] Login',
  props<{ username: string; password: string; rememberMe: boolean }>()
);
export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ response: IAuthenticationResponse }>()
);
export const loginFailed = createAction(
  '[Auth] Login Failed',
  props<{ error }>()
);

export const getIdentityProvider = createAction(
  '[Auth] Set Username',
  props<{ username: string }>()
);

export const getIdentityProviderReturn = createAction(
  '[Auth] Get Identity Provider Return',
  props<{ forwardUrl: string | null }>()
);

export const logout = createAction('[Auth] Logout');
export const tokenLogout = createAction('[Auth] Token Logout');
export const tokenLogoutNotNeeded = createAction(
  '[Auth] Token Logout Not Needed'
);
export const logoutComplete = createAction('[Auth] Logout Success');

// UI Access
export const getAllUIAccess = createAction('[Auth] Get All UI Access');

export const getAllUIAccessSuccess = createAction(
  '[Auth] Get All UI Access Success',
  props<{
    resourceAccessTypes: IResourceAccessType[];
  }>()
);
export const getAllUIAccessFailed = createAction(
  '[Auth] Get All UI Access Failed',
  props<{ error: any }>()
);

export const cancelTimerToRefreshUIAccess = createAction(
  '[Auth] Cancel timer to refresh UI Access'
);

export const startTimerToRefreshUIAccess = createAction(
  '[Auth] Start timer to refresh UI Access'
);
