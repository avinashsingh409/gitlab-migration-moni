import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from './auth.reducers';

export const selectAuthState = createFeatureSelector<AuthState>('auth');

export const isLoggedIn = createSelector(
  selectAuthState,
  (state) => state?.isLoggedIn
);

export const selectLoginState = createSelector(
  selectAuthState,
  (state) => state.loginState
);
export const selectLoginDisplay = createSelector(
  selectLoginState,
  (state) => state?.display ?? 'Waiting'
);

export const selectLoginShowError = createSelector(
  selectLoginState,
  (state) => state?.showError
);
export const selectLoginErrorMessage = createSelector(
  selectLoginState,
  (state) => state?.errorMessage
);
export const showLogin = createSelector(selectAuthState, (state) => {
  return state.showVerifyEmail || !state.isLoggedIn;
});

export const selectUIAccessLoaded = createSelector(
  selectAuthState,
  (state) => state?.uiAccessLoaded
);

export const selectCachedAuthValues = createSelector(
  selectAuthState,
  (state) => state?.cachedAuthValues
);

export const selectNavItemsAccess = createSelector(selectAuthState, (state) => {
  return {
    alertsAccess: state?.alertsTaskCenterAccess,
    issuesManagementAccess: state?.issuesManagementTaskCenterAccess,
    dataExplorerAccess: state?.dataExplorerTaskCenterAccess,
    dashboardsAccess: state?.dashboardsTaskCenterAccess,
    modelConfigAccess: state?.modelConfigUtilityAccess,
    opModelConfigAccess: state?.opModelConfigUtilityAccess,
    userAdminAccess: state?.userAdminUtilityAccess,
    pinConfigAccess: state?.pinConfigUtilityAccess,
    assetConfigAccess: state?.assetConfigUtilityAccess,
    tagConfigAccess: state?.tagConfigUtilityAccess,
    alphaAccess: state?.alphaFeatureAccess,
    alpha2Access: state?.alpha2FeatureAccess,
    beta2Access: state?.beta2FeatureAccess,
  };
});

export const selectMDAccess = createSelector(selectAuthState, (state) => {
  if (
    state?.alertsTaskCenterAccess?.CanView ||
    state?.issuesManagementTaskCenterAccess?.CanView ||
    state?.dataExplorerTaskCenterAccess?.CanView ||
    state?.dashboardsTaskCenterAccess?.CanView ||
    state?.modelConfigUtilityAccess?.CanView ||
    state?.opModelConfigUtilityAccess?.CanView ||
    state?.userAdminUtilityAccess?.CanView ||
    state?.tagConfigUtilityAccess?.CanView ||
    state?.assetConfigUtilityAccess?.CanView
  ) {
    return true;
  } else {
    return false;
  }
});

export const selectEventsAccess = createSelector(selectAuthState, (state) => {
  if (state?.eventsUtilityAccess?.CanView) {
    return true;
  } else {
    return false;
  }
});

export const selectAEAccess = createSelector(selectAuthState, (state) => {
  if (state?.assetConfigUtilityAccess?.CanView) {
    return true;
  } else {
    return false;
  }
});

// Task Centers Access
export const selectAlertsTaskCenterAccess = createSelector(
  selectAuthState,
  (state) => state?.alertsTaskCenterAccess
);

export const selectIssuesManagementTaskCenterAccess = createSelector(
  selectAuthState,
  (state) => state?.issuesManagementTaskCenterAccess
);

export const selectIssuesManagementDiscussionAccess = createSelector(
  selectAuthState,
  (state) => state?.issuesManagementDiscussionAccess
);

export const selectDataExplorerTaskCenterAccess = createSelector(
  selectAuthState,
  (state) => state?.dataExplorerTaskCenterAccess
);

export const selectDashboardsTaskCenterAccess = createSelector(
  selectAuthState,
  (state) => state?.dashboardsTaskCenterAccess
);

// Utilities Access
export const selectUserAdminUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.userAdminUtilityAccess
);

export const selectCustMgmtUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.custMgmtUtilityAccess
);

export const selectPinConfigUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.pinConfigUtilityAccess
);

export const selectModelConfigUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.modelConfigUtilityAccess
);

export const selectOPModeConfigUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.opModelConfigUtilityAccess
);

export const selectEventsUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.eventsUtilityAccess
);

export const selectAssetConfigUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.assetConfigUtilityAccess
);

export const selectTagConfigUtilityAccess = createSelector(
  selectAuthState,
  (state) => state?.tagConfigUtilityAccess
);

//Feature Access
export const selectAlphaFeatureAccess = createSelector(
  selectAuthState,
  (state) => state?.alphaFeatureAccess
);

export const selectBetaFeatureAccess = createSelector(
  selectAuthState,
  (state) => state?.betaFeatureAccess
);

export const selectAlpha2FeatureAccess = createSelector(
  selectAuthState,
  (state) => state?.alpha2FeatureAccess
);

export const selectBeta2FeatureAccess = createSelector(
  selectAuthState,
  (state) => state?.beta2FeatureAccess
);

export const selectNewAssetConfigUtilityAccess = createSelector(
  selectAssetConfigUtilityAccess,
  selectTagConfigUtilityAccess,
  (assetConfigAccess, tagConfigAccess) =>
    assetConfigAccess?.CanView || tagConfigAccess?.CanView
);
