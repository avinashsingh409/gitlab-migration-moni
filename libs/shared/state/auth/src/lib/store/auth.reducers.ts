/* eslint-disable ngrx/on-function-explicit-return-type */
import { IResourceAccessType, ISecurityRights } from '@atonix/atx-core';
import { processResourceAccessType } from '@atonix/shared/utils';
import { createReducer, on } from '@ngrx/store';
import { isEqual } from 'lodash';
import { AtonixAuthenticationConstants } from '../models/authentication-constants';
import * as actions from './auth.actions';
export type LoginStateType =
  | 'Username'
  | 'Password'
  | 'Waiting'
  | 'NewPassword'
  | 'MFA'
  | 'TOTP'
  | 'CustomChallenge'
  | 'MFASetup'
  | 'SelectMFAType'
  | 'PasswordCode'
  | 'ForgotPassword'
  | 'ConfirmEmail';

export interface ILoginState {
  display: LoginStateType;
  showError: boolean;
  errorMessage: string;
  username?: string;
  challengeName?: string;
  challengeParameters?: any;
  userAttributes?: any;
  requiredAttributes?: any;
  attributeName?: string;
  deliveryMedium?: string;
  destination?: string;
}

export interface AuthState {
  loginState: ILoginState | null;
  showVerifyEmail: boolean;
  isLoggedIn: boolean;
  cachedAuthValues: IResourceAccessType[] | null;
  uiAccessLoaded: boolean;
  // Task Centers Access
  alertsTaskCenterAccess: ISecurityRights;
  issuesManagementTaskCenterAccess: ISecurityRights;
  issuesManagementDiscussionAccess: ISecurityRights;
  dataExplorerTaskCenterAccess: ISecurityRights;
  dashboardsTaskCenterAccess: ISecurityRights;

  // Utilities
  modelConfigUtilityAccess: ISecurityRights;
  opModelConfigUtilityAccess: ISecurityRights;
  userAdminUtilityAccess: ISecurityRights;
  custMgmtUtilityAccess: ISecurityRights;
  pinConfigUtilityAccess: ISecurityRights;
  assetConfigUtilityAccess: ISecurityRights;
  tagConfigUtilityAccess: ISecurityRights;
  eventsUtilityAccess: ISecurityRights;

  // Feature
  alphaFeatureAccess: ISecurityRights;
  betaFeatureAccess: ISecurityRights;
  alpha2FeatureAccess: ISecurityRights;
  beta2FeatureAccess: ISecurityRights;
}

export function initialAuthState(): AuthState {
  return {
    loginState: null,
    showVerifyEmail: false,
    isLoggedIn: false,
    uiAccessLoaded: false,
    cachedAuthValues: null,
    alertsTaskCenterAccess: null,
    issuesManagementTaskCenterAccess: null,
    issuesManagementDiscussionAccess: null,
    dataExplorerTaskCenterAccess: null,
    dashboardsTaskCenterAccess: null,
    modelConfigUtilityAccess: null,
    opModelConfigUtilityAccess: null,
    userAdminUtilityAccess: null,
    custMgmtUtilityAccess: null,
    pinConfigUtilityAccess: null,
    assetConfigUtilityAccess: null,
    tagConfigUtilityAccess: null,
    eventsUtilityAccess: null,
    alphaFeatureAccess: null,
    betaFeatureAccess: null,
    alpha2FeatureAccess: null,
    beta2FeatureAccess: null,
  };
}

const _authReducer = createReducer<AuthState>(
  initialAuthState(),
  on(actions.authenticatedSuccess, (state, action) => {
    return { ...state, isLoggedIn: true };
  }),
  on(actions.logout, (state, action) => {
    return { ...state, isLoggedIn: false };
  }),
  on(actions.authFailure, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      username: '',
      showError: true,
      errorMessage: action.message,
    };
    return { ...state, loginState };
  }),
  on(actions.getIdentityProvider, (state, action) => {
    const loginState: ILoginState = {
      display: 'Waiting',
      username: action.username,
      showError: false,
      errorMessage: '',
    };
    return { ...state, loginState };
  }),
  on(actions.getIdentityProviderReturn, (state, action) => {
    if (action.forwardUrl !== '') {
      window.location.href = action.forwardUrl;
      return { ...state };
    } else {
      return {
        ...state,
        loginState: {
          ...state.loginState,
          showError: false,
          errorMessage: '',
          display: 'Password',
        },
      };
    }
  }),
  on(actions.login, (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.loginSuccess, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };

    if (
      action.response.Status === AtonixAuthenticationConstants.LoginSuccessful
    ) {
      loginState.display = action.response.EmailConfirmed
        ? 'Username'
        : 'ConfirmEmail';
      loginState.showError = false;
      loginState.errorMessage = null;
      state = {
        ...state,
        isLoggedIn: true,
        showVerifyEmail: !action.response.EmailConfirmed,
      };
    } else if (
      action.response.Status === AtonixAuthenticationConstants.LoginFailed
    ) {
      loginState.display = 'Username';
      loginState.showError = true;
      loginState.errorMessage = 'Login Failed';
    } else if (
      action.response.Status ===
      AtonixAuthenticationConstants.PasswordChangeRequired
    ) {
      loginState.display = 'NewPassword';
      loginState.userAttributes = action.response.UserAttributes;
      loginState.requiredAttributes = action.response.RequiredAttributes;
    } else if (
      action.response.Status === AtonixAuthenticationConstants.MFARequired
    ) {
      loginState.display = 'MFA';
      loginState.challengeName = action.response.ChallengeName;
      loginState.challengeParameters = action.response.ChallengeParameters;
    } else if (
      action.response.Status === AtonixAuthenticationConstants.TotpRequired
    ) {
      loginState.display = 'TOTP';
    } else if (
      action.response.Status === AtonixAuthenticationConstants.CustomChallenge
    ) {
      loginState.display = 'CustomChallenge';
    } else if (
      action.response.Status === AtonixAuthenticationConstants.MfaSetup
    ) {
      loginState.display = 'MFASetup';
    } else if (
      action.response.Status === AtonixAuthenticationConstants.SelectMfaType
    ) {
      loginState.display = 'SelectMFAType';
    } else if (
      action.response.Status ===
      AtonixAuthenticationConstants.InitialPasswordRequired
    ) {
      loginState.display = 'PasswordCode';
      loginState.userAttributes = action.response.UserAttributes;
      loginState.requiredAttributes = action.response.RequiredAttributes;
    }

    return { ...state, loginState };
  }),
  on(actions.loginFailed, (state, action) => {
    let loginState: ILoginState = state.loginState || {
      display: 'Username',
      showError: true,
      errorMessage: null,
    };
    loginState = {
      ...loginState,
      display: 'Username',
      showError: true,
      errorMessage: 'An error occurred while authenticating.',
    };
    return { ...state, loginState };
  }),
  on(actions.resetLogin, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      username: '',
      showError: false,
      errorMessage: '',
    };
    return { ...state, loginState };
  }),
  on(actions.totpCode, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.totpCodeSuccess, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };
    if (
      action.response.Status === AtonixAuthenticationConstants.LoginSuccessful
    ) {
      loginState.display = 'Username';
      loginState.showError = false;
      loginState.errorMessage = null;
      state = { ...state, isLoggedIn: true };
    } else if (
      action.response.Status === AtonixAuthenticationConstants.LoginFailed
    ) {
      loginState.display = 'TOTP';
      loginState.showError = true;
      loginState.errorMessage = 'Code Failed';
    } else if (
      action.response.Status ===
      AtonixAuthenticationConstants.PasswordChangeRequired
    ) {
      loginState.display = 'NewPassword';
    }

    return { ...state, loginState };
  }),
  on(actions.totpCodeFailure, (state, action) => {
    let loginState: ILoginState = state.loginState || {
      display: 'Username',
      showError: true,
      errorMessage: null,
    };
    loginState = {
      ...loginState,
      display: 'Username',
      showError: true,
      errorMessage: 'An error occurred while sending code.',
    };
    return { ...state, loginState };
  }),
  on(actions.mfaCode, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.mfaCodeSuccess, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };
    if (
      action.response.Status === AtonixAuthenticationConstants.LoginSuccessful
    ) {
      loginState.display = 'Username';
      loginState.showError = false;
      loginState.errorMessage = null;
      state = { ...state, isLoggedIn: true };
    } else if (
      action.response.Status === AtonixAuthenticationConstants.LoginFailed
    ) {
      loginState.display = 'MFA';
      loginState.showError = true;
      loginState.errorMessage = 'Code Failed';
    } else if (
      action.response.Status ===
      AtonixAuthenticationConstants.PasswordChangeRequired
    ) {
      loginState.display = 'NewPassword';
    }

    return { ...state, loginState };
  }),
  on(actions.mfaCodeFailure, (state, action) => {
    let loginState: ILoginState = state.loginState || {
      display: 'Username',
      showError: true,
      errorMessage: null,
    };
    loginState = {
      ...loginState,
      display: 'Username',
      showError: true,
      errorMessage: 'An error occurred while sending code.',
    };
    return { ...state, loginState };
  }),
  on(actions.passwordCode, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.passwordCodeSuccess, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };
    if (action.success) {
      loginState.errorMessage =
        'Password reset successful! Please sign in with your new password';
    } else {
      loginState.display = 'PasswordCode';
      loginState.showError = true;
      loginState.errorMessage = 'Setting Password Failed';
    }
    return { ...state, loginState };
  }),
  on(actions.passwordCodeFailure, (state, action) => {
    let loginState: ILoginState = state.loginState || {
      display: 'Username',
      showError: true,
      errorMessage: null,
    };
    loginState = {
      ...loginState,
      showError: true,
      errorMessage: 'An error occurred while setting password.',
    };
    return { ...state, loginState };
  }),
  on(actions.initialPassword, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.initialPasswordSuccess, (state, action) => {
    const loginState: ILoginState = {
      display: 'Username',
      showError: false,
      errorMessage: '',
    };
    if (
      action.response.Status === AtonixAuthenticationConstants.LoginSuccessful
    ) {
      loginState.display = 'Username';
      loginState.showError = false;
      loginState.errorMessage = null;
      state = { ...state, isLoggedIn: true };
    } else if (
      action.response.Status === AtonixAuthenticationConstants.LoginFailed
    ) {
      loginState.display = 'NewPassword';
      loginState.showError = true;
      loginState.errorMessage = 'Setting Password Failed';
    } else if (
      action.response.Status === AtonixAuthenticationConstants.MFARequired
    ) {
      loginState.display = 'MFA';
      loginState.challengeName = action.response.ChallengeName;
      loginState.challengeParameters = action.response.ChallengeParameters;
    } else if (
      action.response.Status === AtonixAuthenticationConstants.CustomChallenge
    ) {
      loginState.display = 'CustomChallenge';
    } else if (
      action.response.Status === AtonixAuthenticationConstants.MfaSetup
    ) {
      loginState.display = 'MFASetup';
    }
    return { ...state, loginState };
  }),
  on(actions.initialPasswordFailure, (state, action) => {
    let loginState: ILoginState = state.loginState || {
      display: 'Username',
      showError: true,
      errorMessage: null,
    };
    loginState = {
      ...loginState,
      display: 'Username',
      showError: true,
      errorMessage: 'An error occurred while initializing account.',
    };
    return { ...state, loginState };
  }),
  on(actions.forgotPasswordInit, (state, action) => {
    return {
      ...state,
      loginState: {
        display: 'ForgotPassword',
        showError: false,
        errorMessage: '',
      },
    };
  }),
  on(actions.forgotPassword, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.forgotPasswordSuccess, (state, action) => {
    const loginState = {
      ...state.loginState,
      attributeName: action.attributeName,
      deliveryMedium: action.deliveryMedium,
      destination: action.destination,
    };
    if (action.success) {
      loginState.display = 'PasswordCode';
      loginState.showError = true;
      loginState.errorMessage = `A recovery code has been sent to your ${action.deliveryMedium} (${action.destination}) `;
    } else {
      loginState.display = 'Username';
      loginState.showError = true;
      loginState.errorMessage =
        action.errorMessage || 'A code to verify your email has been sent';
    }
    return {
      ...state,
      loginState,
    };
  }),
  on(actions.forgotPasswordFailure, (state, action) => {
    return {
      ...state,
      loginState: {
        ...state.loginState,
        display: 'Username',
        showError: true,
        errorMessage: 'Failed to reset password',
      },
    };
  }),
  on(actions.cognitoUserNotFoundSuccess, (state, action) => {
    const loginState = {
      ...state.loginState,
      attributeName: 'email',
      deliveryMedium: 'email',
      destination: action.userEmail,
    };
    if (action.apiResult) {
      if (action.apiResult === 'NoUser') {
        loginState.display = 'Username';
        loginState.showError = true;
        loginState.errorMessage = `Could not reset password.`;
      } else if (action.apiResult === 'UserExists') {
        loginState.display = 'Username';
        loginState.showError = true;
        loginState.errorMessage = `Could not reset password.`;
      } else {
        loginState.display = 'Username';
        loginState.showError = true;
        loginState.errorMessage = `Could not reset password.`;
      }
    } else {
      loginState.display = 'Username';
      loginState.showError = true;
      loginState.errorMessage = `A recovery code has been sent to ${action.userEmail}.`;
    }
    return {
      ...state,
      loginState,
    };
  }),
  on(actions.cognitoUserNotFoundFailure, (state, action) => {
    return {
      ...state,
      loginState: {
        ...state.loginState,
        display: 'Username',
        showError: true,
        errorMessage: 'Failed to reset password',
      },
    };
  }),
  on(actions.iHaveACode, (state, action) => {
    const loginState: ILoginState = {
      ...state.loginState,
      display: 'PasswordCode',
      showError: false,
      errorMessage: null,
    } as ILoginState;
    return { ...state, loginState };
  }),
  on(actions.dismissVerifyEmailDialog, (state, action) => {
    return { ...state, showVerifyEmail: false };
  }),
  on(actions.submitVerifyEmailCode, (state, action) => {
    return {
      ...state,
      loginState: { ...state.loginState, display: 'Waiting' },
    };
  }),
  on(actions.submitVerifyEmailCodeSuccess, (state, action) => {
    return { ...state, showVerifyEmail: false };
  }),
  on(actions.submitVerifyEmailCodeFailure, (state, action) => {
    return {
      ...state,
      loginState: {
        ...state.loginState,
        display: 'ConfirmEmail',
        showError: true,
        errorMessage: 'Error confirming email.',
      },
    };
  }),
  on(actions.getAllUIAccessSuccess, (state, action) => {
    if (isEqual(state.cachedAuthValues, action.resourceAccessTypes)) {
      return state;
    }

    return {
      ...state,
      cachedAuthValues: action.resourceAccessTypes,
      uiAccessLoaded: true,
      alertsTaskCenterAccess: processResourceAccessType(
        'UI/TaskCenter/Alerts',
        action.resourceAccessTypes
      ),
      issuesManagementTaskCenterAccess: processResourceAccessType(
        'UI/TaskCenter/IssuesManagement',
        action.resourceAccessTypes
      ),
      issuesManagementDiscussionAccess: processResourceAccessType(
        'UI/TaskCenter/IssuesManagement/Discussion',
        action.resourceAccessTypes
      ),
      dataExplorerTaskCenterAccess: processResourceAccessType(
        'UI/TaskCenter/DataExplorer',
        action.resourceAccessTypes
      ),
      dashboardsTaskCenterAccess: processResourceAccessType(
        'UI/TaskCenter/Dashboard',
        action.resourceAccessTypes
      ),
      modelConfigUtilityAccess: processResourceAccessType(
        'UI/Utility/ModelConfig',
        action.resourceAccessTypes
      ),
      opModelConfigUtilityAccess: processResourceAccessType(
        'UI/Utility/OpModeConfig',
        action.resourceAccessTypes
      ),
      userAdminUtilityAccess: processResourceAccessType(
        'UI/Utility/UserAdmin',
        action.resourceAccessTypes
      ),
      custMgmtUtilityAccess: processResourceAccessType(
        'UI/Utility/UserAdmin/CustomerManagement',
        action.resourceAccessTypes
      ),
      pinConfigUtilityAccess: processResourceAccessType(
        'UI/Utility/PinConfig',
        action.resourceAccessTypes
      ),
      assetConfigUtilityAccess: processResourceAccessType(
        'UI/Utility/AssetConfig',
        action.resourceAccessTypes
      ),
      tagConfigUtilityAccess: processResourceAccessType(
        'UI/Utility/TagConfig',
        action.resourceAccessTypes
      ),
      eventsUtilityAccess: processResourceAccessType(
        'UI/Utility/Events',
        action.resourceAccessTypes
      ),
      alphaFeatureAccess: processResourceAccessType(
        'UI/Feature/Alpha',
        action.resourceAccessTypes
      ),
      betaFeatureAccess: processResourceAccessType(
        'UI/Feature/Beta',
        action.resourceAccessTypes
      ),
      alpha2FeatureAccess: processResourceAccessType(
        'UI/Feature/Alpha2',
        action.resourceAccessTypes
      ),
      beta2FeatureAccess: processResourceAccessType(
        'UI/Feature/Beta2',
        action.resourceAccessTypes
      ),
    };
  })
);

export function authReducer(state, action): AuthState {
  return _authReducer(state, action);
}
