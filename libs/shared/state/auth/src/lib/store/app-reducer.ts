import { Action, ActionReducer } from '@ngrx/store';
import * as AuthActions from './auth.actions';

export function logoutMetareducer(reducer: ActionReducer<any>) {
  return function (state: any, action: Action) {
    if (action.type === AuthActions.logoutComplete.type) {
      return reducer(undefined, action);
    }
    return reducer(state, action);
  };
}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface State {}
export function loggingMetareducer(
  reducer: ActionReducer<State>
): ActionReducer<State> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    console.groupEnd();
    return result;
  };
}
