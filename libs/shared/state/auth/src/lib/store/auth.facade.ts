/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AuthState } from './auth.reducers';
import * as AuthActions from '../store/auth.actions';
import * as AuthSelectors from '../store/auth.selectors';

@Injectable({
  providedIn: 'root',
})
export class AuthFacade {
  constructor(private store: Store<AuthState>) {}
  authState$ = this.store.pipe(select(AuthSelectors.selectAuthState));
  isLoggedIn$ = this.store.pipe(select(AuthSelectors.isLoggedIn));
  loginState$ = this.store.pipe(select(AuthSelectors.selectLoginState));
  loginDisplay$ = this.store.pipe(select(AuthSelectors.selectLoginDisplay));
  loginShowError$ = this.store.pipe(select(AuthSelectors.selectLoginShowError));
  loginErrorMessage$ = this.store.pipe(
    select(AuthSelectors.selectLoginErrorMessage)
  );
  showLogin$ = this.store.pipe(select(AuthSelectors.showLogin));

  uiAccessLoaded$ = this.store.pipe(select(AuthSelectors.selectUIAccessLoaded));
  navItemsAccess$ = this.store.pipe(select(AuthSelectors.selectNavItemsAccess));

  // App Access
  hasMDAccess$ = this.store.pipe(select(AuthSelectors.selectMDAccess));
  hasEventsAccess$ = this.store.pipe(select(AuthSelectors.selectEventsAccess));
  hasAEAccess$ = this.store.pipe(select(AuthSelectors.selectAEAccess));

  // Task Centers Access
  alertsTaskCenterAccess$ = this.store.pipe(
    select(AuthSelectors.selectAlertsTaskCenterAccess)
  );
  issuesManagementTaskCenterAccess$ = this.store.pipe(
    select(AuthSelectors.selectIssuesManagementTaskCenterAccess)
  );
  issuesManagementDiscussionAccess$ = this.store.pipe(
    select(AuthSelectors.selectIssuesManagementDiscussionAccess)
  );
  dataExplorerTaskCenterAccess$ = this.store.pipe(
    select(AuthSelectors.selectDataExplorerTaskCenterAccess)
  );
  dashboardsTaskCenterAccess$ = this.store.pipe(
    select(AuthSelectors.selectDashboardsTaskCenterAccess)
  );

  // Utilities Access
  userAdminUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectUserAdminUtilityAccess)
  );

  custMgmtUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectCustMgmtUtilityAccess)
  );

  pinConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectPinConfigUtilityAccess)
  );
  modelConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectModelConfigUtilityAccess)
  );
  opModeConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectOPModeConfigUtilityAccess)
  );
  eventsUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectEventsUtilityAccess)
  );
  assetConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectAssetConfigUtilityAccess)
  );
  tagConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectTagConfigUtilityAccess)
  );
  newAssetConfigUtilityAccess$ = this.store.pipe(
    select(AuthSelectors.selectNewAssetConfigUtilityAccess)
  );

  // Feature Access/
  alphaFeatureAccess$ = this.store.pipe(
    select(AuthSelectors.selectAlphaFeatureAccess)
  );
  betaFeatureAccess$ = this.store.pipe(
    select(AuthSelectors.selectBetaFeatureAccess)
  );

  alpha2FeatureAccess$ = this.store.pipe(
    select(AuthSelectors.selectAlpha2FeatureAccess)
  );
  beta2FeatureAccess$ = this.store.pipe(
    select(AuthSelectors.selectBeta2FeatureAccess)
  );

  getAllUIAccess() {
    this.store.dispatch(AuthActions.getAllUIAccess());
  }

  setUserName(username: string) {
    this.store.dispatch(AuthActions.getIdentityProvider({ username }));
  }
  login(username: string, password: string, rememberMe: boolean) {
    this.store.dispatch(AuthActions.login({ username, password, rememberMe }));
  }
  logout() {
    this.store.dispatch(AuthActions.logout());
  }
  tokenLogout() {
    this.store.dispatch(AuthActions.tokenLogout());
  }

  authInit(code: string, stateKey: string) {
    this.store.dispatch(AuthActions.authInitialize({ code, stateKey }));
  }

  resetLogin() {
    this.store.dispatch(AuthActions.resetLogin());
  }

  totpCode(code: string) {
    this.store.dispatch(AuthActions.totpCode({ code }));
  }

  mfaCode(code: string) {
    this.store.dispatch(AuthActions.mfaCode({ code }));
  }

  initialPassword(password: string, requiredAttributes: any) {
    this.store.dispatch(
      AuthActions.initialPassword({ password, requiredAttributes })
    );
  }

  passwordCode(email: string, password: string, code: string) {
    this.store.dispatch(AuthActions.passwordCode({ email, password, code }));
  }

  forgotPasswordInit() {
    this.store.dispatch(AuthActions.forgotPasswordInit());
  }

  forgotPassword(email: string) {
    this.store.dispatch(AuthActions.forgotPassword({ email }));
  }

  iHaveACode() {
    this.store.dispatch(AuthActions.iHaveACode());
  }

  dismissVerifyEmailDialog() {
    this.store.dispatch(AuthActions.dismissVerifyEmailDialog());
  }

  sendVerifyEmailMessage() {
    this.store.dispatch(AuthActions.sendVerifyEmailMessage());
  }

  submitVerifyEmailCode(code: string) {
    this.store.dispatch(AuthActions.submitVerifyEmailCode({ code }));
  }
  //   userAuthenticated(isAuthenticated: boolean) {
  //     this.store.dispatch(AuthActions.userAuthenticated({ isAuthenticated }));
  //   }
}
