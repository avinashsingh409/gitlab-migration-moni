/* eslint-disable rxjs/no-implicit-any-catch */
/* eslint-disable rxjs/no-unsafe-catch */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/select-style */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AssetTreeDBService } from '@atonix/atx-indexed-db';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { of, timer } from 'rxjs';
import {
  catchError,
  switchMap,
  map,
  mergeMap,
  tap,
  take,
  filter,
  withLatestFrom,
  takeUntil,
  delay,
} from 'rxjs/operators';
import { AtonixAuthenticationConstants } from '../models/authentication-constants';
import { AuthService } from '../services/auth.service';
import { CognitoService } from '../services/cognito.service';
import * as AuthActions from './auth.actions';
import * as AuthSelectors from './auth.selectors';
import {
  AccountFrameworkService,
  AuthorizationFrameworkService,
} from '@atonix/shared/api';
import { IResourceAccessType } from '@atonix/atx-core';
import moment from 'moment';
import { randomStringGenerator, ToastService } from '@atonix/shared/utils';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private store: Store<any>,
    private authService: AuthService,
    private authFrameworkService: AuthorizationFrameworkService,
    private cognitoService: CognitoService,
    private assetTreeDBService: AssetTreeDBService,
    private accountFrameworkService: AccountFrameworkService,
    private snackBarService: ToastService
  ) {}

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      switchMap(() => {
        return this.authService.LogOut().pipe(
          map(() => AuthActions.logoutComplete()),
          catchError(() => of(AuthActions.logoutComplete()))
        );
      })
    )
  );

  tokenLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.tokenLogout),
      withLatestFrom(this.store.pipe(select(AuthSelectors.isLoggedIn))),
      switchMap(([action, isLoggedIn]) => {
        if (isLoggedIn) {
          return this.authService.LogOut().pipe(
            map(() => AuthActions.logoutComplete()),
            catchError(() => of(AuthActions.logoutComplete()))
          );
        } else {
          return of(AuthActions.tokenLogoutNotNeeded());
        }
      })
    )
  );

  logoutSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.logoutComplete),
        tap(() => {
          this.assetTreeDBService
            .clearIndxDBData()
            .pipe(take(1))
            .subscribe((val) => {
              console.log(`indexDB cleared ${val}`);
              this.router.navigate(['']);
            });
        })
      ),
    { dispatch: false }
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      switchMap((n) =>
        this.authService.Login(n.username, n.password, n.rememberMe).pipe(
          map((response) => AuthActions.loginSuccess({ response })),
          catchError((error) => of(AuthActions.loginFailed({ error })))
        )
      )
    )
  );

  loginComplete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        AuthActions.loginSuccess,
        AuthActions.totpCodeSuccess,
        AuthActions.mfaCodeSuccess,
        AuthActions.initialPasswordSuccess
      ),
      filter(
        (n) =>
          n.response.Status === AtonixAuthenticationConstants.LoginSuccessful
      ),
      switchMap((n) => {
        return this.authService.userAuthenticated().pipe(
          map(() => AuthActions.authInitialize({ code: '', stateKey: '' })),
          catchError(() =>
            of(AuthActions.authInitialize({ code: '', stateKey: '' }))
          )
        );
      })
    )
  );

  getAllUIAccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.getAllUIAccess),
      switchMap((action) => this.authFrameworkService.getAllUIAccess()),
      switchMap((resourceAccessTypes) => [
        AuthActions.getAllUIAccessSuccess({
          resourceAccessTypes,
        }),
      ]),
      catchError((error) => {
        console.log(JSON.stringify(error));
        return of(AuthActions.getAllUIAccessFailed({ error }));
      })
    )
  );

  getAllUIAccessSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.getAllUIAccessSuccess),
      switchMap(() => {
        return [
          AuthActions.cancelTimerToRefreshUIAccess(),
          AuthActions.startTimerToRefreshUIAccess(),
        ];
      })
    )
  );

  startTimerToRefreshUIAccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.startTimerToRefreshUIAccess),
      switchMap(() => {
        return timer(900000).pipe(
          switchMap(() => [AuthActions.getAllUIAccess()]),
          takeUntil(
            this.actions$.pipe(ofType(AuthActions.cancelTimerToRefreshUIAccess))
          )
        );
      })
    )
  );

  initializeNav$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.authInitialize),
      map((action) => {
        const code = action.code;
        const stateKey = action.stateKey;
        return { code, stateKey, valid: null };
      }),
      mergeMap((params) =>
        this.authService.getCognito(params.code).pipe(
          map((valid) => {
            // console.log(`valid ${JSON.stringify(valid)}`);
            return { ...params, valid };
          })
        )
      ),
      switchMap((params) => {
        if (params.valid) {
          if (params.stateKey) {
            // This will get the state on the session storage
            const stateObj = this.cognitoService.getState()
              ? JSON.parse(this.cognitoService.getState())
              : null;
            // This will get the redirect url and created date of the state key given
            const redirectURL = stateObj[params.stateKey]?.redirectURL;
            const createdDate = stateObj[params.stateKey]?.createdDate;
            // This will check if the state is more than 5 minutes(TBD). If true, state is expired.
            const isExpired = createdDate
              ? moment().toDate().getTime() -
                  moment(createdDate).toDate().getTime() >
                300000
              : true;
            // Delete state on the session storage
            this.cognitoService.removeState();

            if (redirectURL && !isExpired) {
              window.location.href = redirectURL;
            } else {
              return [
                AuthActions.authFailure({
                  message: '',
                  code: params.code,
                  stateKey: params.stateKey,
                }),
              ];
            }
          }

          return [AuthActions.authenticatedSuccess()];
        } else {
          return [
            AuthActions.authFailure({
              message: '',
              code: params.code,
              stateKey: params.stateKey,
            }),
          ];
        }
      }),
      catchError((error) => {
        this.router.navigate(['']);
        return of(AuthActions.authFailure({ message: error.message }));
      })
    )
  );

  authFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.authFailure),
        map((action) => {
          if (action.code) {
            this.router.navigate([], {
              queryParams: {
                code: null,
              },
              queryParamsHandling: 'merge',
            });
          }

          if (action.stateKey) {
            this.router.navigate([], {
              queryParams: {
                state: null,
              },
              queryParamsHandling: 'merge',
            });
          }
        })
      ),
    {
      dispatch: false,
    }
  );

  totpCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.totpCode),
      map((n) => n.code),
      switchMap((code) =>
        this.authService.SendTOTPCode(code).pipe(
          map((response) => AuthActions.totpCodeSuccess({ response })),
          catchError((error) => of(AuthActions.totpCodeFailure({ error })))
        )
      )
    )
  );
  mfaCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.mfaCode),
      map((n) => n.code),
      switchMap((code) =>
        this.authService.SendMFACode(code).pipe(
          map((response) => AuthActions.mfaCodeSuccess({ response })),
          catchError((error) => of(AuthActions.mfaCodeFailure({ error })))
        )
      )
    )
  );

  initialPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.initialPassword),
      switchMap((action) =>
        this.authService
          .InitialPassword(action.password, action.requiredAttributes)
          .pipe(
            map((response) => AuthActions.initialPasswordSuccess({ response })),
            catchError((error) =>
              of(AuthActions.initialPasswordFailure({ error }))
            )
          )
      )
    )
  );

  passwordCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.passwordCode),
      switchMap((action) =>
        this.authService
          .ConfirmPassword(action.email, action.password, action.code)
          .pipe(
            map((response) =>
              AuthActions.passwordCodeSuccess({
                success: response.success,
                error: response.error,
              })
            ),
            catchError((error) =>
              of(AuthActions.passwordCodeFailure({ error }))
            )
          )
      )
    )
  );

  forgotPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.forgotPassword),
      map((n) => n.email),
      switchMap((email) =>
        this.authService.ForgotPassword(email).pipe(
          map((response) => {
            console.log(response);
            if (
              response?.errorCode &&
              response.errorCode === 'UserNotFoundException'
            ) {
              return AuthActions.cognitoUserNotFound({
                userEmail: email,
              });
            } else {
              return AuthActions.forgotPasswordSuccess({
                email,
                success: response.success,
                errorMessage: response.errorMessage,
                attributeName: response.attributeName,
                deliveryMedium: response.deliveryMedium,
                destination: response.destination,
              });
            }
          }),
          catchError((error) => {
            return of(AuthActions.forgotPasswordFailure({ error }));
          })
        )
      )
    )
  );

  cognitoUserNotFound$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.cognitoUserNotFound),
      switchMap((code) =>
        this.accountFrameworkService.initializeUser(code.userEmail).pipe(
          map((response) =>
            AuthActions.cognitoUserNotFoundSuccess({
              userEmail: code.userEmail,
              apiResult: response,
            })
          ),
          catchError((error) => of(AuthActions.cognitoUserNotFoundFailure()))
        )
      )
    )
  );

  sendVerifyEmailCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.sendVerifyEmailMessage),
      switchMap(() =>
        this.authService.VerifyEmail().pipe(
          map((response) =>
            AuthActions.sendVerifyEmailMessageSuccess({
              success: response.success,
              message: response.message,
            })
          ),
          catchError((error) =>
            of(AuthActions.sendVerifyEmailMessageFailure({ error }))
          )
        )
      )
    )
  );

  submitVerifyEmailCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.submitVerifyEmailCode),
      map((n) => n.code),
      switchMap((code) =>
        this.authService.SubmitVerifyEmailCode(code).pipe(
          map((response) => AuthActions.submitVerifyEmailCodeSuccess()),
          catchError((error) =>
            of(AuthActions.submitVerifyEmailCodeFailure({ error }))
          )
        )
      )
    )
  );

  getUserDomain$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.getIdentityProvider),
      map((n) => n.username),
      switchMap((username) => {
        const stateKey = randomStringGenerator(9);
        const stateObj: {
          [key: string]: { redirectURL: string; createdDate: string };
        } = {};
        stateObj[stateKey] = {
          redirectURL: window.location.href,
          createdDate: moment().toDate().toISOString(),
        };
        this.cognitoService.setState(JSON.stringify(stateObj));
        this.cognitoService.setUsername(username);
        return this.authFrameworkService.identityProvider(username).pipe(
          map((response) => {
            response = response ? `${response}&state=${stateKey}` : response;
            return AuthActions.getIdentityProviderReturn({
              forwardUrl: response,
            });
          }),
          catchError((error) =>
            of(AuthActions.getIdentityProviderReturn({ forwardUrl: '' }))
          )
        );
      })
    )
  );
}
