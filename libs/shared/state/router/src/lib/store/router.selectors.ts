import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RouteReducerState } from './router-state';

export const selectRouterReducerState =
  createFeatureSelector<RouteReducerState>('router');

export const selectRouterState = createSelector(
  selectRouterReducerState,
  (routerReducerState) => routerReducerState?.state
);

export const selectRouterRoute = createSelector(
  selectRouterState,
  (routerState) => routerState?.route
);

export const selectRouterTitle = createSelector(
  selectRouterState,
  (routerState) => routerState?.data?.title as string
);
