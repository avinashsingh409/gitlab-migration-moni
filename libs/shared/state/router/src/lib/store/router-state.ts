import { RouterReducerState, RouterStateSerializer } from '@ngrx/router-store';
import {
  ActivatedRouteSnapshot,
  Data,
  Params,
  RouterStateSnapshot,
} from '@angular/router';

export interface IRouterState {
  url: string;
  route: string;
  queryParams: Params;
  params: Params;
  data: Data;
}

export const routerStateConfig = {
  stateKey: 'router', // state name for routing state
};

export type RouteReducerState = RouterReducerState<IRouterState>;

export class RouteStateSerializer
  implements RouterStateSerializer<IRouterState>
{
  serialize(routerState: RouterStateSnapshot): IRouterState {
    return {
      url: routerState.url,
      route: routerState.url.split('?')[0],
      params: routerParams(routerState.root, (r) => r.params),
      queryParams: routerParams(routerState.root, (r) => r.queryParams),
      data: routerData(routerState.root),
    };
  }
}

function routerParams(
  route: ActivatedRouteSnapshot,
  getter: (r: ActivatedRouteSnapshot) => Params
): Params {
  if (!route) {
    return {};
  }
  const currentParams = getter(route);
  const primaryChild =
    route.children.find((c) => c.outlet === 'primary') || route.firstChild;
  return { ...currentParams, ...routerParams(primaryChild, getter) };
}

function routerData(route: ActivatedRouteSnapshot): Data {
  if (!route) {
    return {};
  }

  const currentData = route.data;
  const primaryChild =
    route.children.find((c) => c.outlet === 'primary') || route.firstChild;
  return { ...currentData, ...routerData(primaryChild) };
}
