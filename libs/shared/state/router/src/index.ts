export * from './lib/shared-state-router.module';
export {
  RouteStateSerializer,
  RouteReducerState,
  routerStateConfig,
} from './lib/store/router-state';
export { IRouterState } from './lib/store/router-state';
import * as RouteSelectors from './lib/store/router.selectors';
export { RouteSelectors };
