const execSync = require('child_process').execSync;
const buildEnvironment = process.argv[2];
// buildEnvironment is either `dev`, `test`, or `stage`

const sourceBranchName = process.argv[3];
// sourceBranchName is something like: refs/heads/master
//                                   : refs/pull/27/merge
//                                   : refs/heads/release/1.0.0

const sourceBranch = process.argv[4];
// sourceBranch is something like: `master`, `merge`, `1.0.0`

const buildReason = process.argv[5];
// buildReason is : `PullRequest`, `IndividualCI`, `Manual`

let targetBranch = '';
if (process.argv.length >= 7) {
  targetBranch = process.argv[6];
}
// targetBranch is the branch we are pulling to.
// refs/heads/release/2.0.0

// Uncommenting these will blow up the build because the
// the console out determines the COMMANDS variable in Azure Devops.
// It can be useful if you need to short-circuit the build and debug
// console.log(buildEnvironment);
// console.log(sourceBranch);
// console.log(sourceBranchName);
// console.log(buildReason);

let baseSha = '';
if (sourceBranchName.startsWith('refs/heads/release')) {
  baseSha = `origin/release/${sourceBranch}`;
} else if (targetBranch.startsWith('refs/heads/release')) {
  const targetSplit = targetBranch.split('/');
  if (targetSplit.length !== 4) {
    console.log('an error occurred. Target Branch is incorrect');
    return;
  } else {
    baseSha = `origin/release/${targetSplit[3]}`;
  }
} else {
  baseSha =
    buildReason === 'PullRequest' ? 'origin/develop' : 'origin/develop~1';
}
console.log(
  JSON.stringify({
    ...commands('lint'),
    ...commands('test'),
    ...commands('build'),
  })
);

function commands(target) {
  let array = JSON.parse(
    execSync(`npx nx print-affected --base=${baseSha} --target=${target}`)
      .toString()
      .trim()
  ).tasks.map((t) => t.target.project);

  // TODO: Stage pipeline should just build atx-md and atx-asset-explorer
  // buildReason === Manual will be : build atx-ae and atx-md
  if (target === 'build' && buildEnvironment === 'test') {
    return {
      ['build1']: ['atx-ae'],
      ['build2']: ['atx-md'],
      ['build3']: ['atx-ev'],
    };
  } else if (buildEnvironment === 'stage' && target === 'build') {
    return {
      ['build1']: ['atx-ae'],
      ['build2']: ['atx-md'],
      ['build3']: ['atx-ev'],
    };
  } else if (buildEnvironment === 'test') {
    return { [target + '1']: [], [target + '2']: [], [target + '3']: [] };
  } else if (
    target === 'build' &&
    buildEnvironment === 'dev' &&
    buildReason === 'Manual'
  ) {
    return {
      ['build1']: ['atx-ae'],
      ['build2']: ['atx-md'],
      ['build3']: ['atx-ev'],
    };
  } else if (buildEnvironment === 'dev' && buildReason === 'Manual') {
    return { [target + '1']: [], [target + '2']: [], [target + '3']: [] };
  } else if (
    target === 'build' &&
    buildEnvironment === 'dev' &&
    buildReason === 'Schedule'
  ) {
    return {
      ['build1']: ['atx-ae'],
      ['build2']: ['atx-md'],
      ['build3']: ['atx-ev'],
    };
  } else if (buildEnvironment === 'dev' && buildReason === 'Schedule') {
    return { [target + '1']: [], [target + '2']: [], [target + '3']: [] };
  }

  array.sort(() => 0.5 - Math.random());
  let third = Math.floor(array.length / 3);
  if (array.length < 3) {
    third = Math.floor(array.length / 2);
  }
  const a1 = array.slice(0, third);
  const a2 = array.slice(third, third * 2);
  const a3 = array.slice(third * 2);
  return {
    [target + '1']: a1,
    [target + '2']: a2,
    [target + '3']: a3,
  };
}
