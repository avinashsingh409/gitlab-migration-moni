const execSync = require('child_process').execSync;
const buildEnvironment = process.argv[2];
// buildEnvironment is either `dev`, `test`, or `stage`

let retString = '';
if (buildEnvironment === 'test') {
  retString = 'TestDeployment';
} else if (buildEnvironment === 'dev') {
  retString = 'DevDeployment';
} else if (buildEnvironment === 'stage') {
  retString = 'StageDeployment';
} else if (buildEnvironment === 'prod') {
  retString = 'ProdDeployment';
}
console.log(`${retString}`);
