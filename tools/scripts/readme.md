# To Debug

- run-many.js

This decides what needs to be run using nx's print-affected command. The output is grabbed as the `COMMAND` in Azure devops, so unfortunately you can't console log in Azure Devops. You can log it locally.

-

You can run these commands with node to try them out. For example

```
node ./tools/scripts/calculate-commands.js dev refs/pull/27/merge merge PullRequest
```

```
node ./tools/scripts/calculate-commands.js stage refs/pull/27/merge merge PullRequest refs/heads/release/2.0.0
```

write out a print-affected command to json.
We only use the `tasks` second of this json file.

```
nx print-affected --target=build > test.json
```

An easy way to mock a change is to update an html file and not commit it. If you want to mock a situation where everything will be rebuilt, up the version in the package.json
