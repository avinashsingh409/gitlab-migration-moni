const execSync = require('child_process').execSync;
console.log(`Build Commands: ${process.argv[2]}`);
console.log(`Build Array Slice: ${process.argv[3]}`);
console.log(`Build Target Command: ${process.argv[4]}`);

const commands = JSON.parse(process.argv[2]);
const projects = commands[process.argv[3]];
const target = process.argv[4];
let isStage = false;
let isTest = false;
let buildEnvironment = '';
if (process.argv.length >= 6) {
  console.log(`Build Configuration: ${process.argv[5]}`);
  if (process.argv[5] === 'stage') {
    isStage = true;
    console.log('stage build running');
  } else if (process.argv[5] === 'test') {
    isTest = true;
    console.log('test build running');
  }
  buildEnvironment = `--configuration ${process.argv[5]}`;
}
console.log(`buildEnvironment: ${buildEnvironment}`);

if (projects.length > 0) {
  if (target === 'test') {
    execSync(
      `npx nx run-many --target=test --projects=${projects.join(
        ','
      )} --parallel`,
      {
        stdio: [0, 1, 2],
      }
    );
  } else if (target === 'build') {
    projects.forEach((project) => {
      let baseHref = '';

      switch (project) {
        case 'test-rigs-atx-asset-explorer':
          baseHref = '/asset-explorer/';
          break;
        case 'test-rigs-atx-data-explorer':
          baseHref = '/data-explorer/';
          break;
        case 'test-rigs-atx-view-explorer':
          baseHref = '/view-explorer/';
          break;
        case 'test-rigs-atx-work-management':
          baseHref = '/work-management/';
          break;
        case 'test-rigs-atx-navigation':
          baseHref = '/navigation/';
          break;
        case 'test-rigs-atx-chart':
          baseHref = '/charts/';
          break;
        case 'atx-md':
          baseHref = '/MD/';
          break;
        case 'atx-ae':
          baseHref = '/AE/';
          break;
        case 'atx-ev':
          baseHref = '/EV/';
          break;
        case 'test-rigs-atx-time-slider':
          baseHref = '/time-slider/';
          break;
        case 'test-rigs-atx-alerts':
          baseHref = '/alerts/';
          break;
        case 'test-rigs-atx-asset-tree':
          baseHref = '/asset-tree/';
          break;
        default:
          console.log('if it hits here you need to define a new project.');
          assertUnreachable(project);
      }
      // --outputPath
      if (isStage) {
        execSync(
          `npx nx build ${project} --prod --configuration=production --base-href=${baseHref} --outputPath=prod/${project}`,
          {
            stdio: [0, 1, 2],
          }
        );
        execSync(
          `npx nx build ${project} --prod --configuration stage --base-href=${baseHref} `,
          {
            stdio: [0, 1, 2],
          }
        );
      } else if (isTest) {
        execSync(
          `npx nx build ${project} --prod ${buildEnvironment} --base-href=${baseHref} `,
          {
            stdio: [0, 1, 2],
          }
        );
        // currently we build all apps in the stage environment. If APIs are new and unavailable on Stage, you can use this to build
        // the production artifacts directly from test
        // execSync(
        //   `npx nx build ${project} --prod --configuration=production --base-href=${baseHref} --outputPath=prod/${project}`,
        //   {
        //     stdio: [0, 1, 2],
        //   }
        // );
        // execSync(
        //   `npx nx build ${project} --prod --configuration=stage --base-href=${baseHref} --outputPath=stage/${project}`,
        //   {
        //     stdio: [0, 1, 2],
        //   }
        // );
      } else {
        execSync(
          `npx nx build ${project} --prod ${buildEnvironment} --base-href=${baseHref} `,
          {
            stdio: [0, 1, 2],
          }
        );
      }
    });
  } else {
    execSync(
      `npx nx run-many --target=${target} --projects=${projects.join(
        ','
      )} --parallel`,
      {
        stdio: [0, 1, 2],
      }
    );
  }
}
