import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NavFacade } from '@atonix/atx-navigation';
import { AppFacade } from './store/facade/app.facade';
import { getInitialNavState } from './store/state/nav.state';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthFacade } from '@atonix/shared/state/auth';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { NotificationBannerComponent } from '@atonix/shared/ui';
import { NotificationDialogComponent } from '@atonix/shared/ui';
import { showModalAnnouncement } from '@atonix/shared/utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();

  constructor(
    private navFacade: NavFacade,
    private appFacade: AppFacade,
    private authFacade: AuthFacade,
    private router: Router,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  ngOnInit(): void {
    this.authFacade.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val) {
          this.authFacade.getAllUIAccess();
        }
      });

    this.authFacade.uiAccessLoaded$
      .pipe(
        withLatestFrom(this.authFacade.hasAEAccess$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([uiAccessLoaded, access]) => {
        if (uiAccessLoaded) {
          if (access) {
            this.navFacade.configure(getInitialNavState());
          }

          const isAE = this.router.url.includes('/asset-explorer');
          this.navFacade.configureNavigationButton('nSpacer', {
            visible: access,
          });
          this.navFacade.configureNavigationButton('asset_tree', {
            visible: isAE && access,
          });
          this.navFacade.configureNavigationButton('asset-explorer', {
            visible: access,
            selected: isAE,
          });

          if (!access) {
            this.router.navigate(['']);
          }
        }
      });

    const windowHostName = window.location.hostname;
    if (
      this.appConfig.enableAnnouncement === 1 &&
      windowHostName.search(/.atonix.com/gi) === -1
    ) {
      this.snackbar.openFromComponent(NotificationBannerComponent, {
        verticalPosition: 'top',
        horizontalPosition: 'center',
        duration: 7000,
        panelClass: 'notification-banner-container',
      });
      if (showModalAnnouncement()) {
        this.dialog.open(NotificationDialogComponent);
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
