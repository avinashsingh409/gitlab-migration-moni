import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthFacade } from '@atonix/shared/state/auth';
import { Observable, Subject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { AppFacade } from '../../store/facade/app.facade';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  constructor(
    public facade: AppFacade,
    public authFacade: AuthFacade,
    private activatedRoute: ActivatedRoute
  ) {
    let result = this.activatedRoute.snapshot.queryParamMap.get('id');
    if (result === 'null' || result === 'undefined') {
      result = '';
    }
    facade.homeLoaded(result);
  }
}
