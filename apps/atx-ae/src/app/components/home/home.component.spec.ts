import { HomeComponent } from './home.component';
import { AppFacade } from '../../store/facade/app.facade';
import { AuthFacade, initialAuthState } from '@atonix/shared/state/auth';
import { render, screen, fireEvent } from '@testing-library/angular';
import { RouterTestingModule } from '@angular/router/testing';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { BehaviorSubject, of } from 'rxjs';
import { AtxMaterialModule } from '@atonix/atx-material';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('HomeComponent', () => {
  let appFacade: AppFacade;
  let authFacade: AuthFacade;

  async function renderHomeComponent() {
    appFacade = createMock(AppFacade);
    appFacade.homeLoaded = jest.fn().mockReturnThis();
    await render(HomeComponent, {
      imports: [RouterTestingModule, AtxMaterialModule],
      providers: [
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: AuthFacade, useValue: authFacade },
        {
          provide: AppFacade,
          useValue: appFacade,
        },
      ],
    });
  }

  test('main content should not be visible when access not loaded', async () => {
    authFacade = createMockWithValues(AuthFacade, {
      uiAccessLoaded$: new BehaviorSubject<boolean>(false),
      hasAEAccess$: new BehaviorSubject<boolean>(false),
    });
    await renderHomeComponent();
    expect(screen.queryByTestId('main_content')).not.toBeInTheDocument();
  });

  test('ae access denied should be shown if the user does not have access', async () => {
    authFacade = createMockWithValues(AuthFacade, {
      uiAccessLoaded$: new BehaviorSubject<boolean>(true),
      hasAEAccess$: new BehaviorSubject<boolean>(false),
    });
    await renderHomeComponent();

    expect(screen.getByTestId('main_content')).toBeInTheDocument();
    expect(screen.getByTestId('access_denied')).toBeInTheDocument();
  });
});
