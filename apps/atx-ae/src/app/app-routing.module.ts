import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {
  routerReducer,
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { HomeComponent } from '../app/components/home/home.component';
import { RouteStateSerializer } from '@atonix/shared/state/router';
import { LazyNgModuleWithProvidersFactory } from '@atonix/shared/utils';
import { TokenGuard } from '@atonix/shared/state/auth';
import { AssetConfigUtilityGuard } from './guards/asset-config-utility.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [TokenGuard] },
  {
    path: 'asset-explorer',
    loadChildren: () =>
      import('@atonix/atx-asset-explorer').then(
        (module) =>
          new LazyNgModuleWithProvidersFactory(
            module.AssetExplorerModule.forRoot({ configType: 'AssetExplorer' })
          )
      ),
    canActivate: [AssetConfigUtilityGuard],
  },
  { path: '**', redirectTo: '' },
];

export const routerStateConfig = {
  stateKey: 'router', // state name for routing state
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      relativeLinkResolution: 'legacy',
    }),
    StoreModule.forFeature(routerStateConfig.stateKey, routerReducer),
    StoreRouterConnectingModule.forRoot(routerStateConfig),
  ],
  exports: [RouterModule, StoreModule, StoreRouterConnectingModule],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: RouteStateSerializer,
    },
  ],
})
export class AppRoutingModule {}
export const AppRoutingComponents = [HomeComponent];
