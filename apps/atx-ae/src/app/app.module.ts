import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationModule } from '@atonix/atx-navigation';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingComponents, AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationEffects } from './store/effects/navigation.effects';
import { AppFacade } from './store/facade/app.facade';
import { AtxIndexedDbModule } from '@atonix/atx-indexed-db';
import { environment } from '@env/environment';
import {
  JwtInterceptorService,
  loggingMetareducer,
  logoutMetareducer,
  SharedStateAuthModule,
} from '@atonix/shared/state/auth';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { SharedUiModule } from '@atonix/shared/ui';
import { AssetConfigUtilityGuard } from './guards/asset-config-utility.guard';

@NgModule({
  declarations: [AppComponent, AppRoutingComponents],
  imports: [
    AtxIndexedDbModule,
    BrowserModule,
    AtxMaterialModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    SharedUiModule,
    SharedStateAuthModule.forRoot(),
    SharedUtilsModule.forRoot(),
    NavigationModule.forRoot(),
    StoreModule.forRoot(
      {},
      {
        metaReducers: environment.production
          ? [logoutMetareducer]
          : [logoutMetareducer, loggingMetareducer],
        // runtimeChecks: {
        //   strictStateImmutability: true,
        //   strictActionImmutability: true,
        //   strictStateSerializability: true,
        //   strictActionSerializability: true,
        //   strictActionWithinNgZone: true,
        // },
      }
    ),
    EffectsModule.forRoot([NavigationEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    AppRoutingModule,
  ],
  providers: [
    AppFacade,
    AssetConfigUtilityGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
