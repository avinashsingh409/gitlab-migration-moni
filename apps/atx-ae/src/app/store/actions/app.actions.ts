import { createAction } from '@ngrx/store';

export const assetCacheCleared = createAction('[MD] Asset Cache Cleared');
