import { createAction, props } from '@ngrx/store';

export const routeChange = createAction(
  '[AE Navigation Component] Route Change',
  props<{ route: string }>()
);

export const helpClick = createAction(
  '[AE Navigation Component] Help Click',
  props<{ url: string }>()
);
