import { INavigationState, assetNavigatorButton } from '@atonix/atx-navigation';

export const initialNavState: Partial<INavigationState> = {
  appName: 'Asset Explorer',
  navPaneState: 'open',
  logoRoute: '/home',
  showLoading: false,
  navigationItems: [
    { ...assetNavigatorButton, behavior: 'toggle', visible: false },
    {
      id: 'nSpacer',
      order: 2,
      icon: '',
      image: '',
      text: 'Task Centers',
      hover: '',
      visible: false,
      enabled: true,
      selected: true,
      data: {},
      type: 'spacer',
    },
    {
      id: 'asset-explorer',
      order: 1,
      icon: 'a360_monitor',
      text: 'Asset Explorer',
      hover: 'Asset Explorer',
      visible: false,
      enabled: true,
      selected: false,
      url: '/asset-explorer',
      newTab: false,
      type: 'route',
    },
    {
      id: 'feedbackSpacer',
      order: 2,
      icon: '',
      image: '',
      text: '',
      hover: '',
      visible: true,
      enabled: true,
      selected: true,
      data: {},
      type: 'spacer',
    },
    {
      id: 'feedback',
      order: 3,
      icon: 'a360_beta',
      text: 'Feedback',
      hover: 'Provide Beta Feedback',
      visible: true,
      enabled: true,
      selected: false,
      newTab: true,
      url: 'https://internal-ux2.ideas.aha.io/',
      type: 'link',
    },
  ],
};

export function getInitialNavState(): Partial<INavigationState> {
  return initialNavState;
}
