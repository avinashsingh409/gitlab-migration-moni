/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { switchMap, map, tap, withLatestFrom, take } from 'rxjs/operators';
import { TimeSliderPaneState, NavActions } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';

@Injectable()
export class NavigationEffects {
  constructor(
    private actions$: Actions,
    private loggerService: LoggerService
  ) {}

  helpClick$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NavActions.helpClick),
        map((n) => n.url),
        tap((url) => {
          window.open(url, '_blank');
        })
      ),
    { dispatch: false }
  );

  // Known task centers are
  // 'work_management','alerts','pa-map','pa-scorecard', 'aHome', 'data-explorer', 'viewExplorer'
  taskCenterLoad$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NavActions.taskCenterLoad),
      switchMap((action) => {
        let timeSliderPaneState: TimeSliderPaneState = 'none';
        if (action.timeSlider) {
          timeSliderPaneState = 'hidden';
        }
        if (action.timeSlider && action.timeSliderOpened) {
          timeSliderPaneState = 'show';
        }

        return [
          NavActions.configureNavigationButton({
            id: 'asset-explorer',
            button: { selected: action.taskCenterID === 'asset-explorer' },
          }),
          NavActions.configureNavigationButton({
            id: 'asset_tree',
            button: { visible: action.assetTree ?? true },
          }),
          NavActions.configure({ configuration: { timeSliderPaneState } }),
        ];
      })
    )
  );
}
