import { InjectionToken } from '@angular/core';

export class AppConfig {
  baseServicesURL: string = '';
  baseSiteURL: string = '';
  baseMicroServicesURL: string = '';
  authCallback: string = '';
  enableAnnouncement: number = 0;
  enableModelConfigLinks: boolean = false;
  enableModelConfig: boolean = false;
  enableAssetConfig: boolean = false;
  enableNewDiagnosticDrilldown: boolean = false;
  enableModelConfigSensitivityAvgAnomalyFilter: boolean = false;
  currency: string = '';
}

export let APP_CONFIG = new InjectionToken<AppConfig>('APP_CONFIG');
