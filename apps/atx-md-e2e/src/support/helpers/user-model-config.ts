/* eslint-disable @nrwl/nx/enforce-module-boundaries */
/* eslint-disable cypress/no-unnecessary-waiting */
import { User } from './user';
import { UserAggridCommon } from './user-aggrid-common';
import * as Pages from '../page/pages';
import { ModelConfigColumnID } from '../page/common/ag-grid/grid-column-ids';
import { Utils } from './utils';
import { FlyoutActionButtons, GridNames } from './user-data';
import {
  ModelConfigSlopeAnalysisColumnID,
  ModelConfigAnomaliesColumnID,
  ModelConfigForecastColumnID,
  ModelConfigTrainingRangeColumnID,
  AnomaliesConfig,
  ConfigurationTabs,
  AlertsNotifiColumn,
} from '../definitions';
import { ApiStub } from './stub/api-stub';
import { AgGridClass, ModelTypes, OpModesTypes } from '../definitions';
import '@4tw/cypress-drag-drop';

const stub = new ApiStub();
const modelConfigPage = new Pages.ModelConfig();
const modelConfigEdit = new Pages.ModelEdit();
const sidePanelColumn = modelConfigPage.sideButtons.column;
const sidePanelViews = modelConfigPage.sideButtons.view;
// const flyoutWindow = modelConfigPage.flyoutWindow;
const agGridColumnHeader = modelConfigPage.agGridColumnHeader;
// const agGridHeaderPanel = modelConfigPage.agGridHeadPanel;
const util = new Utils();
const userAggridCommon = new UserAggridCommon();
const flyoutWindow = modelConfigPage.flyoutWindow;
const modelConfigOpMode = new Pages.ModelConfigOpModes();

export class ModelConfigUser extends User {
  showSideColumnPane() {
    modelConfigPage.sideButtons.column.showPanel();
  }

  showPanel(panelName: 'column' | 'filter' | 'view') {
    userAggridCommon.showPanel(panelName);
    // this.moveMouseToFixLayout();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    userAggridCommon.setColumns(colObj);
  }

  saveView(viewName: string) {
    userAggridCommon.saveView(viewName);
    this.moveMouseToFixLayout();
  }

  moveColumn(
    columnToMove: ModelConfigColumnID,
    targetColumn: ModelConfigColumnID
  ) {
    userAggridCommon.moveColumn(columnToMove, targetColumn);
  }

  deleteSavedView(savedViewName: string) {
    userAggridCommon.deleteSavedView(savedViewName);
  }

  deleteAllSavedViews() {
    userAggridCommon.deleteAllSavedViews();
    this.moveMouseToFixLayout();
  }

  pinColumn(columnID: ModelConfigColumnID, direction: 'Left' | 'Right') {
    userAggridCommon.pinColumn(columnID, direction);
    // this.moveMouseToFixLayout();
  }

  /**
   * this method is to move the mouse to the expand screeningview button
   * to display the screeningview properly due to issue happening only in automation.
   */
  moveMouseToFixLayout() {
    return cy.get(`atx-donut-chart`).eq(2).trigger('mousemove');
  }

  restoreGridToDefaults() {
    userAggridCommon.restoreGridToDefaults();
    // this.moveMouseToFixLayout();
  }

  selectSavedView(viewName: string) {
    userAggridCommon.selectSavedView(viewName);
    cy.wait(500);
    // this.moveMouseToFixLayout();
  }

  clickFloatingFilterBtn() {
    return userAggridCommon.clickToggleFilterBtn();
  }

  /**
   * used to input values in the Filter header, need to open filter head using toggle filter button
   * @columnName is the actual name of the column in the ag grid
   */
  inputFilterValue(value: string, columnName: string, gridName?: AgGridClass) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.inputFloatingFilterValue(value, columnName, gridName);
    cy.wait(1000);
    // this.moveMouseToFixLayout();
  }

  selectFilterValue(value: string, columnName: string, gridName?: AgGridClass) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.selectFloatingFilterValue(value, columnName, gridName);
    cy.wait(1000);
    // this.moveMouseToFixLayout();
  }

  sortColumn(colId: ModelConfigColumnID) {
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.clickColumn(colId);
    cy.wait(1000);
    // this.moveMouseToFixLayout();
  }

  groupByColumn(colId: ModelConfigColumnID) {
    userAggridCommon.groupByColumn(colId);
    // this.moveMouseToFixLayout();
  }

  expandGroup(groupName: string) {
    userAggridCommon.expandGroup(groupName);
    // this.moveMouseToFixLayout();
  }

  openTab(tabName: 'ModelList' | 'TagList' | 'OpModes') {
    switch (tabName) {
      case 'ModelList':
        modelConfigPage.getModelListBtn().click();
        break;
      case 'TagList':
        modelConfigPage.getTagListBtn().click();
        break;
      case 'OpModes':
        modelConfigPage.getOpModesBtn().click().wait(2000);
        break;
      default:
        break;
    }
  }

  selectGridItemByRowIndex(rowIdx) {
    userAggridCommon.selectGridItemByRowIndex(rowIdx);
  }

  multiSelectGridItemByRowIndex(rowIdx) {
    userAggridCommon.multiSelectGridItemByRowIndex(rowIdx);
  }

  selectActionBtnFlyout(actionType: FlyoutActionButtons) {
    flyoutWindow.selectActionBtn(actionType);
  }

  selectModelByName(modelName: string) {
    cy.get(`div.ag-row [col-id="ModelName"]`).contains(modelName).click();
  }

  saveNote() {
    cy.get(`#alertsAction form`).contains('Save').click();
  }

  doubleClickCellByColumnName(columnName: string, index = 0) {
    userAggridCommon.doubleClickCellByColumnName(columnName, index);
  }

  clickCellByColumnName(
    columnName:
      | ModelConfigSlopeAnalysisColumnID
      | ModelConfigAnomaliesColumnID
      | ModelConfigForecastColumnID
      | ModelConfigTrainingRangeColumnID,
    index = 0
  ) {
    return userAggridCommon.clickCellByColumnName(columnName, index);
  }

  selectFromList(
    value: string,
    columnName?:
      | ModelConfigAnomaliesColumnID
      | ModelConfigForecastColumnID
      | ModelConfigTrainingRangeColumnID,
    index = 0
  ) {
    userAggridCommon.selectFromList(value, columnName, index);
  }

  openConfigurationTab(tabName: ConfigurationTabs) {
    return modelConfigEdit.openConfigurationTab(tabName);
  }

  selectAnomaliesFilter(
    filter:
      | 'All'
      | 'Model Bound Criticality'
      | 'Relative Model Bounds'
      | 'Fixed Limits'
  ) {
    modelConfigEdit.selectAnomaliesFilter(filter);
  }

  applyValueToColumn(
    columnName:
      | ModelConfigSlopeAnalysisColumnID
      | ModelConfigAnomaliesColumnID
      | ModelConfigForecastColumnID
      | ModelConfigTrainingRangeColumnID,
    index = 0
  ) {
    cy.wait(1000);
    userAggridCommon.rightClickCellByColumnName(columnName, index);
    userAggridCommon.clickApplyValueToColumn();
  }

  selectOpModeByIndex(rowIdx = 0) {
    stub.OpMode();
    userAggridCommon.selectGridItemByIdx(rowIdx, 'opmode-list');
    return cy.wait('@OpMode');
  }

  selectTagByIndex(rowIdx = 0, gridName: AgGridClass) {
    return userAggridCommon.selectGridItemByIdx(rowIdx, gridName);
  }

  createNewOpMode(title: string, type: OpModesTypes) {
    modelConfigOpMode.createNewOpModeBtn().click();
    modelConfigOpMode.createDialogTitle().type(title);
    modelConfigOpMode.createDialogModeTypeSelect().click();
    modelConfigOpMode.createDialogModeOptions().contains(type).click();
    modelConfigOpMode.createDialogCreateBtn().click();
  }

  moveTagToAssociatedByIdx(index: number) {
    modelConfigOpMode.associatedAssetAndTags().as('AssociatedAssetsAndTags');
    modelConfigOpMode
      .tagRowItems()
      .find(`[col-id="Name"] .draggableTag`)
      .parent()
      .eq(index)
      .as('TagToMove');
    // cy.get(`[aria-label="Asset Filter Input"]`).as('AssociatedAssetsAndTags');
    // need to find another way. For now this is not working in adding tags
    util.dragAndDrop('TagToMove', 'AssociatedAssetsAndTags');
  }

  createNewModel(modelName: string, modelType: ModelTypes) {
    stub.BlankConfig();
    stub.PrioritizedOpModeDefinitionsForTagMap();
    util.newTabListener();
    modelConfigPage.createNewModelBtn().click();
    util.switchToNewTab();
    cy.wait('@BlankConfig');

    modelConfigPage.createNewModelDialog.modelName().clear().type(modelName);
    modelConfigPage.selectModelType(modelType);
    modelConfigPage.createNewModelDialog.createModelBtn().click().wait(1000);
  }

  openContextTab() {
    modelConfigPage.openTrendTab('Context');
    cy.wait(700);
  }

  saveModel(modelName: string) {
    modelConfigPage.saveModelBtn().click().wait(300);
    modelConfigPage.modelSaveChanges
      .content()
      .should('contain.text', modelName);
  }

  rebuildModel() {
    modelConfigPage.modelSaveChanges.saveBtn().click();
  }

  deleteModel(modelName: string) {
    modelConfigPage.moreOptionsBtn().click();
    modelConfigPage.deleteModelBtn().click();

    modelConfigPage.deleteModelDialog
      .title()
      .should('contain.text', 'Delete Model');
    modelConfigPage.deleteModelDialog
      .content()
      .should('contain.text', modelName);

    modelConfigPage.deleteModelDialog.deleteBtn().click();
  }

  deleteOpMode() {
    modelConfigOpMode.moreOptionsBtn().click();
    modelConfigOpMode.deleteOpModeBtn().click();

    modelConfigOpMode.deleteOpModeDialog
      .title()
      .should('contain.text', 'Delete Op Mode');

    modelConfigOpMode.deleteOpModeDialog.deleteBtn().click();
  }

  removeAssociatedAssetsAndTags(name: string) {
    modelConfigOpMode
      .associatedAssetAndTags()
      .contains(name)
      // .parent()
      .find('button')
      .click()
      .wait(200);
  }

  inputOpModeName(name: string, clear = false) {
    return modelConfigOpMode.inputOpModeName(name, clear);
  }

  discardOpModeChanges() {
    stub.OpMode();
    modelConfigOpMode.discardBtn().click();
    return cy.wait('@OpMode');
  }

  saveOpMode() {
    return modelConfigOpMode.saveBtn().click();
  }

  cancelOpModeSave() {
    modelConfigOpMode.opModeSaveDiaglogCancelBtn().click();
  }

  inputAnomalyConfig(config: AnomaliesConfig) {
    modelConfigPage.openConfigurationTab('Anomalies');
    if (config.relativeModelBounds) {
      const relativeModelBounds = config.relativeModelBounds;

      if (relativeModelBounds.relativeUpperBounds.multiplier)
        userAggridCommon
          .clickCellByColumnName('RelativeModelBoundsUpperMultiplier')
          .type(`${relativeModelBounds.relativeUpperBounds.multiplier}{enter}`);
      if (relativeModelBounds.relativeUpperBounds.bias)
        userAggridCommon
          .clickCellByColumnName('RelativeModelBoundsUpperBias')
          .type(`${relativeModelBounds.relativeUpperBounds.bias}{enter}`);
    }

    if (config.relativeModelBounds) {
      const relativeModelBounds = config.relativeModelBounds;

      if (relativeModelBounds.relativeLowerBounds.multiplier)
        userAggridCommon
          .clickCellByColumnName('RelativeModelBoundsLowerMultiplier')
          .type(`${relativeModelBounds.relativeLowerBounds.multiplier}{enter}`);
      if (relativeModelBounds.relativeLowerBounds.bias)
        userAggridCommon
          .clickCellByColumnName('RelativeModelBoundsLowerBias')
          .type(`${relativeModelBounds.relativeLowerBounds.bias}{enter}`);
    }
  }

  openAlertNotifications(columnName: AlertsNotifiColumn) {
    stub.GetAlertNotification();
    // cy.get(`[col-id="${columnName}"] atx-send-message-icon-renderer mat-icon`)
    modelConfigEdit
      .getAlertsNotifBtn(columnName)
      .click()
      .wait('@GetAlertNotification');
  }

  inputAlertNotifCustomMsg(message: string) {
    modelConfigEdit.alertNotifCustomMsg().type(message, { force: true });
  }

  saveAlertNotification() {
    stub.SaveAlertNotification();
    stub.ModelTrend();
    cy.get(`[data-cy="saveNotificationBtn"]`)
      .click({ force: true })
      .wait([`@SaveAlertNotification`, '@ModelTrend']);
  }

  selectUsersToSubscribeByIdx(idx: number) {
    modelConfigEdit.addUsersFld().click();

    return modelConfigEdit.addUsersList().eq(idx).click();
  }

  deleteAlertsNotif() {
    stub.DeleteAlertNotification();
    stub.ModelTrend();
    cy.get(`[data-cy="deteNotificationBtn"]`).click({ force: true });

    cy.get('button')
      .contains('Yes')
      .click({ force: true })
      .wait(['@DeleteAlertNotification', '@ModelTrend']);
  }

  removeSubscribedUser(user: string) {
    modelConfigEdit
      .getAlertsNotifSubscribedUsers()
      .contains(user)
      .find('[data-cy="removeUser"]')
      .click();
  }
}
