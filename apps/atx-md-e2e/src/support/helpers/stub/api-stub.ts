export class ApiStub {
  loadUserDetails() {
    cy.intercept('GET', '/Services/api/Authorization/IdentityProvider').as(
      'identityProvider'
    );
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws1.json',
    }).as('authAws1');
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws2.json',
    });
    cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
      fixture: 'auth/user-authenticated.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/AdvancedPreview', {
      fixture: 'auth/advanced-preview.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/MDApp', {
      fixture: 'auth/mdapp-grant-access.json',
    });
    cy.intercept('GET', '/md/assets/config.json', {
      fixture: 'auth/config.json',
    });
    // cy.intercept('GET', '/Services/api/Account/UserPreferences', {
    //   fixture: 'auth/user-preferences.json',
    // }).as('userPreferencesAlias');
    // cy.wait(`@userPreferencesAlias`);
    this.listConfig('alerts/list-config-test1', 'alerts');
  }

  stubAPI(user: string, api: string) {
    // cy.intercept();
  }

  loadAssetNav() {
    cy.intercept('GET', '/Services/api/UIConfig/LoadNodeFromKey', {
      fixture: 'asset-nav/load-node-from-key-test1.json',
    }).as('assetNavAlias');
  }

  loadAlertsSummary(jsonPath: string) {
    cy.intercept('POST', '/api/alerts/Summary', {
      fixture: `${jsonPath}.json`,
    }).as('alertsSummaryAlias');
  }

  assetAccessRights() {
    cy.intercept('GET', '/Services/api/Authorization/AssetAccessRights', {
      fixture: 'asset-access-rights-test1.json',
    }).as('assetAccessRightsAlias');
  }

  listConfig(jsonPath: string, type: string) {
    cy.intercept('GET', `/Services/api/UIConfig/ListConfig?listType=${type}`, {
      fixture: `${jsonPath}.json`,
    }).as('listConfigAlias');
  }

  getIssueFollowers() {
    cy.intercept(`GET`, `api/Issues/getissuefollowers?*`).as(
      'getIssueFollowers'
    );
  }

  manageIssueFollowers() {
    cy.intercept('POST', `api/Issues/manageissuefollowers`).as(
      'manageIssueFollowers'
    );
  }

  loadAssetNavIssues() {
    cy.intercept('GET', '/api/v1/assets/trees/load?assetId=*').as(
      'loadNodeFromKeyIssues'
    );
  }

  createNewIssue() {
    cy.intercept('GET', '/Services/api/Issues/NewIssue*').as('createNewIssue');
  }

  isUserFederated() {
    cy.intercept(
      'GET',
      '/Services/api/Account/IsUserFederated?username=*',
      'true'
    ).as('isUserFederated');
  }
  userPreferences() {
    cy.intercept('GET', '/Services/api/Account/UserPreferences').as(
      'userPreferences'
    );
  }

  userAccountFederated() {
    cy.intercept(
      `GET`,
      `/Services/api/Account/UserAccount*`,
      `{"UserName":"fcf37c3c-7f1a-4961-9c41-21a7e51488e9","Email":"atonixoianalyst@atonixtest.com","Name":"AtonixOI Analyst Test","AccountStatus":"EXTERNAL_PROVIDER","MFAStatus":null,"EmailVerified":true,"Phone":"+639611441391","PhoneVerified":true}`
    ).as('federatedUser');
  }

  assetIssueImpactTypeMapTotals() {
    cy.intercept(
      'GET',
      '/Services/api/v2/Issues/AssetIssueImpactTypeMapTotals*'
    ).as(`assetIssueImpactTypeMapTotals`);
  }

  tagGrid(fixtureFile?: string) {
    if (fixtureFile)
      cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid', {
        fixture: `/de/${fixtureFile}`,
      }).as('tagGrid');
    else cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid').as('tagGrid');
  }

  tagDataFiltered(fixtureFile?: string) {
    if (fixtureFile)
      cy.intercept(
        'POST',
        '/Services/api/ProcessData/TagsDataFiltered?asset*',
        { fixture: `/de/${fixtureFile}` }
      ).as('tagDataFiltered');
    else
      cy.intercept(
        'POST',
        '/Services/api/ProcessData/TagsDataFiltered?asset*'
      ).as('tagDataFiltered');
  }

  modelTrendForAlertChartingByModelExtId(fixtureFile?: string) {
    if (fixtureFile)
      cy.intercept(
        'GET',
        '/Services/api/Monitoring/ModelTrendForAlertChartingByModelExtId?ModelExtId*',
        { fixture: `/ddd/chart-zoom/${fixtureFile}` }
      ).as('modelTrendForAlertsChartingByModelExtId');
    else
      cy.intercept(
        'GET',
        '/Services/api/Monitoring/ModelTrendForAlertChartingByModelExtId?ModelExtId*'
      ).as('modelTrendForAlertsChartingByModelExtId');
  }

  PDNDModelTrendsWithAssetGuid(fixtureFile?: string) {
    if (fixtureFile)
      cy.intercept(
        'GET',
        '/Services/api/ProcessData/PDNDModelTrendsWithAssetGuid?assetGuid?*',
        { fixture: `/ddd/chart-zoom/${fixtureFile}` }
      ).as('PDNDModelTrendsWithAssetGuid');
    else
      cy.intercept(
        'GET',
        '/Services/api/ProcessData/PDNDModelTrendsWithAssetGuid?assetGuid?*'
      ).as('PDNDModelTrendsWithAssetGuid');
  }

  modelByGUID(fixtureFile?: string) {
    if (fixtureFile)
      cy.intercept('GET', '/Services/api/Monitoring/ModelByGUID?modelExtId*', {
        fixture: `/ddd/chart-zoom/${fixtureFile}`,
      }).as('ModelByGUID');
    else
      cy.intercept(
        'GET',
        '/Services/api/Monitoring/ModelByGUID?modelExtId*'
      ).as('ModelByGUID');
  }

  tagsDataFiltered(fixtureFile?: string) {
    if (fixtureFile) {
      cy.intercept(
        'POST',
        '/Services/api/ProcessData/TagsDataFiltered?asset*',
        {
          fixture: `/ddd/chart-zoom/${fixtureFile}`,
        }
      ).as(fixtureFile);
    } else
      cy.intercept(
        'POST',
        '/Services/api/ProcessData/TagsDataFiltered?asset*'
      ).as('tagsDataFiltered');
  }

  tagsDataFilteredZoom() {
    cy.intercept('POST', 'Services/api/ProcessData/TagsDataFilter**', (req) => {
      req.reply((res) => {
        if (req.body.Title === 'Model Context')
          res.send({
            fixture: `/ddd/chart-zoom/TagsDataFiltered1`,
          });
        else
          res.send({
            fixture: `/ddd/chart-zoom/TagsDataFiltered2`,
          });
      });
    }).as('TagsDataFilteredZoom');
  }

  ModelSummary(fixtureFilePath?: string) {
    const url = `/Services/api/Monitoring/ModelSummary?*`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('ModelSummary');
    else cy.intercept('GET', url).as('ModelSummary');
  }

  config(fixtureFilePath?: string) {
    const url = `/api/models/config`;
    if (fixtureFilePath)
      cy.intercept('POST', url, {
        fixture: fixtureFilePath,
      }).as('config');
    else cy.intercept('POST', url).as('config');
  }

  mlmodellatesttype(fixtureFilePath?: string) {
    const url = `/api/alertengine/mlmodellatesttype?*`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('mlmodellatesttype');
    else cy.intercept('GET', url).as('mlmodellatesttype');
  }

  ModelTrend(fixtureFilePath?: string) {
    const url = `/Services/api/Monitoring/ModelTrend?*`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('ModelTrend');
    else cy.intercept('GET', url).as('ModelTrend');
  }

  OpMode(fixtureFilePath?: string, method = 'GET') {
    const url = `/api/models/opmode*`;
    if (fixtureFilePath)
      cy.intercept(url, {
        fixture: fixtureFilePath,
      }).as('OpMode');
    else cy.intercept(url).as('OpMode');
  }

  BlankConfig(fixtureFilePath?: string) {
    const url = `api/models/blankconfig`;
    if (fixtureFilePath)
      cy.intercept('POST', url, {
        fixture: fixtureFilePath,
      }).as('BlankConfig');
    else cy.intercept('POST', url).as('BlankConfig');
  }

  PrioritizedOpModeDefinitionsForTagMap(fixtureFilePath?: string) {
    const url = `Services/api/Monitoring/PrioritizedOpModeDefinitionsForTagMap*`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('PrioritizedOpModeDefinitionsForTagMap');
    else cy.intercept('GET', url).as('PrioritizedOpModeDefinitionsForTagMap');
  }

  Tags(fixtureFilePath?: string) {
    const url = `api/v1/processdata/servers/**/tags`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('Tags');
    else cy.intercept('GET', url).as('Tags');
  }

  GetAlertNotification(fixtureFilePath?: string) {
    const url = `Services/api/Monitoring/GetAlertNotification*`;
    if (fixtureFilePath)
      cy.intercept('GET', url, {
        fixture: fixtureFilePath,
      }).as('GetAlertNotification');
    else cy.intercept('GET', url).as('GetAlertNotification');
  }

  SaveAlertNotification(fixtureFilePath?: string) {
    const url = `Services/api/Monitoring/SaveAlertNotification*`;
    if (fixtureFilePath)
      cy.intercept('POST', url, {
        fixture: fixtureFilePath,
      }).as('SaveAlertNotification');
    else cy.intercept('POST', url).as('SaveAlertNotification');
  }

  DeleteAlertNotification(fixtureFilePath?: string) {
    const url = `Services/api/Monitoring/DeleteAlertNotification*`;
    if (fixtureFilePath)
      cy.intercept('DELETE', url, {
        fixture: fixtureFilePath,
      }).as('DeleteAlertNotification');
    else cy.intercept('DELETE', url).as('DeleteAlertNotification');
  }
}
