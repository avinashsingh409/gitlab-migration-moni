/* eslint-disable cypress/no-unnecessary-waiting */
// import { browser, protractor, ElementFinder, ElementArrayFinder } from 'protractor';
// import * as path from 'path';

import moment from 'moment';
import { ApiStub } from './stub/api-stub';
import { envToTest, GridNames } from './user-data';

const stub = new ApiStub();

export class Utils {
  getCurrentTime = () => {
    return moment().format('MM/DD/YY hh:mmA');
  };

  dragAndDrop(elemToMoveAlias: string, targetElemAlias, btn = 0) {
    // cy.get(`@${elemToMoveAlias}`)
    //   .trigger(`pointerdown`, { button: 0 })
    //   .trigger('dragstart');
    // // cy.get(`@${targetElemAlias}`).trigger('mousemove').click({ force: true });
    // cy.get(`@${targetElemAlias}`)
    //   .trigger('dragover')
    //   .wait(5000)
    //   .trigger('drop');
    // .trigger('dragend', { force: true });

    // cy.get(`@${elemToMoveAlias}`)
    //   .trigger('pointerdown', { button: 0 })
    //   .trigger('dragstart');
    // cy.get(`@${targetElemAlias}`)
    //   .trigger('dragover')
    //   .wait(10000)
    //   .trigger('drop');

    cy.get(`@${elemToMoveAlias}`).trigger('mousedown', {
      button: 0,
      force: true,
    });
    cy.wait(1000);
    cy.get(`@${targetElemAlias}`)
      .trigger('mousemove', { force: true })
      .trigger('mouseup', { force: true });
  }

  loadUserDetails() {
    cy.intercept('GET', '/Services/api/Authorization/IdentityProvider').as(
      'identityProvider'
    );
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws1.json',
    }).as('authAws1');
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws2.json',
    });
    cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
      fixture: 'auth/user-authenticated.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/AdvancedPreview', {
      fixture: 'auth/advanced-preview.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/MDApp', {
      fixture: 'auth/mdapp-grant-access.json',
    });
    cy.intercept('GET', '/md/assets/config.json', {
      fixture: 'auth/config.json',
    });
    cy.intercept('GET', '/Services/api/Account/UserPreferences', {
      fixture: 'auth/user-preferences.json',
    });
  }
  stubAPI(user: string, api: string) {
    // cy.intercept();
  }
  loadAssetNav() {
    cy.intercept('GET', '/Services/api/UIConfig/LoadNodeFromKey', {
      fixture: 'asset-nav/load-node-from-key-test1.json',
    }).as('assetNavAlias');
  }
  loadAlertsSummary(jsonPath: string) {
    cy.intercept('POST', '/api/alerts/Summary', (req) => {
      req.body = {
        startRow: 0,
        endRow: 100,
        rowGroupCols: [],
        valueCols: [],
        pivotCols: [],
        pivotMode: false,
        groupKeys: [],
        filterModel: {
          AssetID: {
            filterType: 'text',
            type: 'contains',
            filter:
              'MjdjZWJiNzEtZDY3MC00OTBmLTk2NmYtZDJmZWI1MzBiM2Y1fjVmNWY1Yjc0LWE0YTUtNDViNC05N2M5LTM1M2FmZmFkN2Y5OX4xMjg2NTc=',
          },
          Alert: { values: ['true'], filterType: 'set' },
          ActionWatch: { values: ['false'], filterType: 'set' },
          Ignore: { values: ['false'], filterType: 'set' },
        },
        sortModel: [],
      };
    });
  }

  loadAgGridForWait(gridName: GridNames) {
    switch (gridName) {
      case 'AlertsGrid':
        cy.intercept('POST', '/api/alerts/Summary').as('AlertsGrid');
        break;
      case 'IssuesGrid':
        cy.intercept('POST', '/api/Issues/IssuesGrid').as('IssuesGrid');
        break;
      case 'TagGrid':
        cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid').as('TagGrid');
        break;
      case 'HistoryGrid':
        cy.intercept('POST', '/Services/api/Monitoring/AlertTimeline').as(
          'HistoryGrid'
        );
        break;
      case 'TagListGrid':
        cy.intercept('GET', '/api/modelconfig/modelconfigtagslist*').as(
          'TagListGrid'
        );
        break;
      default:
        break;
    }
  }

  dismissAtonixPopUp() {
    cy.get(`atx-notification-dialog button[aria-label="Cancel"]`).click();
    cy.wait(3000);
    cy.get(`.atonix-redirect-container [aria-label="Clear"]`).click();
  }

  newTabListener() {
    cy.wait(2000);
    cy.window().then((win) => {
      cy.stub(win, 'open')
        .as('tabOpen')
        .callsFake((url) => {
          cy.visit(url);
        });
    });
  }

  /**
   * newTabListener() needs to be added before switchToNewTab.
   */
  switchToNewTab() {
    cy.get('@tabOpen', { timeout: 10000 }).should('be.called');
  }

  switchToIframe = (iframe: string) => {
    return cy
      .get(iframe)
      .its('0.contentDocument.body')
      .should('be.visible')
      .then(cy.wrap);
  };

  clickOverlay() {
    cy.get(`.cdk-overlay-backdrop`).click();
  }

  loadModelEditSingle() {
    cy.intercept('GET', '/Services/api/UIConfig/LoadNodeFromKey', {
      fixture: 'asset-nav/load-node-from-key-test1.json',
    }).as('assetNavAlias');
  }

  // ModelSummary() {
  //   const url = `/Services/api/Monitoring/ModelSummary?*`;
  //   if (fixtureFilePath)
  //     cy.intercept('GET', url, {
  //       fixture: fixtureFilePath,
  //     }).as('ModelSummary');
  //   else cy.intercept('GET', url).as('ModelSummary');
  // }

  // config() {
  //   const url = `/api/models/config`;
  //   if (fixtureFilePath)
  //     cy.intercept('POST', url, {
  //       fixture: fixtureFilePath,
  //     }).as('config');
  //   else cy.intercept('POST', url).as('config');
  // }

  // mlmodellatesttype() {
  //   const url = `/api/alertengine/mlmodellatesttype?*`;
  //   if (fixtureFilePath)
  //     cy.intercept('GET', url, {
  //       fixture: fixtureFilePath,
  //     }).as('mlmodellatesttype');
  //   else cy.intercept('GET', url).as('mlmodellatesttype');
  // }

  // ModelTrend(fixtureFilePath?: string) {
  //   const url = `/Services/api/Monitoring/ModelTrend?*`;
  //   if (fixtureFilePath)
  //     cy.intercept('GET', url, {
  //       fixture: fixtureFilePath,
  //     }).as('ModelTrend');
  //   else cy.intercept('GET', url).as('ModelTrend');
  // }

  modelWait() {
    stub.ModelSummary();
    stub.config();
    stub.mlmodellatesttype();
    stub.ModelTrend();
  }

  getUrl() {
    const env = (envToTest as string).toLocaleLowerCase();
    if (env.includes('test')) return 'https://test.atonix.com/';
    else if (env.includes('stage')) return 'https://stage.atonix.com/';
    else return 'https://dev.atonix.com/';
  }
}
