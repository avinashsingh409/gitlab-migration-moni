export class Helper {
  removeItemFromArrayByValue(arr: any, toRemove: any) {
    return arr.filter((value) => !toRemove.includes(value));
  }

  getSnackBarText() {
    return cy.get(`atx-snackbar .snackbar-message`, { timeout: 10000 });
  }

  calcProdductionLossValue(impactToInput, impactCalcVariables) {
    return (
      (impactToInput.productionImpactQuantity / 100) *
      impactCalcVariables.ImpactCapacityValue
    );
  }

  calcProdLossToDate(prodLossValue, impactToInput, impactCalcVariables) {
    return (
      prodLossValue *
      impactToInput.productionDurationToDate *
      impactCalcVariables.ImpactTimeToDaysConversion *
      (impactToInput.derateOverride / 100)
    );
  }

  calcProdMonthlyCost(prodLossValue, impactToInput, impactCalcVariables) {
    return (
      prodLossValue *
      30 *
      impactCalcVariables.ImpactTimeToDaysConversion *
      (impactToInput.derateOverride / 100) *
      (impactToInput.lostMarginOpportunity -
        impactToInput.relevantProductionCost)
    );
  }

  calcProdTotalCost(prodLossValue, impactToInput, impactCalcVariables) {
    const result =
      prodLossValue *
      impactToInput.productionDurationToDate *
      impactCalcVariables.ImpactTimeToDaysConversion *
      (impactToInput.derateOverride / 100) *
      (impactToInput.lostMarginOpportunity -
        impactToInput.relevantProductionCost);

    return result;
  }

  calcNomConsRateChangeValue(impactToInput, impactCalcVariables) {
    return impactCalcVariables.ImpactNomConsRateValue > 0
      ? (impactCalcVariables.ImpactNomConsRateValue *
          impactToInput.efficiencyImpactQuantity) /
          100
      : impactToInput.efficiencyImpactQuantity;
  }

  calcEffImpactMonthlyCost(NomConsRateChangeValue, impactCalcVariables) {
    return (
      NomConsRateChangeValue *
      impactCalcVariables.ImpactConsumableValue *
      (impactCalcVariables.ImpactProdCostToRateConvert /
        impactCalcVariables.ImpactConsCostToRateConvert) *
      impactCalcVariables.ImpactNomCapacityUtilization *
      impactCalcVariables.ImpactCapacityValue *
      impactCalcVariables.ImpactTimeToDaysConversion *
      30
    );
  }

  calcEffImpactTotalCost(
    NomConsRateChangeValue,
    impactToInput,
    impactCalcVariables
  ) {
    return (
      NomConsRateChangeValue *
      impactCalcVariables.ImpactConsumableValue *
      (impactCalcVariables.ImpactProdCostToRateConvert /
        impactCalcVariables.ImpactConsCostToRateConvert) *
      impactCalcVariables.ImpactNomCapacityUtilization *
      impactCalcVariables.ImpactCapacityValue *
      impactCalcVariables.ImpactTimeToDaysConversion *
      impactToInput.efficiencyDurationToDate
    );
  }
}
