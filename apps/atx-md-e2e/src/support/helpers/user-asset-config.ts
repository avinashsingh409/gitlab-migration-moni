/* eslint-disable cypress/no-unnecessary-waiting */
import { User } from './user';
import { Utils } from './utils';
import { ApiStub } from './stub/api-stub';
import * as Pages from '../page/pages';
import { UserAggridCommon } from './user-aggrid-common';
import { TagConfigColumnID } from '../page/common/ag-grid/grid-column-ids';
import { AssetConfigColumnID } from '../definitions';

const assetConfig = new Pages.AssetConfig();
const util = new Utils();
const apiSpy = new ApiStub();
const agGridCommon = new UserAggridCommon();

export class AssetConfigUser extends User {
  openTab(tab: 'Assets' | 'Tags' | 'Attributes' | 'Attachments') {
    assetConfig.openTab(tab);
    cy.wait(400);
  }

  addNewTag(tagName: string, description: string, index = 0) {
    assetConfig.addTagBtn().click().wait(1000);

    this.updateTagValue('Name', tagName, -1);
    this.updateTagValue('Description', description, -1);
    assetConfig.saveTagBtn().click();

    return assetConfig.snackbarMsg();
  }

  updateTagValue(columnID: TagConfigColumnID, value: string, index = 0) {
    agGridCommon.inputCellValue(
      value,
      'atx-tag-configuration',
      columnID,
      index
    );
  }

  clearFilter(columnName: string) {
    agGridCommon.clearFilter('atx-tag-configuration', columnName);
  }

  selectAssetByName(columnName: AssetConfigColumnID, value: string) {
    agGridCommon.clickRowbyColumnName(columnName, value);
  }

  createAsset(assetObj: {
    assetName: string;
    description?: string;
    assetType?: string;
  }) {
    assetConfig.clickCreateAssetBtn();
    assetConfig.inputAssetName(assetObj.assetName);
    assetConfig.inputDescription(assetObj.description);
    assetConfig.selectAssetType(assetObj.assetType);
    assetConfig.clickCreateAssetDialogBtn();
    this.refreshAssetNav();
  }

  deleteAsset(assetName: string) {
    assetConfig.clickDeleteAssetBtn();
    assetConfig.clickDeleteConfirm();
  }
}
