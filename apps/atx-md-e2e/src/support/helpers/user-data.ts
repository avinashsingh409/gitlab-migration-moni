import * as path from 'path';

const localUrl = path.join(process.cwd(), './apps/atx-md/src/environments/');
const userObj = {
  default: {
    email: 'testautomationuser1@atonixtest.com',
    password: 'Passw0rd1!',
  },
  accessDeniedUser: {
    email: 'nino@test.com',
    password: 'Passw0rd1!',
  },
  testAccount1: {
    email: 'nino2@test.com',
    password: 'Passw0rd1!',
  },
};

// const defaultUrl = 'https://d2dm3odpcgy2ww.cloudfront.net/';
const defaultUrl = 'https://test.atonix.com/MD';
const url = {
  test: defaultUrl,
  local: 'http://localhost:4200/',
  dev: 'https://dev.atonix.com/MD',
  stage: 'https://stage.atonix.com/MD',
};

const envToTest =
  Cypress.env().env !== undefined
    ? Cypress.env(Cypress.env().env)
    : Cypress.env().local;

const getTestRigUrl = (testRigName: string) => {
  const urls = {
    alerts: 'alerts/index.html',
    issues: 'work-management/index.html',
  };

  return `${defaultUrl}${urls[testRigName]}`;
};

enum AppNamesNewUI {
  alerts = 'Alerts',
  issues = 'IM',
  dataExplorer = 'DE',
  dashboards = 'Dashboard',
  modelConfig = 'ModelConfig',
  opMode = 'OpMode',
  assetConfig = 'AssetConfig',
  userAdmin = 'UserAdmin',
}

enum NavType {
  tile,
  navBar,
}

type ActionType =
  | 'ActionDiagnose'
  | 'Alert'
  | 'ActionWatch'
  | 'ActionMaintenance';

type FlyoutActionButtons =
  | 'Diagnose'
  | 'Model Maintenance'
  | 'Clear Diagnose'
  | 'Clear Maintenance'
  | 'Add Note'
  | '6 Hours'
  | '24 Hours'
  | '7 Days'
  | 'Custom'
  | 'Clear Status';

type GridNames =
  | 'AlertsGrid'
  | 'IssuesGrid'
  | 'TagGrid'
  | 'HistoryGrid'
  | 'ModelListGrid'
  | 'TagListGrid';

type UserAppAccess =
  | 'Alerts'
  | 'IM'
  | 'IMAdmin'
  | 'IMDiscussion'
  | 'DE'
  | 'Dashboard'
  | 'DashboardView'
  | 'DashboardEdit'
  | 'ModelConfig'
  | 'OpMode'
  | 'AssetConfig'
  | 'UserAdmin';

type UserUtilityAccess =
  | 'UserAdmin'
  | 'AssetConfig'
  | 'TagConfig'
  | 'ModelConfig'
  | 'OPModeConfig'
  | 'PinConfig'
  | 'Events';

interface UserObject {
  name?: string;
  email: string;
  password: string;
  appAccess: UserAppAccess[];
  utilityAccess: UserUtilityAccess[];
}

type ImpactCalcFields =
  | 'Production Impact Quantity'
  | 'Production Impact Duration To Date'
  | 'Production Impact Duration Future'
  | 'Derate Override'
  | 'Lost Margin Opportunity'
  | 'Relevant Production Cost'
  | 'Efficiency Impact Quantity'
  | 'Efficiency Duration To Date'
  | 'Efficiency Duration Future'
  | 'Description';

const ChartTypesSingle = ['Line', 'Area', 'Column', 'Bar', 'Table'];

const chartAggregations = [
  'None Selected',
  'Minimum Value Over Range',
  'Maximum Value Over Range',
  'Average Value Over Range',
  'First Value In Range (Ignore Status)',
  'Last Value In Range (Ignore Status)',
  'Sum of Values In Range',
  'Median Value Over Range',
  'Mode Value Over Range',
  'First Value In Range (Honor Status)',
  'Last Value In Range (Honor Status)',
  'Delta',
  'First Standard Deviation Above',
  'Second Standard Deviation Above',
  'First Standard Deviation Below',
  'Second Standard Deviation Below',
  'Percentile',
];

const assetToTest = {
  dev: {
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Unit'],
    },
  },
  test: {
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: [
        'Live MD Test Group',
        'Live MD Test Station',
        'Live MD Test Unit',
      ],
    },
  },
  stage: {
    alerts: {
      parent: 'nD Test Client',
      children: ['Stage', 'Stage Station', 'New Regression Unit'],
    },
    im: {
      parent: 'Asset Health Monitoring',
      children: ['Test Automation Parent (Dont Touch)'],
    },
    de: {
      parent: 'Demo Clients',
      children: ['Coal Plants', 'Eastern Station', 'Eastern PC1'],
    },
    modelConfig: {
      parent: 'nD Test Client',
      children: ['Stage', 'Stage Station', 'New Regression Unit'],
    },
  },
  undefined: {
    //localhost
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: [
        'Live MD Test Group',
        'Live MD Test Station',
        'Live MD Test Unit',
      ],
    },
  },
};

export {
  userObj,
  url,
  envToTest,
  AppNamesNewUI,
  NavType,
  getTestRigUrl,
  ActionType,
  FlyoutActionButtons,
  GridNames,
  UserAppAccess,
  UserUtilityAccess,
  UserObject,
  ImpactCalcFields,
  ChartTypesSingle,
  chartAggregations,
  assetToTest,
};
