import { AppNamesNewUI, NavType, envToTest } from '../helpers/user-data';
import * as Pages from '../page/pages';
import { ApiStub } from './stub/api-stub';
import { UserAggridCommon } from './user-aggrid-common';

const loginPage = new Pages.Login();
const landingPage = new Pages.LandingPage();
const assetNavigator = new Pages.AssetNavigator();
const navigation = new Pages.Navigation();
const apiStub = new ApiStub();
const agGridCommon = new UserAggridCommon();

export class User {
  visit() {
    cy.visit(envToTest);
  }

  logIn(userObj: { email: string; password: string }, preTest = true) {
    if (preTest) this.visit();
    cy.intercept('GET', '**/UserAuthenticated').as('userAuth');
    apiStub.userPreferences();
    loginPage.inputUserEmail(userObj.email);
    loginPage.clickSignInBtn(); // next button
    // cy.wait('@identityProvider');
    loginPage.inputUserPassword(userObj.password);
    loginPage.clickSignInBtn();
    // cy.wait('@userPreferences', { timeout: 15000 });
    // cy.wait('@userAuth', { timeout: 10000 }).then((interception) => {
    //   console.log(`auth response: `, interception);
    // });
    // cy.wait(`@authAws1`);
  }

  navigateToApp(appName: AppNamesNewUI, navigateUsing: NavType) {
    // const appToNavigate = navigateUsing === NavType.tile ?
    //   landingPage.appTiles(appName) : null;
    if (navigateUsing === NavType.tile) {
      landingPage.navigateToApp(appName);
    } else {
      navigation.navigateToApp(appName);
    }
    // cy.wait(2000);
  }

  selectAssetFromNavigator(assetToSelect: {
    parent: string;
    children?: string[];
  }) {
    assetNavigator.selectParent(assetToSelect);
    if (
      assetNavigator.selectChild !== undefined ||
      assetToSelect.children.length !== 0
    ) {
      assetNavigator.selectChild(assetToSelect.children);
    }
  }

  selectChildAssetFromNavigator(asset: string[]) {
    assetNavigator.selectChild(asset);
  }

  showHideAssetNavigator() {
    navigation.navigationPane.clickAssetNavigatorBtn();
  }

  refreshAssetNav() {
    assetNavigator.refreshAssetNav();
  }

  logOut() {
    landingPage.openUserSettings();
    landingPage.getLogOutBtn().click();
  }

  openAccountSettings() {
    landingPage.openUserSettings();
    landingPage.getAccountBtn().click();
  }

  selectGridItemByIdx(
    rowIdx: number,
    gridName: 'opmode-list' | 'opmode-tag' | 'tag-list' | 'model-list'
  ) {
    return agGridCommon.selectGridItemByIdx(rowIdx, gridName);
  }
  // dragAndDrop(elemToDropAlias: string, targetElemAlias) {}
}
