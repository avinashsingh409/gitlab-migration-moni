/* eslint-disable cypress/no-unnecessary-waiting */
import { User } from './user';
import { Utils } from './utils';
import { ApiStub } from './stub/api-stub';
import * as Pages from '../page/pages';

const dataExplorer = new Pages.DataExplorerHome();
const util = new Utils();
const apiSpy = new ApiStub();

export class DataExplorerUser extends User {
  saveChart(newChartName?: string) {
    apiSpy.tagDataFiltered();
    dataExplorer.getSaveChartBtn().click({ force: true });
    if (newChartName !== undefined)
      dataExplorer.getChartNameFld().clear().type(newChartName);
    dataExplorer.getSavePopupSaveBtn().click();
    cy.wait('@tagDataFiltered');
  }

  createNewCreate(chartType: 'Line' | 'Area' | 'Column' | 'Bar' | 'Table') {
    apiSpy.tagDataFiltered();
    dataExplorer.getCreateNewChartBtn().click({ force: true });
    dataExplorer.getNewSeriesChart().click();
    dataExplorer.getChartTypesDropDownBtn().click();
    dataExplorer.getChartTypesList().contains(chartType);
    cy.wait('@tagDataFiltered', { timeout: 30000 });
  }

  openTab(
    tabName:
      | 'Series'
      | 'Tags List'
      | 'Axis'
      | 'Static Curves'
      | 'Pins'
      | 'Advanced'
  ) {
    dataExplorer.getTab(tabName).click();
  }

  inputSeriesName(seriesName: string, idx = 0) {
    dataExplorer.getSeriesNameFds().eq(idx).clear().type(seriesName);
  }

  addTagToChart(tagName: string) {
    //
  }
}
