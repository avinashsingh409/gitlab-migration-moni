/* eslint-disable cypress/no-unnecessary-waiting */
import { User } from './user';
import { UserAggridCommon } from './user-aggrid-common';
import * as Pages from '../page/pages';
import { IssuesColumnID } from '../page/common/ag-grid/grid-column-ids';
import { Utils } from './utils';
import { ImpactCalcFields, UserObject } from './user-data';
import { ApiStub } from './stub/api-stub';

const util = new Utils();
const apiSpy = new ApiStub();
const userAggridCommon = new UserAggridCommon();
const issueSnapshot = new Pages.IssuesSnapshot();
const impactCalc = new Pages.ImpactCalculatorPage();

export class IssuesUser extends User {
  showPanel(panelName: 'column' | 'filter' | 'view') {
    userAggridCommon.showPanel(panelName);
    cy.wait(400);
    return this.moveMouseToFixLayout();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    userAggridCommon.setColumns(colObj);
    this.moveMouseToFixLayout();
  }

  saveView(viewName: string) {
    userAggridCommon.saveView(viewName);
    this.moveMouseToFixLayout();
  }

  moveColumn(columnToMove: IssuesColumnID, targetColumn: IssuesColumnID) {
    userAggridCommon.moveColumn(columnToMove, targetColumn);
    this.moveMouseToFixLayout();
  }

  deleteSavedView(savedViewName: string) {
    userAggridCommon.deleteSavedView(savedViewName);
    this.moveMouseToFixLayout();
  }

  deleteAllSavedViews() {
    userAggridCommon.deleteAllSavedViews();
    this.moveMouseToFixLayout();
  }

  pinColumn(columnID: IssuesColumnID, direction: 'Left' | 'Right') {
    userAggridCommon.pinColumn(columnID, direction);
    this.moveMouseToFixLayout();
  }

  /**
   * this method is to move the mouse to the expand screeningview button
   * to display the screeningview properly due to issue happening only in automation.
   */
  moveMouseToFixLayout() {
    return cy.get(`atx-donut-chart`).eq(2).trigger('mousemove');
  }

  restoreGridToDefaults() {
    userAggridCommon.restoreGridToDefaults();
    this.moveMouseToFixLayout();
  }

  selectSavedView(viewName: string) {
    cy.wait(800);
    util.loadAgGridForWait('IssuesGrid');
    userAggridCommon.selectSavedView(viewName);
    // cy.wait('@IssuesGrid', { timeout: 15000 });
    this.moveMouseToFixLayout();
    cy.wait(800);
  }

  clickFloatingFilterBtn() {
    return userAggridCommon.clickToggleFilterBtn();
  }

  /**
   * used to input values in the Filter header, need to open filter head using toggle filter button
   * @columnName is the actual name of the column in the ag grid
   */
  inputFilterValue(value: string, columnName: string) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('IssuesGrid');
    userAggridCommon.inputFloatingFilterValue(value, columnName);
    this.moveMouseToFixLayout();
    cy.wait('@IssuesGrid');
  }

  sortColumn(colId: IssuesColumnID) {
    util.loadAgGridForWait('IssuesGrid');
    userAggridCommon.clickColumn(colId);
    // cy.wait('@IssuesGrid');
    cy.wait(2000);
    this.moveMouseToFixLayout();
  }

  groupByColumn(colId: IssuesColumnID) {
    userAggridCommon.groupByColumn(colId);
    this.moveMouseToFixLayout();
  }

  expandGroup(groupName: string) {
    userAggridCommon.expandGroup(groupName);
    this.moveMouseToFixLayout();
  }

  createIssue() {
    apiSpy.createNewIssue();
    apiSpy.loadAssetNavIssues();
    cy.wait(2000);
    issueSnapshot.getCreateIssueBtn().should('be.enabled');
    issueSnapshot.getCreateIssueBtn().click();
    cy.wait('@createNewIssue');
    cy.wait('@loadNodeFromKeyIssues', { timeout: 20000 });
  }

  closeSnackBar() {
    issueSnapshot.getSnackBarCloseBtn().click().wait(1000);
  }

  addDiscussionEntry(entryObj: { title: string; discussionBody: string }) {
    cy.intercept(
      `/Services/api/Discussions/DiscussionEntryWithAttachments*`
    ).as(`addDiscussionEntry`);
    issueSnapshot.addNewEntryBtn().click();
    issueSnapshot.discussionEntryTitle().clear().type(entryObj.title);
    util.switchToIframe('iframe').type(entryObj.discussionBody);
    issueSnapshot.saveDiscussionEntryBtn().click();
    cy.wait('@addDiscussionEntry');
  }

  inputIssueTitle(title: string) {
    issueSnapshot.getIssueTitle().clear().type(title);
  }

  saveIssue() {
    issueSnapshot.getSaveIssueBtn().click();
  }

  followIssue() {
    issueSnapshot.getFollowBtn().click();
  }

  openManageFollowers() {
    apiSpy.getIssueFollowers();
    issueSnapshot.getMoreOptionsBtn().click();
    issueSnapshot.getManageFollowersBtn().click();
    cy.wait('@getIssueFollowers');
  }

  addFollowers(followers: UserObject[]) {
    followers.forEach((follower) => {
      issueSnapshot.inputFollower(follower.email);
      issueSnapshot.selectFollowerList(follower.name);
      issueSnapshot.getAddFollowersBtn().click();
      cy.wait(400);
    });
  }

  saveManageFollowers(saveAndClose = false) {
    apiSpy.manageIssueFollowers();
    if (!saveAndClose) issueSnapshot.getManageSaveBtn().click();
    else issueSnapshot.getManageSaveClose().click();
    cy.wait('@manageIssueFollowers');
    cy.get(`mat-dialog-container button`).contains('OK').click();
  }

  removeFollower(follower: UserObject) {
    issueSnapshot.clickFollowingIssueByName(follower.name);
  }

  /**
   * This method will input, select, and add group in the manage issue followers
   * @param groupName Name of the group
   */
  addFollowersGroup(groupName: string) {
    issueSnapshot.inputFollower(groupName);
    issueSnapshot.selectFollowerList(groupName);
    issueSnapshot.getAddFollowersBtn().click();
    cy.wait(1000);
  }

  openImpactCalculator() {
    issueSnapshot.getImpactCalcBtn().click();
  }

  inputImpactField(
    fieldName: ImpactCalcFields,
    value: any,
    scenarioIdx?: number
  ) {
    impactCalc.inputField(fieldName, value, scenarioIdx);
    cy.wait(300);
  }

  removeScenario(scenarioIdx) {
    impactCalc.getRemoveScenarioBtn().eq(scenarioIdx).click();
  }

  saveImpactCalc() {
    // apiSpy.assetIssueImpactTypeMapTotals();
    impactCalc.getSaveBtn().click();
    // cy.wait('@assetIssueImpactTypeMapTotals');
    cy.wait(1000);
  }

  selectGridItemByRowIndex(rowIdx) {
    userAggridCommon.selectGridItemByRowIndex(rowIdx);
  }

  multiSelectGridItemByRowIndex(rowIdx) {
    userAggridCommon.multiSelectGridItemByRowIndex(rowIdx);
  }
}
