import { envToTest } from './user-data';
import { Utils } from '../../support/helpers/utils';

const util = new Utils();

export class CyRequests {
  opMode(optionsParam) {
    const url = util.getUrl();
    optionsParam.url = `${url}api/models/opmode`;

    return cy.request(optionsParam);
  }
}
