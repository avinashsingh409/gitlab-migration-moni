// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Chainable<Subject> {
    login(email: string, password: string): void;
    saveLocalStorage(): void;
    restoreLocalStorage(): void;
  }
}
//
// -- This is a parent command --
Cypress.Commands.add('login', (email, password) => {
  console.log('Custom command example: Login', email, password);
});

const LOCAL_STORAGE_MEMORY = {};

function localStorageRestore() {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach((key) => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
    // console.log(`localStorage[key]1111: `, key);
  });
}

Cypress.Commands.add('saveLocalStorage', () => {
  Object.keys(localStorage).forEach((key) => {
    // console.log(` localStorage[key]::: `, localStorage[key]);
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add('restoreLocalStorage', () => {
  // console.log(`LOCAL_STORAGE_MEMORY::: `, LOCAL_STORAGE_MEMORY);
  Object.keys(LOCAL_STORAGE_MEMORY).forEach((key) => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
    // console.log(`localStorage[key]::: `, key);
  });
});

//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.on('uncaught:exception', (err, runnable) => {
  // for some reason running test in Test Envi result with this error.
  // Ignoring this cypress specific error for now
  if (err.message.includes(`Cannot set property 'status' of undefined`))
    return false;
  if (err.message.includes(`setting 'status'`)) return false;
  if (
    err.message.includes(
      `Cannot read properties of undefined (reading 'length')`
    )
  )
    return false;
});

Cypress.on('window:before:load', () => {
  if (Cypress.env('restoreLocalStorage')) {
    localStorageRestore();
  }
});

Cypress.on('url:changed', () => {
  if (Cypress.env('restoreLocalStorage')) {
    localStorageRestore();
  }
});

Cypress.on('before:url:changed', () => {
  if (Cypress.env('restoreLocalStorage')) {
    localStorageRestore();
  }
});

Cypress.on('window:load', () => {
  if (Cypress.env('restoreLocalStorage')) {
    localStorageRestore();
  }
});
