// import { Helper } from '../../helpers/helper';

// const helper = new Helper();
// const parentNode = $$(`#nodeContainer .assetTreeNode.top-level`);

export class AssetNavigator {
  assetNavAdHocSelector = () => cy.get(`[data-cy="assetNavAdHocSelector"]`);
  selectParent(assetToSelect: { parent: string; children?: string[] }) {
    const noChildren = assetToSelect.children.length === 0 ? true : false;
    // const parentToSelectOLD = noChildren ? element(by.xpath(`//div[@class="assetTreeNodeText"]/span[text()="${assetToSelect.parent}"]/..`))
    //   : element(by.xpath(`//div[@class="assetTreeNodeText"]/span[text()="${assetToSelect.parent}"]`
    //     + `/../preceding-sibling::div[@class="assetTreeNodeExpander"]/mat-icon`));
    const parentToSelect = noChildren
      ? cy
          .get(`div[class="assetTreeNodeText"]`)
          .contains(`${assetToSelect.parent}`)
          .parent()
      : cy
          .get(`div[class="assetTreeNodeText"]`)
          .contains(`${assetToSelect.parent}`)
          .parent()
          .siblings(`.assetTreeNodeExpander`);

    // helper.waitAndClick(parentToSelect);
    parentToSelect.click();

    // if (noChildren === false) { helper.waitForVisible(parentToSelect.$(`[data-icon="caret-down"]`)); }
    // if (noChildren === false) { helper.waitForVisible(parentToSelect.element(by.xpath(`//mat-icon[text()="arrow_drop_down"]`))); }

    return parentToSelect;
  }

  selectChild(clientNames: string[]) {
    for (const client of clientNames) {
      if (clientNames.indexOf(client) === clientNames.length - 1) {
        cy.contains(`div[class="assetTreeNodeText"] > span`, `${client}`, {
          timeout: 20000,
        })
          .parent(`.assetTreeNodeText`)
          .click();
      } else {
        cy.contains(`div[class="assetTreeNodeText"] > span`, `${client}`, {
          timeout: 20000,
        })
          .parent(`.assetTreeNodeText`)
          .siblings(`.assetTreeNodeExpander`)
          .click();
      }
    }
  }

  refreshAssetNav() {
    cy.get(`[data-cy="assetNavRefreshBtn"]`).click();
  }

  selectAdHoc(adHocName: string, defaultTreeName = `Default Asset Tree`) {
    cy.get(`[data-cy="assetNavAdHocSelector"]`)
      .contains(defaultTreeName)
      .click();

    cy.get(`[data-cy="adHocOptions"]`).contains(adHocName).click();
  }
}
