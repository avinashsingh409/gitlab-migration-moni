import { AppNamesNewUI } from '../../helpers/user-data';

export class LandingPage {
  getAccessDenied = () => cy.get(`[data-cy="accessDenied"]`);
  getLogOutBtn = () => cy.get('atx-user-box button').contains('Logout');
  getAccountBtn = () => cy.get('button').contains('Account');

  appTiles = (appTitle?: AppNamesNewUI) => {
    const appTitles = {
      alerts: cy.get(`mat-card[routerlink="/alerts"]`),
      issues: cy.get(`mat-card[routerlink="/issues"]`),
      dataExplorer: cy.get(`mat-card[routerlink="/data-explorer"]`),
      dashboards: cy.get(`mat-card[routerlink="/dashboards"]`),
    };
    // return appTitles[AppNamesNewUI[appTitle]];
    return appTitles;
  };

  navigateToApp = (appTitle: AppNamesNewUI) => {
    // const appTitles = {
    //   alerts: cy.get(`mat-card[routerlink="/alerts"]`),
    //   issues: cy.get(`mat-card[routerlink="/issues"]`)
    // };
    const to = { timeout: 10000 };
    switch (appTitle) {
      case AppNamesNewUI.alerts:
        cy.get(`mat-card[routerlink="/alerts"]`, to).click();
        break;
      case AppNamesNewUI.issues:
        cy.get(`mat-card[routerlink="/issues"]`, to).click();
        break;
      case AppNamesNewUI.dataExplorer:
        cy.get(`mat-card[routerlink="/data-explorer"]`).click();
        break;
      default:
        break;
    }
  };

  openUserSettings() {
    cy.get(`[data-cy="userSettingsBtn"]`).click();
  }

  appTileExists(appName: AppNamesNewUI) {
    switch (appName) {
      case AppNamesNewUI.alerts:
        return Cypress.$(`mat-card[routerlink="/alerts"]`).length > 0
          ? true
          : false;
      case AppNamesNewUI.issues:
        return Cypress.$(`mat-card[routerlink="/issues"]`).length > 0
          ? true
          : false;
      case AppNamesNewUI.dataExplorer:
        return Cypress.$(`mat-card[routerlink="/data-explorer"]`).length > 0
          ? true
          : false;
      case AppNamesNewUI.dashboards:
        return Cypress.$(`mat-card[routerlink="/dashboards"]`).length > 0
          ? true
          : false;
      default:
        break;
    }
  }
}
