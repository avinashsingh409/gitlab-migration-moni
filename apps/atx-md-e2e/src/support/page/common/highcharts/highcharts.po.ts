export class HighCharts {
  getYAxis = () => cy.get(`.highcharts-axis.highcharts-yaxis`);
  getYAxisLabel = () => this.getYAxis().find('text.highcharts-axis-title');
  getlegends = () => cy.get(`.highcharts-legend.highcharts-no-tooltip text`);
  getChartSeries = () => cy.get(`g.highcharts-series`);
}
