import { FlyoutActionButtons } from '../../helpers/user-data';

export class AlertsFlyOutWindow {
  getflyOutWindowContainer = () => cy.get(`[data-cy="flyoutWindow"]`);
  getNoModelText = () => cy.get(`.noModel`);
  headerBtns = {
    getRefreshBtn: () => cy.get(`[data-cy="refreshBtn"]`),
    getCloseBtn: () => cy.get(`[data-cy="flyOutCloseBtn"]`),
  };
  actionBtns = {
    diagnose: () => cy.get(`[data-cy="diagnoseBtn"]`),
    clearDiagnose: () => cy.get(`[data-cy="clearDiagnoseBtn"]`),
    modelMaintenance: () => cy.get(`[data-cy="modelMaintenanceBtn"]`),
    clearModelMaintenance: () => cy.get(`[data-cy="clearModelMaintenanceBtn"]`),
    addNote: () => cy.get(`[data-cy="addNoteBtn"]`),
  };
  diagnoseNoteFld = () => cy.get(`#diagnoseNote`);
  modelMaintenanceNoteFld = () => cy.get(`#maintenanceNote`);
  addNoteFld = () => cy.get(`#noteNote`);

  selectActionBtn = (actionType: FlyoutActionButtons) => {
    switch (actionType) {
      case 'Diagnose':
        this.actionBtns.diagnose().click();
        break;
      case 'Clear Diagnose':
        this.actionBtns.clearDiagnose().click();
        break;
      case 'Model Maintenance':
        this.actionBtns.modelMaintenance().click();
        break;
      case 'Clear Maintenance':
        this.actionBtns.clearModelMaintenance().click();
        break;
      case 'Add Note':
        this.actionBtns.addNote().click();
        break;
      default:
        break;
    }
  };

  diagnoseStatusIcon = () => cy.get(`#modelConfigActions [svgIcon="diagnose"]`);
  maintainenceStatusIcon = () =>
    cy.get(`#modelConfigActions [svgIcon="maintainence"]`);
  watchStatusIcon = () => cy.get(`#modelConfigActions [svgIcon="watch"]`);
}
