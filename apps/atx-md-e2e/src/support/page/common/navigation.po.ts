// This was copied from protractor and will be updating when needed

import { AppNamesNewUI } from '../../helpers/user-data';

export class Navigation {
  leftNav = {
    expandLeftNavBtn: () => cy.get(`[data-cy="expandLeftNavBtn"]`),
    toggleTimeSliderBtn: () => cy.get(`[data-cy="toggleTimeSliderBtn"]`),
    getApps: (appName: AppNamesNewUI) => {
      switch (appName) {
        case AppNamesNewUI.alerts:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/alerts"]`);
        case AppNamesNewUI.issues:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/issues"]`);
        case AppNamesNewUI.dataExplorer:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/data-explorer"]`);
        case AppNamesNewUI.dashboards:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/dashboards"]`);
        case AppNamesNewUI.modelConfig:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/model-config"]`);
        case AppNamesNewUI.assetConfig:
          return cy.get(`[data-cy="navBarAppBtns"][href*="/asset-config"]`);
        default:
          break;
      }
    },
  };

  navigateToApp = (appName: AppNamesNewUI) => {
    this.leftNav.getApps(appName).click();
  };

  navigationPane = {
    // navItems: $$(`[role="navigation"] > mat-list-item`),
    clickAssetNavigatorBtn: () => {
      cy.get(`[data-cy="assetNavigatorBtn"]`).click();
    },
    // homeBtn: $(`${this.navigationStr}`)
    //   .element(by.cssContainingText(`.mat-list-item`, `Home`)),
  };
}
