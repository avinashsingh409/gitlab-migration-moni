import { AgGridClass } from '../../../../definitions';

export class FloatingFilter {
  getFloatingFilterRow() {
    return cy.get(`.ag-header-row-column-filter`);
  }

  inputFilterValue(value: string, columnName: string, gridName?: AgGridClass) {
    let gridClass = '';

    switch (gridName) {
      case 'opmode-list':
        gridClass = '.opmode-list-grid';
        break;
      case 'opmode-tag':
        gridClass = '.op-mode-tag-list-container';
        break;
      case 'model-list':
        gridClass = '.model-list-grid';
        break;
      case 'tag-list':
        gridClass = '.model-config-tag-list-grid';
        break;
      case 'tag-list-config':
        gridClass = '.tag-grid-container';
        break;
      case 'tag-map-config':
        gridClass = '.tagmap-grid-container';
        break;
      default:
        break;
    }

    const elemSelector = `${gridClass} .ag-floating-filter-body [aria-label="${columnName} Filter Input"]`;
    return cy.get(elemSelector).clear().type(value);
  }

  selectFilterValue(value: string, columnName: string, gridName?: AgGridClass) {
    let gridClass = '';

    switch (gridName) {
      case 'opmode-list':
        gridClass = '.opmode-list-grid';
        break;
      case 'opmode-tag':
        gridClass = '.op-mode-tag-list-container';
        break;
      case 'model-list':
        gridClass = '.model-list-grid';
        break;
      case 'tag-list':
        gridClass = '.model-config-tag-list-grid';
        break;
      case 'tag-list-config':
        gridClass = '.tag-grid-container';
        break;
      case 'tag-map-config':
        gridClass = '.tagmap-grid-container';
        break;
      default:
        break;
    }

    const elemSelector = `${gridClass} .ag-floating-filter-body [aria-label="${columnName} Filter Input"]`;
    cy.get(elemSelector).click({ force: true });
    cy.get(`.ag-filter .ag-set-filter-item`).contains('(Select All)').click();
    return cy.get(`.ag-filter .ag-set-filter-item`).contains(value).click();
  }
}
