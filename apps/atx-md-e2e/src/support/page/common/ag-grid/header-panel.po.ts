import { FloatingFilter } from './column-header/column-floating-filter.po';

const floatingFilterPane = new FloatingFilter();

export class HeaderPanel {
  getTimeStamp() {
    return cy.get(`[name="timeStamp"]`);
  }

  clickToggleFloatingFilterBtn() {
    return cy.get(`[data-cy="toggleFilterBtn"]`).click();
  }

  getFilterChips() {
    return cy.get(`[data-cy="filter-chip-list"] mat-chip`);
  }
}
