/* eslint-disable cypress/no-unnecessary-waiting */
export class SideColumnPanel {
  // sideBtnColumn = $(`.ag-side-button .ag-icon-column`);
  // columnSideBarElements = $$(`.ag-column-select-column`);
  // showHideAllButn = $(`.ag-column-select-header-checkbox input.ag-checkbox-input`);
  // private searchColumnTxt = $(`.ag-column-select-header-filter-wrapper input`);
  // rowGroupColumnDropList = $$(`.ag-column-drop-vertical-list`).get(0);
  // rowGroupColumnItemList = $$(`.ag-column-drop-vertical-cell`);

  // valueDropList = $$(`.ag-column-drop-vertical-list`).get(1);

  showPanel() {
    cy.get('[data-cy="sideButtonColumn"]').parent().parent().click();
    cy.wait(500);
    // cy.get(`.ag-side-button .ag-icon-column`).click();
  }
  clickShowHideBtn() {
    cy.get(`.ag-column-select-header-filter-wrapper input`).click();
    cy.get(`.ag-column-select-header-checkbox input.ag-checkbox-input`).click();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    this.clickCheckbox(colObj.toShow, 'Show');
    this.clickCheckbox(colObj.toHide, 'Hide');
  }

  clickCheckbox(columnArr: string[], checkType: 'Show' | 'Hide') {
    if (columnArr !== undefined) {
      columnArr.forEach((col) => {
        cy.get(
          `div.ag-column-select-list div[class*="ag-column-select-column"] span.ag-column-select-column-label`
        )
          .contains(col)
          .then(($el) => {
            cy.wrap($el)
              .parent()
              .find(`div[class*="ag-checkbox-input-wrapper"]`)
              .then(($el2) => {
                if (
                  !$el2.attr('class').includes('ag-checked') &&
                  checkType === 'Show'
                ) {
                  cy.wrap($el2).click();
                }
                if (
                  $el2.attr('class').includes('ag-checked') &&
                  checkType === 'Hide'
                ) {
                  cy.wrap($el2).click();
                }
              });
          });
      });
    }
  }

  /**
   * This method will check/assert if a checkbox is checked or not.
   * @param columnName column name to assert
   * @param expectedValueBol Checked = true | false
   */
  assertCheckboxStatus(
    columnName: string,
    expectedCheckedStatus: true | false = true
  ) {
    let columnCheckboxStatus: any;
    cy.get(
      `div.ag-column-select-list > div[class*="ag-column-select-column"] span.ag-column-select-column-label`
    )
      .contains(columnName)
      .then(($el) => {
        cy.wrap($el)
          .parent()
          .find(`div[class*="ag-checkbox-input-wrapper"]`)
          .then(($el2) => {
            columnCheckboxStatus = $el2.attr('class').includes('ag-checked')
              ? true
              : false;
            expect(columnCheckboxStatus).equal(
              expectedCheckedStatus,
              `${columnName.toUpperCase()} checkbox should be ${expectedCheckedStatus}`
            );
          });
      });
  }

  getColumnNames(checked?: true | false) {
    const tempArr = [];
    switch (checked) {
      case true:
        cy.get(
          `.ag-column-select-list .ag-column-select-column div[class*="ag-checked"]`
        )
          .parent()
          .parent()
          .find('.ag-column-select-column-label')
          .each(($el, index) => {
            tempArr.push($el.text());
          });
        return tempArr;
      case false:
        cy.get(
          `.ag-column-select-list .ag-column-select-column div[class*="ag-checkbox-input-wrapper"]`
        ).each(($el, index1) => {
          if (!$el.attr('class').includes('ag-checked')) {
            cy.wrap($el)
              .parent()
              .parent()
              .find('.ag-column-select-column-label')
              .each(($el2, index2) => {
                tempArr.push($el2.text());
              });
          }
        });
        return tempArr;
      default:
        cy.get(`.ag-column-select-list .ag-column-select-column div`)
          .parent()
          .parent()
          .find('.ag-column-select-column-label')
          .each(($el, index) => {
            tempArr.push($el.text());
          });
        return tempArr;
    }
  }

  getCheckedColumnNames() {
    return this.getColumnNames(true);
  }

  isSelected() {
    return Cypress.$(`[class="ag-side-button ag-selected"] [title="My Views"]`)
      .length > 0
      ? true
      : false;
  }
}
