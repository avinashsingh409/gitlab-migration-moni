export class SideViewsPanel {
  showPanel() {
    cy.get('[data-cy="sideButtonMyViews"]').parent().parent().click();
  }

  saveView(viewName: string) {
    cy.get(`[data-cy="saveViewFld"]`).type(viewName);
    cy.get(`[data-cy="saveBtn"]`).click();
    // eslint-disable-next-line cypress/no-unnecessary-waiting
  }

  getSavedListItems() {
    return cy.get(`[data-cy="savedViewsListItem"]`, { timeout: 3000 });
  }

  getSelectedView() {
    return cy
      .get(`[data-cy="savedViewsListItem"]`)
      .get('[color="accent"]')
      .parent();
  }

  getSavedViewCount() {
    return Cypress.$(`[data-cy="savedViewsListItem"]`).length;
  }

  getSaveViewPanel() {
    return cy.get(`[class="views-panel"]`);
  }

  clickRestoreToDefaultBtn() {
    cy.get(`[data-cy="resetToDefaultBtn"]`).click();
  }

  isSelected() {
    return Cypress.$(`[class="ag-side-button ag-selected"] [title="My Views"]`)
      .length > 0
      ? true
      : false;
  }

  selectSavedView(viewName: string) {
    cy.get(`[data-cy="savedViewsListItem"]`)
      .contains(viewName)
      .siblings('[data-cy="checkbox"]')
      .click();
  }
}
