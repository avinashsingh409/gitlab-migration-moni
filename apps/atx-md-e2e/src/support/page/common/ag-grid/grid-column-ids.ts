export type AlertsColumnID =
  | 'ActionDiagnose'
  | 'Alert'
  | 'ActionWatch'
  | 'Issues'
  | 'NotePriority'
  | 'UnitAbbrev'
  | 'ActionMaintenance'
  | 'ModelName'
  | 'PercentOutOfBounds'
  | 'PercentExpected'
  | 'Actual'
  | 'LowerLimit'
  | 'Expected'
  | 'UpperLimit'
  | 'EngUnits'
  | 'LastNote'
  | 'LastNoteDate'
  | 'PrioritizedOpMode'
  | 'Score'
  | 'AlertTypesActive'
  | 'ModelTypeAbbrev'
  | 'AddedToAlerts'
  | 'TagGroup'
  | 'TagName'
  | 'AssetCriticality'
  | 'ModelCriticality'
  | 'Criticality'
  | 'AlertPriority'
  | 'OutOfBounds'
  | 'Ignore'
  | 'StatusInfo'
  | 'AssetGUID'
  | 'AssetClassTypeID'
  | 'VariableTypeID';

export type HistoryColumnID =
  | 'Favorite'
  | 'History'
  | 'ChangeDate'
  | 'Executor'
  | 'Note';

export type IssuesColumnID =
  | 'AssetIssueID'
  | 'Title'
  | 'ImpactTotal'
  | 'ActivityStatus'
  | 'ResolutionStatus'
  | 'CreateDate'
  | 'Scorecard'
  | 'CategoryDesc'
  | 'AssignedTo'
  | 'OpenDuration'
  | 'CreatedBy'
  | 'ChangedBy'
  | 'Priority'
  | 'AssetClassTypeDesc'
  | '';

export type TagListColumnID =
  | 'Asset'
  | 'Variable'
  | 'Name'
  | 'Description'
  | 'Units';

export type ModelConfigColumnID =
  | 'Active'
  | 'Standard'
  | 'ActionMaintenance'
  | 'MaintenanceAge'
  | 'ThirtyDayCountOfActions'
  | 'ModelName'
  | 'ModelTypeAbbrev'
  | 'PrioritizedOpMode'
  | 'Score'
  | 'LowerBoundTolerance'
  | 'UpperBoundTolerance'
  | 'LastBuildStatus'
  | 'LastBuildDate'
  | 'LastUpdateDate'
  | 'TagName'
  | 'EngUnits'
  | 'VariableTypeDesc'
  | 'UnitAbbrev'
  | 'AssetDesc'
  | 'AssetTypeDesc';

export type ModelConfigTagColumnID =
  | 'ModelCount'
  | 'TagName'
  | 'TagDesc'
  | 'EngUnits'
  | 'VariableDesc'
  | 'UnitAbbrev'
  | 'AssetDesc'
  | 'AssetTypeDesc'
  | 'AssetCriticality'
  | 'AssetPath';

export type TagConfigColumnID =
  | 'InUse'
  | 'Name'
  | 'Description'
  | 'EngUnit'
  | 'ExistsOnServer'
  | 'Qualifier'
  | 'Source'
  | 'CreateDate'
  | 'ChangeDate'
  | 'deleteButton';

export type TagMapsColumnID = 'AssetName' | 'VariableDesc';
