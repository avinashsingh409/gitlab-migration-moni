import { AgGridMain } from '../../common/ag-grid/ag-grid-main.po';
import {
  AlertsColumnID,
  HistoryColumnID,
} from '../../common/ag-grid/grid-column-ids';

export class AlertsHistoryGrid extends AgGridMain {
  getHistoryItemByIndex(index: number, appName = 'alerts') {
    let historyGrid: string;
    switch (appName) {
      case 'Alerts':
        historyGrid =
          'atx-model-tabs atx-model-history [role="grid"] div[class="ag-center-cols-container"] [role="row"]';
        break;
      case 'Model Config':
        historyGrid =
          'atx-model-history-grid [role="grid"] [ref="centerContainer"] [role="row"]';
        break;
      default:
        historyGrid =
          'atx-model-tabs atx-model-history [role="grid"] div[class="ag-center-cols-container"] [role="row"]';
        break;
    }

    return cy.get(`${historyGrid} `).eq(index);
  }

  getLatestHistoryItem(appName: 'Alerts' | 'Model Config' = 'Alerts') {
    const historyObj: any[] = [];
    this.getHistoryItemByIndex(0, appName).within(() => {
      const columnIDs: HistoryColumnID[] = [
        'Favorite',
        'History',
        'ChangeDate',
        'Executor',
        'Note',
      ];
      columnIDs.forEach((col) => {
        if (col === 'Note')
          cy.get(`[col-id="${col}"]`).then(($el) => {
            historyObj.push({ [col]: $el.text().trim() });
          });

        cy.get(`[col-id="${col}"]`).then(($el) => {
          historyObj.push({ [col]: $el.text() });
        });
      });
    });
    return historyObj;
  }
}
