import { ApiStub } from '../../helpers/stub/api-stub';

const stub = new ApiStub();

export class AssetConfig {
  addTagBtn = () => cy.get(`[data-cy="addTag"]`);
  undoChangesBtn = () => cy.get(`[data-cy="undoTagChanges"]`);
  saveTagBtn = () => cy.get(`[data-cy="saveTagChanges"]`);
  snackbarMsg = () => cy.get(`atx-snackbar .snackbar-message`);

  openTab(tab: 'Assets' | 'Tags' | 'Attributes' | 'Attachments') {
    let link = '';
    switch (tab) {
      case 'Assets':
        link = 'href="/MD/asset-config/assets"';
        break;
      case 'Tags':
        stub.Tags();
        link = 'href="/asset-config/tags"';
        break;
      case 'Attributes':
        link = 'href="/MD/asset-config/attributes"';
        break;
      case 'Attachments':
        link = 'href="/MD/asset-config/attachments"';
        break;
      default:
        break;
    }

    cy.get(`[${link}]`).click();

    if (tab === 'Tags') cy.wait('@Tags');
  }

  clickCreateAssetBtn() {
    cy.get(`[data-cy="createAssetBtn"]`).click();
  }

  clickDeleteAssetBtn() {
    cy.get(`[data-cy="deleteAssetBtn"]`).click();
  }

  inputAssetName(assetName: string) {
    cy.get(`[title="assetName"]`).type(assetName);
  }

  inputDescription(desc: string) {
    if (desc) cy.get(`[title="description"]`).type(desc);
  }

  selectAssetType(assetType?: string) {
    if (assetType) cy.get(`[title="searchString"]`).type(assetType);

    cy.get(`[data-cy="assetTypeTable"] [data-cy="assetTypeName"]`)
      .eq(0)
      .click();
  }

  clickCreateAssetDialogBtn() {
    cy.get(`[data-cy="createAssetDialogCreateBtn"]`).click();
  }

  clickDeleteConfirm() {
    cy.get(`atx-user-admin-asset-delete-confirmation-dialog button`)
      .contains('Continue')
      .click({ force: true });
  }
}
