import { AlertsHistoryGrid } from '../Alerts/model-tabs/alerts-history-ag-grid.po';
import * as agGrid from '../common/ag-grid/ag-grid';
import { AlertsFlyOutWindow } from '../common/alerts-flyout-window.po';
import { Utils } from '../../../support/helpers/utils';

const util = new Utils();
export class ModelConfigOpModes {
  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new agGrid.AgGridMain();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();

  createNewOpModeBtn = () => cy.get(`[data-cy="createOpModeBtn"]`);
  createDialogTitle = () => cy.get(`[data-cy="createDialogTitle"]`);
  createDialogModeTypeSelect = () => cy.get(`[data-cy="createDialogModeType"]`);
  createDialogModeOptions = () => cy.get(`[data-cy="createDialogModeOptions"]`);
  createDialogCreateBtn = () => cy.get(`[data-cy="createDialogCreateBtn"]`);
  createDialogCancelBtn = () => cy.get(`[data-cy="createDialogCancelBtn"]`);
  opModeName = () => cy.get(`[formcontrolname="opModeName"]`);
  associatedAssetAndTags = () =>
    cy.get(`[data-cy="associatedAssetsAndTags"] .mat-chip-list`);
  excludedAssetAndTags = () =>
    cy.get(`[data-cy="excludedAssetsAndTags"] .opmode-chiplist-container`);
  tagRowItems = () =>
    cy.get(`.op-mode-tag-list-container .ag-center-cols-clipper [role="row"]`);
  discardBtn = () => cy.get(`[data-cy="opModeDiscardBtn"]`);
  saveBtn = () => cy.get(`[data-cy="opModeSaveBtn"]`);
  moreOptionsBtn = () => cy.get(`[data-cy="moreOptionsBtn"]`);
  deleteOpModeBtn = () => cy.get(`[data-cy="deleteOpModeBtn"]`);
  deleteOpModeDialog = {
    title: () => cy.get(`atx-delete-op-mode-modal .mat-dialog-title`),
    cancelBtn: () =>
      cy.get(`atx-delete-op-mode-modal button`).contains('Cancel'),
    deleteBtn: () =>
      cy.get(`atx-delete-op-mode-modal button`).contains('Delete'),
  };
  opModeSaveDiaglogCancelBtn = () => cy.get(`[data-cy="cancelOpModeBtn"]`);
  opModeSaveDiaglogSaveBtn = () => cy.get(`[data-cy="saveOpModeBtn"]`);
  affectedModelsList = () => cy.get(`[data-cy="affectedModelName"]`);

  getCriteriaDetails() {
    const criteriaItems = [];
    //data-cy="criteriaGroup"
    //data-cy="criteriaItem"
    cy.get(`[data-cy="criteriaItem"]`).each(($criteriaItem, criteriaIdx) => {
      const tempCriteria = [];
      const criteriaLogic = [];
      const criteriaGroupId = $criteriaItem
        .find(`[data-cy="criteriaGroup"]`)
        .text()
        .split(':')[1]
        .trim();

      cy.wrap($criteriaItem)
        .find(`[data-cy="criteriaLogic"]`)
        .each(($criteriaLogic, criteriaLogicIdx) => {
          criteriaLogic.push({
            tagDesc: $criteriaLogic
              .find(`[data-cy="criteriaLogicTagInfo"]`)
              .text()
              .trim(),
            sign: $criteriaLogic
              .find(`[data-cy="criteriaLogicSelect"]`)
              .text()
              .trim(),
            value: $criteriaLogic.find(`[data-cy="criteriaLogicValue"]`).val(),
            unit: $criteriaLogic
              .find(`[data-cy="criteriaLogicUnit"]`)
              .text()
              .trim(),
          });
        });
      criteriaItems.push({
        criteriaGroupId,
        logic: criteriaLogic,
      });
    });

    return criteriaItems;
  }

  moveTagToAssociated(elementToMoveAlias: string, elementTargetAlias: string) {
    //
  }

  inputOpModeName(name: string, clear = false) {
    if (clear) this.opModeName().clear();
    return this.opModeName().type(name);
  }
}
