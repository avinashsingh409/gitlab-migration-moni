/* eslint-disable cypress/no-unnecessary-waiting */
import { AlertsHistoryGrid } from '../alerts/model-tabs/alerts-history-ag-grid.po';
import * as agGrid from '../common/ag-grid/ag-grid';
import { AlertsFlyOutWindow } from '../common/alerts-flyout-window.po';
import { Utils } from '../../../support/helpers/utils';
import { AnomaliesConfig, ModelTypes } from '../../definitions';
import { UserAggridCommon } from '../../helpers/user-aggrid-common';

const util = new Utils();
const userAgGrid = new UserAggridCommon();
export class ModelConfig {
  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new agGrid.AgGridMain();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();
  flyoutWindow = new AlertsFlyOutWindow();
  historyGrid = new AlertsHistoryGrid();

  getFilterGridBtn = () => cy.get(`[data-cy="createIssueBtn"]`);
  getRefreshGridBtn = () => cy.get(`[data-cy="refreshGridBtn"]`);
  getDownloadGridBtn = () => cy.get(`[data-cy="downloadGridBtn"]`);
  getModelListBtn = () => cy.get(`[data-cy="modelListBtn"]`);
  getTagListBtn = () => cy.get(`[data-cy="tagListBtn"]`);
  getOpModesBtn = () => cy.get(`[data-cy="opModesBtn"]`);
  getNoModelMessage = () =>
    cy.get(`atx-model-config-action-pane .noModel > h2`);
  createNewModelBtn = () => cy.get(`[data-cy="createNewModelBtn"]`);
  saveModelBtn = () => cy.get(`#saveBtn`);
  modelTypeList = () => cy.get(`[data-cy="modelTypeList"] [role="combobox"]`);
  moreOptionsBtn = () => cy.get(`[data-cy="moreOptionsBtn"]`);
  deleteModelBtn = () => cy.get(`[data-cy="deleteModelBtn"]`);
  noModel = () => cy.get(`.noModel`);

  createNewModelDialog = {
    modelName: () => cy.get(`[data-cy="modelNameFld"]`),
    createModelBtn: () => cy.get(`[data-cy="createModelBtn"]`),
  };

  modelSaveChanges = {
    content: () => cy.get(`atx-single-model-save-changes .mat-dialog-content`),
    saveBtn: () =>
      cy.get(`atx-single-model-save-changes button`).contains('OK'),
    cancelBtn: () =>
      cy.get(`atx-single-model-save-changes button`).contains('Cancel'),
  };

  deleteModelDialog = {
    title: () => cy.get(`atx-model-actions-confirmation-dialog .title`),
    content: () =>
      cy.get(`atx-model-actions-confirmation-dialog .content-body`),
    cancelBtn: () =>
      cy.get(`atx-model-actions-confirmation-dialog button`).contains('Cancel'),
    deleteBtn: () =>
      cy.get(`atx-model-actions-confirmation-dialog button`).contains('Delete'),
  };

  openConfigurationTab(
    tabName:
      | 'Inputs'
      | 'Training Range'
      | 'Sensitivity'
      | 'Slope Analysis'
      | 'Averages'
      | 'Build Status'
      | 'Anomalies'
  ) {
    cy.wait(500);
    return cy
      .get(`[data-cy="configurationSection"]`)
      .contains(tabName)
      .trigger('mousemove')
      .click();
  }

  openTrendTab(
    tabName: 'Model Trend' | 'Expected vs Actual' | 'History' | 'Context'
  ) {
    if (tabName === 'History') util.loadAgGridForWait('HistoryGrid');
    cy.get(`.mat-tab-header [role="tab"] .mat-tab-label-content`)
      .contains(tabName)
      .click();
  }

  selectModelType(modelType: ModelTypes) {
    this.modelTypeList().click().wait(1000);
    cy.get(`mat-option`).contains(modelType).click();
  }

  getContextOpModeDetails(): {
    opModeTitle: string;
    changeDate: string;
    criteria: string;
  }[] {
    const opModes = [];
    cy.get(`[data-cy="contextOpModeDetails"]`).each((opModeDetail) => {
      opModes.push({
        opModeTitle: opModeDetail
          .find(`[ data-cy="contextOpModeTitle"]`)
          .text()
          .trim(),
        changeDate: opModeDetail
          .find(`[ data-cy="contextOpModeChangeDate"]`)
          .text()
          .trim(),
        criteria: opModeDetail
          .find(`[ data-cy="contextOpModeCriteria"]`)
          .text()
          .trim(),
      });
    });

    return opModes;
  }
}
