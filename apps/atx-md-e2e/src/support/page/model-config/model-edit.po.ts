/* eslint-disable cypress/no-unnecessary-waiting */
import * as agGrid from '../common/ag-grid/ag-grid';
import { AlertsFlyOutWindow } from '../common/alerts-flyout-window.po';
import { Utils } from '../../../support/helpers/utils';
import { AlertsNotifiColumn, ConfigurationTabs } from '../../definitions';

const util = new Utils();

export class ModelEdit {
  getModelStats = () => cy.get(`atx-model-stats`);
  getRValue = () => cy.get(`[data-cy="RValueValue"]`);
  getRootMeanSquareError = () => cy.get(`[data-cy="RootMeanSquareError"]`);
  getMeanAbsoluteError = () => cy.get(`[data-cy="MeanAbsoluteError"]`);
  getModelInputElements = () => cy.get(`[data-cy="input-tag"]`);
  alertNotifCustomMsg = () => cy.get(`[data-cy="alertNotifCustomMsg"]`);
  addUsersFld = () => cy.get(`[data-cy="alertNotifAddUsers"]`);
  addUsersList = () => cy.get(`[role="listbox"] .mat-option-text`);
  getAlertsNotifSubscribedUsers = () =>
    cy.get(`.alert-notification-dialog-container .mat-chip`);
  alertsNotificationDialog = () =>
    cy.get(`.alert-notification-dialog-container `);
  saveAlertNotifBtn = () => cy.get(``);
  getAlertsNotifBtn = (columnName: AlertsNotifiColumn) =>
    cy.get(`[col-id="${columnName}"] atx-send-message-icon-renderer mat-icon`);

  openConfigurationTab(tabName: ConfigurationTabs) {
    cy.wait(500);
    return cy
      .get(`[data-cy="configurationSection"]`)
      .contains(tabName)
      .trigger('mousemove')
      .click()
      .wait(500);
  }

  openContentTab(tabName: 'Model Trend' | 'Expected vs Actual' | 'History') {
    cy.wait(800);
    if (tabName === 'History') util.loadAgGridForWait('HistoryGrid');
    return cy
      .get(`.mat-tab-header [role="tab"] .mat-tab-label-content`)
      .contains(tabName)
      .click();
  }

  showHideActionsPane() {
    cy.get(`[data-cy="showHideModelConfigFlyOutWindow"]`).click();
  }

  selectAnomaliesFilter(
    filter:
      | 'All'
      | 'Model Bound Criticality'
      | 'Relative Model Bounds'
      | 'Fixed Limits'
  ) {
    cy.get(`[data-cy="AnomaliesFilter"]`).contains(filter).click();
  }

  selectPredictiveMethod(
    predictMethod: 'Linear Regression' | 'Logistic Regression' | 'Neural Net'
  ) {
    console.log(predictMethod);
    cy.get(`[data-cy="predictMethods"]`)
      .contains(predictMethod)
      .find('mat-checkbox')
      .click();
  }

  getModelInputByTagName(tagName: string) {
    return this.getModelInputElements()
      .find(`[data-cy="modelInputName"]`)
      .contains(tagName)
      .parentsUntil('[data-cy="input-tag"]');
  }

  removeInputTag(tagName: string) {
    this.getModelInputByTagName(tagName).find('[data-cy="removeTag"]').click();
  }

  getNotificationDetails() {
    return {
      alertType: () => cy.get(`[data-cy="alertNotifAlertTypeValue"]`),
      modelName: () => cy.get(`[data-cy="alertNotifModelName"]`),
      tag: () => cy.get(`[data-cy="alertNotifTag"]`),
      asset: () => cy.get(`[data-cy="alertNotifAsset"]`),
    };
  }
}
