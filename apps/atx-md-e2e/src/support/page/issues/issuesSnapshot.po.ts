/* eslint-disable cypress/no-unnecessary-waiting */
import { UserObject } from '../../helpers/user-data';

export class IssuesSnapshot {
  getCreateIssueBtn = () => cy.get(`[data-cy="createIssueBtn"]`);
  getSaveIssueBtn = () => cy.get(`[data-cy="saveIssueBtn"]`);
  getFollowBtn = () => cy.get('[data-cy="followBtn"]');
  getSendBtn = () => cy.get(`[data-cy="sendBtn"]`);
  getCancelBtn = () => cy.get(`[data-cy="cancelIssueBtn"]`);
  getIssueTitle = () => cy.get(`[data-cy="issueTitle"]`);
  getResolutionStatusDropdown = () =>
    cy.get(`[data-cy="resolutionStatusDropdown"]`);
  getAssignedToTxtBox = () => cy.get(`[data-cy="assignedToTxtBox"]`);
  getResolvedByDateTxtBox = () => cy.get(`[data-cy="resolveByDateTxtBox"]`);
  getResolvedByDateBtn = () => cy.get(`[data-cy="resolveByDateBtn"]`);
  getPriorityDropdown = () => cy.get(`[data-cy="priorityDropdown"]`);
  getActivityStatusDropdown = () =>
    cy.get(`[data-cy="activityStatusDropdown"]`);
  getEditAssetBtn = () => cy.get(`[data-cy="editAssetBtn"]`);
  getIssuesByOwningAssetBtn = (timeOut?: number) =>
    cy.get(`[data-cy="issuesByOwningAssetBtn"]`, { timeout: timeOut });
  getAlertsByOwningAssetBtn = () =>
    cy.get(`[data-cy="alertsByOwningAssetBtn"]`);
  getSnackBarCloseBtn = () => cy.get(`snack-bar-container .dismiss`);
  addNewEntryBtn = () => cy.get('[data-cy="addNewEntryBtn"]');
  discussionEntryTitle = () => cy.get('[data-cy="discussionEntryTitle"]');
  saveDiscussionEntryBtn = () => cy.get(`[data-cy="saveDiscussionEntryBtn"]`);
  cancelDiscussionEntryBtn = () =>
    cy.get('[data-cy="cancelDiscussionEntryBtn"]');
  getDiscussionEntries = () => cy.get(`atx-discussion-entry .entry-container`);
  getEntryCreatedBy = () => cy.get(`[data-cy="entryCreatedBy"]`);
  createIssueProgressBar = () => cy.get(`atx-issue-modal mat-progress-bar`);
  getMoreOptionsBtn = () => cy.get(`[data-cy="moreOptionsBtn"]`);
  getChangeCategoryClassBtn = () =>
    cy.get(`[data-cy="changeCategoryAndClassBtn"]`);
  getManageFollowersBtn = () => cy.get(`[data-cy="manageFollowersBtn"]`);
  getDeleteIssueBtn = () => cy.get(`[data-cy="deleteIssueBtn"]`);
  getManageFollowersWindow = () => cy.get(`atx-manage-followers-dialog`);
  getManageCancelBtn = () => cy.get(`[data-cy="manageDialogCancelBtn"]`);
  getManageSaveBtn = () => cy.get(`[data-cy="manageDiaglogSaveBtn"]`);
  getManageSaveClose = () => cy.get(`[data-cy="manageDialogSaveCloseBtn"]`);
  getUserNamesFld = () => cy.get(`[data-cy="userNamesFld"]`);
  getAddFollowersBtn = () => cy.get(`[data-cy="addFollowersBtn"]`);
  getFollowerList = () => cy.get(`.mat-option-text`);
  getFollowerChip = () => cy.get(`mat-chip`);
  getFollowerSaveConfirm = () => cy.get(`.mat-dialog-title`);
  getImpactCalcBtn = () => cy.get(`[data-cy="impactCalcBtn"]`);
  getImpactTotal = () => cy.get(`.impact-right-sub-sections input`).eq(0);
  getMonthlyAverage = () => cy.get(`.impact-right-sub-sections input`).eq(1);

  getDiscussionEntryByCreatedBy = (email: string) => {
    return cy.xpath(
      `//*[@data-cy="entryCreatedBy" and text()="${email}"]/ancestor::div[@data-cy="discussionEntryContainer"]`
    );
  };

  getDiscussionEntryNotByCreatedBy = (email: string) => {
    return cy.xpath(
      `//*[@data-cy="entryCreatedBy" and text()!="${email}"]/ancestor::div[@data-cy="discussionEntryContainer"]`
    );
  };

  /**
   * gets the action button from discussion entry by Email.
   * @param email email of user found in discussion entry
   * @param btn edit | deleteEntryBtn
   * @returns element found. Can be asserted by length
   */
  getDiscussionEntryActionBtns = (email: string, btn: 'edit' | 'delete') => {
    const actionBtn = btn === 'edit' ? 'editEntryBtn' : 'deleteEntryBtn';
    return cy.xpath(
      `//*[@data-cy="entryCreatedBy" and text()="${email}"]/ancestor::div[@data-cy="discussionEntryContainer"]//*[@data-cy="${actionBtn}"]`
    );
  };

  /**
   * Grabs Name, User Email, Group, and Following Issue status
   * from Manage Issue Followers pop up
   * @returns [{name: string, email: string, groups: string[], followingIssue: string}]
   */
  getFollowersDetails() {
    const temp = [];
    cy.get(`[data-cy="followersTbl"] .mat-row`).each(($users) => {
      cy.wrap($users)
        .find(`[data-cy="following"] span[mattooltip]`)
        .invoke('attr', 'mattooltip')
        .then(($val) => {
          const userGroups = [];
          $users.find('[data-cy="userGroup"] mat-chip').each((idx, $group) => {
            userGroups.push($group.textContent);
          });
          temp.push({
            name: $users.find('[data-cy="userName"]').text().trim(),
            email: $users.find('[data-cy="userEmail"]').text().trim(),
            groups: userGroups,
            followingIssue: $val.toString().trim(),
          });
        });
    });
    return cy.wrap(temp);
  }

  inputFollower(follower: string) {
    this.getUserNamesFld().type(follower);
    cy.wait(1000);
  }

  /**
   * Select a user from the drop down in Add Users input box
   * @param username name of the user to be selected from the drop down
   */
  selectFollowerList(username: string) {
    this.getFollowerList().contains(username).click();
  }

  /**
   * Selects the Following Issue icon from the  Manage Issue Followers
   * @param name name of the User
   */
  clickFollowingIssueByName(name: string) {
    cy.xpath(
      `*//td[@data-cy="userName" and contains(text(),"${name}")]/../td[@data-cy="following"]/button`
    ).click();
  }

  /**
   * This will grab the user names displayed in Existing Followers pop up
   * @returns user names in the pop up list
   */
  getExistingFollowers() {
    //#mat-dialog-6 > atx-mf-prompt-dialog > div.mat-dialog-actions > button
    const existingFollowers = [];
    cy.get(`atx-mf-prompt-dialog .mat-list > mat-list-item`).each(($list) => {
      existingFollowers.push($list.text());
    });
    cy.get(`atx-mf-prompt-dialog > div.mat-dialog-actions > button`).click();
    return cy.wrap(existingFollowers);
  }
}
