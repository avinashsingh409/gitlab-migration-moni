/* eslint-disable cypress/no-unnecessary-waiting */
import { ImpactCalcFields } from '../../helpers/user-data';

export class ImpactCalculatorPage {
  getImpactCalcTitle = () => cy.get(`[data-cy="impactCalcTitle"]`);
  getSaveBtn = () => cy.get(`[data-cy="saveBtn"]`);
  getCancelBtn = () => cy.get(`pdata-cy="cancelBtn"`);
  getScenarioTitle = () => cy.get(`[data-cy="scenarioTitle"]`);
  getAddNewScenarioBtn = () => cy.get(`[data-cy="addNewScenario"]`);
  getScenarioDescription = () => cy.get(`[formcontrolname="description"]`);
  getRemoveScenarioBtn = () => cy.get(`[data-cy="removeScenarioBtn"]`);
  getScenarioHeaders = () => cy.get(`[data-cy="scenarioHeader"]`);
  getImpactQuantityFld = () =>
    cy.get(`[data-cy="productionImpactQuantityFld"]`);
  getProductionRestrictionValue = () =>
    cy.get(`[data-cy="productionRestrictionValue"]`);
  // <ImpactCapacityUoM/ImpactUoT>
  getProductionLossUnit = () =>
    cy.get(
      `[data-cy="productionLossSection"] [class="production-display-units"]`
    );
  getProductionLossValue = () => cy.get(`[data-cy="productionLossValue"]`);
  // -- Rated Capacity
  getRatedProductionCapacityUnit = () =>
    cy.get(
      `[data-cy="ratedProductionCapacitySection"] [class="production-display-units"]`
    );
  // <ImpactCapacityValue>
  getProductionCapacityValue = () =>
    cy.get(`[data-cy="ratedProductionCapacityValue"]`);

  getProductionToDateFld = () =>
    cy.get(`[data-cy="productionImpactToDateFld"]`);
  getProductionFutureFld = () =>
    cy.get(`[data-cy="productionImpactFutureFld"]`);
  getProductionTotalDuration = () =>
    cy.get(`[data-cy="productionImpactTotalDurationFld"]`);
  getDerateInEffectToDate = () => cy.get(`[formcontrolname="derateElapsed"]`);
  getDerateInEffectFuture = () => cy.get(`[formcontrolname="deratePending"]`);
  getUserDefinedTotalBtn = () =>
    cy.get(`[data-cy="userDefinedTotalBtn"] button`);
  getDerateOverrideFld = () => cy.get(`[formcontrolname="derateOverride"]`);
  getProductionLossToDate = () => cy.get(`[data-cy="productionLossToDate"]`);
  getProductionLossFuture = () => cy.get(`[data-cy="productionLossFuture"]`);
  getProductionLossTotal = () => cy.get(`[data-cy="productionLossTotal"]`);
  getProductionLossDurationUnit = () =>
    cy.get(`[data-cy="productionLossDurationUnit"] .display-units`);
  getLostMarginOpportunityFld = () =>
    cy.get(`[formcontrolname="lostMarginOpportunity"]`);
  getLostMarginOpportunityUnit = () =>
    cy.get(`[data-cy="lostMarginOpportunityUnit"]`);
  getRelevantProductionCostFld = () =>
    cy.get(`[formControlName="relevantProductionCost"]`);
  getRelevantProductionCostUnit = () =>
    cy.get(`[data-cy="relevantProductionCostUnit"]`);

  getProdImpactMonthlyCostToDate = () =>
    cy.get(`[data-cy="prodImpactMonthlyCostToDate"]`);
  getProdImpactMonthlyCostFuture = () =>
    cy.get(`[data-cy="prodImpactMonthlyCostFuture"]`);
  getProdImpactMonthlyCostTotal = () =>
    cy.get(`[data-cy="prodImpactMonthlyCostTotal"]`);
  getProdImpactTotalCostToDate = () =>
    cy.get(`[data-cy="prodImpactTotalCostToDate"]`);
  getProdImpactTotalCostFuture = () =>
    cy.get(`[data-cy="prodImpactTotalCostFuture"]`);
  getProdImpactTotalCostTotal = () =>
    cy.get(`[data-cy="prodImpactTotalCostTotal"]`);

  // -- EFFICIENCY --
  getEfficiencyImpactQuantity = () =>
    cy.get(`[formcontrolname="effImpactQuantity"]`);
  // <ImpactNomConsRateLabel> Impact
  getNomConsRateImpactLabel = () =>
    cy.get(`[data-cy="ImpactNomConsRateLabelImpact"]`);
  getNomConsRateImpactValue = () =>
    cy.get(`[data-cy="ImpactNomConsRateLabelImpactValue"]`);
  // <ImpactNomConsRateLabel>
  getNomConsRateLabel = () => cy.get(`[data-cy="ImpactNomConsRateLabel"]`);
  getNomConsRateValue = () => cy.get(`[data-cy="ImpactNomConsRateValue"]`);
  getImpactNomConsRateLabelUnit = () =>
    cy.get(
      `[data-cy="ImpactNomConsRateLabelSection"] [data-cy="ImpactNomConsRateUoM"] .display-units`
    );
  // <ImpactNomConsRateLabel> Change
  getImpactNomConsRateLabelChange = () =>
    cy.get(`[data-cy="ImpactNomConsRateLabelChange"]`);
  getImpactNomConsRateLabelChangeValue = () =>
    cy.get(`[data-cy="ImpactNomConsRateLabelChangeValue"]`);
  getImpactNomConsRateLabelChangeUnit = () =>
    cy.get(
      `[data-cy="ImpactNomConsRateLabelChangeSection"] [data-cy="ImpactNomConsRateUoM"] .display-units`
    );
  getImpactConsumableLabel = () => cy.get(`[data-cy="ImpactConsumableLabel"]`);
  getImpactConsumableValue = () => cy.get(`[data-cy="ImpactConsumableValue"]`);
  getImpactCosumableUnit = () => cy.get(`[data-cy="ImpactConsumableUoM"]`);
  // <ImpactCapacityValue>
  getEffRatedProductionCapacity = () =>
    cy.get(`[data-cy="effRatedProductionCapacity"]`);
  ImpactConsumableUoM;
  // <[ImpactCapacityUoM/ImpactUoT>
  getEffRatedProductionCapacityUnit = () =>
    cy.get(`[data-cy="ImpactCapacityUoM/ImpactUoT"] .display-units`);
  // <ImpactNomCapacityUtilization>
  getImpactNomCapacityUtilization = () =>
    cy.get(`[data-cy="ImpactNomCapacityUtilization"]`);

  getEfficiencyToDateFld = () => cy.get(`[formcontrolname="effDaysElapsed"]`);
  getEfficiencyFutureFld = () => cy.get(`[formcontrolname="effDaysPending"]`);
  getEfficiencyDurationTotal = () => cy.get(`[data="effDurationTotal"]`);
  getEffImpactMonthlyCostToDate = () =>
    cy.get(`[data-cy="effImpactMonthlyCostToDate"]`);
  getEffImpactMonthlyCostFuture = () =>
    cy.get(`[data-cy="effImpactMonthlyCostFuture"]`);
  getEffImpactMonthlyCostTotal = () =>
    cy.get(`[data-cy="effImpactMonthlyCostTotal"]`);
  getEffImpactTotalCostToDate = () =>
    cy.get(`[data-cy="effImpactTotalCostToDate"]`);
  getEffImpactTotalCostFuture = () =>
    cy.get(`[data-cy="effImpactTotalCostFuture"]`);
  getEffImpactTotalCostTotal = () =>
    cy.get(`[data-cy="effImpactTotalCostTotal"]`);

  // Sub Totals
  // Total Impact
  getSubTotalImpactToDate = () => cy.get(`[data-cy="totalImpactToDate"]`);
  getSubTotalImpactToFuture = () => cy.get(`[data-cy="totalImpactFuture"]`);
  getSubTotalImpactToTotal = () => cy.get(`[data-cy="totalImpactTotal"]`);

  // Probability Weighted Total Impact
  getSubTotalprobWeightedTotalImpactToDate = () =>
    cy.get(`[data-cy="probWeightedTotalImpactToDate"]`);
  getSubTotalprobWeightedTotalImpactFuture = () =>
    cy.get(`[data-cy="probWeightedTotalImpactFuture"]`);
  getSubTotalprobWeightedTotalImpactTotal = () =>
    cy.get(`[data-cy="probWeightedTotalImpactTotal"]`);

  // Monthly Impact
  getSubTotalmonthlyImpactToDate = () =>
    cy.get(`[data-cy="monthlyImpactToDate"]`);
  getSubTotalmonthlyImpactFuture = () =>
    cy.get(`[data-cy="monthlyImpactFuture"]`);
  getSubTotalmonthlyImpactTotal = () =>
    cy.get(`[data-cy="monthlyImpactTotal"]`);

  // Probability Weighted Monthly Impact
  getSubTotalprobWeightedMonthlyImpactToDate = () =>
    cy.get(`[data-cy="probWeightedMonthlyImpactToDate"]`);
  getSubTotalprobWeightedMonthlyImpactFuture = () =>
    cy.get(`[data-cy="probWeightedMonthlyImpactFuture"]`);
  getSubTotalprobWeightedMonthlyImpactTotal = () =>
    cy.get(`[data-cy="probWeightedMonthlyImpactTotal"]`);

  // Header Total
  getProdHeaderTotalToDate = () =>
    cy.get(`[data-cy="productionHeaderToDateTotal"]`);
  getProdHeaderTotalFuture = () =>
    cy.get(`[data-cy="productionHeaderFutureTotal"]`);
  getProdHeaderTotalAvg = () => cy.get(`[data-cy="productionHeaderTotal"]`);
  getProdHeaderImpactTotal = () => cy.get('[data-cy="prodHeaderImpactTotal"]');
  getProdHeaderDuration = () => cy.get(`[data-cy="prodHeaderDuration"]`);
  getProdHeaderMonthlyTotal = () =>
    cy.get(`[data-cy="prodHeaderMonthlyTotal"]`);
  getEffHeaderTotalToDate = () => cy.get(`[data-cy="effHeaderTotalToDate"]`);
  getEffHeaderTotalFuture = () => cy.get(`[data-cy="effHeaderTotalFuture"]`);
  getEffHeaderTotalAvg = () => cy.get(`[data-cy="effHeaderTotal"]`);
  getEffHeaderImpactTotal = () => cy.get('[data-cy="effHeaderImpactTotal"]');
  getEffHeaderDuration = () => cy.get(`[data-cy="effHeaderDuration"]`);
  getEffHeaderMonthlyTotal = () => cy.get(`[data-cy="effHeaderMonthlyTotal"]`);

  // Grand Total
  getGrandTotalImpact = () => cy.get(`[data-cy="totalImpactGrandTotal"]`);
  getGrandTotalProbabilityWeightedTotalImpact = () =>
    cy.get(`[data-cy="probWeightedTotalImpactGrandTotal"]`);
  getGrandTotalMonthlyImpact = () =>
    cy.get(`[data-cy="monthlyImpactGrandTotal"]`);

  clickHeaderPanel(
    headerName:
      | 'ProductionImpact'
      | 'EfficiencyImpact'
      | 'MaintenanceCostImpact'
      | 'OtherCosts'
  ) {
    cy.get(`[data-cy="${headerName}Header"]`).click('left');
  }

  inputField(fieldName: ImpactCalcFields, value: any, scenarioIdx = 0) {
    switch (fieldName) {
      case 'Production Impact Quantity':
        this.getImpactQuantityFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Production Impact Duration To Date':
        this.getProductionToDateFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Production Impact Duration Future':
        this.getProductionFutureFld().eq(scenarioIdx).clear().type('value');
        break;
      case 'Derate Override':
        this.getUserDefinedTotalBtn().eq(scenarioIdx).click();
        this.getDerateOverrideFld().eq(scenarioIdx).clear().type(value);
        cy.wait(500);
        break;
      case 'Lost Margin Opportunity':
        this.getLostMarginOpportunityFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Relevant Production Cost':
        this.getRelevantProductionCostFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Efficiency Impact Quantity':
        this.getEfficiencyImpactQuantity().eq(scenarioIdx).clear().type(value);
        break;
      case 'Efficiency Duration To Date':
        this.getEfficiencyToDateFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Efficiency Duration Future':
        this.getEfficiencyFutureFld().eq(scenarioIdx).clear().type(value);
        break;
      case 'Description':
        this.getScenarioDescription().eq(scenarioIdx).clear().type(value);
        break;
      default:
        break;
    }
  }
}
