import * as agGrid from '../common/ag-grid/ag-grid';

export class Issues {
  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new agGrid.AgGridMain();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();

  getCreateIssueBtn = () => cy.get(`[data-cy="createIssueBtn"]`);
  getFilterGridBtn = () => cy.get(`[data-cy="createIssueBtn"]`);
  getRefreshGridBtn = () => cy.get(`[data-cy="refreshGridBtn"]`);
  getDownloadGridBtn = () => cy.get(`[data-cy="downloadGridBtn"]`);
}
