/* eslint-disable cypress/no-unnecessary-waiting */
import { Helper } from '../../support/helpers/helper';
import { UserAggridCommon } from '../../support/helpers/user-aggrid-common';
import { AssetConfigUser } from '../../support/helpers/user-asset-config';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';
import { AgGridMain } from '../../support/page/pages';

const agGrid = new AgGridMain();
const user = new AssetConfigUser();
const assetNav = new Pages.AssetNavigator();
const agGridCommon = new UserAggridCommon();
const date = new Date();
const tagName = `Tag ${date.toLocaleTimeString()}`;
const tagDescription = `description ${date.toLocaleTimeString()}`;
const clientToSelect = {
  parent: 'Asset Health Monitoring',
  children: [
    'Live MD Test Group',
    'Live MD Test Station',
    'Live MD Test Unit',
    `Test Auto (Don't Touch)`,
  ],
};

describe(`Asset Configuration - Home`, () => {
  before(() => {
    user.logIn(userObj.default);
    user.navigateToApp(AppNamesNewUI.assetConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
  });
  it(`create tag`, () => {
    user.openTab('Tags');
    user
      .addNewTag(tagName, tagDescription)
      .should('contain.text', 'Tags Saved Successfully');
    agGrid.getCellItemByColumnName('Name').should('contain.text', tagName);
    agGrid
      .getCellItemByColumnName('Description')
      .should('contain.text', tagDescription);

    agGrid.getCellItemByColumnName('Name').eq(0).as('tagNameToDrag');
    agGrid.getCellItemByColumnName('AssetName').eq(0).as('tagDropZone');
    new Utils().dragAndDrop('tagNameToDrag', 'tagDropZone');
  });
});
