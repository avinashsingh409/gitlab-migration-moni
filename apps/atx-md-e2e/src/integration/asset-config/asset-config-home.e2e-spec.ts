/* eslint-disable cypress/no-unnecessary-waiting */
import { type } from 'os';
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { AgGridMain } from '../../support/page/pages';

const agGrid = new AgGridMain();
const user = new AlertsUser();
const assetNav = new Pages.AssetNavigator();
const date = new Date();
const assetName1 = `Asset ${date.toLocaleTimeString()}`;
const assetName2 = `Asset-2 ${date.toLocaleTimeString()}`;

describe(`Asset Configuration - Home`, () => {
  before(() => {
    user.logIn(userObj.default);

    user.navigateToApp(AppNamesNewUI.assetConfig, NavType.navBar);
  });

  it(`Open Asset Config`, () => {
    cy.get(`[data-cy="createAssetBtn"]`)
      .invoke('attr', 'ng-reflect-disabled')
      .then((value) => expect(value).to.eq('true'));
    cy.get(`.ag-group-value`).click();
    cy.get(`[data-cy="createAssetBtn"]`)
      .invoke('attr', 'ng-reflect-disabled')
      .then((value) => expect(value).to.eq('false'));
  });
  //*****************Positive Test Scenario*********************************/
  describe(`Create Asset 1`, () => {
    it(`Open Create Asset Configuration`, () => {
      cy.get(`[data-cy="createAssetBtn"]`).click();
    });
    it(`Enter Asset Name`, () => {
      cy.get(`[title="assetName"]`).type(assetName1);
    });
    it(`Enter Asset Description`, () => {
      cy.get(`[title="description"]`).type(`Description ${assetName1}`);
    });
    it(`Select Asset Type`, () => {
      cy.get('td').contains('Construction Management').click();
    });
    it(`Click on Create button`, () => {
      cy.get('button').contains('Create').click();
    });

    it(`Click on Parent Asset to expand`, () => {
      cy.get('.ag-group-contracted').click();
      cy.wait(3000);
    });

    it(`Verify that new asset has been added successfully`, () => {
      cy.get(`[col-id="AssetName"]`).contains(assetName1).should('exist');
    });
  });

  //*****************Keeping Desc blank Test Scenario*********************************/

  describe(`Create Asset 2 - blank description`, () => {
    it(`Open Create Asset Configuration: Positive Test by keeping Description blank`, () => {
      cy.get(`[data-cy="createAssetBtn"]`).click();
    });

    it(`Enter Asset Name`, () => {
      cy.get(`[title="assetName"]`).type(assetName2);
    });

    it(`Select Asset Type`, () => {
      cy.get('td').contains('Design Run').click();
    });

    it(`Click on Create button`, () => {
      cy.get('button').contains('Create').click();
      cy.wait(3000);
    });

    it(`Verify that new asset has been added successfully`, () => {
      cy.get(`[col-id="AssetName"]`).contains(assetName2).should('exist');
    });
  });
  //*****************Keeping Asset Name blank*********************************/
  describe(`Blank Asset Name`, () => {
    it(`Open Create Asset Configuration: Negative Test by keeping AssetName blank`, () => {
      cy.get(`[data-cy="createAssetBtn"]`).click();
    });

    it(`Enter Asset Description`, () => {
      cy.get(`[title="description"]`).type('Keeping Asset Name blank');
    });

    it(`Select Asset Type`, () => {
      cy.get('td').contains('GenericStation').click();
    });

    it(`Make sure "Create" button is disabled`, () => {
      cy.get('button').should('be.disabled');
    });

    it(`Hit on "Cancel" button`, () => {
      cy.get('button').contains('Cancel').click();
      cy.wait(3000);
    });
  });
  //*************Delete Asset****************//
  describe(`Delete Asset `, () => {
    it(`Click on child asset which you want  to delete`, () => {
      cy.get(`[col-id="AssetName"]`).contains(assetName1).click();
      cy.get(`[data-cy="deleteAssetBtn"]`).click();
      cy.get('button').contains('Continue').click();
      cy.wait(5000);
    });
  });
  //***********Edit Asset*********************//
  describe(`Edit Asset`, () => {
    it(`Click on child asset which you want  to edit`, () => {
      //cy.get(`[col-id="AssetName"]`).contains(assetName2).click()
      cy.get(`[col-id="AssetName"]`).contains(assetName2).click();
      cy.get(`[data-cy="editAssetBtn"]`).click();
    });

    it(`Enter Asset Name`, () => {
      cy.get(`[title="assetName"]`).type(' Edit Name');
    });

    it(`Enter Asset Description`, () => {
      cy.get(`[title="description"]`).type(' Edit Asset Desc');
    });

    it(`Select Asset Type`, () => {
      cy.get('td').contains('Design Run ').click();
    });

    it(`Click on "Update" button`, () => {
      cy.get('button').contains('Update').click();
      cy.wait(3000);
    });

    it(`Verify that asset has been updated successfully`, () => {
      cy.get(`[col-id="AssetName"]`).contains(assetName2).click();
    });
  });
  describe(`Others`, () => {
    it(`Verify "Copy" Asset button is working.`, () => {
      cy.get(`[data-cy="copyAssetBtn"]`).click();
    });

    it(`Ensure "Paste" Asset button is working.`, () => {
      cy.get(`[data-cy="pasteAssetBtn"]`).click();
      cy.wait(3000);
    });

    it(`Hit "Cancel" button.`, () => {
      cy.get(`.paste-box`).click();
      cy.get('button').contains('Cancel').click();
    });

    it(`Make sure "Toggle Filter Row" button is functional.`, () => {
      cy.get(`[data-cy="toggleFilterBtn"]`).click();
    });

    it(`Ensure "Copy to Clipboard" button is working.`, () => {
      cy.get(`[data-cy="downloadAsset"]`).click();
      cy.wait(5000);
    });

    it(`Verify "Asset Column" button is working.`, () => {
      cy.get(`[data-cy="sideButtonColumn"]`).click({ force: true });
      cy.wait(3000);
    });

    it(`Checked "Paren ID :Asset Column : " is functional.`, () => {
      cy.get(`.ag-column-select-column-label`).contains('Parent ID').select;
      //Parent ID
    });
  });
});
