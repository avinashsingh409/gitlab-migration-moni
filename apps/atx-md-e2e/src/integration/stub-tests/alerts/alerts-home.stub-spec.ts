import { AlertsUser } from '../../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../../support/helpers/user-data';
import * as Pages from '../../../support/page/pages';
import { Utils } from '../../../support/helpers/utils';
import { Helper } from '../../../support/helpers/helper';
import { ApiStub } from '../../../support/helpers/stub/api-stub';

const alertsPage = new Pages.Alerts();
const helper = new Helper();
const sidePanelColumns = alertsPage.sideButtons.column;
const columnHeaderPane = alertsPage.headerPanel;
const agGridMain = alertsPage.agGridMain;
const flyOutWindow = alertsPage.flyoutWindow;
const util = new Utils();
const user = new AlertsUser();
const stub = new ApiStub();

const clientToSelect = {
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Live MD Test Group'],
  // children: [],
};
const defaultColumns = alertsPage.getDefaultColumns();

describe('Alerts - Home', () => {
  before(() => {
    cy.clearLocalStorage();
    cy.clearCookies();
    user.logIn(userObj.default);
    stub.assetAccessRights();
    stub.loadAlertsSummary('alerts/summary-all-clients');
    stub.loadUserDetails();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.refreshAssetNav(); // cypress bug where /LoadNodeFromKey api doesn't get called
    stub.loadAssetNav();
    cy.wait(`@assetAccessRightsAlias`);
    cy.wait(`@assetNavAlias`);
    user.selectAssetFromNavigator(clientToSelect);
    cy.wait('@alertsSummaryAlias');
  });
  describe('ag grid', () => {
    it('Ag-grid time stamp display current date/time', () => {
      columnHeaderPane.getTimeStamp().then(($el) => {
        expect($el.text().trim()).to.eq(util.getCurrentTime());
      });
    });
    it('default filters - Watch = false', () => {
      const isActive = false;
      const gridItems = agGridMain.getActionColumnItems(
        alertsPage.columnNames.watch,
        isActive
      );
      gridItems.its('length').should('not.eq', 0);

      agGridMain
        .getActionColumnItems(alertsPage.columnNames.watch, undefined, {
          defaultCommandTimeout: 2000,
        })
        .its('length')
        .should('be.gt', 0);
    });
    it('default filters - Alerts = true', () => {
      const isActive = true;
      const gridItems = agGridMain.getActionColumnItems(
        alertsPage.columnNames.alert,
        isActive
      );
      gridItems.invoke('attr', 'class').should('contain', 'active-icon');
    });
    it('default columns after selected / visible', () => {
      user.expandView('ScreeningView');
      user.showPanel('column');
      const tempArr: string[] = sidePanelColumns.getColumnNames(true);

      cy.wrap(
        helper.removeItemFromArrayByValue(defaultColumns, 'External ID')
      ).should(`include.members`, tempArr);
    });
  });
  describe('fly out window', () => {
    it('refresh button should be visible', () => {
      flyOutWindow.headerBtns.getRefreshBtn().should('be.visible');
    });
    it('close button should be visible', () => {
      flyOutWindow.headerBtns.getCloseBtn().should('be.visible');
    });
    it('should display No Model Selected', () => {
      flyOutWindow.getNoModelText().should('be.visible');
      flyOutWindow.getNoModelText().should('contain.text', 'No Model Selected');
    });
  });
});
