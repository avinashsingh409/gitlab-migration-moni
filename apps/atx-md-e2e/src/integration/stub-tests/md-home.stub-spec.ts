// import { getGreeting } from '../support/app.po';
import { AppNamesNewUI, userObj } from '../../support/helpers/user-data';
import { User } from '../../support/helpers/user';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';

const LandingPage = new Pages.LandingPage();
const user = new User();
const util = new Utils();

describe('MD - Home', () => {
  before(() => {
    util.loadUserDetails();
    user.logIn(userObj.default);
  });
  it('App tiles should display', () => {
    const appTitles = LandingPage.appTiles();
    appTitles.alerts.should('be.visible');
    appTitles.issues.should('be.visible');
    appTitles.dataExplorer.should('be.visible');
    appTitles.dashboards.should('be.visible');
    // util.stubAPI('adsf','adf','default')
  });
});
