/* eslint-disable cypress/no-unnecessary-waiting */
import '@percy/cypress';
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  envToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { AgGridMain } from '../../support/page/pages';
import { ApiStub } from '../../support/helpers/stub/api-stub';
import { analyst } from '../../support/helpers/user-accounts';

const agGrid = new AgGridMain();
const user = new AlertsUser();
const assetNav = new Pages.AssetNavigator();
const apiStud = new ApiStub();
const url =
  `diagnostic-drilldown?model=8CFE1390-B152-4294-BBDA-E13151E5B016&asset=d3aef33a-1c0e-4f84-85ca-44e0d3c6273a` +
  `&start=1657840405327&end=1659612355847`;
describe('Diagnostic Drilldown', () => {
  before(() => {
    apiWait();
    user.logIn(analyst);
    cy.visit(envToTest + url);
  });
  it(`DDD - chart loads`, () => {
    cy.wait([
      `@modelTrendForAlertsChartingByModelExtId`,
      `@PDNDModelTrendsWithAssetGuid`,
      `@TagsDataFilteredZoom`,
    ]);
    cy.wait(2000);
    cy.percySnapshot('DDD - chart loads default', {
      widths: [1900],
    });
  });
  it(`Model Context - Zome`, () => {
    cy.get(
      `.model-context-trends-container .highcharts-container .highcharts-root`
    )
      .eq(0)
      .trigger('mousedown', { button: 1, x: 300, y: 50 })
      .trigger('mousemove')
      .trigger('mouseup');
    cy.percySnapshot('DDD - Context chart zoom', {
      widths: [1900],
    });
  });
});

function apiWait() {
  apiStud.modelTrendForAlertChartingByModelExtId(
    `ModelTrendForAlertChartingByModelExtId`
  );
  apiStud.PDNDModelTrendsWithAssetGuid(`PDNDModelTrendsWithAssetGuid`);
  apiStud.modelByGUID(`ModelByGUID`);
  apiStud.tagsDataFilteredZoom();
}
