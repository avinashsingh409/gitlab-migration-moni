import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { AgGridMain } from '../../support/page/pages';

const agGrid = new AgGridMain();
const user = new AlertsUser();
const assetNav = new Pages.AssetNavigator();

describe('Asset Navigator Ad Hoc', () => {
  before(() => {
    user.logIn(userObj.default);
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
  });
  describe(`ALERTS`, () => {
    it(`Select Asset Nav - Ad Hoc`, () => {
      const testData = getCurrEnv();
      assetNav.selectAdHoc('Ad-Hoc-Test-Auto');
      cy.get('.assetTreeNodeText')
        .should('be.visible')
        .should('contain.text', testData.asset);
    });
    it(`ag grid should load`, () => {
      agGrid.getGridRows().should(`have.length.greaterThan`, 0);
    });
  });
  describe(`ISSUES MANAGEMENT`, () => {
    before(() => {
      user.logIn(userObj.default);
      user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    });
    it(`Select Asset Nav - Ad Hoc`, () => {
      const testData = getCurrEnv();
      assetNav.selectAdHoc('Ad-Hoc-Test-Auto');
      cy.get('.assetTreeNodeText')
        .should('be.visible')
        .should('contain.text', testData.asset);
    });
    it(`ag grid should load`, () => {
      agGrid.getGridRows().should(`have.length.greaterThan`, 0);
    });
  });
  describe(`DATA EXPLORER`, () => {
    before(() => {
      user.logIn(userObj.default);
      user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.tile);
    });
    it(`Select Asset Nav - Ad Hoc loads`, () => {
      const testData = getCurrEnv();
      assetNav.selectAdHoc('Ad-Hoc-Test-Auto');
      cy.get('.assetTreeNodeText')
        .should('be.visible')
        .should('contain.text', testData.asset);
    });
    // Will add further checks later
    // it(`ag grid should load`, () => {
    //   agGrid.getGridRows().should(`have.length.greaterThan`, 0);
    // });
  });
  describe(`MODEL CONFIG`, () => {
    before(() => {
      user.logIn(userObj.default);
      user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    });
    it(`Select Asset Nav - Ad Hoc`, () => {
      const testData = getCurrEnv();
      assetNav.selectAdHoc('Ad-Hoc-Test-Auto');
      cy.get('.assetTreeNodeText')
        .should('be.visible')
        .should('contain.text', testData.asset);
    });
    it(`ag grid should load`, () => {
      agGrid.getGridRows().should(`have.length.greaterThan`, 0);
    });
  });
});

function getCurrEnv() {
  switch (Cypress.env().env) {
    case undefined:
      return { env: 'local', asset: 'Live MD Test Unit' };
    case 'dev':
      return { env: 'dev', asset: 'Live MD Test Unit' };
    case 'test':
      return { env: 'test', asset: 'Live MD Test Unit' };
    case 'stage':
      return { env: 'stage', asset: 'New Regression Unit' };
    default:
      return { env: 'local', asset: 'Live MD Test Unit' };
  }
}
