/* eslint-disable cypress/no-unnecessary-waiting */
import _ from 'lodash';
import { ApiStub } from '../../support/helpers/stub/api-stub';
import { resolveStaff } from '../../support/helpers/user-accounts';
import { IssuesUser } from '../../support/helpers/user-issues';

const apiStub = new ApiStub();
const user = new IssuesUser();

describe('Account Settings', () => {
  before(() => {
    user.logIn(resolveStaff);
    apiStub.userAccountFederated();
    cy.saveLocalStorage();
  });
  it(`It should display federated user account`, () => {
    user.openAccountSettings();
    cy.wait('@federatedUser');
    cy.get('label').contains('Federated User').should('exist');
  });
});
