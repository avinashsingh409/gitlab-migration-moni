import { userObj } from '../../support/helpers/user-data';
import { User } from '../../support/helpers/user';
import { Login } from '../../support/page/common/loginPage.po';

const user = new User();
const LoginPage = new Login();
const user1 = userObj.default;

describe('MD Login Forgot Password', () => {
  before(() => user.visit());

  it('forgot password button exists', () => {
    LoginPage.getForgotPasswordBtn().should('not.exist');
    LoginPage.inputUserEmail(user1.email);
    LoginPage.clickSignInBtn();
    LoginPage.getForgotPasswordBtn().should('be.visible');
  });
  it('request password screen should display', () => {
    LoginPage.getForgotPasswordBtn().click();
    LoginPage.getEmailForgotPasswordFld().should('be.visible');
    LoginPage.getPasswordResetBtn().should('be.visible');
  });
  it('set password windows should display', () => {
    LoginPage.setForgotPasswordEmail(user1.email);
    LoginPage.getPasswordResetBtn().click();
    const passwordCodePage = LoginPage.passwordCode;

    passwordCodePage.email().should('be.visible');
    passwordCodePage.code().should('be.visible');
    passwordCodePage.newPassword().should('be.visible');
    passwordCodePage.verifyPassword().should('be.visible');
  });
});
