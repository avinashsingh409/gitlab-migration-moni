import { userObj } from '../../support/helpers/user-data';
import { User } from '../../support/helpers/user';
import { Login } from '../../support/page/common/loginPage.po';
import { LandingPage } from '../../support/page/pages';

const user = new User();
const LoginPage = new Login();
const HomePage = new LandingPage();

describe('MD Login', () => {
  beforeEach(() => {
    cy.clearCookies();
  });

  it('log in using CORRECT credentials', () => {
    user.logIn(userObj.default);
  });
  it('log in using INCORRECT email credentials', () => {
    const incorrectUser = {
      email: 'thisIsAnIncorrectEmailAdd@email.com',
      password: 'randomPasswordGoesHere',
    };

    user.logIn(incorrectUser);
    LoginPage.getError().contains('Login Failed');
  });
  it('log in using INCORRECT password credentials', () => {
    const incorrectUser = {
      email: userObj.default.email,
      password: 'randomPasswordGoesHere',
    };

    user.logIn(incorrectUser);
    LoginPage.getError().contains('Login Failed');
  });
  it('Access denied user', () => {
    user.logIn(userObj.accessDeniedUser);
    HomePage.getAccessDenied().within(() => {
      cy.get(`.mat-card-title`).contains('Access Denied');
      cy.get(`.mat-card-subtitle`).contains(
        'You do not have permission to access this site'
      );
    });
  });
});
