import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const alertsPage = new Pages.Alerts();
const user = new IssuesUser();
const alertsUser = new AlertsUser();
const util = new Utils();
const clientToSelect = assetToTest[Cypress.env().env]['im'];

const agGridSideButtons = alertsPage.sideButtons;
const myViewName = `IM_automated${new Date().toISOString()}`;
const myViewName2 = `v2` + myViewName;

describe('Issues Management Saved View', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
  });
  describe('CRUD view', () => {
    before(() => {
      user.showPanel('view');
      user.deleteAllSavedViews(); // delete any existing saved views
    });
    it('create view, save, and refresh page', () => {
      user.saveView(myViewName);
      agGridSideButtons.view
        .getSelectedView()
        .should('contain.text', myViewName);
      cy.reload();
      user.showPanel('view');
      agGridSideButtons.view
        .getSelectedView()
        .should('contain.text', myViewName);
    });
    it('modify view', () => {
      user.showHideAssetNavigator();
      user.moveColumn('ActivityStatus', 'Title');
      user.saveView(myViewName2);
    });
    it('delete a view', () => {
      agGridSideButtons.view.getSavedListItems().should('have.length', 2);
      user.deleteSavedView(myViewName2);
      agGridSideButtons.view.getSavedListItems().should('have.length', 1);
    });
    it('view in alerts should not appear in Alerts', () => {
      user.navigateToApp(AppNamesNewUI.alerts, NavType.navBar);
      alertsUser.showPanel('view');
      agGridSideButtons.view
        .getSaveViewPanel()
        .should('not.contain.text', myViewName);
    });
  });
});
