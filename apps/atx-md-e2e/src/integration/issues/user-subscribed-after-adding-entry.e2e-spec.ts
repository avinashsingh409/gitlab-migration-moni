/* eslint-disable cypress/no-unnecessary-waiting */
import { liteAnalyst, resolveStaff } from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issuesSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;

describe('Issue Snapshot - Follow Issue', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
  });
  after(cy.restoreLocalStorage);
  afterEach(cy.restoreLocalStorage);
  it(`adding a discussion entry adds user as follower`, () => {
    const entryBlog = {
      title: 'test title',
      discussionBody: 'this is body',
    };
    user.addDiscussionEntry(entryBlog);
    user.openManageFollowers();
    issuesSnapshot.getFollowersDetails().then((followers) => {
      expect(followers[0].name.trim()).eq(resolveStaff.name);
      expect(followers[0].email.trim()).eq(resolveStaff.email);
    });
  });
  it(`refresh page, Button should say Following`, () => {
    cy.reload();
    issuesSnapshot.getFollowBtn().should('contain.text', 'Following');
  });
  it(`different user adding discussion on existing issue`, () => {
    clearLocalReloadLogin(liteAnalyst);
    const entryBlog = {
      title: 'test title',
      discussionBody: 'this is body',
    };
    user.addDiscussionEntry(entryBlog);
    user.openManageFollowers();
    issuesSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(2);
      expect(followers[0].name.trim()).eq(liteAnalyst.name);
      expect(followers[0].email.trim()).eq(liteAnalyst.email);
      expect(followers[1].name.trim()).eq(resolveStaff.name);
      expect(followers[1].email.trim()).eq(resolveStaff.email);
    });
  });
});

function clearLocalReloadLogin(username) {
  cy.clearLocalStorage();
  Cypress.env('restoreLocalStorage', false); // set global variable to false
  cy.reload(); // now cy.reload() won't restore local storage
  user.logIn(username, false);
}

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
