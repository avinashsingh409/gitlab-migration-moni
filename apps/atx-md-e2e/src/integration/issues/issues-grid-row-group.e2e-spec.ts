import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { IssuesColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const issuesPage = new Pages.Issues();
const user = new IssuesUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const myViewName1 = `ALERTS_Pin1 ${new Date().toISOString()}`;
const columnToGroup1: IssuesColumnID = 'AssetClassTypeDesc';
let columnItems: string[];
let uniqueItems: string[];
const columnsToHide = {
  toHide: [
    'Impact',
    'Status',
    'Created',
    'Assigned',
    'Age (Days)',
    'Scorecard',
    'Type',
  ],
};

describe('ALERTS - Grid Row Groups', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.restoreGridToDefaults();
    user.showPanel('column');
    user.setColumns(columnsToHide);
    user.showHideAssetNavigator();
    user.showPanel('column'); // close
  });
  it(`group by ${columnToGroup1}`, () => {
    issuesPage.agGridMain.getItemsByColumnName(columnToGroup1).then((items) => {
      uniqueItems = [...new Set(items)] as string[];
      columnItems = items;
    });

    user.groupByColumn('AssetClassTypeDesc');

    issuesPage.agGridMain.getGroupNames().then((groupNames) => {
      expect(groupNames).to.have.length(uniqueItems.length);
      expect(groupNames).to.include.members([...uniqueItems]);
    });
  });
  it(`group row item count correct`, () => {
    issuesPage.agGridMain.getRowGroupDetails().then((groupDetails) => {
      groupDetails.forEach((group) => {
        expect(columnItems.filter((i) => i === group.name).length).eq(
          Number(group.itemCount)
        );
      });
    });
  });
  it(`expand a group`, () => {
    user.expandGroup(uniqueItems[0]);

    issuesPage.agGridMain
      .getRowGroups()
      .first()
      .get(`.ag-group-expanded`)
      .should('be.visible');
  });
  it(`group members count is correct`, () => {
    issuesPage.agGridMain.getGroupMembersByLevel(1).then(($el) => {
      expect($el.length).eq(
        columnItems.filter((items) => items === uniqueItems[0]).length,
        `Grouped members count should match`
      );
    });
  });
  it(`group members details should be correct`, () => {
    issuesPage.agGridMain
      .getColumnItemsByGroupLevel(1, columnToGroup1)
      .then((colItems) => {
        colItems.forEach((item) => {
          expect(item).eq(uniqueItems[0]);
        });
      });
  });
});
