import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { IssuesColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
const issuesPage = new Pages.Issues();
const user = new IssuesUser();
const clientToSelect = assetToTest[Cypress.env().env]['im'];

const agGridColumnFloatingFilter = issuesPage.agGridFloatingFilter;
const myViewName1 = `ISSUES_floatingFilter ${new Date().toISOString()}`;
const columnToFilter1: IssuesColumnID = 'ResolutionStatus';
const columnToFilter2: IssuesColumnID = 'Title';
let searchTerm: string;
let filteredItems: string[];

describe('ISSUES Ag-Grid Floating Filter', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view');
    user.deleteAllSavedViews();
    user.restoreGridToDefaults();
    // user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    // user.showHideFlyoutWindow();
  });
  it('Floating filter disabled by default', () => {
    agGridColumnFloatingFilter.getFloatingFilterRow().should('not.exist');
  });
  it('show floating filter pane', () => {
    user.clickFloatingFilterBtn();
    agGridColumnFloatingFilter.getFloatingFilterRow().should('be.visible');
  });
  it(`filter column ${columnToFilter1}`, () => {
    /*
    This step will filter a column and compare initial and current result
     */
    const initialGridItems =
      issuesPage.agGridMain.getItemsByColumnName(columnToFilter1);

    initialGridItems.then((gridItems) => {
      searchTerm = gridItems[_.random(0, gridItems.length - 1)];
      user.inputFilterValue(searchTerm, 'Resolution');

      issuesPage.agGridMain
        .getItemsByColumnName(columnToFilter1)
        .then((results) => {
          expect(gridItems.filter((e) => e === searchTerm).length).eq(
            results.length
          ); // check length from previous vs search result
          filteredItems = results;
          results.forEach((e) => expect(e).eq(searchTerm)); // check each result
          // TO BE ADDED: Compare with Donut chart
        });
    });
  });
  it('save view changes persists', () => {
    /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
    user.saveView(myViewName1);
    user.restoreGridToDefaults();
    user.selectSavedView(myViewName1);

    issuesPage.agGridMain
      .getItemsByColumnName(columnToFilter1)
      .then((gridItems) => {
        gridItems.forEach((e) => expect(e).eq(searchTerm)); // check each result
        expect(gridItems.filter((e) => e === searchTerm).length).eq(
          gridItems.length
        ); // check length from previous vs search result
        // TO BE ADDED: Compare with Donut chart
      });
    user.showPanel('view'); // this closes the panel
  });
  it(`filter another column ${columnToFilter2}`, () => {
    /*
    This step will filter another column (2) and check the results
    */
    const initialGridItems =
      issuesPage.agGridMain.getItemsByColumnName(columnToFilter2);

    initialGridItems.then((gridItems) => {
      searchTerm = gridItems[_.random(0, gridItems.length - 1)];
      user.inputFilterValue(searchTerm, 'Title');

      issuesPage.agGridMain
        .getItemsByColumnName(columnToFilter2)
        .then((results) => {
          results.forEach((e) => expect(e).contains(searchTerm)); // check each result
          expect(
            gridItems.filter((e) => (e as string).includes(searchTerm)).length
          ).eq(results.length); // check length from previous vs search result

          // TO BE ADDED: Compare with Donut chart
        });
    });
  });
  it(`filter column ${columnToFilter1} with invalid search term`, () => {
    user.inputFilterValue(`xxx`, 'Resolution');
    // // TO BE ADDED: Compare with Donut chart
  });
  it('close floating filter', () => {
    user.clickFloatingFilterBtn().then(() => {
      expect(Cypress.$(`.ag-header-row-floating-filter`).length).eq(0);
    });
  });
});
