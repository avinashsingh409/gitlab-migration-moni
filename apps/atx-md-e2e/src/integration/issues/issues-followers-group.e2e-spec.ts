/* eslint-disable cypress/no-unnecessary-waiting */
import {
  analyst,
  liteAnalyst,
  resolveStaff,
  testAutomationUser1,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;
const groupToAdd = ['Test Automation Group 1', 'Test Automation Group 2'];
const usersInGroup = [analyst, liteAnalyst, resolveStaff];

describe('Issue Snapshot - Issue Followers GROUP', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
  });
  describe(`Add Group`, () => {
    it(`open Manange Followers window`, () => {
      user.openManageFollowers();
      issueSnapshot.getManageFollowersWindow().should('be.visible');
    });
    it(`user should appear in drop down list after input`, () => {
      issueSnapshot.inputFollower(groupToAdd[0]);
      issueSnapshot.getFollowerList().should('contain.text', groupToAdd[0]);
    });
    it(`chip should be added after selecting from user list`, () => {
      issueSnapshot.selectFollowerList(groupToAdd[0]);
      issueSnapshot.getFollowerChip().should('have.length', 1);
      issueSnapshot
        .getFollowerChip()
        .eq(0)
        .should('contain.text', groupToAdd[0]);
    });
    it(`add selected group`, () => {
      issueSnapshot.getAddFollowersBtn().click();
      issueSnapshot.getFollowersDetails().then((followers) => {
        usersInGroup.forEach((user, idx) => {
          const userMatched = followers.filter(
            (follower) => follower.email === user.email
          );
          expect(userMatched.length).eq(1);
          expect(userMatched[0].name).eq(user.name);
          expect(userMatched[0].groups[0]).eq(groupToAdd[0]);
          expect(userMatched[0].groups.length).eq(1);
        });
      });
    });
    it(`save Manage Issue Followers and confirm changes`, () => {
      issueSnapshot.getManageSaveBtn().click();
      issueSnapshot
        .getFollowerSaveConfirm()
        .should(
          'have.text',
          ' Your changes to the issue followers have been saved. '
        );
      cy.get(`mat-dialog-container button`).contains('OK').click();
      issueSnapshot.getManageCancelBtn().click();
      user.openManageFollowers();
      issueSnapshot.getFollowersDetails().then((followers) => {
        usersInGroup.forEach((user, idx) => {
          const userMatched = followers.filter(
            (follower) => follower.email === user.email
          );
          expect(userMatched.length).eq(1);
          expect(userMatched[0].name).eq(user.name);
          expect(userMatched[0].groups[0]).eq(groupToAdd[0]);
          expect(userMatched[0].groups.length).eq(1);
        });
      });
    });
  });
  describe(`Remove Followers`, () => {
    it(`following icon should change when clicked`, () => {
      user.removeFollower(usersInGroup[1]);
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(
          followers.filter(
            (follower) => follower.name === usersInGroup[1].name
          )[0].followingIssue
        ).eq('Remove User');
      });
    });
    it(`save changes and confirm`, () => {
      user.saveManageFollowers(true);
      user.openManageFollowers();
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(
          followers.length,
          'Followers should be 2 after one follower is removed'
        ).eq(2);
        expect(
          followers.filter((follower) => follower.name === usersInGroup[1].name)
            .length,
          'Removed follower should not be visible'
        ).eq(0);

        followers.forEach((follower) => {
          expect(
            follower.groups.length,
            'Group should be removed from remaining followers'
          ).eq(0);
        });
      });
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
