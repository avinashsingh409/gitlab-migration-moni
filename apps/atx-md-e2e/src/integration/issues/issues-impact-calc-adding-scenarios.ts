/* eslint-disable cypress/no-unnecessary-waiting */
import _ from 'lodash';
import { resolveStaff } from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const impactCalculator = new Pages.ImpactCalculatorPage();
const user = new IssuesUser();
const helper = new Helper();

const util = new Utils();
const clientToSelect = {
  parent: 'Asset Health Monitoring',
  children: [`Test Auto 2 (Dont Touch)`, `Child 1`, `Child 1.2`],
};
const issueTitle = `Impact Calc Vairable Test_${new Date().toISOString()}`;
const numOfScenarios = 3;

const impactCalcVariables = {
  ImpactCapacityUoM: 'Tons',
  ImpactUoT: 'Day',
  ImpactCapacityValue: 100,
  ImpactTimeToDaysConversion: 1,
  ImpactCurrency: '$',
  ImpactCurrencyToDollars: 1,
  ImpactProductionRevenueValue: 50,
  ImpactConsumableLabel: 'Steam Cost',
  ImpactConsumableUoM: '$/lb',
  ImpactConsumableValue: 1,
  ImpactNomConsRateLabel: 'Steam Consumption',
  ImpactNomConsRateUoM: 'lb/ton',
  ImpactNomConsRateValue: 500,
  ImpactConsCostToRateConvert: 1,
  ImpactProdCostToRateConvert: 1,
  ImpactNomCapacityUtilization: 1,
};

const impactToInput = {
  description: `Scenario Description`,
  productionImpactQuantity: 10,
  productionDurationToDate: 1,
  derateOverride: 100,
  lostMarginOpportunity: 50,
  relevantProductionCost: 15,
  efficiencyImpactQuantity: 5,
  efficiencyDurationToDate: 1,
};

// Expected Production values
const expectedProdLossValue = helper.calcProdductionLossValue(
  impactToInput,
  impactCalcVariables
);
const expectedProdLossToDate = helper.calcProdLossToDate(
  expectedProdLossValue,
  impactToInput,
  impactCalcVariables
);
const expectedProdMonthlyCost = helper.calcProdMonthlyCost(
  expectedProdLossValue,
  impactToInput,
  impactCalcVariables
);
const expectedProductionTotalCost = helper.calcProdTotalCost(
  expectedProdLossToDate,
  impactToInput,
  impactCalcVariables
);

// Expected Efficiency values
const expectedNomConsRateChangeValue = helper.calcNomConsRateChangeValue(
  impactToInput,
  impactCalcVariables
);
const expectedEffImpactMonthlyCost = helper.calcEffImpactMonthlyCost(
  expectedNomConsRateChangeValue,
  impactCalcVariables
);
const expectedEffImpactTotalCost = helper.calcEffImpactTotalCost(
  expectedNomConsRateChangeValue,
  impactToInput,
  impactCalcVariables
);

describe('Issues - Adding Scenarios', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
    user.openImpactCalculator();
    impactCalculator.clickHeaderPanel('ProductionImpact');
    impactCalculator.clickHeaderPanel('EfficiencyImpact');
  });

  for (let x = 0; x <= numOfScenarios - 1; x++) {
    describe(`Add New Scenario #${x + 1}`, () => {
      if (x !== 0) {
        it(`Add Scenario 1`, () => {
          impactCalculator
            .getAddNewScenarioBtn()
            .click({ waitForAnimations: true });
          impactCalculator.getScenarioHeaders().should('have.length', x + 1);
        });
      }
      it(`Input impact fields on second scenario `, () => {
        inputFields(x);
      });
      describe(`Production Impact`, () => {
        it(`Impact Montly Cost To Date`, () => {
          impactCalculator
            .getProdImpactMonthlyCostToDate()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedProdMonthlyCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Montly Cost Total`, () => {
          impactCalculator
            .getProdImpactMonthlyCostTotal()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedProdMonthlyCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Total Cost To Date`, () => {
          impactCalculator
            .getProdImpactTotalCostToDate()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedProductionTotalCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Total Cost To Total`, () => {
          impactCalculator
            .getProdImpactTotalCostTotal()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedProductionTotalCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Header Impact Total should be correct`, () => {
          impactCalculator.getProdHeaderImpactTotal().should(
            'have.value',
            `${(expectedProductionTotalCost * (x + 1)).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          );
        });
        it(`Header Duration`, () => {
          impactCalculator
            .getProdHeaderDuration()
            .should(
              'have.value',
              `${impactToInput.productionDurationToDate * (x + 1)} days`
            );
        });
        it(`Header Monthly`, () => {
          impactCalculator.getProdHeaderMonthlyTotal().should(
            'have.value',
            `${(expectedProdMonthlyCost * (x + 1)).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          );
        });
      });
      describe(`Efficiency Impact`, () => {
        it(`Impact Montly Cost To Date`, () => {
          impactCalculator
            .getEffImpactMonthlyCostToDate()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedEffImpactMonthlyCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Montly Cost Total`, () => {
          impactCalculator
            .getEffImpactMonthlyCostTotal()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedEffImpactMonthlyCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Total Cost To Date`, () => {
          impactCalculator
            .getEffImpactTotalCostToDate()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedEffImpactTotalCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Impact Total Cost To Total`, () => {
          impactCalculator
            .getEffImpactTotalCostTotal()
            .eq(x)
            .then(($el) =>
              expect($el.text().trim()).eq(
                `${expectedEffImpactTotalCost.toLocaleString('en-us', {
                  style: 'currency',
                  currency: 'USD',
                })}`
              )
            );
        });
        it(`Header Total - To Date`, () => {
          impactCalculator
            .getEffHeaderTotalToDate()
            .eq(x)
            .should(
              'have.value',
              `${expectedEffImpactTotalCost.toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            );
        });
        it(`Header - Total`, () => {
          impactCalculator
            .getEffHeaderTotalAvg()
            .eq(x)
            .should(
              'have.value',
              `${expectedEffImpactTotalCost.toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            );
        });
        it(`Header Impact Total`, () => {
          impactCalculator.getEffHeaderImpactTotal().should(
            'have.value',
            `${(expectedEffImpactTotalCost * (x + 1)).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          );
        });
        it(`Header Duration`, () => {
          impactCalculator
            .getEffHeaderDuration()
            .should(
              'have.value',
              `${impactToInput.efficiencyDurationToDate * (x + 1)} days`
            );
        });
        it(`Header Monthly`, () => {
          impactCalculator.getEffHeaderMonthlyTotal().should(
            'have.value',
            `${(expectedEffImpactMonthlyCost * (x + 1)).toLocaleString(
              'en-us',
              {
                style: 'currency',
                currency: 'USD',
              }
            )}`
          );
        });
      });
    });
  }
  describe(`Remove A Scenario`, () => {
    it(`remove scenario 2`, () => {
      user.removeScenario(1); // will remove scenario 2

      impactCalculator
        .getScenarioHeaders()
        .should('have.length', numOfScenarios - 1);
    });
    it(`Save scenario`, () => {
      user.saveImpactCalc();
    });
    it(`Impact Total in Issue snapshot should be correct`, () => {
      issueSnapshot.getImpactTotal().should(
        'have.value',
        `${(
          (expectedProductionTotalCost + expectedEffImpactTotalCost) *
          (numOfScenarios - 1)
        ).toLocaleString('en-us', {
          style: 'currency',
          currency: 'USD',
        })}`
      );
    });
    it(`Monthly Average in Issue snapshot should be correct`, () => {
      issueSnapshot.getMonthlyAverage().should(
        'have.value',
        `${(
          (expectedProdMonthlyCost + expectedEffImpactMonthlyCost) *
          (numOfScenarios - 1)
        ).toLocaleString('en-us', {
          style: 'currency',
          currency: 'USD',
        })}`
      );
    });
    it(`Open Impact Calc and verify changes`, () => {
      user.openImpactCalculator();
      impactCalculator.getScenarioHeaders().should('have.length', 2);
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}

function inputFields(scenarioIdx = 0) {
  user.inputImpactField(
    'Description',
    impactToInput.description + ` ${scenarioIdx}`,
    scenarioIdx
  );
  user.inputImpactField(
    'Production Impact Quantity',
    impactToInput.productionImpactQuantity,
    scenarioIdx
  );
  user.inputImpactField(
    'Production Impact Duration To Date',
    impactToInput.productionDurationToDate,
    scenarioIdx
  );
  user.inputImpactField(
    'Derate Override',
    impactToInput.derateOverride,
    scenarioIdx
  );
  user.inputImpactField(
    'Lost Margin Opportunity',
    impactToInput.lostMarginOpportunity.toString(),
    scenarioIdx
  );
  user.inputImpactField(
    'Relevant Production Cost',
    impactToInput.relevantProductionCost,
    scenarioIdx
  );
  user.inputImpactField(
    'Efficiency Impact Quantity',
    impactToInput.efficiencyImpactQuantity,
    scenarioIdx
  );
  user.inputImpactField(
    'Efficiency Duration To Date',
    impactToInput.efficiencyDurationToDate,
    scenarioIdx
  );
}
