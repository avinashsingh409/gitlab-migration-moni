import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';

const util = new Utils();
// const helper = new Helper();
const issuesPage = new Pages.Issues();
const user = new IssuesUser();
const clientToSelect = {
  // parent: 'All Clients',
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Test Automation Parent (Dont Touch)'],
  // children: ['Live MD Test Group'],
};
const columnOrder = [
  'Title',
  'Priority',
  'ID',
  'Impact',
  'Status',
  'Resolution',
  'Created',
  'Assigned',
  'Changed By',
  'Changed',
  'Age (Days)',
];

describe('ISSUES - Reordered Saved View', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    user.showPanel('view');
    user.restoreGridToDefaults();
  });
  describe(`Reorder`, () => {
    it(`Saved view Reorder should load correctly`, () => {
      user.selectSavedView('reorder1');
      user.showPanel('column');

      cy.wrap(issuesPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('reorder1');
      user.showPanel('column');

      cy.wrap(issuesPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
  });
  describe(`Group`, () => {
    const groupedColumns = [
      'Online Maintenance',
      'Outage Maintenance',
      'Plant Operations',
    ];
    before(() => {
      user.showPanel('view');
    });
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('grouped1');
      issuesPage.agGridMain.getGroupNames().then((groupNames) => {
        expect(groupNames).to.deep.eq(groupedColumns);
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.restoreGridToDefaults();
      user.selectSavedView('grouped1');

      issuesPage.agGridMain.getGroupNames().then((groupNames) => {
        expect(groupNames).to.deep.eq(groupedColumns);
      });
    });
  });
  describe(`Group + Reorder`, () => {
    const groupedColumns = [
      'Online Maintenance',
      'Outage Maintenance',
      'Plant Operations',
    ];
    const columnOrder = [
      'Category',
      'Priority',
      'ID',
      'Title',
      'Impact',
      'Status',
      'Resolution',
      'Created',
      'Assigned',
      'Age (Days)',
      'Scorecard',
    ];
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('group+reorder');
      user.showPanel('column');
      issuesPage.agGridMain.getGroupNames().then((groupNames) => {
        expect(groupNames).to.deep.eq(groupedColumns);
      });

      cy.wrap(issuesPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('group+reorder');
      user.showPanel('column');

      issuesPage.agGridMain.getGroupNames().then((groupNames) => {
        expect(groupNames).to.deep.eq(groupedColumns);
      });

      cy.wrap(issuesPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
  });
  describe(`Filter`, () => {
    it(`Saved view Filter should load correctly`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('filter1');
      // util.loadAgGridForWait('IssuesGrid');
      cy.wait('@IssuesGrid', { timeout: 15000 });
      issuesPage.headerPanel.getFilterChips().then((el) => {
        expect(el.length).to.eq(2);
        cy.wrap(el)
          .find('[data-cy="filter-column-name"]')
          .then((e) => {
            expect(e[0].textContent).eq('Status');
            expect(e[1].textContent).eq('Category');
          });
      });
      issuesPage.agGridMain
        .getItemsByColumnName('CategoryDesc')
        .then((items) => {
          items.forEach((item) => expect(item).eq('Plant Operations'));
        });
    });
  });
});
