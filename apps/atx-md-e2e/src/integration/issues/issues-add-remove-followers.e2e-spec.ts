/* eslint-disable cypress/no-unnecessary-waiting */
import {
  analyst,
  liteAnalyst,
  resolveStaff,
  testAutomationUser1,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;
const usersToAdd = [testAutomationUser1, analyst, liteAnalyst];

describe('Issue Snapshot - Issue Followers', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
  });
  describe(`Add Follower`, () => {
    it(`open Manange Followers window`, () => {
      user.openManageFollowers();
      issueSnapshot.getManageFollowersWindow().should('be.visible');
    });
    it(`user should appear in drop down list after`, () => {
      issueSnapshot.inputFollower(usersToAdd[0].email);
      issueSnapshot
        .getFollowerList()
        .should('contain.text', usersToAdd[0].name);
    });
    it(`chip should be added after selecting from user list`, () => {
      issueSnapshot.selectFollowerList(usersToAdd[0].name);
      issueSnapshot.getFollowerChip().should('have.length', 1);
      issueSnapshot
        .getFollowerChip()
        .eq(0)
        .should('contain.text', usersToAdd[0].name);
    });
    it(`add selected follower`, () => {
      issueSnapshot.getAddFollowersBtn().click();
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(followers[0].name.trim()).eq(usersToAdd[0].name);
        expect(followers[0].email.trim()).eq(usersToAdd[0].email);
      });
    });
    it(`save Manage Issue Followers and confirm changes`, () => {
      issueSnapshot.getManageSaveBtn().click();
      issueSnapshot
        .getFollowerSaveConfirm()
        .should(
          'have.text',
          ' Your changes to the issue followers have been saved. '
        );
      cy.get(`mat-dialog-container button`).contains('OK').click();
      issueSnapshot.getManageCancelBtn().click();

      user.openManageFollowers();
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(followers[0].name.trim()).eq(usersToAdd[0].name);
        expect(followers[0].email.trim()).eq(usersToAdd[0].email);
        expect(followers[0].followingIssue).eq('User Following Issue');
      });
    });
  });
  describe(`Add Multiple Followers`, () => {
    it(`should be able to add another follower`, () => {
      user.addFollowers([usersToAdd[1], usersToAdd[2]]);
      issueSnapshot.getFollowersDetails().then((followers) => {
        followers.forEach((follower, idx) => {
          expect(follower.name.trim()).eq(usersToAdd[idx].name);
          expect(follower.email.trim()).eq(usersToAdd[idx].email);
        });
      });
    });
    it(`save and confirm changes`, () => {
      user.saveManageFollowers(true);
      user.openManageFollowers();
      issueSnapshot.getFollowersDetails().then((followers) => {
        followers.forEach((follower, idx) => {
          expect(follower.name.trim()).eq(usersToAdd[idx].name);
          expect(follower.email.trim()).eq(usersToAdd[idx].email);
        });
      });
    });
  });
  describe(`Remove Followers`, () => {
    it(`following icon should change when clicked`, () => {
      user.removeFollower(usersToAdd[1]);
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(
          followers.filter(
            (follower) => follower.name === usersToAdd[1].name
          )[0].followingIssue
        ).eq('Remove User');
      });
    });
    it(`save changes and confirm`, () => {
      user.saveManageFollowers(true);
      user.openManageFollowers();
      issueSnapshot.getFollowersDetails().then((followers) => {
        expect(followers.length).eq(2);
        expect(
          followers.filter((follower) => follower.name === usersToAdd[1].name)
            .length
        ).eq(0);
      });
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
