/* eslint-disable cypress/no-unnecessary-waiting */
import _ from 'lodash';
import {
  resolveStaff,
  testAutomationUser1,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const impactCalculator = new Pages.ImpactCalculatorPage();
const user = new IssuesUser();
const helper = new Helper();
const util = new Utils();

const clientToSelect = {
  parent: 'Asset Health Monitoring',
  children: [`Test Auto 2 (Dont Touch)`, `Child 1`, `Child 1.2`],
};
const issueTitle = `Impact Calc Vairable Test_${new Date().toISOString()}`;

const impactCalcVariables = {
  ImpactCapacityUoM: 'Tons',
  ImpactUoT: 'Day',
  ImpactCapacityValue: 100,
  ImpactTimeToDaysConversion: 1,
  ImpactCurrency: '$',
  ImpactCurrencyToDollars: 1,
  ImpactProductionRevenueValue: 50,
  ImpactConsumableLabel: 'Steam Cost',
  ImpactConsumableUoM: '$/lb',
  ImpactConsumableValue: 1,
  ImpactNomConsRateLabel: 'Steam Consumption',
  ImpactNomConsRateUoM: 'lb/ton',
  ImpactNomConsRateValue: 500,
  ImpactConsCostToRateConvert: 1,
  ImpactProdCostToRateConvert: 1,
  ImpactNomCapacityUtilization: 1,
};

const impactToInput = {
  productionImpactQuantity: 10,
  productionDurationToDate: 1,
  derateOverride: 100,
  lostMarginOpportunity: 50,
  relevantProductionCost: 15,
  efficiencyImpactQuantity: 5,
  efficiencyDurationToDate: 1,
};

// Expected Production values
const expectedProdLossValue = helper.calcProdductionLossValue(
  impactToInput,
  impactCalcVariables
);
const expectedProdLossToDate = helper.calcProdLossToDate(
  expectedProdLossValue,
  impactToInput,
  impactCalcVariables
);
const expectedProdMonthlyCost = helper.calcProdMonthlyCost(
  expectedProdLossValue,
  impactToInput,
  impactCalcVariables
);
const expectedProductionTotalCost = helper.calcProdTotalCost(
  expectedProdLossToDate,
  impactToInput,
  impactCalcVariables
);

// Expected Efficiency values
const expectedNomConsRateChangeValue = helper.calcNomConsRateChangeValue(
  impactToInput,
  impactCalcVariables
);
const expectedEffImpactMonthlyCost = helper.calcEffImpactMonthlyCost(
  expectedNomConsRateChangeValue,
  impactCalcVariables
);
const expectedEffImpactTotalCost = helper.calcEffImpactTotalCost(
  expectedNomConsRateChangeValue,
  impactToInput,
  impactCalcVariables
);

describe('Issues - Impact Calculator - KAES', () => {
  before(() => {
    user.logIn(testAutomationUser1);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
    user.openImpactCalculator();
    impactCalculator.clickHeaderPanel('ProductionImpact');
    impactCalculator.clickHeaderPanel('EfficiencyImpact');
  });
  describe(`Production Impact Values`, () => {
    describe(`Impact Quantity`, () => {
      it(`input Production Impact Quantity`, () => {
        user.inputImpactField(
          'Production Impact Quantity',
          impactToInput.productionImpactQuantity
        );

        impactCalculator
          .getImpactQuantityFld()
          .should(
            'have.value',
            impactToInput.productionImpactQuantity.toString()
          );
      });
      it(`Production Restriction value should be correct`, () => {
        impactCalculator
          .getProductionRestrictionValue()
          .then(($el) =>
            expect($el.text().trim()).eq(
              impactToInput.productionImpactQuantity.toString(),
              `Production Restriction value should be the same as Impact Quantity`
            )
          );
      });
      it(`Production Loss value should be correct`, () => {
        impactCalculator
          .getProductionLossValue()
          .then(($el) =>
            expect($el.text().trim()).eq(
              expectedProdLossValue.toString(),
              `Production Loss value should be the same as Impact Quantity`
            )
          );
      });
    });
    describe(`Production Duration`, () => {
      it(`input Production Duration To Date`, () => {
        user.inputImpactField(
          'Production Impact Duration To Date',
          impactToInput.productionDurationToDate
        );

        impactCalculator
          .getProductionTotalDuration()
          .should(
            'have.value',
            impactToInput.productionDurationToDate.toString()
          );

        impactCalculator
          .getProductionLossToDate()
          .then(($el) =>
            expect($el.text().trim()).eq(expectedProdLossToDate.toString())
          );
        impactCalculator
          .getProductionLossTotal()
          .then(($el) =>
            expect($el.text().trim()).eq(expectedProdLossToDate.toString())
          );
      });
    });
    describe(`% Time Derate in Effect`, () => {
      it(`input % Time Derate in Effect`, () => {
        user.inputImpactField('Derate Override', impactToInput.derateOverride);

        impactCalculator
          .getDerateOverrideFld()
          .should('have.value', impactToInput.derateOverride.toString());
      });
    });
    describe(`Lost Margin Opportunity`, () => {
      it(`input Lost Margin Opportunity`, () => {
        user.inputImpactField(
          'Lost Margin Opportunity',
          impactToInput.lostMarginOpportunity.toString()
        );

        impactCalculator
          .getLostMarginOpportunityFld()
          .should('have.value', impactToInput.lostMarginOpportunity.toString());
      });
    });
    describe(`Relevant Production Cost`, () => {
      it(`input Relevant Production Cost`, () => {
        user.inputImpactField(
          'Relevant Production Cost',
          impactToInput.relevantProductionCost
        );

        impactCalculator
          .getRelevantProductionCostFld()
          .should(
            'have.value',
            impactToInput.relevantProductionCost.toString()
          );
      });
    });
    describe(`Totals`, () => {
      it(`Impact Montly Cost To Date`, () => {
        impactCalculator.getProdImpactMonthlyCostToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProdMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Montly Cost Total`, () => {
        impactCalculator.getProdImpactMonthlyCostTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProdMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Total Cost To Date`, () => {
        impactCalculator.getProdImpactTotalCostToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProductionTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Total Cost To Total`, () => {
        impactCalculator.getProdImpactTotalCostTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProductionTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Header Total - To Date`, () => {
        impactCalculator.getProdHeaderTotalToDate().should(
          'have.value',
          `${expectedProductionTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header - Total`, () => {
        impactCalculator.getProdHeaderTotalAvg().should(
          'have.value',
          `${expectedProductionTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header Impact Total`, () => {
        impactCalculator.getProdHeaderImpactTotal().should(
          'have.value',
          `${expectedProductionTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header Duration`, () => {
        impactCalculator
          .getProdHeaderDuration()
          .should(
            'have.value',
            `${impactToInput.productionDurationToDate} days`
          );
      });
      it(`Header Monthly`, () => {
        impactCalculator.getProdHeaderMonthlyTotal().should(
          'have.value',
          `${expectedProdMonthlyCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`SubTotal - Total Impact To Date`, () => {
        impactCalculator.getSubTotalImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProductionTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Total Impact Total`, () => {
        impactCalculator.getSubTotalImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProductionTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Probability Weighted Total Impact - To Date`, () => {
        impactCalculator
          .getSubTotalprobWeightedTotalImpactToDate()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${expectedProductionTotalCost.toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
      it(`SubTotal - Probability Weighted Total Impact - To Total`, () => {
        impactCalculator.getSubTotalprobWeightedTotalImpactTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProductionTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Monthly Impact - To Date`, () => {
        impactCalculator.getSubTotalmonthlyImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProdMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Monthly Impact - Total`, () => {
        impactCalculator.getSubTotalmonthlyImpactTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedProdMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Probability Weighted Monthly Impact - To Date`, () => {
        impactCalculator
          .getSubTotalprobWeightedMonthlyImpactToDate()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${expectedProdMonthlyCost.toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
      it(`SubTotal - Probability Weighted Monthly Impact - Total`, () => {
        impactCalculator
          .getSubTotalprobWeightedMonthlyImpactTotal()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${expectedProdMonthlyCost.toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
    });
  });
  describe(`Efficiency Impact Values`, () => {
    describe(`Impact Quantity`, () => {
      it(`input Efficiency Impact Quantity`, () => {
        user.inputImpactField(
          'Efficiency Impact Quantity',
          impactToInput.efficiencyImpactQuantity
        );

        impactCalculator
          .getEfficiencyImpactQuantity()
          .should(
            'have.value',
            impactToInput.efficiencyImpactQuantity.toString()
          );
      });
      it(`Nominal Consumption Impact value should be correct`, () => {
        impactCalculator
          .getNomConsRateImpactValue()
          .then(($el) =>
            expect($el.text().trim()).eq(
              impactToInput.efficiencyImpactQuantity.toString()
            )
          );
      });
      it(`Nominal Consumption Change value should be correct`, () => {
        impactCalculator
          .getImpactNomConsRateLabelChangeValue()
          .then(($el) =>
            expect($el.text().trim()).eq(
              expectedNomConsRateChangeValue.toString()
            )
          );
      });
    });
    describe(`Efficiency Duration`, () => {
      it(`input Production Duration To Date`, () => {
        user.inputImpactField(
          'Efficiency Duration To Date',
          impactToInput.efficiencyDurationToDate
        );

        impactCalculator
          .getEfficiencyDurationTotal()
          .should(
            'have.value',
            impactToInput.efficiencyDurationToDate.toString()
          );
      });
    });
    describe(`Totals`, () => {
      it(`Impact Montly Cost To Date`, () => {
        impactCalculator.getEffImpactMonthlyCostToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedEffImpactMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Montly Cost Total`, () => {
        impactCalculator.getEffImpactMonthlyCostTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedEffImpactMonthlyCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Total Cost To Date`, () => {
        impactCalculator.getEffImpactTotalCostToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedEffImpactTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Impact Total Cost To Total`, () => {
        impactCalculator.getEffImpactTotalCostTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${expectedEffImpactTotalCost.toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`Header Total - To Date`, () => {
        impactCalculator.getEffHeaderTotalToDate().should(
          'have.value',
          `${expectedEffImpactTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header - Total`, () => {
        impactCalculator.getEffHeaderTotalAvg().should(
          'have.value',
          `${expectedEffImpactTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header Impact Total`, () => {
        impactCalculator.getEffHeaderImpactTotal().should(
          'have.value',
          `${expectedEffImpactTotalCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`Header Duration`, () => {
        impactCalculator
          .getEffHeaderDuration()
          .should(
            'have.value',
            `${impactToInput.efficiencyDurationToDate} days`
          );
      });
      it(`Header Monthly`, () => {
        impactCalculator.getEffHeaderMonthlyTotal().should(
          'have.value',
          `${expectedEffImpactMonthlyCost.toLocaleString('en-us', {
            style: 'currency',
            currency: 'USD',
          })}`
        );
      });
      it(`SubTotal - Total Impact To Date`, () => {
        impactCalculator.getSubTotalImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${(
              expectedProductionTotalCost + expectedEffImpactTotalCost
            ).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Total Impact Total`, () => {
        impactCalculator.getSubTotalImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${(
              expectedProductionTotalCost + expectedEffImpactTotalCost
            ).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Probability Weighted Total Impact - To Date`, () => {
        impactCalculator
          .getSubTotalprobWeightedTotalImpactToDate()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${(
                expectedProductionTotalCost + expectedEffImpactTotalCost
              ).toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
      it(`SubTotal - Probability Weighted Total Impact - To Total`, () => {
        impactCalculator.getSubTotalprobWeightedTotalImpactTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${(
              expectedProductionTotalCost + expectedEffImpactTotalCost
            ).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Monthly Impact - To Date`, () => {
        impactCalculator.getSubTotalmonthlyImpactToDate().then(($el) =>
          expect($el.text().trim()).eq(
            `${(
              expectedProdMonthlyCost + expectedEffImpactMonthlyCost
            ).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Monthly Impact - Total`, () => {
        impactCalculator.getSubTotalmonthlyImpactTotal().then(($el) =>
          expect($el.text().trim()).eq(
            `${(
              expectedProdMonthlyCost + expectedEffImpactMonthlyCost
            ).toLocaleString('en-us', {
              style: 'currency',
              currency: 'USD',
            })}`
          )
        );
      });
      it(`SubTotal - Probability Weighted Monthly Impact - To Date`, () => {
        impactCalculator
          .getSubTotalprobWeightedMonthlyImpactToDate()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${(
                expectedProdMonthlyCost + expectedEffImpactMonthlyCost
              ).toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
      it(`SubTotal - Probability Weighted Monthly Impact - Total`, () => {
        impactCalculator
          .getSubTotalprobWeightedMonthlyImpactTotal()
          .then(($el) =>
            expect($el.text().trim()).eq(
              `${(
                expectedProdMonthlyCost + expectedEffImpactMonthlyCost
              ).toLocaleString('en-us', {
                style: 'currency',
                currency: 'USD',
              })}`
            )
          );
      });
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
