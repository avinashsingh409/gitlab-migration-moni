/* eslint-disable cypress/no-unnecessary-waiting */
import { Helper } from '../../support/helpers/helper';
import {
  analyst,
  liteAnalyst,
  resolveStaff,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issuesSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();
const helper = new Helper();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;

describe('Issue snapshot - Follow Issue', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    util.newTabListener();
    issuesPage.getCreateIssueBtn().should('be.visible').click();
    cy.wait(2000);
    util.switchToNewTab();
    user.createIssue();
    user.inputIssueTitle(issueTitle);
    user.saveIssue();
  });
  after(cy.restoreLocalStorage);
  afterEach(cy.restoreLocalStorage);
  it(`should be able to click follow button`, () => {
    user.followIssue();
    helper
      .getSnackBarText()
      .should('have.text', 'Successfully subscribed user.');
    issuesSnapshot.getFollowBtn().should('contain.text', 'Following');
  });
  it(`open Manange Followers window`, () => {
    user.openManageFollowers();
    issuesSnapshot.getManageFollowersWindow().should('be.visible');
  });
  it(`follower details should display`, () => {
    issuesSnapshot.getFollowersDetails().then((followers) => {
      expect(followers[0].name.trim()).eq(resolveStaff.name);
      expect(followers[0].email.trim()).eq(resolveStaff.email);
    });
  });
});
