/* eslint-disable cypress/no-unnecessary-waiting */
import _ from 'lodash';
import { resolveStaff } from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const impactCalculator = new Pages.ImpactCalculatorPage();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Asset Health Monitoring',
  children: [`Test Auto 2 (Dont Touch)`, `Child 1`, `Child 1.2`],
};
const issueTitle = `Impact Calc Vairable Test_${new Date().toISOString()}`;

const impactCalcVariables = {
  ImpactCapacityUoM: 'Tons',
  ImpactUoT: 'Day',
  ImpactCapacityValue: 100,
  ImpactTimeToDaysConversion: 1,
  ImpactCurrency: '$',
  ImpactCurrencyToDollars: 1,
  ImpactProductionRevenueValue: 50,
  ImpactConsumableLabel: 'Steam Cost',
  ImpactConsumableUoM: '$/lb',
  ImpactConsumableValue: 1,
  ImpactNomConsRateLabel: 'Steam Consumption',
  ImpactNomConsRateUoM: 'lb/ton',
  ImpactNomConsRateValue: 500,
  ImpactConsCostToRateConvert: 1,
  ImpactProdCostToRateConvert: 1,
  ImpactNomCapacityUtilization: 1,
};

describe('Issues - Impact Calculator', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
  });
  it(`Open Impact Calc`, () => {
    user.openImpactCalculator();
    impactCalculator
      .getImpactCalcTitle()
      .should('have.text', 'Impact Calculator');
  });
  describe(`Production Impact Variables`, () => {
    before(() => {
      impactCalculator.clickHeaderPanel('ProductionImpact');
      impactCalculator.clickHeaderPanel('EfficiencyImpact');
    });
    it(`ImpactCapacityUoM - production loss`, () => {
      impactCalculator
        .getProductionLossUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCapacityUoM}/${impactCalcVariables.ImpactUoT}`
        );
    });
    it(`ImpactCapacityUoM - Rated Production capacity`, () => {
      impactCalculator
        .getRatedProductionCapacityUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCapacityUoM}/${impactCalcVariables.ImpactUoT}`
        );
    });
    it(`ImpactCapacityUoM - Production Loss`, () => {
      impactCalculator
        .getProductionLossDurationUnit()
        .should('contain.text', impactCalcVariables.ImpactCapacityUoM);
    });
    it(`ImpactCapacityUoM - Lost Margin Opportunity`, () => {
      impactCalculator
        .getLostMarginOpportunityUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCurrency}/${impactCalcVariables.ImpactCapacityUoM}`
        );
    });
    it(`ImpactCapacityUoM - Relevant Production Cost`, () => {
      impactCalculator
        .getRelevantProductionCostUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCurrency}/${impactCalcVariables.ImpactCapacityUoM}`
        );
    });
    it(`ImpactUoT - Production Loss`, () => {
      impactCalculator
        .getProductionLossUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCapacityUoM}/${impactCalcVariables.ImpactUoT}`
        );
    });
    it(`ImpactCapacityValue - Production Rated Production Capacity `, () => {
      impactCalculator
        .getProductionCapacityValue()
        .should('contain.text', `${impactCalcVariables.ImpactCapacityValue}`);
    });
    it(`ImpactCurrency`, () => {
      impactCalculator
        .getLostMarginOpportunityUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCurrency}/${impactCalcVariables.ImpactCapacityUoM}`
        );
      impactCalculator
        .getRelevantProductionCostUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCurrency}/${impactCalcVariables.ImpactCapacityUoM}`
        );
    });
  });
  describe(`Efficiency Impact Variables`, () => {
    it(`ImpactNomConsRateLabel - Consumption Rate Impact`, () => {
      impactCalculator
        .getNomConsRateImpactLabel()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactNomConsRateLabel} Impact`
        );
    });
    it(`ImpactNomConsRateLabel - Consumption Rate Label`, () => {
      impactCalculator
        .getNomConsRateLabel()
        .should('contain.text', impactCalcVariables.ImpactNomConsRateLabel);
    });
    it(`ImpactNomConsRateLabel - Consumption Rate Variance / Change`, () => {
      impactCalculator
        .getImpactNomConsRateLabelChange()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactNomConsRateLabel} Change`
        );
    });
    it('ImpactNomConsRateValue - Consumption Rate Value', () => {
      impactCalculator
        .getNomConsRateValue()
        .should('contain.text', impactCalcVariables.ImpactNomConsRateValue);
    });
    it(`ImpactNomConsRateUoM - Consumption Rate Label Unit`, () => {
      impactCalculator
        .getImpactNomConsRateLabelUnit()
        .should('contain.text', impactCalcVariables.ImpactNomConsRateUoM);
    });
    it(`ImpactNomConsRateUoM - Consumption Rate Variance / Change Unit`, () => {
      impactCalculator
        .getImpactNomConsRateLabelChangeUnit()
        .should('contain.text', impactCalcVariables.ImpactNomConsRateUoM);
    });
    it(`ImpactConsumableLabel`, () => {
      impactCalculator
        .getImpactConsumableLabel()
        .should('contain.text', impactCalcVariables.ImpactConsumableLabel);
    });
    it(`ImpactConsumableValue`, () => {
      impactCalculator
        .getImpactConsumableValue()
        .should('contain.text', impactCalcVariables.ImpactConsumableValue);
    });
    it(`ImpactConsumableUoM`, () => {
      impactCalculator
        .getImpactCosumableUnit()
        .should('contain.text', impactCalcVariables.ImpactConsumableUoM);
    });
    it(`ImpactCapacityValue - Efficient Rated Production Capacity`, () => {
      impactCalculator
        .getEffRatedProductionCapacity()
        .should('contain.text', `${impactCalcVariables.ImpactCapacityValue}`);
    });
    it(`ImpactCapacityUoM - Efficient Rated Production Capacity`, () => {
      impactCalculator
        .getEffRatedProductionCapacityUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCapacityUoM}/${impactCalcVariables.ImpactUoT}`
        );
    });
    it(`ImpactUoT - Efficient Rated Production Capacity`, () => {
      impactCalculator
        .getEffRatedProductionCapacityUnit()
        .should(
          'contain.text',
          `${impactCalcVariables.ImpactCapacityUoM}/${impactCalcVariables.ImpactUoT}`
        );
    });
    it(`ImpactNomCapacityUtilization`, () => {
      impactCalculator
        .getImpactNomCapacityUtilization()
        .should(
          'contain.text',
          impactCalcVariables.ImpactNomCapacityUtilization
        );
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
