import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { IssuesColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { IssuesUser } from '../../support/helpers/user-issues';

const util = new Utils();
// const helper = new Helper();
const issuesPage = new Pages.Issues();
const agGridMain = issuesPage.agGridMain;
const user = new IssuesUser();
const clientToSelect = {
  // parent: 'All Clients',
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Live MD Test Group'],
};

const myViewName1 = `ISSUES_Pin1 ${new Date().toISOString()}`;
const myViewName2 = `ISSUES_Pin2 ${new Date().toISOString()}`;
const columnToSort1: IssuesColumnID = 'Title';
const columnToSort2: IssuesColumnID = 'AssetIssueID';
let sortedItems: string[];

describe('Alert - Sorting', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.deleteAllSavedViews();
    user.restoreGridToDefaults();
    user.showPanel('view'); // close
    // user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    // user.showHideFlyoutWindow();
  });
  describe(`Sort ${columnToSort1}`, () => {
    it(`Click column ${columnToSort1} to sort by ASC`, () => {
      let initialGridItems: string[];
      agGridMain.getItemsByColumnName(columnToSort1).then((gridItems) => {
        initialGridItems = gridItems;
      });

      user.sortColumn(columnToSort1);
      agGridMain.getItemsByColumnName(columnToSort1).then((gridItems) => {
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          _.orderBy(initialGridItems.map((a) => a.toLowerCase()))
        );
      });
      cy.reload();
      user.showHideAssetNavigator();
      agGridMain.getItemsByColumnName(columnToSort1).then((gridItems) => {
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          _.orderBy(initialGridItems.map((a) => a.toLowerCase()))
        );
      });
    });
    it(`Click column ${columnToSort1} to sort by DESC`, () => {
      let initialGridItems: string[];
      issuesPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          // console.log(`1 GridItems: `, gridItems);
          initialGridItems = gridItems;
        });

      user.sortColumn(columnToSort1);
      agGridMain.getItemsByColumnName(columnToSort1).then((gridItems) => {
        sortedItems = gridItems;
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          _.orderBy(initialGridItems.map((a) => a.toLowerCase())).reverse()
        );
      });
    });
    it('save view, restore defaults, and select saved view - check changes persists', () => {
      /* 
      This steps will perform: 
      Saving the current view
      restoring defaults and selecting the saved view
      checking the grid if it loaded (items and length) the correct saved view
      */
      user.showPanel('view');
      user.saveView(myViewName1);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName1);

      agGridMain.getItemsByColumnName(columnToSort1).then((gridItems) => {
        console.table(gridItems);
        console.table(sortedItems);
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          sortedItems.map((a) => a.toLowerCase())
        );
        // expect(gridItems).to.eqls(sortedItems);
      });
      user.showPanel('view'); // this closes the panel
    });
  });
  describe(`Sort ${columnToSort2}`, () => {
    it(`Click column ${columnToSort2} to sort by ASC`, () => {
      let initialGridItems: string[];
      agGridMain.getItemsByColumnName(columnToSort2).then((gridItems) => {
        initialGridItems = gridItems;
      });

      user.sortColumn(columnToSort2);
      agGridMain.getItemsByColumnName(columnToSort2).then((gridItems) => {
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          _.orderBy(initialGridItems.map((a) => a.toLowerCase()))
        );
      });
    });
    it(`Click column ${columnToSort2} to sort by DESC`, () => {
      let initialGridItems: string[];
      agGridMain.getItemsByColumnName(columnToSort2).then((gridItems) => {
        initialGridItems = gridItems;
      });

      user.sortColumn(columnToSort2);
      agGridMain.getItemsByColumnName(columnToSort2).then((gridItems) => {
        sortedItems = gridItems;
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          _.orderBy(initialGridItems.map((a) => a.toLowerCase())).reverse()
        );
      });
    });
    it('save view, restore defaults, and select saved view - check changes persists', () => {
      /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
      user.showPanel('view');
      user.saveView(myViewName2);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName2);

      agGridMain.getItemsByColumnName(columnToSort2).then((gridItems) => {
        console.table(gridItems);
        console.table(sortedItems);
        expect(gridItems.map((a) => a.toLowerCase())).to.eqls(
          sortedItems.map((a) => a.toLowerCase())
        );
      });
      user.showPanel('view'); // this closes the panel
    });
  });
});
