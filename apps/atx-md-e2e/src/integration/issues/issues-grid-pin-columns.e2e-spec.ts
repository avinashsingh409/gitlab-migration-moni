import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { IssuesColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';

const util = new Utils();
const alertsPage = new Pages.Alerts();
const user = new IssuesUser();
const clientToSelect = assetToTest[Cypress.env().env]['im'];

const myViewName1 = `ISSUES_Pin1 ${new Date().toISOString()}`;
const myViewName2 = `ISSUES_Pin2 ${new Date().toISOString()}`;
const myViewName3 = `ISSUES_Pin3 ${new Date().toISOString()}`;
const columnToPin1: IssuesColumnID = 'ActivityStatus';
const columnToPin2: IssuesColumnID = 'ResolutionStatus';

describe('Issues Saved View', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view');
    user.restoreGridToDefaults();
    user.showHideAssetNavigator();
  });
  describe('Issues Pin Columns - Left', () => {
    before(() => {
      // user.showPanel('view');
      user.deleteAllSavedViews(); // delete any existing saved views
    });
    it(`Pin ${columnToPin1} column to the left`, () => {
      user.pinColumn(columnToPin1, 'Left');
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it(`Pin ${columnToPin2} column`, () => {
      user.pinColumn(columnToPin2, 'Left');
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName1);

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.moveMouseToFixLayout();
      user.selectSavedView(myViewName1);

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
  });
  describe('Issues Pin Columns - Right', () => {
    before(() => {
      user.restoreGridToDefaults();
    });
    it(`Pin ${columnToPin1} column to the Right`, () => {
      user.pinColumn(columnToPin1, 'Right');
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it(`Pin ${columnToPin2} column`, () => {
      user.pinColumn(columnToPin2, 'Right');
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName2);

      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.selectSavedView(myViewName2);

      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
  });
  describe('Issues Pin Columns - Left and Right', () => {
    it(`Pin ${columnToPin1} column to the Left`, () => {
      user.pinColumn(columnToPin1, 'Left');
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName3);

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.selectSavedView(myViewName3);

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
    });
  });
});
