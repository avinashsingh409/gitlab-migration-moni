/* eslint-disable cypress/no-unnecessary-waiting */
import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';

const util = new Utils();
// const helper = new Helper();
const issuesPage = new Pages.Issues();
const user = new IssuesUser();
const clientToSelect = {
  // parent: 'All Clients',
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Test Automation Parent (Dont Touch)'],
  // children: ['Live MD Test Group'],
};

describe('ISSUES - AG Grid Multi Select', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    cy.wait(3000);
  });
  it(`Should multi select grid items`, () => {
    user.selectGridItemByRowIndex(0);

    user.multiSelectGridItemByRowIndex(1);
    issuesPage.agGridMain.getSelectedRows().should('have.length', 2);
  });
  it(`add another item in the grid`, () => {
    user.multiSelectGridItemByRowIndex(2);
    issuesPage.agGridMain.getSelectedRows().should('have.length', 3);
  });
  it(`deselect a selected item`, () => {
    user.multiSelectGridItemByRowIndex(1);
    issuesPage.agGridMain.getSelectedRows().should('have.length', 2);
  });
});
