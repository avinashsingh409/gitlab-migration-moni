/* eslint-disable cypress/no-unnecessary-waiting */
/* 
  Before running this test, make sure that the following groups
  and members exist is envi to test
  Test Automation Group 1 - Analyst, liteAnalyst, and ResolveStaff
  Test Automation Group 2 - InsightAnalyst
  Test Automation Group 3 - LiteAnalyst and InsightAnalyst
*/
import _ from 'lodash';
import {
  analyst,
  insightAnalyst,
  liteAnalyst,
  resolveStaff,
  explorerAnalyst,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issueSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;
const groupsToAdd = [
  {
    name: 'Test Automation Group 1',
    members: [analyst, liteAnalyst, resolveStaff],
  },
  {
    name: 'Test Automation Group 2',
    members: [insightAnalyst],
  },
  {
    name: 'Test Automation Group 3',
    members: [liteAnalyst, insightAnalyst],
  },
];
const expectedUsers = [analyst, liteAnalyst, resolveStaff, insightAnalyst];

describe('Issue Snapshot - Add Multiple Groups', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    createIssue(issueTitle);
    user.openManageFollowers();
  });
  it(`should add Group 1`, () => {
    // This step will add Test Automation Group 1
    // Analyst, liteAnalyst, resolveStaff is part of the group
    issueSnapshot.getManageFollowersWindow().should('be.visible');
    user.addFollowersGroup(groupsToAdd[0].name);

    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(3);

      groupsToAdd[0].members.forEach((user, idx) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );
        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups[0]).eq(groupsToAdd[0].name);
        expect(userMatched[0].groups.length).eq(1);
      });
    });
  });
  it(`add another group (Group 2)`, () => {
    // Add Test Automation Group 2
    // only InsightAnalyst is part of this group
    user.addFollowersGroup(groupsToAdd[1].name);

    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(4);

      groupsToAdd[1].members.forEach((user, idx) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );
        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups[0]).eq(groupsToAdd[1].name);
        expect(userMatched[0].groups.length).eq(1);
      });
    });
  });
  it(`add a group with members already added (exists in other groups)`, () => {
    // This will add Test Automation Group 3
    // which LiteAnalyst and Insight Analyst is part of
    // Note LiteAnalyst is also part of Group 1 and InsightAnalyst is part of Group 2
    user.addFollowersGroup(groupsToAdd[2].name);

    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(4);

      expectedUsers.forEach((user) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );

        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups).to.eql(
          _.filter(groupsToAdd, { members: [user] }).map((v) => v.name)
        );
      });
    });
  });
  it(`pop up should display already subscribed users`, () => {
    issueSnapshot
      .getExistingFollowers()
      .should('eql', [liteAnalyst.name, insightAnalyst.name]);
  });
  it(`save and confirm subscribers`, () => {
    issueSnapshot.getManageSaveClose().click();
    issueSnapshot
      .getFollowerSaveConfirm()
      .should(
        'have.text',
        ' Your changes to the issue followers have been saved. '
      );
    cy.get(`mat-dialog-container button`).contains('OK').click();
    user.openManageFollowers();

    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(4);

      expectedUsers.forEach((user) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );

        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups).to.eql(
          _.filter(groupsToAdd, { members: [user] }).map((v) => v.name)
        );
      });
    });
  });
  it(`Add a user not part of a group`, () => {
    expectedUsers.push(explorerAnalyst);
    user.addFollowers([explorerAnalyst]);
    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(5);

      expectedUsers.forEach((user) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );

        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups).to.eql(
          _.filter(groupsToAdd, { members: [user] }).map((v) => v.name)
        );
      });
    });
  });
  it(`remove a user (part of a group), should display group column properly`, () => {
    /* 
    Removing a subscribed user in Manage Issue Followers window that has a group
    will remove the group name to the other members of that group in the group column
    Since Lite Analyst is part of Group 1 and Group 3, the group names (Group 1 and Group 3)
    will be removed for users Analyst and ResolveStaff (group1) and InsightAnalyst (group3)
    Only Test Automation Group 2 will be shown. 
    */
    user.removeFollower(liteAnalyst);

    _.remove(groupsToAdd, (o) => o.members.includes(liteAnalyst));
    _.remove(expectedUsers, liteAnalyst);

    user.saveManageFollowers();

    issueSnapshot.getFollowersDetails().then((followers) => {
      expect(followers.length).eq(4);
      expectedUsers.forEach((user) => {
        const userMatched = followers.filter(
          (follower) =>
            follower.email.toLowerCase() === user.email.toLowerCase()
        );

        expect(userMatched.length).eq(1);
        expect(userMatched[0].name).eq(user.name);
        expect(userMatched[0].groups).to.eql(
          _.filter(groupsToAdd, { members: [user] }).map((v) => v.name)
        );
      });
    });
  });
});

function createIssue(issueTitle) {
  util.newTabListener();
  issuesPage.getCreateIssueBtn().should('be.visible').click();
  cy.wait(2000);
  util.switchToNewTab();
  user.createIssue();
  user.inputIssueTitle(issueTitle);
  user.saveIssue();
}
