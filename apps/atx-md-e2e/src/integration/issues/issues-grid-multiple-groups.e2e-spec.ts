/* eslint-disable cypress/no-unnecessary-waiting */

/*
  This test is designed to test with expected data using 
  asset 'Test Automation Parent (Dont Touch)' > 'Test Auto Child 1'
  IF the issues and asset are not there. They will need to be created first
*/

import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { IssuesColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
const issuesPage = new Pages.Issues();
const user = new IssuesUser();
const clientToSelect = {
  // parent: 'All Clients',
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Test Automation Parent (Dont Touch)', 'Test Auto Child 1'],
};
const myViewName1 = `IM_RowGroups ${new Date().toISOString()}`;
const columnToGroup1: IssuesColumnID = 'CategoryDesc';
const columnToGroup2: IssuesColumnID = 'ResolutionStatus';
let columnItems1: string[];
let columnItems2: string[];
let uniqueItems1: string[];
let uniqueItems2: string[];
const columnsToHide = {
  toHide: [
    'Impact',
    'Status',
    'Created',
    'Assigned',
    'Age (Days)',
    'Scorecard',
    'Type',
  ],
};
const testData = [
  {
    title: 'Test Issue 1 - Low',
    category: 'Plant Operations',
    resolution: 'Site Reviewing',
  },
  {
    title: 'Test Issue 2 - Medium',
    category: 'Outage Maintenance',
    resolution: 'Diagnosing',
  },
  {
    title: 'Test Issue 3 - High',
    category: 'Plant Operations',
    resolution: 'Site Reviewing',
  },
  {
    title: 'Test Issue 4',
    category: 'Online Maintenance',
    resolution: 'Work Defined',
  },
  {
    title: 'Test Issue 5',
    category: 'Outage Maintenance',
    resolution: 'Diagnosing',
  },
  {
    title: 'Test Issue 5',
    category: 'Plant Operations',
    resolution: 'Diagnosing',
  },
];

describe('ISSUES - Grid Row Groups', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.deleteAllSavedViews();
    user.restoreGridToDefaults();
    user.showPanel('column');
    user.setColumns(columnsToHide);
    user.showHideAssetNavigator();
    user.showPanel('column'); // close
  });
  it(`group by ${columnToGroup1}`, () => {
    // Get all items in a column
    issuesPage.agGridMain.getItemsByColumnName(columnToGroup1).then((items) => {
      uniqueItems1 = [...new Set(items)] as string[];
      columnItems1 = items;
    });
    issuesPage.agGridMain.getItemsByColumnName(columnToGroup2).then((items) => {
      uniqueItems2 = [...new Set(items)] as string[];
      columnItems2 = items;
    });

    user.groupByColumn(columnToGroup1);
    // Check if grouped column is correct.
    issuesPage.agGridMain.getGroupNames().then((groupNames) => {
      expect(groupNames).to.have.length(uniqueItems1.length);
      expect(groupNames).to.include.members([...uniqueItems1]);
    });
  });
  it(`add another group by ${columnToGroup2}`, () => {
    user.groupByColumn(columnToGroup2);

    issuesPage.agGridMain.getGroupNames().then((groupNames) => {
      expect(groupNames).to.have.length(uniqueItems1.length);
      expect(groupNames).to.include.members([...uniqueItems1]);
    });
  });
  it(`group row item count correct`, () => {
    issuesPage.agGridMain.getRowGroupDetails().then((groupDetails) => {
      groupDetails.forEach((group) => {
        expect(columnItems1.filter((i) => i === group.name).length).eq(
          Number(group.itemCount)
        );
      });
    });
  });
  describe(`Expand first level group`, () => {
    it(`expand a group`, () => {
      user.expandGroup('Plant Operations');

      issuesPage.agGridMain
        .getRowGroups()
        .first()
        .get(`.ag-group-expanded`)
        .should('be.visible');
    });
    it(`group members count is correct`, () => {
      issuesPage.agGridMain.getGroupMembersByLevel(1).then(($el) => {
        expect($el.length).eq(2, `Grouped members count should match`);
      });
    });
    it(`group members details should be correct (name and count)`, () => {
      issuesPage.agGridMain.getRowGroupDetails(1).then((groups) => {
        groups.forEach((group) => {
          const tempTestData = _.filter(testData, {
            category: 'Plant Operations',
            resolution: group.name,
          });
          expect(group.itemCount).eq(
            tempTestData.length,
            `${group.name} = ${group.itemCount} member should be equal to ${tempTestData.length}`
          );
        });
      });
    });
  });
  describe(`Expand second level group`, () => {
    it(`expand second grouped row`, () => {
      user.expandGroup('Site Reviewing');

      issuesPage.agGridMain
        .getRowGroups()
        .first()
        .get(`.ag-group-expanded`)
        .should('be.visible');
    });
    it(`grid row item details should be correct`, () => {
      const tempTestData = _.map(
        _.filter(testData, {
          category: 'Plant Operations',
          resolution: 'Site Reviewing',
        }),
        'title'
      );
      issuesPage.agGridMain
        .getItemsByColumnName('Title', 2)
        .should('deep.equal', tempTestData);
    });
  });
  describe(`Save grid`, () => {
    it(`Save view, restore, open saved view, and verify`, () => {
      user.showPanel('view');
      user.saveView(myViewName1);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName1);
      issuesPage.agGridMain.getGroupNames().then((groupNames) => {
        expect(groupNames).to.have.length(uniqueItems1.length);
        expect(groupNames).to.include.members([...uniqueItems1]);
      });
    });
    it(`expand a group`, () => {
      user.expandGroup('Plant Operations');

      issuesPage.agGridMain
        .getRowGroups()
        .first()
        .get(`.ag-group-expanded`)
        .should('be.visible');
    });
    it(`group members count is correct`, () => {
      issuesPage.agGridMain.getGroupMembersByLevel(1).then(($el) => {
        expect($el.length).eq(2, `Grouped members count should match`);
      });
    });
    it(`group members details should be correct (name and count)`, () => {
      issuesPage.agGridMain.getRowGroupDetails(1).then((groups) => {
        groups.forEach((group) => {
          const tempTestData = _.filter(testData, {
            category: 'Plant Operations',
            resolution: group.name,
          });
          expect(group.itemCount).eq(
            tempTestData.length,
            `${group.name} = ${group.itemCount} member should be equal to ${tempTestData.length}`
          );
        });
      });
    });
    it(`expand second grouped row`, () => {
      user.expandGroup('Site Reviewing');

      issuesPage.agGridMain
        .getRowGroups()
        .first()
        .get(`.ag-group-expanded`)
        .should('be.visible');
    });
    it(`grid row item details should be correct`, () => {
      const tempTestData = _.map(
        _.filter(testData, {
          category: 'Plant Operations',
          resolution: 'Site Reviewing',
        }),
        'title'
      );
      issuesPage.agGridMain
        .getItemsByColumnName('Title', 2)
        .should('deep.equal', tempTestData);
    });
  });
});
