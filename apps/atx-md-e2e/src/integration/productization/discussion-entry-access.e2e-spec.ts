/* eslint-disable cypress/no-unnecessary-waiting */
import {
  analyst,
  discussionManager,
  liteAnalyst,
  resolveCoordinator,
  resolveStaff,
} from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issuesSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;
const userAccounts = [
  { user: resolveStaff, totalEntries: 1, admin: false },
  { user: analyst, totalEntries: 2, admin: false },
  { user: liteAnalyst, totalEntries: 3, admin: false },
  { user: resolveCoordinator, totalEntries: 4, admin: true },
  { user: discussionManager, totalEntries: 5, admin: true },
];
describe('Productization Role - Discussion Entry Access', () => {
  before(() => {
    user.logIn(resolveStaff);
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
    util.newTabListener();
    issuesPage.getCreateIssueBtn().should('be.visible').click();
    cy.wait(2000);
    util.switchToNewTab();
    user.createIssue();
    user.inputIssueTitle(issueTitle);
    user.saveIssue();
  });
  after(cy.restoreLocalStorage);
  afterEach(cy.restoreLocalStorage);

  userAccounts.forEach((userAccount) => {
    describe.only(`${userAccount.user.name.toUpperCase()}`, () => {
      it(`add new discussion entry by ${userAccount.user.name}`, () => {
        // first user only has 1 entry no need to refresh page and login
        if (userAccount.totalEntries > 1)
          clearLocalReloadLogin(userAccount.user);
        const entryBlog = {
          title: 'test title',
          discussionBody: `this is body by ${userAccount.user.name}`,
        };
        user.addDiscussionEntry(entryBlog);
        issuesSnapshot
          .getDiscussionEntries()
          .should('be.visible')
          .should('have.length', userAccount.totalEntries);
      });
      it(`EDIT and DELETE button should be visible for own entry`, () => {
        issuesSnapshot.getDiscussionEntries().then(() => {
          issuesSnapshot
            .getDiscussionEntryByCreatedBy(
              userAccount.user.email.toLocaleLowerCase()
            )
            .should('have.length', 1)
            .each(($discussionEntry) => {
              expect(
                $discussionEntry.find(`[data-cy="editEntryBtn"]`).length
              ).eq(1, 'Edit button is not visible');
              expect(
                $discussionEntry.find(`[data-cy="deleteEntryBtn"]`).length
              ).eq(1, 'Delete button is not visible');
            });

          issuesSnapshot
            .getDiscussionEntryActionBtns(userAccount.user.email, 'edit')
            .should('have.length', 1);
          issuesSnapshot
            .getDiscussionEntryActionBtns(userAccount.user.email, 'delete')
            .should('have.length', 1);
        });
      });
      // step is skipped if total discussion is > 0 (first user)
      if (userAccount.totalEntries > 1) {
        it(`should${
          userAccount.admin ? ' ' : ' NOT '
        }have EDIT & DELETE access to other entries`, () => {
          issuesSnapshot
            .getDiscussionEntryNotByCreatedBy(userAccount.user.email)
            .each(($discussionEntry) => {
              expect(
                $discussionEntry.find(`[data-cy="editEntryBtn"]`).length
              ).eq(
                userAccount.admin ? 1 : 0,
                `Edit button is${
                  userAccount.admin ? ' NOT ' : ' '
                }visible for other user's entry`
              );
              expect(
                $discussionEntry.find(`[data-cy="deleteEntryBtn"]`).length
              ).eq(
                userAccount.admin ? 1 : 0,
                `Delete button is${
                  userAccount.admin ? ' NOT ' : ' '
                }visible for other user's entry`
              );
            });
        });
      }
    });
  });
});

function clearLocalReloadLogin(username) {
  cy.clearLocalStorage();
  Cypress.env('restoreLocalStorage', false); // set global variable to false
  cy.reload(); // now cy.reload() won't restore local storage
  user.logIn(username, false);
}
