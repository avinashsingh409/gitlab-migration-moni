/* eslint-disable cypress/no-unnecessary-waiting */
import { analyst } from '../../support/helpers/user-accounts';
import { AppNamesNewUI, NavType } from '../../support/helpers/user-data';
import { IssuesUser } from '../../support/helpers/user-issues';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const issuesPage = new Pages.Issues();
const issuesSnapshot = new Pages.IssuesSnapshot();
const user = new IssuesUser();

const util = new Utils();
const clientToSelect = {
  parent: 'Demo Clients',
  children: [`Test Automation (Don't Touch)`],
};
const issueTitle = `issue_${new Date().toISOString()}`;

describe('Productization Role - Analyst', () => {
  before(() => {
    user.logIn(analyst);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.issues, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    cy.saveLocalStorage();
  });
  after(cy.restoreLocalStorage);
  afterEach(cy.restoreLocalStorage);

  describe(`Issues Management`, () => {
    it(`Create Issue button should be visible`, () => {
      issuesPage.getCreateIssueBtn().should('be.visible');
    });
    it(`Grid Filter button should be visible`, () => {
      issuesPage.getFilterGridBtn().should('be.visible');
    });
    it(`Grid Refresh button should be visible`, () => {
      issuesPage.getRefreshGridBtn().should('be.visible');
    });
    it(`Grid Download button should be visible`, () => {
      issuesPage.getDownloadGridBtn().should('be.visible');
    });
  });
  describe(`Issue Snapshot`, () => {
    it(`Should be able to create issue`, () => {
      cy.wait(2000);
      util.newTabListener();
      issuesPage.getCreateIssueBtn().click();
      util.switchToNewTab();
      user.createIssue();
    });
    it(`SAVE button should be enabled`, () => {
      issuesSnapshot.getSaveIssueBtn().should('be.enabled');
    });
    it(`CANCEL button should be enabled`, () => {
      issuesSnapshot.getCancelBtn().should('be.enabled');
    });
    it(`FOLLOW button should NOT be enabled`, () => {
      issuesSnapshot.getFollowBtn().should('not.be.enabled');
    });
    it(`SEND button should NOT be enabled`, () => {
      issuesSnapshot.getSendBtn().should('not.be.enabled');
    });
    it(`should be able to SAVE issue`, () => {
      issuesSnapshot.getIssueTitle().clear().type(issueTitle);
      issuesSnapshot.getSaveIssueBtn().click();
      user.closeSnackBar();
    });
    it(`ACTION buttons state should be correct after save`, () => {
      issuesSnapshot.getSaveIssueBtn().should('be.disabled');
      expect(Cypress.$(`[data-cy="cancelIssueBtn"]`).length).eq(0);
      issuesSnapshot.getFollowBtn().should('be.enabled');
      issuesSnapshot.getSendBtn().should('be.enabled');
    });
    it(`Header fields should be enabled`, () => {
      issuesSnapshot.getIssueTitle().should('be.enabled');
      issuesSnapshot
        .getActivityStatusDropdown()
        .invoke('attr', 'class')
        .should('not.include', 'select-disabled');
      issuesSnapshot
        .getResolutionStatusDropdown()
        .invoke('attr', 'class')
        .should('not.include', 'select-disabled');
      issuesSnapshot
        .getAssignedToTxtBox()
        .invoke('attr', 'readonly')
        .should('not.eq', 'true');
      issuesSnapshot
        .getPriorityDropdown()
        .invoke('attr', 'class')
        .should('not.include', 'select-disabled');
      issuesSnapshot.getResolvedByDateTxtBox().should('be.enabled');
    });
    it(`Asset Area - Issues By Owning Asset should be visible`, () => {
      issuesSnapshot.getIssuesByOwningAssetBtn().should('be.enabled');
    });
    it(`Asset Area - Alerts By Owning Asset should be visible`, () => {
      issuesSnapshot.getAlertsByOwningAssetBtn().should('be.visible');
    });
  });
});
