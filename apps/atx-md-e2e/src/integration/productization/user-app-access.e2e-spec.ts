import {
  admin,
  analyst,
  eventsAnalyst,
  explorerAnalyst,
  insightAnalyst,
  liteAnalyst,
  resolveCoordinator,
  resolveStaff,
  staff,
  userAdmin,
} from '../../support/helpers/user-accounts';
import { User } from '../../support/helpers/user';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';
import { AppNamesNewUI } from '../../support/helpers/user-data';

const landingPage = new Pages.LandingPage();
const leftNavPane = new Pages.Navigation();
const user = new User();
const util = new Utils();
const userAccounts = [
  analyst,
  liteAnalyst,
  staff,
  admin,
  explorerAnalyst,
  insightAnalyst,
  resolveCoordinator,
  resolveStaff,
  // eventsAnalyst,
  // userAdmin,
];
const apps = [
  AppNamesNewUI.alerts,
  AppNamesNewUI.issues,
  AppNamesNewUI.dataExplorer,
  AppNamesNewUI.dashboards,
  AppNamesNewUI.modelConfig,
  AppNamesNewUI.opMode,
  AppNamesNewUI.assetConfig,
  AppNamesNewUI.userAdmin,
];

describe('Productization User Role App Access', () => {
  userAccounts.forEach((userAccount) => {
    describe(`${userAccount.name} - Landing App Tile Test`, () => {
      before(() => {
        cy.clearLocalStorage();
        user.logIn(userAccount);
        //util.dismissAtonixPopUp();
      });
      apps.forEach((app) => {
        const appExists = userAccount.appAccess.includes(app);
        it(`${app} tile should ${appExists ? '' : 'NOT '}display`, () => {
          expect(landingPage.appTileExists(app)).to.equal(appExists);
        });
      });
      apps.forEach((app) => {
        const leftNavBtnVisible = userAccount.appAccess.includes(app)
          ? 'be.visible'
          : 'not.be.visible';
        it(`LEFT NAV button ${app} should ${
          userAccount.appAccess.includes(app) ? '' : 'NOT '
        }display`, () => {
          leftNavPane.leftNav.getApps(app).should(leftNavBtnVisible);
        });
      });
    });
  });
});
