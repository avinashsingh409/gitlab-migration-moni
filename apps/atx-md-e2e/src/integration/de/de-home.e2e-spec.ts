import { ApiStub } from '../../support/helpers/stub/api-stub';
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import { DataExplorerUser } from '../../support/helpers/user-de';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const user = new DataExplorerUser();
const dePage = new Pages.DataExplorerHome();
const timeSlider = new Pages.TimeSlider();
const sideNav = new Pages.Navigation();
const alertsUser = new AlertsUser();
const util = new Utils();
const apiStub = new ApiStub();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

describe('Data Explorer - home', () => {
  before(() => {
    user.logIn(userObj.default);
    apiStub.tagGrid();
    user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    // user.showHideAssetNavigator();
    cy.wait('@tagGrid', { timeout: 10000 });
  });
  it(`Charts List pane`, () => {
    dePage.chartList().should('exist');
  });
  it(`Tabs should be present`, () => {
    const tabs = [
      'Series',
      'Tags List',
      'Axis',
      'Static Curves',
      'Pins',
      'Advanced',
    ];
    dePage.getTabs().should('have.length', 6);
    dePage.getTabs().each(($el, idx) => {
      expect($el.text()).eq(tabs[idx]);
    });
  });
  it(`Time Slider should be visible`, () => {
    timeSlider.getTimeSlider().should('be.visible');
  });
  it(`Time slider can be hidden or shown`, () => {
    sideNav.leftNav.toggleTimeSliderBtn().click();
    sideNav.leftNav
      .toggleTimeSliderBtn()
      .invoke('attr', 'class')
      .should('not.include', 'nav-btn-selected-side');

    sideNav.leftNav.toggleTimeSliderBtn().click();
    sideNav.leftNav
      .toggleTimeSliderBtn()
      .invoke('attr', 'class')
      .should('include', 'nav-btn-selected-side');
  });
});
