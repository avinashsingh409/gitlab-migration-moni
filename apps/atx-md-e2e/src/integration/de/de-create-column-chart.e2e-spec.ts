import { stringify } from 'querystring';
import { ApiStub } from '../../support/helpers/stub/api-stub';
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  chartAggregations,
  assetToTest,
} from '../../support/helpers/user-data';
import { DataExplorerUser } from '../../support/helpers/user-de';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const user = new DataExplorerUser();
const dataExplorer = new Pages.DataExplorerHome();
const timeSlider = new Pages.TimeSlider();
const sideNav = new Pages.Navigation();
const util = new Utils();
const apiStub = new ApiStub();
const agGrid = new Pages.AgGridMain();
const highCharts = new Pages.HighCharts();

const clientToSelect = assetToTest[Cypress.env().env]['alerts'];
let gridRowToSelect: Cypress.Chainable;
let asset, variable, tagName, description, unit: string;
const newChartName = `chart name ${new Date().toISOString()}`;
const chartType = 'Column';

describe('Data Explorer - Column Chart', () => {
  before(() => {
    user.logIn(userObj.default);
    apiStub.tagGrid();
    apiStub.tagDataFiltered();
    user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    // user.showHideAssetNavigator();
    cy.wait('@tagGrid', { timeout: 60000 });
    cy.wait('@tagDataFiltered', { timeout: 30000 });
  });
  it(`Create ${chartType} Chart`, () => {
    user.createNewCreate(chartType);
  });
  it(`Tags should display in the ag-grid`, () => {
    agGrid.getGridRows(30000).should('have.length.greaterThan', 0);
  });
  it(`Select a tag from the Tag list`, () => {
    apiStub.tagDataFiltered();
    user.showHideAssetNavigator();
    agGrid
      .getGridRows()
      .eq(1)
      .then(($el) => {
        cy.wrap($el)
          .find('[col-id]')
          .then((col) => {
            tagName = col.eq(2).text().trim();
            unit = col.eq(4).text().trim();
          })
          .dblclick();
      });
  });
  it(`Chart should load series`, () => {
    highCharts
      .getChartSeries()
      .invoke('attr', 'class')
      .then((c) =>
        expect(c).contain(`highcharts-${chartType.toLowerCase()}-series`)
      );
  });
  it(`Tag Legend should display`, () => {
    highCharts.getlegends().should('contain.text', tagName);
  });
  it(`Axis unit should display`, () => {
    highCharts.getYAxisLabel().should('contain.text', unit);
  });
  it(`Aggregation options should display`, () => {
    dataExplorer.getAggregationDropDownBtn().click();
    dataExplorer
      .getAggregationOptions()
      .should('have.length', chartAggregations.length)
      .each((aggregation) => chartAggregations.includes(aggregation.text()));

    util.clickOverlay();
  });
  it(`save chart`, () => {
    user.saveChart(newChartName);
    dataExplorer.getChartTitle().should('have.text', newChartName);
  });
  it(`Chart, Tag Legend, and Axis unit should display in chart`, () => {
    highCharts.getChartSeries().should('be.visible');
    highCharts.getlegends().should('contain.text', tagName);
    highCharts.getYAxisLabel().should('contain.text', unit);
  });
  it(`ChartName should appear in Chart list`, () => {
    user.showHideAssetNavigator();
    dataExplorer.getChartNames().should('contain.text', newChartName);
  });
});
