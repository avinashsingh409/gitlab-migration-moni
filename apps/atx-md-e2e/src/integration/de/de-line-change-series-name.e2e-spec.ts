import { ApiStub } from '../../support/helpers/stub/api-stub';
import { UserAggridCommon } from '../../support/helpers/user-aggrid-common';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  ChartTypesSingle,
  assetToTest,
  envToTest,
} from '../../support/helpers/user-data';
import { DataExplorerUser } from '../../support/helpers/user-de';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const user = new DataExplorerUser();
const dataExplorer = new Pages.DataExplorerHome();
const timeSlider = new Pages.TimeSlider();
const sideNav = new Pages.Navigation();
const util = new Utils();
const apiStub = new ApiStub();
const agGrid = new Pages.AgGridMain();
const highCharts = new Pages.HighCharts();
// const clientToSelect = assetToTest[Cypress.env().env]['alerts'];
const clientToSelect = assetToTest[Cypress.env().env || 'local']['alerts'];
const agGridUser = new UserAggridCommon();

let asset, variable, tagName, description, unit: string;
const newChartName = `chart name ${new Date().toISOString()}`;
const chartType = 'Line';
const newSeriesName = 'Series 1';

describe('Data Explorer - Rename Series Name', () => {
  before(() => {
    user.logIn(userObj.default);
    apiStub.tagGrid();
    apiStub.tagDataFiltered();
    user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    // user.showHideAssetNavigator();
    cy.wait('@tagGrid', { timeout: 60000 });
    cy.wait('@tagDataFiltered', { timeout: 30000 });
  });
  it(`Create Line Chart`, () => {
    user.createNewCreate(chartType);
  });
  it(`Select a tag from the Tag list`, () => {
    apiStub.tagDataFiltered();
    user.showHideAssetNavigator();
    agGrid
      .getGridRows()
      .eq(1)
      .then(($el) => {
        cy.wrap($el)
          .find('[col-id]')
          .then((col) => {
            tagName = col.eq(2).text().trim();
            unit = col.eq(4).text().trim();
          })
          .dblclick();
      });
  });
  it(`Rename a series`, () => {
    user.openTab('Series');
    user.inputSeriesName(newSeriesName);

    dataExplorer.getSeriesTabDetails().then((details) => {
      const seriesRow = details[0];
      expect(seriesRow.seriesName).eq(newSeriesName);
      expect(seriesRow.tagName).eq(tagName);
      expect(seriesRow.isVisible).eq(true);
      expect(seriesRow.isBold).eq(false);
      expect(seriesRow.xAxis).eq(false);
      expect(seriesRow.stackType).eq('Default');
    });
  });
  it(`Verify Tag legend updates`, () => {
    highCharts.getlegends().should('contain.text', newSeriesName);
  });
  it(`Save Chart and verify Series Name change`, () => {
    user.saveChart(newChartName);
    highCharts.getChartSeries().should('be.visible');
    highCharts.getlegends().should('contain.text', newSeriesName);
    highCharts.getYAxisLabel().should('contain.text', unit);
  });
});
describe('Data Explorer - Add Another Tag after rename (BUG 50720)', () => {
  it(`Renamed `, () => {
    user.createNewCreate(chartType);

    apiStub.tagDataFiltered();
    user.showHideAssetNavigator();
    agGrid.getGridRows().eq(1).dblclick();

    user.openTab('Series');
    user.inputSeriesName(newSeriesName);
    user.openTab('Tags List');
    // add another tag by double clicking on tag #2
    agGridUser.doubleClickItemInGridByIdx(2).then((gridItemDetail) => {
      user.openTab('Series');

      dataExplorer.getSeriesTabDetails().then((details) => {
        const seriesRow1 = details[0];
        expect(seriesRow1.seriesName, `Renamed tag series name`).eq(
          newSeriesName
        );
        expect(seriesRow1.tagName, `Renamed tag tag name`).eq(tagName);
        expect(seriesRow1.isVisible).eq(true);
        expect(seriesRow1.isBold).eq(false);
        expect(seriesRow1.xAxis).eq(false);
        expect(seriesRow1.stackType).eq('Default');
        const seriesRow2 = details[1];

        expect(seriesRow2.seriesName).eq(gridItemDetail[3]);
        expect(seriesRow2.tagName).eq(gridItemDetail[2]);
        expect(seriesRow2.isVisible).eq(true);
        expect(seriesRow2.isBold).eq(false);
        expect(seriesRow2.xAxis).eq(false);
        expect(seriesRow2.stackType).eq('Default');
      });
    });
    user.openTab('Series');
  });
  it(`Save Chart and verify Series Name change`, () => {
    user.saveChart(newChartName + '2');
    highCharts.getChartSeries().should('be.visible');
    highCharts.getlegends().should('contain.text', newSeriesName);
    highCharts.getYAxisLabel().should('contain.text', unit);
  });
});
