import { ApiStub } from '../../support/helpers/stub/api-stub';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  ChartTypesSingle,
  assetToTest,
} from '../../support/helpers/user-data';
import { DataExplorerUser } from '../../support/helpers/user-de';
import { Utils } from '../../support/helpers/utils';
import * as Pages from '../../support/page/pages';

const user = new DataExplorerUser();
const dataExplorer = new Pages.DataExplorerHome();
const timeSlider = new Pages.TimeSlider();
const sideNav = new Pages.Navigation();
const util = new Utils();
const apiStub = new ApiStub();
const agGrid = new Pages.AgGridMain();
const highCharts = new Pages.HighCharts();

const clientToSelect = assetToTest[Cypress.env().env]['de'];
let asset, variable, tagName, description, unit: string;
const newChartName = `chart name ${new Date().toISOString()}`;
const chartType = 'Bar';
const newSeriesName = 'Series 1';

describe('Data Explorer - home', () => {
  before(() => {
    user.logIn(userObj.default);
    apiStub.tagGrid();
    apiStub.tagDataFiltered();
    user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    // user.showHideAssetNavigator();
    cy.wait('@tagGrid', { timeout: 60000 });
    cy.wait('@tagDataFiltered', { timeout: 30000 });
  });
  it(`Create Area Chart`, () => {
    user.createNewCreate(chartType);
  });
  it(`Select a tag from the Tag list`, () => {
    apiStub.tagDataFiltered();
    user.showHideAssetNavigator();
    agGrid
      .getGridRows()
      .eq(1)
      .then(($el) => {
        cy.wrap($el)
          .find('[col-id]')
          .then((col) => {
            tagName = col.eq(2).text().trim();
            unit = col.eq(4).text().trim();
          })
          .dblclick();
      });
  });
  it(`Rename a series`, () => {
    user.openTab('Series');
    user.inputSeriesName(newSeriesName);

    dataExplorer.getSeriesTabDetails().then((details) => {
      const seriesRow = details[0];
      expect(seriesRow.seriesName).eq(newSeriesName);
      expect(seriesRow.tagName).eq(tagName);
      expect(seriesRow.isVisible).eq(true);
      expect(seriesRow.isBold).eq(false);
      expect(seriesRow.xAxis).eq(false);
      expect(seriesRow.stackType).eq('Default');
    });
  });
  it(`Verify Tag legend updates`, () => {
    highCharts.getlegends().should('contain.text', newSeriesName);
  });
  it(`Save Chart and verify Series Name change`, () => {
    user.saveChart(newChartName);
    highCharts.getChartSeries().should('be.visible');
    highCharts.getlegends().should('contain.text', newSeriesName);
    highCharts.getYAxisLabel().should('contain.text', unit);
  });
});
