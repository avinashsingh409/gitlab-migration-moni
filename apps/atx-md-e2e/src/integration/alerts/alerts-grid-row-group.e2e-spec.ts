import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];
const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const myViewName1 = `ALERTS_Pin1 ${new Date().toISOString()}`;
const columnToGroup1: AlertsColumnID = 'UnitAbbrev';
let columnItems: string[];
let uniqueItems: string[];

describe('ALERTS - Grid Row Groups', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.restoreGridToDefaults();
    user.showPanel('view'); // close
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
  });
  it(`group by ${columnToGroup1}`, () => {
    alertsPage.agGridMain.getItemsByColumnName('UnitAbbrev').then((items) => {
      uniqueItems = [...new Set(items)] as string[];
      columnItems = items;
    });

    user.groupByColumn('UnitAbbrev');

    alertsPage.agGridMain.getGroupNames().then((groupNames) => {
      expect(groupNames).to.have.length(uniqueItems.length);
      expect(groupNames).to.include.members([...uniqueItems]);
    });
  });
  it(`group row item count correct`, () => {
    alertsPage.agGridMain.getRowGroupDetails().then((groupDetails) => {
      groupDetails.forEach((group) => {
        expect(columnItems.filter((i) => i === group.name).length).eq(
          Number(group.itemCount)
        );
      });
    });
  });
  it(`expand a group`, () => {
    user.expandGroup(uniqueItems[0]);
    alertsPage.agGridMain
      .getRowGroups()
      .first()
      .get(`.ag-group-expanded`)
      .should('be.visible');
  });
  it(`group members count is correct`, () => {
    alertsPage.agGridMain.getGroupMembersByLevel(1).then(($el) => {
      expect($el.length).eq(
        columnItems.filter((items) => items === uniqueItems[0]).length,
        `Grouped members count should match`
      );
    });
  });
  it(`group members details should be correct`, () => {
    alertsPage.agGridMain
      .getColumnItemsByGroupLevel(1, columnToGroup1)
      .then((colItems) => {
        colItems.forEach((item) => {
          expect(item).eq(uniqueItems[0]);
        });
      });
  });
});
