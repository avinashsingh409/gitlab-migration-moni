import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import { analyst } from '../../support/helpers/user-accounts';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const columnOrder = [
  'Model Name',
  'Issues',
  'Watch',
  'Maintenance',
  'Alert',
  'External ID',
  'Note Priority',
  'Unit',
  '% OOB',
  '% EXP',
  'Actual',
  'Lower',
  'Expected',
  'Upper',
  'Units',
  'Last Note',
  'Last Note Date',
  'Mode',
];

describe('ALERTS - SAVED VIEWS', () => {
  before(() => {
    user.logIn(analyst);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
    user.restoreGridToDefaults();
  });
  describe(`Reorder`, () => {
    it(`Saved view Reorder should load correctly`, () => {
      user.selectSavedView('view1_dontDelete');
      user.showPanel('column');

      cy.wrap(alertsPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('view1_dontDelete');
      user.showPanel('column');

      cy.wrap(alertsPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
  });
  describe(`Group`, () => {
    const modelTypes = [
      'APR',
      'Fixed Limit',
      'Frozen Data',
      'Moving Average',
      'Rolling Average',
    ];
    before(() => {
      user.showPanel('view');
    });
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('group1_dontDelete');
      alertsPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.restoreGridToDefaults();
      user.selectSavedView('group1_dontDelete');

      alertsPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });
    });
  });
  describe(`Group + Reorder`, () => {
    const modelTypes = [
      'APR',
      'Fixed Limit',
      'Frozen Data',
      'Moving Average',
      'Rolling Average',
    ];
    const columnOrder = [
      'Model Type',
      'Model Name',
      'Tag Name',
      'Asset',
      'Unit',
      'Diagnose',
      'Watch',
      'Maintenance',
      'Alert',
      'External ID',
      'Issues',
      'Note Priority',
      '% OOB',
      '% EXP',
      'Actual',
      'Lower',
      'Expected',
      'Upper',
    ];
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('group3_dontDelete');
      user.showPanel('column');
      alertsPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });

      cy.wrap(alertsPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('group3_dontDelete');
      user.showPanel('column');

      alertsPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });

      cy.wrap(alertsPage.sideButtons.column.getColumnNames()).then((cols) => {
        expect(cols).to.deep.eq(columnOrder);
      });
    });
  });
  describe(`Filter`, () => {
    it(`Saved view Filter should load correctly`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      util.loadAgGridForWait('AlertsGrid');
      user.selectSavedView('filter1');
      cy.wait('@AlertsGrid', { timeout: 15000 });
      alertsPage.headerPanel.getFilterChips().then((el) => {
        expect(el.length).to.eq(4);
        cy.wrap(el)
          .find('[data-cy="filter-column-name"]')
          .then((e) => {
            expect(e[0].textContent).eq('Alert');
            expect(e[1].textContent).eq('Watch');
            expect(e[2].textContent).eq('Ignore');
            expect(e[3].textContent).eq('Model Type');
          });
      });
      alertsPage.agGridMain
        .getItemsByColumnName('ModelTypeAbbrev')
        .then((items) => {
          items.forEach((item) => expect(item).eq('APR'));
        });
    });
  });
});
