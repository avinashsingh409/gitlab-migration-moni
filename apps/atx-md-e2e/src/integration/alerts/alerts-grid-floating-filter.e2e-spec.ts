import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const agGridColumnFloatingFilter = alertsPage.agGridFloatingFilter;
const myViewName1 = `ALERTS_floatingFilter ${new Date().toISOString()}`;
const columnToFilter1: AlertsColumnID = 'UnitAbbrev';
const columnToFilter2: AlertsColumnID = 'ModelName';
let searchTerm: string;
let filteredItems: string[];

describe('Alert Ag-Grid Floating Filter', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view');
    user.deleteAllSavedViews();
    user.restoreGridToDefaults();
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
  });
  it('Floating filter disabled by default', () => {
    agGridColumnFloatingFilter.getFloatingFilterRow().should('not.exist');
  });
  it('show floating filter pane', () => {
    user.clickFloatingFilterBtn();
    agGridColumnFloatingFilter.getFloatingFilterRow().should('be.visible');
  });
  it(`filter column ${columnToFilter1}`, () => {
    /*
    This step will filter a column and compare initial and current result
     */
    const initialGridItems =
      alertsPage.agGridMain.getItemsByColumnName(columnToFilter1);

    initialGridItems.then((gridItems) => {
      searchTerm = gridItems[_.random(0, gridItems.length - 1)];
      user.inputFilterValue(searchTerm, 'Unit');

      alertsPage.agGridMain
        .getItemsByColumnName(columnToFilter1)
        .then((results) => {
          results.forEach((e) => expect(e).eq(searchTerm)); // check each result
          expect(gridItems.filter((e) => e === searchTerm).length).eq(
            results.length
          ); // check length from previous vs search result
          filteredItems = results;
          agGridHeaderPanel
            .getTotalModelCount()
            .should('have.text', ` Total Models: ${results.length}`);
        });
    });
  });
  it('save view changes persists', () => {
    /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
    user.saveView(myViewName1);
    user.restoreGridToDefaults();
    user.selectSavedView(myViewName1);

    alertsPage.agGridMain
      .getItemsByColumnName(columnToFilter1)
      .then((gridItems) => {
        gridItems.forEach((e) => expect(e).eq(searchTerm)); // check each result
        agGridHeaderPanel
          .getTotalModelCount()
          .should('have.text', ` Total Models: ${gridItems.length}`);
        expect(gridItems).to.eqls(filteredItems);
      });
    user.showPanel('view'); // this closes the panel
  });
  it(`filter another column ${columnToFilter2}`, () => {
    /*
    This step will filter another column (2) and check the results
    */
    const initialGridItems =
      alertsPage.agGridMain.getItemsByColumnName(columnToFilter2);

    initialGridItems.then((gridItems) => {
      searchTerm = gridItems[_.random(0, gridItems.length - 1)];
      user.inputFilterValue(searchTerm, 'Model Name');

      alertsPage.agGridMain
        .getItemsByColumnName(columnToFilter2)
        .then((results) => {
          results.forEach((e) => expect(e).contains(searchTerm)); // check each result
          expect(
            gridItems.filter((e) => (e as string).includes(searchTerm)).length
          ).eq(results.length); // check length from previous vs search result

          agGridHeaderPanel
            .getTotalModelCount()
            .should('have.text', ` Total Models: ${results.length}`);
        });
    });
  });
  it(`filter column ${columnToFilter1} with invalid search term`, () => {
    user.inputFilterValue(`xxx`, 'Unit');
    agGridHeaderPanel
      .getTotalModelCount()
      .should('have.text', ` Total Models: 0`);
  });
  it('close floating filter', () => {
    user.clickFloatingFilterBtn().then(() => {
      expect(Cypress.$(`.ag-header-row-floating-filter`).length).eq(0);
    });
  });
});
