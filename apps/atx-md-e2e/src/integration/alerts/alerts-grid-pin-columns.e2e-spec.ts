import moment from 'moment';
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';

const util = new Utils();
const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const flyoutWindow = alertsPage.flyoutWindow;
const agGridSideButtons = alertsPage.sideButtons;
const agGridColumnHeader = alertsPage.agGridColumnHeader;
const columnsToHide = {
  toHide: ['Issues', 'Note Priority', 'Unit', '% OOB', '% EXP'],
};
const myViewName1 = `ALERTS_Pin1 ${new Date().toISOString()}`;
const myViewName2 = `ALERTS_Pin2 ${new Date().toISOString()}`;
const myViewName3 = `ALERTS_Pin3 ${new Date().toISOString()}`;
const columnToPin1: AlertsColumnID = 'UnitAbbrev';
const columnToPin2: AlertsColumnID = 'ModelName';

describe('Alert Saved View', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view');
    user.restoreGridToDefaults();
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
  });
  describe('Alerts Pin Columns - Left', () => {
    before(() => {
      // hideColumns(columnsToHide);
      // user.showPanel('view');
      user.deleteAllSavedViews(); // delete any existing saved views
    });
    it(`Pin ${columnToPin1} column to the left`, () => {
      user.pinColumn(columnToPin1, 'Left');
      user.moveMouseExpandScreeningView();
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it(`Pin ${columnToPin2} column`, () => {
      user.pinColumn(columnToPin2, 'Left');
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
      user.moveMouseExpandScreeningView();
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName1);
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.moveMouseExpandScreeningView(); // move mouose to reset view - cypress issue
      user.selectSavedView(myViewName1);
      user.moveMouseExpandScreeningView(); // move mouose to reset view - cypress issue

      cy.get(`.ag-pinned-left-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
  });
  describe('Alerts Pin Columns - Right', () => {
    before(() => {
      user.restoreGridToDefaults();
    });
    it(`Pin ${columnToPin1} column to the Right`, () => {
      user.pinColumn(columnToPin1, 'Right');
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it(`Pin ${columnToPin2} column`, () => {
      user.pinColumn(columnToPin2, 'Right');
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName2);
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.selectSavedView(myViewName2);
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
    });
  });
  describe('Alerts Pin Columns - Left and Right', () => {
    it(`Pin ${columnToPin1} column to the Left`, () => {
      user.pinColumn(columnToPin1, 'Left');
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      // expect(agGridColumnHeader.getPinColumnCount('left')).to.eq(1);
    });
    it('save view, verify changes persist', () => {
      user.saveView(myViewName3);
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
    });
    // it('refresh browser, changes persist', () => {
    // });
    it('restore default and select saved view', () => {
      user.restoreGridToDefaults();
      // user.showPanel('view');
      user.selectSavedView(myViewName3);
      cy.get(`.ag-pinned-left-header [col-id="${columnToPin1}"]`).should(
        'exist'
      );
      cy.get(`.ag-pinned-right-header [col-id="${columnToPin2}"]`).should(
        'exist'
      );
    });
  });
});
