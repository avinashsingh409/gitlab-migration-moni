/* eslint-disable cypress/no-unnecessary-waiting */
import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';

const util = new Utils();
const agGridMain = new Pages.AgGridMain();
const user = new AlertsUser();

const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

describe('ALERTS - AG Grid Multi Select', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    cy.wait(3000);
  });
  it(`Should multi select grid items`, () => {
    user.selectGridItemByRowIndex(0);

    user.multiSelectGridItemByRowIndex(1);
    agGridMain.getSelectedRows().should('have.length', 2);
  });
  it(`add another item in the grid`, () => {
    user.multiSelectGridItemByRowIndex(2);
    agGridMain.getSelectedRows().should('have.length', 3);
  });
  it(`deselect a selected item`, () => {
    user.multiSelectGridItemByRowIndex(1);
    agGridMain.getSelectedRows().should('have.length', 2);
  });
});
