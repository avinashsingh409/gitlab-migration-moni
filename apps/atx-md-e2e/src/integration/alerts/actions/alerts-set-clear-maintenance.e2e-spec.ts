import moment from 'moment';
import { AlertsUser } from '../../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../../support/helpers/user-data';
import * as Pages from '../../../support/page/pages';
import { Helper } from '../../../support/helpers/helper';
import { Utils } from '../../../support/helpers/utils';
import { AlertsColumnID } from '../../../support/page/common/ag-grid/grid-column-ids';

const helper = new Helper();
const util = new Utils();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const landingPage = new Pages.LandingPage();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const flyoutWindow = alertsPage.flyoutWindow;
// const defaultColumns = alertsPage.defaultColumns;
let noteValuesFromGrid: string;
let actionDateNowArr: string[];
let modelNameArr: string[];
const columnsToHide = {
  toHide: ['Issues', 'Note Priority', 'Unit', '% OOB', '% EXP'],
};
const notesToSave = `automated test Maintenance ${new Date().toLocaleString()}`;

describe('Alert Actions - Maintenance', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
  });
  describe('Set Maintenance', () => {
    it('open flyout window and Maintenance button detail should be visible', () => {
      user.expandView('ScreeningView');
      user.showHideAssetNavigator();
      hideColumns(columnsToHide); // this will hide some columns so that Actual, Expected, Upper, and Lower Limit will be visible

      const rowIdx = 0;
      modelNameArr = alertsPage.agGridMain.getModelName(rowIdx, {
        action: 'ActionMaintenance',
        actionStatus: false,
      }); // get model name based on parameters set
      // selects model in the grid
      cy.wrap(modelNameArr).then((modelName) => {
        user.selectModelByName(modelName[0]);
      });

      flyoutWindow.actionBtns.modelMaintenance().should('be.visible');
      flyoutWindow.actionBtns.clearModelMaintenance().should('not.exist');
    });
    it('Model Maintenance icon should be not active in ag grid', () => {
      cy.wrap(modelNameArr).then((modelName) =>
        alertsPage.checkModelStatus(modelName[0], 'ActionMaintenance', false)
      );
    });
    it('click MODEL MAINTENANCE button - notes text box should be visibe', () => {
      user.selectActionBtnFlyout('Model Maintenance');
      flyoutWindow.modelMaintenanceNoteFld().should('be.visible');
    });
    it('notes should display correct values (Actual, Expected, Lower, and Upper)', () => {
      const arr: any[] = [];
      cy.wrap(modelNameArr).then((modelName) => {
        alertsPage.agGridMain.getCellRowAliasByTextContent(
          modelName[0],
          'ModelName'
        ); // This returns the whole row element
        cy.get('@cellRow').within(() => {
          const columnIDs: AlertsColumnID[] = [
            'UpperLimit',
            'LowerLimit',
            'Actual',
            'Expected',
          ];
          columnIDs.forEach((col) => {
            cy.get(`[col-id="${col}"]`).then(($el) =>
              arr.push({ [col]: $el.text() })
            );
          });
        });

        cy.wrap(arr).then((notes) => {
          const tempNotes = `Actual = ${
            notes.filter((e) => e.Actual)[0].Actual
          }, \nExpected = ${
            notes.filter((e) => e.Expected)[0].Expected
          }, \nLower = ${
            notes.filter((e) => e.LowerLimit)[0].LowerLimit
          }, \nUpper = ${notes.filter((e) => e.UpperLimit)[0].UpperLimit}`;
          flyoutWindow
            .modelMaintenanceNoteFld()
            .then(($el) => {
              expect($el.val()).to.equal(tempNotes);
            })
            .type(`{enter}${notesToSave}`);
          noteValuesFromGrid = tempNotes;
        });
      });
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Maintenance Note Saved!');
      actionDateNowArr = getDateNow();
    });
    it('last note column should display latest notes', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        alertsPage.agGridMain
          .getCellByTextContent(modelName[0], 'ModelName', 'LastNote')
          .invoke('text')
          .should('equal', `${noteValuesFromGrid}\n${notesToSave}`);
      });
    });
    it('CLEAR MAINTENANCE button should be visible in flyout window', () => {
      flyoutWindow.actionBtns.modelMaintenance().should('not.exist');
      flyoutWindow.actionBtns.clearModelMaintenance().should('be.visible');
    });
    it('ag grid icon should update to MAINTENANCE = True', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        console.log(`modelName::: `, modelName);
        const actionGridCell =
          alertsPage.agGridMain.getCellActionIconByModelName(
            modelName[0],
            'ActionMaintenance'
          );
        actionGridCell.invoke('attr', 'class').should('contain', 'active-icon');
      });
    });
    it('history should display new Maintenance item', () => {
      user.expandView('ScreeningView');
      // user.expandView('ModelContentView');
      user.openContentTab('History');
      const tempHistoryObj =
        alertsPage.historyTab.agGrid.getLatestHistoryItem();

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal(
          'Model Maintenance Set'
        );
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          userObj.default.email
        );
        expect(obj.filter((e) => e.Note)[0].Note).equal(
          noteValuesFromGrid + `\n${notesToSave}`
        );
      });
    });
  });
  describe('Clear Maintenance', () => {
    it('click Clear Maintenance button - notes text box should be visibe', () => {
      user.selectActionBtnFlyout('Clear Maintenance');
      flyoutWindow.modelMaintenanceNoteFld().should('be.visible');
    });
    it('notes should display correct values (Actual, Expected, Lower, and Upper)', () => {
      const arr: any[] = [];
      cy.wrap(modelNameArr).then((modelName) => {
        alertsPage.agGridMain.getCellRowAliasByTextContent(
          modelName[0],
          'ModelName'
        ); // This returns the whole row element
        cy.get('@cellRow').within(() => {
          const columnIDs: AlertsColumnID[] = [
            'UpperLimit',
            'LowerLimit',
            'Actual',
            'Expected',
          ];
          columnIDs.forEach((col) => {
            cy.get(`[col-id="${col}"]`).then(($el) =>
              arr.push({ [col]: $el.text() })
            );
          });
        });

        cy.wrap(arr).then((notes) => {
          const tempNotes = `Actual = ${
            notes.filter((e) => e.Actual)[0].Actual
          }, \nExpected = ${
            notes.filter((e) => e.Expected)[0].Expected
          }, \nLower = ${
            notes.filter((e) => e.LowerLimit)[0].LowerLimit
          }, \nUpper = ${notes.filter((e) => e.UpperLimit)[0].UpperLimit}`;
          flyoutWindow
            .modelMaintenanceNoteFld()
            .then(($el) => {
              expect($el.val()).to.equal(tempNotes);
            })
            .type(`{enter}${notesToSave}`);
          noteValuesFromGrid = tempNotes;
        });
      });
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper
        .getSnackBarText()
        .should('have.text', 'Clear Maintenance Success!');
      actionDateNowArr = getDateNow();
    });
    it('last note column should display latest notes', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        alertsPage.agGridMain
          .getCellByTextContent(modelName[0], 'ModelName', 'LastNote')
          .invoke('text')
          .should('equal', `${noteValuesFromGrid}\n${notesToSave}`);
      });
    });
    it('Maintenance button should be visible in flyout window', () => {
      flyoutWindow.actionBtns.modelMaintenance().should('be.visible');
      flyoutWindow.actionBtns.clearModelMaintenance().should('not.exist');
    });
    it('ag grid icon should update to Maintenance = false', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        console.log(`modelName::: `, modelName);
        const actionGridCell =
          alertsPage.agGridMain.getCellActionIconByModelName(
            modelName[0],
            'ActionMaintenance'
          );
        actionGridCell
          .invoke('attr', 'class')
          .should('not.contain', 'active-icon');
      });
    });
    it('history should display new Clear Maintenance item', () => {
      // user.expandView('ScreeningView');
      // user.expandView('ModelContentView');
      user.openContentTab('History');
      const tempHistoryObj =
        alertsPage.historyTab.agGrid.getLatestHistoryItem();

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal(
          'Model Maintenance Cleared'
        );
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          userObj.default.email
        );
        expect(obj.filter((e) => e.Note)[0].Note).equal(
          noteValuesFromGrid + `\n${notesToSave}`
        );
      });
    });
  });
});

function hideColumns(columnsObj: { toShow?: string[]; toHide?: string[] }) {
  user.showPanel('column');
  user.setColumns(columnsObj);
  user.showPanel('column');
  // cy.wait(5000);
}
function getDateNow(): string[] {
  const dateNow: string[] = [];
  dateNow.push(moment().format('MM/DD/YYYY hh:mm A'));
  dateNow.push(moment().add('1', 'minute').format('MM/DD/YYYY hh:mm A'));
  return dateNow;
}
