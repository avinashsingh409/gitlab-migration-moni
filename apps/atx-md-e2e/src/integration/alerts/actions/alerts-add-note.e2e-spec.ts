import moment from 'moment';
import { AlertsUser } from '../../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  envToTest,
  assetToTest,
} from '../../../support/helpers/user-data';
import * as Pages from '../../../support/page/pages';
import { Helper } from '../../../support/helpers/helper';
import { AlertsColumnID } from '../../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../../support/helpers/utils';

const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const util = new Utils();
const flyoutWindow = alertsPage.flyoutWindow;
let noteValuesFromGrid: string;
let actionDateNowArr: string[];
let modelNameArr: string[];
const columnsToHide = {
  toHide: ['Issues', 'Note Priority', 'Unit', '% OOB', '% EXP'],
};
const notesToSave = `automated test ${new Date().toLocaleString()}`;
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

describe('Alert Actions - Note', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
  });
  describe('Add Note', () => {
    it('open flyout window and ADD NOTE button detail should be visible', () => {
      user.expandView('ScreeningView');
      user.showHideAssetNavigator();
      hideColumns(columnsToHide); // this will hide some columns so that Actual, Expected, Upper, and Lower Limit will be visible

      const rowIdx = 0;
      modelNameArr = alertsPage.agGridMain.getModelName(rowIdx); // get model name based on parameters set
      // selects model in the grid
      cy.wrap(modelNameArr).then((modelName) => {
        user.selectModelByName(modelName[0]);
      });

      flyoutWindow.actionBtns.addNote().should('be.visible');
    });
    it('click ADD NOTE button - notes text box should be visibe', () => {
      user.selectActionBtnFlyout('Add Note');
      flyoutWindow.addNoteFld().should('be.visible');
    });
    it('notes should display be blank', () => {
      flyoutWindow
        .addNoteFld()
        .then(($el) => {
          expect($el.val()).to.equal('');
        })
        .type(notesToSave);
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Note Saved!');
      actionDateNowArr = getDateNow();
    });
    it('last note column should display latest notes', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        alertsPage.agGridMain
          .getCellByTextContent(modelName[0], 'ModelName', 'LastNote')
          .should('have.text', notesToSave);
      });
    });
    it('ADD NOTE button should be visible in flyout window', () => {
      flyoutWindow.actionBtns.addNote().should('be.visible');
    });
    it('history should display new Note item', () => {
      user.expandView('ScreeningView');
      user.expandView('ModelContentView');
      user.openContentTab('History');
      const tempHistoryObj =
        alertsPage.historyTab.agGrid.getLatestHistoryItem();

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal('Note Added');
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          userObj.default.email
        );
        expect(obj.filter((e) => e.Note)[0].Note).equal(notesToSave);
      });
    });
  });
});

function hideColumns(columnsObj: { toShow?: string[]; toHide?: string[] }) {
  user.showPanel('column');
  user.setColumns(columnsObj);
  user.showPanel('column');
  // cy.wait(5000);
}
function getDateNow(): string[] {
  const dateNow: string[] = [];
  dateNow.push(moment().format('MM/DD/YYYY hh:mm A'));
  dateNow.push(moment().add('1', 'minute').format('MM/DD/YYYY hh:mm A'));
  return dateNow;
}
