import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import { Helper } from '../../support/helpers/helper';

const alertsPage = new Pages.Alerts();
const helper = new Helper();
const sidePanelColumns = alertsPage.sideButtons.column;
const columnHeaderPane = alertsPage.headerPanel;
const agGridMain = alertsPage.agGridMain;
const flyOutWindow = alertsPage.flyoutWindow;
const util = new Utils();
const user = new AlertsUser();

const clientToSelect = {
  parent: 'Asset Health Monitoring',
  // children: ['Asset 1', 'Asset 2', 'Asset 7']
  children: ['Live MD Test Group'],
  // children: []
};
const defaultColumns = alertsPage.getDefaultColumns();

describe('Alerts - Home', () => {
  before(() => {
    user.logIn(userObj.default);
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
  });
  describe('ag grid', () => {
    it('Ag-grid time stamp display current date/time', () => {
      columnHeaderPane.getTimeStamp().then(($el) => {
        expect($el.text().trim()).to.eq(util.getCurrentTime());
      });
    });
    it('default filters - Watch = false', () => {
      const isActive = false;
      const gridItems = agGridMain.getActionColumnItems(
        alertsPage.columnNames.watch,
        isActive
      );
      gridItems.its('length').should('not.eq', 0);

      agGridMain
        .getActionColumnItems(alertsPage.columnNames.watch, undefined, {
          defaultCommandTimeout: 2000,
        })
        .its('length')
        .should('be.gt', 0);
    });
    it('default filters - Alerts = true', () => {
      const isActive = true;
      const gridItems = agGridMain.getActionColumnItems(
        alertsPage.columnNames.alert,
        isActive
      );
      gridItems.invoke('attr', 'class').should('contain', 'active-icon');
    });
    it('default columns after selected / visible', () => {
      user.expandView('ScreeningView');
      user.showPanel('column');
      const tempArr: string[] = sidePanelColumns.getColumnNames(true);

      cy.wrap(
        helper.removeItemFromArrayByValue(defaultColumns, 'External ID')
      ).should(`include.members`, tempArr);
    });
  });
  describe('fly out window', () => {
    it('refresh button should be visible', () => {
      flyOutWindow.headerBtns.getRefreshBtn().should('be.visible');
    });
    it('close button should be visible', () => {
      flyOutWindow.headerBtns.getCloseBtn().should('be.visible');
    });
    it('should display No Model Selected', () => {
      flyOutWindow.getNoModelText().should('be.visible');
      flyOutWindow.getNoModelText().should('contain.text', 'No Model Selected');
    });
  });

  // it('hide columns', async () => {
  //   const columnsToHide = ['Issues', 'Note Priority', 'Unit'];
  //   const sampleObj = {
  //     toHide: columnsToHide
  //   };
  //   const beforeColumns = await columnHeaderPane.getColumnHeaderNames();
  //   user.setColumns(sampleObj);
  //   const afterColumns = await columnHeaderPane.getColumnHeaderNames();
  //   expect(beforeColumns).not.toEqual(afterColumns);
  //   columnsToHide.forEach(col => {
  //     expect(afterColumns.includes(col)).toBeFalsy();
  //   });
  // });
  // describe('restore to defaults', () => {
  //   it('restore columns to default', async () => {
  //     const beforeColumns = await columnHeaderPane.getColumnHeaderNames();
  //     browser.sleep(5000);
  //     user.showPanel('save');
  //     user.clickRestoreColumnDefaults();
  //     const afterColumns = await columnHeaderPane.getColumnHeaderNames();
  //     expect(beforeColumns).not.toEqual(afterColumns);
  //   });
  //   it('default filters - Watch = false', async () => {
  //     const gridItems = agGridMain.getActionColumnItems(alertsPage.columnNames.watch);
  //     gridItems.each(async item => {
  //       expect(alertsPage.isIconActive(item)).toBeFalsy(`Watch icon in grid row is active/true`);
  //     });
  //   });
  //   it('default filters - Alerts = true', () => {
  //     const gridItems = agGridMain.getActionColumnItems(alertsPage.columnNames.alert);
  //     gridItems.each(async item => {
  //       expect(alertsPage.isIconActive(item)).toBeTruthy(`Alert icon in grid row is false/not active`);
  //     });
  //   });
  //   it('default columns after selected / visible', async () => {
  //     user.showPanel('column');

  //     expect(alertsPage.sideButtons.column.getColumnNames()).toEqual(defaultColumns);
  //   });
  // });
  // });

  // it('Launch Pad', async () => {
  //   // browser.sleep(5000);
  //   // navigationPage.launchPadBtn.click();
  //   navigationPage.navBarBtn.click();
  //   // browser.sleep(5000);
  //   // // screening-container
  // });
});

// function getAllAttributes(arguments) {
//   const items = {};
//   for (let index = 0; index < arguments[0].attributes.length; ++index) {
//     items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value;
//   }
//   return items;
// }
