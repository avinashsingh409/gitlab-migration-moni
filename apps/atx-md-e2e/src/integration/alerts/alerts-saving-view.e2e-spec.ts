import moment from 'moment';
import { AlertsUser } from '../../support/helpers/user-alerts';
import { IssuesUser } from '../../support/helpers/user-issues';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';

const util = new Utils();
const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const issuesUser = new IssuesUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const flyoutWindow = alertsPage.flyoutWindow;
const agGridSideButtons = alertsPage.sideButtons;
const agGridColumnHeader = alertsPage.agGridColumnHeader;
const columnsToHide = {
  toHide: ['Issues', 'Note Priority', 'Unit', '% OOB', '% EXP'],
};
const myViewName = `ALERTS_automated ${new Date().toISOString()}`;
const myViewName2 = `v2` + myViewName;

describe('Alert Saved View', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
  });
  describe('CRUD view', () => {
    before(() => {
      hideColumns(columnsToHide);
      user.showPanel('view');
      user.deleteAllSavedViews(); // delete any existing saved views
    });
    it('create view, save, and refresh page', () => {
      user.saveView(myViewName);

      agGridSideButtons.view
        .getSelectedView()
        .should('contain.text', myViewName);

      cy.reload();
      user.showPanel('view');

      agGridSideButtons.view
        .getSelectedView()
        .should('contain.text', myViewName);
    });
    it('modify view', () => {
      user.showHideAssetNavigator();
      user.showHideFlyoutWindow();
      user.moveColumn('ModelName', 'ActionMaintenance');
      user.saveView(myViewName2);
    });
    it('delete a view', () => {
      agGridSideButtons.view.getSavedListItems().should('have.length', 2);
      user.deleteSavedView(myViewName2);
      agGridSideButtons.view.getSavedListItems().should('have.length', 1);
    });
    it('view in alerts should not appear in IM', () => {
      user.navigateToApp(AppNamesNewUI.issues, NavType.navBar);
      issuesUser.showPanel('view');
      agGridSideButtons.view
        .getSaveViewPanel()
        .should('not.contain.text', myViewName);
    });
  });
});

function hideColumns(columnsObj: { toShow?: string[]; toHide?: string[] }) {
  user.showPanel('column');
  user.setColumns(columnsObj);
  // user.showPanel('column');
  // cy.wait(5000);
}
function getDateNow(): string[] {
  const dateNow: string[] = [];
  dateNow.push(moment().format('MM/DD/YYYY hh:mm A'));
  dateNow.push(moment().add('1', 'minute').format('MM/DD/YYYY hh:mm A'));
  return dateNow;
}
