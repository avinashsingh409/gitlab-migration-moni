import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];

const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const myViewName1 = `ALERTS_Pin1 ${new Date().toISOString()}`;
const columnToSort1: AlertsColumnID = 'ModelName';
const columnToFilter1: AlertsColumnID = 'UnitAbbrev';
let searchTerm: string;
let filteredItemsNotSorted: string[];
let filteredSortedItems: string[];

describe('Alert - Sorting', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.restoreGridToDefaults();
    user.showPanel('view'); // close
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
  });
  describe(`Filter ${columnToFilter1} and Sort ${columnToSort1} + refresh page`, () => {
    it('add a filter, sort, and refresh', () => {
      user.clickFloatingFilterBtn();

      alertsPage.agGridMain
        .getItemsByColumnName(columnToFilter1)
        .then((gridItems) => {
          searchTerm = gridItems[_.random(0, gridItems.length - 1)];

          user.inputFilterValue(searchTerm, 'Unit'); // filter column

          alertsPage.agGridMain
            .getItemsByColumnName(columnToFilter1)
            .then((filteredItems) => {
              filteredItems.forEach((e) => expect(e).eq(searchTerm)); // check each result
              expect(gridItems.filter((e) => e === searchTerm).length).eq(
                filteredItems.length
              ); // check length from previous vs search result
              agGridHeaderPanel
                .getTotalModelCount()
                .should('have.text', ` Total Models: ${filteredItems.length}`);
            });
        });

      alertsPage.agGridMain // Get items after filter
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          filteredItemsNotSorted = gridItems;
        });

      user.sortColumn(columnToSort1);
      cy.reload();
      user.showHideFlyoutWindow();
      user.showHideAssetNavigator();
      user.expandView('ScreeningView');

      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          expect(gridItems).to.eqls(_.orderBy(filteredItemsNotSorted));
          filteredSortedItems = gridItems;
        });
    });
    it('save view, restore defaults, and select saved view - check changes persists', () => {
      /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
      user.showPanel('view');
      user.saveView(myViewName1);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName1);

      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          expect(gridItems).to.eqls(filteredSortedItems);
        });

      alertsPage.agGridMain
        .getItemsByColumnName(columnToFilter1)
        .then((gridItems) => {
          gridItems.forEach((e) => expect(e).eq(searchTerm));
          agGridHeaderPanel
            .getTotalModelCount()
            .should('have.text', ` Total Models: ${gridItems.length}`);
        });

      user.showPanel('view'); // this closes the panel
    });
  });
});
