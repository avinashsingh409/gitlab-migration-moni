import { AlertsUser } from '../../support/helpers/user-alerts';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const alertsPage = new Pages.Alerts();
const user = new AlertsUser();
const clientToSelect = assetToTest[Cypress.env().env]['alerts'];
const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const myViewName1 = `ALERTS_Pin1 ${new Date().toISOString()}`;
const myViewName2 = `ALERTS_Pin2 ${new Date().toISOString()}`;
const columnToSort1: AlertsColumnID = 'ModelName';
const columnToSort2: AlertsColumnID = 'PercentOutOfBounds';
let sortedItems: string[];

describe('Alert - Sorting', () => {
  before(() => {
    user.logIn(userObj.default);
    //util.dismissAtonixPopUp();
    user.navigateToApp(AppNamesNewUI.alerts, NavType.tile);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.deleteAllSavedViews();
    user.restoreGridToDefaults();
    user.showPanel('view'); // close
    user.expandView('ScreeningView');
    user.showHideAssetNavigator();
    user.showHideFlyoutWindow();
  });
  describe(`Sort ${columnToSort1}`, () => {
    it(`Click column ${columnToSort1} to sort by ASC`, () => {
      let initialGridItems: string[];
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          initialGridItems = gridItems;
        });

      user.sortColumn(columnToSort1);
      cy.reload();
      user.expandView('ScreeningView');
      user.showHideAssetNavigator();
      user.showHideFlyoutWindow();
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          expect(gridItems).to.eqls(_.orderBy(initialGridItems));
        });
    });
    it(`Click column ${columnToSort1} to sort by DESC`, () => {
      let initialGridItems: string[];
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          // console.log(`1 GridItems: `, gridItems);
          initialGridItems = gridItems;
        });

      user.sortColumn(columnToSort1);
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          // console.log(`2 GridItems: `, gridItems);
          sortedItems = gridItems;
          expect(gridItems).to.eqls(_.orderBy(initialGridItems).reverse());
        });
    });
    it('save view, restore defaults, and select saved view - check changes persists', () => {
      /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
      user.showPanel('view');
      user.saveView(myViewName1);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName1);

      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort1)
        .then((gridItems) => {
          expect(gridItems).to.eqls(sortedItems);
          agGridHeaderPanel
            .getTotalModelCount()
            .should('have.text', ` Total Models: ${gridItems.length}`);
        });
      user.showPanel('view'); // this closes the panel
    });
  });
  describe(`Sort ${columnToSort2}`, () => {
    it(`Click column ${columnToSort2} to sort by ASC`, () => {
      let initialGridItems: string[];
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort2)
        .then((gridItems) => {
          initialGridItems = gridItems;
        });

      user.sortColumn(columnToSort2);
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort2)
        .then((gridItems) => {
          expect(gridItems).to.eqls(_.orderBy(initialGridItems));
        });
    });
    it(`Click column ${columnToSort2} to sort by DESC`, () => {
      let initialGridItems: string[];
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort2)
        .then((gridItems) => {
          initialGridItems = gridItems;
        });

      user.sortColumn(columnToSort2);
      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort2)
        .then((gridItems) => {
          sortedItems = gridItems;
          expect(gridItems).to.eqls(_.orderBy(initialGridItems).reverse());
        });
    });
    it('save view, restore defaults, and select saved view - check changes persists', () => {
      /* 
    This steps will perform: 
    Saving the current view
    restoring defaults and selecting the saved view
    checking the grid if it loaded (items and length) the correct saved view
    */
      user.showPanel('view');
      user.saveView(myViewName2);
      user.restoreGridToDefaults();
      user.selectSavedView(myViewName2);

      alertsPage.agGridMain
        .getItemsByColumnName(columnToSort2)
        .then((gridItems) => {
          expect(gridItems).to.eqls(sortedItems);
          agGridHeaderPanel
            .getTotalModelCount()
            .should('have.text', ` Total Models: ${gridItems.length}`);
        });
      user.showPanel('view'); // this closes the panel
    });
  });
});
