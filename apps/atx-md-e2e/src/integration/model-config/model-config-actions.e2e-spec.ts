import moment from 'moment';
import { ModelConfigUser } from '../../support/helpers/user-model-config';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import {
  ModelConfigColumnID,
  ModelConfigTagColumnID,
} from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import { Helper } from '../../support/helpers/helper';
import _ from 'lodash';
import { testAutomationUser1 } from '../../support/helpers/user-accounts';

const util = new Utils();
const helper = new Helper();
const modelConfigPage = new Pages.ModelConfig();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];
let modelNameArr: string[];
let actionDateNowArr: string[];

describe('MODEL CONFIG - Actions', () => {
  before(() => {
    user.logIn(testAutomationUser1);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
  });
  describe(`Default model config actions pane`, () => {
    it(`No Model Selected message shown in actions pane`, () => {
      modelConfigPage
        .getNoModelMessage()
        .should('have.text', 'No Model Selected');
    });
  });
  describe(`Model Action buttons`, () => {
    it(`select a model`, () => {
      modelNameArr = modelConfigPage.agGridMain.getModelName(1, {
        action: 'ActionMaintenance',
        actionStatus: false,
      }); // get model name based on parameters set
      // selects model in the grid
      cy.wrap(modelNameArr).then((modelName) => {
        user.selectModelByName(modelName[0]);
      });
    });
    // need to find a way to check first if diagnose is set or not.
    // it(`diagnose button is visible`, () => {
    //   modelConfigPage.flyoutWindow.actionBtns.diagnose().should('be.visible');
    // });
    it(`model maintenance button is visible`, () => {
      modelConfigPage.flyoutWindow.actionBtns
        .modelMaintenance()
        .should('be.visible');
    });
    it(`add note button is visible`, () => {
      modelConfigPage.flyoutWindow.actionBtns.addNote().should('be.visible');
    });
  });
  describe(`Set Model Maintenance`, () => {
    it(`Click Model Maintenance button`, () => {
      user.selectActionBtnFlyout('Model Maintenance');
    });
    it('notes text box should be visibe', () => {
      modelConfigPage.flyoutWindow
        .modelMaintenanceNoteFld()
        .should('be.visible');
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Maintenance Note Saved!');
      actionDateNowArr = getDateNow();
    });
    it('Clear Maintenance button should be visible in flyout window', () => {
      // modelConfigPage.flyoutWindow.actionBtns
      //   .modelMaintenance()
      // .should('be.visible');
      modelConfigPage.flyoutWindow.actionBtns
        .clearModelMaintenance()
        .should('be.visible');
    });
    it('ag grid icon should update to Maintenance = false', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        const actionGridCell =
          modelConfigPage.agGridMain.getCellActionIconByModelName(
            modelName[0],
            'ActionMaintenance',
            'modelConfig'
          );
        actionGridCell.invoke('attr', 'class').should('contain', 'active-icon');
      });
    });
  });
  describe(`Clear Model Maintenance`, () => {
    it(`Click Clear Model Maintenance button`, () => {
      user.selectActionBtnFlyout('Clear Maintenance');
    });
    it('notes text box should be visibe', () => {
      modelConfigPage.flyoutWindow
        .modelMaintenanceNoteFld()
        .should('be.visible');
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Maintenance Note Saved!');
      actionDateNowArr = getDateNow();
    });
    it('Maintenance button should be visible in flyout window', () => {
      modelConfigPage.flyoutWindow.actionBtns
        .modelMaintenance()
        .should('be.visible');
    });
    it('ag grid icon should update to Maintenance = false', () => {
      cy.wrap(modelNameArr).then((modelName) => {
        const actionGridCell =
          modelConfigPage.agGridMain.getCellActionIconByModelName(
            modelName[0],
            'ActionMaintenance',
            'modelConfig'
          );
        actionGridCell
          .invoke('attr', 'class')
          .should('not.contain', 'active-icon');
      });
    });
    it(`history should be updated`, () => {
      modelConfigPage.openTrendTab('History');
      cy.wait('@HistoryGrid');
      const tempHistoryObj =
        modelConfigPage.historyGrid.getLatestHistoryItem('Model Config');

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal(
          'Model Maintenance Cleared'
        );
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          testAutomationUser1.email
        );
      });
    });
  });
});

function getDateNow(): string[] {
  const dateNow: string[] = [];
  dateNow.push(moment().format('MM/DD/YYYY hh:mm A'));
  dateNow.push(moment().add('1', 'minute').format('MM/DD/YYYY hh:mm A'));
  return dateNow;
}
