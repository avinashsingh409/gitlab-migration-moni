import '@percy/cypress';

import { ModelConfigUser } from '../../support/helpers/user-model-config';
import {
  analyst,
  testAutomationUser1,
} from '../../support/helpers/user-accounts';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import {
  AppNamesNewUI,
  envToTest,
  NavType,
} from '../../support/helpers/user-data';

const user = new ModelConfigUser();
const modelConfigEdit = new Pages.ModelEdit();
const util = new Utils();
const agGrid = new Pages.AgGridMain();

let MAE,
  upperMultiplier,
  upperBias,
  upperBoundaryDistance,
  lowerMultiplier,
  lowerBias,
  lowerBoundaryDistance,
  predictiveMethodSelected;
const newUpperMultipler = 2.34,
  newUpperBias = 2,
  newLowerMultiplier = 1.23,
  newLowerBias = -11;
let configApiResponse = [];

const url = (envToTest as string).toLocaleLowerCase().includes('test')
  ? 'https://test.atonix.com/MD/model-config/mm?modelIds=1f1897dc-daa4-483d-82f8-df2ee4ddb3fe,63d345d7-c666-4c41-b834-7c67c2807889&aid=ec2129f6-355b-418e-82b8-231ac95483f3&start=1671971768583&end=1674650168583'
  : 'https://stage.atonix.com/MD/model-config/m?modelId=1def8081-e5bb-4a63-b050-b5e8f6dde0c4&isStandard=false&aid=ebb3eb71-a9dc-41b4-b06f-adbec69b1a87&start=1669119912804&end=1671711912804';
// const url =
//   'http://localhost:4200/model-config/mm?modelIds=0da53625-43d2-49b8-a71f-1e9d676e3763,CF79AFF6-FBEE-44AD-98D5-44A84F4C8D9E&aid=6f54edee-77a6-4c16-88ed-c9315bc29ce4&start=1671977682027&end=1674656082027';
describe(`APR multi model`, () => {
  before(() => {
    cy.visit(url);
    cy.get('.email-field').should('be.visible');
    user.logIn(testAutomationUser1, false);
  });
  describe('Model Config - Relative Model Bounds', () => {
    it(`Wait for /config response`, () => {
      util.modelWait();

      cy.wait([`@config`, `@ModelTrend`], {
        timeout: 30000,
      }).then((req) => {
        configApiResponse = req[0].response.body;
      });
    });
    // it(`TEST TEST TEST TEST`, () => {//});
    it('Relative model bounds should be correct', () => {
      modelConfigEdit.openConfigurationTab('Anomalies');
      modelConfigEdit.selectAnomaliesFilter('Relative Model Bounds');
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias = relativeBounds.properties.upperBias;
          upperBoundaryDistance = (MAE * upperMultiplier + upperBias).toFixed(
            2
          );
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(
            2
          );

          agGrid
            .getCellItemByColumnName('RelativeModelBoundsUpperBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsLowerBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
          // });
        });
      });
    });
    it(`Change Upper Multiplier`, () => {
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias = relativeBounds.properties.upperBias;
          upperBoundaryDistance = (MAE * newUpperMultipler + upperBias).toFixed(
            2
          );
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(
            2
          );

          user
            .clickCellByColumnName('RelativeModelBoundsUpperMultiplier', index)
            .type(`${newUpperMultipler}{enter}`);

          agGrid
            .getCellItemByColumnName('RelativeModelBoundsUpperBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsLowerBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
    it(`Change Upper Bias`, () => {
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias = relativeBounds.properties.upperBias;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(
            2
          );

          user
            .clickCellByColumnName('RelativeModelBoundsUpperBias', index)
            .type(`{enter}${newUpperBias}{enter}`);

          agGrid
            .getCellItemByColumnName('RelativeModelBoundsUpperBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsLowerBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
    it(`Change Lower Multipler`, () => {
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias = relativeBounds.properties.upperBias;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (
            MAE * newLowerMultiplier -
            lowerBias
          ).toFixed(2);

          user
            .clickCellByColumnName('RelativeModelBoundsLowerMultiplier', index)
            .type(`{enter}${newLowerMultiplier}{enter}`);

          agGrid
            .getCellItemByColumnName('RelativeModelBoundsUpperBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsLowerBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
    it(`Change Lower Bias`, () => {
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias = relativeBounds.properties.upperBias;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (
            MAE * newLowerMultiplier -
            newLowerBias
          ).toFixed(2);

          user
            .clickCellByColumnName('RelativeModelBoundsLowerBias', index)
            .type(`${newLowerBias}{enter}`);
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsUpperBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
          agGrid
            .getCellItemByColumnName('RelativeModelBoundsLowerBound', index)
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
  });
});
