/* eslint-disable cypress/no-unnecessary-waiting */
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
  envToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';
import { ModelConfigUser } from '../../support/helpers/user-model-config';
import { OpModesTypes } from '../../support/definitions';
import { Interception } from 'cypress/types/net-stubbing';
import createOpModePayload from '../../fixtures/op-mode/create-op-mode-1.json';
import { CyRequests } from '../../support/helpers/requests';

const util = new Utils();
const agGridMain = new Pages.AgGridMain();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];
const modelConfigPage = new Pages.ModelConfig();
const opModesPage = new Pages.ModelConfigOpModes();
let tagGridResponse: Interception;
let opModeResponse;
const cyRequests = new CyRequests();
const opModeTitle = `OpMode Test Auto ` + new Date().toISOString();
const modelName = 'Test Auto Model ' + new Date().toISOString();
let tagName: string, tagDesc: string, tagVariableName: string, tagToUse2: any;

describe('MODEL CONFIG - Op Modes tab', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    // cy.wait(3000);
    cy.saveLocalStorage();
    util.loadAgGridForWait('TagGrid');
    user.openTab('OpModes');

    cy.wait('@TagGrid').then((res) => (tagGridResponse = res));
  });
  // skipping this for now until I can come up
  // with a solution for drag and drop
  // it(`Create new Op Mode`, () => {
  //   const opModeTitle = `Op Mode Test ${new Date().toISOString()}`;
  //   const modeType: OpModesTypes = 'Exclusion Period';
  //   user.createNewOpMode(opModeTitle, modeType);
  //   opModesPage.opModeName().should('have.value', opModeTitle);
  // });
  // it(`Move Tag to Associated`, () => {
  //   user.moveTagToAssociatedByIdx(0);
  //   cy.wrap(tagRes).then((tagGridRes) => {
  //     console.log(`XXX: `, tagGridRes);
  //   });
  // });
  it(`Create Op mode through API`, () => {
    cy.wrap(tagGridResponse).then((tagGridRes) => {
      const tagToUse1 = tagGridRes.response.body.Results[0].TagMaps[0];
      tagToUse2 = tagGridRes.response.body.Results[0].TagMaps[0];
      tagName = tagToUse1.Tag.TagName;
      tagDesc = tagToUse1.Tag.TagDesc;
      tagVariableName = tagToUse1.VariableType.VariableName;

      createOpModePayload.assetTagMaps[0].assetVariableTypeTagMapID =
        tagToUse1.AssetVariableTypeTagMapID;
      createOpModePayload.assetTagMaps[0].opModeDefinitionAssetTagMapID = -1;
      createOpModePayload.assetTagMaps[0].include = true;
      createOpModePayload.opModeDefinitionTitle = opModeTitle;
      createOpModePayload.assetTagMaps[0].tagDesc = tagToUse1.Tag.TagDesc;
      createOpModePayload.assetTagMaps[0].tagName = tagToUse1.Tag.TagName;
      createOpModePayload.assetTagMaps[0].bringDescendants = false;

      const tag2 = {
        assetVariableTypeTagMapID: tagToUse2.AssetVariableTypeTagMapID,
        opModeDefinitionAssetTagMapID: -1,
        include: true,
        tagDesc: tagToUse2.Tag.TagDesc,
        tagName: tagToUse2.Tag.TagName,
        bringDescendants: false,
      };
      createOpModePayload.assetTagMaps.push(tag2);

      createOpModePayload.startCriteria[0].logic[0].assetVariableTypeTagMapID =
        tagToUse1.AssetVariableTypeTagMapID;
      createOpModePayload.startCriteria[0].logic[0].tagDesc =
        tagToUse1.Tag.TagDesc;
      createOpModePayload.startCriteria[0].logic[0].tagName =
        tagToUse1.Tag.TagName;
      createOpModePayload.startCriteria[0].logic[0].tagID = tagToUse1.TagID;

      const options = {
        method: 'POST',
        headers: { authorization: tagGridRes.request.headers.authorization },
        body: createOpModePayload,
      };
      cyRequests
        .opMode(options)
        .then(
          (res) =>
            (opModeResponse = res.allRequestResponses[0]['Response Body'].data)
        );
    });
  });
  it(`verify created op mode`, () => {
    user.openTab('TagList');
    user.openTab('OpModes');
    user.inputFilterValue(opModeTitle, 'Operating Mode Name');
    user.selectOpModeByIndex(0).then((req) => {
      const startCriteriaRes = req.response.body.startCriteria;
      const endCriteriaRes = req.response.body.endCriteria;

      cy.wrap(opModesPage.getCriteriaDetails()).then((criteria) => {
        startCriteriaRes.forEach((startCriteriaItemRes) => {
          const criteriaItems = criteria.find(
            (c) =>
              startCriteriaItemRes.criteriaGroupID.toString() ===
              c.criteriaGroupId
          );

          criteriaItems.logic.forEach((criteriaItem, criteriaItemIdx) => {
            const tempLogic = startCriteriaItemRes.logic[criteriaItemIdx];
            const tempTagDesc = `${tempLogic.tagDesc} (${tempLogic.tagName})`;
            expect(criteriaItem.tagDesc).eq(tempTagDesc);
            expect(criteriaItem.sign).eq(tempLogic.logicOperatorTypeAbbrev);
            expect(Number(criteriaItem.value)).eq(tempLogic.value);
            expect(criteriaItem.unit).eq(tempLogic.engUnits);
          });
        });
      });
    });

    opModesPage
      .associatedAssetAndTags()
      .find('mat-chip')
      .should('have.length', 2);
  });
  it(`remove Associated Assetes and Tags`, () => {
    user.removeAssociatedAssetsAndTags(tagToUse2.Tag.TagName);
    opModesPage
      .associatedAssetAndTags()
      .find('mat-chip')
      .should('have.length', 1);
  });
});
