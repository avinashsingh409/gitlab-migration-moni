/* eslint-disable cypress/no-unnecessary-waiting */
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';
import { ModelConfigUser } from '../../support/helpers/user-model-config';

const util = new Utils();
// const helper = new Helper();
const agGridMain = new Pages.AgGridMain();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];

describe('MODEL CONFIG - Op Modes tab', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    cy.wait(3000);
  });
  it(`open Op Modes tab - no selected op mode`, () => {
    user.openTab('OpModes');
    cy.get(`.no-selected-op-mode`).should(
      'have.text',
      'Create or Select an Op Mode to define its Criteria'
    );
  });
});
