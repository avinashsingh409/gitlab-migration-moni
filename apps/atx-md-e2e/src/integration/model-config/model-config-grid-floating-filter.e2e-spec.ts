import { ModelConfigUser } from '../../support/helpers/user-model-config';
import {
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import {
  ModelConfigColumnID,
  ModelConfigTagColumnID,
} from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { testAutomationUser1 } from '../../support/helpers/user-accounts';

const util = new Utils();
const modelConfigPage = new Pages.ModelConfig();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];

const agGridColumnFloatingFilter = modelConfigPage.agGridFloatingFilter;
const columnToFilter1: ModelConfigColumnID = 'PrioritizedOpMode';
const columnToFilter2: ModelConfigColumnID = 'ModelName';
let searchTerm: string;
let filteredItems: string[];

describe('MODEL CONFIG Ag-Grid Floating Filter', () => {
  before(() => {
    user.logIn(testAutomationUser1);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    user.showHideAssetNavigator();
    user.showPanel('view');
    user.restoreGridToDefaults();
    user.showPanel('view');
  });
  describe(`Model List tab`, () => {
    it('Floating filter disabled by default', () => {
      agGridColumnFloatingFilter.getFloatingFilterRow().should('exist');
    });
    it(`filter column ${columnToFilter1}`, () => {
      /*
    This step will filter a column and compare initial and current result
     */
      const initialGridItems =
        modelConfigPage.agGridMain.getItemsByColumnName(columnToFilter1);

      initialGridItems.then((gridItems) => {
        searchTerm = gridItems[_.random(0, gridItems.length - 1)];
        user.selectFilterValue(searchTerm, 'Operating Mode');

        modelConfigPage.agGridMain
          .getItemsByColumnName(columnToFilter1)
          .then((results) => {
            filteredItems = results;
            results.forEach((e) => expect(e).contains(searchTerm)); // check each result
          });
      });
    });
    it(`filter another column ${columnToFilter2}`, () => {
      /*
    This step will filter another column (2) and check the results
    */
      const initialGridItems =
        modelConfigPage.agGridMain.getItemsByColumnName(columnToFilter2);

      initialGridItems.then((gridItems) => {
        searchTerm = gridItems[_.random(0, gridItems.length - 1)];
        user.inputFilterValue(searchTerm, 'Model Name');

        modelConfigPage.agGridMain
          .getItemsByColumnName(columnToFilter2)
          .then((results) => {
            results.forEach((e) => expect(e).contains(searchTerm)); // check each result
            expect(
              gridItems.filter((e) => (e as string).includes(searchTerm)).length
            ).eq(results.length); // check length from previous vs search result
          });
      });
    });
  });
  describe(`Tag List tab`, () => {
    const tagListColumnToFilter1: ModelConfigTagColumnID = 'UnitAbbrev';
    const tagListColumnToFilter2: ModelConfigTagColumnID = 'VariableDesc';
    before(() => {
      util.loadAgGridForWait('TagListGrid');
      user.openTab('TagList');
      cy.wait('@TagListGrid');
    });
    it('Floating filter disabled by default', () => {
      agGridColumnFloatingFilter.getFloatingFilterRow().should('exist');
    });
    it(`filter column ${tagListColumnToFilter1}`, () => {
      /*
      This step will filter a column and compare initial and current result
       */
      const initialGridItems = modelConfigPage.agGridMain.getItemsByColumnName(
        tagListColumnToFilter1
      );

      initialGridItems.then((gridItems) => {
        searchTerm = gridItems[_.random(0, gridItems.length - 1)];
        user.inputFilterValue(searchTerm, 'Unit');

        modelConfigPage.agGridMain
          .getItemsByColumnName(tagListColumnToFilter1)
          .then((results) => {
            filteredItems = results;
            results.forEach((e) => expect(e).eq(searchTerm)); // check each result
          });
      });
    });
    it(`filter another column ${tagListColumnToFilter2}`, () => {
      /*
      This step will filter another column (2) and check the results
      */
      const initialGridItems = modelConfigPage.agGridMain.getItemsByColumnName(
        tagListColumnToFilter2
      );

      initialGridItems.then((gridItems) => {
        searchTerm = gridItems[_.random(0, gridItems.length - 1)];
        user.inputFilterValue(searchTerm, 'Variable');

        modelConfigPage.agGridMain
          .getItemsByColumnName(tagListColumnToFilter2)
          .then((results) => {
            results.forEach((e) => expect(e).contains(searchTerm)); // check each result
            // commenting for now. Since some values in grid are not shown until visible in grid
            // need to find a way to check total, maybe thru api
            // expect(
            //   gridItems.filter((e) => (e as string).includes(searchTerm)).length
            // ).eq(results.length); // check length from previous vs search result
          });
      });
    });
  });
});
