import '@percy/cypress';

import { ModelConfigUser } from '../../support/helpers/user-model-config';
import {
  analyst,
  testAutomationUser1,
} from '../../support/helpers/user-accounts';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import {
  AppNamesNewUI,
  envToTest,
  NavType,
} from '../../support/helpers/user-data';

const user = new ModelConfigUser();
const modelConfigEdit = new Pages.ModelEdit();
const util = new Utils();
const agGrid = new Pages.AgGridMain();

let MAE,
  upperMultiplier,
  upperBias,
  upperBoundaryDistance,
  lowerMultiplier,
  lowerBias,
  lowerBoundaryDistance,
  predictiveMethodSelected;
const newUpperMultipler = 1,
  newUpperBias = 2,
  newLowerMultiplier = 3,
  newLowerBias = -2;
let configApiResponse = [];

const url = (envToTest as string).toLocaleLowerCase().includes('test')
  ? 'https://test.atonix.com/MD/model-config/m?modelId=1f1897dc-daa4-483d-82f8-df2ee4ddb3fe&isStandard=false&aid=ec2129f6-355b-418e-82b8-231ac95483f3&start=1669119897183&end=1671711897183'
  : 'https://stage.atonix.com/MD/model-config/m?modelId=1def8081-e5bb-4a63-b050-b5e8f6dde0c4&isStandard=false&aid=ebb3eb71-a9dc-41b4-b06f-adbec69b1a87&start=1669119912804&end=1671711912804';
// const url =
//   'http://localhost:4200/model-config/m?modelId=0da53625-43d2-49b8-a71f-1e9d676e3763&isStandard=false&aid=6f54edee-77a6-4c16-88ed-c9315bc29ce4&start=1671977682027&end=1674656082027';
describe('Model Config - Relative Model Bounds', () => {
  before(() => {
    cy.visit(url);
    cy.get('.email-field').should('be.visible');
    user.logIn(testAutomationUser1, false);
  });
  it('Relative model bounds should be correct', () => {
    cy.visit(url);
    cy.get('.email-field').should('be.visible');
    user.logIn(testAutomationUser1, false);
    util.modelWait();

    cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
      timeout: 30000,
    }).then((req) => {
      configApiResponse = req[1].response.body;
      predictiveMethodSelected =
        configApiResponse[0].properties.predictiveTypeSelected;

      MAE = configApiResponse[0].properties.predictiveTypes.find(
        (a) => a.type === predictiveMethodSelected
      ).properties.meanAbsoluteError;
      const relativeBounds = configApiResponse[0].anomalies.find(
        (p) => p.type === 'relative_bounds'
      );
      upperMultiplier = relativeBounds.properties.upperMultiplier;
      upperBias = relativeBounds.properties.upperBias;
      upperBoundaryDistance = (MAE * upperMultiplier + upperBias).toFixed(2);
      lowerMultiplier = relativeBounds.properties.lowerMultiplier;
      lowerBias = relativeBounds.properties.lowerBias;
      lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(2);
      modelConfigEdit.showHideActionsPane();
      modelConfigEdit.openConfigurationTab('Anomalies');
      modelConfigEdit.selectAnomaliesFilter('Relative Model Bounds');
      agGrid
        .getCellItemByColumnName('RelativeModelBoundsUpperBound')
        .invoke('text')
        .then((content) => {
          expect(Number(content).toFixed(2).toString()).eq(
            upperBoundaryDistance
          );
        });
      agGrid
        .getCellItemByColumnName('RelativeModelBoundsLowerBound')
        .invoke('text')
        .then((content) => {
          expect(Number(content).toFixed(2).toString()).eq(
            lowerBoundaryDistance
          );
        });
    });
  });
  it(`Change Upper Multiplier`, () => {
    upperBoundaryDistance = (MAE * newUpperMultipler + upperBias).toFixed(2);
    lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(2);

    user
      .clickCellByColumnName('RelativeModelBoundsUpperMultiplier')
      .type(`${newUpperMultipler}{enter}`);

    agGrid
      .getCellItemByColumnName('RelativeModelBoundsUpperBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(upperBoundaryDistance);
      });
    agGrid
      .getCellItemByColumnName('RelativeModelBoundsLowerBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(lowerBoundaryDistance);
      });
  });
  it(`Change Upper Bias`, () => {
    upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(2);
    lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(2);

    user
      .clickCellByColumnName('RelativeModelBoundsUpperBias')
      .type(`{enter}${newUpperBias}{enter}`);

    agGrid
      .getCellItemByColumnName('RelativeModelBoundsUpperBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(upperBoundaryDistance);
      });
    agGrid
      .getCellItemByColumnName('RelativeModelBoundsLowerBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(lowerBoundaryDistance);
      });
  });
  it(`Change Lower Multipler`, () => {
    upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(2);
    lowerBoundaryDistance = (MAE * newLowerMultiplier - lowerBias).toFixed(2);

    user
      .clickCellByColumnName('RelativeModelBoundsLowerMultiplier')
      .type(`{enter}${newLowerMultiplier}{enter}`);

    agGrid
      .getCellItemByColumnName('RelativeModelBoundsUpperBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(upperBoundaryDistance);
      });
    agGrid
      .getCellItemByColumnName('RelativeModelBoundsLowerBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(lowerBoundaryDistance);
      });
  });
  it(`Change Lower Bias`, () => {
    upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(2);
    lowerBoundaryDistance = (MAE * newLowerMultiplier - newLowerBias).toFixed(
      2
    );

    user
      .clickCellByColumnName('RelativeModelBoundsLowerBias')
      .type(`${newLowerBias}{enter}`);
    agGrid
      .getCellItemByColumnName('RelativeModelBoundsUpperBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(upperBoundaryDistance);
      });
    agGrid
      .getCellItemByColumnName('RelativeModelBoundsLowerBound')
      .invoke('text')
      .then((content) => {
        expect(Number(content).toFixed(2).toString()).eq(lowerBoundaryDistance);
      });
  });
});
