import '@percy/cypress';

import { ModelConfigUser } from '../../support/helpers/user-model-config';
import { testAutomationUser1 } from '../../support/helpers/user-accounts';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import { envToTest } from '../../support/helpers/user-data';
import moment, { duration } from 'moment';

const user = new ModelConfigUser();
const modelConfigEdit = new Pages.ModelEdit();
const util = new Utils();
const agGrid = new Pages.AgGridMain();

let MAE,
  upperMultiplier,
  upperBias,
  upperBoundaryDistance,
  lowerMultiplier,
  lowerBias,
  lowerBoundaryDistance,
  predictiveMethodSelected;
const newUpperMultipler = 2.34,
  newUpperBias = 3,
  newLowerMultiplier = 1.23,
  newLowerBias = -11;
const trainingDuration = 5,
  trainingDurationTemporalType = 'Days',
  trainingLag = 3,
  trainingLagTemporalType = 'Days',
  trainingSampleRate = 1100,
  trainingSampleRateTemporalType = 'Days',
  trainingMinDataPoints = 1111;

let configApiResponse = [];

const url = (envToTest as string).toLocaleLowerCase().includes('test')
  ? 'https://test.atonix.com/MD/model-config/mm?modelIds=1f1897dc-daa4-483d-82f8-df2ee4ddb3fe,63d345d7-c666-4c41-b834-7c67c2807889,4527b99b-b803-4cb2-a196-8d2c0c8e3c6b&aid=ec2129f6-355b-418e-82b8-231ac95483f3&start=1672747391085&end=1675425791085'
  : 'https://stage.atonix.com/MD/model-config/mm?modelIds=41bd4ce9-e72b-40a4-9e04-ea33fdb800c8,1def8081-e5bb-4a63-b050-b5e8f6dde0c4,29f7bdab-66aa-478f-a6eb-249dbfd6c97e&aid=ebb3eb71-a9dc-41b4-b06f-adbec69b1a87&start=1680526687560&end=1683118687560';
// const url =
//   'http://localhost:4200/model-config/mm?modelIds=35F15F4F-AE4D-4EA8-88DB-9B7AE50978A6,F638DB2D-A142-4854-A104-036E9B7D00FF,1FFE4728-4D37-4E78-9BAB-127A0C2D3CC8&aid=6f54edee-77a6-4c16-88ed-c9315bc29ce4&start=1673263963352&end=1675942363352';
describe(`APR - Apply Value to Column`, () => {
  before(() => {
    cy.visit(url);
    cy.get('.email-field').should('be.visible');
    user.logIn(testAutomationUser1, false);
  });
  describe('Anomalies - Relative Model Bounds', () => {
    it(`Wait for /config response`, () => {
      util.modelWait();

      cy.wait([`@config`, `@ModelTrend`], {
        timeout: 30000,
      }).then((req) => {
        configApiResponse = req[0].response.body;
        console.log(`configApiResponse: `, configApiResponse);
        expect(configApiResponse.length, `config reponse length`).to.eq(3);
      });
    });
    it(`navigate to Anomalies tab`, () => {
      modelConfigEdit.openConfigurationTab('Anomalies');
      modelConfigEdit.selectAnomaliesFilter('Relative Model Bounds');
    });
    it(`Apply value to column - Upper Multipler`, () => {
      user
        .clickCellByColumnName('RelativeModelBoundsUpperMultiplier')
        .type(`${newUpperMultipler}{enter}`);

      user.applyValueToColumn(`RelativeModelBoundsUpperMultiplier`);
      agGrid.getGridRows().should('have.length', 3);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('RelativeModelBoundsUpperMultiplier', index)
          .invoke('text')
          .then((content) => {
            expect(Number(content)).eq(newUpperMultipler);
          });
      });

      // check Distance
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          // upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias =
            relativeBounds.properties.upperBias !== null
              ? relativeBounds.properties.upperBias
              : 0;
          upperBoundaryDistance = (MAE * newUpperMultipler + upperBias).toFixed(
            2
          );
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(
            2
          );

          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsUpperMultiplier'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newUpperMultipler);
            });
          agGrid
            .getCellItemByModelName(config.name, 'RelativeModelBoundsUpperBias')
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(upperBias);
            });
          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsUpperBound'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
        });
      });
    });
    it(`Apply value to column - Upper Bias`, () => {
      user
        .clickCellByColumnName('RelativeModelBoundsUpperBias')
        .type(`${newUpperBias}{enter}`);

      user.applyValueToColumn(`RelativeModelBoundsUpperBias`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('RelativeModelBoundsUpperBias', index)
          .invoke('text')
          .then((content) => {
            expect(Number(content)).eq(newUpperBias);
          });
      });

      // check Distance
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          // upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias =
            relativeBounds.properties.upperBias !== null
              ? relativeBounds.properties.upperBias
              : 0;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias = relativeBounds.properties.lowerBias;
          lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(
            2
          );

          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsUpperMultiplier'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newUpperMultipler);
            });
          agGrid
            .getCellItemByModelName(config.name, 'RelativeModelBoundsUpperBias')
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newUpperBias);
            });
          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsUpperBound'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                upperBoundaryDistance
              );
            });
        });
      });
    });
    it(`Apply value to column - Lower Multiplier`, () => {
      user
        .clickCellByColumnName('RelativeModelBoundsLowerMultiplier')
        .type(`${newLowerMultiplier}{enter}`);

      user.applyValueToColumn(`RelativeModelBoundsLowerMultiplier`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('RelativeModelBoundsLowerMultiplier', index)
          .invoke('text')
          .then((content) => {
            expect(Number(content)).eq(newLowerMultiplier);
          });
      });

      // check Distance
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          // upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias =
            relativeBounds.properties.upperBias !== null
              ? relativeBounds.properties.upperBias
              : 0;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias =
            relativeBounds.properties.lowerBias !== null
              ? relativeBounds.properties.lowerBias
              : 0;
          lowerBoundaryDistance = (
            MAE * newLowerMultiplier -
            lowerBias
          ).toFixed(2);

          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsLowerMultiplier'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newLowerMultiplier);
            });
          agGrid
            .getCellItemByModelName(config.name, 'RelativeModelBoundsLowerBias')
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(lowerBias);
            });
          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsLowerBound'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
    it(`Apply value to column - Lower Bias`, () => {
      user
        .clickCellByColumnName('RelativeModelBoundsLowerBias')
        .type(`${newLowerBias}{enter}`);

      user.applyValueToColumn(`RelativeModelBoundsLowerBias`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('RelativeModelBoundsLowerBias', index)
          .invoke('text')
          .then((content) => {
            expect(Number(content)).eq(newLowerBias);
          });
      });

      // check Distance
      configApiResponse.forEach((config, index) => {
        cy.wrap(config).then(() => {
          predictiveMethodSelected = config.properties.predictiveTypeSelected;
          MAE = config.properties.predictiveTypes.find(
            (a) => a.type === predictiveMethodSelected
          ).properties.meanAbsoluteError;
          const relativeBounds = config.anomalies.find(
            (p) => p.type === 'relative_bounds'
          );
          // upperMultiplier = relativeBounds.properties.upperMultiplier;
          upperBias =
            relativeBounds.properties.upperBias !== null
              ? relativeBounds.properties.upperBias
              : 0;
          upperBoundaryDistance = (
            MAE * newUpperMultipler +
            newUpperBias
          ).toFixed(2);
          lowerMultiplier = relativeBounds.properties.lowerMultiplier;
          lowerBias =
            relativeBounds.properties.lowerBias !== null
              ? relativeBounds.properties.lowerBias
              : 0;
          lowerBoundaryDistance = (
            MAE * newLowerMultiplier -
            newLowerBias
          ).toFixed(2);

          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsLowerMultiplier'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newLowerMultiplier);
            });
          agGrid
            .getCellItemByModelName(config.name, 'RelativeModelBoundsLowerBias')
            .invoke('text')
            .then((content) => {
              expect(Number(content)).eq(newLowerBias);
            });
          agGrid
            .getCellItemByModelName(
              config.name,
              'RelativeModelBoundsLowerBound'
            )
            .invoke('text')
            .then((content) => {
              expect(Number(content).toFixed(2).toString()).eq(
                lowerBoundaryDistance
              );
            });
        });
      });
    });
  });
  describe(`Anomalies - Fixed Limit`, () => {
    before(() => {
      modelConfigEdit.selectAnomaliesFilter('Fixed Limits');
    });
    it(`Set Fixed Upper Limit Enabled`, () => {
      // set all to No and then Yes to check Limit value default to 1
      user.selectFromList('No', 'EnableUpperFixedLimit');
      user.applyValueToColumn(`EnableUpperFixedLimit`);
      user.selectFromList('Yes', 'EnableUpperFixedLimit');
      user.applyValueToColumn(`EnableUpperFixedLimit`);

      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('EnableUpperFixedLimit', index)
          .should('contain.text', 'Yes');
        agGrid
          .getCellItemByColumnName('UpperFixedLimit', index)
          .should('contain.text', '1');
      });
    });
    it(`Set Fixed Lower Limit Enabled`, () => {
      // set all to No and then Yes to check Limit value default to 1
      user.selectFromList('No', 'EnableLowerFixed');
      user.applyValueToColumn(`EnableLowerFixed`);
      user.selectFromList('Yes', 'EnableLowerFixed');
      user.applyValueToColumn(`EnableLowerFixed`);

      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('EnableLowerFixed', index)
          .should('contain.text', 'Yes');
        agGrid
          .getCellItemByColumnName('LowerFixedLimit', index)
          .should('contain.text', '1');
      });
    });
  });
  describe(`Anomalies - Model Bound Criticality`, () => {
    before(() => {
      modelConfigEdit.selectAnomaliesFilter('Model Bound Criticality');
    });
    it(`Set Model Bound Criticality Default Enabled`, () => {
      // set all to No and then Yes to check Limit value default to 1
      user.selectFromList('No', 'UseDefault');
      user.applyValueToColumn(`UseDefault`);
      user.selectFromList('Yes', 'UseDefault');
      user.applyValueToColumn(`UseDefault`);

      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('UseDefault', index)
          .should('contain.text', 'Yes');
      });
    });
    it(`Upper and Lower should be correct`, () => {
      configApiResponse.forEach((config, index) => {
        const anomaliesCriticality = config.anomalies.find(
          (c) => c.type === 'criticality'
        );
        agGrid
          .getCellItemByColumnName('UpperBound', index)
          .should(
            'contain.text',
            anomaliesCriticality.properties.lowerBoundDefault
          );
        agGrid
          .getCellItemByColumnName('LowerBound', index)
          .should(
            'contain.text',
            anomaliesCriticality.properties.upperBoundDefault
          );
      });
    });
  });
  describe(`Trainig tab`, () => {
    before(() => {
      modelConfigEdit.openConfigurationTab('Training Range');
    });
    it(`Training duration`, () => {
      user
        .clickCellByColumnName('TrainingDuration')
        .type(`${trainingDuration}{enter}`);

      user.applyValueToColumn(`TrainingDuration`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingDuration', index)
          .should('contain.text', trainingDuration);
      });
    });
    it(`Training Duration Temporal Type`, () => {
      user.selectFromList(
        trainingDurationTemporalType,
        'TrainingDurationTemporalType'
      );
      user.applyValueToColumn(`TrainingDurationTemporalType`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingDurationTemporalType', index)
          .should('contain.text', trainingDurationTemporalType);
      });
    });
    it(`Training Lag`, () => {
      user.clickCellByColumnName('TrainingLag').type(`${trainingLag}{enter}`);

      user.applyValueToColumn(`TrainingLag`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingLag', index)
          .should('contain.text', trainingLag);
      });
    });
    it(`Training Lag Temporal Type`, () => {
      user.selectFromList(trainingLagTemporalType, 'TrainingLagTemporalType');
      user.applyValueToColumn(`TrainingLagTemporalType`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingLagTemporalType', index)
          .should('contain.text', trainingLagTemporalType);
      });
    });
    it(`Proposed training range should b correct`, () => {
      const endDate = moment()
        .subtract(trainingLag, 'days')
        .format('MM/DD/YYYY');
      const startDate = moment(endDate)
        .subtract(trainingDuration, 'days')
        .format('MM/DD/YYYY');
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingStartDate', index)
          .should('contain.text', `${startDate} - ${endDate}`);
      });
    });
    it(`Sample Rate`, () => {
      user
        .clickCellByColumnName('TrainingSampleRate')
        .type(`${trainingSampleRate}{enter}`);

      user.applyValueToColumn(`TrainingSampleRate`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingSampleRate', index)
          .should('contain.text', trainingSampleRate);
      });
    });
    it(`Training Sample Rate Temporal Type`, () => {
      user.selectFromList(
        trainingSampleRateTemporalType,
        'TrainingSampleRateTemporalType'
      );
      user.applyValueToColumn(`TrainingSampleRateTemporalType`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingSampleRateTemporalType', index)
          .should('contain.text', trainingSampleRateTemporalType);
      });
    });
    it(`Sample Rate`, () => {
      user
        .clickCellByColumnName('TrainingMinDataPoints')
        .type(`${trainingMinDataPoints}{enter}`);

      user.applyValueToColumn(`TrainingMinDataPoints`);
      agGrid.getGridRows().each(($el, index, $list) => {
        agGrid
          .getCellItemByColumnName('TrainingMinDataPoints', index)
          .should('contain.text', trainingMinDataPoints);
      });
    });
  });
});
