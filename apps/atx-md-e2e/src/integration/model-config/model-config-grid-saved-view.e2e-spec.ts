import { AlertsUser } from '../../support/helpers/user-alerts';
import { ModelConfigUser } from '../../support/helpers/user-model-config';
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import { analyst } from '../../support/helpers/user-accounts';
import * as Pages from '../../support/page/pages';
import { Helper } from '../../support/helpers/helper';
import { AlertsColumnID } from '../../support/page/common/ag-grid/grid-column-ids';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';

const util = new Utils();
// const helper = new Helper();
const modelConfigPage = new Pages.ModelConfig();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];

const columnOrder = [
  'Model Name',
  'Model Type',
  'Variable',
  'Asset Class',
  'Active',
  'Standard',
  'Maintenance',
  'Age',
  'Actions',
  'Operating Mode',
  'Score',
  'Lower Bound Tolerance',
  'Upper Bound Tolerance',
  'Last Build Status',
  'Last Build Date',
  'Last Update Date',
  'Tag Name',
  'Units',
];

describe('ALERTS - SAVED VIEWS', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    user.showPanel('view'); // open
    user.showHideAssetNavigator();
    user.restoreGridToDefaults();
  });
  describe(`Reorder`, () => {
    it(`Saved view Reorder should load correctly`, () => {
      user.selectSavedView('reorder1');
      user.showPanel('column');

      cy.wrap(modelConfigPage.sideButtons.column.getColumnNames()).then(
        (cols) => {
          expect(cols).to.deep.eq(columnOrder);
        }
      );
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('reorder1');
      user.showPanel('column');

      cy.wrap(modelConfigPage.sideButtons.column.getColumnNames()).then(
        (cols) => {
          expect(cols).to.deep.eq(columnOrder);
        }
      );
    });
  });
  describe(`Group`, () => {
    const modelTypes = [
      'APR',
      'Fixed Limit',
      'Frozen Data',
      'Moving Average',
      'Rolling Average',
    ];
    before(() => {
      user.showPanel('view');
    });
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('group1');
      modelConfigPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.restoreGridToDefaults();
      user.selectSavedView('group1');

      modelConfigPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });
    });
  });
  describe(`Group + Reorder`, () => {
    const modelTypes = [
      'APR',
      'Fixed Limit',
      'Frozen Data',
      'Moving Average',
      'Rolling Average',
    ];
    const columnOrder = [
      'Model Type',
      'Model Name',
      'Variable',
      'Asset Class',
      'Active',
      'Standard',
      'Maintenance',
      'Age',
      'Actions',
      'Operating Mode',
      'Score',
      'Lower Bound Tolerance',
      'Upper Bound Tolerance',
      'Last Build Status',
      'Last Build Date',
      'Last Update Date',
      'Tag Name',
      'Units',
    ];
    it(`Saved view Group should load correctly`, () => {
      user.selectSavedView('reorder+group');
      user.showPanel('column');
      modelConfigPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });

      cy.wrap(modelConfigPage.sideButtons.column.getColumnNames()).then(
        (cols) => {
          expect(cols).to.deep.eq(columnOrder);
        }
      );
    });
    it(`Restore defaults, select view again, verify columns`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      user.selectSavedView('reorder+group');
      user.showPanel('column');

      modelConfigPage.agGridMain.getGroupNames().then((groupNames) => {
        groupNames.forEach((modelType) =>
          expect(modelTypes).includes(modelType)
        );
      });

      cy.wrap(modelConfigPage.sideButtons.column.getColumnNames()).then(
        (cols) => {
          expect(cols).to.deep.eq(columnOrder);
        }
      );
    });
  });
  describe(`Filter`, () => {
    it(`Saved view Filter should load correctly`, () => {
      user.showPanel('view');
      user.restoreGridToDefaults();
      // util.loadAgGridForWait('AlertsGrid');
      user.selectSavedView('filter1');
      // cy.wait('@AlertsGrid', { timeout: 15000 });
      modelConfigPage.headerPanel.getFilterChips().then((el) => {
        expect(el.length).to.eq(2);
        cy.wrap(el)
          .find('[data-cy="filter-column-name"]')
          .then((e) => {
            expect(e[0].textContent).eq('Active');
            expect(e[1].textContent).eq('Model Type');
          });
      });
      modelConfigPage.agGridMain
        .getItemsByColumnName('ModelTypeAbbrev')
        .then((items) => {
          items.forEach((item) => expect(item).eq('APR'));
        });
    });
  });
});
