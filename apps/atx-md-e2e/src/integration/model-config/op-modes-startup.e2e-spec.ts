/* eslint-disable cypress/no-unnecessary-waiting */
import {
  userObj,
  AppNamesNewUI,
  NavType,
  assetToTest,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import _ from 'lodash';
import { analyst } from '../../support/helpers/user-accounts';
import { ModelConfigUser } from '../../support/helpers/user-model-config';

const util = new Utils();
const agGridMain = new Pages.AgGridMain();
const user = new ModelConfigUser();
const clientToSelect = assetToTest[Cypress.env().env]['modelConfig'];
const modelConfigPage = new Pages.ModelConfig();
const opModesPage = new Pages.ModelConfigOpModes();

describe('MODEL CONFIG - Op Modes tab', () => {
  before(() => {
    user.logIn(analyst);
    user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    cy.wait(3000);
    user.openTab('OpModes');
  });
  it(`Select Start up Op mode and verify start criteria`, () => {
    user.inputFilterValue('Startup - 1', 'Operating Mode Name');
    user.selectOpModeByIndex(0).then((req) => {
      const startCriteriaRes = req.response.body.startCriteria;
      const endCriteriaRes = req.response.body.endCriteria;

      cy.wrap(opModesPage.getCriteriaDetails()).then((criteria) => {
        startCriteriaRes.forEach((startCriteriaItemRes) => {
          const criteriaItems = criteria.find(
            (c) =>
              startCriteriaItemRes.criteriaGroupID.toString() ===
              c.criteriaGroupId
          );

          criteriaItems.logic.forEach((criteriaItem, criteriaItemIdx) => {
            const tempLogic = startCriteriaItemRes.logic[criteriaItemIdx];
            const tempTagDesc = `${tempLogic.tagDesc} (${tempLogic.tagName})`;
            expect(criteriaItem.tagDesc).eq(tempTagDesc);
            expect(criteriaItem.sign).eq(tempLogic.logicOperatorTypeAbbrev);
            expect(Number(criteriaItem.value)).eq(tempLogic.value);
            expect(criteriaItem.unit).eq(tempLogic.engUnits);
          });
        });
      });
    });
  });
});
