/* eslint-disable cypress/no-unnecessary-waiting */
import {
  userObj,
  AppNamesNewUI,
  NavType,
} from '../../support/helpers/user-data';
import * as Pages from '../../support/page/pages';
import { Utils } from '../../support/helpers/utils';
import { Helper } from '../../support/helpers/helper';
import _ from 'lodash';
import { ModelConfigUser } from '../../support/helpers/user-model-config';
import { AssetConfigUser } from '../../support/helpers/user-asset-config';
import { AnomaliesConfig, OpModesTypes } from '../../support/definitions';
import { Interception } from 'cypress/types/net-stubbing';
import createOpModePayload from '../../fixtures/op-mode/create-op-mode-1.json';
import { CyRequests } from '../../support/helpers/requests';
import { testAutomationUser1 } from '../../support/helpers/user-accounts';
import moment from 'moment';
import { ApiStub } from '../../support/helpers/stub/api-stub';

const util = new Utils();
const helper = new Helper();
const agGrid = new Pages.AgGridMain();
const user = new ModelConfigUser();
const userAssetConfig = new AssetConfigUser();
const modelConfigPage = new Pages.ModelConfig();
const modelConfigEdit = new Pages.ModelEdit();
const opModePage = new Pages.ModelConfigOpModes();
let tagGridResponse: Interception;
const cyRequests = new CyRequests();
const opModeTitle1 = `TestAuto OpMode 1 ` + new Date().toISOString(),
  opModeTitle2 = `TestAuto OpMode 2 ` + new Date().toISOString(),
  opModeTitle3 = `TestAuto OpMode 3 ` + new Date().toISOString();
const modelName = 'TestAuto Model ' + new Date().toISOString();
const tagName1 = `TestAuto Tag 1 ${new Date().toISOString()}`;
const tagName2 = `TestAuto Tag 2 ${new Date().toISOString()}`,
  tagDesc = `Test Auto ${new Date().toISOString()}`;
let tagVariableName: string, opModeResponse;
let tagToUse1, tagToUse2;
const assetToCreate = {
  assetName: `TestAuto Aseet-${new Date().toISOString()}`,
};
const clientToSelect = {
  parent: 'Asset Health Monitoring',
  children: [
    'Live MD Test Group',
    'Live MD Test Station',
    'Live MD Test Unit',
    `Test Auto (Don't Touch)`,
  ],
};
let actionDateNowArr: string[];
let reqOptions, modelUrl;
const stub = new ApiStub();

describe('Create Op Mode + Create Model + E2E Tests', () => {
  before(() => {
    user.logIn(userObj.default);
    user.navigateToApp(AppNamesNewUI.assetConfig, NavType.navBar);
    user.selectAssetFromNavigator(clientToSelect);
    cy.saveLocalStorage();
    // user.openTab('OpModes');
    // util.loadAgGridForWait('TagGrid');
  });

  describe(`Create Asset`, () => {
    it(`create Asset`, () => {
      userAssetConfig.selectAssetByName('AssetName', `Test Auto (Don't Touch)`);
      userAssetConfig.createAsset(assetToCreate);
      userAssetConfig.selectChildAssetFromNavigator([
        `Test Auto (Don't Touch)`,
        assetToCreate.assetName,
      ]);
    });
  });
  describe(`create new Tag and Op Mode`, () => {
    it(`create new tag1 ${tagName1}`, () => {
      userAssetConfig.openTab('Tags');
      userAssetConfig
        .addNewTag(tagName1, tagDesc)
        .should('contain.text', 'Tags Saved Successfully');
      agGrid
        .getCellItemByColumnName('Name', -1)
        .should('contain.text', tagName1);
      agGrid
        .getCellItemByColumnName('Description', -1)
        .should('contain.text', tagDesc);

      user.inputFilterValue(tagName1, 'Tag Name', 'tag-list-config');
      agGrid.getCellItemByColumnName('Name').eq(0).as('tagNameToDrag');
      agGrid.getCellItemByColumnName('AssetName').eq(0).as('tagDropZone');
      new Utils().dragAndDrop('tagNameToDrag', 'tagDropZone');
    });
    it(`create new tag2 ${tagName2}`, () => {
      // userAssetConfig.openTab('Tags');
      userAssetConfig.clearFilter('Tag Name');
      userAssetConfig
        .addNewTag(tagName2, tagDesc)
        .should('contain.text', 'Tags Saved Successfully');
      agGrid
        .getCellItemByColumnName('Name', -1)
        .should('contain.text', tagName2);
      agGrid
        .getCellItemByColumnName('Description', -1)
        .should('contain.text', tagDesc);

      user.inputFilterValue(tagName2, 'Tag Name', 'tag-list-config');
      agGrid.getCellItemByColumnName('Name').eq(0).as('tagNameToDrag');
      agGrid.getCellItemByColumnName('AssetName').eq(0).as('tagDropZone');
      new Utils().dragAndDrop('tagNameToDrag', 'tagDropZone');
    });
    // skipping this for now until I can come up
    // with a solution for drag and drop
    // it(`Create new Op Mode`, () => {
    //   util.loadAgGridForWait('TagGrid');
    //   user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
    //   user.openTab('OpModes');
    //   cy.wait('@TagGrid').then((res) => (tagGridResponse = res));
    //   const opModeTitle = `Op Mode Test ${new Date().toISOString()}`;
    //   const modeType: OpModesTypes = 'Exclusion Period';
    //   user.createNewOpMode(opModeTitle, modeType);
    //   opModesPage.opModeName().should('have.value', opModeTitle);
    // });
    // it(`Move Tag to Associated`, () => {
    //   user.moveTagToAssociatedByIdx(0);
    // cy.wrap(tagRes).then((tagGridRes) => {
    //   console.log(`XXX: `, tagGridRes);
    // });
    // });
    it(`Create Op mode through API`, () => {
      createOpModePayload.opModeDefinitionTitle = opModeTitle1;
      user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
      user.openTab('OpModes');
      util.loadAgGridForWait('TagGrid');
      user.inputFilterValue(tagName1, 'Name');

      cy.wait('@TagGrid').then((tagGridRes) => {
        tagToUse1 = tagGridRes.response.body.Results[0].TagMaps[0];
        tagVariableName = tagToUse1.VariableType
          ? tagToUse1.VariableType.VariableDesc
          : null;
        createOpModePayload.assetTagMaps[0].assetVariableTypeTagMapID =
          tagToUse1.AssetVariableTypeTagMapID;
        createOpModePayload.assetTagMaps[0].tagDesc = tagToUse1.Tag.TagDesc;
        createOpModePayload.assetTagMaps[0].tagName = tagToUse1.Tag.tagName;
        createOpModePayload.startCriteria[0].logic[0].assetVariableTypeTagMapID =
          tagToUse1.AssetVariableTypeTagMapID;
        createOpModePayload.startCriteria[0].logic[0].tagDesc =
          tagToUse1.Tag.TagDesc;
        createOpModePayload.startCriteria[0].logic[0].tagName =
          tagToUse1.Tag.tagName;
        createOpModePayload.startCriteria[0].logic[0].tagID = tagToUse1.TagID;
        reqOptions = {
          method: 'POST',
          headers: { authorization: tagGridRes.request.headers.authorization },
          body: createOpModePayload,
        };
        cyRequests
          .opMode(reqOptions)
          .then(
            (res) =>
              (opModeResponse =
                res.allRequestResponses[0]['Response Body'].data)
          );
      });
      // it(`verify created op mode`, () => {
      //   user.openTab('TagList');
      //   user.openTab('OpModes');
      //   user.inputFilterValue(opModeTitle, 'Operating Mode Name');
      //   user.selectOpModeByIndex(0);
      // });
    });
    it(`Create Op mode through API - excluding created Tag ${tagName2}`, () => {
      createOpModePayload.opModeDefinitionTitle = opModeTitle3;
      user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
      user.openTab('OpModes');
      util.loadAgGridForWait('TagGrid');
      user.inputFilterValue(tagName2, 'Name');

      cy.wait('@TagGrid').then((tagGridRes) => {
        tagToUse2 = tagGridRes.response.body.Results[0].TagMaps[0];

        createOpModePayload.opModeTypeID = 3;
        createOpModePayload.opModeTypeAbbrev = 'Out of Service';
        createOpModePayload.assetTagMaps[0].assetVariableTypeTagMapID =
          tagToUse2.AssetVariableTypeTagMapID;
        createOpModePayload.assetTagMaps[0].tagDesc = tagToUse2.Tag.TagDesc;
        createOpModePayload.assetTagMaps[0].tagName = tagToUse2.Tag.tagName;

        // exlucded tag
        const excludedTag = {
          opModeDefinitionAssetTagMapID: -1,
          assetVariableTypeTagMapID: tagToUse1.AssetVariableTypeTagMapID,
          include: false,
          tagDesc: tagToUse1.Tag.TagDesc,
          tagName: tagToUse1.Tag.tagName,
          bringDescendants: false,
        };
        createOpModePayload.assetTagMaps.push(excludedTag);

        createOpModePayload.startCriteria[0].logic[0].assetVariableTypeTagMapID =
          tagToUse2.AssetVariableTypeTagMapID;
        createOpModePayload.startCriteria[0].logic[0].tagDesc =
          tagToUse2.Tag.TagDesc;
        createOpModePayload.startCriteria[0].logic[0].tagName =
          tagToUse2.Tag.tagName;
        createOpModePayload.startCriteria[0].logic[0].tagID = tagToUse2.TagID;
        reqOptions = {
          method: 'POST',
          headers: { authorization: tagGridRes.request.headers.authorization },
          body: createOpModePayload,
        };

        cyRequests
          .opMode(reqOptions)
          .then(
            (res) =>
              (opModeResponse =
                res.allRequestResponses[0]['Response Body'].data)
          );
      });
      // it(`verify created op mode`, () => {
      //   user.openTab('TagList');
      //   user.openTab('OpModes');
      //   user.inputFilterValue(opModeTitle, 'Operating Mode Name');
      //   user.selectOpModeByIndex(0);
      // });
    });
  });
  describe(`create new Model`, () => {
    it(`Create model with op-mode created - ${opModeTitle1}`, () => {
      const anomaliesConfig: AnomaliesConfig = {
        relativeModelBounds: {
          relativeUpperBounds: {
            multiplier: 4,
            bias: 1,
          },
          relativeLowerBounds: {
            multiplier: 4,
            bias: 1,
          },
        },
      };
      user.openTab('TagList');
      user.inputFilterValue(tagName1, 'Tag Name');
      user.inputFilterValue(tagDesc, 'Tag Description');
      if (tagVariableName) user.inputFilterValue(tagVariableName, 'Variable');
      user.selectTagByIndex(0, 'tag-list');

      user.createNewModel(modelName, 'Rolling Average');
      user.inputAnomalyConfig(anomaliesConfig);
      user.saveModel(modelName);
      user.rebuildModel();
      cy.wait('@PrioritizedOpModeDefinitionsForTagMap', { timeout: 25000 });
    });
    it(`verify op mode created in Context tab`, () => {
      user.openContextTab();

      cy.wrap(modelConfigPage.getContextOpModeDetails()).then(
        (opModeDetails) => {
          const temp = opModeDetails.find(
            (opMode) => opMode.opModeTitle === `Startup : ${opModeTitle1}`
          );
          const startCriteria = createOpModePayload.startCriteria[0];
          const opModeTitle = temp.opModeTitle;
          expect(opModeTitle).eq(`Startup : ${opModeTitle1}`);
          expect(temp.criteria).eq(
            `[${tagName1} ${startCriteria.logic[0].logicOperatorTypeAbbrev} ${startCriteria.logic[0].value}]`
          );
        }
      );
    });
    it(`create another op mode and verify in context tab`, () => {
      createOpModePayload.opModeDefinitionTitle = opModeTitle2;

      createOpModePayload.opModeTypeID = 3;
      createOpModePayload.opModeTypeAbbrev = 'Out of Service';
      const includedTag = {
        opModeDefinitionAssetTagMapID: -1,
        assetVariableTypeTagMapID: tagToUse1.AssetVariableTypeTagMapID,
        include: true,
        tagDesc: tagToUse1.Tag.TagDesc,
        tagName: tagToUse1.Tag.tagName,
        bringDescendants: false,
      };
      createOpModePayload.assetTagMaps = []; // clear assetTagMaps
      createOpModePayload.assetTagMaps.push(includedTag);

      cyRequests
        .opMode(reqOptions)
        .then(
          (res) =>
            (opModeResponse = res.allRequestResponses[0]['Response Body'].data)
        );

      stub.PrioritizedOpModeDefinitionsForTagMap();
      cy.reload();
      cy.wait('@PrioritizedOpModeDefinitionsForTagMap', { timeout: 15000 });
      user.openContextTab();
      cy.wrap(modelConfigPage.getContextOpModeDetails()).then(
        (opModeDetails) => {
          const temp = opModeDetails.find(
            (opMode) =>
              opMode.opModeTitle ===
              `${createOpModePayload.opModeTypeAbbrev} : ${opModeTitle2}`
          );
          const startCriteria = createOpModePayload.startCriteria[0];

          expect(temp.opModeTitle).eq(
            `${createOpModePayload.opModeTypeAbbrev} : ${opModeTitle2}`
          );
          expect(temp.criteria).eq(
            `[${tagName2} ${startCriteria.logic[0].logicOperatorTypeAbbrev} ${startCriteria.logic[0].value}]`
          );
        }
      );
    });
  });
  describe(`Op Mode update message`, () => {
    it(`Op mode change pop up should appear`, () => {
      helper
        .getSnackBarText()
        .should(
          'have.text',
          `This model has a related op mode that has changed since its last build or save date and may not reflect` +
            ` the latest criteria defined.  Save this model to reflect the latest op mode criteria.  See the "Context" tab for details.`
        );
    });
    it(`save and verify Op mode change pop up no lower visible`, () => {
      user.saveModel(modelName);
      user.rebuildModel();
      helper.getSnackBarText().should('not.exist');
    });
  });
  describe(`Set Model Maintenance`, () => {
    it(`Click Model Maintenance button`, () => {
      user.selectActionBtnFlyout('Model Maintenance');
    });
    it('notes text box should be visibe', () => {
      modelConfigPage.flyoutWindow
        .modelMaintenanceNoteFld()
        .should('be.visible');
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Maintenance Note Saved!');
    });
    it('Clear Maintenance button should be visible in flyout window', () => {
      modelConfigPage.flyoutWindow.actionBtns
        .clearModelMaintenance()
        .should('be.visible');
    });
    it(`Maintanance Status should be visible`, () => {
      modelConfigPage.flyoutWindow
        .maintainenceStatusIcon()
        .should('be.visible');
    });
  });
  describe(`Clear Model Maintenance`, () => {
    it(`Click Clear Model Maintenance button`, () => {
      user.selectActionBtnFlyout('Clear Maintenance');
    });
    it('notes text box should be visibe', () => {
      modelConfigPage.flyoutWindow
        .modelMaintenanceNoteFld()
        .should('be.visible');
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Maintenance Note Saved!');
      actionDateNowArr = getDateNow();
    });
    it('Maintenance button should be visible in flyout window', () => {
      modelConfigPage.flyoutWindow.actionBtns
        .modelMaintenance()
        .should('be.visible');
    });
    it(`history should be updated`, () => {
      modelConfigPage.openTrendTab('History');
      cy.wait('@HistoryGrid');
      const tempHistoryObj =
        modelConfigPage.historyGrid.getLatestHistoryItem('Model Config');

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal(
          'Model Maintenance Cleared'
        );
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          testAutomationUser1.email
        );
      });
    });
  });
  describe(`Set Diagnose`, () => {
    it(`Click Model Maintenance button`, () => {
      user.selectActionBtnFlyout('Diagnose');
      modelConfigPage.flyoutWindow.diagnoseNoteFld().should('be.visible');
    });
    it('notes text box should be visibe', () => {
      modelConfigPage.flyoutWindow.diagnoseNoteFld().should('be.visible');
    });
    it('Save note and snackbar message should appear', () => {
      user.saveNote();
      helper.getSnackBarText().should('have.text', 'Diagnose Note Saved');
      actionDateNowArr = getDateNow();
    });
    it('Clear Diagnose button should be visible in flyout window', () => {
      modelConfigPage.flyoutWindow.actionBtns.diagnose().should('not.exist');
      modelConfigPage.flyoutWindow.actionBtns
        .clearDiagnose()
        .should('be.visible');
    });
    it(`Diagnose Status should be visible`, () => {
      modelConfigPage.flyoutWindow.diagnoseStatusIcon().should('be.visible');
    });
    it(`history should be updated`, () => {
      modelConfigPage.openTrendTab('History');
      // cy.wait('@HistoryGrid');
      const tempHistoryObj =
        modelConfigPage.historyGrid.getLatestHistoryItem('Model Config');

      cy.wrap(tempHistoryObj).then((obj) => {
        expect(obj.filter((e) => e.History)[0].History).to.equal(
          'Diagnose Set'
        );
        expect(actionDateNowArr).includes(
          obj.filter((e) => e.ChangeDate)[0].ChangeDate
        );
        expect(obj.filter((e) => e.Executor)[0].Executor).to.equal(
          testAutomationUser1.email
        );
      });
    });
  });
  describe(`Update Op Mode shows affected Models`, () => {
    before(() => {
      user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);

      user.selectAssetFromNavigator(clientToSelect);
      user.openTab('OpModes');
      util.loadAgGridForWait('TagGrid');
      user.inputFilterValue(opModeTitle1, 'Operating Mode Name', 'opmode-list');
      user.selectOpModeByIndex(0);
    });
    it(`Update Op Mode Name shows affected models`, () => {
      user.inputOpModeName(`Edited ${opModeTitle1}`, true);
      user.saveOpMode();
      opModePage.affectedModelsList().should('contain.text', modelName);
    });
    // it.skip(`discard Changes`, () => {
    //   user.cancelOpModeSave();
    //   user.discardOpModeChanges();
    //   opModePage.opModeName().should('contain.value', opModeTitle1);
    // });
    it(`Update Op Mode name and verify in Model Context tab`, () => {
      stub.OpMode();
      stub.PrioritizedOpModeDefinitionsForTagMap();
      opModePage.opModeSaveDiaglogSaveBtn().click();
      cy.wait('@OpMode');

      user.openTab('ModelList');
      user.inputFilterValue(modelName, 'Model Name');
      util.newTabListener();
      user.doubleClickCellByColumnName('ModelName');
      util.switchToNewTab();

      // cy.visit(modelUrl);
      cy.intercept('GET', '**/UserAuthenticated').as('userAuth');
      stub.userPreferences();
      cy.wait('@PrioritizedOpModeDefinitionsForTagMap', { timeout: 15000 });
    });
    it(`Op mode change pop up should appear`, () => {
      helper
        .getSnackBarText()
        .should(
          'have.text',
          `This model has a related op mode that has changed since its last build or save date and may not reflect` +
            ` the latest criteria defined.  Save this model to reflect the latest op mode criteria.  See the "Context" tab for details.`
        );
    });
    it(`verify op mode created in Context tab`, () => {
      user.openContextTab();
      cy.wrap(modelConfigPage.getContextOpModeDetails()).then(
        (opModeDetails) => {
          const temp = opModeDetails.find(
            (opMode) =>
              opMode.opModeTitle === `Startup : Edited ${opModeTitle1}`
          );
          const startCriteria = createOpModePayload.startCriteria[0];

          expect(temp.opModeTitle).eq(`Startup : Edited ${opModeTitle1}`);
          expect(temp.criteria).eq(
            `[${tagName1} ${startCriteria.logic[0].logicOperatorTypeAbbrev} ${startCriteria.logic[0].value}]`
          );
        }
      );
    });
  });
  describe(`Alerts Notification`, () => {
    let selectedSubscribedUser1 = '',
      selectedSubscribedUser2 = '';
    it(`open Alerts configuration`, () => {
      user.openConfigurationTab('Alerts');
    });
    it(`alerts notif should be disabled`, () => {
      modelConfigEdit
        .getAlertsNotifBtn('AnomalyFrequencyNotifications')
        .invoke('attr', 'class')
        .should('contain', 'disable');
      modelConfigEdit
        .getAlertsNotifBtn('HighHighNotifications')
        .invoke('attr', 'class')
        .should('contain', 'disable');
    });
    it(`open and verify Anomaly Frequency Alert notification`, () => {
      user.openAlertNotifications('AnomalyFrequencyNotifications');
      modelConfigEdit
        .getNotificationDetails()
        .alertType()
        .should('contain.text', 'Anomaly Frequency Alert');
      modelConfigEdit
        .getNotificationDetails()
        .modelName()
        .should('contain.text', modelName);
      modelConfigEdit
        .getNotificationDetails()
        .tag()
        .should('include.text', tagName1)
        .and('contain.text', tagDesc);
      modelConfigEdit
        .getNotificationDetails()
        .asset()
        .should('contain.text', `Test Auto (Don't Touch)`);
    });
    it(`update and verify Alerts notification custom message`, () => {
      const message = 'This is a test custom message';
      user.inputAlertNotifCustomMsg(message);
      user.selectUsersToSubscribeByIdx(0).then(($selectedUser) => {
        selectedSubscribedUser1 = $selectedUser.text();
        user.saveAlertNotification();

        user.openAlertNotifications('AnomalyFrequencyNotifications');
        modelConfigEdit
          .getAlertsNotifSubscribedUsers()
          .should('contain.text', $selectedUser.text());
      });
    });
    it(`subscribed user should not appear in Users drop down]`, () => {
      modelConfigEdit.addUsersFld().click();
      modelConfigEdit
        .addUsersFld()
        .should('not.contain.text', selectedSubscribedUser1);

      cy.get(`button`).contains('Cancel').click();
    });
    it(`Alerts notification button enabled`, () => {
      modelConfigEdit
        .getAlertsNotifBtn('AnomalyFrequencyNotifications')
        .invoke('attr', 'class')
        .should('not.contain', 'disable');
    });
    it(`add another subscribed user2`, () => {
      user.openAlertNotifications('AnomalyFrequencyNotifications');

      user.selectUsersToSubscribeByIdx(0).then(($selectedUser) => {
        selectedSubscribedUser2 = $selectedUser.text();
        user.saveAlertNotification();

        user.openAlertNotifications('AnomalyFrequencyNotifications');
        modelConfigEdit
          .getAlertsNotifSubscribedUsers()
          .should('contain.text', $selectedUser.text());
      });
    });
    it(`remove subscribed user1`, () => {
      user.removeSubscribedUser(selectedSubscribedUser1);
      user.saveAlertNotification();
      user.openAlertNotifications('AnomalyFrequencyNotifications');
      modelConfigEdit
        .getAlertsNotifSubscribedUsers()
        .should('contain.text', selectedSubscribedUser2)
        .and('not.contain.text', selectedSubscribedUser1);
    });
    it(`delete alerts notif`, () => {
      user.deleteAlertsNotif();
    });
  });
  describe(`Delete Model, Op Mode, and Asset`, () => {
    it(`delete model`, () => {
      user.deleteModel(modelName);

      modelConfigPage.noModel().should('contain.text', 'No Model Selected');
    });
    it(`delete op mode`, () => {
      clientToSelect.children.push(assetToCreate.assetName);
      user.navigateToApp(AppNamesNewUI.modelConfig, NavType.navBar);
      user.selectAssetFromNavigator(clientToSelect);
      user.openTab('OpModes');
      util.loadAgGridForWait('TagGrid');
      user.inputFilterValue(opModeTitle1, 'Operating Mode Name', 'opmode-list');
      user.selectOpModeByIndex(0);

      user.deleteOpMode();
    });
    it(`delete Asset`, () => {
      user.navigateToApp(AppNamesNewUI.assetConfig, NavType.navBar);
      userAssetConfig.selectAssetByName('AssetName', assetToCreate.assetName);
      userAssetConfig.deleteAsset(assetToCreate.assetName);
    });
  });
});

function getDateNow(): string[] {
  const dateNow: string[] = [];
  dateNow.push(moment().format('MM/DD/YYYY hh:mm A'));
  dateNow.push(moment().add('1', 'minute').format('MM/DD/YYYY hh:mm A'));
  return dateNow;
}
