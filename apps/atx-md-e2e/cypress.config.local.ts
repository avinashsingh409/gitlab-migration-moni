import { defineConfig } from 'cypress';

export default defineConfig({
  fileServerFolder: '.',
  fixturesFolder: './apps/atx-md-e2e/src/fixtures',
  modifyObstructiveCode: false,
  video: false,
  videosFolder: '../../dist/cypress/apps/atx-md-e2e/videos',
  screenshotsFolder: '../../dist/cypress/apps/atx-md-e2e/screenshots',
  chromeWebSecurity: false,
  defaultCommandTimeout: 10000,
  env: {
    local: 'http://localhost:4200/',
    dev: 'https://dev.atonix.com/MD',
    test: 'https://test.atonix.com/MD',
    stage: 'https://stage.atonix.com/MD',
    restoreLocalStorage: true,
  },
  viewportWidth: 1920,
  viewportHeight: 1080,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      // return require('./apps/atx-md-e2e/src/plugins/index.js')(on, config);
    },
    specPattern:
      './apps/atx-md-e2e/src/integration/**/*.e2e-spec.{js,jsx,ts,tsx}',
    supportFile: './apps/atx-md-e2e/src/support/e2e.ts',
  },
});
