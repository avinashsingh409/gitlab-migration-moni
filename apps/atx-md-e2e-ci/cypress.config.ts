import { defineConfig } from 'cypress';
import { nxE2EPreset } from '@nrwl/cypress/plugins/cypress-preset';
import setupNodeEvents from './src/plugins/index';

const cypressJsonConfig = {
  fileServerFolder: '.',
  fixturesFolder: './src/fixtures',
  video: false,
  viewportWidth: 1900,
  viewportHeight: 1060,
  retries: {
    runMode: 2,
    openMode: 0,
  },
  env: {
    local: 'http://localhost:4200/',
    dev: 'https://dev.atonix.com/MD',
    test: 'https://test.atonix.com/MD',
    stage: 'https://stage.atonix.com/MD',
    restoreLocalStorage: true,
  },
  videosFolder: '../../dist/cypress/apps/atx-md-e2e-ci/videos',
  screenshotsFolder: '../../dist/cypress/apps/atx-md-e2e-ci/screenshots',
  chromeWebSecurity: false,
  specPattern: 'src/**/*.spec.{js,jsx,ts,tsx}',
  supportFile: 'src/support/e2e.ts',
};
export default defineConfig({
  e2e: {
    ...nxE2EPreset(__dirname),
    ...cypressJsonConfig,
    setupNodeEvents,
  },
});
