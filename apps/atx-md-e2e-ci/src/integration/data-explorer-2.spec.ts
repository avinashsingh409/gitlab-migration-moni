import '@percy/cypress';
import { User } from '../support/helpers/user';
import { AppNamesNewUI, NavType, userObj } from '../support/helpers/user-data';
const user = new User();

describe('atx-md-data-explorer', () => {
  before(() => {
    cy.clearCookies();
    cy.clock(Date.UTC(2022, 0, 1), ['Date']);
  });
  describe('visual regression - data explorer part 2', () => {
    beforeEach(() => {
      cy.atonixUserLightBackground();
      cy.dataExplorerTaskCenter2();
    });
    it('load data explorer home', () => {
      cy.visit('/');
      user.logInCiCD(userObj.default);

      cy.wait(
        ['@notifications', '@userauth-default', '@usepreferences-default'],
        { requestTimeout: 10000 }
      );

      user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.navBar);

      cy.wait('@asset-access')
        .should('include.all.keys', ['request', 'response'])
        .then(() => {
          cy.wait(['@live-md-test-unit-tag-grid', '@live-md-test-unit-trends']);
        });
    });
    it('should snapshot grouped series chart', () => {
      cy.get('.mat-list-option').should('be.visible').eq(1).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@Tags`).then(() => {
        cy.get(`.highcharts-root`).should('be.visible').wait(1000);
        cy.percySnapshot('DE White - Grouped series chart', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot line chart with no pins and chart zoomed', () => {
      cy.get('.mat-list-option').should('be.visible').eq(9).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@ArchivesExtFromTagId`).then(() => {
        cy.get(`.highcharts-root`).should('be.visible').wait(1000);
        cy.percySnapshot('DE White - Line Chart No Pins', {
          widths: [1900],
          scope: '.main-nav',
        });

        cy.get(`.highcharts-container .highcharts-root`)
          .should('be.visible')
          .trigger('mousedown', { button: 1, x: 300, y: 100 })
          .trigger('mousemove')
          .trigger('mouseup');
        cy.get(`.highcharts-container .highcharts-root`) // move mouse to check zoom issue before
          .should('be.visible')
          .trigger('mousemove', { x: 100, y: 50 })
          .trigger('mouseup');
        cy.wait(2000);
        cy.percySnapshot('DE White - Line Chart No Pins - ZOOM', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot line chart with pins', () => {
      cy.get('.mat-list-option').should('be.visible').eq(4).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);
      cy.wait(`@ArchivesExtFromTagId`).then(() => {
        cy.wait(2000);
        cy.percySnapshot('DE White - Line Chart With Pins', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot chart scatter plot', () => {
      cy.get('.mat-list-option').should('be.visible').eq(3).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);
      cy.wait(`@ArchivesExtFromTagId`).then(() => {
        cy.wait(2000);
        cy.percySnapshot('DE White - Scatter Plot', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot table', () => {
      cy.get('.mat-list-option').should('be.visible').eq(6).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);
      cy.wait(`@ArchivesExtFromTagId`).then(() => {
        cy.wait(2000);
        cy.percySnapshot('DE White - Table', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
  });
});
