// import '@percy/cypress';
// import { AlertsUser } from '../support/helpers/user-alerts';
// import { envToTest } from '../support/helpers/user-data';
// import { ApiStub } from '../support/helpers/stub/api-stub';
// import { analyst } from '../support/helpers/user-accounts';

// const user = new AlertsUser();
// const apiStud = new ApiStub();
// const url =
//   `diagnostic-drilldown?model=8CFE1390-B152-4294-BBDA-E13151E5B016&asset=d3aef33a-1c0e-4f84-85ca-44e0d3c6273a` +
//   `&start=1659256349130&end=1660051121050`;
// describe('Diagnostic Drilldown', () => {
//   before(() => {
//     cy.atonixUser();
//     apiWait();
//     // cy.visit('/');
//     cy.visit(envToTest + url);
//     user.logInCiCD(analyst);
//   });
//   it(`DDD - chart loads`, () => {
//     cy.wait([
//       `@modelTrendForAlertsChartingByModelExtId`,
//       `@PDNDModelTrendsWithAssetGuid`,
//       `@TagsDataFilteredZoom`,
//     ]);
//     cy.wait(2800);
//     cy.percySnapshot('DDD - chart loads default', {
//       widths: [1900],
//     });
//   });
//   it(`Expand Model Context`, () => {
//     cy.get(`.bottom-section .trend-header`)
//       .contains(`keyboard_arrow_up`)
//       .eq(0)
//       .click();
//     cy.wait(2000);
//     cy.percySnapshot('DDD - Expand Model Context Chart', {
//       widths: [1900],
//     });
//   });
//   it(`Model Context - Zoom`, () => {
//     cy.get(
//       `.model-context-trends-container .highcharts-container .highcharts-root`
//     )
//       .eq(0)
//       .trigger('mousedown', { button: 1, x: 300, y: 100 })
//       .trigger('mousemove')
//       .trigger('mouseup');

//     cy.wait(5000);
//     cy.percySnapshot('DDD - Chart Zoom', {
//       widths: [1900],
//     });
//   });
// });

// function apiWait() {
//   apiStud.modelTrendForAlertChartingByModelExtId(
//     `ModelTrendForAlertChartingByModelExtId`
//   );
//   apiStud.PDNDModelTrendsWithAssetGuid(`PDNDModelTrendsWithAssetGuid`);
//   apiStud.modelByGUID(`ModelByGUID`);
//   apiStud.tagsDataFilteredZoom();
// }
