import '@percy/cypress';
import { User } from '../support/helpers/user';
import { AppNamesNewUI, NavType, userObj } from '../support/helpers/user-data';
const user = new User();

describe('atx-md-data-explorer', () => {
  before(() => {
    cy.clearCookies();
    cy.clock(Date.UTC(2022, 0, 1), ['Date']);
  });
  describe('visual regression - data explorer part one', () => {
    beforeEach(() => {
      cy.atonixUser();
      cy.dataExplorerTaskCenter1();
    });
    it('load data explorer home', () => {
      cy.visit('/');
      user.logInCiCD(userObj.default);
      cy.wait(
        ['@notifications', '@userauth-default', '@usepreferences-default'],
        { requestTimeout: 10000 }
      );

      user.navigateToApp(AppNamesNewUI.dataExplorer, NavType.navBar);

      cy.wait('@asset-access')
        .should('include.all.keys', ['request', 'response'])
        .then(() => {
          cy.wait(['@live-md-test-unit-tag-grid', '@live-md-test-unit-trends']);
        });
    });
    it('should snapshot data explorer home page', () => {
      cy.get('.mat-list-option').should('be.visible').eq(0).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@Tags`).then(() => {
        cy.get(`.highcharts-root`).should('be.visible').wait(1000);

        cy.percySnapshot('DE Home Page Dark', {
          widths: [1900],
        });
      });
    });
    it('should snapshot data explorer Group Series', () => {
      cy.get('.mat-list-option').should('be.visible').eq(1).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@Tags`).then(() => {
        cy.get(`.highcharts-root`).should('be.visible').wait(1000);
        cy.percySnapshot('DE Dark - Grouped Series', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot data explorer Group Series Table', () => {
      cy.get('.mat-list-option').should('be.visible').eq(7).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@Tags`).then(() => {
        cy.get(`.mat-table`).should('be.visible').wait(1000);
        cy.percySnapshot('DE Dark - Grouped Series Table', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
    it('should snapshot data explorer Group Series Ordering', () => {
      cy.get('.mat-list-option').should('be.visible').eq(8).click();
      cy.wait(['@line-chart-measurements']).should('include.all.keys', [
        'request',
        'response',
      ]);

      cy.wait(`@Tags`).then(() => {
        cy.get(`.highcharts-root`).should('be.visible').wait(1000);
        cy.percySnapshot('DE Dark - Grouped Series Ordering', {
          widths: [1900],
          scope: '.main-nav',
        });
      });
    });
  });
});
