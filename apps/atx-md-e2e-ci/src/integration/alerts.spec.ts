import '@percy/cypress';
import { AlertsUser } from '../support/helpers/user-alerts';
import { AppNamesNewUI, NavType, userObj } from '../support/helpers/user-data';
const user = new AlertsUser();

describe('atx-md-alerts', () => {
  before(() => {
    cy.clearCookies();
    cy.clock(Date.UTC(2021, 0, 1), ['Date']);
  });

  describe('visual regression - alerts snapshots', () => {
    beforeEach(() => {
      cy.atonixUser();
      cy.alertsTaskCenter();
    });

    it('should load alerts home page and snaspshot alerts home page with no model selected', () => {
      cy.visit('/');
      user.logInCiCD(userObj.default);
      cy.wait(
        ['@notifications', '@userauth-default', '@usepreferences-default'],
        { requestTimeout: 10000 }
      );
      user.navigateToApp(AppNamesNewUI.alerts, NavType.navBar);
      cy.wait(['@alerts-list-config', '@asset-access'], {
        requestTimeout: 10000,
      });
      cy.wait('@live-md-test-unit-summary').then(() => {
        cy.percySnapshot('ASV Dark - Home with no model selected', {
          widths: [1900],
          scope: '.bottom-half',
        });
      });
    });
    it('should select model, load tabs section and load Alerts Actions Pane and snapshot each', () => {
      user.selectModelByName('Live Test Unit on Dev - Model 3').then(() => {
        cy.get('.screening-container .ag-side-buttons').should('be.visible');
        cy.get('#alertsAction').should('be.visible');

        cy.percySnapshot('ASV Dark - Grid', {
          widths: [1900],
          scope: '.screening-container',
        });
        cy.percySnapshot('ASV Dark - Action Pane ', {
          scope: '#alertsAction',
        });

        cy.get('.chartComponentContent')
          .should('be.visible')
          .then(() => {
            cy.percySnapshot('ASV Dark - Trend Tab', {
              scope: '.model-trend-chart',
            });
            user.openContentTab('Context').then(() => {
              cy.get('.chart').should('be.visible');

              cy.wait(3000);
              cy.percySnapshot('ASV Dark - Context Tab', {
                widths: [1900],
                scope: '.chart',
              });

              cy.get(`.highcharts-container .highcharts-root`)
                .should('be.visible')
                .trigger('mousedown', { button: 1, x: 300, y: 100 })
                .trigger('mousemove')
                .trigger('mouseup');
              cy.get(`.highcharts-container .highcharts-root`) // move mouse to check zoom issue before
                .should('be.visible')
                .trigger('mousemove', { x: 100, y: 50 })
                .trigger('mouseup');

              cy.wait(3000);
              cy.percySnapshot('ASV Dark -  Context tab chart ZOOM', {
                widths: [1900],
                scope: '.chart',
              });
            });
          });
      });

      user.openContentTab('Actions').then(() => {
        cy.get('.chart').should('be.visible');
        cy.wait(3000).then(() => {
          cy.percySnapshot('ASV Dark - Actions Tab', {
            widths: [1900],
            scope: '.chart',
          });
        });
      });
    });
    it('should update diagnose icon in grid and button in Alerts Actions Pane', () => {
      user.selectActionBtnFlyout('Diagnose');
      user.saveAction();
      cy.wait(3000).then(() => {
        cy.get('.screening-container .ag-side-buttons')
          .should('be.visible')
          .then(() => {
            if (cy.get('.ag-side-buttons')) {
              cy.percySnapshot(
                'ASV Dark - Grid selected row diagnose icon changed',
                {
                  widths: [1900],
                  scope: '.screening-container',
                }
              );
            }
          });
        cy.get('#alertsAction')
          .should('be.visible')
          .then(() => {
            cy.percySnapshot(
              'ASV Dark - Action Pane diagnose button changed ',
              {
                widths: [1900],
                scope: '#alertsAction',
              }
            );
          });
      });
    });
  });
});
