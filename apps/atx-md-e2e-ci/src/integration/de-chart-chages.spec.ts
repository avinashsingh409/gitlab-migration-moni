// import '@percy/cypress';
// import { User } from '../support/helpers/user';

// import { ApiStub } from '../support/helpers/stub/api-stub';
// import { DataExplorerUser } from '../support/helpers/user-de';
// import { analyst } from '../support/helpers/user-accounts';
// import * as Pages from '../support/page/pages';

// const apiStub = new ApiStub();
// const user = new DataExplorerUser();
// const navigation = new Pages.Navigation();

// describe('atx-md-data-explorer', () => {
//   describe('DE - Chart Changes', () => {
//     it('DEFAULT chart', () => {
//       cy.visit(
//         '/data-explorer?id=MjdjZWJiNzEtZDY3MC00OTBmLTk2NmYtZDJmZWI1MzBiM2Y1fjVmNWY1Yjc0LWE0YTUtNDViNC05N2M5LTM1M2FmZmFkN2Y5OX43OTAwMDA1MTE%3D&' +
//           'trend=1629126&start=1629473880000&end=1629733080000'
//       );
//       apiWait();
//       cy.get('.email-field').should('be.visible');
//       user.logInCiCD(analyst);
//       cy.wait(
//         [
//           `@TagGrid`,
//           `@PDTrends`,
//           `@GetCriteriaWithDataTimeFilters`,
//           `@TagsDataFiltered`,
//           `@ArchivesExtFromTagId`,
//           `@Asset`,
//         ],
//         { timeout: 5000 }
//       );
//       cy.wait(2000);
//       user.inputSeriesName(`Updated Series 1`, 0);
//       user.inputSeriesName(`Updated Series 2`, 1);
//       user.inputSeriesName(`Updated Series 3`, 2);

//       // cy.percySnapshot('DE - chart name/legend change', {
//       //   widths: [1900],
//       //   scope: `g.highcharts-legend .highcharts-legend-box`,
//       // });

//       cy.get(`[data-cy="expandColapseChartBtn"]`).click().wait(1000);
//       // hide time slider to show chart legends refer to bug
//       // https://dev.azure.com/AtonixDigital/Asset360/_workitems/edit/51023
//       navigation.leftNav.toggleTimeSliderBtn().click().wait(2000);

//       cy.percySnapshot(
//         'DE - Series name update + chart Expanded + Timeslider hidden',
//         {
//           widths: [1900],
//         }
//       );
//     });
//   });
// });

// function apiWait() {
//   const fixturePath = `/de/chart-changes/`;
//   apiStub.PDTrends(`${fixturePath}PDTrends`);
//   apiStub.GetCriteriaWithDataTimeFilters(
//     `${fixturePath}GetCriteriaWithDataTimeFilters`
//   );
//   apiStub.TagGrid(`${fixturePath}TagGrid`);
//   apiStub.TagsDataFiltered(`${fixturePath}TagsDataFiltered`);
//   apiStub.ArchivesExtFromTagId(`${fixturePath}ArchivesExtFromTagId`);
//   apiStub.Asset(`${fixturePath}Asset`);
//   apiStub.LoadNodeFromKey(`${fixturePath}LoadNodeFromKey`);
// }
