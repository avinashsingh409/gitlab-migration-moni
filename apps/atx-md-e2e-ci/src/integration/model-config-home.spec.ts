// import '@percy/cypress';

// import { ApiStub } from '../support/helpers/stub/api-stub';
// import { ModelConfigUser } from '../support/helpers/user-model-config';
// import { analyst } from '../support/helpers/user-accounts';
// import { Utils } from '../support/helpers/utils';

// const apiStub = new ApiStub();
// const user = new ModelConfigUser();
// const util = new Utils();
// // model1 = Model Dependent tag10220
// // model2 = Model Import Scale Test Model 101
// // model3 = Model Import Scale Test Model 106

// describe('atx-md-model-config', () => {
//   describe('Model Config Home', () => {
//     it('Default home', () => {
//       cy.visit('/model-config?start=1659177725537&end=1661856125537');
//       apiWait();
//       cy.get('.email-field').should('be.visible');
//       user.logInCiCD(analyst);
//       cy.wait([`@processDataServers`, `@modelconfiglist`], { timeout: 10000 });

//       cy.wait(2000);
//       cy.percySnapshot('Model config home - grid', {
//         scope: 'atx-model-list .top-section',
//         widths: [1900],
//       });

//       util.modelWait(1);
//       user.selectGridItemByRowIndex(0);
//       cy.wait([
//         `@ModelSummary`,
//         `@config`,
//         `@mlmodellatesttype`,
//         `@ModelTrend`,
//       ]);
//       cy.wait(2000);
//       cy.percySnapshot('Model config home - APR Need Rebuild', {
//         scope: 'atx-model-stats',
//         widths: [1900],
//       });

//       util.modelWait(2);
//       user.selectGridItemByRowIndex(1);
//       cy.wait([
//         `@ModelSummary`,
//         `@config`,
//         `@mlmodellatesttype`,
//         `@ModelTrend`,
//       ]);
//       cy.wait(2000);
//       cy.percySnapshot('Model config home - APR Model Stats', {
//         scope: 'atx-model-stats',
//         widths: [1900],
//       });
//       cy.percySnapshot('Model config home - APR chart', {
//         scope: '.highcharts-root',
//         widths: [1900],
//       });

//       util.modelWait(3);
//       user.selectGridItemByRowIndex(2);
//       cy.wait([
//         `@ModelSummary`,
//         `@config`,
//         `@mlmodellatesttype`,
//         `@ModelTrend`,
//       ]);
//       cy.wait(2000);
//       cy.percySnapshot(
//         'Model config home - Model Selected - Diagnosed, Model Maintenance, Watch',
//         {
//           scope: 'atx-model-config-action-pane',
//           widths: [1900],
//         }
//       );

//       util.modelWait(4);
//       user.selectGridItemByRowIndex(3);
//       cy.wait([
//         `@ModelSummary`,
//         `@config`,
//         `@mlmodellatesttype`,
//         `@ModelTrend`,
//       ]);
//       cy.wait(2000);
//       cy.percySnapshot('Model config home - Moving Average - New MEA', {
//         scope: 'atx-model-stats',
//         widths: [1900],
//       });
//     });
//   });
// });

// function apiWait() {
//   const fixturePath = `/model-config/`;
//   apiStub.processDataServers(`${fixturePath}servers`);
//   apiStub.modelconfiglist(`${fixturePath}modelconfiglist`);
// }
