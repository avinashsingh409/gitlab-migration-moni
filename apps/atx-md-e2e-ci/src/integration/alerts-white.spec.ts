import '@percy/cypress';
import { AlertsUser } from '../support/helpers/user-alerts';
import { AppNamesNewUI, NavType, userObj } from '../support/helpers/user-data';
const user = new AlertsUser();

describe('atx-md-alerts', () => {
  before(() => {
    cy.clearCookies();
    cy.clock(Date.UTC(2021, 0, 1), ['Date']);
    //const now = new Date(Date.UTC(2020, 2, 14)).getTime();
  });
  describe('visual regression - alerts', () => {
    beforeEach(() => {
      cy.atonixUserLightBackground();
      cy.alertsTaskCenter();
    });
    it('should load home page and snapshot sections with light theme', () => {
      cy.visit('/');
      user.logInCiCD(userObj.default);
      cy.wait([
        '@notifications',
        '@userauth-default',
        '@usepreferences-default',
      ]);
      cy.percySnapshot('Homepage - Light', { widths: [1900] });

      user.navigateToApp(AppNamesNewUI.alerts, NavType.navBar);
      cy.wait(['@alerts-list-config', '@asset-access']);

      cy.wait('@live-md-test-unit-summary')
        .should('include.all.keys', ['request', 'response'])
        .then(() => {
          user.selectModelByName('Live Test Unit on Dev - Model 3');
          cy.wait([
            '@live-md-test-unit-pdndmodel',
            '@live-md-test-unit-model-trend',
            '@live-md-test-unit-action-items',
            '@model-context-measurements',
          ]).then(() => {
            cy.get('.screening-container')
              .should('be.visible')
              .then(() => {
                cy.percySnapshot('ASV White - Grid', {
                  scope: '.screening-container',
                });
              });

            cy.get('#alertsAction')
              .should('be.visible')
              .then(() => {
                cy.percySnapshot('ASV White - Action Pane ', {
                  scope: '#alertsAction',
                });
              });

            cy.get('.chartComponentContent')
              .should('be.visible')
              .then(() => {
                user.openContentTab('Actions').then(() => {
                  cy.wait(3000);
                  cy.percySnapshot('Alerts Actions Tab - Light', {
                    widths: [1900],
                    scope: '.models-container',
                  });
                });

                user.openContentTab('Context').then(() => {
                  cy.wait(3000);
                  cy.percySnapshot('Alerts Context Tab - Light', {
                    widths: [1900],
                    scope: '.models-container',
                  });
                });
              });
          });
        });
    });
  });
});
