// import '@percy/cypress';

// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const util = new Utils();
// const agGrid = new Pages.AgGridMain();

// let MAE,
//   upperMultiplier,
//   upperBias,
//   upperBoundaryDistance,
//   lowerMultiplier,
//   lowerBias,
//   lowerBoundaryDistance;
// const newUpperMultipler = 4,
//   newUpperBias = 2,
//   newLowerMultiplier = 3,
//   newLowerBias = -2;

// describe('Model Config - Relative Model Bounds', () => {
//   it('Relative model bounds should be correct', () => {
//     cy.visit(
//       '/model-config/m?modelId=23B95558-AD2C-478E-A0ED-070D8F381E06&isStandard=false&aid=6493f5a6-6502-4aaa-88b1-09e803befb62&start=1665311505309&end=1667989905309'
//     );
//     util.modelWait(1);
//     cy.get('.email-field').should('be.visible');
//     user.logInCiCD(analyst);

//     cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
//       timeout: 15000,
//     }).then((req) => {
//       MAE = req[3].response.body.OutputDetails.MAE;

//       const relativeBounds = req[1].response.body[0].anomalies.find(
//         (p) => p.type === 'relative_bounds'
//       );
//       upperMultiplier = relativeBounds.properties.upperMultiplier;
//       upperBias = relativeBounds.properties.upperBias;
//       upperBoundaryDistance = (MAE * upperMultiplier + upperBias).toFixed(3);
//       lowerMultiplier = relativeBounds.properties.lowerMultiplier;
//       lowerBias = relativeBounds.properties.lowerBias;
//       lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(3);

//       modelConfigEdit.showHideActionsPane();
//       modelConfigEdit.openConfigurationTab('Sensitivity');
//       modelConfigEdit.selectAnomaliesFilter('Relative Model Bounds');

//       agGrid
//         .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//         .should('have.text', upperBoundaryDistance);
//       agGrid
//         .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//         .should('have.text', lowerBoundaryDistance);
//     });
//   });
//   it(`Change Upper Multiplier `, () => {
//     upperBoundaryDistance = (MAE * newUpperMultipler + upperBias).toFixed(3);
//     lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(3);

//     user
//       .clickCellByColumnName('RelativeModelBoundsUpperMultiplier')
//       .type(`{enter}${newUpperMultipler}{enter}`);

//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//       .should('have.text', upperBoundaryDistance);
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//       .should('have.text', lowerBoundaryDistance);
//   });
//   it(`Change Upper Bias `, () => {
//     upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(3);
//     lowerBoundaryDistance = (MAE * lowerMultiplier - lowerBias).toFixed(3);

//     user
//       .clickCellByColumnName('RelativeModelBoundsUpperBias')
//       .type(`{enter}${newUpperBias}{enter}`);

//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//       .should('have.text', upperBoundaryDistance);
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//       .should('have.text', lowerBoundaryDistance);
//   });
//   it(`Change Lower Multipler  `, () => {
//     upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(3);
//     lowerBoundaryDistance = (MAE * newLowerMultiplier - lowerBias).toFixed(3);

//     user
//       .clickCellByColumnName('RelativeModelBoundsLowerMultiplier')
//       .type(`{enter}${newLowerMultiplier}{enter}`);

//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//       .should('have.text', upperBoundaryDistance);
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//       .should('have.text', lowerBoundaryDistance);
//   });
//   it(`Change Lower Bias `, () => {
//     upperBoundaryDistance = (MAE * newUpperMultipler + newUpperBias).toFixed(3);
//     lowerBoundaryDistance = (MAE * newLowerMultiplier - newLowerBias).toFixed(
//       3
//     );

//     user
//       .clickCellByColumnName('RelativeModelBoundsLowerBias')
//       .type(`{enter}${newLowerBias}{enter}`);

//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//       .should('have.text', upperBoundaryDistance);
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//       .should('have.text', lowerBoundaryDistance);
//   });
// });
