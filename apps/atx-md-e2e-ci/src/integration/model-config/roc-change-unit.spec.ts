// import '@percy/cypress';

// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const util = new Utils();
// const agGrid = new Pages.AgGridMain();

// describe('Model Config - ROC change units', () => {
//   it('change Tag Unit in Slope Analysis Tab', () => {
//     cy.visit(
//       '/model-config/m?modelId=23B95558-AD2C-478E-A0ED-070D8F381E06&isStandard=false&aid=6493f5a6-6502-4aaa-88b1-09e803befb62&start=1665311505309&end=1667989905309'
//     );
//     util.modelWait('ROC');
//     cy.get('.email-field').should('be.visible');
//     user.logInCiCD(analyst);

//     cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
//       timeout: 15000,
//     });
//     user.openConfigurationTab('Slope Analysis');
//     cy.wait(500);
//     modelConfigEdit.showHideActionsPane();

//     user.selectFromList('PSID/year', 'TagUnits');
//     agGrid
//       .getCellItemByColumnName('TagUnits')
//       .should('contain.text', 'PSID/year');
//   });
//   it(`change persists in Sensitity Tab`, () => {
//     user.openConfigurationTab('Sensitivity');

//     user.selectAnomaliesFilter('Relative Model Bounds');
//     agGrid
//       .getCellItemByColumnName('TagUnits')
//       .should('contain.text', 'PSID/year');
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsLowerBound')
//       .should('contain.text', 'PSID/year');
//     agGrid
//       .getCellItemByColumnName('RelativeModelBoundsUpperBound')
//       .should('contain.text', 'PSID/year');
//   });
// });
