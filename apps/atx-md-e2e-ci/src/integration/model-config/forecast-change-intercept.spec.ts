// import '@percy/cypress';

// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const util = new Utils();
// const agGrid = new Pages.AgGridMain();

// describe('Model Config - Forecast', () => {
//   beforeEach(() => cy.clock(Date.UTC(2021, 10, 9), ['Date']));
//   it('Relative - change Time Span Duration', () => {
//     cy.visit(
//       '/model-config/m?modelId=23B95558-AD2C-478E-A0ED-070D8F381E06&isStandard=false&aid=6493f5a6-6502-4aaa-88b1-09e803befb62&start=1665311505309&end=1667989905309'
//     );
//     util.modelWait('Forecast');
//     cy.get('.email-field').should('be.visible');
//     user.logInCiCD(analyst);

//     cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
//       timeout: 15000,
//     });
//     modelConfigEdit.showHideActionsPane();

//     user
//       .clickCellByColumnName('ForecastTimeSpanDuration')
//       .type('{enter}1{enter}');

//     agGrid
//       .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//       .should('contain.text', '11/16/2021');
//   });
//   describe(`Days - Time Span Units`, () => {
//     it(`Change Time Span Unit to Days`, () => {
//       user.selectFromList('Days', 'ForecastTimeSpanDurationUnitOfTime');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '11/10/2021');
//     });
//     it(`Change Time Span duration`, () => {
//       user
//         .clickCellByColumnName('ForecastTimeSpanDuration')
//         .type('{enter}5{enter}');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '11/14/2021');
//     });
//   });
//   describe(`Weeks - Time Span Units`, () => {
//     it(`Change Time Span Unit to Weeks`, () => {
//       user.selectFromList('Weeks', 'ForecastTimeSpanDurationUnitOfTime');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '12/14/2021');
//     });
//     it(`Change Time Span duration`, () => {
//       user
//         .clickCellByColumnName('ForecastTimeSpanDuration')
//         .type('{enter}1{enter}');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '11/16/2021');
//     });
//   });
//   describe(`Months - Time Span Units`, () => {
//     it(`Change Time Span Unit to Months`, () => {
//       user.selectFromList('Months', 'ForecastTimeSpanDurationUnitOfTime');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '12/09/2021');
//     });
//     it(`Change Time Span duration`, () => {
//       user
//         .clickCellByColumnName('ForecastTimeSpanDuration')
//         .type('{enter}5{enter}');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '04/09/2022');
//     });
//   });
//   describe(`Years - Time Span Units`, () => {
//     it(`Change Time Span Unit to Years`, () => {
//       user.selectFromList('Years', 'ForecastTimeSpanDurationUnitOfTime');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '11/09/2026');
//     });
//     it(`Change Time Span duration`, () => {
//       user
//         .clickCellByColumnName('ForecastTimeSpanDuration')
//         .type('{enter}1{enter}');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('contain.text', '11/09/2022');
//     });
//   });

//   describe(`Change Earliest Acceptable Intercept Type to Fixed`, () => {
//     it(`ForecastRelativeEarliesInterceptDate should be empty and locked`, () => {
//       user.selectFromList('Fixed', 'ForecastHorizonType');
//       agGrid
//         .getCellItemByColumnName('ForecastRelativeEarliesInterceptDate')
//         .should('be.empty')
//         .invoke('attr', 'class')
//         .should('contain', 'locked');
//     });
//     it(`ForecastTimeSpanDuration should be empty and locked`, () => {
//       agGrid
//         .getCellItemByColumnName('ForecastTimeSpanDuration')
//         .invoke('attr', 'class')
//         .should('contain', 'locked');
//       agGrid
//         .getCellItemByColumnName('ForecastTimeSpanDurationUnitOfTime')
//         .should('be.empty');
//     });
//     it(`ForecastTimeSpanDurationUnitOfTime should be empty and locked`, () => {
//       agGrid
//         .getCellItemByColumnName('ForecastTimeSpanDurationUnitOfTime')
//         .invoke('attr', 'class')
//         .should('contain', 'locked');
//     });
//   });
// });
