// import '@percy/cypress';

// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const util = new Utils();

// describe('Model Config - Frozen Data', () => {
//   it('Default home', () => {
//     cy.visit(
//       '/model-config/m?modelId=23B95558-AD2C-478E-A0ED-070D8F381E06&isStandard=false&aid=6493f5a6-6502-4aaa-88b1-09e803befb62&start=1636330134980&end=1636464180000'
//     );
//     util.modelWait('FD');
//     cy.get('.email-field').should('be.visible');
//     user.logInCiCD(analyst);

//     cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
//       timeout: 15000,
//     });
//     cy.percySnapshot('Model Single Edit - FD Header Section', {
//       scope: '.header-section',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FD Model Stats', {
//       scope: 'atx-model-stats',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FD Input Tab', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//     modelConfigEdit.openConfigurationTab('Training Range');
//     cy.percySnapshot('Model Single Edit - FD Training Range', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//     modelConfigEdit.openConfigurationTab('Sensitivity');
//     cy.wait(500);
//     cy.percySnapshot('Model Single Edit - FD Sensitivity', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FD Model Trend', {
//       scope: '.highcharts-root',
//       widths: [1900],
//     });
//   });
// });
