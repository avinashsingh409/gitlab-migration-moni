// import '@percy/cypress';

// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const util = new Utils();

// describe('Model Config - FIXED LIMIT', () => {
//   it('Default home', () => {
//     cy.visit(
//       '/model-config/m?modelId=8d285a8b-3d1b-4eff-8a5c-f233434c71fc&isStandard=false&aid=a5046d7e-2e80-4d8c-bdc7-dabbe9d6eeea&start=1664362522207&end=1666954522208'
//     );
//     util.modelWait('FL');
//     cy.get('.email-field').should('be.visible');
//     user.logInCiCD(analyst);

//     cy.wait([`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`], {
//       timeout: 15000,
//     });
//     cy.percySnapshot('Model Single Edit - FL Header Section', {
//       scope: '.header-section',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FL Model Stats', {
//       scope: 'atx-model-stats',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FL Model Trend', {
//       scope: '.highcharts-root',
//       widths: [1900],
//     });
//     cy.percySnapshot('Model Single Edit - FL Input Tab', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//     modelConfigEdit.openConfigurationTab('Training Range');
//     cy.percySnapshot('Model Single Edit - FL Training Range', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//     modelConfigEdit.openConfigurationTab('Sensitivity');
//     cy.wait(500);
//     cy.percySnapshot('Model Single Edit - FL Sensitivity', {
//       scope: '[data-cy="configurationSection"]',
//       widths: [1900],
//     });
//   });
// });
