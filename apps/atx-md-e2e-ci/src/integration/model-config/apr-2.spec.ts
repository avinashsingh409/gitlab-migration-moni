// import '@percy/cypress';
// import { ModelConfigUser } from '../../support/helpers/user-model-config';
// import { analyst } from '../../support/helpers/user-accounts';
// import * as Pages from '../../support/page/pages';
// import { Utils } from '../../support/helpers/utils';
// import modelConfigDetails from '../../fixtures/model-config/model-1/config.json';

// const user = new ModelConfigUser();
// const modelConfigEdit = new Pages.ModelEdit();
// const navigation = new Pages.Navigation();
// const util = new Utils();

// describe('Model Config - APR', () => {
//   describe(`Predictive Method`, () => {
//     it('Change to Logistic Regression', () => {
//       cy.visit(
//         '/model-config/m?modelId=23B95558-AD2C-478E-A0ED-070D8F381E06&isStandard=false&aid=6493f5a6-6502-4aaa-88b1-09e803befb62&start=1665311505309&end=1667989905309'
//       );
//       util.modelWait(1);
//       cy.get('.email-field').should('be.visible');
//       user.logInCiCD(analyst);

//       cy.wait(
//         [`@ModelSummary`, `@config`, `@mlmodellatesttype`, `@ModelTrend`],
//         {
//           timeout: 15000,
//         }
//       );
//       modelConfigEdit.selectPredictiveMethod('Logistic Regression');

//       const logReg = modelConfigDetails[0].properties.predictiveTypes.filter(
//         (p) => p.type === 'logreg'
//       )[0].properties;

//       modelConfigEdit
//         .getRValue()
//         .should(
//           'contain.text',
//           logReg.rPearsonsCorrelationCoefficient.toFixed(4)
//         );

//       modelConfigEdit
//         .getRootMeanSquareError()
//         .should('contain.text', logReg.rootMeanSquareError.toFixed(4));

//       modelConfigEdit
//         .getMeanAbsoluteError()
//         .should('contain.text', logReg.meanAbsoluteError.toFixed(4));
//     });
//     it('Change to Linear Regression', () => {
//       modelConfigEdit.selectPredictiveMethod('Linear Regression');

//       const logReg = modelConfigDetails[0].properties.predictiveTypes.filter(
//         (p) => p.type === 'linreg'
//       )[0].properties;

//       modelConfigEdit
//         .getRValue()
//         .should(
//           'contain.text',
//           logReg.rPearsonsCorrelationCoefficient.toFixed(4)
//         );

//       modelConfigEdit
//         .getRootMeanSquareError()
//         .should('contain.text', logReg.rootMeanSquareError.toFixed(4));

//       modelConfigEdit
//         .getMeanAbsoluteError()
//         .should('contain.text', logReg.meanAbsoluteError.toFixed(4));
//     });
//     it('Change to Nueral Net', () => {
//       modelConfigEdit.selectPredictiveMethod('Neural Net');

//       const logReg = modelConfigDetails[0].properties.predictiveTypes.filter(
//         (p) => p.type === 'gff'
//       )[0].properties;

//       modelConfigEdit
//         .getRValue()
//         .should(
//           'contain.text',
//           logReg.rPearsonsCorrelationCoefficient.toFixed(4)
//         );

//       modelConfigEdit
//         .getRootMeanSquareError()
//         .should('contain.text', logReg.rootMeanSquareError.toFixed(4));

//       modelConfigEdit
//         .getMeanAbsoluteError()
//         .should('contain.text', logReg.meanAbsoluteError.toFixed(4));
//     });
//   });
//   describe(`Model Input`, () => {
//     it(`should display correct model input tag`, () => {
//       const independentTags = modelConfigDetails[0].independents;
//       modelConfigEdit.getModelInputElements().each((modelInput) => {
//         const tagName = modelInput.find('[data-cy="modelInputName"]').text();
//         const tagDesc = modelInput.find(`[data-cy="modelInputDesc"]`).text();
//         const configuredCount = Number(modelInput.find(`.config-field`).text());
//         const expectedValues = independentTags.find(
//           (tag) => tag.tagName === tagName
//         );

//         cy.wrap(modelInput.find('.forced-inclusion-field mat-icon'))
//           .should('have.attr', 'svgicon')
//           .and(
//             'equal',
//             expectedValues.forcedInclusion ? 'atx_lock' : 'atx_lock_open'
//           );
//         cy.wrap(modelInput.find('.in-use-field mat-icon'))
//           .should('have.attr', 'svgicon')
//           .and('equal', expectedValues.inUse ? 'atx_check' : 'atx_alert');
//         expect(tagName).to.equal(expectedValues.tagName);
//         expect(tagDesc).to.equal(`(${expectedValues.tagDesc})`);
//         expect(configuredCount).to.equal(1); // 1 for single edit
//       });
//       modelConfigEdit
//         .getModelInputElements()
//         .should('have.length', independentTags.length);
//     });
//     it(`remove a tag`, () => {
//       const tagToRemove = modelConfigDetails[0].independents[1];
//       modelConfigEdit.removeInputTag(tagToRemove.tagName);

//       const independentTags = modelConfigDetails[0].independents.filter(
//         (p) => p.tagName !== tagToRemove.tagName
//       );

//       modelConfigEdit.getModelInputElements().each((modelInput) => {
//         const tagName = modelInput.find('[data-cy="modelInputName"]').text();
//         const tagDesc = modelInput.find(`[data-cy="modelInputDesc"]`).text();
//         const configuredCount = Number(modelInput.find(`.config-field`).text());
//         const expectedValues = independentTags.find(
//           (tag) => tag.tagName === tagName
//         );

//         cy.wrap(modelInput.find('.forced-inclusion-field mat-icon'))
//           .should('have.attr', 'svgicon')
//           .and(
//             'equal',
//             expectedValues.forcedInclusion ? 'atx_lock' : 'atx_lock_open'
//           );
//         cy.wrap(modelInput.find('.in-use-field mat-icon'))
//           .should('have.attr', 'svgicon')
//           .and('equal', expectedValues.inUse ? 'atx_check' : 'atx_alert');
//         expect(tagName).to.equal(expectedValues.tagName);
//         expect(tagDesc).to.equal(`(${expectedValues.tagDesc})`);
//         expect(configuredCount).to.equal(1); // 1 for single edit
//       });
//       modelConfigEdit
//         .getModelInputElements()
//         .should('have.length', independentTags.length);
//     });
//   });
// });
