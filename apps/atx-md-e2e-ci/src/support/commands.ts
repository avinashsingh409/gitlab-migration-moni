// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Chainable<Subject> {
    login(email: string, password: string): void;
    atonixUser(userName?: string): void;
    analystUser(): void;
    atonixUserLightBackground(): void;
    alertsTaskCenter(): void;
    dataExplorerTaskCenter1(): void;
    dataExplorerTaskCenter2(): void;
  }
}

Cypress.Commands.add('login', (email, password) => {
  console.log('Custom command example: Login', email, password);
});

Cypress.Commands.add('atonixUser', (userName) => {
  const fixturePath = userName ? `account/${userName}/` : `account/`;

  cy.intercept('GET', '/Services/api/Authorization/CognitoSettings', {
    fixture: 'authorization/cognito.json',
  }).as('cognitoSettings');
  cy.intercept(
    'GET',
    '/Services/api/Authorization/IdentityProvider?username=testautomationuser1@atonixtest.com&redirectUri=http://localhost:4200/',
    {
      fixture: 'authorization/identity.json',
    }
  ).as('identityProvider');
  cy.intercept('GET', '/Services/api/Authorization/UI', {
    fixture: 'authorization/ui-default.json',
  }).as('ui-default');
  cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
    // fixture: 'account/userauthenticated-default.json',
    fixture: `${fixturePath}userauthenticated-default.json`,
  }).as('userauth-default');
  cy.intercept('GET', '/Services/api/Account/UserInfo', {
    // fixture: 'account/userinfo-default.json',
    fixture: `${fixturePath}userinfo-default.json`,
  }).as('userinfo-default');
  cy.intercept('GET', '/Services/api/Account/UserPreferences', {
    fixture: 'account/usepreferences-default.json',
  }).as('usepreferences-default');
  cy.intercept('GET', '/Services/api/Jobs/Notifications?pageSize=5&offset=0', {
    fixture: 'account/notifications.json',
  }).as('notifications');
  cy.intercept('GET', '/Services/api/Authorization/AssetAccessRights', {
    // fixture: 'authorization/asset-access.json',
    fixture: `${fixturePath}asset-access.json`,
  }).as('asset-access');
  const loadNodeFromKeyPath = userName
    ? `ui-config/assets/${userName}/`
    : `ui-config/assets/`;
  cy.intercept('GET', '/api/v1/assets/trees/load**', {
    // fixture: 'ui-config/assets/live-md-test-unit-asset.json',
    fixture: `${loadNodeFromKeyPath}live-md-test-unit-asset.json`,
  }).as('live-md-test-unit-asset');
});

Cypress.Commands.add('analystUser', () => {
  cy.intercept('GET', '/Services/api/Authorization/CognitoSettings', {
    fixture: 'authorization/cognito.json',
  }).as('cognitoSettings');
  cy.intercept(
    'GET',
    '/Services/api/Authorization/IdentityProvider?username=testautomationuser1@atonixtest.com&redirectUri=http://localhost:4200/',
    {
      fixture: 'authorization/identity.json',
    }
  ).as('identityProvider');
  cy.intercept('GET', '/Services/api/Authorization/UI', {
    fixture: 'authorization/ui-default.json',
  }).as('ui-default');
  cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
    fixture: 'account/userauthenticated-default.json',
  }).as('userauth-default');
  cy.intercept('GET', '/Services/api/Account/UserInfo', {
    fixture: 'account/userinfo-default.json',
  }).as('userinfo-default');
  cy.intercept('GET', '/Services/api/Account/UserPreferences', {
    fixture: 'account/usepreferences-default.json',
  }).as('usepreferences-default');
  cy.intercept('GET', '/Services/api/Jobs/Notifications?pageSize=5&offset=0', {
    fixture: 'account/notifications.json',
  }).as('notifications');
  cy.intercept('GET', '/Services/api/Authorization/AssetAccessRights', {
    fixture: 'authorization/asset-access.json',
  }).as('asset-access');
  cy.intercept('GET', '/api/v1/assets/trees/load**', {
    fixture: 'ui-config/assets/live-md-test-unit-asset.json',
  }).as('live-md-test-unit-asset');
});

Cypress.Commands.add('atonixUserLightBackground', () => {
  cy.intercept('GET', '/Services/api/Authorization/CognitoSettings', {
    fixture: 'authorization/cognito.json',
  }).as('cognitoSettings');
  cy.intercept(
    'GET',
    '/Services/api/Authorization/IdentityProvider?username=testautomationuser1@atonixtest.com&redirectUri=http://localhost:4200/',
    {
      fixture: 'authorization/identity.json',
    }
  ).as('identityProvider');
  cy.intercept('GET', '/Services/api/Authorization/UI', {
    fixture: 'authorization/ui-default.json',
  }).as('ui-default');
  cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
    fixture: 'account/userauthenticated-default.json',
  }).as('userauth-default');
  cy.intercept('GET', '/Services/api/Account/UserInfo', {
    fixture: 'account/userinfo-default.json',
  }).as('userinfo-default');
  cy.intercept('GET', '/Services/api/Account/UserPreferences', {
    fixture: 'account/usepreferences-light.json',
  }).as('usepreferences-default');
  cy.intercept('GET', '/Services/api/Jobs/Notifications?pageSize=5&offset=0', {
    fixture: 'account/notifications.json',
  }).as('notifications');
  cy.intercept('GET', '/Services/api/Authorization/AssetAccessRights', {
    fixture: 'authorization/asset-access.json',
  }).as('asset-access');
  cy.intercept('GET', '/api/v1/assets/trees/load**', {
    fixture: 'ui-config/assets/live-md-test-unit-asset.json',
  }).as('live-md-test-unit-asset');
});

Cypress.Commands.add('alertsTaskCenter', () => {
  cy.intercept('GET', '/Services/api/UIConfig/ListConfig?listType=alerts', {
    fixture: 'ui-config/alerts-list-config.json',
  }).as('alerts-list-config');
  cy.intercept('POST', '/api/alerts/Summary', {
    fixture: 'alerts/live-md-test-unit-summary.json',
  }).as('live-md-test-unit-summary');
  cy.intercept('GET', '/api/alerts/ActionItemsAnalysis**', {
    fixture: 'alerts/live-md-test-unit-action-items.json',
  }).as('live-md-test-unit-action-items');
  cy.intercept('GET', '/Services/api/ProcessData/PDNDModelTrends**', {
    fixture: 'process-data/trends/live-md-test-unit-pdndmodel.json',
  }).as('live-md-test-unit-pdndmodel');
  cy.intercept('POST', 'Services/api/ProcessData/TagsDataFilter**', {
    fixture: 'process-data/measurements/model-context-measurements.json',
  }).as('model-context-measurements');
  cy.intercept('GET', '/Services/api/Monitoring/ModelTrendForAlertCharting**', {
    fixture: 'monitoring/live-md-test-unit-model-trend.json',
  }).as('live-md-test-unit-model-trend');
  cy.intercept('POST', '/Services/api/Monitoring/AlertTimeline', {
    fixture: 'monitoring/live-md-test-unit-alert-timeline.json',
  }).as('live-md-test-unit-alert-timeline');
  cy.intercept('POST', 'Services/api/Monitoring/ActionItem', {
    fixture: 'alerts/live-md-test-unit-action-result',
  }).as('live-md-test-alerts-action');
});

Cypress.Commands.add('dataExplorerTaskCenter1', () => {
  cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid', {
    fixture: 'grid-process-data/live-md-test-unit-tag-grid.json',
  }).as('live-md-test-unit-tag-grid');
  cy.intercept('GET', '/Services/api/ProcessData/PDTrends**', {
    fixture: 'process-data/trends/live-md-test-unit-trends.json',
  }).as('live-md-test-unit-trends');

  let interceptCount = 0;
  cy.intercept('POST', 'Services/api/ProcessData/TagsDataFilter**', (req) => {
    req.reply((res) => {
      interceptCount++;
      if (interceptCount === 1) {
        res.send({
          fixture: 'process-data/measurements/group-series-chart.json',
        });
      } else if (interceptCount === 2) {
        res.send({
          fixture: 'process-data/measurements/group-series-chart-2.json',
        });
      } else if (interceptCount === 3) {
        res.send({
          fixture: 'process-data/measurements/group-series-chart-3.json',
        });
      } else if (interceptCount === 4) {
        res.send({
          fixture: 'process-data/measurements/group-series-chart-4.json',
        });
      } else {
        res.send({
          fixture: 'process-data/measurements/group-series-chart-4.json',
        });
      }
    });
  }).as('line-chart-measurements');

  cy.intercept('POST', '/api/v1/gridprocessdata/Tags').as('Tags');
});
let interceptCount = 1;
Cypress.Commands.add('dataExplorerTaskCenter2', () => {
  cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid', {
    fixture: 'grid-process-data/live-md-test-unit-tag-grid.json',
  }).as('live-md-test-unit-tag-grid');
  cy.intercept('GET', '/Services/api/ProcessData/PDTrends**', {
    fixture: 'process-data/trends/live-md-test-unit-trends.json',
  }).as('live-md-test-unit-trends');

  cy.intercept('POST', 'Services/api/ProcessData/TagsDataFilter**', (req) => {
    req.reply((res) => {
      if (interceptCount === 1) {
        interceptCount++;
        res.send({
          fixture: 'process-data/measurements/group-series-chart.json',
        });
      } else if (interceptCount === 2) {
        interceptCount++;
        res.send({
          fixture: 'process-data/measurements/line-chart-measurements.json',
        });
      } else if (interceptCount === 3) {
        interceptCount++;
        res.send({
          fixture:
            'process-data/measurements/line-chart-with-pins-measurements.json',
        });
      } else if (interceptCount === 4) {
        interceptCount++;
        res.send({
          fixture: 'process-data/measurements/scatter-plot.json',
        });
      } else {
        interceptCount++;
        res.send({
          fixture: 'process-data/measurements/table.json',
        });
      }
    });
  }).as('line-chart-measurements');

  cy.intercept('POST', '/Services/api/ProcessData/ArchivesExtFromTagId').as(
    'ArchivesExtFromTagId'
  );

  cy.intercept('POST', '/api/v1/gridprocessdata/Tags').as('Tags');
});

//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
