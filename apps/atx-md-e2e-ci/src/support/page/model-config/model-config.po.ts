import { AlertsHistoryGrid } from '../alerts/model-tabs/alerts-history-ag-grid.po';
import * as agGrid from '../common/ag-grid/ag-grid';
import { AlertsFlyOutWindow } from '../common/alerts-flyout-window.po';
import { Utils } from '../../../support/helpers/utils';

const util = new Utils();
export class ModelConfig {
  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new agGrid.AgGridMain();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();
  flyoutWindow = new AlertsFlyOutWindow();
  historyGrid = new AlertsHistoryGrid();

  getFilterGridBtn = () => cy.get(`[data-cy="createIssueBtn"]`);
  getRefreshGridBtn = () => cy.get(`[data-cy="refreshGridBtn"]`);
  getDownloadGridBtn = () => cy.get(`[data-cy="downloadGridBtn"]`);
  getModelListBtn = () => cy.get(`[data-cy="modelListBtn"]`);
  getTagListBtn = () => cy.get(`[data-cy="tagListBtn"]`);
  getNoModelMessage = () =>
    cy.get(`atx-model-config-action-pane .noModel > h2`);
  getModelStats = () => cy.get(`atx-model-stats`);

  openContentTab(tabName: 'Model Trend' | 'Expected vs Actual' | 'History') {
    if (tabName === 'History') util.loadAgGridForWait('HistoryGrid');
    cy.get(
      `atx-model-list-bottom-view-panel .mat-tab-header [role="tab"] .mat-tab-label-content`
    )
      .contains(tabName)
      .click();
  }
}
