// import * as userData from '../helpers/user-data';
export class Login {
  passwordCode = {
    email: () => cy.get(`[formControlName="email"]`),
    code: () => cy.get(`[formcontrolname="code"]`),
    newPassword: () => cy.get('[formcontrolname="password1"]'),
    verifyPassword: () => cy.get('[formcontrolname="password2"]'),
  };

  getInputUserEmail = () => cy.get(`[data-cy="username"]`, { timeout: 10000 });
  getInputUserPassword = () =>
    cy.get(`[data-cy="password"]`, { timeout: 5000 });
  getSubmitBtn = () => cy.get('[type="submit"]');
  getUsernameTxt = () => cy.get(`[data-cy="userNameTxt"]`, { timeout: 3000 });
  getError = () => cy.get(`[data-cy="error"]`);
  getSpinner = () => cy.get(`mat-spinner[role="progressbar"]`);
  getForgotPasswordBtn = () => cy.get(`[data-cy="forgotPasswordBtn"]`);
  getEmailForgotPasswordFld = () =>
    cy.get(`[data-cy="emailForgotPasswordFld"]`);
  getPasswordResetBtn = () => cy.get(`[data-cy="requestPasswordResetBtn"]`);

  inputUserEmail(email: string) {
    this.getInputUserEmail().clear().type(email);
  }

  inputUserPassword(password: string) {
    this.getInputUserPassword().clear().type(password);
  }

  clickSignInBtn() {
    this.getSubmitBtn().click({ timeout: 1000 });
  }

  setForgotPasswordEmail(email: string) {
    this.getEmailForgotPasswordFld().type(email);
  }
}
