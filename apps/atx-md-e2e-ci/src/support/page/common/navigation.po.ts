// This was copied from protractor and will be updating when needed

import { AppNamesNewUI } from '../../helpers/user-data';

export class Navigation {
  timeSlider = {
    getCalendarBtn: () => cy.get(`[data-cy="calendarBtn"]`),
  };

  navigationPane = {
    // navItems: $$(`[role="navigation"] > mat-list-item`),
    clickAssetNavigatorBtn: () => {
      cy.get(`[data-cy="assetNavigatorBtn"]`).click();
    },
    // homeBtn: $(`${this.navigationStr}`)
    //   .element(by.cssContainingText(`.mat-list-item`, `Home`)),
  };
  leftNav = {
    expandLeftNavBtn: () => cy.get(`[data-cy="expandLeftNavBtn"]`),
    toggleTimeSliderBtn: () => cy.get(`[data-cy="toggleTimeSliderBtn"]`),
    getApps: (appName: AppNamesNewUI) => {
      switch (appName) {
        case AppNamesNewUI.alerts:
          return cy.get(`[data-cy="navBarAppBtns"][href="/alerts"]`);
        case AppNamesNewUI.issues:
          return cy.get(`[data-cy="navBarAppBtns"][href="/issues"]`);
        case AppNamesNewUI.dataExplorer:
          return cy.get(`[data-cy="navBarAppBtns"][href="/data-explorer"]`);
        case AppNamesNewUI.dashboards:
          return cy.get(`[data-cy="navBarAppBtns"][href="/dashboards"]`);
        default:
          return cy.get('');
      }
    },
  };

  navigateToApp = (appName: AppNamesNewUI) => {
    this.leftNav.getApps(appName).click();
  };
}
