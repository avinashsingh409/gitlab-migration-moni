import { SideColumnPanel } from './side-column-panel.po';
// import { SideButtonSavePanel } from './side-buttons-save';
import { SideViewsPanel } from './side-views-panel.po';
export class SideButtons {
  column = new SideColumnPanel();
  filter = 'filterObj class here';
  view = new SideViewsPanel();
}
