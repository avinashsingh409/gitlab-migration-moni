// import { Helper } from '../../../helpers/helper';
// import { Utils } from '../../../helpers/utils';
import { SideButtons } from './right-panel/side-buttons';

import { ActionType } from '../../../helpers/user-data';
import {
  AlertsColumnID,
  HistoryColumnID,
  IssuesColumnID,
  ModelConfigColumnID,
  ModelConfigForecastColumnID,
  ModelConfigAnomaliesColumnID,
  ModelConfigSlopeAnalysisColumnID,
  ModelConfigTagColumnID,
  TagListColumnID,
} from '../../common/ag-grid/grid-column-ids';

// const helper = new Helper();
// const util = new Utils();

export class AgGridMain {
  getGridRows = (timeout = 15000) =>
    cy.get(`div.ag-center-cols-container div.ag-row`, { timeout });
  getGridHorizontalScrollBar = () =>
    cy.get(`.ag-body-horizontal-scroll-viewport`);
  getSelectedRows = () =>
    cy.get(`div.ag-center-cols-container div.ag-row.ag-row-selected`);

  sideButtons = new SideButtons();
  /**
   * this gets the column action items in ag grid
   * @param actionName uses AlertsPage.columnNames
   * @param isActive if action item is active
   */
  getActionColumnItems(
    actionName: string,
    isActive?: boolean,
    commandOptions?: any
  ) {
    let imgClassName: string;

    if (isActive !== undefined) {
      imgClassName = isActive
        ? `img[class='screening-icon active-icon']`
        : `img[class='screening-icon']`;
    } else {
      imgClassName = 'img';
    }

    return cy.get(`.ag-cell[col-id="${actionName}"] ${imgClassName}`, {
      timeout: 10000,
    });
  }

  /**
   * This method gets the model name of a specfic model from the grid. Can be filtered by Action and Status.
   * @param rowIdx index of model
   * @param actionStatusObj {action: ActionType, actionStatus: boolean }
   * @returns array of model name
   */
  getModelName(
    rowIdx: number,
    actionStatusObj?: { action: ActionType; actionStatus: boolean }
  ) {
    let action: string;
    let actionStatus: boolean;
    if (actionStatusObj) {
      action = actionStatusObj.action;
      actionStatus = actionStatusObj.actionStatus;
    }
    const iconStatus =
      actionStatus === true
        ? `img[class*="active-icon"]`
        : `img:not([class*="active-icon"])`;
    const selector =
      actionStatusObj !== undefined
        ? `div.ag-center-cols-container > div.ag-row > div[col-id="${action}"] ` +
          iconStatus
        : `div.ag-row [col-id="ModelName"]`;
    const modelNameArr: string[] = [];

    cy.get(selector).then(($el) => {
      // cy.get(`div.ag-row [col-id="ModelName"]`).then((el) => {
      let resArray: any[];
      if (actionStatusObj !== undefined) {
        resArray = Array.from(
          $el.parent().parent().parent().find(`[col-id="ModelName"]`)
        );
      } else {
        resArray = Array.from($el);
      }
      modelNameArr.push(resArray[rowIdx].textContent);
    });
    return modelNameArr;
  }

  getCellRowAliasByTextContent(
    cellContent: string,
    columnId: AlertsColumnID | HistoryColumnID | ModelConfigColumnID
  ) {
    cy.get(`div.ag-row [col-id="${columnId}"]`)
      .contains(cellContent)
      .parent()
      .as('cellRow');
  }

  /**
   * This method grabs a cell element based on text to search
   * @param textContent text to search
   * @param columnName columnName of text to search
   * @param cellColumnNameTarget ColumnID of target cell
   * @returns
   */
  getCellByTextContent(
    textContent: string,
    columnName: AlertsColumnID | HistoryColumnID | ModelConfigColumnID,
    cellColumnNameTarget: AlertsColumnID | HistoryColumnID | ModelConfigColumnID
  ) {
    this.getCellRowAliasByTextContent(textContent, columnName);
    return cy.get('@cellRow').find(`[col-id="${cellColumnNameTarget}"]`);
  }

  /**
   *
   * @param colId AlertsColumnID | IssuesColumnID
   * @param rowLevel If using groups, rowLevel is the heirarchy of groups you have. Starts with 0.
   * 0 = no groups or first group row level.
   * 1 = row below 1st group (0 index)  shown when expanded
   * 2 = row below 2nd group (1 index)  shown when 1st group is expanded
   * @returns [String] - Cell content in an array
   */
  getItemsByColumnName(
    colId:
      | AlertsColumnID
      | IssuesColumnID
      | ModelConfigColumnID
      | ModelConfigTagColumnID,
    rowLevel?: number,
    skipEmpty = true
  ) {
    const selector =
      rowLevel !== undefined
        ? `[role="row"].ag-row-level-${rowLevel} [role="gridcell"][col-id="${colId}"]`
        : `[role="gridcell"][col-id="${colId}"]`;
    const items = [];
    cy.get(selector).each(($el) => {
      if (skipEmpty && $el.text().trim().length > 0) {
        items.push($el.text());
      }
    });
    return cy.wrap(items);
  }

  /**
   *
   * @param groupLevel (Index starts at 0) Group level 0 for 1st group. 1 for next sub group, etc. if blank then it will return all groups
   * @returns row group elements
   */
  getRowGroups(groupLevel = undefined) {
    const selector =
      groupLevel !== undefined
        ? `[role="rowgroup"] [role="row"].ag-row-level-${groupLevel} .ag-row-group`
        : `[role="rowgroup"] [role="row"] .ag-row-group`;
    return cy.get(selector);
  }

  getGroupNames() {
    const items = [];
    this.getRowGroups()
      .find('.ag-group-value')
      .each(($el) => {
        items.push($el.text());
      });
    return cy.wrap(items);
  }
  /**
   * This will get the name and count (items inside the group)
   * @param level (Index starts at 0) Group level 0 for 1st group. 1 for next sub group, etc. if blank then it will return all groups
   * @returns { name : string, itemCount: number }
   */
  getRowGroupDetails(level = undefined) {
    const items = [];
    const groupName = [];
    const count = [];
    this.getRowGroups(level)
      .find(`.ag-group-value`)
      .each(($el) => {
        groupName.push($el.text());
      })
      .siblings(`.ag-group-child-count`)
      .each(($el) => {
        count.push($el.text());
      });
    cy.wrap(groupName).then((g) => {
      g.forEach((g, idx) => {
        const trimCount = count[idx].slice(1, count[idx].indexOf(')'));
        items.push({ name: g, itemCount: Number(trimCount) });
      });
    });

    return cy.wrap(items);
  }

  expandGroupByName(groupName: string) {
    this.getRowGroups()
      .contains(groupName)
      .siblings('.ag-group-contracted')
      .click();
  }

  getGroupMembersByLevel(level: number) {
    // [role="rowgroup"] .ag-row.ag-row-level-1.ag-row-group
    return cy.get(`[role="rowgroup"] .ag-row.ag-row-level-${level}`);
  }

  /**
   * This will return the items under a group. NOTE: Group needs to be expanded first
   * @param level (Index starts at 0) Group level. 0 for 1st group. 1 for next sub group, etc
   * @param colId AlertsColumnID
   * @returns string[] of the items
   */
  getColumnItemsByGroupLevel(
    level: number,
    colId: AlertsColumnID | IssuesColumnID | ModelConfigColumnID
  ) {
    const items = [];
    this.getGroupMembersByLevel(level)
      .find(`[col-id="${colId}"]`)
      .each(($el) => {
        items.push($el.text());
      });
    return cy.wrap(items);
  }

  /**
   * Returns the elements by Column Name
   * @param columnName ColumnName of the Ag Grid
   * @returns [Elements] of the column. Can be accessed by returned.eq(idx)
   */
  getColumElementsByColumName(
    columnName:
      | AlertsColumnID
      | HistoryColumnID
      | TagListColumnID
      | ModelConfigColumnID
  ) {
    return cy.get(
      `[class="ag-center-cols-container"] [col-id="${columnName}"]`
    );
  }

  clickGridItemByRow(rowIdx: number) {
    this.getGridRows().eq(rowIdx).click({ force: true });
    cy.wait(500);
  }

  /**
   *
   * @param modelName
   * @param columnID
   * @returns
   */
  getCellActionIconByModelName(
    modelName: string,
    columnID: AlertsColumnID,
    app = 'alerts'
  ) {
    // this.getCellRowAlias(modelName);
    const grid = app === 'alerts' ? 'screening' : 'grid';
    this.getCellRowAliasByTextContent(modelName, 'ModelName');
    return cy
      .get('@cellRow')
      .find(`[col-id="${columnID}"] atx-${grid}-actions-renderer img`);
  }

  getCellItemByColumnName(
    columnName:
      | ModelConfigSlopeAnalysisColumnID
      | ModelConfigAnomaliesColumnID
      | ModelConfigForecastColumnID,
    index = 0
  ) {
    return cy.get(`.ag-body-viewport [col-id="${columnName}"]`).eq(index);
  }
}
