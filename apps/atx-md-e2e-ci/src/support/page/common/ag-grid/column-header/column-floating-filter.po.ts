export class FloatingFilter {
  getFloatingFilterRow() {
    return cy.get(`.ag-header-row-floating-filter`);
  }

  inputFilterValue(value: string, columnName: string) {
    cy.get(`.ag-floating-filter-body [aria-label="${columnName} Filter Input"]`)
      .clear()
      .type(value);
  }
}
