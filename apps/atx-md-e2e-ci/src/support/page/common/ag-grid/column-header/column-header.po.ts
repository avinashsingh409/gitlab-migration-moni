import {
  AlertsColumnID,
  IssuesColumnID,
  ModelConfigColumnID,
} from '../grid-column-ids';

export class ColumnHeader {
  /**
   * This will return the aliasName which you can use as reference for the column header
   * and use it as cy.get('@aliasName')
   * @param columnID column ID of the desired column
   * @param aliasName alias name used for reference
   */
  getColumnHeaderElemAlias(
    columnID: AlertsColumnID | IssuesColumnID | ModelConfigColumnID,
    aliasName: string
  ) {
    cy.get(`[col-id="${columnID}"] .ag-header-cell-label`)
      .parent()
      .as(`${aliasName}`);
  }

  getColumnHeaderElement(
    columnID: AlertsColumnID | IssuesColumnID | ModelConfigColumnID
  ) {
    return cy.get(`[col-id="${columnID}"] .ag-header-cell-label`).parent();
  }

  /**
   * pins a column in a direction. Given the Menu options for the column is already option.
   * @param direction 'Left' or 'Right
   */
  pinColumn(direction: string) {
    cy.get(`[class="ag-menu-option-part ag-menu-option-text"]`)
      .contains('Pin Column')
      .click();
    cy.wait(500);
    cy.get(`[class="ag-menu-option-part ag-menu-option-text"]`)
      .contains('Pin Column')
      .click();
    cy.get(`[class="ag-menu-option-part ag-menu-option-text"]`)
      .contains(`Pin ${direction}`)
      .click();
  }

  openColumnMenu(
    columnID: AlertsColumnID | IssuesColumnID | ModelConfigColumnID
  ) {
    this.getColumnHeaderElement(columnID).find(`[ref="eMenu"]`).click();
  }

  getPinColumnCount(direction: 'left' | 'right') {
    return Cypress.$(`.ag-pinned-${direction}-header [role="columnheader"]`)
      .length;
  }

  getColumnHeaderParentDiv(
    colId: AlertsColumnID | IssuesColumnID | ModelConfigColumnID
  ) {
    return cy.get(`.ag-header-cell[col-id="${colId}"]`);
  }

  groupBy(columnId: AlertsColumnID | IssuesColumnID | ModelConfigColumnID) {
    this.openColumnMenu(columnId);
    return cy.get(`.ag-menu-option`).contains('Group by').click();
  }
}
