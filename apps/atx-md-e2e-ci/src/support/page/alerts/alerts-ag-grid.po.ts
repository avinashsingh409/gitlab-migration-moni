import { AgGridMain } from '../common/ag-grid/ag-grid-main.po';
import { AlertsColumnID } from '../common/ag-grid/grid-column-ids';

export class AlertsGrid extends AgGridMain {
  /**
   * Returns the cell element of Alerts Action by ModelName
   * @param modelName Modelname of selected model in the grid
   * @param columnID AlertsColumnID type
   * @returns This returns the cell element img of the action
   */
  getCellActionIconByModelName(modelName: string, columnID: AlertsColumnID) {
    // this.getCellRowAlias(modelName);
    this.getCellRowAliasByTextContent(modelName, 'ModelName');
    return cy
      .get('@cellRow')
      .find(`[col-id="${columnID}"] atx-screening-actions-renderer img`);
  }

  getCellByModelName(modelName: string, columnID: AlertsColumnID) {
    this.getCellRowAliasByTextContent(modelName, columnID);
    return cy.get('@cellRow').find(`[col-id="${columnID}"]`);
  }
}
