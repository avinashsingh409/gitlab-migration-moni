import { trim } from 'lodash';
import { HeaderPanel } from '../common/ag-grid/header-panel.po';

export class AlertsGridHeader extends HeaderPanel {
  getShowHideFlyoutBtn() {
    return cy.get(`[data-cy="showHideFlyOutWindow"]`);
  }

  getTotalModelCount() {
    return cy.get(`[data-cy="totalModels"]`);
  }
}
