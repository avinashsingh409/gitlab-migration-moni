import { AgGridMain } from '../../common/ag-grid/ag-grid-main.po';
import { HistoryColumnID } from '../../common/ag-grid/grid-column-ids';

export class AlertsHistoryGrid extends AgGridMain {
  getHistoryItemByIndex(index: number) {
    return cy
      .get(
        `atx-model-tabs atx-model-history [role="grid"] div[class="ag-center-cols-container"] [role="row"]`
      )
      .eq(index);
  }

  getLatestHistoryItem() {
    const historyObj: any[] = [];
    this.getHistoryItemByIndex(0).within(() => {
      const columnIDs: HistoryColumnID[] = [
        'Favorite',
        'History',
        'ChangeDate',
        'Executor',
        'Note',
      ];
      columnIDs.forEach((col) => {
        cy.get(`[col-id="${col}"]`).then(($el) => {
          historyObj.push({ [col]: $el.text() });
        });
      });
    });
    return historyObj;
  }
}
