// import { Helper } from '../../helpers/helper';
import * as agGrid from '../common/ag-grid/ag-grid';
import { AlertsFlyOutWindow } from './alerts-flyout-window.po';
// import { AlertsFlyoutWindow } from './alerts-flyout-window.po';
import { alertsColumns } from './alerts-columns';
import { ActionType } from '../../helpers/user-data';
import { AlertsGrid } from './alerts-ag-grid.po';
import { HistoryTab } from './model-tabs/history-tab.po';
import { AlertsGridHeader } from './alerts-grid-header.po';

// import { HistoryTab } from './alerts-history-tab.po';
// const helper = new Helper();

export class Alerts {
  columnNames = {
    diagnose: 'ActionDiagnose',
    alert: 'Alert',
    watch: 'ActionWatch',
    maintenance: 'ActionMaintenance',
    issues: 'Issues',
    unit: 'UnitAbbrev',
  };

  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new AlertsGrid();
  flyoutWindow = new AlertsFlyOutWindow();
  historyTab = new HistoryTab();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridHeadPanel = new AlertsGridHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();

  getExpandScreenBtn = () => cy.get(`[data-cy="expandScreeningView"]`);
  getExpandModelContentBtn = () => cy.get(`[data-cy="expandModelContentView"]`);

  getDefaultColumns = () => alertsColumns;

  expandView = (view: 'ScreeningView' | 'ModelContentView') => {
    switch (view) {
      case 'ScreeningView':
        this.getExpandScreenBtn().click('center');
        break;
      case 'ModelContentView':
        this.getExpandModelContentBtn().click('center');
        break;
      default:
        break;
    }
  };

  /**
   * This validation method will check the status of the model.
   * @param modelName model name of selected Model to validation
   * @param actionType ActionType = 'ActionDiagnose' | 'Alerts' | 'ActionWatch' | 'ActionMaintenance'
   * @param toExpect action status to expect
   */
  checkModelStatus = (
    modelName: string,
    actionType: ActionType,
    toExpect: boolean
  ) => {
    cy.get(`div.ag-row [col-id="ModelName"]`)
      .contains(modelName)
      .siblings(`[col-id="${actionType}"]`)
      // .find(`[title="Diagnose"]`)
      .find(`img[title]`)
      .then(($el) => {
        expect($el.attr('class').includes('active-icon')).eq(toExpect);
        // activeStatus.push($el.attr('class').includes('active-icon'));
      });
  };
}
