export class DataExplorerHome {
  getChartTitle = () => cy.get(`[data-cy="chartTitle"]`);
  chartList = () => cy.get(`.chart-list-container`);
  getCreateNewChartBtn = () => cy.get(`[data-cy="createNewChartBtn"]`);
  getNewSeriesChart = () => cy.get(`[data-cy="newSeriesChart"]`);
  getNewGroupedSeriesChart = () => cy.get(`[data-cy="newGroupSeriesChart"]`);
  getTabs = () => cy.get(`div.mat-tab-list .mat-tab-label-content`);
  getSaveChartBtn = () => cy.get(`[data-cy="saveChartBtn"]`);
  getMoreOptionsBtn = () => cy.get(`[data-cy="moreOptionsBtn"]`);
  getCopyChartLinkAsShownBtn = () =>
    cy.get(`[data-cy="copyChartLinkAsShownBtn"]`);
  getCopyChartLinkLiveBtn = () => cy.get(`[data-cy="copyChartLinkLiveBtn"]`);
  getDownloadChartBtn = () => cy.get(`[data-cy="downloadChartBtn"]`);
  getDeleteChartBtn = () => cy.get(`[data-cy="deleteChartBtn"]`);
  getChartNames = () => cy.get(`[data-cy="chartListNames"]`);
  getChartNameFld = () => cy.get(`[data-cy="chartNameFld"]`);
  getSavePopupSaveBtn = () =>
    cy.get(`atx-save-modal .mat-dialog-actions button`).eq(1);
  getSavePopupCancelBtn = () =>
    cy.get(`atx-save-modal .mat-dialog-actions button`).eq(0);
  getChartTypesList = () => cy.get(`[role=listbox] .mat-option-text`);
  getChartTypesDropDownBtn = () =>
    cy.get(`[data-cy="chartTypeSection"] [data-cy="chartType"] [role]`);
  getAggregationDropDownBtn = () =>
    cy.get(`[data-cy="chartTypeSection"] [data-cy="aggregation"] [role]`);
  getAggregationOptions = () =>
    cy.get(`[data-cy="aggregationOption"] .mat-option-text`);
  getSeriesNameFds = () => cy.get(`[formControlName="seriesName"]`);
  getTagNameFlds = () => cy.get(`[formcontrolname="tagName"]`);
  getAxisUnits = () =>
    cy.get(`[formcontrolname="axisUnits"] .mat-select-value-text span`);
  getColorPicker = () => cy.get(`atx-color-picker div`);

  getTab(
    tabName:
      | 'Series'
      | 'Tags List'
      | 'Axis'
      | 'Static Curves'
      | 'Pins'
      | 'Advanced'
  ) {
    return cy.get(`div.mat-tab-list`).contains(tabName).parent();
  }

  getSeriesTabDetails() {
    const seriesRowDetails = [];

    cy.get(`[formarrayname="series"] [data-cy="seriesRow"]`).each(
      (seriesRow) => {
        const isVisible = !seriesRow
          .find(`[data-cy="showHideBtn"] mat-icon`)
          .text()
          .includes('off');
        const temp = seriesRow.find(`atx-color-picker rect`).attr('style');
        const color = temp.substring(temp.indexOf(':') + 2, temp.length - 1);
        const seriesName = seriesRow
          .find(`[formControlName="seriesName"]`)
          .val();
        const tagName = seriesRow.find(`[formcontrolname="tagName"]`).val();
        const axisUnits = seriesRow
          .find(`[formcontrolname="axisUnits"] .mat-select-value-text span`)
          .text();
        const stackType = seriesRow
          .find(`[formControlName="stackType"] .mat-select-value-text span`)
          .text();
        const isBold = seriesRow
          .find(`[data-cy="isBoldBox"]`)
          .attr('class')
          .includes('checked');
        const xAxis = seriesRow
          .find(`[data-cy="xAxisBox"]`)
          .attr('class')
          .includes('checked');

        seriesRowDetails.push({
          isVisible,
          color,
          seriesName,
          tagName,
          axisUnits,
          stackType,
          isBold,
          xAxis,
        });
      }
    );
    return cy.wrap(seriesRowDetails);
  }

  changeSeriesColor(colorHex: string, seriesIdx = 0) {
    this.getColorPicker().eq(seriesIdx).click();
    cy.get(`.cdk-overlay-container input`)
      .clear()
      .type(colorHex)
      .click()
      .type(`{esc}`);
  }

  clickBoldSeries(seriesIdx = 0) {
    cy.get(`[data-cy="isBoldBox"] input`).eq(seriesIdx).click({ force: true });
  }

  clickShowHideSeries(seriesIdx = 0) {
    cy.get(`[data-cy="showHideBtn"]`).eq(seriesIdx).click();
  }
}
