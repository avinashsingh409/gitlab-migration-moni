import * as agGrid from '../common/ag-grid/ag-grid';

export class Issues {
  sideButtons = new agGrid.SideButtons();
  headerPanel = new agGrid.HeaderPanel();
  agGridMain = new agGrid.AgGridMain();
  agGridColumnHeader = new agGrid.ColumnHeader();
  agGridFloatingFilter = new agGrid.FloatingFilter();
}
