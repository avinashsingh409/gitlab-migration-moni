import { UserObject } from './user-data';

const analyst: UserObject = {
  name: 'Analyst',
  email: 'AtonixOIAnalyst@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['Alerts', 'IM', 'DE', 'Dashboard'],
  utilityAccess: [],
};

const liteAnalyst: UserObject = {
  name: 'Lite Analyst',
  email: 'AtonixOILiteAnalyst@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['Alerts', 'IM', 'DE', 'Dashboard'],
  utilityAccess: [],
};

const staff: UserObject = {
  name: 'Staff',
  email: 'AtonixOIStaff@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['IM', 'DE', 'Dashboard'],
  utilityAccess: [],
};

const admin: UserObject = {
  name: 'Admin',
  email: 'AtoinxOIAdmin@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['Alerts', 'IM', 'DE', 'Dashboard', 'IMAdmin'],
  utilityAccess: [],
};

const explorerAnalyst: UserObject = {
  name: 'Explorer Analyst',
  email: 'AtonixOIExplorerAnalyst@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['DE', 'Dashboard'],
  utilityAccess: [],
};

const insightAnalyst: UserObject = {
  name: 'Insight Analyst',
  email: 'AtonixOIInsightAnalyst@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['DE', 'Dashboard'],
  utilityAccess: [],
};

const resolveCoordinator: UserObject = {
  name: 'Resolve Coordinator',
  email: 'AtonixOIResolveCoordinator@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['IM', 'IMAdmin'],
  utilityAccess: [],
};

const resolveStaff: UserObject = {
  name: 'Resolve Staff',
  email: 'AtonixOIResolveStaff@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: ['IM'],
  utilityAccess: [],
};

const eventsAnalyst: UserObject = {
  name: 'Events Analyst',
  email: 'AtonixOIEventsAnalyst@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: [],
  utilityAccess: [],
};

const alpha: UserObject = {
  name: 'Alpha',
  email: 'AtonixOIAlpha@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: [],
  utilityAccess: [],
};

const beta: UserObject = {
  name: 'Beta',
  email: 'AtonixOIBeta@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: [],
  utilityAccess: [],
};

const discussionManager: UserObject = {
  name: 'Discussion Manager',
  email: 'AtonixOIDiscussionManager@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: [],
  utilityAccess: [],
};

const userAdmin: UserObject = {
  name: 'User Admin',
  email: 'AtonixOIUserAdmin@AtonixTest.com',
  password: 'Passw0rd1!',
  appAccess: [],
  utilityAccess: [],
};

export {
  analyst,
  liteAnalyst,
  staff,
  admin,
  explorerAnalyst,
  insightAnalyst,
  resolveCoordinator,
  resolveStaff,
  eventsAnalyst,
  alpha,
  beta,
  discussionManager,
  userAdmin,
};
