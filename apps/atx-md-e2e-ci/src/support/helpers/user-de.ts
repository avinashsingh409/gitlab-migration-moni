import { User } from './user';
import { Utils } from './utils';
import { ApiStub } from './stub/api-stub';
import * as Pages from '../page/pages';

const dataExplorer = new Pages.DataExplorerHome();
const util = new Utils();
const apiSpy = new ApiStub();

export class DataExplorerUser extends User {
  saveChart(newChartName?: string) {
    apiSpy.TagsDataFiltered();
    dataExplorer.getSaveChartBtn().click();
    if (newChartName !== undefined)
      dataExplorer.getChartNameFld().clear().type(newChartName);
    dataExplorer.getSavePopupSaveBtn().click();
    cy.wait('@TagsDataFiltered');
  }

  createNewCreate(chartType: 'Line' | 'Area' | 'Column' | 'Bar' | 'Table') {
    apiSpy.TagsDataFiltered();
    dataExplorer.getCreateNewChartBtn().click();
    dataExplorer.getNewSeriesChart().click();
    dataExplorer.getChartTypesDropDownBtn().click();
    dataExplorer.getChartTypesList().contains(chartType);
    cy.wait('@TagsDataFiltered', { timeout: 30000 });
  }

  openTab(
    tabName:
      | 'Series'
      | 'Tags List'
      | 'Axis'
      | 'Static Curves'
      | 'Pins'
      | 'Advanced'
  ) {
    dataExplorer.getTab(tabName).click();
  }

  inputSeriesName(seriesName: string, idx = 0) {
    dataExplorer.getSeriesNameFds().eq(idx).clear().type(seriesName);
  }

  addTagToChart(tagName: string) {
    //
  }

  changeSeriesColor(colorHex: string, seriesIdx: number) {
    dataExplorer.changeSeriesColor(colorHex, seriesIdx);
  }

  boldSeries(seriesIdx: number) {
    dataExplorer.clickBoldSeries(seriesIdx);
  }

  showHideSeries(seriesIdx: number) {
    dataExplorer.clickShowHideSeries(seriesIdx);
  }
}
