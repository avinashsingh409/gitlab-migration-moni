import { User } from './user';
import { UserAggridCommon } from './user-aggrid-common';
import { IssuesColumnID } from '../page/common/ag-grid/grid-column-ids';
import { Utils } from './utils';

const util = new Utils();
const userAggridCommon = new UserAggridCommon();

export class IssuesUser extends User {
  showPanel(panelName: 'column' | 'filter' | 'view') {
    userAggridCommon.showPanel(panelName);
    cy.wait(400);
    this.moveMouseToFixLayout();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    userAggridCommon.setColumns(colObj);
    this.moveMouseToFixLayout();
  }

  saveView(viewName: string) {
    userAggridCommon.saveView(viewName);
    this.moveMouseToFixLayout();
  }

  moveColumn(columnToMove: IssuesColumnID, targetColumn: IssuesColumnID) {
    userAggridCommon.moveColumn(columnToMove, targetColumn);
    this.moveMouseToFixLayout();
  }

  deleteSavedView(savedViewName: string) {
    userAggridCommon.deleteSavedView(savedViewName);
    this.moveMouseToFixLayout();
  }

  deleteAllSavedViews() {
    userAggridCommon.deleteAllSavedViews();
    this.moveMouseToFixLayout();
  }

  pinColumn(columnID: IssuesColumnID, direction: 'Left' | 'Right') {
    userAggridCommon.pinColumn(columnID, direction);
    this.moveMouseToFixLayout();
  }

  /**
   * this method is to move the mouse to the expand screeningview button
   * to display the screeningview properly due to issue happening only in automation.
   */
  moveMouseToFixLayout() {
    cy.get(`atx-donut-chart`).eq(2).trigger('mousemove');
  }

  restoreGridToDefaults() {
    userAggridCommon.restoreGridToDefaults();
    this.moveMouseToFixLayout();
  }

  selectSavedView(viewName: string) {
    cy.wait(800);
    // util.loadAlertsSummaryForWait('IssuesGrid');
    userAggridCommon.selectSavedView(viewName);
    // cy.wait('@IssuesGrid');
    this.moveMouseToFixLayout();
    cy.wait(800);
  }

  clickFloatingFilterBtn() {
    return userAggridCommon.clickToggleFilterBtn();
  }

  /**
   * used to input values in the Filter header, need to open filter head using toggle filter button
   * @columnName is the actual name of the column in the ag grid
   */
  inputFilterValue(value: string, columnName: string) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('IssuesGrid');
    userAggridCommon.inputFloatingFilterValue(value, columnName);
    this.moveMouseToFixLayout();
    cy.wait('@IssuesGrid');
  }

  sortColumn(colId: IssuesColumnID) {
    util.loadAgGridForWait('IssuesGrid');
    userAggridCommon.clickColumn(colId);
    // cy.wait('@IssuesGrid');
    cy.wait(2000);
    this.moveMouseToFixLayout();
  }

  groupByColumn(colId: IssuesColumnID) {
    userAggridCommon.groupByColumn(colId);
    this.moveMouseToFixLayout();
  }

  expandGroup(groupName: string) {
    userAggridCommon.expandGroup(groupName);
    this.moveMouseToFixLayout();
  }
}
