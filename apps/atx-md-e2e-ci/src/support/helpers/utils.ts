// import { browser, protractor, ElementFinder, ElementArrayFinder } from 'protractor';
// import * as path from 'path';

import moment from 'moment';
import { ApiStub } from './stub/api-stub';
import { GridNames } from './user-data';

const stub = new ApiStub();

export class Utils {
  getCurrentTime = () => {
    return moment().format('MM/DD/YY hh:mmA');
  };

  dragAndDrop(elemToMoveAlias: string, targetElemAlias, btn = 0) {
    cy.get(`@${elemToMoveAlias}`).trigger(`mousedown`, { button: btn });
    cy.get(`@${targetElemAlias}`).trigger('mousemove');
    cy.get(`@${targetElemAlias}`).trigger('mouseup');
  }

  loadUserDetails() {
    cy.intercept('GET', '/Services/api/Authorization/IdentityProvider').as(
      'identityProvider'
    );
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws1.json',
    }).as('authAws1');
    cy.intercept('POST', '*.amazonaws.com/', {
      fixture: 'auth/cognito-aws2.json',
    });
    cy.intercept('GET', '/Services/api/Account/UserAuthenticated', {
      fixture: 'auth/user-authenticated.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/AdvancedPreview', {
      fixture: 'auth/advanced-preview.json',
    });
    cy.intercept('GET', '/Services/api/Authorization/MDApp', {
      fixture: 'auth/mdapp-grant-access.json',
    });
    cy.intercept('GET', '/md/assets/config.json', {
      fixture: 'auth/config.json',
    });
    cy.intercept('GET', '/Services/api/Account/UserPreferences', {
      fixture: 'auth/user-preferences.json',
    });
  }
  stubAPI(user: string, api: string) {
    // cy.intercept();
  }
  loadAssetNav() {
    cy.intercept('GET', '/api/v1/assets/trees/load', {
      fixture: 'asset-nav/load-node-from-key-test1.json',
    }).as('assetNavAlias');
  }
  loadAlertsSummary(jsonPath: string) {
    cy.intercept('POST', '/api/alerts/Summary', (req) => {
      req.body = {
        startRow: 0,
        endRow: 100,
        rowGroupCols: [],
        valueCols: [],
        pivotCols: [],
        pivotMode: false,
        groupKeys: [],
        filterModel: {
          AssetID: {
            filterType: 'text',
            type: 'contains',
            filter:
              'MjdjZWJiNzEtZDY3MC00OTBmLTk2NmYtZDJmZWI1MzBiM2Y1fjVmNWY1Yjc0LWE0YTUtNDViNC05N2M5LTM1M2FmZmFkN2Y5OX4xMjg2NTc=',
          },
          Alert: { values: ['true'], filterType: 'set' },
          ActionWatch: { values: ['false'], filterType: 'set' },
          Ignore: { values: ['false'], filterType: 'set' },
        },
        sortModel: [],
      };
    });
  }

  loadAgGridForWait(gridName: GridNames) {
    switch (gridName) {
      case 'AlertsGrid':
        cy.intercept('POST', '/api/alerts/Summary').as('AlertsGrid');
        break;
      case 'IssuesGrid':
        cy.intercept('POST', '/api/Issues/IssuesGrid').as('IssuesGrid');
        break;
      case 'TagGrid':
        cy.intercept('POST', '/api/v1/gridprocessdata/TagGrid').as('TagGrid');
        break;
      case 'HistoryGrid':
        cy.intercept('POST', '/Services/api/Monitoring/AlertTimeline').as(
          'HistoryGrid'
        );
        break;
      default:
        break;
    }
  }

  modelWait(idx: number | string) {
    const model1Fixture = idx ? `/model-config/model-${idx}/` : undefined;
    stub.ModelSummary(`${model1Fixture}ModelSummary`);
    stub.config(`${model1Fixture}config`);
    stub.mlmodellatesttype(`${model1Fixture}mlmodellatesttype`);
    stub.ModelTrend(`${model1Fixture}ModelTrend`);
  }

  getModelConfig(idx: number | string) {
    cy.fixture(`/model-config/model-${idx}/config.json`).as(
      'modelConfigDetails'
    );
  }

  // refreshPageStub() {
  //   cy.reload();
  //   // stub.assetAccessRights();
  //   // stub.loadUserDetails();
  //   stub.refreshBrowserAuth();
  //   stub.loadUserDetails2();
  //   // cy.wait(`@configRefresh`);
  //   cy.wait(`@cognitoSettingsRefresh`);
  // }
}
