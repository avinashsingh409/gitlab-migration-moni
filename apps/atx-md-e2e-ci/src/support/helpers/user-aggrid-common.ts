import { group } from 'console';
import {
  AlertsColumnID,
  IssuesColumnID,
  ModelConfigColumnID,
} from '../page/common/ag-grid/grid-column-ids';
import * as Pages from '../page/pages';
import { Utils } from './utils';

const alertsPage = new Pages.Alerts();
const sidePanelColumn = alertsPage.sideButtons.column;
const sidePanelViews = alertsPage.sideButtons.view;
const agGridColumnHeader = alertsPage.agGridColumnHeader;
const agGridFloatingFilter = alertsPage.agGridFloatingFilter;
const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const utils = new Utils();
const agGridMain = new Pages.AgGridMain();

export class UserAggridCommon {
  showPanel(panelName: 'column' | 'filter' | 'view') {
    switch (panelName) {
      case 'column':
        // alertsPage.sideButtons.column.showPanel();
        alertsPage.agGridMain.sideButtons.column.showPanel();
        break;
      case 'filter':
        break;
      case 'view':
        // alertsPage.sideButtons.save.showPanel();
        alertsPage.agGridMain.sideButtons.view.showPanel();
        break;
      default:
        break;
    }
    cy.wait(1500);
  }

  pinColumn(
    columnID: AlertsColumnID | IssuesColumnID | ModelConfigColumnID,
    direction: 'Left' | 'Right'
  ) {
    // agGridColumnHeader.getColumnHeaderElemAlias(columnID, columnAlias);
    // cy.get('@noteColumnAlias').find(`[ref="eMenu"]`).click();
    agGridColumnHeader.openColumnMenu(columnID);
    agGridColumnHeader.pinColumn(direction);
    cy.wait(3000);
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    sidePanelColumn.clickCheckbox(colObj.toShow, 'Show');
    sidePanelColumn.clickCheckbox(colObj.toHide, 'Hide');
  }

  moveColumn(
    columnToMove: AlertsColumnID | IssuesColumnID | ModelConfigColumnID,
    targetColumn: AlertsColumnID | IssuesColumnID | ModelConfigColumnID
  ) {
    agGridColumnHeader.getColumnHeaderElemAlias(columnToMove, 'columnToMove');
    agGridColumnHeader.getColumnHeaderElemAlias(targetColumn, 'targetColumn');

    utils.dragAndDrop('columnToMove', 'targetColumn');
  }

  restoreGridToDefaults() {
    sidePanelViews.clickRestoreToDefaultBtn();
  }

  saveView(viewName: string) {
    sidePanelViews.saveView(viewName);
    cy.wait(2000);
  }

  deleteAllSavedViews() {
    cy.get(`atx-saved-tool-panel`).then(() => {
      let savedListsCount = Cypress.$(`[data-cy="savedViewsListItem"]`).length;

      if (savedListsCount > 0) {
        cy.intercept('DELETE', '**/ListConfig?id=*').as('saveListApi');
        cy.get(`[data-cy="deleteViewBtn"]`).each(($el) => {
          cy.get(`[data-cy="deleteViewBtn"]`).eq(0).click();
          cy.on('window:confirm', () => true);
          cy.wait('@saveListApi', { timeout: 5000 });
        });
        cy.wait(2000);
        savedListsCount = Cypress.$(`[data-cy="savedViewsListItem"]`).length;
      }
    });
  }

  deleteSavedView(savedViewName: string) {
    cy.intercept('DELETE', '**/ListConfig?id=*').as('saveListApi');
    cy.get(`[class="views-panel"]`)
      .contains(savedViewName)
      .siblings(`.filterLoadDelete`)
      .click();
    cy.wait('@saveListApi', { timeout: 5000 });
  }

  selectSavedView(viewName: string) {
    cy.get(`[data-cy="savedViewsListItem"]`)
      .contains(viewName)
      .siblings('[data-cy="checkbox"]')
      .click();
  }

  clickToggleFilterBtn() {
    return agGridHeaderPanel.clickToggleFloatingFilterBtn();
  }

  inputFloatingFilterValue(value: string, columnName: string) {
    agGridFloatingFilter.inputFilterValue(value, columnName);
  }

  clickColumn(colId: AlertsColumnID | IssuesColumnID | ModelConfigColumnID) {
    return agGridColumnHeader.getColumnHeaderParentDiv(colId).click();
  }

  groupByColumn(colId: AlertsColumnID | IssuesColumnID | ModelConfigColumnID) {
    return agGridColumnHeader.groupBy(colId);
  }

  expandGroup(groupName: string) {
    return alertsPage.agGridMain.expandGroupByName(groupName);
  }

  selectGridItemByRowIndex(rowIdx) {
    agGridMain.clickGridItemByRow(rowIdx);
  }

  multiSelectGridItemByRowIndex(rowIdx) {
    cy.get(`body`).type('{ctrl}', { release: false });
    this.selectGridItemByRowIndex(rowIdx);
  }

  // doubleClickItemInGridByIdx(idx: number) {
  //   const gridItemDetails = [];
  //   agGridMain
  //     .getGridRows()
  //     .eq(2)
  //     .then(($el) => {
  //       cy.wrap($el)
  //         .find('[col-id]')
  //         .then((col) => {
  //           // tagName = col.eq(2).text().trim();
  //           // unit = col.eq(4).text().trim();
  //           // console.log(`XXX: `, col.text());
  //           gridItemDetails.push(col.text().trim());
  //         })
  //         .dblclick();
  //     });
  //   cy.wait(2000);
  //   return cy.wrap(gridItemDetails);
  // }
  doubleClickItemInGridByIdx(idx: number) {
    const gridItemDetails = [];
    agGridMain
      .getGridRows()
      .eq(2)
      .then(($el) => {
        cy.wrap($el)
          .find('[col-id]')
          .then((col) => {
            col.each((item) => {
              gridItemDetails.push(col.eq(item).text().trim());
            });
            // tagName = col.eq(2).text().trim();
            // unit = col.eq(4).text().trim();
            // console.log(`XXX: `, col.length);
            // gridItemDetails.push(col.text().trim());
          })
          .dblclick();
      });
    cy.wait(2000);
    return cy.wrap(gridItemDetails);
  }

  doubleClickCellByColumnName(columnName: string, index = 0) {
    cy.get(`.ag-body-viewport [col-id="${columnName}"]`).eq(index).dblclick();
  }

  clickCellByColumnName(columnName: string, index = 0) {
    return cy
      .get(`.ag-body-viewport [col-id="${columnName}"]`)
      .eq(index)
      .click();
  }

  selectFromList(value: string, columnName?: string, index = 0) {
    if (!columnName) {
      cy.get(`.ag-list .ag-list-item`).contains(value).click();
    } else {
      this.clickCellByColumnName(columnName, index).type('{enter}').wait(300);
      cy.get(`.ag-list .ag-list-item`).contains(value).click({ force: true });
    }
  }
}
