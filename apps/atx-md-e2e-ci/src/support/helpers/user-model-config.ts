import { User } from './user';
import { UserAggridCommon } from './user-aggrid-common';
import * as Pages from '../page/pages';
import {
  ModelConfigColumnID,
  ModelConfigForecastColumnID,
  ModelConfigAnomaliesColumnID,
  ModelConfigSlopeAnalysisColumnID,
} from '../page/common/ag-grid/grid-column-ids';
import { Utils } from './utils';
import { FlyoutActionButtons } from './user-data';

const modelConfigPage = new Pages.ModelConfig();
const modelConfigEdit = new Pages.ModelEdit();
const sidePanelColumn = modelConfigPage.sideButtons.column;
const sidePanelViews = modelConfigPage.sideButtons.view;
// const flyoutWindow = modelConfigPage.flyoutWindow;
const agGridColumnHeader = modelConfigPage.agGridColumnHeader;
// const agGridHeaderPanel = modelConfigPage.agGridHeadPanel;
const util = new Utils();
const userAggridCommon = new UserAggridCommon();
const flyoutWindow = modelConfigPage.flyoutWindow;

export class ModelConfigUser extends User {
  showSideColumnPane() {
    modelConfigPage.sideButtons.column.showPanel();
  }

  showPanel(panelName: 'column' | 'filter' | 'view') {
    userAggridCommon.showPanel(panelName);
    // this.moveMouseToFixLayout();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    userAggridCommon.setColumns(colObj);
  }

  saveView(viewName: string) {
    userAggridCommon.saveView(viewName);
    this.moveMouseToFixLayout();
  }

  moveColumn(
    columnToMove: ModelConfigColumnID,
    targetColumn: ModelConfigColumnID
  ) {
    userAggridCommon.moveColumn(columnToMove, targetColumn);
  }

  deleteSavedView(savedViewName: string) {
    userAggridCommon.deleteSavedView(savedViewName);
  }

  deleteAllSavedViews() {
    userAggridCommon.deleteAllSavedViews();
    this.moveMouseToFixLayout();
  }

  pinColumn(columnID: ModelConfigColumnID, direction: 'Left' | 'Right') {
    userAggridCommon.pinColumn(columnID, direction);
    // this.moveMouseToFixLayout();
  }

  /**
   * this method is to move the mouse to the expand screeningview button
   * to display the screeningview properly due to issue happening only in automation.
   */
  moveMouseToFixLayout() {
    return cy.get(`atx-donut-chart`).eq(2).trigger('mousemove');
  }

  restoreGridToDefaults() {
    userAggridCommon.restoreGridToDefaults();
    // this.moveMouseToFixLayout();
  }

  selectSavedView(viewName: string) {
    userAggridCommon.selectSavedView(viewName);
    cy.wait(500);
    // this.moveMouseToFixLayout();
  }

  clickFloatingFilterBtn() {
    return userAggridCommon.clickToggleFilterBtn();
  }

  /**
   * used to input values in the Filter header, need to open filter head using toggle filter button
   * @columnName is the actual name of the column in the ag grid
   */
  inputFilterValue(value: string, columnName: string) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.inputFloatingFilterValue(value, columnName);
    cy.wait(1000);
    // this.moveMouseToFixLayout();
  }

  sortColumn(colId: ModelConfigColumnID) {
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.clickColumn(colId);
    cy.wait(1000);
    // this.moveMouseToFixLayout();
  }

  groupByColumn(colId: ModelConfigColumnID) {
    userAggridCommon.groupByColumn(colId);
    // this.moveMouseToFixLayout();
  }

  expandGroup(groupName: string) {
    userAggridCommon.expandGroup(groupName);
    // this.moveMouseToFixLayout();
  }

  openTab(tabName: 'ModelList' | 'TagList') {
    if (tabName === 'ModelList') modelConfigPage.getModelListBtn().click();
    else modelConfigPage.getTagListBtn().click();
  }

  selectGridItemByRowIndex(rowIdx) {
    userAggridCommon.selectGridItemByRowIndex(rowIdx);
  }

  multiSelectGridItemByRowIndex(rowIdx) {
    userAggridCommon.multiSelectGridItemByRowIndex(rowIdx);
  }

  selectActionBtnFlyout(actionType: FlyoutActionButtons) {
    flyoutWindow.selectActionBtn(actionType);
  }

  selectModelByName(modelName: string) {
    cy.get(`div.ag-row [col-id="ModelName"]`).contains(modelName).click();
  }

  saveNote() {
    cy.get(`#alertsAction form`).contains('Save').click();
  }

  doubleClickCellByColumnName(columnName: string, index = 0) {
    userAggridCommon.doubleClickCellByColumnName(columnName, index);
  }

  clickCellByColumnName(
    columnName:
      | ModelConfigSlopeAnalysisColumnID
      | ModelConfigAnomaliesColumnID
      | ModelConfigForecastColumnID,
    index = 0
  ) {
    return userAggridCommon.clickCellByColumnName(columnName, index);
  }

  selectFromList(
    value: string,
    columnName?: ModelConfigAnomaliesColumnID | ModelConfigForecastColumnID,
    index = 0
  ) {
    userAggridCommon.selectFromList(value, columnName, index);
  }

  openConfigurationTab(
    tabName:
      | 'Inputs'
      | 'Training Range'
      | 'Sensitivity'
      | 'Slope Analysis'
      | 'Averages'
      | 'Build Status'
  ) {
    return modelConfigEdit.openConfigurationTab(tabName);
  }

  selectAnomaliesFilter(
    filter:
      | 'All'
      | 'Model Bound Criticality'
      | 'Relative Model Bounds'
      | 'Fixed Limits'
  ) {
    modelConfigEdit.selectAnomaliesFilter(filter);
  }
}
