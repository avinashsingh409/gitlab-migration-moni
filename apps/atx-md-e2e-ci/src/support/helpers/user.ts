import { AppNamesNewUI, NavType, envToTest } from '../helpers/user-data';
import * as Pages from '../page/pages';

const loginPage = new Pages.Login();
const landingPage = new Pages.LandingPage();
const assetNavigator = new Pages.AssetNavigator();
const navigation = new Pages.Navigation();

export class User {
  visit() {
    cy.visit(envToTest);
  }

  logInCiCD(userObj: { email: string; password: string }) {
    loginPage.inputUserEmail(userObj.email);
    loginPage.clickSignInBtn(); // next button
    // cy.wait('@identityProvider');
    loginPage.inputUserPassword(userObj.password);
    loginPage.clickSignInBtn();
    // cy.wait('@usepreferences-default');
  }

  navigateToApp(appName: AppNamesNewUI, navigateUsing: NavType) {
    // const appToNavigate = navigateUsing === NavType.tile ?
    //   landingPage.appTiles(appName) : null;
    if (navigateUsing === NavType.tile) {
      landingPage.navigateToApp(appName);
    } else {
      navigation.navigateToApp(appName);
    }
    // cy.wait(2000);
  }

  selectAssetFromNavigator(assetToSelect: {
    parent: string;
    children?: string[];
  }) {
    assetNavigator.selectParent(assetToSelect);
    if (
      assetNavigator.selectChild !== undefined ||
      assetToSelect.children?.length !== 0
    ) {
      assetNavigator.selectChild(assetToSelect.children);
    }
  }

  showHideAssetNavigator() {
    navigation.navigationPane.clickAssetNavigatorBtn();
  }

  refreshAssetNav() {
    assetNavigator.refreshAssetNav();
  }

  logOut() {
    landingPage.openUserSettings();
    landingPage.getLogOutBtn().click();
  }

  // dragAndDrop(elemToDropAlias: string, targetElemAlias) {}
}
