import * as path from 'path';

const localUrl = path.join(process.cwd(), './apps/atx-md/src/environments/');
const userObj = {
  default: {
    email: 'testautomationuser1@atonixtest.com',
    password: 'Passw0rd1!',
  },
  accessDeniedUser: {
    email: 'nino@test.com',
    password: 'Passw0rd1!',
  },
  testAccount1: {
    email: 'nino2@test.com',
    password: 'Passw0rd1!',
  },
};

// const defaultUrl = 'https://d2dm3odpcgy2ww.cloudfront.net/';
const defaultUrl = 'https://test.atonix.com/MD';
const url = {
  test: defaultUrl,
  local: 'http://localhost:4200/',
  dev: 'https://dev.atonix.com/MD',
  stage: 'https://stage.atonix.com/MD',
};

const envToTest =
  Cypress.env().env !== undefined
    ? Cypress.env(Cypress.env().env)
    : Cypress.env().local;

const getTestRigUrl = (testRigName: string) => {
  const urls = {
    alerts: 'alerts/index.html',
    issues: 'work-management/index.html',
  };

  return `${defaultUrl}${urls[testRigName]}`;
};

enum AppNamesNewUI {
  alerts = 'Alerts',
  issues = 'IM',
  dataExplorer = 'DE',
  dashboards = 'Dashboard',
}

enum NavType {
  tile,
  navBar,
}

type ActionType =
  | 'ActionDiagnose'
  | 'Alert'
  | 'ActionWatch'
  | 'ActionMaintenance';

type FlyoutActionButtons =
  | 'Diagnose'
  | 'Model Maintenance'
  | 'Clear Diagnose'
  | 'Clear Maintenance'
  | 'Add Note'
  | '6 Hours'
  | '24 Hours'
  | '7 Days'
  | 'Custom'
  | 'Clear Status';

type GridNames = 'AlertsGrid' | 'IssuesGrid' | 'TagGrid' | 'HistoryGrid';

type UserAppAccess =
  | 'Alerts'
  | 'IM'
  | 'IMAdmin'
  | 'IMDiscussion'
  | 'DE'
  | 'Dashboard'
  | 'DashboardView'
  | 'DashboardEdit';

type UserUtilityAccess =
  | 'UserAdmin'
  | 'AssetConfig'
  | 'TagConfig'
  | 'ModelConfig'
  | 'OPModeConfig'
  | 'PinConfig'
  | 'Events';

interface UserObject {
  name?: string;
  email: string;
  password: string;
  appAccess: UserAppAccess[];
  utilityAccess: UserUtilityAccess[];
}

const assetToTest = {
  dev: {
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Unit'],
    },
  },
  test: {
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: [
        'Live MD Test Group',
        'Live MD Test Station',
        'Live MD Test Unit',
      ],
    },
  },
  stage: {
    alerts: {
      parent: 'nD Test Client',
      children: ['Stage', 'Stage Station', 'New Regression Unit'],
    },
    im: {
      parent: 'Asset Health Monitoring',
      children: ['Test Automation Parent (Dont Touch)'],
    },
    de: {
      parent: 'Demo Clients',
      children: ['Coal Plants', 'Eastern Station', 'Eastern PC1'],
    },
    modelConfig: {
      parent: 'nD Test Client',
      children: ['Stage', 'Stage Station', 'New Regression Unit'],
    },
  },
  undefined: {
    //localhost
    alerts: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group'],
    },
    im: { parent: 'Asset Health Monitoring', children: ['Live MD Test Group'] },
    de: {
      parent: 'Asset Health Monitoring',
      children: ['Live MD Test Group', 'Live MD Test Station'],
    },
    modelConfig: {
      parent: 'Asset Health Monitoring',
      children: [
        'Live MD Test Group',
        'Live MD Test Station',
        'Live MD Test Unit',
      ],
    },
  },
};

export {
  userObj,
  url,
  envToTest,
  AppNamesNewUI,
  NavType,
  getTestRigUrl,
  ActionType,
  FlyoutActionButtons,
  GridNames,
  UserAppAccess,
  UserUtilityAccess,
  UserObject,
  assetToTest,
};
