import { User } from './user';
import { UserAggridCommon } from './user-aggrid-common';
import * as Pages from '../page/pages';
import { FlyoutActionButtons } from './user-data';
import { AlertsColumnID } from '../page/common/ag-grid/grid-column-ids';
import { Utils } from './utils';

const alertsPage = new Pages.Alerts();
const sidePanelColumn = alertsPage.sideButtons.column;
const sidePanelViews = alertsPage.sideButtons.view;
const flyoutWindow = alertsPage.flyoutWindow;
const agGridColumnHeader = alertsPage.agGridColumnHeader;
const agGridHeaderPanel = alertsPage.agGridHeadPanel;
const util = new Utils();
const userAggridCommon = new UserAggridCommon();

export class AlertsUser extends User {
  expandView(view: 'ScreeningView' | 'ModelContentView') {
    alertsPage.expandView(view);
  }

  showSideColumnPane() {
    alertsPage.sideButtons.column.showPanel();
  }

  showPanel(panelName: 'column' | 'filter' | 'view') {
    userAggridCommon.showPanel(panelName);
    this.moveMouseExpandScreeningView();
  }

  setColumns(colObj: { toShow?: string[]; toHide?: string[] }) {
    userAggridCommon.setColumns(colObj);
  }

  selectModelByName(modelName: string) {
    return cy
      .get(`div.ag-row [col-id="ModelName"]`)
      .contains(modelName)
      .click();
  }

  selectActionBtnFlyout(actionType: FlyoutActionButtons) {
    flyoutWindow.selectActionBtn(actionType);
  }

  saveAction() {
    flyoutWindow.clickSaveButton();
  }

  saveNote() {
    cy.get(`#alertsAction form`).contains('Save').click();
  }

  openContentTab(tabName: 'Trend' | 'History' | 'Context' | 'Actions') {
    return cy.get(`[data-cy="${tabName.toLowerCase()}TabBtn"]`).click();
  }

  saveView(viewName: string) {
    userAggridCommon.saveView(viewName);
    this.moveMouseExpandScreeningView();
  }

  moveColumn(columnToMove: AlertsColumnID, targetColumn: AlertsColumnID) {
    userAggridCommon.moveColumn(columnToMove, targetColumn);
  }

  showHideFlyoutWindow() {
    agGridHeaderPanel.getShowHideFlyoutBtn().click();
    cy.wait(2000);
  }

  deleteSavedView(savedViewName: string) {
    userAggridCommon.deleteSavedView(savedViewName);
  }

  deleteAllSavedViews() {
    userAggridCommon.deleteAllSavedViews();
    this.moveMouseExpandScreeningView();
  }

  pinColumn(columnID: AlertsColumnID, direction: 'Left' | 'Right') {
    userAggridCommon.pinColumn(columnID, direction);
    this.moveMouseExpandScreeningView();
  }

  /**
   * this method is to move the mouse to the expand screeningview button
   * to display the screeningview properly due to issue happening only in automation.
   */
  moveMouseExpandScreeningView() {
    cy.get(`[data-cy="expandScreeningView"]`).trigger('mousemove');
  }

  restoreGridToDefaults() {
    userAggridCommon.restoreGridToDefaults();
    this.moveMouseExpandScreeningView();
  }

  selectSavedView(viewName: string) {
    userAggridCommon.selectSavedView(viewName);
    this.moveMouseExpandScreeningView();
  }

  clickFloatingFilterBtn() {
    return userAggridCommon.clickToggleFilterBtn();
  }

  /**
   * used to input values in the Filter header, need to open filter head using toggle filter button
   * @columnName is the actual name of the column in the ag grid
   */
  inputFilterValue(value: string, columnName: string) {
    cy.log(`Search Term: `, value);
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.inputFloatingFilterValue(value, columnName);
    this.moveMouseExpandScreeningView();
    cy.wait('@AlertsGrid');
  }

  sortColumn(colId: AlertsColumnID) {
    util.loadAgGridForWait('AlertsGrid');
    userAggridCommon.clickColumn(colId);
    cy.wait('@AlertsGrid');
    cy.wait(1000);
    this.moveMouseExpandScreeningView();
  }

  groupByColumn(colId: AlertsColumnID) {
    userAggridCommon.groupByColumn(colId);
    this.moveMouseExpandScreeningView();
  }

  expandGroup(groupName: string) {
    userAggridCommon.expandGroup(groupName);
    this.moveMouseExpandScreeningView();
  }
}
