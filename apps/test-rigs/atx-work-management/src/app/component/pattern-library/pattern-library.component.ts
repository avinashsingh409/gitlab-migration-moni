import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pattern-library',
  templateUrl: './pattern-library.component.html',
  styleUrls: ['./pattern-library.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PatternLibraryComponent {
  panelOpenState = false;
}
