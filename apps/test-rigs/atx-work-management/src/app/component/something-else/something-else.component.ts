import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {
  UntypedFormArray,
  UntypedFormBuilder,
  FormControl,
  UntypedFormGroup,
} from '@angular/forms';

@Component({
  selector: 'app-something-else',
  templateUrl: './something-else.component.html',
  styleUrls: ['./something-else.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SomethingElseComponent {
  panelOpenState = false;

  scenariosForm: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder) {
    this.scenariosForm = this.fb.group({
      name: '',
      scenarios: this.fb.array([]),
    });
  }

  get scenarios(): UntypedFormArray {
    return this.scenariosForm.get('scenarios') as UntypedFormArray;
  }

  newScenario(): UntypedFormGroup {
    return this.fb.group({
      description: '',
      probability: '',
      note: '',
    });
  }

  addscenarios() {
    this.scenarios.push(this.newScenario());
  }

  removeScenario(i: number) {
    this.scenarios.removeAt(i);
  }

  onSubmit() {
    console.log(this.scenariosForm.value);
  }
}
