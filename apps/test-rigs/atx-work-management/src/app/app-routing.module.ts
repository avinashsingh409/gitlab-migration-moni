import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TokenGuard } from '@atonix/shared/state/auth';
import { SomethingElseComponent } from './component/something-else/something-else.component';
import { PatternLibraryComponent } from './component/pattern-library/pattern-library.component';

const routes: Routes = [
  {
    path: 'issues',
    loadChildren: () =>
      import('@atonix/atx-work-management').then((m) => m.WorkManagementModule),
    canActivate: [TokenGuard],
  },
  { path: 'somethingelse', component: SomethingElseComponent },
  { path: 'patternlibrary', component: PatternLibraryComponent },
  { path: '', redirectTo: '/issues', pathMatch: 'full' },
  { path: '**', redirectTo: '/issues' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
