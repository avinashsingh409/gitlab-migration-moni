import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NavFacade } from '@atonix/atx-navigation';
import { getInitialNavState } from './store/nav.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  constructor(private navFacade: NavFacade) {}

  ngOnInit() {
    this.navFacade.configure(getInitialNavState());
  }
}
