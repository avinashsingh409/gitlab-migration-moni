/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import {
  TimeSliderPaneState,
  NavActions,
  IButtonData,
} from '@atonix/atx-navigation';
import { switchMap, map, tap, withLatestFrom } from 'rxjs/operators';
import {
  workItemSelect,
  workItemAddToAsset,
} from '@atonix/atx-work-management';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthSelectors } from '@atonix/shared/state/auth';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Injectable()
export class NavEffects {
  setApplicationAsset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NavActions.setApplicationAsset),
      switchMap((action) => {
        const id = action.asset as string;

        return [
          NavActions.configureNavigationButton({
            id: 'work_management',
            button: { url: '/home', urlParams: { id } },
          }),
        ];
      })
    )
  );

  selectNavButtons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NavActions.taskCenterLoad),
      switchMap((action) => {
        let timeSliderPaneState: TimeSliderPaneState = 'none';
        if (action.timeSlider) {
          timeSliderPaneState = 'hidden';
        }
        if (action.timeSlider && action.timeSliderOpened) {
          timeSliderPaneState = 'show';
        }

        return [
          NavActions.configureNavigationButton({
            id: 'work_management',
            button: { selected: action.taskCenterID === 'work_management' },
          }),
          NavActions.configureNavigationButton({
            id: 'something_else',
            button: { selected: action.taskCenterID === 'something_else' },
          }),
          NavActions.configureNavigationButton({
            id: 'pattern_library',
            button: { selected: action.taskCenterID === 'pattern_library' },
          }),
          NavActions.configureNavigationButton({
            id: 'asset_tree',
            button: { visible: action.assetTree ?? true },
          }),
          NavActions.configure({ configuration: { timeSliderPaneState } }),
        ];
      })
    )
  );

  selectWorkItem$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(workItemSelect),
        map((n) => n.id),
        tap((id) => {
          let url = `${window.location.protocol}//${window.location.host}/issues/i?iid=${id}`;
          const hostname = window.location.hostname;
          if (hostname !== 'localhost') {
            url = `${this.appConfig.baseSiteURL}/MD/issues/i?iid=${id}`;
          }

          window.open(url).focus();
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<any>,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}
}
