import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationModule } from '@atonix/atx-navigation';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { WorkManagementModule } from '@atonix/atx-work-management';
import { NavEffects } from './store/nav.effects';
import { SomethingElseComponent } from './component/something-else/something-else.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AtxMaterialModule } from '@atonix/atx-material';
import {
  JwtInterceptorService,
  logoutMetareducer,
  SharedStateAuthModule,
} from '@atonix/shared/state/auth';
import { environment } from '@env/environment';
import { PatternLibraryComponent } from './component/pattern-library/pattern-library.component';
import { SharedUiModule } from '@atonix/shared/ui';

@NgModule({
  declarations: [AppComponent, SomethingElseComponent, PatternLibraryComponent],
  imports: [
    AtxMaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedUiModule,
    SharedStateAuthModule.forRoot(),
    NavigationModule.forRoot(),
    StoreModule.forRoot({}, { metaReducers: [logoutMetareducer] }),
    EffectsModule.forRoot([NavEffects]),
    WorkManagementModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
