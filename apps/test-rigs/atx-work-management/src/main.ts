import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from '@env/environment';
import { APP_CONFIG, AppConfig } from '@atonix/app-config';
import { AppModule } from './app/app.module';

const windowHostName = window.location.hostname;
const assetConfigFile =
  windowHostName !== 'localhost'
    ? '/work-management/assets/config.json'
    : '/assets/config.json';
fetch(assetConfigFile)
  .then((response) => response.json())
  .catch((error) => {
    console.error(error);
  })
  .then((config: AppConfig) => {
    if (!config) {
      console.error('missing app config');
    } else {
      if (environment.production) {
        enableProdMode();
      }

      platformBrowserDynamic([{ provide: APP_CONFIG, useValue: config }])
        .bootstrapModule(AppModule)
        .catch((err) => console.error(err));
    }
  });
