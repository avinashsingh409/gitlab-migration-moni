/* eslint-disable ngrx/no-typed-global-store */
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { NavActions } from '@atonix/atx-navigation';

@Injectable()
export class AppFacade {
  constructor(private store: Store<any>) {}

  homeLoaded(route: string) {
    this.store.dispatch(
      NavActions.taskCenterLoad({
        taskCenterID: 'aHome',
        timeSlider: true,
        assetTree: false,
        asset: route,
      })
    );
  }
}
