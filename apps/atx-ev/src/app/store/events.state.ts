import { INavigationState, assetNavigatorButton } from '@atonix/atx-navigation';

export const initialEventsState: Partial<INavigationState> = {
  appName: 'Events Dashboard',
  navPaneState: 'open',
  logoRoute: '/home',
  showLoading: false,
  layoutItems: [
    {
      id: 'nNavbar',
      order: 1,
      icon: 'a360_sideToggle',
      text: 'NavbarBtn',
      hover: 'NavbarBtn',
      visible: true,
      enabled: true,
      selected: false,
      type: 'asset_button',
      data: {},
    },
  ],
  navigationItems: [
    { ...assetNavigatorButton, behavior: 'toggle', visible: false },
    {
      id: 'nSpacer',
      order: 2,
      icon: '',
      image: '',
      text: '',
      hover: '',
      visible: false,
      enabled: true,
      selected: true,
      data: {},
      type: 'spacer',
    },
    {
      id: 'events',
      order: 1,
      icon: 'a360_events',
      text: 'Events',
      hover: 'Events',
      visible: false,
      enabled: true,
      selected: false,
      url: '/events',
      newTab: false,
      type: 'route',
    },

    {
      id: 'dashboards',
      order: 3,
      icon: 'a360_views',
      text: 'Dashboards',
      hover: 'Dashboards',
      visible: false,
      enabled: true,
      selected: false,
      newTab: false,
      url: '/dashboards',
      type: 'route',
    },
  ],
};

export function getInitialEventsState(): Partial<INavigationState> {
  return initialEventsState;
}
