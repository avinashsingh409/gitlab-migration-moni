/* eslint-disable rxjs/no-unsafe-switchmap */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { switchMap, map, tap, withLatestFrom, take } from 'rxjs/operators';
import { TimeSliderPaneState, NavActions } from '@atonix/atx-navigation';
import { LoggerService } from '@atonix/shared/utils';
import { ROUTER_CANCEL, ROUTER_NAVIGATED } from '@ngrx/router-store';
import { RouteSelectors } from '@atonix/shared/state/router';
import { OnInitEffects } from '@ngrx/effects/src/lifecycle_hooks';
import { RouteService } from '../services/route.service';
import { Router } from '@angular/router';
import { PowerGenActions } from '@atonix/shared/state/power-gen';

@Injectable()
export class NavigationEffects {
  constructor(
    private actions$: Actions,
    private loggerService: LoggerService,
    private router: Router,
    private store: Store<any>,
    private routeService: RouteService
  ) {}

  helpClick$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NavActions.helpClick),
        map((n) => n.url),
        tap((url) => {
          window.open(url, '_blank');
        })
      ),
    { dispatch: false }
  );

  navigated$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      withLatestFrom(this.store.select(RouteSelectors.selectRouterRoute)),
      switchMap(([navigated, route]) => {
        if (route.startsWith('/events')) {
          this.router.navigate([], {
            queryParams: {
              id: null,
            },
            queryParamsHandling: 'merge',
          });
        }
        return this.routeService.setRouteActions(route);
      })
    )
  );
  navigatedCancel$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_CANCEL),
      // This type is used because a router_cancel event is emitted
      // instead of router_navigated when we add an asset to the url
      withLatestFrom(this.store.select(RouteSelectors.selectRouterRoute)),
      switchMap(([navigated, route]) => {
        if (route.startsWith('/events')) {
          this.router.navigate([], {
            queryParams: {
              id: null,
            },
            queryParamsHandling: 'merge',
          });
        }
        return this.routeService.setRouteActions(route);
      })
    )
  );

  setInitialAppState$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NavActions.setInitialAppState),
      withLatestFrom(this.store.select(RouteSelectors.selectRouterRoute)),
      switchMap(([navigated, route]) => {
        const retVal = this.routeService.setRouteActions(route);
        return [...retVal, PowerGenActions.enter()];
      })
    )
  );
}
