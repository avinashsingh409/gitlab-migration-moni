import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {
  routerReducer,
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { HomeComponent } from './components/home.component';
import { TokenGuard } from '@atonix/shared/state/auth';
import { RouteStateSerializer } from '@atonix/shared/state/router';
import { LazyNgModuleWithProvidersFactory } from '@atonix/shared/utils';
import { EventsUtilityGuard } from './guards/events-utility.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [TokenGuard] },
  {
    path: 'dashboards',
    loadChildren: () =>
      import('@atonix/atx-events').then((m) => m.EventsModule),
    canActivate: [EventsUtilityGuard],
  },
  {
    path: 'events',
    loadChildren: () =>
      import('@atonix/atx-asset-explorer').then(
        (module) =>
          new LazyNgModuleWithProvidersFactory(
            module.AssetExplorerModule.forRoot({ configType: 'PowerGen' })
          )
      ),
    canActivate: [EventsUtilityGuard],
  },
  { path: '**', redirectTo: '' },
];

export const routerStateConfig = {
  stateKey: 'router', // state name for routing state
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      relativeLinkResolution: 'legacy',
    }),
    StoreModule.forFeature(routerStateConfig.stateKey, routerReducer),
    StoreRouterConnectingModule.forRoot(routerStateConfig),
  ],
  exports: [RouterModule, StoreModule, StoreRouterConnectingModule],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: RouteStateSerializer,
    },
  ],
})
export class AppRoutingModule {}
export const AppRoutingComponents = [HomeComponent];
