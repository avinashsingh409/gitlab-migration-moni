import { Component, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthFacade } from '@atonix/shared/state/auth';
import { AppFacade } from '../store/app.facade';

@Component({
  selector: 'atx-app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  constructor(
    public facade: AppFacade,
    public authFacade: AuthFacade,
    private activatedRoute: ActivatedRoute
  ) {
    let result = this.activatedRoute.snapshot.queryParamMap.get('id');
    if (result === 'null' || result === 'undefined') {
      result = '';
    }
    facade.homeLoaded(result);
  }
}
