import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NavFacade } from '@atonix/atx-navigation';
import { AuthFacade } from '@atonix/shared/state/auth';
import { Observable, Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { getInitialEventsState } from './store/events.state';
import { NotificationBannerComponent } from '@atonix/shared/ui';
import { NotificationDialogComponent } from '@atonix/shared/ui';
import { showModalAnnouncement } from '@atonix/shared/utils';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';

@Component({
  selector: 'atx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  public isLoggedIn$: Observable<boolean>;
  unsubscribe$ = new Subject<void>();

  constructor(
    private navFacade: NavFacade,
    private authFacade: AuthFacade,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  ngOnInit(): void {
    this.authFacade.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val) {
          this.authFacade.getAllUIAccess();
        }
      });

    this.authFacade.uiAccessLoaded$
      .pipe(
        withLatestFrom(this.authFacade.hasEventsAccess$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([uiAccessLoaded, access]) => {
        if (uiAccessLoaded) {
          if (access) {
            this.navFacade.configure(getInitialEventsState());
            this.navFacade.setInitialAppState();
          }

          this.navFacade.configureNavigationButton('nSpacer', {
            visible: access,
          });
          this.navFacade.configureNavigationButton('events', {
            visible: access,
          });
          this.navFacade.configureNavigationButton('dashboards', {
            visible: access,
          });

          if (!access) {
            this.router.navigate(['']);
          }
        }
      });

    const windowHostName = window.location.hostname;
    if (
      this.appConfig.enableAnnouncement === 1 &&
      windowHostName.search(/.atonix.com/gi) === -1
    ) {
      this.snackbar.openFromComponent(NotificationBannerComponent, {
        verticalPosition: 'top',
        horizontalPosition: 'center',
        duration: 7000,
        panelClass: 'notification-banner-container',
      });
      if (showModalAnnouncement()) {
        this.dialog.open(NotificationDialogComponent);
      }
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
