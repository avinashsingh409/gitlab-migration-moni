import { Injectable } from '@angular/core';
import { NavActions } from '@atonix/atx-navigation';
import { Action } from '@ngrx/store';

@Injectable({
  providedIn: 'root',
})
export class RouteService {
  setRouteActions(route: string): Action[] {
    if (!route) {
      return this.homeRoute();
    }
    if (route.startsWith('/events')) {
      return [
        NavActions.configureNavigationButton({
          id: 'aHome',
          button: { selected: false },
        }),
        NavActions.configureNavigationButton({
          id: 'dashboards',
          button: { selected: false },
        }),
        NavActions.configureNavigationButton({
          id: 'events',
          button: { selected: true },
        }),
        NavActions.configure({
          configuration: { timeSliderPaneState: 'none' },
        }),
        // NavActions.routeChange({ route }),
      ];
    } else if (route.startsWith('/dashboards')) {
      return [
        NavActions.configureNavigationButton({
          id: 'aHome',
          button: { selected: false },
        }),
        NavActions.configureNavigationButton({
          id: 'events',
          button: { selected: false },
        }),
        NavActions.configureNavigationButton({
          id: 'dashboards',
          button: { selected: true },
        }),
        NavActions.configure({
          configuration: { timeSliderPaneState: 'show' },
        }),
      ];
    } else {
      return this.homeRoute();
    }
  }

  homeRoute(): Action[] {
    return [
      NavActions.configureNavigationButton({
        id: 'aHome',
        button: { selected: true },
      }),
      NavActions.configureNavigationButton({
        id: 'events',
        button: { selected: false },
      }),
      NavActions.configureNavigationButton({
        id: 'dashboards',
        button: { selected: false },
      }),
      NavActions.configure({
        configuration: { timeSliderPaneState: 'none' },
      }),
    ];
  }
}
