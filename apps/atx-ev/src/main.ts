import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from '@env/environment';
import { LicenseManager } from '@ag-grid-enterprise/all-modules';
import { AppModule } from './app/app.module';
import { APP_CONFIG } from '@atonix/app-config';

LicenseManager.setLicenseKey(
  // eslint-disable-next-line max-len
  'CompanyName=SHI International Corp._on_behalf_of_Atonix Digital, LLC,LicensedApplication=Asset 360,LicenseType=SingleApplication,LicensedConcurrentDeveloperCount=5,LicensedProductionInstancesCount=3,AssetReference=AG-036826,SupportServicesEnd=15_February_2024_[v2]_MTcwNzk1NTIwMDAwMA==7726d034a18fb6a89602a2168ed8c24b'
);

const windowHostName = window.location.hostname;
const assetConfigFile =
  windowHostName !== 'localhost'
    ? '/ev/assets/config.json'
    : '/assets/config.json';
fetch(assetConfigFile)
  .then((response) => response.json())
  .catch((error) => {
    console.error(error);
  })
  .then((config) => {
    if (!config) {
      console.error('missing app config');
    } else {
      if (environment.production) {
        enableProdMode();
      }
      if (windowHostName !== 'localhost') {
        config.baseSiteURL = `https://${window.location.hostname}`;
        config.authCallback = `https://${window.location.hostname}/MD/`;
      }

      platformBrowserDynamic([{ provide: APP_CONFIG, useValue: config }])
        .bootstrapModule(AppModule)
        .catch((err) => console.error(err));
    }
  });
