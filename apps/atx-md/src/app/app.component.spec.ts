import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { AppComponent } from './app.component';
import { BehaviorSubject, Subject } from 'rxjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppFacade } from './store/facade/app.facade';
import { ToastService } from '@atonix/shared/utils';
import { AuthFacade, initialAuthState } from '@atonix/shared/state/auth';
import { provideMockStore } from '@ngrx/store/testing';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { MatSnackBar } from '@angular/material/snack-bar';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let navFacade: NavFacade;
  let mockToastService: ToastService;
  let snackBar: MatSnackBar;
  let authFacade: AuthFacade;
  let dialog: MatDialogModule;
  const initialState: any = { auth: initialAuthState };

  beforeEach(() => {
    authFacade = createMockWithValues(AuthFacade, {
      isLoggedIn$: new BehaviorSubject<boolean>(false),
      navItemsAccess$: new BehaviorSubject<any>(null),
      uiAccessLoaded$: new BehaviorSubject<boolean>(false),
    });
    snackBar = createMockWithValues(MatSnackBar, {
      openFromComponent: jest.fn(),
    });
    mockToastService = createMockWithValues(ToastService, {
      openSnackBar: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FlexLayoutModule,
        MatIconModule,
        MatCardModule,
        MatDialogModule,
        NoopAnimationsModule,
      ],
      providers: [
        {
          provide: NavFacade,
          useValue: navFacade,
        },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        { provide: APP_CONFIG, useValue: AppConfig },
        {
          provide: ToastService,
          useValue: mockToastService,
        },
        {
          provide: MatSnackBar,
          useValue: snackBar,
        },
        {
          provide: MatDialogModule,
          useValue: dialog,
        },
        { provide: AuthFacade, useValue: authFacade },
        provideMockStore({
          initialState: {
            auth: initialState,
          },
        }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [AppComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });
});
