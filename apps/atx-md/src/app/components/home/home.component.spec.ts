import { HomeComponent } from './home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppFacade } from '../../store/facade/app.facade';
import { AuthFacade, initialAuthState } from '@atonix/shared/state/auth';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';
import { AtxMaterialModule } from '@atonix/atx-material';
import { render, screen, fireEvent } from '@testing-library/angular';
import { BehaviorSubject } from 'rxjs';
import { NavFacade } from '@atonix/atx-navigation';
import { MATERIAL_SANITY_CHECKS } from '@angular/material/core';
describe('HomeComponent', () => {
  let appFacade: AppFacade;
  let authFacade: AuthFacade;
  let navFacade: NavFacade;

  async function renderHomeComponent() {
    appFacade = createMock(AppFacade);
    appFacade.homeLoaded = jest.fn().mockReturnThis();
    navFacade = createMockWithValues(NavFacade, {
      queryParams$: new BehaviorSubject<string>(''),
    });
    await render(HomeComponent, {
      imports: [RouterTestingModule, AtxMaterialModule],
      providers: [
        { provide: NavFacade, useValue: navFacade },
        { provide: AuthFacade, useValue: authFacade },
        { provide: MATERIAL_SANITY_CHECKS, useValue: false },
        {
          provide: AppFacade,
          useValue: appFacade,
        },
      ],
    });
  }

  test('main content should not be visible when access not loaded', async () => {
    authFacade = createMockWithValues(AuthFacade, {
      uiAccessLoaded$: new BehaviorSubject<boolean>(false),
      hasMDAccess$: new BehaviorSubject<boolean>(false),
    });
    await renderHomeComponent();
    expect(screen.queryByTestId('main_content')).not.toBeInTheDocument();
  });

  test('md access denied should be shown if the user does not have access', async () => {
    authFacade = createMockWithValues(AuthFacade, {
      uiAccessLoaded$: new BehaviorSubject<boolean>(true),
      hasMDAccess$: new BehaviorSubject<boolean>(false),
    });
    await renderHomeComponent();

    expect(screen.getByTestId('main_content')).toBeInTheDocument();
    expect(screen.getByTestId('access_denied')).toBeInTheDocument();
  });
});
