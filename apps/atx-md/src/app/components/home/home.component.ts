import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavFacade } from '@atonix/atx-navigation';
import { AuthFacade } from '@atonix/shared/state/auth';
import { Observable } from 'rxjs';
import { AppFacade } from '../../store/facade/app.facade';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  queryParams$: Observable<any>;

  constructor(
    public authFacade: AuthFacade,
    public facade: AppFacade,
    private navFacade: NavFacade,
    private activatedRoute: ActivatedRoute
  ) {
    this.queryParams$ = this.navFacade.queryParams$;

    let result = this.activatedRoute.snapshot.queryParamMap.get('id');
    if (result === 'null' || result === 'undefined') {
      result = '';
    }

    facade.homeLoaded(result);
  }
}
