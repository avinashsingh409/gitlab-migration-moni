import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  Inject,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, withLatestFrom } from 'rxjs/operators';
import { IButtonData, NavFacade } from '@atonix/atx-navigation';
import { slideInAnimation } from './animations';
import { getInitialNavState } from './store/state/nav.state';
import { AuthFacade, AuthService } from '@atonix/shared/state/auth';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { NotificationBannerComponent } from '@atonix/shared/ui';
import { NotificationDialogComponent } from '@atonix/shared/ui';
import { showModalAnnouncement } from '@atonix/shared/utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject<void>();

  constructor(
    private navFacade: NavFacade,
    private authFacade: AuthFacade,
    private authservice: AuthService,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private router: Router,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  ngOnInit(): void {
    const windowHostName = window.location.hostname;
    if (
      this.appConfig.enableAnnouncement === 1 &&
      windowHostName.search(/.atonix.com/gi) === -1
    ) {
      this.snackbar.openFromComponent(NotificationBannerComponent, {
        verticalPosition: 'top',
        horizontalPosition: 'center',
        duration: 7000,
        panelClass: 'notification-banner-container',
      });
      if (showModalAnnouncement()) {
        this.dialog.open(NotificationDialogComponent);
      }
    }

    this.authservice.onRefreshToken$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val) {
          this.authFacade.getAllUIAccess();
        }
      });

    this.authFacade.isLoggedIn$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        if (val) {
          this.authFacade.getAllUIAccess();
        }
      });

    this.authFacade.uiAccessLoaded$
      .pipe(
        withLatestFrom(this.authFacade.navItemsAccess$),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(([uiAccessLoaded, navItemsAccess]) => {
        if (uiAccessLoaded) {
          const alertsAccess = navItemsAccess.alertsAccess?.CanView ?? false;
          const issuesManagementAccess =
            navItemsAccess.issuesManagementAccess?.CanView ?? false;
          const dataExplorerAccess =
            navItemsAccess.dataExplorerAccess?.CanView ?? false;
          const dashboardsAccess =
            navItemsAccess.dashboardsAccess?.CanView ?? false;
          const modelConfigAccess =
            navItemsAccess.modelConfigAccess?.CanView ?? false;
          const opModelConfigAccess =
            navItemsAccess.opModelConfigAccess?.CanView ?? false;
          const userAdminAccess =
            navItemsAccess.userAdminAccess?.CanView ?? false;
          const pinConfigAccess =
            navItemsAccess.pinConfigAccess?.CanView ?? false;
          const assetConfigAccess =
            navItemsAccess.assetConfigAccess?.CanView ?? false;
          const tagConfigAccess =
            navItemsAccess.tagConfigAccess?.CanView ?? false;
          const alphaAccess = navItemsAccess.alphaAccess?.CanView ?? false;
          const alpha2Access = navItemsAccess.alpha2Access?.CanView ?? false;
          const beta2Access = navItemsAccess.beta2Access?.CanView ?? false;
          const newAssetConfigAccess = assetConfigAccess || tagConfigAccess;
          const newModelConfigAccess = alphaAccess && modelConfigAccess;
          this.navFacade.configure(
            getInitialNavState(this.appConfig.baseSiteURL)
          );
          this.navFacade.configureNavigationButton('nSpacer', {
            visible:
              (alertsAccess ||
                issuesManagementAccess ||
                dataExplorerAccess ||
                dashboardsAccess) &&
              (modelConfigAccess || opModelConfigAccess || userAdminAccess),
          });
          this.navFacade.configureNavigationButton('alerts', {
            visible: alertsAccess,
          });
          this.navFacade.configureNavigationButton('work_management', {
            visible: issuesManagementAccess,
          });
          this.navFacade.configureNavigationButton('data-explorer', {
            visible: dataExplorerAccess,
          });
          this.navFacade.configureNavigationButton('view-explorer', {
            visible: dashboardsAccess,
          });
          this.navFacade.configureNavigationButton('nSpacer2', {
            visible:
              modelConfigAccess || opModelConfigAccess || userAdminAccess,
          });
          this.navFacade.configureNavigationButton('modelConfig', {
            visible: modelConfigAccess,
          });
          this.navFacade.configureNavigationButton('opModeConfig', {
            visible: opModelConfigAccess,
          });
          this.navFacade.configureNavigationButton('assetConfig', {
            visible: newAssetConfigAccess,
          });
          this.navFacade.configureNavigationButton('userAdmin', {
            visible: userAdminAccess,
          });

          const url = this.router.url;
          if (
            (url.includes('/alerts') && !alertsAccess) ||
            (url.includes('/issues') && !issuesManagementAccess) ||
            (url.includes('/data-explorer') && !dataExplorerAccess) ||
            (url.includes('/dashboards') && !dashboardsAccess) ||
            (url.includes('/user-admin') && !userAdminAccess) ||
            (url.includes('/system-config') && !pinConfigAccess) ||
            (url.includes('/asset-config') && !newAssetConfigAccess)
          ) {
            this.router.navigate(['']);
          }
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
