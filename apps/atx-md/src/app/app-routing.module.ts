import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {
  routerReducer,
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { HomeComponent } from './components/home/home.component';
import {
  routerStateConfig,
  RouteStateSerializer,
} from '@atonix/shared/state/router';
import { routes } from './app-routes';

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: false,
      relativeLinkResolution: 'legacy',
    }),
    StoreModule.forFeature(routerStateConfig.stateKey, routerReducer),
    StoreRouterConnectingModule.forRoot(routerStateConfig),
  ],
  exports: [RouterModule, StoreModule, StoreRouterConnectingModule],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: RouteStateSerializer,
    },
  ],
})
export class AppRoutingModule {}
export const AppRoutingComponents = [HomeComponent];
