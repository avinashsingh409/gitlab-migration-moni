/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { AuthActions, AuthFacade } from '@atonix/shared/state/auth';
import { processResourceAccessType } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import { isNil } from 'lodash';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { AssetConfigActions } from '@atonix/atx-asset-config';

@Injectable({
  providedIn: 'root',
})
export class AssetConfigUtilityGuard implements CanActivate {
  constructor(
    private router: Router,
    public service: AuthorizationFrameworkService,
    public authFacade: AuthFacade,
    public store: Store<any>,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAccessInApi(): Observable<boolean> {
    return this.service.getAllUIAccess().pipe(
      take(1),
      map(
        (resourceAccessTypes) =>
          AuthActions.getAllUIAccessSuccess({ resourceAccessTypes }),
        catchError((error: unknown) => {
          return of(false);
        })
      ),
      tap((action) => this.store.dispatch(action)),
      map((val) => {
        const assetConfigAccess = processResourceAccessType(
          'UI/Utility/AssetConfig',
          val.resourceAccessTypes
        );
        const tagConfigAccess = processResourceAccessType(
          'UI/Utility/TagConfig',
          val.resourceAccessTypes
        );

        return assetConfigAccess?.CanView || tagConfigAccess?.CanView;
      }),
      catchError((_error: unknown) => {
        return of(false);
      })
    );
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authFacade.newAssetConfigUtilityAccess$.pipe(
      withLatestFrom(this.authFacade.isLoggedIn$, this.authFacade.authState$),
      switchMap(([assetConfigAccess, isLoggedIn, authState]) => {
        if (!isLoggedIn) {
          this.authFacade.authInit('', '');
        }
        if (!this.appConfig.enableAssetConfig) {
          this.router.navigate(['']);
        }
        if (!isNil(assetConfigAccess)) {
          if (!assetConfigAccess) {
            this.router.navigate(['']);
          } else {
            this.store.dispatch(AssetConfigActions.fillTabs(authState));
          }
          return of(assetConfigAccess);
        } else {
          return this.getAccessInApi();
        }
      })
    );
  }
}
