/* eslint-disable ngrx/no-typed-global-store */
import { Inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AppConfig, APP_CONFIG } from '@atonix/app-config';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { AuthActions, AuthFacade } from '@atonix/shared/state/auth';
import { processResourceAccessType } from '@atonix/shared/utils';
import { Store } from '@ngrx/store';
import { isNil } from 'lodash';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ModelConfigUtilityGuard implements CanActivate {
  constructor(
    private router: Router,
    public service: AuthorizationFrameworkService,
    public authFacade: AuthFacade,
    public store: Store<any>,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {}

  getAccessInApi(): Observable<boolean> {
    return this.service.getAllUIAccess().pipe(
      take(1),
      map(
        (resourceAccessTypes) =>
          AuthActions.getAllUIAccessSuccess({ resourceAccessTypes }),
        catchError((error: unknown) => {
          return of(false);
        })
      ),
      tap((action) => this.store.dispatch(action)),
      map((val) => {
        const access = processResourceAccessType(
          'UI/Utility/ModelConfig',
          val.resourceAccessTypes
        );
        return access?.CanView;
      }),
      catchError((error: unknown) => {
        return of(false);
      })
    );
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authFacade.modelConfigUtilityAccess$.pipe(
      withLatestFrom(this.authFacade.isLoggedIn$),
      switchMap(([modelConfigAccess, isLoggedIn]) => {
        if (!isLoggedIn) {
          this.authFacade.authInit('', '');
        }
        if (!this.appConfig.enableModelConfig) {
          this.router.navigate(['']);
        }
        if (!isNil(modelConfigAccess)) {
          if (!modelConfigAccess.CanView) {
            this.router.navigate(['']);
          }
          return of(modelConfigAccess.CanView);
        } else {
          return this.getAccessInApi();
        }
      })
    );
  }
}
