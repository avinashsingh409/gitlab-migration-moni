/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/avoid-mapping-selectors */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { isNil } from '@atonix/atx-core';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { AuthActions, AuthFacade } from '@atonix/shared/state/auth';
import { processResourceAccessType } from '@atonix/shared/utils';

@Injectable({
  providedIn: 'root',
})
export class UserAdminUtilityGuard implements CanActivate {
  constructor(
    private router: Router,
    public service: AuthorizationFrameworkService,
    public authFacade: AuthFacade,
    public store: Store<any>
  ) {}

  getAccessInApi(): Observable<boolean> {
    return this.service.getAllUIAccess().pipe(
      take(1),
      map(
        (resourceAccessTypes) =>
          AuthActions.getAllUIAccessSuccess({ resourceAccessTypes }),
        catchError((error: unknown) => {
          return of(false);
        })
      ),
      tap((action) => this.store.dispatch(action)),
      map((val) => {
        const access = processResourceAccessType(
          'UI/Utility/UserAdmin',
          val.resourceAccessTypes
        );
        return access.CanView;
      }),
      catchError((error: unknown) => {
        return of(false);
      })
    );
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authFacade.userAdminUtilityAccess$.pipe(
      withLatestFrom(this.authFacade.isLoggedIn$),
      switchMap(([userAdminAccess, isLoggedIn]) => {
        if (!isLoggedIn) {
          this.authFacade.authInit('', '');
        }
        if (!isNil(userAdminAccess)) {
          if (!userAdminAccess.CanView) {
            this.router.navigate(['']);
          }
          return of(userAdminAccess.CanView);
        } else {
          return this.getAccessInApi();
        }
      })
    );
  }
}
