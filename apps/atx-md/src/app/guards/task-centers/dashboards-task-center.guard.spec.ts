import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { ToastService } from '@atonix/shared/utils';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { DashboardsTaskCenterGuard } from './dashboards-task-center.guard';
import { RouterTestingModule } from '@angular/router/testing';
import {
  createMock,
  createMockWithValues,
} from '@testing-library/angular/jest-utils';

describe('DashboardsTaskCenterGuard', () => {
  let guard: DashboardsTaskCenterGuard;
  let appService: AuthorizationFrameworkService;

  beforeEach(() => {
    appService = createMockWithValues(AuthorizationFrameworkService, {
      getAllUIAccess: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: AuthorizationFrameworkService,
          useValue: appService,
        },
        provideMockStore({}),
      ],
    });
    guard = TestBed.inject(DashboardsTaskCenterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
