import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { DataExplorerTaskCenterGuard } from './data-explorer-task-center.guard';
import { RouterTestingModule } from '@angular/router/testing';
import { createMockWithValues } from '@testing-library/angular/jest-utils';
describe('DataExplorerTaskCenterGuard', () => {
  let guard: DataExplorerTaskCenterGuard;
  let appService: AuthorizationFrameworkService;

  beforeEach(() => {
    appService = createMockWithValues(AuthorizationFrameworkService, {
      getAllUIAccess: jest.fn(),
    });

    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {
          provide: AuthorizationFrameworkService,
          useValue: appService,
        },
        provideMockStore({}),
      ],
    });
    guard = TestBed.inject(DataExplorerTaskCenterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
