/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/avoid-mapping-selectors */
/* eslint-disable ngrx/select-style */
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  ParamMap,
  Routes,
  Router,
} from '@angular/router';
import { ISecurityRights, isNil } from '@atonix/atx-core';
import { AuthorizationFrameworkService } from '@atonix/shared/api';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { AuthActions, AuthFacade } from '@atonix/shared/state/auth';
import { processResourceAccessType } from '@atonix/shared/utils';
import { produce } from 'immer';

@Injectable({
  providedIn: 'root',
})
export class DataExplorerTaskCenterGuard implements CanActivate {
  constructor(
    public service: AuthorizationFrameworkService,
    public authFacade: AuthFacade,
    public store: Store<any>,
    private router: Router
  ) {}

  getAccessInApi(next: ActivatedRouteSnapshot): Observable<boolean> {
    return this.service.getAllUIAccess().pipe(
      take(1),
      map(
        (resourceAccessTypes) =>
          AuthActions.getAllUIAccessSuccess({ resourceAccessTypes }),
        catchError(() => {
          return of(false);
        })
      ),
      tap((action) => this.store.dispatch(action)),
      map((val) => {
        const dataExplorerAccess = processResourceAccessType(
          'UI/TaskCenter/DataExplorer',
          val.resourceAccessTypes
        );
        return dataExplorerAccess.CanView;
      }),
      catchError(() => {
        return of(false);
      })
    );
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.authFacade.dataExplorerTaskCenterAccess$.pipe(
      withLatestFrom(this.authFacade.isLoggedIn$),
      switchMap(([dataExplorerAccess, isLoggedIn]) => {
        if (!isLoggedIn) {
          this.authFacade.authInit('', '');
        }
        if (!isNil(dataExplorerAccess)) {
          if (!dataExplorerAccess.CanView) {
            this.router.navigate(['']);
          }
          return of(dataExplorerAccess.CanView);
        } else {
          return this.getAccessInApi(next);
        }
      })
    );
  }
}
