import { Routes } from '@angular/router';
import { TokenGuard } from '@atonix/shared/state/auth';
import { HomeComponent } from './components/home/home.component';
import { AlertsTaskCenterGuard } from './guards/task-centers/alerts-task-center.guard';
import { DashboardsTaskCenterGuard } from './guards/task-centers/dashboards-task-center.guard';
import { DataExplorerTaskCenterGuard } from './guards/task-centers/data-explorer-task-center.guard';
import { IssuesManagementTaskCenterGuard } from './guards/task-centers/issues-management-task-center.guard';
import { AssetConfigUtilityGuard } from './guards/utilities/asset-config-utility.guard';
import { ModelConfigUtilityGuard } from './guards/utilities/model-config-utility.guard';
import { PinConfigUtilityGuard } from './guards/utilities/pin-config-utility.guard';
import { UserAdminUtilityGuard } from './guards/utilities/user-admin-utility.guard';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [TokenGuard],
    data: { title: 'Prometheus APM' },
  },
  {
    path: 'alerts',
    loadChildren: () =>
      import('@atonix/atx-alerts').then((m) => m.AlertsModule),
    canActivate: [AlertsTaskCenterGuard],
    data: { title: 'Alerts' },
  },
  {
    path: 'dashboards',
    loadChildren: () =>
      import('@atonix/atx-view-explorer').then((m) => m.ViewExplorerModule),
    canActivate: [DashboardsTaskCenterGuard],
    data: { title: 'Dashboards' },
  },
  {
    path: 'diagnostic-drilldown',
    loadChildren: () =>
      import('@atonix/atx-diagnostic-drilldown').then(
        (m) => m.DiagnosticDrilldownModule
      ),
    canActivate: [AlertsTaskCenterGuard],
    data: { title: 'Diagnostic Drilldown' },
  },
  {
    path: 'issues',
    loadChildren: () =>
      import('@atonix/atx-work-management').then((m) => m.WorkManagementModule),
    canActivate: [IssuesManagementTaskCenterGuard],
    data: { title: 'Issues Management' },
  },
  {
    path: 'system-config',
    loadChildren: () =>
      import('@atonix/atx-system-config').then((m) => m.SystemConfigModule),
    canActivate: [PinConfigUtilityGuard],
    data: { title: 'Pin Configuration' },
  },
  {
    path: 'data-explorer',
    loadChildren: () =>
      import('@atonix/atx-data-explorer-v2').then(
        (m) => m.DataExplorerModuleV2
      ),
    canActivate: [DataExplorerTaskCenterGuard],
    data: { title: 'Data Explorer' },
  },
  {
    path: 'user-admin',
    loadChildren: () =>
      import('@atonix/atx-user-admin').then((m) => m.UserAdminModule),
    canActivate: [UserAdminUtilityGuard],
    data: { title: 'User Admin' },
  },
  {
    path: 'model-config',
    loadChildren: () =>
      import('@atonix/atx-model-config').then((m) => m.ModelConfigModule),
    canActivate: [ModelConfigUtilityGuard],
    data: { title: 'Model Config' },
  },
  {
    path: 'asset-config',
    loadChildren: () =>
      import('@atonix/atx-asset-config').then((m) => m.AssetConfigModule),
    canActivate: [AssetConfigUtilityGuard],
    data: { title: 'Asset Config' },
  },
  {
    path: 'account-settings',
    loadChildren: () =>
      import('@atonix/atx-account-settings').then(
        (m) => m.AccountSettingsModule
      ),
    canActivate: [TokenGuard],
    data: { title: 'Account Settings' },
  },
  { path: '**', redirectTo: '' },
];
