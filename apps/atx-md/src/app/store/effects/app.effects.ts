/* eslint-disable ngrx/no-typed-global-store */
/* eslint-disable ngrx/prefer-concat-latest-from */
/* eslint-disable ngrx/no-multiple-actions-in-effects */
/* eslint-disable ngrx/prefer-effect-callback-in-block-statement */
import { Inject, Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, tap, switchMap, withLatestFrom } from 'rxjs/operators';
import {
  NavActions,
  selectNavItems,
  TimeSliderPaneState,
} from '@atonix/atx-navigation';
import {
  ROUTER_CANCEL,
  ROUTER_NAVIGATED,
  ROUTER_NAVIGATION,
} from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { RouteSelectors } from '@atonix/shared/state/router';
import { Title } from '@angular/platform-browser';

@Injectable()
export class AppEffects {
  constructor(
    private actions$: Actions,
    private store: Store<any>,
    private titleService: Title
  ) {}

  // Known task centers are
  // 'work_management','alerts','pa-map','pa-scorecard', 'aHome', 'data-explorer', 'viewExplorer'
  taskCenterLoad$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NavActions.taskCenterLoad),
      switchMap((action) => {
        let timeSliderPaneState: TimeSliderPaneState = 'none';
        if (action.timeSlider) {
          timeSliderPaneState = 'hidden';
        }
        if (action.timeSlider && action.timeSliderOpened) {
          timeSliderPaneState = 'show';
        }
        return [
          NavActions.configureNavigationButton({
            id: 'aHome',
            button: { selected: action.taskCenterID === 'aHome' },
          }),
          NavActions.configureNavigationButton({
            id: 'asset_tree',
            button: { visible: action.assetTree ?? true },
          }),
          NavActions.configure({ configuration: { timeSliderPaneState } }),
        ];
      })
    )
  );

  // This will observe the "ROUTER_NAVIGATION" action of the ngrx router store.
  // "ROUTER_NAVIGATION" action is equivalent to the NavigationStart event of the Router.
  navigation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      withLatestFrom(this.store.select(RouteSelectors.selectRouterRoute)),
      switchMap(([navigated, route]) => {
        if (route === '/alerts') {
          return [
            NavActions.showRightTrayIcon(),
            NavActions.routeChange({ route }),
          ];
        } else {
          return [
            NavActions.hideRightTrayIcon(),
            NavActions.routeChange({ route }),
          ];
        }
      })
    )
  );

  navigationCanceled$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ROUTER_CANCEL),
        map((router: any) => router?.payload?.routerState?.data),
        withLatestFrom(this.store.select(RouteSelectors.selectRouterTitle)),
        tap(([data, title]) => {
          this.titleService.setTitle(data?.title ?? title);
        })
      ),
    { dispatch: false }
  );

  navigationTitleSwitcher$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ROUTER_NAVIGATION),
        withLatestFrom(this.store.select(RouteSelectors.selectRouterTitle)),
        tap(([navigated, title]) => {
          this.titleService.setTitle(title);
        })
      ),
    { dispatch: false }
  );

  navigated$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATED),
      withLatestFrom(
        this.store.select(RouteSelectors.selectRouterRoute),
        this.store.select(selectNavItems)
      ),
      switchMap(([navigated, route, navItems]) => {
        const navItem = navItems.find(({ url }) => route.includes(url));
        const actions = [
          NavActions.navItemSetSelected({ navItemId: navItem?.id }),
        ];

        if (route.includes('/system-config/time-range')) {
          return [
            ...actions,
            NavActions.showAssetTreeNavItemOnly({ show: true }),
          ];
        } else if (route.includes('/user-admin')) {
          return [
            ...actions,
            NavActions.configureNavigationButton({
              id: 'asset_tree',
              button: { visible: false },
            }),
            NavActions.configure({
              configuration: { timeSliderPaneState: 'none' },
            }),
            NavActions.showAssetTreeNavItemOnly({ show: false }),
          ];
        } else if (route.includes('/account-settings')) {
          return [
            ...actions,
            NavActions.configureNavigationButton({
              id: 'asset_tree',
              button: { visible: false },
            }),
            NavActions.configure({
              configuration: { timeSliderPaneState: 'none' },
            }),
            NavActions.showAssetTreeNavItemOnly({ show: false }),
          ];
        } else if (route.includes('/diagnostic-drilldown')) {
          return [NavActions.navItemSetSelected({ navItemId: 'alerts' })];
        } else {
          return [
            ...actions,
            NavActions.showAssetTreeNavItemOnly({ show: false }),
          ];
        }
      })
    )
  );
}
