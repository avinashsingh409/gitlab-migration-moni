import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationModule } from '@atonix/atx-navigation';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppRoutingComponents, AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppEffects } from './store/effects/app.effects';
import { AppFacade } from './store/facade/app.facade';
import { extModules } from '../../../build-specifics';

import {
  JwtInterceptorService,
  loggingMetareducer,
  logoutMetareducer,
  SharedStateAuthModule,
  TokenGuard,
} from '@atonix/shared/state/auth';
import { SharedUtilsModule } from '@atonix/shared/utils';
import { environment } from '@env/environment';
import { AssetTreeModule } from '@atonix/atx-asset-tree';
import { AtxMaterialModule } from '@atonix/atx-material';
import { GlobalErrorHandler } from './global-error-handler';
import { SharedUiModule } from '@atonix/shared/ui';
import { AlertsTaskCenterGuard } from './guards/task-centers/alerts-task-center.guard';
import { IssuesManagementTaskCenterGuard } from './guards/task-centers/issues-management-task-center.guard';
import { DataExplorerTaskCenterGuard } from './guards/task-centers/data-explorer-task-center.guard';
import { DashboardsTaskCenterGuard } from './guards/task-centers/dashboards-task-center.guard';
import { UserAdminUtilityGuard } from './guards/utilities/user-admin-utility.guard';
import { PinConfigUtilityGuard } from './guards/utilities/pin-config-utility.guard';

@NgModule({
  declarations: [AppComponent, AppRoutingComponents],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    AtxMaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedUiModule,
    AssetTreeModule.forRoot(),
    NavigationModule.forRoot(),
    SharedStateAuthModule.forRoot(),
    SharedUtilsModule.forRoot(),
    StoreModule.forRoot(
      {},
      {
        metaReducers: environment.production
          ? [logoutMetareducer]
          : [logoutMetareducer, loggingMetareducer],
        // runtimeChecks: {
        //   strictStateImmutability: true,
        //   strictActionImmutability: true,
        //   strictStateSerializability: true,
        //   strictActionSerializability: true,
        //   strictActionWithinNgZone: true,
        // },
      }
    ),
    EffectsModule.forRoot([AppEffects]),
    extModules,
  ],
  providers: [
    AppFacade,
    TokenGuard,
    AlertsTaskCenterGuard,
    IssuesManagementTaskCenterGuard,
    DataExplorerTaskCenterGuard,
    DashboardsTaskCenterGuard,
    UserAdminUtilityGuard,
    PinConfigUtilityGuard,
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useExisting: JwtInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
